---
title: 'adchithooku song lyrics'
album: 'Viswasam'
artist: 'D Imman'
lyricist: 'Viveka'
director: 'Siva'
path: '/albums/viswasam-song-lyrics'
song: 'Adchithooku'
image: ../../images/albumart/viswasam.jpg
date: 2019-01-10
singers:
- D.Imman
- Aditya Gadhavi
- Narayanan
lang: tamil
youtubeLink: "https://www.youtube.com/embed/mzNbJKKJdSw"
type: 'mass'
---



<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">

  <div class="lyrico-lyrics-wrapper">Heyy thunak thunak
<input type="checkbox" class="lyrico-select-lyric-line" value="Heyy thunak thunak"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunak thunak
<input type="checkbox" class="lyrico-select-lyric-line" value="Thunak thunak"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhin dhak dhindhaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhin dhak dhindhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey thunnaakk thunnaak
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey thunnaakk thunnaak"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhin dhak dhindhaa (2 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhin dhak dhindhaa "/></div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Angaali pangaalai vaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Angaali pangaalai vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini aattam thaan eppodhum
<input type="checkbox" class="lyrico-select-lyric-line" value="Ini aattam thaan eppodhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi adi
<input type="checkbox" class="lyrico-select-lyric-line" value="Adi adi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Mangaatha katta pola
<input type="checkbox" class="lyrico-select-lyric-line" value="Mangaatha katta pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha vattaaram naam kaiyil
<input type="checkbox" class="lyrico-select-lyric-line" value="Indha vattaaram naam kaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudi pudi
<input type="checkbox" class="lyrico-select-lyric-line" value="Pudi pudi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Hey adchithooku adchithooku
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey adchithooku adchithooku"/>
</div>
<div class="lyrico-lyrics-wrapper">Adchithooku
<input type="checkbox" class="lyrico-select-lyric-line" value="Adchithooku"/>
</div>
<div class="lyrico-lyrics-wrapper">Adchithooku thooku adchithooku
<input type="checkbox" class="lyrico-select-lyric-line" value="Adchithooku thooku adchithooku"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey adchithooku adchithooku
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey adchithooku adchithooku"/>
</div>
<div class="lyrico-lyrics-wrapper">Adchithooku
<input type="checkbox" class="lyrico-select-lyric-line" value="Adchithooku"/>
</div>
<div class="lyrico-lyrics-wrapper">Adchithooku thooku adchithooku
<input type="checkbox" class="lyrico-select-lyric-line" value="Adchithooku thooku adchithooku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Heyy thunak thunak
<input type="checkbox" class="lyrico-select-lyric-line" value="Heyy thunak thunak"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunak thunak
<input type="checkbox" class="lyrico-select-lyric-line" value="Thunak thunak"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhin dhak dhindhaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhin dhak dhindhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey thunnaakk thunnaak
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey thunnaakk thunnaak"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhin dhak dhindhaa (2 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhin dhak dhindhaa"/></div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Angaali pangaalai vaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Angaali pangaalai vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini aattam thaan eppodhum
<input type="checkbox" class="lyrico-select-lyric-line" value="Ini aattam thaan eppodhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi adi
<input type="checkbox" class="lyrico-select-lyric-line" value="Adi adi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Mangaatha katta pola
<input type="checkbox" class="lyrico-select-lyric-line" value="Mangaatha katta pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha vattaaram naam kaiyil
<input type="checkbox" class="lyrico-select-lyric-line" value="Indha vattaaram naam kaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudi pudi
<input type="checkbox" class="lyrico-select-lyric-line" value="Pudi pudi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Hey adchithooku adchithooku
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey adchithooku adchithooku"/>
</div>
<div class="lyrico-lyrics-wrapper">Adchithooku
<input type="checkbox" class="lyrico-select-lyric-line" value="Adchithooku"/>
</div>
<div class="lyrico-lyrics-wrapper">Adchithooku thooku adchithooku
<input type="checkbox" class="lyrico-select-lyric-line" value="Adchithooku thooku adchithooku"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey adchithooku adchithooku
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey adchithooku adchithooku"/>
</div>
<div class="lyrico-lyrics-wrapper">Adchithooku
<input type="checkbox" class="lyrico-select-lyric-line" value="Adchithooku"/>
</div>
<div class="lyrico-lyrics-wrapper">Adchithooku thooku thooku
<input type="checkbox" class="lyrico-select-lyric-line" value="Adchithooku thooku thooku"/>
</div>
<div class="lyrico-lyrics-wrapper">Thooku thooku adchithooku
<input type="checkbox" class="lyrico-select-lyric-line" value="Thooku thooku adchithooku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thadabudalaa varum
<input type="checkbox" class="lyrico-select-lyric-line" value="Thadabudalaa varum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanmaana padai padai
<input type="checkbox" class="lyrico-select-lyric-line" value="Thanmaana padai padai"/>
</div>
<div class="lyrico-lyrics-wrapper">Arabikkadal namai kondaaduthu
<input type="checkbox" class="lyrico-select-lyric-line" value="Arabikkadal namai kondaaduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kidaikkumada pala
<input type="checkbox" class="lyrico-select-lyric-line" value="Kidaikkumada pala"/>
</div>
<div class="lyrico-lyrics-wrapper">Kelvikku vidai vidai
<input type="checkbox" class="lyrico-select-lyric-line" value="Kelvikku vidai vidai"/>
</div>
<div class="lyrico-lyrics-wrapper">Urchaagam vandhu koothaaduthu
<input type="checkbox" class="lyrico-select-lyric-line" value="Urchaagam vandhu koothaaduthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Don-eh darr aavaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Don-eh darr aavaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaulathu girr aavaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhaulathu girr aavaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhaanda madurakkaaran
<input type="checkbox" class="lyrico-select-lyric-line" value="Vandhaanda madurakkaaran"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Hey adchithooku adchithooku
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey adchithooku adchithooku"/>
</div>
<div class="lyrico-lyrics-wrapper">Adchithooku
<input type="checkbox" class="lyrico-select-lyric-line" value="Adchithooku"/>
</div>
<div class="lyrico-lyrics-wrapper">Adchithooku thooku adchithooku
<input type="checkbox" class="lyrico-select-lyric-line" value="Adchithooku thooku adchithooku"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey adchithooku adchithooku
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey adchithooku adchithooku"/>
</div>
<div class="lyrico-lyrics-wrapper">Adchithooku
<input type="checkbox" class="lyrico-select-lyric-line" value="Adchithooku"/>
</div>
<div class="lyrico-lyrics-wrapper">Adchithooku thooku thooku
<input type="checkbox" class="lyrico-select-lyric-line" value="Adchithooku thooku thooku"/>
</div>
<div class="lyrico-lyrics-wrapper">Thooku thooku adchithooku
<input type="checkbox" class="lyrico-select-lyric-line" value="Thooku thooku adchithooku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Heyy thunak thunak
<input type="checkbox" class="lyrico-select-lyric-line" value="Heyy thunak thunak"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunak thunak
<input type="checkbox" class="lyrico-select-lyric-line" value="Thunak thunak"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhin dhak dhindhaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhin dhak dhindhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey thunnaakk thunnaak
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey thunnaakk thunnaak"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhin dhak dhindhaa (2 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhin dhak dhindhaa"/></div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Aaaaa….aaa…….
<input type="checkbox" class="lyrico-select-lyric-line" value="Aaaaa….aaa……."/>
</div>
<div class="lyrico-lyrics-wrapper">Haaa….aaaa….aaa….
<input type="checkbox" class="lyrico-select-lyric-line" value="Haaa….aaaa….aaa…."/>
</div>
<div class="lyrico-lyrics-wrapper">Haa….aaa…..aaa….
<input type="checkbox" class="lyrico-select-lyric-line" value="Haa….aaa…..aaa…."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thumkaa thumkaa…(2 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Thumkaa thumkaa"/></div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Naan nenaichathu ellaam
<input type="checkbox" class="lyrico-select-lyric-line" value="Naan nenaichathu ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ovvonna yen nadakkuthu thannaala
<input type="checkbox" class="lyrico-select-lyric-line" value="Ovvonna yen nadakkuthu thannaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Mel irukkura megam
<input type="checkbox" class="lyrico-select-lyric-line" value="Mel irukkura megam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oyaama poo thoovuthu en mela
<input type="checkbox" class="lyrico-select-lyric-line" value="Oyaama poo thoovuthu en mela"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ada karuvaa nee porakkura
<input type="checkbox" class="lyrico-select-lyric-line" value="Ada karuvaa nee porakkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Iranthaa dandanakkara
<input type="checkbox" class="lyrico-select-lyric-line" value="Iranthaa dandanakkara"/>
</div>
<div class="lyrico-lyrics-wrapper">Maththiyila konja naalu
<input type="checkbox" class="lyrico-select-lyric-line" value="Maththiyila konja naalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Semma scene-ah sethara vaikkanum
<input type="checkbox" class="lyrico-select-lyric-line" value="Semma scene-ah sethara vaikkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paathaa pathara vaikkanum
<input type="checkbox" class="lyrico-select-lyric-line" value="Paathaa pathara vaikkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Appa thaanda nee en aalu
<input type="checkbox" class="lyrico-select-lyric-line" value="Appa thaanda nee en aalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Pudhusaachi
<input type="checkbox" class="lyrico-select-lyric-line" value="Pudhusaachi"/>
</div>
  <div class="lyrico-lyrics-wrapper">En poruppuda
<input type="checkbox" class="lyrico-select-lyric-line" value="En poruppuda"/>
</div>
  <div class="lyrico-lyrics-wrapper">Ini vegaadhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Ini vegaadhu"/>
</div>
  <div class="lyrico-lyrics-wrapper">Un paruppuda
<input type="checkbox" class="lyrico-select-lyric-line" value="Un paruppuda"/>
</div>
  <div class="lyrico-lyrics-wrapper">Vethu gethu ellaam
<input type="checkbox" class="lyrico-select-lyric-line" value="Vethu gethu ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatta koodaathu
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaatta koodaathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Hey adchithooku adchithooku
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey adchithooku adchithooku"/>
</div>
<div class="lyrico-lyrics-wrapper">Adchithooku
<input type="checkbox" class="lyrico-select-lyric-line" value="Adchithooku"/>
</div>
<div class="lyrico-lyrics-wrapper">Adchithooku thooku adchithooku
<input type="checkbox" class="lyrico-select-lyric-line" value="Adchithooku thooku adchithooku"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey adchithooku adchithooku
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey adchithooku adchithooku"/>
</div>
<div class="lyrico-lyrics-wrapper">Adchithooku
<input type="checkbox" class="lyrico-select-lyric-line" value="Adchithooku"/>
</div>
<div class="lyrico-lyrics-wrapper">Adchithooku thooku adchithooku
<input type="checkbox" class="lyrico-select-lyric-line" value="Adchithooku thooku adchithooku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Angaali pangaalai vaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Angaali pangaalai vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini aattam thaan eppodhum
<input type="checkbox" class="lyrico-select-lyric-line" value="Ini aattam thaan eppodhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi adi
<input type="checkbox" class="lyrico-select-lyric-line" value="Adi adi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Mangaatha katta pola
<input type="checkbox" class="lyrico-select-lyric-line" value="Mangaatha katta pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha vattaaram naam kaiyil
<input type="checkbox" class="lyrico-select-lyric-line" value="Indha vattaaram naam kaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudi pudi
<input type="checkbox" class="lyrico-select-lyric-line" value="Pudi pudi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thadabudalaa varum
<input type="checkbox" class="lyrico-select-lyric-line" value="Thadabudalaa varum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanmaana padai padai
<input type="checkbox" class="lyrico-select-lyric-line" value="Thanmaana padai padai"/>
</div>
<div class="lyrico-lyrics-wrapper">Arabikkadal namai kondaaduthu
<input type="checkbox" class="lyrico-select-lyric-line" value="Arabikkadal namai kondaaduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kidaikkumada pala
<input type="checkbox" class="lyrico-select-lyric-line" value="Kidaikkumada pala"/>
</div>
<div class="lyrico-lyrics-wrapper">Kelvikku vidai vidai
<input type="checkbox" class="lyrico-select-lyric-line" value="Kelvikku vidai vidai"/>
</div>
<div class="lyrico-lyrics-wrapper">Urchaagam vandhu koothaaduthu
<input type="checkbox" class="lyrico-select-lyric-line" value="Urchaagam vandhu koothaaduthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Don-eh darr aavaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Don-eh darr aavaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaulathu girr aavaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhaulathu girr aavaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhaanda madurakkaaran
<input type="checkbox" class="lyrico-select-lyric-line" value="Vandhaanda madurakkaaran"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Alekka velaiyaadu
<input type="checkbox" class="lyrico-select-lyric-line" value="Alekka velaiyaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adichaa kekka yaaru……
<input type="checkbox" class="lyrico-select-lyric-line" value="Adichaa kekka yaaru……"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Durru takkuttu darru takkuttu
<input type="checkbox" class="lyrico-select-lyric-line" value="Durru takkuttu darru takkuttu"/>
</div>
<div class="lyrico-lyrics-wrapper">Darakku darakku
<input type="checkbox" class="lyrico-select-lyric-line" value="Darakku darakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Durru takkuttu darru takkuttu
<input type="checkbox" class="lyrico-select-lyric-line" value="Durru takkuttu darru takkuttu"/>
</div>
<div class="lyrico-lyrics-wrapper">Darakku darakku
<input type="checkbox" class="lyrico-select-lyric-line" value="Darakku darakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Dirukku thakkita thakkita
<input type="checkbox" class="lyrico-select-lyric-line" value="Dirukku thakkita thakkita"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakkitta thakkitta
<input type="checkbox" class="lyrico-select-lyric-line" value="Thakkitta thakkitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Dirukku thakkita thakkita
<input type="checkbox" class="lyrico-select-lyric-line" value="Dirukku thakkita thakkita"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakkitta thakkitta
<input type="checkbox" class="lyrico-select-lyric-line" value="Thakkitta thakkitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Durr thakka durr thakka
<input type="checkbox" class="lyrico-select-lyric-line" value="Durr thakka durr thakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Durr thakka
<input type="checkbox" class="lyrico-select-lyric-line" value="Durr thakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Durr ka thakitta durrkka thakitta
<input type="checkbox" class="lyrico-select-lyric-line" value="Durr ka thakitta durrkka thakitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Durrkka thakitta
<input type="checkbox" class="lyrico-select-lyric-line" value="Durrkka thakitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakka thakka thakka thakka
<input type="checkbox" class="lyrico-select-lyric-line" value="Thakka thakka thakka thakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Durr ka thakitta durrkka thakitta
<input type="checkbox" class="lyrico-select-lyric-line" value="Durr ka thakitta durrkka thakitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Durr durr durr durr
<input type="checkbox" class="lyrico-select-lyric-line" value="Durr durr durr durr"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Hey adchithooku adchithooku
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey adchithooku adchithooku"/>
</div>
<div class="lyrico-lyrics-wrapper">Adchithooku
<input type="checkbox" class="lyrico-select-lyric-line" value="Adchithooku"/>
</div>
<div class="lyrico-lyrics-wrapper">Adchithooku thooku adchithooku
<input type="checkbox" class="lyrico-select-lyric-line" value="Adchithooku thooku adchithooku"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey adchithooku adchithooku
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey adchithooku adchithooku"/>
</div>
<div class="lyrico-lyrics-wrapper">Adchithooku
<input type="checkbox" class="lyrico-select-lyric-line" value="Adchithooku"/>
</div>
<div class="lyrico-lyrics-wrapper">Adchithooku thooku thooku
<input type="checkbox" class="lyrico-select-lyric-line" value="Adchithooku thooku thooku"/>
</div>
<div class="lyrico-lyrics-wrapper">Thooku thooku adchithooku
<input type="checkbox" class="lyrico-select-lyric-line" value="Thooku thooku adchithooku"/>
</div>
</pre>