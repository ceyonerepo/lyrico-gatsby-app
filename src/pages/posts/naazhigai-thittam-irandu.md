---
title: "naazhigai song lyrics"
album: "Thittam Irandu"
artist: "Satish Raghunathan"
lyricist: "Thava Kumar"
director: "Vignesh Karthick"
path: "/albums/thittam-irandu-lyrics"
song: "Naazhigai"
image: ../../images/albumart/thittam-irandu.jpg
date: 2021-07-30
lang: tamil
youtubeLink: "https://www.youtube.com/embed/fW1H0Jl3Jp8"
type: "melody"
singers:
  - Karthika Vaidyanathan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Naazhigai theerumpodhilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naazhigai theerumpodhilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Neramum dhooram aanadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neramum dhooram aanadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mounamae paarvai veesudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mounamae paarvai veesudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangalil kaadhal pesudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangalil kaadhal pesudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidiyalin karaiyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidiyalin karaiyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un agam varava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un agam varava"/>
</div>
<div class="lyrico-lyrics-wrapper">Karuvinil orumurai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuvinil orumurai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un mugam perava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un mugam perava"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kannil moondraam pirai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kannil moondraam pirai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanbaen en aayul varai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanbaen en aayul varai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unai pola nesam kolla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai pola nesam kolla"/>
</div>
<div class="lyrico-lyrics-wrapper">Unadhaanen unmaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unadhaanen unmaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Narai koodum kaalam kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Narai koodum kaalam kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai thaedum endhan idhayathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai thaedum endhan idhayathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Oyaamal thaalaatum reengaaramae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oyaamal thaalaatum reengaaramae"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhuvum pesaamal un 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhuvum pesaamal un "/>
</div>
<div class="lyrico-lyrics-wrapper">tholil naan saayavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tholil naan saayavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Indrae mun jenmam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indrae mun jenmam "/>
</div>
<div class="lyrico-lyrics-wrapper">paarthen anbae naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paarthen anbae naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naazhigai theerumpodhilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naazhigai theerumpodhilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Neramum dhooram aanadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neramum dhooram aanadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mounamae paarvai veesudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mounamae paarvai veesudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangalil kaadhal pesudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangalil kaadhal pesudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidiyalin karaiyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidiyalin karaiyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un agam varava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un agam varava"/>
</div>
<div class="lyrico-lyrics-wrapper">Karuvinil orumurai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuvinil orumurai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un mugam perava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un mugam perava"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kannil moondraam pirai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kannil moondraam pirai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanbaen en aayul varai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanbaen en aayul varai"/>
</div>
</pre>
