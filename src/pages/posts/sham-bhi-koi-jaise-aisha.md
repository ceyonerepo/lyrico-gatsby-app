---
title: "sham song lyrics"
album: "Aisha"
artist: "Amit Trivedi"
lyricist: "Javed Akhtar"
director: "Rajshree Ojha"
path: "/albums/aisha-lyrics"
song: "sham bhi koi"
image: ../../images/albumart/aisha.jpg
date: 2010-08-06
lang: hindi
youtubeLink: "https://www.youtube.com/embed/fFyXcX-s0C8"
type: "love"
singers:
  - Nikhil D'Souza
  - Amit Trivedi
  - Neuman Pinto
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Sham bhi koi jaise hai nadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sham bhi koi jaise hai nadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Lehar lehar jaise beh rahi hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lehar lehar jaise beh rahi hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Koi ankahi, koi ansuni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koi ankahi, koi ansuni"/>
</div>
<div class="lyrico-lyrics-wrapper">Baat dheemi dheemi keh rahi hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baat dheemi dheemi keh rahi hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kahin na kahin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kahin na kahin"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaagi hui hai koi aarzoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaagi hui hai koi aarzoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kahin na kahin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kahin na kahin"/>
</div>
<div class="lyrico-lyrics-wrapper">Khoye hue se hain main aur tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khoye hue se hain main aur tu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ke boom boom boom para para
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ke boom boom boom para para"/>
</div>
<div class="lyrico-lyrics-wrapper">Hai khaamosh dono
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hai khaamosh dono"/>
</div>
<div class="lyrico-lyrics-wrapper">Ke boom boom boom para para
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ke boom boom boom para para"/>
</div>
<div class="lyrico-lyrics-wrapper">Hai madhosh dono
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hai madhosh dono"/>
</div>
<div class="lyrico-lyrics-wrapper">Jo gummsum gummsum hai yeh fizayein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jo gummsum gummsum hai yeh fizayein"/>
</div>
<div class="lyrico-lyrics-wrapper">Jo kehti sunti hai yeh nigahein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jo kehti sunti hai yeh nigahein"/>
</div>
<div class="lyrico-lyrics-wrapper">Gummsum gummsum hai yeh fizayein hai na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gummsum gummsum hai yeh fizayein hai na"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeh kaisa samay hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh kaisa samay hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaisa sama hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaisa sama hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ke shaam hai pighal rahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ke shaam hai pighal rahi"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeh sab kuch haseen hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh sab kuch haseen hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Sab kuch jawan hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sab kuch jawan hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Hai zindagi machal rahi jagmagati jilmilati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hai zindagi machal rahi jagmagati jilmilati"/>
</div>
<div class="lyrico-lyrics-wrapper">Palak palak pe khwaab hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palak palak pe khwaab hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Sun yeh hawayein gungunaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sun yeh hawayein gungunaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Jo geet lajawab hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jo geet lajawab hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ke boom boom boom para para
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ke boom boom boom para para"/>
</div>
<div class="lyrico-lyrics-wrapper">Hai khaamosh dono
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hai khaamosh dono"/>
</div>
<div class="lyrico-lyrics-wrapper">Ke boom boom boom para para
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ke boom boom boom para para"/>
</div>
<div class="lyrico-lyrics-wrapper">Hai madhosh dono
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hai madhosh dono"/>
</div>
<div class="lyrico-lyrics-wrapper">Jo gummsum gummsum hai yeh fizayein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jo gummsum gummsum hai yeh fizayein"/>
</div>
<div class="lyrico-lyrics-wrapper">Jo kehti sunti hai yeh nigahein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jo kehti sunti hai yeh nigahein"/>
</div>
<div class="lyrico-lyrics-wrapper">Gummsum gummsum hai yeh fizayein hai na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gummsum gummsum hai yeh fizayein hai na"/>
</div>
</pre>
