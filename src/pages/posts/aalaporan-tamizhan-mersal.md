---
title: "aalaporan tamizhan song lyrics"
album: "Mersal"
artist: "A.R. Rahman"
lyricist: "Vivek"
director: "Atlee"
path: "/albums/mersal-lyrics"
song: "Aalaporan Tamizhan"
image: ../../images/albumart/mersal.jpg
date: 2017-10-18
lang: tamil
youtubeLink: "https://www.youtube.com/embed/xsbLtHql4g8"
type: "Mass"
singers:
  - Kailash Kher
  - Sathya Prakash
  - Deepak
  - Pooja AV
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ooru Kannu Oravu kannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooru Kannu Oravu kannu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Mochchu Paakkum Ninnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Mochchu Paakkum Ninnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinna Magaraasan Vaaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna Magaraasan Vaaran"/>
</div>
<div class="lyrico-lyrics-wrapper">Meesa Murukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meesa Murukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enga Mannu Thanga Mannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Mannu Thanga Mannu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Vaikkum Singamannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Vaikkum Singamannu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muthumani Rathinaththa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthumani Rathinaththa "/>
</div>
<div class="lyrico-lyrics-wrapper">Petheduththa Ranjitham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Petheduththa Ranjitham"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorukunne Vaalu Kannu  
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorukunne Vaalu Kannu  "/>
</div>
<div class="lyrico-lyrics-wrapper">Appanukkum Sammadham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appanukkum Sammadham"/>
</div>
<div class="lyrico-lyrics-wrapper">Endha Edam Valikandaalum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endha Edam Valikandaalum "/>
</div>
<div class="lyrico-lyrics-wrapper">Kannuthaane Kalangum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannuthaane Kalangum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannai Pola Engalukku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannai Pola Engalukku "/>
</div>
<div class="lyrico-lyrics-wrapper">Kaavallaa Nee Varannum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaavallaa Nee Varannum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aalaporan Tamizhan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalaporan Tamizhan "/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagam Ellamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagam Ellamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetrimagan Vazhithaan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetrimagan Vazhithaan "/>
</div>
<div class="lyrico-lyrics-wrapper">Inimae Ellamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inimae Ellamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Veeranna Yarunu Intha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veeranna Yarunu Intha "/>
</div>
<div class="lyrico-lyrics-wrapper">Naatukke Avan Sonnanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naatukke Avan Sonnanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaayilla Maattukum Ada 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaayilla Maattukum Ada "/>
</div>
<div class="lyrico-lyrics-wrapper">Needhiya Avan Thanthaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Needhiya Avan Thanthaanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Solli Solli Sarithirathil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solli Solli Sarithirathil "/>
</div>
<div class="lyrico-lyrics-wrapper">Paer Porippaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paer Porippaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil Alli Kaaththil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil Alli Kaaththil "/>
</div>
<div class="lyrico-lyrics-wrapper">Namma Thaen Thamizh Thelippaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma Thaen Thamizh Thelippaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Minnum Ulagam Medai Thanga Tamila Paada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minnum Ulagam Medai Thanga Tamila Paada"/>
</div>
<div class="lyrico-lyrics-wrapper">Pachchaththamizh Uchchi Pugazh Yeri sirukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pachchaththamizh Uchchi Pugazh Yeri sirukkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varaaiyo Vaaraai Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varaaiyo Vaaraai Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbaa Vandha Oli Koduppom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbaa Vandha Oli Koduppom"/>
</div>
<div class="lyrico-lyrics-wrapper">Varaaiyo Vaaraai Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varaaiyo Vaaraai Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Vambaa Vandha Sulukkedupom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vambaa Vandha Sulukkedupom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thamizhanda Ennaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thamizhanda Ennaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sonnaale Thimurerum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sonnaale Thimurerum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaaththoda Kalandhaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaththoda Kalandhaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu Than Un Adayaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu Than Un Adayaalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Anba Kotti Enga Mozhi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Anba Kotti Enga Mozhi "/>
</div>
<div class="lyrico-lyrics-wrapper">Adiththalam Pottom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiththalam Pottom"/>
</div>
<div class="lyrico-lyrics-wrapper">Magudaththa Tharikira 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Magudaththa Tharikira "/>
</div>
‘<div class="lyrico-lyrics-wrapper">La’garaththa Seththom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="La’garaththa Seththom"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalaimurai Kadandhume 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaimurai Kadandhume "/>
</div>
<div class="lyrico-lyrics-wrapper">Virivatha Paaththom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Virivatha Paaththom"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagathin Muthal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagathin Muthal "/>
</div>
<div class="lyrico-lyrics-wrapper">Mozhi Usurena Kaaththom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mozhi Usurena Kaaththom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naal Nagara Maatrangal Nerum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naal Nagara Maatrangal Nerum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unmozhi Saayum Enbaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmozhi Saayum Enbaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Par Ilaya Thamizhanum Varuvan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Par Ilaya Thamizhanum Varuvan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thai Thamizh Thookki Nippaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thai Thamizh Thookki Nippaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadaisi Thamizhanin Raththam Elum Veezhadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadaisi Thamizhanin Raththam Elum Veezhadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Thamizhliname Veezhadhe Thamzhiliname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thamizhliname Veezhadhe Thamzhiliname"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nedundhooram Un Isai Kekkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nedundhooram Un Isai Kekkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pirai Neetti Pournami Aakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirai Neetti Pournami Aakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vethakkaattil Vinmeen Pookkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vethakkaattil Vinmeen Pookkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhichaalum Nesandhaan Uyir Ezhayum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhichaalum Nesandhaan Uyir Ezhayum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Neththi Muththam Podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Neththi Muththam Podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Varungaalam Vaasana Sekkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varungaalam Vaasana Sekkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muthumani Rathinaththa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthumani Rathinaththa "/>
</div>
<div class="lyrico-lyrics-wrapper">Petheduththa Ranjitham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Petheduththa Ranjitham"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorukunne Vaalu Kannu  
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorukunne Vaalu Kannu  "/>
</div>
<div class="lyrico-lyrics-wrapper">Appanukkum Sammadham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appanukkum Sammadham"/>
</div>
<div class="lyrico-lyrics-wrapper">Endha Edam Valikandaalum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endha Edam Valikandaalum "/>
</div>
<div class="lyrico-lyrics-wrapper">Kannuthaane Kalangum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannuthaane Kalangum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanna Pola Engalukku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna Pola Engalukku "/>
</div>
<div class="lyrico-lyrics-wrapper">Kaavala Nee Varanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaavala Nee Varanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alaporan Tamilan Ulagam Ellame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alaporan Tamilan Ulagam Ellame"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetri Magan Vazhithan Inime Ellame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetri Magan Vazhithan Inime Ellame"/>
</div>
<div class="lyrico-lyrics-wrapper">Veeranna Yarunu Intha Naatukke Avan Sonnane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veeranna Yarunu Intha Naatukke Avan Sonnane"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaayilla Maatukum Avan Needhiya Thanthaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaayilla Maatukum Avan Needhiya Thanthaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varaayo Vaarai Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varaayo Vaarai Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbaa Vandha Oli Koduppom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbaa Vandha Oli Koduppom"/>
</div>
<div class="lyrico-lyrics-wrapper">Varaayo Vaarai Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varaayo Vaarai Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Vambaa Vandha Sulukkedupom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vambaa Vandha Sulukkedupom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thamizhale Onnaanom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thamizhale Onnaanom"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaraadhu Ennaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaraadhu Ennaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thamizhale Onnaanom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thamizhale Onnaanom"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaraathu Ennaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaraathu Ennaalum"/>
</div>
</pre>
