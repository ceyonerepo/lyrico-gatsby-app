---
title: "vaale vaale vaaleele song lyrics"
album: "Singam II"
artist: "Devi Sri Prasad"
lyricist: "Viveka"
director: "Hari Gopalakrishnan"
path: "/albums/singam-2-lyrics"
song: "Vaale Vaale Vaaleele"
image: ../../images/albumart/singam-2.jpg
date: 2013-07-05
lang: tamil
youtubeLink: "https://www.youtube.com/embed/3nhQ5bdB-a0"
type: "Mass Entry"
singers:
  - 	Shankar Mahadevan
  - 	Nakash Aziz
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hey Yayiye Yo Yo  Yo Yo Yo Yo  Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Yayiye Yo Yo  Yo Yo Yo Yo  Hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yayiye Vaaya Vaaya Vaaya Vaaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yayiye Vaaya Vaaya Vaaya Vaaya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Kaatukulla Nozhanjirukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Kaatukulla Nozhanjirukkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhillaana Oru Singam Naan Paathuputta Kadalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhillaana Oru Singam Naan Paathuputta Kadalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kooda Neeraaviya Pongum Hey Vettaikku Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooda Neeraaviya Pongum Hey Vettaikku Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kilambi vantha Veri Pidicha Singam En Viralu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kilambi vantha Veri Pidicha Singam En Viralu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Patta Uralum Kooda Nangoorama Mungum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Patta Uralum Kooda Nangoorama Mungum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aala Kadaloram Velaiyaadum Aalu Da Aala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aala Kadaloram Velaiyaadum Aalu Da Aala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paathaalaye Ida Podum Vela Da Bore Adicha Sandai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paathaalaye Ida Podum Vela Da Bore Adicha Sandai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Podum Ooru Kaaran Da Inge Poranthathaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podum Ooru Kaaran Da Inge Poranthathaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanum Konjam Kovakaaran Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum Konjam Kovakaaran Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaale Vaale Vaaleele Lema Pole Pole Polele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaale Vaale Vaaleele Lema Pole Pole Polele"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Le Hei Vaale Vaale Vaalele Lema Pole Pole Polele Le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Le Hei Vaale Vaale Vaalele Lema Pole Pole Polele Le"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Yayiye Yo Yo  Yo Yo Yo Yo  Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Yayiye Yo Yo  Yo Yo Yo Yo  Hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yayiye Vaaya Vaaya Vaaya Vaaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yayiye Vaaya Vaaya Vaaya Vaaya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yele Le Le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yele Le Le"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yele Le Le Ye Le Le Yele Le Le Ye Le Le Yele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yele Le Le Ye Le Le Yele Le Le Ye Le Le Yele"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Le Le Le Le Le Le Le Eh Eh Eh Eh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Le Le Le Le Le Le Le Eh Eh Eh Eh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Seerum En Ennatha Etta Vachen Ippo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Seerum En Ennatha Etta Vachen Ippo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yethutho vannathil Satta Thachen Hey Thaniyaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethutho vannathil Satta Thachen Hey Thaniyaaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ukkanthu Pulli Vachu Sila Thappana Aalukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ukkanthu Pulli Vachu Sila Thappana Aalukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanni Vachen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanni Vachen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oorukula Ponathum Dhinam Thooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorukula Ponathum Dhinam Thooru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaarum Vela Da Yaaru Enna Solla Maaten Pinnala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaarum Vela Da Yaaru Enna Solla Maaten Pinnala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Paaru Da Kannamoochi Aattathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Paaru Da Kannamoochi Aattathula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalanthirukken Naanu Da Oorukulla Pathu Peru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalanthirukken Naanu Da Oorukulla Pathu Peru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Porukki Irundha Antha Pathu Pera Naanum Ipo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porukki Irundha Antha Pathu Pera Naanum Ipo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Porukki Eduppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porukki Eduppen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaale Vaale Vaalele Lema Pola Pole Polele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaale Vaale Vaalele Lema Pola Pole Polele"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Le Hei Vaale Vaale Vaalele Lema Pola Polele Le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Le Hei Vaale Vaale Vaalele Lema Pola Polele Le"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Saathinu Saaminu Sanda Potta Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Saathinu Saaminu Sanda Potta Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saathikkum Aasaiya Kaathil vitta Hey Pagal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saathikkum Aasaiya Kaathil vitta Hey Pagal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neram Kudikka Nee Kathukitta Un Ponjathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neram Kudikka Nee Kathukitta Un Ponjathi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pullaiya Theruvil Vitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pullaiya Theruvil Vitta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Seiyum Vela Deivamunnu Ennam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Seiyum Vela Deivamunnu Ennam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ippo Venum  Da Poiya Nikkumsaathi Ellam Thooki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo Venum  Da Poiya Nikkumsaathi Ellam Thooki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thoora veesuda Onnumilla Vaazhkai Idhu Onna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoora veesuda Onnumilla Vaazhkai Idhu Onna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sernthu Vaazhu Da Muthunaga Oorathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sernthu Vaazhu Da Muthunaga Oorathula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thuraimugam Da Namma Thatheduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuraimugam Da Namma Thatheduthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaakurathu Aarumugam Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaakurathu Aarumugam Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaale vaale Vaalele Lema Pola Pole Polele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaale vaale Vaalele Lema Pola Pole Polele"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Le Hei Vaale Vaale Vaalele Lema Pole Pole Polele Le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Le Hei Vaale Vaale Vaalele Lema Pole Pole Polele Le"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Yayiye Yo Yo Yo Yo Yo Yo Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Yayiye Yo Yo Yo Yo Yo Yo Hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yayiye Yo Yo Yo Yo Yo Yo Yo Yo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yayiye Yo Yo Yo Yo Yo Yo Yo Yo"/>
</div>
</pre>
