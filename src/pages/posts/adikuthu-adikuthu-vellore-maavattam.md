---
title: "adikuthu adikuthu song lyrics"
album: "Vellore Maavattam"
artist: "Sundar C Babu"
lyricist: "Na. Muthukumar"
director: "R.N.R. Manohar"
path: "/albums/vellore-maavattam-lyrics"
song: "Adikuthu Adikuthu"
image: ../../images/albumart/vellore-maavattam.jpg
date: 2011-10-06
lang: tamil
youtubeLink: "https://www.youtube.com/embed/uTtWZ5coQ3g"
type: "happy"
singers:
  - Senthuil Das
  - Manikka Vinayagam
  - Navven Madhav
  - Maanasa
  - Malgudi Subha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Adikkidhu adikkidhu alai onnu adikkidhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adikkidhu adikkidhu alai onnu adikkidhu"/>
</div>
<div class="lyrico-lyrics-wrapper">manasula gudhikkidhu sadhosam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasula gudhikkidhu sadhosam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manakkudhu manakkudhu idhayathil manakkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manakkudhu manakkudhu idhayathil manakkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">kama kama kamavena poo vaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kama kama kamavena poo vaasam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Molaikkidhu mudhukula orasudhu aagaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Molaikkidhu mudhukula orasudhu aagaasam"/>
</div>
<div class="lyrico-lyrics-wrapper">palikkidhu palikkidhu nenaichadhu palikkidhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="palikkidhu palikkidhu nenaichadhu palikkidhu"/>
</div>
<div class="lyrico-lyrics-wrapper">idhu ivan paadhaigal idhigaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhu ivan paadhaigal idhigaasam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edudaa edudaa melathai edudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edudaa edudaa melathai edudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">adidaa adidaa thaalathai adidaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adidaa adidaa thaalathai adidaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeley yeley yeley yeley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeley yeley yeley yeley"/>
</div>
<div class="lyrico-lyrics-wrapper">koothukkatti kondaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koothukkatti kondaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">meley meley meley meley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meley meley meley meley"/>
</div>
<div class="lyrico-lyrics-wrapper">poagapoaraan nammaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poagapoaraan nammaalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adikkidhu adikkidhu alai onnu adikkidhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adikkidhu adikkidhu alai onnu adikkidhu"/>
</div>
<div class="lyrico-lyrics-wrapper">manasula gudhikkidhu sadhosam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasula gudhikkidhu sadhosam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manakkudhu manakkudhu idhayathil manakkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manakkudhu manakkudhu idhayathil manakkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">kama kama kamavena poo vaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kama kama kamavena poo vaasam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Padikkira vayasula buththiyil yerala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padikkira vayasula buththiyil yerala"/>
</div>
<div class="lyrico-lyrics-wrapper">moottaiya thookkurom mudhukoadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moottaiya thookkurom mudhukoadu"/>
</div>
<div class="lyrico-lyrics-wrapper">varugira thalaimurai velichathai paarkkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varugira thalaimurai velichathai paarkkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">adhukkoru pudhu vazhi nee poadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adhukkoru pudhu vazhi nee poadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manasukku pudichadhu evanukku kedaikkidhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasukku pudichadhu evanukku kedaikkidhu"/>
</div>
<div class="lyrico-lyrics-wrapper">onakkadhu kedaichadhu vailaiyaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="onakkadhu kedaichadhu vailaiyaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">neruppukkum naduvilum porakkurom paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neruppukkum naduvilum porakkurom paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">ena seyalpadu nam nanbaney thunivoadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ena seyalpadu nam nanbaney thunivoadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nanbanaa nee pirarkenavey irukkattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanbanaa nee pirarkenavey irukkattum"/>
</div>
<div class="lyrico-lyrics-wrapper">un perai oor sollattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un perai oor sollattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee endru sonnaal nee mattum illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee endru sonnaal nee mattum illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnoadu engal vaasam ellaam koodavarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnoadu engal vaasam ellaam koodavarum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adikkidhu adikkidhu alai onnu adikkidhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adikkidhu adikkidhu alai onnu adikkidhu"/>
</div>
<div class="lyrico-lyrics-wrapper">manasula gudhikkidhu sadhosam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasula gudhikkidhu sadhosam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manakkudhu manakkudhu idhayathil manakkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manakkudhu manakkudhu idhayathil manakkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">kama kama kamavena poo vaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kama kama kamavena poo vaasam"/>
</div>
</pre>
