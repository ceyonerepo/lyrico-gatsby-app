---
title: "uruttu song lyrics"
album: "Enna Solla Pogirai"
artist: "Vivek – Mervin"
lyricist: "Maathevan"
director: "A. Hariharan"
path: "/albums/enna-solla-pogirai-lyrics"
song: "Uruttu"
image: ../../images/albumart/enna-solla-pogirai.jpg
date: 2022-01-13
lang: tamil
youtubeLink: "https://www.youtube.com/embed/1MTKezYNwGU"
type: "happy"
singers:
  - Vivek Siva
  - Sivaangi Krishnakumar
  - Mervin Solomon
  - Santesh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hi Welcome Everybody
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hi Welcome Everybody"/>
</div>
<div class="lyrico-lyrics-wrapper">Single Ah Suththi Kondu Irukkum Aangale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Single Ah Suththi Kondu Irukkum Aangale"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Single Thaan Ena Suththi Irukkum Pengale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Single Thaan Ena Suththi Irukkum Pengale"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppada Enakku Kalyanam Panni Veppinganu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppada Enakku Kalyanam Panni Veppinganu"/>
</div>
<div class="lyrico-lyrics-wrapper">Wait Panra 90s Kids Uncle Eh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wait Panra 90s Kids Uncle Eh"/>
</div>
<div class="lyrico-lyrics-wrapper">No Tension Innikku Vikram Veettu Function
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="No Tension Innikku Vikram Veettu Function"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uruttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uruttu"/>
</div>
<div class="lyrico-lyrics-wrapper">Uruttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uruttu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Maatikichu Aadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Maatikichu Aadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sweet Edu Kondaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sweet Edu Kondaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Mandabatha Thedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Mandabatha Thedu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sweet Edu Kondaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sweet Edu Kondaadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Maatikichu Aadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Maatikichu Aadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sweet Edu Kondaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sweet Edu Kondaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Mandabatha Thedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Mandabatha Thedu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sweet Edu Kondaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sweet Edu Kondaadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Coatu Suitu Pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Coatu Suitu Pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Getharukkum Mappillaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Getharukkum Mappillaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Cook Uh Comali Ah Maatha Porale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cook Uh Comali Ah Maatha Porale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Settai Senjukittu Suththi Varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Settai Senjukittu Suththi Varum"/>
</div>
<div class="lyrico-lyrics-wrapper">Drama Queen Ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Drama Queen Ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakka Comedy ah Maatha Porane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakka Comedy ah Maatha Porane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ottikka Ottikka Kattikka Kattikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ottikka Ottikka Kattikka Kattikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Happily Everu Afteru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Happily Everu Afteru"/>
</div>
<div class="lyrico-lyrics-wrapper">Muttikka Muttikka Thothikka Thothikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muttikka Muttikka Thothikka Thothikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Happily Everu Afteru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Happily Everu Afteru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ottikka Ottikka Kattikka Kattikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ottikka Ottikka Kattikka Kattikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Happily Everu Afteru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Happily Everu Afteru"/>
</div>
<div class="lyrico-lyrics-wrapper">Muttikka Muttikka Thothikka Thothikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muttikka Muttikka Thothikka Thothikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Happily Everu Afteru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Happily Everu Afteru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Maatikichu Aadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Maatikichu Aadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sweet Edu Kondaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sweet Edu Kondaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Mandabatha Thedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Mandabatha Thedu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sweet Edu Kondaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sweet Edu Kondaadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Maatikichu Aadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Maatikichu Aadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sweet Edu Kondaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sweet Edu Kondaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Mandabatha Thedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Mandabatha Thedu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sweet Edu Kondaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sweet Edu Kondaadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Uruttu Uruttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Uruttu Uruttu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Theeraama Paathu Rasikanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Theeraama Paathu Rasikanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini Ketkaama Giftu Kodukanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Ketkaama Giftu Kodukanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Thevaina Maavu Araikanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Thevaina Maavu Araikanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Yes Sir
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yes Sir"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu Therlaina Net La Kathukanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu Therlaina Net La Kathukanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adadada Soldringa Senjingana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adadada Soldringa Senjingana"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakka Dealu Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakka Dealu Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">We Are Made For Ru Each Otheru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="We Are Made For Ru Each Otheru"/>
</div>
<div class="lyrico-lyrics-wrapper">Super Jodi Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Super Jodi Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oor Paarthaale Kannu Padum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oor Paarthaale Kannu Padum"/>
</div>
<div class="lyrico-lyrics-wrapper">Fairy Tale Lu Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fairy Tale Lu Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">You Both Are Hashtagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Both Are Hashtagu"/>
</div>
<div class="lyrico-lyrics-wrapper">Couple Goalu Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Couple Goalu Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Cute Aana Ponnu Ready
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Cute Aana Ponnu Ready"/>
</div>
<div class="lyrico-lyrics-wrapper">Adho Adho Adho Adho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adho Adho Adho Adho"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhu Vandhaalum Kaiya Pudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhu Vandhaalum Kaiya Pudi"/>
</div>
<div class="lyrico-lyrics-wrapper">Acho Acho Acho Acho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Acho Acho Acho Acho"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava Minjuna Vittu Pudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Minjuna Vittu Pudi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aama Aama Aama Aama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aama Aama Aama Aama"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini Ava Thaan Da Katti Pudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Ava Thaan Da Katti Pudi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uruttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uruttu"/>
</div>
<div class="lyrico-lyrics-wrapper">Uruttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uruttu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Vaai Un Uruttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Vaai Un Uruttu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ottikka Ottikka Kattikka Kattikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ottikka Ottikka Kattikka Kattikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Happily Everu Afteru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Happily Everu Afteru"/>
</div>
<div class="lyrico-lyrics-wrapper">Muttikka Muttikka Thothikka Thothikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muttikka Muttikka Thothikka Thothikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Happily Everu Afteru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Happily Everu Afteru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ottikka Ottikka Kattikka Kattikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ottikka Ottikka Kattikka Kattikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Happily Everu Afteru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Happily Everu Afteru"/>
</div>
<div class="lyrico-lyrics-wrapper">Muttikka Muttikka Thothikka Thothikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muttikka Muttikka Thothikka Thothikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Happily Everu Afteru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Happily Everu Afteru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Cute Aana Ponnu Ready
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Cute Aana Ponnu Ready"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhu Vandhaalum Kaiya Pudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhu Vandhaalum Kaiya Pudi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava Minjuna Vittu Pudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Minjuna Vittu Pudi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini Ava Thaan Da Katti Pudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Ava Thaan Da Katti Pudi"/>
</div>
</pre>
