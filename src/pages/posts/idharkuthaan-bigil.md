---
title: 'idharkuthaan song lyrics'
album: 'Bigil'
artist: 'A R Rahman'
lyricist: 'Vivek'
director: 'Atlee'
path: '/albums/bigil-song-lyrics'
song: 'Idharkuthaan'
image: ../../images/albumart/bigil.jpg
date: 2019-10-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/jodCorrvw88"
type: 'sad'
singers: 
- Dhee
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Idharkuthaan valimai serthomae
<input type="checkbox" class="lyrico-select-lyric-line" value="Idharkuthaan valimai serthomae"/>
</div>
<div class="lyrico-lyrics-wrapper">Idharkuthaano…
<input type="checkbox" class="lyrico-select-lyric-line" value="Idharkuthaano…"/>
</div>
<div class="lyrico-lyrics-wrapper">Ranangal kaaranangal aagidudhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Ranangal kaaranangal aagidudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaaranangalaai irundha thozhgal thozhgal
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaaranangalaai irundha thozhgal thozhgal"/>
</div>
<div class="lyrico-lyrics-wrapper">Sumandha baarangalai anbodu ninaikudhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Sumandha baarangalai anbodu ninaikudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Muyandru paarthomae
<input type="checkbox" class="lyrico-select-lyric-line" value="Muyandru paarthomae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh oh oh…hoo oo indha nodigale
<input type="checkbox" class="lyrico-select-lyric-line" value="Oh oh oh…hoo oo indha nodigale"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh oh oh…hoo oo inba vedigalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Oh oh oh…hoo oo inba vedigalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Idharkae andha vazhigalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Idharkae andha vazhigalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanneer thuligalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kanneer thuligalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanneer thuligalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kanneer thuligalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Silandhiyin vazhigalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Silandhiyin vazhigalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Valaigalaaga maarum indha naalo
<input type="checkbox" class="lyrico-select-lyric-line" value="Valaigalaaga maarum indha naalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharigalin nadhigalil aadaiaagum noolo
<input type="checkbox" class="lyrico-select-lyric-line" value="Tharigalin nadhigalil aadaiaagum noolo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ozhividum padhakkangal kolvar yaaro
<input type="checkbox" class="lyrico-select-lyric-line" value="Ozhividum padhakkangal kolvar yaaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Avargalin kaalgalil dhinam uzhaitha saero
<input type="checkbox" class="lyrico-select-lyric-line" value="Avargalin kaalgalil dhinam uzhaitha saero"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Oh oh oh…hoo oo indha nodigale
<input type="checkbox" class="lyrico-select-lyric-line" value="Oh oh oh…hoo oo indha nodigale"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh oh oh…hoo oo inba vedigalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Oh oh oh…hoo oo inba vedigalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Idharkae andha vazhigalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Idharkae andha vazhigalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanneer thuligalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kanneer thuligalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanneer thuligalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kanneer thuligalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Oh oh oh…hoo oo
<input type="checkbox" class="lyrico-select-lyric-line" value="Oh oh oh…hoo oo"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh oh oh…
<input type="checkbox" class="lyrico-select-lyric-line" value="Oh oh oh…"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh oh oh…hoo oo hoo hoo oo
<input type="checkbox" class="lyrico-select-lyric-line" value="Oh oh oh…hoo oo hoo hoo oo"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh oh oh…hoo oo
<input type="checkbox" class="lyrico-select-lyric-line" value="Oh oh oh…hoo oo"/>
</div>
</pre>