---
title: "you are my high song lyrics"
album: "Prati Roju Pandage"
artist: "S Thaman"
lyricist: "Srijo"
director: "Maruthi Dasari"
path: "/albums/prati-roju-pandage-lyrics"
song: "You Are My High"
image: ../../images/albumart/prati-roju-pandage.jpg
date: 2019-12-20
lang: telugu
youtubeLink: "https://www.youtube.com/embed/MmBNLamjIQY"
type: "love"
singers:
  - Raashi Khanna
  - Deepu
  - Rahul Nambiar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kanubomme Nuvu Kanabadithey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanubomme Nuvu Kanabadithey "/>
</div>
<div class="lyrico-lyrics-wrapper">Sari Kalalegareysenugaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sari Kalalegareysenugaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Kanukemo Thalakindhuluga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanukemo Thalakindhuluga "/>
</div>
<div class="lyrico-lyrics-wrapper">Padi Madi Mathi Thirigenugaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padi Madi Mathi Thirigenugaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Hyranaa Padipoyaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hyranaa Padipoyaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Hai Ni Vadhilina Yedhavalanaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hai Ni Vadhilina Yedhavalanaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Inkonchem Adegesaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inkonchem Adegesaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Thiyyani Haani Ni Vaddhanakaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiyyani Haani Ni Vaddhanakaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Honey I’m A Feeling 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Honey I’m A Feeling "/>
</div>
<div class="lyrico-lyrics-wrapper">Feeling Feeling Feeling High 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Feeling Feeling Feeling High "/>
</div>
<div class="lyrico-lyrics-wrapper">Take Me On A Journey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Take Me On A Journey "/>
</div>
<div class="lyrico-lyrics-wrapper">Flying Flying Flying High 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Flying Flying Flying High "/>
</div>
<div class="lyrico-lyrics-wrapper">The Only Reason 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="The Only Reason "/>
</div>
<div class="lyrico-lyrics-wrapper">I’m Going Going Going High 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I’m Going Going Going High "/>
</div>
<div class="lyrico-lyrics-wrapper">Baby You’ve Been 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby You’ve Been "/>
</div>
<div class="lyrico-lyrics-wrapper">Running On My Mind 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Running On My Mind "/>
</div>
<div class="lyrico-lyrics-wrapper">You Are My High
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Are My High"/>
</div>
<div class="lyrico-lyrics-wrapper">High High  
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="High High  "/>
</div>
<div class="lyrico-lyrics-wrapper">You Are My High 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Are My High "/>
</div>
<div class="lyrico-lyrics-wrapper">You Are My High 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Are My High "/>
</div>
<div class="lyrico-lyrics-wrapper">You Are My High 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Are My High "/>
</div>
<div class="lyrico-lyrics-wrapper">You Are My High 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Are My High "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanubomme Nuvu Kanabadithey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanubomme Nuvu Kanabadithey "/>
</div>
<div class="lyrico-lyrics-wrapper">Sari Kalalegareysenugaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sari Kalalegareysenugaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Kanukemo Thalakindhuluga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanukemo Thalakindhuluga "/>
</div>
<div class="lyrico-lyrics-wrapper">Padi Madi Mathi Thirigenugaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padi Madi Mathi Thirigenugaa "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Upiraaginaa Longipokani 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Upiraaginaa Longipokani "/>
</div>
<div class="lyrico-lyrics-wrapper">Rechagotte Manasumanavi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rechagotte Manasumanavi "/>
</div>
<div class="lyrico-lyrics-wrapper">Vayasu Villuni Yekkupettani 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayasu Villuni Yekkupettani "/>
</div>
<div class="lyrico-lyrics-wrapper">Repu Yevariki Thelusunani 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Repu Yevariki Thelusunani "/>
</div>
<div class="lyrico-lyrics-wrapper">Intha Kaalaanikaa Neeku Ee Korikaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Kaalaanikaa Neeku Ee Korikaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Yeppudo Gunde Nichanu Gaa Kaanukaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeppudo Gunde Nichanu Gaa Kaanukaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Neelo Illaa Kotha Konaalane 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelo Illaa Kotha Konaalane "/>
</div>
<div class="lyrico-lyrics-wrapper">Chusukundhamani Innallu Aagaanugaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chusukundhamani Innallu Aagaanugaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Honey I’m A Feeling 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Honey I’m A Feeling "/>
</div>
<div class="lyrico-lyrics-wrapper">Feeling Feeling Feeling High 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Feeling Feeling Feeling High "/>
</div>
<div class="lyrico-lyrics-wrapper">Take Me On A Journey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Take Me On A Journey "/>
</div>
<div class="lyrico-lyrics-wrapper">Flying Flying Flying High 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Flying Flying Flying High "/>
</div>
<div class="lyrico-lyrics-wrapper">The Only Reason 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="The Only Reason "/>
</div>
<div class="lyrico-lyrics-wrapper">I’m Going Going Going High 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I’m Going Going Going High "/>
</div>
<div class="lyrico-lyrics-wrapper">Baby You’ve Been 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby You’ve Been "/>
</div>
<div class="lyrico-lyrics-wrapper">Running On My Mind 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Running On My Mind "/>
</div>
<div class="lyrico-lyrics-wrapper">You're My High 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You're My High "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanubomme
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanubomme"/>
</div>
</pre>
