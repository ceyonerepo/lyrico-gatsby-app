---
title: "ramar analum song lyrics"
album: "Hey Ram"
artist: "Ilaiyaraaja"
lyricist: "Vaali"
director: "Kamal Haasan"
path: "/albums/hey-ram-song-lyrics"
song: "Ramar Analum Babar Analum"
image: ../../images/albumart/hey-ram.jpg
date: 2000-02-18
lang: tamil
youtubeLink: "https://www.youtube.com/embed/xdD9Pn7kymM"
type: "happy"
singers:
  - Kamal Haasan
  - Hariharan
  - Jolly Mukherjee
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ramar Aanaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ramar Aanaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Babar Aanaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Babar Aanaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Route Tu Onnuthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Route Tu Onnuthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Gate Tu Onnuthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gate Tu Onnuthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Snehitha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Snehitha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naamam Pottaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naamam Pottaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Namaas Pannaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namaas Pannaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nasthi Aagaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nasthi Aagaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dosthi Enbathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dosthi Enbathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Snehitha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Snehitha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Namma Pattaanida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma Pattaanida"/>
</div>
<div class="lyrico-lyrics-wrapper">Nandru Sonnaneda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nandru Sonnaneda"/>
</div>
<div class="lyrico-lyrics-wrapper">Natpu Saagaathada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natpu Saagaathada"/>
</div>
<div class="lyrico-lyrics-wrapper">Endru Sonnaanada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endru Sonnaanada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saami Rendaayutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saami Rendaayutta"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhoomi Rendaagume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhoomi Rendaagume"/>
</div>
<div class="lyrico-lyrics-wrapper">Sneham Undaayutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sneham Undaayutta"/>
</div>
<div class="lyrico-lyrics-wrapper">Logam Nanaagume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Logam Nanaagume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ullathai Solbavar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullathai Solbavar"/>
</div>
<div class="lyrico-lyrics-wrapper">Sonnathai Seibavar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sonnathai Seibavar"/>
</div>
<div class="lyrico-lyrics-wrapper">Naame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Amjad Endraalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amjad Endraalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Iyer Endraalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iyer Endraalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendu Bloodume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendu Bloodume"/>
</div>
<div class="lyrico-lyrics-wrapper">Reddu Thaanada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Reddu Thaanada"/>
</div>
<div class="lyrico-lyrics-wrapper">Sagothara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sagothara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mosque Sendraalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mosque Sendraalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mandir Sendraalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandir Sendraalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Friendship Pol Oru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friendship Pol Oru"/>
</div>
<div class="lyrico-lyrics-wrapper">Worship Ethada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Worship Ethada"/>
</div>
<div class="lyrico-lyrics-wrapper">Sagothara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sagothara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rendu Nanbaragale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendu Nanbaragale"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendu Kannagidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendu Kannagidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ondru Punnaaginaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondru Punnaaginaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ondru Kanneervidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondru Kanneervidum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Undaakkavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Undaakkavum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paththu Maathangale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paththu Maathangale"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Undaakkavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Undaakkavum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paththu Maathangale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paththu Maathangale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ullathai Solbavar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullathai Solbavar"/>
</div>
<div class="lyrico-lyrics-wrapper">Sonnathai Seibavar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sonnathai Seibavar"/>
</div>
<div class="lyrico-lyrics-wrapper">Naame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uchaththile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uchaththile"/>
</div>
<div class="lyrico-lyrics-wrapper">Kick Erinaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kick Erinaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjaanathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjaanathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Anjaathathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anjaathathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sonnaanada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sonnaanada"/>
</div>
<div class="lyrico-lyrics-wrapper">Mei Aam Ada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mei Aam Ada"/>
</div>
<div class="lyrico-lyrics-wrapper">Thraatchai Rasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thraatchai Rasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesum Nijam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesum Nijam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bale Bale Bale Bale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bale Bale Bale Bale"/>
</div>
<div class="lyrico-lyrics-wrapper">Bale Bale Bale Bale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bale Bale Bale Bale"/>
</div>
<div class="lyrico-lyrics-wrapper">Bale Bale Bale Bale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bale Bale Bale Bale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaagai Enum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaagai Enum"/>
</div>
<div class="lyrico-lyrics-wrapper">Tansen Udan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tansen Udan"/>
</div>
<div class="lyrico-lyrics-wrapper">Friend Aagalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friend Aagalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Panpaadalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panpaadalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sangeethame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sangeethame"/>
</div>
<div class="lyrico-lyrics-wrapper">Santhoshame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Santhoshame"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellorukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellorukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhu Swaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhu Swaram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bale Bale Bale Bale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bale Bale Bale Bale"/>
</div>
<div class="lyrico-lyrics-wrapper">Bale Bale Bale Bale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bale Bale Bale Bale"/>
</div>
<div class="lyrico-lyrics-wrapper">Bale Bale Bale Bale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bale Bale Bale Bale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ramar Aanaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ramar Aanaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Babar Aanaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Babar Aanaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Route Tu Onnuthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Route Tu Onnuthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Gate Tu Onnuthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gate Tu Onnuthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Snehitha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Snehitha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naamam Pottaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naamam Pottaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Namaas Pannaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namaas Pannaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nasthi Aagaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nasthi Aagaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dosthi Enbathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dosthi Enbathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Snehitha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Snehitha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Namma Pattaanida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma Pattaanida"/>
</div>
<div class="lyrico-lyrics-wrapper">Nandru Sonnaneda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nandru Sonnaneda"/>
</div>
<div class="lyrico-lyrics-wrapper">Natpu Saagaathada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natpu Saagaathada"/>
</div>
<div class="lyrico-lyrics-wrapper">Endru Sonnaanada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endru Sonnaanada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saami Rendaayutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saami Rendaayutta"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhoomi Rendaagumqe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhoomi Rendaagumqe"/>
</div>
<div class="lyrico-lyrics-wrapper">Sneham Undaayutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sneham Undaayutta"/>
</div>
<div class="lyrico-lyrics-wrapper">Logam Nanaagume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Logam Nanaagume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ullathai Solbavar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullathai Solbavar"/>
</div>
<div class="lyrico-lyrics-wrapper">Sonnathai Seibavar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sonnathai Seibavar"/>
</div>
<div class="lyrico-lyrics-wrapper">Naame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ramar Aanaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ramar Aanaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Babar Aanaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Babar Aanaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Route Tu Onnuthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Route Tu Onnuthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Gate Tu Onnuthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gate Tu Onnuthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Sagothara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sagothara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naamam Pottaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naamam Pottaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Namaas Pannaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namaas Pannaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nasthi Aagaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nasthi Aagaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dosthi Enbathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dosthi Enbathey"/>
</div>
<div class="lyrico-lyrics-wrapper">Sagothara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sagothara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Namma Pattaanida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma Pattaanida"/>
</div>
<div class="lyrico-lyrics-wrapper">Nandru Sonnaneda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nandru Sonnaneda"/>
</div>
<div class="lyrico-lyrics-wrapper">Natpu Saagaathada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natpu Saagaathada"/>
</div>
<div class="lyrico-lyrics-wrapper">Endru Sonnaanada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endru Sonnaanada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saami Rendaayutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saami Rendaayutta"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhoomi Rendaagumqe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhoomi Rendaagumqe"/>
</div>
<div class="lyrico-lyrics-wrapper">Sneham Undaayutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sneham Undaayutta"/>
</div>
<div class="lyrico-lyrics-wrapper">Logam Nanaagume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Logam Nanaagume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ullathai Solbavar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullathai Solbavar"/>
</div>
<div class="lyrico-lyrics-wrapper">Sonnathai Seibavar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sonnathai Seibavar"/>
</div>
<div class="lyrico-lyrics-wrapper">Naame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naame"/>
</div>
</pre>
