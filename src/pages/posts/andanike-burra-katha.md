---
title: "andanike song lyrics"
album: "Burra Katha"
artist: "Sai Karthik"
lyricist: "Bhaskarabhatla"
director: "Diamond Ratna Babu"
path: "/albums/burra-katha-lyrics"
song: "Andanike"
image: ../../images/albumart/burra-katha.jpg
date: 2019-07-05
lang: telugu
youtubeLink: "https://www.youtube.com/embed/J-j2l5XLOww"
type: "love"
singers:
  - Hemachandra
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hey Andanike Nuvvu Andaniveee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Andanike Nuvvu Andaniveee"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaa Brahma Chadavani Grandhanive
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaa Brahma Chadavani Grandhanive"/>
</div>
<div class="lyrico-lyrics-wrapper">Heyyyy Chuda Sakkani Chinnari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyyyy Chuda Sakkani Chinnari"/>
</div>
<div class="lyrico-lyrics-wrapper">Masakkali Nuvve Pilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Masakkali Nuvve Pilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Okka Debbaku Naa Gunde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okka Debbaku Naa Gunde"/>
</div>
<div class="lyrico-lyrics-wrapper">Fasak Chesavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fasak Chesavey"/>
</div>
<div class="lyrico-lyrics-wrapper">Theney Maatale Koti Veenalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theney Maatale Koti Veenalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pranamanthata Mogeley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pranamanthata Mogeley"/>
</div>
<div class="lyrico-lyrics-wrapper">Raave Raave Raave Audi Caralle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raave Raave Raave Audi Caralle"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnosaari Neney Trail Eh Vesthale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnosaari Neney Trail Eh Vesthale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muddadu Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muddadu Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Lovely Jilebi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lovely Jilebi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Peru Vinte Paradyaanamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Peru Vinte Paradyaanamey"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirivennela Raada Madhyahnamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirivennela Raada Madhyahnamey"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Pichhi Pichhigaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Pichhi Pichhigaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ittaa Nachhesthu Unte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ittaa Nachhesthu Unte"/>
</div>
<div class="lyrico-lyrics-wrapper">Eedu Goda Meeda Kodi Laaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eedu Goda Meeda Kodi Laaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuseydaa Pillaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuseydaa Pillaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadiche Oh Chandamaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadiche Oh Chandamaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Kulike Oh Sathyabhaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulike Oh Sathyabhaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Inkaaa Neekardam Kaadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inkaaa Neekardam Kaadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Prema Unnave Nuvve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Prema Unnave Nuvve"/>
</div>
<div class="lyrico-lyrics-wrapper">Tabalaa Jazz Allee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tabalaa Jazz Allee"/>
</div>
<div class="lyrico-lyrics-wrapper">Ne Beat Eh Vesthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne Beat Eh Vesthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jakir Hussainalle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jakir Hussainalle"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Andanike Nuvvu Andaniveee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Andanike Nuvvu Andaniveee"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaa Brahma Chadavani Grandhanive
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaa Brahma Chadavani Grandhanive"/>
</div>
<div class="lyrico-lyrics-wrapper">Heyyyy Chuda Sakkani Chinnari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyyyy Chuda Sakkani Chinnari"/>
</div>
<div class="lyrico-lyrics-wrapper">Masakkali Nuvve Pilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Masakkali Nuvve Pilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Okka Debbaku Naa Gunde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okka Debbaku Naa Gunde"/>
</div>
<div class="lyrico-lyrics-wrapper">Fasak Chesavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fasak Chesavey"/>
</div>
<div class="lyrico-lyrics-wrapper">Theney Maatale Koti Veenalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theney Maatale Koti Veenalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pranamanthata Mogeley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pranamanthata Mogeley"/>
</div>
<div class="lyrico-lyrics-wrapper">Itu Raave Raave Audi Caralle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Itu Raave Raave Audi Caralle"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnosaari Neney Trail Eh Vesthale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnosaari Neney Trail Eh Vesthale"/>
</div>
</pre>
