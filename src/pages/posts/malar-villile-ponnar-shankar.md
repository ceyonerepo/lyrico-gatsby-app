---
title: "malar villile song lyrics"
album: "Ponnar Shankar"
artist: "Ilaiyaraaja"
lyricist: "Vairamuthu"
director: "Thiagarajan"
path: "/albums/ponnar-shankar-lyrics"
song: "Malar Villile"
image: ../../images/albumart/ponnar-shankar.jpg
date: 2011-04-09
lang: tamil
youtubeLink: "https://www.youtube.com/embed/SJuQky5TMqg"
type: "love"
singers:
  - Shreya Ghoshal
  - Darshana K.T
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Oooo oooo Oooo oooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oooo oooo Oooo oooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Oooo oooo Oooo oooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oooo oooo Oooo oooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Malar villilae ambondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malar villilae ambondru"/>
</div>
<div class="lyrico-lyrics-wrapper">Vittaanae thozhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vittaanae thozhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Villil ambu vittaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Villil ambu vittaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Villil ambu vittaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Villil ambu vittaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Malar villilae ambondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malar villilae ambondru"/>
</div>
<div class="lyrico-lyrics-wrapper">Vittaanae thozhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vittaanae thozhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Villil ambu vittaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Villil ambu vittaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mangai nenjil kaayangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mangai nenjil kaayangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Seidhaanae thozhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seidhaanae thozhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poo vaendhan illaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poo vaendhan illaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pularaadhen pozhudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pularaadhen pozhudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaan nilavum thaen nilavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaan nilavum thaen nilavum"/>
</div>
<div class="lyrico-lyrics-wrapper">Saerndhadhae saerndhadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saerndhadhae saerndhadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Poonguruvi maalai kondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poonguruvi maalai kondu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhadhae vandhadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhadhae vandhadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mangala vaathiyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mangala vaathiyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Muzhangudhu muzhangudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muzhangudhu muzhangudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Manigalum aasaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manigalum aasaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kulungudhu kulungudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulungudhu kulungudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Manjalum kungumam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manjalum kungumam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalandhadhu kalandhadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalandhadhu kalandhadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Silirkkindra manadhinil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silirkkindra manadhinil"/>
</div>
<div class="lyrico-lyrics-wrapper">Thulirndhida arumbidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thulirndhida arumbidum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Malar villilae ambondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malar villilae ambondru"/>
</div>
<div class="lyrico-lyrics-wrapper">Vittaanae thozhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vittaanae thozhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mangai nenjil kaayangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mangai nenjil kaayangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Seidhaanae thozhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seidhaanae thozhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aasai thozhi thozhi thozhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai thozhi thozhi thozhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Odum maegangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odum maegangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru pandhal podattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru pandhal podattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaavum minnalgal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaavum minnalgal"/>
</div>
<div class="lyrico-lyrics-wrapper">Oli vattam podattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oli vattam podattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paadum paravaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadum paravaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan perai paadattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan perai paadattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanam thaedi poi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam thaedi poi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nal vaazhthu koorattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nal vaazhthu koorattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aa nilavil manjam adhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa nilavil manjam adhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaeindhu vida koodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaeindhu vida koodum"/>
</div>
<div class="lyrico-lyrics-wrapper">Malaril manjam kooda vaadi vidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malaril manjam kooda vaadi vidum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manadhil manjam kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manadhil manjam kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaekkam kolla koodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaekkam kolla koodum"/>
</div>
<div class="lyrico-lyrics-wrapper">Madiyil manjam ondru ida vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madiyil manjam ondru ida vendum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanavinil poi vidaVaram thandhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavinil poi vidaVaram thandhaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhalai kandu kollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalai kandu kollum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangalai kondaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangalai kondaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mangala vaathiyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mangala vaathiyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Muzhangudhu muzhangudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muzhangudhu muzhangudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Manigalum aasaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manigalum aasaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kulungudhu kulungudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulungudhu kulungudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Manjalum kungumam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manjalum kungumam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalandhadhu kalandhadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalandhadhu kalandhadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Silirkkindra manadhinil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silirkkindra manadhinil"/>
</div>
<div class="lyrico-lyrics-wrapper">Thulirndhida arumbidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thulirndhida arumbidum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Malar villilae ambondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malar villilae ambondru"/>
</div>
<div class="lyrico-lyrics-wrapper">Vittaanae thozhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vittaanae thozhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mangai nenjil kaayangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mangai nenjil kaayangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Seidhaanae thozhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seidhaanae thozhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalai adhi kaalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalai adhi kaalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan mugathinil yezha vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan mugathinil yezha vendum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maalai vandhaalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maalai vandhaalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan maarbinil vizha vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan maarbinil vizha vendum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poiyaai sila naeram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poiyaai sila naeram"/>
</div>
<div class="lyrico-lyrics-wrapper">Siru oodal vara vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru oodal vara vendum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaiyaal anaikkaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyaal anaikkaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan meiyaal thoda vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan meiyaal thoda vendum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nadhiyil irangidum padaginai pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadhiyil irangidum padaginai pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninaivin alaigalil avan neendha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaivin alaigalil avan neendha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aa idaiyil thavazhndhidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa idaiyil thavazhndhidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Maegalai manigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maegalai manigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannan theendida dhinam yaenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannan theendida dhinam yaenga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maayavan thirumugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maayavan thirumugam"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarppadhu thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarppadhu thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Karu mai vizhigal kalithidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karu mai vizhigal kalithidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vasantha vizhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vasantha vizhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mangala vaathiyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mangala vaathiyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Muzhangudhu muzhangudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muzhangudhu muzhangudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Manigalum aasaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manigalum aasaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kulungudhu kulungudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulungudhu kulungudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Manjalum kungumam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manjalum kungumam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalandhadhu kalandhadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalandhadhu kalandhadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Silirkkindra manadhinil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silirkkindra manadhinil"/>
</div>
<div class="lyrico-lyrics-wrapper">Thulirndhida arumbidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thulirndhida arumbidum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Malar villilae ambondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malar villilae ambondru"/>
</div>
<div class="lyrico-lyrics-wrapper">Vittaanae thozhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vittaanae thozhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaamangai nenjil kaayangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaamangai nenjil kaayangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Seidhaanae thozhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seidhaanae thozhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aasai thozhi thozhi thozhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai thozhi thozhi thozhi"/>
</div>
</pre>
