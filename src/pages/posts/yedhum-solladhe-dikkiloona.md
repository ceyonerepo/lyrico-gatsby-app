---
title: "yedhum solladhe song lyrics"
album: "Dikkiloona"
artist: "Yuvan shankar raja"
lyricist: "Ku. Karthik"
director: "Karthik Yogi"
path: "/albums/dikkiloona-song-lyrics"
song: "Yedhum Solladhe"
image: ../../images/albumart/dikkiloona.jpg
date: 2021-09-10
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Qy8q2R3aIKU"
type: "sad"
singers:
  - Yuvan Shankar Raja
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yedhum solladhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhum solladhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaam ennaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaam ennaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Endrae naanum ingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endrae naanum ingu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kandu kondenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandu kondenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaaro pol ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaro pol ennai"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum paarthalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum paarthalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirum rendu thundaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirum rendu thundaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Poga kandenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poga kandenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pen megam endhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pen megam endhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Minnal thottu kanneer sindhudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minnal thottu kanneer sindhudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Neerodai nadhi paadhai vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neerodai nadhi paadhai vittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maari ponadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maari ponadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai kolludhu en pizhai thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai kolludhu en pizhai thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh pennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh pennae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yedhum solladhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhum solladhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaam ennaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaam ennaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Endrae naanum ingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endrae naanum ingu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kandu kondenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandu kondenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaaro pol ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaro pol ennai"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum paarthalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum paarthalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirum rendu thundaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirum rendu thundaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Poga kandenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poga kandenae"/>
</div>
</pre>
