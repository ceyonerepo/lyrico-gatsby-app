---
title: "vettrimunai vel piditha song lyrics"
album: "Biskoth"
artist: "Radhan"
lyricist: "Radhan"
director: "R. Kannan"
path: "/albums/biskoth-lyrics"
song: "Vettrimunai Vel Piditha"
image: ../../images/albumart/biskoth.jpg
date: 2020-11-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/cY2VLCA7li0"
type: "melody"
singers:
  - Bamba Bhagyaraj
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">vettrimunai vel piditha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vettrimunai vel piditha"/>
</div>
<div class="lyrico-lyrics-wrapper">nattramizhan kaal pidithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nattramizhan kaal pidithu"/>
</div>
<div class="lyrico-lyrics-wrapper">katru elum kaalai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="katru elum kaalai "/>
</div>
<div class="lyrico-lyrics-wrapper">endru uthithone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="endru uthithone"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sutri varum senai kandu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sutri varum senai kandu"/>
</div>
<div class="lyrico-lyrics-wrapper">petravalai pola rendu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="petravalai pola rendu"/>
</div>
<div class="lyrico-lyrics-wrapper">porkarangam neela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="porkarangam neela"/>
</div>
<div class="lyrico-lyrics-wrapper">vaari anaithone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaari anaithone"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vettrimunai vel piditha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vettrimunai vel piditha"/>
</div>
<div class="lyrico-lyrics-wrapper">nattramizhan kaal pidithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nattramizhan kaal pidithu"/>
</div>
<div class="lyrico-lyrics-wrapper">katru elum kaalai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="katru elum kaalai "/>
</div>
<div class="lyrico-lyrics-wrapper">endru uthithone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="endru uthithone"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sutri varum senai kandu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sutri varum senai kandu"/>
</div>
<div class="lyrico-lyrics-wrapper">petravalai pola rendu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="petravalai pola rendu"/>
</div>
<div class="lyrico-lyrics-wrapper">porkarangam neela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="porkarangam neela"/>
</div>
<div class="lyrico-lyrics-wrapper">vaari anaithone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaari anaithone"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">viliyil iru sooriyangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viliyil iru sooriyangal"/>
</div>
<div class="lyrico-lyrics-wrapper">moliyil kular veesu thingal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moliyil kular veesu thingal"/>
</div>
<div class="lyrico-lyrics-wrapper">eliyavarin tholan endre
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eliyavarin tholan endre"/>
</div>
<div class="lyrico-lyrics-wrapper">elunthone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="elunthone"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">veeram vilaiyadum karamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veeram vilaiyadum karamum"/>
</div>
<div class="lyrico-lyrics-wrapper">eera kasivana manamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eera kasivana manamum"/>
</div>
<div class="lyrico-lyrics-wrapper">serum oru theeranagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="serum oru theeranagi"/>
</div>
<div class="lyrico-lyrics-wrapper">amarnthone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="amarnthone"/>
</div>
</pre>
