---
title: "yaar ivan song lyrics"
album: "Thuppakki Munai"
artist: "LV Muthu Ganesh"
lyricist: "Pa Vijay"
director: "Dinesh Selvaraj"
path: "/albums/thuppakki-munai-lyrics"
song: "Yaar Ivan"
image: ../../images/albumart/thuppakki-munai.jpg
date: 2018-12-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/f0rayc7CA6g"
type: "mass"
singers:
  - LV Muthu
  - Karthi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yaar ivan ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar ivan ivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Valiyavan idhayamilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valiyavan idhayamilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Thee kathir ivan puthir ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thee kathir ivan puthir ivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Peyero birla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peyero birla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Orr padai ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orr padai ivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadai ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadai ivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadaigal illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadaigal illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ner kunam ivan anal ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ner kunam ivan anal ivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivanae birla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivanae birla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oor moththam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oor moththam"/>
</div>
<div class="lyrico-lyrics-wrapper">Irutta irutta irutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irutta irutta irutta"/>
</div>
<div class="lyrico-lyrics-wrapper">Veratti adikkuraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veratti adikkuraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoottaavil ethiri perai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoottaavil ethiri perai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhuthi vachi azhikkiraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhuthi vachi azhikkiraan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Encounter podum naalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Encounter podum naalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Enni kondu irukkiraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enni kondu irukkiraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Gunnai eduthu triggerai azhuthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gunnai eduthu triggerai azhuthi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattam katti mudikkiraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattam katti mudikkiraan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaakki sattaiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaakki sattaiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">Garvam enkiraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Garvam enkiraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Police thimiru thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Police thimiru thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Perumai enkiraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perumai enkiraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannai thaan ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannai thaan ivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalan enkiraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalan enkiraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuppakki munaiyaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuppakki munaiyaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thozhan enkiraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozhan enkiraan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hoo ooo hoo ooo ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo ooo hoo ooo ooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaar ivan ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar ivan ivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Valiyavan idhayamilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valiyavan idhayamilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Thee kathir ivan puthir ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thee kathir ivan puthir ivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Peyero birla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peyero birla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Orr padai ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orr padai ivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadai ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadai ivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadaigal illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadaigal illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ner kunam ivan anal ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ner kunam ivan anal ivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivanae birla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivanae birla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ethiri kannai alasuraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethiri kannai alasuraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Uththu uththu paakkuraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uththu uththu paakkuraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannu kadiyil achcha nizhalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu kadiyil achcha nizhalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kandu pudichu adikkiraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandu pudichu adikkiraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Adikkiraan adikkiraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adikkiraan adikkiraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Adikkiraan adikkiraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adikkiraan adikkiraan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sattathukku mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sattathukku mela"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethuvum illaiyinnu ninaikkiraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethuvum illaiyinnu ninaikkiraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Petra thaaikku mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Petra thaaikku mela"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha epco va mathikkuraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha epco va mathikkuraan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aimpulanum koormaiyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aimpulanum koormaiyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi nenjam nermaiyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi nenjam nermaiyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivai irukkum varaikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivai irukkum varaikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai jeyikka evanum illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai jeyikka evanum illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Therichu vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Therichu vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaam thakitta theem
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaam thakitta theem"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakkita tha thakitta thigi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakkita tha thakitta thigi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakka thakka thom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakka thakka thom"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaam thakitta theem
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaam thakitta theem"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakkita tha thakitta thigi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakkita tha thakitta thigi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakka thakka thom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakka thakka thom"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakitta theem
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakitta theem"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakkita tha thakitta thigi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakkita tha thakitta thigi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakka thakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakka thakka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaar ivan ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar ivan ivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Valiyavan idhayamilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valiyavan idhayamilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Thee kathir ivan puthir ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thee kathir ivan puthir ivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Peyero birla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peyero birla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Orr padai ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orr padai ivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadai ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadai ivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadaigal illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadaigal illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ner kunam ivan anal ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ner kunam ivan anal ivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivanae birla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivanae birla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oor moththam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oor moththam"/>
</div>
<div class="lyrico-lyrics-wrapper">Irutta irutta irutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irutta irutta irutta"/>
</div>
<div class="lyrico-lyrics-wrapper">Veratti adikkuraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veratti adikkuraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoottaavil ethiri perai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoottaavil ethiri perai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhuthi vachi azhikkiraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhuthi vachi azhikkiraan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Encounter podum naalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Encounter podum naalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Enni kondu irukkiraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enni kondu irukkiraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Gunnai eduthu triggerai azhuthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gunnai eduthu triggerai azhuthi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattam katti mudikkiraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattam katti mudikkiraan"/>
</div>
</pre>