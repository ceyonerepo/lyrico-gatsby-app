---
title: "sangili bungili song lyrics"
album: "Marainthirunthu Paarkum Marmam Enna"
artist: "Achu Rajamani"
lyricist: "Pa Vijay"
director: "R. Rahesh"
path: "/albums/marainthirunthu-paarkum-marmam-enna-lyrics"
song: "Sangili Bungili"
image: ../../images/albumart/marainthirunthu-paarkum-marmam-enna.jpg
date: 2018-08-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/xo50dexY0MQ"
type: "happy"
singers:
  - Achu
  - Kaushik Menon
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">sangili bungili plana pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sangili bungili plana pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">sangili mungili seena pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sangili mungili seena pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">sattunu buttunu kiyara pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sattunu buttunu kiyara pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">sara saranu kizhichchi kaatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sara saranu kizhichchi kaatu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sangili bungili plana pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sangili bungili plana pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">sangili mungili seena pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sangili mungili seena pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">sattunu buttunu kiyara pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sattunu buttunu kiyara pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">sara saranu kizhichchi kaatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sara saranu kizhichchi kaatu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">investment onnum illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="investment onnum illa"/>
</div>
<div class="lyrico-lyrics-wrapper">it tholla illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="it tholla illa"/>
</div>
<div class="lyrico-lyrics-wrapper">business pichchukittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="business pichchukittu"/>
</div>
<div class="lyrico-lyrics-wrapper">pookudhadi enna solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pookudhadi enna solla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">investment onnum illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="investment onnum illa"/>
</div>
<div class="lyrico-lyrics-wrapper">it tholla illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="it tholla illa"/>
</div>
<div class="lyrico-lyrics-wrapper">business pichchukittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="business pichchukittu"/>
</div>
<div class="lyrico-lyrics-wrapper">pookudhadi enna solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pookudhadi enna solla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">evanukkum pangumilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="evanukkum pangumilla"/>
</div>
<div class="lyrico-lyrics-wrapper">paavam onnumilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paavam onnumilla"/>
</div>
<div class="lyrico-lyrics-wrapper">sathamilla raththamilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sathamilla raththamilla"/>
</div>
<div class="lyrico-lyrics-wrapper">sathamilla kuththamilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sathamilla kuththamilla"/>
</div>
<div class="lyrico-lyrics-wrapper">kazhattu kazhattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kazhattu kazhattu"/>
</div>
<div class="lyrico-lyrics-wrapper">kazhuththu meela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kazhuththu meela"/>
</div>
<div class="lyrico-lyrics-wrapper">vaadaa vaadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaadaa vaadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">koppam kaasum illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koppam kaasum illa"/>
</div>
<div class="lyrico-lyrics-wrapper">police kesum illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="police kesum illa"/>
</div>
<div class="lyrico-lyrics-wrapper">total laasum illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="total laasum illa"/>
</div>
<div class="lyrico-lyrics-wrapper">aadaatha attam illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aadaatha attam illa"/>
</div>
<div class="lyrico-lyrics-wrapper">konjang kuda bayamum illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konjang kuda bayamum illa"/>
</div>
<div class="lyrico-lyrics-wrapper">kootam paatha thayakamilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kootam paatha thayakamilla"/>
</div>
<div class="lyrico-lyrics-wrapper">chennai puraa enga illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chennai puraa enga illa"/>
</div>
<div class="lyrico-lyrics-wrapper">enga thollai inga illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga thollai inga illa"/>
</div>
<div class="lyrico-lyrics-wrapper">oorakkanum eada pakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oorakkanum eada pakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">kothu viralum kure vaikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kothu viralum kure vaikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">rendu veelum para parakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rendu veelum para parakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">oththa nodiyil thee pidikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oththa nodiyil thee pidikum"/>
</div>
<div class="lyrico-lyrics-wrapper">sketch pottu nadamadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sketch pottu nadamadum"/>
</div>
<div class="lyrico-lyrics-wrapper">helmet motham mugam modum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="helmet motham mugam modum"/>
</div>
<div class="lyrico-lyrics-wrapper">cheinna paatha bayam odum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cheinna paatha bayam odum"/>
</div>
<div class="lyrico-lyrics-wrapper">semma spped la kai maarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="semma spped la kai maarum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">business pichchukittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="business pichchukittu"/>
</div>
<div class="lyrico-lyrics-wrapper">pookudhadi enna solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pookudhadi enna solla"/>
</div>
<div class="lyrico-lyrics-wrapper">evanukkum pangumilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="evanukkum pangumilla"/>
</div>
<div class="lyrico-lyrics-wrapper">paavam onnumilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paavam onnumilla"/>
</div>
<div class="lyrico-lyrics-wrapper">sathamilla raththamilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sathamilla raththamilla"/>
</div>
<div class="lyrico-lyrics-wrapper">sathamilla kuththamilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sathamilla kuththamilla"/>
</div>
<div class="lyrico-lyrics-wrapper">kazhattu kazhattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kazhattu kazhattu"/>
</div>
<div class="lyrico-lyrics-wrapper">kazhuththu meela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kazhuththu meela"/>
</div>
<div class="lyrico-lyrics-wrapper">vaadaa vaadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaadaa vaadaa"/>
</div>
</pre>
