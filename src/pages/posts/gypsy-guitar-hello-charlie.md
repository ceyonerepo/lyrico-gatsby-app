---
title: "gypsy guitar song lyrics"
album: "Hello Charlie"
artist: "Gourov Dasgupta"
lyricist: "Shellee"
director: "Pankaj Saraswat"
path: "/albums/hello-charlie-lyrics"
song: "Gypsy Guitar"
image: ../../images/albumart/hello-charlie.jpg
date: 2021-04-09
lang: hindi
youtubeLink: "https://www.youtube.com/embed/snQWhqiNBRE"
type: "Love"
singers:
  - Yasser Desai
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kona, kona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kona, kona"/>
</div>
<div class="lyrico-lyrics-wrapper">Dil ka hai jagmag tujhse kona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil ka hai jagmag tujhse kona"/>
</div>
<div class="lyrico-lyrics-wrapper">Kya sona chandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kya sona chandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Koyi noor laage ankhiyan di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koyi noor laage ankhiyan di"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rag rag beh raha bas tera nasha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rag rag beh raha bas tera nasha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ishq ke taar chhede tune haan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishq ke taar chhede tune haan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ki na puche kya karaar ve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ki na puche kya karaar ve"/>
</div>
<div class="lyrico-lyrics-wrapper">Mera dil gypsy guitar ve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mera dil gypsy guitar ve"/>
</div>
<div class="lyrico-lyrics-wrapper">Ki na puche kya karaar ve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ki na puche kya karaar ve"/>
</div>
<div class="lyrico-lyrics-wrapper">Mera dil gypsy guitar ve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mera dil gypsy guitar ve"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hona, hona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hona, hona"/>
</div>
<div class="lyrico-lyrics-wrapper">Tujhse tha milna yun hi hona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tujhse tha milna yun hi hona"/>
</div>
<div class="lyrico-lyrics-wrapper">Haan yeh aazaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haan yeh aazaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Dariyaan si kuch hai meri rawaani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dariyaan si kuch hai meri rawaani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tujhe mil ke bhula main har raasta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tujhe mil ke bhula main har raasta"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuch aise taar chhede tune haan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuch aise taar chhede tune haan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ki na puche kya karaar ve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ki na puche kya karaar ve"/>
</div>
<div class="lyrico-lyrics-wrapper">Mera dil gypsy guitar ve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mera dil gypsy guitar ve"/>
</div>
<div class="lyrico-lyrics-wrapper">Ki na puche kya karaar ve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ki na puche kya karaar ve"/>
</div>
<div class="lyrico-lyrics-wrapper">Mera dil gypsy guitar ve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mera dil gypsy guitar ve"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mera dil gypsy guitar ve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mera dil gypsy guitar ve"/>
</div>
</pre>
