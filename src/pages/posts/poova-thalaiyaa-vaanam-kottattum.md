---
title: "poova thalaiyaa song lyrics"
album: "Vaanam Kottattum"
artist: "Sid Sriram"
lyricist: "Siva Ananth"
director: "Dhana"
path: "/albums/vaanam-kottattum-lyrics"
song: "Poova Thalaiyaa"
image: ../../images/albumart/vaanam-kottattum.jpg
date: 2020-02-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/hR2DrAAny8U"
type: "Motivational"
singers:
  - Sid Sriram
  - Anthony Daasan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Poova Thalaiyaa Pottu Paathen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poova Thalaiyaa Pottu Paathen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thala Keezha Thirupi Ketten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thala Keezha Thirupi Ketten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Velai Pesi Vaanga Paathen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velai Pesi Vaanga Paathen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bathile Illaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bathile Illaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aandavane Aandavane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aandavane Aandavane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konjam Pesu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Pesu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kavalai Illaa Idhayam Undaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavalai Illaa Idhayam Undaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannil Kaattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannil Kaattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Naal Iravila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Naal Iravila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Konjam Azhuthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Konjam Azhuthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yenda Poranthomnu Ninaichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenda Poranthomnu Ninaichen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaniya Oru Pidi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaniya Oru Pidi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nesam Thinnu Paathen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nesam Thinnu Paathen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thelivaachu Ul Nenju Nerupaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thelivaachu Ul Nenju Nerupaachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kekkaatha Kelvikku Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kekkaatha Kelvikku Ellaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bathil Thedi Paakkayilethaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bathil Thedi Paakkayilethaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thirakkaatha Kathavonnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirakkaatha Kathavonnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaana Oru Thirai Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaana Oru Thirai Pola"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vilakidalaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilakidalaachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vadakkukkum Thekkukkum Poga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadakkukkum Thekkukkum Poga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vazhikaatti Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhikaatti Oh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaarunnu Sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarunnu Sollu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Thaayatha Urutti Pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Thaayatha Urutti Pottu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Onnaa Aaraa Enni Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnaa Aaraa Enni Paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaarum Illa Kaatukkulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarum Illa Kaatukkulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanthaan Raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanthaan Raja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yenathu Unathendru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenathu Unathendru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yethuvum Ingey Kidaiyaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethuvum Ingey Kidaiyaathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidhigal Udaiyaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidhigal Udaiyaamal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidaigal Kannil Theriyathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidaigal Kannil Theriyathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adimel Adi Vaithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adimel Adi Vaithu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Methuvaai Methuvaai Nadai Podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Methuvaai Methuvaai Nadai Podu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mudivum Aarambam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudivum Aarambam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meendum Thodangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meendum Thodangu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mudhal Paattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhal Paattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meendum Thodangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meendum Thodangu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mudhal Paattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhal Paattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meendum Thodangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meendum Thodangu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mudhal Paattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhal Paattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaada Budhi Sollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada Budhi Sollum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thittam Mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thittam Mattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Motham Kelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Motham Kelu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yendaa Vetti Veesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yendaa Vetti Veesum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kathi Keelai Kodu Unda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathi Keelai Kodu Unda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kooru Sari Thavara Pirichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooru Sari Thavara Pirichu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vari Variyaai Padichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vari Variyaai Padichu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dharumam Needhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dharumam Needhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Velluminu Sattamilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velluminu Sattamilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadavul Ninnu Kollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadavul Ninnu Kollum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhaikalellaam Kanavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhaikalellaam Kanavu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theerppu Solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theerppu Solla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanathula Yaarum illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanathula Yaarum illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aandavane Aandavane Konjam Pesu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aandavane Aandavane Konjam Pesu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Iruntha intha Pakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Iruntha intha Pakkam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ora Paarvai Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ora Paarvai Paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaada Budhi Sollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada Budhi Sollum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thittam Mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thittam Mattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Motham Kelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Motham Kelu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yendaa Vetti Veesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yendaa Vetti Veesum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kathi Keelai Kodu Unda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathi Keelai Kodu Unda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooru"/>
</div>
</pre>
