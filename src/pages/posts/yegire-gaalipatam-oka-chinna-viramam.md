---
title: "yegire gaalipatam song lyrics"
album: "Oka Chinna Viramam"
artist: "Bharath Manchiraju"
lyricist: "Ganesh Salaadi"
director: "Sundeep Cheguri"
path: "/albums/oka-chinna-viramam-lyrics"
song: "Yegire Gaalipatam"
image: ../../images/albumart/oka-chinna-viramam.jpg
date: 2020-02-14
lang: telugu
youtubeLink: "https://www.youtube.com/embed/U4ShGpxnASM"
type: "melody"
singers:
  - Sugandh Sekar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Dama Dama Dama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dama Dama Dama"/>
</div>
<div class="lyrico-lyrics-wrapper">Theenumar Paina Kinda Mogisthare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theenumar Paina Kinda Mogisthare"/>
</div>
<div class="lyrico-lyrics-wrapper">Pita Pita Pita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pita Pita Pita"/>
</div>
<div class="lyrico-lyrics-wrapper">Lade Pilla Full Deep Love Chestunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lade Pilla Full Deep Love Chestunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Bara Bara Bara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bara Bara Bara"/>
</div>
<div class="lyrico-lyrics-wrapper">Sport Bike Rai Rai Antu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sport Bike Rai Rai Antu"/>
</div>
<div class="lyrico-lyrics-wrapper">Raise Authunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raise Authunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Attu eetu attu vunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Attu eetu attu vunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Vunna Lokam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vunna Lokam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanu Vadelsi Tanee Chuse
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanu Vadelsi Tanee Chuse"/>
</div>
<div class="lyrico-lyrics-wrapper">Adigo Akkade Akkade Regedhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adigo Akkade Akkade Regedhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Brathuku Nepalu Gurka Laa Iyindi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Brathuku Nepalu Gurka Laa Iyindi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ammai Kanpiste Pallu Ane Bayateti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammai Kanpiste Pallu Ane Bayateti"/>
</div>
<div class="lyrico-lyrics-wrapper">Navina Yadavalani Chustunte Chavu Vache
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Navina Yadavalani Chustunte Chavu Vache"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Egire Galaipatam Galipatam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Egire Galaipatam Galipatam"/>
</div>
<div class="lyrico-lyrics-wrapper">Gadipatam Ra Bhamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gadipatam Ra Bhamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Evado Evvado
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evado Evvado"/>
</div>
<div class="lyrico-lyrics-wrapper">Daram Tenchakundaa Chudaraaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daram Tenchakundaa Chudaraaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Egire Galaipatam Galipatam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Egire Galaipatam Galipatam"/>
</div>
<div class="lyrico-lyrics-wrapper">Egire Galaipatam Raa Chelima
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Egire Galaipatam Raa Chelima"/>
</div>
<div class="lyrico-lyrics-wrapper">Vadili Petavooo Anteraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadili Petavooo Anteraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Evado Manakanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evado Manakanna"/>
</div>
<div class="lyrico-lyrics-wrapper">Hight Undi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hight Undi"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhabu Undi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhabu Undi"/>
</div>
<div class="lyrico-lyrics-wrapper">Rangu Undi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangu Undi"/>
</div>
<div class="lyrico-lyrics-wrapper">Adapilla Chutune Untunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adapilla Chutune Untunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Iye Iye Iye Iye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iye Iye Iye Iye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ammai Nippu Iyna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammai Nippu Iyna"/>
</div>
<div class="lyrico-lyrics-wrapper">Uppani Ukkana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uppani Ukkana"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaba Abhabo Lekana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaba Abhabo Lekana"/>
</div>
<div class="lyrico-lyrics-wrapper">Gundelo Railegaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundelo Railegaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Iye Iye Iye Iye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iye Iye Iye Iye"/>
</div>
<div class="lyrico-lyrics-wrapper">Iye Iye Iye Iye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iye Iye Iye Iye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arere Low Neku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arere Low Neku"/>
</div>
<div class="lyrico-lyrics-wrapper">Jagetu Vestuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jagetu Vestuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Kurasa Neskatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurasa Neskatu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mokalu Piki Una
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mokalu Piki Una"/>
</div>
<div class="lyrico-lyrics-wrapper">Koriki Tintuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koriki Tintuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallu Ane Pagelelaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallu Ane Pagelelaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gummadi Kaye Ale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gummadi Kaye Ale"/>
</div>
<div class="lyrico-lyrics-wrapper">Tisthunna Diste Anta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tisthunna Diste Anta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Egire Galaipatam Galipatam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Egire Galaipatam Galipatam"/>
</div>
<div class="lyrico-lyrics-wrapper">Gadipatam Ra Bhamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gadipatam Ra Bhamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Evado Evvado
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evado Evvado"/>
</div>
<div class="lyrico-lyrics-wrapper">Daram Tenchakundaa Chudaraaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daram Tenchakundaa Chudaraaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Egire Galaipatam Galipatam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Egire Galaipatam Galipatam"/>
</div>
<div class="lyrico-lyrics-wrapper">Egire Galaipatam Raa Chelima
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Egire Galaipatam Raa Chelima"/>
</div>
<div class="lyrico-lyrics-wrapper">Vadili Petavooo Anteraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadili Petavooo Anteraa"/>
</div>
</pre>
