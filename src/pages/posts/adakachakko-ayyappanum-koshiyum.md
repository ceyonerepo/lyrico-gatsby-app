---
title: "Adakachakko song lyrics"
album: "Ayyappanum Koshiyum"
artist: "Jakes Bejoy"
lyricist: "B. K. Harinarayanan"
director: "Sachy"
path: "/albums/ayyappanum-koshiyum-lyrics"
song: "Adakachakko"
image: ../../images/albumart/ayyappanum-koshiyum.jpg
date: 2020-02-07
lang: malayalam
youtubeLink: "https://www.youtube.com/embed/BS8dtfFlS3I"
type: "happy"
singers:
  - Prithviraj Sukumaran
  - Biju Menon
  - Nanjamma
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vannotte Vannotte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vannotte Vannotte"/>
</div>
<div class="lyrico-lyrics-wrapper">Vannavan Inn Madangoola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vannavan Inn Madangoola"/>
</div>
<div class="lyrico-lyrics-wrapper">Korukkam Korthidam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Korukkam Korthidam"/>
</div>
<div class="lyrico-lyrics-wrapper">Chaavanaranividey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chaavanaranividey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaalathil Kandolam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaalathil Kandolam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaattu Pakakkali Aalunne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattu Pakakkali Aalunne"/>
</div>
<div class="lyrico-lyrics-wrapper">Njerambil Thee Poothey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Njerambil Thee Poothey"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuruthi Chinthidunney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuruthi Chinthidunney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pala Naadin Kuralaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala Naadin Kuralaaney"/>
</div>
<div class="lyrico-lyrics-wrapper">Eri Thottakk Theerillada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eri Thottakk Theerillada"/>
</div>
<div class="lyrico-lyrics-wrapper">Mundoor Maadandey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mundoor Maadandey"/>
</div>
<div class="lyrico-lyrics-wrapper">Perum Kayyitt Poottumbo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perum Kayyitt Poottumbo"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaarikuzhal Kottatheda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaarikuzhal Kottatheda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adakachakko Adachacko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adakachakko Adachacko"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Ayyappan Koshiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Ayyappan Koshiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thinthaka Porrr
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thinthaka Porrr"/>
</div>
<div class="lyrico-lyrics-wrapper">Anthichu Nikkana Naattukar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anthichu Nikkana Naattukar"/>
</div>
<div class="lyrico-lyrics-wrapper">Panthayam Vekkana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panthayam Vekkana"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholkkana Thaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholkkana Thaar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konnaalum Theerathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konnaalum Theerathey"/>
</div>
<div class="lyrico-lyrics-wrapper">Porintey Moochaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porintey Moochaaney"/>
</div>
<div class="lyrico-lyrics-wrapper">Aakasham Kottunney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aakasham Kottunney"/>
</div>
<div class="lyrico-lyrics-wrapper">Chenda Murukki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chenda Murukki"/>
</div>
<div class="lyrico-lyrics-wrapper">Anka Thadiyinakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anka Thadiyinakkam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vannotte Vannotte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vannotte Vannotte"/>
</div>
<div class="lyrico-lyrics-wrapper">Vannavan Inn Madangoola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vannavan Inn Madangoola"/>
</div>
<div class="lyrico-lyrics-wrapper">Korukkam Korthidam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Korukkam Korthidam"/>
</div>
<div class="lyrico-lyrics-wrapper">Chaavanaranividey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chaavanaranividey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalathil Kandolam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalathil Kandolam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaattu Pakakkali Aalunne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattu Pakakkali Aalunne"/>
</div>
<div class="lyrico-lyrics-wrapper">Njerambil Thee Poothey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Njerambil Thee Poothey"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuruthi Chinthidunney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuruthi Chinthidunney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kudaka Paalakalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudaka Paalakalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Ruthivechaar Ezhuthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ruthivechaar Ezhuthi"/>
</div>
<div class="lyrico-lyrics-wrapper">Oduvil Thammil Oral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oduvil Thammil Oral"/>
</div>
<div class="lyrico-lyrics-wrapper">Chithari Ponavarey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chithari Ponavarey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Udaloru Pamparamaay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udaloru Pamparamaay"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholodu Tholidaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholodu Tholidaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Podiman Paariyithaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podiman Paariyithaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Theruvo Poridamaay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theruvo Poridamaay"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chuvadu Vekkumpol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chuvadu Vekkumpol"/>
</div>
<div class="lyrico-lyrics-wrapper">Oozhikollanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oozhikollanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Viralin Thumpath Moorcha Keranu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viralin Thumpath Moorcha Keranu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanlu Kannil Kadalu Nenjil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanlu Kannil Kadalu Nenjil"/>
</div>
<div class="lyrico-lyrics-wrapper">Chavitti Kuthich Kadakam Marinju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chavitti Kuthich Kadakam Marinju"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pakalo Paathirayo Ariyaatheeadaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakalo Paathirayo Ariyaatheeadaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeraveerilitha Iniyum Korthiday
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeraveerilitha Iniyum Korthiday"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadichuvakkan Bhoomi Viraykkanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadichuvakkan Bhoomi Viraykkanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyilla Njaanilla Kaahalame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyilla Njaanilla Kaahalame"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaarmegham Thammil Muttum Matt
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaarmegham Thammil Muttum Matt"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adakachakko Adachacko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adakachakko Adachacko"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Ayyappan Koshiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Ayyappan Koshiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thinthaka Porrr
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thinthaka Porrr"/>
</div>
<div class="lyrico-lyrics-wrapper">Anthichu Nikkana Naattukar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anthichu Nikkana Naattukar"/>
</div>
<div class="lyrico-lyrics-wrapper">Panthayam Vekkana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panthayam Vekkana"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholkkana Thaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholkkana Thaar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vannotte Vannotte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vannotte Vannotte"/>
</div>
<div class="lyrico-lyrics-wrapper">Vannavan Inn Madangoola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vannavan Inn Madangoola"/>
</div>
<div class="lyrico-lyrics-wrapper">Korukkam Korthidam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Korukkam Korthidam"/>
</div>
<div class="lyrico-lyrics-wrapper">Chaavanaranividey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chaavanaranividey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalathil Kandolam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalathil Kandolam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaattu Pakakkali Aalunne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattu Pakakkali Aalunne"/>
</div>
<div class="lyrico-lyrics-wrapper">Njerambil Thee Poothey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Njerambil Thee Poothey"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuruthi Chinthidunney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuruthi Chinthidunney"/>
</div>
</pre>
