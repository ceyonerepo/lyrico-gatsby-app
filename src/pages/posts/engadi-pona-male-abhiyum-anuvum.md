---
title: "engadi pona male song lyrics"
album: "Abhiyum Anuvum"
artist: "Dharan Kumar"
lyricist: "Madhan Karky"
director: "B.R. Vijayalakshmi"
path: "/albums/abhiyum-anuvum-lyrics"
song: "Engadi Pona Male"
image: ../../images/albumart/abhiyum-anuvum.jpg
date: 2018-05-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ddp0i1tF-rY"
type: "sad"
singers:
  - Benny Dayal
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Pesiya Vaarthaigal Kasakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesiya Vaarthaigal Kasakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Veesiya Paarvaigal Arukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veesiya Paarvaigal Arukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Koosiya Theendalgal Erikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koosiya Theendalgal Erikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Erivom Urave 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Erivom Urave "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Netrai Maatrida Thudikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netrai Maatrida Thudikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Indrai Thaandida Ninaika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indrai Thaandida Ninaika"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalai Thondridum Kadakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalai Thondridum Kadakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadapom Urave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadapom Urave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Kelvikuri Unnul Valargirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Kelvikuri Unnul Valargirathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Pathil Karaikirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathil Karaikirathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Karaivom Urave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karaivom Urave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muttru Pulli Ena Vazhkkai Virigirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muttru Pulli Ena Vazhkkai Virigirathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Athil Paranthiduvọm Urave Urave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athil Paranthiduvọm Urave Urave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engadi Pona Engadi Pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engadi Pona Engadi Pona"/>
</div>
<div class="lyrico-lyrics-wrapper">Engadi Pona Ennai Vittu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engadi Pona Ennai Vittu "/>
</div>
<div class="lyrico-lyrics-wrapper">Valikuthada Valikuthada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valikuthada Valikuthada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engadi Pọna Engadi Pọna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engadi Pọna Engadi Pọna"/>
</div>
<div class="lyrico-lyrics-wrapper">Engadi Pọna Ennai Vittu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engadi Pọna Ennai Vittu "/>
</div>
<div class="lyrico-lyrics-wrapper">Valikuthada Valikuthada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valikuthada Valikuthada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhalil Vizhuvathum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhalil Vizhuvathum "/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal Udaivathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Udaivathum"/>
</div>
<div class="lyrico-lyrics-wrapper">Bọomiyil Naalume Nigazhvathuthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bọomiyil Naalume Nigazhvathuthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Aayinum Ivvali 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayinum Ivvali "/>
</div>
<div class="lyrico-lyrics-wrapper">Yaararivaar Urave Urave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaararivaar Urave Urave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kurivugal Nikalkaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurivugal Nikalkaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Pirivugal Nimmathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirivugal Nimmathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Athai Thaithara Marukkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athai Thaithara Marukkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalaththin Pernathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalaththin Pernathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Alaikalil Valiya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alaikalil Valiya "/>
</div>
<div class="lyrico-lyrics-wrapper">Naamun Nakarnthiduvom Urave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naamun Nakarnthiduvom Urave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muthamidum Antha Kanavu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthamidum Antha Kanavu "/>
</div>
<div class="lyrico-lyrics-wrapper">Kọththi Kọlluthe Iravil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kọththi Kọlluthe Iravil"/>
</div>
<div class="lyrico-lyrics-wrapper">Yudha Kalamena Manathu Urave 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yudha Kalamena Manathu Urave "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vattamidum Oru Thavaril 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vattamidum Oru Thavaril "/>
</div>
<div class="lyrico-lyrics-wrapper">Natta Naduvil Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natta Naduvil Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Viddu Sella Vazhi Sollu Urave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viddu Sella Vazhi Sollu Urave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Kelvikuri Unnul Valargirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Kelvikuri Unnul Valargirathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Pathil Karaikirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathil Karaikirathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Karaivọm Urave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karaivọm Urave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muttru Pulli Ena Vazhkkai Virigirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muttru Pulli Ena Vazhkkai Virigirathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Athil Paranthiduvọm Urave Urave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athil Paranthiduvọm Urave Urave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pesiya Vaarthaigal Kasakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesiya Vaarthaigal Kasakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Veesiya Paarvaigal Arukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veesiya Paarvaigal Arukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kọosiya Theendalgal Erikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kọosiya Theendalgal Erikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Erivọm Urave 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Erivọm Urave "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Netrai Maatrida Thudikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netrai Maatrida Thudikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Indrai Thaandida Ninaika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indrai Thaandida Ninaika"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalai Thọndridum Kadakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalai Thọndridum Kadakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadapọm Urave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadapọm Urave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engadi Pona Engadi Pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engadi Pona Engadi Pona"/>
</div>
<div class="lyrico-lyrics-wrapper">Engadi Pona Ennai Vittu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engadi Pona Ennai Vittu "/>
</div>
<div class="lyrico-lyrics-wrapper">Valikuthadi Valikuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valikuthadi Valikuthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Engadi Pona Ennai Vittu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engadi Pona Ennai Vittu "/>
</div>
<div class="lyrico-lyrics-wrapper">Valikuthadi Valikuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valikuthadi Valikuthadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engadi Pona Engadi Pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engadi Pona Engadi Pona"/>
</div>
<div class="lyrico-lyrics-wrapper">Engadi Pona Ennai Vittu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engadi Pona Ennai Vittu "/>
</div>
<div class="lyrico-lyrics-wrapper">Valikuthadi Valikuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valikuthadi Valikuthadi"/>
</div>
</pre>
