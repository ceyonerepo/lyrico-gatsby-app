---
title: "vaazhvey oru poro song lyrics"
album: "Maanagaram"
artist: "Javed Riaz"
lyricist: "Lalithanand"
director: "Lokesh Kanagaraj"
path: "/albums/maanagaram-lyrics"
song: "Vaazhvey Oru Poro"
image: ../../images/albumart/maanagaram.jpg
date: 2017-03-10
lang: tamil
youtubeLink: 
type: "mass"
singers:
  -	Sharanya Gopinath
  - Suraj Jagan
  - Lawrence
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Varum Vazhi Ingae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varum Vazhi Ingae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogum Vazhi Yengae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogum Vazhi Yengae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidhi Vazhi Sendrae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidhi Vazhi Sendrae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedu Maname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedu Maname"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadandhadhu Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadandhadhu Enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathirupadhu Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathirupadhu Enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadaigalai Vendre
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadaigalai Vendre"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedu Maname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedu Maname"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhve Oru Poro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhve Oru Poro"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaal Veesidum Kooro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaal Veesidum Kooro"/>
</div>
<div class="lyrico-lyrics-wrapper">Poratam Nooro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poratam Nooro"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar Meelvadhum Yaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar Meelvadhum Yaaro"/>
</div>
</pre>
