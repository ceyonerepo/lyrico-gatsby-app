---
title: "enthan kan munne song lyrics"
album: "Nanban"
artist: "Harris Jayaraj"
lyricist: "Madhan Karky"
director: "S. Shankar"
path: "/albums/nanban-lyrics"
song: "Enthan Kan Munne"
image: ../../images/albumart/nanban.jpg
date: 2012-01-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/gbn-ccHLQ68"
type: "Sad"
singers:
  - Aalap Raju
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Endhan Kan Munne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan Kan Munne"/>
</div>
<div class="lyrico-lyrics-wrapper">Kan Munne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan Munne"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanaamal Ponene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanaamal Ponene"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarum Paarkaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarum Paarkaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vin Meenaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vin Meenaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Veenaai Naan Aanene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veenaai Naan Aanene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhayam Kizhiyum Oli Ketten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam Kizhiyum Oli Ketten"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhaiya Idhaiya Edhir Paarthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhaiya Idhaiya Edhir Paarthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayam Kizhiyum Oli Ketten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam Kizhiyum Oli Ketten"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhaiya Idhaiya Edhir Paarthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhaiya Idhaiya Edhir Paarthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mazhai Ketkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai Ketkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Erikiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Erikiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Oli Ketkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oli Ketkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhigalai Paarkiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhigalai Paarkiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanavai Kanavai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavai Kanavai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalaiththaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalaiththaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Thodarndhida Viduvaaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodarndhida Viduvaaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Valigal Valigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valigal Valigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Koduthaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koduthaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Urangida Viduvaaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urangida Viduvaaya"/>
</div>
</pre>
