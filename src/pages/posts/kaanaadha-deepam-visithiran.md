---
title: "kaanaadha deepam song lyrics"
album: "Visithiran"
artist: "GV Prakash"
lyricist: "Yugabharathi"
director: "M Padmakumar"
path: "/albums/visithiran-lyrics"
song: "Kaanaadha Deepam"
image: ../../images/albumart/visithiran.jpg
date: 2022-05-06
lang: tamil
youtubeLink: "https://www.youtube.com/embed/qcy-FozAORU"
type: "happy"
singers:
  - Arjun
  - Sinduri
  - Padmalatha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Pon Vaan Engum Poo Minnal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pon Vaan Engum Poo Minnal"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhai Thoovudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai Thoovudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinmeen Koottam Thean
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinmeen Koottam Thean"/>
</div>
<div class="lyrico-lyrics-wrapper">Sindha Vilaiyaadudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sindha Vilaiyaadudhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Aanandham Neer Vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Aanandham Neer Vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai Serudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai Serudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Pol Ingu Yaarendru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Pol Ingu Yaarendru"/>
</div>
<div class="lyrico-lyrics-wrapper">Puram Pesudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puram Pesudhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaanaadha Deepam Ontrai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanaadha Deepam Ontrai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kangal Kaattidudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kangal Kaattidudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Kan Moodidaamal Naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan Moodidaamal Naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaana Kaadhal Kooppidudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaana Kaadhal Kooppidudhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ketkaada Keedam Ontrai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketkaada Keedam Ontrai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kaigal Meettidudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kaigal Meettidudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Pesidaamal Pesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Pesidaamal Pesum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pechchai Nenjam Kettidudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pechchai Nenjam Kettidudhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kulirodai Meedhu Yaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulirodai Meedhu Yaaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Veesiya Moongil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veesiya Moongil"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai Paadaamal Paadi Poga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai Paadaamal Paadi Poga"/>
</div>
<div class="lyrico-lyrics-wrapper">Edho Aagidudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edho Aagidudhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karum Paarai Meedhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karum Paarai Meedhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam Thooriya Thooral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam Thooriya Thooral"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai Neelaambal Poovai Maatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai Neelaambal Poovai Maatta"/>
</div>
<div class="lyrico-lyrics-wrapper">Nesam Koodidudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nesam Koodidudhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaanaadha Deebam Ontrai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanaadha Deebam Ontrai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kangal Kaattidudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kangal Kaattidudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Kan Moodidaamal Naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan Moodidaamal Naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaana Kaadhal Kooppidudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaana Kaadhal Kooppidudhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ketkaadha Keedham Ontrai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketkaadha Keedham Ontrai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kaigal Meettidudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kaigal Meettidudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Pesidaamal Pesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Pesidaamal Pesum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pechchai Nenjam Kettidudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pechchai Nenjam Kettidudhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Kai Kortha Naan Poga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kai Kortha Naan Poga"/>
</div>
<div class="lyrico-lyrics-wrapper">Thooram Kuraiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thooram Kuraiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">En Ull Moochum Unai Sera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Ull Moochum Unai Sera"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhala Kuraiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhala Kuraiyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Kan Paartha Naan Vaazha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kan Paartha Naan Vaazha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam Vidiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam Vidiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai Kaanaamal Irunthaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai Kaanaamal Irunthaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Raatiri Thodarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raatiri Thodarum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanneer Meedhu Oor Podum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanneer Meedhu Oor Podum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolam Azhium
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolam Azhium"/>
</div>
<div class="lyrico-lyrics-wrapper">Per Anbaale Uyir Podum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Per Anbaale Uyir Podum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolamaa Azhium
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolamaa Azhium"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Vaai Vaarthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Vaai Vaarthai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Ketka Vaazhven Iraium
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Ketka Vaazhven Iraium"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai Moochodu Mudi Poda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai Moochodu Mudi Poda"/>
</div>
<div class="lyrico-lyrics-wrapper">Neengume Piraium
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neengume Piraium"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaanaada Deebam Ontrai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanaada Deebam Ontrai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kangal Kaattidudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kangal Kaattidudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Kan Moodidaamal Naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan Moodidaamal Naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaana Kaadhal Kooppidudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaana Kaadhal Kooppidudhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ketkaadha Keedham Ontrai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketkaadha Keedham Ontrai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kaigal Meettidudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kaigal Meettidudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Pesidaamal Pesum Pechchai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Pesidaamal Pesum Pechchai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjam Kettidudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjam Kettidudhe"/>
</div>
</pre>
