---
title: "life ippo gaali song lyrics"
album: "Odu Raja Odu"
artist: "Tosh Nanda"
lyricist: "Ken Royson - Naveen Paul Rajan"
director: "Nishanth Ravindaran - Jathin Sanker Raj"
path: "/albums/odu-raja-odu-lyrics"
song: "Life Ippo Gaali"
image: ../../images/albumart/odu-raja-odu.jpg
date: 2018-08-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/XBLaDgiJoKk"
type: "happy"
singers:
  - Aparna
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Life-u Ippo Gaali Aachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life-u Ippo Gaali Aachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Luck-u Ippo Doacka Pochi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Luck-u Ippo Doacka Pochi"/>
</div>
<div class="lyrico-lyrics-wrapper">Lakshmiya Sanithaan Thooki Pochi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lakshmiya Sanithaan Thooki Pochi"/>
</div>
<div class="lyrico-lyrics-wrapper">Lot Of Aapu Confirm Aachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lot Of Aapu Confirm Aachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Laksham Kanavu Bulb'ah Pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laksham Kanavu Bulb'ah Pochu"/>
</div>
<div class="lyrico-lyrics-wrapper">Lorry Tyre'la Nasungi Pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lorry Tyre'la Nasungi Pochu"/>
</div>
<div class="lyrico-lyrics-wrapper">Lottery Pola Aasa Kaati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lottery Pola Aasa Kaati"/>
</div>
<div class="lyrico-lyrics-wrapper">Battery Illa Phone-ah Pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Battery Illa Phone-ah Pochu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maatikichi Meen Mullu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maatikichi Meen Mullu"/>
</div>
<div class="lyrico-lyrics-wrapper">Why
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Why"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kazhutha Kuththum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kazhutha Kuththum"/>
</div>
<div class="lyrico-lyrics-wrapper">Medhu Medhuvaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Medhu Medhuvaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Poison Kettu Saptean Naanaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poison Kettu Saptean Naanaai"/>
</div>
<div class="lyrico-lyrics-wrapper">But Sagavum Mudiyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="But Sagavum Mudiyala"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhavum Pudikala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhavum Pudikala"/>
</div>
<div class="lyrico-lyrics-wrapper">Odu Odu Odu Raa Ja Ja Ja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odu Odu Odu Raa Ja Ja Ja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dream Onnu Naanum Paarthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dream Onnu Naanum Paarthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Parthathai Mudika Planum Potten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parthathai Mudika Planum Potten"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalaiya Adaku Vechu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaiya Adaku Vechu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanaka Theepan Kadhaiya Mudipen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanaka Theepan Kadhaiya Mudipen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaathu Kedanthen Kaalam Kaniya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathu Kedanthen Kaalam Kaniya"/>
</div>
<div class="lyrico-lyrics-wrapper">Neck Momentla Pidichadhu Saniyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neck Momentla Pidichadhu Saniyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Suddenly I Fell In A Ditch
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suddenly I Fell In A Ditch"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnuchu Moochu Vada Pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnuchu Moochu Vada Pochu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maatikichi Meen Mullu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maatikichi Meen Mullu"/>
</div>
<div class="lyrico-lyrics-wrapper">Why
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Why"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kazhutha Kuththum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kazhutha Kuththum"/>
</div>
<div class="lyrico-lyrics-wrapper">Medhu Medhuvaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Medhu Medhuvaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Poison Kettu Saptean Naanaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poison Kettu Saptean Naanaai"/>
</div>
<div class="lyrico-lyrics-wrapper">But Sagavum Mudiyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="But Sagavum Mudiyala"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhavum Pudikala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhavum Pudikala"/>
</div>
<div class="lyrico-lyrics-wrapper">Odu Odu Odu Raa Ja Ja Ja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odu Odu Odu Raa Ja Ja Ja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Life-u Ippo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life-u Ippo"/>
</div>
<div class="lyrico-lyrics-wrapper">Luck-u Ippo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Luck-u Ippo"/>
</div>
</pre>
