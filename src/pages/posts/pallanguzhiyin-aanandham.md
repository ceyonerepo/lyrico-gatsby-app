---
title: "pallangkuzhiyin song lyrics"
album: "Aanandham"
artist: "S. A. Rajkumar"
lyricist: "Yugabharathi"
director: "N. Lingusamy"
path: "/albums/aanandham-lyrics"
song: "Pallangkuzhiyin"
image: ../../images/albumart/aanandham.jpg
date: 2001-05-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/kPyTSzFHvEc"
type: "love"
singers:
  - Unnikrishnan
  - Harini
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Pallaanguzhiyin Vattam Paarthen Otrai Naanayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pallaanguzhiyin Vattam Paarthen Otrai Naanayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pullaanguzhalin Thulaigal Paarthen Otrai Naanayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pullaanguzhalin Thulaigal Paarthen Otrai Naanayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thudikum Kangalil Kanmani Paarthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thudikum Kangalil Kanmani Paarthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadigarathil Neram Paarthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadigarathil Neram Paarthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sevvandhi Poovil Naduvil Paarthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sevvandhi Poovil Naduvil Paarthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhesiya Kodiyil Sakaram Paarthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhesiya Kodiyil Sakaram Paarthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Iravil Oru Naal Pournami Paarthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravil Oru Naal Pournami Paarthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Otrai Naanayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Otrai Naanayam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pallaanguzhiyin Vattam Paarthen Otrai Naanayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pallaanguzhiyin Vattam Paarthen Otrai Naanayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pullaanguzhalin Thulaigal Paarthen Otrai Naanayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pullaanguzhalin Thulaigal Paarthen Otrai Naanayam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi Kaalam Muzhuvadhum Kaathirupen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Kaalam Muzhuvadhum Kaathirupen"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Kaanum Idathinil Poothirupen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kaanum Idathinil Poothirupen"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Otrai Rubaai Pakkam Irandum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Otrai Rubaai Pakkam Irandum"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhan Anbu Serndhirukum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan Anbu Serndhirukum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil Vaithu Kaathiru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil Vaithu Kaathiru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanga Aabaranam Ondrum Thevaiyillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanga Aabaranam Ondrum Thevaiyillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha Naanayam Podhadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Naanayam Podhadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thazhuvum Manadhai Kunguma Chimizhil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thazhuvum Manadhai Kunguma Chimizhil"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaduka Mudiyadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaduka Mudiyadhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Selva Seedhanamae Nee Sirikaiyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Selva Seedhanamae Nee Sirikaiyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pala Sillarai Sidhari Vidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala Sillarai Sidhari Vidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Selavu Seithida Ninaithaal Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Selavu Seithida Ninaithaal Kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayam Padhari Vidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam Padhari Vidum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pallaanguzhiyin Vattam Paarthen Otrai Naanayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pallaanguzhiyin Vattam Paarthen Otrai Naanayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pullaanguzhalin Thulaigal Paarthen Otrai Naanayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pullaanguzhalin Thulaigal Paarthen Otrai Naanayam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Netru Nadanthadhu Naadagamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Netru Nadanthadhu Naadagamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Kaasu Koduthadhu Soosagamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kaasu Koduthadhu Soosagamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Otrai Rubaai Pakkam Irandu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Otrai Rubaai Pakkam Irandu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Solla Kaasu Thandhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Solla Kaasu Thandhaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Enni Enni Paarkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enni Enni Paarkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi Perazhagae Unnai Serndhidavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Perazhagae Unnai Serndhidavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha Naanayam Oar Saatchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Naanayam Oar Saatchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Irukum Uyirum Unakae Ubayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irukum Uyirum Unakae Ubayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Edharku Aaraaichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edharku Aaraaichi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha Naanayathil Unnai Paarthirupen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Naanayathil Unnai Paarthirupen"/>
</div>
<div class="lyrico-lyrics-wrapper">Pirar Paarkavum Vida Maaten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirar Paarkavum Vida Maaten"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadavul Vandhu Ketaal Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadavul Vandhu Ketaal Kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanikai Ida Maaten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanikai Ida Maaten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pallaanguzhiyin Vattam Paarthen Otrai Naanayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pallaanguzhiyin Vattam Paarthen Otrai Naanayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pullaanguzhalin Thulaigal Paarthen Otrai Naanayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pullaanguzhalin Thulaigal Paarthen Otrai Naanayam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thudikum Kangalil Kanmani Paarthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thudikum Kangalil Kanmani Paarthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadigarathil Neram Paarthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadigarathil Neram Paarthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Sevvandhi Poovil Naduvil Paarthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sevvandhi Poovil Naduvil Paarthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhesiya Kodiyil Sakaram Paarthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhesiya Kodiyil Sakaram Paarthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iravil Oru Naal Pournami Paarthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravil Oru Naal Pournami Paarthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Otrai Naanayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Otrai Naanayam"/>
</div>
</pre>
