---
title: "hey thikamaka modale song lyrics"
album: "Maha Samudram"
artist: "Chaitan Bharadwaj"
lyricist: "Kittu Vissapragada"
director: "Ajay Bhupathi"
path: "/albums/maha-samudram-lyrics"
song: "Hey Thikamaka Modale"
image: ../../images/albumart/maha-samudram.jpg
date: 2021-10-14
lang: telugu
youtubeLink: "https://www.youtube.com/embed/Aw0t-3l9-BU"
type: "love"
singers:
  - Haricharan
  - Nutana Mohan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hey thikamaka modhale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey thikamaka modhale"/>
</div>
<div class="lyrico-lyrics-wrapper">Yadha sodha vinadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yadha sodha vinadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Anukundhe thaduvaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anukundhe thaduvaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thega nacchi nacchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thega nacchi nacchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Picche patti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Picche patti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey teliyaka thagile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey teliyaka thagile"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholakari chinuke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholakari chinuke"/>
</div>
<div class="lyrico-lyrics-wrapper">Mohamaatam vodilo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mohamaatam vodilo"/>
</div>
<div class="lyrico-lyrics-wrapper">Saradaga jaari vaanai maare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saradaga jaari vaanai maare"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey etu ponundho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey etu ponundho"/>
</div>
<div class="lyrico-lyrics-wrapper">Daggaraga unna dooraale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daggaraga unna dooraale"/>
</div>
<div class="lyrico-lyrics-wrapper">Chali gaalullo pathangullaga thoole
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chali gaalullo pathangullaga thoole"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adagaalanna chithadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adagaalanna chithadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Choopullo yemundoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choopullo yemundoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Pedhavanchullo rahasyamlona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pedhavanchullo rahasyamlona"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalamunakalive
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalamunakalive"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aa thega thadabaduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa thega thadabaduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Porabaduthu nilabadithe ela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porabaduthu nilabadithe ela"/>
</div>
<div class="lyrico-lyrics-wrapper">Arakora chanuve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arakora chanuve"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaddhoddhani adakka niliche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaddhoddhani adakka niliche"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haa padhuguri yedure
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa padhuguri yedure"/>
</div>
<div class="lyrico-lyrics-wrapper">Thwara thwara yegabadithe ela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thwara thwara yegabadithe ela"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanuvunu thadithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanuvunu thadithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Gapchup ani manassu nalige
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gapchup ani manassu nalige"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nakki nakki dhaakkuttunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nakki nakki dhaakkuttunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Lo lo andhaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lo lo andhaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Vethikinadi visirinadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vethikinadi visirinadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Choopu kougilai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choopu kougilai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thatti thatti thaakindhemo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thatti thatti thaakindhemo"/>
</div>
<div class="lyrico-lyrics-wrapper">Preme vaanalle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Preme vaanalle"/>
</div>
<div class="lyrico-lyrics-wrapper">Minne mannai mana iddharila maraye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minne mannai mana iddharila maraye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye kshanamaina thanaloni premantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye kshanamaina thanaloni premantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Okkinthaina thirigosthundemo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okkinthaina thirigosthundemo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye vivaram nacchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye vivaram nacchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Mecchi ubbi tabbibiyindo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mecchi ubbi tabbibiyindo"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedha kadalilo alaluga yegasena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedha kadalilo alaluga yegasena"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haa padhuguri yedure
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa padhuguri yedure"/>
</div>
<div class="lyrico-lyrics-wrapper">Thwara thwara yegabadithe ela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thwara thwara yegabadithe ela"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanuvunu thadithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanuvunu thadithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Gapchup ani manassu nalige
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gapchup ani manassu nalige"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nacchi nacchi pai pai vaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nacchi nacchi pai pai vaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Preme choopinche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Preme choopinche"/>
</div>
<div class="lyrico-lyrics-wrapper">Maguvanala chulakanaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maguvanala chulakanaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Chooda raadhuga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chooda raadhuga"/>
</div>
<div class="lyrico-lyrics-wrapper">Vacchi vacchi vaalindemo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vacchi vacchi vaalindemo"/>
</div>
<div class="lyrico-lyrics-wrapper">Seetha kokalle kanne kalle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seetha kokalle kanne kalle"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa yadha gutte laagaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa yadha gutte laagaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey egasindha lolona aaratam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey egasindha lolona aaratam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaasepaina dache paniledha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasepaina dache paniledha"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa kalalo kuda nuvve vacchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa kalalo kuda nuvve vacchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Picche pattinche em erugaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Picche pattinche em erugaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeduruga nilavaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeduruga nilavaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haa padhuguri yedure
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa padhuguri yedure"/>
</div>
<div class="lyrico-lyrics-wrapper">Thwara thwara yegabadithe ela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thwara thwara yegabadithe ela"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanuvunu thadithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanuvunu thadithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Gapchup ani manassu nalige
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gapchup ani manassu nalige"/>
</div>
</pre>
