---
title: "anbe unna song lyrics"
album: "Nanjupuram"
artist: "Raaghav"
lyricist: "Magudeshwaran"
director: "Charles"
path: "/albums/nanjupuram-lyrics"
song: "Anbe Unna"
image: ../../images/albumart/nanjupuram.jpg
date: 2011-04-01
lang: tamil
youtubeLink: "https://www.youtube.com/embed/BOqJ6r9rNq0"
type: "happy"
singers:
  - JSK Sruthi
  - Kavitha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Anbey unnakkaanavandhen aathangaraikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbey unnakkaanavandhen aathangaraikku"/>
</div>
<div class="lyrico-lyrics-wrapper">ennaiye nee kondupoan kaadhal siraikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennaiye nee kondupoan kaadhal siraikku"/>
</div>
<div class="lyrico-lyrics-wrapper">athuvaanai kaattukkulla agadhiyapoala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athuvaanai kaattukkulla agadhiyapoala"/>
</div>
<div class="lyrico-lyrics-wrapper">thathalaikka vaazhureney thannanthaniyaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thathalaikka vaazhureney thannanthaniyaaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En patham mannil palakkuniyamillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En patham mannil palakkuniyamillai"/>
</div>
<div class="lyrico-lyrics-wrapper">en veeram Sondra idam theriyavillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en veeram Sondra idam theriyavillai"/>
</div>
<div class="lyrico-lyrics-wrapper">pudhu thoodhaaga selvoarum yaarumillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pudhu thoodhaaga selvoarum yaarumillai"/>
</div>
<div class="lyrico-lyrics-wrapper">manam marulikkidakkudhadi mudiyavillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manam marulikkidakkudhadi mudiyavillai"/>
</div>
<div class="lyrico-lyrics-wrapper">anji irunda manadhu innum vidiyavillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anji irunda manadhu innum vidiyavillai"/>
</div>
<div class="lyrico-lyrics-wrapper">ennai thedaadhey endhan mael naan paadavillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennai thedaadhey endhan mael naan paadavillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhoadu sonnaai kaadhal vaakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhoadu sonnaai kaadhal vaakku"/>
</div>
<div class="lyrico-lyrics-wrapper">vaaraamal poanaai aanvarakka poakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaaraamal poanaai aanvarakka poakku"/>
</div>
<div class="lyrico-lyrics-wrapper">neer paarchi vaithaai kaadhal kanji
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neer paarchi vaithaai kaadhal kanji"/>
</div>
<div class="lyrico-lyrics-wrapper">perukku arugil venneerin moochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="perukku arugil venneerin moochu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbey unnakkaanavandhen aathangaraikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbey unnakkaanavandhen aathangaraikku"/>
</div>
<div class="lyrico-lyrics-wrapper">ennaiye nee kondupoan kaadhal siraikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennaiye nee kondupoan kaadhal siraikku"/>
</div>
<div class="lyrico-lyrics-wrapper">athuvaanai kaattukkulla agadhiyapoala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athuvaanai kaattukkulla agadhiyapoala"/>
</div>
<div class="lyrico-lyrics-wrapper">thathalaikka vaazhureney thannanthaniyaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thathalaikka vaazhureney thannanthaniyaaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yezhaipen ival edhai thedipoavaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yezhaipen ival edhai thedipoavaal"/>
</div>
<div class="lyrico-lyrics-wrapper">yeikkum ulaginai endru kandu pizhivaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yeikkum ulaginai endru kandu pizhivaai"/>
</div>
<div class="lyrico-lyrics-wrapper">ellaam poi poi yedhum illai unmai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellaam poi poi yedhum illai unmai"/>
</div>
<div class="lyrico-lyrics-wrapper">illaam poi poi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="illaam poi poi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maayamey maayamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maayamey maayamey"/>
</div>
<div class="lyrico-lyrics-wrapper">maayamey indha vaazhkkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maayamey indha vaazhkkai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En patham mannil palakkuniyamillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En patham mannil palakkuniyamillai"/>
</div>
<div class="lyrico-lyrics-wrapper">en veeram Sondra idam theriyavillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en veeram Sondra idam theriyavillai"/>
</div>
<div class="lyrico-lyrics-wrapper">pudhu thoodhaaga selvoarum yaarumillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pudhu thoodhaaga selvoarum yaarumillai"/>
</div>
<div class="lyrico-lyrics-wrapper">manam marulikkidakkudhadi mudiyavillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manam marulikkidakkudhadi mudiyavillai"/>
</div>
<div class="lyrico-lyrics-wrapper">anji irunda manadhu innum vidiyavillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anji irunda manadhu innum vidiyavillai"/>
</div>
<div class="lyrico-lyrics-wrapper">ennai thedaadhey endhan mael naan paadavillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennai thedaadhey endhan mael naan paadavillai"/>
</div>
</pre>
