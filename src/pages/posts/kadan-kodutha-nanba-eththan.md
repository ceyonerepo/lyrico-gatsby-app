---
title: "kadan kodutha nanba song lyrics"
album: "Eththan"
artist: "Taj Noor"
lyricist: "Kabilan"
director: "L Suresh"
path: "/albums/eththan-lyrics"
song: "Kadan Kodutha Nanba"
image: ../../images/albumart/eththan.jpg
date: 2011-05-27
lang: tamil
youtubeLink: "https://www.youtube.com/embed/cq30CEc10cs"
type: "happy"
singers:
  - Mukesh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">kadana kodutha nanbaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadana kodutha nanbaa "/>
</div>
<div class="lyrico-lyrics-wrapper">nee besttu besttu thaandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee besttu besttu thaandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">adha udaney thiruppi kettaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adha udaney thiruppi kettaa "/>
</div>
<div class="lyrico-lyrics-wrapper">adhu konjam kashttam thaandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adhu konjam kashttam thaandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">nee konja kaalam koduthaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee konja kaalam koduthaa "/>
</div>
<div class="lyrico-lyrics-wrapper">naan munnerividuvendaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan munnerividuvendaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee konja kaalam koduthaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee konja kaalam koduthaa "/>
</div>
<div class="lyrico-lyrics-wrapper">naan munnerividuvendaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan munnerividuvendaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hei kadana vaangi thaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei kadana vaangi thaaney"/>
</div>
<div class="lyrico-lyrics-wrapper">indha Ambaani Bilkatsu Mallaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indha Ambaani Bilkatsu Mallaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Malkotha vaazhndhu kaattalaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malkotha vaazhndhu kaattalaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">yaa yaa yaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaa yaa yaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kadana kodutha nanbaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadana kodutha nanbaa "/>
</div>
<div class="lyrico-lyrics-wrapper">nee besttu besttu thaandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee besttu besttu thaandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">adha udaney thiruppi kettaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adha udaney thiruppi kettaa "/>
</div>
<div class="lyrico-lyrics-wrapper">adhu konjam kashttam thaandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adhu konjam kashttam thaandaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meththamele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meththamele"/>
</div>
<div class="lyrico-lyrics-wrapper">Car-a paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Car-a paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">paththumaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paththumaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Veettappaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veettappaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">ellaamey kadan vaangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellaamey kadan vaangi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattiyadhuthaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattiyadhuthaaney"/>
</div>
<div class="lyrico-lyrics-wrapper">hei suvaru meley poster paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hei suvaru meley poster paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">posttarukkulley padaththappaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="posttarukkulley padaththappaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">ellaamey kadan vaangi eduthadhuthaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellaamey kadan vaangi eduthadhuthaaney"/>
</div>
<div class="lyrico-lyrics-wrapper">adi enakkaaga kadan koduthavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi enakkaaga kadan koduthavan"/>
</div>
<div class="lyrico-lyrics-wrapper">ada avanthaaney en kadavuladaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada avanthaaney en kadavuladaa"/>
</div>
<div class="lyrico-lyrics-wrapper">ada kathirikka thodangi Computer varaikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada kathirikka thodangi Computer varaikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">ulagathil kadathaan avasiyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulagathil kadathaan avasiyam"/>
</div>
<div class="lyrico-lyrics-wrapper">un jalanam thoadangi maranam varaikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un jalanam thoadangi maranam varaikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">kodukkal vaangal avasiyam avasiyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kodukkal vaangal avasiyam avasiyam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kadana kodutha nanbaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadana kodutha nanbaa "/>
</div>
<div class="lyrico-lyrics-wrapper">nee besttu besttu thaandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee besttu besttu thaandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">adha udaney thiruppi kettaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adha udaney thiruppi kettaa "/>
</div>
<div class="lyrico-lyrics-wrapper">adhu konjam kashttam thaandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adhu konjam kashttam thaandaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadana vaangi seiyum thozhilil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadana vaangi seiyum thozhilil "/>
</div>
<div class="lyrico-lyrics-wrapper">kaalaneram serumboadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaalaneram serumboadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">thadaigalum padiyaai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thadaigalum padiyaai "/>
</div>
<div class="lyrico-lyrics-wrapper">maarividum thaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maarividum thaaney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hei kaatrey illaa boomi illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei kaatrey illaa boomi illa"/>
</div>
<div class="lyrico-lyrics-wrapper">kadaney illaa manidhan illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadaney illaa manidhan illai"/>
</div>
<div class="lyrico-lyrics-wrapper">kadavulum kadan vaangithaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadavulum kadan vaangithaaney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mudhal thedhi adhu oru naaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhal thedhi adhu oru naaley"/>
</div>
<div class="lyrico-lyrics-wrapper">ada kadan thedhi adhu pala naaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada kadan thedhi adhu pala naaley"/>
</div>
<div class="lyrico-lyrics-wrapper">naan kadanaa vaangiya panathathaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan kadanaa vaangiya panathathaaney"/>
</div>
<div class="lyrico-lyrics-wrapper">naalu madangaai perukkuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naalu madangaai perukkuven"/>
</div>
<div class="lyrico-lyrics-wrapper">kadan vaangum naaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadan vaangum naaney"/>
</div>
<div class="lyrico-lyrics-wrapper">palarukkum kadan tharum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="palarukkum kadan tharum "/>
</div>
<div class="lyrico-lyrics-wrapper">naal varumey varumey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naal varumey varumey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kadana kodutha nanbaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadana kodutha nanbaa "/>
</div>
<div class="lyrico-lyrics-wrapper">nee besttu besttu thaandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee besttu besttu thaandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">adha udaney thiruppi kettaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adha udaney thiruppi kettaa "/>
</div>
<div class="lyrico-lyrics-wrapper">adhu konjam kashttam thaandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adhu konjam kashttam thaandaa"/>
</div>
</pre>
