---
title: "hello saare song lyrics"
album: "Thambi"
artist: "Govind Vasantha"
lyricist: "Karthik Netha"
director: "Jeethu Joseph"
path: "/albums/thambi-lyrics"
song: "Hello Saare"
image: ../../images/albumart/thambi.jpg
date: 2019-12-20
lang: tamil
youtubeLink: "https://www.youtube.com/embed/DJZeiu95CDQ"
type: "happy"
singers:
  - Suresh Peters
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hello saarae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hello saarae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unga trouser ellaam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unga trouser ellaam "/>
</div>
<div class="lyrico-lyrics-wrapper">avukka poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avukka poren"/>
</div>
<div class="lyrico-lyrics-wrapper">Erakkam edhum illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Erakkam edhum illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Inchi inchaai thiruda poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inchi inchaai thiruda poren"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada karumam unga dharumam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada karumam unga dharumam"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu pinam thinni sasththiram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu pinam thinni sasththiram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hello saarae saarae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hello saarae saarae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanna moodi vaazha poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna moodi vaazha poren"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuniya vachi unga mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuniya vachi unga mela"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeri poga poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeri poga poren"/>
</div>
<div class="lyrico-lyrics-wrapper">Emotions are illusions
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emotions are illusions"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada kurangu kai mela poovo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada kurangu kai mela poovo"/>
</div>
<div class="lyrico-lyrics-wrapper">So called vaazhvil thevai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="So called vaazhvil thevai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mmaanang ketta rooba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mmaanang ketta rooba"/>
</div>
<div class="lyrico-lyrics-wrapper">Rooba rooba rooba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rooba rooba rooba"/>
</div>
<div class="lyrico-lyrics-wrapper">Konaa paavam thinaa pochii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konaa paavam thinaa pochii"/>
</div>
<div class="lyrico-lyrics-wrapper">En anukkal ovvondrum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En anukkal ovvondrum "/>
</div>
<div class="lyrico-lyrics-wrapper">mirugamae hoouuuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mirugamae hoouuuu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhndhaalum i need money
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhndhaalum i need money"/>
</div>
<div class="lyrico-lyrics-wrapper">Veezhndhaalum i need money
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veezhndhaalum i need money"/>
</div>
<div class="lyrico-lyrics-wrapper">Sethaalum en saambal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sethaalum en saambal "/>
</div>
<div class="lyrico-lyrics-wrapper">yelam pogumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yelam pogumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanalum hiding Money
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanalum hiding Money"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponaalum i need money
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponaalum i need money"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanneerum kai netti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneerum kai netti"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaasu ketkkumae hoouuuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasu ketkkumae hoouuuu"/>
</div>
</pre>
