---
title: "kavan irundhu song lyrics"
album: "Bachelor"
artist: "Santhosh Sivashanmugam - Sathish Selvakumar"
lyricist: "Navakkarai Naveen Prabanjam"
director: "Sathish Selvakumar"
path: "/albums/bachelor-lyrics"
song: "Kavan Irundhu"
image: ../../images/albumart/bachelor.jpg
date: 2021-12-03
lang: tamil
youtubeLink: "https://www.youtube.com/embed/YWNahFxfAFs"
type: "sad"
singers:
  - Navakkarai Naveen Prabanjam
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">kavan erinthu aalum sellum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kavan erinthu aalum sellum"/>
</div>
<div class="lyrico-lyrics-wrapper">gaana mayile alagana mayile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gaana mayile alagana mayile"/>
</div>
<div class="lyrico-lyrics-wrapper">inge kavaluku vantha ethum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inge kavaluku vantha ethum"/>
</div>
<div class="lyrico-lyrics-wrapper">ethu kuyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethu kuyile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nanan nanam nane nanan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanan nanam nane nanan"/>
</div>
<div class="lyrico-lyrics-wrapper">nane nanuna nanan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nane nanuna nanan"/>
</div>
<div class="lyrico-lyrics-wrapper">nane nanuna nanan nanuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nane nanuna nanan nanuna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vambu thumbu pesavila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vambu thumbu pesavila"/>
</div>
<div class="lyrico-lyrics-wrapper">valli mane adi pulli mane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valli mane adi pulli mane"/>
</div>
<div class="lyrico-lyrics-wrapper">nee vanthu oru muthuam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee vanthu oru muthuam"/>
</div>
<div class="lyrico-lyrics-wrapper">thaadi valli mane 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaadi valli mane "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nanan nanam nane nanan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanan nanam nane nanan"/>
</div>
<div class="lyrico-lyrics-wrapper">nane nanuna nanan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nane nanuna nanan"/>
</div>
<div class="lyrico-lyrics-wrapper">nane nanuna nanan nanuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nane nanuna nanan nanuna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kayilangu thinnun koora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kayilangu thinnun koora"/>
</div>
<div class="lyrico-lyrics-wrapper">mathe unthanai ada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mathe unthanai ada"/>
</div>
<div class="lyrico-lyrics-wrapper">mathe unthanai nan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mathe unthanai nan "/>
</div>
<div class="lyrico-lyrics-wrapper">kanagathil kandu nanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanagathil kandu nanum"/>
</div>
<div class="lyrico-lyrics-wrapper">kanden senthene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanden senthene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nanan nanam nane nanan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanan nanam nane nanan"/>
</div>
<div class="lyrico-lyrics-wrapper">nane nanuna nanan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nane nanuna nanan"/>
</div>
<div class="lyrico-lyrics-wrapper">nane nanuna nanan nanuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nane nanuna nanan nanuna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vambu thumbu pesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vambu thumbu pesa"/>
</div>
<div class="lyrico-lyrics-wrapper">vendam vede vananere
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vendam vede vananere"/>
</div>
<div class="lyrico-lyrics-wrapper">vana vendum vanare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vana vendum vanare"/>
</div>
<div class="lyrico-lyrics-wrapper">nee vantha vali sella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee vantha vali sella"/>
</div>
<div class="lyrico-lyrics-wrapper">venum venu vanare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="venum venu vanare"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nanan nanam nane nanan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanan nanam nane nanan"/>
</div>
<div class="lyrico-lyrics-wrapper">nane nanuna nanan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nane nanuna nanan"/>
</div>
<div class="lyrico-lyrics-wrapper">nane nanuna nanan nanuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nane nanuna nanan nanuna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">adi vai thiranthu mozhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi vai thiranthu mozhi"/>
</div>
<div class="lyrico-lyrics-wrapper">sonnal muthu uthirumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sonnal muthu uthirumo"/>
</div>
<div class="lyrico-lyrics-wrapper">aali muthu uthirumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aali muthu uthirumo"/>
</div>
<div class="lyrico-lyrics-wrapper">un varnam ethu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un varnam ethu "/>
</div>
<div class="lyrico-lyrics-wrapper">theriya venum thananinu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theriya venum thananinu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kavan erinthu aalum sellum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kavan erinthu aalum sellum"/>
</div>
<div class="lyrico-lyrics-wrapper">gaana mayile alagana mayile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gaana mayile alagana mayile"/>
</div>
<div class="lyrico-lyrics-wrapper">inge kavaluku vantha ethum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inge kavaluku vantha ethum"/>
</div>
<div class="lyrico-lyrics-wrapper">ethu kuyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethu kuyile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nanan nanam nane nanan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanan nanam nane nanan"/>
</div>
<div class="lyrico-lyrics-wrapper">nane nanuna nanan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nane nanuna nanan"/>
</div>
<div class="lyrico-lyrics-wrapper">nane nanuna nanan nanuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nane nanuna nanan nanuna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vambu thumbu pesavila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vambu thumbu pesavila"/>
</div>
<div class="lyrico-lyrics-wrapper">valli mane adi pulli mane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valli mane adi pulli mane"/>
</div>
<div class="lyrico-lyrics-wrapper">nee vanthu oru muthuam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee vanthu oru muthuam"/>
</div>
<div class="lyrico-lyrics-wrapper">thaadi valli mane 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaadi valli mane "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nanan nanam nane nanan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanan nanam nane nanan"/>
</div>
<div class="lyrico-lyrics-wrapper">nane nanuna nanan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nane nanuna nanan"/>
</div>
<div class="lyrico-lyrics-wrapper">nane nanuna nanan nanuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nane nanuna nanan nanuna"/>
</div>
</pre>
