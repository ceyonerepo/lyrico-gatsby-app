---
title: "okko kshanamulo song lyrics"
album: "Appudu Ippudu"
artist: "Padmanav Bharadwaj"
lyricist: "Chirravuri Vijaykumar"
director: "Chalapathi Puvvala"
path: "/albums/appudu-ippudu-lyrics"
song: "Okko Kshanamulo"
image: ../../images/albumart/appudu-ippudu.jpg
date: 2021-09-03
lang: telugu
youtubeLink: "https://www.youtube.com/embed/RYq6UgSo3S0"
type: "love"
singers:
  - Harika Narayan
  - Lokesh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">okko kshanamulo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="okko kshanamulo"/>
</div>
<div class="lyrico-lyrics-wrapper">okko vidamuga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="okko vidamuga"/>
</div>
<div class="lyrico-lyrics-wrapper">tha tha tha tha repake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tha tha tha tha repake"/>
</div>
<div class="lyrico-lyrics-wrapper">okko kshanamulo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="okko kshanamulo"/>
</div>
<div class="lyrico-lyrics-wrapper">okko vidamuga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="okko vidamuga"/>
</div>
<div class="lyrico-lyrics-wrapper">the the thikamaka pettake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="the the thikamaka pettake"/>
</div>
<div class="lyrico-lyrics-wrapper">ennandhalanu munupu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennandhalanu munupu"/>
</div>
<div class="lyrico-lyrics-wrapper">kallenaadu erugave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kallenaadu erugave"/>
</div>
<div class="lyrico-lyrics-wrapper">sandhram maadhiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sandhram maadhiri"/>
</div>
<div class="lyrico-lyrics-wrapper">manasu ninge 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasu ninge "/>
</div>
<div class="lyrico-lyrics-wrapper">daati egirene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="daati egirene"/>
</div>
<div class="lyrico-lyrics-wrapper">adugule tullene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adugule tullene"/>
</div>
<div class="lyrico-lyrics-wrapper">aashale tarimene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aashale tarimene"/>
</div>
<div class="lyrico-lyrics-wrapper">allare pallavai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="allare pallavai"/>
</div>
<div class="lyrico-lyrics-wrapper">alluko alludai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alluko alludai"/>
</div>
<div class="lyrico-lyrics-wrapper">andame sandya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="andame sandya"/>
</div>
<div class="lyrico-lyrics-wrapper">vela abhayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vela abhayam"/>
</div>
<div class="lyrico-lyrics-wrapper">nara naram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nara naram"/>
</div>
<div class="lyrico-lyrics-wrapper">chali jwaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chali jwaram"/>
</div>
<div class="lyrico-lyrics-wrapper">okataye eruvuram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="okataye eruvuram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">okko kshanamulo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="okko kshanamulo"/>
</div>
<div class="lyrico-lyrics-wrapper">okko vidamuga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="okko vidamuga"/>
</div>
<div class="lyrico-lyrics-wrapper">tha tha tha tha repake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tha tha tha tha repake"/>
</div>
<div class="lyrico-lyrics-wrapper">okko kshanamulo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="okko kshanamulo"/>
</div>
<div class="lyrico-lyrics-wrapper">okko vidamuga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="okko vidamuga"/>
</div>
<div class="lyrico-lyrics-wrapper">the the thikamaka pettake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="the the thikamaka pettake"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">jillumannade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jillumannade"/>
</div>
<div class="lyrico-lyrics-wrapper">yeda madi gadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yeda madi gadi"/>
</div>
<div class="lyrico-lyrics-wrapper">pelli thondare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pelli thondare"/>
</div>
<div class="lyrico-lyrics-wrapper">gollumannade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gollumannade"/>
</div>
<div class="lyrico-lyrics-wrapper">gada bida vayase
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gada bida vayase"/>
</div>
<div class="lyrico-lyrics-wrapper">niddare pattade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="niddare pattade"/>
</div>
<div class="lyrico-lyrics-wrapper">niluvuna dadaputtane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="niluvuna dadaputtane"/>
</div>
<div class="lyrico-lyrics-wrapper">vechhaga nuli vechhaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vechhaga nuli vechhaga"/>
</div>
<div class="lyrico-lyrics-wrapper">pichhigaa yama pichhiga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pichhigaa yama pichhiga"/>
</div>
<div class="lyrico-lyrics-wrapper">kulaasa kuhu kuhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulaasa kuhu kuhu"/>
</div>
<div class="lyrico-lyrics-wrapper">vilasa virahame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vilasa virahame"/>
</div>
<div class="lyrico-lyrics-wrapper">kummuthunnade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kummuthunnade"/>
</div>
<div class="lyrico-lyrics-wrapper">kumari guma guma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kumari guma guma"/>
</div>
<div class="lyrico-lyrics-wrapper">chalaaki chima chima
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chalaaki chima chima"/>
</div>
<div class="lyrico-lyrics-wrapper">kummuthunnade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kummuthunnade"/>
</div>
</pre>
