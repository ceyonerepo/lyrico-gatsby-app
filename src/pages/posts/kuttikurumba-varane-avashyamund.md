---
title: "kuttikurumba song lyrics"
album: "Varane Avashyamund"
artist: "Alphons Joseph"
lyricist: "Santhosh Varma - Anoop Sathyan"
director: "Anoop Sathyan"
path: "/albums/varane-avashyamund-lyrics"
song: "Kuttikurumba"
image: ../../images/albumart/varane-avashyamund.jpg
date: 2020-02-07
lang: malayalam
youtubeLink: "https://www.youtube.com/embed/51Up5WFTgwM"
type: "melody"
singers:
  - K. S. Chithra
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kuttikkurumba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuttikkurumba"/>
</div>
<div class="lyrico-lyrics-wrapper">Kombulla Vamba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kombulla Vamba"/>
</div>
<div class="lyrico-lyrics-wrapper">Ambili Kombil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ambili Kombil"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninne Iruthan Aaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninne Iruthan Aaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Vannathaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vannathaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninne Merukkan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninne Merukkan"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Kayyilente
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Kayyilente"/>
</div>
<div class="lyrico-lyrics-wrapper">Maanikya Cheppil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maanikya Cheppil"/>
</div>
<div class="lyrico-lyrics-wrapper">Snehathin Theera Neeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Snehathin Theera Neeru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ila Neeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ila Neeru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athile Ithile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athile Ithile"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Kaattum Ninte Kurumbum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kaattum Ninte Kurumbum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaliyaay Karuthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaliyaay Karuthan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninte Ettane Pole Neeyaararu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninte Ettane Pole Neeyaararu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuttikkurumba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuttikkurumba"/>
</div>
<div class="lyrico-lyrics-wrapper">Kombulla Vamba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kombulla Vamba"/>
</div>
<div class="lyrico-lyrics-wrapper">Ambili Kombil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ambili Kombil"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninne Iruthan Aaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninne Iruthan Aaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Vannathaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vannathaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Minnunnoru Kannullatharaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minnunnoru Kannullatharaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalla Nottam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalla Nottam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nokkunnathaara Nithaaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nokkunnathaara Nithaaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pennivalithaaraaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pennivalithaaraaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mele Etha Kombil Eridunnol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mele Etha Kombil Eridunnol"/>
</div>
<div class="lyrico-lyrics-wrapper">Kunju Urumbin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kunju Urumbin"/>
</div>
<div class="lyrico-lyrics-wrapper">Koodonnu Kandaal Melle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koodonnu Kandaal Melle"/>
</div>
<div class="lyrico-lyrics-wrapper">Chaanju Nilkkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chaanju Nilkkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thenmaavin Chilla Chaadiyirangunnoll
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thenmaavin Chilla Chaadiyirangunnoll"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kali Paranjaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kali Paranjaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thellonnunidanjal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thellonnunidanjal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannu Chuvappichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu Chuvappichu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paanju Varukilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paanju Varukilum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aliyum Thenaayi Arike
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aliyum Thenaayi Arike"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuttikkurumbi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuttikkurumbi"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjunna Kalli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjunna Kalli"/>
</div>
<div class="lyrico-lyrics-wrapper">Ambili Kombil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ambili Kombil"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninne Iruthan Aaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninne Iruthan Aaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Vannathaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vannathaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naaveriyum Kaanthaari Pole
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naaveriyum Kaanthaari Pole"/>
</div>
<div class="lyrico-lyrics-wrapper">Thottal Pollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottal Pollum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thee Kanal Poleoruthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thee Kanal Poleoruthi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivalkku Enthu Patty
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivalkku Enthu Patty"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaane Kettum Mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaane Kettum Mattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Maari Thudangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maari Thudangi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vannu Koottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vannu Koottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Koodi Thudangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koodi Thudangi"/>
</div>
<div class="lyrico-lyrics-wrapper">Enno Illa Kadhayaanennalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enno Illa Kadhayaanennalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kelkkaan Ishtam Vannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelkkaan Ishtam Vannu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Olichirikkyan Odimarayaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Olichirikkyan Odimarayaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini Aavillalla Eppozho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Aavillalla Eppozho"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoni Thudangiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoni Thudangiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Pathiye Chaayaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathiye Chaayaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kothiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kothiyo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuttikkurumbi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuttikkurumbi"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjunna Kalli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjunna Kalli"/>
</div>
<div class="lyrico-lyrics-wrapper">Ambili Kombil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ambili Kombil"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninne Iruthan Aaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninne Iruthan Aaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Vannathaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vannathaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninne Merukkan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninne Merukkan"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Kayyilente
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Kayyilente"/>
</div>
<div class="lyrico-lyrics-wrapper">Maanikya Cheppil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maanikya Cheppil"/>
</div>
<div class="lyrico-lyrics-wrapper">Snehathin Theera Neeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Snehathin Theera Neeru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ila Neeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ila Neeru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athile Ithile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athile Ithile"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Kaattum Ninte Kurumbum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kaattum Ninte Kurumbum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaliyaay Karuthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaliyaay Karuthan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninte Ettane Pole Neeyaararu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninte Ettane Pole Neeyaararu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuttikkurumbi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuttikkurumbi"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjunna Kalli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjunna Kalli"/>
</div>
<div class="lyrico-lyrics-wrapper">Ambili Kombil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ambili Kombil"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninne Iruthan Aaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninne Iruthan Aaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Vannathaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vannathaaru"/>
</div>
</pre>
