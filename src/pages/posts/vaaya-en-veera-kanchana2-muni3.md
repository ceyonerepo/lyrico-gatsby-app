---
title: "vaaya en veera song lyrics"
album: "Kanchana2-Muni3"
artist: "S. Thaman - Leon James - C. Sathya"
lyricist: "	Ko. Sesha"
director: "Raghava Lawrence"
path: "/albums/kanchana2-muni3-lyrics"
song: "Vaaya En Veera"
image: ../../images/albumart/kanchana2-muni3.jpg
date: 2015-04-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Gl9Ln-BBvbE"
type: "Love"
singers:
  - Shakthisree Gopalan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Raappagalaa Azhudhaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raappagalaa Azhudhaachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannurendum Vaadi Pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannurendum Vaadi Pochu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naappadhunaal Naal Vidinjaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naappadhunaal Naal Vidinjaachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thurumbena Elachaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thurumbena Elachaachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aasa Noyi Aaraadhaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasa Noyi Aaraadhaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Masangu Vizhi Kasangudhaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Masangu Vizhi Kasangudhaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai Pudikka Neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai Pudikka Neeyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaaya En Veera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaya En Veera"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannakkuzhi Kuzhi Kaanju Kedakkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannakkuzhi Kuzhi Kaanju Kedakkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaya En Veera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaya En Veera"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil Vali Vali Konjam Maranju Pogattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil Vali Vali Konjam Maranju Pogattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaaya En Veera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaya En Veera"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannakkuzhi Kuzhi Kaanju Kedakkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannakkuzhi Kuzhi Kaanju Kedakkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaya Nee Vaayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaya Nee Vaayaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayil Thogai Meley Mazhaiyai Polavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayil Thogai Meley Mazhaiyai Polavae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Moochukkaaththula Maarudham Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moochukkaaththula Maarudham Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Mama Vaa Maarbodu Paanjikko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mama Vaa Maarbodu Paanjikko"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam Saanjikko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Saanjikko"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Menjikko Nidhaanamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Menjikko Nidhaanamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raasaava Un Rosaa Poovum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raasaava Un Rosaa Poovum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naandhaaney Nenjil Ennai Vedhachikko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naandhaaney Nenjil Ennai Vedhachikko"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam Anaichikko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Anaichikko"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Valaichikko Dhaaraalama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Valaichikko Dhaaraalama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neelaadho Nee Ennai Theendum Nimishangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelaadho Nee Ennai Theendum Nimishangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nooru Jenmam Ponaal Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nooru Jenmam Ponaal Enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Needhaan En Sondham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Needhaan En Sondham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaaya En Veera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaya En Veera"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannakkuzhi Kuzhi Kaanju Kedakkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannakkuzhi Kuzhi Kaanju Kedakkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaya En Veera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaya En Veera"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil Vali Vali Konjam Maranju Pogattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil Vali Vali Konjam Maranju Pogattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaaya En Veera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaya En Veera"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannakkuzhi Kuzhi Kaanju Kedakkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannakkuzhi Kuzhi Kaanju Kedakkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaya Nee Vaayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaya Nee Vaayaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayil Thogai Meley Mazhaiyai Polavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayil Thogai Meley Mazhaiyai Polavae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaarthigai Pochu Maargazhi Aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaarthigai Pochu Maargazhi Aachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Panikaathum Analpola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panikaathum Analpola"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodhikkudhey Nadhi Thudikkudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodhikkudhey Nadhi Thudikkudhey"/>
</div>
<div class="lyrico-lyrics-wrapper">Parithavikkudhey Paayaamathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parithavikkudhey Paayaamathaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paavai Thaabam Yaarukku Laabam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paavai Thaabam Yaarukku Laabam"/>
</div>
<div class="lyrico-lyrics-wrapper">Puyalodu Elapola Usur Odudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puyalodu Elapola Usur Odudhey"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Koodavey Unnai Thedudhey Oyaamathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Koodavey Unnai Thedudhey Oyaamathaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhaadhey Poonkodi Kaatre Varudaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhaadhey Poonkodi Kaatre Varudaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinveliye Vaanavilpol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinveliye Vaanavilpol"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnal Maaraadho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnal Maaraadho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaaya En Veera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaya En Veera"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannakkuzhi Kuzhi Kaanju Kedakkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannakkuzhi Kuzhi Kaanju Kedakkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaya En Veera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaya En Veera"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil Vali Vali Konjam Maranju Pogattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil Vali Vali Konjam Maranju Pogattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaaya En Veera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaya En Veera"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannakkuzhi Kuzhi Kaanju Kedakkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannakkuzhi Kuzhi Kaanju Kedakkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaya Nee Vaayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaya Nee Vaayaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayil Thogai Meley Mazhaiyai Polavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayil Thogai Meley Mazhaiyai Polavae"/>
</div>
</pre>
