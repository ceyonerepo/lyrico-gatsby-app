---
title: "yaarathu yaarathu song lyrics"
album: "Kaavalan"
artist: "Vidyasagar"
lyricist: "Yugabharathi"
director: "Siddique"
path: "/albums/kaavalan-lyrics"
song: "Yaarathu Yaarathu"
image: ../../images/albumart/kaavalan.jpg
date: 2011-01-15
lang: tamil
youtubeLink: "https://www.youtube.com/embed/E4S5bkdYHMs"
type: "love"
singers:
  - Karthik
  - Suchitra
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yaarathu Yaarathu Yaarathu Yaarathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarathu Yaarathu Yaarathu Yaarathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sollaamal Nenjaththai Thollai Seivathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollaamal Nenjaththai Thollai Seivathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Moodaamal Kanrendai Moodi Selvathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moodaamal Kanrendai Moodi Selvathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yar Athu Yar Athu Yar Athu Yaar Athu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yar Athu Yar Athu Yar Athu Yaar Athu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nerungaamal Nerungi Vanthadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nerungaamal Nerungi Vanthadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vilagaamal Vilagi Nirpathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilagaamal Vilagi Nirpathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vinaiyaaga Kelvi Thanthadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinaiyaaga Kelvi Thanthadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thelivaaga Kulamba Nenaipadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thelivaaga Kulamba Nenaipadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaar Athu Yaar Athu Yaar Athu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar Athu Yaar Athu Yaar Athu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaar Yaar Athu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar Yaar Athu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennil Oru Sadu Gudu Sadu Gudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennil Oru Sadu Gudu Sadu Gudu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalai Maalai Nadakiradhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalai Maalai Nadakiradhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannil Dhinam Kathakali Kathakali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannil Dhinam Kathakali Kathakali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thoonkumbothu Thodargiradhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoonkumbothu Thodargiradhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennil Oru Sadu Gudu Sadu Gudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennil Oru Sadu Gudu Sadu Gudu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalai Maalai Nadakiradhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalai Maalai Nadakiradhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannil Dhinam Kathakali Kathakali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannil Dhinam Kathakali Kathakali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thoonkumbothu Thodargiradhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoonkumbothu Thodargiradhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iravilum Aval Pagalilum Aval
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravilum Aval Pagalilum Aval"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manadhinai Thoduvadhu Therigiradhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manadhinai Thoduvadhu Therigiradhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanavilum Aval Ninaivilum Aval
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavilum Aval Ninaivilum Aval"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nizhalena Thodarvathu Purigiradhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nizhalena Thodarvathu Purigiradhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irunthaalum Illaa Avalai Idhayam Thedudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irunthaalum Illaa Avalai Idhayam Thedudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaar Athu Yaar Athu Yaar Athu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar Athu Yaar Athu Yaar Athu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaar Yaar Athu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar Yaar Athu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uchan Thalai Naduvinil Aval
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uchan Thalai Naduvinil Aval"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Vedhaalam Pol Erangugiraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Vedhaalam Pol Erangugiraal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennul Aval Erangiya Thimurinil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennul Aval Erangiya Thimurinil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Imsai Raajiyam Thodangugiraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imsai Raajiyam Thodangugiraal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aval Ival Ena Eval Eval Ena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aval Ival Ena Eval Eval Ena"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maraivinil Irunthaval Kuzhapugiraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maraivinil Irunthaval Kuzhapugiraal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avalathu Mugam Evalaiyum Vida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avalathu Mugam Evalaiyum Vida"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhagilum Azhagena Unarthugiraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagilum Azhagena Unarthugiraal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irunthaalum Illaamal Aval Kalagam Seigiraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irunthaalum Illaamal Aval Kalagam Seigiraal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaar Athu Yaar Athu Yaar Athu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar Athu Yaar Athu Yaar Athu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaar Yaar Athu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar Yaar Athu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sollaamal Nenjaththai Thollai Seivathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollaamal Nenjaththai Thollai Seivathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Moodaamal Kanraendai Moodi Chelvathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moodaamal Kanraendai Moodi Chelvathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaar Athu Yaar Athu Yaar Athu Yaar Athu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar Athu Yaar Athu Yaar Athu Yaar Athu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nerungaamal Nerungi Vanthadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nerungaamal Nerungi Vanthadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vilagaamal Vilagi Nirppathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilagaamal Vilagi Nirppathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vinaiyaaga Kelvi Thanthadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinaiyaaga Kelvi Thanthadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thelivaaga Kulamba Nenaipadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thelivaaga Kulamba Nenaipadhu"/>
</div>
</pre>
