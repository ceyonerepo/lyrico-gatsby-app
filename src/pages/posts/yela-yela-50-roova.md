---
title: "yela yela song lyrics"
album: "50 Roova"
artist: "VT. Bharathy - VT.Monish"
lyricist: "G. Panneerselvam"
director: "G. Panneerselvam"
path: "/albums/50-roova-lyrics"
song: "Yela Yela"
image: ../../images/albumart/50-roova.jpg
date: 2019-12-13
lang: tamil
youtubeLink: "https://www.youtube.com/embed/WzMGoNMYR0c"
type: "friendship"
singers:
  - Akhil
  - Mano Bala
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">oh ohohoh oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oh ohohoh oh"/>
</div>
<div class="lyrico-lyrics-wrapper">oh ohohoh oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oh ohohoh oh"/>
</div>
<div class="lyrico-lyrics-wrapper">oh ohohoh oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oh ohohoh oh"/>
</div>
<div class="lyrico-lyrics-wrapper">oh ohohoh oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oh ohohoh oh"/>
</div>
<div class="lyrico-lyrics-wrapper">oh ohohoh oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oh ohohoh oh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">yela yela thalla thalla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yela yela thalla thalla"/>
</div>
<div class="lyrico-lyrics-wrapper">machi oru quarter sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="machi oru quarter sollu"/>
</div>
<div class="lyrico-lyrics-wrapper">nanban oda paket la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanban oda paket la"/>
</div>
<div class="lyrico-lyrics-wrapper">epothume locker illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="epothume locker illa"/>
</div>
<div class="lyrico-lyrics-wrapper">life ah kondada 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="life ah kondada "/>
</div>
<div class="lyrico-lyrics-wrapper">venumda kootali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="venumda kootali"/>
</div>
<div class="lyrico-lyrics-wrapper">avanilla vaalkai than 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avanilla vaalkai than "/>
</div>
<div class="lyrico-lyrics-wrapper">epothume poli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="epothume poli"/>
</div>
<div class="lyrico-lyrics-wrapper">nanban okara idhayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanban okara idhayam"/>
</div>
<div class="lyrico-lyrics-wrapper">than narkali innum yen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="than narkali innum yen"/>
</div>
<div class="lyrico-lyrics-wrapper">nikira ukaru pangali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nikira ukaru pangali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">yela yela thalla thalla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yela yela thalla thalla"/>
</div>
<div class="lyrico-lyrics-wrapper">machi oru quarter sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="machi oru quarter sollu"/>
</div>
<div class="lyrico-lyrics-wrapper">nanban oda paket la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanban oda paket la"/>
</div>
<div class="lyrico-lyrics-wrapper">epothume locker illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="epothume locker illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aana oona party vaipom naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aana oona party vaipom naa"/>
</div>
<div class="lyrico-lyrics-wrapper">adutha naalu treat ah 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adutha naalu treat ah "/>
</div>
<div class="lyrico-lyrics-wrapper">ketpom na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ketpom na"/>
</div>
<div class="lyrico-lyrics-wrapper">mappu konjam eera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mappu konjam eera"/>
</div>
<div class="lyrico-lyrics-wrapper">oothi podu machi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oothi podu machi"/>
</div>
<div class="lyrico-lyrics-wrapper">natpu konjam kooda 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="natpu konjam kooda "/>
</div>
<div class="lyrico-lyrics-wrapper">vachu kodu echi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vachu kodu echi"/>
</div>
<div class="lyrico-lyrics-wrapper">nanban than nanban than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanban than nanban than"/>
</div>
<div class="lyrico-lyrics-wrapper">love ah sonna na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="love ah sonna na"/>
</div>
<div class="lyrico-lyrics-wrapper">obama pone than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="obama pone than"/>
</div>
<div class="lyrico-lyrics-wrapper">venun muna na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="venun muna na"/>
</div>
<div class="lyrico-lyrics-wrapper">light house room il 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="light house room il "/>
</div>
<div class="lyrico-lyrics-wrapper">than thookiduven na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="than thookiduven na"/>
</div>
<div class="lyrico-lyrics-wrapper">enna than aanalum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna than aanalum "/>
</div>
<div class="lyrico-lyrics-wrapper">pathiduven naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathiduven naa"/>
</div>
<div class="lyrico-lyrics-wrapper">enna enna than senjalum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna enna than senjalum "/>
</div>
<div class="lyrico-lyrics-wrapper">nenju thangidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenju thangidum"/>
</div>
<div class="lyrico-lyrics-wrapper">ye friend ku onnu na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ye friend ku onnu na"/>
</div>
<div class="lyrico-lyrics-wrapper">pokali moonchi penthudum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pokali moonchi penthudum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">yela yela thalla thalla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yela yela thalla thalla"/>
</div>
<div class="lyrico-lyrics-wrapper">machi oru quarter sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="machi oru quarter sollu"/>
</div>
<div class="lyrico-lyrics-wrapper">nanban oda paket la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanban oda paket la"/>
</div>
<div class="lyrico-lyrics-wrapper">epothume locker illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="epothume locker illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">uppu illa soru rusikathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uppu illa soru rusikathu"/>
</div>
<div class="lyrico-lyrics-wrapper">natpu illa vaalka inikathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="natpu illa vaalka inikathu"/>
</div>
<div class="lyrico-lyrics-wrapper">kutti suvar pothum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kutti suvar pothum "/>
</div>
<div class="lyrico-lyrics-wrapper">kunthikalam machi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kunthikalam machi"/>
</div>
<div class="lyrico-lyrics-wrapper">nee iruntha pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee iruntha pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">athu than rich uh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu than rich uh"/>
</div>
<div class="lyrico-lyrics-wrapper">sutho nu suthuna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sutho nu suthuna "/>
</div>
<div class="lyrico-lyrics-wrapper">love varum da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="love varum da"/>
</div>
<div class="lyrico-lyrics-wrapper">love than ok na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="love than ok na"/>
</div>
<div class="lyrico-lyrics-wrapper">sontham varum da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sontham varum da"/>
</div>
<div class="lyrico-lyrics-wrapper">engeyum epothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engeyum epothum"/>
</div>
<div class="lyrico-lyrics-wrapper">natpu varum da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="natpu varum da"/>
</div>
<div class="lyrico-lyrics-wrapper">thol mela kai pota
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thol mela kai pota"/>
</div>
<div class="lyrico-lyrics-wrapper">yaru tholan than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaru tholan than"/>
</div>
<div class="lyrico-lyrics-wrapper">enna enna than nee senjalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna enna than nee senjalum"/>
</div>
<div class="lyrico-lyrics-wrapper">nenju thangidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenju thangidum"/>
</div>
<div class="lyrico-lyrics-wrapper">en friend ku onnu than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en friend ku onnu than"/>
</div>
<div class="lyrico-lyrics-wrapper">pokali moonchi penthidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pokali moonchi penthidum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">yela yela thalla thalla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yela yela thalla thalla"/>
</div>
<div class="lyrico-lyrics-wrapper">machi oru quarter sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="machi oru quarter sollu"/>
</div>
<div class="lyrico-lyrics-wrapper">nanban oda paket la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanban oda paket la"/>
</div>
<div class="lyrico-lyrics-wrapper">epothume locker illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="epothume locker illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">life ah kondada 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="life ah kondada "/>
</div>
<div class="lyrico-lyrics-wrapper">venumda kootali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="venumda kootali"/>
</div>
<div class="lyrico-lyrics-wrapper">avanilla vaalkai than 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avanilla vaalkai than "/>
</div>
<div class="lyrico-lyrics-wrapper">epothume poli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="epothume poli"/>
</div>
<div class="lyrico-lyrics-wrapper">nanban okara idhayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanban okara idhayam"/>
</div>
<div class="lyrico-lyrics-wrapper">than narkali innum yen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="than narkali innum yen"/>
</div>
<div class="lyrico-lyrics-wrapper">nikira ukaru pangali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nikira ukaru pangali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">yela yela thalla thalla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yela yela thalla thalla"/>
</div>
<div class="lyrico-lyrics-wrapper">machi oru quarter sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="machi oru quarter sollu"/>
</div>
<div class="lyrico-lyrics-wrapper">nanban oda paket la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanban oda paket la"/>
</div>
<div class="lyrico-lyrics-wrapper">epothume locker illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="epothume locker illa"/>
</div>
</pre>
