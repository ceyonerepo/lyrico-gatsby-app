---
title: "azhagu azhagu song lyrics"
album: "Sangathamizhan"
artist: "	Vivek - Mervin"
lyricist: "Madhan Karky"
director: "Vijay Chandar"
path: "/albums/sangathamizhan-lyrics"
song: "Azhagu Azhagu"
image: ../../images/albumart/sangathamizhan.jpg
date: 2019-11-15
lang: tamil
youtubeLink: "https://www.youtube.com/embed/QkZyATf9qVs"
type: "love"
singers:
  - Swetha Mohan
  - Mervin Solomon
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kanukkula Yei Pattampoochi Sirikuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanukkula Yei Pattampoochi Sirikuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjukulla Athu Kutti Rekka Virikuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjukulla Athu Kutti Rekka Virikuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanukkula Yei Pattampoochi Sirikuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanukkula Yei Pattampoochi Sirikuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjukulla Athu Kutti Rekka Virikuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjukulla Athu Kutti Rekka Virikuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaanum Yaavilum Inbam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanum Yaavilum Inbam"/>
</div>
<div class="lyrico-lyrics-wrapper">Karainthae Poguthu Nenjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karainthae Poguthu Nenjam"/>
</div>
<div class="lyrico-lyrics-wrapper">En Ullankaiyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Ullankaiyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">En Punnagaikalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Punnagaikalaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ketkkum Yaavilum Thaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketkkum Yaavilum Thaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Asaiyum Yaavilum Nadanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asaiyum Yaavilum Nadanam"/>
</div>
<div class="lyrico-lyrics-wrapper">En Swaasa Saalaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Swaasa Saalaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Vaasa Thiruvizha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Vaasa Thiruvizha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhodu Kadhai Pesum Kaatru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhodu Kadhai Pesum Kaatru"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalodu Uraiyadum Dhoosu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalodu Uraiyadum Dhoosu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yarin Pechai Naan Ketka Pogiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yarin Pechai Naan Ketka Pogiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Nil Endru Azhaikkindra Pookal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nil Endru Azhaikkindra Pookal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa Endru Izhukkindra Thozhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Endru Izhukkindra Thozhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yarin Pechaithaan Naan Ketpadhoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yarin Pechaithaan Naan Ketpadhoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thithina Thathina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thithina Thathina"/>
</div>
<div class="lyrico-lyrics-wrapper">Thithina Thathina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thithina Thathina"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagu Azhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagu Azhagu"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagu Azhagu Azhaikkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagu Azhagu Azhaikkuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thithina Thathina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thithina Thathina"/>
</div>
<div class="lyrico-lyrics-wrapper">Thithina Thathina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thithina Thathina"/>
</div>
<div class="lyrico-lyrics-wrapper">Siragu Siragu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siragu Siragu"/>
</div>
<div class="lyrico-lyrics-wrapper">Puthiya Siragu Muzhaikkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthiya Siragu Muzhaikkuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thithina Thathina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thithina Thathina"/>
</div>
<div class="lyrico-lyrics-wrapper">Thithina Thathina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thithina Thathina"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagu Azhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagu Azhagu"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagu Azhagu Azhaikkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagu Azhagu Azhaikkuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thithina Thathina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thithina Thathina"/>
</div>
<div class="lyrico-lyrics-wrapper">Thithina Thathina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thithina Thathina"/>
</div>
<div class="lyrico-lyrics-wrapper">Siragu Siragu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siragu Siragu"/>
</div>
<div class="lyrico-lyrics-wrapper">Puthiya Siragu Muzhaikkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthiya Siragu Muzhaikkuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaanum Yaavilum Inbam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanum Yaavilum Inbam"/>
</div>
<div class="lyrico-lyrics-wrapper">Karainthae Poguthu Nenjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karainthae Poguthu Nenjam"/>
</div>
<div class="lyrico-lyrics-wrapper">En Ullankaiyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Ullankaiyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">En Punnagaikalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Punnagaikalaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ketkkum Yaavilum Thaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketkkum Yaavilum Thaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Asaiyum Yaavilum Nadanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asaiyum Yaavilum Nadanam"/>
</div>
<div class="lyrico-lyrics-wrapper">En Swaasa Saalaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Swaasa Saalaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Vaasa Thiruvizha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Vaasa Thiruvizha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhodu Kadhai Pesum Kaatru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhodu Kadhai Pesum Kaatru"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalodu Uraiyadum Dhoosu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalodu Uraiyadum Dhoosu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yarin Pechai Naan Ketka Pogiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yarin Pechai Naan Ketka Pogiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Nil Endru Azhaikkindra Pookal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nil Endru Azhaikkindra Pookal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa Endru Izhukkindra Thozhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Endru Izhukkindra Thozhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yarin Pechaithaan Naan Ketpadhoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yarin Pechaithaan Naan Ketpadhoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaarpathanikkullae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaarpathanikkullae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Poovai Polae Vazhndhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Poovai Polae Vazhndhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Micham Meedhi Vaazha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Micham Meedhi Vaazha"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Veedhi Vanthenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Veedhi Vanthenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaththi Petikkullae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaththi Petikkullae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Vaanam Ingu Kanden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Vaanam Ingu Kanden"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhosai Kallin Melae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhosai Kallin Melae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Paasam Kandenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Paasam Kandenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanukkula Yei Pattampoochi Sirikuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanukkula Yei Pattampoochi Sirikuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjukulla Athu Kutti Rekka Virikuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjukulla Athu Kutti Rekka Virikuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thattavilla En Ulagamae Thorakkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thattavilla En Ulagamae Thorakkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pichikittu Parakkuthadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pichikittu Parakkuthadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanukkula Yei Pattampoochi Sirikuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanukkula Yei Pattampoochi Sirikuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjukulla Athu Kutti Rekka Virikuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjukulla Athu Kutti Rekka Virikuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thattavilla En Ulagamae Thorakkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thattavilla En Ulagamae Thorakkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pichikittu Parakkuthadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pichikittu Parakkuthadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veredhum En Nenjikku Vendaamada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veredhum En Nenjikku Vendaamada"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai Korathae Ichittarandam Kaanbomadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai Korathae Ichittarandam Kaanbomadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaalmeenaai Naan Vaan Vittu Veezhnthenadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaalmeenaai Naan Vaan Vittu Veezhnthenadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Orr Naalil Naan En Aayul Vazhnthenae Unnaladaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orr Naalil Naan En Aayul Vazhnthenae Unnaladaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thithina Thathina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thithina Thathina"/>
</div>
<div class="lyrico-lyrics-wrapper">Thithina Thathina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thithina Thathina"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagu Azhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagu Azhagu"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagu Azhagu Azhaikkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagu Azhagu Azhaikkuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thithina Thathina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thithina Thathina"/>
</div>
<div class="lyrico-lyrics-wrapper">Thithina Thathina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thithina Thathina"/>
</div>
<div class="lyrico-lyrics-wrapper">Siragu Siragu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siragu Siragu"/>
</div>
<div class="lyrico-lyrics-wrapper">Puthiya Siragu Muzhaikkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthiya Siragu Muzhaikkuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thithina Thathina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thithina Thathina"/>
</div>
<div class="lyrico-lyrics-wrapper">Thithina Thathina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thithina Thathina"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagu Azhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagu Azhagu"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagu Azhagu Azhaikkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagu Azhagu Azhaikkuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thithina Thathina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thithina Thathina"/>
</div>
<div class="lyrico-lyrics-wrapper">Thithina Thathina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thithina Thathina"/>
</div>
<div class="lyrico-lyrics-wrapper">Siragu Siragu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siragu Siragu"/>
</div>
<div class="lyrico-lyrics-wrapper">Puthiya Siragu Muzhaikkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthiya Siragu Muzhaikkuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thithina Thathina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thithina Thathina"/>
</div>
<div class="lyrico-lyrics-wrapper">Thithina Thathina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thithina Thathina"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagu Azhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagu Azhagu"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagu Azhagu Azhaikkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagu Azhagu Azhaikkuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thithina Thathina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thithina Thathina"/>
</div>
<div class="lyrico-lyrics-wrapper">Thithina Thathina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thithina Thathina"/>
</div>
<div class="lyrico-lyrics-wrapper">Siragu Siragu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siragu Siragu"/>
</div>
<div class="lyrico-lyrics-wrapper">Puthiya Siragu Muzhaikkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthiya Siragu Muzhaikkuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thithina Thathina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thithina Thathina"/>
</div>
<div class="lyrico-lyrics-wrapper">Thithina Thathina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thithina Thathina"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagu Azhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagu Azhagu"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagu Azhagu Azhaikkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagu Azhagu Azhaikkuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thithina Thathina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thithina Thathina"/>
</div>
<div class="lyrico-lyrics-wrapper">Thithina Thathina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thithina Thathina"/>
</div>
<div class="lyrico-lyrics-wrapper">Siragu Siragu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siragu Siragu"/>
</div>
<div class="lyrico-lyrics-wrapper">Puthiya Siragu Muzhaikkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthiya Siragu Muzhaikkuthu"/>
</div>
</pre>
