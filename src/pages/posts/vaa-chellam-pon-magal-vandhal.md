---
title: 'vaa chellam song lyrics'
album: 'Pon Magal Vandhal'
artist: 'Govind Vasantha'
lyricist: 'Vivek'
director: 'J J Fredrick'
path: '/albums/pon-magal-vandhal-song-lyrics'
song: 'Vaa Chellam'
image: ../../images/albumart/ponmagal-vandhal.jpg
date: 2020-05-29
lang: tamil
youtubeLink: "https://www.youtube.com/embed/obNVMWJvxSA"
type: 'parenting'
singers: 
- Brinda Sivakumar
---

<pre class="lyrics-native">
    Coming Soon
</pre>

<pre class="lyrics-english">

  <div class="lyrico-lyrics-wrapper">Vaa chellam en vaazhka
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaa chellam en vaazhka"/>
</div>
<div class="lyrico-lyrics-wrapper">Parisaa thandha poovae
<input type="checkbox" class="lyrico-select-lyric-line" value="Parisaa thandha poovae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaayaaga enna petha
<input type="checkbox" class="lyrico-select-lyric-line" value="Thaayaaga enna petha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaayae vaadi
<input type="checkbox" class="lyrico-select-lyric-line" value="Thaayae vaadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Nee kaettaa en usura
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee kaettaa en usura"/>
</div>
<div class="lyrico-lyrics-wrapper">Paalaa thaaren poovae
<input type="checkbox" class="lyrico-select-lyric-line" value="Paalaa thaaren poovae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee neendhathaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee neendhathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaigal konden vaadi
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaigal konden vaadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kodi kangalum podhaadhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Kodi kangalum podhaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagaana un mogam paakka
<input type="checkbox" class="lyrico-select-lyric-line" value="Azhagaana un mogam paakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Varam vaangi vaikkavae
<input type="checkbox" class="lyrico-select-lyric-line" value="Varam vaangi vaikkavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai podhaadhae…ae….ae….
<input type="checkbox" class="lyrico-select-lyric-line" value="Kai podhaadhae…ae….ae…."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Nooru vaanavil podhaadhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Nooru vaanavil podhaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Una pola oviyam theetta
<input type="checkbox" class="lyrico-select-lyric-line" value="Una pola oviyam theetta"/>
</div>
<div class="lyrico-lyrics-wrapper">Podhum pillai vaasam mattum
<input type="checkbox" class="lyrico-select-lyric-line" value="Podhum pillai vaasam mattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Podhum vaadi
<input type="checkbox" class="lyrico-select-lyric-line" value="Podhum vaadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Nadavandi sirippakkaatti
<input type="checkbox" class="lyrico-select-lyric-line" value="Nadavandi sirippakkaatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee dhaan enna nadakka vacha
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee dhaan enna nadakka vacha"/>
</div>
<div class="lyrico-lyrics-wrapper">Panikatti kannakkatti
<input type="checkbox" class="lyrico-select-lyric-line" value="Panikatti kannakkatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee dhaan sogam marakka vacha
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee dhaan sogam marakka vacha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Sinungal pol edho senju
<input type="checkbox" class="lyrico-select-lyric-line" value="Sinungal pol edho senju"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee dhaan enna karaiya vacha
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee dhaan enna karaiya vacha"/>
</div>
<div class="lyrico-lyrics-wrapper">Poga pora vazhiyil ellaam
<input type="checkbox" class="lyrico-select-lyric-line" value="Poga pora vazhiyil ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee dhaan inbam pozhiya vacha
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee dhaan inbam pozhiya vacha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kaalam varai nee dhaanae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaalam varai nee dhaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaaval vara naan dhaanae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaaval vara naan dhaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa thoranamae…
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaa thoranamae…"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagae …  Kaaranamae….
<input type="checkbox" class="lyrico-select-lyric-line" value="Azhagae …  Kaaranamae…."/>
</div>
</pre>