---
title: "kanavilum kavithai song lyrics"
album: "Irudhi Pakkam"
artist: "MS Jones Rupert"
lyricist: "Mano Ve Kannathasan"
director: "Mano Ve Kannathasan"
path: "/albums/irudhi-pakkam-song-lyrics"
song: "Kanavilum Kavithai"
image: ../../images/albumart/irudhi-pakkam.jpg
date: 2021-12-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/GEfFPd5KGXo"
type: "love"
singers:
  - Priyanka NK
  - Syed Subahan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">ithu pothume ithu pothume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu pothume ithu pothume"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu pothume ithu pothume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu pothume ithu pothume"/>
</div>
<div class="lyrico-lyrics-wrapper">kanavilum kavithai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavilum kavithai"/>
</div>
<div class="lyrico-lyrics-wrapper">eluthugiren kanne kanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eluthugiren kanne kanne"/>
</div>
<div class="lyrico-lyrics-wrapper">unai kanda naal mudhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unai kanda naal mudhal"/>
</div>
<div class="lyrico-lyrics-wrapper">ninaivilum nitham nilai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninaivilum nitham nilai"/>
</div>
<div class="lyrico-lyrics-wrapper">kulaithene nenje
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulaithene nenje"/>
</div>
<div class="lyrico-lyrics-wrapper">unai ninaikum pothu ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unai ninaikum pothu ellam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kanavilum kavithai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavilum kavithai"/>
</div>
<div class="lyrico-lyrics-wrapper">eluthugiren kanne kanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eluthugiren kanne kanne"/>
</div>
<div class="lyrico-lyrics-wrapper">unai kanda naal mudhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unai kanda naal mudhal"/>
</div>
<div class="lyrico-lyrics-wrapper">ninaivilum nitham nilai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninaivilum nitham nilai"/>
</div>
<div class="lyrico-lyrics-wrapper">kulaithene nenje
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulaithene nenje"/>
</div>
<div class="lyrico-lyrics-wrapper">unai ninaikum pothu ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unai ninaikum pothu ellam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">manjal veyil mangi vila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manjal veyil mangi vila"/>
</div>
<div class="lyrico-lyrics-wrapper">mayakamo manathil vila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mayakamo manathil vila"/>
</div>
<div class="lyrico-lyrics-wrapper">minnal idai thevathaiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="minnal idai thevathaiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">minmini pol siragadithai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="minmini pol siragadithai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kanavilum kavithai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavilum kavithai"/>
</div>
<div class="lyrico-lyrics-wrapper">eluthugiren kanne kanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eluthugiren kanne kanne"/>
</div>
<div class="lyrico-lyrics-wrapper">unai kanda naal mudhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unai kanda naal mudhal"/>
</div>
<div class="lyrico-lyrics-wrapper">ninaivilum nitham nilai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninaivilum nitham nilai"/>
</div>
<div class="lyrico-lyrics-wrapper">kulaithene nenje nenje nenje 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulaithene nenje nenje nenje "/>
</div>
<div class="lyrico-lyrics-wrapper">unai ninaikum pothu ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unai ninaikum pothu ellam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">paritha sendra manathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paritha sendra manathai"/>
</div>
<div class="lyrico-lyrics-wrapper">pathiramai vaithu kol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathiramai vaithu kol"/>
</div>
<div class="lyrico-lyrics-wrapper">pathilaga un manathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathilaga un manathai"/>
</div>
<div class="lyrico-lyrics-wrapper">vaithu vidu en udalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaithu vidu en udalil"/>
</div>
<div class="lyrico-lyrics-wrapper">pramanum unai padaikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pramanum unai padaikum"/>
</div>
<div class="lyrico-lyrics-wrapper">kanavu than kandirupan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavu than kandirupan"/>
</div>
<div class="lyrico-lyrics-wrapper">kanavil kanda athisayathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavil kanda athisayathai"/>
</div>
<div class="lyrico-lyrics-wrapper">unnail puguthu padaithu vittan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnail puguthu padaithu vittan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">manjal veyil mangi vila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manjal veyil mangi vila"/>
</div>
<div class="lyrico-lyrics-wrapper">mayakamo manathil vila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mayakamo manathil vila"/>
</div>
<div class="lyrico-lyrics-wrapper">minnal idai thevathaiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="minnal idai thevathaiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">minmini pol siragadithai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="minmini pol siragadithai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kanavilum kavithai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavilum kavithai"/>
</div>
<div class="lyrico-lyrics-wrapper">eluthugiren kanne kanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eluthugiren kanne kanne"/>
</div>
<div class="lyrico-lyrics-wrapper">unai kanda naal mudhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unai kanda naal mudhal"/>
</div>
<div class="lyrico-lyrics-wrapper">ninaivilum nilai kulaithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninaivilum nilai kulaithu"/>
</div>
<div class="lyrico-lyrics-wrapper">pogiren nenje nenje nenje 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pogiren nenje nenje nenje "/>
</div>
<div class="lyrico-lyrics-wrapper">unai ninaikum pothu ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unai ninaikum pothu ellam"/>
</div>
</pre>
