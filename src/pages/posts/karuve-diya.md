---
title: "karuve song lyrics"
album: "Diya"
artist: "Sam CS"
lyricist: "Madhan Karky"
director: "A.L. Vijay"
path: "/albums/diya-song-lyrics"
song: "Karuve"
image: ../../images/albumart/diya.jpg
date: 2018-04-27
lang: tamil
youtubeLink: "https://www.youtube.com/embed/v4QsNAeA2GU"
type: "sad"
singers:
  - K.S. Chithra
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yaaro Yaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaro Yaaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai Vidhaithathu Yaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai Vidhaithathu Yaaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaro Yaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaro Yaaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai Sidhaithathu Yaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai Sidhaithathu Yaaro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaaro Yaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaro Yaaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai Ezhuthiyathu Yaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai Ezhuthiyathu Yaaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaro Yaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaro Yaaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai Azhithathu Yaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai Azhithathu Yaaro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theeye Theeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeye Theeye"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai Anaithathu Yaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai Anaithathu Yaaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Poove Poove
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poove Poove"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai Nasukiya Thaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai Nasukiya Thaaro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thondrum Unnai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thondrum Unnai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kondrathaaradi Karuve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondrathaaradi Karuve"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neel Thuyarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neel Thuyarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Piravi Thuyarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Piravi Thuyarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Enave Urave Kalainthayadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enave Urave Kalainthayadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paazh Ulagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paazh Ulagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Suzhalum Naragam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suzhalum Naragam"/>
</div>
<div class="lyrico-lyrics-wrapper">Enave Azhage Karainthaayadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enave Azhage Karainthaayadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaayin Theeyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaayin Theeyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeinthu Theeinthidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeinthu Theeinthidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Karuve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuve"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Urangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Urangu"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyire Urangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyire Urangu"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhuve Kadaisi Thaalaattena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhuve Kadaisi Thaalaattena"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaai Vizhiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaai Vizhiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhiyum Thuliyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhiyum Thuliyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Karaivaai Kadaisi Neeraattena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karaivaai Kadaisi Neeraattena"/>
</div>
<div class="lyrico-lyrics-wrapper">Pulli Unnil Kolli Vaikkirom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pulli Unnil Kolli Vaikkirom"/>
</div>
<div class="lyrico-lyrics-wrapper">Karuve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuve"/>
</div>
</pre>
