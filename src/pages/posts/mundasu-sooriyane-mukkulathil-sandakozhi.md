---
title: "mundasu sooriyane mukkulathil song lyrics"
album: "Sandakozhi"
artist: "Yuvan Sankar Raja"
lyricist: "Pa. Vijay"
director: "N. Linguswamy"
path: "/albums/sandakozhi-lyrics"
song: "Mundasu Sooriyane Mukkulathil"
image: ../../images/albumart/sandakozhi.jpg
date: 2005-12-16
lang: tamil
youtubeLink: "https://www.youtube.com/embed/mxW2U8IHSZw"
type: "Mass"
singers:
  - Karthik
  - Sriram
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Mundaasu Sooriyane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mundaasu Sooriyane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mukkulaththil Moothavane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mukkulaththil Moothavane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theyaatha Chandirane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theyaatha Chandirane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Therapola Ninnavane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Therapola Ninnavane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nooru Thalamuraiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nooru Thalamuraiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ooraalum Kulame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooraalum Kulame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veera Paramparaikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veera Paramparaikku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Viththaana Iname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viththaana Iname"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veecharuva Saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veecharuva Saami"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ithu Velukambu Bhoomi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu Velukambu Bhoomi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kathti Theeta Vantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathti Theeta Vantha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Nenjeduthu Kaami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Nenjeduthu Kaami"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mundaasu Sooriyane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mundaasu Sooriyane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mukkulaththil Moothavane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mukkulaththil Moothavane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thayaatha Chandirane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thayaatha Chandirane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Therpola Ninnavane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Therpola Ninnavane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maanamulla Veeramulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maanamulla Veeramulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vamsam Varuthaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vamsam Varuthaiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Maraiyaatha Sooriyanin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Maraiyaatha Sooriyanin"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Amsam Varuthaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amsam Varuthaiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vettaruvaa Veralirukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vettaruvaa Veralirukkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Singam Varuthaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Singam Varuthaiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enga Thanmaanam Kaathu Nikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Thanmaanam Kaathu Nikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thangam Varuthaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangam Varuthaiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ettu Patti Sanththukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettu Patti Sanththukkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saamipolada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saamipolada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aiya Nizhalukooda Saanjathulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiya Nizhalukooda Saanjathulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bhoomi Melada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhoomi Melada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Thaala Kuninju Yaarum Paarthathilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Thaala Kuninju Yaarum Paarthathilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unga Thala Nimirndhu Naanga Paarthathilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unga Thala Nimirndhu Naanga Paarthathilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veecharuva Saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veecharuva Saami"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ithu Velukambu Bhoomi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu Velukambu Bhoomi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaththi Theeta Vantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaththi Theeta Vantha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Nenjeduthu Kaami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Nenjeduthu Kaami"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ali Alli Koduthathile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ali Alli Koduthathile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sevanththa Kaiyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sevanththa Kaiyada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ithu Aruvaala Thooki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu Aruvaala Thooki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninna Aiyanaaruda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninna Aiyanaaruda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kamba Koozhu Kudikkum oru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kamba Koozhu Kudikkum oru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadavul Yethuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadavul Yethuda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Atha Enga Ooru Ellakulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atha Enga Ooru Ellakulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vanthu Paarada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthu Paarada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Namma Vamsathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma Vamsathula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oruthan Kooda Kozha illada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oruthan Kooda Kozha illada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aiyaa Vaazhum Mannula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyaa Vaazhum Mannula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaarum Ingae Ezha Illada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarum Ingae Ezha Illada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ithu Paramparaiya Alli Thandha vaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu Paramparaiya Alli Thandha vaanam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ezhu Thalamuraiya Vaazhndhu Varum Maanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhu Thalamuraiya Vaazhndhu Varum Maanam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veecharuva Saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veecharuva Saami"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ithu Velukambu Bhoomi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu Velukambu Bhoomi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaththi Theeta Vantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaththi Theeta Vantha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Nenjeduthu Kaami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Nenjeduthu Kaami"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mundaasu Sooriyane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mundaasu Sooriyane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mukkulaththil Moothavane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mukkulaththil Moothavane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theyaatha Chandirane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theyaatha Chandirane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Therapola Ninnavane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Therapola Ninnavane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nooru Thalamuraiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nooru Thalamuraiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ooraalum Kulame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooraalum Kulame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veera Paramparaikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veera Paramparaikku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Viththaana Iname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viththaana Iname"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veecharuva Saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veecharuva Saami"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ithu Velukambu Bhoomi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu Velukambu Bhoomi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kathti Theeta Vantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathti Theeta Vantha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Nenjeduthu Kaami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Nenjeduthu Kaami"/>
</div>
</pre>
