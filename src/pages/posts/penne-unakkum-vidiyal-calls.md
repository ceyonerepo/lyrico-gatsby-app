---
title: "penne unakkum vidiyal song lyrics"
album: "Calls"
artist: "Thameem Ansari"
lyricist: "K.Bhuvana"
director: "J. Sabarish"
path: "/albums/calls-lyrics"
song: "Penne Unakkum Vidiyal"
image: ../../images/albumart/calls.jpg
date: 2021-02-26
lang: tamil
youtubeLink: "https://www.youtube.com/embed/_8KmjNCZks4"
type: "Motivational"
singers:
  - Ajaey shravan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">penne unakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="penne unakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">vidiyal undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidiyal undu"/>
</div>
<div class="lyrico-lyrics-wrapper">etharkum anjaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="etharkum anjaa"/>
</div>
<div class="lyrico-lyrics-wrapper">manamum undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manamum undu"/>
</div>
<div class="lyrico-lyrics-wrapper">megam poliyum tharunam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="megam poliyum tharunam"/>
</div>
<div class="lyrico-lyrics-wrapper">un kannil eeram illai iniye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kannil eeram illai iniye"/>
</div>
<div class="lyrico-lyrics-wrapper">vaanai thodum undhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaanai thodum undhan"/>
</div>
<div class="lyrico-lyrics-wrapper">vetri arugil ulagai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetri arugil ulagai "/>
</div>
<div class="lyrico-lyrics-wrapper">rasika varume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rasika varume"/>
</div>
<div class="lyrico-lyrics-wrapper">un vasantha kaalam ithuve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un vasantha kaalam ithuve"/>
</div>
<div class="lyrico-lyrics-wrapper">ini valkai unthan vasame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ini valkai unthan vasame"/>
</div>
<div class="lyrico-lyrics-wrapper">un sogam theerthu vidume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un sogam theerthu vidume"/>
</div>
<div class="lyrico-lyrics-wrapper">un nagai thulir vidume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un nagai thulir vidume"/>
</div>
<div class="lyrico-lyrics-wrapper">vidai theriya vaalkaiyil than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidai theriya vaalkaiyil than"/>
</div>
<div class="lyrico-lyrics-wrapper">kalai nayamum irukirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalai nayamum irukirathe"/>
</div>
<div class="lyrico-lyrics-wrapper">vali illa vaalkaiyil than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vali illa vaalkaiyil than"/>
</div>
<div class="lyrico-lyrics-wrapper">valiyum inge irukiratha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valiyum inge irukiratha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oodidum vaalkai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oodidum vaalkai "/>
</div>
<div class="lyrico-lyrics-wrapper">penne ithuve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="penne ithuve"/>
</div>
<div class="lyrico-lyrics-wrapper">kalathil vetrigal unathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalathil vetrigal unathe"/>
</div>
<div class="lyrico-lyrics-wrapper">vaalai veesiye 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalai veesiye "/>
</div>
<div class="lyrico-lyrics-wrapper">veera mangaiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veera mangaiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">oodu nee oodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oodu nee oodu"/>
</div>
<div class="lyrico-lyrics-wrapper">niranthamilla vaalkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="niranthamilla vaalkai"/>
</div>
<div class="lyrico-lyrics-wrapper">inge nithigaka porattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inge nithigaka porattam"/>
</div>
<div class="lyrico-lyrics-wrapper">mugam theriya moodargal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mugam theriya moodargal"/>
</div>
<div class="lyrico-lyrics-wrapper">mudakuvatha penne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudakuvatha penne"/>
</div>
<div class="lyrico-lyrics-wrapper">nilavin alagum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nilavin alagum "/>
</div>
<div class="lyrico-lyrics-wrapper">vaalum varai than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalum varai than"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu puriyatha unnakum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu puriyatha unnakum"/>
</div>
<div class="lyrico-lyrics-wrapper">penne neeyum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="penne neeyum "/>
</div>
<div class="lyrico-lyrics-wrapper">thuninthaal vaalkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thuninthaal vaalkai"/>
</div>
<div class="lyrico-lyrics-wrapper">sathanai endre vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sathanai endre vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu mudivumalla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu mudivumalla"/>
</div>
<div class="lyrico-lyrics-wrapper">thodarnthidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thodarnthidum"/>
</div>
<div class="lyrico-lyrics-wrapper">thodangidu vetri paathayai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thodangidu vetri paathayai"/>
</div>
<div class="lyrico-lyrics-wrapper">sadhanai nilaithidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sadhanai nilaithidum"/>
</div>
<div class="lyrico-lyrics-wrapper">indha boomiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indha boomiyile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vidai theriya vaalkaiyil than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidai theriya vaalkaiyil than"/>
</div>
<div class="lyrico-lyrics-wrapper">kalai nayamum irukirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalai nayamum irukirathe"/>
</div>
<div class="lyrico-lyrics-wrapper">vali illa vaalkaiyil than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vali illa vaalkaiyil than"/>
</div>
<div class="lyrico-lyrics-wrapper">valiyum inge irukiratha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valiyum inge irukiratha"/>
</div>
</pre>
