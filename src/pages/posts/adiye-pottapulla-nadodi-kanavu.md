---
title: "adiye pottapulla song lyrics"
album: "Nadodi Kanavu"
artist: "Sabesh Murali"
lyricist: "Sirgazhi Sirpi"
director: "Veera Selva"
path: "/albums/nadodi-kanavu-lyrics"
song: "Adiye Pottapulla"
image: ../../images/albumart/nadodi-kanavu.jpg
date: 2018-08-03
lang: tamil
youtubeLink: "https://www.youtube.com/embed/hYoEWP6hIN0"
type: "love"
singers:
  - Harihara Sudhan
  - Namitha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">adiye adiye adiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adiye adiye adiye"/>
</div>
<div class="lyrico-lyrics-wrapper">adiye potta pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adiye potta pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">kotta mutha vedikatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kotta mutha vedikatha"/>
</div>
<div class="lyrico-lyrics-wrapper">soora kaathaaga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="soora kaathaaga "/>
</div>
<div class="lyrico-lyrics-wrapper">suthi suthi thaakaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suthi suthi thaakaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">adiye potta pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adiye potta pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">kotta mutha vedikatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kotta mutha vedikatha"/>
</div>
<div class="lyrico-lyrics-wrapper">soora kaathaaga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="soora kaathaaga "/>
</div>
<div class="lyrico-lyrics-wrapper">suthi suthi thaakaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suthi suthi thaakaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">manja kelangaaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manja kelangaaka"/>
</div>
<div class="lyrico-lyrics-wrapper">unna kuduthiye urasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna kuduthiye urasi"/>
</div>
<div class="lyrico-lyrics-wrapper">kulucha atha olinju pathiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulucha atha olinju pathiye"/>
</div>
<div class="lyrico-lyrics-wrapper">oora kannala sulati adikatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oora kannala sulati adikatha"/>
</div>
<div class="lyrico-lyrics-wrapper">otha sollala ena nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="otha sollala ena nee"/>
</div>
<div class="lyrico-lyrics-wrapper">kooru podatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kooru podatha"/>
</div>
<div class="lyrico-lyrics-wrapper">oora kannala sulati adikatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oora kannala sulati adikatha"/>
</div>
<div class="lyrico-lyrics-wrapper">otha sollala ena nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="otha sollala ena nee"/>
</div>
<div class="lyrico-lyrics-wrapper">kooru podatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kooru podatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">adiye adiye 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adiye adiye "/>
</div>
<div class="lyrico-lyrics-wrapper">adiye potta pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adiye potta pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">kotta mutha vedikatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kotta mutha vedikatha"/>
</div>
<div class="lyrico-lyrics-wrapper">soora kaathaaga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="soora kaathaaga "/>
</div>
<div class="lyrico-lyrics-wrapper">suthi suthi thaakaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suthi suthi thaakaatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">alai adikira pechala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alai adikira pechala"/>
</div>
<div class="lyrico-lyrics-wrapper">mada mada nu saikatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mada mada nu saikatha"/>
</div>
<div class="lyrico-lyrics-wrapper">kilu kilupa pola enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kilu kilupa pola enna"/>
</div>
<div class="lyrico-lyrics-wrapper">aada vaikatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aada vaikatha"/>
</div>
<div class="lyrico-lyrics-wrapper">sulukku edukura kaiyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sulukku edukura kaiyala"/>
</div>
<div class="lyrico-lyrics-wrapper">sruthi ethura thannala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sruthi ethura thannala"/>
</div>
<div class="lyrico-lyrics-wrapper">sekku madu pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sekku madu pola"/>
</div>
<div class="lyrico-lyrics-wrapper">enna sutha vaikatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna sutha vaikatha"/>
</div>
<div class="lyrico-lyrics-wrapper">vaala en seevu en
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaala en seevu en"/>
</div>
<div class="lyrico-lyrics-wrapper">panankallai erakuraye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="panankallai erakuraye"/>
</div>
<div class="lyrico-lyrics-wrapper">bothaya yethuraye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bothaya yethuraye"/>
</div>
<div class="lyrico-lyrics-wrapper">aasaiya thoonduraye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aasaiya thoonduraye"/>
</div>
<div class="lyrico-lyrics-wrapper">pon vande odathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pon vande odathe"/>
</div>
<div class="lyrico-lyrics-wrapper">vaasatha moodathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaasatha moodathe"/>
</div>
<div class="lyrico-lyrics-wrapper">pon vande odathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pon vande odathe"/>
</div>
<div class="lyrico-lyrics-wrapper">vaasatha moodathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaasatha moodathe"/>
</div>
<div class="lyrico-lyrics-wrapper">pata kata thaneeyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pata kata thaneeyile"/>
</div>
<div class="lyrico-lyrics-wrapper">kenda meena ilukuraye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kenda meena ilukuraye"/>
</div>
<div class="lyrico-lyrics-wrapper">vitha nella pola enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vitha nella pola enna"/>
</div>
<div class="lyrico-lyrics-wrapper">oora vaikiraye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oora vaikiraye"/>
</div>
<div class="lyrico-lyrics-wrapper">minminigal veluchathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="minminigal veluchathula"/>
</div>
<div class="lyrico-lyrics-wrapper">sadugudugal aada vanthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sadugudugal aada vanthe"/>
</div>
<div class="lyrico-lyrics-wrapper">kanna katti enna neyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanna katti enna neyum"/>
</div>
<div class="lyrico-lyrics-wrapper">theda vaikuraye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theda vaikuraye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">adiye adiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adiye adiye"/>
</div>
<div class="lyrico-lyrics-wrapper">adiye potta pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adiye potta pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">kotta mutha vedikatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kotta mutha vedikatha"/>
</div>
<div class="lyrico-lyrics-wrapper">soora kaathaaga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="soora kaathaaga "/>
</div>
<div class="lyrico-lyrics-wrapper">suthi suthi thaakaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suthi suthi thaakaatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kuru kurunu pakatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuru kurunu pakatha"/>
</div>
<div class="lyrico-lyrics-wrapper">kuruku saalu otathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuruku saalu otathe"/>
</div>
<div class="lyrico-lyrics-wrapper">nungu thinga aasa pattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nungu thinga aasa pattu"/>
</div>
<div class="lyrico-lyrics-wrapper">nonthu pogatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nonthu pogatha"/>
</div>
<div class="lyrico-lyrics-wrapper">vettu kiliya thullatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vettu kiliya thullatha"/>
</div>
<div class="lyrico-lyrics-wrapper">vetti kathaiyum pesatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetti kathaiyum pesatha"/>
</div>
<div class="lyrico-lyrics-wrapper">atta kathiyai vachukitu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="atta kathiyai vachukitu"/>
</div>
<div class="lyrico-lyrics-wrapper">enna vettatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna vettatha"/>
</div>
<div class="lyrico-lyrics-wrapper">naal thorum urugurene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naal thorum urugurene"/>
</div>
<div class="lyrico-lyrics-wrapper">nee patha marugurene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee patha marugurene"/>
</div>
<div class="lyrico-lyrics-wrapper">un jaada purunchuduchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un jaada purunchuduchu"/>
</div>
<div class="lyrico-lyrics-wrapper">ul nokkam therunchuduchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ul nokkam therunchuduchu"/>
</div>
<div class="lyrico-lyrics-wrapper">puliyaaga sethukatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puliyaaga sethukatha"/>
</div>
<div class="lyrico-lyrics-wrapper">silaiyaaga maathatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="silaiyaaga maathatha"/>
</div>
<div class="lyrico-lyrics-wrapper">puliyaaga sethukatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puliyaaga sethukatha"/>
</div>
<div class="lyrico-lyrics-wrapper">silaiyaaga maathatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="silaiyaaga maathatha"/>
</div>
<div class="lyrico-lyrics-wrapper">nan puducha naatukatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan puducha naatukatta"/>
</div>
<div class="lyrico-lyrics-wrapper">matikitta koothu katta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="matikitta koothu katta"/>
</div>
<div class="lyrico-lyrics-wrapper">soru thanee enaku ipo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="soru thanee enaku ipo"/>
</div>
<div class="lyrico-lyrics-wrapper">theva illaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theva illaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">oora kooti soru pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oora kooti soru pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">maalai mathi thaali podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maalai mathi thaali podu"/>
</div>
<div class="lyrico-lyrics-wrapper">vayilathan vandi otta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vayilathan vandi otta"/>
</div>
<div class="lyrico-lyrics-wrapper">theva illaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theva illaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">adiye potta pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adiye potta pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">kotta mutha vedikatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kotta mutha vedikatha"/>
</div>
<div class="lyrico-lyrics-wrapper">soora kaathaaga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="soora kaathaaga "/>
</div>
<div class="lyrico-lyrics-wrapper">suthi suthi thaakaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suthi suthi thaakaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">manja kelangaaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manja kelangaaka"/>
</div>
<div class="lyrico-lyrics-wrapper">unna kuduthiye urasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna kuduthiye urasi"/>
</div>
<div class="lyrico-lyrics-wrapper">kulucha atha olinju pathiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulucha atha olinju pathiye"/>
</div>
<div class="lyrico-lyrics-wrapper">oora kannala sulati adikatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oora kannala sulati adikatha"/>
</div>
<div class="lyrico-lyrics-wrapper">otha sollala ena nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="otha sollala ena nee"/>
</div>
<div class="lyrico-lyrics-wrapper">kooru podatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kooru podatha"/>
</div>
<div class="lyrico-lyrics-wrapper">oora kannala sulati adikatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oora kannala sulati adikatha"/>
</div>
<div class="lyrico-lyrics-wrapper">otha sollala ena nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="otha sollala ena nee"/>
</div>
<div class="lyrico-lyrics-wrapper">kooru podatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kooru podatha"/>
</div>
</pre>
