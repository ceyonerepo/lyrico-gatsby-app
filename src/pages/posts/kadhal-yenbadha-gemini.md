---
title: "kadhal yenbatha song lyrics"
album: "Gemini"
artist: "Bharathwaj"
lyricist: "Vairamuthu"
director: "Saran"
path: "/albums/gemini-lyrics"
song: "Kadhal Yenbatha"
image: ../../images/albumart/gemini.jpg
date: 2002-04-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/MT4ilzdH5Q4"
type: "love"
singers:
  - Timothy
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kadhal Enbadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Enbadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaamam Enbadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaamam Enbadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Irandin Mathiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irandin Mathiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Innoru Unarchiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innoru Unarchiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyirai Uyiraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirai Uyiraal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulle Kudaindhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulle Kudaindhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirin Uyrai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirin Uyrai"/>
</div>
<div class="lyrico-lyrics-wrapper">Unarum Muyarchiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unarum Muyarchiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhal Enbadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Enbadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaamam Enbadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaamam Enbadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Irandin Mathiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irandin Mathiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Innoru Unarchiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innoru Unarchiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyirai Uyiraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirai Uyiraal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulle Kudaindhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulle Kudaindhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirin Uyrai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirin Uyrai"/>
</div>
<div class="lyrico-lyrics-wrapper">Unarum Muyarchiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unarum Muyarchiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vennila Thondri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vennila Thondri"/>
</div>
<div class="lyrico-lyrics-wrapper">Venneer Thelithadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venneer Thelithadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Poo Vizhundhadhil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poo Vizhundhadhil"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhoomi Udaindhadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhoomi Udaindhadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhal Kadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Kadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Kadhal Endren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Kadhal Endren"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal Kadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Kadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Kadhal Endren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Kadhal Endren"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatril Kaatril Oar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatril Kaatril Oar"/>
</div>
<div class="lyrico-lyrics-wrapper">Osai Ketkkudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Osai Ketkkudhe"/>
</div>
</pre>
