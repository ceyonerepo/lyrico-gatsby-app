---
title: "lakeeran song lyrics"
album: "Haseen Dillruba"
artist: "Amit Trivedi"
lyricist: "Sidhant Mago"
director: "Vinil Mathew"
path: "/albums/haseen-dillruba-lyrics"
song: "Lakeeran"
image: ../../images/albumart/haseen-dillruba.jpg
date: 2021-07-02
lang: hindi
youtubeLink: "https://www.youtube.com/embed/lXJJ0wabpdk"
type: "love"
singers:
  - Asees Kaur
  - Devenderpal Singh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Naal Naal Rehke Vi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naal Naal Rehke Vi"/>
</div>
<div class="lyrico-lyrics-wrapper">Door Door Rehna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Door Door Rehna"/>
</div>
<div class="lyrico-lyrics-wrapper">Khul Ke Na Kuch Keh Pawan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khul Ke Na Kuch Keh Pawan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ya Te Chup Rehna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ya Te Chup Rehna"/>
</div>
<div class="lyrico-lyrics-wrapper">Ya Te Kauda Kehna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ya Te Kauda Kehna"/>
</div>
<div class="lyrico-lyrics-wrapper">Gall Koyi Mithi Na Sunawan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gall Koyi Mithi Na Sunawan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Us Nu Manawan Ya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Us Nu Manawan Ya"/>
</div>
<div class="lyrico-lyrics-wrapper">Khud Nu Manawan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khud Nu Manawan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kismat Nu Kaise Samjhawan Jaawan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kismat Nu Kaise Samjhawan Jaawan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ke Sab Kuch Ab Lakeeran Ab Lakeeran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ke Sab Kuch Ab Lakeeran Ab Lakeeran"/>
</div>
<div class="lyrico-lyrics-wrapper">Lakeeran Te Chadd De
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lakeeran Te Chadd De"/>
</div>
<div class="lyrico-lyrics-wrapper">Lakeeran Te Chadd Ke Jaawan Re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lakeeran Te Chadd Ke Jaawan Re"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Din Katt Jaan Pher Raatein Shuru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Din Katt Jaan Pher Raatein Shuru"/>
</div>
<div class="lyrico-lyrics-wrapper">Aur Raatan Ch Ghar Vich Kalle Phirun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aur Raatan Ch Ghar Vich Kalle Phirun"/>
</div>
<div class="lyrico-lyrics-wrapper">Main Phirun, Main Phirun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main Phirun, Main Phirun"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Khud Naal Din Bhar Gallan Karun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Khud Naal Din Bhar Gallan Karun"/>
</div>
<div class="lyrico-lyrics-wrapper">Aur Kisi De Vi Naal Na Hans Ke Milun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aur Kisi De Vi Naal Na Hans Ke Milun"/>
</div>
<div class="lyrico-lyrics-wrapper">Na Milun, Na Milun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na Milun, Na Milun"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho Ankh Band Karke Hanju Chupawan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho Ankh Band Karke Hanju Chupawan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kismat Nu Kaise Samjhawan Jaavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kismat Nu Kaise Samjhawan Jaavan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ke Sab Kuch Ab Lakeeran Ab Lakeeran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ke Sab Kuch Ab Lakeeran Ab Lakeeran"/>
</div>
<div class="lyrico-lyrics-wrapper">Lakeeran Te Chadd De
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lakeeran Te Chadd De"/>
</div>
<div class="lyrico-lyrics-wrapper">Lakeeran Te Chadd Ke Jaawan Re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lakeeran Te Chadd Ke Jaawan Re"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ke Sab Kuch Ab Lakeeran Ab Lakeeran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ke Sab Kuch Ab Lakeeran Ab Lakeeran"/>
</div>
<div class="lyrico-lyrics-wrapper">Lakeeran Te Chadd De
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lakeeran Te Chadd De"/>
</div>
<div class="lyrico-lyrics-wrapper">Lakeeran Te Chadd Ke Jaawan Re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lakeeran Te Chadd Ke Jaawan Re"/>
</div>
</pre>
