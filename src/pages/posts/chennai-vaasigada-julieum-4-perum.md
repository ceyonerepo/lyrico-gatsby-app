---
title: "chennai vaasigada song lyrics"
album: "Julieum 4 Perum"
artist: "Raghu Sravan Kumar"
lyricist: "G G Ganesh"
director: "R V Satheesh"
path: "/albums/julieum-4-perum-lyrics"
song: "Chennai Vaasigada"
image: ../../images/albumart/julieum-4-perum.jpg
date: 2017-04-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/SLPniJFi_6U"
type: "mass"
singers:
  -	Madhan Rapking
  - Raghu Sravan Kumar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Chennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chennai"/>
</div>
<div class="lyrico-lyrics-wrapper">Chennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chennai"/>
</div>
<div class="lyrico-lyrics-wrapper">Chennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chennai"/>
</div>
<div class="lyrico-lyrics-wrapper">Chennai Vasiga Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chennai Vasiga Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chennai"/>
</div>
<div class="lyrico-lyrics-wrapper">Chennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chennai"/>
</div>
<div class="lyrico-lyrics-wrapper">Chennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chennai"/>
</div>
<div class="lyrico-lyrics-wrapper">Chennai Vasiga Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chennai Vasiga Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chennai"/>
</div>
<div class="lyrico-lyrics-wrapper">Chennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chennai"/>
</div>
<div class="lyrico-lyrics-wrapper">Chennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chennai"/>
</div>
<div class="lyrico-lyrics-wrapper">Chennai Vasiga Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chennai Vasiga Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aasoku Eesoku Apodhu Double'u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasoku Eesoku Apodhu Double'u"/>
</div>
<div class="lyrico-lyrics-wrapper">So Many Things Idhu Chennai Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="So Many Things Idhu Chennai Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Lasapadi Jeans'u Glad To Meet U Jaashunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lasapadi Jeans'u Glad To Meet U Jaashunu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maara Thamizh Thimiru Idhu Chennai Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maara Thamizh Thimiru Idhu Chennai Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Panamkandu Fox'a Maaranum Raasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panamkandu Fox'a Maaranum Raasa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yepodhum Rush Idhu Chennai Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yepodhum Rush Idhu Chennai Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Gopa Gopaana Nichaluku Marina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gopa Gopaana Nichaluku Marina"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellam Thangum Idam Idhu Chennai Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellam Thangum Idam Idhu Chennai Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaada Machan Neeyum Chennai Vasi Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada Machan Neeyum Chennai Vasi Dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollitharen Naanum Romba Easy Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollitharen Naanum Romba Easy Dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaada Machan Neeyum Chennai Vasi Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada Machan Neeyum Chennai Vasi Dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollitharen Naanum Romba Easy Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollitharen Naanum Romba Easy Dhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chummave Nee Irundhalum Romba Busy Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chummave Nee Irundhalum Romba Busy Dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Odum Janam Kootam Adhu Onnu Pasi Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odum Janam Kootam Adhu Onnu Pasi Dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhukum Paasiyil Running Race'u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhukum Paasiyil Running Race'u"/>
</div>
<div class="lyrico-lyrics-wrapper">Numma Winning Ayita Neeyum Mass'u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Numma Winning Ayita Neeyum Mass'u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vazhukum Paasiyil Running Race'u 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhukum Paasiyil Running Race'u "/>
</div>
<div class="lyrico-lyrics-wrapper">Numma Winning Ayita Neeyum Mass'u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Numma Winning Ayita Neeyum Mass'u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaada Machan Neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada Machan Neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaada Machan Neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada Machan Neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaada Machan Neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada Machan Neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Vaada Machan Neeyum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Vaada Machan Neeyum "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hmm..Kadana Poka Parakuren Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm..Kadana Poka Parakuren Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Kadal Alla Noraya Odayuren Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Kadal Alla Noraya Odayuren Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thenga Paricha Thenna Maratha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thenga Paricha Thenna Maratha "/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Building Clock La Theduren Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Building Clock La Theduren Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Da Mapila Ooru Idhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Da Mapila Ooru Idhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sematti Adiya Veyil Iruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sematti Adiya Veyil Iruku"/>
</div>
<div class="lyrico-lyrics-wrapper">Eduvata Payala Mattikitu    
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eduvata Payala Mattikitu    "/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanga Enga Da Vazhi Iruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanga Enga Da Vazhi Iruku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Udhatin Varthaigal Pazhakangal Ingae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udhatin Varthaigal Pazhakangal Ingae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyril Kalakum Anbu Yenge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyril Kalakum Anbu Yenge"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaada Machan Neeyum Chennai Vasi Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada Machan Neeyum Chennai Vasi Dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollitharen Naanum Romba Easy Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollitharen Naanum Romba Easy Dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaada Machan Neeyum Chennai Vasi Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada Machan Neeyum Chennai Vasi Dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollitharen Naanum Romba Easy Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollitharen Naanum Romba Easy Dhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Basin Bridge'u Andhaanda Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Basin Bridge'u Andhaanda Dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vyasarpadi Vandhurume 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vyasarpadi Vandhurume "/>
</div>
<div class="lyrico-lyrics-wrapper">Gaana Paatu Angu Neeye Allikanum Da 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaana Paatu Angu Neeye Allikanum Da "/>
</div>
<div class="lyrico-lyrics-wrapper">Kutty Japan Richi Street'u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kutty Japan Richi Street'u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pakkathula Chella Pulla Chotta Bommai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkathula Chella Pulla Chotta Bommai"/>
</div>
<div class="lyrico-lyrics-wrapper">Hindi Maalum Sowcarpet'a Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hindi Maalum Sowcarpet'a Dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">T.Nagar Nungambakkam Ellaiyaha Konda Numma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="T.Nagar Nungambakkam Ellaiyaha Konda Numma"/>
</div>
<div class="lyrico-lyrics-wrapper">World Famous Kollywood'u Kodambakkam'ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="World Famous Kollywood'u Kodambakkam'ae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nelson Manickam Indha Arcot'u Salaiyum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nelson Manickam Indha Arcot'u Salaiyum "/>
</div>
<div class="lyrico-lyrics-wrapper">Sandhikura Pulliyum Enga Soolaimedu Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhikura Pulliyum Enga Soolaimedu Dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nelson Manickam Indha Arcot'u Salaiyum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nelson Manickam Indha Arcot'u Salaiyum "/>
</div>
<div class="lyrico-lyrics-wrapper">Sandhikura Pulliyum Enga Soolaimedu Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhikura Pulliyum Enga Soolaimedu Dhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ella Sanamum Vandhu Iranga Varaverkum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ella Sanamum Vandhu Iranga Varaverkum "/>
</div>
<div class="lyrico-lyrics-wrapper">Koyambedum Imbaam Periya Marketum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koyambedum Imbaam Periya Marketum"/>
</div>
<div class="lyrico-lyrics-wrapper">[ella Sanamum Vandhu Iranga Varaverkum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="[ella Sanamum Vandhu Iranga Varaverkum "/>
</div>
<div class="lyrico-lyrics-wrapper">Koyambedum Imbaam Periya Marketum]
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koyambedum Imbaam Periya Marketum]"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaada Machan Neeyum Chennai Vasi Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada Machan Neeyum Chennai Vasi Dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollitharen Naanum Romba Easy Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollitharen Naanum Romba Easy Dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaada Machan Neeyum Chennai Vasi Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada Machan Neeyum Chennai Vasi Dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollitharen Naanum Romba Easy Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollitharen Naanum Romba Easy Dhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chummave Nee Irundhalum Romba Busy Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chummave Nee Irundhalum Romba Busy Dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Odum Janam Kootam Adhu Onnu Pasi Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odum Janam Kootam Adhu Onnu Pasi Dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhukum Paasiyil Running Race'u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhukum Paasiyil Running Race'u"/>
</div>
<div class="lyrico-lyrics-wrapper">Numma Winning Ayita Neeyum Mass'u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Numma Winning Ayita Neeyum Mass'u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vazhukum Paasiyil Running Race'u 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhukum Paasiyil Running Race'u "/>
</div>
<div class="lyrico-lyrics-wrapper">Numma Winning Ayita Neeyum Mass'u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Numma Winning Ayita Neeyum Mass'u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaada Machan Neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada Machan Neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaada Machan Neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada Machan Neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaada Machan Neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada Machan Neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Vaada Machan Neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Vaada Machan Neeyum"/>
</div>
</pre>
