---
title: "kathanayaka song lyrics"
album: "NTR Kathanayakudu"
artist: "M.M. Keeravani"
lyricist: "K. Siva Datta - Dr.K. Rama Krishna"
director: "Krish Jagarlamudi"
path: "/albums/ntr-kathanayakudu-lyrics"
song: "Kathanayaka"
image: ../../images/albumart/ntr-kathanayakudu.jpg
date: 2019-01-09
lang: telugu
youtubeLink: "https://www.youtube.com/embed/QxdFY0qUrkc"
type: "happy"
singers:
  - Kailash Kher
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ghana keerthisandhra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ghana keerthisandhra"/>
</div>
<div class="lyrico-lyrics-wrapper">Vijithaakilandra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vijithaakilandra"/>
</div>
<div class="lyrico-lyrics-wrapper">Janathaasudeendra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Janathaasudeendra"/>
</div>
<div class="lyrico-lyrics-wrapper">Mani deepaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mani deepaka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ghana keerthisandhra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ghana keerthisandhra"/>
</div>
<div class="lyrico-lyrics-wrapper">Vijithaakilandhra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vijithaakilandhra"/>
</div>
<div class="lyrico-lyrics-wrapper">Janathaasudheendhra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Janathaasudheendhra"/>
</div>
<div class="lyrico-lyrics-wrapper">Mani deepaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mani deepaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Thrisathakaadhika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thrisathakaadhika"/>
</div>
<div class="lyrico-lyrics-wrapper">Chithramaalika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chithramaalika"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaithrayaathrika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaithrayaathrika"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathaanaayakaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathaanaayakaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ghana keerthisandhra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ghana keerthisandhra"/>
</div>
<div class="lyrico-lyrics-wrapper">Vijithaakilandra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vijithaakilandra"/>
</div>
<div class="lyrico-lyrics-wrapper">Janathaasudeendra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Janathaasudeendra"/>
</div>
<div class="lyrico-lyrics-wrapper">Mani deepaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mani deepaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Thrisathakaadhika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thrisathakaadhika"/>
</div>
<div class="lyrico-lyrics-wrapper">Chithramaalika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chithramaalika"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaithrayaathrika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaithrayaathrika"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathaanaayakaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathaanaayakaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aahaaryangika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aahaaryangika"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaachika poorvaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaachika poorvaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhbutha athulitha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhbutha athulitha"/>
</div>
<div class="lyrico-lyrics-wrapper">Natanaa ghatikaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natanaa ghatikaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bheemasena veerarjuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bheemasena veerarjuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Krishna dhaanakarna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Krishna dhaanakarna"/>
</div>
<div class="lyrico-lyrics-wrapper">Maanadhana suyodhana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maanadhana suyodhana"/>
</div>
<div class="lyrico-lyrics-wrapper">Bheeshma bruhannala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bheeshma bruhannala"/>
</div>
<div class="lyrico-lyrics-wrapper">Vishwamithra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vishwamithra"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lankeswara dhasakantaraavanaasuradhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lankeswara dhasakantaraavanaasuradhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Puradhi purusha bhoomikaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puradhi purusha bhoomikaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Poshakaa saakshathsaakshathkaaraka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poshakaa saakshathsaakshathkaaraka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thvadeeya chaaayaachithracchaaditha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thvadeeya chaaayaachithracchaaditha"/>
</div>
<div class="lyrico-lyrics-wrapper">Raajitha ranjitha chithrayavanikaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raajitha ranjitha chithrayavanikaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Na idham poorvaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na idham poorvaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Rasothpaadhakaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rasothpaadhakaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Keerthikanyakaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Keerthikanyakaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Manonaayakaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manonaayakaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathaanaayakaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathaanaayakaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathaanaayakaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathaanaayakaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ghana keerthisandhra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ghana keerthisandhra"/>
</div>
<div class="lyrico-lyrics-wrapper">Vijithaakilandhra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vijithaakilandhra"/>
</div>
<div class="lyrico-lyrics-wrapper">Janathaasudheendhra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Janathaasudheendhra"/>
</div>
<div class="lyrico-lyrics-wrapper">Mani deepaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mani deepaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Thrisathakaadhika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thrisathakaadhika"/>
</div>
<div class="lyrico-lyrics-wrapper">Chithramaalika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chithramaalika"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaithrayaathrika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaithrayaathrika"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathaanaayakaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathaanaayakaa"/>
</div>
</pre>
