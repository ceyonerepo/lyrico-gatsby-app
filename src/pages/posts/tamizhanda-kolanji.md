---
title: "tamizhanda song lyrics"
album: "Kolanji"
artist: "Natarajan Sankaran"
lyricist: "Naveen"
director: "Dhanaram Saravanan"
path: "/albums/kolanji-lyrics"
song: "Tamizhanda"
image: ../../images/albumart/kolanji.jpg
date: 2019-07-26
lang: tamil
youtubeLink: "https://www.youtube.com/embed/JdEc3DqUmF4"
type: "mass"
singers:
  - Deepak
  - Naveen
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Oru vellakaaran kitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru vellakaaran kitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee thamizhla pesuviya daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thamizhla pesuviya daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ulagathin mudhal inam naanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagathin mudhal inam naanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Manidhathin maru peyar naanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manidhathin maru peyar naanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kal thondri mann thondraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kal thondri mann thondraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha kaalathil piranthavan naanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha kaalathil piranthavan naanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Emmarabil jaathigal illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emmarabil jaathigal illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Em kulathil vedhangal illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Em kulathil vedhangal illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Samathuvathin pillaigal naanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samathuvathin pillaigal naanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Thala mimirndhae nadappomdaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thala mimirndhae nadappomdaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thamizhandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thamizhandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pachcha thamizhanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pachcha thamizhanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Thamizhandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thamizhandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pachcha thamizhanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pachcha thamizhanda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aadhikka unarvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadhikka unarvai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aruththerivom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aruththerivom"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbukku mattum adi panivom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbukku mattum adi panivom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thamizhandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thamizhandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pachcha thamizhanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pachcha thamizhanda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pachcha thamizhanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pachcha thamizhanda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tamil theriyadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tamil theriyadha"/>
</div>
<div class="lyrico-lyrics-wrapper">English karan kitta thamizhla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="English karan kitta thamizhla"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesa kudathunu therinja unnaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesa kudathunu therinja unnaku"/>
</div>
<div class="lyrico-lyrics-wrapper">English theriyadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="English theriyadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Pachcha thamizhan kitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pachcha thamizhan kitta"/>
</div>
<div class="lyrico-lyrics-wrapper">English la pesa kudathunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="English la pesa kudathunu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen da theriyama pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen da theriyama pochu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pirappukum ellaa uyirkkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirappukum ellaa uyirkkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sonnavan thamizhanthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sonnavan thamizhanthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaadhum oorae yaavarum kaelir
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaadhum oorae yaavarum kaelir"/>
</div>
<div class="lyrico-lyrics-wrapper">Sonnavan thamizhanthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sonnavan thamizhanthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalvi selvam kondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalvi selvam kondu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhnthavan thamizhanthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhnthavan thamizhanthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellai ivanukku illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellai ivanukku illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagam mattumthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagam mattumthaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sila yaettu thittanagal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sila yaettu thittanagal"/>
</div>
<div class="lyrico-lyrics-wrapper">Theettum sattangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theettum sattangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Suttu podiyaakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suttu podiyaakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Thamizh paattu sathangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thamizh paattu sathangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Chittu kuruvigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chittu kuruvigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhesiya mozhiyaakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhesiya mozhiyaakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aadhikka unarvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadhikka unarvai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aruththerivom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aruththerivom"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbukku mattum adi panivom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbukku mattum adi panivom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thamizhandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thamizhandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pachcha thamizhanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pachcha thamizhanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Thamizhandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thamizhandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pachcha thamizhanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pachcha thamizhanda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thamizhandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thamizhandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pachcha thamizhanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pachcha thamizhanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Thamizhandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thamizhandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pachcha thamizhanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pachcha thamizhanda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thamizhandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thamizhandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pachcha thamizhanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pachcha thamizhanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Thamizhandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thamizhandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pachcha thamizhanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pachcha thamizhanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Thamizhandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thamizhandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pachcha thamizhanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pachcha thamizhanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Thamizhandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thamizhandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pachcha thamizhanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pachcha thamizhanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Thamizhandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thamizhandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pachcha thamizhanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pachcha thamizhanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Thamizhandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thamizhandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pachcha thamizhanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pachcha thamizhanda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thamizhandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thamizhandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pachcha thamizhanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pachcha thamizhanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Thamizhandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thamizhandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pachcha thamizhanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pachcha thamizhanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Thamizhandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thamizhandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pachcha thamizhanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pachcha thamizhanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Thamizhandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thamizhandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pachcha thamizhanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pachcha thamizhanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Thamizhandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thamizhandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pachcha thamizhanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pachcha thamizhanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Thamizhandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thamizhandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pachcha thamizhanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pachcha thamizhanda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ulagathin mudhal inam naanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagathin mudhal inam naanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Manidhathin maru peyar naanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manidhathin maru peyar naanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kal thondri mann thondraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kal thondri mann thondraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha kaalathil piranthavan naanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha kaalathil piranthavan naanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Emmarabil jaathigal illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emmarabil jaathigal illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Em kulathil vedhangal illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Em kulathil vedhangal illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Samathuvathin pillaigal naanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samathuvathin pillaigal naanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Thala mimirndhae nadappomdaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thala mimirndhae nadappomdaa"/>
</div>
</pre>
