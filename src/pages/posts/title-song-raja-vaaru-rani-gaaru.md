---
title: "title song song lyrics"
album: "Raja Vaaru Rani Gaaru"
artist: "Jay Krish"
lyricist: "Sanapati Bharadwaj Patrudu"
director: "Ravi Kiran Kola"
path: "/albums/raja-vaaru-rani-gaaru-lyrics"
song: "Title Song"
image: ../../images/albumart/raja-vaaru-rani-gaaru.jpg
date: 2019-11-29
lang: telugu
youtubeLink: "https://www.youtube.com/embed/uY-zFo4KS7Q"
type: "love"
singers:
  - Anurag Kulkarni
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Raja Vaaru Raasinuttharam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja Vaaru Raasinuttharam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooru Daati Pothundi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooru Daati Pothundi"/>
</div>
<div class="lyrico-lyrics-wrapper">Mm 90 Voollu Thirigi Thirigi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mm 90 Voollu Thirigi Thirigi "/>
</div>
<div class="lyrico-lyrics-wrapper">Malli Eppudu Vasthundi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malli Eppudu Vasthundi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anaganaganaga Rajuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anaganaganaga Rajuu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sogasari Kanulaa Ranii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sogasari Kanulaa Ranii"/>
</div>
<div class="lyrico-lyrics-wrapper">Inukondayyaa Sebuthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inukondayyaa Sebuthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Iddari Gaadhaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iddari Gaadhaa "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anaganaganaga Rajuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anaganaganaga Rajuu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sogasari Kanulaa Ranii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sogasari Kanulaa Ranii"/>
</div>
<div class="lyrico-lyrics-wrapper">Inukondayyaa Sebuthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inukondayyaa Sebuthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Iddari Gaadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iddari Gaadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yepudochindho Gaanii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yepudochindho Gaanii"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedurochindhoy Ranii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedurochindhoy Ranii"/>
</div>
<div class="lyrico-lyrics-wrapper">Thega Mechaadoi Rajuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thega Mechaadoi Rajuu"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasichaduu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasichaduu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gunde Gadhulannitilonuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gunde Gadhulannitilonuu"/>
</div>
<div class="lyrico-lyrics-wrapper">Valapune Koluvunchaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valapune Koluvunchaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalupulesi Thalapulona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalupulesi Thalapulona"/>
</div>
<div class="lyrico-lyrics-wrapper">Thelinaaduu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thelinaaduu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ledu Mari Rajanu Vaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ledu Mari Rajanu Vaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ranimayamaipoyaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ranimayamaipoyaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanaki Thaane Guruthuledu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanaki Thaane Guruthuledu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adigi Chuudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adigi Chuudu"/>
</div>
<div class="lyrico-lyrics-wrapper">Rajaa Vaaru Ranii Gaaruu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rajaa Vaaru Ranii Gaaruu"/>
</div>
<div class="lyrico-lyrics-wrapper">Repo Maapo Okatavtaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Repo Maapo Okatavtaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Uruu Vaadaa Eeduu Joduu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uruu Vaadaa Eeduu Joduu"/>
</div>
<div class="lyrico-lyrics-wrapper">Baagundantaaruu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baagundantaaruu"/>
</div>
<div class="lyrico-lyrics-wrapper">Rajaa Veruu Ranii Veruu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rajaa Veruu Ranii Veruu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chustuu Undu Jantavutaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chustuu Undu Jantavutaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Rojanthaa Uruuranthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Rojanthaa Uruuranthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Horetthe Theeruu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Horetthe Theeruu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oho Maa Kadhaku Mechi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oho Maa Kadhaku Mechi"/>
</div>
<div class="lyrico-lyrics-wrapper">O Anna Rupaay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Anna Rupaay"/>
</div>
<div class="lyrico-lyrics-wrapper">O Akka Rendu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Akka Rendu"/>
</div>
<div class="lyrico-lyrics-wrapper">Peddayana Padhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peddayana Padhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaarini Vaari Kutumbalni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaarini Vaari Kutumbalni"/>
</div>
<div class="lyrico-lyrics-wrapper">Devudu Sallanga Sudaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Devudu Sallanga Sudaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Mari Malli Kadhalokelthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mari Malli Kadhalokelthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theegani Laagaka Koosunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theegani Laagaka Koosunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Donkani Yevarata Kadhipedhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Donkani Yevarata Kadhipedhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Raajuki Bhayamata Premani Teluputa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raajuki Bhayamata Premani Teluputa"/>
</div>
<div class="lyrico-lyrics-wrapper">Chethavadantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chethavadantha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lopata Chitapata Yedha Manta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lopata Chitapata Yedha Manta"/>
</div>
<div class="lyrico-lyrics-wrapper">Raniki Adi Vinapadadanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raniki Adi Vinapadadanta"/>
</div>
<div class="lyrico-lyrics-wrapper">Aagani Parugata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagani Parugata"/>
</div>
<div class="lyrico-lyrics-wrapper">Raadhata Alasata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raadhata Alasata"/>
</div>
<div class="lyrico-lyrics-wrapper">Loo Salupanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Loo Salupanta"/>
</div>
<div class="lyrico-lyrics-wrapper">Yem Valapanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yem Valapanta"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam Paamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam Paamai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaate Vesenata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaate Vesenata"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhii Ranii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhii Ranii"/>
</div>
<div class="lyrico-lyrics-wrapper">Ledhee Chota
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ledhee Chota"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ooru Daatesindhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooru Daatesindhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Yem Parledhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yem Parledhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Rani Gaaru Kachitamga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rani Gaaru Kachitamga "/>
</div>
<div class="lyrico-lyrics-wrapper">Thirigotharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirigotharu"/>
</div>
<div class="lyrico-lyrics-wrapper">Raja Vaaru Thana 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja Vaaru Thana "/>
</div>
<div class="lyrico-lyrics-wrapper">Premani Cheptharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Premani Cheptharu"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheppi Theeratharu Anthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheppi Theeratharu Anthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Rajaa Vaaru Ranii Gaaruu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rajaa Vaaru Ranii Gaaruu"/>
</div>
<div class="lyrico-lyrics-wrapper">Repo Maapo Okatavtaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Repo Maapo Okatavtaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Uruu Vaadaa Eeduu Joduu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uruu Vaadaa Eeduu Joduu"/>
</div>
<div class="lyrico-lyrics-wrapper">Baagundantaaruu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baagundantaaruu"/>
</div>
<div class="lyrico-lyrics-wrapper">Rajaa Veruu Ranii Veruu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rajaa Veruu Ranii Veruu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chustuu Undu Jantavutaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chustuu Undu Jantavutaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Rojanthaa Uruuranthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Rojanthaa Uruuranthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Horetthe Theeruu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Horetthe Theeruu"/>
</div>
</pre>
