---
title: "kelappu kelappu song lyrics"
album: "MGR Magan"
artist: "Anthony Daasan"
lyricist: "Muragan Manthiram"
director: "Ponram"
path: "/albums/mgr-magan-lyrics"
song: "Kelappu Kelappu"
image: ../../images/albumart/mgr-magan.jpg
date: 2021-11-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/tu-QCRZUDLM"
type: "happy"
singers:
  - Anthony Daasan
  - Pooja Vaidyanath
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kelappu kelappu kela kelappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelappu kelappu kela kelappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kelappu kelappu kela kelappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelappu kelappu kela kelappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kelappu kelappu kela kelappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelappu kelappu kela kelappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kelappu kelappu kela kelappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelappu kelappu kela kelappu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaeni periyakulam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaeni periyakulam"/>
</div>
<div class="lyrico-lyrics-wrapper">Bodi madura kambam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bodi madura kambam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pera osathi pugazh kelappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pera osathi pugazh kelappu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeh vaadipatti vaiyampatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh vaadipatti vaiyampatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Aandipatti usilampatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aandipatti usilampatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Puzhuthi parakka makka kelappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puzhuthi parakka makka kelappu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kelappu kelappu kela kelappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelappu kelappu kela kelappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kelappu kelappu kela kelappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelappu kelappu kela kelappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kelappu kelappu kela kelappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelappu kelappu kela kelappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kelappu kelappu kela kelappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelappu kelappu kela kelappu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adiyei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiyei"/>
</div>
<div class="lyrico-lyrics-wrapper">Naangalam pala soli karainga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naangalam pala soli karainga"/>
</div>
<div class="lyrico-lyrics-wrapper">Inaikku yeh-hennu iruppom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inaikku yeh-hennu iruppom"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalaikku oh oh-nnu iruppom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalaikku oh oh-nnu iruppom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bennicuik-ku vanthathaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bennicuik-ku vanthathaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponga paanai ponguthunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponga paanai ponguthunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Nandri solli vaazhthiduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nandri solli vaazhthiduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Mullai aara thanthavanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mullai aara thanthavanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Perusoda panchayathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perusoda panchayathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhichom naanga kattupattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhichom naanga kattupattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yelangala thullikittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yelangala thullikittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adakkum veeram jallikattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adakkum veeram jallikattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Soththu paththa viththu vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soththu paththa viththu vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dharmam kaththa ella saamii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dharmam kaththa ella saamii"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanam paaathu vaazhum bhoomi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam paaathu vaazhum bhoomi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazha vachaaru vella saamii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazha vachaaru vella saamii"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adida kottu mozhanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adida kottu mozhanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma amudha iduppu kulunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma amudha iduppu kulunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Yei adida kottu mozhanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei adida kottu mozhanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma amudha iduppu kulunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma amudha iduppu kulunga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kelappu kelappu kela kelappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelappu kelappu kela kelappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kelappu kelappu kela kelappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelappu kelappu kela kelappu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yow
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yow"/>
</div>
<div class="lyrico-lyrics-wrapper">Naangalam vaadi patti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naangalam vaadi patti"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandhaiyila vambuluthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhaiyila vambuluthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaththala kundu sandhaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaththala kundu sandhaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Yelam vuttu poravaluga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yelam vuttu poravaluga"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga kittayevaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga kittayevaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kummi kulava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kummi kulava"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga kottum azhaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga kottum azhaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilavattamellaam vattamidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilavattamellaam vattamidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga azhaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga azhaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Amman edhira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amman edhira"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhoomi thantha kathira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhoomi thantha kathira"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponga vachi poosa pannom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponga vachi poosa pannom"/>
</div>
<div class="lyrico-lyrics-wrapper">Vamsam valara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vamsam valara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oororam kaaval yaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oororam kaaval yaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Aruva yendhi nikkiraaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aruva yendhi nikkiraaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalavaada yaarum illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalavaada yaarum illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaaval kaappaan ayyanaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaval kaappaan ayyanaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhiyaadha veeramunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhiyaadha veeramunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Usura uzhaikkum koottamunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usura uzhaikkum koottamunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Pasiyodu yaarum vandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pasiyodu yaarum vandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Virunthae vaikkum vamsam naanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Virunthae vaikkum vamsam naanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeyum naanum veru illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum naanum veru illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaada pangaali koothaadalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada pangaali koothaadalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Veesum kaathum baedham illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veesum kaathum baedham illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorae onnaaga kondaadalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorae onnaaga kondaadalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adida kottu mozhanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adida kottu mozhanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma amudha iduppu kulunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma amudha iduppu kulunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Yei adida kottu mozhanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei adida kottu mozhanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma amudha iduppu kulunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma amudha iduppu kulunga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeh thaeni periyakulam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh thaeni periyakulam"/>
</div>
<div class="lyrico-lyrics-wrapper">Bodi madura kambam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bodi madura kambam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pera osathi pugazh kelappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pera osathi pugazh kelappu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeh vaadipatti vaiyampatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh vaadipatti vaiyampatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Aandipatti usilampatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aandipatti usilampatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Puzhuthi parakka makka kelappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puzhuthi parakka makka kelappu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kelappu kelappu kela kelappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelappu kelappu kela kelappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kelappu kelappu kela kelappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelappu kelappu kela kelappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kelappu kelappu kela kelappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelappu kelappu kela kelappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kelappu kelappu kela kelappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelappu kelappu kela kelappu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pongaloo pongal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pongaloo pongal"/>
</div>
<div class="lyrico-lyrics-wrapper">Paal paanai ponga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paal paanai ponga"/>
</div>
<div class="lyrico-lyrics-wrapper">Paththum peruga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paththum peruga"/>
</div>
<div class="lyrico-lyrics-wrapper">Paasangal vedikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paasangal vedikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Noiyum nodiyum theru ooram poga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Noiyum nodiyum theru ooram poga"/>
</div>
<div class="lyrico-lyrics-wrapper">Pongaloo pongal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pongaloo pongal"/>
</div>
<div class="lyrico-lyrics-wrapper">Pongaloo pongal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pongaloo pongal"/>
</div>
<div class="lyrico-lyrics-wrapper">Pongaloo pongal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pongaloo pongal"/>
</div>
</pre>
