---
title: "manasa manasa song lyrics"
album: "Most Eligible Bachelor"
artist: "Gopi Sundar"
lyricist: "Surendra Krishna"
director: "Bommarillu Bhaskar"
path: "/albums/most-eligible-bachelor-lyrics"
song: "Manasa Manasa"
image: ../../images/albumart/most-eligible-bachelor.jpg
date: 2021-10-15
lang: telugu
youtubeLink: "https://www.youtube.com/embed/HF2qTYvDaOw"
type: "love"
singers:
  - Sid Sriram
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Mansaa Mansaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mansaa Mansaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasaa Manasaa Manasaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasaa Manasaa Manasaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Brathimaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Brathimaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanavalalo padaboke manasaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanavalalo padaboke manasaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pilichaa aricha ayinaa nuvvu vinakunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pilichaa aricha ayinaa nuvvu vinakunda"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanavaipu velathaave Manasaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanavaipu velathaave Manasaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa maata alusaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa maata alusaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nevevaro thelusaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nevevaro thelusaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naathone untavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naathone untavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanne nadipisthaavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanne nadipisthaavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannaadipisthaave manasaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannaadipisthaave manasaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mansaa manasara brathimaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mansaa manasara brathimaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanavalalo padaboke manasaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanavalalo padaboke manasaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pilichaa aricha aina nuvvu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pilichaa aricha aina nuvvu "/>
</div>
<div class="lyrico-lyrics-wrapper">vinakunda thana vaipu veltha manasaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vinakunda thana vaipu veltha manasaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Emundhi thanalona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emundhi thanalona"/>
</div>
<div class="lyrico-lyrics-wrapper">gammatthu ante adhi thaati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gammatthu ante adhi thaati"/>
</div>
<div class="lyrico-lyrics-wrapper">Matthedho undhantu antu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Matthedho undhantu antu"/>
</div>
<div class="lyrico-lyrics-wrapper">thanakanna andhalu unnayi ante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanakanna andhalu unnayi ante"/>
</div>
<div class="lyrico-lyrics-wrapper">Andhaanike thaanoo Aakaashamantu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhaanike thaanoo Aakaashamantu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvve naa maataa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvve naa maataa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nuvve naa maata vinakunte manasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvve naa maata vinakunte manasa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaane nee maata vintundhaa aashaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaane nee maata vintundhaa aashaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa maata alusa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa maata alusa"/>
</div>
<div class="lyrico-lyrics-wrapper">nevevaro thelusa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nevevaro thelusa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naathone untavu nanne 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naathone untavu nanne "/>
</div>
<div class="lyrico-lyrics-wrapper">nadipisthaavu manasaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nadipisthaavu manasaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mansaa Mansaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mansaa Mansaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasaa Manasaa Manasaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasaa Manasaa Manasaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Brathimaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Brathimaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanavalalo padaboke manasaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanavalalo padaboke manasaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pilichaa aricha ayinaa nuvvu vinakunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pilichaa aricha ayinaa nuvvu vinakunda"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanavaipu velathaave Manasaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanavaipu velathaave Manasaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thelivantha naa sonthaamanukuntu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thelivantha naa sonthaamanukuntu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiriga thanamundhu nunchunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiriga thanamundhu nunchunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa peru mari chat
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa peru mari chat"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa maatale vuntu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa maatale vuntu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mathipoyi nilicha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mathipoyi nilicha"/>
</div>
<div class="lyrico-lyrics-wrapper">Badhulekkada undhantu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Badhulekkada undhantu"/>
</div>
<div class="lyrico-lyrics-wrapper">prathichota vethika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="prathichota vethika"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanatho vunde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanatho vunde"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanatho vunde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanatho vunde"/>
</div>
<div class="lyrico-lyrics-wrapper">Okkakka nimisham marala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okkakka nimisham marala"/>
</div>
<div class="lyrico-lyrics-wrapper">Marala pudathaava manasaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marala pudathaava manasaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa maata alusaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa maata alusaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nevevaro thelusaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nevevaro thelusaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naathone untavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naathone untavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanne nadipisthaavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanne nadipisthaavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannaadipisthaave manasaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannaadipisthaave manasaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mansaa Mansaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mansaa Mansaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasaa Manasaa Manasaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasaa Manasaa Manasaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Brathimaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Brathimaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanavalalo padaboke manasaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanavalalo padaboke manasaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pilichaa aricha ayinaa nuvvu vinakunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pilichaa aricha ayinaa nuvvu vinakunda"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanavaipu velathaave Manasaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanavaipu velathaave Manasaa"/>
</div>
</pre>
