---
title: "kanave urave song lyrics"
album: "Plan Panni Pannanum"
artist: "Yuvan Shankar Raja"
lyricist: "Niranjan Bharathi"
director: "Badri Venkatesh"
path: "/albums/plan-panni-pannanum-lyrics"
song: "Kanave Urave"
image: ../../images/albumart/plan-panni-pannanum.jpg
date: 2021-12-30
lang: tamil
youtubeLink: "https://www.youtube.com/embed/HpsCtODtnAs"
type: "sad"
singers:
  - Shreya Ghoshal
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kanave Kanave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanave Kanave"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhiyil Nigazhaa Nijame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhiyil Nigazhaa Nijame"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai Naan Izhanthaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai Naan Izhanthaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Neruppil Eriyum Uyire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neruppil Eriyum Uyire"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Urave Urave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urave Urave"/>
</div>
<div class="lyrico-lyrics-wrapper">Siragai Needhaan Koduthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siragai Needhaan Koduthaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinnai Paarthen Udane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinnai Paarthen Udane"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhai Nee Parithaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai Nee Parithaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ilakkindri Idhayangal Iyangidumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilakkindri Idhayangal Iyangidumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Iyangida Idhayamum Muyandridumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iyangida Idhayamum Muyandridumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaname Veezhndhadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaname Veezhndhadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaalum Vidiyaadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaalum Vidiyaadhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manadhai Sudume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manadhai Sudume"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodhikkum Kanneer Mazhaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodhikkum Kanneer Mazhaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Iruzhil Azhiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruzhil Azhiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennam Sellum Dhisaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennam Sellum Dhisaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhkkai Inimel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkkai Inimel"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodiya Thanimai Chiraiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodiya Thanimai Chiraiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Sondham Irunthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sondham Irunthum"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku Valiye Thunaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku Valiye Thunaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhagiya Aasaiyaal Naane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagiya Aasaiyaal Naane"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakkoru Kallarai Aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkoru Kallarai Aanen"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatrile Oothidum Thugalaai Ponene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatrile Oothidum Thugalaai Ponene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidhaiyile Payirai Naan Kanden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidhaiyile Payirai Naan Kanden"/>
</div>
<div class="lyrico-lyrics-wrapper">Chediyile Thulirai Naan Kanden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chediyile Thulirai Naan Kanden"/>
</div>
<div class="lyrico-lyrics-wrapper">Pengalin Kanavugal Kaanal Neerdhaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pengalin Kanavugal Kaanal Neerdhaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhunthidum Alaigal Uyaraadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhunthidum Alaigal Uyaraadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Valigalil Valimai Vaaraadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valigalil Valimai Vaaraadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ranangalil Vazhigal Pirakkaadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ranangalil Vazhigal Pirakkaadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Suttaal Minnum Thangam Naanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suttaal Minnum Thangam Naanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Poo Manadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poo Manadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanave Kanave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanave Kanave"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhiyil Nigazhaa Nijame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhiyil Nigazhaa Nijame"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai Naan Izhanthaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai Naan Izhanthaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Neruppil Eriyum Uyire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neruppil Eriyum Uyire"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Urave Urave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urave Urave"/>
</div>
<div class="lyrico-lyrics-wrapper">Siragai Needhaan Koduthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siragai Needhaan Koduthaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinnai Paarthen Udane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinnai Paarthen Udane"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhai Nee Parithaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai Nee Parithaai"/>
</div>
</pre>
