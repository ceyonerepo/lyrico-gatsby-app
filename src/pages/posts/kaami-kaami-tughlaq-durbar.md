---
title: "kaami kaami song lyrics"
album: "Tughlaq Durbar"
artist: "Govind Vasantha"
lyricist: "Madhan Karky"
director: "Balaji Tharaneetharan"
path: "/albums/tughlaq-durbar-song-lyrics"
song: "Kaami Kaami"
image: ../../images/albumart/tughlaq-durbar.jpg
date: 2021-09-10
lang: tamil
youtubeLink: "https://www.youtube.com/embed/DxZ_jwCEIh4"
type: "love"
singers:
  - Govind Vasantha
  - Swastika Swaminathan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kancha kattula kathal kovuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kancha kattula kathal kovuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ka ka ki kee ka ka ki kee ku koo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ka ka ki kee ka ka ki kee ku koo"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam kaathula kathal kekkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam kaathula kathal kekkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ke keh kai kai ko koh kou
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ke keh kai kai ko koh kou"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanam kathula kaichal kazhuthula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanam kathula kaichal kazhuthula"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaarananttha kaarananttha kaaranamttha kelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaarananttha kaarananttha kaaranamttha kelu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaajal kannula kikkaaki kikkaaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaajal kannula kikkaaki kikkaaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathellam kaa kaa ki kee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathellam kaa kaa ki kee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kapike kotta kannukkaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kapike kotta kannukkaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Katikitta kettikaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katikitta kettikaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathal kettak kaarak kovam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathal kettak kaarak kovam"/>
</div>
<div class="lyrico-lyrics-wrapper">Katta koodathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katta koodathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaattu kaattuk koonthal kaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattu kaattuk koonthal kaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaajukathilli kaadhukkaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaajukathilli kaadhukkaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathalikka kodi kelvi kekka koodathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathalikka kodi kelvi kekka koodathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalamellam kalamellam kathirukka kalamilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalamellam kalamellam kathirukka kalamilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathaakova keethaakavo kaami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathaakova keethaakavo kaami"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattarellam kattarellam kattalaiya keppathilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattarellam kattarellam kattalaiya keppathilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanathathum kekkathathum kaami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanathathum kekkathathum kaami"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaami kaami kaami kaami 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaami kaami kaami kaami "/>
</div>
<div class="lyrico-lyrics-wrapper">kaami kaami kaami kaami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaami kaami kaami kaami"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaami konjam kathal kaami kaamatchiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaami konjam kathal kaami kaamatchiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaami kaami kaami kaami 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaami kaami kaami kaami "/>
</div>
<div class="lyrico-lyrics-wrapper">kaami kaami kaami kaami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaami kaami kaami kaami"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaami konjam kathal kaami kaamatchiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaami konjam kathal kaami kaamatchiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaapik kotta kannukaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaapik kotta kannukaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Katikitta kettikaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katikitta kettikaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathal kettak kaarak kovam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathal kettak kaarak kovam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatta koodathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatta koodathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaattu kaattuk koonthal kaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattu kaattuk koonthal kaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaajukathilli kaadhukkaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaajukathilli kaadhukkaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathalikka kodi kelvi kekka koodathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathalikka kodi kelvi kekka koodathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey kavithayila kathaiyila thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey kavithayila kathaiyila thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanda kadhal karumanga keppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanda kadhal karumanga keppen"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavula thaan kanavula thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavula thaan kanavula thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Color koottai kattikitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Color koottai kattikitta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanakkula thaan kanakkula thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanakkula thaan kanakkula thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Killi killadi kedi kindu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Killi killadi kedi kindu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kiliyonna kanakku panni kirukkaanan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kiliyonna kanakku panni kirukkaanan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ka ka ki ki ku ku ke ke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ka ka ki ki ku ku ke ke"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai kai kai kai kai ko ko kow
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai kai kai kai kai ko ko kow"/>
</div>
<div class="lyrico-lyrics-wrapper">Ka ka ki ki ku ku ke ke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ka ka ki ki ku ku ke ke"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai kai ko ko kow
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai kai ko ko kow"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey kanna kaathil kiliya keechen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey kanna kaathil kiliya keechen"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuyila kooven ketha kelen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuyila kooven ketha kelen"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai kai kai kai kaiya koduda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai kai kai kai kaiya koduda"/>
</div>
<div class="lyrico-lyrics-wrapper">Kovakkaara kowthari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kovakkaara kowthari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaami kaami kaami kaami 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaami kaami kaami kaami "/>
</div>
<div class="lyrico-lyrics-wrapper">kaami kaami kaami kaami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaami kaami kaami kaami"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaami konjam kathal kaami kaamatchiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaami konjam kathal kaami kaamatchiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaami kaami kaami kaami 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaami kaami kaami kaami "/>
</div>
<div class="lyrico-lyrics-wrapper">kaami kaami kaami kaami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaami kaami kaami kaami"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaami konjam kathal kaami kaamatchiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaami konjam kathal kaami kaamatchiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaami kaami kaami kaami 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaami kaami kaami kaami "/>
</div>
<div class="lyrico-lyrics-wrapper">kaami kaami kaami kaami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaami kaami kaami kaami"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaami konjam kathal kaami kaamatchiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaami konjam kathal kaami kaamatchiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaami kaami kaami kaami 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaami kaami kaami kaami "/>
</div>
<div class="lyrico-lyrics-wrapper">kaami kaami kaami kaami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaami kaami kaami kaami"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaami konjam kathal kaami kaamatchiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaami konjam kathal kaami kaamatchiye"/>
</div>
</pre>
