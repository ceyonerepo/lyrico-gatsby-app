---
title: "uyire uyire song lyrics"
album: "Sollividava"
artist: "Jassie Gift"
lyricist: "Madhan Karky"
director: "Arjun Sarja"
path: "/albums/sollividava-lyrics"
song: "Uyire Uyire"
image: ../../images/albumart/sollividava.jpg
date: 2018-02-09
lang: tamil
youtubeLink: "https://www.youtube.com/embed/lz5DbNCGNOw"
type: "love"
singers:
  - G.V. Prakash Kumar
  - Harini
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Uyirae uyirae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirae uyirae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sila kodi kelvi ennil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sila kodi kelvi ennil"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirae uyirae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirae uyirae"/>
</div>
<div class="lyrico-lyrics-wrapper">Badhil yaavum yaavum unnil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Badhil yaavum yaavum unnil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru kaadu erigira podhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru kaadu erigira podhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sila pookkal thirapadhu yeno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sila pookkal thirapadhu yeno"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyirae uyirae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirae uyirae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sila kodi kelvi ennil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sila kodi kelvi ennil"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirae uyirae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirae uyirae"/>
</div>
<div class="lyrico-lyrics-wrapper">Badhil yaavum yaavum unnil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Badhil yaavum yaavum unnil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ohoooooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohoooooooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanneerin thuli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneerin thuli"/>
</div>
<div class="lyrico-lyrics-wrapper">Viduthalai ketkiratho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viduthalai ketkiratho"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharava uyirae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharava uyirae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yar nenjin vali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yar nenjin vali"/>
</div>
<div class="lyrico-lyrics-wrapper">En nenjil yeridutho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En nenjil yeridutho"/>
</div>
<div class="lyrico-lyrics-wrapper">Peravaa uyirae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peravaa uyirae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Desamae swaasamaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Desamae swaasamaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhegamae thiyagamaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhegamae thiyagamaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha deivangalin paadhangalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha deivangalin paadhangalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeraattathaan kanneero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeraattathaan kanneero"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirae uyirae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirae uyirae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sila kodi kelvi ennil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sila kodi kelvi ennil"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirae uyirae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirae uyirae"/>
</div>
<div class="lyrico-lyrics-wrapper">Badhil yaavum yaavum unnil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Badhil yaavum yaavum unnil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kattrengum pugai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattrengum pugai"/>
</div>
<div class="lyrico-lyrics-wrapper">En moochil narumanama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En moochil narumanama"/>
</div>
<div class="lyrico-lyrics-wrapper">Muraiya uyirae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muraiya uyirae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaayangal ennil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaayangal ennil"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaramal sugam tharudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaramal sugam tharudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pizhaya uyirae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pizhaya uyirae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veeranai saagavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veeranai saagavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kozhaiyaai vaazhavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kozhaiyaai vaazhavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhan rendukkum thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhan rendukkum thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Maiyyathilae yen ennai vaithayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maiyyathilae yen ennai vaithayo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyirae uyirae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirae uyirae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sila kodi kelvi ennil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sila kodi kelvi ennil"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirae uyirae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirae uyirae"/>
</div>
<div class="lyrico-lyrics-wrapper">Badhil yaavum yaavum unnil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Badhil yaavum yaavum unnil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Intha porum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha porum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudigiradhendru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudigiradhendru"/>
</div>
<div class="lyrico-lyrics-wrapper">En nenjam ganapathu yeno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En nenjam ganapathu yeno"/>
</div>
</pre>
