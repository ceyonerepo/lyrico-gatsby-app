---
title: "netri pottil song lyrics"
album: "Ko"
artist: "Harris Jayaraj"
lyricist: "Madhan Karky"
director: "K.V. Anand"
path: "/albums/ko-lyrics"
song: "Netri Pottil"
image: ../../images/albumart/ko.jpg
date: 2011-04-22
lang: tamil
youtubeLink: "https://www.youtube.com/embed/sXUqZ0Fidc8"
type: "mass"
singers:
  - Naresh Iyer
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Netri Pottil Patriya Ennam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netri Pottil Patriya Ennam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ettuthikkum parava Virumbum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettuthikkum parava Virumbum"/>
</div>
<div class="lyrico-lyrics-wrapper">Putrai Kattum Erumbugal Naangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Putrai Kattum Erumbugal Naangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Saththam Pottaal Ulagam Thirumbum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saththam Pottaal Ulagam Thirumbum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Netri Pottil Patriya Ennam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netri Pottil Patriya Ennam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ettuthikkum parava Virumbum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettuthikkum parava Virumbum"/>
</div>
<div class="lyrico-lyrics-wrapper">Putrai Kattum Erumbugal Naangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Putrai Kattum Erumbugal Naangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Saththam Pottaal Ulagam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saththam Pottaal Ulagam "/>
</div>
<div class="lyrico-lyrics-wrapper">Thirumbum Thirumbum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirumbum Thirumbum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen Piranthom Endre Irunthom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Piranthom Endre Irunthom"/>
</div>
<div class="lyrico-lyrics-wrapper">Kan Thiranthom Avvaan Paranthom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan Thiranthom Avvaan Paranthom"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye Maatram Thediye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Maatram Thediye"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinam Oru Netrai Thorkirom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam Oru Netrai Thorkirom"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetru Paathaiyil Boomi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetru Paathaiyil Boomi "/>
</div>
<div class="lyrico-lyrics-wrapper">Suththa Paarkirom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suththa Paarkirom"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhaketrum Sudar 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhaketrum Sudar "/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatraai Selvome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatraai Selvome"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Netri Pottil Patriya Ennam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netri Pottil Patriya Ennam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ettuthikkum parava Virumbum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettuthikkum parava Virumbum"/>
</div>
<div class="lyrico-lyrics-wrapper">Putrai Kattum Erumbugal Naangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Putrai Kattum Erumbugal Naangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Saththam Pottaal Ulagam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saththam Pottaal Ulagam "/>
</div>
<div class="lyrico-lyrics-wrapper">Thirumbum Thirumbum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirumbum Thirumbum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen Piranthom Endre Irunthom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Piranthom Endre Irunthom"/>
</div>
<div class="lyrico-lyrics-wrapper">Kan Thiranthom Avvaan Paranthom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan Thiranthom Avvaan Paranthom"/>
</div>
<div class="lyrico-lyrics-wrapper">Cafe Beachilum Kanavile Kottai Kattinom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cafe Beachilum Kanavile Kottai Kattinom"/>
</div>
<div class="lyrico-lyrics-wrapper">Facebook Wallilum Engal Kolgai Theettinom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Facebook Wallilum Engal Kolgai Theettinom"/>
</div>
<div class="lyrico-lyrics-wrapper">Inainthome Munainthome Paarpome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inainthome Munainthome Paarpome"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Cafe Beachilum Kanavile Kottai Kattinom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cafe Beachilum Kanavile Kottai Kattinom"/>
</div>
<div class="lyrico-lyrics-wrapper">Facebook Wallilum Engal Kolgai Theettinom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Facebook Wallilum Engal Kolgai Theettinom"/>
</div>
<div class="lyrico-lyrics-wrapper">Inainthome Munainthome Paarpomae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inainthome Munainthome Paarpomae"/>
</div>
</pre>
