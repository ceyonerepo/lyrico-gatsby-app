---
title: "kooduvittu aavi ponathe song lyrics"
album: "Aelay"
artist: "Kaber Vasuki - Aruldev"
lyricist: "K. Chitrasenan"
director: "Halitha Shameem"
path: "/albums/aelay-song-lyrics"
song: "Kooduvittu Aavi Ponathe"
image: ../../images/albumart/aelay.jpg
date: 2021-002-28
lang: tamil
youtubeLink: "https://www.youtube.com/embed/n1Q6e9b-JNk"
type: "Sad"
singers:
  - K. Chitrasenan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">kooduvittu aavi ponathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kooduvittu aavi ponathe"/>
</div>
<div class="lyrico-lyrics-wrapper">un pirivai enni kudumbamellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un pirivai enni kudumbamellam"/>
</div>
<div class="lyrico-lyrics-wrapper">kumuri aluguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kumuri aluguthe"/>
</div>
<div class="lyrico-lyrics-wrapper">kooduvittu aavi ponathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kooduvittu aavi ponathe"/>
</div>
<div class="lyrico-lyrics-wrapper">un pirivai enni kudumbamellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un pirivai enni kudumbamellam"/>
</div>
<div class="lyrico-lyrics-wrapper">kumuri aluguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kumuri aluguthe"/>
</div>
<div class="lyrico-lyrics-wrapper">kumuri aluguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kumuri aluguthe"/>
</div>
<div class="lyrico-lyrics-wrapper">naanum inge neeyum enge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naanum inge neeyum enge"/>
</div>
<div class="lyrico-lyrics-wrapper">uruvam inge usurum enge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uruvam inge usurum enge"/>
</div>
<div class="lyrico-lyrics-wrapper">unnidan oru naal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnidan oru naal"/>
</div>
<div class="lyrico-lyrics-wrapper">kooduvom ange
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kooduvom ange"/>
</div>
<div class="lyrico-lyrics-wrapper">unnai kaanave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai kaanave"/>
</div>
<div class="lyrico-lyrics-wrapper">seruvom ange
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seruvom ange"/>
</div>
<div class="lyrico-lyrics-wrapper">unnai kaanave 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai kaanave "/>
</div>
<div class="lyrico-lyrics-wrapper">seruvom ange
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seruvom ange"/>
</div>
<div class="lyrico-lyrics-wrapper">kooduvittu aavi ponathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kooduvittu aavi ponathe"/>
</div>
<div class="lyrico-lyrics-wrapper">un pirivai enni kudumbamellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un pirivai enni kudumbamellam"/>
</div>
<div class="lyrico-lyrics-wrapper">kumuri aluguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kumuri aluguthe"/>
</div>
<div class="lyrico-lyrics-wrapper">kumuri aluguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kumuri aluguthe"/>
</div>
<div class="lyrico-lyrics-wrapper">ice vikka ponaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ice vikka ponaai"/>
</div>
<div class="lyrico-lyrics-wrapper">ice aaga aanaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ice aaga aanaai"/>
</div>
<div class="lyrico-lyrics-wrapper">asaiyatha angam kandu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="asaiyatha angam kandu"/>
</div>
<div class="lyrico-lyrics-wrapper">alugirom thanaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alugirom thanaai"/>
</div>
<div class="lyrico-lyrics-wrapper">un pol ullaam kondor
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un pol ullaam kondor"/>
</div>
<div class="lyrico-lyrics-wrapper">oorukul illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oorukul illai"/>
</div>
<div class="lyrico-lyrics-wrapper">un mugam thediye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un mugam thediye"/>
</div>
<div class="lyrico-lyrics-wrapper">paaduthu un pillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paaduthu un pillai"/>
</div>
<div class="lyrico-lyrics-wrapper">un mugam thediye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un mugam thediye"/>
</div>
<div class="lyrico-lyrics-wrapper">paaduthu un pillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paaduthu un pillai"/>
</div>
<div class="lyrico-lyrics-wrapper">kooduvittu aavi ponathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kooduvittu aavi ponathe"/>
</div>
<div class="lyrico-lyrics-wrapper">un pirivai enni kudumbamellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un pirivai enni kudumbamellam"/>
</div>
<div class="lyrico-lyrics-wrapper">kumuri aluguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kumuri aluguthe"/>
</div>
<div class="lyrico-lyrics-wrapper">kumuri aluguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kumuri aluguthe"/>
</div>
<div class="lyrico-lyrics-wrapper">kumuri aluguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kumuri aluguthe"/>
</div>
</pre>
