---
title: "you and me song lyrics"
album: "Kilometers and Kilometers"
artist: "Sooraj S. Kurup"
lyricist: "Sooraj S. Kurup, Vinayak Sasikumar"
director: "Jeo Baby"
path: "/albums/kilometers-and kilometers-lyrics"
song: "You And Me"
image: ../../images/albumart/kilometers-and-kilometers.jpg
date: 2020-08-31
lang: malayalam
youtubeLink: "https://www.youtube.com/embed/zc17-C56M-8"
type: "happy"
singers:
  - Sooraj S. Kurup
  - Neethu Naduvathettu
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vaazhve Nee Enna Polave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhve Nee Enna Polave"/>
</div>
<div class="lyrico-lyrics-wrapper">nizhal pole koode njaanitha..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nizhal pole koode njaanitha.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaane Mounam Pularnnuvo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaane Mounam Pularnnuvo"/>
</div>
<div class="lyrico-lyrics-wrapper">Marakkan Manasse Mozhinjuvo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marakkan Manasse Mozhinjuvo"/>
</div>
<div class="lyrico-lyrics-wrapper">Paade Njaninnerinjuvo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paade Njaninnerinjuvo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kithappaarnnu Shwasam Thengiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kithappaarnnu Shwasam Thengiyo"/>
</div>
</pre>
