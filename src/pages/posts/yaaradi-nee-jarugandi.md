---
title: "yaaradi nee song lyrics"
album: "Jarugandi"
artist: "Bobo Shashi"
lyricist: "Uma Devi"
director: "AN Pitchumani"
path: "/albums/jarugandi-lyrics"
song: "Yaaradi Nee"
image: ../../images/albumart/jarugandi.jpg
date: 2018-10-26
lang: tamil
youtubeLink: "https://www.youtube.com/embed/PvmdxKeBxg0"
type: "love"
singers:
  - Yuvan Shankar Raja
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yaaradi nee Naanaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaradi nee Naanaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi kaalam kaiyil varumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi kaalam kaiyil varumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaanaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanaaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pogum vazhi Poovaaga ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogum vazhi Poovaaga ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini kaatril mithappen naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini kaatril mithappen naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Noolaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Noolaaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaarovaaga ninaithen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarovaaga ninaithen"/>
</div>
<div class="lyrico-lyrics-wrapper">Poove unakkai kaninthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poove unakkai kaninthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Theevil naduvil maramaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theevil naduvil maramaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan iruppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan iruppen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oyvaai paravai irukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oyvaai paravai irukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Uravaai siragai virikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uravaai siragai virikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Aazhakadalil padaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aazhakadalil padaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Iravin nilavu nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravin nilavu nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pudhu pudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu pudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhaigalai alli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhaigalai alli"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhaigalum kulithidumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhaigalum kulithidumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uravindri valargindra vembil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uravindri valargindra vembil"/>
</div>
<div class="lyrico-lyrics-wrapper">Panai vanthu mulaithidumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panai vanthu mulaithidumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Suda suda sooriyan kandu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suda suda sooriyan kandu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagamum thulirthidumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagamum thulirthidumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nagargindra megangal pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nagargindra megangal pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuyarangal nagarnthidumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuyarangal nagarnthidumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh ooh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh ooh"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanathin thaaragai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanathin thaaragai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaasal thedi vanthathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaasal thedi vanthathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaathaigal thooramai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaathaigal thooramai"/>
</div>
<div class="lyrico-lyrics-wrapper">Oodi poguthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oodi poguthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi thaagangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi thaagangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Theernthu nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theernthu nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Sainthu kollum velaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sainthu kollum velaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoothanai polvae vanthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoothanai polvae vanthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaangi kollavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaangi kollavae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannukkul achangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannukkul achangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Neengattumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neengattumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Katrinil thendralum neenthattumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katrinil thendralum neenthattumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nammodu kaalangal serattumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nammodu kaalangal serattumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ondrodu ondraaga maarattumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondrodu ondraaga maarattumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru vazhi thirakkindra pothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru vazhi thirakkindra pothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maru vazhi adaikindratheno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maru vazhi adaikindratheno"/>
</div>
<div class="lyrico-lyrics-wrapper">Maru vazhi adaikindra pothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maru vazhi adaikindra pothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru vazhi thiranthidum thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru vazhi thiranthidum thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiranthidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiranthidum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaaradi nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaradi nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaradi nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaradi nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaradi nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaradi nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaradi nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaradi nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee nee nee 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee nee nee "/>
</div>
<div class="lyrico-lyrics-wrapper">nee nee nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee nee nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pudhu pudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu pudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhaigalai alli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhaigalai alli"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhaigalum kulithidumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhaigalum kulithidumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uravindri valargindra vembil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uravindri valargindra vembil"/>
</div>
<div class="lyrico-lyrics-wrapper">Panai vanthu mulaithidumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panai vanthu mulaithidumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Suda suda sooriyan kandu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suda suda sooriyan kandu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagamum thulirthidumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagamum thulirthidumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nagargindra megangal pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nagargindra megangal pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuyarangal nagarnthidumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuyarangal nagarnthidumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaaradi nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaradi nee"/>
</div>
</pre>
