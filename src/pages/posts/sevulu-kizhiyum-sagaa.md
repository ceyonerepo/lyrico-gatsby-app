---
title: "sevulu kizhiyum song lyrics"
album: "Sagaa"
artist: "Shabir"
lyricist: "Shabir"
director: "Murugesh"
path: "/albums/sagaa-lyrics"
song: "Sevulu Kizhiyum"
image: ../../images/albumart/sagaa.jpg
date: 2019-02-01
lang: tamil
youtubeLink: "https://www.youtube.com/embed/HFPBNqhCBYQ"
type: "intro"
singers:
  - Shabir ft Dr Burn
  - Rudra
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">bam bam bam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bam bam bam"/>
</div>
<div class="lyrico-lyrics-wrapper">sevulu kizhiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sevulu kizhiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">dagalu odaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dagalu odaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">enga pakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga pakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">vambiruntha vanee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vambiruntha vanee"/>
</div>
<div class="lyrico-lyrics-wrapper">vanthavellaam gaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanthavellaam gaali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">bam bam bam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bam bam bam"/>
</div>
<div class="lyrico-lyrics-wrapper">sevulu kizhiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sevulu kizhiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">dagalu odaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dagalu odaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">enga pakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga pakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">vambiruntha vanee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vambiruntha vanee"/>
</div>
<div class="lyrico-lyrics-wrapper">vanthavellaam gaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanthavellaam gaali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nan khandiduan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan khandiduan"/>
</div>
<div class="lyrico-lyrics-wrapper">venritalam konthiduan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="venritalam konthiduan"/>
</div>
<div class="lyrico-lyrics-wrapper">nallathellam cholliduan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nallathellam cholliduan"/>
</div>
<div class="lyrico-lyrics-wrapper">sonnataiye seyvadanal than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sonnataiye seyvadanal than"/>
</div>
<div class="lyrico-lyrics-wrapper">iravai ravinil atindiduyne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iravai ravinil atindiduyne"/>
</div>
<div class="lyrico-lyrics-wrapper">emene yemarhiduyne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="emene yemarhiduyne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pore dodangitalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pore dodangitalam"/>
</div>
<div class="lyrico-lyrics-wrapper">manual painalil sayvatu yaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manual painalil sayvatu yaar"/>
</div>
<div class="lyrico-lyrics-wrapper">irutill yaar mudire matiduvatu nanthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irutill yaar mudire matiduvatu nanthan"/>
</div>
<div class="lyrico-lyrics-wrapper">unaku uirbichi koduthiduyne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unaku uirbichi koduthiduyne"/>
</div>
<div class="lyrico-lyrics-wrapper">ganakqup padavi naan chaytiduvene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ganakqup padavi naan chaytiduvene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">micham meedi vittu vaipadilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="micham meedi vittu vaipadilla"/>
</div>
<div class="lyrico-lyrics-wrapper">endhan valakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="endhan valakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">muchu nikkum varai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muchu nikkum varai"/>
</div>
<div class="lyrico-lyrics-wrapper">irukkil kolvatu palakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irukkil kolvatu palakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">qati vaikka gairill wal quti nanada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="qati vaikka gairill wal quti nanada"/>
</div>
<div class="lyrico-lyrics-wrapper">verttie unnai vellri kannum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="verttie unnai vellri kannum "/>
</div>
<div class="lyrico-lyrics-wrapper">varai pulitanada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varai pulitanada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nagara vettai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nagara vettai"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu dravida koattai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu dravida koattai"/>
</div>
<div class="lyrico-lyrics-wrapper">atharum veechu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="atharum veechu"/>
</div>
<div class="lyrico-lyrics-wrapper">thenarum moochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thenarum moochu"/>
</div>
<div class="lyrico-lyrics-wrapper">therinji pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="therinji pochu"/>
</div>
<div class="lyrico-lyrics-wrapper">ini enna pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ini enna pochu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nagara vettai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nagara vettai"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu dravida koattai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu dravida koattai"/>
</div>
<div class="lyrico-lyrics-wrapper">atharum veechu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="atharum veechu"/>
</div>
<div class="lyrico-lyrics-wrapper">thenarum moochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thenarum moochu"/>
</div>
<div class="lyrico-lyrics-wrapper">therinji pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="therinji pochu"/>
</div>
<div class="lyrico-lyrics-wrapper">ini enna pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ini enna pochu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">bam bam bam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bam bam bam"/>
</div>
<div class="lyrico-lyrics-wrapper">sevulu kizhiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sevulu kizhiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">dagalu odaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dagalu odaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">enga pakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga pakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">vambiruntha vanee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vambiruntha vanee"/>
</div>
<div class="lyrico-lyrics-wrapper">vanthavellaam gaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanthavellaam gaali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">bam bam bam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bam bam bam"/>
</div>
<div class="lyrico-lyrics-wrapper">sevulu kizhiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sevulu kizhiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">dagalu odaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dagalu odaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">enga pakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga pakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">vambiruntha vanee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vambiruntha vanee"/>
</div>
<div class="lyrico-lyrics-wrapper">vanthavellaam gaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanthavellaam gaali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nan khandiduan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan khandiduan"/>
</div>
<div class="lyrico-lyrics-wrapper">venritalam konthiduan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="venritalam konthiduan"/>
</div>
<div class="lyrico-lyrics-wrapper">nallathellam cholliduan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nallathellam cholliduan"/>
</div>
<div class="lyrico-lyrics-wrapper">sonnataiye seyvadanal than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sonnataiye seyvadanal than"/>
</div>
<div class="lyrico-lyrics-wrapper">iravai ravinil atindiduyne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iravai ravinil atindiduyne"/>
</div>
<div class="lyrico-lyrics-wrapper">emene yemarhiduyne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="emene yemarhiduyne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pore dodangitalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pore dodangitalam"/>
</div>
<div class="lyrico-lyrics-wrapper">manual painalil sayvatu yaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manual painalil sayvatu yaar"/>
</div>
<div class="lyrico-lyrics-wrapper">irutill yaar mudire matiduvatu nanthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irutill yaar mudire matiduvatu nanthan"/>
</div>
<div class="lyrico-lyrics-wrapper">unaku uirbichi koduthiduyne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unaku uirbichi koduthiduyne"/>
</div>
<div class="lyrico-lyrics-wrapper">ganakqup padavi naan chaytiduvene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ganakqup padavi naan chaytiduvene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">micham meedi vittu vaipadilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="micham meedi vittu vaipadilla"/>
</div>
<div class="lyrico-lyrics-wrapper">endhan valakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="endhan valakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">muchu nikkum varai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muchu nikkum varai"/>
</div>
<div class="lyrico-lyrics-wrapper">irukkil kolvatu palakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irukkil kolvatu palakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">qati vaikka gairill wal quti nanada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="qati vaikka gairill wal quti nanada"/>
</div>
<div class="lyrico-lyrics-wrapper">verttie unnai vellri kannum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="verttie unnai vellri kannum "/>
</div>
<div class="lyrico-lyrics-wrapper">varai pulitanada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varai pulitanada"/>
</div>
</pre>
