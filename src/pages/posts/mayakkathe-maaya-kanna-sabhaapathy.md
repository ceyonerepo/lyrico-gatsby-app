---
title: "mayakkathe maaya kanna song lyrics"
album: "Sabhaapathy"
artist: "Sam C. S"
lyricist: "Vivek"
director: "R. Srinivasa Rao"
path: "/albums/sabhaapathy-lyrics"
song: "Mayakkathe Maaya Kanna"
image: ../../images/albumart/sabhaapathy.jpg
date: 2021-11-19
lang: tamil
youtubeLink: "https://www.youtube.com/embed/wG8QluIncrA"
type: "love"
singers:
  - Srinisha Jayaseelan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Mayakkadhae maaya kanna nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayakkadhae maaya kanna nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayakkadhae maaya kanna nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayakkadhae maaya kanna nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mayakkadhae maaya kanna nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayakkadhae maaya kanna nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayakkadhae maaya kannaal nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayakkadhae maaya kannaal nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Maraikkaadhae maaya kannaal nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maraikkaadhae maaya kannaal nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayakkadhae neeee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayakkadhae neeee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Orr oonjal varumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orr oonjal varumo"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum adhilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum adhilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadi varalaamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadi varalaamo"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kola kuzhalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kola kuzhalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum adhile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum adhile"/>
</div>
<div class="lyrico-lyrics-wrapper">Neendhi varalaamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neendhi varalaamo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sriranga nayaganae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sriranga nayaganae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oranga nadagamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oranga nadagamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaar vanna thaeraaga va va va va
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaar vanna thaeraaga va va va va"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayargal koodi vara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayargal koodi vara"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaraaro padi vara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraaro padi vara"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai alli thaalaatta vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai alli thaalaatta vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mayakkadhae maaya kanna nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayakkadhae maaya kanna nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayakkadhae maaya kanna nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayakkadhae maaya kanna nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaa.aaa..Haa.aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaa.aaa..Haa.aaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Marappachi ivalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marappachi ivalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Un manam than goluva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un manam than goluva"/>
</div>
<div class="lyrico-lyrics-wrapper">Padiyil azhagaai amarndhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padiyil azhagaai amarndhen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mayil thogai ivalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayil thogai ivalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Un uruvae mayilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un uruvae mayilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Udanae asaindhen isaindhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udanae asaindhen isaindhen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unaiyae naan serum varai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unaiyae naan serum varai"/>
</div>
<div class="lyrico-lyrics-wrapper">Agalaadhu asaiyaadhu ival aasai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Agalaadhu asaiyaadhu ival aasai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirudiya vennai pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirudiya vennai pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai sera vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai sera vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mayakkadhae maaya kanna nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayakkadhae maaya kanna nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayakkadhae maaya kannaal nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayakkadhae maaya kannaal nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Maraikkaadhae maaya kannaal nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maraikkaadhae maaya kannaal nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayakkadhae nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayakkadhae nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sriranga nayaganae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sriranga nayaganae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oranga nadagamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oranga nadagamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaar vanna thaeraaga va va va va
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaar vanna thaeraaga va va va va"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayargal koodi vara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayargal koodi vara"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaraaro padi vara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraaro padi vara"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai alli thaalaatta vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai alli thaalaatta vaa"/>
</div>
</pre>
