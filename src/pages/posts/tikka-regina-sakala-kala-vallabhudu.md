---
title: "tikka regina song lyrics"
album: "Sakala Kala Vallabhudu"
artist: "Ajay Patnaik"
lyricist: "Giridhar Naidu"
director: "Shiva Ganesh"
path: "/albums/sakala-kala-vallabhudu-lyrics"
song: "Tikka Regina"
image: ../../images/albumart/sakala-kala-vallabhudu.jpg
date: 2019-02-01
lang: telugu
youtubeLink: "https://www.youtube.com/embed/g4c3rukcL3E"
type: "happy"
singers:
  - Geetha Madhuri
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Thikka Regina Vankaragaallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thikka Regina Vankaragaallu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mattikottina Pokiri Veellu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mattikottina Pokiri Veellu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sacchugakkurey Chillaragallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sacchugakkurey Chillaragallu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pettukuntey Yevvadaina Vadalaru Vellu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pettukuntey Yevvadaina Vadalaru Vellu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Naa Rakalugaa Tryschesi Baasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Naa Rakalugaa Tryschesi Baasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mateh Ayyaruga Ee Mass Herosu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mateh Ayyaruga Ee Mass Herosu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaatey Theeserakam Scenestey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaatey Theeserakam Scenestey"/>
</div>
<div class="lyrico-lyrics-wrapper">Greesu Chelladhu Dadelake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Greesu Chelladhu Dadelake"/>
</div>
<div class="lyrico-lyrics-wrapper">Koditey Ika Freeze
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koditey Ika Freeze"/>
</div>
<div class="lyrico-lyrics-wrapper">Neraalu Ghoralu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neraalu Ghoralu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Neraalu Ghoralu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Neraalu Ghoralu"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheseti Manavvallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheseti Manavvallu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kinda Meeda Paddarandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kinda Meeda Paddarandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nkaa Yedho Undo Enti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nkaa Yedho Undo Enti"/>
</div>
<div class="lyrico-lyrics-wrapper">Sakala Kala Vallabulu Veerenayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sakala Kala Vallabulu Veerenayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhunu Chusi Namametti Govindayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhunu Chusi Namametti Govindayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Sakala Kala Vallabulu Veerenayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sakala Kala Vallabulu Veerenayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Ekanesi Konda Laagey Thingaralayyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ekanesi Konda Laagey Thingaralayyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mee Naa maaluu Maaksale Vaddhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mee Naa maaluu Maaksale Vaddhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo.. Naa maaluu Nekpinchoddu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo.. Naa maaluu Nekpinchoddu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Aavesam Balagaale Vaddu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Aavesam Balagaale Vaddu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedaalosthe Padata Raddu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedaalosthe Padata Raddu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sketchulesthe Fix Inka Feelavvarugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sketchulesthe Fix Inka Feelavvarugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sixerayina Bould Ayina Aatodalaruga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sixerayina Bould Ayina Aatodalaruga"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattukunte Musalipattu Ika Saramegaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattukunte Musalipattu Ika Saramegaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Votaminka Oppukoru Vodilellarugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Votaminka Oppukoru Vodilellarugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aakegiri Mullumeeda Padinaa Raama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aakegiri Mullumeeda Padinaa Raama"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey Mullochhi Mullochhi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey Mullochhi Mullochhi "/>
</div>
<div class="lyrico-lyrics-wrapper">Aakumeeda Padina Maama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aakumeeda Padina Maama"/>
</div>
<div class="lyrico-lyrics-wrapper">Chirigedi Aakera Vinara Vemaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chirigedi Aakera Vinara Vemaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Irukupatti Pottelu Kondanu Kotti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irukupatti Pottelu Kondanu Kotti"/>
</div>
<div class="lyrico-lyrics-wrapper">Gelupu tecchu kuntundaa dane Padagotti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gelupu tecchu kuntundaa dane Padagotti"/>
</div>
<div class="lyrico-lyrics-wrapper">Mogudu Chacchi Mundemo Mulana Padithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mogudu Chacchi Mundemo Mulana Padithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Ranku Mogudu Visirada Rayini Kotti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ranku Mogudu Visirada Rayini Kotti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sakala Kala Vallabulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sakala Kala Vallabulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vankara Gaallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vankara Gaallu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sakala Kala Vallabulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sakala Kala Vallabulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chemchagaallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chemchagaallu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sakala Kala Vallabulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sakala Kala Vallabulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thimmiri Gaallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thimmiri Gaallu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sakala Kala Vallabulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sakala Kala Vallabulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vadalaru Veellu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadalaru Veellu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mee Naa Meshala Lekkaloddu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mee Naa Meshala Lekkaloddu"/>
</div>
<div class="lyrico-lyrics-wrapper">Poo te Vesthe Padaleraddu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poo te Vesthe Padaleraddu"/>
</div>
<div class="lyrico-lyrics-wrapper">Saadaa Seedaa Vetakara loddu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saadaa Seedaa Vetakara loddu"/>
</div>
<div class="lyrico-lyrics-wrapper">Padina Lechi Padaraa Moddu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padina Lechi Padaraa Moddu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ataka Meeda Pidatha Goddu Rakame Kada Raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ataka Meeda Pidatha Goddu Rakame Kada Raa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallu Moosi Nalla Pilli Milkesey Raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallu Moosi Nalla Pilli Milkesey Raa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanchea Velli Chenu MEsey Gang Eh Shururaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanchea Velli Chenu MEsey Gang Eh Shururaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Erri Mokham Elaga Bette Facelu Meerasaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Erri Mokham Elaga Bette Facelu Meerasaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thandaanaa Thaana Kathalu Vinaraa Kannaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandaanaa Thaana Kathalu Vinaraa Kannaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Entha Unna Andariki Dhoola Teerchey Nanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entha Unna Andariki Dhoola Teerchey Nanna"/>
</div>
<div class="lyrico-lyrics-wrapper">EnkkiPelli Subbi Chaavu Vacchinattugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="EnkkiPelli Subbi Chaavu Vacchinattugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Illu Kaali Okadunte Peekinattugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illu Kaali Okadunte Peekinattugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Chutta Thisukocchi Okadu Nippunadigenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chutta Thisukocchi Okadu Nippunadigenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dongamunda Pellikemo Chavumelamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dongamunda Pellikemo Chavumelamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Emi Theliyanillakemo Intha Gooramaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emi Theliyanillakemo Intha Gooramaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sakala Kala Vallabulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sakala Kala Vallabulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dopidigallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dopidigallu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sakala Kala Vallabulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sakala Kala Vallabulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Danger Veellu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Danger Veellu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sakala Kala Vallabulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sakala Kala Vallabulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pandem Kollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pandem Kollu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sakala Kala Vallabulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sakala Kala Vallabulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thelaru Veelluu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thelaru Veelluu"/>
</div>
</pre>
