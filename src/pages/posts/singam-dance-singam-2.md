---
title: "singam dance song lyrics"
album: "Singam II"
artist: "Devi Sri Prasad"
lyricist: "Viveka"
director: "Hari Gopalakrishnan"
path: "/albums/singam-2-lyrics"
song: "Singam Dance"
image: ../../images/albumart/singam-2.jpg
date: 2013-07-05
lang: tamil
youtubeLink: "https://www.youtube.com/embed/KnuTVez_v-k"
type: "Love"
singers:
  - Devi Sri Prasad
  - Baba Sehgal
  - Sharmila
  - Geetha Madhuri
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yoo This Is The Chance 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yoo This Is The Chance "/>
</div>
<div class="lyrico-lyrics-wrapper">Lets Do Singam Dance
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lets Do Singam Dance"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Everybody Put Your Handsup And Say
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Everybody Put Your Handsup And Say"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Welcome To Valantine’s Day Do The Roll And Pump
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Welcome To Valantine’s Day Do The Roll And Pump"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Up The Beat On The Floor And Say Repeat Repeat
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Up The Beat On The Floor And Say Repeat Repeat"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Repeat Repeat
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Repeat Repeat"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Repeat Aahaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Repeat Aahaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Everybody Put Your Handsup And Say
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Everybody Put Your Handsup And Say"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Welcome To Valantine’s Day This Is The Day For
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Welcome To Valantine’s Day This Is The Day For"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Romance Baby So
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romance Baby So"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lets Sing And Dance Its Singam Dance 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lets Sing And Dance Its Singam Dance "/>
</div>
<div class="lyrico-lyrics-wrapper">Its Singam Dance
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Its Singam Dance"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hoi Sunday Monday Tuesday
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoi Sunday Monday Tuesday"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Wednesday Thursday Friday Saturday
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wednesday Thursday Friday Saturday"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ettaam Naalaai Vendum Vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettaam Naalaai Vendum Vendum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Weekly Oru Valantine’s Day
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Weekly Oru Valantine’s Day"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Kaadhal Ennaku Muthikichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Kaadhal Ennaku Muthikichu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muthikichu Happy Bp Thothikichu Thothikichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthikichu Happy Bp Thothikichu Thothikichu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Love Ezhuthukulla Ellaam Irukiradhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love Ezhuthukulla Ellaam Irukiradhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lets Sing And Dance Its Singam Dance
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lets Sing And Dance Its Singam Dance"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lets Sing And Dance Its Singam Dance
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lets Sing And Dance Its Singam Dance"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lets Sing And Dance
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lets Sing And Dance"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Summer Winter Spring And Autumn
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summer Winter Spring And Autumn"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ovvoru Naatilum Change Aagum Kaadhal Pookum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ovvoru Naatilum Change Aagum Kaadhal Pookum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Season Mattum Ella Oorilum Ondraagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Season Mattum Ella Oorilum Ondraagum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Eyes Um Eye Um Ottikichu Ottikichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Eyes Um Eye Um Ottikichu Ottikichu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Heartum Heartum Muttukichu Muttukichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Heartum Heartum Muttukichu Muttukichu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhage Unnai Thedi Vandhen I Love U Sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhage Unnai Thedi Vandhen I Love U Sollu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">I Love You
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Love You"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lets Sing And Dance Hey Its Singam Dance
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lets Sing And Dance Hey Its Singam Dance"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lets Sing And Dance Its Singam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lets Sing And Dance Its Singam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dance Lets Sing And Dance
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dance Lets Sing And Dance"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Everybody Put Your Handsup And Say
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Everybody Put Your Handsup And Say"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Welcome To valantine’s Day Do The Roll And Pump
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Welcome To valantine’s Day Do The Roll And Pump"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Up The Beat On The Floor And Say Repeat
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Up The Beat On The Floor And Say Repeat"/>
</div>
</pre>
