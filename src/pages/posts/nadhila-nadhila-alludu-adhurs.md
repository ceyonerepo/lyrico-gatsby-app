---
title: "nadhila nadhila song lyrics"
album: "Alludu Adhurs"
artist: "Devi Sri Prasad"
lyricist: "Sri Mani"
director: "Santosh Srinivas"
path: "/albums/alludu-adhurs-lyrics"
song: "Nadhila Nadhila Nadhila"
image: ../../images/albumart/alludu-adhurs.jpg
date: 2021-01-14
lang: telugu
youtubeLink: "https://www.youtube.com/embed/8io0LWqAl_k"
type: "love"
singers:
  - Sagar
  - Haripriya
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nadhila nadhila nadhila 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadhila nadhila nadhila "/>
</div>
<div class="lyrico-lyrics-wrapper">kadhilave oo nadhila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhilave oo nadhila"/>
</div>
<div class="lyrico-lyrics-wrapper">Alalaa alalaa alalaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alalaa alalaa alalaa "/>
</div>
<div class="lyrico-lyrics-wrapper">thadisave nannu ila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thadisave nannu ila"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalalaa kalalaa kalalaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalalaa kalalaa kalalaa "/>
</div>
<div class="lyrico-lyrics-wrapper">kalisave oo kalalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalisave oo kalalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathala kathala kathala 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathala kathala kathala "/>
</div>
<div class="lyrico-lyrics-wrapper">maarave naa kathala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maarave naa kathala"/>
</div>
<div class="lyrico-lyrics-wrapper">Chaka chakamantu naa manasepudu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chaka chakamantu naa manasepudu "/>
</div>
<div class="lyrico-lyrics-wrapper">parigeduthundhe nee vaipe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="parigeduthundhe nee vaipe"/>
</div>
<div class="lyrico-lyrics-wrapper">Taka takamantu naa madhikepudu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Taka takamantu naa madhikepudu "/>
</div>
<div class="lyrico-lyrics-wrapper">vinabaduthundhe nee pilupe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vinabaduthundhe nee pilupe"/>
</div>
<div class="lyrico-lyrics-wrapper">Repa repalade kanu reppalalo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Repa repalade kanu reppalalo "/>
</div>
<div class="lyrico-lyrics-wrapper">merisedhepudu nee roope
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="merisedhepudu nee roope"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa ooprike pranam ante nee chupe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa ooprike pranam ante nee chupe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nadhila nadhila nadhila 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadhila nadhila nadhila "/>
</div>
<div class="lyrico-lyrics-wrapper">kadhilave oo nadhila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhilave oo nadhila"/>
</div>
<div class="lyrico-lyrics-wrapper">Alalaa alalaa alalaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alalaa alalaa alalaa "/>
</div>
<div class="lyrico-lyrics-wrapper">thadisave nannu ila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thadisave nannu ila"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalalaa kalalaa kalalaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalalaa kalalaa kalalaa "/>
</div>
<div class="lyrico-lyrics-wrapper">kalisave oo kalalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalisave oo kalalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathala kathala kathala 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathala kathala kathala "/>
</div>
<div class="lyrico-lyrics-wrapper">maarave naa kathala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maarave naa kathala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andham lo nuvu hanikaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andham lo nuvu hanikaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Chusthene champe metthani 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chusthene champe metthani "/>
</div>
<div class="lyrico-lyrics-wrapper">katthe nadum ompe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="katthe nadum ompe"/>
</div>
<div class="lyrico-lyrics-wrapper">Andharilo nuvu kottha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andharilo nuvu kottha "/>
</div>
<div class="lyrico-lyrics-wrapper">rakam maikamlo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rakam maikamlo"/>
</div>
<div class="lyrico-lyrics-wrapper">Munche maatalu hammo vinasompe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munche maatalu hammo vinasompe"/>
</div>
<div class="lyrico-lyrics-wrapper">Mallela theeganu alluku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mallela theeganu alluku "/>
</div>
<div class="lyrico-lyrics-wrapper">perigina roja puvvuvi nuvvele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="perigina roja puvvuvi nuvvele"/>
</div>
<div class="lyrico-lyrics-wrapper">Vennela kallunu vennuga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vennela kallunu vennuga "/>
</div>
<div class="lyrico-lyrics-wrapper">pusenu nee chirunavvele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pusenu nee chirunavvele"/>
</div>
<div class="lyrico-lyrics-wrapper">Siggula santhanu buggana 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siggula santhanu buggana "/>
</div>
<div class="lyrico-lyrics-wrapper">chutti ennalani ooristhave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chutti ennalani ooristhave"/>
</div>
<div class="lyrico-lyrics-wrapper">Repo mapo neeke sontham chesthale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Repo mapo neeke sontham chesthale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nadhila nadhila nadhila 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadhila nadhila nadhila "/>
</div>
<div class="lyrico-lyrics-wrapper">kadhilave oo nadhila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhilave oo nadhila"/>
</div>
<div class="lyrico-lyrics-wrapper">Alalaa alalaa alalaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alalaa alalaa alalaa "/>
</div>
<div class="lyrico-lyrics-wrapper">thadisave nannu ila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thadisave nannu ila"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalalaa kalalaa kalalaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalalaa kalalaa kalalaa "/>
</div>
<div class="lyrico-lyrics-wrapper">kalisave oo kalalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalisave oo kalalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathala kathala kathala 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathala kathala kathala "/>
</div>
<div class="lyrico-lyrics-wrapper">maarave naa kathala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maarave naa kathala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thenelalo viri thene nuvve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thenelalo viri thene nuvve"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiyyanga nuvve panchuthe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiyyanga nuvve panchuthe "/>
</div>
<div class="lyrico-lyrics-wrapper">chedhaina theepe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chedhaina theepe"/>
</div>
<div class="lyrico-lyrics-wrapper">May nelalo poga manchu nuve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="May nelalo poga manchu nuve"/>
</div>
<div class="lyrico-lyrics-wrapper">Challanga nuvve thakithe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Challanga nuvve thakithe "/>
</div>
<div class="lyrico-lyrics-wrapper">yendaina manche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yendaina manche"/>
</div>
<div class="lyrico-lyrics-wrapper">Chiru chiru peddavula 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chiru chiru peddavula "/>
</div>
<div class="lyrico-lyrics-wrapper">chura chura katthiki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chura chura katthiki"/>
</div>
<div class="lyrico-lyrics-wrapper">Padhune penchenu tholi muddhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padhune penchenu tholi muddhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gadi gadi parugula 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gadi gadi parugula "/>
</div>
<div class="lyrico-lyrics-wrapper">gadiyaralaku selivika aapoddhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gadiyaralaku selivika aapoddhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chali gili penchuthu champesthunnadhi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chali gili penchuthu champesthunnadhi "/>
</div>
<div class="lyrico-lyrics-wrapper">nee kanu saigala theepi visham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee kanu saigala theepi visham"/>
</div>
<div class="lyrico-lyrics-wrapper">Kougili oushadamistha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kougili oushadamistha "/>
</div>
<div class="lyrico-lyrics-wrapper">raa ika ee nimsham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="raa ika ee nimsham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nadhila nadhila nadhila 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadhila nadhila nadhila "/>
</div>
<div class="lyrico-lyrics-wrapper">kadhilave oo nadhila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhilave oo nadhila"/>
</div>
<div class="lyrico-lyrics-wrapper">Alalaa alalaa alalaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alalaa alalaa alalaa "/>
</div>
<div class="lyrico-lyrics-wrapper">thadisave nannu ila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thadisave nannu ila"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalalaa kalalaa kalalaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalalaa kalalaa kalalaa "/>
</div>
<div class="lyrico-lyrics-wrapper">kalisave oo kalalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalisave oo kalalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathala kathala kathala 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathala kathala kathala "/>
</div>
<div class="lyrico-lyrics-wrapper">maarave naa kathala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maarave naa kathala"/>
</div>
</pre>
