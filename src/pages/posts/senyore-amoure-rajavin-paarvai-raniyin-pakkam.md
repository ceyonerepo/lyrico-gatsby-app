---
title: "senyore amoure song lyrics"
album: "Rajavin Paarvai Raniyin Pakkam"
artist: "Leander Lee Marty"
lyricist: "Vignesh K Jeyapal"
director: "Azhagu Raj"
path: "/albums/rajavin-paarvai-raniyin-pakkam-lyrics"
song: "Senyore Amoure"
image: ../../images/albumart/rajavin-paarvai-raniyin-pakkam.jpg
date: 2018-09-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/qBN8rqEgqII"
type: "love"
singers:
  - Christopher Stanley
  - Shilvi Sharon
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">en agam puram siram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en agam puram siram"/>
</div>
<div class="lyrico-lyrics-wrapper">nee thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee thaane"/>
</div>
<div class="lyrico-lyrics-wrapper">un idam valam kalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un idam valam kalam"/>
</div>
<div class="lyrico-lyrics-wrapper">naan thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan thaane"/>
</div>
<div class="lyrico-lyrics-wrapper">pon vannan unthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pon vannan unthan"/>
</div>
<div class="lyrico-lyrics-wrapper">kannangaleey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannangaleey"/>
</div>
<div class="lyrico-lyrics-wrapper">kannangale un 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannangale un "/>
</div>
<div class="lyrico-lyrics-wrapper">kan thinnuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kan thinnuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">ohhhh nanbillai seevale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ohhhh nanbillai seevale"/>
</div>
<div class="lyrico-lyrics-wrapper">ven velli thoovale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ven velli thoovale"/>
</div>
<div class="lyrico-lyrics-wrapper">senyore amoure
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="senyore amoure"/>
</div>
<div class="lyrico-lyrics-wrapper">senthoora thoorale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="senthoora thoorale"/>
</div>
<div class="lyrico-lyrics-wrapper">sitrinba kaathale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sitrinba kaathale"/>
</div>
<div class="lyrico-lyrics-wrapper">senyore amoure
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="senyore amoure"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">en sugam tharum varam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en sugam tharum varam"/>
</div>
<div class="lyrico-lyrics-wrapper">nee thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee thaane"/>
</div>
<div class="lyrico-lyrics-wrapper">un manam migum manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un manam migum manam"/>
</div>
<div class="lyrico-lyrics-wrapper">naan thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan thaane"/>
</div>
<div class="lyrico-lyrics-wrapper">men melliyathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="men melliyathum"/>
</div>
<div class="lyrico-lyrics-wrapper">sitridaiyee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sitridaiyee"/>
</div>
<div class="lyrico-lyrics-wrapper">mei meeri unnil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mei meeri unnil"/>
</div>
<div class="lyrico-lyrics-wrapper">karaiyaveey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karaiyaveey"/>
</div>
<div class="lyrico-lyrics-wrapper">ohhh kan kanda kaanale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ohhh kan kanda kaanale"/>
</div>
<div class="lyrico-lyrics-wrapper">perinba saarale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="perinba saarale"/>
</div>
<div class="lyrico-lyrics-wrapper">senyore amoure
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="senyore amoure"/>
</div>
<div class="lyrico-lyrics-wrapper">oh oh oh oh oh oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oh oh oh oh oh oh"/>
</div>
<div class="lyrico-lyrics-wrapper">naan saayum tholgale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan saayum tholgale"/>
</div>
<div class="lyrico-lyrics-wrapper">enai saaikum kathale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enai saaikum kathale"/>
</div>
<div class="lyrico-lyrics-wrapper">senyore amoure
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="senyore amoure"/>
</div>
</pre>
