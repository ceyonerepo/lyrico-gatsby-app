---
title: "unnale unnale song lyrics"
album: "Kennedy Club"
artist: "D. Imman"
lyricist: "Viveka"
director: "Suseenthiran"
path: "/albums/kennedy-club-lyrics"
song: "Unnale Unnale"
image: ../../images/albumart/kennedy-club.jpg
date: 2019-08-22
lang: tamil
youtubeLink: "https://www.youtube.com/embed/fDMm9tznbI8"
type: "motivational"
singers:
  - Vijay Yesudas
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Unnaalae Unnaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaalae Unnaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudiyaathendraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudiyaathendraal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Yaaraalum Yaaraalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Yaaraalum Yaaraalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudiyaathu – Vaaaaauu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudiyaathu – Vaaaaauu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnaalae Unnaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaalae Unnaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudiyaathendraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudiyaathendraal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Yaaraalum Yaaraalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Yaaraalum Yaaraalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudiyaathu – Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudiyaathu – Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indrodu Indrodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indrodu Indrodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Ulagam Ondrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Ulagam Ondrum"/>
</div>
<div class="lyrico-lyrics-wrapper">Illaiyaagi Pogaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illaiyaagi Pogaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjodu Nenjodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjodu Nenjodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nambikkai Mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nambikkai Mattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vattri Pogakkoodathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vattri Pogakkoodathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnaalae Unnaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaalae Unnaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudiyaathendraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudiyaathendraal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Yaaraalum Yaaraalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Yaaraalum Yaaraalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudiyaathu – Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudiyaathu – Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vilunthaal Asingam Endraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilunthaal Asingam Endraal"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhai Dhaan Vizhumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai Dhaan Vizhumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Paravai Parandhuvittaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paravai Parandhuvittaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Marangal Azhumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marangal Azhumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Marunaalum Varumdhaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marunaalum Varumdhaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu Vaasam Thaangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu Vaasam Thaangi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnaalae Unnaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaalae Unnaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudiyaathendraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudiyaathendraal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Yaaraalum Yaaraalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Yaaraalum Yaaraalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudiyaathu – Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudiyaathu – Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalaththai Poloru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalaththai Poloru"/>
</div>
<div class="lyrico-lyrics-wrapper">Thozhan Varumaa Varumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozhan Varumaa Varumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kayaththai Aatridum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kayaththai Aatridum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paaru Idhamaa Idhamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaru Idhamaa Idhamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Perum Sogam Ena Thondrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perum Sogam Ena Thondrum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nigalvellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nigalvellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Maruvaaram Siripoottum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maruvaaram Siripoottum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninaivae Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaivae Dhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Elidhaai Yaarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Elidhaai Yaarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sigaram Serntha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sigaram Serntha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhai Yedhum Illaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhai Yedhum Illaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadaigal Modhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadaigal Modhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Peridhaai Saadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peridhaai Saadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalaradhae Thalirae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaradhae Thalirae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Valikkum Kaayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valikkum Kaayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nirantharam Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nirantharam Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vettri Ondrae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vettri Ondrae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaalkaiyum Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaalkaiyum Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnaalae Unnaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaalae Unnaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudiyaathendraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudiyaathendraal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Yaaraalum Yaaraalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Yaaraalum Yaaraalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudiyaathu – Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudiyaathu – Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnaalae Unnaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaalae Unnaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudiyaathendraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudiyaathendraal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Yaaraalum Yaaraalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Yaaraalum Yaaraalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudiyaathu – Vaaaaaahaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudiyaathu – Vaaaaaahaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Haaaaaaaaaaaaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haaaaaaaaaaaaaaaaa"/>
</div>
</pre>
