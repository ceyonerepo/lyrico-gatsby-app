---
title: "pathu kilo song lyrics"
album: "Melnaattu Marumagan"
artist: "V. Kishorkumar"
lyricist: "Nanchil Rajan"
director: "MSS"
path: "/albums/melnaattu-marumagan-lyrics"
song: "Pathu Kilo"
image: ../../images/albumart/melnaattu-marumagan.jpg
date: 2018-02-16
lang: tamil
youtubeLink: "https://www.youtube.com/embed/RZp1_TjFMuw"
type: "love"
singers:
  - Jiththin
  - Anitha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">pathu kilo mutham kondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathu kilo mutham kondu"/>
</div>
<div class="lyrico-lyrics-wrapper">pacha kutha poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pacha kutha poren"/>
</div>
<div class="lyrico-lyrics-wrapper">athu panalana loan vangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu panalana loan vangi"/>
</div>
<div class="lyrico-lyrics-wrapper">patha vaika poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="patha vaika poren"/>
</div>
<div class="lyrico-lyrics-wrapper">nan pathu kilo mutham kondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan pathu kilo mutham kondu"/>
</div>
<div class="lyrico-lyrics-wrapper">pacha kutha poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pacha kutha poren"/>
</div>
<div class="lyrico-lyrics-wrapper">athu panalana loan vangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu panalana loan vangi"/>
</div>
<div class="lyrico-lyrics-wrapper">patha vaika poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="patha vaika poren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">hey thama thundu odambula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey thama thundu odambula"/>
</div>
<div class="lyrico-lyrics-wrapper">yemma periya muthama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yemma periya muthama"/>
</div>
<div class="lyrico-lyrics-wrapper">summa onnu pothum da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="summa onnu pothum da"/>
</div>
<div class="lyrico-lyrics-wrapper">patta podum manasu la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="patta podum manasu la"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ichu ichu pacha kiliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ichu ichu pacha kiliye"/>
</div>
<div class="lyrico-lyrics-wrapper">pakkam vayen di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pakkam vayen di"/>
</div>
<div class="lyrico-lyrics-wrapper">nachu nachu ichu tharen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nachu nachu ichu tharen"/>
</div>
<div class="lyrico-lyrics-wrapper">vetkam kuraiyen di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetkam kuraiyen di"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pathu kilo mutham kondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathu kilo mutham kondu"/>
</div>
<div class="lyrico-lyrics-wrapper">pacha kutha poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pacha kutha poren"/>
</div>
<div class="lyrico-lyrics-wrapper">athu panalana loan vangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu panalana loan vangi"/>
</div>
<div class="lyrico-lyrics-wrapper">patha vaika poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="patha vaika poren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nan pathu kilo mutham kondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan pathu kilo mutham kondu"/>
</div>
<div class="lyrico-lyrics-wrapper">pacha kutha poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pacha kutha poren"/>
</div>
<div class="lyrico-lyrics-wrapper">athu panalana loan vangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu panalana loan vangi"/>
</div>
<div class="lyrico-lyrics-wrapper">patha vaika poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="patha vaika poren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">meter mela potu tharen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meter mela potu tharen"/>
</div>
<div class="lyrico-lyrics-wrapper">suda suda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suda suda"/>
</div>
<div class="lyrico-lyrics-wrapper">nadurula thongi pova
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nadurula thongi pova"/>
</div>
<div class="lyrico-lyrics-wrapper">coola coola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="coola coola"/>
</div>
<div class="lyrico-lyrics-wrapper">meter mela potu tharen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meter mela potu tharen"/>
</div>
<div class="lyrico-lyrics-wrapper">suda suda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suda suda"/>
</div>
<div class="lyrico-lyrics-wrapper">nadurula thongi pova
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nadurula thongi pova"/>
</div>
<div class="lyrico-lyrics-wrapper">coola coola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="coola coola"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">acharama venuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="acharama venuma"/>
</div>
<div class="lyrico-lyrics-wrapper">apurama venuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="apurama venuma"/>
</div>
<div class="lyrico-lyrics-wrapper">aasaiyodu vangiko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aasaiyodu vangiko"/>
</div>
<div class="lyrico-lyrics-wrapper">aaiyisu than kootiko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaiyisu than kootiko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ada ithu ethuku ilavasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada ithu ethuku ilavasam"/>
</div>
<div class="lyrico-lyrics-wrapper">vachu iruken kai vasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vachu iruken kai vasam"/>
</div>
<div class="lyrico-lyrics-wrapper">athu ahuku ethatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu ahuku ethatha"/>
</div>
<div class="lyrico-lyrics-wrapper">vanga poren unnidam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanga poren unnidam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thapa onna vangina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thapa onna vangina"/>
</div>
<div class="lyrico-lyrics-wrapper">sorgam inge ilavasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sorgam inge ilavasam"/>
</div>
<div class="lyrico-lyrics-wrapper">sorgatha nee vangina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sorgatha nee vangina"/>
</div>
<div class="lyrico-lyrics-wrapper">en sontham ellam un vasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en sontham ellam un vasam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">hey ichu ichu pacha kiliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey ichu ichu pacha kiliye"/>
</div>
<div class="lyrico-lyrics-wrapper">pakkam vayen di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pakkam vayen di"/>
</div>
<div class="lyrico-lyrics-wrapper">nachu nachu ichu tharen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nachu nachu ichu tharen"/>
</div>
<div class="lyrico-lyrics-wrapper">vetkam kuraiyen di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetkam kuraiyen di"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nan pathu kilo mutham kondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan pathu kilo mutham kondu"/>
</div>
<div class="lyrico-lyrics-wrapper">pacha kutha poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pacha kutha poren"/>
</div>
<div class="lyrico-lyrics-wrapper">athu panalana loan vangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu panalana loan vangi"/>
</div>
<div class="lyrico-lyrics-wrapper">patha vaika poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="patha vaika poren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nan pathu kilo mutham kondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan pathu kilo mutham kondu"/>
</div>
<div class="lyrico-lyrics-wrapper">pacha kutha poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pacha kutha poren"/>
</div>
<div class="lyrico-lyrics-wrapper">athu panalana loan vangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu panalana loan vangi"/>
</div>
<div class="lyrico-lyrics-wrapper">patha vaika poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="patha vaika poren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">hey heater aaga maruthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey heater aaga maruthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">thegam thegam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thegam thegam"/>
</div>
<div class="lyrico-lyrics-wrapper">heart kulla poduthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="heart kulla poduthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">melam thaalam ah ah ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="melam thaalam ah ah ah"/>
</div>
<div class="lyrico-lyrics-wrapper">hey heater aaga maruthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey heater aaga maruthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">thegam thegam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thegam thegam"/>
</div>
<div class="lyrico-lyrics-wrapper">heart kulla poduthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="heart kulla poduthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">melam thaalam ah ah ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="melam thaalam ah ah ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pakkam vantha pothuma 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pakkam vantha pothuma "/>
</div>
<div class="lyrico-lyrics-wrapper">patha vacha theeruma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="patha vacha theeruma"/>
</div>
<div class="lyrico-lyrics-wrapper">pakuvama sanchuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pakuvama sanchuko"/>
</div>
<div class="lyrico-lyrics-wrapper">pathirama vangiko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathirama vangiko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nee enaku kidachathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee enaku kidachathu"/>
</div>
<div class="lyrico-lyrics-wrapper">ulaga maga adhisayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulaga maga adhisayam"/>
</div>
<div class="lyrico-lyrics-wrapper">nan unaku kedachathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan unaku kedachathu"/>
</div>
<div class="lyrico-lyrics-wrapper">inai piriya uyir thavam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inai piriya uyir thavam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">uchi mutham paatham varai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uchi mutham paatham varai"/>
</div>
<div class="lyrico-lyrics-wrapper">koocham tharum paravasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koocham tharum paravasam"/>
</div>
<div class="lyrico-lyrics-wrapper">vepatha nee thoondura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vepatha nee thoondura"/>
</div>
<div class="lyrico-lyrics-wrapper">thookam ellam kuranchidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thookam ellam kuranchidum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ichu ichu pacha kiliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ichu ichu pacha kiliye"/>
</div>
<div class="lyrico-lyrics-wrapper">pakkam vayen di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pakkam vayen di"/>
</div>
<div class="lyrico-lyrics-wrapper">nachu nachu ichu tharen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nachu nachu ichu tharen"/>
</div>
<div class="lyrico-lyrics-wrapper">vetkam kuraiyen di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetkam kuraiyen di"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nan pathu kilo mutham kondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan pathu kilo mutham kondu"/>
</div>
<div class="lyrico-lyrics-wrapper">pacha kutha poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pacha kutha poren"/>
</div>
<div class="lyrico-lyrics-wrapper">athu panalana loan vangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu panalana loan vangi"/>
</div>
<div class="lyrico-lyrics-wrapper">patha vaika poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="patha vaika poren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nan pathu kilo mutham kondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan pathu kilo mutham kondu"/>
</div>
<div class="lyrico-lyrics-wrapper">pacha kutha poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pacha kutha poren"/>
</div>
<div class="lyrico-lyrics-wrapper">athu panalana loan vangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu panalana loan vangi"/>
</div>
<div class="lyrico-lyrics-wrapper">patha vaika poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="patha vaika poren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">hey thama thundu odambula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey thama thundu odambula"/>
</div>
<div class="lyrico-lyrics-wrapper">yemma periya muthama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yemma periya muthama"/>
</div>
<div class="lyrico-lyrics-wrapper">summa onnu pothum da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="summa onnu pothum da"/>
</div>
<div class="lyrico-lyrics-wrapper">patta podum manasu la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="patta podum manasu la"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ichu ichu pacha kiliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ichu ichu pacha kiliye"/>
</div>
<div class="lyrico-lyrics-wrapper">pakkam vayen di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pakkam vayen di"/>
</div>
<div class="lyrico-lyrics-wrapper">nachu nachu ichu tharen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nachu nachu ichu tharen"/>
</div>
<div class="lyrico-lyrics-wrapper">vetkam kuraiyen di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetkam kuraiyen di"/>
</div>
</pre>
