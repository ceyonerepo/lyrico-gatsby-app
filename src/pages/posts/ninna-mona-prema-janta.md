---
title: "ninna mona song lyrics"
album: "Prema Janta"
artist: "Nikhilesh Thogari"
lyricist: "Nikhilesh Thogari"
director: "Nikhilesh Thogari"
path: "/albums/prema-janta-lyrics"
song: "Ninna Mona"
image: ../../images/albumart/prema-janta.jpg
date: 2019-06-28
lang: telugu
youtubeLink: "https://www.youtube.com/embed/SL6Lvdbe1xE"
type: "happy"
singers:
  - Rahul Sipligunj
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">ninnu munna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninnu munna "/>
</div>
<div class="lyrico-lyrics-wrapper">chinna pilla gallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chinna pilla gallu"/>
</div>
<div class="lyrico-lyrics-wrapper">ippudemo herolayyaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ippudemo herolayyaru"/>
</div>
<div class="lyrico-lyrics-wrapper">ninna monna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninna monna"/>
</div>
<div class="lyrico-lyrics-wrapper">cheddeelesinollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cheddeelesinollu"/>
</div>
<div class="lyrico-lyrics-wrapper">ippudemo veera phojulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ippudemo veera phojulu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ninnu munna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninnu munna "/>
</div>
<div class="lyrico-lyrics-wrapper">chinna pilla gallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chinna pilla gallu"/>
</div>
<div class="lyrico-lyrics-wrapper">ippudemo herolayyaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ippudemo herolayyaru"/>
</div>
<div class="lyrico-lyrics-wrapper">ninna monna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninna monna"/>
</div>
<div class="lyrico-lyrics-wrapper">cheddeelesinollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cheddeelesinollu"/>
</div>
<div class="lyrico-lyrics-wrapper">ippudemo veera phojulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ippudemo veera phojulu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">letha letha meesalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="letha letha meesalu"/>
</div>
<div class="lyrico-lyrics-wrapper">dammu chupe roshalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dammu chupe roshalu"/>
</div>
<div class="lyrico-lyrics-wrapper">inter mediattu yeshalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inter mediattu yeshalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ninnu munna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninnu munna "/>
</div>
<div class="lyrico-lyrics-wrapper">chinna pilla gallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chinna pilla gallu"/>
</div>
<div class="lyrico-lyrics-wrapper">ippudemo herolayyaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ippudemo herolayyaru"/>
</div>
<div class="lyrico-lyrics-wrapper">ninna monna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninna monna"/>
</div>
<div class="lyrico-lyrics-wrapper">cheddeelesinollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cheddeelesinollu"/>
</div>
<div class="lyrico-lyrics-wrapper">ippudemo veera phojulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ippudemo veera phojulu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">okavaipu ammailu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="okavaipu ammailu"/>
</div>
<div class="lyrico-lyrics-wrapper">okavaipu abbailu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="okavaipu abbailu"/>
</div>
<div class="lyrico-lyrics-wrapper">evariki viru emi karu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="evariki viru emi karu"/>
</div>
<div class="lyrico-lyrics-wrapper">eduraithe mari kangaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eduraithe mari kangaru"/>
</div>
<div class="lyrico-lyrics-wrapper">okavaipu inter books
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="okavaipu inter books"/>
</div>
<div class="lyrico-lyrics-wrapper">okavaipu kanning looks
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="okavaipu kanning looks"/>
</div>
<div class="lyrico-lyrics-wrapper">chivariki viru emautharo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chivariki viru emautharo"/>
</div>
<div class="lyrico-lyrics-wrapper">parents aithe bejaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="parents aithe bejaru"/>
</div>
<div class="lyrico-lyrics-wrapper">evarevaro idiots
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="evarevaro idiots"/>
</div>
<div class="lyrico-lyrics-wrapper">evaro mari intelligents
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="evaro mari intelligents"/>
</div>
<div class="lyrico-lyrics-wrapper">idhariki mediatu intermediate
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhariki mediatu intermediate"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ninnu munna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninnu munna "/>
</div>
<div class="lyrico-lyrics-wrapper">chinna pilla gallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chinna pilla gallu"/>
</div>
<div class="lyrico-lyrics-wrapper">ippudemo herolayyaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ippudemo herolayyaru"/>
</div>
<div class="lyrico-lyrics-wrapper">ninna monna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninna monna"/>
</div>
<div class="lyrico-lyrics-wrapper">cheddeelesinollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cheddeelesinollu"/>
</div>
<div class="lyrico-lyrics-wrapper">ippudemo veera phojulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ippudemo veera phojulu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">class lo kanthri thinks
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="class lo kanthri thinks"/>
</div>
<div class="lyrico-lyrics-wrapper">canteen lo discussionsu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="canteen lo discussionsu"/>
</div>
<div class="lyrico-lyrics-wrapper">campus lo kalisarante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="campus lo kalisarante"/>
</div>
<div class="lyrico-lyrics-wrapper">chillara panulaku cheerings
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chillara panulaku cheerings"/>
</div>
<div class="lyrico-lyrics-wrapper">matallo 2 meanings 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="matallo 2 meanings "/>
</div>
<div class="lyrico-lyrics-wrapper">chetallo 2 feelings
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chetallo 2 feelings"/>
</div>
<div class="lyrico-lyrics-wrapper">teenage college ante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="teenage college ante"/>
</div>
<div class="lyrico-lyrics-wrapper">allari panulaku address
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="allari panulaku address"/>
</div>
<div class="lyrico-lyrics-wrapper">lolopala teliyani doubts
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lolopala teliyani doubts"/>
</div>
<div class="lyrico-lyrics-wrapper">paikemo no comments
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paikemo no comments"/>
</div>
<div class="lyrico-lyrics-wrapper">confusion common 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="confusion common "/>
</div>
<div class="lyrico-lyrics-wrapper">centre intermediate 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="centre intermediate "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ninnu munna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninnu munna "/>
</div>
<div class="lyrico-lyrics-wrapper">chinna pilla gallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chinna pilla gallu"/>
</div>
<div class="lyrico-lyrics-wrapper">ippudemo herolayyaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ippudemo herolayyaru"/>
</div>
<div class="lyrico-lyrics-wrapper">ninna monna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninna monna"/>
</div>
<div class="lyrico-lyrics-wrapper">cheddeelesinollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cheddeelesinollu"/>
</div>
<div class="lyrico-lyrics-wrapper">ippudemo veera phojulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ippudemo veera phojulu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">letha letha meesalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="letha letha meesalu"/>
</div>
<div class="lyrico-lyrics-wrapper">dammu chupe roshalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dammu chupe roshalu"/>
</div>
<div class="lyrico-lyrics-wrapper">inter mediattu yeshalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inter mediattu yeshalu"/>
</div>
</pre>
