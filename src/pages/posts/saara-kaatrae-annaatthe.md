---
title: "saara kaatrae song lyrics"
album: "Annaatthe"
artist: "D.Imman"
lyricist: "Yugabharathi"
director: "Siva"
path: "/albums/annaatthe-lyrics"
song: "Saara Kaatrae"
image: ../../images/albumart/annaatthe.jpg
date: 2021-10-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Z0LM8sDR-Ng"
type: "love"
singers:
  - Sid Sriram
  - Shreya Ghoshal
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Saara saara kaatrae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saara saara kaatrae"/>
</div>
<div class="lyrico-lyrics-wrapper">Saara saara kaatrae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saara saara kaatrae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saara saara kaatrae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saara saara kaatrae"/>
</div>
<div class="lyrico-lyrics-wrapper">Poongi vazhigiradhae sandhosha oottrae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poongi vazhigiradhae sandhosha oottrae"/>
</div>
<div class="lyrico-lyrics-wrapper">Saara saara kaatrae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saara saara kaatrae"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbaai pozhigiradhae aanandha keetrae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbaai pozhigiradhae aanandha keetrae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saada sadannu kannrendum thean thoova
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saada sadannu kannrendum thean thoova"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanaigiradhae en aaiyul regaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanaigiradhae en aaiyul regaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Paada padannu kairendum seeratta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paada padannu kairendum seeratta"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhugiradhae namm tholil maalaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhugiradhae namm tholil maalaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pachchai manadhu paalniraam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pachchai manadhu paalniraam"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbil sivandhu pogudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbil sivandhu pogudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sattrae irunda vaanilai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sattrae irunda vaanilai"/>
</div>
<div class="lyrico-lyrics-wrapper">Unn azhagai kandathumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unn azhagai kandathumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Minnoli perudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minnoli perudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thinathuk thagidaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thinathuk thagidaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thinathuk thagidaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thinathuk thagidaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaanae thaanae thaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanae thaanae thaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thane thinathuk thagidaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thane thinathuk thagidaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thinathuk thagidaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thinathuk thagidaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaanae naana naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanae naana naa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saara saara kaatrae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saara saara kaatrae"/>
</div>
<div class="lyrico-lyrics-wrapper">Poongi vazhigiradhae sandhosha oottrae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poongi vazhigiradhae sandhosha oottrae"/>
</div>
<div class="lyrico-lyrics-wrapper">Saara saara kaatrae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saara saara kaatrae"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbaai pozhigiradhae aanandha keetrae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbaai pozhigiradhae aanandha keetrae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaazh isaiyum ezh isaiyum unn kuraalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaazh isaiyum ezh isaiyum unn kuraalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee nerunga paarpathuthaan sorkangaalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee nerunga paarpathuthaan sorkangaalo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Deivaam marandhu koduthidaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Deivaam marandhu koduthidaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Varaam ethanai kodiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varaam ethanai kodiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Alli kodukka thunindha kaadhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alli kodukka thunindha kaadhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhai solvadhu needhiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai solvadhu needhiyo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Siththam unaai enni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siththam unaai enni"/>
</div>
<div class="lyrico-lyrics-wrapper">Sadugudu vilaiyaadudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sadugudu vilaiyaadudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Puththam pudhu vetkkam pugunthida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puththam pudhu vetkkam pugunthida"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadai maarudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadai maarudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Andhi pagalai marandhu uravae neelaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhi pagalai marandhu uravae neelaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbae nee vandhaayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbae nee vandhaayae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saara saara kaatrae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saara saara kaatrae"/>
</div>
<div class="lyrico-lyrics-wrapper">Poongi vazhigiradhae sandhosha oottrae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poongi vazhigiradhae sandhosha oottrae"/>
</div>
<div class="lyrico-lyrics-wrapper">Saara saara kaatrae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saara saara kaatrae"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbaai pozhigiradhae aanandha keetrae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbaai pozhigiradhae aanandha keetrae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Silu siluvendru poonthendral soodettra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silu siluvendru poonthendral soodettra"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyiranuvae pon oonjal aadudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyiranuvae pon oonjal aadudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kulukulunu thee veiyil thaalaatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulukulunu thee veiyil thaalaatta"/>
</div>
<div class="lyrico-lyrics-wrapper">Adaai mazhaiyil en aasai moolgudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adaai mazhaiyil en aasai moolgudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Latcham paravai polae en
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Latcham paravai polae en"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullaam midhandhu pogudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullaam midhandhu pogudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sattrae irunda vaanilai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sattrae irunda vaanilai"/>
</div>
<div class="lyrico-lyrics-wrapper">Unn azhagai kandathumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unn azhagai kandathumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Minnoli perudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minnoli perudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thinathuk thagidaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thinathuk thagidaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thinathuk thagidaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thinathuk thagidaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaanae thaanae thaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanae thaanae thaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanae thinathuk thagidaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanae thinathuk thagidaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thinathuk thagidaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thinathuk thagidaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaanae naana naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanae naana naa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Heyy heyy thinathuk thagidaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy heyy thinathuk thagidaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thinathuk thagidaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thinathuk thagidaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaanae thaanae thaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanae thaanae thaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanae thinathuk thagidaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanae thinathuk thagidaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thinathuk thagidaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thinathuk thagidaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaanae naana naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanae naana naa"/>
</div>
</pre>
