---
title: "shaamein song lyrics"
album: "The Gorilla Bounce"
artist: "King - Harjas Harjaayi"
lyricist: "King - Harjas Harjaayi"
director: "Yash Chhabra"
path: "/albums/the-gorilla-bounce-lyrics"
song: "Shaamein"
image: ../../images/albumart/the-gorilla-bounce.jpg
date: 2021-05-21
lang: hindi
youtubeLink: "https://www.youtube.com/embed/tmTe6SiC0Po"
type: "Mass"
singers:
  - King
  - Harjas Harjaayi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Tu jaane naa jaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu jaane naa jaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Maine teri baatein ki hain
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maine teri baatein ki hain"/>
</div>
<div class="lyrico-lyrics-wrapper">Tujhko pata bhi nahi hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tujhko pata bhi nahi hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kharab yeh raatein ki hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kharab yeh raatein ki hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jitne bhi gham thhe tumhare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jitne bhi gham thhe tumhare"/>
</div>
<div class="lyrico-lyrics-wrapper">Meri yaad bhula deti hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meri yaad bhula deti hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Haste hasaate mujhko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haste hasaate mujhko"/>
</div>
<div class="lyrico-lyrics-wrapper">Phir kyun rula deti hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phir kyun rula deti hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jab hoti hain shaamein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jab hoti hain shaamein"/>
</div>
<div class="lyrico-lyrics-wrapper">Teri yaad bula leti hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teri yaad bula leti hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Tanha joh rehta hu main
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tanha joh rehta hu main"/>
</div>
<div class="lyrico-lyrics-wrapper">Mujhko sula deti hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mujhko sula deti hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jab hoti hain shaamein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jab hoti hain shaamein"/>
</div>
<div class="lyrico-lyrics-wrapper">Teri yaad bula leti hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teri yaad bula leti hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Tanha joh rehta hu main
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tanha joh rehta hu main"/>
</div>
<div class="lyrico-lyrics-wrapper">Mujhko sula deti hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mujhko sula deti hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Teri yaad mein main baitha rehta tanha sa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teri yaad mein main baitha rehta tanha sa"/>
</div>
<div class="lyrico-lyrics-wrapper">Na koyi kaam hua na hi mera mann laga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na koyi kaam hua na hi mera mann laga"/>
</div>
<div class="lyrico-lyrics-wrapper">Na message kiya na hi call kar saka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na message kiya na hi call kar saka"/>
</div>
<div class="lyrico-lyrics-wrapper">Dil na toot jaaye dil hi dil mein darr laga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil na toot jaaye dil hi dil mein darr laga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jabse parr lage tujhe main na udd saka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jabse parr lage tujhe main na udd saka"/>
</div>
<div class="lyrico-lyrics-wrapper">Khayal raha koyi mujhe khudka na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khayal raha koyi mujhe khudka na"/>
</div>
<div class="lyrico-lyrics-wrapper">Door raha taaki tujhse jud sakun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Door raha taaki tujhse jud sakun"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeh khanzar mera aa k mujhe khud laga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh khanzar mera aa k mujhe khud laga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tu jaane naa jaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu jaane naa jaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Maine teri baatein ki hain
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maine teri baatein ki hain"/>
</div>
<div class="lyrico-lyrics-wrapper">Tujhko pata bhi nahi hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tujhko pata bhi nahi hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kharab yeh raatein ki hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kharab yeh raatein ki hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jitne bhi gham thhe tumhare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jitne bhi gham thhe tumhare"/>
</div>
<div class="lyrico-lyrics-wrapper">Meri yaad bhula deti hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meri yaad bhula deti hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Haste hasaate mujhko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haste hasaate mujhko"/>
</div>
<div class="lyrico-lyrics-wrapper">Phir kyun rula deti hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phir kyun rula deti hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jab hoti hain shaamein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jab hoti hain shaamein"/>
</div>
<div class="lyrico-lyrics-wrapper">Teri yaad bula leti hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teri yaad bula leti hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Tanha joh rehta hu main
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tanha joh rehta hu main"/>
</div>
<div class="lyrico-lyrics-wrapper">Mujhko sula deti hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mujhko sula deti hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jab hoti hain shaamein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jab hoti hain shaamein"/>
</div>
<div class="lyrico-lyrics-wrapper">Teri yaad bula leti hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teri yaad bula leti hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Tanha joh rehta hu main
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tanha joh rehta hu main"/>
</div>
<div class="lyrico-lyrics-wrapper">Mujhko sula deti hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mujhko sula deti hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tujhe sochun jab jab meri aankhein bhar aaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tujhe sochun jab jab meri aankhein bhar aaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Ke maine pyaar kiya hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ke maine pyaar kiya hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Haali dino maine tujhe kahi kehte suna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haali dino maine tujhe kahi kehte suna"/>
</div>
<div class="lyrico-lyrics-wrapper">Ke maine vaar kiya hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ke maine vaar kiya hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thoda theek se lagana dosh seekh leti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoda theek se lagana dosh seekh leti"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeh sab dekh meri rooh bhi aankhein meech leti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh sab dekh meri rooh bhi aankhein meech leti"/>
</div>
<div class="lyrico-lyrics-wrapper">Aafat to nahi thi koyi aisi ke tu tode dil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aafat to nahi thi koyi aisi ke tu tode dil"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoda ruk jaati to wafa bhi karna seekh leti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoda ruk jaati to wafa bhi karna seekh leti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Le karta dil tere hawale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Le karta dil tere hawale"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu isme shehar ek banale baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu isme shehar ek banale baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Jab toota dil leke hi nikle to
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jab toota dil leke hi nikle to"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaise mile ghar tera?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaise mile ghar tera?"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kyuki!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kyuki!"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tu jaane naa jaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu jaane naa jaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Maine teri baatein ki hain
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maine teri baatein ki hain"/>
</div>
<div class="lyrico-lyrics-wrapper">Tujhko pata bhi nahi hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tujhko pata bhi nahi hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kharab yeh raatein ki hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kharab yeh raatein ki hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jitne bhi gham thhe tumhare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jitne bhi gham thhe tumhare"/>
</div>
<div class="lyrico-lyrics-wrapper">Meri yaad bhula deti hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meri yaad bhula deti hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Haste hasaate mujhko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haste hasaate mujhko"/>
</div>
<div class="lyrico-lyrics-wrapper">Phir kyun rula deti hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phir kyun rula deti hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jab hoti hain shaamein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jab hoti hain shaamein"/>
</div>
<div class="lyrico-lyrics-wrapper">Teri yaad bula leti hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teri yaad bula leti hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Tanha joh rehta hu main
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tanha joh rehta hu main"/>
</div>
<div class="lyrico-lyrics-wrapper">Mujhko sula deti hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mujhko sula deti hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jab hoti hain shaamein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jab hoti hain shaamein"/>
</div>
<div class="lyrico-lyrics-wrapper">Teri yaad bula leti hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teri yaad bula leti hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Tanha joh rehta hu main
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tanha joh rehta hu main"/>
</div>
<div class="lyrico-lyrics-wrapper">Mujhko sula deti hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mujhko sula deti hai"/>
</div>
</pre>
