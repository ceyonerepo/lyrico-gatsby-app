---
title: "kya song lyrics"
album: "Crook"
artist: "Pritam - Babbu Maan"
lyricist: "Kumaar"
director: "Mohit Suri"
path: "/albums/crook-lyrics"
song: "Kya Sunaun Main"
image: ../../images/albumart/crook.jpg
date: 2010-10-08
lang: hindi
youtubeLink: "https://www.youtube.com/embed/pX5m9gN7Z60"
type: "love"
singers:
  - Neeraj Shridhar
  - Dominique Cerejo
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kya sunaun main? kya bataun main?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kya sunaun main? kya bataun main?"/>
</div>
<div class="lyrico-lyrics-wrapper">Kya woh keh gayi thi isharon se?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kya woh keh gayi thi isharon se?"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuchh nigahon se, kuchh adaaon se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuchh nigahon se, kuchh adaaon se"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuchh churaya lab ke kinaron se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuchh churaya lab ke kinaron se"/>
</div>
<div class="lyrico-lyrics-wrapper">Use jo chhua tha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Use jo chhua tha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nasha sa hua tha..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nasha sa hua tha.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dil yeh mera karne laga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil yeh mera karne laga"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhak dhak when she hugged me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhak dhak when she hugged me"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thamne lagi jamne lagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thamne lagi jamne lagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Rag rag when she loved me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rag rag when she loved me"/>
</div>
<div class="lyrico-lyrics-wrapper">Dil yeh mera karne laga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil yeh mera karne laga"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhak dhak when she hugged me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhak dhak when she hugged me"/>
</div>
<div class="lyrico-lyrics-wrapper">Thamne lagi jamne lagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thamne lagi jamne lagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Rag rag when she loved me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rag rag when she loved me"/>
</div>
<div class="lyrico-lyrics-wrapper">Kya? Kya? Kya? Kya? Kya?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kya? Kya? Kya? Kya? Kya?"/>
</div>
<div class="lyrico-lyrics-wrapper">Mujhse milne ko woh to aayi thi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mujhse milne ko woh to aayi thi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Reshmi se libaas mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Reshmi se libaas mein"/>
</div>
<div class="lyrico-lyrics-wrapper">Main door baitha tha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main door baitha tha"/>
</div>
<div class="lyrico-lyrics-wrapper">Aake baithi wo yaar mere hi paas mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aake baithi wo yaar mere hi paas mein"/>
</div>
<div class="lyrico-lyrics-wrapper">Usne baatein ki, uski baaton mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usne baatein ki, uski baaton mein"/>
</div>
<div class="lyrico-lyrics-wrapper">Aate aate main aa gaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aate aate main aa gaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Main uski raahon mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main uski raahon mein"/>
</div>
<div class="lyrico-lyrics-wrapper">Uski baahon mein jaane kaise chala gaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uski baahon mein jaane kaise chala gaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhuan sa utha tha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhuan sa utha tha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nason mein ghula tha..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nason mein ghula tha.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dil yeh mera karne laga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil yeh mera karne laga"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhak dhak when she hugged me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhak dhak when she hugged me"/>
</div>
<div class="lyrico-lyrics-wrapper">Thamne lagi jamne lagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thamne lagi jamne lagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Rag rag when she loved me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rag rag when she loved me"/>
</div>
<div class="lyrico-lyrics-wrapper">Dil yeh mera karne laga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil yeh mera karne laga"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhak dhak when she hugged me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhak dhak when she hugged me"/>
</div>
<div class="lyrico-lyrics-wrapper">Thamne lagi jamne lagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thamne lagi jamne lagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Rag rag when she loved me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rag rag when she loved me"/>
</div>
<div class="lyrico-lyrics-wrapper">Kya? Kya? Kya? Kya? Kya?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kya? Kya? Kya? Kya? Kya?"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ever since you got my eye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ever since you got my eye"/>
</div>
<div class="lyrico-lyrics-wrapper">my eye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="my eye"/>
</div>
<div class="lyrico-lyrics-wrapper">Boy what you win my life
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boy what you win my life"/>
</div>
<div class="lyrico-lyrics-wrapper">my life
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="my life"/>
</div>
<div class="lyrico-lyrics-wrapper">You got me going, you don't even try
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You got me going, you don't even try"/>
</div>
<div class="lyrico-lyrics-wrapper">Ever since you got my eye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ever since you got my eye"/>
</div>
<div class="lyrico-lyrics-wrapper">my eye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="my eye"/>
</div>
<div class="lyrico-lyrics-wrapper">Boy what you win my life
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boy what you win my life"/>
</div>
<div class="lyrico-lyrics-wrapper">my life
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="my life"/>
</div>
<div class="lyrico-lyrics-wrapper">You got me going, you don't even try
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You got me going, you don't even try"/>
</div>
<div class="lyrico-lyrics-wrapper">You going, you don't even try
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You going, you don't even try"/>
</div>
<div class="lyrico-lyrics-wrapper">You got me going, you don't even try
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You got me going, you don't even try"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kitne saalon se usne apna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kitne saalon se usne apna"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeh dil tha rakha sambhaal ke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh dil tha rakha sambhaal ke"/>
</div>
<div class="lyrico-lyrics-wrapper">Main jaa ke seene mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main jaa ke seene mein"/>
</div>
<div class="lyrico-lyrics-wrapper">Chupke chupke se le gaya dil nikaal ke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chupke chupke se le gaya dil nikaal ke"/>
</div>
<div class="lyrico-lyrics-wrapper">Sach batata hoon, tum na samjho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sach batata hoon, tum na samjho"/>
</div>
<div class="lyrico-lyrics-wrapper">Hain joothi baatein kitaab ki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hain joothi baatein kitaab ki"/>
</div>
<div class="lyrico-lyrics-wrapper">Woh sun na paayi thi maine raaton ko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Woh sun na paayi thi maine raaton ko"/>
</div>
<div class="lyrico-lyrics-wrapper">Uski neendein kharaab ki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uski neendein kharaab ki"/>
</div>
<div class="lyrico-lyrics-wrapper">Haseen woh safar tha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haseen woh safar tha"/>
</div>
<div class="lyrico-lyrics-wrapper">Naya sa asar tha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naya sa asar tha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dil yeh mera karne laga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil yeh mera karne laga"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhak dhak when she hugged me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhak dhak when she hugged me"/>
</div>
<div class="lyrico-lyrics-wrapper">Thamne lagi jamne lagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thamne lagi jamne lagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Rag rag when she loved me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rag rag when she loved me"/>
</div>
<div class="lyrico-lyrics-wrapper">Dil yeh mera karne laga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil yeh mera karne laga"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhak dhak when she hugged me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhak dhak when she hugged me"/>
</div>
<div class="lyrico-lyrics-wrapper">Thamne lagi jamne lagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thamne lagi jamne lagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Rag rag when she loved me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rag rag when she loved me"/>
</div>
<div class="lyrico-lyrics-wrapper">Dil yeh mera karne laga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil yeh mera karne laga"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhak dhak when she hugged me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhak dhak when she hugged me"/>
</div>
<div class="lyrico-lyrics-wrapper">Thamne lagi jamne lagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thamne lagi jamne lagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Rag rag when she loved me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rag rag when she loved me"/>
</div>
<div class="lyrico-lyrics-wrapper">Kya? Kya? Kya? Kya? Kya?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kya? Kya? Kya? Kya? Kya?"/>
</div>
</pre>
