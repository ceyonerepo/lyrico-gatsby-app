---
title: "enaku nee thandi song lyrics"
album: "Nayae Payae"
artist: "NR Raghunanthan "
lyricist: "Sakthi Vasan"
director: "Sakthi Vasan"
path: "/albums/nayae-payae-song-lyrics"
song: "Enaku Nee Thandi"
image: ../../images/albumart/nayae-payae.jpg
date: 2021-04-23
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Q3YIMdm2d5I"
type: "Love"
singers:
  - Jithin Raj
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">enaku nee thandi devathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enaku nee thandi devathai"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu poiya irunthalum thevala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu poiya irunthalum thevala"/>
</div>
<div class="lyrico-lyrics-wrapper">oorum paakala ulagam paakala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oorum paakala ulagam paakala"/>
</div>
<div class="lyrico-lyrics-wrapper">unna pole oru ponna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna pole oru ponna"/>
</div>
<div class="lyrico-lyrics-wrapper">unga amma mulugama irukum pothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unga amma mulugama irukum pothu"/>
</div>
<div class="lyrico-lyrics-wrapper">moonu velaiyum thinnala venna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moonu velaiyum thinnala venna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">enaku nee thandi devathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enaku nee thandi devathai"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu poiya irunthalum thevala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu poiya irunthalum thevala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vella vella nu iruka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vella vella nu iruka"/>
</div>
<div class="lyrico-lyrics-wrapper">vellakaari pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vellakaari pola"/>
</div>
<div class="lyrico-lyrics-wrapper">antha vennilava seevi seevi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antha vennilava seevi seevi"/>
</div>
<div class="lyrico-lyrics-wrapper">senjan ah un thola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="senjan ah un thola"/>
</div>
<div class="lyrico-lyrics-wrapper">vella vella nu iruka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vella vella nu iruka"/>
</div>
<div class="lyrico-lyrics-wrapper">vellakaari pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vellakaari pola"/>
</div>
<div class="lyrico-lyrics-wrapper">antha vennilava seevi seevi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antha vennilava seevi seevi"/>
</div>
<div class="lyrico-lyrics-wrapper">senjan ah un thola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="senjan ah un thola"/>
</div>
<div class="lyrico-lyrics-wrapper">kathi eduthu keeruna than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathi eduthu keeruna than"/>
</div>
<div class="lyrico-lyrics-wrapper">sevakkum ennoda odambu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sevakkum ennoda odambu"/>
</div>
<div class="lyrico-lyrics-wrapper">kaathu lasaa aduchale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaathu lasaa aduchale"/>
</div>
<div class="lyrico-lyrics-wrapper">unaku theriyuthu pacha narambu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unaku theriyuthu pacha narambu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">enaku nee thandi devathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enaku nee thandi devathai"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu poiya irunthalum thevala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu poiya irunthalum thevala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">karumbu charu oothi oothi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karumbu charu oothi oothi"/>
</div>
<div class="lyrico-lyrics-wrapper">evandi senjan un kurala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="evandi senjan un kurala"/>
</div>
<div class="lyrico-lyrics-wrapper">nooru carrot thuruvi thuruvi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nooru carrot thuruvi thuruvi"/>
</div>
<div class="lyrico-lyrics-wrapper">evandi senjan un virala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="evandi senjan un virala"/>
</div>
<div class="lyrico-lyrics-wrapper">un nilala pathu pathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un nilala pathu pathu"/>
</div>
<div class="lyrico-lyrics-wrapper">ulaga alagiyum mirala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulaga alagiyum mirala"/>
</div>
<div class="lyrico-lyrics-wrapper">un nilala pathu pathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un nilala pathu pathu"/>
</div>
<div class="lyrico-lyrics-wrapper">ulaga alagiyum mirala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulaga alagiyum mirala"/>
</div>
<div class="lyrico-lyrics-wrapper">unaku kodutha alaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unaku kodutha alaga"/>
</div>
<div class="lyrico-lyrics-wrapper">vera evalukume avan tharala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vera evalukume avan tharala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">enaku nee thandi devathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enaku nee thandi devathai"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu poiya irunthalum thevalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu poiya irunthalum thevalai"/>
</div>
</pre>
