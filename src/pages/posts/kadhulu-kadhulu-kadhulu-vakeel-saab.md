---
title: "kadhulu kadhulu song lyrics"
album: "Vakeel Saab"
artist: "S. Thaman"
lyricist: "Suddala Ashok Teja"
director: "Venu Sriram"
path: "/albums/vakeel-saab-lyrics"
song: "Kadhulu Kadhulu Kadhulu"
image: ../../images/albumart/vakeel-saab.jpg
date: 2021-04-09
lang: telugu
youtubeLink: "https://www.youtube.com/embed/6nDPbwOAC8k"
type: "melody"
singers:
  - Sri Krishna
  - Vedala Hemachandra
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kadhulu kadhulu kadhulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhulu kadhulu kadhulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Katlu tenchukoni kadhulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katlu tenchukoni kadhulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vadhulu vadhulu vadhulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadhulu vadhulu vadhulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Baanisa sankellanu vadhulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baanisa sankellanu vadhulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam thana kallu terichi gaalisthunnadhi neelo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam thana kallu terichi gaalisthunnadhi neelo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalika emaindhani ugra jwalika emaindhani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalika emaindhani ugra jwalika emaindhani"/>
</div>
<div class="lyrico-lyrics-wrapper">Debba kodithe puli nenu aadadhannantundha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Debba kodithe puli nenu aadadhannantundha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoka thokkithe naagu thananu aadadhanukuntundha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoka thokkithe naagu thananu aadadhanukuntundha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhulu kadhulu kadhulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhulu kadhulu kadhulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Katlu tenchukoni kadhulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katlu tenchukoni kadhulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vadhulu vadhulu vadhulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadhulu vadhulu vadhulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Baanisa sankellanu vadhulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baanisa sankellanu vadhulu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gaajutho gaayalu chey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaajutho gaayalu chey"/>
</div>
<div class="lyrico-lyrics-wrapper">Chunnine uri thaadu chey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chunnine uri thaadu chey"/>
</div>
<div class="lyrico-lyrics-wrapper">Rangulu pette gollane guchhe bakulu chey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangulu pette gollane guchhe bakulu chey"/>
</div>
<div class="lyrico-lyrics-wrapper">Pirithanam aavahinchi parigetthe kaalltho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirithanam aavahinchi parigetthe kaalltho"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendu thodala madhya thanni narakam parichayam chey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendu thodala madhya thanni narakam parichayam chey"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee sharirame neeku aayudha karmagaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee sharirame neeku aayudha karmagaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Bathuku samar bhumilo neeku neeve sainyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bathuku samar bhumilo neeku neeve sainyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhulu kadhulu kadhulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhulu kadhulu kadhulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Katlu tenchukoni kadhulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katlu tenchukoni kadhulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vadhulu vadhulu vadhulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadhulu vadhulu vadhulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Baanisa sankellanu vadhulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baanisa sankellanu vadhulu"/>
</div>
</pre>
