---
title: "anaamika song lyrics"
album: "Mouna Guru"
artist: "S Thaman"
lyricist: "unknown"
director: "Shantha Kumar"
path: "/albums/mouna-guru-lyrics"
song: "Anaamika"
image: ../../images/albumart/mouna-guru.jpg
date: 2011-12-16
lang: tamil
youtubeLink: "https://www.youtube.com/embed/0jLl-K3XfN4"
type: "love"
singers:
  - thaman
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hey anaamika hey anaamika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey anaamika hey anaamika"/>
</div>
<div class="lyrico-lyrics-wrapper">adimanaveligalil anaamika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adimanaveligalil anaamika"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey anaamika hey anaamika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey anaamika hey anaamika"/>
</div>
<div class="lyrico-lyrics-wrapper">alaiyena alaindhidum anaamika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alaiyena alaindhidum anaamika"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey anaamika hey anaamika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey anaamika hey anaamika"/>
</div>
<div class="lyrico-lyrics-wrapper">adai mazhai kudai yena anaamika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adai mazhai kudai yena anaamika"/>
</div>
<div class="lyrico-lyrics-wrapper">hey anaamika hey anaamika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey anaamika hey anaamika"/>
</div>
<div class="lyrico-lyrics-wrapper">araiyinil piraiyena anaamika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="araiyinil piraiyena anaamika"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En idhaya thisaimaani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En idhaya thisaimaani"/>
</div>
<div class="lyrico-lyrics-wrapper">kaatuginra thisaiyil ne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatuginra thisaiyil ne"/>
</div>
<div class="lyrico-lyrics-wrapper">ennavenru avathaani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennavenru avathaani"/>
</div>
<div class="lyrico-lyrics-wrapper">kaadhal thaana?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadhal thaana?"/>
</div>
<div class="lyrico-lyrics-wrapper">Un vizhiyil vaazhvenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un vizhiyil vaazhvenaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un nizhalil veelvena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un nizhalil veelvena"/>
</div>
<div class="lyrico-lyrics-wrapper">kelviketkum nenjodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kelviketkum nenjodu"/>
</div>
<div class="lyrico-lyrics-wrapper">kaadhal thaana?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadhal thaana?"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey hey un arugile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hey un arugile"/>
</div>
<div class="lyrico-lyrics-wrapper">nodigalin idaiveli perugida kandaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nodigalin idaiveli perugida kandaen"/>
</div>
<div class="lyrico-lyrics-wrapper">hey hey un arugile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey hey un arugile"/>
</div>
<div class="lyrico-lyrics-wrapper">puthu oru uravinai arumbidak kandaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthu oru uravinai arumbidak kandaen"/>
</div>
<div class="lyrico-lyrics-wrapper">hey hey un arugile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey hey un arugile"/>
</div>
<div class="lyrico-lyrics-wrapper">unai unai neeye virumbidak kandaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unai unai neeye virumbidak kandaen"/>
</div>
<div class="lyrico-lyrics-wrapper">oh oh en kanavile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oh oh en kanavile"/>
</div>
<div class="lyrico-lyrics-wrapper">or irudhaya peyarchiyai kandaene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="or irudhaya peyarchiyai kandaene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey anaamika hey anaamika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey anaamika hey anaamika"/>
</div>
<div class="lyrico-lyrics-wrapper">adimanaveligalil anaamika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adimanaveligalil anaamika"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey anaamika hey anaamika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey anaamika hey anaamika"/>
</div>
<div class="lyrico-lyrics-wrapper">alaiyena alaindhidum anaamika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alaiyena alaindhidum anaamika"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En idhaya thisaimaani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En idhaya thisaimaani"/>
</div>
<div class="lyrico-lyrics-wrapper">kaatuginra thisaiyil ne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatuginra thisaiyil ne"/>
</div>
<div class="lyrico-lyrics-wrapper">ennavenru avathaani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennavenru avathaani"/>
</div>
<div class="lyrico-lyrics-wrapper">kaadhal thaana?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadhal thaana?"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaarodum kaana ondrai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarodum kaana ondrai"/>
</div>
<div class="lyrico-lyrics-wrapper">yen unnil naanum kandaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yen unnil naanum kandaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey un udalmozhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey un udalmozhi"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhal mozhiyuthe!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhal mozhiyuthe!"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyirodu yeno inru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirodu yeno inru"/>
</div>
<div class="lyrico-lyrics-wrapper">vannangalin kooda kanden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vannangalin kooda kanden"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey un ethiroli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey un ethiroli"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjil pathiyuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjil pathiyuthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhinasari kanavathan unavena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinasari kanavathan unavena"/>
</div>
<div class="lyrico-lyrics-wrapper">unaitharum ninaivugal thekkugiren!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unaitharum ninaivugal thekkugiren!"/>
</div>
<div class="lyrico-lyrics-wrapper">hey un araikurai uraigalai karaiyumun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey un araikurai uraigalai karaiyumun"/>
</div>
<div class="lyrico-lyrics-wrapper">uraisirai araigalil pootugiren!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uraisirai araigalil pootugiren!"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey hey un arugile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hey un arugile"/>
</div>
<div class="lyrico-lyrics-wrapper">unaiunai neeye virumbidak kandaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unaiunai neeye virumbidak kandaen"/>
</div>
<div class="lyrico-lyrics-wrapper">oh oh en kanavile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oh oh en kanavile"/>
</div>
<div class="lyrico-lyrics-wrapper">or irudhayapeyarchiyai kandaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="or irudhayapeyarchiyai kandaen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey anaamika hey anaamika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey anaamika hey anaamika"/>
</div>
<div class="lyrico-lyrics-wrapper">adimanaveligalil anaamika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adimanaveligalil anaamika"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey anaamika hey anaamika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey anaamika hey anaamika"/>
</div>
<div class="lyrico-lyrics-wrapper">alaiyena alaindhidum anaamika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alaiyena alaindhidum anaamika"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey anaamika hey anaamika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey anaamika hey anaamika"/>
</div>
<div class="lyrico-lyrics-wrapper">thanna nana nan na na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanna nana nan na na"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey anaamika hey anaamika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey anaamika hey anaamika"/>
</div>
<div class="lyrico-lyrics-wrapper">kaadhal thaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadhal thaana"/>
</div>
</pre>
