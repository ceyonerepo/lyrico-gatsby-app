---
title: "aahaa song lyrics"
album: "Vaazhl"
artist: "Pradeep Kumar"
lyricist: "Pradeep Kumar"
director: "Arun Prabu Purushothaman"
path: "/albums/vaazhl-lyrics"
song: "Aahaa"
image: ../../images/albumart/vaazhl.jpg
date: 2021-07-16
lang: tamil
youtubeLink: "https://www.youtube.com/embed/bzDyCRQQWZs"
type: "happy"
singers:
  - Pradeep Kumar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hooohooohooooohooo..aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hooohooohooooohooo..aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hooohooohoo..hooohoooaa.
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hooohooohoo..hooohoooaa."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanaa…..aaa..aaa.aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanaa…..aaa..aaa.aaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pongi odudhey…ye….ye…..ye…ye…..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pongi odudhey…ye….ye…..ye…ye….."/>
</div>
<div class="lyrico-lyrics-wrapper">Then….mazhai pozhindhadhey…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Then….mazhai pozhindhadhey…"/>
</div>
<div class="lyrico-lyrics-wrapper">Poo…adhai rusiththadhey………
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poo…adhai rusiththadhey………"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaha…….aaha….aaha….
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaha…….aaha….aaha…."/>
</div>
<div class="lyrico-lyrics-wrapper">Meiiiiiiiiiii…..marandhen………..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meiiiiiiiiiii…..marandhen……….."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanaa…..aaa..aaa.aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanaa…..aaa..aaa.aaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pongi odudhey…ye….ye…..ye…ye…..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pongi odudhey…ye….ye…..ye…ye….."/>
</div>
<div class="lyrico-lyrics-wrapper">Kanaa…..aaa..aaa.aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanaa…..aaa..aaa.aaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pongi odudhey…ye….ye…..ye…ye…..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pongi odudhey…ye….ye…..ye…ye….."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paar……Enai Kavarndhadhey…ye…ye.ye.ye….
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paar……Enai Kavarndhadhey…ye…ye.ye.ye…."/>
</div>
<div class="lyrico-lyrics-wrapper">Vennn.nila Siriththadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vennn.nila Siriththadhey"/>
</div>
<div class="lyrico-lyrics-wrapper">hey……hey…….heyhae……..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey……hey…….heyhae…….."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaha….aaha……..aaha…….aaha……
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaha….aaha……..aaha…….aaha……"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaha….ha…..ha……aahaaaa..haaha….
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaha….ha…..ha……aahaaaa..haaha…."/>
</div>
<div class="lyrico-lyrics-wrapper">Aha….aaha……..aaha…….aaha……
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aha….aaha……..aaha…….aaha……"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaha….ha…..ha……aahaaaa..haaha….
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaha….ha…..ha……aahaaaa..haaha…."/>
</div>
<div class="lyrico-lyrics-wrapper">Aaaha.Aaha….ha…..ha……aahaaaa..haaha….
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaha.Aaha….ha…..ha……aahaaaa..haaha…."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaha….aaha……..aaha…….aaha……
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaha….aaha……..aaha…….aaha……"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaha….ha…..ha……aahaaaa..haaha….
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaha….ha…..ha……aahaaaa..haaha…."/>
</div>
<div class="lyrico-lyrics-wrapper">Aha….aaha……..aaha…….aaha……
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aha….aaha……..aaha…….aaha……"/>
</div>
<div class="lyrico-lyrics-wrapper">Meiiiiiiiiiii………..marandhen………..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meiiiiiiiiiii………..marandhen……….."/>
</div>
<div class="lyrico-lyrics-wrapper">Meiiiiiiiiiii………..unarndhen………..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meiiiiiiiiiii………..unarndhen……….."/>
</div>
</pre>
