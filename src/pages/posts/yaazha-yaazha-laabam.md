---
title: "yaazha yaazha song lyrics"
album: "Laabam"
artist: "D.Imman"
lyricist: "Yugabharathi"
director: "SP. Jhananathan"
path: "/albums/laabam-lyrics"
song: "Yaazha Yaazha"
image: ../../images/albumart/laabam.jpg
date: 2021-09-09
lang: tamil
youtubeLink: "https://www.youtube.com/embed/AVaYeZQI5GQ"
type: "happy"
singers:
  - Shruti Haasan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yaazha yaazha kaadhal yaazhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaazha yaazha kaadhal yaazhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalum ennai meetu thozhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalum ennai meetu thozhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir oyaamal nee korukkum raagangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir oyaamal nee korukkum raagangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Neelum neelum vaazh naala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelum neelum vaazh naala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaazha yaazha kaadhal yaazhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaazha yaazha kaadhal yaazhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalum ennai meetu thozhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalum ennai meetu thozhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir oyaamal nee korukkum raagangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir oyaamal nee korukkum raagangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Neelum neelum vaazh naala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelum neelum vaazh naala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadavulumey kekaatha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadavulumey kekaatha "/>
</div>
<div class="lyrico-lyrics-wrapper">tamizhisaiyum neethaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tamizhisaiyum neethaney"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathiroliyum kaatatha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathiroliyum kaatatha "/>
</div>
<div class="lyrico-lyrics-wrapper">vidiyalai kaatum unnai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidiyalai kaatum unnai"/>
</div>
<div class="lyrico-lyrics-wrapper">Sernthean Naaney Naaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sernthean Naaney Naaney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaazha yaazha kaadhal yaazhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaazha yaazha kaadhal yaazhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalum ennai meetu thozhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalum ennai meetu thozhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir oyaamal nee korukkum raagangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir oyaamal nee korukkum raagangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Neelum neelum vaazh naala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelum neelum vaazh naala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaazha yaazha kaadhal yaazhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaazha yaazha kaadhal yaazhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalum ennai meetu thozhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalum ennai meetu thozhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir oyaamal nee korukkum raagangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir oyaamal nee korukkum raagangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Neelum neelum vaazh naala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelum neelum vaazh naala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ethaiyum tharum naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethaiyum tharum naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayam tharavaa maaten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam tharavaa maaten"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirin naduvey unai naan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirin naduvey unai naan "/>
</div>
<div class="lyrico-lyrics-wrapper">vithaiyaai potten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vithaiyaai potten"/>
</div>
<div class="lyrico-lyrics-wrapper">Maghizhvaana neram ondrey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maghizhvaana neram ondrey"/>
</div>
<div class="lyrico-lyrics-wrapper">Theriyaamal mun vaazhthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theriyaamal mun vaazhthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Puyale un paarvai thotta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puyale un paarvai thotta"/>
</div>
<div class="lyrico-lyrics-wrapper">Nodi naanum mallaanthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nodi naanum mallaanthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Narai vizhum varai vaazhnthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Narai vizhum varai vaazhnthey"/>
</div>
<div class="lyrico-lyrics-wrapper">Theerpom naalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theerpom naalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Iruppathai pakirnthaaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruppathai pakirnthaaley"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarthaan yezhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarthaan yezhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Patharaaathey nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Patharaaathey nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai padaipola kaathiduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai padaipola kaathiduven"/>
</div>
<div class="lyrico-lyrics-wrapper">Piththu pidithida muththam koduthidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Piththu pidithida muththam koduthidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottidum ennai nee alli eduthidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottidum ennai nee alli eduthidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaazha yaazha kaadhal yaazhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaazha yaazha kaadhal yaazhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaazha yaazha kaadhal yaazhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaazha yaazha kaadhal yaazhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalum ennai meetu thozhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalum ennai meetu thozhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir oyaamal nee korukkum raagangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir oyaamal nee korukkum raagangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Neelum neelum vaazh naala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelum neelum vaazh naala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadavulumey kekaatha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadavulumey kekaatha "/>
</div>
<div class="lyrico-lyrics-wrapper">tamizhisaiyum neethaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tamizhisaiyum neethaney"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathiroliyum kaatatha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathiroliyum kaatatha "/>
</div>
<div class="lyrico-lyrics-wrapper">vidiyalai kaatum unnai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidiyalai kaatum unnai"/>
</div>
<div class="lyrico-lyrics-wrapper">Sernthean Naaney Naaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sernthean Naaney Naaney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaazha yaazha kaadhal yaazhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaazha yaazha kaadhal yaazhaa"/>
</div>
</pre>
