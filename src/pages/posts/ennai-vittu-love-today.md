---
title: "Ennai Vittu song lyrics"
album: "Love Today"
artist: "Yuvan Shankar Raja"
lyricist: "Pradeep Ranganathan"
director: "Pradeep Ranganathan"
path: "/albums/love-today-lyrics"
song: "Ennai Vittu"
image: ../../images/albumart/love-today.jpg
date: 2022-11-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/R3w8u-38-Eo?controls=0"
type: "Love"
singers:
  - Sid Sriram
---


<pre class="lyrics-native"></pre>
<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ennai vittu uyir ponaalum</div>
<div class="lyrico-lyrics-wrapper">Unnai vittu naan pomaaten</div>
<div class="lyrico-lyrics-wrapper">Jenmam pala eduthaalum</div>
<div class="lyrico-lyrics-wrapper">Unnai yarukum thara maaten</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai vittu uyir ponaalum</div>
<div class="lyrico-lyrics-wrapper">Unnai vittu naan pomaaten</div>
<div class="lyrico-lyrics-wrapper">Sathiyama! solluren di</div>
<div class="lyrico-lyrics-wrapper">Unnai yarukum thara maaten</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee illaa neram</div>
<div class="lyrico-lyrics-wrapper">Adhu nilavae illa vaanamae!</div>
<div class="lyrico-lyrics-wrapper">Irandum irundu pogum</div>
<div class="lyrico-lyrics-wrapper">Siru velicham thedi odumae!</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnil thulaindha ennai</div>
<div class="lyrico-lyrics-wrapper">Udanae meetukudu</div>
<div class="lyrico-lyrics-wrapper">Illai ennul neeyum</div>
<div class="lyrico-lyrics-wrapper">Azhagai udanae thulaindhuvidu</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho ooo oo oooo ho</div>
<div class="lyrico-lyrics-wrapper">Ennai vittu uyir ponaalum</div>
<div class="lyrico-lyrics-wrapper">Unnai vittu naan pomaaten</div>
<div class="lyrico-lyrics-wrapper">pomaaten</div>
<div class="lyrico-lyrics-wrapper">Jenmam pala eduthaalum</div>
<div class="lyrico-lyrics-wrapper">Unnai yarukum thara maaten</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai vittu uyir ponaalum</div>
<div class="lyrico-lyrics-wrapper">ponaalum</div>
<div class="lyrico-lyrics-wrapper">Unnai vittu naan pomaaten</div>
<div class="lyrico-lyrics-wrapper">pomaaten</div>
<div class="lyrico-lyrics-wrapper">Sathiyama! solluren di</div>
<div class="lyrico-lyrics-wrapper">Unnai yarukum thara maatenHaa..aaa.</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadal man pol nee</div>
<div class="lyrico-lyrics-wrapper">Ennai udhari sendralumae varuven</div>
<div class="lyrico-lyrics-wrapper">Alaigal polae</div>
<div class="lyrico-lyrics-wrapper">Naan thirumba thirumba</div>
<div class="lyrico-lyrics-wrapper">Un pinnae varuven..varuven</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai thedi alaigindrenae!</div>
<div class="lyrico-lyrics-wrapper">Engae sendraayo!</div>
<div class="lyrico-lyrics-wrapper">Siru pillai pole azhugindrenae!</div>
<div class="lyrico-lyrics-wrapper">Thiruppi varuvaayo</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhiyoram vazhiyum kanneerukku</div>
<div class="lyrico-lyrics-wrapper">Valigal aayiram</div>
<div class="lyrico-lyrics-wrapper">Andha valigalai thudaikka</div>
<div class="lyrico-lyrics-wrapper">Pirandhavan naan di</div>
<div class="lyrico-lyrics-wrapper">Nambudi neeyum</div>
<div class="lyrico-lyrics-wrapper">Unna namburen naanum</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai vittu uyir ponaalum</div>
<div class="lyrico-lyrics-wrapper">ponaalum</div>
<div class="lyrico-lyrics-wrapper">Unnai vittu naan pomaaten</div>
<div class="lyrico-lyrics-wrapper">pomaaten</div>
<div class="lyrico-lyrics-wrapper">Jenmam pala eduthaalum</div>
<div class="lyrico-lyrics-wrapper">Unnai yarukum thara maaten</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai vittu uyir ponaalum</div>
<div class="lyrico-lyrics-wrapper">Unnai vittu naan pomaaten</div>
<div class="lyrico-lyrics-wrapper">Sathiyama! solluren di</div>
<div class="lyrico-lyrics-wrapper">Unnai yarukum thara maaten</div></pre>