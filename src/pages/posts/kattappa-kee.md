---
title: "kattappa song lyrics"
album: "Kee"
artist: "Vishal Chandrasekhar"
lyricist: "Thamarai"
director: "Kalees"
path: "/albums/kee-lyrics"
song: "Kattappa"
image: ../../images/albumart/kee.jpg
date: 2019-05-10
lang: tamil
youtubeLink: "https://www.youtube.com/embed/lTSCFrwvt70"
type: "mass"
singers:
  - Krishnaprasad
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Retta vaaludaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Retta vaaludaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ragalaiyaana aaludaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ragalaiyaana aaludaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Natpunu vandhaakka fevicoludaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natpunu vandhaakka fevicoludaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Coding thatti vittaa ellamae cooludaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Coding thatti vittaa ellamae cooludaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Rule-sa odaikkuradhae en rule-udaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rule-sa odaikkuradhae en rule-udaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kattappa kattappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattappa kattappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaththi yedukkaadhappaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaththi yedukkaadhappaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga cut adichaa moraikkaadhappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga cut adichaa moraikkaadhappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thatti kodutha yetti parappom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thatti kodutha yetti parappom"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattu kadangaadha kootamappaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattu kadangaadha kootamappaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey kattappa kattappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey kattappa kattappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaththi yedukkaadhappaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaththi yedukkaadhappaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga cut adichaa moraikkaadhappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga cut adichaa moraikkaadhappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thatti kodutha yetti parappom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thatti kodutha yetti parappom"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattu kadangaadha kootamappaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattu kadangaadha kootamappaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadaisi benchu pakkamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadaisi benchu pakkamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee vandhu nillu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee vandhu nillu"/>
</div>
<div class="lyrico-lyrics-wrapper">Munnera vazhi sollurom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munnera vazhi sollurom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thonaiyaa canteenukku vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thonaiyaa canteenukku vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga accountula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga accountula"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkum tea sollurom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkum tea sollurom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veriyaaga theriyaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veriyaaga theriyaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinam kondadurom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam kondadurom"/>
</div>
<div class="lyrico-lyrics-wrapper">Tinderula thindaadurom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tinderula thindaadurom"/>
</div>
<div class="lyrico-lyrics-wrapper">Notice board-u koottam koodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Notice board-u koottam koodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Arrear vizhundhaakkaa allaadurom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arrear vizhundhaakkaa allaadurom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kattappa kattappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattappa kattappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaththi yedukkaadhappaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaththi yedukkaadhappaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga cut adichaa moraikkaadhappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga cut adichaa moraikkaadhappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thatti kodutha yetti parappom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thatti kodutha yetti parappom"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattu kadangaadha kootamappaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattu kadangaadha kootamappaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Love-vu bow-vu aachunaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love-vu bow-vu aachunaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam vittupudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam vittupudi"/>
</div>
<div class="lyrico-lyrics-wrapper">Life is long-u machi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life is long-u machi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Friend-u gaandu yethinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friend-u gaandu yethinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kattipudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kattipudi"/>
</div>
<div class="lyrico-lyrics-wrapper">Natpae strong-u machi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natpae strong-u machi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Break up-u top up-u packupkellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Break up-u top up-u packupkellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Mersal aavaadhadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mersal aavaadhadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Siricha cool-u pudichaa dhool-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siricha cool-u pudichaa dhool-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Mavanae morachakka dead poolu daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mavanae morachakka dead poolu daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kattappa kattappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattappa kattappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaththi yedukkaadhappaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaththi yedukkaadhappaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga cut adichaa moraikkaadhappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga cut adichaa moraikkaadhappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thatti kodutha yetti parappom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thatti kodutha yetti parappom"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattu kadangaadha kootamappaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattu kadangaadha kootamappaa"/>
</div>
</pre>
