---
title: "thimiru pudichavan song lyrics"
album: "Thimiru Pudichavan"
artist: "Vijay Antony"
lyricist: "Arun Bharathi"
director: "Ganeshaa"
path: "/albums/thimiru-pudichavan-lyrics"
song: "Thimiru Pudichavan"
image: ../../images/albumart/thimiru-pudichavan.jpg
date: 2018-11-16
lang: tamil
youtubeLink: "https://www.youtube.com/embed/lc6uHSCo-Fk"
type: "intro song"
singers:
  - Rahul Nambiar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ding ding ding
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ding ding ding"/>
</div>
<div class="lyrico-lyrics-wrapper">Daga dang dang dang
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daga dang dang dang"/>
</div>
<div class="lyrico-lyrics-wrapper">Digi ding ding ding
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Digi ding ding ding"/>
</div>
<div class="lyrico-lyrics-wrapper">Daga dang dang dang
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daga dang dang dang"/>
</div>
<div class="lyrico-lyrics-wrapper">Digi ding ding ding
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Digi ding ding ding"/>
</div>
<div class="lyrico-lyrics-wrapper">Daga dang dang dang
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daga dang dang dang"/>
</div>
<div class="lyrico-lyrics-wrapper">Ding ding ding 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ding ding ding "/>
</div>
<div class="lyrico-lyrics-wrapper">ding ding ding
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ding ding ding"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ding ding ding
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ding ding ding"/>
</div>
<div class="lyrico-lyrics-wrapper">Daga dang dang dang
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daga dang dang dang"/>
</div>
<div class="lyrico-lyrics-wrapper">Digi ding ding ding
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Digi ding ding ding"/>
</div>
<div class="lyrico-lyrics-wrapper">Daga dang dang dang
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daga dang dang dang"/>
</div>
<div class="lyrico-lyrics-wrapper">Digi ding ding ding
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Digi ding ding ding"/>
</div>
<div class="lyrico-lyrics-wrapper">Daga dang dang dang
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daga dang dang dang"/>
</div>
<div class="lyrico-lyrics-wrapper">Ding ding ding 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ding ding ding "/>
</div>
<div class="lyrico-lyrics-wrapper">ding ding ding
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ding ding ding"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thimiru pudichavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thimiru pudichavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan thimirukkae pudichavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan thimirukkae pudichavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thimiru pudichavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thimiru pudichavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan thimirukkae pudichavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan thimirukkae pudichavan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nermaiyil nadappavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nermaiyil nadappavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vervaiyil uzhaippavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vervaiyil uzhaippavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoongamal vizhippavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoongamal vizhippavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooyamal uzhaippavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooyamal uzhaippavan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vazhaiyamal iruppavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhaiyamal iruppavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhai poga maruppavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhai poga maruppavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaayangal poruppavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaayangal poruppavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kobathil vedippavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kobathil vedippavan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ding ding ding
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ding ding ding"/>
</div>
<div class="lyrico-lyrics-wrapper">Daga dang dang dang
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daga dang dang dang"/>
</div>
<div class="lyrico-lyrics-wrapper">Digi ding ding ding
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Digi ding ding ding"/>
</div>
<div class="lyrico-lyrics-wrapper">Daga dang dang dang
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daga dang dang dang"/>
</div>
<div class="lyrico-lyrics-wrapper">Digi ding ding ding
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Digi ding ding ding"/>
</div>
<div class="lyrico-lyrics-wrapper">Daga dang dang dang
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daga dang dang dang"/>
</div>
<div class="lyrico-lyrics-wrapper">Ding ding ding 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ding ding ding "/>
</div>
<div class="lyrico-lyrics-wrapper">ding ding ding
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ding ding ding"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thimiru pudichavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thimiru pudichavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan thimirukkae pudichavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan thimirukkae pudichavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thimiru pudichavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thimiru pudichavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan thimirukkae pudichavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan thimirukkae pudichavan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poliya yaaraiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poliya yaaraiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Potri pesa maataan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Potri pesa maataan"/>
</div>
<div class="lyrico-lyrics-wrapper">Poligal yaaraiyum kudavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poligal yaaraiyum kudavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaikka maataan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaikka maataan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullathai pesuvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullathai pesuvaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Oomaiyaga maataan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oomaiyaga maataan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nallathai seiyavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nallathai seiyavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Neram paakka maataan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neram paakka maataan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thunithavan vazhvil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunithavan vazhvil"/>
</div>
<div class="lyrico-lyrics-wrapper">Thozhvigal illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozhvigal illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunivukku vaguppugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunivukku vaguppugal"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeduthiduvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeduthiduvaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Bayanthavan vazhvil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bayanthavan vazhvil "/>
</div>
<div class="lyrico-lyrics-wrapper">vetrigal illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetrigal illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Yemanukku maranam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yemanukku maranam "/>
</div>
<div class="lyrico-lyrics-wrapper">koduthiduvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koduthiduvaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ding ding ding
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ding ding ding"/>
</div>
<div class="lyrico-lyrics-wrapper">Daga dang dang dang
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daga dang dang dang"/>
</div>
<div class="lyrico-lyrics-wrapper">Digi ding ding ding
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Digi ding ding ding"/>
</div>
<div class="lyrico-lyrics-wrapper">Daga dang dang dang
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daga dang dang dang"/>
</div>
<div class="lyrico-lyrics-wrapper">Digi ding ding ding
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Digi ding ding ding"/>
</div>
<div class="lyrico-lyrics-wrapper">Daga dang dang dang
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daga dang dang dang"/>
</div>
<div class="lyrico-lyrics-wrapper">Ding ding ding 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ding ding ding "/>
</div>
<div class="lyrico-lyrics-wrapper">ding ding ding
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ding ding ding"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ding ding ding
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ding ding ding"/>
</div>
<div class="lyrico-lyrics-wrapper">Daga dang dang dang
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daga dang dang dang"/>
</div>
<div class="lyrico-lyrics-wrapper">Digi ding ding ding
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Digi ding ding ding"/>
</div>
<div class="lyrico-lyrics-wrapper">Daga dang dang dang
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daga dang dang dang"/>
</div>
<div class="lyrico-lyrics-wrapper">Digi ding ding ding
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Digi ding ding ding"/>
</div>
<div class="lyrico-lyrics-wrapper">Daga dang dang dang
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daga dang dang dang"/>
</div>
<div class="lyrico-lyrics-wrapper">Ding ding ding 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ding ding ding "/>
</div>
<div class="lyrico-lyrics-wrapper">ding ding ding
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ding ding ding"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thimiru pudichavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thimiru pudichavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan thimirukkae pudichavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan thimirukkae pudichavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thimiru pudichavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thimiru pudichavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan thimirukkae pudichavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan thimirukkae pudichavan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nermaiyil nadappavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nermaiyil nadappavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vervaiyil uzhaippavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vervaiyil uzhaippavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoongamal vizhippavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoongamal vizhippavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooyamal uzhaippavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooyamal uzhaippavan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vazhaiyamal iruppavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhaiyamal iruppavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhai poga maruppavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhai poga maruppavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaayangal poruppavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaayangal poruppavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kobathil vedippavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kobathil vedippavan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ding ding ding
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ding ding ding"/>
</div>
<div class="lyrico-lyrics-wrapper">Daga dang dang dang
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daga dang dang dang"/>
</div>
<div class="lyrico-lyrics-wrapper">Digi ding ding ding
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Digi ding ding ding"/>
</div>
<div class="lyrico-lyrics-wrapper">Daga dang dang dang
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daga dang dang dang"/>
</div>
<div class="lyrico-lyrics-wrapper">Digi ding ding ding
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Digi ding ding ding"/>
</div>
<div class="lyrico-lyrics-wrapper">Daga dang dang dang
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daga dang dang dang"/>
</div>
<div class="lyrico-lyrics-wrapper">Ding ding ding 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ding ding ding "/>
</div>
<div class="lyrico-lyrics-wrapper">ding ding ding
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ding ding ding"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ding ding ding
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ding ding ding"/>
</div>
<div class="lyrico-lyrics-wrapper">Daga dang dang dang
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daga dang dang dang"/>
</div>
<div class="lyrico-lyrics-wrapper">Digi ding ding ding
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Digi ding ding ding"/>
</div>
<div class="lyrico-lyrics-wrapper">Daga dang dang dang
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daga dang dang dang"/>
</div>
<div class="lyrico-lyrics-wrapper">Digi ding ding ding
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Digi ding ding ding"/>
</div>
<div class="lyrico-lyrics-wrapper">Daga dang dang dang
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daga dang dang dang"/>
</div>
<div class="lyrico-lyrics-wrapper">Ding ding ding 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ding ding ding "/>
</div>
<div class="lyrico-lyrics-wrapper">ding ding ding
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ding ding ding"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ding ding ding
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ding ding ding"/>
</div>
<div class="lyrico-lyrics-wrapper">Daga dang dang dang
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daga dang dang dang"/>
</div>
<div class="lyrico-lyrics-wrapper">Digi ding ding ding
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Digi ding ding ding"/>
</div>
<div class="lyrico-lyrics-wrapper">Daga dang dang dang
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daga dang dang dang"/>
</div>
<div class="lyrico-lyrics-wrapper">Digi ding ding ding
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Digi ding ding ding"/>
</div>
<div class="lyrico-lyrics-wrapper">Daga dang dang dang
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daga dang dang dang"/>
</div>
<div class="lyrico-lyrics-wrapper">Ding ding ding 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ding ding ding "/>
</div>
<div class="lyrico-lyrics-wrapper">ding ding ding
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ding ding ding"/>
</div>
</pre>
