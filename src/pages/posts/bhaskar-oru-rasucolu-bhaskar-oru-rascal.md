---
title: "bhaskar oru rasucolu song lyrics"
album: "Bhaskar Oru Rascal"
artist: "Amresh Ganesh"
lyricist: "Karunakaran"
director: "Siddique"
path: "/albums/bhaskar-oru-rascal-lyrics"
song: "Bhaskar Oru Rasucolu"
image: ../../images/albumart/bhaskar-oru-rascal.jpg
date: 2018-05-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/DrWLQ0zypAI"
type: "happy"
singers:
  - Amresh Ganesh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Naan nallavananu theriyathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan nallavananu theriyathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kettavanaanu theriyathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kettavanaanu theriyathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Good boyannu theriyathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Good boyannu theriyathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhaskar uru rasucolu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhaskar uru rasucolu"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhaskar oru rasucolu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhaskar oru rasucolu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Immam periya aalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Immam periya aalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil paasam poduthae dealu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil paasam poduthae dealu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna maari dealing
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna maari dealing"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayyo yetho yetho feeling
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyo yetho yetho feeling"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bhaskar oru rasucolu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhaskar oru rasucolu"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhaskar oru rasucolu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhaskar oru rasucolu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan yaaru nee konjam kaattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan yaaru nee konjam kaattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada maari pochu en roottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada maari pochu en roottu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bhaskar oru rasucolu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhaskar oru rasucolu"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhaskar oru rasucolu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhaskar oru rasucolu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayyo idhu oru maari feeling
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyo idhu oru maari feeling"/>
</div>
<div class="lyrico-lyrics-wrapper">Yetho maari yetho maari feeling
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yetho maari yetho maari feeling"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalangatheda kalangatheda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalangatheda kalangatheda"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu oru idhu oru type feeling
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu oru idhu oru type feeling"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bhaskar oru rasucolu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhaskar oru rasucolu"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhaskar oru rasucolu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhaskar oru rasucolu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan yaaru nee konjam kaattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan yaaru nee konjam kaattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada maari pochu en roottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada maari pochu en roottu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada rendum rendum naalagidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada rendum rendum naalagidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhka kulfia coolagidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhka kulfia coolagidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathium aruvavum than poyidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathium aruvavum than poyidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhusa life-u turn aagidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhusa life-u turn aagidum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manasae yengiduchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasae yengiduchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Rasikka thoniduchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rasikka thoniduchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Venam boss-u…venam boss-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venam boss-u…venam boss-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Aayiduvan pola paathi loosu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayiduvan pola paathi loosu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bhaskar oru rasucolu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhaskar oru rasucolu"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhaskar oru rasucolu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhaskar oru rasucolu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan yaaru nee konjam kaattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan yaaru nee konjam kaattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada maari pochu en roottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada maari pochu en roottu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thudukku thudkku thudukku thukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thudukku thudkku thudukku thukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Sillu chikka ammukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sillu chikka ammukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Thudukku thudkku thudukku thukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thudukku thudkku thudukku thukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Sillu chikka ammukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sillu chikka ammukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vetkatha vittu pesa thonum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetkatha vittu pesa thonum"/>
</div>
<div class="lyrico-lyrics-wrapper">Yetho onnula ninnae naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yetho onnula ninnae naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Right-a wrong-a rendu gunam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Right-a wrong-a rendu gunam"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendum ketta mirandan naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendum ketta mirandan naanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meendum re-entry-ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meendum re-entry-ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhukku feel panri-ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhukku feel panri-ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Venam boss-u venam boss-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venam boss-u venam boss-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhka eppothum namma tastu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhka eppothum namma tastu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan yaaru nee konjam kaattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan yaaru nee konjam kaattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada maari pochu en roottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada maari pochu en roottu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bhaskar oru rasucolu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhaskar oru rasucolu"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhaskar oru rasucolu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhaskar oru rasucolu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayyo idhu oru maari feeling
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyo idhu oru maari feeling"/>
</div>
<div class="lyrico-lyrics-wrapper">Yetho maari yetho maari feeling
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yetho maari yetho maari feeling"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalangatheda kalangatheda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalangatheda kalangatheda"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu oru idhu oru type feeling
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu oru idhu oru type feeling"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bhaskar oru rasucolu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhaskar oru rasucolu"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhaskar oru rasucolu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhaskar oru rasucolu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan yaaru nee konjam kaattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan yaaru nee konjam kaattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada maari pochu en roottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada maari pochu en roottu"/>
</div>
</pre>
