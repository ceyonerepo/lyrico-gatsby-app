---
title: "sarvesaa song lyrics"
album: "Sangathalaivan"
artist: "Robert Sargunam"
lyricist: "Yugabharathi"
director: "Manimaaran"
path: "/albums/sangathalaivan-song-lyrics"
song: "Sarvesaa"
image: ../../images/albumart/sangathalaivan.jpg
date: 2021-02-26
lang: tamil
youtubeLink: "https://www.youtube.com/embed/_U_SGRfkdAg"
type: "Motivational"
singers:
  - Jayamoorthy
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">yethana yethana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yethana yethana"/>
</div>
<div class="lyrico-lyrics-wrapper">anniya company
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anniya company"/>
</div>
<div class="lyrico-lyrics-wrapper">paaren sarvesaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paaren sarvesaa"/>
</div>
<div class="lyrico-lyrics-wrapper">nalla paaren sarvesaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nalla paaren sarvesaa"/>
</div>
<div class="lyrico-lyrics-wrapper">athula onnaachum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athula onnaachum"/>
</div>
<div class="lyrico-lyrics-wrapper">uruppadiyaa kooren sarvesaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uruppadiyaa kooren sarvesaa"/>
</div>
<div class="lyrico-lyrics-wrapper">yethana yethana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yethana yethana"/>
</div>
<div class="lyrico-lyrics-wrapper">anniya company
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anniya company"/>
</div>
<div class="lyrico-lyrics-wrapper">paaren sarvesaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paaren sarvesaa"/>
</div>
<div class="lyrico-lyrics-wrapper">nalla paaren sarvesaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nalla paaren sarvesaa"/>
</div>
<div class="lyrico-lyrics-wrapper">athula onnaachum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athula onnaachum"/>
</div>
<div class="lyrico-lyrics-wrapper">uruppadiyaa kooren sarvesaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uruppadiyaa kooren sarvesaa"/>
</div>
<div class="lyrico-lyrics-wrapper">angayum ingayum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="angayum ingayum"/>
</div>
<div class="lyrico-lyrics-wrapper">yethana manthiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yethana manthiri"/>
</div>
<div class="lyrico-lyrics-wrapper">paaren sarvesaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paaren sarvesaa"/>
</div>
<div class="lyrico-lyrics-wrapper">neeyum paaren sarvesaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeyum paaren sarvesaa"/>
</div>
<div class="lyrico-lyrics-wrapper">athula yaarachum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athula yaarachum"/>
</div>
<div class="lyrico-lyrics-wrapper">yokkiyama kooren sarvesaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yokkiyama kooren sarvesaa"/>
</div>
<div class="lyrico-lyrics-wrapper">eamaarum kaalam inee illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eamaarum kaalam inee illa"/>
</div>
<div class="lyrico-lyrics-wrapper">thozhilali vella 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thozhilali vella "/>
</div>
<div class="lyrico-lyrics-wrapper">poraada vanthom kaduppula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poraada vanthom kaduppula"/>
</div>
<div class="lyrico-lyrics-wrapper">perum koobam ulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="perum koobam ulla"/>
</div>
<div class="lyrico-lyrics-wrapper">naadala naanga thayangala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naadala naanga thayangala"/>
</div>
<div class="lyrico-lyrics-wrapper">parakottu mella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="parakottu mella"/>
</div>
<div class="lyrico-lyrics-wrapper">sernthomey onna sivappula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sernthomey onna sivappula"/>
</div>
<div class="lyrico-lyrics-wrapper">thesa ettum solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thesa ettum solla"/>
</div>
<div class="lyrico-lyrics-wrapper">yethana yethana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yethana yethana"/>
</div>
<div class="lyrico-lyrics-wrapper">anniya company
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anniya company"/>
</div>
<div class="lyrico-lyrics-wrapper">paaren sarvesaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paaren sarvesaa"/>
</div>
<div class="lyrico-lyrics-wrapper">nalla paaren sarvesaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nalla paaren sarvesaa"/>
</div>
<div class="lyrico-lyrics-wrapper">athula onnaachum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athula onnaachum"/>
</div>
<div class="lyrico-lyrics-wrapper">uruppadiyaa kooren sarvesaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uruppadiyaa kooren sarvesaa"/>
</div>
<div class="lyrico-lyrics-wrapper">yethana yethana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yethana yethana"/>
</div>
<div class="lyrico-lyrics-wrapper">anniya company
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anniya company"/>
</div>
<div class="lyrico-lyrics-wrapper">paaren sarvesaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paaren sarvesaa"/>
</div>
<div class="lyrico-lyrics-wrapper">nalla paaren sarvesaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nalla paaren sarvesaa"/>
</div>
<div class="lyrico-lyrics-wrapper">athula onnaachum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athula onnaachum"/>
</div>
<div class="lyrico-lyrics-wrapper">uruppadiyaa kooren sarvesaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uruppadiyaa kooren sarvesaa"/>
</div>
<div class="lyrico-lyrics-wrapper">koolikkaga paadupattom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koolikkaga paadupattom"/>
</div>
<div class="lyrico-lyrics-wrapper">koozh kudikka lolu pattom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koozh kudikka lolu pattom"/>
</div>
<div class="lyrico-lyrics-wrapper">koolikkaga paadupattom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koolikkaga paadupattom"/>
</div>
<div class="lyrico-lyrics-wrapper">koozh kudikka lolu pattom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koozh kudikka lolu pattom"/>
</div>
<div class="lyrico-lyrics-wrapper">vela seiyya oora vittom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vela seiyya oora vittom"/>
</div>
<div class="lyrico-lyrics-wrapper">vera aala ulla vittom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vera aala ulla vittom"/>
</div>
<div class="lyrico-lyrics-wrapper">vela seiyya oora vittom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vela seiyya oora vittom"/>
</div>
<div class="lyrico-lyrics-wrapper">vera aala ulla vittom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vera aala ulla vittom"/>
</div>
<div class="lyrico-lyrics-wrapper">kaadukalani kaayavittom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadukalani kaayavittom"/>
</div>
<div class="lyrico-lyrics-wrapper">corporate-a meyavittom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="corporate-a meyavittom"/>
</div>
<div class="lyrico-lyrics-wrapper">kaadukalani kaayavittom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadukalani kaayavittom"/>
</div>
<div class="lyrico-lyrics-wrapper">corporate-a meyavittom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="corporate-a meyavittom"/>
</div>
<div class="lyrico-lyrics-wrapper">maadumanayai neengi vittom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maadumanayai neengi vittom"/>
</div>
<div class="lyrico-lyrics-wrapper">maanamkettu thoongi vittom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maanamkettu thoongi vittom"/>
</div>
<div class="lyrico-lyrics-wrapper">maadumanayai neengi vittom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maadumanayai neengi vittom"/>
</div>
<div class="lyrico-lyrics-wrapper">maanamkettu thoongi vittom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maanamkettu thoongi vittom"/>
</div>
<div class="lyrico-lyrics-wrapper">keezhadiya moodi maraikiraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="keezhadiya moodi maraikiraan"/>
</div>
<div class="lyrico-lyrics-wrapper">aniyaayamaiyaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aniyaayamaiyaa "/>
</div>
<div class="lyrico-lyrics-wrapper">vantheri natta kedukkuran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vantheri natta kedukkuran"/>
</div>
<div class="lyrico-lyrics-wrapper">avamaanamaiyya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avamaanamaiyya"/>
</div>
<div class="lyrico-lyrics-wrapper">meathaavi summa irukkuraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meathaavi summa irukkuraan"/>
</div>
<div class="lyrico-lyrics-wrapper">vevagaaramaiyya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vevagaaramaiyya"/>
</div>
<div class="lyrico-lyrics-wrapper">aatchi nadaththuraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aatchi nadaththuraan"/>
</div>
<div class="lyrico-lyrics-wrapper">kalikaalamaiyya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalikaalamaiyya"/>
</div>
<div class="lyrico-lyrics-wrapper">yethana yethana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yethana yethana"/>
</div>
<div class="lyrico-lyrics-wrapper">anniya company
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anniya company"/>
</div>
<div class="lyrico-lyrics-wrapper">paaren sarvesaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paaren sarvesaa"/>
</div>
<div class="lyrico-lyrics-wrapper">nalla paaren sarvesaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nalla paaren sarvesaa"/>
</div>
<div class="lyrico-lyrics-wrapper">athula onnaachum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athula onnaachum"/>
</div>
<div class="lyrico-lyrics-wrapper">uruppadiyaa kooren sarvesaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uruppadiyaa kooren sarvesaa"/>
</div>
<div class="lyrico-lyrics-wrapper">yethana yethana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yethana yethana"/>
</div>
<div class="lyrico-lyrics-wrapper">anniya company
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anniya company"/>
</div>
<div class="lyrico-lyrics-wrapper">paaren sarvesaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paaren sarvesaa"/>
</div>
<div class="lyrico-lyrics-wrapper">nalla paaren sarvesaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nalla paaren sarvesaa"/>
</div>
<div class="lyrico-lyrics-wrapper">athula onnaachum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athula onnaachum"/>
</div>
<div class="lyrico-lyrics-wrapper">uruppadiyaa kooren sarvesaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uruppadiyaa kooren sarvesaa"/>
</div>
<div class="lyrico-lyrics-wrapper">paaththaaley theettunguran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paaththaaley theettunguran"/>
</div>
<div class="lyrico-lyrics-wrapper">padikkapona neetunguran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="padikkapona neetunguran"/>
</div>
<div class="lyrico-lyrics-wrapper">paaththaaley theettunguran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paaththaaley theettunguran"/>
</div>
<div class="lyrico-lyrics-wrapper">padikkapona neetunguran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="padikkapona neetunguran"/>
</div>
<div class="lyrico-lyrics-wrapper">corparate-a koottunguran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="corparate-a koottunguran"/>
</div>
<div class="lyrico-lyrics-wrapper">nammala kanda shootunguran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nammala kanda shootunguran"/>
</div>
<div class="lyrico-lyrics-wrapper">corparate-a koottunguran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="corparate-a koottunguran"/>
</div>
<div class="lyrico-lyrics-wrapper">nammala kanda shootunguran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nammala kanda shootunguran"/>
</div>
<div class="lyrico-lyrics-wrapper">yeamaanthaa rateunguraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yeamaanthaa rateunguraan"/>
</div>
<div class="lyrico-lyrics-wrapper">yenaku podu vottunguraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yenaku podu vottunguraan"/>
</div>
<div class="lyrico-lyrics-wrapper">yeamaanthaa rateunguraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yeamaanthaa rateunguraan"/>
</div>
<div class="lyrico-lyrics-wrapper">yenaku podu vottunguraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yenaku podu vottunguraan"/>
</div>
<div class="lyrico-lyrics-wrapper">poaraatam waste unguran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poaraatam waste unguran"/>
</div>
<div class="lyrico-lyrics-wrapper">naanum un caste unguran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naanum un caste unguran"/>
</div>
<div class="lyrico-lyrics-wrapper">poaraatam waste unguran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poaraatam waste unguran"/>
</div>
<div class="lyrico-lyrics-wrapper">naanum un caste unguran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naanum un caste unguran"/>
</div>
<div class="lyrico-lyrics-wrapper">saaraya aala thorakkuraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saaraya aala thorakkuraan"/>
</div>
<div class="lyrico-lyrics-wrapper">modhalaliyayiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="modhalaliyayiya"/>
</div>
<div class="lyrico-lyrics-wrapper">supportaa leader irukkuran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="supportaa leader irukkuran"/>
</div>
<div class="lyrico-lyrics-wrapper">karungaaliyayiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karungaaliyayiya"/>
</div>
<div class="lyrico-lyrics-wrapper">ulaikkama oora mulunguraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulaikkama oora mulunguraan"/>
</div>
<div class="lyrico-lyrics-wrapper">silaperuyayiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="silaperuyayiya"/>
</div>
<div class="lyrico-lyrics-wrapper">uthaikkama neethi kidaikkuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uthaikkama neethi kidaikkuma"/>
</div>
<div class="lyrico-lyrics-wrapper">nilai maarumaiyya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nilai maarumaiyya"/>
</div>
<div class="lyrico-lyrics-wrapper">yethana yethana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yethana yethana"/>
</div>
<div class="lyrico-lyrics-wrapper">anniya company
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anniya company"/>
</div>
<div class="lyrico-lyrics-wrapper">paaren sarvesaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paaren sarvesaa"/>
</div>
<div class="lyrico-lyrics-wrapper">nalla paaren sarvesaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nalla paaren sarvesaa"/>
</div>
<div class="lyrico-lyrics-wrapper">athula onnaachum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athula onnaachum"/>
</div>
<div class="lyrico-lyrics-wrapper">uruppadiyaa kooren sarvesaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uruppadiyaa kooren sarvesaa"/>
</div>
<div class="lyrico-lyrics-wrapper">yethana yethana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yethana yethana"/>
</div>
<div class="lyrico-lyrics-wrapper">anniya company
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anniya company"/>
</div>
<div class="lyrico-lyrics-wrapper">paaren sarvesaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paaren sarvesaa"/>
</div>
<div class="lyrico-lyrics-wrapper">nalla paaren sarvesaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nalla paaren sarvesaa"/>
</div>
<div class="lyrico-lyrics-wrapper">athula onnaachum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athula onnaachum"/>
</div>
<div class="lyrico-lyrics-wrapper">uruppadiyaa kooren sarvesaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uruppadiyaa kooren sarvesaa"/>
</div>
</pre>
