---
title: "aandipatti song lyrics"
album: "Aayiram Vilakku"
artist: "Srikanth Deva"
lyricist: "Vairamuthu"
director: "S.P. Hosimin"
path: "/albums/aayiram-vilakku-lyrics"
song: "Aandipatti"
image: ../../images/albumart/aayiram-vilakku.jpg
date: 2011-09-23
lang: tamil
youtubeLink: 
type: "happy"
singers:
  - Karthik
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aaluda aaludaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaluda aaludaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaluda aaludaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaluda aaludaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaluda aaludaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaluda aaludaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaluda aaludaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaluda aaludaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theludaa theludaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theludaa theludaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Theludaa theludaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theludaa theludaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Theludaa theludaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theludaa theludaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Theludaa theludaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theludaa theludaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaluda aaludaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaluda aaludaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaluda aaludaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaluda aaludaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aandippatti usilambatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aandippatti usilambatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaludaa aaludaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaludaa aaludaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Asandhupputta kottappora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asandhupputta kottappora"/>
</div>
<div class="lyrico-lyrics-wrapper">Theludaa theludaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theludaa theludaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaluda aaludaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaluda aaludaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaluda aaludaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaluda aaludaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Theludaa theludaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theludaa theludaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Theludaa theludaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theludaa theludaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aandippatti usilambatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aandippatti usilambatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaludaa aaludaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaludaa aaludaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Asandhupputta kottappora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asandhupputta kottappora"/>
</div>
<div class="lyrico-lyrics-wrapper">Theludaa theludaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theludaa theludaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veerappaandi veerappaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veerappaandi veerappaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Therudaa therudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Therudaa therudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aiyiraameenaa sikkapporaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyiraameenaa sikkapporaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarudaa paarudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarudaa paarudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarudaa paarudaa parudaa parudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarudaa paarudaa parudaa parudaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oththa sollu porukkamaatten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oththa sollu porukkamaatten"/>
</div>
<div class="lyrico-lyrics-wrapper">Rosakkaarandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rosakkaarandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Parambaraiyaa yeppavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parambaraiyaa yeppavum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan paasakkaarandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan paasakkaarandaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oththa sollu porukkamaatten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oththa sollu porukkamaatten"/>
</div>
<div class="lyrico-lyrics-wrapper">Rosakkaarandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rosakkaarandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Parambaraiyaa yeppavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parambaraiyaa yeppavum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan paasakkaarandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan paasakkaarandaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaludaa aaludaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaludaa aaludaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhura jilla aaludaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhura jilla aaludaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhooludaa dhooludaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhooludaa dhooludaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thottadhellaam dhooludaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottadhellaam dhooludaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enidam aayudham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enidam aayudham"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhumilla paarudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhumilla paarudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagaivanaae pagaivanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagaivanaae pagaivanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Paththuviral veludaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paththuviral veludaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adimai endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adimai endru"/>
</div>
<div class="lyrico-lyrics-wrapper">Aakkavum theriyaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aakkavum theriyaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adimai endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adimai endru"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhavum mudiyaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhavum mudiyaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Panaththil ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panaththil ennai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaangavum mudiyaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaangavum mudiyaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Bottilukkul sooriyan adangaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bottilukkul sooriyan adangaadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veettukku yaarum vandhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veettukku yaarum vandhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan vellaattai urichivaippen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan vellaattai urichivaippen"/>
</div>
<div class="lyrico-lyrics-wrapper">Machchaanga yaarum vandhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machchaanga yaarum vandhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan kallaattai virundhu vaippen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan kallaattai virundhu vaippen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaarukkum pallaakku pallaakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarukkum pallaakku pallaakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Thookkaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thookkaadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uzhaichi munnaeru munnaeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uzhaichi munnaeru munnaeru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaikkum poraadu poraadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaikkum poraadu poraadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoongaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoongaadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakku undaagum varallaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakku undaagum varallaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aandippatti usilambatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aandippatti usilambatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Ah ah Aandippatti usilambatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ah ah Aandippatti usilambatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaludaa aaludaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaludaa aaludaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Asandhupputta kottappora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asandhupputta kottappora"/>
</div>
<div class="lyrico-lyrics-wrapper">Theludaa theludaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theludaa theludaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sottanum sottanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sottanum sottanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vervai mannil sottanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vervai mannil sottanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottanum kottanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottanum kottanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodippanam kottanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodippanam kottanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aalanum aalanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalanum aalanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbinaalae aalanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbinaalae aalanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhanum vaazhanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhanum vaazhanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paravaippola vaazhanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paravaippola vaazhanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edhiripperai solli adippendaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhiripperai solli adippendaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhmai yena eppavum thaduppendaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhmai yena eppavum thaduppendaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Paththavachchaa aala mudippendaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paththavachchaa aala mudippendaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Paasam vachchaa tholakkoduppendaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paasam vachchaa tholakkoduppendaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oorukkae kodaippidippen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorukkae kodaippidippen"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku oorellaam kodippidinga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku oorellaam kodippidinga"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandaikki thuninjipputtaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandaikki thuninjipputtaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku saappaadae adithadinga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku saappaadae adithadinga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thenamum kondaattam kondaattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thenamum kondaattam kondaattam"/>
</div>
<div class="lyrico-lyrics-wrapper">kondaattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kondaattam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaikkum santhoasam theeraadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaikkum santhoasam theeraadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasula pandhattam pandhattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasula pandhattam pandhattam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pandhattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pandhattam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaikkum urchaagam maaraadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaikkum urchaagam maaraadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aandippatti usilambatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aandippatti usilambatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaludaa aaludaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaludaa aaludaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Asandhupputta kottappora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asandhupputta kottappora"/>
</div>
<div class="lyrico-lyrics-wrapper">Theludaa theludaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theludaa theludaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Veerappaandi veerappaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veerappaandi veerappaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Therudaa therudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Therudaa therudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aiyiraameenaa sikkapporen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyiraameenaa sikkapporen"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarudaa paarudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarudaa paarudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarudaa paarudaa parudaa parudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarudaa paarudaa parudaa parudaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oththa sollu porukkamaatten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oththa sollu porukkamaatten"/>
</div>
<div class="lyrico-lyrics-wrapper">Rosakkaarandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rosakkaarandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Parambaraiyaa yeppavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parambaraiyaa yeppavum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan paasakkaarandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan paasakkaarandaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oththa sollu porukkamaatten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oththa sollu porukkamaatten"/>
</div>
<div class="lyrico-lyrics-wrapper">Rosakkaarandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rosakkaarandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Parambaraiyaa yeppavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parambaraiyaa yeppavum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan paasakkaarandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan paasakkaarandaa"/>
</div>
</pre>
