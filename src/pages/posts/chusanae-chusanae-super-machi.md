---
title: "chusanae chusanae song lyrics"
album: "Super Machi"
artist: "Thaman S"
lyricist: "Krishna Kanth"
director: "Puli Vasu"
path: "/albums/super-machi-lyrics"
song: "Chusanae Chusanae"
image: ../../images/albumart/super-machi.jpg
date: 2022-01-14
lang: telugu
youtubeLink: "https://www.youtube.com/embed/yGkngy6KSIs"
type: "love"
singers:
  - Rita Thyagarajan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Chusane Chusane Korukunna Pillagadu Tane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chusane Chusane Korukunna Pillagadu Tane"/>
</div>
<div class="lyrico-lyrics-wrapper">Chusane Chusane Chudagane Nelapaina Lene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chusane Chusane Chudagane Nelapaina Lene"/>
</div>
<div class="lyrico-lyrics-wrapper">Vechi Vechi Chuse Kanulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vechi Vechi Chuse Kanulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mani Inka Vere Panulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mani Inka Vere Panulu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Reppadati Vache Kalalu Ceppaleni Haye Asalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Reppadati Vache Kalalu Ceppaleni Haye Asalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Na Kannule Navvalile Kanniru Karentala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na Kannule Navvalile Kanniru Karentala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Agipoyene Manasu Chinni Gundekem Telusu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Agipoyene Manasu Chinni Gundekem Telusu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gnapakalade Golusu Tiranivvali Na Korika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gnapakalade Golusu Tiranivvali Na Korika"/>
</div>
<div class="lyrico-lyrics-wrapper">Agipoyene Manasu Chinni Gundekem Telusu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Agipoyene Manasu Chinni Gundekem Telusu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gnapakalade Golusu Tiranivvali Na Korika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gnapakalade Golusu Tiranivvali Na Korika"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chusane Chusane Korukunna Pillagadu Tane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chusane Chusane Korukunna Pillagadu Tane"/>
</div>
<div class="lyrico-lyrics-wrapper">Chusane Chusane Chudagane Nelapaina Lene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chusane Chusane Chudagane Nelapaina Lene"/>
</div>
<div class="lyrico-lyrics-wrapper">Chusane Chusane Chusane Ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chusane Chusane Chusane Ho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kallu Rendu Valipoyene Chusaka Ninnu Nene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallu Rendu Valipoyene Chusaka Ninnu Nene"/>
</div>
<div class="lyrico-lyrics-wrapper">Gunde Chappudagipoyene Aa Nimisam Agaleke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gunde Chappudagipoyene Aa Nimisam Agaleke"/>
</div>
<div class="lyrico-lyrics-wrapper">Ni Kosame Vachanuga Na Prema Techanura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ni Kosame Vachanuga Na Prema Techanura"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnintaga Mechanura Nalone Dachanura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnintaga Mechanura Nalone Dachanura"/>
</div>
<div class="lyrico-lyrics-wrapper">Na Kannule Navvalile Kanniru Karentala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na Kannule Navvalile Kanniru Karentala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Agipoyene Manasu Chinni Gundekem Telusu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Agipoyene Manasu Chinni Gundekem Telusu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gnapakalade Golusu Tiranivvali Na Korika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gnapakalade Golusu Tiranivvali Na Korika"/>
</div>
<div class="lyrico-lyrics-wrapper">Agipoyene Manasu Chinni Gundekem Telusu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Agipoyene Manasu Chinni Gundekem Telusu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gnapakalade Golusu Tiranivvali Na Korika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gnapakalade Golusu Tiranivvali Na Korika"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chusane Chusane Korukunna Pillagadu Tane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chusane Chusane Korukunna Pillagadu Tane"/>
</div>
<div class="lyrico-lyrics-wrapper">Chusane Chusane Chudagane Nelapaina Lene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chusane Chusane Chudagane Nelapaina Lene"/>
</div>
<div class="lyrico-lyrics-wrapper">Chusane Chusane Chusane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chusane Chusane Chusane"/>
</div>
</pre>
