---
title: "en thani iravu song lyrics"
album: "Asuraguru"
artist: "Ganesh Raghavendra"
lyricist: "Kabilan Vairamuthu"
director: "A. Raajdheep"
path: "/albums/asuraguru-song-lyrics"
song: "En Thani Iravu"
image: ../../images/albumart/asuraguru.jpg
date: 2020-03-13
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ERxZkoIESYI"
type: "Love"
singers:
  - Padmalatha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">en thani iravu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en thani iravu"/>
</div>
<div class="lyrico-lyrics-wrapper">men vali tharudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="men vali tharudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">un theendalil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un theendalil "/>
</div>
<div class="lyrico-lyrics-wrapper">ulladhu theervu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulladhu theervu"/>
</div>
<div class="lyrico-lyrics-wrapper">en thalai kunivu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en thalai kunivu"/>
</div>
<div class="lyrico-lyrics-wrapper">men mazhai kunivu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="men mazhai kunivu"/>
</div>
<div class="lyrico-lyrics-wrapper">nal maanathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nal maanathil"/>
</div>
<div class="lyrico-lyrics-wrapper">nanaindhidum vaazhvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanaindhidum vaazhvu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aadaigal thaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aadaigal thaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">aazha aazha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aazha aazha"/>
</div>
<div class="lyrico-lyrics-wrapper">en kangal paaigindrathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kangal paaigindrathey"/>
</div>
<div class="lyrico-lyrics-wrapper">rathathil paadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rathathil paadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">thenaai maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thenaai maari"/>
</div>
<div class="lyrico-lyrics-wrapper">thithippu koodugindrathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thithippu koodugindrathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">en thani iravu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en thani iravu"/>
</div>
<div class="lyrico-lyrics-wrapper">men vali tharudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="men vali tharudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">un theendalil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un theendalil "/>
</div>
<div class="lyrico-lyrics-wrapper">ulladhu theervu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulladhu theervu"/>
</div>
<div class="lyrico-lyrics-wrapper">en thalai kunivu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en thalai kunivu"/>
</div>
<div class="lyrico-lyrics-wrapper">men mazhai kunivu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="men mazhai kunivu"/>
</div>
<div class="lyrico-lyrics-wrapper">nal maanathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nal maanathil"/>
</div>
<div class="lyrico-lyrics-wrapper">nanaindhidum vaazhvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanaindhidum vaazhvu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">minu minukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="minu minukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">en viyarvayiley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en viyarvayiley"/>
</div>
<div class="lyrico-lyrics-wrapper">aayiram asaigal manakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aayiram asaigal manakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">azhuthangalin andha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="azhuthangalin andha"/>
</div>
<div class="lyrico-lyrics-wrapper">niruthangalil en aanava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="niruthangalil en aanava"/>
</div>
<div class="lyrico-lyrics-wrapper">thoranai maraiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoranai maraiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">kaamam pookum satham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaamam pookum satham"/>
</div>
<div class="lyrico-lyrics-wrapper">kaadhil nee kelada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadhil nee kelada"/>
</div>
<div class="lyrico-lyrics-wrapper">viraga pattampoochi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viraga pattampoochi"/>
</div>
<div class="lyrico-lyrics-wrapper">vizhiyil nee paarada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vizhiyil nee paarada"/>
</div>
<div class="lyrico-lyrics-wrapper">naan kattilukku mel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan kattilukku mel"/>
</div>
<div class="lyrico-lyrics-wrapper">oru sottu marundhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru sottu marundhu"/>
</div>
<div class="lyrico-lyrics-wrapper">un noigal theerumada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un noigal theerumada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">payanangalai engum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="payanangalai engum "/>
</div>
<div class="lyrico-lyrics-wrapper">mudiyavida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudiyavida"/>
</div>
<div class="lyrico-lyrics-wrapper">dhoorangal than 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhoorangal than "/>
</div>
<div class="lyrico-lyrics-wrapper">ini pidikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ini pidikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">unatharuge naan nadakayile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unatharuge naan nadakayile"/>
</div>
<div class="lyrico-lyrics-wrapper">en paadangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en paadangal"/>
</div>
<div class="lyrico-lyrics-wrapper">adikadi inikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adikadi inikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">elagum thegathin mel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="elagum thegathin mel"/>
</div>
<div class="lyrico-lyrics-wrapper">idhazhai nee kondu va
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhazhai nee kondu va"/>
</div>
<div class="lyrico-lyrics-wrapper">paruva dhiravathile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paruva dhiravathile"/>
</div>
<div class="lyrico-lyrics-wrapper">iravai nee thoykka va
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iravai nee thoykka va"/>
</div>
<div class="lyrico-lyrics-wrapper">un agadhiyathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un agadhiyathai"/>
</div>
<div class="lyrico-lyrics-wrapper">nee maranthidada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee maranthidada"/>
</div>
<div class="lyrico-lyrics-wrapper">indha uyvathiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indha uyvathiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">kudi pugadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kudi pugadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aadaigal thaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aadaigal thaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">aazha aazha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aazha aazha"/>
</div>
<div class="lyrico-lyrics-wrapper">en kangal paaigindrathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kangal paaigindrathey"/>
</div>
<div class="lyrico-lyrics-wrapper">rathathil paadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rathathil paadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">thenaai maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thenaai maari"/>
</div>
<div class="lyrico-lyrics-wrapper">thithippu koodugindrathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thithippu koodugindrathey"/>
</div>
</pre>
