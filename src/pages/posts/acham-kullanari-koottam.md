---
title: "acham song lyrics"
album: "Kullanari Koottam"
artist: "V. Selvaganesh"
lyricist: "Na. Muthukumar"
director: "Sribalaji"
path: "/albums/kullanari-koottam-lyrics"
song: "Acham"
image: ../../images/albumart/kullanari-koottam.jpg
date: 2011-03-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/utJLs5huEJo"
type: "mass"
singers:
  - Rashmi
  - Kalpana
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ada Bayam Onnum Illai Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Bayam Onnum Illai Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga Settaikku Ellai Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Settaikku Ellai Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeley Yeley Ingavaadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeley Yeley Ingavaadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">En Getha Konjam Paarththu Podaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Getha Konjam Paarththu Podaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Bayam Onnum Illai Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Bayam Onnum Illai Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga Settaikku Ellai Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Settaikku Ellai Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeley Yeley Ingavaadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeley Yeley Ingavaadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">En Getha Konjam Paarththu Podaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Getha Konjam Paarththu Podaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Achcham Adhu Enge Virkkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achcham Adhu Enge Virkkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanakkam Adhu Enge Virkkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanakkam Adhu Enge Virkkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetkam Adhu Enge Virkkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetkam Adhu Enge Virkkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Kilo Enna Vilai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Kilo Enna Vilai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Selai Adhu Enge Virkkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Selai Adhu Enge Virkkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pookkal Adhu Enge Virkkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pookkal Adhu Enge Virkkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sogam Adhu Enge Virkkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sogam Adhu Enge Virkkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Avai Ellaam Enna Vilai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avai Ellaam Enna Vilai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Samayal Araiyai Paarthadhillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samayal Araiyai Paarthadhillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarthadhillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarthadhillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Valaiyal Oliyai Kettadhillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valaiyal Oliyai Kettadhillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kettadhillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kettadhillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theruvil Kolam Pottadhillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theruvil Kolam Pottadhillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pottadhillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pottadhillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pazhaiya Kaalam Naangalillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pazhaiya Kaalam Naangalillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iravu Muzhukka Iravu Muzhukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravu Muzhukka Iravu Muzhukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Rondhu Adippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rondhu Adippom"/>
</div>
<div class="lyrico-lyrics-wrapper">Korainja Patcham Moonu Beeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Korainja Patcham Moonu Beeru"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaangi Kudippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaangi Kudippom"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Pasangapola Bussil Yeri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Pasangapola Bussil Yeri"/>
</div>
<div class="lyrico-lyrics-wrapper">Footboard Adippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Footboard Adippom"/>
</div>
<div class="lyrico-lyrics-wrapper">Hei Theatter Poyi Dhammu Adichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei Theatter Poyi Dhammu Adichi"/>
</div>
<div class="lyrico-lyrics-wrapper">Visilum Adippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Visilum Adippom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mugathodu Meesai Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mugathodu Meesai Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanaal Aangal Naanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanaal Aangal Naanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Manadhodu Bayame Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manadhodu Bayame Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Modhippaarppom Vaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Modhippaarppom Vaanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Marriage Aagaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marriage Aagaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Pasanga Veettukku Povonga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pasanga Veettukku Povonga"/>
</div>
<div class="lyrico-lyrics-wrapper">Bajji Sojji Thinnuttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bajji Sojji Thinnuttu"/>
</div>
<div class="lyrico-lyrics-wrapper">Baradham Aadida Solvonga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baradham Aadida Solvonga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Love Letter Yaarum Thandhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love Letter Yaarum Thandhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kindal Seivom Naanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kindal Seivom Naanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Puyalodu Pottippottaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puyalodu Pottippottaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudaiya Vellum Ponga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudaiya Vellum Ponga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaradi Koondhal Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaradi Koondhal Ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Andhakaalam Aiyaiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhakaalam Aiyaiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaru Inchi Kraappu Vaikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaru Inchi Kraappu Vaikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhiradi Naanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhiradi Naanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iravu Muzhukka Iravu Muzhukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravu Muzhukka Iravu Muzhukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Rondhu Adippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rondhu Adippom"/>
</div>
<div class="lyrico-lyrics-wrapper">Korainja Patcham Moonu Beeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Korainja Patcham Moonu Beeru"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaangi Kudippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaangi Kudippom"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Pasangapola Bus-sil Yeri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Pasangapola Bus-sil Yeri"/>
</div>
<div class="lyrico-lyrics-wrapper">Footboard Adippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Footboard Adippom"/>
</div>
<div class="lyrico-lyrics-wrapper">Hei Theatter Poyi Dhammu Adichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei Theatter Poyi Dhammu Adichi"/>
</div>
<div class="lyrico-lyrics-wrapper">Visilum Adippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Visilum Adippom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Boys Hostel Ulleppoyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boys Hostel Ulleppoyi"/>
</div>
<div class="lyrico-lyrics-wrapper">Raging Naanga Seivom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raging Naanga Seivom"/>
</div>
<div class="lyrico-lyrics-wrapper">Main Roattil Trafic Jammil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main Roattil Trafic Jammil"/>
</div>
<div class="lyrico-lyrics-wrapper">Jogging Naanga Povom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jogging Naanga Povom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">College Examil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="College Examil"/>
</div>
<div class="lyrico-lyrics-wrapper">Bittuthaan Adichi Velvome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bittuthaan Adichi Velvome"/>
</div>
<div class="lyrico-lyrics-wrapper">Professorum Paarthuttaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Professorum Paarthuttaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kissuthaan Thandhuttu Chelvome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kissuthaan Thandhuttu Chelvome"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pen Endraal Scootythaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pen Endraal Scootythaanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Bullet Yeri Povom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bullet Yeri Povom"/>
</div>
<div class="lyrico-lyrics-wrapper">Film Kaatti Yaarum Vandhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Film Kaatti Yaarum Vandhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Bulletaaga Paaivom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bulletaaga Paaivom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhulathaan Kammal Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhulathaan Kammal Ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha Kaalam Aiyaiyaiyao
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Kaalam Aiyaiyaiyao"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoppul Mela Kammal Podum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoppul Mela Kammal Podum"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhiradi Naanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhiradi Naanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iravu Muzhukka Iravu Muzhukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravu Muzhukka Iravu Muzhukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Rondhu Adippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rondhu Adippom"/>
</div>
<div class="lyrico-lyrics-wrapper">Korainja Patcham Moonu Beeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Korainja Patcham Moonu Beeru"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaangi Kudippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaangi Kudippom"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Pasangapola Bus Sil Yeri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Pasangapola Bus Sil Yeri"/>
</div>
<div class="lyrico-lyrics-wrapper">Footboard Adippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Footboard Adippom"/>
</div>
<div class="lyrico-lyrics-wrapper">Hei Theatter Poyi Dhammu Adichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei Theatter Poyi Dhammu Adichi"/>
</div>
<div class="lyrico-lyrics-wrapper">Visilum Adippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Visilum Adippom"/>
</div>
</pre>
