---
title: "desingu raja song lyrics"
album: "Dum Dum Dum"
artist: "Karthik Raja"
lyricist: "Na. Muthukumar - Vaali - Pa. Vijay"
director: "Azhagam Perumal"
path: "/albums/dum-dum-dum-song-lyrics"
song: "Desingu Raja Desingu Raja"
image: ../../images/albumart/dum-dum-dum.jpg
date: 2001-04-13
lang: tamil
youtubeLink: "https://www.youtube.com/embed/JHTZ43dRtLc"
type: "love"
singers:
  - Harish Raghavendra
  - Sujatha Mohan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Desinga Raja Desinga Raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Desinga Raja Desinga Raja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haiyoo Haiyoo Haiyoo Haiyoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haiyoo Haiyoo Haiyoo Haiyoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Haiyoo Haiyoo Haiyoo Haiii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haiyoo Haiyoo Haiyoo Haiii"/>
</div>
<div class="lyrico-lyrics-wrapper">Haiyoo Haiyoo Haiyoo Haiyoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haiyoo Haiyoo Haiyoo Haiyoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Haiyoo Haiyoo Haiyoo Haiii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haiyoo Haiyoo Haiyoo Haiii"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Desinga Raja Desinga Raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Desinga Raja Desinga Raja"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiru Thiru Thirunru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiru Thiru Thirunru"/>
</div>
<div class="lyrico-lyrics-wrapper">Muzhippadhu Yen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muzhippadhu Yen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanjavur Rani Thanjavur Rani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanjavur Rani Thanjavur Rani"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuru Kuru Kurunru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuru Kuru Kurunru"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarpadhu Yen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarpadhu Yen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poova Thalaiya Pottu Paarthaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poova Thalaiya Pottu Paarthaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Poovonnu Vizhindadhu Thalaiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovonnu Vizhindadhu Thalaiyile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaaya Pazhama Kettu Paarthaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaya Pazhama Kettu Paarthaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaayonnu Kaninjadhu Kanaville
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaayonnu Kaninjadhu Kanaville"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ini Onnum Onnum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Onnum Onnum"/>
</div>
<div class="lyrico-lyrics-wrapper">Onna Serndhu Moona Aayidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna Serndhu Moona Aayidum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hei Desinga Raja Desinga Raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei Desinga Raja Desinga Raja"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiru Thiru Thirunru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiru Thiru Thirunru"/>
</div>
<div class="lyrico-lyrics-wrapper">Muzhippadhu Yen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muzhippadhu Yen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanjavur Rani Thanjavur Rani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanjavur Rani Thanjavur Rani"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuru Kuru Kurunru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuru Kuru Kurunru"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarpadhu Yen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarpadhu Yen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjukulle Nenjukulle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjukulle Nenjukulle"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenapukku Alaville
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenapukku Alaville"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannukulle Kannukulle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannukulle Kannukulle"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavukku Vilai Illaee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavukku Vilai Illaee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Manadhil Pai Marangal Viriyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Manadhil Pai Marangal Viriyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha Kappal Ettu Dhisai Adaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Kappal Ettu Dhisai Adaiyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Idhayam Mummadangu Thudikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Idhayam Mummadangu Thudikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Manadhin Baaram Enni Kanakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Manadhin Baaram Enni Kanakkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vinaavinai Ketten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinaavinai Ketten"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidai Varume Thanaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidai Varume Thanaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adupuli Aataththile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adupuli Aataththile"/>
</div>
<div class="lyrico-lyrics-wrapper">Odum Puli Katathile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odum Puli Katathile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ottaipaanai Thittathale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ottaipaanai Thittathale"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhiyudhe Kuttathile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhiyudhe Kuttathile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Idhayam Rayilum Seiyum Kalagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Idhayam Rayilum Seiyum Kalagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu Iruppu Padhai Vittu Vilagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu Iruppu Padhai Vittu Vilagum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thalaikizhai Thirumbiyidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaikizhai Thirumbiyidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Sariyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Sariyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirisangu Sorkka Nilai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirisangu Sorkka Nilai"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Dhaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Dhaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vinaa Ketten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinaa Ketten"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidai Varume Thanaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidai Varume Thanaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Desinga Raja Desinga Raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Desinga Raja Desinga Raja"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Chinna Ponnu Sirichaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Chinna Ponnu Sirichaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Natchathiram Parichaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natchathiram Parichaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nichyathai Mudichaa Melam Kottanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nichyathai Mudichaa Melam Kottanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sundaikkayi Ettana Sumaikulli Pathana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sundaikkayi Ettana Sumaikulli Pathana"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponna Naanga Kattuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponna Naanga Kattuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Nagai Tharanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nagai Tharanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Summa Aadade Sozhiyan Kudumi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summa Aadade Sozhiyan Kudumi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethana Pavun Venum Vaangikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethana Pavun Venum Vaangikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kulla Podadhe Mappilai Murukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulla Podadhe Mappilai Murukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Jillavulla Padhithaan Pirikkannum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jillavulla Padhithaan Pirikkannum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ini Onnum Onnum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Onnum Onnum"/>
</div>
<div class="lyrico-lyrics-wrapper">Onna Serndhu Moona Aayidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna Serndhu Moona Aayidum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hei Desinga Raja Desinga Raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei Desinga Raja Desinga Raja"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiru Thiru Thirunru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiru Thiru Thirunru"/>
</div>
<div class="lyrico-lyrics-wrapper">Muzhippadhu Yen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muzhippadhu Yen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanjavur Rani Thanjavur Rani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanjavur Rani Thanjavur Rani"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuru Kuru Kurunru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuru Kuru Kurunru"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarpadhu Yen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarpadhu Yen"/>
</div>
</pre>
