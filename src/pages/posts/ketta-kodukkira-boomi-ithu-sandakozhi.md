---
title: "ketta kudukkira boomi ithu song lyrics"
album: "Sandakozhi"
artist: "Yuvan Sankar Raja"
lyricist: "Thamarai"
director: "N. Linguswamy"
path: "/albums/sandakozhi-lyrics"
song: "Ketta Kudukkira Boomi Ithu"
image: ../../images/albumart/sandakozhi.jpg
date: 2005-12-16
lang: tamil
youtubeLink: "https://www.youtube.com/embed/o48UkqGAyns"
type: "Festival"
singers:
  - Chinmayi
  - Ganga Sitharasu
  - Jassie Gift
  - Sujatha Mohan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ketta Kodukkura Bhoomi Ithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketta Kodukkura Bhoomi Ithu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kekkaama Kodukkura Saami Ithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kekkaama Kodukkura Saami Ithu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ketta Kodukkura Bhoomi Ithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketta Kodukkura Bhoomi Ithu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kekkaama Kodukkura Saami Ithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kekkaama Kodukkura Saami Ithu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaiyila Kaththi Irukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyila Kaththi Irukkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meesa Suththi Irukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meesa Suththi Irukkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Periya Neththi Irukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Periya Neththi Irukkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kobam Appadi Irukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kobam Appadi Irukkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaduvom Paaduvom Kondaaduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaduvom Paaduvom Kondaaduvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kooduvom Seruvom Kooththaduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooduvom Seruvom Kooththaduvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaduvom Paaduvom Konadaaduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaduvom Paaduvom Konadaaduvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kooduvom Seruvom Kooththaduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooduvom Seruvom Kooththaduvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saami Saami Saamida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saami Saami Saamida"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vellai Kuthira Yerivarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vellai Kuthira Yerivarum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Namma Kula Saamida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma Kula Saamida"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaami Kaami Kaamida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaami Kaami Kaamida"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenja Polandhu Saththiyathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenja Polandhu Saththiyathil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Soodam Yethi Kaamida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soodam Yethi Kaamida"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ketta Kodukkura Bhoomi Ithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketta Kodukkura Bhoomi Ithu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kekkaama Kodukkura Saami Ithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kekkaama Kodukkura Saami Ithu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ullukkule Oru Aasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullukkule Oru Aasa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Olichchu Vechan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Olichchu Vechan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Atha Unkitta Saami Kitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atha Unkitta Saami Kitta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Solli Vechen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solli Vechen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjukkulla Oru Aasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjukkulla Oru Aasa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Olichchu Vechen Atha Saami Kitta Sollalaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Olichchu Vechen Atha Saami Kitta Sollalaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Marachchu Vechen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marachchu Vechen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ellarukkum Ennanavo Kanavirukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellarukkum Ennanavo Kanavirukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Yaarukku Yaarathaan Pudichchirukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Yaarukku Yaarathaan Pudichchirukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Intha Jodi Engalukku Pudichirukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Jodi Engalukku Pudichirukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iva Vellakkeththa Enga Veedu Thavam Kedakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iva Vellakkeththa Enga Veedu Thavam Kedakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manakka Manakka Poo Thaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manakka Manakka Poo Thaara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eduthu Eduthu Varuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eduthu Eduthu Varuvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanakku vazhakku Paarkaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanakku vazhakku Paarkaama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thadukkku Thulli Tharuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadukkku Thulli Tharuvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaduvom Paaduvom Kondaaduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaduvom Paaduvom Kondaaduvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kooduvom Seruvom Kooththaduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooduvom Seruvom Kooththaduvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaduvom Paaduvom Konadaaduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaduvom Paaduvom Konadaaduvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kooduvom Seruvom Kooththaduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooduvom Seruvom Kooththaduvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paththu Mile Thoorathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paththu Mile Thoorathula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Varuva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Varuva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Intha Bhakthanoda Manasu Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Bhakthanoda Manasu Ellaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thee Pidikkkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thee Pidikkkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muththam Kodu Thooraththula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muththam Kodu Thooraththula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Iruppe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Iruppe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Antha Sootile Manasellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Sootile Manasellam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kulir Edukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulir Edukkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aasapatta Naayana Adanjaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasapatta Naayana Adanjaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ava Aaradi Saelaiyile Mudinjaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Aaradi Saelaiyile Mudinjaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adangaatha Kaalaikku Kayiraaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adangaatha Kaalaikku Kayiraaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Mayil Thogai Vaachchirukku Vagaiyaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Mayil Thogai Vaachchirukku Vagaiyaaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saami Katti Pari Pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saami Katti Pari Pogum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Samayal Kattu Pari Pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samayal Kattu Pari Pogum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaasa Kolam Kai Maarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaasa Kolam Kai Maarum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaarisu Onnu Uruvaagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaarisu Onnu Uruvaagum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ketta Kodukkura Bhoomi Ithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketta Kodukkura Bhoomi Ithu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kekkaama Kodukkura Saami Ithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kekkaama Kodukkura Saami Ithu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ketta Kodukkura Bhoomi Ithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketta Kodukkura Bhoomi Ithu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaiyila Kaththi Irukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyila Kaththi Irukkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meesa Suththi Irukkkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meesa Suththi Irukkkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Periya Nethti Irukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Periya Nethti Irukkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kobam Appadi Irukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kobam Appadi Irukkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaduvom Paaduvom Kondaaduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaduvom Paaduvom Kondaaduvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kooduvom Seruvom Kooththaduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooduvom Seruvom Kooththaduvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaduvom Paaduvom Kondaaduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaduvom Paaduvom Kondaaduvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kooduvom Seruvom Kooththaduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooduvom Seruvom Kooththaduvom"/>
</div>
</pre>
