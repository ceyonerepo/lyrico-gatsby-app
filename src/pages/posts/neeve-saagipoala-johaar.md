---
title: "neeve saagipoala song lyrics"
album: "Johaar"
artist: "Priyadarshan - Balasubramanian"
lyricist: "Chaitanya Prasad"
director: "Marni Teja Chowdary"
path: "/albums/johaar-lyrics"
song: "Neeve Saagipoala"
image: ../../images/albumart/johaar.jpg
date: 2020-08-15
lang: telugu
youtubeLink: "https://www.youtube.com/embed/WZu3FUysw9s"
type: "love"
singers:
  - Haricharan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hey Madhuram Kaadhaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Madhuram Kaadhaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Ninne Premisthu Unte Oka Thodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninne Premisthu Unte Oka Thodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Koti Aashale Pooche Kaanthule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koti Aashale Pooche Kaanthule"/>
</div>
<div class="lyrico-lyrics-wrapper">Neelo Dhaagi Levaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelo Dhaagi Levaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaari Moosina Saagaraannilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaari Moosina Saagaraannilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaatudhaam Ivaalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaatudhaam Ivaalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kooladhosina Kaalaraasina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooladhosina Kaalaraasina"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasha Veedaka Raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasha Veedaka Raa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeve Saagipo Alaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeve Saagipo Alaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Prema Vaagu Vale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prema Vaagu Vale"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Vaale Sandhyaalni Dhaate
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Vaale Sandhyaalni Dhaate"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaabille Navve Jaanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaabille Navve Jaanai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gelavaali Ante Chematodchaalanthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gelavaali Ante Chematodchaalanthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Saagani Poru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saagani Poru"/>
</div>
<div class="lyrico-lyrics-wrapper">Bathukante Inthe Ika Raajee 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bathukante Inthe Ika Raajee "/>
</div>
<div class="lyrico-lyrics-wrapper">Antoo Undadhe Oo Oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antoo Undadhe Oo Oo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninne Maarchuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninne Maarchuko"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeke Nuvvu Thodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeke Nuvvu Thodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeve Neelaaga Saagipolevaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeve Neelaaga Saagipolevaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheeksha Poonavaa Aa Aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheeksha Poonavaa Aa Aa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sirulevi Levule Avi Vente Raavule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirulevi Levule Avi Vente Raavule"/>
</div>
<div class="lyrico-lyrics-wrapper">Preminche Manase Unte Chaalule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Preminche Manase Unte Chaalule"/>
</div>
<div class="lyrico-lyrics-wrapper">Eeroju Chedhainaa Repinkaa Needheraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eeroju Chedhainaa Repinkaa Needheraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Prema Mudhra Nuvvu Vesei Raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prema Mudhra Nuvvu Vesei Raa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aa Manchi Roju Kosam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Manchi Roju Kosam "/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu Choosthunnaavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu Choosthunnaavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Andaa, Nee Dhandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Andaa, Nee Dhandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu Raadhanukunnaavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu Raadhanukunnaavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Niluvellaa Kalle Ayi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niluvellaa Kalle Ayi "/>
</div>
<div class="lyrico-lyrics-wrapper">Vechi Chooshaavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vechi Chooshaavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yadha Needhe Vyadha Naadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yadha Needhe Vyadha Naadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanne Cheraga Raa Raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanne Cheraga Raa Raa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhairyam Nuvve Kaadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhairyam Nuvve Kaadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Premake Praanam Neevu Lemmaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Premake Praanam Neevu Lemmaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jeevitham Ankitham Chesukommaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jeevitham Ankitham Chesukommaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeve Saagipo Alaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeve Saagipo Alaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Prema Vaagu Vale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prema Vaagu Vale"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Vaale Sandhyaalni Dhaate
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Vaale Sandhyaalni Dhaate"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaabille Navve Jaanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaabille Navve Jaanai"/>
</div>
</pre>