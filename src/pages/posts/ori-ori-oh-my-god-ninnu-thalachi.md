---
title: "ori ori oh my god song lyrics"
album: "Ninnu Thalachi"
artist: "Yellender mahaveera"
lyricist: "poornachary"
director: "Anil thota"
path: "/albums/ninnu-thalachi-lyrics"
song: "Ori Ori Oh My God"
image: ../../images/albumart/ninnu-thalachi.jpg
date: 2019-09-27
lang: telugu
youtubeLink: "https://www.youtube.com/embed/SHHfNHio7s0"
type: "love"
singers:
  - Ranjith
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">ori ori oh my god
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ori ori oh my god"/>
</div>
<div class="lyrico-lyrics-wrapper">heart beat 100 nu thakera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="heart beat 100 nu thakera"/>
</div>
<div class="lyrico-lyrics-wrapper">pulse ratu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pulse ratu "/>
</div>
<div class="lyrico-lyrics-wrapper">meteru dhati pooyera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meteru dhati pooyera"/>
</div>
<div class="lyrico-lyrics-wrapper">body weightu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="body weightu "/>
</div>
<div class="lyrico-lyrics-wrapper">mellanga loosai pooyara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mellanga loosai pooyara"/>
</div>
<div class="lyrico-lyrics-wrapper">kallatho gaayam chesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kallatho gaayam chesi"/>
</div>
<div class="lyrico-lyrics-wrapper">naa kallanu kattesindhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa kallanu kattesindhi"/>
</div>
<div class="lyrico-lyrics-wrapper">guppedu  maatalu cheppi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="guppedu  maatalu cheppi"/>
</div>
<div class="lyrico-lyrics-wrapper">bhale goppaga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bhale goppaga "/>
</div>
<div class="lyrico-lyrics-wrapper">thappuku pothunaadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thappuku pothunaadhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ori ori oh my god
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ori ori oh my god"/>
</div>
<div class="lyrico-lyrics-wrapper">heart beat 100 nu thakera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="heart beat 100 nu thakera"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">bhrama dhevude 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bhrama dhevude "/>
</div>
<div class="lyrico-lyrics-wrapper">thappu chesara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thappu chesara"/>
</div>
<div class="lyrico-lyrics-wrapper">entha andhame endhukichera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="entha andhame endhukichera"/>
</div>
<div class="lyrico-lyrics-wrapper">manushule chuudaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manushule chuudaga"/>
</div>
<div class="lyrico-lyrics-wrapper">paiki sunnitham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paiki sunnitham"/>
</div>
<div class="lyrico-lyrics-wrapper">matalo dhacheru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="matalo dhacheru"/>
</div>
<div class="lyrico-lyrics-wrapper">entha karkasham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="entha karkasham"/>
</div>
<div class="lyrico-lyrics-wrapper">odhu ra mama odhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="odhu ra mama odhu"/>
</div>
<div class="lyrico-lyrics-wrapper">ee mayalo padane vadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ee mayalo padane vadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">budhiga vundalante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="budhiga vundalante"/>
</div>
<div class="lyrico-lyrics-wrapper">on pegguni kotti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="on pegguni kotti"/>
</div>
<div class="lyrico-lyrics-wrapper">kukkani pattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kukkani pattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ori ori oh my god
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ori ori oh my god"/>
</div>
<div class="lyrico-lyrics-wrapper">heart beat 100 nu thakera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="heart beat 100 nu thakera"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">okka matalo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="okka matalo "/>
</div>
<div class="lyrico-lyrics-wrapper">yedhi cheppare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yedhi cheppare"/>
</div>
<div class="lyrico-lyrics-wrapper">vunna patuga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vunna patuga"/>
</div>
<div class="lyrico-lyrics-wrapper">platu thippude
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="platu thippude"/>
</div>
<div class="lyrico-lyrics-wrapper">sutiga lifeke 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sutiga lifeke "/>
</div>
<div class="lyrico-lyrics-wrapper">cheppi welcomu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cheppi welcomu"/>
</div>
<div class="lyrico-lyrics-wrapper">jindhagi purthiga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jindhagi purthiga"/>
</div>
<div class="lyrico-lyrics-wrapper">chesi narakamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chesi narakamu"/>
</div>
<div class="lyrico-lyrics-wrapper">pacheeseata adi mamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pacheeseata adi mamu"/>
</div>
<div class="lyrico-lyrics-wrapper">pichodini chesthare 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pichodini chesthare "/>
</div>
<div class="lyrico-lyrics-wrapper">i love u da ante 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="i love u da ante "/>
</div>
<div class="lyrico-lyrics-wrapper">just we are friends 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="just we are friends "/>
</div>
<div class="lyrico-lyrics-wrapper">antuntare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antuntare"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ori ori oh my god
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ori ori oh my god"/>
</div>
<div class="lyrico-lyrics-wrapper">heart beat 100 nu thakera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="heart beat 100 nu thakera"/>
</div>
</pre>
