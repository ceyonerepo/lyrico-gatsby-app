---
title: 'jigidi killadi song lyrics'
album: 'Pattas'
artist: 'Vivek - Mervin'
lyricist: 'Vivek'
director: 'R.S.Durai Senthilkumar'
path: '/albums/pattas-song-lyrics'
song: 'Jigidi Killadi'
image: ../../images/albumart/pattas.jpg
date: 2020-01-15
lang: tamil
singers:
- Anirudh Ravichander Ravichander
youtubeLink: 'https://www.youtube.com/embed/bdHWFi5NfR0'
type: 'folk'
---



<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Ethir veetu heorini nee
<input type="checkbox" class="lyrico-select-lyric-line" value="Ethir veetu heorini nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Lemon mintu cooler maa nee
<input type="checkbox" class="lyrico-select-lyric-line" value="Lemon mintu cooler maa nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhoo konjam glamour-u dhaan nee
<input type="checkbox" class="lyrico-select-lyric-line" value="Yedhoo konjam glamour-u dhaan nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhukennamaaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Adhukennamaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Double xl torture maa nee
<input type="checkbox" class="lyrico-select-lyric-line" value="Double xl torture maa nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Padam ootum theater maa nee
<input type="checkbox" class="lyrico-select-lyric-line" value="Padam ootum theater maa nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Peter ku daughteru dhaan nee
<input type="checkbox" class="lyrico-select-lyric-line" value="Peter ku daughteru dhaan nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Salappaadhamma
<input type="checkbox" class="lyrico-select-lyric-line" value="Salappaadhamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thoondil pottu paaru
<input type="checkbox" class="lyrico-select-lyric-line" value="Thoondil pottu paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Puliyaa iruppaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Puliyaa iruppaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vala virichanna eliyaa kadippaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Vala virichanna eliyaa kadippaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Porivechadhumae kizhiyaa parappa
<input type="checkbox" class="lyrico-select-lyric-line" value="Porivechadhumae kizhiyaa parappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jigidi kilaadi
<input type="checkbox" class="lyrico-select-lyric-line" value="Jigidi kilaadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Pakkathula vantha
<input type="checkbox" class="lyrico-select-lyric-line" value="Pakkathula vantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Olikkum melody
<input type="checkbox" class="lyrico-select-lyric-line" value="Olikkum melody"/>
</div>
<div class="lyrico-lyrics-wrapper">Macham mattum illa
<input type="checkbox" class="lyrico-select-lyric-line" value="Macham mattum illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Agumark rowdy
<input type="checkbox" class="lyrico-select-lyric-line" value="Agumark rowdy"/>
</div>
<div class="lyrico-lyrics-wrapper">Namalukkae toughu
<input type="checkbox" class="lyrico-select-lyric-line" value="Namalukkae toughu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudukkum thirudi
<input type="checkbox" class="lyrico-select-lyric-line" value="Kudukkum thirudi"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhappu ammaadi
<input type="checkbox" class="lyrico-select-lyric-line" value="Adhappu ammaadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Jillu vudum   Jigidi kilaadi
<input type="checkbox" class="lyrico-select-lyric-line" value="Jillu vudum   Jigidi kilaadi"/>
</div>
  <div class="lyrico-lyrics-wrapper">Ginnu kannu   Jigidi kilaadi
<input type="checkbox" class="lyrico-select-lyric-line" value="Ginnu kannu   Jigidi kilaadi"/>
</div>
  <div class="lyrico-lyrics-wrapper">Ulla vara   Jigidi kilaadi
<input type="checkbox" class="lyrico-select-lyric-line" value="Ulla vara   Jigidi kilaadi"/>
</div>
  <div class="lyrico-lyrics-wrapper">Enna panna   Jigidi kilaadi (2 Times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Enna panna   Jigidi kilaadi"/></div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ethir veetu heorini nee
<input type="checkbox" class="lyrico-select-lyric-line" value="Ethir veetu heorini nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Lemon mintu cooler maa nee
<input type="checkbox" class="lyrico-select-lyric-line" value="Lemon mintu cooler maa nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhoo konjam glamour-u dhaan nee
<input type="checkbox" class="lyrico-select-lyric-line" value="Yedhoo konjam glamour-u dhaan nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhukennamaaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Adhukennamaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Double xl torture maa nee
<input type="checkbox" class="lyrico-select-lyric-line" value="Double xl torture maa nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Padam ootum theater maa nee
<input type="checkbox" class="lyrico-select-lyric-line" value="Padam ootum theater maa nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Peter ku daughteru dhaan nee
<input type="checkbox" class="lyrico-select-lyric-line" value="Peter ku daughteru dhaan nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Salappaadhamma
<input type="checkbox" class="lyrico-select-lyric-line" value="Salappaadhamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Emaathura head office-u
<input type="checkbox" class="lyrico-select-lyric-line" value="Emaathura head office-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Onnodadha Kddd……
<input type="checkbox" class="lyrico-select-lyric-line" value="Onnodadha Kddd……"/>
</div>
<div class="lyrico-lyrics-wrapper">Head weightla 8-u kilo
<input type="checkbox" class="lyrico-select-lyric-line" value="Head weightla 8-u kilo"/>
</div>
<div class="lyrico-lyrics-wrapper">Poyaachu di koodi
<input type="checkbox" class="lyrico-select-lyric-line" value="Poyaachu di koodi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Instavukkae nee illana
<input type="checkbox" class="lyrico-select-lyric-line" value="Instavukkae nee illana"/>
</div>
<div class="lyrico-lyrics-wrapper">Vyabaaram illana dii
<input type="checkbox" class="lyrico-select-lyric-line" value="Vyabaaram illana dii"/>
</div>
<div class="lyrico-lyrics-wrapper">Installmentil un dhuttu ellaam
<input type="checkbox" class="lyrico-select-lyric-line" value="Installmentil un dhuttu ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kariyaaukuren vaa dii
<input type="checkbox" class="lyrico-select-lyric-line" value="Kariyaaukuren vaa dii"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Bomma pulla ippo iva dhaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Bomma pulla ippo iva dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Annabelle
<input type="checkbox" class="lyrico-select-lyric-line" value="Annabelle"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaya vutta namma nilamai
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaaya vutta namma nilamai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirugal
<input type="checkbox" class="lyrico-select-lyric-line" value="Thirugal"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinna arakkinu oorula
<input type="checkbox" class="lyrico-select-lyric-line" value="Chinna arakkinu oorula"/>
</div>
<div class="lyrico-lyrics-wrapper">Thagaval
<input type="checkbox" class="lyrico-select-lyric-line" value="Thagaval"/>
</div>
<div class="lyrico-lyrics-wrapper">Jigidi Kilaadi
<input type="checkbox" class="lyrico-select-lyric-line" value="Jigidi Kilaadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Pakkathula vantha
<input type="checkbox" class="lyrico-select-lyric-line" value="Pakkathula vantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Olikkum melody
<input type="checkbox" class="lyrico-select-lyric-line" value="Olikkum melody"/>
</div>
<div class="lyrico-lyrics-wrapper">Macham mattum illa
<input type="checkbox" class="lyrico-select-lyric-line" value="Macham mattum illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Agumark rowdy
<input type="checkbox" class="lyrico-select-lyric-line" value="Agumark rowdy"/>
</div>
<div class="lyrico-lyrics-wrapper">Namalukkae toughu
<input type="checkbox" class="lyrico-select-lyric-line" value="Namalukkae toughu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudukkum thirudi
<input type="checkbox" class="lyrico-select-lyric-line" value="Kudukkum thirudi"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhappu ammaadi
<input type="checkbox" class="lyrico-select-lyric-line" value="Adhappu ammaadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Jillu vudum   Jigidi kilaadi
<input type="checkbox" class="lyrico-select-lyric-line" value="Jillu vudum   Jigidi kilaadi"/>
</div>
  <div class="lyrico-lyrics-wrapper">Ginnu kannu   Jigidi kilaadi
<input type="checkbox" class="lyrico-select-lyric-line" value="Ginnu kannu   Jigidi kilaadi"/>
</div>
  <div class="lyrico-lyrics-wrapper">Ulla vara   Jigidi kilaadi
<input type="checkbox" class="lyrico-select-lyric-line" value="Ulla vara   Jigidi kilaadi"/>
</div>
  <div class="lyrico-lyrics-wrapper">Enna panna   Jigidi kilaadi (2 Times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Enna panna   Jigidi kilaadi "/></div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ethir veetu heorini nee nee
<input type="checkbox" class="lyrico-select-lyric-line" value="Ethir veetu heorini nee nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Lemon mintu cooler maa nee
<input type="checkbox" class="lyrico-select-lyric-line" value="Lemon mintu cooler maa nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhoo konjam glamour-u dhaan nee
<input type="checkbox" class="lyrico-select-lyric-line" value="Yedhoo konjam glamour-u dhaan nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhukennamaaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Adhukennamaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Double xl torture maa nee
<input type="checkbox" class="lyrico-select-lyric-line" value="Double xl torture maa nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Padam ootum theater maa nee
<input type="checkbox" class="lyrico-select-lyric-line" value="Padam ootum theater maa nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Peter ku daughteru dhaan nee
<input type="checkbox" class="lyrico-select-lyric-line" value="Peter ku daughteru dhaan nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Salappaadhamma
<input type="checkbox" class="lyrico-select-lyric-line" value="Salappaadhamma"/>
</div>
</pre>