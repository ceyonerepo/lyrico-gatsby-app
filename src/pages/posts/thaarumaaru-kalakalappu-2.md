---
title: "thaarumaaru song lyrics"
album: "Kalakalappu 2"
artist: "Hiphop Tamizha"
lyricist: "Mohan Rajan - Hiphop Tamizha"
director: "Sundar C"
path: "/albums/kalakalappu-2-lyrics"
song: "Thaarumaaru"
image: ../../images/albumart/kalakalappu-2.jpg
date: 2018-02-09
lang: tamil
youtubeLink: "https://www.youtube.com/embed/3JvPvNDf6f8"
type: "love"
singers:
  - Sanjith Hegde
  - Sniggy
  - Hiphop Tamizha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kaiyapudhicha tharumaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyapudhicha tharumaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattiyanacha tharumaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattiyanacha tharumaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam siricha tharumaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam siricha tharumaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnalae pennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnalae pennae"/>
</div>
<div class="lyrico-lyrics-wrapper">En manasu sukku nooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En manasu sukku nooru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Okay okay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okay okay"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini ellam okay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini ellam okay"/>
</div>
<div class="lyrico-lyrics-wrapper">Locku lock-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Locku lock-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kooda lock-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kooda lock-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Annenae azhagiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annenae azhagiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanallum azhagula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanallum azhagula"/>
</div>
<div class="lyrico-lyrics-wrapper">Unatha minja intha ulagathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unatha minja intha ulagathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarum illayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarum illayae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">La la love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="La la love"/>
</div>
<div class="lyrico-lyrics-wrapper">La la love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="La la love"/>
</div>
<div class="lyrico-lyrics-wrapper">La la love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="La la love"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Non stop-u kadhalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Non stop-u kadhalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Full stop-u aagathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Full stop-u aagathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hi bye sonnalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hi bye sonnalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Heartbreaku-u podathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heartbreaku-u podathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un height enna sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un height enna sollu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un weight enna sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un weight enna sollu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna naan thooki konja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna naan thooki konja"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini gym poga poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini gym poga poren"/>
</div>
<div class="lyrico-lyrics-wrapper">Arnold-a maara poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arnold-a maara poren"/>
</div>
<div class="lyrico-lyrics-wrapper">Unkooda gethaaga vazhaporen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unkooda gethaaga vazhaporen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Come on
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Come on"/>
</div>
<div class="lyrico-lyrics-wrapper">Dance with me now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dance with me now"/>
</div>
<div class="lyrico-lyrics-wrapper">I got to show
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I got to show"/>
</div>
<div class="lyrico-lyrics-wrapper">All this love to you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="All this love to you"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Okay okay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okay okay"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini ellam okay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini ellam okay"/>
</div>
<div class="lyrico-lyrics-wrapper">Locku lock-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Locku lock-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kooda lock-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kooda lock-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Annenae azhagiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annenae azhagiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanallum azhagula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanallum azhagula"/>
</div>
<div class="lyrico-lyrics-wrapper">Unatha minja intha ulagathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unatha minja intha ulagathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarum illaYaarum illa…yaarum illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarum illaYaarum illa…yaarum illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ya ya ya ya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ya ya ya ya"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarum illayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarum illayae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">La la love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="La la love"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Naam agalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam agalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Naam agalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam agalam"/>
</div>
<div class="lyrico-lyrics-wrapper">La la love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="La la love"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaiyapudhicha tharumaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyapudhicha tharumaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattiyanacha tharumaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattiyanacha tharumaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam siricha tharumaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam siricha tharumaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnalae pennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnalae pennae"/>
</div>
<div class="lyrico-lyrics-wrapper">En manasu sukku nooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En manasu sukku nooru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Okay okay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okay okay"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini ellam okay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini ellam okay"/>
</div>
<div class="lyrico-lyrics-wrapper">Locku lock-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Locku lock-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kooda lock-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kooda lock-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Annenae azhagiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annenae azhagiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanallum azhagula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanallum azhagula"/>
</div>
<div class="lyrico-lyrics-wrapper">Unatha minja intha ulagathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unatha minja intha ulagathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarum illayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarum illayae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Naam agalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam agalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Naam agalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam agalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Naam agalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam agalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Naam agalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam agalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Come on
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Come on"/>
</div>
<div class="lyrico-lyrics-wrapper">Dance with me now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dance with me now"/>
</div>
<div class="lyrico-lyrics-wrapper">I got to show
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I got to show"/>
</div>
<div class="lyrico-lyrics-wrapper">All this love to you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="All this love to you"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ohoo oo hooo yeh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohoo oo hooo yeh"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohoo oo hooo yeh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohoo oo hooo yeh"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohoo oo hooo yeh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohoo oo hooo yeh"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohoo oo hooo yeh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohoo oo hooo yeh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Come on
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Come on"/>
</div>
<div class="lyrico-lyrics-wrapper">Dance with me now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dance with me now"/>
</div>
<div class="lyrico-lyrics-wrapper">I got to show
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I got to show"/>
</div>
<div class="lyrico-lyrics-wrapper">All this love to you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="All this love to you"/>
</div>
</pre>
