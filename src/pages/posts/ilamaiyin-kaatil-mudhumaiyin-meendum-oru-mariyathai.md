---
title: "ilamaiyin kaatil mudhumaiyin song lyrics"
album: "Meendum Oru Mariyathai"
artist: "N. R. Raghunanthan - Sharran Surya - Yuvan Shankar Raja"
lyricist: "Na. Muthukumar"
director: "Bharathiraja"
path: "/albums/meendum-oru-mariyathai-lyrics"
song: "Ilamaiyin Kaatil Mudhumaiyin"
image: ../../images/albumart/meendum-oru-mariyathai.jpg
date: 2020-02-21
lang: tamil
youtubeLink: "https://www.youtube.com/embed/i-OwOYGJ22U"
type: "haapy"
singers:
  - Sathyaprakash
  - Vaishali
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ilamayin Kaatil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilamayin Kaatil"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudhumayin Moongil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhumayin Moongil"/>
</div>
<div class="lyrico-lyrics-wrapper">Isaikindra Paatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Isaikindra Paatu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ketkirathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketkirathaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alai Kadal Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alai Kadal Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Anubhavam Modhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anubhavam Modhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayathil Saaral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayathil Saaral"/>
</div>
<div class="lyrico-lyrics-wrapper">Adikirathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adikirathaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nadhigalil Viluntha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadhigalil Viluntha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilai Elaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilai Elaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadhi Vazhi Sellum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadhi Vazhi Sellum"/>
</div>
<div class="lyrico-lyrics-wrapper">Padagu Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padagu Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidhi Vazhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidhi Vazhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Inaindhu Thirigirom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inaindhu Thirigirom"/>
</div>
<div class="lyrico-lyrics-wrapper">Parakkave Vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parakkave Vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Siragu Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siragu Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaatrodu Mella Kai Korthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatrodu Mella Kai Korthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pala Kadhai Pesi Naam Nadakalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala Kadhai Pesi Naam Nadakalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ilamayin Kaatil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilamayin Kaatil"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudhumayin Moongil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhumayin Moongil"/>
</div>
<div class="lyrico-lyrics-wrapper">Isaikindra Paatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Isaikindra Paatu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ketkirathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketkirathaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alai Kadal Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alai Kadal Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Anubhavam Modhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anubhavam Modhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayathil Saaral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayathil Saaral"/>
</div>
<div class="lyrico-lyrics-wrapper">Adikirathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adikirathaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Yaar Ivan Endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Yaar Ivan Endru"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar Ival Endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar Ival Endru"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoorathil Paravai Paarkirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoorathil Paravai Paarkirathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Baarangal Yaavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Baarangal Yaavum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thooramaai Oda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thooramaai Oda"/>
</div>
<div class="lyrico-lyrics-wrapper">Eeramaai Kangal Thulirkirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eeramaai Kangal Thulirkirathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Netru Adhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Netru Adhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Odi Ponathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odi Ponathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Varum Naalai Engeyo Ullathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varum Naalai Engeyo Ullathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Intha Nimidam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Nimidam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nam Kaiyil Ullathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam Kaiyil Ullathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Kondaada Ul Nenjam Solluthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondaada Ul Nenjam Solluthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pudhu Vasantha Kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu Vasantha Kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nam Kilaiyil Thondrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam Kilaiyil Thondrum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhi Paadhai Dhorum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi Paadhai Dhorum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanna Poo Maname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanna Poo Maname"/>
</div>
<div class="lyrico-lyrics-wrapper">Pon Vidiyal Varum Vaanil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pon Vidiyal Varum Vaanil"/>
</div>
<div class="lyrico-lyrics-wrapper">Poru Maname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poru Maname"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ilamayin Kaatil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilamayin Kaatil"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudhumayin Moongil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhumayin Moongil"/>
</div>
<div class="lyrico-lyrics-wrapper">Isaikindra Paatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Isaikindra Paatu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ketkirathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketkirathaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alai Kadal Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alai Kadal Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Anubhavam Modhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anubhavam Modhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayathil Saaral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayathil Saaral"/>
</div>
<div class="lyrico-lyrics-wrapper">Adikirathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adikirathaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ohoo Nee Oru Payani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohoo Nee Oru Payani"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Oru Payani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Oru Payani"/>
</div>
<div class="lyrico-lyrics-wrapper">Padhaiyo Romba Neendadhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padhaiyo Romba Neendadhadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ohoo Nee Oru Kanavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohoo Nee Oru Kanavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Oru Kanavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Oru Kanavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangalo Nammai Vendudhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangalo Nammai Vendudhadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Siru Poovil Paniyaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru Poovil Paniyaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Irukalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irukalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadum Veyilil Karinthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadum Veyilil Karinthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirikalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirikalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nam Inba Thunbangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam Inba Thunbangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Marakalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marakalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Adiyodu Veraaga Pirakalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiyodu Veraaga Pirakalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sila Kadhaigal Thodarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sila Kadhaigal Thodarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sila Kadhaigal Mudiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sila Kadhaigal Mudiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu Mudintha Piragum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu Mudintha Piragum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingu Thodarkirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingu Thodarkirathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu Vidaigal Kedaiyaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu Vidaigal Kedaiyaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidukadhaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidukadhaiye"/>
</div>
</pre>
