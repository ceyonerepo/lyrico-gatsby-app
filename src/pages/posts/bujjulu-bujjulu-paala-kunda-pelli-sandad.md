---
title: "bujjulu bujjulu song lyrics"
album: "Pelli Sanda D"
artist: "M.M.keeravaani"
lyricist: "Chandrabose"
director: "Gowri Ronanki"
path: "/albums/pelli-sandad-lyrics"
song: "Bujjulu Bujjulu - Paala Kunda"
image: ../../images/albumart/pelli-sandad.jpg
date: 2021-10-15
lang: telugu
youtubeLink: "https://www.youtube.com/embed/YenrySU7fN8"
type: "love"
singers:
  - Baba Sehgal
  - Mangli
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Paala kunda netthinetti panjugutta pothavunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paala kunda netthinetti panjugutta pothavunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Borabanda poragaadu rayi petti kottinadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Borabanda poragaadu rayi petti kottinadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Rayi petti kottinadu rayi petti kottinadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rayi petti kottinadu rayi petti kottinadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kunda paalu gutta gutta gutakalesi thaginadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kunda paalu gutta gutta gutakalesi thaginadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chillu padda kunda thoti kottina intiketta ponuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chillu padda kunda thoti kottina intiketta ponuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Porada nee meeda kopamochhera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porada nee meeda kopamochhera"/>
</div>
<div class="lyrico-lyrics-wrapper">Kopamochhera kopamochhera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kopamochhera kopamochhera"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee bunga moothi soodaneeki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee bunga moothi soodaneeki"/>
</div>
<div class="lyrico-lyrics-wrapper">Rayi thoti kottina kanti yerupu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rayi thoti kottina kanti yerupu"/>
</div>
<div class="lyrico-lyrics-wrapper">Soodaniki kunda pagalakottina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soodaniki kunda pagalakottina"/>
</div>
<div class="lyrico-lyrics-wrapper">Alaka needhi soodaneeku allrentho chesina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alaka needhi soodaneeku allrentho chesina"/>
</div>
<div class="lyrico-lyrics-wrapper">Bujjulu bujjulu bujjulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bujjulu bujjulu bujjulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Bujjulu bujjulu bujjulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bujjulu bujjulu bujjulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Konipedatha bangaru gajjelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konipedatha bangaru gajjelu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Bujjulu bujjulu bujjulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Bujjulu bujjulu bujjulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thinipistha theeyani munjulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thinipistha theeyani munjulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gosa theeripoyelaaga gummaristha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gosa theeripoyelaaga gummaristha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ghatu ghatu natu natu muddhulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ghatu ghatu natu natu muddhulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Gosa theeripoyelaaga gummaristha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Gosa theeripoyelaaga gummaristha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ghatu ghatu natu natu muddhulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ghatu ghatu natu natu muddhulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ghatu ghatu natu natu muddhulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ghatu ghatu natu natu muddhulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Bujjulu bujjulu bujjulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bujjulu bujjulu bujjulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattukunta bangaru gajjelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattukunta bangaru gajjelu"/>
</div>
<div class="lyrico-lyrics-wrapper">Bujjulu bujjulu bujjulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bujjulu bujjulu bujjulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Koriki thinta theeyani munjulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koriki thinta theeyani munjulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gosa theeripoyelaga teesukunta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gosa theeripoyelaga teesukunta"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee ghatu ghatu natu natu muddhulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee ghatu ghatu natu natu muddhulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Gosa theeripoyelaga teesukunta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Gosa theeripoyelaga teesukunta"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee ghatu ghatu natu natu muddhulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee ghatu ghatu natu natu muddhulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee ghatu ghatu natu natu muddhulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee ghatu ghatu natu natu muddhulu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chamkilu konaneeki charminar pothavunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chamkilu konaneeki charminar pothavunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Apsalgunj kada addamochinav
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Apsalgunj kada addamochinav"/>
</div>
<div class="lyrico-lyrics-wrapper">Bukka gulal bugga meedha jallinav
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bukka gulal bugga meedha jallinav"/>
</div>
<div class="lyrico-lyrics-wrapper">Meeda jallinav meeda jallinav
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meeda jallinav meeda jallinav"/>
</div>
<div class="lyrico-lyrics-wrapper">Bugga meeda jallinav
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bugga meeda jallinav"/>
</div>
<div class="lyrico-lyrics-wrapper">Bukka gulal bugga meedha jallinav
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bukka gulal bugga meedha jallinav"/>
</div>
<div class="lyrico-lyrics-wrapper">Rangu chusi maa ayya rankesthadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangu chusi maa ayya rankesthadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Machha chusi maa amma rachha chesthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machha chusi maa amma rachha chesthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Porada nee thoti peekulaatara ha haa haa...
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porada nee thoti peekulaatara ha haa haa..."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mee ammante bayamantav
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mee ammante bayamantav"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenante premantav ayyante vanukantav
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenante premantav ayyante vanukantav"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannu chusthe kulukantav
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannu chusthe kulukantav"/>
</div>
<div class="lyrico-lyrics-wrapper">Mee ammante bayamantav
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mee ammante bayamantav"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenante premantav ayyante vanukantav
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenante premantav ayyante vanukantav"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannu chusthe kulukantav
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannu chusthe kulukantav"/>
</div>
<div class="lyrico-lyrics-wrapper">Andharikante nenu istamantav istamantav
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andharikante nenu istamantav istamantav"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa muchhtantha lolone daachukuntav
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa muchhtantha lolone daachukuntav"/>
</div>
<div class="lyrico-lyrics-wrapper">Bujjulu bujjulu bujjulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bujjulu bujjulu bujjulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Bujjulu bujjulu bujjulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bujjulu bujjulu bujjulu"/>
</div>
<div class="lyrico-lyrics-wrapper">korikestha ravvala gajulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="korikestha ravvala gajulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Bujjulu bujjulu bujjulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bujjulu bujjulu bujjulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pettestha kaaliki mettelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pettestha kaaliki mettelu"/>
</div>
<div class="lyrico-lyrics-wrapper">Addu evvadochhi nannu aaputhunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Addu evvadochhi nannu aaputhunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Katti vestha medalo pusthelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katti vestha medalo pusthelu"/>
</div>
<div class="lyrico-lyrics-wrapper">Addu evvadochhi nannu aaputhunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Addu evvadochhi nannu aaputhunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Katti vestha medalo pusthelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katti vestha medalo pusthelu"/>
</div>
<div class="lyrico-lyrics-wrapper">Katti vestha medalo pusthelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katti vestha medalo pusthelu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bujjulu bujjulu bujjulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bujjulu bujjulu bujjulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Andhukunta ravvala gajulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhukunta ravvala gajulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Bujjulu bujjulu bujjulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bujjulu bujjulu bujjulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pettukunta kaaliki mettelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pettukunta kaaliki mettelu"/>
</div>
<div class="lyrico-lyrics-wrapper">Addam evvadochhimana aaputhunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Addam evvadochhimana aaputhunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattamanta medalo pusthelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattamanta medalo pusthelu"/>
</div>
<div class="lyrico-lyrics-wrapper">Addam evvadochhimana aaputhunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Addam evvadochhimana aaputhunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattamanta medalo pusthelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattamanta medalo pusthelu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayipodham aalu magalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayipodham aalu magalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhalle bhalle bhalle bujjulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhalle bhalle bhalle bujjulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhalle bhalle bhalle bujjulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhalle bhalle bhalle bujjulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhalle ha bhalle ha bhalle bujjulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhalle ha bhalle ha bhalle bujjulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhalle ha bhalle ha bhalle bujjulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhalle ha bhalle ha bhalle bujjulu"/>
</div>
</pre>
