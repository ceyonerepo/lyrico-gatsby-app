---
title: "oru paarvaizhala song lyrics"
album: "Idiot"
artist: "Vikram Selva"
lyricist: "Mani Amudhan "
director: "Rambhala"
path: "/albums/idiot-lyrics"
song: "Oru Paarvaizhala"
image: ../../images/albumart/idiot.jpg
date: 2022-04-01
lang: tamil
youtubeLink: "https://www.youtube.com/embed/F8joNW6XH-w"
type: "love"
singers:
  - Anand Aravind Akash
  - Balaji
  - Arvind Annest
  - Shembaga Raj
  - Hemesh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Oru Paarvaizha La Avagaricha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Paarvaizha La Avagaricha"/>
</div>
<div class="lyrico-lyrics-wrapper">You Make My Soul 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Make My Soul "/>
</div>
<div class="lyrico-lyrics-wrapper">Sing Sing Swing Swing
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sing Sing Swing Swing"/>
</div>
<div class="lyrico-lyrics-wrapper">Siru Punagaiaala 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru Punagaiaala "/>
</div>
<div class="lyrico-lyrics-wrapper">La Sedharadicha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="La Sedharadicha"/>
</div>
<div class="lyrico-lyrics-wrapper">Lip Lip Kiss Kiss 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lip Lip Kiss Kiss "/>
</div>
<div class="lyrico-lyrics-wrapper">Focus On Happiness
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Focus On Happiness"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ettu Eduthu Neeyum Kalla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettu Eduthu Neeyum Kalla"/>
</div>
<div class="lyrico-lyrics-wrapper">Etti Enna Paakum Vela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Etti Enna Paakum Vela"/>
</div>
<div class="lyrico-lyrics-wrapper">Ettu Kallu Poochi Polla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettu Kallu Poochi Polla"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoongurenae Nanum Mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoongurenae Nanum Mela"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Always Loving You
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Always Loving You"/>
</div>
<div class="lyrico-lyrics-wrapper">Dreaming Of U And Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dreaming Of U And Me"/>
</div>
<div class="lyrico-lyrics-wrapper">Mickey Mouse Run Away
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mickey Mouse Run Away"/>
</div>
<div class="lyrico-lyrics-wrapper">Keep Smile For A While
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Keep Smile For A While"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Per Ine Thevai- Ey Illa- Ey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Per Ine Thevai- Ey Illa- Ey"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Perilae Azhaikalam Ennai-Ey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Perilae Azhaikalam Ennai-Ey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chocolate Baby Heart
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chocolate Baby Heart"/>
</div>
<div class="lyrico-lyrics-wrapper">Wanna Bite Moon Light
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wanna Bite Moon Light"/>
</div>
<div class="lyrico-lyrics-wrapper">Come Come Shake Shake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Come Come Shake Shake"/>
</div>
<div class="lyrico-lyrics-wrapper">Fun Fun Always
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fun Fun Always"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Dhevadhai Enakkena 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Dhevadhai Enakkena "/>
</div>
<div class="lyrico-lyrics-wrapper">Pirandhu Vittaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirandhu Vittaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Bip Lop Lip Bob 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bip Lop Lip Bob "/>
</div>
<div class="lyrico-lyrics-wrapper">One Man Falling Love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="One Man Falling Love"/>
</div>
<div class="lyrico-lyrics-wrapper">En Uyirinil Uyir 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Uyirinil Uyir "/>
</div>
<div class="lyrico-lyrics-wrapper">Ena Kalandhu Vittal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ena Kalandhu Vittal"/>
</div>
<div class="lyrico-lyrics-wrapper">Bip Lop Lip Bob 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bip Lop Lip Bob "/>
</div>
<div class="lyrico-lyrics-wrapper">Who’S That Lucky Girl
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Who’S That Lucky Girl"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endhan Padhai Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan Padhai Ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan Veettai Serum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan Veettai Serum"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhan Vaarthai Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan Vaarthai Ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Patriyae Koorum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Patriyae Koorum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Knock Bang Bang Gang Gang
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Knock Bang Bang Gang Gang"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheer Up Long Walk Road Trip
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheer Up Long Walk Road Trip"/>
</div>
<div class="lyrico-lyrics-wrapper">Candle Light Dinner Night
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Candle Light Dinner Night"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Uyirin Edai Dhan Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Uyirin Edai Dhan Enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Edai Pottaal Theriyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Edai Pottaal Theriyum"/>
</div>
<div class="lyrico-lyrics-wrapper">En Aayul Neelam Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Aayul Neelam Enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Un A Kadhal Alavae Adhuvum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un A Kadhal Alavae Adhuvum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lets Go Bora Bora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lets Go Bora Bora"/>
</div>
<div class="lyrico-lyrics-wrapper">I Am Mean Pora Pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Am Mean Pora Pora"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Dhevadhai Enakkena 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Dhevadhai Enakkena "/>
</div>
<div class="lyrico-lyrics-wrapper">Pirandhu Vittaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirandhu Vittaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Bip Lop Lip Bob 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bip Lop Lip Bob "/>
</div>
<div class="lyrico-lyrics-wrapper">One Man Falling Love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="One Man Falling Love"/>
</div>
<div class="lyrico-lyrics-wrapper">En Uyirinil Uyir 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Uyirinil Uyir "/>
</div>
<div class="lyrico-lyrics-wrapper">Ena Kalandhu Vittal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ena Kalandhu Vittal"/>
</div>
<div class="lyrico-lyrics-wrapper">Bip Lop Lip Bob 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bip Lop Lip Bob "/>
</div>
<div class="lyrico-lyrics-wrapper">Who’S That Lucky Girl
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Who’S That Lucky Girl"/>
</div>
</pre>
