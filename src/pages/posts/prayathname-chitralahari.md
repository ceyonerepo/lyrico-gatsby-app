---
title: "prayathname song lyrics"
album: "Chitralahari"
artist: "Devi Sri Prasad"
lyricist: "Chandrabose"
director: "Kishore Tirumala"
path: "/albums/chitralahari-lyrics"
song: "Prayathname"
image: ../../images/albumart/chitralahari.jpg
date: 2019-04-12
lang: telugu
youtubeLink: "https://www.youtube.com/embed/vSIE_Pi1ejs"
type: "sad"
singers:
  - Kailash Kher
  - Vishnupriya Ravi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vodipovadam Ante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vodipovadam Ante"/>
</div>
<div class="lyrico-lyrics-wrapper">Aagipovadam Kaadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagipovadam Kaadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Marintha Goppaga Porade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marintha Goppaga Porade"/>
</div>
<div class="lyrico-lyrics-wrapper">Avakasham Pondhadame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avakasham Pondhadame"/>
</div>
<div class="lyrico-lyrics-wrapper">Adugu Adugu Veyyanidhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adugu Adugu Veyyanidhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Antharikshame Andhenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antharikshame Andhenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Paduthu Paduthu Levanidhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paduthu Paduthu Levanidhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Pasi Paadam Parugulu Theesena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pasi Paadam Parugulu Theesena"/>
</div>
<div class="lyrico-lyrics-wrapper">Munigi Munigi Thelanidhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munigi Munigi Thelanidhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Mahasandhrame Longenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mahasandhrame Longenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Karigi Karigi Velaganidhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karigi Karigi Velaganidhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Kovvotthi Cheekatini Tharimena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kovvotthi Cheekatini Tharimena"/>
</div>
<div class="lyrico-lyrics-wrapper">Mugimpe Emayinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mugimpe Emayinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhyalo Vadhaloddhura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhyalo Vadhaloddhura"/>
</div>
<div class="lyrico-lyrics-wrapper">Eee Sadhana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eee Sadhana"/>
</div>
<div class="lyrico-lyrics-wrapper">Prayathname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prayathname"/>
</div>
<div class="lyrico-lyrics-wrapper">Modati Vijayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Modati Vijayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Prayathname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prayathname"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana Aayudham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana Aayudham"/>
</div>
<div class="lyrico-lyrics-wrapper">Prayathname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prayathname"/>
</div>
<div class="lyrico-lyrics-wrapper">Modati Vijayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Modati Vijayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Prayathname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prayathname"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana Aayudham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana Aayudham"/>
</div>
<div class="lyrico-lyrics-wrapper">Vodipovadam Ante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vodipovadam Ante"/>
</div>
<div class="lyrico-lyrics-wrapper">Aagipovadam Kaadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagipovadam Kaadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Marintha Goppaga Porade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marintha Goppaga Porade"/>
</div>
<div class="lyrico-lyrics-wrapper">Avakasham Pondhadame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avakasham Pondhadame"/>
</div>
<div class="lyrico-lyrics-wrapper">Velle Daarullonaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velle Daarullonaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Raalle Addosthunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raalle Addosthunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Adduni Kastha Mettuga Malichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adduni Kastha Mettuga Malichi"/>
</div>
<div class="lyrico-lyrics-wrapper">Etthuku Edhagali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Etthuku Edhagali"/>
</div>
<div class="lyrico-lyrics-wrapper">Chese Poratamlo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chese Poratamlo"/>
</div>
<div class="lyrico-lyrics-wrapper">Raktham Chindesthunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raktham Chindesthunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Erra Sira Gaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Erra Sira Gaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Charithani Raasthundhanukovali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Charithani Raasthundhanukovali"/>
</div>
<div class="lyrico-lyrics-wrapper">Adugantu Vesaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adugantu Vesaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Aagakunda Sagali Raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagakunda Sagali Raa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Sadhana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Sadhana"/>
</div>
<div class="lyrico-lyrics-wrapper">Prayathname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prayathname"/>
</div>
<div class="lyrico-lyrics-wrapper">Modati Vijayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Modati Vijayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Prayathname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prayathname"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana Aayudham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana Aayudham"/>
</div>
<div class="lyrico-lyrics-wrapper">Prayathname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prayathname"/>
</div>
<div class="lyrico-lyrics-wrapper">Modati Vijayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Modati Vijayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Prayathname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prayathname"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana Aayudham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana Aayudham"/>
</div>
<div class="lyrico-lyrics-wrapper">Vodipovadam Ante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vodipovadam Ante"/>
</div>
<div class="lyrico-lyrics-wrapper">Aagipovadam Kaadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagipovadam Kaadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Marintha Goppaga Porade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marintha Goppaga Porade"/>
</div>
<div class="lyrico-lyrics-wrapper">Avakasham Pondhadame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avakasham Pondhadame"/>
</div>
</pre>
