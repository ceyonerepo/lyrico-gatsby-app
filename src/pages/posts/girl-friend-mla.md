---
title: "girl friend song lyrics"
album: "MLA"
artist: "Mani Sharma"
lyricist: "Ramajogayya Sastry"
director: "Upendra Madhav"
path: "/albums/mla-lyrics"
song: "Girl Friend"
image: ../../images/albumart/mla.jpg
date: 2018-03-23
lang: telugu
youtubeLink: "https://www.youtube.com/embed/FVlbGpsP5uI"
type: "love"
singers:
  -	Anurag Kulkarni
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Girlfriend.. Girlfriend
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Girlfriend.. Girlfriend"/>
</div>
<div class="lyrico-lyrics-wrapper">All the best andi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="All the best andi"/>
</div>
<div class="lyrico-lyrics-wrapper">Bujji heart beatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bujji heart beatu"/>
</div>
<div class="lyrico-lyrics-wrapper">Doosukellipota inkenduk anta late
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Doosukellipota inkenduk anta late"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ori devuo nuvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ori devuo nuvu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enta kutra chesavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enta kutra chesavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Una chota una
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Una chota una"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa zindagini kalipavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa zindagini kalipavu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chalo padi padi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chalo padi padi"/>
</div>
<div class="lyrico-lyrics-wrapper">Tada padi yega padi chebuta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tada padi yega padi chebuta"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanks a lot niku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanks a lot niku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Are re girlfriend girlfriend
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Are re girlfriend girlfriend"/>
</div>
<div class="lyrico-lyrics-wrapper">Nachindi naku girlfriend
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nachindi naku girlfriend"/>
</div>
<div class="lyrico-lyrics-wrapper">Are re girlfriend girlfriend
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Are re girlfriend girlfriend"/>
</div>
<div class="lyrico-lyrics-wrapper">Nachindi naku girlfriend
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nachindi naku girlfriend"/>
</div>
<div class="lyrico-lyrics-wrapper">Are re girlfriend girlfriend
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Are re girlfriend girlfriend"/>
</div>
<div class="lyrico-lyrics-wrapper">Nachindi naku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nachindi naku"/>
</div>
<div class="lyrico-lyrics-wrapper">Girlfriend girlfriend
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Girlfriend girlfriend"/>
</div>
<div class="lyrico-lyrics-wrapper">Nachindi naku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nachindi naku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaro sense apude andi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaro sense apude andi"/>
</div>
<div class="lyrico-lyrics-wrapper">Tanu naku soulmate ani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tanu naku soulmate ani"/>
</div>
<div class="lyrico-lyrics-wrapper">Hot wheels pai swaari chestu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hot wheels pai swaari chestu"/>
</div>
<div class="lyrico-lyrics-wrapper">kadilindi naa kalala journey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadilindi naa kalala journey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manasantha traffic jam em chestu una
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasantha traffic jam em chestu una"/>
</div>
<div class="lyrico-lyrics-wrapper">Rojanta tanamate alochistu una
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rojanta tanamate alochistu una"/>
</div>
<div class="lyrico-lyrics-wrapper">Eno eno hurricanelu jantakoodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eno eno hurricanelu jantakoodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Na pai dooki chestu unte prema dhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na pai dooki chestu unte prema dhadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa taluvari tallupula 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa taluvari tallupula "/>
</div>
<div class="lyrico-lyrics-wrapper">rootupavanalaku pulakarinchipoya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rootupavanalaku pulakarinchipoya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Are re girlfriend girlfriend
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Are re girlfriend girlfriend"/>
</div>
<div class="lyrico-lyrics-wrapper">Nachindi naku girlfriend
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nachindi naku girlfriend"/>
</div>
<div class="lyrico-lyrics-wrapper">Are re girlfriend girlfriend
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Are re girlfriend girlfriend"/>
</div>
<div class="lyrico-lyrics-wrapper">Nachindi naku girlfriend
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nachindi naku girlfriend"/>
</div>
<div class="lyrico-lyrics-wrapper">Are re girlfriend girlfriend
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Are re girlfriend girlfriend"/>
</div>
<div class="lyrico-lyrics-wrapper">Nachindi naku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nachindi naku"/>
</div>
<div class="lyrico-lyrics-wrapper">Girlfriend girlfriend
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Girlfriend girlfriend"/>
</div>
<div class="lyrico-lyrics-wrapper">Nachindi naku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nachindi naku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ooru peru emo gani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooru peru emo gani"/>
</div>
<div class="lyrico-lyrics-wrapper">Edayithe emundi le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edayithe emundi le"/>
</div>
<div class="lyrico-lyrics-wrapper">Tiru tellu bale bagundi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tiru tellu bale bagundi"/>
</div>
<div class="lyrico-lyrics-wrapper">nachindi anduvale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nachindi anduvale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bangaram manasantu ante vinanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bangaram manasantu ante vinanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aamate manishayte tane antanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aamate manishayte tane antanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolate leni ettulo thelipoya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolate leni ettulo thelipoya"/>
</div>
<div class="lyrico-lyrics-wrapper">Digiraleni mattulo undipoya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Digiraleni mattulo undipoya"/>
</div>
<div class="lyrico-lyrics-wrapper">Nane evar ani adigite
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nane evar ani adigite"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee kshanamuna peru marichipoya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee kshanamuna peru marichipoya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Are re girlfriend girlfriend
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Are re girlfriend girlfriend"/>
</div>
<div class="lyrico-lyrics-wrapper">Nachindi naku girlfriend
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nachindi naku girlfriend"/>
</div>
<div class="lyrico-lyrics-wrapper">Are re girlfriend girlfriend
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Are re girlfriend girlfriend"/>
</div>
<div class="lyrico-lyrics-wrapper">Nachindi naku girlfriend
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nachindi naku girlfriend"/>
</div>
<div class="lyrico-lyrics-wrapper">Are re girlfriend girlfriend
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Are re girlfriend girlfriend"/>
</div>
<div class="lyrico-lyrics-wrapper">Nachindi naku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nachindi naku"/>
</div>
<div class="lyrico-lyrics-wrapper">Girlfriend girlfriend
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Girlfriend girlfriend"/>
</div>
<div class="lyrico-lyrics-wrapper">Nachindi naku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nachindi naku"/>
</div>
</pre>
