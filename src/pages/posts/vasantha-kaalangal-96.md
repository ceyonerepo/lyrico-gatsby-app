---
title: "vasantha kaalangal song lyrics"
album: "96"
artist: "Govind Vasantha"
lyricist: "Uma Devi"
director: "C Premkumar"
path: "/albums/96-lyrics"
song: "Vasantha Kaalangal"
image: ../../images/albumart/96.jpg
date: 2018-10-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/622tpLBfntI"
type: "sad"
singers:
  - Chinmayi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vasantha kaalangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vasantha kaalangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kasanthu poguthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kasanthu poguthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enathu thoorangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enathu thoorangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooyadho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooyadho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyirin thaagangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirin thaagangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kidanthu saaguthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kidanthu saaguthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadantha kaalangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadantha kaalangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaradho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaradho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paarvaiyin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarvaiyin"/>
</div>
<div class="lyrico-lyrics-wrapper">Paaramayil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaramayil"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhumo"/>
</div>
<div class="lyrico-lyrics-wrapper">En nenjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En nenjam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaarthaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaarthaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kozhaipol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kozhaipol"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaazhirunthum raagamindri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaazhirunthum raagamindri"/>
</div>
<div class="lyrico-lyrics-wrapper">Yengi poguthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yengi poguthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vasantha kaalangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vasantha kaalangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kasanthu poguthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kasanthu poguthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enathu thoorangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enathu thoorangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooyadho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooyadho"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmm kaadhalin vedhangalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm kaadhalin vedhangalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Nyanyangal maari poguthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nyanyangal maari poguthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennangal meeriduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennangal meeriduthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa…baarangal megam aaguthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa…baarangal megam aaguthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pathaigal nooraai thondruthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathaigal nooraai thondruthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnodu ondragavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnodu ondragavae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal nilavaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal nilavaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada naan kaayava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada naan kaayava"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalai ozhiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalai ozhiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Emarava vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emarava vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaayum irulil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaayum irulil"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada nee vaazhava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada nee vaazhava"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidiyumintha kaalai namathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidiyumintha kaalai namathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vasantha kaalangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vasantha kaalangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kasanthu poguthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kasanthu poguthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enathu thoorangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enathu thoorangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooyadho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooyadho"/>
</div>
</pre>
