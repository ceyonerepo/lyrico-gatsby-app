---
title: "idhu enna maayamo song lyrics"
album: "Adithya Varma"
artist: "Radhan"
lyricist: "Sivakarthikeyan"
director: "Gireesaaya"
path: "/albums/adithya-varma-lyrics"
song: "Idhu Enna Maayamo"
image: ../../images/albumart/adithya-varma.jpg
date: 2019-11-22
lang: tamil
youtubeLink: "https://www.youtube.com/embed/QXGdlq6W8GA"
type: "love"
singers:
  - Ranjith Govind
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Idhu Enna Maayamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Enna Maayamo"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakenna Aanadhoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakenna Aanadhoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayathil Kaayamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayathil Kaayamo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal Undaanadho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Undaanadho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu Enna Maayamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Enna Maayamo"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakenna Aanadhoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakenna Aanadhoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirukkul Sugamoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirukkul Sugamoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam Thallaaduthoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam Thallaaduthoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nijamaa Idhu Nijamaa En Anbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijamaa Idhu Nijamaa En Anbae"/>
</div>
<div class="lyrico-lyrics-wrapper">Varamaa Unai Adaivenae Naan Indre
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varamaa Unai Adaivenae Naan Indre"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavaai Dhinam Unaiyae Kandaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavaai Dhinam Unaiyae Kandaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanavaai Nee Vendum Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanavaai Nee Vendum Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mudhal Mudhal Endhan Manadhinil Pudhu Maatram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhal Mudhal Endhan Manadhinil Pudhu Maatram"/>
</div>
<div class="lyrico-lyrics-wrapper">Mugam Muzhuvathum Indha Siripinai Kaattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mugam Muzhuvathum Indha Siripinai Kaattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Iru Vizhigalum Ival Yarena Ketkkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iru Vizhigalum Ival Yarena Ketkkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagae Uyirae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagae Uyirae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaarodumae Illaathadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarodumae Illaathadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Enna Endru Yaarodum Sollathadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Enna Endru Yaarodum Sollathadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarvaigalalil Vandhaathidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarvaigalalil Vandhaathidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagiya Pennin Kangal Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagiya Pennin Kangal Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppodhum Polladhadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppodhum Polladhadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irava Adhu Pagalaa Theriyamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irava Adhu Pagalaa Theriyamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Varavaa Un Nizhalaaga Naan Ingae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varavaa Un Nizhalaaga Naan Ingae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nagaraa Indha Nodigal Azhiyaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nagaraa Indha Nodigal Azhiyaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Perava Un Kaadhalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perava Un Kaadhalai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakena Oru Pirapeduthaval Neeyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakena Oru Pirapeduthaval Neeyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakullae Enai Dhinam Erikkira Theeyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakullae Enai Dhinam Erikkira Theeyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Arakkanaiyae Adaikkida Nee Vaa Anbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arakkanaiyae Adaikkida Nee Vaa Anbae"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagae Uyirae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagae Uyirae"/>
</div>
</pre>
