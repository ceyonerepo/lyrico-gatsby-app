---
title: "en iniya thanimaye lyrics"
album: "teddy"
artist: "D Imman"
lyricist: "madhan karky"
director: "shakti soundar rajan"
path: "/albums/teddy-song-lyrics"
song: "en iniya thanimaye"
image: ../../images/albumart/teddy.jpg
date: 2021-03-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/U0tOvqAmcb8"
type: "motivational"
singers:
  - Sid sriram
---

<pre class="lyrics-native">
</pre>
<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">En Iniya Thanimaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Iniya Thanimaye"/>
</div>
<div class="lyrico-lyrics-wrapper">En Iniya Thanimaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Iniya Thanimaye"/>
</div>
<div class="lyrico-lyrics-wrapper">En Iniya Thanimaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Iniya Thanimaye"/>
</div>
<div class="lyrico-lyrics-wrapper">En Iniya Thanimaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Iniya Thanimaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Puthithana Adhikalaiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthithana Adhikalaiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Pugai Soogum Nedunjalaiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pugai Soogum Nedunjalaiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnodu Naan Nadanthal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnodu Naan Nadanthal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaam Perazhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaam Perazhagu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mazhai Veezhum Ila Maalayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai Veezhum Ila Maalayo"/>
</div>
<div class="lyrico-lyrics-wrapper">Isaiyilla Idaivelayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Isaiyilla Idaivelayo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennodu Nee Nadanthal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennodu Nee Nadanthal"/>
</div>
<div class="lyrico-lyrics-wrapper">Inbam En Ulagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inbam En Ulagu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnodu Mattum Thaan..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnodu Mattum Thaan.."/>
</div>
<div class="lyrico-lyrics-wrapper">En Neram Enathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Neram Enathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnodu Mattum Thaan..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnodu Mattum Thaan.."/>
</div>
<div class="lyrico-lyrics-wrapper">Mei Pesum Manadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mei Pesum Manadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manitharin Mozhi Kettu.. Kettu..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manitharin Mozhi Kettu.. Kettu.."/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayam Pazhuthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam Pazhuthai"/>
</div>
<div class="lyrico-lyrics-wrapper">Unadhamaithiyil Thaane..aanen..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unadhamaithiyil Thaane..aanen.."/>
</div>
<div class="lyrico-lyrics-wrapper">Muzhu Muzhudhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muzhu Muzhudhai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Iniya Thanimaye..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Iniya Thanimaye.."/>
</div>
<div class="lyrico-lyrics-wrapper">En Iniya Thanimaye..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Iniya Thanimaye.."/>
</div>
<div class="lyrico-lyrics-wrapper">En Iniya Thanimaye..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Iniya Thanimaye.."/>
</div>
<div class="lyrico-lyrics-wrapper">En Iniya Thanimaye..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Iniya Thanimaye.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alai Modhum Karai Meedhilo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alai Modhum Karai Meedhilo"/>
</div>
<div class="lyrico-lyrics-wrapper">Manal Paadham Sudum Podhilo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manal Paadham Sudum Podhilo"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnodu Naan Nadanthal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnodu Naan Nadanthal"/>
</div>
<div class="lyrico-lyrics-wrapper">Manne Poochiragu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manne Poochiragu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karaigindra Adi Vaanamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karaigindra Adi Vaanamo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuraiyadha Perundhooramo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuraiyadha Perundhooramo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennodu Nee Nadanthal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennodu Nee Nadanthal"/>
</div>
<div class="lyrico-lyrics-wrapper">Inbam En Ulagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inbam En Ulagu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Thaayin Karuvil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Thaayin Karuvil"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennodu Pirandhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennodu Pirandhai"/>
</div>
<div class="lyrico-lyrics-wrapper">En Vaazhvin Mudivil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Vaazhvin Mudivil"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennodu Irupai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennodu Irupai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uravugal Vandhu Serum Neengum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uravugal Vandhu Serum Neengum"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethaan Nilayai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethaan Nilayai"/>
</div>
<div class="lyrico-lyrics-wrapper">Adharkku Unakkoru Nandri Sonnen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adharkku Unakkoru Nandri Sonnen"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudhal Muraiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhal Muraiyai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Iniya Thanimaye..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Iniya Thanimaye.."/>
</div>
<div class="lyrico-lyrics-wrapper">En Iniya Thanimaye..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Iniya Thanimaye.."/>
</div>
<div class="lyrico-lyrics-wrapper">En Iniya Thanimaye..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Iniya Thanimaye.."/>
</div>
<div class="lyrico-lyrics-wrapper">En Iniya Thanimaye..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Iniya Thanimaye.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu Varai Karka Kalaigallellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Varai Karka Kalaigallellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnudan Karkum Velaiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnudan Karkum Velaiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">En Uyir Thozhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Uyir Thozhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyenben Neeyenben
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyenben Neeyenben"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu Varai Kaana Kaatchigalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Varai Kaana Kaatchigalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnudan Kaanum Velaiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnudan Kaanum Velaiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhan Kadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan Kadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyenben Neeyenben
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyenben Neeyenben"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Silar Ennai Nerunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Silar Ennai Nerunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennidam Pesa Thodanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennidam Pesa Thodanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Siru Oodal Kondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru Oodal Kondu"/>
</div>
<div class="lyrico-lyrics-wrapper">Neengi Pogindrai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neengi Pogindrai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kavalaigal Ennai Varutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavalaigal Ennai Varutha"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnidam Ennai Thuratha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnidam Ennai Thuratha"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Madiyai Thandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Madiyai Thandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaayai Aagindrai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaayai Aagindrai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Thuyilena Anaithidu Thanimaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Thuyilena Anaithidu Thanimaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kanavilum Thodarnthidu Thanimaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kanavilum Thodarnthidu Thanimaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Kan Vizhikayil Irundhidu Thanimaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan Vizhikayil Irundhidu Thanimaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanimaiye…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanimaiye…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Iniya Thanimaye..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Iniya Thanimaye.."/>
</div>
<div class="lyrico-lyrics-wrapper">En Iniya Thanimaye..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Iniya Thanimaye.."/>
</div>
<div class="lyrico-lyrics-wrapper">En Iniya Thanimaye..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Iniya Thanimaye.."/>
</div>
<div class="lyrico-lyrics-wrapper">En Iniya Thanimaye..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Iniya Thanimaye.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Iniya Thanimaye..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Iniya Thanimaye.."/>
</div>
<div class="lyrico-lyrics-wrapper">En Iniya Thanimaye..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Iniya Thanimaye.."/>
</div>
<div class="lyrico-lyrics-wrapper">En Iniya Thanimaye..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Iniya Thanimaye.."/>
</div>
</pre>
