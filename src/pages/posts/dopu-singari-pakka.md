---
title: "dopu singari song lyrics"
album: "Pakka"
artist: "C. Sathya"
lyricist: "Yugabharathi"
director: "S.S. Surya"
path: "/albums/pakka-song-lyrics"
song: "Dopu Singari"
image: ../../images/albumart/pakka.jpg
date: 2018-04-27
lang: tamil
youtubeLink: "https://www.youtube.com/embed/qsQ_r5iqD8Q"
type: "happy"
singers:
  - Mahalingam
  - Lakshmi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Dopu singariyay aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dopu singariyay aaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dumukku oyyaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dumukku oyyaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Aapu vachalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aapu vachalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakkaa pakkaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkaa pakkaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pakkaa pakkaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkaa pakkaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakkaa pakkaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkaa pakkaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey unna thottu enna thottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey unna thottu enna thottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaren thullikittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaren thullikittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Munna vittu pinna vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munna vittu pinna vittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thothaaga ninnukittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thothaaga ninnukittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Padikka poren mettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padikka poren mettu"/>
</div>
<div class="lyrico-lyrics-wrapper">Jora irukkum kaiya thattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jora irukkum kaiya thattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey alla vittu killa vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey alla vittu killa vittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasa solla vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasa solla vittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Konja vittu kenja vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konja vittu kenja vittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesaama minja vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesaama minja vittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adippen aala vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adippen aala vittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Modha bayantha yerakattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Modha bayantha yerakattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey nikkatha thaalakattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey nikkatha thaalakattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhill irundha melam kottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhill irundha melam kottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Neruppa nada pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neruppa nada pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan othayila vithayila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan othayila vithayila"/>
</div>
<div class="lyrico-lyrics-wrapper">Bambarama sutthayila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bambarama sutthayila"/>
</div>
<div class="lyrico-lyrics-wrapper">Yethukku athir vettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethukku athir vettu"/>
</div>
<div class="lyrico-lyrics-wrapper">Podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dopu singari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dopu singari"/>
</div>
<div class="lyrico-lyrics-wrapper">Dumukku oyyaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dumukku oyyaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Aapu vachalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aapu vachalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakkaa pakkaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkaa pakkaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Top-u pallaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Top-u pallaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Damakku dammaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Damakku dammaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Paappu vanthalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paappu vanthalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kokkaa kokkaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kokkaa kokkaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enga ooru che guevara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga ooru che guevara"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey yelunthakka thanguveera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey yelunthakka thanguveera"/>
</div>
<div class="lyrico-lyrics-wrapper">Beer-ah nee koppalicha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Beer-ah nee koppalicha"/>
</div>
<div class="lyrico-lyrics-wrapper">Setthulathaan mukkulicha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Setthulathaan mukkulicha"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaraara run adicha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraara run adicha"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhaga dhaan kannadicha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhaga dhaan kannadicha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vinaada manaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinaada manaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Vellaavi ponnu oda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vellaavi ponnu oda"/>
</div>
<div class="lyrico-lyrics-wrapper">Oyaama seranumae kondadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oyaama seranumae kondadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal seiyaama 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal seiyaama "/>
</div>
<div class="lyrico-lyrics-wrapper">aagadha nee pannada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aagadha nee pannada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanaada kaiaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanaada kaiaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaadha thindaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaadha thindaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Kachery vekkanumae kick ooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kachery vekkanumae kick ooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaru kandalum podadha nee mukkada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaru kandalum podadha nee mukkada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mallandha velayilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mallandha velayilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Malligai poo mela vizha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malligai poo mela vizha"/>
</div>
<div class="lyrico-lyrics-wrapper">Summa neeyum vittu vidadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summa neeyum vittu vidadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Sela kattadha sedhigala 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sela kattadha sedhigala "/>
</div>
<div class="lyrico-lyrics-wrapper">sellakili pesayilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sellakili pesayilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmm kotta venum thannaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm kotta venum thannaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Velai ennanu nee kaattu pinaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velai ennanu nee kaattu pinaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oththaikku othaiNikkira onna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oththaikku othaiNikkira onna"/>
</div>
<div class="lyrico-lyrics-wrapper">Jeikka yevan irukkan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jeikka yevan irukkan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee muthukku mutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee muthukku mutha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhu sirikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhu sirikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Yemanum kavunthirupaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yemanum kavunthirupaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ammaayi appaayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammaayi appaayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinnaayi chippaayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinnaayi chippaayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhaama nee irundha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhaama nee irundha"/>
</div>
<div class="lyrico-lyrics-wrapper">Loolaayi vaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Loolaayi vaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnae nee pogadha da veenaayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnae nee pogadha da veenaayi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuppaayi suppaayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuppaayi suppaayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Povaadha thappaayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Povaadha thappaayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthomae neeyum naanum aalaayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthomae neeyum naanum aalaayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhkai yengenum novaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkai yengenum novaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee aaraayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee aaraayi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ettu ooru seemayilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettu ooru seemayilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pottu vacha poomayilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pottu vacha poomayilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sokka vaikka yennu vadhalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sokka vaikka yennu vadhalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aasai pallaakku aadidumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai pallaakku aadidumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Athanaiumae koodidumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athanaiumae koodidumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sari"/>
</div>
<div class="lyrico-lyrics-wrapper">Jallikattu kaala munnaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jallikattu kaala munnaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Athuvara neeyum mallukatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athuvara neeyum mallukatta"/>
</div>
<div class="lyrico-lyrics-wrapper">Venum manmelae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venum manmelae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aama raasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aama raasa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey unna thottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey unna thottu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey enna thottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey enna thottu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey unna thottu enna thottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey unna thottu enna thottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaren thullikittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaren thullikittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ha haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ha haa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Munna vittu pinna vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munna vittu pinna vittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thothaaga ninnukittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thothaaga ninnukittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Padikka poren mettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padikka poren mettu"/>
</div>
<div class="lyrico-lyrics-wrapper">Jora irukkum kaiya thattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jora irukkum kaiya thattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dopu singari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dopu singari"/>
</div>
<div class="lyrico-lyrics-wrapper">Dumukku oyyaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dumukku oyyaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Aapu vachalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aapu vachalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakkaa pakkaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkaa pakkaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Top-u pallaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Top-u pallaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Damakku dammaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Damakku dammaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Paappu vanthalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paappu vanthalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kokkaa kokkaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kokkaa kokkaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Doppu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Doppu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yay dumukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yay dumukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Aappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakkaa pakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkaa pakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Aappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey damakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey damakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey paappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey paappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakkaa pakkaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkaa pakkaa"/>
</div>
</pre>
