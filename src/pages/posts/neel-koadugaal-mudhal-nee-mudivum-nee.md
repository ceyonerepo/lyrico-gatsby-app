---
title: "neel koadugaal song lyrics"
album: "Mudhal Nee Mudivum Nee"
artist: "Darbuka Siva"
lyricist: "Keerthi"
director: "Darbuka Siva"
path: "/albums/mudhal-nee-mudivum-nee-lyrics"
song: "Neel Koadugaal"
image: ../../images/albumart/mudhal-nee-mudivum-nee.jpg
date: 2022-01-21
lang: tamil
youtubeLink: "https://www.youtube.com/embed/7da3QVJ_hGs"
type: "happy"
singers:
  - Bombay Jayashri
  - Dima El Sayed
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Neel kodugaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neel kodugaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Neel paigiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neel paigiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan kaangaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan kaangaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen theigiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen theigiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mugam illaamal mudhal illaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mugam illaamal mudhal illaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Muran illaamal mudivu illaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muran illaamal mudivu illaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Alaigindren tholaigindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alaigindren tholaigindren"/>
</div>
<div class="lyrico-lyrics-wrapper">Izhugindren vazhigindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Izhugindren vazhigindren"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodugaal kodugaal kodi neero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodugaal kodugaal kodi neero"/>
</div>
<div class="lyrico-lyrics-wrapper">Kizhaiyaagiyae neengiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kizhaiyaagiyae neengiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Selveno vizhagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Selveno vizhagi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neel kodugaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neel kodugaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Neel paigiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neel paigiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan kaangaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan kaangaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen theigiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen theigiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hmm mm mm mm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm mm mm mm"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmm mm mm mm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm mm mm mm"/>
</div>
</pre>
