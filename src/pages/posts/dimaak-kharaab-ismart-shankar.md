---
title: "dimaak kharaab song lyrics"
album: "Ismart Shankar"
artist: "Mani Sharma"
lyricist: "Kasarla Shyam"
director: "Puri Jagannadh"
path: "/albums/ismart-shankar-lyrics"
song: "Dimaak Kharaab"
image: ../../images/albumart/ismart-shankar.jpg
date: 2019-07-18
lang: telugu
youtubeLink: "https://www.youtube.com/embed/V5c4BnmPTpU"
type: "happy"
singers:
  - Keerthana Sharma
  - Saketh Komanduri
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vani yeda meeda undeti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vani yeda meeda undeti"/>
</div>
<div class="lyrico-lyrics-wrapper">Ghama ghaama ghandhaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ghama ghaama ghandhaalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandha maamayyalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandha maamayyalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa raika mudi meedha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa raika mudi meedha"/>
</div>
<div class="lyrico-lyrics-wrapper">Raalina saalyya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raalina saalyya"/>
</div>
<div class="lyrico-lyrics-wrapper">Ranga raamayyalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ranga raamayyalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaani nadumuki undeti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaani nadumuki undeti"/>
</div>
<div class="lyrico-lyrics-wrapper">Billala molathaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Billala molathaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandha maamayyalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandha maamayyalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa pattu kuchula koka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa pattu kuchula koka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kantina salayyo rangaramayyalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kantina salayyo rangaramayyalo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hali hali hali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hali hali hali"/>
</div>
<div class="lyrico-lyrics-wrapper">This is DJ
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="This is DJ"/>
</div>
<div class="lyrico-lyrics-wrapper">Feek feek feek feek
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Feek feek feek feek"/>
</div>
<div class="lyrico-lyrics-wrapper">iSmart shankar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iSmart shankar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Silaka silaka silaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silaka silaka silaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhi sitharaangi silaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhi sitharaangi silaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Pilaga pilaga pilaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pilaga pilaga pilaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Petti pora suraka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Petti pora suraka"/>
</div>
<div class="lyrico-lyrics-wrapper">Giraka giraka giraka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Giraka giraka giraka"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhi sedha baayi giraka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhi sedha baayi giraka"/>
</div>
<div class="lyrico-lyrics-wrapper">Uraka uraka uraka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uraka uraka uraka"/>
</div>
<div class="lyrico-lyrics-wrapper">Sotta bugge koraka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sotta bugge koraka"/>
</div>
<div class="lyrico-lyrics-wrapper">Jilelamma jitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jilelamma jitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Pilla paala pitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pilla paala pitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Jilelamma jitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jilelamma jitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninne thelai kutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninne thelai kutta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unte dhammunte nee poola pakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unte dhammunte nee poola pakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Estha challesey nee semata sukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Estha challesey nee semata sukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Vastha thaagestha nee soke gataka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vastha thaagestha nee soke gataka"/>
</div>
<div class="lyrico-lyrics-wrapper">Estha yeistha nee thote gutaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Estha yeistha nee thote gutaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Jilelamma jitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jilelamma jitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Pilla paala pitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pilla paala pitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Jilelamma jitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jilelamma jitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninne thelai kutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninne thelai kutta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">See see see see
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="See see see see"/>
</div>
<div class="lyrico-lyrics-wrapper">Silaka silaka silaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silaka silaka silaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhi sitharaangi silaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhi sitharaangi silaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Pilaga pilaga pilaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pilaga pilaga pilaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Petti pora suraka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Petti pora suraka"/>
</div>
<div class="lyrico-lyrics-wrapper">Giraka giraka giraka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Giraka giraka giraka"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhi sedha baayi giraka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhi sedha baayi giraka"/>
</div>
<div class="lyrico-lyrics-wrapper">Uraka uraka uraka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uraka uraka uraka"/>
</div>
<div class="lyrico-lyrics-wrapper">Sotta bugge koraka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sotta bugge koraka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pattukoni jokisthe chetake nadumu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattukoni jokisthe chetake nadumu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhimak kharaabe dhimak kharaab
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhimak kharaabe dhimak kharaab"/>
</div>
<div class="lyrico-lyrics-wrapper">Thattukoni oopestha pataak ay tadumu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thattukoni oopestha pataak ay tadumu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhim dhim dhim dhim dhimak kharaab
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhim dhim dhim dhim dhimak kharaab"/>
</div>
<div class="lyrico-lyrics-wrapper">Satthuvani choopisthe pilloda dinamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Satthuvani choopisthe pilloda dinamu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhathathane isthara mallepoola vanamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhathathane isthara mallepoola vanamu"/>
</div>
<div class="lyrico-lyrics-wrapper">iSmart ye ee shankaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iSmart ye ee shankaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Pelchesthaa nee bankaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pelchesthaa nee bankaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Vasthaavaa naa centeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vasthaavaa naa centeru"/>
</div>
<div class="lyrico-lyrics-wrapper">Choosthaara nee temperu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choosthaara nee temperu"/>
</div>
<div class="lyrico-lyrics-wrapper">Asaluku ney kiraake nanne gellakke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asaluku ney kiraake nanne gellakke"/>
</div>
<div class="lyrico-lyrics-wrapper">Masuluthundi dhimaake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Masuluthundi dhimaake"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvve FASAK ye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvve FASAK ye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jilelamma jitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jilelamma jitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Pilla paala pitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pilla paala pitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Jilelamma jitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jilelamma jitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninne thelai kutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninne thelai kutta"/>
</div>
<div class="lyrico-lyrics-wrapper">Silaka silaka silaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silaka silaka silaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhi sitharaangi silaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhi sitharaangi silaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Pilaga pilaga pilaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pilaga pilaga pilaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Petti pora suraka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Petti pora suraka"/>
</div>
<div class="lyrico-lyrics-wrapper">Giraka giraka giraka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Giraka giraka giraka"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhi sedha baayi giraka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhi sedha baayi giraka"/>
</div>
<div class="lyrico-lyrics-wrapper">Uraka uraka uraka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uraka uraka uraka"/>
</div>
<div class="lyrico-lyrics-wrapper">Sotta bugge koraka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sotta bugge koraka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ompulalo dhaachuncha chekumuki raallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ompulalo dhaachuncha chekumuki raallu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhimaak kharaab ye dhimaak kharaab
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhimaak kharaab ye dhimaak kharaab"/>
</div>
<div class="lyrico-lyrics-wrapper">Nippulane puttinchu thakinche vellu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nippulane puttinchu thakinche vellu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhim dhim dhim dhim dhimaak kharaab
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhim dhim dhim dhim dhimaak kharaab"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennilane mingesi emaagundi vollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennilane mingesi emaagundi vollu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thinnelapai manchesa chalo jonna selu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thinnelapai manchesa chalo jonna selu"/>
</div>
<div class="lyrico-lyrics-wrapper">iSmart ye ee shankaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iSmart ye ee shankaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Samaramlo yama kinkaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samaramlo yama kinkaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Atlaithe nuvvu superu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atlaithe nuvvu superu"/>
</div>
<div class="lyrico-lyrics-wrapper">Swargaale mana properu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Swargaale mana properu"/>
</div>
<div class="lyrico-lyrics-wrapper">Muddulake giraake poddu poyake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muddulake giraake poddu poyake"/>
</div>
<div class="lyrico-lyrics-wrapper">Voddu ante siraake thella vaarake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Voddu ante siraake thella vaarake"/>
</div>
</pre>
