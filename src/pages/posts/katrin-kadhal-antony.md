---
title: "katrin kadhal song lyrics"
album: "Antony"
artist: "Sivatmikha"
lyricist: "Mohankumar Govindaraj"
director: "Kutti Kumar"
path: "/albums/antony-lyrics"
song: "Katrin Kadhal"
image: ../../images/albumart/antony.jpg
date: 2018-06-01
lang: tamil
youtubeLink: "https://www.youtube.com/embed/A5_U_1aObXE"
type: "love"
singers:
  - Yazin Nizar
  - Sinduri Vishal
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">kaatrin kadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatrin kadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">kathoram konjum bothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathoram konjum bothu"/>
</div>
<div class="lyrico-lyrics-wrapper">ulagam pesum un 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulagam pesum un "/>
</div>
<div class="lyrico-lyrics-wrapper">chella mozhiyaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chella mozhiyaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaatrin kadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatrin kadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">kathoram konjum bothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathoram konjum bothu"/>
</div>
<div class="lyrico-lyrics-wrapper">ulagam pesum un 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulagam pesum un "/>
</div>
<div class="lyrico-lyrics-wrapper">chella mozhiyaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chella mozhiyaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">dhoorangal dhooram illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhoorangal dhooram illai"/>
</div>
<div class="lyrico-lyrics-wrapper">athu enthan kathal thollai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu enthan kathal thollai"/>
</div>
<div class="lyrico-lyrics-wrapper">dhoorangal dhooram illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhoorangal dhooram illai"/>
</div>
<div class="lyrico-lyrics-wrapper">athu enthan kathal thollai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu enthan kathal thollai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nanum vazha neeye vazhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanum vazha neeye vazhi"/>
</div>
<div class="lyrico-lyrics-wrapper">nan unai sera thavikiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan unai sera thavikiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kannale nan unai vendrene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannale nan unai vendrene"/>
</div>
<div class="lyrico-lyrics-wrapper">kan mun nindru kannale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kan mun nindru kannale"/>
</div>
<div class="lyrico-lyrics-wrapper">nan unil nuzhaindhene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan unil nuzhaindhene"/>
</div>
<div class="lyrico-lyrics-wrapper">moongil nan isai pole
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moongil nan isai pole"/>
</div>
<div class="lyrico-lyrics-wrapper">en kadhal un mele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kadhal un mele"/>
</div>
<div class="lyrico-lyrics-wrapper">karai modhum alai pole
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karai modhum alai pole"/>
</div>
<div class="lyrico-lyrics-wrapper">dhinam serven uyir mele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhinam serven uyir mele"/>
</div>
<div class="lyrico-lyrics-wrapper">karai modhum alai pole
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karai modhum alai pole"/>
</div>
<div class="lyrico-lyrics-wrapper">dhinam serven uyir mele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhinam serven uyir mele"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaatrin kadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatrin kadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">kathoram konjum bothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathoram konjum bothu"/>
</div>
<div class="lyrico-lyrics-wrapper">ulagam pesum un 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulagam pesum un "/>
</div>
<div class="lyrico-lyrics-wrapper">chella mozhiyaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chella mozhiyaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaatrin kadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatrin kadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">kathoram konjum bothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathoram konjum bothu"/>
</div>
<div class="lyrico-lyrics-wrapper">ulagam pesum un 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulagam pesum un "/>
</div>
<div class="lyrico-lyrics-wrapper">chella mozhiyaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chella mozhiyaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">dhoorangal dhooram illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhoorangal dhooram illai"/>
</div>
<div class="lyrico-lyrics-wrapper">athu enthan kathal thollai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu enthan kathal thollai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nanum vazha neeye vazhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanum vazha neeye vazhi"/>
</div>
<div class="lyrico-lyrics-wrapper">nan unai sera thavikiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan unai sera thavikiren"/>
</div>
</pre>
