---
title: "sandhikkatha kangalil song lyrics"
album: "180"
artist: "Sharreth"
lyricist: "Madhan Karky"
director: "Jayendra Panchapakesan"
path: "/albums/180-lyrics"
song: "Sandhikkatha Kangalil"
image: ../../images/albumart/180.jpg
date: 2011-06-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/zUnKswDpJkc"
type: "love"
singers:
  - K.S. Chithra
  - S. Sowmya
  - Unni Menon
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Santhikkadha Kangalil Inbangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Santhikkadha Kangalil Inbangal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Seiyapogiraen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seiyapogiraen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sinthikkaathu Sinthidum Kondalaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sinthikkaathu Sinthidum Kondalaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Peiyappogiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peiyappogiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbin Aalai Aanaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbin Aalai Aanaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yengum Yezhai Naanaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yengum Yezhai Naanaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanneerai Thaedum Meenaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanneerai Thaedum Meenaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Santhikkaaatha Kangalil Inbangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Santhikkaaatha Kangalil Inbangal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Seiyapogiraen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seiyapogiraen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oogam Seithaenillai Moagam Un Meethaanaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oogam Seithaenillai Moagam Un Meethaanaen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhaigal Kadhaigal Kathaithuvittu Pogaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhaigal Kadhaigal Kathaithuvittu Pogaamal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vithaigal Vithaigal Vithaithuvittu Povomae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vithaigal Vithaigal Vithaithuvittu Povomae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thisaiariyaa Paravaigalaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thisaiariyaa Paravaigalaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Naan Neel Vaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Naan Neel Vaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veliyiley Mithakkirom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veliyiley Mithakkirom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Santhikkaatha Kangalil Inbangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Santhikkaatha Kangalil Inbangal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Seiyapogiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seiyapogiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sinthikkaathu Sinthidum Kondalaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sinthikkaathu Sinthidum Kondalaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Peiyappogiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peiyappogiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pogum Nam Dhoorangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogum Nam Dhoorangal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neelam Thaan Koodaatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelam Thaan Koodaatho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inaiyum Munaiyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inaiyum Munaiyam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhayam Endru Aanaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam Endru Aanaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Payanam Mudiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Payanam Mudiyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Payamum Vittu Pogaatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Payamum Vittu Pogaatho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mudivariyaa Adivaananmaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudivariyaa Adivaananmaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aen Aen Nee Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aen Aen Nee Naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thinam Thinam Thodarkirom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thinam Thinam Thodarkirom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Santhikkaatha kangalil inbangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Santhikkaatha kangalil inbangal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">seiyapogiraen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seiyapogiraen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sinthikkaathu sinthidum kondalaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sinthikkaathu sinthidum kondalaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">peiyappogiraen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="peiyappogiraen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbin Aalaai Aanaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbin Aalaai Aanaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yengum YelaiNaanaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yengum YelaiNaanaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanneerai Thedum Meenaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanneerai Thedum Meenaai"/>
</div>
</pre>
