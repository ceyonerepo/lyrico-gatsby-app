---
title: "nan thangurathu 5star song lyrics"
album: "Chasing"
artist: "Thashi"
lyricist: "Viveka"
director: "Veerakumar"
path: "/albums/chasing-song-lyrics"
song: "Nan Thangurathu 5star"
image: ../../images/albumart/chasing.jpg
date: 2021-04-16
lang: tamil
youtubeLink: "https://www.youtube.com/embed/A7LLBpaoaHs"
type: "Item Song"
singers:
  - Vinaitha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">nan thanguradhu 5 star
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan thanguradhu 5 star"/>
</div>
<div class="lyrico-lyrics-wrapper">nan thinguradhu palam soru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan thinguradhu palam soru"/>
</div>
<div class="lyrico-lyrics-wrapper">nan thanguradhu 5 star
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan thanguradhu 5 star"/>
</div>
<div class="lyrico-lyrics-wrapper">nan thinguradhu palam soru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan thinguradhu palam soru"/>
</div>
<div class="lyrico-lyrics-wrapper">kusvathsing joke pidikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kusvathsing joke pidikum"/>
</div>
<div class="lyrico-lyrics-wrapper">kovil kulam poga pidikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kovil kulam poga pidikum"/>
</div>
<div class="lyrico-lyrics-wrapper">ecr root pidikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ecr root pidikum"/>
</div>
<div class="lyrico-lyrics-wrapper">pagavathar paatu pidikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pagavathar paatu pidikum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nan thanguradhu 5 star
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan thanguradhu 5 star"/>
</div>
<div class="lyrico-lyrics-wrapper">nan thinguradhu palam soru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan thinguradhu palam soru"/>
</div>
<div class="lyrico-lyrics-wrapper">nan thanguradhu 5 star
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan thanguradhu 5 star"/>
</div>
<div class="lyrico-lyrics-wrapper">nan thinguradhu palam soru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan thinguradhu palam soru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nan kaiya vacha sappatil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan kaiya vacha sappatil"/>
</div>
<div class="lyrico-lyrics-wrapper">kaaram sekka venam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaaram sekka venam"/>
</div>
<div class="lyrico-lyrics-wrapper">en kanna paatha yaarukum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kanna paatha yaarukum"/>
</div>
<div class="lyrico-lyrics-wrapper">minnal paaka venam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="minnal paaka venam"/>
</div>
<div class="lyrico-lyrics-wrapper">nan ulla vantha roomuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan ulla vantha roomuku"/>
</div>
<div class="lyrico-lyrics-wrapper">ac poda venam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ac poda venam"/>
</div>
<div class="lyrico-lyrics-wrapper">nan mella thotta kaichaluku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan mella thotta kaichaluku"/>
</div>
<div class="lyrico-lyrics-wrapper">oosi poda venam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oosi poda venam"/>
</div>
<div class="lyrico-lyrics-wrapper">nan ooram nikkum theruvuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan ooram nikkum theruvuku"/>
</div>
<div class="lyrico-lyrics-wrapper">vega thada venam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vega thada venam"/>
</div>
<div class="lyrico-lyrics-wrapper">nan ukkantha scooteril 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan ukkantha scooteril "/>
</div>
<div class="lyrico-lyrics-wrapper">petrole venam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="petrole venam"/>
</div>
<div class="lyrico-lyrics-wrapper">mixi ooda nan thotta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mixi ooda nan thotta"/>
</div>
<div class="lyrico-lyrics-wrapper">minsaaram venam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="minsaaram venam"/>
</div>
<div class="lyrico-lyrics-wrapper">nan look vitta coffee ku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan look vitta coffee ku"/>
</div>
<div class="lyrico-lyrics-wrapper">sakkaraye venam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sakkaraye venam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nan thanguradhu 5 star
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan thanguradhu 5 star"/>
</div>
<div class="lyrico-lyrics-wrapper">nan thinguradhu palam soru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan thinguradhu palam soru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nan eerinallee car ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan eerinallee car ellam"/>
</div>
5<div class="lyrico-lyrics-wrapper">th gearil oodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="th gearil oodum"/>
</div>
<div class="lyrico-lyrics-wrapper">nan pathaale traffic
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan pathaale traffic"/>
</div>
<div class="lyrico-lyrics-wrapper">patcha signal podum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="patcha signal podum"/>
</div>
<div class="lyrico-lyrics-wrapper">nan thummuratha ketale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan thummuratha ketale"/>
</div>
<div class="lyrico-lyrics-wrapper">nathaswaram mayangum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nathaswaram mayangum"/>
</div>
<div class="lyrico-lyrics-wrapper">nan thoora ninnu pathaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan thoora ninnu pathaale"/>
</div>
<div class="lyrico-lyrics-wrapper">gangai nathi pongum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gangai nathi pongum"/>
</div>
<div class="lyrico-lyrics-wrapper">nan malligai poo vachalle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan malligai poo vachalle"/>
</div>
<div class="lyrico-lyrics-wrapper">aangaluku danger
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aangaluku danger"/>
</div>
<div class="lyrico-lyrics-wrapper">en kaal nadantha land ku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kaal nadantha land ku"/>
</div>
<div class="lyrico-lyrics-wrapper">ellam value romba super
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellam value romba super"/>
</div>
<div class="lyrico-lyrics-wrapper">nan malaiyila nenanchale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan malaiyila nenanchale"/>
</div>
<div class="lyrico-lyrics-wrapper">megathuku fever
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="megathuku fever"/>
</div>
<div class="lyrico-lyrics-wrapper">en marmamana punnagaiku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en marmamana punnagaiku"/>
</div>
<div class="lyrico-lyrics-wrapper">unga kadha over
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unga kadha over"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nan thanguradhu 5 star
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan thanguradhu 5 star"/>
</div>
<div class="lyrico-lyrics-wrapper">nan thinguradhu palam soru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan thinguradhu palam soru"/>
</div>
<div class="lyrico-lyrics-wrapper">nan thanguradhu 5 star
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan thanguradhu 5 star"/>
</div>
<div class="lyrico-lyrics-wrapper">nan thinguradhu palam soru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan thinguradhu palam soru"/>
</div>
<div class="lyrico-lyrics-wrapper">kusvathsing joke pidikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kusvathsing joke pidikum"/>
</div>
<div class="lyrico-lyrics-wrapper">kovil kulam poga pidikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kovil kulam poga pidikum"/>
</div>
<div class="lyrico-lyrics-wrapper">ecr root pidikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ecr root pidikum"/>
</div>
<div class="lyrico-lyrics-wrapper">pagavathar paatu pidikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pagavathar paatu pidikum"/>
</div>
<div class="lyrico-lyrics-wrapper">nan thanguradhu 5 star
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan thanguradhu 5 star"/>
</div>
<div class="lyrico-lyrics-wrapper">nan thinguradhu palam soru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan thinguradhu palam soru"/>
</div>
</pre>
