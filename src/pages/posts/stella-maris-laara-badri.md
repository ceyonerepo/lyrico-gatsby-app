---
title: "stella maris laara song lyrics"
album: "Badri"
artist: "Ramana Gogula"
lyricist: "Palani Bharathi"
director: "P. A. Arun Prasad"
path: "/albums/badri-lyrics"
song: "Stella Maris Laara"
image: ../../images/albumart/badri.jpg
date: 2001-04-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/0Td-ZbP46cA"
type: "happy"
singers:
  - Tippu
  - Vivek
  - Dhamu
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">namma thalaivar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="namma thalaivar"/>
</div>
<div class="lyrico-lyrics-wrapper">modern college 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="modern college "/>
</div>
<div class="lyrico-lyrics-wrapper">figure ah madakitanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="figure ah madakitanda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">stella maris lara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="stella maris lara"/>
</div>
<div class="lyrico-lyrics-wrapper">chellamma college meera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chellamma college meera"/>
</div>
<div class="lyrico-lyrics-wrapper">ethiraj college sheela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethiraj college sheela"/>
</div>
<div class="lyrico-lyrics-wrapper">meenatchi college mala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meenatchi college mala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">atatadda ultra modern ponnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="atatadda ultra modern ponnu"/>
</div>
<div class="lyrico-lyrics-wrapper">annan pocket kulla vandhachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="annan pocket kulla vandhachu"/>
</div>
<div class="lyrico-lyrics-wrapper">kai tattuda nanpa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kai tattuda nanpa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">annan poghum valli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="annan poghum valli"/>
</div>
<div class="lyrico-lyrics-wrapper">follow pannungada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="follow pannungada"/>
</div>
<div class="lyrico-lyrics-wrapper">ellorum figure bidipom jora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellorum figure bidipom jora"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sipisi bike la figure nga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sipisi bike la figure nga"/>
</div>
<div class="lyrico-lyrics-wrapper">back la kadarkarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="back la kadarkarai"/>
</div>
<div class="lyrico-lyrics-wrapper">salaiyile parapom parapom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="salaiyile parapom parapom"/>
</div>
<div class="lyrico-lyrics-wrapper">five star hotel ice cream
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="five star hotel ice cream"/>
</div>
<div class="lyrico-lyrics-wrapper">parlour i kalathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="parlour i kalathai"/>
</div>
<div class="lyrico-lyrics-wrapper">naam kalipom cool ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naam kalipom cool ah"/>
</div>
<div class="lyrico-lyrics-wrapper">teen age vatakkil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="teen age vatakkil "/>
</div>
<div class="lyrico-lyrics-wrapper">full stop namakilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="full stop namakilla"/>
</div>
<div class="lyrico-lyrics-wrapper">eppodhum sight adipom tula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eppodhum sight adipom tula"/>
</div>
</pre>
