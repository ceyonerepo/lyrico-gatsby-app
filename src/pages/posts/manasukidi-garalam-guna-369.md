---
title: "manasukidi garalam song lyrics"
album: "Guna 369"
artist: "Chaitan Bharadwaj"
lyricist: "Ramajogayya Sastry"
director: "Arjun Jandyala"
path: "/albums/guna-369-lyrics"
song: "Manasukidi Garalam"
image: ../../images/albumart/guna-369.jpg
date: 2019-08-02
lang: telugu
youtubeLink: "https://www.youtube.com/embed/q81_bEBvt_4"
type: "love"
singers:
  - Vijay Yesudas
  - Shweta Mohan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Manasukidi Garalam Garalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasukidi Garalam Garalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sekanukoka Maranam Maranam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sekanukoka Maranam Maranam"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhuta Ninu Choodaleni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhuta Ninu Choodaleni"/>
</div>
<div class="lyrico-lyrics-wrapper">Praanamantha Kalavaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Praanamantha Kalavaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalalakidhi Vilayam Vilayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalalakidhi Vilayam Vilayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Naliginadhi Hrudhayam Hrudhayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naliginadhi Hrudhayam Hrudhayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Bathuku Baruvaina Chedhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bathuku Baruvaina Chedhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vedhanantha Cherisagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedhanantha Cherisagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Digulu Peduthondhi Alajadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Digulu Peduthondhi Alajadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaganamoka Pidugai Paibadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaganamoka Pidugai Paibadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhupu Chedhirindhi Yedha Sadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhupu Chedhirindhi Yedha Sadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pralayamai Vulikipadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pralayamai Vulikipadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvantu Leni Kanneeti Basha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvantu Leni Kanneeti Basha"/>
</div>
<div class="lyrico-lyrics-wrapper">Minnandhaleni Sandhrala Ghosha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minnandhaleni Sandhrala Ghosha"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhisha Teliyadhe Nishi Cheragadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhisha Teliyadhe Nishi Cheragadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninu Kalavatam Ela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninu Kalavatam Ela"/>
</div>
<div class="lyrico-lyrics-wrapper">Nirasa Lonu Needhele Dhyasaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nirasa Lonu Needhele Dhyasaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nitturpulonu Nuvvele Shwasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nitturpulonu Nuvvele Shwasa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yetu Vidudhala Teliyani Valaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yetu Vidudhala Teliyani Valaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalu Cherugula Yentilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalu Cherugula Yentilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nidhara Raani Kannuga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nidhara Raani Kannuga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ragilindhi Baadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ragilindhi Baadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninna Monnalaa Neetho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninna Monnalaa Neetho"/>
</div>
<div class="lyrico-lyrics-wrapper">Ne Lenu Kadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne Lenu Kadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Janma Jathagaa Vidiponi Manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Janma Jathagaa Vidiponi Manam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalathalonu Kalisi Unnam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalathalonu Kalisi Unnam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidhiki Baliga Origindhi Paapam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidhiki Baliga Origindhi Paapam"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana Prema Paavuram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana Prema Paavuram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Madhatiputa Lne Thadabadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhatiputa Lne Thadabadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Valapu Katha Vilapisthunnadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valapu Katha Vilapisthunnadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thudhaku Yetuvaipo Theliyani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thudhaku Yetuvaipo Theliyani"/>
</div>
<div class="lyrico-lyrics-wrapper">Payanamai…Kadhilinadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Payanamai…Kadhilinadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvantu Leni Kanneeti Baasha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvantu Leni Kanneeti Baasha"/>
</div>
<div class="lyrico-lyrics-wrapper">Minnandhaleni Sandhrala Ghosha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minnandhaleni Sandhrala Ghosha"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhisha Theliyadhe Nishi Cheragadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhisha Theliyadhe Nishi Cheragadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninu Kalavatam Yelaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninu Kalavatam Yelaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nirasa Lonu Needhele Dhyasaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nirasa Lonu Needhele Dhyasaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nitturpulonu Nuvvele Shwasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nitturpulonu Nuvvele Shwasa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yetu Vidudhala Teliyani Valaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yetu Vidudhala Teliyani Valaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalu Cherugula Yentilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalu Cherugula Yentilaa"/>
</div>
</pre>
