---
title: "enda poovum song lyrics"
album: "Unarvu"
artist: "Nakul Abhyankar"
lyricist: "Bala"
director: "Subu Venkat"
path: "/albums/unarvu-lyrics"
song: "Enda Poovum"
image: ../../images/albumart/unarvu.jpg
date: 2019-07-19
lang: tamil
youtubeLink: "https://www.youtube.com/embed/YQl29sn7dZw"
type: "love"
singers:
  - Ramya Bhat
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Endha Poovum Solladha Andha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endha Poovum Solladha Andha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaarthai Sonnal Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaarthai Sonnal Enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal Konden Un Mele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Konden Un Mele"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Paarvai Veesum Bodhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Paarvai Veesum Bodhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholil Saayum Bodhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholil Saayum Bodhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatraai Maarippovenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatraai Maarippovenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kulirum Theeyaai Vandhaayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulirum Theeyaai Vandhaayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannanthaniyaai Ennullae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannanthaniyaai Ennullae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vanna Nilavaai Vandhaayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanna Nilavaai Vandhaayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Neethaanae En Kadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Neethaanae En Kadhal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endha Poovum Solladha Andha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endha Poovum Solladha Andha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaarthai Sollven Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaarthai Sollven Enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum Vidumaa En Anbe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum Vidumaa En Anbe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhazhgalin Idaiveli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhazhgalin Idaiveli"/>
</div>
<div class="lyrico-lyrics-wrapper">Muthathaal Niraindhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthathaal Niraindhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalai Mudhal Kaal Varai Ularugiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalai Mudhal Kaal Varai Ularugiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Iruvarin Paadhangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruvarin Paadhangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Alaigalil Nanaindhidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alaigalil Nanaindhidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagiya Tharunathil Kaigal Korkkindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagiya Tharunathil Kaigal Korkkindren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadigaara Neram Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadigaara Neram Kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Vairiyaaga Maara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Vairiyaaga Maara"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilagaamal Nillayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilagaamal Nillayo"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Ennendru En Nenjam Ketka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Ennendru En Nenjam Ketka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal Endru Solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Endru Solla"/>
</div>
<div class="lyrico-lyrics-wrapper">Nam Dhooram Theindhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam Dhooram Theindhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Karayaamal Nilladho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karayaamal Nilladho"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha Ponmaalai Nerangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Ponmaalai Nerangal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vanna Nilavaai Vandhaayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanna Nilavaai Vandhaayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Neethaanae En Kadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Neethaanae En Kadhal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endha Poovum Solladha Andha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endha Poovum Solladha Andha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaarthai Sonnal Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaarthai Sonnal Enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal Konden Un Mele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Konden Un Mele"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Paarvai Veesum Bodhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Paarvai Veesum Bodhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholil Saayum Bodhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholil Saayum Bodhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatraai Maarippovenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatraai Maarippovenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kulirum Theeyaai Vandhaayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulirum Theeyaai Vandhaayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannanthaniyaai Ennullae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannanthaniyaai Ennullae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vanna Nilavaai Vandhaayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanna Nilavaai Vandhaayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Neethaanae En Kadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Neethaanae En Kadhal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endha Poovum Solladha Andha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endha Poovum Solladha Andha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaarthai Sonnal Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaarthai Sonnal Enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal Konden Un Mele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Konden Un Mele"/>
</div>
</pre>
