---
title: "singilu singilu song lyrics"
album: "90 ML"
artist: "Anup Rubens"
lyricist: "Chandrabose"
director: "SekharReddy Yerra"
path: "/albums/90ml-lyrics"
song: "Singilu Singilu"
image: ../../images/albumart/90ml.jpg
date: 2019-12-06
lang: telugu
youtubeLink: "https://www.youtube.com/embed/Ab4nSs3zRLs"
type: "love"
singers:
  - Rahul Sipligunj
  - M M Manasi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yay Listen Song
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yay Listen Song"/>
</div>
<div class="lyrico-lyrics-wrapper">Single Song
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Single Song"/>
</div>
<div class="lyrico-lyrics-wrapper">There Is One Boy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="There Is One Boy"/>
</div>
<div class="lyrico-lyrics-wrapper">One Girl
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="One Girl"/>
</div>
<div class="lyrico-lyrics-wrapper">With One Heart
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="With One Heart"/>
</div>
<div class="lyrico-lyrics-wrapper">One Beat
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="One Beat"/>
</div>
<div class="lyrico-lyrics-wrapper">Chal Eyyara Steppu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chal Eyyara Steppu"/>
</div>
<div class="lyrico-lyrics-wrapper">Let's Go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Let's Go"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Singilu Singilu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Singilu Singilu"/>
</div>
<div class="lyrico-lyrics-wrapper">Singilu Singilu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Singilu Singilu"/>
</div>
<div class="lyrico-lyrics-wrapper">Singaaraanive
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Singaaraanive"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu Singile Singile 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu Singile Singile "/>
</div>
<div class="lyrico-lyrics-wrapper">Singile Singile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Singile Singile"/>
</div>
<div class="lyrico-lyrics-wrapper">Singaaraanive
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Singaaraanive"/>
</div>
<div class="lyrico-lyrics-wrapper">Naatho Mingilu Mingilu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naatho Mingilu Mingilu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naatho Mingilu Mingilu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naatho Mingilu Mingilu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naatho Mingilu Mingilu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naatho Mingilu Mingilu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayye Naa Bangaranive
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayye Naa Bangaranive"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannu Ringulu Ringulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannu Ringulu Ringulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thippe Naa Singaaraanive
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thippe Naa Singaaraanive"/>
</div>
<div class="lyrico-lyrics-wrapper">Singilu Singilu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Singilu Singilu"/>
</div>
<div class="lyrico-lyrics-wrapper">Singilu Singilu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Singilu Singilu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sundarangudive
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sundarangudive"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu Singile Singile 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu Singile Singile "/>
</div>
<div class="lyrico-lyrics-wrapper">Singile Singile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Singile Singile"/>
</div>
<div class="lyrico-lyrics-wrapper">Sundarangudive
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sundarangudive"/>
</div>
<div class="lyrico-lyrics-wrapper">Naatho Mingilu Mingilu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naatho Mingilu Mingilu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naatho Mingilu Mingilu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naatho Mingilu Mingilu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naatho Mingilu Mingilu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naatho Mingilu Mingilu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayye Naa Madhana Kamudive
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayye Naa Madhana Kamudive"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannu Tingilu Tingilu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannu Tingilu Tingilu"/>
</div>
<div class="lyrico-lyrics-wrapper">Tingilu Chese
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tingilu Chese"/>
</div>
<div class="lyrico-lyrics-wrapper">Thingari Raajaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thingari Raajaave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kandalu Chusanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandalu Chusanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Raathi Kandalu Chusanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raathi Kandalu Chusanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Kandala Venaka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Kandala Venaka "/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaagi Vunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaagi Vunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Gundenu Chusanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundenu Chusanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chusi Karigi Poyanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chusi Karigi Poyanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Neelo Neelo Neelo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelo Neelo Neelo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalisipoyanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalisipoyanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey Ompulu Chusanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey Ompulu Chusanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ompu Sompulu Chusanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ompu Sompulu Chusanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Ompula Venuka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Ompula Venuka"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaagi Vunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaagi Vunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Oddhika Chusanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oddhika Chusanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chusi Vongi Poyanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chusi Vongi Poyanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeku Neeku Neeku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeku Neeku Neeku "/>
</div>
<div class="lyrico-lyrics-wrapper">Longipoyanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Longipoyanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Iddaramu Eenadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iddaramu Eenadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Muggurayyamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muggurayyamu"/>
</div>
<div class="lyrico-lyrics-wrapper">Eyy Mugguraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eyy Mugguraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Inkokarevaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inkokarevaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Iddaramu Eenadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iddaramu Eenadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Muggurayyamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muggurayyamu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Moodo Manishi Pere
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Moodo Manishi Pere"/>
</div>
<div class="lyrico-lyrics-wrapper">Prema Ishq Love 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prema Ishq Love "/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal Kadhal Kadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Kadhal Kadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu Single
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu Single"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey Nenu Single
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey Nenu Single"/>
</div>
<div class="lyrico-lyrics-wrapper">Singilu Singilu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Singilu Singilu"/>
</div>
<div class="lyrico-lyrics-wrapper">Singilu Singilu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Singilu Singilu"/>
</div>
<div class="lyrico-lyrics-wrapper">Singaaraanive
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Singaaraanive"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu Singile Singile 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu Singile Singile "/>
</div>
<div class="lyrico-lyrics-wrapper">Singile Singile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Singile Singile"/>
</div>
<div class="lyrico-lyrics-wrapper">Singaaraanive
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Singaaraanive"/>
</div>
<div class="lyrico-lyrics-wrapper">Naatho Mingilu Mingilu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naatho Mingilu Mingilu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayye Naa Bangaranive
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayye Naa Bangaranive"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">All This Time
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="All This Time"/>
</div>
<div class="lyrico-lyrics-wrapper">I Am A Single
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Am A Single"/>
</div>
<div class="lyrico-lyrics-wrapper">All This Time
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="All This Time"/>
</div>
<div class="lyrico-lyrics-wrapper">You Are A Single
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Are A Single"/>
</div>
<div class="lyrico-lyrics-wrapper">It's The Time
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="It's The Time"/>
</div>
<div class="lyrico-lyrics-wrapper">To Mingle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="To Mingle"/>
</div>
<div class="lyrico-lyrics-wrapper">Singilu Why Single
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Singilu Why Single"/>
</div>
<div class="lyrico-lyrics-wrapper">Body Says
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Body Says"/>
</div>
<div class="lyrico-lyrics-wrapper">Why Single
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Why Single"/>
</div>
<div class="lyrico-lyrics-wrapper">Mind Says
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mind Says"/>
</div>
<div class="lyrico-lyrics-wrapper">Why Single
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Why Single"/>
</div>
<div class="lyrico-lyrics-wrapper">Single Plus
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Single Plus"/>
</div>
<div class="lyrico-lyrics-wrapper">Single Double
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Single Double"/>
</div>
<div class="lyrico-lyrics-wrapper">Singilu Why Single
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Singilu Why Single"/>
</div>
<div class="lyrico-lyrics-wrapper">Buggalu Chusanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Buggalu Chusanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pilla Buggalu Chusanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pilla Buggalu Chusanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Buggalu Naalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Buggalu Naalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Reputhunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Reputhunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Aggini Chusanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aggini Chusanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chusi Buggai Poyanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chusi Buggai Poyanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeku Neeku Neeku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeku Neeku Neeku "/>
</div>
<div class="lyrico-lyrics-wrapper">Bookai Poyanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bookai Poyanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Meeesam Chusanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meeesam Chusanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kora Meesam Chusanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kora Meesam Chusanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Meesam Naku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Meesam Naku"/>
</div>
<div class="lyrico-lyrics-wrapper">Chesthunna Mosam Chusanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chesthunna Mosam Chusanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chusi Mojey Paddanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chusi Mojey Paddanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Neetho Neetho Neetho 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neetho Neetho Neetho "/>
</div>
<div class="lyrico-lyrics-wrapper">Raaji Paddanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raaji Paddanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mugguraina Iddarame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mugguraina Iddarame"/>
</div>
<div class="lyrico-lyrics-wrapper">Okkarayyamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okkarayyamu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ententententi Malli Cheppu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ententententi Malli Cheppu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mugguraina Iddarame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mugguraina Iddarame"/>
</div>
<div class="lyrico-lyrics-wrapper">Okkarayyamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okkarayyamu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Okka Life
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Okka Life"/>
</div>
<div class="lyrico-lyrics-wrapper">Ika Wonderful
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ika Wonderful"/>
</div>
<div class="lyrico-lyrics-wrapper">Beautiful Colorful
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Beautiful Colorful"/>
</div>
<div class="lyrico-lyrics-wrapper">Joyful Full Full Full Full
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Joyful Full Full Full Full"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu Single
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu Single"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey Nenu Single
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey Nenu Single"/>
</div>
<div class="lyrico-lyrics-wrapper">Singilu Mingilu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Singilu Mingilu"/>
</div>
<div class="lyrico-lyrics-wrapper">Singilu Mingilu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Singilu Mingilu"/>
</div>
<div class="lyrico-lyrics-wrapper">Singilu Mingilu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Singilu Mingilu"/>
</div>
<div class="lyrico-lyrics-wrapper">Singilu Mingilu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Singilu Mingilu"/>
</div>
<div class="lyrico-lyrics-wrapper">Singilu Mingilu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Singilu Mingilu"/>
</div>
<div class="lyrico-lyrics-wrapper">Singilu Mingilu Double
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Singilu Mingilu Double"/>
</div>
<div class="lyrico-lyrics-wrapper">Singilu Singilu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Singilu Singilu"/>
</div>
<div class="lyrico-lyrics-wrapper">Singilu Singilu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Singilu Singilu"/>
</div>
<div class="lyrico-lyrics-wrapper">Singaaraanive
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Singaaraanive"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu Singile Singile 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu Singile Singile "/>
</div>
<div class="lyrico-lyrics-wrapper">Singile Singile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Singile Singile"/>
</div>
<div class="lyrico-lyrics-wrapper">Singaaraanive
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Singaaraanive"/>
</div>
<div class="lyrico-lyrics-wrapper">Naatho Mingilu Mingilu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naatho Mingilu Mingilu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mingilu Mingilu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mingilu Mingilu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mingilu Mingilu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mingilu Mingilu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayye Naa Banga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayye Naa Banga"/>
</div>
<div class="lyrico-lyrics-wrapper">Banga Banga Banga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Banga Banga Banga "/>
</div>
<div class="lyrico-lyrics-wrapper">Banga Banga Banga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Banga Banga Banga "/>
</div>
<div class="lyrico-lyrics-wrapper">Banga Bangaranive
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Banga Bangaranive"/>
</div>
</pre>
