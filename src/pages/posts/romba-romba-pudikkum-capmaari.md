---
title: "romba romba pudikkum song lyrics"
album: "Capmaari"
artist: "Siddharth Vipin"
lyricist: "Mohan Rajan"
director: "S.A. Chandrasekhar"
path: "/albums/capmaari-lyrics"
song: "Romba Romba Pudikkum"
image: ../../images/albumart/capmaari.jpg
date: 2019-12-13
lang: tamil
youtubeLink: "https://www.youtube.com/embed/zwjwYF4miV0"
type: "love"
singers:
  - Vijay Antony
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Romba Romba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba Romba"/>
</div>
<div class="lyrico-lyrics-wrapper">Romba Romba Romba Romba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba Romba Romba Romba"/>
</div>
<div class="lyrico-lyrics-wrapper">Romba Romba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba Romba"/>
</div>
<div class="lyrico-lyrics-wrapper">Romba Romba Romba Romba Pudikkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba Romba Romba Romba Pudikkuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paaththaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaththaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Konjam Konjam Pudikkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Konjam Konjam Pudikkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirichaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirichaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Innum Konjam Pudikkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Innum Konjam Pudikkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Anaichaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anaichaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu Romba Romba Pudikkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu Romba Romba Pudikkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Vaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vaadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paaththaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaththaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Konjam Kooda Pudikala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Konjam Kooda Pudikala"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirichaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirichaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Innum Kooda Pudikala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Innum Kooda Pudikala"/>
</div>
<div class="lyrico-lyrics-wrapper">Anaichaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anaichaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu Suththaamaga Pudikala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu Suththaamaga Pudikala"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Podaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Podaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Ice Vacha Konjam Pudikalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Ice Vacha Konjam Pudikalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Kai Vacha Innum Pudikalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kai Vacha Innum Pudikalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Kiss Pannaa Romba Pudikkalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kiss Pannaa Romba Pudikkalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippo Ok Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo Ok Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paaththaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaththaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Konjam Konjam Pudikkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Konjam Konjam Pudikkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirichaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirichaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Innum Konjam Pudikkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Innum Konjam Pudikkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Anaichaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anaichaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu Romba Romba Pudikkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu Romba Romba Pudikkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Vaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vaadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kojurathu Konjam Konjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kojurathu Konjam Konjam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Thitturathu Konjam Konjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Thitturathu Konjam Konjam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudikkum Pudikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudikkum Pudikum"/>
</div>
<div class="lyrico-lyrics-wrapper">Chella Chella Sanda Pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chella Chella Sanda Pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mella Vandhu Pesurathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mella Vandhu Pesurathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Romba Romba Romba Pudikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba Romba Romba Pudikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pammurathu Konjam Konjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pammurathu Konjam Konjam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Kenjurathu Innum Konjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kenjurathu Innum Konjam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Muththam Vaikkum Neram Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muththam Vaikkum Neram Ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pitham Yera Seiyurathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pitham Yera Seiyurathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Romba Romba Romba Pudikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba Romba Romba Pudikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Velli Malar Thoovum Neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velli Malar Thoovum Neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Vekkam Dhooram Pogaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vekkam Dhooram Pogaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veliyil Kuliru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veliyil Kuliru"/>
</div>
<div class="lyrico-lyrics-wrapper">Udambil Analu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udambil Analu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjilae Pudhu Vidha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjilae Pudhu Vidha"/>
</div>
<div class="lyrico-lyrics-wrapper">Paravasam Aachae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paravasam Aachae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kattilukku Vettkam Vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattilukku Vettkam Vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannaa Moodi Thoongaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaa Moodi Thoongaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhkai Muzhudhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkai Muzhudhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippadi Irukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippadi Irukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Adadaa Romba Pudikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adadaa Romba Pudikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paaththaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaththaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Konjam Konjam Pudikkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Konjam Konjam Pudikkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirichaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirichaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Innum Konjam Pudikkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Innum Konjam Pudikkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Anaichaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anaichaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu Romba Romba Pudikkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu Romba Romba Pudikkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Vaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vaadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paaththaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaththaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Konjam Konjam Pudikkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Konjam Konjam Pudikkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirichaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirichaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Innum Konjam Pudikkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Innum Konjam Pudikkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Anaichaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anaichaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu Romba Romba Pudikkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu Romba Romba Pudikkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Vaadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vaadaa"/>
</div>
</pre>
