---
title: "uyire koodu payudhey song lyrics"
album: "Naruvi"
artist: "Christy"
lyricist: "Yugabharathi"
director: "Raja Muralidharan"
path: "/albums/naruvi-lyrics"
song: "Uyire Koodu Payudhey"
image: ../../images/albumart/naruvi.jpg
date: 2021-10-22
lang: tamil
youtubeLink: "https://www.youtube.com/embed/U8ORz1kPE9A"
type: "love"
singers:
  - Abe
  - Vanthana Srinivasan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">uyire koodu payuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyire koodu payuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">uruvam jodi seruthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uruvam jodi seruthe"/>
</div>
<div class="lyrico-lyrics-wrapper">paniyum suduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paniyum suduthe"/>
</div>
<div class="lyrico-lyrics-wrapper">paruvam thoduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paruvam thoduthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">uyire thooral poduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyire thooral poduthe"/>
</div>
<div class="lyrico-lyrics-wrapper">uravum oodi poguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uravum oodi poguthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">annan thanni erangama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="annan thanni erangama"/>
</div>
<div class="lyrico-lyrics-wrapper">kannu rendum urangama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannu rendum urangama"/>
</div>
<div class="lyrico-lyrics-wrapper">unna enni sila naala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna enni sila naala"/>
</div>
<div class="lyrico-lyrics-wrapper">kirukagi poguren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kirukagi poguren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">innum enna nerungama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="innum enna nerungama"/>
</div>
<div class="lyrico-lyrics-wrapper">innum yen mayangama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="innum yen mayangama"/>
</div>
<div class="lyrico-lyrics-wrapper">sollu pulla oru vaartha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sollu pulla oru vaartha"/>
</div>
<div class="lyrico-lyrics-wrapper">sugamaga saguren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sugamaga saguren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">epavume nee than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="epavume nee than"/>
</div>
<div class="lyrico-lyrics-wrapper">ullam urugi kollai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ullam urugi kollai "/>
</div>
<div class="lyrico-lyrics-wrapper">ittu pora enna thaluvi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ittu pora enna thaluvi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">otha nodi kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="otha nodi kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">unna vilagi vaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna vilagi vaala"/>
</div>
<div class="lyrico-lyrics-wrapper">nenapena sollu nerungi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenapena sollu nerungi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pullu mela oru aasai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pullu mela oru aasai"/>
</div>
<div class="lyrico-lyrics-wrapper">sollu pulla oru baasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sollu pulla oru baasa"/>
</div>
<div class="lyrico-lyrics-wrapper">venna vachu thara poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="venna vachu thara poren"/>
</div>
<div class="lyrico-lyrics-wrapper">valinje nee vara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valinje nee vara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thottu thottu katha pesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thottu thottu katha pesa"/>
</div>
<div class="lyrico-lyrics-wrapper">thotu thotu udal koosa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thotu thotu udal koosa"/>
</div>
<div class="lyrico-lyrics-wrapper">kotti vitta mala pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kotti vitta mala pola"/>
</div>
<div class="lyrico-lyrics-wrapper">nananjene thamarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nananjene thamarai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">unna vida ethum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna vida ethum"/>
</div>
<div class="lyrico-lyrics-wrapper">inga perusu illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inga perusu illa"/>
</div>
<div class="lyrico-lyrics-wrapper">unmaiyila nee than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unmaiyila nee than"/>
</div>
<div class="lyrico-lyrics-wrapper">theiva parise
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theiva parise"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">mettiyena maarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mettiyena maarum"/>
</div>
<div class="lyrico-lyrics-wrapper">velli koluse alli thara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="velli koluse alli thara"/>
</div>
<div class="lyrico-lyrics-wrapper">thane intha manase
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thane intha manase"/>
</div>
</pre>
