---
title: "amma nanna song lyrics"
album: "Vinaya Vidheya Rama"
artist: "Devi Sri Prasad"
lyricist: "Ramajogayya Sastry"
director: "Boyapati Srinu"
path: "/albums/vinaya-vidheya-rama-lyrics"
song: "Amma Nanna"
image: ../../images/albumart/vinaya-vidheya-rama.jpg
date: 2019-01-11
lang: telugu
youtubeLink: "https://www.youtube.com/embed/siXFycg9ML0"
type: "affection"
singers:
  - Kaala Bhairava
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Amma nanna leni pasivaallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma nanna leni pasivaallu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aina anni unnollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aina anni unnollu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ningi nela veeri nesthalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ningi nela veeri nesthalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Komma remma chuttalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Komma remma chuttalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ee aadi paade pandavulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee aadi paade pandavulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalathe leni maharajulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalathe leni maharajulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee bandham leni bandhuvulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee bandham leni bandhuvulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalisuntaranta yenaleni rojulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalisuntaranta yenaleni rojulu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Laali jo laali jo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laali jo laali jo"/>
</div>
<div class="lyrico-lyrics-wrapper">Laali jo laali jo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laali jo laali jo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhile dehale aidhaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhile dehale aidhaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Praanam maathram okatega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Praanam maathram okatega"/>
</div>
<div class="lyrico-lyrics-wrapper">Veru moolalanni verainaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veru moolalanni verainaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Velle maargam okategaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velle maargam okategaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oka raktham kaane kaakunnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oka raktham kaane kaakunnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha kanna minnai kalisaaruga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha kanna minnai kalisaaruga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee bhandham pere vivarangaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee bhandham pere vivarangaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vivarinche maate janminchaledhuga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vivarinche maate janminchaledhuga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Laali jo laali jo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laali jo laali jo"/>
</div>
<div class="lyrico-lyrics-wrapper">Laali jo laali jo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laali jo laali jo"/>
</div>
</pre>
