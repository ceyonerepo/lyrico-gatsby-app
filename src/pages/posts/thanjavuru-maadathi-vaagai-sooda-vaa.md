---
title: "thanjavuru maadathi song lyrics"
album: "Vaagai Sooda Vaa"
artist: "Ghibran"
lyricist: "Ve. Ramasamy"
director: "A. Sarkunam"
path: "/albums/vaagai-sooda-vaa-lyrics"
song: "Thanjavuru Maadathi"
image: ../../images/albumart/vaagai-sooda-vaa.jpg
date: 2011-09-30
lang: tamil
youtubeLink: "https://www.youtube.com/embed/UBATPj6z2ZU"
type: "happy"
singers:
  - Jayamoorthy
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Thanjavuru maadathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanjavuru maadathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pampara konda maadathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pampara konda maadathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vengala thonda maadathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vengala thonda maadathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava kunthaniyaam maadathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava kunthaniyaam maadathi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalikkaatu ooda mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalikkaatu ooda mela"/>
</div>
<div class="lyrico-lyrics-wrapper">Maadu mecha maadathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maadu mecha maadathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatukkula kandeduthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatukkula kandeduthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaada mutta maadathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaada mutta maadathi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sayangkaalam veedu vanthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sayangkaalam veedu vanthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vega vacha maadathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vega vacha maadathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Puncha poona mamangaaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puncha poona mamangaaran"/>
</div>
<div class="lyrico-lyrics-wrapper">Thinga vanthan maadathi aaa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thinga vanthan maadathi aaa aaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Puncha poona mamangaaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puncha poona mamangaaran"/>
</div>
<div class="lyrico-lyrics-wrapper">Thinga vanthan maadathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thinga vanthan maadathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thinga vantha mamangaaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thinga vantha mamangaaran"/>
</div>
<div class="lyrico-lyrics-wrapper">Oodi poonan maadathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oodi poonan maadathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aamampaanaiyil neenthunathu athu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aamampaanaiyil neenthunathu athu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paampukunju maadathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paampukunju maadathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatilava eduthathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatilava eduthathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paambu mutta maadathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paambu mutta maadathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Hahahahahaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hahahahahaha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En thanjavuru maadathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En thanjavuru maadathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pampara konda maadathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pampara konda maadathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vengala thonda maadathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vengala thonda maadathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava kunthaniyaam maadathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava kunthaniyaam maadathi"/>
</div>
</pre>
