---
title: "aasai illa manithan illai song lyrics"
album: "Kannum Kannum Kollaiyadithaal"
artist: "Masala Coffee - Harshavardhan Rameshwar"
lyricist: "Harshavardhan Rameshwar"
director: "Desingh Periyasamy"
path: "/albums/kannum-kannum-kollaiyadithaal-lyrics"
song: "Aasai Illa Manithan Illai"
image: ../../images/albumart/kannum-kannum-kollaiyadithaal.jpg
date: 2020-02-28
lang: tamil
youtubeLink: "https://www.youtube.com/embed/jDVcqlvQq10"
type: "Enjoyment"
singers:
  - Harshavardhan Rameshwar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kannum Kannum Kollaiyadithaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannum Kannum Kollaiyadithaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannum Kannum Kollaiyadithaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannum Kannum Kollaiyadithaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aasai Illaa Manidhan Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai Illaa Manidhan Illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poigal Solla Thadaigal Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poigal Solla Thadaigal Illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhaikkai Ellaam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhaikkai Ellaam "/>
</div>
<div class="lyrico-lyrics-wrapper">Vesham Thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vesham Thaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninaithu Paarthaal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaithu Paarthaal "/>
</div>
<div class="lyrico-lyrics-wrapper">Mosam Thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mosam Thaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannum Kannum Kollaiyadithaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannum Kannum Kollaiyadithaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannum Kannum Kollaiyadithaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannum Kannum Kollaiyadithaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannum Kannum Kollaiyadithaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannum Kannum Kollaiyadithaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannum Kannum Kollaiyadithaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannum Kannum Kollaiyadithaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannum Kannum Kollaiyadithaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannum Kannum Kollaiyadithaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannum Kannum Kollaiyadithaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannum Kannum Kollaiyadithaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannum Kannum Kollaiyadithaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannum Kannum Kollaiyadithaal"/>
</div>
</pre>
