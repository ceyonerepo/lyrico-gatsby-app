---
title: "anasuya song lyrics"
album: "Jai Sena"
artist: "S Ravi Shankar"
lyricist: "Sirasri"
director: "V Samudra"
path: "/albums/jai-sena-lyrics"
song: "Anasuya Anasuya Anasuya - gunde jaari"
image: ../../images/albumart/jai-sena.jpg
date: 2021-01-29
lang: telugu
youtubeLink: "https://www.youtube.com/embed/IjBpW06u1z8"
type: "love"
singers:
  - Dhanunjay
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">anasuya anasuya anasuya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anasuya anasuya anasuya"/>
</div>
<div class="lyrico-lyrics-wrapper">gunde jaari gallanthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gunde jaari gallanthai"/>
</div>
<div class="lyrico-lyrics-wrapper">nee premalo padipoya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee premalo padipoya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">anasuya anasuya anasuya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anasuya anasuya anasuya"/>
</div>
<div class="lyrico-lyrics-wrapper">nee andham chusthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee andham chusthe"/>
</div>
<div class="lyrico-lyrics-wrapper">aadollandhariki asuya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aadollandhariki asuya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nagarjuna show lo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nagarjuna show lo"/>
</div>
<div class="lyrico-lyrics-wrapper">kotlu gelichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kotlu gelichi"/>
</div>
<div class="lyrico-lyrics-wrapper">villa koni pedatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="villa koni pedatha"/>
</div>
<div class="lyrico-lyrics-wrapper">kohinoor vajram thecchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kohinoor vajram thecchi"/>
</div>
<div class="lyrico-lyrics-wrapper">jadalne pedatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jadalne pedatha"/>
</div>
<div class="lyrico-lyrics-wrapper">koncham nuvvu karunisthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koncham nuvvu karunisthe"/>
</div>
<div class="lyrico-lyrics-wrapper">dil lone gudi kadatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dil lone gudi kadatha"/>
</div>
<div class="lyrico-lyrics-wrapper">deepam dhoopam veliginchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="deepam dhoopam veliginchi"/>
</div>
<div class="lyrico-lyrics-wrapper">nee gudilone pedatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee gudilone pedatha"/>
</div>
<div class="lyrico-lyrics-wrapper">sudi thirigi ne cm aithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sudi thirigi ne cm aithe"/>
</div>
<div class="lyrico-lyrics-wrapper">naa zillaki nee peredhatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa zillaki nee peredhatha"/>
</div>
<div class="lyrico-lyrics-wrapper">aithe vote lu mottham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aithe vote lu mottham"/>
</div>
<div class="lyrico-lyrics-wrapper">nee party ke veyistha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee party ke veyistha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">anasuya anasuya anasuya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anasuya anasuya anasuya"/>
</div>
<div class="lyrico-lyrics-wrapper">gunde jaari gallanthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gunde jaari gallanthai"/>
</div>
<div class="lyrico-lyrics-wrapper">nee premalo padipoya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee premalo padipoya"/>
</div>
<div class="lyrico-lyrics-wrapper">padipoya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="padipoya "/>
</div>
<div class="lyrico-lyrics-wrapper">anasuya padipoya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anasuya padipoya"/>
</div>
<div class="lyrico-lyrics-wrapper">anasuya padipoya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anasuya padipoya"/>
</div>
<div class="lyrico-lyrics-wrapper">anasuya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anasuya"/>
</div>
<div class="lyrico-lyrics-wrapper">anasuya padipoya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anasuya padipoya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">jaldi aaja baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jaldi aaja baby"/>
</div>
<div class="lyrico-lyrics-wrapper">ye dil tu leja baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ye dil tu leja baby"/>
</div>
<div class="lyrico-lyrics-wrapper">thoda love pannum baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoda love pannum baby"/>
</div>
<div class="lyrico-lyrics-wrapper">love me touch me 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="love me touch me "/>
</div>
<div class="lyrico-lyrics-wrapper">hug me kiss me 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hug me kiss me "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">gold mine ye thavvi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gold mine ye thavvi"/>
</div>
<div class="lyrico-lyrics-wrapper">nee kallaku kaatika pettestha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee kallaku kaatika pettestha"/>
</div>
<div class="lyrico-lyrics-wrapper">gold mine ye thavvi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gold mine ye thavvi"/>
</div>
<div class="lyrico-lyrics-wrapper">neeku vaddanale cheyistha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeku vaddanale cheyistha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ninnu chusthe gundelo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninnu chusthe gundelo"/>
</div>
<div class="lyrico-lyrics-wrapper">ban bhaaja mogindhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ban bhaaja mogindhi"/>
</div>
<div class="lyrico-lyrics-wrapper">land mine ye gundelo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="land mine ye gundelo"/>
</div>
<div class="lyrico-lyrics-wrapper">pelinattu naakundhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pelinattu naakundhi"/>
</div>
<div class="lyrico-lyrics-wrapper">love paatale chebuthanante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="love paatale chebuthanante"/>
</div>
<div class="lyrico-lyrics-wrapper">college ye kattistha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="college ye kattistha"/>
</div>
<div class="lyrico-lyrics-wrapper">atta singnal isthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="atta singnal isthe"/>
</div>
<div class="lyrico-lyrics-wrapper">aa kick ye verappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aa kick ye verappa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">anasuya anasuya anasuya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anasuya anasuya anasuya"/>
</div>
<div class="lyrico-lyrics-wrapper">gunde jaari gallanthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gunde jaari gallanthai"/>
</div>
<div class="lyrico-lyrics-wrapper">nee premalo padipoya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee premalo padipoya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pawan kalyan janasena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pawan kalyan janasena"/>
</div>
<div class="lyrico-lyrics-wrapper">party ticket ippistha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="party ticket ippistha"/>
</div>
<div class="lyrico-lyrics-wrapper">mahesh babu thoti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mahesh babu thoti"/>
</div>
<div class="lyrico-lyrics-wrapper">neeku selfie ne theeyistha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeku selfie ne theeyistha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">bunny hero bike medha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bunny hero bike medha"/>
</div>
<div class="lyrico-lyrics-wrapper">desamantha chupistha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="desamantha chupistha"/>
</div>
<div class="lyrico-lyrics-wrapper">ram charan trujet lo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ram charan trujet lo"/>
</div>
<div class="lyrico-lyrics-wrapper">world mottham thippestha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="world mottham thippestha"/>
</div>
<div class="lyrico-lyrics-wrapper">bangaram tho nesina cheera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bangaram tho nesina cheera"/>
</div>
<div class="lyrico-lyrics-wrapper">naaku giftuga isthava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naaku giftuga isthava"/>
</div>
<div class="lyrico-lyrics-wrapper">cheerakotena kalamandhir
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cheerakotena kalamandhir"/>
</div>
<div class="lyrico-lyrics-wrapper">shopuni raasistha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="shopuni raasistha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">anasuya anasuya anasuya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anasuya anasuya anasuya"/>
</div>
<div class="lyrico-lyrics-wrapper">gunde jaari gallanthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gunde jaari gallanthai"/>
</div>
<div class="lyrico-lyrics-wrapper">nee premalo padipoya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee premalo padipoya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">asuya asuya asuya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="asuya asuya asuya"/>
</div>
<div class="lyrico-lyrics-wrapper">ne neetho thirigithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ne neetho thirigithe"/>
</div>
<div class="lyrico-lyrics-wrapper">chuse kallaki asuya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chuse kallaki asuya "/>
</div>
<div class="lyrico-lyrics-wrapper">nagarjuna show lo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nagarjuna show lo"/>
</div>
<div class="lyrico-lyrics-wrapper">kotlu gelichi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kotlu gelichi "/>
</div>
<div class="lyrico-lyrics-wrapper">villa koni pedatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="villa koni pedatha"/>
</div>
<div class="lyrico-lyrics-wrapper">kohinoor vajram thecchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kohinoor vajram thecchi"/>
</div>
<div class="lyrico-lyrics-wrapper">jadalne pedatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jadalne pedatha"/>
</div>
<div class="lyrico-lyrics-wrapper">koncham nuvvu karunisthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koncham nuvvu karunisthe"/>
</div>
<div class="lyrico-lyrics-wrapper">dil lone gudi kadatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dil lone gudi kadatha"/>
</div>
<div class="lyrico-lyrics-wrapper">deepam dhoopam veliginchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="deepam dhoopam veliginchi"/>
</div>
<div class="lyrico-lyrics-wrapper">nee gudilone pedatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee gudilone pedatha"/>
</div>
<div class="lyrico-lyrics-wrapper">sudi thirigi ne cm aithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sudi thirigi ne cm aithe"/>
</div>
<div class="lyrico-lyrics-wrapper">naa zillaki nee peredhatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa zillaki nee peredhatha"/>
</div>
<div class="lyrico-lyrics-wrapper">aithe vote lu mottham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aithe vote lu mottham"/>
</div>
<div class="lyrico-lyrics-wrapper">nee party ke veyistha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee party ke veyistha"/>
</div>
</pre>
