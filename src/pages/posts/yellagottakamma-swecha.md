---
title: "yellagottakamma song lyrics"
album: "Swecha"
artist: "Bhole Shawali"
lyricist: "Bhole Shawali"
director: "KPN Chawhan"
path: "/albums/swecha-lyrics"
song: "Yellagottakamma"
image: ../../images/albumart/swecha.jpg
date: 2020-02-28
lang: telugu
youtubeLink: "https://www.youtube.com/embed/IcdzPoJZvqw"
type: "sad"
singers:
  - Sri Druthi
  - Varam
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yellagottakamma Yellagottaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yellagottakamma Yellagottaku"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannathalli leni Janma Nakenduku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannathalli leni Janma Nakenduku"/>
</div>
<div class="lyrico-lyrics-wrapper">Yellagottakamma Yellagottaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yellagottakamma Yellagottaku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Amma Thananni Nannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma Thananni Nannu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ammeyakamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammeyakamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Ammukuntunannu Ammaleni danni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammukuntunannu Ammaleni danni"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheyyodamma Nannu Cheyyodhamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheyyodamma Nannu Cheyyodhamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Ammanu minchina Daivam ledani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammanu minchina Daivam ledani"/>
</div>
<div class="lyrico-lyrics-wrapper">Andaru antaramma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andaru antaramma"/>
</div>
<div class="lyrico-lyrics-wrapper">ade amma kadupulo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ade amma kadupulo"/>
</div>
<div class="lyrico-lyrics-wrapper">adapilla pudithe Dayyam antarendukamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adapilla pudithe Dayyam antarendukamma"/>
</div>
<div class="lyrico-lyrics-wrapper">amma antu pliche Dudaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="amma antu pliche Dudaki"/>
</div>
<div class="lyrico-lyrics-wrapper">pematho Palisthadi Aavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pematho Palisthadi Aavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Amma antu piliche Naapaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma antu piliche Naapaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanikarame chupamma nuvvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanikarame chupamma nuvvu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeduposthundamma yeduposthundi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeduposthundamma yeduposthundi"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeduposthundamma yeduposthundi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeduposthundamma yeduposthundi"/>
</div>
<div class="lyrico-lyrics-wrapper">Yadikelli nenu Bhrathakalamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yadikelli nenu Bhrathakalamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Okkasari nannu Olloki pilichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okkasari nannu Olloki pilichi"/>
</div>
<div class="lyrico-lyrics-wrapper">Jila paadi Nannu Nidhrapucchamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jila paadi Nannu Nidhrapucchamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yellagottakamma Yellagottaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yellagottakamma Yellagottaku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa kantiki Katuka Nuvvu peduthunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa kantiki Katuka Nuvvu peduthunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallaku Andham Anukunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallaku Andham Anukunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Katuka venuka Cheekati Vundani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Katuka venuka Cheekati Vundani"/>
</div>
<div class="lyrico-lyrics-wrapper">Anukoledhamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anukoledhamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa nudhitina Bottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa nudhitina Bottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu peduthunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu peduthunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Addam lo Muddhuga chusukunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Addam lo Muddhuga chusukunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Bottu Nudhiti Raatha paina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Bottu Nudhiti Raatha paina"/>
</div>
<div class="lyrico-lyrics-wrapper">Macchundhani Gurthinchaledhamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Macchundhani Gurthinchaledhamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Naaku rendu jedalu vesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naaku rendu jedalu vesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Badiki Pamputhunte Gundela ninda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Badiki Pamputhunte Gundela ninda"/>
</div>
<div class="lyrico-lyrics-wrapper">Upponginanamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Upponginanamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa jadala venuka Naa jathakamundani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa jadala venuka Naa jathakamundani"/>
</div>
<div class="lyrico-lyrics-wrapper">Oohinchaledamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oohinchaledamma"/>
</div>
<div class="lyrico-lyrics-wrapper">NenOohinchaledamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="NenOohinchaledamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yellagottakamma Yellagottaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yellagottakamma Yellagottaku"/>
</div>
<div class="lyrico-lyrics-wrapper">Yellagoduthunte Eduposthundamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yellagoduthunte Eduposthundamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Yellagottakamma Yellagottaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yellagottakamma Yellagottaku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adajanmane Yetthinanduku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adajanmane Yetthinanduku"/>
</div>
<div class="lyrico-lyrics-wrapper">Adajanmane Yetthinanduku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adajanmane Yetthinanduku"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatithippalu Thappaledu Neeku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatithippalu Thappaledu Neeku"/>
</div>
<div class="lyrico-lyrics-wrapper">Chettakuppalo padukoni nuvvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chettakuppalo padukoni nuvvu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalli Odiney Vethukuthunnava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalli Odiney Vethukuthunnava"/>
</div>
<div class="lyrico-lyrics-wrapper">Oopiradani Uyyalalona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oopiradani Uyyalalona"/>
</div>
<div class="lyrico-lyrics-wrapper">Ukkiri Bikkiri Ayipothunava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ukkiri Bikkiri Ayipothunava"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadapillaga Puttavani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadapillaga Puttavani"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeku theliyadhemo Chittikanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeku theliyadhemo Chittikanna"/>
</div>
<div class="lyrico-lyrics-wrapper">Andhuke ikkada Narakanni Nuvvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhuke ikkada Narakanni Nuvvu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chusthunnave Bujji Kanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chusthunnave Bujji Kanna"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalanti Puttuke Nuvvu Puttave Thalli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalanti Puttuke Nuvvu Puttave Thalli"/>
</div>
<div class="lyrico-lyrics-wrapper">Pranalu Vunnatha varaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pranalu Vunnatha varaku"/>
</div>
<div class="lyrico-lyrics-wrapper">Chasthu Brathakali Malli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chasthu Brathakali Malli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theesukelthanamma Theesukelthanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theesukelthanamma Theesukelthanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalanti Kashtalu Rakunda Ninnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalanti Kashtalu Rakunda Ninnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Saadukuntanamma Saadukuntanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saadukuntanamma Saadukuntanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninukanna Paapishti Kanna Thallini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninukanna Paapishti Kanna Thallini"/>
</div>
<div class="lyrico-lyrics-wrapper">Maripisthanamma Maripisthanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maripisthanamma Maripisthanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu Bhaga Chusukoni Muripisthanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu Bhaga Chusukoni Muripisthanu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gurthochana nanna Gurthukocchana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gurthochana nanna Gurthukocchana"/>
</div>
<div class="lyrico-lyrics-wrapper">Niku Bharamaina Papishti Biddani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niku Bharamaina Papishti Biddani"/>
</div>
<div class="lyrico-lyrics-wrapper">Gurthochana nanna Gurthukocchana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gurthochana nanna Gurthukocchana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Devudante Naku Teliyani Lokanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Devudante Naku Teliyani Lokanni"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannugaa Nannu Srushinchinavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannugaa Nannu Srushinchinavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adapillaga Puttinanduku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adapillaga Puttinanduku"/>
</div>
<div class="lyrico-lyrics-wrapper">Angattlo Bommaga Ammesinaavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Angattlo Bommaga Ammesinaavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanna Antu Alladuthunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanna Antu Alladuthunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanikarame Chupinchakunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanikarame Chupinchakunda"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu Chesina Appula Kindha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu Chesina Appula Kindha"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhali Pashuvunu Chesavu nannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhali Pashuvunu Chesavu nannu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gurthochana nanna Gurthukocchana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gurthochana nanna Gurthukocchana"/>
</div>
<div class="lyrico-lyrics-wrapper">Gurthochana nanna Gurthukocchana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gurthochana nanna Gurthukocchana"/>
</div>
</pre>
