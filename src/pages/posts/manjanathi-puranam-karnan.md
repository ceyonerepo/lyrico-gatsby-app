---
title: 'manjanathi puranam song lyrics'
album: 'Karnan'
artist: 'Santhosh Narayanan'
lyricist: 'Yugabharathi'
director: 'Mari Selvaraj'
path: '/albums/karnan-song-lyrics'
song: 'Manjanathi Puranam'
image: ../../images/albumart/karnan.jpg
date: 2021-04-09
lang: tamil
youtubeLink: "https://www.youtube.com/embed/oAa5jfKylgc"
type: 'celebration'
singers: 
- Thenisai Thendrl Deva
- Reetha Anthony
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">

<div class="lyrico-lyrics-wrapper">En aalu manjanathi eduppaana sembaruthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En aalu manjanathi eduppaana sembaruthi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannala ennai koththi kalangadicha pathagathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannala ennai koththi kalangadicha pathagathi"/>
</div>
<div class="lyrico-lyrics-wrapper">En aalu manjanathi eduppaana sembaruthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En aalu manjanathi eduppaana sembaruthi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannala ennai kothi kalangadicha pathagathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannala ennai kothi kalangadicha pathagathi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En kakkathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kakkathula"/>
</div>
<div class="lyrico-lyrics-wrapper">En kakkathula vecha thunda thozhu mela pottuvutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kakkathula vecha thunda thozhu mela pottuvutta"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoranaiya naanum nadakka vaalibatha yethivutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoranaiya naanum nadakka vaalibatha yethivutta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En kakkathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kakkathula"/>
</div>
<div class="lyrico-lyrics-wrapper">En kakkathula vecha thunda thozhu mela pottuvutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kakkathula vecha thunda thozhu mela pottuvutta"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoranaiya naanum nadakka vaalibatha yethivutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoranaiya naanum nadakka vaalibatha yethivutta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Antha sittazhagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha sittazhagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha sittazhagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha sittazhagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha sittazhagi sottazhagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha sittazhagi sottazhagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhaiya kelu ravuttu vandukku ravuttu vandukku ravuttu vandukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhaiya kelu ravuttu vandukku ravuttu vandukku ravuttu vandukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En aalu manjanathi eduppaana sembaruthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En aalu manjanathi eduppaana sembaruthi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannala ennai kothi kalangadicha pathagathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannala ennai kothi kalangadicha pathagathi"/>
</div>
<div class="lyrico-lyrics-wrapper">En aalu manjanathi…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En aalu manjanathi…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vallanattu malaiyoram vaaram oru tharam paathom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vallanattu malaiyoram vaaram oru tharam paathom"/>
</div>
<div class="lyrico-lyrics-wrapper">Mullukkaattu moottoram moochu mutta theneduthom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mullukkaattu moottoram moochu mutta theneduthom"/>
</div>
<div class="lyrico-lyrics-wrapper">Kakkulathu pakkathula kaalasamy koiyilila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kakkulathu pakkathula kaalasamy koiyilila"/>
</div>
<div class="lyrico-lyrics-wrapper">Saadhiyathaan palikoduthu sandhanam kungumam poosikittom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saadhiyathaan palikoduthu sandhanam kungumam poosikittom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En manjanathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En manjanathi"/>
</div>
<div class="lyrico-lyrics-wrapper">En manjanathi yemanoda veettu velakka yethuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En manjanathi yemanoda veettu velakka yethuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Erumaiyaattam thirinja payala yaanai mela yethuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Erumaiyaattam thirinja payala yaanai mela yethuna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Antha mottazhagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha mottazhagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha mottazhagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha mottazhagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha mottazhagi pottazhagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha mottazhagi pottazhagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhaiya kelu ravuttu vandukku ravuttu vandukku ravuttu vandukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhaiya kelu ravuttu vandukku ravuttu vandukku ravuttu vandukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yei…yei podu podu riththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei…yei podu podu riththa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yei podu podu riththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei podu podu riththa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yei podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yei podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yei podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yei podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei podu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yei riththa riththa podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei riththa riththa podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yei riththa riththa podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei riththa riththa podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yei ritha yei ritha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei ritha yei ritha"/>
</div>
<div class="lyrico-lyrics-wrapper">Yei ritha yei ritha yei podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei ritha yei ritha yei podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yei podu podu podu podu podu podu podu podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei podu podu podu podu podu podu podu podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Riththa yei riththa yei riththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Riththa yei riththa yei riththa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karuppandhorai manneduthu kaara veedu kattikkittom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuppandhorai manneduthu kaara veedu kattikkittom"/>
</div>
<div class="lyrico-lyrics-wrapper">Naayikutty naaleduthu kozhandhayaakki konjikkittom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naayikutty naaleduthu kozhandhayaakki konjikkittom"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadhavathi saathisanam veleduthu vaarumunnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadhavathi saathisanam veleduthu vaarumunnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaleduthu sandayida vaasalila kaathirundhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaleduthu sandayida vaasalila kaathirundhen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En manjanathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En manjanathi"/>
</div>
<div class="lyrico-lyrics-wrapper">En manjanathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En manjanathi"/>
</div>
<div class="lyrico-lyrics-wrapper">En manjanathi odambukkulla enna ezhavu poondhucho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En manjanathi odambukkulla enna ezhavu poondhucho"/>
</div>
<div class="lyrico-lyrics-wrapper">Cholerannu vandha noyi yeman kannedhire thinniduche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cholerannu vandha noyi yeman kannedhire thinniduche"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En mottazhagi pottazhagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En mottazhagi pottazhagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhaiya keli ravuttu vandukku ravut ravu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhaiya keli ravuttu vandukku ravut ravu"/>
</div>
</pre>