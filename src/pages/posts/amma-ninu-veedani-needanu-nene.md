---
title: "amma song lyrics"
album: "Ninu Veedani Needanu Nene"
artist: "S. Thaman"
lyricist: "Ramajogayya Sastry"
director: "Caarthick Raju"
path: "/albums/ninu-veedani-needanu-nene-lyrics"
song: "Amma"
image: ../../images/albumart/ninu-veedani-needanu-nene.jpg
date: 2019-07-12
lang: telugu
youtubeLink: "https://www.youtube.com/embed/tud4WGDZXgY"
type: "sentiment"
singers:
  - Srikrishna
  - Nandita Jyoti
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Amma Oo Amma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma Oo Amma"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Janmaku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Janmaku "/>
</div>
<div class="lyrico-lyrics-wrapper">Inthe Runam Amma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inthe Runam Amma"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanna Nanna Ika Dorakadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanna Nanna Ika Dorakadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ga Mee Prema
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ga Mee Prema"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappinchaleni Daina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappinchaleni Daina"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Thala Raatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Thala Raatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Thappu Valane Meeku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Thappu Valane Meeku"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Yedakotha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Yedakotha"/>
</div>
<div class="lyrico-lyrics-wrapper">Munde Thondara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munde Thondara"/>
</div>
<div class="lyrico-lyrics-wrapper">Padina Vegam Lo Na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padina Vegam Lo Na"/>
</div>
<div class="lyrico-lyrics-wrapper">Ala Laaga Viriganu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ala Laaga Viriganu"/>
</div>
<div class="lyrico-lyrics-wrapper">Andani Lokam Vaipu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andani Lokam Vaipu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adugulu Vesthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adugulu Vesthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Selavantu Kadilanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Selavantu Kadilanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Amma Oo Amma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma Oo Amma"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Janmaku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Janmaku "/>
</div>
<div class="lyrico-lyrics-wrapper">Inthe Runam Amma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inthe Runam Amma"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanna Nanna Ika Dorakadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanna Nanna Ika Dorakadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ga Mee Prema
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ga Mee Prema"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kani Penchina Mee Lona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kani Penchina Mee Lona"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanneti Thadai Migilanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneti Thadai Migilanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheyi Jaarina Jeevithamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheyi Jaarina Jeevithamai"/>
</div>
<div class="lyrico-lyrics-wrapper">Manninchamani Adiganu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manninchamani Adiganu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Chote Nenunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Chote Nenunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Meetho Ne Nenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meetho Ne Nenu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mee Oopiri Chirugaalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mee Oopiri Chirugaalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalise Untanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalise Untanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kala Lona Alarinche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kala Lona Alarinche"/>
</div>
<div class="lyrico-lyrics-wrapper">Mee Kanu Paapanauthanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mee Kanu Paapanauthanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maru Janmantu Raasunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maru Janmantu Raasunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Mee Pasi Paapai Pudathanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mee Pasi Paapai Pudathanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Amma Oo Amma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma Oo Amma"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Janmaku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Janmaku "/>
</div>
<div class="lyrico-lyrics-wrapper">Inthe Runam Amma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inthe Runam Amma"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanna Nanna Ika Dorakadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanna Nanna Ika Dorakadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ga Mee Prema
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ga Mee Prema"/>
</div>
</pre>
