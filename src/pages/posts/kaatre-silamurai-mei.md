---
title: "kaatre silamurai song lyrics"
album: "Mei"
artist: "Prithvi Kumar"
lyricist: "Christoper Pradeep"
director: "SA Baskaran"
path: "/albums/mei-lyrics"
song: "Kaatre Silamurai"
image: ../../images/albumart/mei.jpg
date: 2019-08-23
lang: tamil
youtubeLink: "https://www.youtube.com/embed/LD1VC8sfI2g"
type: "love"
singers:
  - Prithvi Kumar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vaazhkaiyae Naadagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkaiyae Naadagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Adil Nadigargal Naamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adil Nadigargal Naamey"/>
</div>
<div class="lyrico-lyrics-wrapper">Velaiyil Sodhanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velaiyil Sodhanai"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu Dhinam Dhinam Dhannae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu Dhinam Dhinam Dhannae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanilae Vaanavil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanilae Vaanavil"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhin Nirangalum Naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhin Nirangalum Naanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Siragugal Sudhandhiram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siragugal Sudhandhiram"/>
</div>
<div class="lyrico-lyrics-wrapper">Parandhidavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parandhidavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhkaiyae Naadagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkaiyae Naadagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Adil Nadigargal Naamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adil Nadigargal Naamey"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhkaiyae Oor Kadal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkaiyae Oor Kadal"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhil Alaigalum Naamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhil Alaigalum Naamae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanilae Vaanavil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanilae Vaanavil"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhin Nirangalum Naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhin Nirangalum Naanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Siragugal Sudhandhiram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siragugal Sudhandhiram"/>
</div>
<div class="lyrico-lyrics-wrapper">Parandhidavaa Melae Melae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parandhidavaa Melae Melae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kelvigal Ketpadhil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelvigal Ketpadhil"/>
</div>
<div class="lyrico-lyrics-wrapper">Thindaatam Kondaatam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thindaatam Kondaatam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pandhaatam Yeno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pandhaatam Yeno"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatrey SilaMurai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatrey SilaMurai"/>
</div>
<div class="lyrico-lyrics-wrapper">Arivurai Sollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arivurai Sollum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arivurai Sollum Kaadhalaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arivurai Sollum Kaadhalaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sollamalae Iravae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollamalae Iravae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagalum Thalai Keezhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagalum Thalai Keezhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Maarum Maayamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maarum Maayamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhalil Uravaa Oru Kelvi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalil Uravaa Oru Kelvi"/>
</div>
<div class="lyrico-lyrics-wrapper">Yennai Naanae Kaeten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yennai Naanae Kaeten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhkaiyae Naadagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkaiyae Naadagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhil Nadigargal Naamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhil Nadigargal Naamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanilae Vaanavil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanilae Vaanavil"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhin Nirangalum Naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhin Nirangalum Naanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naalaimel Nambikkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalaimel Nambikkai"/>
</div>
<div class="lyrico-lyrics-wrapper">Illaamal Thoongaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illaamal Thoongaadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavellam Nee Paarkkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavellam Nee Paarkkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nijamaagum Un Kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijamaagum Un Kaalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vinmeengal Vazhi Kaatum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinmeengal Vazhi Kaatum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aagaayam Madisaayum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagaayam Madisaayum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhvadhum Saavadhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvadhum Saavadhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponadhum Ulladhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponadhum Ulladhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Orumurai Dhaan Ini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orumurai Dhaan Ini"/>
</div>
<div class="lyrico-lyrics-wrapper">Paaraatta En Nanbanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaraatta En Nanbanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Murai Kaadhalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Murai Kaadhalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhalaa Un Vaazhkaiyae Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalaa Un Vaazhkaiyae Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagey Dhinamum Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagey Dhinamum Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Yendrendu Anaithida Maayamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yendrendu Anaithida Maayamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Kaadhalil Vizhava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kaadhalil Vizhava"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kai Korthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kai Korthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnoda Kaatraga Marayava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnoda Kaatraga Marayava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhkaiyae Naadagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkaiyae Naadagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Adil Nadigargal Naamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adil Nadigargal Naamey"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanilae Vaanavil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanilae Vaanavil"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhin Nirangalum Naamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhin Nirangalum Naamae"/>
</div>
</pre>
