---
title: "mudhal muraiyaaga penne song lyrics"
album: "Singam III"
artist: "Harris Jayaraj"
lyricist: "Thamarai - Ramya NSK"
director: "Hari Gopalakrishnan"
path: "/albums/singam-3-lyrics"
song: "Mudhal Muraiyaaga Penne"
image: ../../images/albumart/singam-3.jpg
date: 2017-02-09
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Jd3mIIzvutg"
type: "Love"
singers:
  - Harish Raghavendra
  - Swetha Mohan
  - Ramya NSK
  - Karthik
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Mudhal Muraiyaga Pennai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhal Muraiyaga Pennai "/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai paarthen 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai paarthen "/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Muzhuvadhumaaga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Muzhuvadhumaaga "/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Andre Thotren 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Andre Thotren "/>
</div>
<div class="lyrico-lyrics-wrapper">Oru murai Dhaane 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru murai Dhaane "/>
</div>
<div class="lyrico-lyrics-wrapper">Ondre Ondru Keten 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondre Ondru Keten "/>
</div>
<div class="lyrico-lyrics-wrapper">En Uyirudan naanum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Uyirudan naanum "/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Indru Serthen 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Indru Serthen "/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Thaane Nee Thaane 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Thaane Nee Thaane "/>
</div>
<div class="lyrico-lyrics-wrapper">En thaai Pola 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En thaai Pola "/>
</div>
<div class="lyrico-lyrics-wrapper">Thoongatha Sei Pola 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoongatha Sei Pola "/>
</div>
<div class="lyrico-lyrics-wrapper">Thurathadha Pei Pola 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thurathadha Pei Pola "/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal Seithaai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Seithaai "/>
</div>
<div class="lyrico-lyrics-wrapper">kaadhalil Vizha Maaten 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadhalil Vizha Maaten "/>
</div>
<div class="lyrico-lyrics-wrapper">Endre kaanthalaai irundhen 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endre kaanthalaai irundhen "/>
</div>
<div class="lyrico-lyrics-wrapper">Un kangalaal Ennai kavvi kondaai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kangalaal Ennai kavvi kondaai "/>
</div>
<div class="lyrico-lyrics-wrapper">kandhalaagi Vizhundhen 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kandhalaagi Vizhundhen "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mudhal Muraiyaga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhal Muraiyaga "/>
</div>
<div class="lyrico-lyrics-wrapper">Pennai Unnai paarthen 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pennai Unnai paarthen "/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Muzhuvadhumaaga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Muzhuvadhumaaga "/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Andre Thotren 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Andre Thotren "/>
</div>
<div class="lyrico-lyrics-wrapper">Oru murai Dhaane 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru murai Dhaane "/>
</div>
<div class="lyrico-lyrics-wrapper">Ondre Ondru Keten 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondre Ondru Keten "/>
</div>
<div class="lyrico-lyrics-wrapper">En Uyirudan naanum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Uyirudan naanum "/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Indru Serthen 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Indru Serthen "/>
</div>
<div class="lyrico-lyrics-wrapper">Manjariye Maa radhiye 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manjariye Maa radhiye "/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum Vaa Vaa veliye 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Vaa Vaa veliye "/>
</div>
<div class="lyrico-lyrics-wrapper">Un Idaiyai Naan Anaithe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Idaiyai Naan Anaithe "/>
</div>
<div class="lyrico-lyrics-wrapper">Parappen mele Vaa Kiliye 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parappen mele Vaa Kiliye "/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa Endru Nee Sonnal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Endru Nee Sonnal "/>
</div>
<div class="lyrico-lyrics-wrapper">varuven Engum Thaniye 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varuven Engum Thaniye "/>
</div>
<div class="lyrico-lyrics-wrapper">Mulmadiyo Vinveliyo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mulmadiyo Vinveliyo "/>
</div>
<div class="lyrico-lyrics-wrapper">nadapen naanum un vazhiye 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nadapen naanum un vazhiye "/>
</div>
<div class="lyrico-lyrics-wrapper">Enai Kaanaamal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai Kaanaamal "/>
</div>
<div class="lyrico-lyrics-wrapper">Mugal Konamalum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mugal Konamalum "/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinam Nindrayadi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam Nindrayadi "/>
</div>
<div class="lyrico-lyrics-wrapper">Enai Vendrayadi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai Vendrayadi "/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Thinam Thinam Ennai Vaiya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Thinam Thinam Ennai Vaiya "/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Kutram Naan Seiya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Kutram Naan Seiya "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pagalellam Paarkamal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagalellam Paarkamal "/>
</div>
<div class="lyrico-lyrics-wrapper">Yekkam Yeniyil Yerum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yekkam Yeniyil Yerum "/>
</div>
<div class="lyrico-lyrics-wrapper">Idayorey Illadha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idayorey Illadha "/>
</div>
<div class="lyrico-lyrics-wrapper">Inikkum Rathiri Vendum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inikkum Rathiri Vendum "/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Vandha Pin Thaaney 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vandha Pin Thaaney "/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhvil Ithanai Saaram 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvil Ithanai Saaram "/>
</div>
<div class="lyrico-lyrics-wrapper">Un Aasai niraivetra 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Aasai niraivetra "/>
</div>
<div class="lyrico-lyrics-wrapper">vegam Ennaiyum meerum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vegam Ennaiyum meerum "/>
</div>
<div class="lyrico-lyrics-wrapper">Viral Korthalenna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viral Korthalenna "/>
</div>
<div class="lyrico-lyrics-wrapper">Niral ketalenna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niral ketalenna "/>
</div>
<div class="lyrico-lyrics-wrapper">Pazhi Theerthalenna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pazhi Theerthalenna "/>
</div>
<div class="lyrico-lyrics-wrapper">Padham paarthaalenna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padham paarthaalenna "/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Kaavalan Thaane 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Kaavalan Thaane "/>
</div>
<div class="lyrico-lyrics-wrapper">Irundhum Kollaiyida vandhene 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irundhum Kollaiyida vandhene "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mudhal Muraiyaga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhal Muraiyaga "/>
</div>
<div class="lyrico-lyrics-wrapper">Anbe Unnai paarthen 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbe Unnai paarthen "/>
</div>
<div class="lyrico-lyrics-wrapper">En Mugavariyaga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Mugavariyaga "/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Andre Yetren 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Andre Yetren "/>
</div>
<div class="lyrico-lyrics-wrapper">Oru murai Dhaane 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru murai Dhaane "/>
</div>
<div class="lyrico-lyrics-wrapper">Ondre Ondru Keten 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondre Ondru Keten "/>
</div>
<div class="lyrico-lyrics-wrapper">En Uyirudan naanum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Uyirudan naanum "/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Indru Serthen 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Indru Serthen "/>
</div>
</pre>
