---
title: "tarrake tararrake song lyrics"
album: "Jada"
artist: "Sam CS"
lyricist: "Logan"
director: "Jada Kumaran"
path: "/albums/jada-lyrics"
song: "Tarrake Tararrake"
image: ../../images/albumart/jada.jpg
date: 2019-12-06
lang: tamil
youtubeLink: "https://www.youtube.com/embed/HGsClfKq-y0"
type: "happy"
singers:
  - Benny Dayal
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hoohoo Hoo Hoo Hoo Hoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoohoo Hoo Hoo Hoo Hoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoohoo Hoo Hoo Hoo Hoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoohoo Hoo Hoo Hoo Hoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoohoo Hoo Hoo Hoo Hoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoohoo Hoo Hoo Hoo Hoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoohoo Hoo Hoo Hoo Hoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoohoo Hoo Hoo Hoo Hoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoohoo Hoo Hoo Hoo Hoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoohoo Hoo Hoo Hoo Hoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoohoo Hoo Hoo Hoo Hoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoohoo Hoo Hoo Hoo Hoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tarraakae Tararraakae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tarraakae Tararraakae"/>
</div>
<div class="lyrico-lyrics-wrapper">Joraa Thatti Thokkae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Joraa Thatti Thokkae"/>
</div>
<div class="lyrico-lyrics-wrapper">Tarraakae Tararraakae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tarraakae Tararraakae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattaadhaa Nee Joke-Ey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattaadhaa Nee Joke-Ey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Check In Panni Skill Ah Kaattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Check In Panni Skill Ah Kaattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sedharaa Maatom Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sedharaa Maatom Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Corner Ball-Ah Goal-Ah Potta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Corner Ball-Ah Goal-Ah Potta"/>
</div>
<div class="lyrico-lyrics-wrapper">Asaraa Mattom Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asaraa Mattom Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hitting Panni Passing Podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hitting Panni Passing Podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sit-Aah Scene-U Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sit-Aah Scene-U Daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Bending Panni Kaali Pannaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bending Panni Kaali Pannaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoguraa Maattom Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoguraa Maattom Daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaada Vaada Vaa Vaa Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada Vaada Vaa Vaa Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hoohoo Hoo Hoo Hoo Hoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoohoo Hoo Hoo Hoo Hoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoohoo Hoo Hoo Hoo Hoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoohoo Hoo Hoo Hoo Hoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoohoo Hoo Hoo Hoo Hoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoohoo Hoo Hoo Hoo Hoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoohoo Hoo Hoo Hoo Hoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoohoo Hoo Hoo Hoo Hoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theemtha Theemtha Thanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theemtha Theemtha Thanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Theemtha Theemtha Thanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theemtha Theemtha Thanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kilicha Coataiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kilicha Coataiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaichi Pottukuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaichi Pottukuvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Toss-Eh Pottadhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Toss-Eh Pottadhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga Style-La
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Style-La"/>
</div>
<div class="lyrico-lyrics-wrapper">Neymer Messi Nu Pera Vachikuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neymer Messi Nu Pera Vachikuvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholukku Slot-La Pagaiya Muchikuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholukku Slot-La Pagaiya Muchikuvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aadaa Vuttu Naanga Amara Vaipom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadaa Vuttu Naanga Amara Vaipom"/>
</div>
<div class="lyrico-lyrics-wrapper">Oda Vittu Team-Ah Odhara Vaippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oda Vittu Team-Ah Odhara Vaippom"/>
</div>
<div class="lyrico-lyrics-wrapper">Locality Bhasaiya Pesa Maatom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Locality Bhasaiya Pesa Maatom"/>
</div>
<div class="lyrico-lyrics-wrapper">Local Naanga Hi-Fy Theda Maatom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Local Naanga Hi-Fy Theda Maatom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannula Jerkku Kaalula Thalukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannula Jerkku Kaalula Thalukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Pithirla Kaattuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pithirla Kaattuvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Moonchila Morappu Pechila Sharp-U
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moonchila Morappu Pechila Sharp-U"/>
</div>
<div class="lyrico-lyrics-wrapper">Pasiya Thookuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pasiya Thookuvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Adra Merlaaa Udra Merlaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adra Merlaaa Udra Merlaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adchi Kalttuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adchi Kalttuvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiyila Thaduppu Lungila Madippu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyila Thaduppu Lungila Madippu"/>
</div>
<div class="lyrico-lyrics-wrapper">Bagundai Aakuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bagundai Aakuvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaada Vaada Vaa Vaa Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada Vaada Vaa Vaa Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hoohoo Hoo Hoo Hoo Hoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoohoo Hoo Hoo Hoo Hoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoohoo Hoo Hoo Hoo Hoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoohoo Hoo Hoo Hoo Hoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoohoo Hoo Hoo Hoo Hoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoohoo Hoo Hoo Hoo Hoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoohoo Hoo Hoo Hoo Hoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoohoo Hoo Hoo Hoo Hoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pithirla Kaattuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pithirla Kaattuvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Pithirla Kaattuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pithirla Kaattuvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Asiya Thookuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asiya Thookuvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Asiya Thookuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asiya Thookuvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Adchi Kalttuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adchi Kalttuvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Adchi Kalttuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adchi Kalttuvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Bagundai Aakuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bagundai Aakuvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Bagundai Aakuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bagundai Aakuvom"/>
</div>
</pre>
