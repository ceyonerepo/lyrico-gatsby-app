---
title: 'partha mudhal naale song lyrics'
album: 'Vettaiyadu Vilayadu'
artist: 'Harris Jayaraj'
lyricist: 'Thamarai'
director: 'Gowtham Vasudev Menon'
path: '/albums/vettaiyadu-vilayadu-song-lyrics'
song: 'Partha Mudhal naale'
image: ../../images/albumart/vettaiyadu-vilayadu.jpg
date: 2006-08-25
lang: tamil
singers: 
- Bombay Jayasree
- Unnimenon
youtubeLink: "https://www.youtube.com/embed/dEq3-fTJBV0"
type: 'love'
---

<pre class="lyrics-native">
    Coming Soon
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Paartha mudhal naalae unnai paartha mudhal naalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Paartha mudhal naalae unnai paartha mudhal naalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaatchi pizhai polae unarndhen kaatchi pizhai polae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaatchi pizhai polae unarndhen kaatchi pizhai polae"/>
</div>
<div class="lyrico-lyrics-wrapper">Orr alaiyaai vandhu enai adithaai
<input type="checkbox" class="lyrico-select-lyric-line" value="Orr alaiyaai vandhu enai adithaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadalaai maari pin enai izhuthaai
<input type="checkbox" class="lyrico-select-lyric-line" value="Kadalaai maari pin enai izhuthaai"/>
</div>
<div class="lyrico-lyrics-wrapper">En padhaagai thaangiya un mugam un mugam endrum maraiyaadhae
<input type="checkbox" class="lyrico-select-lyric-line" value="En padhaagai thaangiya un mugam un mugam endrum maraiyaadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kaatti kodukkiradhae kannae kaatti kodukkiradhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaatti kodukkiradhae kannae kaatti kodukkiradhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal vazhigiradhae kannil kaadhal vazhigiradhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaadhal vazhigiradhae kannil kaadhal vazhigiradhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un vizhiyil vazhiyum piriyangalai
<input type="checkbox" class="lyrico-select-lyric-line" value="Un vizhiyil vazhiyum piriyangalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarthen kadandhen pagal iravai
<input type="checkbox" class="lyrico-select-lyric-line" value="Paarthen kadandhen pagal iravai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un alaadhi anbinil nanaindha pin nanaindha pin naanum mazhai aanen
<input type="checkbox" class="lyrico-select-lyric-line" value="Un alaadhi anbinil nanaindha pin nanaindha pin naanum mazhai aanen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kaalai ezhundhadhum en kangal mudhalil
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaalai ezhundhadhum en kangal mudhalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedi pidippadhundhan mugamae
<input type="checkbox" class="lyrico-select-lyric-line" value="Thedi pidippadhundhan mugamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thookkam varugaiyil kan paarkkum kadaisi
<input type="checkbox" class="lyrico-select-lyric-line" value="Thookkam varugaiyil kan paarkkum kadaisi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatchikkul nirpadhum un mugamae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaatchikkul nirpadhum un mugamae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Enai pattri enakkae theriyaadha palavum
<input type="checkbox" class="lyrico-select-lyric-line" value="Enai pattri enakkae theriyaadha palavum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee arindhu nadappadhai viyappen
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee arindhu nadappadhai viyappen"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai yedhum ketkaamal unadhaasai anaithum
<input type="checkbox" class="lyrico-select-lyric-line" value="Unai yedhum ketkaamal unadhaasai anaithum"/>
</div>
<div class="lyrico-lyrics-wrapper">Niraivetra vendum endru thavippen
<input type="checkbox" class="lyrico-select-lyric-line" value="Niraivetra vendum endru thavippen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Pogindren ena nee pala nooru muraigal
<input type="checkbox" class="lyrico-select-lyric-line" value="Pogindren ena nee pala nooru muraigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidai pettrum pogaamal iruppaai
<input type="checkbox" class="lyrico-select-lyric-line" value="Vidai pettrum pogaamal iruppaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Sari endru sari endru unai poga cholli
<input type="checkbox" class="lyrico-select-lyric-line" value="Sari endru sari endru unai poga cholli"/>
</div>
 <div class="lyrico-lyrics-wrapper">Kadhavoram naanum nirka sirippaai  (2 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Kadhavoram naanum nirka sirippaai"/></div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kaatti kodukkiradhae kannae kaatti kodukkiradhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaatti kodukkiradhae kannae kaatti kodukkiradhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal vazhigiradhae kannil kaadhal vazhigiradhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaadhal vazhigiradhae kannil kaadhal vazhigiradhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Orr alaiyaai vandhu enai adithaai
<input type="checkbox" class="lyrico-select-lyric-line" value="Orr alaiyaai vandhu enai adithaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadalaai maari pin enai izhuthaai
<input type="checkbox" class="lyrico-select-lyric-line" value="Kadalaai maari pin enai izhuthaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Un alaadhi anbinil nanaindha pin nanaindha pin naanum mazhai aanen
<input type="checkbox" class="lyrico-select-lyric-line" value="Un alaadhi anbinil nanaindha pin nanaindha pin naanum mazhai aanen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Unnai marandhu nee thookkathil sirithaai
<input type="checkbox" class="lyrico-select-lyric-line" value="Unnai marandhu nee thookkathil sirithaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoongaamal adhai kandu rasithen
<input type="checkbox" class="lyrico-select-lyric-line" value="Thoongaamal adhai kandu rasithen"/>
</div>
<div class="lyrico-lyrics-wrapper">Thookkam marandhu naan unai paarkkum kaatchi
<input type="checkbox" class="lyrico-select-lyric-line" value="Thookkam marandhu naan unai paarkkum kaatchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavaaga vandhadhendru ninaithen
<input type="checkbox" class="lyrico-select-lyric-line" value="Kanavaaga vandhadhendru ninaithen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Yaarum maanidarae illaadha idathil
<input type="checkbox" class="lyrico-select-lyric-line" value="Yaarum maanidarae illaadha idathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Siru veedu katti kolla thondrum
<input type="checkbox" class="lyrico-select-lyric-line" value="Siru veedu katti kolla thondrum"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum naanum angae vaazhgindra vaazhvai
<input type="checkbox" class="lyrico-select-lyric-line" value="Neeyum naanum angae vaazhgindra vaazhvai"/>
</div>
<div class="lyrico-lyrics-wrapper">Maram thorum sedhukkida vendum
<input type="checkbox" class="lyrico-select-lyric-line" value="Maram thorum sedhukkida vendum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kan paarthu kadhaikka mudiyaamal naanum
<input type="checkbox" class="lyrico-select-lyric-line" value="Kan paarthu kadhaikka mudiyaamal naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thavikindra oru pennum nee thaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Thavikindra oru pennum nee thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kan kotta mudiyaamal mudiyaamal paarkum
<input type="checkbox" class="lyrico-select-lyric-line" value="Kan kotta mudiyaamal mudiyaamal paarkum"/>
</div>
 <div class="lyrico-lyrics-wrapper">Salikkaadha oru pennum nee thaan  (2 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Salikkaadha oru pennum nee thaan"/></div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Paartha mudhal naalae unnai paartha mudhal naalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Paartha mudhal naalae unnai paartha mudhal naalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatchi pizhai polae unarndhen kaatchi pizhai polae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaatchi pizhai polae unarndhen kaatchi pizhai polae"/>
</div>
<div class="lyrico-lyrics-wrapper">Orr alaiyaai vandhu enai adithaai
<input type="checkbox" class="lyrico-select-lyric-line" value="Orr alaiyaai vandhu enai adithaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadalaai maari pin enai izhuthaai
<input type="checkbox" class="lyrico-select-lyric-line" value="Kadalaai maari pin enai izhuthaai"/>
</div>
<div class="lyrico-lyrics-wrapper">En padhaagai thaangiya un mugam un mugam endrum maraiyaadhae…
<input type="checkbox" class="lyrico-select-lyric-line" value="En padhaagai thaangiya un mugam un mugam endrum maraiyaadhae…"/>
</div>
</pre>