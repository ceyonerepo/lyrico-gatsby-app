---
title: "naan sonnadhum mazhai vandhucha song lyrics"
album: "Mayakkam Enna"
artist: "G.V. Prakash Kumar"
lyricist: "Selvaraghavan"
director: "Selvaraghavan"
path: "/albums/mayakkam-enna-lyrics"
song: "Naan Sonnadhum Mazhai Vandhucha"
image: ../../images/albumart/mayakkam-enna.jpg
date: 2011-11-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/cFnrbQvEuz0"
type: "happy"
singers:
  - Naresh Iyer
  - Saindhavi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Naan Sonnadhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Sonnadhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhai Vanthucha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai Vanthucha"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Sollala Veyil Vandhucha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Sollala Veyil Vandhucha"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Rendumae Idham Thanthucha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Rendumae Idham Thanthucha"/>
</div>
<div class="lyrico-lyrics-wrapper">Muthu Muthu Pechi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthu Muthu Pechi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Kannula Poi Irukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kannula Poi Irukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kannoda Mai Irukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kannoda Mai Irukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Kalliyae Arivirukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Kalliyae Arivirukka"/>
</div>
<div class="lyrico-lyrics-wrapper">En Moochu Ninnu Pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Moochu Ninnu Pochu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaathodu Kaathaga Ullavandhiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathodu Kaathaga Ullavandhiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaattodu Kaadaaga Katti Pottiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattodu Kaadaaga Katti Pottiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Oothaatha Oothellaam Ulla Oothuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oothaatha Oothellaam Ulla Oothuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">En Pechellaam Ninnu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Pechellaam Ninnu "/>
</div>
<div class="lyrico-lyrics-wrapper">Poyi Moolai Suthuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poyi Moolai Suthuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Sonnadhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Sonnadhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhai Vanthucha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai Vanthucha"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Sollala Veyil Vandhucha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Sollala Veyil Vandhucha"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Rendumae Idham Thanthucha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Rendumae Idham Thanthucha"/>
</div>
<div class="lyrico-lyrics-wrapper">Muthu Muthu Pechi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthu Muthu Pechi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karuvaattu Kozhamba Neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuvaattu Kozhamba Neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Rusi Yethura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rusi Yethura"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Vaatti Thinnu Paarkka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Vaatti Thinnu Paarkka"/>
</div>
<div class="lyrico-lyrics-wrapper">Usuppethura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usuppethura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi Podi Podi Podi Potta Mayilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Podi Podi Podi Potta Mayilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Olai Yedhum Vandhucha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Olai Yedhum Vandhucha"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Thookki Poga Thaan Varuven Ninnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Thookki Poga Thaan Varuven Ninnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kili Vandhu Bathil Sollucha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kili Vandhu Bathil Sollucha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karu Naakku Kaara Pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karu Naakku Kaara Pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Karuppetti Nirathu Mulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuppetti Nirathu Mulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Edupatta Nenappu Tholla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edupatta Nenappu Tholla"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Kalavaani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kalavaani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Oh Karuvaattuk Kozhamba Neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Oh Karuvaattuk Kozhamba Neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Rusi Yethura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rusi Yethura"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Vaatti Thinnu Paarkka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Vaatti Thinnu Paarkka"/>
</div>
<div class="lyrico-lyrics-wrapper">Usupethura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usupethura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Sonnadhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Sonnadhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhai Vanthucha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai Vanthucha"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Sollala Veyil Vandhucha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Sollala Veyil Vandhucha"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Rendumae Idham Thanthucha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Rendumae Idham Thanthucha"/>
</div>
<div class="lyrico-lyrics-wrapper">Muthu Muthu Pechi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthu Muthu Pechi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaduaaduaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaduaaduaadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aathaadi Aadu Meikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aathaadi Aadu Meikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Raasa Vanthara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raasa Vanthara"/>
</div>
<div class="lyrico-lyrics-wrapper">Ena Aadu Thinna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ena Aadu Thinna"/>
</div>
<div class="lyrico-lyrics-wrapper">Echi Pulla Meya Vanthara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Echi Pulla Meya Vanthara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi Podi Podi Podi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Podi Podi Podi"/>
</div>
<div class="lyrico-lyrics-wrapper">Mutta Kanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mutta Kanni"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattam Katti Paanjen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattam Katti Paanjen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Kanna Moodi Konjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Kanna Moodi Konjam"/>
</div>
<div class="lyrico-lyrics-wrapper">Saanja Podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saanja Podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavula Thee Midhichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavula Thee Midhichen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannaadi Valaiyal Thaaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaadi Valaiyal Thaaren"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhukku Jimikki Thaaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhukku Jimikki Thaaren"/>
</div>
<div class="lyrico-lyrics-wrapper">Kazhuthukku Thaali Thaaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kazhuthukku Thaali Thaaren"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Variyaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Variyaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Karuvaattu Kozhamba Ah Ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuvaattu Kozhamba Ah Ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum Rusi Yethura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Rusi Yethura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Sonnadhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Sonnadhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhai Vanthucha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai Vanthucha"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Sollala Veyil Vandhucha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Sollala Veyil Vandhucha"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Rendumae Idham Thanthucha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Rendumae Idham Thanthucha"/>
</div>
<div class="lyrico-lyrics-wrapper">Muthu Muthu Pechi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthu Muthu Pechi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Kannula Poi Irukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kannula Poi Irukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kannoda Mai Irukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kannoda Mai Irukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Kalliyae Arivirukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Kalliyae Arivirukka"/>
</div>
<div class="lyrico-lyrics-wrapper">En Moochu Ninnu Pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Moochu Ninnu Pochu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaathodu Kaathaga Ullavandhiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathodu Kaathaga Ullavandhiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaattodu Kaadaaga Katti Pottiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattodu Kaadaaga Katti Pottiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Oothaatha Oothellaam Ulla Oothuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oothaatha Oothellaam Ulla Oothuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">En Pechellaam Ninnu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Pechellaam Ninnu "/>
</div>
<div class="lyrico-lyrics-wrapper">Poyi Moolai Suthuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poyi Moolai Suthuthu"/>
</div>
</pre>
