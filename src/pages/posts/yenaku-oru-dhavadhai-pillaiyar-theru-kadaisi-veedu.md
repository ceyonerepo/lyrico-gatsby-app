---
title: "yenaku oru dhavadhai song lyrics"
album: "Pillaiyar Theru Kadaisi Veedu"
artist: "Chakri"
lyricist: "Snehan"
director: "Thirumalai Kishore"
path: "/albums/pillaiyar-theru-kadaisi-veedu-lyrics"
song: "Yenaku Oru Dhavadhai"
image: ../../images/albumart/pillaiyar-theru-kadaisi-veedu.jpg
date: 2011-06-24
lang: tamil
youtubeLink: "https://www.youtube.com/embed/fo3fXrFy67k"
type: "love"
singers:
  - Hariharan
  - Kousalya
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Enakku Oru Devathaiyai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku Oru Devathaiyai "/>
</div>
<div class="lyrico-lyrics-wrapper">Koduthathu Deivamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koduthathu Deivamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku Oru Deivathaiyae 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku Oru Deivathaiyae "/>
</div>
<div class="lyrico-lyrics-wrapper">Koduthathu Kaathalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koduthathu Kaathalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnudan Vaazhnthidum Naatkal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnudan Vaazhnthidum Naatkal "/>
</div>
<div class="lyrico-lyrics-wrapper">Ullathai Neengidavillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullathai Neengidavillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Innum Oru Jenmathin Maelae 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum Oru Jenmathin Maelae "/>
</div>
<div class="lyrico-lyrics-wrapper">Nambikkai Vanthidavillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nambikkai Vanthidavillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Unthan Aruginilthaan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unthan Aruginilthaan "/>
</div>
<div class="lyrico-lyrics-wrapper">En Uyirum Ulavuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Uyirum Ulavuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unthan Madiyilthaan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unthan Madiyilthaan "/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir Thuratha Thodanguthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Thuratha Thodanguthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakku Oru Devathaiyai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku Oru Devathaiyai "/>
</div>
<div class="lyrico-lyrics-wrapper">Koduthathu Deivamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koduthathu Deivamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku Oru Deivathaiyae 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku Oru Deivathaiyae "/>
</div>
<div class="lyrico-lyrics-wrapper">Koduthathu Kaathalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koduthathu Kaathalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Mattum Naan Kaanum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Mattum Naan Kaanum "/>
</div>
<div class="lyrico-lyrics-wrapper">Munnae Ulagam Illaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munnae Ulagam Illaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kandavudan Unnil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandavudan Unnil "/>
</div>
<div class="lyrico-lyrics-wrapper">Thulaindhenemo Kaanavillaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thulaindhenemo Kaanavillaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanavil Vannangalaai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanavil Vannangalaai "/>
</div>
<div class="lyrico-lyrics-wrapper">Ennullae Vanthaayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennullae Vanthaayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthu Nizhalaikooda 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthu Nizhalaikooda "/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Ezhu Varnamaakkinaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Ezhu Varnamaakkinaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Nee Kaetkaamal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Nee Kaetkaamal "/>
</div>
<div class="lyrico-lyrics-wrapper">Ullathai Koithaayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullathai Koithaayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjai Thirudi Thirudithaan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjai Thirudi Thirudithaan "/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathal Thenai Suvaikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathal Thenai Suvaikkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Enthan Sammatham Varum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enthan Sammatham Varum "/>
</div>
<div class="lyrico-lyrics-wrapper">Munne Nee Munthidakkoodaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munne Nee Munthidakkoodaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Varudi Nerudi Thinnaal Køøda 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varudi Nerudi Thinnaal Køøda "/>
</div>
<div class="lyrico-lyrics-wrapper">Tigatta Villaiyae Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tigatta Villaiyae Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
È<div class="lyrico-lyrics-wrapper">nakku Oru Deivathaiyae 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nakku Oru Deivathaiyae "/>
</div>
<div class="lyrico-lyrics-wrapper">Køduthathu Kaathalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Køduthathu Kaathalae"/>
</div>
È<div class="lyrico-lyrics-wrapper">nakku Oru Devathaiyai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nakku Oru Devathaiyai "/>
</div>
<div class="lyrico-lyrics-wrapper">Køduthathu Deivamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Køduthathu Deivamae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idi Mazhaiyø Èrimalaiyø Thaangiduvøamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idi Mazhaiyø Èrimalaiyø Thaangiduvøamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Yugamø Šila Nødiyø Vaazhnthiduvøamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Yugamø Šila Nødiyø Vaazhnthiduvøamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pennena Man Maelae 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pennena Man Maelae "/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthathu Unnaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthathu Unnaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unthan Šumaiyai Šumakkathaan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unthan Šumaiyai Šumakkathaan "/>
</div>
<div class="lyrico-lyrics-wrapper">Intha Mannaai Maarinen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Mannaai Maarinen"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaiye Èn Vaazhvil Annaiyai Kendene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaiye Èn Vaazhvil Annaiyai Kendene"/>
</div>
<div class="lyrico-lyrics-wrapper">Chella Køzhanthai Pølathaan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chella Køzhanthai Pølathaan "/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil Vaithu Paarkkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil Vaithu Paarkkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Pakkam Iruppathanaal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Pakkam Iruppathanaal "/>
</div>
<div class="lyrico-lyrics-wrapper">Antha Šørkkamum Kasakkuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Šørkkamum Kasakkuthey"/>
</div>
<div class="lyrico-lyrics-wrapper">Nizhalum Unnai Pirinthidalaam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nizhalum Unnai Pirinthidalaam "/>
</div>
<div class="lyrico-lyrics-wrapper">Inainthirupaen Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inainthirupaen Naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
È<div class="lyrico-lyrics-wrapper">nakku Oru Devathaiyai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nakku Oru Devathaiyai "/>
</div>
<div class="lyrico-lyrics-wrapper">Køduthathu Deivamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Køduthathu Deivamae"/>
</div>
È<div class="lyrico-lyrics-wrapper">nakku Oru Deivathaiyae 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nakku Oru Deivathaiyae "/>
</div>
<div class="lyrico-lyrics-wrapper">Køduthathu Kaathalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Køduthathu Kaathalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnudan Vaazhnthidum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnudan Vaazhnthidum "/>
</div>
<div class="lyrico-lyrics-wrapper">Naatkal Ullathai Neengidavillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naatkal Ullathai Neengidavillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Innum Oru Jenmathin Maelae 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum Oru Jenmathin Maelae "/>
</div>
<div class="lyrico-lyrics-wrapper">Nambikkai Vanthidavillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nambikkai Vanthidavillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Unthan Aruginilthaan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unthan Aruginilthaan "/>
</div>
È<div class="lyrico-lyrics-wrapper">n Uyirum Ulavuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="n Uyirum Ulavuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unthan Madiyilthaan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unthan Madiyilthaan "/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir Thuratha Thødanguthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Thuratha Thødanguthu"/>
</div>
È<div class="lyrico-lyrics-wrapper">nakku Oru Devathaiyai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nakku Oru Devathaiyai "/>
</div>
<div class="lyrico-lyrics-wrapper">Køduthathu Deivamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Køduthathu Deivamae"/>
</div>
È<div class="lyrico-lyrics-wrapper">nakku Oru Deivathaiyae 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nakku Oru Deivathaiyae "/>
</div>
<div class="lyrico-lyrics-wrapper">Køduthathu Kaathalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Køduthathu Kaathalae"/>
</div>
</pre>
