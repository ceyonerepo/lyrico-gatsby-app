---
title: "Mere Jaan song lyrics"
album: "Sardar"
artist: "G. V. Prakash Kumar"
lyricist: "Yugabharathi"
director: "P.S Mithran"
path: "/albums/sardar-lyrics"
song: "Mere Jaan"
image: ../../images/albumart/sardar.jpg
date: 2022-10-24
lang: tamil
youtubeLink: "https://www.youtube.com/embed/6DZv2YAsZsg?controls=0"
type: "Love"
singers:
  - Nakash Aziz
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Jaan mere jaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaan mere jaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanananaa naananaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanananaa naananaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaan mere jaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaan mere jaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanananaa….
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanananaa…."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Mere jaan mere jaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mere jaan mere jaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee thalli nikkaathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thalli nikkaathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mere piyaar mere dil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mere piyaar mere dil"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kenja vaikkaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kenja vaikkaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Mere jaan mere jaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mere jaan mere jaan"/>
</div>
<div class="lyrico-lyrics-wrapper">En nenjila thaikkaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En nenjila thaikkaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Mere piyaar mere dil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mere piyaar mere dil"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna vittuttu pogaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna vittuttu pogaatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Unna paathaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna paathaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pookkum poonthottam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pookkum poonthottam"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna nee senja pennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna nee senja pennae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Dhoora ninnaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhoora ninnaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Moochil un vaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moochil un vaasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Bodha nee thantha kannae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bodha nee thantha kannae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ennadi adi irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennadi adi irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkum enakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkum enakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavula nee vanthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavula nee vanthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaartha sikkikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaartha sikkikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ye kannoda kanna paartha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye kannoda kanna paartha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal solla naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal solla naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathirupen kannitheevu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathirupen kannitheevu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadha pola thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadha pola thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Neeyum vanthaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum vanthaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakkulla thagaraaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkulla thagaraaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasa yethetho seiya vacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasa yethetho seiya vacha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kaalam kalikaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam kalikaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan romba paavandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan romba paavandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kizhichu enna thacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kizhichu enna thacha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ennadi adi irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennadi adi irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkum enakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkum enakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavula nee vanthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavula nee vanthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaartha sikkikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaartha sikkikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Mere jaan….
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mere jaan…."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Mere jaan mere jaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mere jaan mere jaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna thallividaathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna thallividaathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mathuva pothuva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mathuva pothuva"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna suththa vidaathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna suththa vidaathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Mere jaan mere jaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mere jaan mere jaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna konnu podaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna konnu podaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhaga pathama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhaga pathama"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna senjuttu pogaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna senjuttu pogaatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Unna paathaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna paathaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pookkum poonthottam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pookkum poonthottam"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna nee senja pennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna nee senja pennae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Dhoora ninnaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhoora ninnaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Moochil un vaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moochil un vaasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Bodha nee thantha kannae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bodha nee thantha kannae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ennadi adi irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennadi adi irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkum enakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkum enakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavula nee vanthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavula nee vanthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaartha sikkikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaartha sikkikkum"/>
</div>
</pre>
