---
title: "paala pitta song lyrics"
album: "Maharshi"
artist: "Devi Sri Prasad"
lyricist: "Sri Mani"
director: "Vamshi Paidipally"
path: "/albums/maharshi-lyrics"
song: "Paala Pitta"
image: ../../images/albumart/maharshi.jpg
date: 2019-05-09
lang: telugu
youtubeLink: "https://www.youtube.com/embed/g-o2S_Ds3l8"
type: "love"
singers:
  - Rahul Sipligunj
  - M.M. Manasi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Evo gusagusale naalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evo gusagusale naalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Valase vidisi valapey virisey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valase vidisi valapey virisey"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedaloo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedaloo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paala pittalo valapu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paala pittalo valapu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee paita mettupai valindhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee paita mettupai valindhey"/>
</div>
<div class="lyrico-lyrics-wrapper">Poola puttalo merupu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poola puttalo merupu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kattu pattulo doorindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kattu pattulo doorindhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Thene pattula nee pilupe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thene pattula nee pilupe"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanu katti padesindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanu katti padesindhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pilla naa gundelona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pilla naa gundelona"/>
</div>
<div class="lyrico-lyrics-wrapper">Ille kattesinaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ille kattesinaave"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallaapu jalli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallaapu jalli"/>
</div>
<div class="lyrico-lyrics-wrapper">Rangu mugge pettesinaavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangu mugge pettesinaavey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kodavalanchulo merupu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodavalanchulo merupu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee churuku choopulo cherindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee churuku choopulo cherindhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Gadapakaddhina pasupu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gadapakaddhina pasupu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee chilipi muddhula thakindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee chilipi muddhula thakindhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Malupu thirigi naa manasitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malupu thirigi naa manasitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee vaipuki mallindhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee vaipuki mallindhey"/>
</div>
<div class="lyrico-lyrics-wrapper">Pilloda gundelona ille kattesinave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pilloda gundelona ille kattesinave"/>
</div>
<div class="lyrico-lyrics-wrapper">Innaalla siggulanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innaalla siggulanni"/>
</div>
<div class="lyrico-lyrics-wrapper">Yella gottesinavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yella gottesinavey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Villu laanti nee vollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Villu laanti nee vollu"/>
</div>
<div class="lyrico-lyrics-wrapper">Visuruthuntey baanaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Visuruthuntey baanaalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaddi parakapai aggi pullalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaddi parakapai aggi pullalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhaggumannavey naa kallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhaggumannavey naa kallu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee maataloni rojaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee maataloni rojaalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gucchuthunte mari mullu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gucchuthunte mari mullu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nippu pettina thene pattula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nippu pettina thene pattula"/>
</div>
<div class="lyrico-lyrics-wrapper">Nidhra pattadhey raathrullu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nidhra pattadhey raathrullu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee nadumu choosthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee nadumu choosthey"/>
</div>
<div class="lyrico-lyrics-wrapper">Malle theega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malle theega"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa manasu danini alley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa manasu danini alley"/>
</div>
<div class="lyrico-lyrics-wrapper">Thooneega…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thooneega…"/>
</div>
<div class="lyrico-lyrics-wrapper">Mella mellaga challinavuga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mella mellaga challinavuga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottha kalalu baagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottha kalalu baagaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pilla naa gundelona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pilla naa gundelona"/>
</div>
<div class="lyrico-lyrics-wrapper">Ille kattesinaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ille kattesinaave"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallaapu jalli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallaapu jalli"/>
</div>
<div class="lyrico-lyrics-wrapper">Rangu mugge pettesinaavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangu mugge pettesinaavey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paala pittalo valapu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paala pittalo valapu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee paita mettupai valindhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee paita mettupai valindhey"/>
</div>
<div class="lyrico-lyrics-wrapper">Poola puttalo merupu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poola puttalo merupu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kattu pattulo doorindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kattu pattulo doorindhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Thene pattula nee pilupe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thene pattula nee pilupe"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanu katti padesindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanu katti padesindhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pilloda gundelona ille kattesinave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pilloda gundelona ille kattesinave"/>
</div>
<div class="lyrico-lyrics-wrapper">Innaalla siggulanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innaalla siggulanni"/>
</div>
<div class="lyrico-lyrics-wrapper">Yella gottesinavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yella gottesinavey"/>
</div>
</pre>
