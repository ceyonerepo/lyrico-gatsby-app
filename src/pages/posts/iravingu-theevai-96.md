---
title: "iravingu theevai song lyrics"
album: "96"
artist: "Govind Vasantha"
lyricist: "Uma Devi"
director: "C Premkumar"
path: "/albums/96-lyrics"
song: "Iravingu Theevai"
image: ../../images/albumart/96.jpg
date: 2018-10-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/b3-lyX9O6kY"
type: "melody"
singers:
  - Chinmayi
  - Pradeep Kumar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Iravingu theevai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravingu theevai"/>
</div>
<div class="lyrico-lyrics-wrapper">Namai chooluthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namai chooluthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidiyalum irulaai varuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidiyalum irulaai varuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninaivugal theeyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaivugal theeyai"/>
</div>
<div class="lyrico-lyrics-wrapper">Alai mothuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alai mothuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Udalingu saavaai azhuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udalingu saavaai azhuthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pirivae uruvaai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirivae uruvaai "/>
</div>
<div class="lyrico-lyrics-wrapper">karainthu pogiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karainthu pogiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirin uyirai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirin uyirai "/>
</div>
<div class="lyrico-lyrics-wrapper">pirinthu pogiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pirinthu pogiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Malaigalin nadhipol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malaigalin nadhipol"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam vazhinthu vanthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam vazhinthu vanthaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Varundidum nilathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varundidum nilathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Pala kadalgal thanthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala kadalgal thanthaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavae thunaiyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavae thunaiyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ozhinthu pogattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ozhinthu pogattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iravingu theevai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravingu theevai"/>
</div>
<div class="lyrico-lyrics-wrapper">Namai chooluthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namai chooluthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidiyalum irulaai varuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidiyalum irulaai varuthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Intha thamarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha thamarai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kulam meeri 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulam meeri "/>
</div>
<div class="lyrico-lyrics-wrapper">thani aaguthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thani aaguthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Athan sooriyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athan sooriyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagal indri 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagal indri "/>
</div>
<div class="lyrico-lyrics-wrapper">veyil kaayuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veyil kaayuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru padhayilIru jeevan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru padhayilIru jeevan "/>
</div>
<div class="lyrico-lyrics-wrapper">thunai theduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thunai theduthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada kaalangal thadai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada kaalangal thadai"/>
</div>
<div class="lyrico-lyrics-wrapper">Meeri thadai poduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meeri thadai poduthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee indri naanae dhinam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee indri naanae dhinam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhvathoru vazhva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvathoru vazhva"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhvae vaa nee thaan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhvae vaa nee thaan "/>
</div>
<div class="lyrico-lyrics-wrapper">uyirin uyirae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyirin uyirae"/>
</div>
<div class="lyrico-lyrics-wrapper">Varavaa varavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varavaa varavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thinam thinam uyirthezhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thinam thinam uyirthezhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam andraadam maayumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam andraadam maayumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir varai nirainthunai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir varai nirainthunai"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam kondaadi vazhumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam kondaadi vazhumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Marangal saainthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marangal saainthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Koodu veezhnthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koodu veezhnthum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuyilgal raagam paadumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuyilgal raagam paadumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Iravu theernthu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravu theernthu "/>
</div>
<div class="lyrico-lyrics-wrapper">ointha podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ointha podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilavu porumai kaakkumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilavu porumai kaakkumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mazhai vazhi kadal vidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai vazhi kadal vidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinkadhal mannai cherummae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinkadhal mannai cherummae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai udal pirinthinum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai udal pirinthinum"/>
</div>
<div class="lyrico-lyrics-wrapper">En kadhal unnai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kadhal unnai"/>
</div>
<div class="lyrico-lyrics-wrapper">Sernthu vaazhumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sernthu vaazhumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee poi vaa vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee poi vaa vaa vaa"/>
</div>
</pre>
