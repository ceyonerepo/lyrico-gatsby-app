---
title: "yaarum ellai song lyrics"
album: "Echcharikkai"
artist: "Sundaramurthy KS"
lyricist: "Kabilan"
director: "Sarjun KM"
path: "/albums/echcharikkai-lyrics"
song: "Yaarum Ellai"
image: ../../images/albumart/echcharikkai.jpg
date: 2018-08-24
lang: tamil
youtubeLink: "https://www.youtube.com/embed/63-LXxe-DYc"
type: "love"
singers:
  - Sathya Prakash
  - Jananie SV
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ezhezhu vanna poovai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhezhu vanna poovai"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarkkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarkkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethir paarkkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethir paarkkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan pattam pochi pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan pattam pochi pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Maarinen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maarinen"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadu maarinen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadu maarinen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Imaiyo oviyam pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imaiyo oviyam pol"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai varaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai varaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal thoorigai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal thoorigai"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirae ullam ennum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirae ullam ennum"/>
</div>
<div class="lyrico-lyrics-wrapper">Veetil nuzhaindhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veetil nuzhaindhaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Illai vaadagai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illai vaadagai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaarum illai ennodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarum illai ennodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyae endhan kannodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyae endhan kannodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thevai yaavum unnodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thevai yaavum unnodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thernthedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thernthedu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaarum illai ennodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarum illai ennodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyae endhan kannodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyae endhan kannodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thevai yaavum unnodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thevai yaavum unnodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thernthedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thernthedu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haaaaaaaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haaaaaaaaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Haaaaaaaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haaaaaaaaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Haaaaaaaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haaaaaaaaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Haa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa aaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee kalaigalin kaadhali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kalaigalin kaadhali"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan undhan kaadhal vaasagan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan undhan kaadhal vaasagan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee thanimaiyin maanudan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thanimaiyin maanudan"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan undhan kaadhal kottai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan undhan kaadhal kottai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pazhagiya paravaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pazhagiya paravaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Siragugalaal ennai anaithaaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siragugalaal ennai anaithaaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayathil valarpiriyaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayathil valarpiriyaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ival manathil nee mulaithaaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ival manathil nee mulaithaaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ohhoooooo oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohhoooooo oo"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo ooooooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo ooooooooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaarum illai ennodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarum illai ennodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyae endhan kannodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyae endhan kannodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thevai yaavum unnodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thevai yaavum unnodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thernthedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thernthedu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaarum illai ennodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarum illai ennodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyae endhan kannodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyae endhan kannodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thevai yaavum unnodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thevai yaavum unnodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thernthedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thernthedu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh ezhezhu vanna poovai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh ezhezhu vanna poovai"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarkkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarkkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethir paarkkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethir paarkkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan pattam pochi pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan pattam pochi pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Maarinen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maarinen"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadu maarinen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadu maarinen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Imaiyo oviyam pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imaiyo oviyam pol"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai varaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai varaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal thoorigai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal thoorigai"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirae ullam ennum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirae ullam ennum"/>
</div>
<div class="lyrico-lyrics-wrapper">Veetil nuzhaindhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veetil nuzhaindhaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Illai vaadagai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illai vaadagai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaarum illai ennodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarum illai ennodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyae endhan kannodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyae endhan kannodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thevai yaavum unnodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thevai yaavum unnodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thernthedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thernthedu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaarum illai ennodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarum illai ennodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyae endhan kannodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyae endhan kannodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thevai yaavum unnodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thevai yaavum unnodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thernthedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thernthedu"/>
</div>
</pre>
