---
title: "kanaka mahalakshmi song lyrics"
album: "Bangaru Bullodu"
artist: "Sai Karthik"
lyricist: "Ramajogayya Sastry"
director: "P. V. Giri"
path: "/albums/bangaru-bullodu-lyrics"
song: "Kanaka Mahalakshmi"
image: ../../images/albumart/bangaru-bullodu.jpg
date: 2021-01-23
lang: telugu
youtubeLink: "https://www.youtube.com/embed/S_nfAGhH2CM"
type: "love"
singers:
  - Dattu
  - M L Gayathri
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hey Nenemo Patasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Nenemo Patasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvemo Machisu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvemo Machisu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuv Nenu Jantaithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuv Nenu Jantaithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Brekai Pothaadhi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Brekai Pothaadhi "/>
</div>
<div class="lyrico-lyrics-wrapper">Silence-U Silence-U Silence-U
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silence-U Silence-U Silence-U"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhoogolam Thappuddhe Balance-U
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhoogolam Thappuddhe Balance-U"/>
</div>
<div class="lyrico-lyrics-wrapper">Neenneeku Goggles-U
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neenneeku Goggles-U"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuv Naaku Bangles-U
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuv Naaku Bangles-U"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana Iddhari Jodiki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana Iddhari Jodiki"/>
</div>
<div class="lyrico-lyrics-wrapper">Shockai Pothaaru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shockai Pothaaru "/>
</div>
<div class="lyrico-lyrics-wrapper">Couples-U Couples-U Couples-U
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Couples-U Couples-U Couples-U"/>
</div>
<div class="lyrico-lyrics-wrapper">Aakaasham Thaakelaa Whistles-U
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aakaasham Thaakelaa Whistles-U"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chalo Rai Rai Rai Rai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chalo Rai Rai Rai Rai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vachhindhi License-U
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vachhindhi License-U"/>
</div>
<div class="lyrico-lyrics-wrapper">Okataipoyelaa Chikkindhi Chance-U
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okataipoyelaa Chikkindhi Chance-U"/>
</div>
<div class="lyrico-lyrics-wrapper">Thai Thai Thai Thai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thai Thai Thai Thai"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaallo Thele Dreams-U
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaallo Thele Dreams-U"/>
</div>
<div class="lyrico-lyrics-wrapper">Ika Choosko Neelo Naalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ika Choosko Neelo Naalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Happy Feelings-U
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Happy Feelings-U"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oye Kanaka Mahalakshmi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oye Kanaka Mahalakshmi"/>
</div>
<div class="lyrico-lyrics-wrapper">Boddu Kanaka Mahalakshmi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boddu Kanaka Mahalakshmi"/>
</div>
<div class="lyrico-lyrics-wrapper">Life Anthaa Neetho Untaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life Anthaa Neetho Untaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Andhamtho Attach Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Andhamtho Attach Me"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanaka Mahalakshmi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanaka Mahalakshmi"/>
</div>
<div class="lyrico-lyrics-wrapper">Boddu Kanaka Mahalakshmi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boddu Kanaka Mahalakshmi"/>
</div>
<div class="lyrico-lyrics-wrapper">Rojo Rojaa Isthaa Neeku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rojo Rojaa Isthaa Neeku"/>
</div>
<div class="lyrico-lyrics-wrapper">Romance Gatraa Teach Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romance Gatraa Teach Me"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Nenemo Patasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Nenemo Patasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvemo Machisu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvemo Machisu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuv Nenu Jantaithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuv Nenu Jantaithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Brekai Pothaadhi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Brekai Pothaadhi "/>
</div>
<div class="lyrico-lyrics-wrapper">Silence-U Silence-U Silence-U
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silence-U Silence-U Silence-U"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhoogolam Thappuddhe Balance-U
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhoogolam Thappuddhe Balance-U"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Attaa Ettaa Pudithive
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Attaa Ettaa Pudithive"/>
</div>
<div class="lyrico-lyrics-wrapper">Bubbly Barbie Bommalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bubbly Barbie Bommalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Settle-Ayipothive
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Settle-Ayipothive"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhil Mobile Simmulaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhil Mobile Simmulaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheppamante Kashtame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheppamante Kashtame"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Andhaala Formula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Andhaala Formula"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheyyi Patti Eluko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheyyi Patti Eluko"/>
</div>
<div class="lyrico-lyrics-wrapper">Two In One Scheme-U Laa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Two In One Scheme-U Laa"/>
</div>
<div class="lyrico-lyrics-wrapper">Left-U Right-U Thalathalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Left-U Right-U Thalathalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Front-U Back-U Galagalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Front-U Back-U Galagalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayyo Baabu Ye Angle 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyo Baabu Ye Angle "/>
</div>
<div class="lyrico-lyrics-wrapper">Lo Ninne Choodaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lo Ninne Choodaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasha Padaa Konte Kala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasha Padaa Konte Kala"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha Dhooramendhukalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Dhooramendhukalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gundepai Nucklessulaa Pettesukuntaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundepai Nucklessulaa Pettesukuntaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oye Kanaka Mahalakshmi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oye Kanaka Mahalakshmi"/>
</div>
<div class="lyrico-lyrics-wrapper">Boddu Kanaka Mahalakshmi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boddu Kanaka Mahalakshmi"/>
</div>
<div class="lyrico-lyrics-wrapper">Life Anthaa Neetho Untaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life Anthaa Neetho Untaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Andhamtho Attach Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Andhamtho Attach Me"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanaka Mahalakshmi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanaka Mahalakshmi"/>
</div>
<div class="lyrico-lyrics-wrapper">Boddu Kanaka Mahalakshmi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boddu Kanaka Mahalakshmi"/>
</div>
<div class="lyrico-lyrics-wrapper">Rojo Rojaa Isthaa Neeku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rojo Rojaa Isthaa Neeku"/>
</div>
<div class="lyrico-lyrics-wrapper">Romance Gatraa Teach Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romance Gatraa Teach Me"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Nenemo Patasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Nenemo Patasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvemo Machisu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvemo Machisu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuv Nenu Jantaithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuv Nenu Jantaithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Brekai Pothaadhi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Brekai Pothaadhi "/>
</div>
<div class="lyrico-lyrics-wrapper">Silence-U Silence-U Silence-U
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silence-U Silence-U Silence-U"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhoogolam Thappuddhe Balance-U
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhoogolam Thappuddhe Balance-U"/>
</div>
</pre>
