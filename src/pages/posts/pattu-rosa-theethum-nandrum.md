---
title: "pattu rosa song lyrics"
album: "Theethum Nandrum"
artist: "C. Sathya"
lyricist: "Muthamizh"
director: "Rasu Ranjith"
path: "/albums/theethum-nandrum-lyrics"
song: "Pattu Rosa"
image: ../../images/albumart/theethum-nandrum.jpg
date: 2021-03-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Ri5SGeYIp7M"
type: "Love"
singers:
  - C.Sathya
  - Aparna Balamurali
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">epadi pathalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="epadi pathalum"/>
</div>
<div class="lyrico-lyrics-wrapper">alaga iruka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alaga iruka"/>
</div>
<div class="lyrico-lyrics-wrapper">ennadi seiya nan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennadi seiya nan"/>
</div>
<div class="lyrico-lyrics-wrapper">pidikuthu kirukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pidikuthu kirukka"/>
</div>
<div class="lyrico-lyrics-wrapper">eh epadi pathalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eh epadi pathalum"/>
</div>
<div class="lyrico-lyrics-wrapper">alaga iruka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alaga iruka"/>
</div>
<div class="lyrico-lyrics-wrapper">ennadi seiya nan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennadi seiya nan"/>
</div>
<div class="lyrico-lyrics-wrapper">pidikuthu kirukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pidikuthu kirukka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kothivita manasa nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kothivita manasa nee"/>
</div>
<div class="lyrico-lyrics-wrapper">pichu pichu eraika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pichu pichu eraika"/>
</div>
<div class="lyrico-lyrics-wrapper">kothadimai pola nanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kothadimai pola nanum"/>
</div>
<div class="lyrico-lyrics-wrapper">matikiten muluka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="matikiten muluka"/>
</div>
<div class="lyrico-lyrics-wrapper">mulika ah muluka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mulika ah muluka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pattu rosa unna pathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pattu rosa unna pathu"/>
</div>
<div class="lyrico-lyrics-wrapper">kettu pochu manasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kettu pochu manasu"/>
</div>
<div class="lyrico-lyrics-wrapper">thottu pakka solli enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thottu pakka solli enna"/>
</div>
<div class="lyrico-lyrics-wrapper">kenjuthadi vayasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kenjuthadi vayasu"/>
</div>
<div class="lyrico-lyrics-wrapper">sotaiyaga sundi ponen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sotaiyaga sundi ponen"/>
</div>
<div class="lyrico-lyrics-wrapper">en paathaiyil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en paathaiyil "/>
</div>
<div class="lyrico-lyrics-wrapper">sukavachu vanthutaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sukavachu vanthutaye"/>
</div>
<div class="lyrico-lyrics-wrapper">un paavaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un paavaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">sotaiyaga sundi ponen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sotaiyaga sundi ponen"/>
</div>
<div class="lyrico-lyrics-wrapper">en paathaiyil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en paathaiyil "/>
</div>
<div class="lyrico-lyrics-wrapper">sukavachu vanthutaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sukavachu vanthutaye"/>
</div>
<div class="lyrico-lyrics-wrapper">un paavaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un paavaiyil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">epadi pathalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="epadi pathalum"/>
</div>
<div class="lyrico-lyrics-wrapper">epadi pathalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="epadi pathalum"/>
</div>
<div class="lyrico-lyrics-wrapper">epadi pathalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="epadi pathalum"/>
</div>
<div class="lyrico-lyrics-wrapper">alaga iruka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alaga iruka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">unna patha piragu naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna patha piragu naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">enna maranthene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna maranthene"/>
</div>
<div class="lyrico-lyrics-wrapper">angayum ingayum suthi thirunjen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="angayum ingayum suthi thirunjen"/>
</div>
<div class="lyrico-lyrics-wrapper">enna kaatu manthiram potta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna kaatu manthiram potta"/>
</div>
<div class="lyrico-lyrics-wrapper">puthiya than theeta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthiya than theeta"/>
</div>
<div class="lyrico-lyrics-wrapper">konjama konjama sitha therunja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konjama konjama sitha therunja"/>
</div>
<div class="lyrico-lyrics-wrapper">kodukku kanna vachu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kodukku kanna vachu "/>
</div>
<div class="lyrico-lyrics-wrapper">kuthi kuthi enna konna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuthi kuthi enna konna"/>
</div>
<div class="lyrico-lyrics-wrapper">kodaiya mathi putta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kodaiya mathi putta"/>
</div>
<div class="lyrico-lyrics-wrapper">kaiya katti summa ninen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaiya katti summa ninen"/>
</div>
<div class="lyrico-lyrics-wrapper">thenodu sakkaraya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thenodu sakkaraya"/>
</div>
<div class="lyrico-lyrics-wrapper">thedithan unna kanden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thedithan unna kanden"/>
</div>
<div class="lyrico-lyrics-wrapper">ora kanna kaati puta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ora kanna kaati puta"/>
</div>
<div class="lyrico-lyrics-wrapper">uchanthala pathikichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uchanthala pathikichu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">epadi pathalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="epadi pathalum"/>
</div>
<div class="lyrico-lyrics-wrapper">alaga iruka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alaga iruka"/>
</div>
<div class="lyrico-lyrics-wrapper">ennadi seiya nan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennadi seiya nan"/>
</div>
<div class="lyrico-lyrics-wrapper">pidikuthu kirukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pidikuthu kirukka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pattu rosa unna pathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pattu rosa unna pathu"/>
</div>
<div class="lyrico-lyrics-wrapper">kettu pochu manasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kettu pochu manasu"/>
</div>
<div class="lyrico-lyrics-wrapper">thottu pakka solli enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thottu pakka solli enna"/>
</div>
<div class="lyrico-lyrics-wrapper">kenjuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kenjuthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">kenjuthadi vayasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kenjuthadi vayasu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kadhalula kanavulaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhalula kanavulaga"/>
</div>
<div class="lyrico-lyrics-wrapper">naanum pakuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naanum pakuren"/>
</div>
<div class="lyrico-lyrics-wrapper">kaadhukulla ragasiyama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadhukulla ragasiyama"/>
</div>
<div class="lyrico-lyrics-wrapper">etho pesuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="etho pesuren"/>
</div>
<div class="lyrico-lyrics-wrapper">meda pottu mega mootam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meda pottu mega mootam"/>
</div>
<div class="lyrico-lyrics-wrapper">mela kooduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mela kooduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">aasa ellam en manathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aasa ellam en manathil"/>
</div>
<div class="lyrico-lyrics-wrapper">aatam aaduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aatam aaduthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pattu rosa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pattu rosa"/>
</div>
<div class="lyrico-lyrics-wrapper">rosa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rosa"/>
</div>
<div class="lyrico-lyrics-wrapper">pattu rosa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pattu rosa"/>
</div>
<div class="lyrico-lyrics-wrapper">rosa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rosa"/>
</div>
</pre>
