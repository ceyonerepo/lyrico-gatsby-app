---
title: "ariyathariyathe song lyrics"
album: "Ayyappanum Koshiyum"
artist: "Jakes Bejoy"
lyricist: "Rafeeq Ahmed"
director: "Sachy"
path: "/albums/ayyappanum-koshiyum-lyrics"
song: "Ariyathariyathe"
image: ../../images/albumart/ayyappanum-koshiyum.jpg
date: 2020-02-07
lang: malayalam
youtubeLink: "https://www.youtube.com/embed/mkqv_4crcXc"
type: "melody"
singers:
  - Kottakkal Madhu
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ariyaathariyaathariyaanerathurul Pottum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ariyaathariyaathariyaanerathurul Pottum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaaladiyil Njodiyil Thiriyaa Neram Chira Pottum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaladiyil Njodiyil Thiriyaa Neram Chira Pottum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ariyaathariyaathariyaanerathurul Pottum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ariyaathariyaathariyaanerathurul Pottum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaaladiyil Njodiyil Thiriyaa Neram Chira Pottum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaladiyil Njodiyil Thiriyaa Neram Chira Pottum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhiye Perumalayaay Maarum Maaraalakal Polum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhiye Perumalayaay Maarum Maaraalakal Polum"/>
</div>
<div class="lyrico-lyrics-wrapper">Idivaal Muna Shila Vetti Pilarum Pilarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idivaal Muna Shila Vetti Pilarum Pilarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Innalevareyithu Mundaka Vayalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innalevareyithu Mundaka Vayalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Innu Niranju Kavinjoru Kadalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innu Niranju Kavinjoru Kadalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannil Kathum Iruttil Nammude Swantham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannil Kathum Iruttil Nammude Swantham"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhiyum Thodiyum Kalavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhiyum Thodiyum Kalavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Innalevareyithu Mundaka Vayalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innalevareyithu Mundaka Vayalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Innu Niranju Kavinjoru Kadalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innu Niranju Kavinjoru Kadalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannil Kathum Iruttil Nammude Swantham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannil Kathum Iruttil Nammude Swantham"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhiyum Thodiyum Kalavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhiyum Thodiyum Kalavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ariyaathariyaathariyaanerathurul Pottum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ariyaathariyaathariyaanerathurul Pottum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaaladiyil Njodiyil Thiriyaa Neram Chira Pottum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaladiyil Njodiyil Thiriyaa Neram Chira Pottum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thekkemala Ponmala Pole Pettennoru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thekkemala Ponmala Pole Pettennoru"/>
</div>
<div class="lyrico-lyrics-wrapper">Vithu Mulache Kochommal Pachilanaambil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vithu Mulache Kochommal Pachilanaambil"/>
</div>
<div class="lyrico-lyrics-wrapper">Uchakkum Nanavu Podiche Kacholam Natta Valappil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uchakkum Nanavu Podiche Kacholam Natta Valappil"/>
</div>
<div class="lyrico-lyrics-wrapper">Chikkannoru Valli Chenache
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chikkannoru Valli Chenache"/>
</div>
<div class="lyrico-lyrics-wrapper">Ottonnu Velukkum Mumbe Pada Padarnne Pada Padarnne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ottonnu Velukkum Mumbe Pada Padarnne Pada Padarnne"/>
</div>
<div class="lyrico-lyrics-wrapper">Innalevareyithu Mundaka Vayalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innalevareyithu Mundaka Vayalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Innu Niranju Kavinjoru Kadalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innu Niranju Kavinjoru Kadalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannil Kathum Iruttil Nammude Swantham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannil Kathum Iruttil Nammude Swantham"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhiyum Thodiyum Kalavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhiyum Thodiyum Kalavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ottakku Mulachathu Valli Thuzhalee Valli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ottakku Mulachathu Valli Thuzhalee Valli"/>
</div>
<div class="lyrico-lyrics-wrapper">Chuttolam Mullukalloru Kurukkan Vallee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chuttolam Mullukalloru Kurukkan Vallee"/>
</div>
<div class="lyrico-lyrics-wrapper">Ottakku Mulachathu Valli Thuzhalee Valli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ottakku Mulachathu Valli Thuzhalee Valli"/>
</div>
<div class="lyrico-lyrics-wrapper">Chuttolam Mullukalooru Kurukkan Vallee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chuttolam Mullukalooru Kurukkan Vallee"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Valliyizhanju Padarnne Aa Sakalm Chutti Varinje
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Valliyizhanju Padarnne Aa Sakalm Chutti Varinje"/>
</div>
<div class="lyrico-lyrics-wrapper">Nervazhiyum Kunnum Vayalum Athile Maranje
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nervazhiyum Kunnum Vayalum Athile Maranje"/>
</div>
<div class="lyrico-lyrics-wrapper">Athil Maranje Athil Maranje
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athil Maranje Athil Maranje"/>
</div>
<div class="lyrico-lyrics-wrapper">Ariyaathariyaathariyaanerathurul Pottum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ariyaathariyaathariyaanerathurul Pottum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaaladiyil Njodiyil Thiriyaa Neram Chira Pottum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaladiyil Njodiyil Thiriyaa Neram Chira Pottum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhiye Perumalayaay Maarum Maaraalakal Polum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhiye Perumalayaay Maarum Maaraalakal Polum"/>
</div>
<div class="lyrico-lyrics-wrapper">Idivaal Muna Shila Vetti Pilarum Pilarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idivaal Muna Shila Vetti Pilarum Pilarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Innalevareyithu Mundaka Vayalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innalevareyithu Mundaka Vayalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Innu Niranju Kavinjoru Kadalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innu Niranju Kavinjoru Kadalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannil Kathum Iruttil Nammude Swantham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannil Kathum Iruttil Nammude Swantham"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhiyum Thodiyum Kalavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhiyum Thodiyum Kalavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Innalevareyithu Mundaka Vayalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innalevareyithu Mundaka Vayalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Innu Niranju Kavinjoru Kadalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innu Niranju Kavinjoru Kadalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannil Kathum Iruttil Nammude Swantham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannil Kathum Iruttil Nammude Swantham"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhiyum Thodiyum Kalavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhiyum Thodiyum Kalavu"/>
</div>
</pre>
