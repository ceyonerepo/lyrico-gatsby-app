---
title: "makelara vichaaramu song lyrics"
album: "Sarvam Thaala Mayam"
artist: "Tyagaraja"
lyricist: "Tyagaraja"
director: "Rajiv Menon"
path: "/albums/sarvam-thaala-mayam-lyrics"
song: "Makelara Vichaaramu"
image: ../../images/albumart/sarvam-thaala-mayam.jpg
date: 2019-02-01
lang: tamil
youtubeLink: "https://www.youtube.com/embed/3ub4S2qbsHg"
type: "melody"
singers:
  - Bombay Jayashree
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Makelara Vichaaramu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Makelara Vichaaramu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Makelara Vichaaramu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Makelara Vichaaramu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Makelara Vichaaramu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Makelara Vichaaramu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Makelara Vicharamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Makelara Vicharamu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Makelara Vichaaramu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Makelara Vichaaramu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Makelara Vicharamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Makelara Vicharamu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Makelaraa Vicharamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Makelaraa Vicharamu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maruganna Sri Ramachandra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maruganna Sri Ramachandra"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Makelara Vicharamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Makelara Vicharamu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maruganna Sri Ramachandraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maruganna Sri Ramachandraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Makelaraa Vicharamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Makelaraa Vicharamu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sakethara Ja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sakethara Ja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sakethara Ja Kumara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sakethara Ja Kumara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sakethara Ja Kumara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sakethara Ja Kumara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sa Bhaktha Mandhara Srikara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sa Bhaktha Mandhara Srikara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sa Bhaktha Mandhara Srikara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sa Bhaktha Mandhara Srikara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Makelara Vicharamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Makelara Vicharamu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maruganna Sri Ramachandra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maruganna Sri Ramachandra"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Makelara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Makelara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jathakoorchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jathakoorchi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jathakoorchi Nataku Suthramunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jathakoorchi Nataku Suthramunu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jathakoorchi Nataku Suthramunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jathakoorchi Nataku Suthramunu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jagamella Mechaga Karamuna Nidi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jagamella Mechaga Karamuna Nidi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jathakoorchi Nataka Suthramunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jathakoorchi Nataka Suthramunu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jagamella Mechaga Karamuna Nidi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jagamella Mechaga Karamuna Nidi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gathi Thaapaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gathi Thaapaka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gathi Thaapaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gathi Thaapaka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gathi Thaapaka Dinchevu Sumee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gathi Thaapaka Dinchevu Sumee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Natha Thyagraja Gireesa Vinutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natha Thyagraja Gireesa Vinutha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Natha Thyagraja Gireesa Vinutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natha Thyagraja Gireesa Vinutha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Natha Thiyagraja Gireesa Vinutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natha Thiyagraja Gireesa Vinutha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Makelara Vicharamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Makelara Vicharamu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maruganna Sri Ramachandra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maruganna Sri Ramachandra"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Makelaraa Vicharamu Aaa Aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Makelaraa Vicharamu Aaa Aaa"/>
</div>
</pre>
