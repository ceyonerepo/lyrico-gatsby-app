---
title: "dhooramai song lyrics"
album: "Peranbu"
artist: "Yuvan Shankar Raja"
lyricist: "Vairamuthu"
director: "Ram"
path: "/albums/peranbu-lyrics"
song: "Dhooramai"
image: ../../images/albumart/peranbu.jpg
date: 2019-02-01
lang: tamil
youtubeLink: "https://www.youtube.com/embed/7GoArfp7pww"
type: "happy"
singers:
  - Vijay Yesudas
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Dhooramaai siru oli thonuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhooramaai siru oli thonuthey"/>
</div>
<div class="lyrico-lyrics-wrapper">Siru kuyil koovuthey sitru uyire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru kuyil koovuthey sitru uyire"/>
</div>
<div class="lyrico-lyrics-wrapper">Soolnilai mananilai maatruthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soolnilai mananilai maatruthey"/>
</div>
<div class="lyrico-lyrics-wrapper">Udal nilai theatruthey kutru uyire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udal nilai theatruthey kutru uyire"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thisaigalai nee maranthu vidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thisaigalai nee maranthu vidu"/>
</div>
<div class="lyrico-lyrics-wrapper">payanangalai ohh thodarnthu vidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="payanangalai ohh thodarnthu vidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Saalai kaatil tholaiyilam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saalai kaatil tholaiyilam"/>
</div>
<div class="lyrico-lyrics-wrapper">kaalai oontri ettu vai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaalai oontri ettu vai"/>
</div>
<div class="lyrico-lyrics-wrapper">Saalai vanthu seruvaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saalai vanthu seruvaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhooramaai siru oli thonuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhooramaai siru oli thonuthey"/>
</div>
<div class="lyrico-lyrics-wrapper">Siru kuyil koovthey sitru uyire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru kuyil koovthey sitru uyire"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kollai azhagu theeraathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kollai azhagu theeraathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuruvi ingu saagathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuruvi ingu saagathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vellai poogal vaadathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vellai poogal vaadathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Veyil soodu neraathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veyil soodu neraathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ingey thontrum siriya malai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingey thontrum siriya malai"/>
</div>
<div class="lyrico-lyrics-wrapper">Iyaragai thaayin periya manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iyaragai thaayin periya manam"/>
</div>
<div class="lyrico-lyrics-wrapper">Parugum neeril paalin suvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parugum neeril paalin suvai"/>
</div>
<div class="lyrico-lyrics-wrapper">Parivodu uravaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parivodu uravaadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kulalodu pona siru kaatru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulalodu pona siru kaatru"/>
</div>
<div class="lyrico-lyrics-wrapper">Isaiyaga maari veliyerum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Isaiyaga maari veliyerum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un meethu madum mazhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un meethu madum mazhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kotti megam kalainthu odumey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kotti megam kalainthu odumey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Peru thunbam palagi ponaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peru thunbam palagi ponaley"/>
</div>
<div class="lyrico-lyrics-wrapper">Siru thunbam ethum nerathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru thunbam ethum nerathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanneeril valum meenugu ethu kulirgalamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanneeril valum meenugu ethu kulirgalamey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thisaigalai nee maranthu vidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thisaigalai nee maranthu vidu"/>
</div>
<div class="lyrico-lyrics-wrapper">payangalai ohh thodarnthu vidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="payangalai ohh thodarnthu vidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Piraiyum mella nilavu aagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Piraiyum mella nilavu aagum"/>
</div>
<div class="lyrico-lyrics-wrapper">kuraium unthan azhagu aagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuraium unthan azhagu aagum"/>
</div>
<div class="lyrico-lyrics-wrapper">Valaium maari vayal paayum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valaium maari vayal paayum"/>
</div>
<div class="lyrico-lyrics-wrapper">varamey odivaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varamey odivaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Salavai seyutha poongatru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Salavai seyutha poongatru"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaai paal pontra neer ootru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaai paal pontra neer ootru"/>
</div>
<div class="lyrico-lyrics-wrapper">Saaral mozhiyil paaratu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saaral mozhiyil paaratu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vera enna vendumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vera enna vendumo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mozhiyatra boomi ithuvaagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mozhiyatra boomi ithuvaagum"/>
</div>
<div class="lyrico-lyrics-wrapper">Muga pavam mozhiyaagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muga pavam mozhiyaagum"/>
</div>
<div class="lyrico-lyrics-wrapper">Malar pootha ithalil nagaiputhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malar pootha ithalil nagaiputhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai magiz ootta vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai magiz ootta vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pani mootam moodi ponalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pani mootam moodi ponalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadhi ottam nintru pogaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadhi ottam nintru pogaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidhi moodum vaalvu vidai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidhi moodum vaalvu vidai"/>
</div>
<div class="lyrico-lyrics-wrapper">Theadi theadi nadai poda vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theadi theadi nadai poda vaa"/>
</div>
</pre>
