---
title: "thinam thinam song lyrics"
album: "Vaanam Kottattum"
artist: "Sid Sriram"
lyricist: "Siva Ananth"
director: "Dhana"
path: "/albums/vaanam-kottattum-lyrics"
song: "Thinam Thinam"
image: ../../images/albumart/vaanam-kottattum.jpg
date: 2020-02-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/0Vy2WZoT-uk"
type: "Love"
singers:
  - Sid Sriram
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Pesaadhae…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesaadhae…"/>
</div>
<div class="lyrico-lyrics-wrapper">Veenaa un sol…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veenaa un sol…"/>
</div>
<div class="lyrico-lyrics-wrapper">Odaadhae…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odaadhae…"/>
</div>
<div class="lyrico-lyrics-wrapper">En chellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En chellam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Podhadhae…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhadhae…"/>
</div>
<div class="lyrico-lyrics-wrapper">Kondaada endhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondaada endhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogadhae…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogadhae…"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhiyillai nil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhiyillai nil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Utchiyila urugum neer
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Utchiyila urugum neer"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhai kadantha pin kaattaarae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai kadantha pin kaattaarae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uchchi kottugira udhathoram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uchchi kottugira udhathoram"/>
</div>
<div class="lyrico-lyrics-wrapper">Kovam motti izhukkuthu paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kovam motti izhukkuthu paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ilai nuni thodum oru thooral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilai nuni thodum oru thooral"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi ver varai seraadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi ver varai seraadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Un paarvaiyil naan vizhum punniyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un paarvaiyil naan vizhum punniyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh thinam oh thinam varaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh thinam oh thinam varaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thinam oh thinam oh thinam varaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thinam oh thinam oh thinam varaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thinam oh thinam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thinam oh thinam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaaraiyoo…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaraiyoo…"/>
</div>
<div class="lyrico-lyrics-wrapper">Oodoodi ennidam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oodoodi ennidam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kooraaiyoo…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooraaiyoo…"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru padal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru padal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aagayam…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagayam…"/>
</div>
<div class="lyrico-lyrics-wrapper">Engaeyum un nizhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engaeyum un nizhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanandham…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanandham…"/>
</div>
<div class="lyrico-lyrics-wrapper">En arugil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En arugil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Utchiyila urugum neer
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Utchiyila urugum neer"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhai kadantha pin kaattaarae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai kadantha pin kaattaarae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uchchi kottugira udhathoram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uchchi kottugira udhathoram"/>
</div>
<div class="lyrico-lyrics-wrapper">Kovam motti izhukkuthu paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kovam motti izhukkuthu paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ilai nuni thodum oru thooral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilai nuni thodum oru thooral"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi ver varai seraadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi ver varai seraadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Un paarvaiyil naan vizhum punniyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un paarvaiyil naan vizhum punniyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh thinam oh thinam varaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh thinam oh thinam varaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thinam oh thinam oh thinam varaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thinam oh thinam oh thinam varaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thinam oh thinam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thinam oh thinam"/>
</div>
<div class="lyrico-lyrics-wrapper">Varaatha…varaatha…ohh…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varaatha…varaatha…ohh…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pesum…ohh…ohh..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesum…ohh…ohh.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pesadhae…ohh oo hoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesadhae…ohh oo hoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Oodadhae…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oodadhae…"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogaadhae…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogaadhae…"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogaadhae…oh..ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogaadhae…oh..ooo"/>
</div>
</pre>
