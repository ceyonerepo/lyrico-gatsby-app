---
title: 'oliyum oliyum song lyrics'
album: 'Comali'
artist: 'Hiphop Tamizha'
lyricist: 'Kabilan Vairamuthu'
director: 'Pradeep Ranganathan'
path: '/albums/comali-song-lyrics'
song: 'Oliyum Oliyum'
image: ../../images/albumart/comali.jpg
date: 2019-08-15
lang: tamil
youtubeLink: "https://www.youtube.com/embed/rBBD7N7qkE8"
type: 'philosophy'
singers: 
- Sathya Narayan
- Ajay Krishnaa
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">

  <div class="lyrico-lyrics-wrapper">Tagudagu tagudagu tagudagu tagudagu
<input type="checkbox" class="lyrico-select-lyric-line" value="Tagudagu tagudagu tagudagu tagudagu"/>
</div>
<div class="lyrico-lyrics-wrapper">Tagudagu tagudagu tagadam taa
<input type="checkbox" class="lyrico-select-lyric-line" value="Tagudagu tagudagu tagadam taa"/>
</div>
<div class="lyrico-lyrics-wrapper">Tagudagu tagudagu tagudagu tagudagu
<input type="checkbox" class="lyrico-select-lyric-line" value="Tagudagu tagudagu tagudagu tagudagu"/>
</div>
<div class="lyrico-lyrics-wrapper">Tagudagu tagudagu tagadam taa
<input type="checkbox" class="lyrico-select-lyric-line" value="Tagudagu tagudagu tagadam taa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Oru oliyum oliyum paaka
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru oliyum oliyum paaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Annaikku ooru kooduchae
<input type="checkbox" class="lyrico-select-lyric-line" value="Annaikku ooru kooduchae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippo channela maathi maathiyae
<input type="checkbox" class="lyrico-select-lyric-line" value="Ippo channela maathi maathiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma oravu andhuduchae
<input type="checkbox" class="lyrico-select-lyric-line" value="Namma oravu andhuduchae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Super staru jodi ellam
<input type="checkbox" class="lyrico-select-lyric-line" value="Super staru jodi ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Paati aayiruchae
<input type="checkbox" class="lyrico-select-lyric-line" value="Paati aayiruchae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippo pethi ellaam valandhu vandhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Ippo pethi ellaam valandhu vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Jodi serndhuruchae
<input type="checkbox" class="lyrico-select-lyric-line" value="Jodi serndhuruchae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Tagudagu tagudagu tagudagu tagudagu
<input type="checkbox" class="lyrico-select-lyric-line" value="Tagudagu tagudagu tagudagu tagudagu"/>
</div>
<div class="lyrico-lyrics-wrapper">Tagudagu tagudagu tagadam taa
<input type="checkbox" class="lyrico-select-lyric-line" value="Tagudagu tagudagu tagadam taa"/>
</div>
<div class="lyrico-lyrics-wrapper">Tagudagu tagudagu tagudagu tagudagu
<input type="checkbox" class="lyrico-select-lyric-line" value="Tagudagu tagudagu tagudagu tagudagu"/>
</div>
<div class="lyrico-lyrics-wrapper">Tagudagu tagudagu tagadam taa
<input type="checkbox" class="lyrico-select-lyric-line" value="Tagudagu tagudagu tagadam taa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Javvu mittaai watchu katti
<input type="checkbox" class="lyrico-select-lyric-line" value="Javvu mittaai watchu katti"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam pochu annikku
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaalam pochu annikku"/>
</div>
<div class="lyrico-lyrics-wrapper">BP sugar-a watchil paathu
<input type="checkbox" class="lyrico-select-lyric-line" value="BP sugar-a watchil paathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhka pochu innaikku
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaazhka pochu innaikku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Emmadhamumsammadham nu
<input type="checkbox" class="lyrico-select-lyric-line" value="Emmadhamumsammadham nu"/>
</div>
<div class="lyrico-lyrics-wrapper">Solli thandhiyae
<input type="checkbox" class="lyrico-select-lyric-line" value="Solli thandhiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sammadhaththa paadhiyila maathikitiyae
<input type="checkbox" class="lyrico-select-lyric-line" value="Sammadhaththa paadhiyila maathikitiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Oooh…
<input type="checkbox" class="lyrico-select-lyric-line" value="Oooh…"/>
</div>
<div class="lyrico-lyrics-wrapper">Koyilukkul aandavana paatha aalathaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Koyilukkul aandavana paatha aalathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kalagathukku adiyaala korthuvitiyae
<input type="checkbox" class="lyrico-select-lyric-line" value="Un kalagathukku adiyaala korthuvitiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thagudhi illa tharuthalaikkum
<input type="checkbox" class="lyrico-select-lyric-line" value="Thagudhi illa tharuthalaikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thimiru irukkudhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Thimiru irukkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Tamil naatula pozhaikkanumna
<input type="checkbox" class="lyrico-select-lyric-line" value="Tamil naatula pozhaikkanumna"/>
</div>
<div class="lyrico-lyrics-wrapper">Odambu valikkudhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Odambu valikkudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Nadar kada nair kada
<input type="checkbox" class="lyrico-select-lyric-line" value="Nadar kada nair kada"/>
</div>
<div class="lyrico-lyrics-wrapper">Ella edathilum
<input type="checkbox" class="lyrico-select-lyric-line" value="Ella edathilum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nagaland um mizoram um
<input type="checkbox" class="lyrico-select-lyric-line" value="Nagaland um mizoram um"/>
</div>
<div class="lyrico-lyrics-wrapper">Vela seiyudhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Vela seiyudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Na enga iruken
<input type="checkbox" class="lyrico-select-lyric-line" value="Na enga iruken"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakkenna aachu
<input type="checkbox" class="lyrico-select-lyric-line" value="Enakkenna aachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippo india-vula machaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Ippo india-vula machaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaroda aatchi
<input type="checkbox" class="lyrico-select-lyric-line" value="Yaaroda aatchi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
 &   <div class="lyrico-lyrics-wrapper">Na enga iruken
<input type="checkbox" class="lyrico-select-lyric-line" value="Na enga iruken"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakkenna aachu
<input type="checkbox" class="lyrico-select-lyric-line" value="Enakkenna aachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippo india-vula machaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Ippo india-vula machaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaroda aatchi
<input type="checkbox" class="lyrico-select-lyric-line" value="Yaaroda aatchi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Oru oliyum oliyum paaka
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru oliyum oliyum paaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Annaikku ooru kooduchae
<input type="checkbox" class="lyrico-select-lyric-line" value="Annaikku ooru kooduchae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippo channela maathi maathiyae
<input type="checkbox" class="lyrico-select-lyric-line" value="Ippo channela maathi maathiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma oravu andhuduchae
<input type="checkbox" class="lyrico-select-lyric-line" value="Namma oravu andhuduchae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Super staru jodi ellam
<input type="checkbox" class="lyrico-select-lyric-line" value="Super staru jodi ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Paati aayiruchae
<input type="checkbox" class="lyrico-select-lyric-line" value="Paati aayiruchae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippo pethi ellaam valandhu vandhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Ippo pethi ellaam valandhu vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Jodi serndhuruchae
<input type="checkbox" class="lyrico-select-lyric-line" value="Jodi serndhuruchae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Mottaiyum mottaiyum sendhuchaam
<input type="checkbox" class="lyrico-select-lyric-line" value="Mottaiyum mottaiyum sendhuchaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Murunga marathula erichaam
<input type="checkbox" class="lyrico-select-lyric-line" value="Murunga marathula erichaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Katta erumbu kadichuchaam
<input type="checkbox" class="lyrico-select-lyric-line" value="Katta erumbu kadichuchaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalu kaalu-nu kathuchaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaalu kaalu-nu kathuchaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ohhh ..annaiku 90’s kidu
<input type="checkbox" class="lyrico-select-lyric-line" value="Ohhh ..annaiku 90’s kidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Cd -yil paartha
<input type="checkbox" class="lyrico-select-lyric-line" value="Cd -yil paartha"/>
</div>
  <div class="lyrico-lyrics-wrapper">Kasamusa kasamusadaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Kasamusa kasamusadaa"/>
</div>
  <div class="lyrico-lyrics-wrapper">Innaiku 2k kidu</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Innaiku 2k kidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Tiktokil paarthu sick aagi
<input type="checkbox" class="lyrico-select-lyric-line" value="Tiktokil paarthu sick aagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kedakudhu paar
<input type="checkbox" class="lyrico-select-lyric-line" value="Kedakudhu paar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Roadhudhaan podattum
<input type="checkbox" class="lyrico-select-lyric-line" value="Roadhudhaan podattum"/>
</div>
  <div class="lyrico-lyrics-wrapper">Ohooo
<input type="checkbox" class="lyrico-select-lyric-line" value="Ohooo"/>
</div>
  <div class="lyrico-lyrics-wrapper">Naadu dhaan marattum
<input type="checkbox" class="lyrico-select-lyric-line" value="Naadu dhaan marattum"/>
</div>
  <div class="lyrico-lyrics-wrapper">Aahaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Aahaa"/>
</div>
  <div class="lyrico-lyrics-wrapper">Vivasayam pannithaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Vivasayam pannithaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vivasaayi vaazhattum
<input type="checkbox" class="lyrico-select-lyric-line" value="Vivasaayi vaazhattum"/>
</div>
  <div class="lyrico-lyrics-wrapper">Adhu ….
<input type="checkbox" class="lyrico-select-lyric-line" value="Adhu …."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Aaangilam padikkattum
<input type="checkbox" class="lyrico-select-lyric-line" value="Aaangilam padikkattum"/>
</div>
  <div class="lyrico-lyrics-wrapper">Yah yah
<input type="checkbox" class="lyrico-select-lyric-line" value="Yah yah"/>
</div>
  <div class="lyrico-lyrics-wrapper">Hindiyum pesattum
<input type="checkbox" class="lyrico-select-lyric-line" value="Hindiyum pesattum"/>
</div>
  <div class="lyrico-lyrics-wrapper">Kya kyaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Kya kyaa"/>
</div>
  <div class="lyrico-lyrics-wrapper">Thaai mozhi tamil mattum
<input type="checkbox" class="lyrico-select-lyric-line" value="Thaai mozhi tamil mattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalamai thaangattum
<input type="checkbox" class="lyrico-select-lyric-line" value="Thalamai thaangattum"/>
</div>
  <div class="lyrico-lyrics-wrapper">Tamizhanda..
<input type="checkbox" class="lyrico-select-lyric-line" value="Tamizhanda.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Theeyaama vegura
<input type="checkbox" class="lyrico-select-lyric-line" value="Theeyaama vegura"/>
</div>
<div class="lyrico-lyrics-wrapper">Aayavin dosaiyaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Aayavin dosaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhoomiyae aagattum
<input type="checkbox" class="lyrico-select-lyric-line" value="Bhoomiyae aagattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellarum nalla irukattum
<input type="checkbox" class="lyrico-select-lyric-line" value="Ellarum nalla irukattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Dang dang yaaradhu…
<input type="checkbox" class="lyrico-select-lyric-line" value="Dang dang yaaradhu…"/>
</div>
<div class="lyrico-lyrics-wrapper">Peyadhu..
<input type="checkbox" class="lyrico-select-lyric-line" value="Peyadhu.."/>
</div>
<div class="lyrico-lyrics-wrapper">Enna venum…
<input type="checkbox" class="lyrico-select-lyric-line" value="Enna venum…"/>
</div>
<div class="lyrico-lyrics-wrapper">Color venum…
<input type="checkbox" class="lyrico-select-lyric-line" value="Color venum…"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna color..
<input type="checkbox" class="lyrico-select-lyric-line" value="Enna color.."/>
</div>
<div class="lyrico-lyrics-wrapper">Pacha color..
<input type="checkbox" class="lyrico-select-lyric-line" value="Pacha color.."/>
</div>
<div class="lyrico-lyrics-wrapper">Enna pachcha..
<input type="checkbox" class="lyrico-select-lyric-line" value="Enna pachcha.."/>
</div>
<div class="lyrico-lyrics-wrapper">Maa pachcha…
<input type="checkbox" class="lyrico-select-lyric-line" value="Maa pachcha…"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna maa…
<input type="checkbox" class="lyrico-select-lyric-line" value="Enna maa…"/>
</div>
<div class="lyrico-lyrics-wrapper">Cinema…
<input type="checkbox" class="lyrico-select-lyric-line" value="Cinema…"/>
</div>
<div class="lyrico-lyrics-wrapper">Dang dang dang dang ennamaa…
<input type="checkbox" class="lyrico-select-lyric-line" value="Dang dang dang dang ennamaa…"/>
</div>
<div class="lyrico-lyrics-wrapper">Ungammaa…heyyy
<input type="checkbox" class="lyrico-select-lyric-line" value="Ungammaa…heyyy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Oru oliyum oliyum paaka
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru oliyum oliyum paaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Annaikku ooru kooduchae
<input type="checkbox" class="lyrico-select-lyric-line" value="Annaikku ooru kooduchae"/>
</div>
  <div class="lyrico-lyrics-wrapper">Kooduchae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kooduchae"/>
</div>
  <div class="lyrico-lyrics-wrapper">Ippo channela maathi maathiyae
<input type="checkbox" class="lyrico-select-lyric-line" value="Ippo channela maathi maathiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma oravu andhuduchae
<input type="checkbox" class="lyrico-select-lyric-line" value="Namma oravu andhuduchae"/>
</div>
  <div class="lyrico-lyrics-wrapper">Andhuduchae
<input type="checkbox" class="lyrico-select-lyric-line" value="Andhuduchae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Super staru jodi ellam
<input type="checkbox" class="lyrico-select-lyric-line" value="Super staru jodi ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Paati aayiruchae
<input type="checkbox" class="lyrico-select-lyric-line" value="Paati aayiruchae"/>
</div>
  <div class="lyrico-lyrics-wrapper">Aayiruchae
<input type="checkbox" class="lyrico-select-lyric-line" value="Aayiruchae"/>
</div>
  <div class="lyrico-lyrics-wrapper">Ippo pethi ellaam valandhu vandhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Ippo pethi ellaam valandhu vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Jodi serndhuruchae
<input type="checkbox" class="lyrico-select-lyric-line" value="Jodi serndhuruchae"/>
</div>
  <div class="lyrico-lyrics-wrapper">Serndhuruchae
<input type="checkbox" class="lyrico-select-lyric-line" value="Serndhuruchae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">20<div class="lyrico-lyrics-wrapper">thu varusathula</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="thu varusathula"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithana nadanthuruchae
<input type="checkbox" class="lyrico-select-lyric-line" value="Ithana nadanthuruchae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoonginom mulichu paatha
<input type="checkbox" class="lyrico-select-lyric-line" value="Thoonginom mulichu paatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagamae maariduchae
<input type="checkbox" class="lyrico-select-lyric-line" value="Ulagamae maariduchae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Na enga iruken
<input type="checkbox" class="lyrico-select-lyric-line" value="Na enga iruken"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakkenna aachu
<input type="checkbox" class="lyrico-select-lyric-line" value="Enakkenna aachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippo india-vula machaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Ippo india-vula machaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaroda aatchi
<input type="checkbox" class="lyrico-select-lyric-line" value="Yaaroda aatchi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
 &   <div class="lyrico-lyrics-wrapper">Na enga iruken
<input type="checkbox" class="lyrico-select-lyric-line" value="Na enga iruken"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakkenna aachu
<input type="checkbox" class="lyrico-select-lyric-line" value="Enakkenna aachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippo india-vula machaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Ippo india-vula machaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaroda aatchi
<input type="checkbox" class="lyrico-select-lyric-line" value="Yaaroda aatchi"/>
</div>
</pre>