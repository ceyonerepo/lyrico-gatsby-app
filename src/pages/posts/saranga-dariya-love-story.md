---
title: "saranga dariya song lyrics"
album: "Love Story"
artist: "Pawan Ch"
lyricist: "Suddala Ashok Teja"
director: "Sekhar Kammula"
path: "/albums/love-story-lyrics"
song: "Saranga Dariya"
image: ../../images/albumart/love-story.jpg
date: 2021-09-24
lang: telugu
youtubeLink: "https://www.youtube.com/embed/h9Am4CYaLng"
type: "happy"
singers:
  - Mangli
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Dhani Kudi Bujam Meeda Kadava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhani Kudi Bujam Meeda Kadava"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhani Guttepu Raikalu Meriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhani Guttepu Raikalu Meriya"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhi Rammante Radhura Cheliya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhi Rammante Radhura Cheliya"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhani Perey Saranga Dhariya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhani Perey Saranga Dhariya"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhani Yedam Bujam Meedha Kadava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhani Yedam Bujam Meedha Kadava"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhani Yejantu Raikalu Meriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhani Yejantu Raikalu Meriya"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhi Rammante Radhura Cheliya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhi Rammante Radhura Cheliya"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhani Perey Saranga Dhariya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhani Perey Saranga Dhariya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kallaku Yendi Gajjel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallaku Yendi Gajjel"/>
</div>
<div class="lyrico-lyrics-wrapper">Lekunna Nadisthe Ghal Ghal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lekunna Nadisthe Ghal Ghal"/>
</div>
<div class="lyrico-lyrics-wrapper">Koppula Malle Dhandal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koppula Malle Dhandal"/>
</div>
<div class="lyrico-lyrics-wrapper">Lekunna Chekkili Gil Gil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lekunna Chekkili Gil Gil"/>
</div>
<div class="lyrico-lyrics-wrapper">Navvula Levura Mutyal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Navvula Levura Mutyal"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhi Navvithe Vasthayi Muripal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhi Navvithe Vasthayi Muripal"/>
</div>
<div class="lyrico-lyrics-wrapper">Notlo Sunnam Kaasul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Notlo Sunnam Kaasul"/>
</div>
<div class="lyrico-lyrics-wrapper">Lekunna Thamalapakul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lekunna Thamalapakul"/>
</div>
<div class="lyrico-lyrics-wrapper">Munipantitho Muripantitho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munipantitho Muripantitho"/>
</div>
<div class="lyrico-lyrics-wrapper">Muripantitho Nokkithe Pedhavul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muripantitho Nokkithe Pedhavul"/>
</div>
<div class="lyrico-lyrics-wrapper">Erraga Ayithadi Raa Mana Dhil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Erraga Ayithadi Raa Mana Dhil"/>
</div>
<div class="lyrico-lyrics-wrapper">Churiya Churiya Churiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Churiya Churiya Churiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhi Surma Pettina Churiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhi Surma Pettina Churiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhi Rammante Radhura Cheliya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhi Rammante Radhura Cheliya"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhani Pere Saranga Dhariya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhani Pere Saranga Dhariya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhani Kudi Bujam Meeda Kadava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhani Kudi Bujam Meeda Kadava"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhani Guttepu Raikalu Meriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhani Guttepu Raikalu Meriya"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhi Rammante Radhura Cheliya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhi Rammante Radhura Cheliya"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhani Perey Saranga Dhariya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhani Perey Saranga Dhariya"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhani Yedam Bujam Meedha Kadava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhani Yedam Bujam Meedha Kadava"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhani Yejantu Raikalu Meriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhani Yejantu Raikalu Meriya"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhi Rammante Radhura Cheliya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhi Rammante Radhura Cheliya"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhani Perey Saranga Dhariya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhani Perey Saranga Dhariya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Range Leni Na Angi Jeda 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Range Leni Na Angi Jeda "/>
</div>
<div class="lyrico-lyrics-wrapper">Thakithe Adhi Nallangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakithe Adhi Nallangi"/>
</div>
<div class="lyrico-lyrics-wrapper">Matala Ghatu Lavangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Matala Ghatu Lavangi"/>
</div>
<div class="lyrico-lyrics-wrapper">Marla Padithe Adhi Shivangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marla Padithe Adhi Shivangi"/>
</div>
<div class="lyrico-lyrics-wrapper">Theegalu Leni Sarangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theegalu Leni Sarangi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vayinchapothe Adhi Firaangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayinchapothe Adhi Firaangi"/>
</div>
<div class="lyrico-lyrics-wrapper">Gudiya Gudiya Gudiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gudiya Gudiya Gudiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhi Chikki Chikkani Chidiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhi Chikki Chikkani Chidiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhi Rammante Radhura Cheliya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhi Rammante Radhura Cheliya"/>
</div>
<div class="lyrico-lyrics-wrapper">Daani Pere Saranga Dhariya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daani Pere Saranga Dhariya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhani Sampel Ennela Kuriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhani Sampel Ennela Kuriya"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhani Sevulaki Dhuddhul Meriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhani Sevulaki Dhuddhul Meriya"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhi Rammante Radhura Cheliya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhi Rammante Radhura Cheliya"/>
</div>
<div class="lyrico-lyrics-wrapper">Daani Pere Saranga Dhariya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daani Pere Saranga Dhariya"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhani Nadum Mudathale Meriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhani Nadum Mudathale Meriya"/>
</div>
<div class="lyrico-lyrics-wrapper">Padipothadi Mogolla Dhuniya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padipothadi Mogolla Dhuniya"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhi Rammante Radhura Cheliya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhi Rammante Radhura Cheliya"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhani Pere Saranga Dhariya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhani Pere Saranga Dhariya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhani Kudi Bujam Meeda Kadava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhani Kudi Bujam Meeda Kadava"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhani Guttepu Raikalu Meriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhani Guttepu Raikalu Meriya"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhi Rammante Radhura Cheliya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhi Rammante Radhura Cheliya"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhani Perey Saranga Dhariya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhani Perey Saranga Dhariya"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhani Yedam Bujam Meedha Kadava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhani Yedam Bujam Meedha Kadava"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhani Yejantu Raikalu Meriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhani Yejantu Raikalu Meriya"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhi Rammante Radhura Cheliya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhi Rammante Radhura Cheliya"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhani Perey Saranga Dhariya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhani Perey Saranga Dhariya"/>
</div>
</pre>
