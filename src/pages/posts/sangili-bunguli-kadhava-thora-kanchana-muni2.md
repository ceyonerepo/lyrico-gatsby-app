---
title: "sangili bunguli kadhava thora song lyrics"
album: "Kanchana-Muni2"
artist: "S. Thaman"
lyricist: "Viveka"
director: "Raghava Lawrence"
path: "/albums/kanchana-muni2-lyrics"
song: "Sangili Bunguli Kadhava Thora"
image: ../../images/albumart/kanchana-muni2.jpg
date: 2011-07-22
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ecPEYJ1NiUg"
type: "Love"
singers:
  - Velmurugan
  - Kailash Kher
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Teppa kettukkuravangellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teppa kettukkuravangellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kettukkalaa….mmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kettukkalaa….mmm"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye.. kaayae kalupanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye.. kaayae kalupanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanji ooththi nellikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanji ooththi nellikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooyae puliyanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooyae puliyanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Uppu kacha nellikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uppu kacha nellikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaa aa yei yei ah ah…..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaa aa yei yei ah ah….."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye.. kaayae kalupanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye.. kaayae kalupanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanji ooththi nellikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanji ooththi nellikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooyae puliyanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooyae puliyanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Uppu kacha nellikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uppu kacha nellikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye thaavani thaarikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye thaavani thaarikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannam thaan baerikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannam thaan baerikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaayoram kovakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaayoram kovakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaarthai thaan paavakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaarthai thaan paavakka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Soodu vara kaththarikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soodu vara kaththarikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Moodu vara murungakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moodu vara murungakka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sangili mungili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sangili mungili"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathava thora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathava thora"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan maataen vengalapuli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan maataen vengalapuli"/>
</div>
<div class="lyrico-lyrics-wrapper">Aah yei yei aah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aah yei yei aah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sangili mungili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sangili mungili"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathava thora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathava thora"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan potten vengalapuli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan potten vengalapuli"/>
</div>
<div class="lyrico-lyrics-wrapper">Aah yei yei aah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aah yei yei aah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye.. kaayae kalupanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye.. kaayae kalupanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanji ooththi nellikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanji ooththi nellikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooyae puliyanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooyae puliyanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Uppu kacha nellikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uppu kacha nellikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye thaavani thaarikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye thaavani thaarikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannam thaan baerikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannam thaan baerikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaayoram kovakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaayoram kovakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaarthai thaan paavakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaarthai thaan paavakka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Soodu vara kaththarikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soodu vara kaththarikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Moodu vara murungakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moodu vara murungakka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sangili mungili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sangili mungili"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathava thora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathava thora"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan maataen vengalapuli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan maataen vengalapuli"/>
</div>
<div class="lyrico-lyrics-wrapper">Aah yei yei aah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aah yei yei aah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sangili mungili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sangili mungili"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathava thora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathava thora"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan potten vengalapuli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan potten vengalapuli"/>
</div>
<div class="lyrico-lyrics-wrapper">Aah yei yei aah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aah yei yei aah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ganpat kaashiram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ganpat kaashiram"/>
</div>
<div class="lyrico-lyrics-wrapper">Arae raam shaeth karam beedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arae raam shaeth karam beedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Tontadoin tontadoin tontadoin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tontadoin tontadoin tontadoin"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye vaazhkaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye vaazhkaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Paambu puththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paambu puththu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paakatha romba uththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paakatha romba uththu"/>
</div>
<div class="lyrico-lyrics-wrapper">Podumda rendu koththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podumda rendu koththu"/>
</div>
<div class="lyrico-lyrics-wrapper">Appaala ellaam veththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appaala ellaam veththu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye ponnoda kaiya korthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye ponnoda kaiya korthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Boomiya neeyum suththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boomiya neeyum suththu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naama ellaam romba geththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naama ellaam romba geththu"/>
</div>
<div class="lyrico-lyrics-wrapper">Manmadhan veetu soththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manmadhan veetu soththu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mela keezha bothai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mela keezha bothai"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeththum menida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeththum menida"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivalo meesa mela aasai vaikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivalo meesa mela aasai vaikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Raa…nidaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raa…nidaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye velu solluren kelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye velu solluren kelu"/>
</div>
<div class="lyrico-lyrics-wrapper">Innaiku seththa naalaikku paalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innaiku seththa naalaikku paalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye velu solluren kelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye velu solluren kelu"/>
</div>
<div class="lyrico-lyrics-wrapper">Innaiku seththa tommorow milku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innaiku seththa tommorow milku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Verthurichi potta sokka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verthurichi potta sokka"/>
</div>
<div class="lyrico-lyrics-wrapper">Venam neeyum n gokka makka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venam neeyum n gokka makka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sangili mungili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sangili mungili"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathava thora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathava thora"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan potten vengalapuli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan potten vengalapuli"/>
</div>
<div class="lyrico-lyrics-wrapper">Aah yei yei aah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aah yei yei aah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sangili mungili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sangili mungili"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathava thora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathava thora"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan potten vengalapuli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan potten vengalapuli"/>
</div>
<div class="lyrico-lyrics-wrapper">Aah yei yei aah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aah yei yei aah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Come on …
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Come on …"/>
</div>
<div class="lyrico-lyrics-wrapper">Are you ready ….
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Are you ready …."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hei hei…(14)
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei hei…(14)"/>
</div>
<div class="lyrico-lyrics-wrapper">Heiiiii heiiiii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heiiiii heiiiii"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye thilla ye thilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye thilla ye thilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye damukku ye thilla ye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye damukku ye thilla ye"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye thilla ye thilla ye thilla thilla thilla thilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye thilla ye thilla ye thilla thilla thilla thilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye thaavani paani poori
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye thaavani paani poori"/>
</div>
<div class="lyrico-lyrics-wrapper">Thabaela meni kaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thabaela meni kaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaradi puzhal yaeri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaradi puzhal yaeri"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhageththi vantha lorry
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhageththi vantha lorry"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye…. ahhh….
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye…. ahhh…."/>
</div>
<div class="lyrico-lyrics-wrapper">Ye thaavani paani poori
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye thaavani paani poori"/>
</div>
<div class="lyrico-lyrics-wrapper">Thabaela meni kaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thabaela meni kaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaradi puzhal yaeri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaradi puzhal yaeri"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhageththi vantha lorry
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhageththi vantha lorry"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Laeththu pattara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laeththu pattara"/>
</div>
<div class="lyrico-lyrics-wrapper">Sammatiya nimuthura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sammatiya nimuthura"/>
</div>
<div class="lyrico-lyrics-wrapper">Adada paaththi vettura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adada paaththi vettura"/>
</div>
<div class="lyrico-lyrics-wrapper">Mammattiya paduthuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mammattiya paduthuraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi maalu solluren kelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi maalu solluren kelu"/>
</div>
<div class="lyrico-lyrics-wrapper">Innaiku seththa naalaikku paalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innaiku seththa naalaikku paalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi maalu solluren kelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi maalu solluren kelu"/>
</div>
<div class="lyrico-lyrics-wrapper">Innaiku seththa tommorow milku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innaiku seththa tommorow milku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dangerana boomikulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dangerana boomikulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum naanum keerom pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum naanum keerom pulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kumari kumari size kumari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kumari kumari size kumari"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarthu putta kaali memory
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarthu putta kaali memory"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye kaththari kaththari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye kaththari kaththari"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarvai kaththari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarvai kaththari"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarthuputta aasupathiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarthuputta aasupathiri"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye.. kaayae kalupanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye.. kaayae kalupanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanji ooththi nellikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanji ooththi nellikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooyae puliyanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooyae puliyanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Uppu kacha nellikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uppu kacha nellikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye thaavani thaarikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye thaavani thaarikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannam thaan baerikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannam thaan baerikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaayoram kovakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaayoram kovakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaarthai thaan paavakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaarthai thaan paavakka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Soodu vara kaththarikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soodu vara kaththarikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Moodu vara murungakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moodu vara murungakka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sangili mungili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sangili mungili"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathava thora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathava thora"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan maataen vengalapuli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan maataen vengalapuli"/>
</div>
<div class="lyrico-lyrics-wrapper">Aah yei yei aah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aah yei yei aah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sangili mungili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sangili mungili"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathava thora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathava thora"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan potten vengalapuli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan potten vengalapuli"/>
</div>
<div class="lyrico-lyrics-wrapper">Aah yei yei aah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aah yei yei aah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kumari kumari size kumari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kumari kumari size kumari"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarthu putta kaali memory
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarthu putta kaali memory"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye kaththari kaththari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye kaththari kaththari"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarvai kaththari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarvai kaththari"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarthuputta aasupathiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarthuputta aasupathiri"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey vaadi ippadi vaadi ippadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey vaadi ippadi vaadi ippadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolusu potta sonu pappadi..yeh..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolusu potta sonu pappadi..yeh.."/>
</div>
<div class="lyrico-lyrics-wrapper">Thallungadii…..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thallungadii….."/>
</div>
</pre>
