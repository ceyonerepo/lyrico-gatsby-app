---
title: "ayya song lyrics"
album: "Seethakaathi"
artist: "Govind Vasantha"
lyricist: "Thiagarajan Kumararaja"
director: "Balaji Tharaneetharan"
path: "/albums/seethakaathi-lyrics"
song: "Ayya"
image: ../../images/albumart/seethakaathi.jpg
date: 2018-12-20
lang: tamil
youtubeLink: "https://www.youtube.com/embed/271JBFRTB2o"
type: "mass"
singers:
  - Vijay Prakash
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Sutrum puvi mutrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sutrum puvi mutrum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pugazh yettum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pugazh yettum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan mugam kaatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan mugam kaatta"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattrum uyir attrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattrum uyir attrum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiran thirai mevidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiran thirai mevidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Munnum ithan pinnum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munnum ithan pinnum"/>
</div>
<div class="lyrico-lyrics-wrapper">Inaiyillathoru vinaiyattra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inaiyillathoru vinaiyattra"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottum vizhi kaattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottum vizhi kaattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalai arangeridum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalai arangeridum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Katrai ozhi patri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katrai ozhi patri"/>
</div>
<div class="lyrico-lyrics-wrapper">Thigal viththai athil kalainthutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thigal viththai athil kalainthutta"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattum kai thattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattum kai thattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ozhi kadal meeridum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ozhi kadal meeridum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattum karai muttum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattum karai muttum"/>
</div>
<div class="lyrico-lyrics-wrapper">Peru vellam ivar purandoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peru vellam ivar purandoda"/>
</div>
<div class="lyrico-lyrics-wrapper">Muttra ver pattraa varai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muttra ver pattraa varai"/>
</div>
<div class="lyrico-lyrics-wrapper">Arandrodidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arandrodidum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theera oottraaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theera oottraaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Maara mattraaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maara mattraaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Seraa aalangal sendru sernthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seraa aalangal sendru sernthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Theera oottraaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theera oottraaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Maara mattraaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maara mattraaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhaamal vazhvanaan naan naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhaamal vazhvanaan naan naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Athavanai kaiyalae maraippan yevan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athavanai kaiyalae maraippan yevan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Athiyin nookkathai arinthaan yevan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athiyin nookkathai arinthaan yevan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Irappai kadanthu irulai kizhithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irappai kadanthu irulai kizhithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidiyal thanthu yezhuvan ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidiyal thanthu yezhuvan ivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayya hahahahaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayya hahahahaha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Athavanai kaiyalae maraippan yevan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athavanai kaiyalae maraippan yevan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Athiyin nookkathai arinthaan yevan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athiyin nookkathai arinthaan yevan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Irappai kadanthu irulai kizhithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irappai kadanthu irulai kizhithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidiyal thanthu yezhuvan ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidiyal thanthu yezhuvan ivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayya hahahahaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayya hahahahaha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meen konda vaanathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meen konda vaanathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Meen konda vaanathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meen konda vaanathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Min konda mugilaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Min konda mugilaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Min konda mugilaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Min konda mugilaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Soozhali ozhiyellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soozhali ozhiyellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Soozhali ozhiyellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soozhali ozhiyellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Soozh konda sangaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soozh konda sangaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Soozh konda sangaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soozh konda sangaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaan konda maramellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaan konda maramellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaan konda maramellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaan konda maramellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Than konda vithaiyaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Than konda vithaiyaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Than konda vithaiyaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Than konda vithaiyaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Misai veesum kaatrai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Misai veesum kaatrai"/>
</div>
<div class="lyrico-lyrics-wrapper">Misai veesum kaatrai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Misai veesum kaatrai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thean isaiyaakum kuzhalaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thean isaiyaakum kuzhalaaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meen konda vaanathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meen konda vaanathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Min konda mugilaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Min konda mugilaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Soozhali ozhiyellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soozhali ozhiyellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Soozh konda sangaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soozh konda sangaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaan konda maramellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaan konda maramellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Than konda vithaiyaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Than konda vithaiyaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Misai veesum kaatrai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Misai veesum kaatrai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thean isaiyaakum kuzhalaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thean isaiyaakum kuzhalaaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sillu theernthu silaiyaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sillu theernthu silaiyaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Sitharaidikkum puyalaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sitharaidikkum puyalaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Arparikkum alaiyaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arparikkum alaiyaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Meenduyirthu nilaiyaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meenduyirthu nilaiyaaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theendaatha thee kuchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theendaatha thee kuchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kulluraiyum neruppaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulluraiyum neruppaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir varaiyarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir varaiyarai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un siraiyaraiyenum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un siraiyaraiyenum"/>
</div>
<div class="lyrico-lyrics-wrapper">Palankathai dhoolaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palankathai dhoolaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athavanai kaiyalae maraippan yevan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athavanai kaiyalae maraippan yevan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Athiyin nookkathai arinthaan yevan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athiyin nookkathai arinthaan yevan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Irappai kadanthu irulai kizhithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irappai kadanthu irulai kizhithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidiyal thanthu yezhuvan ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidiyal thanthu yezhuvan ivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayya hahahahaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayya hahahahaha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Athavanai kaiyalae maraippan yevan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athavanai kaiyalae maraippan yevan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Athiyin nookkathai arinthaan yevan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athiyin nookkathai arinthaan yevan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Irappai kadanthu irulai kizhithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irappai kadanthu irulai kizhithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidiyal thanthu yezhuvan ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidiyal thanthu yezhuvan ivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayya hahahahaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayya hahahahaha"/>
</div>
</pre>
