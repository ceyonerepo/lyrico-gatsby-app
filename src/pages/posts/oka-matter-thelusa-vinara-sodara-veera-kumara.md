---
title: "oka matter thelusa song lyrics"
album: "Vinara Sodara Veera Kumara"
artist: "Shravan Bharadwaj"
lyricist: "Laxmi Bhupala"
director: "Sateesh Chandra Nadella"
path: "/albums/vinara-sodara-veera-kumara-lyrics"
song: "Oka Matter Thelusa"
image: ../../images/albumart/vinara-sodara-veera-kumara.jpg
date: 2019-03-22
lang: telugu
youtubeLink: "https://www.youtube.com/embed/N0UMNldgB1A"
type: "love"
singers:
  - Shravan Bharadwaj
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">eh oka matter thelusa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eh oka matter thelusa"/>
</div>
<div class="lyrico-lyrics-wrapper">hey love chesesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey love chesesa"/>
</div>
<div class="lyrico-lyrics-wrapper">hey katthilanti figaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey katthilanti figaru"/>
</div>
<div class="lyrico-lyrics-wrapper">hey padipoyindhehe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey padipoyindhehe"/>
</div>
<div class="lyrico-lyrics-wrapper">aa kallalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aa kallalo"/>
</div>
<div class="lyrico-lyrics-wrapper">nenante pichhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenante pichhi"/>
</div>
<div class="lyrico-lyrics-wrapper">chusindhe hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chusindhe hey"/>
</div>
<div class="lyrico-lyrics-wrapper">gundello gucchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gundello gucchi"/>
</div>
<div class="lyrico-lyrics-wrapper">kelikindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kelikindhe"/>
</div>
<div class="lyrico-lyrics-wrapper">hey na stylu nacchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey na stylu nacchi"/>
</div>
<div class="lyrico-lyrics-wrapper">navvindhe hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="navvindhe hey"/>
</div>
<div class="lyrico-lyrics-wrapper">ee naram naram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ee naram naram"/>
</div>
<div class="lyrico-lyrics-wrapper">salaparalu endhehe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="salaparalu endhehe"/>
</div>
<div class="lyrico-lyrics-wrapper">hey hey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey hey "/>
</div>
<div class="lyrico-lyrics-wrapper">noonugu meesame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="noonugu meesame"/>
</div>
<div class="lyrico-lyrics-wrapper">hey hey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey hey "/>
</div>
<div class="lyrico-lyrics-wrapper">thirigindhe pogaruga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thirigindhe pogaruga"/>
</div>
<div class="lyrico-lyrics-wrapper">hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey hey"/>
</div>
<div class="lyrico-lyrics-wrapper">naa shirtu collare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa shirtu collare"/>
</div>
<div class="lyrico-lyrics-wrapper">hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey hey"/>
</div>
<div class="lyrico-lyrics-wrapper">lechindhi styluga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lechindhi styluga"/>
</div>
<div class="lyrico-lyrics-wrapper">premalona paddavadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="premalona paddavadu"/>
</div>
<div class="lyrico-lyrics-wrapper">hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey hey"/>
</div>
<div class="lyrico-lyrics-wrapper">ambani kante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ambani kante"/>
</div>
<div class="lyrico-lyrics-wrapper">richhugadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="richhugadu"/>
</div>
<div class="lyrico-lyrics-wrapper">hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey hey"/>
</div>
<div class="lyrico-lyrics-wrapper">kullu pudithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kullu pudithe"/>
</div>
<div class="lyrico-lyrics-wrapper">okkasari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="okkasari"/>
</div>
<div class="lyrico-lyrics-wrapper">hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey hey"/>
</div>
<div class="lyrico-lyrics-wrapper">maa janta chusi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maa janta chusi"/>
</div>
<div class="lyrico-lyrics-wrapper">sacchi pondehe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sacchi pondehe"/>
</div>
<div class="lyrico-lyrics-wrapper">hey thodagottalivaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey thodagottalivaale"/>
</div>
<div class="lyrico-lyrics-wrapper">time mocchero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="time mocchero"/>
</div>
<div class="lyrico-lyrics-wrapper">hey manamante pilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey manamante pilla"/>
</div>
<div class="lyrico-lyrics-wrapper">pichhai podhiro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pichhai podhiro"/>
</div>
<div class="lyrico-lyrics-wrapper">hey ee andhagadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey ee andhagadu"/>
</div>
<div class="lyrico-lyrics-wrapper">meesam thippithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meesam thippithe"/>
</div>
<div class="lyrico-lyrics-wrapper">hey aallamma baabu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey aallamma baabu"/>
</div>
<div class="lyrico-lyrics-wrapper">naa kaalle kadagale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa kaalle kadagale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">hey chinna pedda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey chinna pedda"/>
</div>
<div class="lyrico-lyrics-wrapper">pilla jalla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pilla jalla"/>
</div>
<div class="lyrico-lyrics-wrapper">nanne chushaaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanne chushaaka"/>
</div>
<div class="lyrico-lyrics-wrapper">hero laagunnade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hero laagunnade"/>
</div>
<div class="lyrico-lyrics-wrapper">evade annare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="evade annare"/>
</div>
<div class="lyrico-lyrics-wrapper">hey naa heightu weightu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey naa heightu weightu"/>
</div>
<div class="lyrico-lyrics-wrapper">nacche pilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nacche pilla"/>
</div>
<div class="lyrico-lyrics-wrapper">lavvu chesindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lavvu chesindhe"/>
</div>
<div class="lyrico-lyrics-wrapper">hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey hey"/>
</div>
<div class="lyrico-lyrics-wrapper">maccho undi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maccho undi"/>
</div>
<div class="lyrico-lyrics-wrapper">petti puttindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="petti puttindhe"/>
</div>
<div class="lyrico-lyrics-wrapper">hey orey sannasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey orey sannasi"/>
</div>
<div class="lyrico-lyrics-wrapper">jaragara hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jaragara hey"/>
</div>
<div class="lyrico-lyrics-wrapper">evadochado chudararey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="evadochado chudararey"/>
</div>
<div class="lyrico-lyrics-wrapper">hey mana life inka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey mana life inka"/>
</div>
<div class="lyrico-lyrics-wrapper">settai poyindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="settai poyindhe"/>
</div>
<div class="lyrico-lyrics-wrapper">hey maa jodiki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey maa jodiki"/>
</div>
<div class="lyrico-lyrics-wrapper">perette dhammundha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="perette dhammundha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">eh oka matter thelusa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eh oka matter thelusa"/>
</div>
<div class="lyrico-lyrics-wrapper">hey love chesesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey love chesesa"/>
</div>
<div class="lyrico-lyrics-wrapper">hey katthilanti figaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey katthilanti figaru"/>
</div>
<div class="lyrico-lyrics-wrapper">hey padipoyindhehe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey padipoyindhehe"/>
</div>
</pre>
