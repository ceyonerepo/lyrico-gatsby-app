---
title: "kadhal kattu merandi song lyrics"
album: "Koothan"
artist: "Balz G"
lyricist: "Viveka"
director: "Venky AL"
path: "/albums/koothan-lyrics"
song: "Kadhal Kattu Merandi"
image: ../../images/albumart/koothan.jpg
date: 2018-10-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/oEoSsvjuOS8"
type: "happy"
singers:
  - Remya Nambeesan
  - Harihara Sudhan
  - Vidya Lakshmi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">odu odu odu odu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="odu odu odu odu"/>
</div>
<div class="lyrico-lyrics-wrapper">kathal katu merandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathal katu merandi"/>
</div>
<div class="lyrico-lyrics-wrapper">kaatu vaasi perandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatu vaasi perandi"/>
</div>
<div class="lyrico-lyrics-wrapper">kaduchu thinna varandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaduchu thinna varandi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">odu odu odu odu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="odu odu odu odu"/>
</div>
<div class="lyrico-lyrics-wrapper">kannala than pirandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannala than pirandi"/>
</div>
<div class="lyrico-lyrics-wrapper">kaayam panna porandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaayam panna porandi"/>
</div>
<div class="lyrico-lyrics-wrapper">sokku podi surendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sokku podi surendi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">odu odu odu odu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="odu odu odu odu"/>
</div>
<div class="lyrico-lyrics-wrapper">kathal katu merandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathal katu merandi"/>
</div>
<div class="lyrico-lyrics-wrapper">kaatu vaasi perandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatu vaasi perandi"/>
</div>
<div class="lyrico-lyrics-wrapper">kaduchu thinna varandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaduchu thinna varandi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">odu odu odu odu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="odu odu odu odu"/>
</div>
<div class="lyrico-lyrics-wrapper">kannala than pirandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannala than pirandi"/>
</div>
<div class="lyrico-lyrics-wrapper">kaayam panna porandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaayam panna porandi"/>
</div>
<div class="lyrico-lyrics-wrapper">sokku podi surendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sokku podi surendi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">irubathu vayasaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irubathu vayasaana"/>
</div>
<div class="lyrico-lyrics-wrapper">kuzhantha naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuzhantha naan"/>
</div>
<div class="lyrico-lyrics-wrapper">yarukum tholaiye 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yarukum tholaiye "/>
</div>
<div class="lyrico-lyrics-wrapper">thara maten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thara maten"/>
</div>
<div class="lyrico-lyrics-wrapper">ethana kokiya potalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethana kokiya potalum"/>
</div>
<div class="lyrico-lyrics-wrapper">ponnunga vambukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponnunga vambukku"/>
</div>
<div class="lyrico-lyrics-wrapper">vara maten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vara maten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sangu sakara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sangu sakara"/>
</div>
<div class="lyrico-lyrics-wrapper">un sangu sakara kannula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un sangu sakara kannula"/>
</div>
<div class="lyrico-lyrics-wrapper">jaadaiyaaga pesi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jaadaiyaaga pesi "/>
</div>
<div class="lyrico-lyrics-wrapper">enna mayakatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna mayakatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">yei santhadi cape la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yei santhadi cape la"/>
</div>
<div class="lyrico-lyrics-wrapper">saaku pokku kaati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saaku pokku kaati"/>
</div>
<div class="lyrico-lyrics-wrapper">engala pirikkatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engala pirikkatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">selathu maangaaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="selathu maangaaya"/>
</div>
<div class="lyrico-lyrics-wrapper">selaikkula palukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="selaikkula palukka"/>
</div>
<div class="lyrico-lyrics-wrapper">vaikka paakatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaikka paakatha"/>
</div>
<div class="lyrico-lyrics-wrapper">devathaiye vanthalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="devathaiye vanthalum"/>
</div>
<div class="lyrico-lyrics-wrapper">thedi odum paakum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thedi odum paakum"/>
</div>
<div class="lyrico-lyrics-wrapper">aalu naan illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aalu naan illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vellakatti pechu kaaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vellakatti pechu kaaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">un alli kittu poganum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un alli kittu poganum "/>
</div>
<div class="lyrico-lyrics-wrapper">nu thonuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nu thonuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">meena paatha poona pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meena paatha poona pola"/>
</div>
<div class="lyrico-lyrics-wrapper">alaiya maaten di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alaiya maaten di"/>
</div>
<div class="lyrico-lyrics-wrapper">intha maamanum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha maamanum "/>
</div>
<div class="lyrico-lyrics-wrapper">raamanum onnu than di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="raamanum onnu than di"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kathal katu merandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathal katu merandi"/>
</div>
<div class="lyrico-lyrics-wrapper">kaatu vaasi perandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatu vaasi perandi"/>
</div>
<div class="lyrico-lyrics-wrapper">kaduchu thinna varandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaduchu thinna varandi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pacha kili mooku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pacha kili mooku"/>
</div>
<div class="lyrico-lyrics-wrapper">mooku mooku mooku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mooku mooku mooku"/>
</div>
<div class="lyrico-lyrics-wrapper">cheri pazha cake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cheri pazha cake"/>
</div>
<div class="lyrico-lyrics-wrapper">cake cake cake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cake cake cake"/>
</div>
<div class="lyrico-lyrics-wrapper">vethala pota naaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vethala pota naaku"/>
</div>
<div class="lyrico-lyrics-wrapper">naaku naaku naaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naaku naaku naaku"/>
</div>
<div class="lyrico-lyrics-wrapper">pola nanum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pola nanum "/>
</div>
<div class="lyrico-lyrics-wrapper">sevanthu nikuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sevanthu nikuren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vantha ponnum paiyan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vantha ponnum paiyan "/>
</div>
<div class="lyrico-lyrics-wrapper">kitta pambaatha di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kitta pambaatha di"/>
</div>
<div class="lyrico-lyrics-wrapper">paluthu koluthu kathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paluthu koluthu kathu"/>
</div>
<div class="lyrico-lyrics-wrapper">iruken vadi en chella kutty
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iruken vadi en chella kutty"/>
</div>
<div class="lyrico-lyrics-wrapper">kola kolaya munthiri ka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kola kolaya munthiri ka"/>
</div>
<div class="lyrico-lyrics-wrapper">koothu adipom chandirika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koothu adipom chandirika"/>
</div>
<div class="lyrico-lyrics-wrapper">meesa vacha aambala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meesa vacha aambala"/>
</div>
<div class="lyrico-lyrics-wrapper">en aasaya nee purunjuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en aasaya nee purunjuko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">bit podatha reelugala suthatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bit podatha reelugala suthatha"/>
</div>
<div class="lyrico-lyrics-wrapper">intha nenju maarathu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha nenju maarathu da"/>
</div>
<div class="lyrico-lyrics-wrapper">kathal onnum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathal onnum "/>
</div>
<div class="lyrico-lyrics-wrapper">kannadi porul illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannadi porul illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kathal katu merandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathal katu merandi"/>
</div>
<div class="lyrico-lyrics-wrapper">kaatu vaasi perandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatu vaasi perandi"/>
</div>
<div class="lyrico-lyrics-wrapper">kaduchu thinna varandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaduchu thinna varandi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">odu odu odu odu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="odu odu odu odu"/>
</div>
<div class="lyrico-lyrics-wrapper">kannala than pirandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannala than pirandi"/>
</div>
<div class="lyrico-lyrics-wrapper">kaayam panna porandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaayam panna porandi"/>
</div>
<div class="lyrico-lyrics-wrapper">sokku podi surendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sokku podi surendi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">irubathu vayasaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irubathu vayasaana"/>
</div>
<div class="lyrico-lyrics-wrapper">kuzhantha naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuzhantha naan"/>
</div>
<div class="lyrico-lyrics-wrapper">yarukum tholaiye 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yarukum tholaiye "/>
</div>
<div class="lyrico-lyrics-wrapper">thara maten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thara maten"/>
</div>
<div class="lyrico-lyrics-wrapper">ethana kokiya potalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethana kokiya potalum"/>
</div>
<div class="lyrico-lyrics-wrapper">ponnunga vambukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponnunga vambukku"/>
</div>
<div class="lyrico-lyrics-wrapper">vara maten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vara maten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">odu odu odu odu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="odu odu odu odu"/>
</div>
<div class="lyrico-lyrics-wrapper">odu odu odu odu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="odu odu odu odu"/>
</div>
<div class="lyrico-lyrics-wrapper">odu odu odu odu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="odu odu odu odu"/>
</div>
<div class="lyrico-lyrics-wrapper">odu odu odu odu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="odu odu odu odu"/>
</div>
<div class="lyrico-lyrics-wrapper">odu odu odu odu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="odu odu odu odu"/>
</div>
<div class="lyrico-lyrics-wrapper">odu odu odu odu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="odu odu odu odu"/>
</div>
</pre>
