---
title: "idho thaanaagave song lyrics"
album: "Adhe Kangal"
artist: "Ghibran"
lyricist: "Uma Devi"
director: "Rohin Venkatesan"
path: "/albums/adhe-kangal-lyrics"
song: "Idho Thaanaagave"
image: ../../images/albumart/adhe-kangal.jpg
date: 2017-01-26
lang: tamil
youtubeLink: "https://www.youtube.com/embed/JTiffb-2898"
type: "love"
singers:
  - Yazin Nizar
  - Clinton Cerejo
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Idho thaanagavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idho thaanagavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeno neeyagiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeno neeyagiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhi kaanamalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhi kaanamalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai polagiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai polagiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Izhai vannamai minnudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Izhai vannamai minnudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai chellamaai kolludhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai chellamaai kolludhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal kaadhal thoondudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal kaadhal thoondudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarvai kangal meeludhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarvai kangal meeludhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aadhava poovo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadhava poovo"/>
</div>
<div class="lyrico-lyrics-wrapper">Maadhava poovo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maadhava poovo"/>
</div>
<div class="lyrico-lyrics-wrapper">Uravaa pagayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uravaa pagayaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal endhan vizhigal aagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal endhan vizhigal aagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagae unnai paarkudhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagae unnai paarkudhadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Un vaasam endhan vazhigal aagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un vaasam endhan vazhigal aagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Arugil ennai serkudhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arugil ennai serkudhadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada-kaatru ennum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada-kaatru ennum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudhirai yeri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudhirai yeri"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayam maayam aagudhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam maayam aagudhadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi-aasaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi-aasaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai thirudichella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai thirudichella"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalavum nyaayam aagumadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalavum nyaayam aagumadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muttrum thurandhaval
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muttrum thurandhaval"/>
</div>
<div class="lyrico-lyrics-wrapper">Mogam tharubaval
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mogam tharubaval"/>
</div>
<div class="lyrico-lyrics-wrapper">Needhaano pennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Needhaano pennae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Netri sudarena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netri sudarena"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilavai kortthaval
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilavai kortthaval"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyaanaai thaanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyaanaai thaanadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ottrai poovin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ottrai poovin"/>
</div>
<div class="lyrico-lyrics-wrapper">Brammaanda kaadaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Brammaanda kaadaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyaanaai pennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyaanaai pennae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Attrai naalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Attrai naalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Amudham vaartthaval
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amudham vaartthaval"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyo thaanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyo thaanadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aruginil varudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aruginil varudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirinai sududhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirinai sududhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thozhi un ninaivadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozhi un ninaivadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanimaiayi tholaindhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanimaiayi tholaindhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninaivinil tholaindhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaivinil tholaindhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyae uravadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyae uravadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal endhan vizhigal aagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal endhan vizhigal aagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagae unnai paarkudhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagae unnai paarkudhadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Un vaasam endhan vazhigal aagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un vaasam endhan vazhigal aagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Arugil ennai serkudhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arugil ennai serkudhadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada-kaatru ennum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada-kaatru ennum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudhirai yeri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudhirai yeri"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayam maayam aagudhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam maayam aagudhadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi-aasaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi-aasaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai thirudichella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai thirudichella"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalavum nyaayam aagumadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalavum nyaayam aagumadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey kaadhal endhan vizhigal aagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey kaadhal endhan vizhigal aagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagae unnai paarkudhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagae unnai paarkudhadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Un vaasam endhan vazhigal aagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un vaasam endhan vazhigal aagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Arugil ennai serkudhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arugil ennai serkudhadi"/>
</div>
</pre>
