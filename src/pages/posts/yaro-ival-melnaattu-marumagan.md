---
title: "yaro ival song lyrics"
album: "Melnaattu Marumagan"
artist: "V. Kishorkumar"
lyricist: "Na. Muthukumar"
director: "MSS"
path: "/albums/melnaattu-marumagan-lyrics"
song: "Yaro Ival"
image: ../../images/albumart/melnaattu-marumagan.jpg
date: 2018-02-16
lang: tamil
youtubeLink: "https://www.youtube.com/embed/i_nvuqj7MVw"
type: "love"
singers:
  - Karthik
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">yaro ival yar ivalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaro ival yar ivalo"/>
</div>
<div class="lyrico-lyrics-wrapper">santhosathin ther ivalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="santhosathin ther ivalo"/>
</div>
<div class="lyrico-lyrics-wrapper">kannal ennai kolbavalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannal ennai kolbavalo"/>
</div>
<div class="lyrico-lyrics-wrapper">kaayam indri velbavalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaayam indri velbavalo"/>
</div>
<div class="lyrico-lyrics-wrapper">oru murai ival parkira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru murai ival parkira"/>
</div>
<div class="lyrico-lyrics-wrapper">pothu kothikirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pothu kothikirathe"/>
</div>
<div class="lyrico-lyrics-wrapper">maru murai ival parkira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maru murai ival parkira"/>
</div>
<div class="lyrico-lyrics-wrapper">pothu kulirgirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pothu kulirgirathe"/>
</div>
<div class="lyrico-lyrics-wrapper">kanavula pakura devathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavula pakura devathai"/>
</div>
<div class="lyrico-lyrics-wrapper">nerila vanthathu en kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nerila vanthathu en kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">intha nodiyila vaalura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha nodiyila vaalura"/>
</div>
<div class="lyrico-lyrics-wrapper">inbame pothum vera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inbame pothum vera"/>
</div>
<div class="lyrico-lyrics-wrapper">enna naan paada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna naan paada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">yaro ival yar ivalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaro ival yar ivalo"/>
</div>
<div class="lyrico-lyrics-wrapper">santhosathin ther ivalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="santhosathin ther ivalo"/>
</div>
<div class="lyrico-lyrics-wrapper">kannal ennai kolbavalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannal ennai kolbavalo"/>
</div>
<div class="lyrico-lyrics-wrapper">kaayam indri velbavalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaayam indri velbavalo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">poonkaatri pen ival than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poonkaatri pen ival than"/>
</div>
<div class="lyrico-lyrics-wrapper">narumanangal kooti vaithal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="narumanangal kooti vaithal"/>
</div>
<div class="lyrico-lyrics-wrapper">pogindra paathai engum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pogindra paathai engum"/>
</div>
<div class="lyrico-lyrics-wrapper">puthu velicham kaati vaithal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthu velicham kaati vaithal"/>
</div>
<div class="lyrico-lyrics-wrapper">en nenjil oodi vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en nenjil oodi vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">en nenjil oodi vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en nenjil oodi vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">theepangalai etri vaithal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theepangalai etri vaithal"/>
</div>
<div class="lyrico-lyrics-wrapper">nilavinila malarinila enga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nilavinila malarinila enga"/>
</div>
<div class="lyrico-lyrics-wrapper">ival valarnthalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ival valarnthalo"/>
</div>
<div class="lyrico-lyrics-wrapper">ninaikaiyile inikirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninaikaiyile inikirathe"/>
</div>
<div class="lyrico-lyrics-wrapper">enakena piranthalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enakena piranthalo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kanavula pakura devathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavula pakura devathai"/>
</div>
<div class="lyrico-lyrics-wrapper">nerila vanthathu en kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nerila vanthathu en kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">intha nodiyila vaalura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha nodiyila vaalura"/>
</div>
<div class="lyrico-lyrics-wrapper">inbame pothum vera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inbame pothum vera"/>
</div>
<div class="lyrico-lyrics-wrapper">enna naan paada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna naan paada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">yaro ival yar ivalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaro ival yar ivalo"/>
</div>
<div class="lyrico-lyrics-wrapper">santhosathin ther ivalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="santhosathin ther ivalo"/>
</div>
<div class="lyrico-lyrics-wrapper">kannal ennai kolbavalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannal ennai kolbavalo"/>
</div>
<div class="lyrico-lyrics-wrapper">kaayam indri velbavalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaayam indri velbavalo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">engeyo pogum megam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engeyo pogum megam"/>
</div>
<div class="lyrico-lyrics-wrapper">enatharugil vanthathu enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enatharugil vanthathu enna"/>
</div>
<div class="lyrico-lyrics-wrapper">punnagaiyal saavi vaithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="punnagaiyal saavi vaithu"/>
</div>
<div class="lyrico-lyrics-wrapper">idhayathiyum thiranthu enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhayathiyum thiranthu enna"/>
</div>
<div class="lyrico-lyrics-wrapper">mozhi ethum thevai illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mozhi ethum thevai illai"/>
</div>
<div class="lyrico-lyrics-wrapper">mozhi ethum thevai illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mozhi ethum thevai illai"/>
</div>
<div class="lyrico-lyrics-wrapper">manangalae pothum enben
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manangalae pothum enben"/>
</div>
<div class="lyrico-lyrics-wrapper">vizhigalile vaanavillin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vizhigalile vaanavillin"/>
</div>
<div class="lyrico-lyrics-wrapper">vannangalai kandene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vannangalai kandene"/>
</div>
<div class="lyrico-lyrics-wrapper">vazhi thunaiyai ival irunthal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vazhi thunaiyai ival irunthal"/>
</div>
<div class="lyrico-lyrics-wrapper">vaazhkaiyil velvene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaazhkaiyil velvene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kanavula pakura devathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavula pakura devathai"/>
</div>
<div class="lyrico-lyrics-wrapper">nerila vanthathu en kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nerila vanthathu en kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">intha nodiyila vaalura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha nodiyila vaalura"/>
</div>
<div class="lyrico-lyrics-wrapper">inbame pothum vera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inbame pothum vera"/>
</div>
<div class="lyrico-lyrics-wrapper">enna naan paada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna naan paada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">yaro ival yar ivalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaro ival yar ivalo"/>
</div>
<div class="lyrico-lyrics-wrapper">santhosathin ther ivalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="santhosathin ther ivalo"/>
</div>
<div class="lyrico-lyrics-wrapper">kannal ennai kolbavalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannal ennai kolbavalo"/>
</div>
<div class="lyrico-lyrics-wrapper">kaayam indri velbavalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaayam indri velbavalo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oru murai ival parkira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru murai ival parkira"/>
</div>
<div class="lyrico-lyrics-wrapper">pothu kothikirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pothu kothikirathe"/>
</div>
<div class="lyrico-lyrics-wrapper">maru murai ival parkira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maru murai ival parkira"/>
</div>
<div class="lyrico-lyrics-wrapper">pothu kulirgirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pothu kulirgirathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kanavula pakura devathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavula pakura devathai"/>
</div>
<div class="lyrico-lyrics-wrapper">nerila vanthathu en kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nerila vanthathu en kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">intha nodiyila vaalura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha nodiyila vaalura"/>
</div>
<div class="lyrico-lyrics-wrapper">inbame pothum vera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inbame pothum vera"/>
</div>
<div class="lyrico-lyrics-wrapper">enna naan paada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna naan paada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kanavula pakura devathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavula pakura devathai"/>
</div>
<div class="lyrico-lyrics-wrapper">nerila vanthathu en kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nerila vanthathu en kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">intha nodiyila vaalura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha nodiyila vaalura"/>
</div>
<div class="lyrico-lyrics-wrapper">inbame pothum vera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inbame pothum vera"/>
</div>
<div class="lyrico-lyrics-wrapper">enna naan paada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna naan paada"/>
</div>
</pre>
