---
title: "azhagha poranthuputa song lyrics"
album: "Siruthai"
artist: "Vidyasagar"
lyricist: "Viveka"
director: "Siva"
path: "/albums/siruthai-lyrics"
song: "Azhagha Poranthuputa"
image: ../../images/albumart/siruthai.jpg
date: 2011-01-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/42IMFXqjHx0"
type: "happy"
singers:
  - Malathy Lakshman
  - Priyadharshini
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Maanam Mariyaadhai Marandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maanam Mariyaadhai Marandhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaasa Udal Thaanam Ketpavarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaasa Udal Thaanam Ketpavarai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thavikkavae Vidugindra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thavikkavae Vidugindra"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eenappazhakkam Thaan Ivalukku Illaiyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eenappazhakkam Thaan Ivalukku Illaiyada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sandhikka Vandhavanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhikka Vandhavanai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pandhikku Azhaippaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pandhikku Azhaippaalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mundhikka Maruththavanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mundhikka Maruththavanai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mullaaga Muraippaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mullaaga Muraippaalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalam Un Thalaiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam Un Thalaiyai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vellaiyadikkumun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vellaiyadikkumun"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kavarchippadam Ivalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavarchippadam Ivalai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kollaiyadi Komaganae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kollaiyadi Komaganae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paapaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paapaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhagaa Porandhupputten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaa Porandhupputten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaradi Sandhana Katta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaradi Sandhana Katta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhagaa Porandhupputtaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaa Porandhupputtaen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaradi Sandhana Katta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaradi Sandhana Katta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adadaa Thaenu Sottaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adadaa Thaenu Sottaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manakkum Lavangappatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manakkum Lavangappatta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aasai Moochu Mutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai Moochu Mutta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irundhum Irukka Yetta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irundhum Irukka Yetta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aasai Moochu Mutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai Moochu Mutta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irundhum Irukka Yetta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irundhum Irukka Yetta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kulichi Mudichi Kulathukkuthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulichi Mudichi Kulathukkuthaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuliru Kaaichchalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuliru Kaaichchalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Parikka Marandha Pookal Saerndhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Parikka Marandha Pookal Saerndhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Podudhu Koochalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podudhu Koochalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ae Podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Podu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alaga Porandhupputta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alaga Porandhupputta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaradi Sandhana Katta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaradi Sandhana Katta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adadaa Thaenu Sottaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adadaa Thaenu Sottaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manakkum Lavanga Pattai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manakkum Lavanga Pattai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaathottam Adhigamulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathottam Adhigamulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaakki Nada Meththai Veedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaakki Nada Meththai Veedu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kandakkanda Idathil Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandakkanda Idathil Ellaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jannalaa Vachuvidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jannalaa Vachuvidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manmadharu Vandhu Pala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manmadharu Vandhu Pala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paadangala Karkkavidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadangala Karkkavidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manasula Aasa Ulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasula Aasa Ulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aalukkellaam Arthamidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalukkellaam Arthamidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Periya Peru Enakkirundhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Periya Peru Enakkirundhum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Periya Peru Enakkirundum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Periya Peru Enakkirundum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chinna Veeduthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna Veeduthaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Periyavanga Kaal Kadukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Periyavanga Kaal Kadukka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninna Veeduthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninna Veeduthaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Singili Gingili Piththappoyindhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Singili Gingili Piththappoyindhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andhira Gongura Thinippo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhira Gongura Thinippo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhaga Porandhupputta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhaga Porandhupputta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaradi Sandhana Katta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaradi Sandhana Katta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adadaa Thenu Sotaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adadaa Thenu Sotaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manakkum Lavangappatai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manakkum Lavangappatai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaaliba Koottamellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaliba Koottamellaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaasalula Kooduthappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaasalula Kooduthappa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Policesu Bandhobasthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Policesu Bandhobasthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pottaalum Adangalappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pottaalum Adangalappa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennoda Kaippesiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennoda Kaippesiyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eppodhum Sinungudhappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppodhum Sinungudhappa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Million Kanakkil Ingae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Million Kanakkil Ingae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Missed Callu Irukkudhappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Missed Callu Irukkudhappa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pazhutha Palam Irukkum idam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pazhutha Palam Irukkum idam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paluththa Palam Irukkum Idam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paluththa Palam Irukkum Idam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paravai Koodumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paravai Koodumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Padikkavandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Padikkavandha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Veedum Pallikkoodamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Veedum Pallikkoodamae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Singili Gingili Piththappoyindhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Singili Gingili Piththappoyindhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andhira Gongura Thinippo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhira Gongura Thinippo"/>
</div>
</pre>
