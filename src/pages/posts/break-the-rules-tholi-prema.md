---
title: "break the rules song lyrics"
album: "Tholi Prema"
artist: "S Thaman"
lyricist: "Sri Mani"
director: "Venky Atluri"
path: "/albums/tholi-prema-lyrics"
song: "Break The Rules"
image: ../../images/albumart/tholi-prema.jpg
date: 2018-02-10
lang: telugu
youtubeLink: "https://www.youtube.com/embed/noqTT3Dntf8"
type: "happy"
singers:
  -	Raghu Dixit
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Break The Rules 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Break The Rules "/>
</div>
<div class="lyrico-lyrics-wrapper">Break The Rules 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Break The Rules "/>
</div>
<div class="lyrico-lyrics-wrapper">Just Break The Rules 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Just Break The Rules "/>
</div>
<div class="lyrico-lyrics-wrapper">Rodasee Lo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rodasee Lo "/>
</div>
<div class="lyrico-lyrics-wrapper">Doosukellaro Keka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Doosukellaro Keka "/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Make The Rules 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Make The Rules "/>
</div>
<div class="lyrico-lyrics-wrapper">Make The Rules
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Make The Rules"/>
</div>
<div class="lyrico-lyrics-wrapper">Lets Make The Rules
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lets Make The Rules"/>
</div>
<div class="lyrico-lyrics-wrapper">Manamu Korukunte 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manamu Korukunte "/>
</div>
<div class="lyrico-lyrics-wrapper">Dorike Chavela 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dorike Chavela "/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Sodium Radium 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Sodium Radium "/>
</div>
<div class="lyrico-lyrics-wrapper">Rodiam Helium 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rodiam Helium "/>
</div>
<div class="lyrico-lyrics-wrapper">Barium Thorium 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Barium Thorium "/>
</div>
<div class="lyrico-lyrics-wrapper">Kundhi Formula 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kundhi Formula "/>
</div>
<div class="lyrico-lyrics-wrapper">Formula Formula Formula 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Formula Formula Formula "/>
</div>
<div class="lyrico-lyrics-wrapper">Mana Paatallo Lyrics 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana Paatallo Lyrics "/>
</div>
<div class="lyrico-lyrics-wrapper">Maatallo Ethics 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maatallo Ethics "/>
</div>
<div class="lyrico-lyrics-wrapper">Gundello Freedom 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundello Freedom "/>
</div>
<div class="lyrico-lyrics-wrapper">Ku Ledu Formula 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ku Ledu Formula "/>
</div>
<div class="lyrico-lyrics-wrapper">Formula Formula Formula 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Formula Formula Formula "/>
</div>
<div class="lyrico-lyrics-wrapper">Kshanaala Jindageelo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kshanaala Jindageelo "/>
</div>
<div class="lyrico-lyrics-wrapper">No Compromise Anela 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="No Compromise Anela "/>
</div>
<div class="lyrico-lyrics-wrapper">Mana Viraga Parugu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana Viraga Parugu "/>
</div>
<div class="lyrico-lyrics-wrapper">Taraga Turagu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Taraga Turagu "/>
</div>
<div class="lyrico-lyrics-wrapper">Thirugu Leni Gola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirugu Leni Gola"/>
</div>
<div class="lyrico-lyrics-wrapper">La La La La La La 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="La La La La La La "/>
</div>
<div class="lyrico-lyrics-wrapper">Lalala La La La 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lalala La La La "/>
</div>
<div class="lyrico-lyrics-wrapper">La La La Lalala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="La La La Lalala"/>
</div>
<div class="lyrico-lyrics-wrapper">La La La La La La 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="La La La La La La "/>
</div>
<div class="lyrico-lyrics-wrapper">Lalala  La La La 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lalala  La La La "/>
</div>
<div class="lyrico-lyrics-wrapper">La La La Lalala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="La La La Lalala"/>
</div>
<div class="lyrico-lyrics-wrapper">Break The Rules 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Break The Rules "/>
</div>
<div class="lyrico-lyrics-wrapper">Break The Rules 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Break The Rules "/>
</div>
<div class="lyrico-lyrics-wrapper">Just Break The Rules 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Just Break The Rules "/>
</div>
<div class="lyrico-lyrics-wrapper">Rodasee Lo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rodasee Lo "/>
</div>
<div class="lyrico-lyrics-wrapper">Doosukellaro Keka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Doosukellaro Keka "/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Make The Rules 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Make The Rules "/>
</div>
<div class="lyrico-lyrics-wrapper">Make The Rules
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Make The Rules"/>
</div>
<div class="lyrico-lyrics-wrapper">Lets Make The Rules
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lets Make The Rules"/>
</div>
<div class="lyrico-lyrics-wrapper">Manamu Korukunte 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manamu Korukunte "/>
</div>
<div class="lyrico-lyrics-wrapper">Dorike Chavela  
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dorike Chavela  "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mohana Muralini 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mohana Muralini "/>
</div>
<div class="lyrico-lyrics-wrapper">Valachina Vaadu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valachina Vaadu "/>
</div>
<div class="lyrico-lyrics-wrapper">Tiyyaga Raadhanu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tiyyaga Raadhanu "/>
</div>
<div class="lyrico-lyrics-wrapper">Pilichina Vaadu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pilichina Vaadu "/>
</div>
<div class="lyrico-lyrics-wrapper">Kammani Velalu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kammani Velalu "/>
</div>
<div class="lyrico-lyrics-wrapper">Kolisena Vaadu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolisena Vaadu "/>
</div>
<div class="lyrico-lyrics-wrapper">Parimala Vanamuna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parimala Vanamuna "/>
</div>
<div class="lyrico-lyrics-wrapper">Priyamagu Vaadu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Priyamagu Vaadu "/>
</div>
<div class="lyrico-lyrics-wrapper">Chinni Krishnudu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinni Krishnudu "/>
</div>
<div class="lyrico-lyrics-wrapper">Maa Chetikandaadu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa Chetikandaadu "/>
</div>
<div class="lyrico-lyrics-wrapper">Chilipi Krishnudu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chilipi Krishnudu "/>
</div>
<div class="lyrico-lyrics-wrapper">Maa Manasu Vadaladu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa Manasu Vadaladu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinni Krishnudu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinni Krishnudu "/>
</div>
<div class="lyrico-lyrics-wrapper">Maa Chetikandaadu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa Chetikandaadu "/>
</div>
<div class="lyrico-lyrics-wrapper">Chilipi Krishnudu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chilipi Krishnudu "/>
</div>
<div class="lyrico-lyrics-wrapper">Maa Manasu Vadaladu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa Manasu Vadaladu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hare Hare Muraare 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hare Hare Muraare "/>
</div>
<div class="lyrico-lyrics-wrapper">Hare Hare Muraare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hare Hare Muraare"/>
</div>
<div class="lyrico-lyrics-wrapper">Hare Hare Muraare 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hare Hare Muraare "/>
</div>
<div class="lyrico-lyrics-wrapper">Hare Hare Muraare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hare Hare Muraare"/>
</div>
<div class="lyrico-lyrics-wrapper">La La La La La La 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="La La La La La La "/>
</div>
<div class="lyrico-lyrics-wrapper">Lalala La La La 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lalala La La La "/>
</div>
<div class="lyrico-lyrics-wrapper">La La La Lalala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="La La La Lalala"/>
</div>
<div class="lyrico-lyrics-wrapper">La La La La La La Lalala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="La La La La La La Lalala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Claas Room Lo Bench 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Claas Room Lo Bench "/>
</div>
<div class="lyrico-lyrics-wrapper">Ke Atthukku Pokura 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ke Atthukku Pokura "/>
</div>
<div class="lyrico-lyrics-wrapper">Rekkale Vippi Choodaraa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rekkale Vippi Choodaraa "/>
</div>
<div class="lyrico-lyrics-wrapper">O Rank Kosam Potine 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Rank Kosam Potine "/>
</div>
<div class="lyrico-lyrics-wrapper">Kaasepu Aapara 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasepu Aapara "/>
</div>
<div class="lyrico-lyrics-wrapper">Romance Kee Space Ivvaraa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romance Kee Space Ivvaraa "/>
</div>
<div class="lyrico-lyrics-wrapper">Teey Paradaa Chey Saradaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teey Paradaa Chey Saradaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Veligi Podaa Kalala Paradaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veligi Podaa Kalala Paradaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Fire Ki Ice Ki 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Fire Ki Ice Ki "/>
</div>
<div class="lyrico-lyrics-wrapper">Neeruki Joruki Speed 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeruki Joruki Speed "/>
</div>
<div class="lyrico-lyrics-wrapper">Ki Undoka Formula 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ki Undoka Formula "/>
</div>
<div class="lyrico-lyrics-wrapper">Manalle Theguva Pogaru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manalle Theguva Pogaru "/>
</div>
<div class="lyrico-lyrics-wrapper">Jiguru Vagaru Ki 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jiguru Vagaru Ki "/>
</div>
<div class="lyrico-lyrics-wrapper">Ledanta Formula 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ledanta Formula "/>
</div>
<div class="lyrico-lyrics-wrapper">Yugaala Yuvataramlo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yugaala Yuvataramlo "/>
</div>
<div class="lyrico-lyrics-wrapper">Saraina History La 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saraina History La "/>
</div>
<div class="lyrico-lyrics-wrapper">Mana Viraga Parugu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana Viraga Parugu "/>
</div>
<div class="lyrico-lyrics-wrapper">Taraga Turaga Thirugu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Taraga Turaga Thirugu "/>
</div>
<div class="lyrico-lyrics-wrapper">Leni Golaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Leni Golaa "/>
</div>
<div class="lyrico-lyrics-wrapper">La La La La La La 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="La La La La La La "/>
</div>
<div class="lyrico-lyrics-wrapper">Lalala La La La 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lalala La La La "/>
</div>
<div class="lyrico-lyrics-wrapper">La La La Lalala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="La La La Lalala"/>
</div>
<div class="lyrico-lyrics-wrapper">La La La La La La 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="La La La La La La "/>
</div>
<div class="lyrico-lyrics-wrapper">Lalala  La La La 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lalala  La La La "/>
</div>
<div class="lyrico-lyrics-wrapper">La La La Lalala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="La La La Lalala"/>
</div>
<div class="lyrico-lyrics-wrapper">Break The Rules 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Break The Rules "/>
</div>
<div class="lyrico-lyrics-wrapper">Break The Rules 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Break The Rules "/>
</div>
<div class="lyrico-lyrics-wrapper">Just Break The Rules 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Just Break The Rules "/>
</div>
<div class="lyrico-lyrics-wrapper">Rodasee Lo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rodasee Lo "/>
</div>
<div class="lyrico-lyrics-wrapper">Doosukellaro Keka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Doosukellaro Keka "/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Make The Rules 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Make The Rules "/>
</div>
<div class="lyrico-lyrics-wrapper">Make The Rules
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Make The Rules"/>
</div>
<div class="lyrico-lyrics-wrapper">Lets Make The Rules
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lets Make The Rules"/>
</div>
<div class="lyrico-lyrics-wrapper">Manamu Korukunte 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manamu Korukunte "/>
</div>
<div class="lyrico-lyrics-wrapper">Dorike Chavela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dorike Chavela"/>
</div>
</pre>
