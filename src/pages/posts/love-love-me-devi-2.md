---
title: "love love me song lyrics"
album: "Devi 2"
artist: "Sam C. S."
lyricist: "Madhan Karky"
director: "A.L. Vijay"
path: "/albums/devi-2-lyrics"
song: "Love Love Me"
image: ../../images/albumart/devi-2.jpg
date: 2019-05-31
lang: tamil
youtubeLink: "https://www.youtube.com/embed/AgC_yAjgLi8"
type: "love"
singers:
  - Sam C. S.
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Love Love Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love Love Me"/>
</div>
<div class="lyrico-lyrics-wrapper">Love Miss Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love Miss Me"/>
</div>
<div class="lyrico-lyrics-wrapper">Love Hit Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love Hit Me"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennodu Vaa Vaa Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennodu Vaa Vaa Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhage Vaa Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhage Vaa Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Love Love Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love Love Me"/>
</div>
<div class="lyrico-lyrics-wrapper">Love Miss Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love Miss Me"/>
</div>
<div class="lyrico-lyrics-wrapper">Love Hit Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love Hit Me"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennodu Vaa Vaa Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennodu Vaa Vaa Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhage Vaa Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhage Vaa Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Note My Phone Number
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Note My Phone Number"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Oru Murai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Oru Murai"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Murai Anumathi Nuzhaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Murai Anumathi Nuzhaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhi Vazhi Idhazh Vazhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhi Vazhi Idhazh Vazhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakulle Varavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakulle Varavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mirattinaal Virattinaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mirattinaal Virattinaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Bayandhu Naan Povene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bayandhu Naan Povene"/>
</div>
<div class="lyrico-lyrics-wrapper">Urukkamaai Nerukkamaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urukkamaai Nerukkamaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Manaththai Naan Solvene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manaththai Naan Solvene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ulle Thudikudhu Thudikudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulle Thudikudhu Thudikudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasai Vedikudhu Vedikudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai Vedikudhu Vedikudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Edho Nadakudhu Nadakudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edho Nadakudhu Nadakudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadakadhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadakadhadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Penne Note My Phone Number
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Penne Note My Phone Number"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Thayanginen Thayanginen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Thayanginen Thayanginen"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudhal Vari Uraikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhal Vari Uraikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayanginen Kiranginen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayanginen Kiranginen"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagi Nee Muraikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagi Nee Muraikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adikkadi Unakku Pin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adikkadi Unakku Pin"/>
</div>
<div class="lyrico-lyrics-wrapper">Alaigiren Odadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alaigiren Odadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirakkathaan Thudikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirakkathaan Thudikkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Maraithu Nee Moodadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maraithu Nee Moodadhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kangal Enai Enai Erikudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangal Enai Enai Erikudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaayo Siridhena Sirikudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaayo Siridhena Sirikudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanam Enakulle Parakudhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam Enakulle Parakudhadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Penne Note My Phone Number
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Penne Note My Phone Number"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Love Love Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love Love Me"/>
</div>
<div class="lyrico-lyrics-wrapper">Love Miss Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love Miss Me"/>
</div>
<div class="lyrico-lyrics-wrapper">Love Hit Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love Hit Me"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennodu Va Va Va
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennodu Va Va Va"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhage Vaa Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhage Vaa Nee"/>
</div>
</pre>
