---
title: "siru siru kanavugal song lyrics"
album: "180"
artist: "Sharreth"
lyricist: "Madhan Karky"
director: "Jayendra Panchapakesan"
path: "/albums/180-lyrics"
song: "Siru Siru Kanavugal"
image: ../../images/albumart/180.jpg
date: 2011-06-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/N2iJnP2peB8"
type: "mass"
singers:
  - S. Vidhyashankar
  - Master Aswath
  - P. Ajith
  - Master Sharath
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Thuru Thuru Kannil Thuru Neengum Bothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuru Thuru Kannil Thuru Neengum Bothu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Siru Siru Kanavugal Siragu Soodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru Siru Kanavugal Siragu Soodum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thuru Thuru Kangalil Siru Siru Kanavugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuru Thuru Kangalil Siru Siru Kanavugal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naalai Podum Seithithaalil Endhan Peyarai Kaattumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalai Podum Seithithaalil Endhan Peyarai Kaattumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ellai Thaandi Nobal Parisu En Kai Kettumey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellai Thaandi Nobal Parisu En Kai Kettumey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Noyillatha Bhoomi Panthondrai Naanae Kattuvaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Noyillatha Bhoomi Panthondrai Naanae Kattuvaen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Annai Kannil Inbam Undaakka Vin Muttuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annai Kannil Inbam Undaakka Vin Muttuven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thuru Thuru Kannil Thuru  Neengum Bothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuru Thuru Kannil Thuru  Neengum Bothu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Siru Siru Kanavugal Siragu Soodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru Siru Kanavugal Siragu Soodum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thuru Thuru Kangalil Siru Siru Kanavugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuru Thuru Kangalil Siru Siru Kanavugal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Puthiya Puthiya Ulagam Veyndaamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthiya Puthiya Ulagam Veyndaamey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neytrulagam Naan Kaanbaen Thoosilla Poongaatriley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neytrulagam Naan Kaanbaen Thoosilla Poongaatriley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mazhaigal Vizha Visai Seivaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhaigal Vizha Visai Seivaen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhigal Azha Thadai Poduvaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhigal Azha Thadai Poduvaen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanavai Vithai Ena Puthaikiraen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavai Vithai Ena Puthaikiraen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thuru Thuru Kannil Thuru Neengum bothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuru Thuru Kannil Thuru Neengum bothu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Siru Siru Kanavugal Siragu Soodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru Siru Kanavugal Siragu Soodum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thuru Thuru Kangalil Siru Siru Kanavugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuru Thuru Kangalil Siru Siru Kanavugal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thiraiyaal Moodum Pothum Vinnil Thee Maraivathillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiraiyaal Moodum Pothum Vinnil Thee Maraivathillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pasiyaal vaadumbothum Kannil Thee Kuraivathillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pasiyaal vaadumbothum Kannil Thee Kuraivathillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pala Naal Irulum Oru Naal Surulum Enavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala Naal Irulum Oru Naal Surulum Enavey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Marulum Manathil Oliyaai Thiralum Kanavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marulum Manathil Oliyaai Thiralum Kanavey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thuru Thuru Kannil Thuru Neengum Bothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuru Thuru Kannil Thuru Neengum Bothu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Siru Siru Kanavugal Siragu Soodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru Siru Kanavugal Siragu Soodum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thuru Thuru Kangalil Siru Siru Kanavugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuru Thuru Kangalil Siru Siru Kanavugal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanavellaam Koodumey Kaigal Koodum Veylaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavellaam Koodumey Kaigal Koodum Veylaiyil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irulellaam Theeyumey Theeyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irulellaam Theeyumey Theeyil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanavellaam Koodumey Kaigal Koodum Veylaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavellaam Koodumey Kaigal Koodum Veylaiyil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irulellaam Theeyumey Theeyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irulellaam Theeyumey Theeyil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanavellaam Koodumey Kaigal Koodum Veylaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavellaam Koodumey Kaigal Koodum Veylaiyil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irulellaam Theeyumey Theeyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irulellaam Theeyumey Theeyil"/>
</div>
</pre>
