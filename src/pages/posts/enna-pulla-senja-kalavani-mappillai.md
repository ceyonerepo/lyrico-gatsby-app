---
title: "enna pulla senja song lyrics"
album: "Kalavani Mappillai"
artist: "NR Raghunanthan"
lyricist: "Mohan Rajan"
director: "Gandhi Manivasagam"
path: "/albums/kalavani-mappillai-lyrics"
song: "Enna Pulla Senja"
image: ../../images/albumart/kalavani-mappillai.jpg
date: 2018-11-06
lang: tamil
youtubeLink: "https://www.youtube.com/embed/KhUnQGpA9TM"
type: "love"
singers:
  - Ranjith
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Enna pulla senja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna pulla senja"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee enna pulla senja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee enna pulla senja"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinna pulla pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna pulla pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai nikka vechu senja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai nikka vechu senja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna pulla senja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna pulla senja"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee enna pulla senja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee enna pulla senja"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinna pulla pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna pulla pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai nikka vechu senja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai nikka vechu senja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai kandathum kaadhalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai kandathum kaadhalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppadi vanthuchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppadi vanthuchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjatha ketten di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjatha ketten di"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai innikku paarthatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai innikku paarthatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Pola thaan anbula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pola thaan anbula"/>
</div>
<div class="lyrico-lyrics-wrapper">Yennikkum paapendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yennikkum paapendi"/>
</div>
<div class="lyrico-lyrics-wrapper">Un pakkathil irukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un pakkathil irukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ovvaru nodiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ovvaru nodiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai tholaichendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai tholaichendi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna pulla senja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna pulla senja"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee enna pulla senja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee enna pulla senja"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinna pulla pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna pulla pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai nikka vechu senja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai nikka vechu senja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ethanai suriyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethanai suriyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethanai santhiran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethanai santhiran"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarvayil vachi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarvayil vachi "/>
</div>
<div class="lyrico-lyrics-wrapper">nee paakkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee paakkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Athanai minnalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athanai minnalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Athanai innalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athanai innalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjila thanthu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjila thanthu "/>
</div>
<div class="lyrico-lyrics-wrapper">nee kokkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee kokkura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ethanai suriyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethanai suriyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethanai santhiran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethanai santhiran"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarvayil vachi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarvayil vachi "/>
</div>
<div class="lyrico-lyrics-wrapper">nee paakkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee paakkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Athanai minnalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athanai minnalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Athanai innalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athanai innalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjila thanthu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjila thanthu "/>
</div>
<div class="lyrico-lyrics-wrapper">nee kokkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee kokkura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee kollura kollura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kollura kollura"/>
</div>
<div class="lyrico-lyrics-wrapper">Kollura kannaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kollura kannaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee allura allura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee allura allura"/>
</div>
<div class="lyrico-lyrics-wrapper">Allura anbaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allura anbaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai paarthathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai paarthathum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai kaanala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai kaanala"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna aanatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna aanatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Onnum thonala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnum thonala"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kannadi pattadhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kannadi pattadhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Salladai aayittendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Salladai aayittendi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna pulla senja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna pulla senja"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee enna pulla senja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee enna pulla senja"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinna pulla pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna pulla pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai nikka vechu senja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai nikka vechu senja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai inganaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai inganaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithanai manikku paapennu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithanai manikku paapennu"/>
</div>
<div class="lyrico-lyrics-wrapper">Josiyam sollala di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Josiyam sollala di"/>
</div>
<div class="lyrico-lyrics-wrapper">Un athanai azhaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un athanai azhaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithanai pakkathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithanai pakkathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarthathum thangala di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarthathum thangala di"/>
</div>
<div class="lyrico-lyrics-wrapper">En raagu kaalathaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En raagu kaalathaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalla nerama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla nerama"/>
</div>
<div class="lyrico-lyrics-wrapper">Mathuna pochandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mathuna pochandi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna pulla senja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna pulla senja"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee enna pulla senja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee enna pulla senja"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinna pulla pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna pulla pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai nikka vechu senja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai nikka vechu senja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thana nana thana nana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana nana thana nana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana nana thana nana naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana nana thana nana naa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thana nana thana nana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana nana thana nana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana nana thana nana naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana nana thana nana naa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Petha thaayi kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Petha thaayi kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Potti poda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Potti poda"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru sandaikariya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru sandaikariya "/>
</div>
<div class="lyrico-lyrics-wrapper">naan paathutten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan paathutten"/>
</div>
<div class="lyrico-lyrics-wrapper">Sonna pecha kettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sonna pecha kettu"/>
</div>
<div class="lyrico-lyrics-wrapper">Bommai pola aada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bommai pola aada"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippovae othiga paathutten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippovae othiga paathutten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Petha thaayi kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Petha thaayi kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Potti poda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Potti poda"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru sandaikariya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru sandaikariya "/>
</div>
<div class="lyrico-lyrics-wrapper">naan paathutten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan paathutten"/>
</div>
<div class="lyrico-lyrics-wrapper">Sonna pecha kettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sonna pecha kettu"/>
</div>
<div class="lyrico-lyrics-wrapper">Bommai pola aada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bommai pola aada"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippovae othiga paathutten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippovae othiga paathutten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee nikkira nikkira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee nikkira nikkira"/>
</div>
<div class="lyrico-lyrics-wrapper">Nikkira nenjellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikkira nenjellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan suthuren suthuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan suthuren suthuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Suthuren oorellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suthuren oorellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha rathiri konjam neelanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha rathiri konjam neelanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha suriyan konjam thunganum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha suriyan konjam thunganum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee paakatha kekatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee paakatha kekatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Poi nooru sollanum di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poi nooru sollanum di"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna pulla senja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna pulla senja"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee enna pulla senja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee enna pulla senja"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinna pulla pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna pulla pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai nikka vechu senja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai nikka vechu senja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai kandathum kaadhalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai kandathum kaadhalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppadi vanthuchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppadi vanthuchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjatha ketten di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjatha ketten di"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai innikku paarthatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai innikku paarthatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Pola thaan anbula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pola thaan anbula"/>
</div>
<div class="lyrico-lyrics-wrapper">Yennikkum paapendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yennikkum paapendi"/>
</div>
<div class="lyrico-lyrics-wrapper">Un pakkathil irukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un pakkathil irukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ovvaru nodiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ovvaru nodiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai tholaichendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai tholaichendi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna pulla senja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna pulla senja"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee enna pulla senja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee enna pulla senja"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinna pulla pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna pulla pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai nikka vechu senja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai nikka vechu senja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna pulla senja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna pulla senja"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee enna pulla senja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee enna pulla senja"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinna pulla pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna pulla pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai nikka vechu senja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai nikka vechu senja"/>
</div>
</pre>
