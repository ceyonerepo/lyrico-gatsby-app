---
title: "maalai mangum neram song lyrics"
album: "Rowthiram"
artist: "Prakash Nikki"
lyricist: "Thamarai"
director: "Gokul"
path: "/albums/rowthiram-lyrics"
song: "Maalai Mangum Neram"
image: ../../images/albumart/rowthiram.jpg
date: 2011-08-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/hfsFOP3TYVQ"
type: "love"
singers:
  - Ranina Reddy
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Maalai Mangum Neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maalai Mangum Neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Mogam Kannin Oram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Mogam Kannin Oram"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Paarthu Kondae Nindraalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Paarthu Kondae Nindraalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Podhum Endru Thondrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhum Endru Thondrum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalai Vandhaal Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalai Vandhaal Enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Veyil Etti Paarthaal Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veyil Etti Paarthaal Enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Gadikaaram Kaattum Neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gadikaaram Kaattum Neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhai Namba Matten Naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai Namba Matten Naanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poongaattrum Porvai Ketkum Neram Neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poongaattrum Porvai Ketkum Neram Neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeyaai Maarum Dhegam Dhegam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeyaai Maarum Dhegam Dhegam"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kaigal Ennai Thottu Kolam Podum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kaigal Ennai Thottu Kolam Podum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhvin Ellai Thedum Thedum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvin Ellai Thedum Thedum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maalai Mangum Neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maalai Mangum Neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Mogam Kannin Oram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Mogam Kannin Oram"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Paarthu Kondae Nindraalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Paarthu Kondae Nindraalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Podhum Endru Thondrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhum Endru Thondrum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Veetil Naam Irunthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Veetil Naam Irunthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Orr Ilaiyil Nam Virunthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orr Ilaiyil Nam Virunthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Iru Thookkam Oru Kanavil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iru Thookkam Oru Kanavil"/>
</div>
<div class="lyrico-lyrics-wrapper">Moozhgi Vaazhkai Thodangum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moozhgi Vaazhkai Thodangum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Samaiyal Seidhiduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Samaiyal Seidhiduven"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Vandhu Anaithiduvaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vandhu Anaithiduvaai"/>
</div>
<div class="lyrico-lyrics-wrapper">En Pasiyum Un Pasiyum Sernthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Pasiyum Un Pasiyum Sernthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ondraai Adangum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondraai Adangum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Kettu Aasai Patta Paadal Nooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Kettu Aasai Patta Paadal Nooru"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum Naanum Sernthae Ketpom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Naanum Sernthae Ketpom"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaalaattai Kannil Sonna Aanum Nee Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaalaattai Kannil Sonna Aanum Nee Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam Neram Thaandi Vaazhvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam Neram Thaandi Vaazhvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maalai Mangum Neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maalai Mangum Neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Mogam Kannin Oram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Mogam Kannin Oram"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Paarthu Kondae Nindraalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Paarthu Kondae Nindraalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Podhum Endru Thondrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhum Endru Thondrum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalai Vandhaal Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalai Vandhaal Enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Veyil Etti Paarthaal Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veyil Etti Paarthaal Enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Gadikaaram Kaattum Neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gadikaaram Kaattum Neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhai Namba Matten Naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai Namba Matten Naanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paal Sindhum Pournamiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paal Sindhum Pournamiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Naam Nanaivom Pani Iravil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam Nanaivom Pani Iravil"/>
</div>
<div class="lyrico-lyrics-wrapper">Nam Moochu Kaaichalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam Moochu Kaaichalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha Paniyum Nadungum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Paniyum Nadungum"/>
</div>
<div class="lyrico-lyrics-wrapper">Veedengum Un Porutkal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veedengum Un Porutkal"/>
</div>
<div class="lyrico-lyrics-wrapper">Asainthaadum Un Udaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asainthaadum Un Udaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaniyaaga Naan Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaniyaaga Naan Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Endrae Solli Sinungum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endrae Solli Sinungum"/>
</div>
<div class="lyrico-lyrics-wrapper">Theendaamal Theendi Pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theendaamal Theendi Pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaadai Kaatrae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadai Kaatrae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thookkam Theernthu Naatkal Aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thookkam Theernthu Naatkal Aachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Vaasam Ennil Pattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Vaasam Ennil Pattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaadi Ponen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadi Ponen"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaasal Thoonaai Naanum Aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaasal Thoonaai Naanum Aanen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maalai Mangum Neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maalai Mangum Neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Mogam Kannin Oram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Mogam Kannin Oram"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Paarthu Kondae Nindraalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Paarthu Kondae Nindraalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Podhum Endru Thondrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhum Endru Thondrum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalai Vandhaal Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalai Vandhaal Enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Veyil Etti Paarthaal Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veyil Etti Paarthaal Enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Gadikaaram Kaattum Neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gadikaaram Kaattum Neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhai Namba Matten Naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai Namba Matten Naanum"/>
</div>
</pre>
