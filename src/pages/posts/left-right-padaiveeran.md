---
title: "left right song lyrics"
album: "Padaiveeran"
artist: "Karthik Raja"
lyricist: "Dhana Sekaran"
director: "Dhana Sekaran"
path: "/albums/padaiveeran-lyrics"
song: "Left Right"
image: ../../images/albumart/padaiveeran.jpg
date: 2018-02-02
lang: tamil
youtubeLink: "https://www.youtube.com/embed/pniU8i6swWo"
type: "mass"
singers:
  - Karthik Raja
  - Dhana Sekaran
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Left right left right
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Left right left right"/>
</div>
<div class="lyrico-lyrics-wrapper">Left right left right
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Left right left right"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Left right adichu paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Left right adichu paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Kambi kathava odachi paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kambi kathava odachi paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Parakka parakka vaanam paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parakka parakka vaanam paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuthikka kuthikka aazham paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuthikka kuthikka aazham paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Odi odi ulagam paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odi odi ulagam paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Onjidaama vaazhka paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onjidaama vaazhka paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaaval kaaran dress ah paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaval kaaran dress ah paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Kambu suthi kaaval paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kambu suthi kaaval paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thotta vedikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thotta vedikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Gun’nu paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gun’nu paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Thotta setharum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thotta setharum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sound’ah paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sound’ah paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thotta vedikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thotta vedikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Gun’nu paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gun’nu paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Thotta setharum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thotta setharum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sound’ah paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sound’ah paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Police kaaran midukka paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Police kaaran midukka paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Potta theriyum thaarumaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Potta theriyum thaarumaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Salichu pochu pazhaiya soru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Salichu pochu pazhaiya soru"/>
</div>
<div class="lyrico-lyrics-wrapper">Pothum saami pozhappa paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pothum saami pozhappa paaru"/>
</div>
</pre>
