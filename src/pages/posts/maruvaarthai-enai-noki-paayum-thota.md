---
title: "maruvaarthai song lyrics"
album: "Enai Noki Paayum Thota"
artist: "Darbuka Siva"
lyricist: "Thamarai"
director: "Gautham Vasudev Menon"
path: "/albums/enai-noki-paayum-thota-lyrics"
song: "Maruvaarthai"
image: ../../images/albumart/enai-noki-paayum-thota.jpg
date: 2019-11-29
lang: tamil
youtubeLink: "https://www.youtube.com/embed/U3lyojCm6jA"
type: "love"
singers:
  - Sid Sriram
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Maru varthai pesathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maru varthai pesathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Madimeedhu nee thoongidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madimeedhu nee thoongidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Imai pola naankaakha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imai pola naankaakha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavai nee maridu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavai nee maridu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mayil thogai polle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayil thogai polle"/>
</div>
<div class="lyrico-lyrics-wrapper">Viral unnai varudum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viral unnai varudum"/>
</div>
<div class="lyrico-lyrics-wrapper">Manappaadamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manappaadamai"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyraiyadal nigazhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyraiyadal nigazhum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhi neerum veenaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhi neerum veenaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Imaithanda koodathena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imaithanda koodathena"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuliyaaga naan serththen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuliyaaga naan serththen"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadalaaga kannaanadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadalaaga kannaanadhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maranthaalum naan unnai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maranthaalum naan unnai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninaikkaadha naal illaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaikkaadha naal illaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Pirinthalum en anbu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirinthalum en anbu"/>
</div>
<div class="lyrico-lyrics-wrapper">Orupodhum poi illaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orupodhum poi illaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidiyadha kaalaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidiyadha kaalaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudiyaadha maalaigalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudiyaadha maalaigalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Vadiyadha vervai thuligal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadiyadha vervai thuligal"/>
</div>
<div class="lyrico-lyrics-wrapper">Piriyadha porvai nodigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Piriyadha porvai nodigal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manikaattum kadigaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manikaattum kadigaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharumvaathai arindhom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharumvaathai arindhom"/>
</div>
<div class="lyrico-lyrics-wrapper">Udaimaatrum idaivelai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udaimaatrum idaivelai"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhan pinbe unarndhom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhan pinbe unarndhom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maravathey manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maravathey manam"/>
</div>
<div class="lyrico-lyrics-wrapper">Madinthalum varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madinthalum varum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muthal nee mudivum nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthal nee mudivum nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Alar nee Agilam nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alar nee Agilam nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tholaidhooram sendralum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholaidhooram sendralum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoduvaanam endralum nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoduvaanam endralum nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhioramthaane marainthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhioramthaane marainthaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirodu munbe kalanthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirodu munbe kalanthaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhal ennum malar kondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhal ennum malar kondu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadidhangal varaindhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadidhangal varaindhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Badhil naanum tharum munbe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Badhil naanum tharum munbe"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavaagi kalainthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavaagi kalainthai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pidivadham pidi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pidivadham pidi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sinam theerum adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sinam theerum adi"/>
</div>
<div class="lyrico-lyrics-wrapper">Izhandhom ezhilkolam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Izhandhom ezhilkolam"/>
</div>
<div class="lyrico-lyrics-wrapper">Inimel mazhaikalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inimel mazhaikalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maruvarthai pesadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maruvarthai pesadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Madimeedhu nee thoongidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madimeedhu nee thoongidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Imai pola naankaakha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imai pola naankaakha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavai nee maridu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavai nee maridu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mayil thogai polle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayil thogai polle"/>
</div>
<div class="lyrico-lyrics-wrapper">Viral unnai varudum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viral unnai varudum"/>
</div>
<div class="lyrico-lyrics-wrapper">Manappaadamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manappaadamai"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyraiyadal nigazhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyraiyadal nigazhum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhi neerum veenaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhi neerum veenaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Imaithanda koodadhena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imaithanda koodadhena"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuliyaga naan serthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuliyaga naan serthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadalaaga kannaanathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadalaaga kannaanathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Marandhaalum naan unnai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marandhaalum naan unnai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninaikkadha naal illaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaikkadha naal illaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Pirinthalum en anbu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirinthalum en anbu"/>
</div>
<div class="lyrico-lyrics-wrapper">Orupodhum poi illaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orupodhum poi illaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maru varththai pesaadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maru varththai pesaadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Madimeedhu nee thoongidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madimeedhu nee thoongidu"/>
</div>
</pre>
