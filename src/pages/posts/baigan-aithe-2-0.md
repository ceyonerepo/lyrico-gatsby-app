---
title: "baigan song lyrics"
album: "Aithe 2.0"
artist: "Arun Chiluveru"
lyricist: "Kittu Vissapragada"
director: "Raj Madiraju"
path: "/albums/aithe-2-0-lyrics"
song: "Baigan"
image: ../../images/albumart/aithe-2-0.jpg
date: 2018-03-16
lang: telugu
youtubeLink: "https://www.youtube.com/embed/eR8XMkeSF2Y"
type: "happy"
singers:
  -	Dev Negi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">inter daati eamcet rasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inter daati eamcet rasi"/>
</div>
<div class="lyrico-lyrics-wrapper">fancy ranke vaste
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="fancy ranke vaste"/>
</div>
<div class="lyrico-lyrics-wrapper">happy days cinema choosi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="happy days cinema choosi"/>
</div>
<div class="lyrico-lyrics-wrapper">btech join aipothe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="btech join aipothe"/>
</div>
<div class="lyrico-lyrics-wrapper">real life lo dirty dayse
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="real life lo dirty dayse"/>
</div>
<div class="lyrico-lyrics-wrapper">btech choopistunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="btech choopistunte"/>
</div>
<div class="lyrico-lyrics-wrapper">semester la arrearlanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="semester la arrearlanni"/>
</div>
<div class="lyrico-lyrics-wrapper">courierlai padutunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="courierlai padutunte"/>
</div>
<div class="lyrico-lyrics-wrapper">emotion lo fees lu katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="emotion lo fees lu katti"/>
</div>
<div class="lyrico-lyrics-wrapper">confusion lo slippulu petti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="confusion lo slippulu petti"/>
</div>
<div class="lyrico-lyrics-wrapper">commotion lo future nette
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="commotion lo future nette"/>
</div>
<div class="lyrico-lyrics-wrapper">gajibiji engineering nanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gajibiji engineering nanne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">baingain mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="baingain mein"/>
</div>
<div class="lyrico-lyrics-wrapper">baingan mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="baingan mein"/>
</div>
<div class="lyrico-lyrics-wrapper">bainagan mein mila diya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bainagan mein mila diya"/>
</div>
<div class="lyrico-lyrics-wrapper">baingain mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="baingain mein"/>
</div>
<div class="lyrico-lyrics-wrapper">baingan mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="baingan mein"/>
</div>
<div class="lyrico-lyrics-wrapper">bainagan mein mila diya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bainagan mein mila diya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">attendence class room
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="attendence class room"/>
</div>
<div class="lyrico-lyrics-wrapper">kannaa canteen lo padutunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannaa canteen lo padutunte"/>
</div>
<div class="lyrico-lyrics-wrapper">librareello bookkulu tappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="librareello bookkulu tappa"/>
</div>
<div class="lyrico-lyrics-wrapper">annee dorukuthu unte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="annee dorukuthu unte"/>
</div>
<div class="lyrico-lyrics-wrapper">lyaabulllona computer lu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lyaabulllona computer lu"/>
</div>
<div class="lyrico-lyrics-wrapper">out dated ayyi unte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="out dated ayyi unte"/>
</div>
<div class="lyrico-lyrics-wrapper">gate kaada watchman gaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gate kaada watchman gaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">mana senior ayi unte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mana senior ayi unte"/>
</div>
<div class="lyrico-lyrics-wrapper">rotation lo nightout cheesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rotation lo nightout cheesi"/>
</div>
<div class="lyrico-lyrics-wrapper">frustrationlo exams raasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="frustrationlo exams raasi"/>
</div>
<div class="lyrico-lyrics-wrapper">depression lo jutte raalche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="depression lo jutte raalche"/>
</div>
<div class="lyrico-lyrics-wrapper">gajibiji engineering nanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gajibiji engineering nanne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">baingain mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="baingain mein"/>
</div>
<div class="lyrico-lyrics-wrapper">baingan mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="baingan mein"/>
</div>
<div class="lyrico-lyrics-wrapper">bainagan mein mila diya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bainagan mein mila diya"/>
</div>
<div class="lyrico-lyrics-wrapper">baingain mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="baingain mein"/>
</div>
<div class="lyrico-lyrics-wrapper">baingan mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="baingan mein"/>
</div>
<div class="lyrico-lyrics-wrapper">bainagan mein mila diya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bainagan mein mila diya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">edustu gadiche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edustu gadiche"/>
</div>
<div class="lyrico-lyrics-wrapper">semesterlu edu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="semesterlu edu"/>
</div>
<div class="lyrico-lyrics-wrapper">mottangaa internals
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mottangaa internals"/>
</div>
<div class="lyrico-lyrics-wrapper">one fifty rendu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="one fifty rendu"/>
</div>
<div class="lyrico-lyrics-wrapper">apaina extrenals
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="apaina extrenals"/>
</div>
<div class="lyrico-lyrics-wrapper">seventy rendu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seventy rendu"/>
</div>
<div class="lyrico-lyrics-wrapper">nadimitla supple lu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nadimitla supple lu"/>
</div>
<div class="lyrico-lyrics-wrapper">kottaayi gundu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kottaayi gundu"/>
</div>
<div class="lyrico-lyrics-wrapper">vaivaa lab experiments
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaivaa lab experiments"/>
</div>
<div class="lyrico-lyrics-wrapper">records projects
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="records projects"/>
</div>
<div class="lyrico-lyrics-wrapper">events backlogs
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="events backlogs"/>
</div>
<div class="lyrico-lyrics-wrapper">placements nonsense
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="placements nonsense"/>
</div>
<div class="lyrico-lyrics-wrapper">phookat lo kaanichche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="phookat lo kaanichche"/>
</div>
<div class="lyrico-lyrics-wrapper">feeshersu farewellsu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="feeshersu farewellsu"/>
</div>
<div class="lyrico-lyrics-wrapper">chachchee chedi paasaithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chachchee chedi paasaithe"/>
</div>
<div class="lyrico-lyrics-wrapper">interview fail aithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="interview fail aithe"/>
</div>
<div class="lyrico-lyrics-wrapper">ghajinilaa dandayaatra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ghajinilaa dandayaatra"/>
</div>
<div class="lyrico-lyrics-wrapper">cheskuntu pothunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cheskuntu pothunte"/>
</div>
<div class="lyrico-lyrics-wrapper">benzi vaipu lookkulistu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="benzi vaipu lookkulistu"/>
</div>
<div class="lyrico-lyrics-wrapper">ganji kosam cheyyi chaastu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ganji kosam cheyyi chaastu"/>
</div>
<div class="lyrico-lyrics-wrapper">nightu chukkalerukuntu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nightu chukkalerukuntu"/>
</div>
<div class="lyrico-lyrics-wrapper">lungi bokka lekkaleduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lungi bokka lekkaleduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">base kinda age perigi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="base kinda age perigi"/>
</div>
<div class="lyrico-lyrics-wrapper">veedhulanta appu perigi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veedhulanta appu perigi"/>
</div>
<div class="lyrico-lyrics-wrapper">galli galli lolli petti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="galli galli lolli petti"/>
</div>
<div class="lyrico-lyrics-wrapper">madichi lopalettukunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="madichi lopalettukunna"/>
</div>
<div class="lyrico-lyrics-wrapper">btech certificatu ki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="btech certificatu ki"/>
</div>
<div class="lyrico-lyrics-wrapper">dandam bhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dandam bhai"/>
</div>
</pre>
