---
title: "o chinna navva chaalu song lyrics"
album: "Entha Manchivaadavuraa"
artist: "Gopi Sundar"
lyricist: "Ramajogayya Sastry"
director: "Satish Vegesna"
path: "/albums/entha-manchivaadavuraa-lyrics"
song: "O Chinna Navva Chaalu"
image: ../../images/albumart/entha-manchivaadavuraa.jpg
date: 2020-01-15
lang: telugu
youtubeLink: "https://www.youtube.com/embed/85Ij4HUTqVg"
type: "happy"
singers:
  - Anurag Kulkarni
  - Geetha Madhuri
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">O chinna navve chaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O chinna navve chaalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ala palakaridham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ala palakaridham"/>
</div>
<div class="lyrico-lyrics-wrapper">O Chinna maate chaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Chinna maate chaalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Bandhaalallukundhaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bandhaalallukundhaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye ooru meedhe peru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye ooru meedhe peru"/>
</div>
<div class="lyrico-lyrics-wrapper">Adigi telusukundham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adigi telusukundham"/>
</div>
<div class="lyrico-lyrics-wrapper">Evaraina mana vaarega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evaraina mana vaarega"/>
</div>
<div class="lyrico-lyrics-wrapper">Varasa kalupukundhaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varasa kalupukundhaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oka ningiki oka nelaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oka ningiki oka nelaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Oke oopiri chirugaaliki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oke oopiri chirugaaliki"/>
</div>
<div class="lyrico-lyrics-wrapper">Anubandhamai manamandharam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anubandhamai manamandharam"/>
</div>
<div class="lyrico-lyrics-wrapper">Eeroju kalisindhaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eeroju kalisindhaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa sangathe sarikotthaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa sangathe sarikotthaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Eenadila mari konthagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eenadila mari konthagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana manasuku gurthundaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana manasuku gurthundaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Mallee anukundhaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mallee anukundhaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Dum dum andhamaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dum dum andhamaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Bandham allukunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bandham allukunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Bathuke nithya nandanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bathuke nithya nandanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Chuttu inthamandhi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chuttu inthamandhi "/>
</div>
<div class="lyrico-lyrics-wrapper">chuttalunna vela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chuttalunna vela"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaha entha sambaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaha entha sambaram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanneellakaina santhoshalakaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneellakaina santhoshalakaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Yehe kaavaali thodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yehe kaavaali thodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sadhaa evarikaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sadhaa evarikaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Pushpaka vimanamemari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pushpaka vimanamemari"/>
</div>
<div class="lyrico-lyrics-wrapper">Prathi okkari manasante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prathi okkari manasante"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey anubhandhalennocchina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey anubhandhalennocchina"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhi irukaipodhanthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhi irukaipodhanthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasaina manavaarantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasaina manavaarantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana pakkana koluvai unte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana pakkana koluvai unte"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey korendukinka maredi lede
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey korendukinka maredi lede"/>
</div>
<div class="lyrico-lyrics-wrapper">Prayaanamantha padanisale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prayaanamantha padanisale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dum dum andhamaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dum dum andhamaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Bandham allukunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bandham allukunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Bathuke nithya nandanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bathuke nithya nandanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Chuttu inthamandhi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chuttu inthamandhi "/>
</div>
<div class="lyrico-lyrics-wrapper">chuttalunna vela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chuttalunna vela"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaha entha sambaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaha entha sambaram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maarindhi kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maarindhi kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vegam deeni raagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vegam deeni raagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Daggarlo dhooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daggarlo dhooram"/>
</div>
<div class="lyrico-lyrics-wrapper">Identho pramadam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Identho pramadam"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey panimaala o kaaranam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey panimaala o kaaranam"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhoti vethikeddham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhoti vethikeddham"/>
</div>
<div class="lyrico-lyrics-wrapper">Padimandh kalisetattuga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padimandh kalisetattuga"/>
</div>
<div class="lyrico-lyrics-wrapper">Rojoka pandaga puttiddham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rojoka pandaga puttiddham"/>
</div>
<div class="lyrico-lyrics-wrapper">Apudepudo thelisina vaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Apudepudo thelisina vaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ipudipude kalisina vaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ipudipude kalisina vaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Asalepudu vidavaka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asalepudu vidavaka "/>
</div>
<div class="lyrico-lyrics-wrapper">jatha nadichela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jatha nadichela"/>
</div>
<div class="lyrico-lyrics-wrapper">Ravvantha bathukuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ravvantha bathukuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Rangulu poddhaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangulu poddhaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dum dum andhamaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dum dum andhamaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Bandham allukunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bandham allukunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Bathuke nithya nandanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bathuke nithya nandanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Chuttu inthamandhi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chuttu inthamandhi "/>
</div>
<div class="lyrico-lyrics-wrapper">chuttalunna vela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chuttalunna vela"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaha entha sambaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaha entha sambaram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dum dum andhamaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dum dum andhamaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Bandham allukunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bandham allukunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Bathuke nithya nandanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bathuke nithya nandanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Chuttu inthamandhi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chuttu inthamandhi "/>
</div>
<div class="lyrico-lyrics-wrapper">chuttalunna vela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chuttalunna vela"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaha entha sambaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaha entha sambaram"/>
</div>
</pre>
