---
title: "kaththi vetti song lyrics"
album: "Muthuramalingam"
artist: "Ilaiyaraaja"
lyricist: "Panchu Arunachalam"
director: "Rajadurai"
path: "/albums/muthuramalingam-lyrics"
song: "Kaththi Vetti"
image: ../../images/albumart/muthuramalingam.jpg
date: 2017-02-24
lang: tamil
youtubeLink: "https://www.youtube.com/embed/bqxZBwcDvXg"
type: "love"
singers:
  - S P Balasubrahmanyam
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">kathi vetti kathi vetti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathi vetti kathi vetti"/>
</div>
<div class="lyrico-lyrics-wrapper">kambu suthura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kambu suthura"/>
</div>
<div class="lyrico-lyrics-wrapper">kannukulla kathi vachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannukulla kathi vachu"/>
</div>
<div class="lyrico-lyrics-wrapper">nenja kuthura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenja kuthura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kathi vetti kathi vetti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathi vetti kathi vetti"/>
</div>
<div class="lyrico-lyrics-wrapper">kambu suthura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kambu suthura"/>
</div>
<div class="lyrico-lyrics-wrapper">kannukulla kathi vachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannukulla kathi vachu"/>
</div>
<div class="lyrico-lyrics-wrapper">nenja kuthura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenja kuthura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">unakulla irunthu aetho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unakulla irunthu aetho"/>
</div>
<div class="lyrico-lyrics-wrapper">onnu enna suthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="onnu enna suthu"/>
</div>
<div class="lyrico-lyrics-wrapper">valachathu enna enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valachathu enna enna"/>
</div>
<div class="lyrico-lyrics-wrapper">manasukkul aasai alai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasukkul aasai alai"/>
</div>
<div class="lyrico-lyrics-wrapper">pol purandu karai thottu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pol purandu karai thottu "/>
</div>
<div class="lyrico-lyrics-wrapper">varuvathu enna enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varuvathu enna enna"/>
</div>
<div class="lyrico-lyrics-wrapper">rekka katti rakka katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rekka katti rakka katti"/>
</div>
<div class="lyrico-lyrics-wrapper">vaa maalai muthu katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaa maalai muthu katti"/>
</div>
<div class="lyrico-lyrics-wrapper">muthu katti thaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muthu katti thaa"/>
</div>
<div class="lyrico-lyrics-wrapper">rekka katti rakka katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rekka katti rakka katti"/>
</div>
<div class="lyrico-lyrics-wrapper">vaa maalai muthu katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaa maalai muthu katti"/>
</div>
<div class="lyrico-lyrics-wrapper">muthu katti thaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muthu katti thaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kathi vetti kathi vetti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathi vetti kathi vetti"/>
</div>
<div class="lyrico-lyrics-wrapper">kambu suthura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kambu suthura"/>
</div>
<div class="lyrico-lyrics-wrapper">kannukulla kathi vachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannukulla kathi vachu"/>
</div>
<div class="lyrico-lyrics-wrapper">nenja kuthura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenja kuthura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kathi vetti kathi vetti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathi vetti kathi vetti"/>
</div>
<div class="lyrico-lyrics-wrapper">kambu suthura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kambu suthura"/>
</div>
<div class="lyrico-lyrics-wrapper">kannukulla kathi vachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannukulla kathi vachu"/>
</div>
<div class="lyrico-lyrics-wrapper">nenja kuthura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenja kuthura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kathi vaithu kattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathi vaithu kattum"/>
</div>
<div class="lyrico-lyrics-wrapper">pothu kollum alagile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pothu kollum alagile"/>
</div>
<div class="lyrico-lyrics-wrapper">kandu kandu en manasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kandu kandu en manasu"/>
</div>
<div class="lyrico-lyrics-wrapper">thattu thadangala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thattu thadangala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kathu kitta enna vendru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathu kitta enna vendru"/>
</div>
<div class="lyrico-lyrics-wrapper">sonna puriyuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sonna puriyuma"/>
</div>
<div class="lyrico-lyrics-wrapper">muthathula solli kodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muthathula solli kodu"/>
</div>
<div class="lyrico-lyrics-wrapper">kanne en muthamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanne en muthamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">un kattula mazhai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kattula mazhai "/>
</div>
<div class="lyrico-lyrics-wrapper">peiyuthu en nenjil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="peiyuthu en nenjil "/>
</div>
<div class="lyrico-lyrics-wrapper">eeram kanden kanden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eeram kanden kanden"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">senkatula sernthaduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="senkatula sernthaduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">singara jodi kanden kanden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="singara jodi kanden kanden"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">muthu kolla kathu pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muthu kolla kathu pola"/>
</div>
<div class="lyrico-lyrics-wrapper">paduven da renda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paduven da renda"/>
</div>
<div class="lyrico-lyrics-wrapper">kitta vanthu katti kolla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kitta vanthu katti kolla"/>
</div>
<div class="lyrico-lyrics-wrapper">kaalam ithu neram ithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaalam ithu neram ithu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">rekka katti rakka katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rekka katti rakka katti"/>
</div>
<div class="lyrico-lyrics-wrapper">vaa maalai muthu katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaa maalai muthu katti"/>
</div>
<div class="lyrico-lyrics-wrapper">muthu katti thaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muthu katti thaa"/>
</div>
<div class="lyrico-lyrics-wrapper">rekka katti rakka katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rekka katti rakka katti"/>
</div>
<div class="lyrico-lyrics-wrapper">vaa maalai muthu katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaa maalai muthu katti"/>
</div>
<div class="lyrico-lyrics-wrapper">muthu katti thaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muthu katti thaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kathi katti kathi katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathi katti kathi katti"/>
</div>
<div class="lyrico-lyrics-wrapper">kambu suthura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kambu suthura"/>
</div>
<div class="lyrico-lyrics-wrapper">kannukulla kathi vachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannukulla kathi vachu"/>
</div>
<div class="lyrico-lyrics-wrapper">nenja kuthura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenja kuthura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kathi katti kathi katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathi katti kathi katti"/>
</div>
<div class="lyrico-lyrics-wrapper">kambu suthura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kambu suthura"/>
</div>
<div class="lyrico-lyrics-wrapper">kannukulla kathi vachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannukulla kathi vachu"/>
</div>
<div class="lyrico-lyrics-wrapper">nenja kuthura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenja kuthura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">senthurupu sittinam pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="senthurupu sittinam pol"/>
</div>
<div class="lyrico-lyrics-wrapper">sella kizhi onnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sella kizhi onnu"/>
</div>
<div class="lyrico-lyrics-wrapper">sinna vanam seendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sinna vanam seendi"/>
</div>
<div class="lyrico-lyrics-wrapper">vittu sittai parakuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vittu sittai parakuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">atha magan vaathiyarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="atha magan vaathiyarai"/>
</div>
<div class="lyrico-lyrics-wrapper">yaaruku kedaikumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaaruku kedaikumo"/>
</div>
<div class="lyrico-lyrics-wrapper">en pulappil enna ithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en pulappil enna ithu"/>
</div>
<div class="lyrico-lyrics-wrapper">andre vithichathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="andre vithichathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">mutharamum maniyaramum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mutharamum maniyaramum"/>
</div>
<div class="lyrico-lyrics-wrapper">nan sooda ena ketkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan sooda ena ketkuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kuthalathil kuyil koovuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuthalathil kuyil koovuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">kondadi namai vaalthi paduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kondadi namai vaalthi paduthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kathi kathi aadi varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathi kathi aadi varum"/>
</div>
<div class="lyrico-lyrics-wrapper">velli aaru kooda 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="velli aaru kooda "/>
</div>
<div class="lyrico-lyrics-wrapper">kotukindra aruviyum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kotukindra aruviyum "/>
</div>
<div class="lyrico-lyrics-wrapper">ketti melam kotta kotaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ketti melam kotta kotaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">rekka katti rakka katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rekka katti rakka katti"/>
</div>
<div class="lyrico-lyrics-wrapper">vaa maalai muthu katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaa maalai muthu katti"/>
</div>
<div class="lyrico-lyrics-wrapper">muthu katti thaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muthu katti thaa"/>
</div>
<div class="lyrico-lyrics-wrapper">rekka katti rakka katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rekka katti rakka katti"/>
</div>
<div class="lyrico-lyrics-wrapper">vaa maalai muthu katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaa maalai muthu katti"/>
</div>
<div class="lyrico-lyrics-wrapper">muthu katti thaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muthu katti thaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kathi katti kathi katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathi katti kathi katti"/>
</div>
<div class="lyrico-lyrics-wrapper">kambu suthura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kambu suthura"/>
</div>
<div class="lyrico-lyrics-wrapper">kannukulla kathi vachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannukulla kathi vachu"/>
</div>
<div class="lyrico-lyrics-wrapper">nenja kuthura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenja kuthura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">unakulla iruntha aetho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unakulla iruntha aetho"/>
</div>
<div class="lyrico-lyrics-wrapper">onnu enakulla pugunthathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="onnu enakulla pugunthathu"/>
</div>
<div class="lyrico-lyrics-wrapper">enna enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna enna"/>
</div>
<div class="lyrico-lyrics-wrapper">manasukkul aasai alai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasukkul aasai alai"/>
</div>
<div class="lyrico-lyrics-wrapper">pol purandu karai thatta 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pol purandu karai thatta "/>
</div>
<div class="lyrico-lyrics-wrapper">varuvathu enna enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varuvathu enna enna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">rekka katti rakka katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rekka katti rakka katti"/>
</div>
<div class="lyrico-lyrics-wrapper">vaa maalai muthu katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaa maalai muthu katti"/>
</div>
<div class="lyrico-lyrics-wrapper">muthu katti thaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muthu katti thaa"/>
</div>
<div class="lyrico-lyrics-wrapper">rekka katti rakka katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rekka katti rakka katti"/>
</div>
<div class="lyrico-lyrics-wrapper">vaa maalai muthu katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaa maalai muthu katti"/>
</div>
<div class="lyrico-lyrics-wrapper">muthu katti thaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muthu katti thaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kathi vetti kathi vetti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathi vetti kathi vetti"/>
</div>
<div class="lyrico-lyrics-wrapper">kambu suthura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kambu suthura"/>
</div>
<div class="lyrico-lyrics-wrapper">kannukulla kathi vachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannukulla kathi vachu"/>
</div>
<div class="lyrico-lyrics-wrapper">nenja kuthura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenja kuthura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kathi vetti kathi vetti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathi vetti kathi vetti"/>
</div>
<div class="lyrico-lyrics-wrapper">kambu suthura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kambu suthura"/>
</div>
<div class="lyrico-lyrics-wrapper">kannukulla kathi vachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannukulla kathi vachu"/>
</div>
<div class="lyrico-lyrics-wrapper">nenja kuthura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenja kuthura"/>
</div>
</pre>
