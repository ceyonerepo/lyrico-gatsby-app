---
title: "amma alugiren song lyrics"
album: "Kaali"
artist: "Vijay Antony"
lyricist: "Arun Bharathi"
director: "Kiruthiga Udhayanidhi"
path: "/albums/kaali-lyrics"
song: "Amma Alugiren"
image: ../../images/albumart/kaali.jpg
date: 2018-05-18
lang: tamil
youtubeLink: "https://www.youtube.com/embed/DAQyTv_XaTQ"
type: "sad"
singers:
  - Nivas
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ohoohooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohoohooooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohooooo hooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohooooo hooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Haaahaaa haaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haaahaaa haaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Haaahaaa haaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haaahaaa haaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi vayitril idam koduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi vayitril idam koduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannukkul kaathavalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannukkul kaathavalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan rusiyaai saapidavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan rusiyaai saapidavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinam pasiyinil paduthavalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam pasiyinil paduthavalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thoppul kodi valiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoppul kodi valiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un uyirai kudithen naan amma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un uyirai kudithen naan amma"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan thaan jeyithidavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan thaan jeyithidavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Anudhinamum thotraai neeyamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anudhinamum thotraai neeyamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Kurudhiyinai kuduthavalae thaayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurudhiyinai kuduthavalae thaayae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ammaaaaaluvugiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammaaaaaluvugiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan ammaaaluvugiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan ammaaaluvugiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Ammaaaaaluvugiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammaaaaaluvugiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan ammaaaakatharugiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan ammaaaakatharugiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Panikudam udainthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panikudam udainthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan varugaiyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan varugaiyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadum valiyilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadum valiyilum"/>
</div>
<div class="lyrico-lyrics-wrapper">Siru punnagai purinthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru punnagai purinthaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unviral pidithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unviral pidithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan nadakaiyilaeKaal edaravae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan nadakaiyilaeKaal edaravae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjam padhariyae thudithaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjam padhariyae thudithaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oorum paalai raththamaaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorum paalai raththamaaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Koduthu vittu povaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koduthu vittu povaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Sagum bothum pillaikaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sagum bothum pillaikaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjam vaaduvaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjam vaaduvaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada andradam kondada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada andradam kondada"/>
</div>
<div class="lyrico-lyrics-wrapper">Naam vanthu mandrada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam vanthu mandrada"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaayin paadham irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaayin paadham irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Pala kovil kulangal etharkku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala kovil kulangal etharkku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ammaaaaaluvugiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammaaaaaluvugiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan ammaaaluvugiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan ammaaaluvugiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Ammaaaaaluvugiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammaaaaaluvugiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan ammaaaakatharugiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan ammaaaakatharugiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi vayitril idam koduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi vayitril idam koduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannukkul kaathavalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannukkul kaathavalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan rusiyaai saapidavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan rusiyaai saapidavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinam pasiyinil paduthavalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam pasiyinil paduthavalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thoppul kodi valiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoppul kodi valiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un uyirai kudithen naan amma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un uyirai kudithen naan amma"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan thaan jeyithidavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan thaan jeyithidavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Anudhinamum thotraai neeyamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anudhinamum thotraai neeyamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuridhiyinai kuduthavalae thaayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuridhiyinai kuduthavalae thaayae"/>
</div>
</pre>
