---
title: "main teri ho gayi song lyrics"
album: "Sardar Ka Grandson"
artist: "Tanishk Bagchi"
lyricist: "Millind Gaba - Tanishk Bagchi"
director: "Kaashvie Nair"
path: "/albums/sardar-ka-grandson-lyrics"
song: "Main Teri Ho Gayi"
image: ../../images/albumart/sardar-ka-grandson.jpg
date: 2021-05-18
lang: hindi
youtubeLink: "https://www.youtube.com/embed/508byt6vFfk"
type: "Love"
singers:
  - Millind Gaba
  - Pallavi Gaba
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Mainu teri banke rehna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mainu teri banke rehna"/>
</div>
<div class="lyrico-lyrics-wrapper">Teri hun bas yaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teri hun bas yaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Na chahun chaand sitare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na chahun chaand sitare"/>
</div>
<div class="lyrico-lyrics-wrapper">Maahi sabse pyaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maahi sabse pyaara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jahaan kahega wahaan reh lungi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jahaan kahega wahaan reh lungi"/>
</div>
<div class="lyrico-lyrics-wrapper">Hans hans ke sab kuch seh lungi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hans hans ke sab kuch seh lungi"/>
</div>
<div class="lyrico-lyrics-wrapper">Jahaan kahega wahaan reh loongi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jahaan kahega wahaan reh loongi"/>
</div>
<div class="lyrico-lyrics-wrapper">Hans hans ke sab kuch seh lungi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hans hans ke sab kuch seh lungi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maahiya yeh waada kar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maahiya yeh waada kar"/>
</div>
<div class="lyrico-lyrics-wrapper">Ve main teri ho gayi aan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ve main teri ho gayi aan"/>
</div>
<div class="lyrico-lyrics-wrapper">Mujhe door na kari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mujhe door na kari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ve main teri ho gayi aan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ve main teri ho gayi aan"/>
</div>
<div class="lyrico-lyrics-wrapper">Mujhe door na kari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mujhe door na kari"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeh dor mahobbat ki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh dor mahobbat ki"/>
</div>
<div class="lyrico-lyrics-wrapper">Kamzor na kari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kamzor na kari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadmon mein duniya yeh saari rakh dun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadmon mein duniya yeh saari rakh dun"/>
</div>
<div class="lyrico-lyrics-wrapper">Tere aage jaan har vaari rakh dun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere aage jaan har vaari rakh dun"/>
</div>
<div class="lyrico-lyrics-wrapper">Teri meri jodi ki nishani rakh dun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teri meri jodi ki nishani rakh dun"/>
</div>
<div class="lyrico-lyrics-wrapper">Ishq yeh apni zubaani rakh dun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishq yeh apni zubaani rakh dun"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tere kol kol aake apna tujhe bana ke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere kol kol aake apna tujhe bana ke"/>
</div>
<div class="lyrico-lyrics-wrapper">Ab mujhko yaara koyi na fikar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ab mujhko yaara koyi na fikar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mahiya yeh waada kar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mahiya yeh waada kar"/>
</div>
<div class="lyrico-lyrics-wrapper">Ve main tere ho gaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ve main tere ho gaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Mujhe door na kari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mujhe door na kari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ve main tere ho gaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ve main tere ho gaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Mujhe door na kari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mujhe door na kari"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeh dor mahobbat ki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh dor mahobbat ki"/>
</div>
<div class="lyrico-lyrics-wrapper">Kamzor na kari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kamzor na kari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ve main teri ho gayi aan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ve main teri ho gayi aan"/>
</div>
<div class="lyrico-lyrics-wrapper">Mujhe door na kari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mujhe door na kari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ve main teri ho gayi aan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ve main teri ho gayi aan"/>
</div>
<div class="lyrico-lyrics-wrapper">Mujhe door na kari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mujhe door na kari"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeh dor mohabbat ki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh dor mohabbat ki"/>
</div>
<div class="lyrico-lyrics-wrapper">Kamzor na kari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kamzor na kari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Main tere ho gaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main tere ho gaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Mujhe door na kari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mujhe door na kari"/>
</div>
<div class="lyrico-lyrics-wrapper">Main tere ho gaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main tere ho gaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Mujhe door na kari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mujhe door na kari"/>
</div>
</pre>
