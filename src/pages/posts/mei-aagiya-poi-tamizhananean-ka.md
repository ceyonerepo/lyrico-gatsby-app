---
title: "mei aagiya poi song lyrics"
album: "Tamizhananean Ka"
artist: "Vynod Subramaniam"
lyricist: "Vynod Subramaniam"
director: "Sathish Ramakrishnan"
path: "/albums/tamizhananean-ka-song-lyrics"
song: "Mei Aagiya Poi"
image: ../../images/albumart/tamizhananean-ka.jpg
date: 2018-02-23
lang: tamil
youtubeLink: "https://www.youtube.com/embed/8PKc2D4JhW4"
type: "sad"
singers:
  - Shanthini Sathyanathan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">mei aagiya poiyaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mei aagiya poiyaale"/>
</div>
<div class="lyrico-lyrics-wrapper">uyiraagiya mei 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyiraagiya mei "/>
</div>
<div class="lyrico-lyrics-wrapper">bothayil aaduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bothayil aaduthe"/>
</div>
<div class="lyrico-lyrics-wrapper">aaduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaduthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">en peyar marandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en peyar marandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">dhegam marandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhegam marandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">en uyir mattum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en uyir mattum "/>
</div>
<div class="lyrico-lyrics-wrapper">thaniyaari thalladuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaniyaari thalladuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">thalladuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalladuthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ichai konda kaigalaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ichai konda kaigalaal"/>
</div>
<div class="lyrico-lyrics-wrapper">echai aanadhu thegam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="echai aanadhu thegam"/>
</div>
<div class="lyrico-lyrics-wrapper">en thuyil urindhapin 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en thuyil urindhapin "/>
</div>
<div class="lyrico-lyrics-wrapper">illa kadavul eppo meethum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="illa kadavul eppo meethum "/>
</div>
<div class="lyrico-lyrics-wrapper">en thaai thandha thegam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en thaai thandha thegam"/>
</div>
<div class="lyrico-lyrics-wrapper">dhinam saaguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhinam saaguthe"/>
</div>
<div class="lyrico-lyrics-wrapper">naan konda dhegam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan konda dhegam"/>
</div>
<div class="lyrico-lyrics-wrapper">paavam aanadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paavam aanadhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">bothaiyin aayul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bothaiyin aayul"/>
</div>
<div class="lyrico-lyrics-wrapper">kaithi aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaithi aanen"/>
</div>
<div class="lyrico-lyrics-wrapper">vaai than pesaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaai than pesaa "/>
</div>
<div class="lyrico-lyrics-wrapper">oomai aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oomai aanen"/>
</div>
<div class="lyrico-lyrics-wrapper">illatha karunai indha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="illatha karunai indha"/>
</div>
<div class="lyrico-lyrics-wrapper">naragathil dhinam thedinen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naragathil dhinam thedinen"/>
</div>
<div class="lyrico-lyrics-wrapper">kan pala ennai meya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kan pala ennai meya"/>
</div>
<div class="lyrico-lyrics-wrapper">kai pala dhegam theenduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kai pala dhegam theenduthe"/>
</div>
<div class="lyrico-lyrics-wrapper">udal noga uyir poga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="udal noga uyir poga"/>
</div>
<div class="lyrico-lyrics-wrapper">ini vendinen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ini vendinen"/>
</div>
<div class="lyrico-lyrics-wrapper">udal noga uyir poga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="udal noga uyir poga"/>
</div>
<div class="lyrico-lyrics-wrapper">ini vendinen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ini vendinen"/>
</div>
</pre>
