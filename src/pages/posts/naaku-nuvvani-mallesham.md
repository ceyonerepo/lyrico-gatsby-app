---
title: "naaku nuvvani song lyrics"
album: "Mallesham"
artist: "Mark K Robin"
lyricist: "Chandrabose"
director: "Raj R"
path: "/albums/mallesham-lyrics"
song: "Naaku Nuvvani"
image: ../../images/albumart/mallesham.jpg
date: 2019-06-21
lang: telugu
youtubeLink: "https://www.youtube.com/embed/1HwHifEFltk"
type: "love"
singers:
  - Sri Krishna
  - Ramya Behara
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Naaku Nuvvani Mari Neeku Nenani 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naaku Nuvvani Mari Neeku Nenani "/>
</div>
<div class="lyrico-lyrics-wrapper">Mana Rendu Gundelooge Uyyaalaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana Rendu Gundelooge Uyyaalaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu Nenani Yika Yeru Kamani 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu Nenani Yika Yeru Kamani "/>
</div>
<div class="lyrico-lyrics-wrapper">Mana Janta Peru Preme Ayyelaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana Janta Peru Preme Ayyelaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Sudasakkagaa Ilaa Ilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sudasakkagaa Ilaa Ilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mucchataadagaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mucchataadagaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Ramasakkagaa Alaa Alaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ramasakkagaa Alaa Alaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadipaadagaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadipaadagaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Yennenni Aashalo Yennenni Oohalo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yennenni Aashalo Yennenni Oohalo "/>
</div>
<div class="lyrico-lyrics-wrapper">Melesukunna Kongu Mullalo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Melesukunna Kongu Mullalo "/>
</div>
<div class="lyrico-lyrics-wrapper">Ennenni Navvulo Yinkenni Rangulo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennenni Navvulo Yinkenni Rangulo "/>
</div>
<div class="lyrico-lyrics-wrapper">Kalesukunna Konte Kallalo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalesukunna Konte Kallalo "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gunugu Puvvulaa Tangedu Navvulaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gunugu Puvvulaa Tangedu Navvulaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Mana Rendu Gundelooge Uyyalaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana Rendu Gundelooge Uyyalaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Goru Vankaki Singaari Silakalaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Goru Vankaki Singaari Silakalaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Mana Janta Peru Preme Ayyelaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana Janta Peru Preme Ayyelaa "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yennenni Poddulo Yennenni Muddulo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yennenni Poddulo Yennenni Muddulo "/>
</div>
<div class="lyrico-lyrics-wrapper">Mudesukunna Pasupu Taadulo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudesukunna Pasupu Taadulo "/>
</div>
<div class="lyrico-lyrics-wrapper">Yennenni Navvulo Yinkenni Ravvalo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yennenni Navvulo Yinkenni Ravvalo "/>
</div>
<div class="lyrico-lyrics-wrapper">Kalesukunna Eedu Jodulo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalesukunna Eedu Jodulo "/>
</div>
<div class="lyrico-lyrics-wrapper">Naaku Nuvvani Mari Neeku Nenani 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naaku Nuvvani Mari Neeku Nenani "/>
</div>
<div class="lyrico-lyrics-wrapper">Mana Rendu Gundelooge Uyyaalaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana Rendu Gundelooge Uyyaalaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu Nenani Yika Yeru Kamani 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu Nenani Yika Yeru Kamani "/>
</div>
<div class="lyrico-lyrics-wrapper">Mana Janta Peru Preme Ayyelaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana Janta Peru Preme Ayyelaa"/>
</div>
</pre>
