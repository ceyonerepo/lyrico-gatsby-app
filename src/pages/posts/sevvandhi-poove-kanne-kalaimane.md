---
title: "sevvandhi poove song lyrics"
album: "Kanne Kalaimane"
artist: "Yuvan Shankar Raja"
lyricist: "Vairamuthu"
director: "Seenu Ramasamy"
path: "/albums/kanne-kalaimane-lyrics"
song: "Sevvandhi Poove"
image: ../../images/albumart/kanne-kalaimane.jpg
date: 2019-02-22
lang: tamil
youtubeLink: "https://www.youtube.com/embed/pPOoFDq6QBQ"
type: "love"
singers:
  - Karthik
  - Pragathi Guruprasad
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Sevvandhi Poove
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sevvandhi Poove"/>
</div>
<div class="lyrico-lyrics-wrapper">Sevvana Theeve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sevvana Theeve"/>
</div>
<div class="lyrico-lyrics-wrapper">Theanagi Vanthai Munnale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theanagi Vanthai Munnale"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanagi Ponen Unnale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanagi Ponen Unnale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Veettukaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Veettukaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Thennaattu Veera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thennaattu Veera"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannaga Vanthai Munnale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaga Vanthai Munnale"/>
</div>
<div class="lyrico-lyrics-wrapper">Pennagi Ponen Unnale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pennagi Ponen Unnale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adiye Vangi Kaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiye Vangi Kaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Muththam Kadana Thaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muththam Kadana Thaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vatti Asalukku Mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vatti Asalukku Mela"/>
</div>
<div class="lyrico-lyrics-wrapper">Pottu Thaaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pottu Thaaren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unga Pinaiya Thanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unga Pinaiya Thanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyira Ezhuthi Thantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyira Ezhuthi Thantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Iravukku Ennai Kadana Thaaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravukku Ennai Kadana Thaaren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalyanam Selai Konjum Kasakkathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalyanam Selai Konjum Kasakkathum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannala Innum Konjum Nasukkathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannala Innum Konjum Nasukkathum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalli Podathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalli Podathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaappa Podathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaappa Podathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Pillai Seiven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Pillai Seiven"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam Thollai Seiven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Thollai Seiven"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanna Aasaikku Thoothai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna Aasaikku Thoothai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanmai Seiven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanmai Seiven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veettil Velai Seiven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veettil Velai Seiven"/>
</div>
<div class="lyrico-lyrics-wrapper">Thottam Thooimai Seiven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottam Thooimai Seiven"/>
</div>
<div class="lyrico-lyrics-wrapper">Santharpam Parthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Santharpam Parthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaaimai Seiven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaimai Seiven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Appavi Poonai Paalai Kudikkuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appavi Poonai Paalai Kudikkuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Poonaikku Paalai Enna Porukkuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poonaikku Paalai Enna Porukkuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Satru Thalli Poo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Satru Thalli Poo"/>
</div>
<div class="lyrico-lyrics-wrapper">Neram Solli Poo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neram Solli Poo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sevvandhi Poove
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sevvandhi Poove"/>
</div>
<div class="lyrico-lyrics-wrapper">Sevvana Theeve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sevvana Theeve"/>
</div>
<div class="lyrico-lyrics-wrapper">Theanagi Vanthai Munnale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theanagi Vanthai Munnale"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanagi Ponen Unnale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanagi Ponen Unnale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh En Veettukaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh En Veettukaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Thennaattu Veera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thennaattu Veera"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannaga Vanthai Munnale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaga Vanthai Munnale"/>
</div>
<div class="lyrico-lyrics-wrapper">Pennagi Ponen Unnale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pennagi Ponen Unnale"/>
</div>
</pre>
