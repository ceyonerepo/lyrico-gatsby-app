---
title: "we love you money item song lyrics"
album: "Bodha"
artist: "Siddharth Vipin"
lyricist: "Lalitha Anand"
director: "Suresh G"
path: "/albums/bodha-lyrics"
song: "We Love You Money"
image: ../../images/albumart/bodha.jpg
date: 2018-07-02
lang: tamil
youtubeLink: "https://www.youtube.com/embed/jET_lZMaVeY"
type: "happy"
singers:
  - Jagadeesh
  - Siddharth Vipin
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">un mela undaana love aala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un mela undaana love aala"/>
</div>
<div class="lyrico-lyrics-wrapper">thinam thookam varala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thinam thookam varala"/>
</div>
<div class="lyrico-lyrics-wrapper">kannala unna paathale ada 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannala unna paathale ada "/>
</div>
<div class="lyrico-lyrics-wrapper">thannale thala kaalu puriyalaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thannale thala kaalu puriyalaye"/>
</div>
<div class="lyrico-lyrics-wrapper">colour nee figure nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="colour nee figure nee"/>
</div>
<div class="lyrico-lyrics-wrapper">pookuthu vaasam semaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pookuthu vaasam semaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">kanmani ponmani 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanmani ponmani "/>
</div>
<div class="lyrico-lyrics-wrapper">money money money
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="money money money"/>
</div>
<div class="lyrico-lyrics-wrapper">we love you money
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="we love you money"/>
</div>
<div class="lyrico-lyrics-wrapper">we love you money
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="we love you money"/>
</div>
<div class="lyrico-lyrics-wrapper">we love you money
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="we love you money"/>
</div>
<div class="lyrico-lyrics-wrapper">we love you money
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="we love you money"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">un mela undaana love aala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un mela undaana love aala"/>
</div>
<div class="lyrico-lyrics-wrapper">thinam thookam varala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thinam thookam varala"/>
</div>
<div class="lyrico-lyrics-wrapper">kannala unna paathale ada 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannala unna paathale ada "/>
</div>
<div class="lyrico-lyrics-wrapper">thannale thala kaalu puriyalaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thannale thala kaalu puriyalaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">munnalagum nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munnalagum nee"/>
</div>
<div class="lyrico-lyrics-wrapper">pinnalagum nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pinnalagum nee"/>
</div>
<div class="lyrico-lyrics-wrapper">kaiyila kedacha style
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaiyila kedacha style"/>
</div>
<div class="lyrico-lyrics-wrapper">white um nee black um nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="white um nee black um nee"/>
</div>
<div class="lyrico-lyrics-wrapper">mayakum alagu smile 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mayakum alagu smile "/>
</div>
<div class="lyrico-lyrics-wrapper">smile smile smile smile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="smile smile smile smile"/>
</div>
<div class="lyrico-lyrics-wrapper">smile smile smile smile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="smile smile smile smile"/>
</div>
<div class="lyrico-lyrics-wrapper">nee kaasa monaalisa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee kaasa monaalisa"/>
</div>
<div class="lyrico-lyrics-wrapper">sa sa sa sa sa sa sa sa sa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sa sa sa sa sa sa sa sa sa"/>
</div>
<div class="lyrico-lyrics-wrapper">we love you money
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="we love you money"/>
</div>
<div class="lyrico-lyrics-wrapper">we love you money
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="we love you money"/>
</div>
<div class="lyrico-lyrics-wrapper">we love you money
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="we love you money"/>
</div>
<div class="lyrico-lyrics-wrapper">we love you money
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="we love you money"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ulagin santhosam nee than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulagin santhosam nee than"/>
</div>
<div class="lyrico-lyrics-wrapper">ulagin sacharavum nee than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulagin sacharavum nee than"/>
</div>
<div class="lyrico-lyrics-wrapper">ambani petiyila nee thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ambani petiyila nee thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">anjara petiyilum nee thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anjara petiyilum nee thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">viyarvai sintha vaipa nee than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viyarvai sintha vaipa nee than"/>
</div>
<div class="lyrico-lyrics-wrapper">vesam poda vaipa nee than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vesam poda vaipa nee than"/>
</div>
<div class="lyrico-lyrics-wrapper">yenga vaikuraye nee than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yenga vaikuraye nee than"/>
</div>
<div class="lyrico-lyrics-wrapper">ellorin kathaliyum nee than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellorin kathaliyum nee than"/>
</div>
<div class="lyrico-lyrics-wrapper">monolisa monolisa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="monolisa monolisa"/>
</div>
<div class="lyrico-lyrics-wrapper">nee than kaasaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee than kaasaa"/>
</div>
<div class="lyrico-lyrics-wrapper">we love you money
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="we love you money"/>
</div>
<div class="lyrico-lyrics-wrapper">we love you money
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="we love you money"/>
</div>
<div class="lyrico-lyrics-wrapper">we need you money
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="we need you money"/>
</div>
<div class="lyrico-lyrics-wrapper">we need you money
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="we need you money"/>
</div>
<div class="lyrico-lyrics-wrapper">money money money
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="money money money"/>
</div>
<div class="lyrico-lyrics-wrapper">money money money
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="money money money"/>
</div>
</pre>
