---
title: "yean penne neeyum song lyrics"
album: "Iravukku Aayiram Kangal"
artist: "Sam CS"
lyricist: "Sam CS"
director: "Mu. Maran"
path: "/albums/iravukku-aayiram-kangal-lyrics"
song: "Yean Penne Neeyum"
image: ../../images/albumart/iravukku-aayiram-kangal.jpg
date: 2018-05-11
lang: tamil
youtubeLink: "https://www.youtube.com/embed/MCPGAeVeDWc"
type: "love"
singers:
  - Haricharan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ohooooo hoo ooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohooooo hoo ooooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohooooo hoo ooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohooooo hoo ooooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohooooo hoo ooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohooooo hoo ooooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohooooo hoo ooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohooooo hoo ooooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yean pennae neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yean pennae neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil aasa sekkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil aasa sekkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee illamae naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee illamae naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Saagum nilaiku aakura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saagum nilaiku aakura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yean pennae neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yean pennae neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil aasa sekkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil aasa sekkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee illamae naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee illamae naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Saagum nilaiku aakura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saagum nilaiku aakura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yenoru nilavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenoru nilavu"/>
</div>
<div class="lyrico-lyrics-wrapper">En vaan vittu ponadhu yeno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En vaan vittu ponadhu yeno"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen siru uyira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen siru uyira"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee chillaki ponadhu yeno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee chillaki ponadhu yeno"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unna paarkaama thudichenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna paarkaama thudichenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vegamma vendhae naan vedichenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vegamma vendhae naan vedichenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil theeyoda thavichenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil theeyoda thavichenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Una paakum mattum uyir pudichenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Una paakum mattum uyir pudichenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ohooooo hoo ooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohooooo hoo ooooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohooooo hoo ooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohooooo hoo ooooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohooooo hoo ooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohooooo hoo ooooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohooooo hoo ooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohooooo hoo ooooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yean pennae neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yean pennae neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil aasa sekkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil aasa sekkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee illamae naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee illamae naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Saagum nilaiku aakura 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saagum nilaiku aakura "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yean pennae neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yean pennae neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil aasa sekkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil aasa sekkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee illamae naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee illamae naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Saagum nilaiku aakura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saagum nilaiku aakura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thara raa rara raam mm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thara raa rara raam mm"/>
</div>
<div class="lyrico-lyrics-wrapper">Thara raa rara raa haa aa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thara raa rara raa haa aa aaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thara raa thara ra thara raa raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thara raa thara ra thara raa raa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eraga eraga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eraga eraga"/>
</div>
<div class="lyrico-lyrics-wrapper">En vazhkai unna thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En vazhkai unna thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Alaiyaa dhalaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alaiyaa dhalaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee venum venummunnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee venum venummunnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eraga eraga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eraga eraga"/>
</div>
<div class="lyrico-lyrics-wrapper">En vazhkai unna thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En vazhkai unna thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Alaiyaa dhalaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alaiyaa dhalaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee venum venummunnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee venum venummunnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manadhoram mazhai thoorum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manadhoram mazhai thoorum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vegudhooram nee agalum bodhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vegudhooram nee agalum bodhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Manadhoram mazhai thoorum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manadhoram mazhai thoorum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vegudhooram nee agalum bodhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vegudhooram nee agalum bodhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yean pennae neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yean pennae neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil aasa sekkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil aasa sekkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee illamae naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee illamae naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Saagum nilaiku aakura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saagum nilaiku aakura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yean pennae neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yean pennae neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil aasa sekkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil aasa sekkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee illamae naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee illamae naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Saagum nilaiku aakura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saagum nilaiku aakura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yean pennae neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yean pennae neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil aasa sekkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil aasa sekkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee illamae naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee illamae naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Saagum nilaiku aakura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saagum nilaiku aakura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yean pennae neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yean pennae neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil aasa sekkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil aasa sekkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee illamae naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee illamae naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Saagum nilaiku aakura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saagum nilaiku aakura"/>
</div>
</pre>
