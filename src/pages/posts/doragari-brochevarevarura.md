---
title: "doragari song lyrics"
album: "Brochevarevarura"
artist: "Vivek Sagar"
lyricist: "Bharadwaj"
director: "Vivek Athreya"
path: "/albums/brochevarevarura-lyrics"
song: "Doragari"
image: ../../images/albumart/brochevarevarura.jpg
date: 2019-06-28
lang: telugu
youtubeLink: "https://www.youtube.com/embed/qMRpBqli8dc"
type: "happy"
singers:
  - Vivek Sagar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Dora Gaari Sogasari Siri Dotalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dora Gaari Sogasari Siri Dotalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Dorasaani Silakamma Undiro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dorasaani Silakamma Undiro"/>
</div>
<div class="lyrico-lyrics-wrapper">Galabaala Gadasari Peda Mookatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Galabaala Gadasari Peda Mookatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Selime Adhi Jesero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Selime Adhi Jesero"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogamanche Pade Thadi Podi Yelallo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogamanche Pade Thadi Podi Yelallo"/>
</div>
<div class="lyrico-lyrics-wrapper">Talavonchesina Sitaaru Selo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Talavonchesina Sitaaru Selo"/>
</div>
<div class="lyrico-lyrics-wrapper">Sigurinche Siru Saavaasaale Nindero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sigurinche Siru Saavaasaale Nindero"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathake Mude Padero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathake Mude Padero"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Silakaa Alupantu Leka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Silakaa Alupantu Leka "/>
</div>
<div class="lyrico-lyrics-wrapper">Vaagu Vankaa Yenaka Parugidaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaagu Vankaa Yenaka Parugidaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaa Danakaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaa Danakaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Paikegaradame Andaaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paikegaradame Andaaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Beruke Beme Padaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Beruke Beme Padaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Dora Gaari Sogasari Siri Dotalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dora Gaari Sogasari Siri Dotalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Dorasaani Silakamma Undiro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dorasaani Silakamma Undiro"/>
</div>
<div class="lyrico-lyrics-wrapper">Galabaala Gadasari Peda Mookatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Galabaala Gadasari Peda Mookatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Selime Adhi Jesero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Selime Adhi Jesero"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oho Aahaa Gadhumuthundi Soodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oho Aahaa Gadhumuthundi Soodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oho Aahaa Gadabida Goodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oho Aahaa Gadabida Goodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oho Aahaa Kalabadindhi Thodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oho Aahaa Kalabadindhi Thodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oho Aahaa Kuvakuva Laadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oho Aahaa Kuvakuva Laadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooroka Theeru Meeroka Theeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooroka Theeru Meeroka Theeru"/>
</div>
<div class="lyrico-lyrics-wrapper">Gandaragolam Sandadi Meeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gandaragolam Sandadi Meeru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ehey Yelaapaalaa Denikantaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ehey Yelaapaalaa Denikantaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yelaakolam Sesukuntaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yelaakolam Sesukuntaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kondaa Kona Daatukuntaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondaa Kona Daatukuntaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Enchakkaa Saagipo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enchakkaa Saagipo"/>
</div>
<div class="lyrico-lyrics-wrapper">Enchakkaa Saagipo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enchakkaa Saagipo"/>
</div>
<div class="lyrico-lyrics-wrapper">Resukka Naavalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Resukka Naavalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ningekki Thelipo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ningekki Thelipo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sannajaaji Kuluku Sallagaali Suruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sannajaaji Kuluku Sallagaali Suruku"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandamaama Varaku Yedemainaa Seraale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandamaama Varaku Yedemainaa Seraale"/>
</div>
<div class="lyrico-lyrics-wrapper">Allibilli Parugu Saagetantha Varaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allibilli Parugu Saagetantha Varaku"/>
</div>
<div class="lyrico-lyrics-wrapper">Yegusukka Yelugu Nee Senthake Raavaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yegusukka Yelugu Nee Senthake Raavaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Thoti Thiragaalantoo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Thoti Thiragaalantoo "/>
</div>
<div class="lyrico-lyrics-wrapper">Mabbule Yenta Padipovaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mabbule Yenta Padipovaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Bugga Nimaraalantoo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Bugga Nimaraalantoo "/>
</div>
<div class="lyrico-lyrics-wrapper">Sukkale Nela Digi Raavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sukkale Nela Digi Raavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ehey Yelaapaalaa Denikantaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ehey Yelaapaalaa Denikantaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yelaakolam Sesukuntaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yelaakolam Sesukuntaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kondaa Kona Daatukuntaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondaa Kona Daatukuntaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Enchakkaa Saagipo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enchakkaa Saagipo"/>
</div>
<div class="lyrico-lyrics-wrapper">Enchakkaa Saagipo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enchakkaa Saagipo"/>
</div>
<div class="lyrico-lyrics-wrapper">Resukka Naavalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Resukka Naavalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ningekki Thelipo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ningekki Thelipo"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Silakaa Alupantu Leka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Silakaa Alupantu Leka "/>
</div>
<div class="lyrico-lyrics-wrapper">Vaagu Vankaa Yenaka Parugidaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaagu Vankaa Yenaka Parugidaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaa Danakaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaa Danakaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Paikegaradame Andaaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paikegaradame Andaaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Beruke Beme Padaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Beruke Beme Padaale"/>
</div>
</pre>
