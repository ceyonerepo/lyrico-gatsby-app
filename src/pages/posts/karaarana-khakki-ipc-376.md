---
title: "karaarana khakki song lyrics"
album: "IPC 376"
artist: "Yaadhav Ramalinkgam"
lyricist: "Madras Miran"
director: "Ramkumar Subbaraman"
path: "/albums/ipc-376-lyrics"
song: "Karaarana Khakki"
image: ../../images/albumart/ipc-376.jpg
date: 2021-10-29
lang: tamil
youtubeLink: "https://www.youtube.com/embed/lYjCHcZan0s"
type: "mass"
singers:
  - Gana Balachandar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">yo yo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yo yo"/>
</div>
<div class="lyrico-lyrics-wrapper">lets do this baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lets do this baby"/>
</div>
<div class="lyrico-lyrics-wrapper">nothing to fear
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nothing to fear"/>
</div>
<div class="lyrico-lyrics-wrapper">nothing to lose
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nothing to lose"/>
</div>
<div class="lyrico-lyrics-wrapper">its my destiny
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="its my destiny"/>
</div>
<div class="lyrico-lyrics-wrapper">to be a queen of bay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="to be a queen of bay"/>
</div>
<div class="lyrico-lyrics-wrapper">rock it baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rock it baby"/>
</div>
<div class="lyrico-lyrics-wrapper">rock it baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rock it baby"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">karaar aana kaakki ava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karaar aana kaakki ava"/>
</div>
<div class="lyrico-lyrics-wrapper">terror aana jockey naan dhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="terror aana jockey naan dhan"/>
</div>
<div class="lyrico-lyrics-wrapper">yaara vena ketu paathuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaara vena ketu paathuko"/>
</div>
<div class="lyrico-lyrics-wrapper">hey police na vera levelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey police na vera levelu"/>
</div>
<div class="lyrico-lyrics-wrapper">pinjudunda un sevulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pinjudunda un sevulu"/>
</div>
<div class="lyrico-lyrics-wrapper">selladhu un route a mathiko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="selladhu un route a mathiko"/>
</div>
<div class="lyrico-lyrics-wrapper">iva mutila than irukum bodhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iva mutila than irukum bodhu"/>
</div>
<div class="lyrico-lyrics-wrapper">sideu vagudu ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sideu vagudu ma"/>
</div>
<div class="lyrico-lyrics-wrapper">an duty pakum bodhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="an duty pakum bodhu"/>
</div>
<div class="lyrico-lyrics-wrapper">iva pakka pagudu ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iva pakka pagudu ma"/>
</div>
<div class="lyrico-lyrics-wrapper">ah duty pakum bodhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ah duty pakum bodhu"/>
</div>
<div class="lyrico-lyrics-wrapper">iva pakka sagudu ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iva pakka sagudu ma"/>
</div>
<div class="lyrico-lyrics-wrapper">vecha ona polandhukura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vecha ona polandhukura"/>
</div>
<div class="lyrico-lyrics-wrapper">steelu thagadu ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="steelu thagadu ma"/>
</div>
<div class="lyrico-lyrics-wrapper">ah panic aaga maata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ah panic aaga maata"/>
</div>
<div class="lyrico-lyrics-wrapper">ava romba ruggedu ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ava romba ruggedu ma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">karaar aana kaakki ava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karaar aana kaakki ava"/>
</div>
<div class="lyrico-lyrics-wrapper">terror aana jockey naan dhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="terror aana jockey naan dhan"/>
</div>
<div class="lyrico-lyrics-wrapper">yaara vena ketu paathuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaara vena ketu paathuko"/>
</div>
<div class="lyrico-lyrics-wrapper">hey police na vera levelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey police na vera levelu"/>
</div>
<div class="lyrico-lyrics-wrapper">pinjudunda un sevulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pinjudunda un sevulu"/>
</div>
<div class="lyrico-lyrics-wrapper">selladhu un route a mathiko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="selladhu un route a mathiko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">verappa vandhu morappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="verappa vandhu morappa"/>
</div>
<div class="lyrico-lyrics-wrapper">thillu mullu ellam olippa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thillu mullu ellam olippa"/>
</div>
<div class="lyrico-lyrics-wrapper">mutti muli ellam pidhungida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mutti muli ellam pidhungida"/>
</div>
<div class="lyrico-lyrics-wrapper">un thola than urichu thonga poda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un thola than urichu thonga poda"/>
</div>
<div class="lyrico-lyrics-wrapper">ippa varuvaa vandhu morappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ippa varuvaa vandhu morappa"/>
</div>
<div class="lyrico-lyrics-wrapper">oru punch vitta pinju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru punch vitta pinju"/>
</div>
<div class="lyrico-lyrics-wrapper">thongidum moonju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thongidum moonju"/>
</div>
<div class="lyrico-lyrics-wrapper">fear eh illa fireuh dhan ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="fear eh illa fireuh dhan ma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">etta nenchu etta ninnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="etta nenchu etta ninnu"/>
</div>
<div class="lyrico-lyrics-wrapper">enna theendunaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna theendunaa"/>
</div>
<div class="lyrico-lyrics-wrapper">item kaaran matti kinna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="item kaaran matti kinna"/>
</div>
<div class="lyrico-lyrics-wrapper">uiyum batula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uiyum batula"/>
</div>
<div class="lyrico-lyrics-wrapper">bundobast jaburujastu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bundobast jaburujastu"/>
</div>
<div class="lyrico-lyrics-wrapper">inga kaatuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inga kaatuna"/>
</div>
<div class="lyrico-lyrics-wrapper">thooki pottu midhichiduva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thooki pottu midhichiduva"/>
</div>
<div class="lyrico-lyrics-wrapper">kaiyila maatina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaiyila maatina"/>
</div>
<div class="lyrico-lyrics-wrapper">brilliant ah sketchu podum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="brilliant ah sketchu podum"/>
</div>
<div class="lyrico-lyrics-wrapper">enga routeu na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga routeu na"/>
</div>
<div class="lyrico-lyrics-wrapper">vella veshti wrongu panna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vella veshti wrongu panna"/>
</div>
<div class="lyrico-lyrics-wrapper">veppa veattunaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veppa veattunaa"/>
</div>
<div class="lyrico-lyrics-wrapper">danguvara daarakuva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="danguvara daarakuva"/>
</div>
<div class="lyrico-lyrics-wrapper">dhutta neetuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhutta neetuna"/>
</div>
<div class="lyrico-lyrics-wrapper">vijayasanthi polayum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vijayasanthi polayum"/>
</div>
<div class="lyrico-lyrics-wrapper">semma shotu na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="semma shotu na"/>
</div>
<div class="lyrico-lyrics-wrapper">vecha oru panchula dhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vecha oru panchula dhan"/>
</div>
<div class="lyrico-lyrics-wrapper">ukandhuduva benchula la dhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ukandhuduva benchula la dhan"/>
</div>
<div class="lyrico-lyrics-wrapper">yaara vena ketu pathuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaara vena ketu pathuko"/>
</div>
<div class="lyrico-lyrics-wrapper">kolaruna kolaya kalapu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kolaruna kolaya kalapu"/>
</div>
<div class="lyrico-lyrics-wrapper">dowlathuna udaiyum tholuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dowlathuna udaiyum tholuku"/>
</div>
<div class="lyrico-lyrics-wrapper">ipc 376 ah therinjiko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ipc 376 ah therinjiko"/>
</div>
<div class="lyrico-lyrics-wrapper">omenlingo omenlingo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="omenlingo omenlingo"/>
</div>
<div class="lyrico-lyrics-wrapper">retamiyo retamiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="retamiyo retamiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">billi billi billi billapi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="billi billi billi billapi"/>
</div>
<div class="lyrico-lyrics-wrapper">billi billi billi billapi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="billi billi billi billapi"/>
</div>
<div class="lyrico-lyrics-wrapper">arasabandu arasabandu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arasabandu arasabandu"/>
</div>
<div class="lyrico-lyrics-wrapper">nimabandu kichilibandu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nimabandu kichilibandu"/>
</div>
<div class="lyrico-lyrics-wrapper">seemaragibandu ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seemaragibandu ah"/>
</div>
<div class="lyrico-lyrics-wrapper">seemaragi bandu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seemaragi bandu"/>
</div>
<div class="lyrico-lyrics-wrapper">rathatha paathale 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rathatha paathale "/>
</div>
<div class="lyrico-lyrics-wrapper">seeridum singam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seeridum singam"/>
</div>
<div class="lyrico-lyrics-wrapper">mathabadi eppodhume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mathabadi eppodhume"/>
</div>
<div class="lyrico-lyrics-wrapper">agmarku thangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="agmarku thangam"/>
</div>
<div class="lyrico-lyrics-wrapper">accusedu dhan paathale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="accusedu dhan paathale"/>
</div>
<div class="lyrico-lyrics-wrapper">odiduvan therikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="odiduvan therikka"/>
</div>
<div class="lyrico-lyrics-wrapper">vandhale thappu thanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vandhale thappu thanda"/>
</div>
<div class="lyrico-lyrics-wrapper">rate ah ellam koraikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rate ah ellam koraikka"/>
</div>
<div class="lyrico-lyrics-wrapper">ponnungala kaaval kaakum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponnungala kaaval kaakum"/>
</div>
<div class="lyrico-lyrics-wrapper">kaaval dheivam dhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaaval dheivam dhan"/>
</div>
<div class="lyrico-lyrics-wrapper">kodala uruvi maatikuva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kodala uruvi maatikuva"/>
</div>
<div class="lyrico-lyrics-wrapper">kaali pola dhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaali pola dhan"/>
</div>
<div class="lyrico-lyrics-wrapper">yarukume asaramatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yarukume asaramatta"/>
</div>
<div class="lyrico-lyrics-wrapper">erangi seiva daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="erangi seiva daa"/>
</div>
<div class="lyrico-lyrics-wrapper">kaakiyila gambirama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaakiyila gambirama"/>
</div>
<div class="lyrico-lyrics-wrapper">nadandhu varuvaa da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nadandhu varuvaa da"/>
</div>
<div class="lyrico-lyrics-wrapper">thappu thanda pandravana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thappu thanda pandravana"/>
</div>
<div class="lyrico-lyrics-wrapper">ulla podanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulla podanum"/>
</div>
<div class="lyrico-lyrics-wrapper">pablicu la katti pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pablicu la katti pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">thola urikanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thola urikanum"/>
</div>
<div class="lyrico-lyrics-wrapper">saudi pola india la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saudi pola india la"/>
</div>
<div class="lyrico-lyrics-wrapper">sattam podanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sattam podanum"/>
</div>
<div class="lyrico-lyrics-wrapper">paliyal balathkaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paliyal balathkaram"/>
</div>
<div class="lyrico-lyrics-wrapper">pandaravanga bayandhu odanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pandaravanga bayandhu odanum"/>
</div>
<div class="lyrico-lyrics-wrapper">rock it baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rock it baby"/>
</div>
</pre>
