---
title: "aao ji aao song lyrics"
album: "Oh Manapenne"
artist: "Vishal Chandrashekhar"
lyricist: "Mohan Rajan"
director: "Kaarthikk Sundar"
path: "/albums/oh-manapenne-lyrics"
song: "Aao Ji Aao"
image: ../../images/albumart/oh-manapenne.jpg
date: 2021-10-22
lang: tamil
youtubeLink: "https://www.youtube.com/embed/XGQ5X9Du6uM"
type: "happy"
singers:
  - Gana Bala
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Oh my god
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh my god"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappu kanakka pottuto
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappu kanakka pottuto"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhkal gaali aachu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkal gaali aachu da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Life very sad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life very sad"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru gundu sattikulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru gundu sattikulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Adichom paaru nondi da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adichom paaru nondi da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna solla"/>
</div>
<div class="lyrico-lyrics-wrapper">Current u la dan kaiya vechom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Current u la dan kaiya vechom"/>
</div>
<div class="lyrico-lyrics-wrapper">Bulb u mela bulb u thaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bulb u mela bulb u thaaney"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaangiputtom ada flug um kedaikala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaangiputtom ada flug um kedaikala"/>
</div>
<div class="lyrico-lyrics-wrapper">Chutch um kedaikala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chutch um kedaikala"/>
</div>
<div class="lyrico-lyrics-wrapper">Charge u pona oru battery aagittom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Charge u pona oru battery aagittom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aao ji aao
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aao ji aao"/>
</div>
<div class="lyrico-lyrics-wrapper">Pazhaiya dabbata vasam sikkittom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pazhaiya dabbata vasam sikkittom"/>
</div>
<div class="lyrico-lyrics-wrapper">Aiyayo aiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyayo aiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Pousu paa pata pincture aagitom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pousu paa pata pincture aagitom"/>
</div>
<div class="lyrico-lyrics-wrapper">Headache theeradho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Headache theeradho"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhuvum puriyama inge ninnutom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhuvum puriyama inge ninnutom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhakai kooti pona map u la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhakai kooti pona map u la"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum poiten da maapula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum poiten da maapula"/>
</div>
<div class="lyrico-lyrics-wrapper">Dabbah thedi odum life u la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dabbah thedi odum life u la"/>
</div>
<div class="lyrico-lyrics-wrapper">Vera vazhi illa aiyo aiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vera vazhi illa aiyo aiyo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Otta satti kulla oothi vecha water u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Otta satti kulla oothi vecha water u"/>
</div>
<div class="lyrico-lyrics-wrapper">Ice u katti aana thappikkum paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ice u katti aana thappikkum paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga bulding u strong u da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga bulding u strong u da"/>
</div>
<div class="lyrico-lyrics-wrapper">Basement u weak u da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Basement u weak u da"/>
</div>
<div class="lyrico-lyrics-wrapper">Mothathula naanga aanom bake u da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mothathula naanga aanom bake u da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aao ji aao
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aao ji aao"/>
</div>
<div class="lyrico-lyrics-wrapper">Pazhaiya dabbata vasama sikkittom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pazhaiya dabbata vasama sikkittom"/>
</div>
<div class="lyrico-lyrics-wrapper">Aiyayo aiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyayo aiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Pousu paapata pincture aagitom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pousu paapata pincture aagitom"/>
</div>
<div class="lyrico-lyrics-wrapper">Headache theeradho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Headache theeradho"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhuvum puriyama inge ninnutom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhuvum puriyama inge ninnutom"/>
</div>
<div class="lyrico-lyrics-wrapper">Aiyayo aiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyayo aiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Aiyayo aiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyayo aiyo"/>
</div>
</pre>
