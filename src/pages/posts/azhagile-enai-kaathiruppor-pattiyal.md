---
title: "azhagile enai song lyrics"
album: "Kaathiruppor Pattiyal"
artist: "Sean Roldan"
lyricist: "Sean Roldan"
director: "Balaiya D. Rajasekhar"
path: "/albums/kaathiruppor-pattiyal-song-lyrics"
song: "Azhagile Enai"
image: ../../images/albumart/kaathiruppor-pattiyal.jpg
date: 2018-05-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/d2bIWhHdQsI"
type: "love"
singers:
  - Pradheep
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">azhagile enai adipathendadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="azhagile enai adipathendadi"/>
</div>
<div class="lyrico-lyrics-wrapper">alaruthe kuzhanthai pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alaruthe kuzhanthai pol"/>
</div>
<div class="lyrico-lyrics-wrapper">en manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en manam"/>
</div>
<div class="lyrico-lyrics-wrapper">kadugu pola ne vedipathu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadugu pola ne vedipathu "/>
</div>
<div class="lyrico-lyrics-wrapper">yenadi olirudhe veruppilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yenadi olirudhe veruppilum"/>
</div>
<div class="lyrico-lyrics-wrapper">un mugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un mugam"/>
</div>
<div class="lyrico-lyrics-wrapper">kathalo kathal epozhudhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathalo kathal epozhudhum"/>
</div>
<div class="lyrico-lyrics-wrapper">un meethu pothume kobam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un meethu pothume kobam"/>
</div>
<div class="lyrico-lyrics-wrapper">solvathilai ipothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="solvathilai ipothu"/>
</div>
<div class="lyrico-lyrics-wrapper">sugameee 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sugameee "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">azhagile enai adipathendadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="azhagile enai adipathendadi"/>
</div>
<div class="lyrico-lyrics-wrapper">alaruthe kuzhanthai pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alaruthe kuzhanthai pol"/>
</div>
<div class="lyrico-lyrics-wrapper">en manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en manam"/>
</div>
<div class="lyrico-lyrics-wrapper">kadugu pola ne vedipathu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadugu pola ne vedipathu "/>
</div>
<div class="lyrico-lyrics-wrapper">yenadi olirudhe veruppilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yenadi olirudhe veruppilum"/>
</div>
<div class="lyrico-lyrics-wrapper">un mugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un mugam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vazhnalil idhu varai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vazhnalil idhu varai"/>
</div>
<div class="lyrico-lyrics-wrapper">kanatha kanuvu nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanatha kanuvu nee"/>
</div>
<div class="lyrico-lyrics-wrapper">vanthu sera enna vadagai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanthu sera enna vadagai"/>
</div>
<div class="lyrico-lyrics-wrapper">nalum un manadhile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nalum un manadhile"/>
</div>
<div class="lyrico-lyrics-wrapper">nan vazha uyiraiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan vazha uyiraiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">thanthu poven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanthu poven"/>
</div>
<div class="lyrico-lyrics-wrapper">innumeruppathen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="innumeruppathen"/>
</div>
<div class="lyrico-lyrics-wrapper">ullathile nee ukanthu kondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ullathile nee ukanthu kondu"/>
</div>
<div class="lyrico-lyrics-wrapper">vilangam yen seygirai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vilangam yen seygirai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nesamo nesam epozhudhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nesamo nesam epozhudhum"/>
</div>
<div class="lyrico-lyrics-wrapper">un meethu mosama nanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un meethu mosama nanum"/>
</div>
<div class="lyrico-lyrics-wrapper">solvadhanai ipothu uyire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="solvadhanai ipothu uyire"/>
</div>
</pre>
