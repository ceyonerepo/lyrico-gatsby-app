---
title: "kela kela kelappu song lyrics"
album: "Ongala Podanum Sir"
artist: "Rejimon"
lyricist: "Murugan Manthiram"
director: "Sreejith Vijayan"
path: "/albums/ongala-podanum-sir-lyrics"
song: "Kela Kela Kelappu"
image: ../../images/albumart/ongala-podanum-sir.jpg
date: 2019-09-13
lang: tamil
youtubeLink: "https://www.youtube.com/embed/I4a2At4Zu6k"
type: "happy"
singers:
  - Anthony Daasan
  - Arya
  - Archana
  - Vishnuvardhan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">kela kela kelappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kela kela kelappu"/>
</div>
<div class="lyrico-lyrics-wrapper">kelappu machi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kelappu machi"/>
</div>
<div class="lyrico-lyrics-wrapper">kelappu kelappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kelappu kelappu"/>
</div>
<div class="lyrico-lyrics-wrapper">kela kela kaleppu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kela kela kaleppu"/>
</div>
<div class="lyrico-lyrics-wrapper">kalakku neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalakku neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">kalakku kalakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalakku kalakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kela kela kelappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kela kela kelappu"/>
</div>
<div class="lyrico-lyrics-wrapper">kalappu machi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalappu machi"/>
</div>
<div class="lyrico-lyrics-wrapper">kelappu kelappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kelappu kelappu"/>
</div>
<div class="lyrico-lyrics-wrapper">kela kela kelappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kela kela kelappu"/>
</div>
<div class="lyrico-lyrics-wrapper">kalakku neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalakku neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">kalakku kalakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalakku kalakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">alli vacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alli vacha"/>
</div>
<div class="lyrico-lyrics-wrapper">koonthala than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koonthala than"/>
</div>
<div class="lyrico-lyrics-wrapper">killi ippo potuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="killi ippo potuda"/>
</div>
<div class="lyrico-lyrics-wrapper">malligaiya maranthuputu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malligaiya maranthuputu"/>
</div>
<div class="lyrico-lyrics-wrapper">modern ponna thiriyura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="modern ponna thiriyura"/>
</div>
<div class="lyrico-lyrics-wrapper">alli vacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alli vacha"/>
</div>
<div class="lyrico-lyrics-wrapper">koonthala than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koonthala than"/>
</div>
<div class="lyrico-lyrics-wrapper">killi ippo potuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="killi ippo potuda"/>
</div>
<div class="lyrico-lyrics-wrapper">malligaiya maranthuputu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malligaiya maranthuputu"/>
</div>
<div class="lyrico-lyrics-wrapper">modern ponna thiriyura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="modern ponna thiriyura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">podu thooki podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="podu thooki podu"/>
</div>
<div class="lyrico-lyrics-wrapper">pazhasa thooki 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pazhasa thooki "/>
</div>
<div class="lyrico-lyrics-wrapper">oram podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oram podu"/>
</div>
<div class="lyrico-lyrics-wrapper">podu yeathi podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="podu yeathi podu"/>
</div>
<div class="lyrico-lyrics-wrapper">yevanum yethutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yevanum yethutha"/>
</div>
<div class="lyrico-lyrics-wrapper">avana podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avana podu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kela kela kelappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kela kela kelappu"/>
</div>
<div class="lyrico-lyrics-wrapper">kelappu machi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kelappu machi"/>
</div>
<div class="lyrico-lyrics-wrapper">kelappu kelappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kelappu kelappu"/>
</div>
<div class="lyrico-lyrics-wrapper">kela kela kaleppu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kela kela kaleppu"/>
</div>
<div class="lyrico-lyrics-wrapper">kalakku neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalakku neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">kalakku kalakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalakku kalakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kela kela kelappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kela kela kelappu"/>
</div>
<div class="lyrico-lyrics-wrapper">kalappu machi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalappu machi"/>
</div>
<div class="lyrico-lyrics-wrapper">kelappu kelappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kelappu kelappu"/>
</div>
<div class="lyrico-lyrics-wrapper">kela kela kelappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kela kela kelappu"/>
</div>
<div class="lyrico-lyrics-wrapper">kalakku neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalakku neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">kalakku kalakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalakku kalakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">mallaiya machan thanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mallaiya machan thanda"/>
</div>
<div class="lyrico-lyrics-wrapper">ambani annan thanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ambani annan thanda"/>
</div>
<div class="lyrico-lyrics-wrapper">nayanthara atha ponna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nayanthara atha ponna"/>
</div>
<div class="lyrico-lyrics-wrapper">maman kitta kadhal sonna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maman kitta kadhal sonna"/>
</div>
<div class="lyrico-lyrics-wrapper">whatsapp full aachu maapu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="whatsapp full aachu maapu"/>
</div>
<div class="lyrico-lyrics-wrapper">veetla paathale aapu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veetla paathale aapu"/>
</div>
<div class="lyrico-lyrics-wrapper">catting old fashion aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="catting old fashion aachu"/>
</div>
<div class="lyrico-lyrics-wrapper">dating trendinga aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dating trendinga aachu"/>
</div>
<div class="lyrico-lyrics-wrapper">aasa peraasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aasa peraasa"/>
</div>
<div class="lyrico-lyrics-wrapper">nee adakku on aasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee adakku on aasa"/>
</div>
<div class="lyrico-lyrics-wrapper">dosa podi dosa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dosa podi dosa"/>
</div>
<div class="lyrico-lyrics-wrapper">onna pathu naan koosa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="onna pathu naan koosa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">podu thooki podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="podu thooki podu"/>
</div>
<div class="lyrico-lyrics-wrapper">pazhasa thooki 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pazhasa thooki "/>
</div>
<div class="lyrico-lyrics-wrapper">oram podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oram podu"/>
</div>
<div class="lyrico-lyrics-wrapper">podu yeathi podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="podu yeathi podu"/>
</div>
<div class="lyrico-lyrics-wrapper">yevanum yethutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yevanum yethutha"/>
</div>
<div class="lyrico-lyrics-wrapper">avana podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avana podu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">saa gatha poranthom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saa gatha poranthom"/>
</div>
<div class="lyrico-lyrics-wrapper">oru naal yellarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru naal yellarum"/>
</div>
<div class="lyrico-lyrics-wrapper">kavala yedhukku today
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kavala yedhukku today"/>
</div>
<div class="lyrico-lyrics-wrapper">fulla fun panrom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="fulla fun panrom"/>
</div>
<div class="lyrico-lyrics-wrapper">thathuva gyani yella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thathuva gyani yella"/>
</div>
<div class="lyrico-lyrics-wrapper">iruntha konjan poo oram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iruntha konjan poo oram"/>
</div>
<div class="lyrico-lyrics-wrapper">nalaiku yenna nadanthalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nalaiku yenna nadanthalum"/>
</div>
<div class="lyrico-lyrics-wrapper">brave ah than face panrom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="brave ah than face panrom"/>
</div>
<div class="lyrico-lyrics-wrapper">illama iruku thudipu iruku katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="illama iruku thudipu iruku katti"/>
</div>
<div class="lyrico-lyrics-wrapper">kakka nalu ponna madakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kakka nalu ponna madakku"/>
</div>
<div class="lyrico-lyrics-wrapper">iravu muzhudhum vediyum varaikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iravu muzhudhum vediyum varaikum"/>
</div>
<div class="lyrico-lyrics-wrapper">beach house party nadathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="beach house party nadathu"/>
</div>
<div class="lyrico-lyrics-wrapper">yevana pathum bayame illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yevana pathum bayame illa"/>
</div>
<div class="lyrico-lyrics-wrapper">nala peru vanga neramu illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nala peru vanga neramu illa"/>
</div>
<div class="lyrico-lyrics-wrapper">innaiku setha nalaiku paalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="innaiku setha nalaiku paalu"/>
</div>
<div class="lyrico-lyrics-wrapper">athukkula oru quarter ah sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athukkula oru quarter ah sollu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aasaikku alave illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aasaikku alave illa"/>
</div>
<div class="lyrico-lyrics-wrapper">yaarukum nerma illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaarukum nerma illa"/>
</div>
<div class="lyrico-lyrics-wrapper">inime naan enna solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inime naan enna solla"/>
</div>
<div class="lyrico-lyrics-wrapper">yevanumey sariyaa illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yevanumey sariyaa illa"/>
</div>
<div class="lyrico-lyrics-wrapper">podaa moditu poda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="podaa moditu poda"/>
</div>
<div class="lyrico-lyrics-wrapper">veena pesatha rooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veena pesatha rooda"/>
</div>
<div class="lyrico-lyrics-wrapper">otta potutu pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="otta potutu pona"/>
</div>
<div class="lyrico-lyrics-wrapper">thaana mariduma da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaana mariduma da"/>
</div>
<div class="lyrico-lyrics-wrapper">google il voor thedum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="google il voor thedum"/>
</div>
<div class="lyrico-lyrics-wrapper">antha google yaara thedum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antha google yaara thedum"/>
</div>
<div class="lyrico-lyrics-wrapper">namma vooru paiyan thedum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="namma vooru paiyan thedum"/>
</div>
<div class="lyrico-lyrics-wrapper">athil namakum peruma serum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athil namakum peruma serum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">podu thooki podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="podu thooki podu"/>
</div>
<div class="lyrico-lyrics-wrapper">pazhasa thooki 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pazhasa thooki "/>
</div>
<div class="lyrico-lyrics-wrapper">oram podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oram podu"/>
</div>
<div class="lyrico-lyrics-wrapper">podu yeathi podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="podu yeathi podu"/>
</div>
<div class="lyrico-lyrics-wrapper">yevanum yethutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yevanum yethutha"/>
</div>
<div class="lyrico-lyrics-wrapper">avana podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avana podu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">podu thooki podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="podu thooki podu"/>
</div>
<div class="lyrico-lyrics-wrapper">pazhasa thooki 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pazhasa thooki "/>
</div>
<div class="lyrico-lyrics-wrapper">oram podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oram podu"/>
</div>
<div class="lyrico-lyrics-wrapper">podu yeathi podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="podu yeathi podu"/>
</div>
<div class="lyrico-lyrics-wrapper">yevanum yethutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yevanum yethutha"/>
</div>
<div class="lyrico-lyrics-wrapper">avana podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avana podu"/>
</div>
</pre>
