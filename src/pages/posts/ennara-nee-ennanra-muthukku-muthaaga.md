---
title: "ennara nee ennanra song lyrics"
album: "Muthukku Muthaaga"
artist: "Kavi Periyathambi"
lyricist: "Na. Muthukumar - Nandalala"
director: "Rasu Madhuravan"
path: "/albums/muthukku-muthaaga-lyrics"
song: "Ennara Nee Ennanra"
image: ../../images/albumart/muthukku-muthaaga.jpg
date: 2011-03-18
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Dfykwivw_3s"
type: "happy"
singers:
  - Mukesh
  - Krishna Beura
  - Roshini
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ennaandra nee ennaandra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaandra nee ennaandra"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaandra ennaandra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaandra ennaandra"/>
</div>
<div class="lyrico-lyrics-wrapper">Onnappaarthu visiladippen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnappaarthu visiladippen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaandra nee ennaandra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaandra nee ennaandra"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Visiladichaa palla odaippen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Visiladichaa palla odaippen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaandra nee ennaandra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaandra nee ennaandra"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yei sembaattikki ennavaiyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei sembaattikki ennavaiyi"/>
</div>
<div class="lyrico-lyrics-wrapper">Appuramaa enna vaiyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appuramaa enna vaiyi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaandra nee ennaandra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaandra nee ennaandra"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennaandra nee ennaandra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaandra nee ennaandra"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaandra nee ennaandra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaandra nee ennaandra"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye gumma ye gumthalakkadi gumma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye gumma ye gumthalakkadi gumma"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye gummiyadikkanum yemma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye gummiyadikkanum yemma"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye gunijadikkam summa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye gunijadikkam summa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yei kumaripponnunga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei kumaripponnunga "/>
</div>
<div class="lyrico-lyrics-wrapper">kulunga kulunga kulunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulunga kulunga kulunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Adinga gummiyathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adinga gummiyathaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yei koozhakkaraicha sombula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei koozhakkaraicha sombula"/>
</div>
<div class="lyrico-lyrics-wrapper">Usura karaichikkudikkire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usura karaichikkudikkire"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaandra nee ennaandra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaandra nee ennaandra"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koatti pudichu alaiyura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koatti pudichu alaiyura"/>
</div>
<div class="lyrico-lyrics-wrapper">Moappa naayaa kolaikkira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moappa naayaa kolaikkira"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaandra nee ennaandra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaandra nee ennaandra"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mallippoo nee yemburadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mallippoo nee yemburadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Emmanasa kaavu paakkuradi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emmanasa kaavu paakkuradi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yei Ennaandra nee ennaandra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei Ennaandra nee ennaandra"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennaandra nee ennaandra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaandra nee ennaandra"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koakku maakku pannudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koakku maakku pannudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Keeththu maaththaa vettuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Keeththu maaththaa vettuven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yei Ennaandra nee ennaandra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei Ennaandra nee ennaandra"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennaandra nee ennaandra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaandra nee ennaandra"/>
</div>
<div class="lyrico-lyrics-wrapper">Salavathaakka veluththuruven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Salavathaakka veluththuruven"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaandra nee ennaandra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaandra nee ennaandra"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye sanga pudichu aruthuruven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye sanga pudichu aruthuruven"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaandra nee ennaandra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaandra nee ennaandra"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yei koththaa onnu vachirukken
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei koththaa onnu vachirukken"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaayappoattu thachiduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaayappoattu thachiduven"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaandra nee ennaandra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaandra nee ennaandra"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennaandra nee ennaandra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaandra nee ennaandra"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaandra nee ennaandra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaandra nee ennaandra"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yei kaachi moochinnu kaththura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei kaachi moochinnu kaththura"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaakkaa mullaa kuththura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaakkaa mullaa kuththura"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaandra nee ennaandra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaandra nee ennaandra"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yendaa enna suthura 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yendaa enna suthura "/>
</div>
<div class="lyrico-lyrics-wrapper">thondathanni vaththura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thondathanni vaththura"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaandra nee ennaandra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaandra nee ennaandra"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yei yei en thoappey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei yei en thoappey"/>
</div>
<div class="lyrico-lyrics-wrapper">En thengaa naan thinnaa unakkemmaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En thengaa naan thinnaa unakkemmaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yei Ennaandra nee ennaandra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei Ennaandra nee ennaandra"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennaandra nee ennaandra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaandra nee ennaandra"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yei paruppu vegumaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei paruppu vegumaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Odippoadaa vegamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odippoadaa vegamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennaandra nee ennaandra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaandra nee ennaandra"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaandra ennaandra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaandra ennaandra"/>
</div>
<div class="lyrico-lyrics-wrapper">Onnappaarthu visiladippen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnappaarthu visiladippen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaandra nee ennaandra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaandra nee ennaandra"/>
</div>
</pre>
