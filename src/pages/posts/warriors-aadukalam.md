---
title: "warriors song lyrics"
album: "Aadukalam"
artist: "G. V. Prakash Kumar"
lyricist: "Yogi B"
director: "V. I. S. Jayapalan"
path: "/albums/aadukalam-lyrics"
song: "Warriors"
image: ../../images/albumart/aadukalam.jpg
date: 2011-01-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/qaNNURhggdQ"
type: "rap"
singers:
  - Alwa Vasu
  - Yogi B
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hello hello mic testing
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hello hello mic testing"/>
</div>
<div class="lyrico-lyrics-wrapper">Hello one two three hello
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hello one two three hello"/>
</div>
<div class="lyrico-lyrics-wrapper">yov ennaya kora koranu kekuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yov ennaya kora koranu kekuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">ethaiyavathu kondu vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethaiyavathu kondu vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">kaila kuduthureenga ungapaatla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaila kuduthureenga ungapaatla"/>
</div>
<div class="lyrico-lyrics-wrapper">petaai karan party nadathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="petaai karan party nadathum"/>
</div>
<div class="lyrico-lyrics-wrapper">ayyuvp ninaivu kopai mahanaatirku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ayyuvp ninaivu kopai mahanaatirku"/>
</div>
<div class="lyrico-lyrics-wrapper">varugai thanthirukindra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varugai thanthirukindra"/>
</div>
<div class="lyrico-lyrics-wrapper">ungal anaivaraiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ungal anaivaraiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">vilaa kamitti sarbaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vilaa kamitti sarbaga"/>
</div>
<div class="lyrico-lyrics-wrapper">varuga varuga ena varaverkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varuga varuga ena varaverkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Warriors come out to play
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Warriors come out to play"/>
</div>
<div class="lyrico-lyrics-wrapper">It's all goin down today
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="It's all goin down today"/>
</div>
<div class="lyrico-lyrics-wrapper">Heads up it's do or die
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heads up it's do or die"/>
</div>
<div class="lyrico-lyrics-wrapper">You gotta kill to survive
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You gotta kill to survive"/>
</div>
<div class="lyrico-lyrics-wrapper">They all bringing blood tonight
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="They all bringing blood tonight"/>
</div>
<div class="lyrico-lyrics-wrapper">am I bringing blood tonight
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="am I bringing blood tonight"/>
</div>
<div class="lyrico-lyrics-wrapper">Go to raid thamilan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Go to raid thamilan "/>
</div>
<div class="lyrico-lyrics-wrapper">it's time for pride
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="it's time for pride"/>
</div>
<div class="lyrico-lyrics-wrapper">Ready steady woop out the cage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ready steady woop out the cage"/>
</div>
<div class="lyrico-lyrics-wrapper">Bo bo bo Feel the bully and the rage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bo bo bo Feel the bully and the rage"/>
</div>
<div class="lyrico-lyrics-wrapper">kokarikum kozhi vellathappo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kokarikum kozhi vellathappo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Can't complain respect all the rules
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Can't complain respect all the rules"/>
</div>
<div class="lyrico-lyrics-wrapper">That be for fun it's winner take all
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="That be for fun it's winner take all"/>
</div>
<div class="lyrico-lyrics-wrapper">The ultimate showdown
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="The ultimate showdown"/>
</div>
<div class="lyrico-lyrics-wrapper">one and only standing tall
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="one and only standing tall"/>
</div>
<div class="lyrico-lyrics-wrapper">Champion
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Champion"/>
</div>
<div class="lyrico-lyrics-wrapper">Champion
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Champion"/>
</div>
<div class="lyrico-lyrics-wrapper">Champion
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Champion"/>
</div>
<div class="lyrico-lyrics-wrapper">Champion
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Champion"/>
</div>
<div class="lyrico-lyrics-wrapper">They've got damn lies hit on you low
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="They've got damn lies hit on you low"/>
</div>
<div class="lyrico-lyrics-wrapper">The boys on the river
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="The boys on the river"/>
</div>
<div class="lyrico-lyrics-wrapper">Count your applause and before you know
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Count your applause and before you know"/>
</div>
<div class="lyrico-lyrics-wrapper">It's payback sucker
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="It's payback sucker"/>
</div>
<div class="lyrico-lyrics-wrapper">My pain and agony will soon forever fade away
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="My pain and agony will soon forever fade away"/>
</div>
<div class="lyrico-lyrics-wrapper">Take a look into my eyes and 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Take a look into my eyes and "/>
</div>
<div class="lyrico-lyrics-wrapper">you will see the bird of prey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="you will see the bird of prey"/>
</div>
<div class="lyrico-lyrics-wrapper">And if you wanna know what happened to my enemies
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="And if you wanna know what happened to my enemies"/>
</div>
<div class="lyrico-lyrics-wrapper">karuppu skin them alive
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karuppu skin them alive"/>
</div>
<div class="lyrico-lyrics-wrapper">Now bo-oh a rope around them neck swinging
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Now bo-oh a rope around them neck swinging"/>
</div>
<div class="lyrico-lyrics-wrapper">Hang em all at the gates of Hell
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hang em all at the gates of Hell"/>
</div>
<div class="lyrico-lyrics-wrapper">We be love-rough and rowdy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="We be love-rough and rowdy"/>
</div>
<div class="lyrico-lyrics-wrapper">Tribal Kings don't stop 'til we heard the bell
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tribal Kings don't stop 'til we heard the bell"/>
</div>
<div class="lyrico-lyrics-wrapper">Show them up with all those pearls sunk in
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Show them up with all those pearls sunk in"/>
</div>
<div class="lyrico-lyrics-wrapper">Only the best will live to tell
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Only the best will live to tell"/>
</div>
<div class="lyrico-lyrics-wrapper">It's all guts and glory 'til the end
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="It's all guts and glory 'til the end"/>
</div>
<div class="lyrico-lyrics-wrapper">Fightin cocks what I hear you yell
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fightin cocks what I hear you yell"/>
</div>
<div class="lyrico-lyrics-wrapper">aadukalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aadukalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Fightin cocks what I hear you yell
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fightin cocks what I hear you yell"/>
</div>
<div class="lyrico-lyrics-wrapper">aadukalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aadukalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Fightin cocks what I hear you yell
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fightin cocks what I hear you yell"/>
</div>
<div class="lyrico-lyrics-wrapper">aadukalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aadukalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Fightin cocks what I hear you yell
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fightin cocks what I hear you yell"/>
</div>
<div class="lyrico-lyrics-wrapper">aadukalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aadukalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Fightin cocks what I hear you yell c'mon
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fightin cocks what I hear you yell c'mon"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh my god
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh my god"/>
</div>
<div class="lyrico-lyrics-wrapper">Ladies and Gentlemen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ladies and Gentlemen"/>
</div>
<div class="lyrico-lyrics-wrapper">I give you the new reigning 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I give you the new reigning "/>
</div>
<div class="lyrico-lyrics-wrapper">champion of the
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="champion of the"/>
</div>
<div class="lyrico-lyrics-wrapper">Game over now I'm number one
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Game over now I'm number one"/>
</div>
<div class="lyrico-lyrics-wrapper">Unbeatable grand champion
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unbeatable grand champion"/>
</div>
<div class="lyrico-lyrics-wrapper">Big money come we're movin up
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Big money come we're movin up"/>
</div>
<div class="lyrico-lyrics-wrapper">My shawty loves grand champion
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="My shawty loves grand champion"/>
</div>
<div class="lyrico-lyrics-wrapper">Game over now I'm number one
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Game over now I'm number one"/>
</div>
<div class="lyrico-lyrics-wrapper">Unbeatable grand champion
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unbeatable grand champion"/>
</div>
<div class="lyrico-lyrics-wrapper">Big money come we're movin' up
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Big money come we're movin' up"/>
</div>
<div class="lyrico-lyrics-wrapper">My shawty loves grand champion
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="My shawty loves grand champion"/>
</div>
<div class="lyrico-lyrics-wrapper">Rock, what ya doin'
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rock, what ya doin'"/>
</div>
<div class="lyrico-lyrics-wrapper">My makkal now are ya ready?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="My makkal now are ya ready?"/>
</div>
<div class="lyrico-lyrics-wrapper">Like got it a
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Like got it a"/>
</div>
<div class="lyrico-lyrics-wrapper">It's the mother
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="It's the mother"/>
</div>
<div class="lyrico-lyrics-wrapper">mother of all party downs
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mother of all party downs"/>
</div>
<div class="lyrico-lyrics-wrapper">The new king in town
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="The new king in town"/>
</div>
<div class="lyrico-lyrics-wrapper">Everybody saw how I stupid
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Everybody saw how I stupid"/>
</div>
<div class="lyrico-lyrics-wrapper">Smack that clown
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Smack that clown"/>
</div>
<div class="lyrico-lyrics-wrapper">Cash money
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cash money"/>
</div>
<div class="lyrico-lyrics-wrapper">Paid in full
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paid in full"/>
</div>
<div class="lyrico-lyrics-wrapper">Got my mumma a new succesory
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Got my mumma a new succesory"/>
</div>
<div class="lyrico-lyrics-wrapper">By the homies rockin' new shades
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="By the homies rockin' new shades"/>
</div>
<div class="lyrico-lyrics-wrapper">Now we eat only biriyani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Now we eat only biriyani"/>
</div>
<div class="lyrico-lyrics-wrapper">My big brother he got it hard up a gang
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="My big brother he got it hard up a gang"/>
</div>
<div class="lyrico-lyrics-wrapper">So I let him know that with a fat gold ring
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="So I let him know that with a fat gold ring"/>
</div>
<div class="lyrico-lyrics-wrapper">To those who cared for me in pain and strife
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="To those who cared for me in pain and strife"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanks, I'm living the good life
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanks, I'm living the good life"/>
</div>
<div class="lyrico-lyrics-wrapper">But all of this wouldn't be possible
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="But all of this wouldn't be possible"/>
</div>
<div class="lyrico-lyrics-wrapper">If it weren't for my one very special
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="If it weren't for my one very special"/>
</div>
<div class="lyrico-lyrics-wrapper">Our love is worth more than any money
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Our love is worth more than any money"/>
</div>
<div class="lyrico-lyrics-wrapper">So when I gave it all back I got the honey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="So when I gave it all back I got the honey"/>
</div>
<div class="lyrico-lyrics-wrapper">I got the honey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I got the honey"/>
</div>
<div class="lyrico-lyrics-wrapper">Game over now I'm number one
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Game over now I'm number one"/>
</div>
<div class="lyrico-lyrics-wrapper">Unbeatable grand champion
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unbeatable grand champion"/>
</div>
<div class="lyrico-lyrics-wrapper">Big money come we're movin' up
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Big money come we're movin' up"/>
</div>
<div class="lyrico-lyrics-wrapper">My shawty loves grand champion
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="My shawty loves grand champion"/>
</div>
<div class="lyrico-lyrics-wrapper">Big money come we're movin' up
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Big money come we're movin' up"/>
</div>
<div class="lyrico-lyrics-wrapper">My shawty loves grand champion
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="My shawty loves grand champion"/>
</div>
</pre>
