---
title: "avan song lyrics"
album: "Seethakaathi"
artist: "Govind Vasantha"
lyricist: "Madhan Karky"
director: "Balaji Tharaneetharan"
path: "/albums/seethakaathi-lyrics"
song: "Avan"
image: ../../images/albumart/seethakaathi.jpg
date: 2018-12-20
lang: tamil
youtubeLink: "https://www.youtube.com/embed/7iIXMRDQsVo"
type: "melody"
singers:
  - Harish Sivaramakrishnan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hmmm mmmmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmmm mmmmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Mmmm mmmmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mmmm mmmmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmmm hmmmmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmmm hmmmmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Mmm mmm mmmmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mmm mmm mmmmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmmm hmmmmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmmm hmmmmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Mmm mmm mmmmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mmm mmm mmmmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmmm hmmmmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmmm hmmmmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Mmm mmm mmmmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mmm mmm mmmmm"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avan thugal neeya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan thugal neeya"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan thazhal neeya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan thazhal neeya"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan nizhal neeya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan nizhal neeya"/>
</div>
<div class="lyrico-lyrics-wrapper">Avanae neeya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avanae neeya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avan thugal neeya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan thugal neeya"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan thazhal neeya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan thazhal neeya"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan nizhal neeya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan nizhal neeya"/>
</div>
<div class="lyrico-lyrics-wrapper">Avanae neeya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avanae neeya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hmm hmm hmmkuhmmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm hmm hmmkuhmmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Poriyaai sudaraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poriyaai sudaraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un vizhigalil avan ozhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un vizhigalil avan ozhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Asaivaai ozhiyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asaivaai ozhiyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un udalengum avan mozhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un udalengum avan mozhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thee thindra piragum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thee thindra piragum"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan theeravillaiyae thuliyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan theeravillaiyae thuliyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mann unda piragum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mann unda piragum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un ullae ullae avan muzhaikkiraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un ullae ullae avan muzhaikkiraan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un punnagai avanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un punnagai avanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee paarkum paarvai athu avanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee paarkum paarvai athu avanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un vaarthaigal avanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un vaarthaigal avanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee vazhum vaazhkaiyae avan avan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee vazhum vaazhkaiyae avan avan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un paadhaiyum avanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un paadhaiyum avanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un paadhai pookkalum avanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un paadhai pookkalum avanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Poo vaasamum avanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poo vaasamum avanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kollum swasamum avan avan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kollum swasamum avan avan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poriyaai sudaraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poriyaai sudaraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un vizhigalil avan ozhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un vizhigalil avan ozhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Asaivaai ozhiyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asaivaai ozhiyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un udalengum avan mozhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un udalengum avan mozhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thee thindra piragum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thee thindra piragum"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan theera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan theera"/>
</div>
<div class="lyrico-lyrics-wrapper">villaiyae thuliyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="villaiyae thuliyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mann unda piragum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mann unda piragum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un ullae ullae 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un ullae ullae "/>
</div>
<div class="lyrico-lyrics-wrapper">avan muzhaikkiraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avan muzhaikkiraan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avan thugal neeya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan thugal neeya"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan thazhal neeya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan thazhal neeya"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan nizhal neeya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan nizhal neeya"/>
</div>
<div class="lyrico-lyrics-wrapper">Avanae neeya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avanae neeya"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan thugal neeya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan thugal neeya"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan thazhal neeya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan thazhal neeya"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan nizhal neeya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan nizhal neeya"/>
</div>
<div class="lyrico-lyrics-wrapper">Avanae neeya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avanae neeya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Huh huh huh huh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Huh huh huh huh"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmm hmm hmmkuhmmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm hmm hmmkuhmmm"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee oru naadagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee oru naadagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuru naadagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuru naadagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu medaiyaai avan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu medaiyaai avan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeyo oru bommaiyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyo oru bommaiyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir bommaiyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir bommaiyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai asaippavan avan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai asaippavan avan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neengaa kanavaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neengaa kanavaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kannil neezhukindran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kannil neezhukindran"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudiya orr isaiyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudiya orr isaiyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kaadhil vazhugindraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kaadhil vazhugindraan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paavam ammaranam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paavam ammaranam"/>
</div>
<div class="lyrico-lyrics-wrapper">Athan velai kedukkiraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athan velai kedukkiraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Adadaa iranthum iranthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adadaa iranthum iranthum"/>
</div>
<div class="lyrico-lyrics-wrapper">Iranthum kodukkiraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iranthum kodukkiraan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Seethakaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seethakaathi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poriyaai sudaraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poriyaai sudaraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un vizhigalil avan ozhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un vizhigalil avan ozhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Asaivaai ozhiyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asaivaai ozhiyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un udalengum avan mozhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un udalengum avan mozhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thee thindra piragum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thee thindra piragum"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan theeravillaiyae thuliyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan theeravillaiyae thuliyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mann unda piragum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mann unda piragum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un ullae ullae avan muzhaikkiraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un ullae ullae avan muzhaikkiraan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un punnagai avanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un punnagai avanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee paarkum paarvai athu avanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee paarkum paarvai athu avanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un vaarthaigal avanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un vaarthaigal avanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee vazhum vaazhkaiyae avan avan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee vazhum vaazhkaiyae avan avan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un paadhaiyum avanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un paadhaiyum avanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un paadhai pookkalum avanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un paadhai pookkalum avanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Poo vaasamum avanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poo vaasamum avanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kollum swasamum avan avan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kollum swasamum avan avan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avan thugal neeya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan thugal neeya"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan thazhal neeya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan thazhal neeya"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan nizhal neeya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan nizhal neeya"/>
</div>
<div class="lyrico-lyrics-wrapper">Avanae neeya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avanae neeya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avan thugal neeya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan thugal neeya"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan thazhal neeya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan thazhal neeya"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan nizhal neeya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan nizhal neeya"/>
</div>
<div class="lyrico-lyrics-wrapper">Avanae neeya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avanae neeya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Huh huh huh huh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Huh huh huh huh"/>
</div>
<div class="lyrico-lyrics-wrapper">Huh huh huh huh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Huh huh huh huh"/>
</div>
<div class="lyrico-lyrics-wrapper">Huh huh huh huh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Huh huh huh huh"/>
</div>
<div class="lyrico-lyrics-wrapper">Huh huh huh huh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Huh huh huh huh"/>
</div>
</pre>
