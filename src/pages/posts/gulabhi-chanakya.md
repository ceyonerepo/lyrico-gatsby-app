---
title: "gulabhi song lyrics"
album: "Chanakya"
artist: "Vishal Chandrasekhar"
lyricist: Ramajogayya Sastry"
director: "Thiru"
path: "/albums/chanakya-lyrics"
song: "Gulabhi"
image: ../../images/albumart/chanakya.jpg
date: 2019-10-05
lang: telugu
youtubeLink: "https://www.youtube.com/embed/6Hk3cpCcRmU"
type: "love"
singers:
  - Anurag Kulkarni
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Cinderella Pori 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cinderella Pori "/>
</div>
<div class="lyrico-lyrics-wrapper">Umbrella Pattana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Umbrella Pattana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Englandu Rani 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Englandu Rani "/>
</div>
<div class="lyrico-lyrics-wrapper">Salute Kottana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Salute Kottana"/>
</div>
<div class="lyrico-lyrics-wrapper">Bettu Cheyamake 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bettu Cheyamake "/>
</div>
<div class="lyrico-lyrics-wrapper">Andhanantha Yethuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhanantha Yethuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Dikku Mokku Neeve 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dikku Mokku Neeve "/>
</div>
<div class="lyrico-lyrics-wrapper">Entha Kastapettina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entha Kastapettina"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Bahubalila Kaththi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Bahubalila Kaththi "/>
</div>
<div class="lyrico-lyrics-wrapper">Dusi Raakala Rakaasi Pilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dusi Raakala Rakaasi Pilla"/>
</div>
<div class="lyrico-lyrics-wrapper">James Bondu Gun Theesi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="James Bondu Gun Theesi "/>
</div>
<div class="lyrico-lyrics-wrapper">Kalchake Angry Rangeela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalchake Angry Rangeela"/>
</div>
<div class="lyrico-lyrics-wrapper">Keka Petti Dookamaake 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Keka Petti Dookamaake "/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvala Lady Bruce Lee La
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvala Lady Bruce Lee La"/>
</div>
<div class="lyrico-lyrics-wrapper">Yuddamaapi Maripove 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yuddamaapi Maripove "/>
</div>
<div class="lyrico-lyrics-wrapper">Buddhudi Sister La
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Buddhudi Sister La"/>
</div>
<div class="lyrico-lyrics-wrapper">Ne Vallane Maa Zindagi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne Vallane Maa Zindagi "/>
</div>
<div class="lyrico-lyrics-wrapper">Reverse Gerai Nilichinde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Reverse Gerai Nilichinde"/>
</div>
<div class="lyrico-lyrics-wrapper">Ne Vallane Maa Life Ila 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne Vallane Maa Life Ila "/>
</div>
<div class="lyrico-lyrics-wrapper">Roadeki Yekyekki Yedichinde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Roadeki Yekyekki Yedichinde"/>
</div>
<div class="lyrico-lyrics-wrapper">Ne Vallane Ma Body Ki 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne Vallane Ma Body Ki "/>
</div>
<div class="lyrico-lyrics-wrapper">Bindassu BP Periginde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bindassu BP Periginde"/>
</div>
<div class="lyrico-lyrics-wrapper">Ne Vallane Ma Fate Ila 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne Vallane Ma Fate Ila "/>
</div>
<div class="lyrico-lyrics-wrapper">Dustbin Lo Paper La Naliginde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dustbin Lo Paper La Naliginde"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Baby Gulabi Gulabi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby Gulabi Gulabi "/>
</div>
<div class="lyrico-lyrics-wrapper">Maa Life Line Nuvve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa Life Line Nuvve"/>
</div>
<div class="lyrico-lyrics-wrapper">Baby Gulabi Gulabi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby Gulabi Gulabi "/>
</div>
<div class="lyrico-lyrics-wrapper">Maa Lucky Star Nuvve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa Lucky Star Nuvve"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Chuttu Tirigi Jai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Chuttu Tirigi Jai "/>
</div>
<div class="lyrico-lyrics-wrapper">Kotte Diehardu Fans Maymay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kotte Diehardu Fans Maymay"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Koncham Karigi Maa Kosam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Koncham Karigi Maa Kosam "/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Autograph Cheyave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Autograph Cheyave"/>
</div>
<div class="lyrico-lyrics-wrapper">Na Na Na Naana Naana 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na Na Na Naana Naana "/>
</div>
<div class="lyrico-lyrics-wrapper">Na Nanna Nanna Na Na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na Nanna Nanna Na Na"/>
</div>
<div class="lyrico-lyrics-wrapper">Na Na Na Naana Naana 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na Na Na Naana Naana "/>
</div>
<div class="lyrico-lyrics-wrapper">Na Nanna Nanna Na Na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na Nanna Nanna Na Na"/>
</div>
<div class="lyrico-lyrics-wrapper">Baby Gulabi Gulabi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby Gulabi Gulabi "/>
</div>
<div class="lyrico-lyrics-wrapper">Na Na Na Naana Naana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na Na Na Naana Naana"/>
</div>
<div class="lyrico-lyrics-wrapper">Baby Gulabi Gulabi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby Gulabi Gulabi "/>
</div>
<div class="lyrico-lyrics-wrapper">Na Na Na Naana Naana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na Na Na Naana Naana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ayyayo Pori Oh Oh Oh Pori 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyayo Pori Oh Oh Oh Pori "/>
</div>
<div class="lyrico-lyrics-wrapper">Na Life Lineye Nuvve Oh Oh Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na Life Lineye Nuvve Oh Oh Oh"/>
</div>
<div class="lyrico-lyrics-wrapper">Yennaley Ee Thippalu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yennaley Ee Thippalu "/>
</div>
<div class="lyrico-lyrics-wrapper">Yennaley Padigapulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yennaley Padigapulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yegirayi Ma Chippulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yegirayi Ma Chippulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Arigayi Kalchippalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arigayi Kalchippalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chuttura Tippinchi Nuvvu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chuttura Tippinchi Nuvvu "/>
</div>
<div class="lyrico-lyrics-wrapper">Chupinchodey Chukkalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chupinchodey Chukkalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kopaley Guppinchi Maa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kopaley Guppinchi Maa "/>
</div>
<div class="lyrico-lyrics-wrapper">Batukulu Cheyake Mukkalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Batukulu Cheyake Mukkalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ne Ego Lu Tesesi Maa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne Ego Lu Tesesi Maa "/>
</div>
<div class="lyrico-lyrics-wrapper">Godu Vinave Koncham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Godu Vinave Koncham"/>
</div>
<div class="lyrico-lyrics-wrapper">Okay Anu Ee Okkasariki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okay Anu Ee Okkasariki"/>
</div>
<div class="lyrico-lyrics-wrapper">Ne Vallane Maa Zindagi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne Vallane Maa Zindagi "/>
</div>
<div class="lyrico-lyrics-wrapper">Reverse Gerai Nilichinde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Reverse Gerai Nilichinde"/>
</div>
<div class="lyrico-lyrics-wrapper">Ne Vallane Maa Life Ila 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne Vallane Maa Life Ila "/>
</div>
<div class="lyrico-lyrics-wrapper">Roadeki Yekyekki Yedichinde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Roadeki Yekyekki Yedichinde"/>
</div>
<div class="lyrico-lyrics-wrapper">Ne Vallane Ma Body Ki 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne Vallane Ma Body Ki "/>
</div>
<div class="lyrico-lyrics-wrapper">Bindassu BP Periginde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bindassu BP Periginde"/>
</div>
<div class="lyrico-lyrics-wrapper">Ne Vallane Ma Fate Ila 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne Vallane Ma Fate Ila "/>
</div>
<div class="lyrico-lyrics-wrapper">Dustbin Lo Paper La Naliginde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dustbin Lo Paper La Naliginde"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Baby Gulabi Gulabi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby Gulabi Gulabi "/>
</div>
<div class="lyrico-lyrics-wrapper">Maa Life Line Nuvve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa Life Line Nuvve"/>
</div>
<div class="lyrico-lyrics-wrapper">Baby Gulabi Gulabi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby Gulabi Gulabi "/>
</div>
<div class="lyrico-lyrics-wrapper">Maa Lucky Star Nuvve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa Lucky Star Nuvve"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Chuttu Tirigi Jai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Chuttu Tirigi Jai "/>
</div>
<div class="lyrico-lyrics-wrapper">Kotte Diehardu Fans Maymay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kotte Diehardu Fans Maymay"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Koncham Karigi Maa Kosam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Koncham Karigi Maa Kosam "/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Autograph Cheyave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Autograph Cheyave"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Bahubalila Kaththi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Bahubalila Kaththi "/>
</div>
<div class="lyrico-lyrics-wrapper">Dusi Raakala Rakaasi Pilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dusi Raakala Rakaasi Pilla"/>
</div>
<div class="lyrico-lyrics-wrapper">James Bondu Gun Theesi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="James Bondu Gun Theesi "/>
</div>
<div class="lyrico-lyrics-wrapper">Kalchake Angry Rangeela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalchake Angry Rangeela"/>
</div>
<div class="lyrico-lyrics-wrapper">Keka Petti Dookamaake 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Keka Petti Dookamaake "/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvala Lady Bruce Lee La
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvala Lady Bruce Lee La"/>
</div>
<div class="lyrico-lyrics-wrapper">Yuddamaapi Maripove 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yuddamaapi Maripove "/>
</div>
<div class="lyrico-lyrics-wrapper">Buddhudi Sister La
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Buddhudi Sister La"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Cinderella Pori 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cinderella Pori "/>
</div>
<div class="lyrico-lyrics-wrapper">Umbrella Pattana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Umbrella Pattana"/>
</div>
<div class="lyrico-lyrics-wrapper">Englandu Rani 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Englandu Rani "/>
</div>
<div class="lyrico-lyrics-wrapper">Salute Kottana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Salute Kottana"/>
</div>
<div class="lyrico-lyrics-wrapper">Bettu Cheyamake 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bettu Cheyamake "/>
</div>
<div class="lyrico-lyrics-wrapper">Andhanantha Yethuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhanantha Yethuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Dikku Mokku Neeve 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dikku Mokku Neeve "/>
</div>
<div class="lyrico-lyrics-wrapper">Entha Kastapettina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entha Kastapettina"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Baby Gulabi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby Gulabi"/>
</div>
</pre>
