---
title: "hey keechu kiliye song lyrics"
album: "Mugavari"
artist: "Deva"
lyricist: "Vairamuthu"
director: "V. Z. Durai"
path: "/albums/mugavari-lyrics"
song: "Hey Keechu Kiliye"
image: ../../images/albumart/mugavari.jpg
date: 2000-02-19
lang: tamil
youtubeLink: "https://www.youtube.com/embed/-0IhwvwUZmY"
type: "happy"
singers:
  - Hariharan
  - Chorus
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hey Hey Keechu Kiliyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Hey Keechu Kiliyae"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kaadhil Thithithaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kaadhil Thithithaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Isaiyaalae Enathu Pudhiya Naalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Isaiyaalae Enathu Pudhiya Naalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Indru Thiranthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Indru Thiranthaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Hey Hey Keechu Kiliyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Hey Hey Keechu Kiliyae"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kaadhil Thithithaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kaadhil Thithithaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Isaiyaalae Enathu Pudhiya Naalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Isaiyaalae Enathu Pudhiya Naalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Indru Thiranthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Indru Thiranthaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thana Thana Thanaa Nath Thana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana Thana Thanaa Nath Thana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana Thana Thanaa Nath Thana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana Thana Thanaa Nath Thana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karuvondru Pirappathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuvondru Pirappathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pathu Maadhathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathu Maadhathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Iruthayam Thudippathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruthayam Thudippathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhu Maadhathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhu Maadhathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhan Uyir Sathai Isaivathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhan Uyir Sathai Isaivathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Endrum Antha Naadhathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endrum Antha Naadhathil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sonaarae Isaiyae Piyaah Piyaah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sonaarae Isaiyae Piyaah Piyaah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyirgalin Swaasam Kaatru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirgalin Swaasam Kaatru"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha Kaatrin Swaasam Gaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Kaatrin Swaasam Gaanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagae Isaiyae Ye Yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagae Isaiyae Ye Yeah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endhira Vaazhkaiyin Idaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhira Vaazhkaiyin Idaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil Eerathai Pusivathu Isaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil Eerathai Pusivathu Isaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaam Isaiyae Ye Yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaam Isaiyae Ye Yeah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal Vanthaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Vanthaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Angum Isai Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Angum Isai Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanneer Vanthaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneer Vanthaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Angum Isai Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Angum Isai Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thottil Kuzhanthai Ondru Azhuthaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottil Kuzhanthai Ondru Azhuthaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhai Thoonga Vaippathum Intha Isai Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai Thoonga Vaippathum Intha Isai Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yutha Kalathil Thookkam Tholaithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yutha Kalathil Thookkam Tholaithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kan Vizhippatharkum Intha Isai Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan Vizhippatharkum Intha Isai Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Isaiyodu Vanthom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Isaiyodu Vanthom"/>
</div>
<div class="lyrico-lyrics-wrapper">Isaiyodu Vaazhvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Isaiyodu Vaazhvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Isaiyodu Povom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Isaiyodu Povom"/>
</div>
<div class="lyrico-lyrics-wrapper">isaiyaavom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="isaiyaavom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Hey Hey Keechu Kiliyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Hey Hey Keechu Kiliyae"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kaadhil Thithithaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kaadhil Thithithaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Isaiyaalae Enathu Pudhiya Naalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Isaiyaalae Enathu Pudhiya Naalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Indru Thiranthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Indru Thiranthaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Innisai Nindru Ponaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innisai Nindru Ponaal"/>
</div>
<div class="lyrico-lyrics-wrapper">En Idhayam Nindru Pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Idhayam Nindru Pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Isaiyae Uyirae Yeyeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Isaiyae Uyirae Yeyeah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enthan Thai Mozhi Isaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enthan Thai Mozhi Isaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">En Imaigal Thudippathum Isaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Imaigal Thudippathum Isaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Engum Isaiyae Ye Yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engum Isaiyae Ye Yeah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mounam Mounam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mounam Mounam"/>
</div>
<div class="lyrico-lyrics-wrapper">En Nenjai Adaikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nenjai Adaikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Geetham Kettaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Geetham Kettaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhumeendum Thudikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhumeendum Thudikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aimpulangal Enthan Iruppu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aimpulangal Enthan Iruppu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sevi Mattum Thaan Romba Sirappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sevi Mattum Thaan Romba Sirappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil Ullathu Jeevan Pirappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil Ullathu Jeevan Pirappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanaal Kaadhil Ullathu Jeevan Enakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanaal Kaadhil Ullathu Jeevan Enakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Isaiyodu Vanthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Isaiyodu Vanthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Isaiyodu Vaazhven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Isaiyodu Vaazhven"/>
</div>
<div class="lyrico-lyrics-wrapper">Isaiyodu Poven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Isaiyodu Poven"/>
</div>
<div class="lyrico-lyrics-wrapper">isayaaven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="isayaaven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Hey Hey Keechu Kiliyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Hey Hey Keechu Kiliyae"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kaadhil Thithithaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kaadhil Thithithaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Isaiyaalae Enathu Pudhiya Naalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Isaiyaalae Enathu Pudhiya Naalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Indru Thiranthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Indru Thiranthaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Hey Hey Keechu Kiliyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Hey Hey Keechu Kiliyae"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kaadhil Thithithaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kaadhil Thithithaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Isaiyaalae Enathu Pudhiya Naalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Isaiyaalae Enathu Pudhiya Naalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Indru Thiranthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Indru Thiranthaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Hey Wow Wow
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Hey Wow Wow"/>
</div>
</pre>
