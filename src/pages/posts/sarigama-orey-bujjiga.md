---
title: "sarigama song lyrics"
album: "Orey Bujjiga"
artist: "Anup Rubens"
lyricist: "Vanamali"
director: "Vijay Kumar Konda"
path: "/albums/orey-bujjiga-lyrics"
song: "Sarigama"
image: ../../images/albumart/orey-bujjiga.jpg
date: 2020-10-02
lang: telugu
youtubeLink: "https://www.youtube.com/embed/7ccQwTTc9oE"
type: "love"
singers:
  - Anup Rubens
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Sarigama gama gaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarigama gama gaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Hangama cheddama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hangama cheddama"/>
</div>
<div class="lyrico-lyrics-wrapper">Padanisa nisa nisa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padanisa nisa nisa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee nisha nee damma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee nisha nee damma"/>
</div>
<div class="lyrico-lyrics-wrapper">O misa misa misalade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O misa misa misalade"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee neede o wonderla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee neede o wonderla"/>
</div>
<div class="lyrico-lyrics-wrapper">Thala thalamani chupe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thala thalamani chupe"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakala o thunderla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakala o thunderla"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kannullo ee velallo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kannullo ee velallo"/>
</div>
<div class="lyrico-lyrics-wrapper">O thousand watts vennelane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O thousand watts vennelane"/>
</div>
<div class="lyrico-lyrics-wrapper">Chusthu unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chusthu unna"/>
</div>
<div class="lyrico-lyrics-wrapper">Na gundello lo lothullo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na gundello lo lothullo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee bomme geesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee bomme geesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Inka loniki nettesthunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inka loniki nettesthunna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Na gunde chappudu nuvve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na gunde chappudu nuvve"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa ohala uppena nuvve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa ohala uppena nuvve"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Prathi kshanam nee kale kantunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prathi kshanam nee kale kantunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Panchana na gundeney Neeke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panchana na gundeney Neeke"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvve na sontham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvve na sontham"/>
</div>
<div class="lyrico-lyrics-wrapper">Antu innalu vesa neetho aduge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antu innalu vesa neetho aduge"/>
</div>
<div class="lyrico-lyrics-wrapper">Choodan na lokame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choodan na lokame"/>
</div>
<div class="lyrico-lyrics-wrapper">Ne lona ee kshaname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne lona ee kshaname"/>
</div>
<div class="lyrico-lyrics-wrapper">Aagaadhe ee praname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagaadhe ee praname"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayindhi ni vashame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayindhi ni vashame"/>
</div>
<div class="lyrico-lyrics-wrapper">Ni preme na gamyam kani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ni preme na gamyam kani"/>
</div>
<div class="lyrico-lyrics-wrapper">Neetho roju needai raani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neetho roju needai raani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Na gunde chappudu nuvve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na gunde chappudu nuvve"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa ohala uppena nuvve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa ohala uppena nuvve"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarigama gama gaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarigama gama gaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Hangama cheddama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hangama cheddama"/>
</div>
<div class="lyrico-lyrics-wrapper">Padanisa nisa nisa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padanisa nisa nisa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee nisha nee damma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee nisha nee damma"/>
</div>
</pre>
