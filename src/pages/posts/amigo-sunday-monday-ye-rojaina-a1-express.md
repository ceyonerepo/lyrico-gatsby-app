---
title: "amigo song lyrics"
album: "A1 Express"
artist: "Hiphop Tamizha"
lyricist: "Ramajogayya Shastri"
director: "Dennis Jeevan Kanukolanu"
path: "/albums/a1-express-lyrics"
song: "Amigo - Sunday Monday ye Rojaina"
image: ../../images/albumart/a1-express.jpg
date: 2021-03-05
lang: telugu
youtubeLink: "https://www.youtube.com/embed/NJWnxJlA3c8"
type: "love"
singers:
  - Inno Genga
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Sunday monday ye rojaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sunday monday ye rojaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaram mottham nee dhyasegaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaram mottham nee dhyasegaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnati nee fan boye ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnati nee fan boye ni"/>
</div>
<div class="lyrico-lyrics-wrapper">Friend ga maarcheshaavugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friend ga maarcheshaavugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">I am your amigo go go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I am your amigo go go"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pilla don’t leave me now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pilla don’t leave me now"/>
</div>
<div class="lyrico-lyrics-wrapper">I am your amigo go go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I am your amigo go go"/>
</div>
<div class="lyrico-lyrics-wrapper">Pilla don’t leave me now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pilla don’t leave me now"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeah yeah hey pillaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeah yeah hey pillaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa ningiki nindaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa ningiki nindaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Rainbow vaale rangula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rainbow vaale rangula"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa dhaarula podavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa dhaarula podavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee andhaala navvulaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee andhaala navvulaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvu rammannaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvu rammannaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mabbulu dhigi raalenilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mabbulu dhigi raalenilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeah yeah yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeah yeah yeah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Padi padi venta thirigina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padi padi venta thirigina"/>
</div>
<div class="lyrico-lyrics-wrapper">Prema parugula bujji manasuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prema parugula bujji manasuku"/>
</div>
<div class="lyrico-lyrics-wrapper">Shanthi dhorikenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shanthi dhorikenu"/>
</div>
<div class="lyrico-lyrics-wrapper">Neetho chanuve kaastha perigenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neetho chanuve kaastha perigenu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh my god its real
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh my god its real"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalamannadhi aagi nilichenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalamannadhi aagi nilichenu"/>
</div>
<div class="lyrico-lyrics-wrapper">Veli chivarana poolu molichenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veli chivarana poolu molichenu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kotthaga nenai merisaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kotthaga nenai merisaane"/>
</div>
<div class="lyrico-lyrics-wrapper">My days are turning beautiful
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="My days are turning beautiful"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ee life ki idhi chaalu le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee life ki idhi chaalu le"/>
</div>
<div class="lyrico-lyrics-wrapper">Prathi nimisham neetho untunna idhi chaalune
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prathi nimisham neetho untunna idhi chaalune"/>
</div>
<div class="lyrico-lyrics-wrapper">Edho naaduna kalalu nijam avuthaayile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edho naaduna kalalu nijam avuthaayile"/>
</div>
<div class="lyrico-lyrics-wrapper">Prema friendship ki geethalu cheripesaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prema friendship ki geethalu cheripesaave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sunday monday ye rojaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sunday monday ye rojaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaram mottham nee dhyasegaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaram mottham nee dhyasegaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnati nee fan boye ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnati nee fan boye ni"/>
</div>
<div class="lyrico-lyrics-wrapper">Friend ga maarcheshaavugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friend ga maarcheshaavugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">I am your amigo go go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I am your amigo go go"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pilla don’t leave me now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pilla don’t leave me now"/>
</div>
<div class="lyrico-lyrics-wrapper">I am your amigo go go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I am your amigo go go"/>
</div>
<div class="lyrico-lyrics-wrapper">Pilla don’t leave me now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pilla don’t leave me now"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeah yeah hey pillaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeah yeah hey pillaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sunday monday ye rojaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sunday monday ye rojaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaram mottham nee dhyasegaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaram mottham nee dhyasegaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnati nee fan boye ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnati nee fan boye ni"/>
</div>
<div class="lyrico-lyrics-wrapper">Friend ga maarcheshaavugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friend ga maarcheshaavugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">I am your amigo go go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I am your amigo go go"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pilla don’t leave me now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pilla don’t leave me now"/>
</div>
<div class="lyrico-lyrics-wrapper">I am your amigo go go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I am your amigo go go"/>
</div>
<div class="lyrico-lyrics-wrapper">Pilla don’t leave me now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pilla don’t leave me now"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeah yeah hey pillaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeah yeah hey pillaa"/>
</div>
</pre>
