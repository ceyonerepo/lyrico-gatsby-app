---
title: "bonalu song lyrics"
album: "Ismart Shankar"
artist: "Mani Sharma"
lyricist: "Kasarla Shyam"
director: "Puri Jagannadh"
path: "/albums/ismart-shankar-lyrics"
song: "Bonalu"
image: ../../images/albumart/ismart-shankar.jpg
date: 2019-07-18
lang: telugu
youtubeLink: "https://www.youtube.com/embed/IB26C2Dc-Xg"
type: "devotional"
singers:
  - Rahul Sipligunj
  - Mohana Bhogaraju
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nee mukku pogu merupolona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee mukku pogu merupolona"/>
</div>
<div class="lyrico-lyrics-wrapper">Poddhu podise thurupolana maisamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poddhu podise thurupolana maisamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yarra yarrani suride
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yarra yarrani suride"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee nudhutana bottayye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee nudhutana bottayye"/>
</div>
<div class="lyrico-lyrics-wrapper">O sallani soopula thalli maayamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O sallani soopula thalli maayamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ammalaganna ammaranna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammalaganna ammaranna"/>
</div>
<div class="lyrico-lyrics-wrapper">Pacchi pasupu bommaranna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pacchi pasupu bommaranna"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaapa chettu kommaranna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaapa chettu kommaranna"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhoopamese dhummuranna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhoopamese dhummuranna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey aashada masamanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey aashada masamanna"/>
</div>
<div class="lyrico-lyrics-wrapper">Andhulo aadhivaaramanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhulo aadhivaaramanna"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottha kundala bonamanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottha kundala bonamanna"/>
</div>
<div class="lyrico-lyrics-wrapper">Netthi ketthenu patnamanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netthi ketthenu patnamanna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yo say yo say hey bonalu rey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yo say yo say hey bonalu rey"/>
</div>
<div class="lyrico-lyrics-wrapper">Challo challo gandi maisammaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Challo challo gandi maisammaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Yo say yo say hey bonalu rey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yo say yo say hey bonalu rey"/>
</div>
<div class="lyrico-lyrics-wrapper">Challo challo gandi maisammaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Challo challo gandi maisammaro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey raaye raaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey raaye raaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey raaye raaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey raaye raaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey raaye raaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey raaye raaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Raaye raaye raaye raaye maisamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raaye raaye raaye raaye maisamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Balkampet yellammave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Balkampet yellammave"/>
</div>
<div class="lyrico-lyrics-wrapper">Maa thalli bangaru maisammavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa thalli bangaru maisammavey"/>
</div>
<div class="lyrico-lyrics-wrapper">Ujjaini mankalivey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ujjaini mankalivey"/>
</div>
<div class="lyrico-lyrics-wrapper">Maa yamma oorura pochammave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa yamma oorura pochammave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yo say yo say hey bonalu rey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yo say yo say hey bonalu rey"/>
</div>
<div class="lyrico-lyrics-wrapper">Challo challo gandi maisammaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Challo challo gandi maisammaro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yo say yo say hey bonalu rey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yo say yo say hey bonalu rey"/>
</div>
<div class="lyrico-lyrics-wrapper">Challo challo gandi maisammaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Challo challo gandi maisammaro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arey revula puttindhamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey revula puttindhamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Renuka yellamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Renuka yellamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Jeripothula theesi jadala chuttindhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jeripothula theesi jadala chuttindhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nagu pamula theesi nadumuna kattindhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nagu pamula theesi nadumuna kattindhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeduguru akka chellellu yenta raanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeduguru akka chellellu yenta raanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Yededu lokalu eluthunnadhamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yededu lokalu eluthunnadhamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Mavurala yellamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mavurala yellamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhandal thalli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhandal thalli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">This is bark bark barkath puraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="This is bark bark barkath puraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dj ismart dis dis disco bonal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dj ismart dis dis disco bonal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Peyyi ninda gavvalni persukkunnavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peyyi ninda gavvalni persukkunnavey"/>
</div>
<div class="lyrico-lyrics-wrapper">Veyyi kandla thalli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veyyi kandla thalli"/>
</div>
<div class="lyrico-lyrics-wrapper">Niku yaata pothuley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niku yaata pothuley"/>
</div>
<div class="lyrico-lyrics-wrapper">Nimmakaya dhandallo nindugunnavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nimmakaya dhandallo nindugunnavey"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallu kunda thecchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallu kunda thecchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha sakaa posthamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha sakaa posthamey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arey chintha poola cheera kattinavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey chintha poola cheera kattinavey"/>
</div>
<div class="lyrico-lyrics-wrapper">Chetha shoolam katthi pattinavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chetha shoolam katthi pattinavey"/>
</div>
<div class="lyrico-lyrics-wrapper">Mottham duniyaane eluthunnavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mottham duniyaane eluthunnavey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raaye raaye thalli raaye raaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raaye raaye thalli raaye raaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey Raaye raaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey Raaye raaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Raaye raaye raaye raaye maisamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raaye raaye raaye raaye maisamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jubilee hills peddammavey mayamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jubilee hills peddammavey mayamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Jagamele maa thallivey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jagamele maa thallivey"/>
</div>
<div class="lyrico-lyrics-wrapper">Golkonda yellammavey mayamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Golkonda yellammavey mayamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Lashkar ke nuv raanivey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lashkar ke nuv raanivey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yo say yo say
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yo say yo say"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey potha rajuro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey potha rajuro"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey jajjanakara jajjanakara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey jajjanakara jajjanakara"/>
</div>
<div class="lyrico-lyrics-wrapper">Teenmaar ro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teenmaar ro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yo say yo say hey bonalu rey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yo say yo say hey bonalu rey"/>
</div>
<div class="lyrico-lyrics-wrapper">Challo challo gandi maisammaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Challo challo gandi maisammaro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yesko mama teenmaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yesko mama teenmaar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aggi gundalallo nuvvu baggumannavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aggi gundalallo nuvvu baggumannavey"/>
</div>
<div class="lyrico-lyrics-wrapper">Suttu muttu sukkallo muddhugunnavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suttu muttu sukkallo muddhugunnavey"/>
</div>
<div class="lyrico-lyrics-wrapper">Puttalona unnatti matti rupamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puttalona unnatti matti rupamey"/>
</div>
<div class="lyrico-lyrics-wrapper">Bayilona putti allinavu bandhamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bayilona putti allinavu bandhamey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey gaali dhooli antha nuvveley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey gaali dhooli antha nuvveley"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaali galla thalli nuvveley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaali galla thalli nuvveley"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee janamantha nee biddaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee janamantha nee biddaley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raaye raaye ehey raaye raaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raaye raaye ehey raaye raaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Amma Raaye raaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma Raaye raaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Raaye raaye raaye raaye maisamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raaye raaye raaye raaye maisamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bejawada dhurgammave maa thalli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bejawada dhurgammave maa thalli"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalakattha mahankaalivey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalakattha mahankaalivey"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanchilunna kakakshivey maayamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanchilunna kakakshivey maayamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhurolona meenakshivey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhurolona meenakshivey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yo say yo say
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yo say yo say"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey eeragola ro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey eeragola ro"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey tottellatho pottella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey tottellatho pottella"/>
</div>
<div class="lyrico-lyrics-wrapper">Bandi kadhilero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bandi kadhilero"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yo say yo say hey bonalu rey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yo say yo say hey bonalu rey"/>
</div>
<div class="lyrico-lyrics-wrapper">Challo challo gandi maisammaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Challo challo gandi maisammaro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">This is hamara kirraak
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="This is hamara kirraak"/>
</div>
<div class="lyrico-lyrics-wrapper">Bonal bonal bonal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bonal bonal bonal"/>
</div>
</pre>
