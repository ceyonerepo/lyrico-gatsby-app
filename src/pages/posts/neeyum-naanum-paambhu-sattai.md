---
title: "neeyum naanum song lyrics"
album: "Paambhu Sattai"
artist: "Ajeesh"
lyricist: "Madhan Karky"
director: "Adam Dasan"
path: "/albums/paambhu-sattai-lyrics"
song: "Neeyum Naanum"
image: ../../images/albumart/paambhu-sattai.jpg
date: 2017-03-24
lang: tamil
youtubeLink: "https://www.youtube.com/embed/9vLTQWAajg8"
type: "love"
singers:
  - Ajeesh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Chinnoondu koondukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinnoondu koondukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna pootta paakkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna pootta paakkuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponnae nee maata thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponnae nee maata thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Maattura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maattura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennoda kannukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennoda kannukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna mooda paakkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna mooda paakkuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappichi poi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappichi poi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanni kaattura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanni kaattura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Perundhu sannal vazhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perundhu sannal vazhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathaaga nee vandhiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathaaga nee vandhiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennoda moochukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennoda moochukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee vandhu serum munnae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee vandhu serum munnae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanamalae poriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanamalae poriyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeyum naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Onaa serndhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onaa serndhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">En manasellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En manasellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinusa dhinusaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinusa dhinusaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Maarumadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maarumadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeyum naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Onaa serndhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onaa serndhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">En vazhvellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En vazhvellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nedhamum pudhusaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nedhamum pudhusaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Maarumadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maarumadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Minnala minnala pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minnala minnala pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee sirichu maranji pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee sirichu maranji pora"/>
</div>
<div class="lyrico-lyrics-wrapper">Adiyae idiyae marukkaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiyae idiyae marukkaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Siriyen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siriyen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gaandhathu kaathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaandhathu kaathaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Izhukkuriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Izhukkuriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjayum thookki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjayum thookki"/>
</div>
<div class="lyrico-lyrics-wrapper">Poguriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poguriyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Venaa parandhida venaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venaa parandhida venaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru nodi nillen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru nodi nillen"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kannulla naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kannulla naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Neendhanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neendhanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Venaa alanjida venaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venaa alanjida venaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru badhil sollen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru badhil sollen"/>
</div>
<div class="lyrico-lyrics-wrapper">En nenjam nerayum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En nenjam nerayum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeyum naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Onaa serndhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onaa serndhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">En manasellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En manasellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinusa dhinusaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinusa dhinusaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Maarumadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maarumadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeyum naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Onaa serndhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onaa serndhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">En vazhvellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En vazhvellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nedhamum pudhusaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nedhamum pudhusaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Maarumadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maarumadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeyum naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Onaa serndhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onaa serndhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">En vazhvellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En vazhvellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nedhamum pudhusaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nedhamum pudhusaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Maarumadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maarumadi"/>
</div>
</pre>
