---
title: "naattukatta song lyrics"
album: "Gemini"
artist: "Bharathwaj"
lyricist: "Vairamuthu"
director: "Saran"
path: "/albums/gemini-lyrics"
song: "Naattukatta"
image: ../../images/albumart/gemini.jpg
date: 2002-04-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/0AcIHui360g"
type: "love"
singers:
  - Shankar Mahadevan
  - Swarnalatha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Naatu Katta Naatu Katta Maatikitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naatu Katta Naatu Katta Maatikitta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thillaalangadiyo Hey Thillaalangadiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thillaalangadiyo Hey Thillaalangadiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Thillaalangadiyo Hey Thillaalangadiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thillaalangadiyo Hey Thillaalangadiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Katta Katta Katta Katta Naatu Katta Naatu Katta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Katta Katta Katta Katta Naatu Katta Naatu Katta"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Kitta Kitta Kitta Vandhu Maatikitta Maatikitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kitta Kitta Kitta Vandhu Maatikitta Maatikitta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sandakaara Mayilu Ippo Kitta Vandhiruchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandakaara Mayilu Ippo Kitta Vandhiruchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Moodi Vacha Kuyilu Ippo Mutta Vandhiruchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moodi Vacha Kuyilu Ippo Mutta Vandhiruchi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thillaalangadiyo Hey Thillaalangadiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thillaalangadiyo Hey Thillaalangadiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Thillaalangadiyo Hey Thillaalangadiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thillaalangadiyo Hey Thillaalangadiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Katta Katta Katta Katta Naatu Katta Naatu Katta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Katta Katta Katta Katta Naatu Katta Naatu Katta"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Kitta Kitta Kitta Vandhu Maatikitta Maatikitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kitta Kitta Kitta Vandhu Maatikitta Maatikitta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sottu Sottu Mazhaiya Rasika Vitu Vitu Veyiladikanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sottu Sottu Mazhaiya Rasika Vitu Vitu Veyiladikanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanniponnu Kaadhal Rusiku Chinna Chinna Oodal Irukanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanniponnu Kaadhal Rusiku Chinna Chinna Oodal Irukanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anjo Patho Mutham Kodukanum Ange Inge Meesa Uruthanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anjo Patho Mutham Kodukanum Ange Inge Meesa Uruthanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pathu Viral Payanam Pannanum Pallu Padama Kaadha Kadikanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathu Viral Payanam Pannanum Pallu Padama Kaadha Kadikanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kanindha Maarbin Idukula En Kavalaigala Pudhaikanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kanindha Maarbin Idukula En Kavalaigala Pudhaikanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Moochi Vidum Nerupula En Mogangalai Yerikanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Moochi Vidum Nerupula En Mogangalai Yerikanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thillaalangadiyo Hey Thillaalangadiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thillaalangadiyo Hey Thillaalangadiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Thillaalangadiyo Hey Thillaalangadiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thillaalangadiyo Hey Thillaalangadiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Katta Katta Katta Katta Naatu Katta Naatu Katta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Katta Katta Katta Katta Naatu Katta Naatu Katta"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Kitta Kitta Kitta Kitta Kitta Vandhu Maatikitta Maatikitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kitta Kitta Kitta Kitta Kitta Vandhu Maatikitta Maatikitta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Cherry Pazham Neramiruku Chikunu Dhaan Udambiruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cherry Pazham Neramiruku Chikunu Dhaan Udambiruku"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeraalamaai Azhagiruku Dhaaraalamaai Manasiruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeraalamaai Azhagiruku Dhaaraalamaai Manasiruku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjaampazham Kaninjiruku Neram Nalla Amanjiruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjaampazham Kaninjiruku Neram Nalla Amanjiruku"/>
</div>
<div class="lyrico-lyrics-wrapper">Thapu Thanda Pana Chonna Thandaal Senja Udambiruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thapu Thanda Pana Chonna Thandaal Senja Udambiruku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Pazhutha Maarbu Paarkaiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Pazhutha Maarbu Paarkaiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">En Ozhukam Yenbadhu Parakuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Ozhukam Yenbadhu Parakuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam Pozhudhu Saayum Velaiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Pozhudhu Saayum Velaiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Pombalai Yenbadhai Marakudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Pombalai Yenbadhai Marakudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thillaalangadiyo Hey Thillaalangadiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thillaalangadiyo Hey Thillaalangadiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Thillaalangadiyo Hey Thillaalangadiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thillaalangadiyo Hey Thillaalangadiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Katta Katta Katta Katta Naatu Katta Naatu Katta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Katta Katta Katta Katta Naatu Katta Naatu Katta"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Kitta Kitta Kitta Vandhu Maatikitta Maatikitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kitta Kitta Kitta Vandhu Maatikitta Maatikitta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sandakaara Mayilu Ippo Kitta Vandhiruchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandakaara Mayilu Ippo Kitta Vandhiruchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Moodi Vacha Kuyilu Ippo Mutta Vandhiruchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moodi Vacha Kuyilu Ippo Mutta Vandhiruchi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Thillaalangadiyo Hey Thillaalangadiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Thillaalangadiyo Hey Thillaalangadiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Thillaalangadiyo Hey Thillaalangadiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thillaalangadiyo Hey Thillaalangadiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Thillaalangadiyo Hey Thillaalangadiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thillaalangadiyo Hey Thillaalangadiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Thillaalangadiyo Hey Thillaalangadiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thillaalangadiyo Hey Thillaalangadiyo"/>
</div>
</pre>
