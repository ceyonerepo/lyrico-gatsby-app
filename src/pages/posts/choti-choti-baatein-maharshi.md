---
title: "choti choti baatein song lyrics"
album: "Maharshi"
artist: "Devi Sri Prasad"
lyricist: "Sri Mani"
director: "Vamshi Paidipally"
path: "/albums/maharshi-lyrics"
song: "Choti Choti Baatein"
image: ../../images/albumart/maharshi.jpg
date: 2019-05-09
lang: telugu
youtubeLink: "https://www.youtube.com/embed/afsWVNIIKQU"
type: "love"
singers:
  - Devi Sri Prasad
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Choti choti choti choti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choti choti choti choti"/>
</div>
<div class="lyrico-lyrics-wrapper">Choti choti baatein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choti choti baatein"/>
</div>
<div class="lyrico-lyrics-wrapper">Meeti meeti meeti meeti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meeti meeti meeti meeti"/>
</div>
<div class="lyrico-lyrics-wrapper">Meeti meeti yaadein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meeti meeti yaadein"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Choti choti choti choti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choti choti choti choti"/>
</div>
<div class="lyrico-lyrics-wrapper">Choti choti baatein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choti choti baatein"/>
</div>
<div class="lyrico-lyrics-wrapper">Meeti meeti meeti meeti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meeti meeti meeti meeti"/>
</div>
<div class="lyrico-lyrics-wrapper">Meeti meeti yaadein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meeti meeti yaadein"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">O parichayam eppudu chinnadey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O parichayam eppudu chinnadey"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee chelimikey kaalamey chaaladey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee chelimikey kaalamey chaaladey"/>
</div>
<div class="lyrico-lyrics-wrapper">Enno vela kathalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enno vela kathalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey inko katha modhalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey inko katha modhalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enno vela kathalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enno vela kathalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey inko katha modhalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey inko katha modhalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Choti choti choti choti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choti choti choti choti"/>
</div>
<div class="lyrico-lyrics-wrapper">Choti choti baatein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choti choti baatein"/>
</div>
<div class="lyrico-lyrics-wrapper">Meeti meeti meeti meeti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meeti meeti meeti meeti"/>
</div>
<div class="lyrico-lyrics-wrapper">Meeti meeti yaadein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meeti meeti yaadein"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aata laaga paata laaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aata laaga paata laaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Nerchukunte raanidantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nerchukunte raanidantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Snehamante emitante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Snehamante emitante"/>
</div>
<div class="lyrico-lyrics-wrapper">Pusthakalu cheppaleni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pusthakalu cheppaleni"/>
</div>
<div class="lyrico-lyrics-wrapper">Paatam antaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paatam antaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Korukunte cheradhanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Korukunte cheradhanta"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaddu antey velladhanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaddu antey velladhanta"/>
</div>
<div class="lyrico-lyrics-wrapper">Nesthamantey emitantey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nesthamantey emitantey"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannavaallu ivvaleni aasthenanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannavaallu ivvaleni aasthenanta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Isthu neekai praanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Isthu neekai praanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Panchisthu thana abhimanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panchisthu thana abhimanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Neelo prathi ontari tharunam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelo prathi ontari tharunam"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheripesthuuuuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheripesthuuuuu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enno vela kathalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enno vela kathalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey inko katha modhalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey inko katha modhalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Choti choti choti choti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choti choti choti choti"/>
</div>
<div class="lyrico-lyrics-wrapper">Choti choti baatein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choti choti baatein"/>
</div>
<div class="lyrico-lyrics-wrapper">Meeti meeti meeti meeti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meeti meeti meeti meeti"/>
</div>
<div class="lyrico-lyrics-wrapper">Meeti meeti yaadein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meeti meeti yaadein"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gurthulevi leni naadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gurthulevi leni naadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Brathikinattu gurthuraadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Brathikinattu gurthuraadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiyyanaina gnaapakaallaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiyyanaina gnaapakaallaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gundelona acchayevi savasale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundelona acchayevi savasale"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhaadhalevi leni naadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhaadhalevi leni naadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Navvukaina viluvundadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Navvukaina viluvundadhey"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallalona kanneellunnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallalona kanneellunnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pedavullo navvu cheragadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pedavullo navvu cheragadhey"/>
</div>
<div class="lyrico-lyrics-wrapper">Sneham navvu cheragadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sneham navvu cheragadhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee kashtam thanadanukuntu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kashtam thanadanukuntu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kalane thanadhiga kantu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kalane thanadhiga kantu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee gelupuni maathram neeke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee gelupuni maathram neeke"/>
</div>
<div class="lyrico-lyrics-wrapper">Vadilesthuuuuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadilesthuuuuu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enno vela kathalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enno vela kathalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey inko katha modhalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey inko katha modhalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Choti choti choti choti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choti choti choti choti"/>
</div>
<div class="lyrico-lyrics-wrapper">Choti choti baatein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choti choti baatein"/>
</div>
<div class="lyrico-lyrics-wrapper">Meeti meeti meeti meeti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meeti meeti meeti meeti"/>
</div>
<div class="lyrico-lyrics-wrapper">Meeti meeti yaadein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meeti meeti yaadein"/>
</div>
</pre>
