---
title: "konjam sirikkirean song lyrics"
album: "Amutha"
artist: "Arun Gopan"
lyricist: "G. Ra"
director: "P.S. Arjun"
path: "/albums/amutha-lyrics"
song: "Konjam Sirikkirean"
image: ../../images/albumart/amutha.jpg
date: 2018-05-16
lang: tamil
youtubeLink: "https://www.youtube.com/embed/F_mRgDz1Mko"
type: "love"
singers:
  - Vineeth Sreenivasan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Konjam Sirikkirean
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Sirikkirean"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam Thavikkirean
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Thavikkirean"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhai Koottile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai Koottile"/>
</div>
<div class="lyrico-lyrics-wrapper">Veyil Theduthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veyil Theduthey"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Eatho Aanean Aa Aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Eatho Aanean Aa Aa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konjam Sirikkirean
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Sirikkirean"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam Thavikkirean
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Thavikkirean"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhai Koottile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai Koottile"/>
</div>
<div class="lyrico-lyrics-wrapper">Veyil Theduthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veyil Theduthey"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Eatho Aanean Aa Aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Eatho Aanean Aa Aa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paarvaiyila Enna Kudikkiriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarvaiyila Enna Kudikkiriye"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarkkayila Nenju Thudikkiriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarkkayila Nenju Thudikkiriye"/>
</div>
<div class="lyrico-lyrics-wrapper">Pothumithu Pothum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pothumithu Pothum "/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhalae Kolluthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhalae Kolluthey"/>
</div>
<div class="lyrico-lyrics-wrapper">Thenamum Puthusa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thenamum Puthusa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Paakkuraen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Paakkuraen"/>
</div>
<div class="lyrico-lyrics-wrapper">Irudhayam Thudikkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irudhayam Thudikkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Atha Kaetkkiraen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atha Kaetkkiraen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konjam Sirikkirean
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Sirikkirean"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam Thavikkirean
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Thavikkirean"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhai Koottile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai Koottile"/>
</div>
<div class="lyrico-lyrics-wrapper">Veyil Theduthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veyil Theduthey"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Eatho Aanean Aa Aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Eatho Aanean Aa Aa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thunjum Manjam Anjum Anjum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunjum Manjam Anjum Anjum"/>
</div>
<div class="lyrico-lyrics-wrapper">Minjum Nenjum Kenjum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minjum Nenjum Kenjum "/>
</div>
<div class="lyrico-lyrics-wrapper">Kenjum Thaniya Thaniya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kenjum Thaniya Thaniya"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaniya Thaniya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaniya Thaniya"/>
</div>
<div class="lyrico-lyrics-wrapper">Panjum Theeyum Minjum Minjum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panjum Theeyum Minjum Minjum"/>
</div>
<div class="lyrico-lyrics-wrapper">Michcham Indri Konjum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Michcham Indri Konjum "/>
</div>
<div class="lyrico-lyrics-wrapper">Konjum Vidhiya Sadhiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjum Vidhiya Sadhiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidhiya Sadhiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidhiya Sadhiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhikal Enge Thunjum Thunjum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhikal Enge Thunjum Thunjum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavithai Inge Minjum Minjum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavithai Inge Minjum Minjum"/>
</div>
<div class="lyrico-lyrics-wrapper">Udalil Unarvil Uyiril 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udalil Unarvil Uyiril "/>
</div>
<div class="lyrico-lyrics-wrapper">Salanam Varavum Tharavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Salanam Varavum Tharavum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pazhagi Kondaal Sariya Sariya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pazhagi Kondaal Sariya Sariya"/>
</div>
<div class="lyrico-lyrics-wrapper">Maraiththu Kondaal Muraiya Muraiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maraiththu Kondaal Muraiya Muraiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Sariya Sariya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sariya Sariya"/>
</div>
<div class="lyrico-lyrics-wrapper">Maraiththu Kondaal Muraiya Muraiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maraiththu Kondaal Muraiya Muraiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannaadi Chaalai Mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaadi Chaalai Mela"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanaaka Nadanthaeney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanaaka Nadanthaeney"/>
</div>
<div class="lyrico-lyrics-wrapper">Veesura Kaaththu Mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veesura Kaaththu Mela"/>
</div>
<div class="lyrico-lyrics-wrapper">Poovaaka Paranthaeney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovaaka Paranthaeney"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattaasu Chaththam Pola 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattaasu Chaththam Pola "/>
</div>
<div class="lyrico-lyrics-wrapper">Chattunnu Vanthaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chattunnu Vanthaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Veesura Kaaththum Kooda 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veesura Kaaththum Kooda "/>
</div>
<div class="lyrico-lyrics-wrapper">Nikkaatho Unnaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikkaatho Unnaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Sevvaanam Saayam Poka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sevvaanam Saayam Poka "/>
</div>
<div class="lyrico-lyrics-wrapper">Minnala Sirikkathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minnala Sirikkathey"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini Vazhi Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Vazhi Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappikka Mudiyaathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappikka Mudiyaathey"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilagi Vittu Pokaatha Pokaathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilagi Vittu Pokaatha Pokaathey"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaangaamalae Theeyaakumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaangaamalae Theeyaakumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Inimael Maname Saruke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inimael Maname Saruke"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudivu Varai Ennum Unaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudivu Varai Ennum Unaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konjam Sirikkirean
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Sirikkirean"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam Thavikkirean
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Thavikkirean"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhai Koottile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai Koottile"/>
</div>
<div class="lyrico-lyrics-wrapper">Veyil Theduthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veyil Theduthey"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Eatho Aanean
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Eatho Aanean"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarvaiyila Enna Kudikkiriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarvaiyila Enna Kudikkiriye"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarkkayila Nenju Thudikkiriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarkkayila Nenju Thudikkiriye"/>
</div>
<div class="lyrico-lyrics-wrapper">Pothumithu Pothum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pothumithu Pothum "/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhalae Kolluthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhalae Kolluthey"/>
</div>
<div class="lyrico-lyrics-wrapper">Thenamum Puthusa Unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thenamum Puthusa Unna"/>
</div>
<div class="lyrico-lyrics-wrapper">Paakkuraen Paakkuraen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paakkuraen Paakkuraen"/>
</div>
<div class="lyrico-lyrics-wrapper">Irudhayam Thudikkira Thaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irudhayam Thudikkira Thaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarkkayil Kooduthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarkkayil Kooduthey"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaal Mullum Pookkuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaal Mullum Pookkuthey"/>
</div>
</pre>
