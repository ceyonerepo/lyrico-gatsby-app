---
title: "azhagiye song lyrics"
album: "Kaatru Veliyidai"
artist: "A R Rahman"
lyricist: "Madhan Karky - Navneet Virk"
director: "Mani Ratnam"
path: "/albums/kaatru-veliyidai-lyrics"
song: "Azhagiye"
image: ../../images/albumart/kaatru-veliyidai.jpg
date: 2017-04-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/CFj1HXUGhaY"
type: "love"
singers:
  -	Arjun Chandy
  - Haricharan
  - Jonita Gandhi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Waiting for the punnagai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Waiting for the punnagai"/>
</div>
<div class="lyrico-lyrics-wrapper">Siri di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siri di"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanavillai heart beat
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanavillai heart beat"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirudi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adada naan kavignan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adada naan kavignan"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai paathu kettu pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai paathu kettu pona"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavignan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavignan"/>
</div>
<div class="lyrico-lyrics-wrapper">Honest-ah naan pesavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Honest-ah naan pesavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Illa idhu pothuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illa idhu pothuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh my darling
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh my darling"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga coming
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga coming"/>
</div>
<div class="lyrico-lyrics-wrapper">Puthu puthu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthu puthu "/>
</div>
<div class="lyrico-lyrics-wrapper">kanakellam pending
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanakellam pending"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooh"/>
</div>
-<div class="lyrico-lyrics-wrapper">ah naan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ah naan "/>
</div>
<div class="lyrico-lyrics-wrapper">ketkavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ketkavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yes ah yes-ah 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yes ah yes-ah "/>
</div>
<div class="lyrico-lyrics-wrapper">no-ah yes-ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="no-ah yes-ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhagiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Marry me marry me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marry me marry me"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Flirt with me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Flirt with me"/>
</div>
<div class="lyrico-lyrics-wrapper">Get high with me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Get high with me"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kovam vandhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kovam vandhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Koocham vandhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koocham vandhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Don’t worry
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Don’t worry"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey azhagiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey azhagiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey azhagiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey azhagiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Marry me marry me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marry me marry me"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Flirt with me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Flirt with me"/>
</div>
<div class="lyrico-lyrics-wrapper">Get high with me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Get high with me"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal vanthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal vanthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Matter vanthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Matter vanthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Call-adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Call-adi"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey azhagiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey azhagiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Aarum ketka edhu ondrai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aarum ketka edhu ondrai"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhuondrai naan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhuondrai naan "/>
</div>
<div class="lyrico-lyrics-wrapper">ketten unnai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ketten unnai"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhai thanthal nandri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai thanthal nandri"/>
</div>
<div class="lyrico-lyrics-wrapper">Pidivaatham indri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pidivaatham indri"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee thanthal nandri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thanthal nandri"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuli thulirae eehh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuli thulirae eehh"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuli kaalam ketten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuli kaalam ketten"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuli kaadhal ketten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuli kaadhal ketten"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuli kaamam ketten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuli kaamam ketten"/>
</div>
<div class="lyrico-lyrics-wrapper">Maru uyirae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maru uyirae"/>
</div>
<div class="lyrico-lyrics-wrapper">Marukkadhae nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marukkadhae nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Marakadhae nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marakadhae nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Enthan azhagiyae nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enthan azhagiyae nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ni ni ni ni ni ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ni ni ni ni ni ni"/>
</div>
<div class="lyrico-lyrics-wrapper">nin ni nin ni nin ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nin ni nin ni nin ni"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Marry me marry me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marry me marry me"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Flirt with me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Flirt with me"/>
</div>
<div class="lyrico-lyrics-wrapper">Get high with me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Get high with me"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kovam vandhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kovam vandhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Koocham vandhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koocham vandhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Don’t worry
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Don’t worry"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey azhagiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey azhagiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey azhagiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey azhagiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Marry me marry me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marry me marry me"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Flirt with me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Flirt with me"/>
</div>
<div class="lyrico-lyrics-wrapper">Get high with me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Get high with me"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal vanthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal vanthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Matter vanthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Matter vanthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Call adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Call adi"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey azhagiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey azhagiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey"/>
</div>
</pre>
