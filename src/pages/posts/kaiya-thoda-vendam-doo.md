---
title: "kaiya thoda vendam song lyrics"
album: "Doo"
artist: "Abhishek — Lawrence"
lyricist: "Yugabharathi"
director: "Sriram Padmanabhan"
path: "/albums/doo-lyrics"
song: "Kaiya Thoda Vendam"
image: ../../images/albumart/doo.jpg
date: 2011-08-05
lang: tamil
youtubeLink: "https://www.youtube.com/embed/v8bHnRu1lAY"
type: "love"
singers:
  - Ranjith
  - VJ Divya
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kaiya thodavenaam kaalil vizhavenaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiya thodavenaam kaalil vizhavenaam"/>
</div>
<div class="lyrico-lyrics-wrapper">kannaala pesu poadhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannaala pesu poadhum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maalai thara venaam koodavara venaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maalai thara venaam koodavara venaam"/>
</div>
<div class="lyrico-lyrics-wrapper">thaalli nee poanaa poadhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaalli nee poanaa poadhum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vera edhu venum sollu pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vera edhu venum sollu pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">vedhanai unnaala enna solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vedhanai unnaala enna solla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poadhum vazhiyaadha onnum venaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poadhum vazhiyaadha onnum venaam"/>
</div>
<div class="lyrico-lyrics-wrapper">nee vandhu enna thollapanna venaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee vandhu enna thollapanna venaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adiye unna naanaa viduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiye unna naanaa viduven"/>
</div>
<div class="lyrico-lyrics-wrapper">viral thaanaa thoduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viral thaanaa thoduven"/>
</div>
<div class="lyrico-lyrics-wrapper">nee venunnu sonnaalum vidammaatten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee venunnu sonnaalum vidammaatten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unna theeyaa suduven viral saaraa puzhiven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna theeyaa suduven viral saaraa puzhiven"/>
</div>
<div class="lyrico-lyrics-wrapper">nee thaayennu kettaalum tharammaatten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee thaayennu kettaalum tharammaatten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaiya thodavenaam kaalil vizhavenaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiya thodavenaam kaalil vizhavenaam"/>
</div>
<div class="lyrico-lyrics-wrapper">kannaala pesu poadhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannaala pesu poadhum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poonai kudikkaadha paalu edhukkaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poonai kudikkaadha paalu edhukkaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">sollu enakku sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sollu enakku sollu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paala kudikkaama vaaya thorakkaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paala kudikkaama vaaya thorakkaama"/>
</div>
<div class="lyrico-lyrics-wrapper">nillu odhungi nillu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nillu odhungi nillu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eerangaayaadha oorkoalam poala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eerangaayaadha oorkoalam poala"/>
</div>
<div class="lyrico-lyrics-wrapper">eppoadhum vaasappulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eppoadhum vaasappulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eesan kooraadha sedhigalai neeyum sollida thevaiyilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eesan kooraadha sedhigalai neeyum sollida thevaiyilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi nandhavanam poovey nee netti murikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi nandhavanam poovey nee netti murikka"/>
</div>
<div class="lyrico-lyrics-wrapper">vennilavum vandhidumey unnai rasikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vennilavum vandhidumey unnai rasikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hei kuththiruven kaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei kuththiruven kaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">nee kutham irukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee kutham irukka"/>
</div>
<div class="lyrico-lyrics-wrapper">kolliyida ennidudhey konjam sirikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kolliyida ennidudhey konjam sirikka"/>
</div>
<div class="lyrico-lyrics-wrapper">mesa sevappaaga aasa neruppaaga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mesa sevappaaga aasa neruppaaga "/>
</div>
<div class="lyrico-lyrics-wrapper">vandhu edhukku thotta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vandhu edhukku thotta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaiya thodavenaam kaalil vizhavenaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiya thodavenaam kaalil vizhavenaam"/>
</div>
<div class="lyrico-lyrics-wrapper">kannaala pesu poadhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannaala pesu poadhum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Velai salippaaga paayum veruppaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velai salippaaga paayum veruppaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">enna kavuthupputta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna kavuthupputta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maadham pooraavum maargazhiya poala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maadham pooraavum maargazhiya poala"/>
</div>
<div class="lyrico-lyrics-wrapper">veiyila maaravacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veiyila maaravacha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koodi poadaadha sooriyana poala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koodi poadaadha sooriyana poala"/>
</div>
<div class="lyrico-lyrics-wrapper">kannula soodu vacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannula soodu vacha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye vandhathadi aasa naan unna anaikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye vandhathadi aasa naan unna anaikka"/>
</div>
<div class="lyrico-lyrics-wrapper">ippozhudhum eppozhudhum meththa virikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ippozhudhum eppozhudhum meththa virikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hei hei solluradha kelu naan oththu uzhaikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei hei solluradha kelu naan oththu uzhaikka"/>
</div>
<div class="lyrico-lyrics-wrapper">munnazhagum pinnazhagum moththam kodukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munnazhagum pinnazhagum moththam kodukka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaiya thodavenaam kaalil vizhavenaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiya thodavenaam kaalil vizhavenaam"/>
</div>
<div class="lyrico-lyrics-wrapper">kannaala pesu poadhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannaala pesu poadhum"/>
</div>
</pre>
