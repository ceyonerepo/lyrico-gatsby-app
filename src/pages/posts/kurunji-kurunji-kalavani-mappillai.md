---
title: "kurunji kurunji song lyrics"
album: "Kalavani Mappillai"
artist: "NR Raghunanthan"
lyricist: "Mani Amuthan"
director: "Gandhi Manivasagam"
path: "/albums/kalavani-mappillai-lyrics"
song: "Kurunji Kurunji"
image: ../../images/albumart/kalavani-mappillai.jpg
date: 2018-11-06
lang: tamil
youtubeLink: "https://www.youtube.com/embed/3jCU6P401Y4"
type: "happy"
singers:
  - Sathya Prakash
  - Velu
  - Namitha Babu
  - Sanjana
  - Suganya 
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kurunji kurunji poovukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurunji kurunji poovukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana naalu vanthaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana naalu vanthaachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana malai thanthaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana malai thanthaachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arunji purinji nadakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arunji purinji nadakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Un aalu vanthaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un aalu vanthaachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un pechu ennaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un pechu ennaachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Panthala nattu thoranam kattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panthala nattu thoranam kattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dam dam dam dam dum dum dum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dam dam dam dam dum dum dum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mathalam thattu melamum kottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mathalam thattu melamum kottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dam dam dam dam dum dum dum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dam dam dam dam dum dum dum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kurunji kurunji poovukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurunji kurunji poovukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana naalu vanthaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana naalu vanthaachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana malai thanthaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana malai thanthaachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arunji purinji nadakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arunji purinji nadakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Un aalu vanthaachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un aalu vanthaachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Un pechu ennaachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un pechu ennaachi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Marudhani kannam konda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marudhani kannam konda"/>
</div>
<div class="lyrico-lyrics-wrapper">Manjal kezhangae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manjal kezhangae"/>
</div>
<div class="lyrico-lyrics-wrapper">Magarani pola vazha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Magarani pola vazha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaari valanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaari valanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam pola vazhkai kedaikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam pola vazhkai kedaikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakku maharasan vanthu puttanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakku maharasan vanthu puttanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sandhanam kungumam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhanam kungumam"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana manakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana manakka"/>
</div>
<div class="lyrico-lyrics-wrapper">En sondhamum bandhamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En sondhamum bandhamum"/>
</div>
<div class="lyrico-lyrics-wrapper">Malaichi nikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malaichi nikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandhanam kungumam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhanam kungumam"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana manakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana manakka"/>
</div>
<div class="lyrico-lyrics-wrapper">En sondhamum bandhamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En sondhamum bandhamum"/>
</div>
<div class="lyrico-lyrics-wrapper">Malaichi nikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malaichi nikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ketti melam ketti melam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketti melam ketti melam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kotta pogudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kotta pogudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu metti kaala thottu vazha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu metti kaala thottu vazha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vattam poduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vattam poduthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ketti melam ketti melam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketti melam ketti melam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kotta pogudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kotta pogudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu metti kaala thottu vazha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu metti kaala thottu vazha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vattam poduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vattam poduthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kurunji kurunji poovukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurunji kurunji poovukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana naalu vanthaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana naalu vanthaachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana malai thanthaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana malai thanthaachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arunji purinji nadakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arunji purinji nadakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Un aalu vanthaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un aalu vanthaachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un pechu ennaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un pechu ennaachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaaahaaaaaahaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaahaaaaaahaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kupidira thooram thaan poganum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kupidira thooram thaan poganum"/>
</div>
<div class="lyrico-lyrics-wrapper">En kolamthula pulliyaga maaranum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kolamthula pulliyaga maaranum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poruthum thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poruthum thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Paththu thaan porunthanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paththu thaan porunthanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee varuthamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee varuthamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Illaenu varunthanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illaenu varunthanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sollama solla vantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollama solla vantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaarthaigal arivenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaarthaigal arivenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Illatha kettalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illatha kettalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Uruvakki tharuvenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uruvakki tharuvenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakketha maari naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakketha maari naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Maari kittae varuvenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maari kittae varuvenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai ketta aprom thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai ketta aprom thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini moochum viduvenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini moochum viduvenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannamthil eeram muththam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannamthil eeram muththam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathoram moochi saththam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathoram moochi saththam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kettu kittae irukkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kettu kittae irukkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kekka kekka kodakkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kekka kekka kodakkanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kurunji kurunji poovukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurunji kurunji poovukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana naalu vanthaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana naalu vanthaachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana malai thanthaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana malai thanthaachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arunji purinji nadakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arunji purinji nadakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Un aalu vanthaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un aalu vanthaachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un pechu ennaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un pechu ennaachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeley cho cho chu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeley cho cho chu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeley cho cho chu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeley cho cho chu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeley cho cho chu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeley cho cho chu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeley cho cho chu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeley cho cho chu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbaala un
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbaala un"/>
</div>
<div class="lyrico-lyrics-wrapper">Thimira adakkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thimira adakkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">En anaippula nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En anaippula nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayangi kedakkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayangi kedakkanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Porvaiyila seththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porvaiyila seththu"/>
</div>
<div class="lyrico-lyrics-wrapper">Seththu polaikkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seththu polaikkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un vervaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un vervaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Vethaiya naan mulaikkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vethaiya naan mulaikkanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thangathil aninchalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangathil aninchalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhikatha methuvathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhikatha methuvathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thongattan ah naan rendu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thongattan ah naan rendu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhai thuliya tharava naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai thuliya tharava naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thangamae ganammannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangamae ganammannu"/>
</div>
<div class="lyrico-lyrics-wrapper">Polambathae paiya thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Polambathae paiya thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Manjathil un udambu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manjathil un udambu"/>
</div>
<div class="lyrico-lyrics-wrapper">Irukathu ganamma thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irukathu ganamma thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kattil nadu nadunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattil nadu nadunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathum kanam kanamga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathum kanam kanamga"/>
</div>
<div class="lyrico-lyrics-wrapper">Thottu kitta irukkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottu kitta irukkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Chella thollai kodukkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chella thollai kodukkanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kurunji kurunji poovukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurunji kurunji poovukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana naalu vanthaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana naalu vanthaachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana malai thanthaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana malai thanthaachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arunji purinji nadakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arunji purinji nadakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Un aalu vanthaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un aalu vanthaachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un pechu ennaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un pechu ennaachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Marudhani kannam konda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marudhani kannam konda"/>
</div>
<div class="lyrico-lyrics-wrapper">Manjal kezhangae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manjal kezhangae"/>
</div>
<div class="lyrico-lyrics-wrapper">Magarani pola vazha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Magarani pola vazha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaari valanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaari valanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam pola vazhkai kedaikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam pola vazhkai kedaikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakku maharasan vanthu puttanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakku maharasan vanthu puttanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sandhanam kungumam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhanam kungumam"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana manakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana manakka"/>
</div>
<div class="lyrico-lyrics-wrapper">En sondhamum bandhamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En sondhamum bandhamum"/>
</div>
<div class="lyrico-lyrics-wrapper">Malaichi nikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malaichi nikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandhanam kungumam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhanam kungumam"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana manakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana manakka"/>
</div>
<div class="lyrico-lyrics-wrapper">En sondhamum bandhamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En sondhamum bandhamum"/>
</div>
<div class="lyrico-lyrics-wrapper">Malaichi nikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malaichi nikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ketti melam ketti melam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketti melam ketti melam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kotta pogudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kotta pogudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu metti kaala thottu vazha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu metti kaala thottu vazha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vattam poduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vattam poduthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ketti melam ketti melam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketti melam ketti melam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kotta pogudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kotta pogudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu metti kaala thottu vazha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu metti kaala thottu vazha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vattam poduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vattam poduthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ketti melam ketti melam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketti melam ketti melam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kotta pogudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kotta pogudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu metti kaala thottu vazha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu metti kaala thottu vazha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vattam poduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vattam poduthu"/>
</div>
</pre>
