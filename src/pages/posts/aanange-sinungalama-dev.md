---
title: "anangae sinungalama song lyrics"
album: "Dev"
artist: "Harris Jayaraj"
lyricist: "Thamarai"
director: "Rajath Ravishankar"
path: "/albums/dev-lyrics"
song: "Anangae Sinungalama"
image: ../../images/albumart/dev.jpg
date: 2019-02-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/AmPa9DDlqo4"
type: "love"
singers:
  - Hariharan
  - Christopher Stanley
  - Tippu
  - Krish
  - Arjun Chandy
  - Bharath Sundar
  - Sharanya Gopinath
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aanange Sinungalama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanange Sinungalama"/>
</div>
<div class="lyrico-lyrics-wrapper">Nerungi Anaikka Naanirukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nerungi Anaikka Naanirukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Thaan Tharunam Thaniye Varanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Thaan Tharunam Thaniye Varanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Therinthey Nadikkalama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Therinthey Nadikkalama"/>
</div>
<div class="lyrico-lyrics-wrapper">Idazhai Thuninthu Yaar Kodukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idazhai Thuninthu Yaar Kodukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Muthalil Tharanum Piragey Peranum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthalil Tharanum Piragey Peranum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karai Thandiye Vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karai Thandiye Vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Puyal Pola Modhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puyal Pola Modhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhi Enbathan Peril
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhi Enbathan Peril"/>
</div>
<div class="lyrico-lyrics-wrapper">Kayal Konda Maadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kayal Konda Maadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Imai Saamaram Veesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imai Saamaram Veesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Allum Pothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Allum Pothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sumai Neengiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sumai Neengiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum Sainthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum Sainthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Sugam Veru Yethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sugam Veru Yethu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sunantha Paranthu Vaa Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sunantha Paranthu Vaa Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagai Maranthu Poi Vidalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagai Maranthu Poi Vidalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ahha Ohhoo Yehey Aaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahha Ohhoo Yehey Aaha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sunantha Virainthu Vaa Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sunantha Virainthu Vaa Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Urugi Karainthu Sainthidalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urugi Karainthu Sainthidalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ahha Ohhoo Yehey Aaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahha Ohhoo Yehey Aaha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pagalellam Paithiyamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagalellam Paithiyamai"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai Enni Yeangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai Enni Yeangi"/>
</div>
<div class="lyrico-lyrics-wrapper">Rathirikku Kathiruntha Rathi Naaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rathirikku Kathiruntha Rathi Naaney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vennilaavai Alli Veesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vennilaavai Alli Veesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Velichangal Aakki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velichangal Aakki"/>
</div>
<div class="lyrico-lyrics-wrapper">Siripathu Iyarkayin Sadhi Thaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siripathu Iyarkayin Sadhi Thaaney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arai Engum Unthan Udaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arai Engum Unthan Udaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Suvarengum Un Padangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suvarengum Un Padangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadanthalum Unthan Thadangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadanthalum Unthan Thadangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Polladha Ninaivugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Polladha Ninaivugal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aanange Sinungalama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanange Sinungalama"/>
</div>
<div class="lyrico-lyrics-wrapper">Nerungi Anaikka Naanirukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nerungi Anaikka Naanirukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Thaan Tharunam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Thaan Tharunam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaniye Varanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaniye Varanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Therinthey Nadikkalama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Therinthey Nadikkalama"/>
</div>
<div class="lyrico-lyrics-wrapper">Idazhai Thuninthu Yaar Kodukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idazhai Thuninthu Yaar Kodukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Muthalil Tharanum Piragey Peranum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthalil Tharanum Piragey Peranum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Naan Etharkku Paarthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Naan Etharkku Paarthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhungum Vizhiyai Saadugindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhungum Vizhiyai Saadugindren"/>
</div>
<div class="lyrico-lyrics-wrapper">Adaada Azhaga Vizhigal Kazhuga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adaada Azhaga Vizhigal Kazhuga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nodiyum Piriya Maaten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nodiyum Piriya Maaten"/>
</div>
<div class="lyrico-lyrics-wrapper">Pirinthal Udirndu Poividuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirinthal Udirndu Poividuven"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayam Enathu Uthiram Unathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam Enathu Uthiram Unathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karai Thandiye Vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karai Thandiye Vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Puyal Pola Modhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puyal Pola Modhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhi Enbathan Paeril
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhi Enbathan Paeril"/>
</div>
<div class="lyrico-lyrics-wrapper">Kayal Konda Maadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kayal Konda Maadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Imai Saamaram Veesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imai Saamaram Veesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Allum Pothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Allum Pothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sumai Neengiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sumai Neengiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum Sainthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum Sainthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Sugam Vearu Yethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sugam Vearu Yethu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sunantha Paranthu Vaa Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sunantha Paranthu Vaa Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagai Maranthu Poi Vidalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagai Maranthu Poi Vidalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ahha Ohhoo Yehey Aaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahha Ohhoo Yehey Aaha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sunantha Virainthu Vaa Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sunantha Virainthu Vaa Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Urugi Karainthu Sainthidalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urugi Karainthu Sainthidalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ahha Ohhoo Yehey Aaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahha Ohhoo Yehey Aaha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sunantha Paranthu Vaa Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sunantha Paranthu Vaa Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagai Maranthu Poi Vidalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagai Maranthu Poi Vidalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ahha Ohhoo Yehey Aaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahha Ohhoo Yehey Aaha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sunantha Virainthu Vaa Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sunantha Virainthu Vaa Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Urugi Karainthu Sainthidalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urugi Karainthu Sainthidalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ahha Ohhoo Yehey Aaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahha Ohhoo Yehey Aaha"/>
</div>
</pre>
