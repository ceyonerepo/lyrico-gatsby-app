---
title: "chhal gaya chhalaa song lyrics"
album: "The Girl On The Train"
artist: "Sunny Inder"
lyricist: "Kumaar"
director: "Ribhu Dasgupta"
path: "/albums/the-girl-on-the-train-lyrics"
song: "Chhal Gaya Chhalaa"
image: ../../images/albumart/the-girl-on-the-train.jpg
date: 2021-02-26
lang: hindi
youtubeLink: "https://www.youtube.com/embed/NiymDv11Ids"
type: "sad"
singers:
  - Sukhwinder Singh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Badlan nu thageya saavan ne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Badlan nu thageya saavan ne"/>
</div>
<div class="lyrico-lyrics-wrapper">Baarish cheekhan maare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baarish cheekhan maare"/>
</div>
<div class="lyrico-lyrics-wrapper">Gusse vich peele naina ne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gusse vich peele naina ne"/>
</div>
<div class="lyrico-lyrics-wrapper">Hanju khare khare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hanju khare khare"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho badlan nu thageya saavan ne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho badlan nu thageya saavan ne"/>
</div>
<div class="lyrico-lyrics-wrapper">Baarish cheekhan maare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baarish cheekhan maare"/>
</div>
<div class="lyrico-lyrics-wrapper">Gusse vich peele naina ne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gusse vich peele naina ne"/>
</div>
<div class="lyrico-lyrics-wrapper">Hanju khare khare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hanju khare khare"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haye samajh nahi aandi dard rooh de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haye samajh nahi aandi dard rooh de"/>
</div>
<div class="lyrico-lyrics-wrapper">Kyun nahi sunda ae allah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kyun nahi sunda ae allah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chhala chhala chhala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chhala chhala chhala"/>
</div>
<div class="lyrico-lyrics-wrapper">Chhal gaya oye chhalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chhal gaya oye chhalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Chhala chhala chhala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chhala chhala chhala"/>
</div>
<div class="lyrico-lyrics-wrapper">Chhal gaya oye chhalla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chhal gaya oye chhalla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
(<div class="lyrico-lyrics-wrapper">chhala chhala chhala hoye)
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chhala chhala chhala hoye)"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tod gaya hai parinda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tod gaya hai parinda"/>
</div>
<div class="lyrico-lyrics-wrapper">Dil hawavan da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil hawavan da"/>
</div>
<div class="lyrico-lyrics-wrapper">Patta patta tutteya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Patta patta tutteya"/>
</div>
<div class="lyrico-lyrics-wrapper">Piplan diyan chhawan da hoye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Piplan diyan chhawan da hoye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tod gaya hai parinda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tod gaya hai parinda"/>
</div>
<div class="lyrico-lyrics-wrapper">Dil hawavan da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil hawavan da"/>
</div>
<div class="lyrico-lyrics-wrapper">Patta patta tutteya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Patta patta tutteya"/>
</div>
<div class="lyrico-lyrics-wrapper">Piplan diyan chhawan da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Piplan diyan chhawan da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jis tan laage chot door tak
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jis tan laage chot door tak"/>
</div>
<div class="lyrico-lyrics-wrapper">Woh hi machaave halla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Woh hi machaave halla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chhala chhala chhala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chhala chhala chhala"/>
</div>
<div class="lyrico-lyrics-wrapper">Chhal gaya oye chhalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chhal gaya oye chhalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Chhala chhala chhala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chhala chhala chhala"/>
</div>
<div class="lyrico-lyrics-wrapper">Chhal gaya oye chhalla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chhal gaya oye chhalla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chhala chhala chhala hoye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chhala chhala chhala hoye"/>
</div>
</pre>
