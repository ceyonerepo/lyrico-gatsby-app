---
title: "mama mama song lyrics"
album: "Kanabadutaledu"
artist: "Madhu Ponnas"
lyricist: "Madhunandan"
director: "Balaraju M"
path: "/albums/kanabadutaledu-lyrics"
song: "Mama Mama"
image: ../../images/albumart/kanabadutaledu.jpg
date: 2021-08-19
lang: telugu
youtubeLink: "https://www.youtube.com/embed/A5OEiBD4JsM"
type: "happy"
singers:
  - Rahul Nambiar
  - Madhu Ponnas
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Instagramulo stalking
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Instagramulo stalking"/>
</div>
<div class="lyrico-lyrics-wrapper">Whatsapp groupulo chatting
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Whatsapp groupulo chatting"/>
</div>
<div class="lyrico-lyrics-wrapper">Selfie la tho freezing
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Selfie la tho freezing"/>
</div>
<div class="lyrico-lyrics-wrapper">Every second
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Every second"/>
</div>
<div class="lyrico-lyrics-wrapper">Smiley la tho greeting
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Smiley la tho greeting"/>
</div>
<div class="lyrico-lyrics-wrapper">As per the trend
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="As per the trend"/>
</div>
<div class="lyrico-lyrics-wrapper">Facebook page lo floating
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Facebook page lo floating"/>
</div>
<div class="lyrico-lyrics-wrapper">Twitter site lo trolling
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Twitter site lo trolling"/>
</div>
<div class="lyrico-lyrics-wrapper">Online lo Rocking
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Online lo Rocking"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh my darling
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh my darling"/>
</div>
<div class="lyrico-lyrics-wrapper">Off line lo always
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Off line lo always"/>
</div>
<div class="lyrico-lyrics-wrapper">Lazy living
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lazy living"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Google lo gaalisthoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Google lo gaalisthoo"/>
</div>
<div class="lyrico-lyrics-wrapper">YouTube lo viharisthoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="YouTube lo viharisthoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Life anthe solo gaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life anthe solo gaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Migaladam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Migaladam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey mama mama nacchaledu mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey mama mama nacchaledu mama"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee trend ey nacchaledu mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee trend ey nacchaledu mama"/>
</div>
<div class="lyrico-lyrics-wrapper">Don't waste time listen to me mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Don't waste time listen to me mama"/>
</div>
<div class="lyrico-lyrics-wrapper">Meaning eh leni life ey avasaramaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meaning eh leni life ey avasaramaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey mama mama nacchaledu mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey mama mama nacchaledu mama"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee trend ey nacchaledu mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee trend ey nacchaledu mama"/>
</div>
<div class="lyrico-lyrics-wrapper">Don't waste time listen to me mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Don't waste time listen to me mama"/>
</div>
<div class="lyrico-lyrics-wrapper">Meaning eh leni life ey avasaramaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meaning eh leni life ey avasaramaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maati maatikosari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maati maatikosari"/>
</div>
<div class="lyrico-lyrics-wrapper">Smart phone choostaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Smart phone choostaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Matter emi lekunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Matter emi lekunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Just scroll chestaru Sodaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Just scroll chestaru Sodaraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arey Cute face pettesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey Cute face pettesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Dubsmash chestaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dubsmash chestaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Hot gunna dressesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hot gunna dressesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Mass dance vesthaaru Choodaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mass dance vesthaaru Choodaraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rey mamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rey mamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalla mundu unna vaalle 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalla mundu unna vaalle "/>
</div>
<div class="lyrico-lyrics-wrapper">neeku ayina valluraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeku ayina valluraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Cellu phonu pakkanetti 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cellu phonu pakkanetti "/>
</div>
<div class="lyrico-lyrics-wrapper">lokamantha choodaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lokamantha choodaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna okka jeevithaanni 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna okka jeevithaanni "/>
</div>
<div class="lyrico-lyrics-wrapper">waste cheyyamaakuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="waste cheyyamaakuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">waste cheyyamaakuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="waste cheyyamaakuraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey mama mama nacchaledu mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey mama mama nacchaledu mama"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee trend ey nacchaledu mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee trend ey nacchaledu mama"/>
</div>
<div class="lyrico-lyrics-wrapper">Don't waste time listen to me mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Don't waste time listen to me mama"/>
</div>
<div class="lyrico-lyrics-wrapper">Meaning eh leni life ey avasaramaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meaning eh leni life ey avasaramaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey mama mama nacchaledu mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey mama mama nacchaledu mama"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee trend ey nacchaledu mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee trend ey nacchaledu mama"/>
</div>
<div class="lyrico-lyrics-wrapper">Don't waste time listen to me mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Don't waste time listen to me mama"/>
</div>
<div class="lyrico-lyrics-wrapper">Meaning eh leni life ey avasaramaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meaning eh leni life ey avasaramaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Soul mate kosamantu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soul mate kosamantu"/>
</div>
<div class="lyrico-lyrics-wrapper">Tinder app lo doori
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tinder app lo doori"/>
</div>
<div class="lyrico-lyrics-wrapper">Aagamaaga mouthaaru mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagamaaga mouthaaru mama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Panee paata lekunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panee paata lekunte"/>
</div>
<div class="lyrico-lyrics-wrapper">TikTok on chesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="TikTok on chesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Araachakam chestaaru mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Araachakam chestaaru mama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vodduraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vodduraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee lonely life ey sodaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee lonely life ey sodaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Cell phonne dooram pettaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cell phonne dooram pettaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">anushulatho dosthee kattaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anushulatho dosthee kattaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhyalona mediator endukantu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhyalona mediator endukantu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anthakanna andamaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anthakanna andamaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Bandhamedi leduraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bandhamedi leduraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey mama mama nacchaledu mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey mama mama nacchaledu mama"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee trend ey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee trend ey "/>
</div>
<div class="lyrico-lyrics-wrapper">nacchaledu mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nacchaledu mama"/>
</div>
<div class="lyrico-lyrics-wrapper">Don't waste time listen to me mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Don't waste time listen to me mama"/>
</div>
<div class="lyrico-lyrics-wrapper">Meaning eh leni life ey avasaramaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meaning eh leni life ey avasaramaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey mama mama nacchaledu mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey mama mama nacchaledu mama"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee trend ey nacchaledu mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee trend ey nacchaledu mama"/>
</div>
<div class="lyrico-lyrics-wrapper">Don't waste time listen to me mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Don't waste time listen to me mama"/>
</div>
<div class="lyrico-lyrics-wrapper">Meaning eh leni life ey avasaramaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meaning eh leni life ey avasaramaa"/>
</div>
</pre>
