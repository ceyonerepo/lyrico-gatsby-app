---
title: "mattikkiten song lyrics"
album: "Padaiveeran"
artist: "Karthik Raja"
lyricist: "Dhana Sekaran"
director: "Dhana Sekaran"
path: "/albums/padaiveeran-lyrics"
song: "Mattikkiten"
image: ../../images/albumart/padaiveeran.jpg
date: 2018-02-02
lang: tamil
youtubeLink: "https://www.youtube.com/embed/n1RDN_KBTOY"
type: "love"
singers:
  - Rita Thyagarajan
  - Haricharan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ayyayyo maatikitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyayyo maatikitten"/>
</div>
<div class="lyrico-lyrics-wrapper">Unkitta maatikitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unkitta maatikitten"/>
</div>
<div class="lyrico-lyrics-wrapper">Valikkaama kaatikitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valikkaama kaatikitten"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavethu nijamethu theriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavethu nijamethu theriyala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee yenna suththi vitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee yenna suththi vitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Yengengo paththavacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yengengo paththavacha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannukkul kaththi vacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannukkul kaththi vacha"/>
</div>
<div class="lyrico-lyrics-wrapper">Killi killi paathapinnum puriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Killi killi paathapinnum puriyala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ooo ooo ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooo ooo ooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayyayyo maatikitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyayyo maatikitten"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayyayyo maatikitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyayyo maatikitten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sanda pottu paathiruppa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sanda pottu paathiruppa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandi kuthira saanji ninnu paathiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandi kuthira saanji ninnu paathiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathi pesi paathiruppa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathi pesi paathiruppa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallipoovu kannadichen paathiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallipoovu kannadichen paathiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeh kokku nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh kokku nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kezuthi naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kezuthi naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna koththi kutharida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna koththi kutharida"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathi katharida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathi katharida"/>
</div>
<div class="lyrico-lyrics-wrapper">Kammaa karaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kammaa karaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Yema nozhaiyira nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yema nozhaiyira nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ayyayyo maatikitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyayyo maatikitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Enkitta maatikitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enkitta maatikitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Valikkaama kaatikitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valikkaama kaatikitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavethu nijamethu theriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavethu nijamethu theriyala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa unna suththi vitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa unna suththi vitten"/>
</div>
<div class="lyrico-lyrics-wrapper">Yengengo paththavachen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yengengo paththavachen"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannukkul kaththi vachen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannukkul kaththi vachen"/>
</div>
<div class="lyrico-lyrics-wrapper">Killi killi paathapinnum puriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Killi killi paathapinnum puriyala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pongi thirinja pottapulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pongi thirinja pottapulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponna paathaa nenju kulla nikkuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponna paathaa nenju kulla nikkuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Panju virinja kaatukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panju virinja kaatukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanni ponnu kanga thooki vaikuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanni ponnu kanga thooki vaikuraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan singari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan singari"/>
</div>
<div class="lyrico-lyrics-wrapper">En sirippu thee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En sirippu thee"/>
</div>
<div class="lyrico-lyrics-wrapper">En pattu selaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En pattu selaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Suththi yerinthida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suththi yerinthida"/>
</div>
<div class="lyrico-lyrics-wrapper">Valaiyaa vizhuguthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valaiyaa vizhuguthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thisaiyaa maraikuthadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thisaiyaa maraikuthadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enkitta sakthi illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enkitta sakthi illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnapol puthi illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnapol puthi illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirudittaa pottapulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirudittaa pottapulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanda kanavula alaiyiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanda kanavula alaiyiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kolasaami kovilukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolasaami kovilukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Kurumbaadu nenthu vitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurumbaadu nenthu vitten"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaapatha vendikitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaapatha vendikitten"/>
</div>
<div class="lyrico-lyrics-wrapper">Thisai ethu vazhi ethu theriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thisai ethu vazhi ethu theriyala"/>
</div>
</pre>
