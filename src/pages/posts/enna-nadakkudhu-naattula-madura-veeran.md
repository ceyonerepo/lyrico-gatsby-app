---
title: "enna nadakkudhu naattula song lyrics"
album: "Madura Veeran"
artist: "Santhosh Dhayanidhi"
lyricist: "Yugabharathi"
director: "P.G. Muthiah"
path: "/albums/madura-veeran-lyrics"
song: "Enna Nadakkudhu Naattula"
image: ../../images/albumart/madura-veeran.jpg
date: 2018-02-02
lang: tamil
youtubeLink: "https://www.youtube.com/embed/lKoJyCp5Zqw"
type: "mass"
singers:
  - Jaya Moorthy
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Enna Nadakudhu Naatula Adha Eduthu Sollanum Paatula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Nadakudhu Naatula Adha Eduthu Sollanum Paatula"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Nadakudhu Naatula Adha Eduthu Sollanum Paatula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Nadakudhu Naatula Adha Eduthu Sollanum Paatula"/>
</div>
<div class="lyrico-lyrics-wrapper">Kollai Adichavan Kootula Kollai Adichavan Kootula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kollai Adichavan Kootula Kollai Adichavan Kootula"/>
</div>
<div class="lyrico-lyrics-wrapper">Numma Kudumbam Nikkudhu Roatula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Numma Kudumbam Nikkudhu Roatula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Nadakudhu Naatula Adha Eduthu Sollanum Paatula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Nadakudhu Naatula Adha Eduthu Sollanum Paatula"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Nadakudhu Naatula Adha Eduthu Sollanum Paatula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Nadakudhu Naatula Adha Eduthu Sollanum Paatula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edhaiyum Naama Paakaama Edhuthu Kelvi Kekaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhaiyum Naama Paakaama Edhuthu Kelvi Kekaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhaiyum Naama Paakaama Edhuthu Kelvi Kekaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhaiyum Naama Paakaama Edhuthu Kelvi Kekaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Adangi Odungi Kedapadhaala Aalukaalu Naataama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adangi Odungi Kedapadhaala Aalukaalu Naataama"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Nadakudhu Naatula Enna Nadakudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Nadakudhu Naatula Enna Nadakudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Nadakudhu Naatula Adha Eduthu Sollanum Paatula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Nadakudhu Naatula Adha Eduthu Sollanum Paatula"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Nadakudhu Naatula Adha Eduthu Sollanum Paatula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Nadakudhu Naatula Adha Eduthu Sollanum Paatula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ethana Ethana Katchi Irukudhu Onnum Nadakaliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethana Ethana Katchi Irukudhu Onnum Nadakaliye"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooru Mukilum Thikilum Saami Irukudhu Kanna Thorakaliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooru Mukilum Thikilum Saami Irukudhu Kanna Thorakaliye"/>
</div>
<div class="lyrico-lyrics-wrapper">Buthanum Gandhiyum Ninna Edathula Pullu Mulaikudhadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Buthanum Gandhiyum Ninna Edathula Pullu Mulaikudhadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinna Pullaiyum Appanum Serndhu Kudichida Sanda Nadakudhadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna Pullaiyum Appanum Serndhu Kudichida Sanda Nadakudhadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ethana Ethana Saadhi Irukudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethana Ethana Saadhi Irukudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethana Ethana Saadhi Irukudhu Enni Mudikalaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethana Ethana Saadhi Irukudhu Enni Mudikalaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhil Kuppanum Suppanum Sethu Madivadha Sattam Thadukalaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhil Kuppanum Suppanum Sethu Madivadha Sattam Thadukalaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethana Ethana Saadhi Irukudhu Enni Mudikalaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethana Ethana Saadhi Irukudhu Enni Mudikalaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhil Kuppanum Suppanum Sethu Madivadha Sattam Thadukalaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhil Kuppanum Suppanum Sethu Madivadha Sattam Thadukalaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mela Yaaru Keezha Yaaru Sonnaa Paavamadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mela Yaaru Keezha Yaaru Sonnaa Paavamadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naama Naalu Perum Onnaa Serndhaa Ellam Maarumadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naama Naalu Perum Onnaa Serndhaa Ellam Maarumadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mela Yaaru Keezha Yaaru Sonnaa Paavamadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mela Yaaru Keezha Yaaru Sonnaa Paavamadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naama Naalu Perum Onnaa Serndhaa Ellam Maarumadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naama Naalu Perum Onnaa Serndhaa Ellam Maarumadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Munnaiyum Pinnaiyum Enna Nadandhadhu Konjam Padikanume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munnaiyum Pinnaiyum Enna Nadandhadhu Konjam Padikanume"/>
</div>
<div class="lyrico-lyrics-wrapper">Inga Ulladhu Mothamum Makkalukendru Nee Pangu Pirikanume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga Ulladhu Mothamum Makkalukendru Nee Pangu Pirikanume"/>
</div>
<div class="lyrico-lyrics-wrapper">Lanjamum Oozhalum Innum Edhukunu Etti Odhaikanumdaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lanjamum Oozhalum Innum Edhukunu Etti Odhaikanumdaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vesam Podura Yaaraiyum Unnma Nerupula Vechi Erikanumdaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vesam Podura Yaaraiyum Unnma Nerupula Vechi Erikanumdaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nalladha Senjida Ulla Varaiyilum Otti Irukanume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalladha Senjida Ulla Varaiyilum Otti Irukanume"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam Kaiyile Vandhida Vote'U Porikinga Mottai Adikanume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam Kaiyile Vandhida Vote'U Porikinga Mottai Adikanume"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalladha Senjida Ulla Varaiyilum Otti Irukanume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalladha Senjida Ulla Varaiyilum Otti Irukanume"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam Kaiyile Vandhida Vote'U Porikinga Mottai Adikanume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam Kaiyile Vandhida Vote'U Porikinga Mottai Adikanume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mosamaana Peiyum Oda Sanga Oodhanumdaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mosamaana Peiyum Oda Sanga Oodhanumdaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Samudhaayam Vaazha Neeyum Naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samudhaayam Vaazha Neeyum Naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sondham Aaganumdaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sondham Aaganumdaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Nadakudhu Naatula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Nadakudhu Naatula"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Nadakudhu Naatula Adha Eduthu Sollanum Paatula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Nadakudhu Naatula Adha Eduthu Sollanum Paatula"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Nadakudhu Naatula Adha Eduthu Sollanum Paatula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Nadakudhu Naatula Adha Eduthu Sollanum Paatula"/>
</div>
<div class="lyrico-lyrics-wrapper">Kollai Adichavan Kootula Kollai Adichavan Kootula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kollai Adichavan Kootula Kollai Adichavan Kootula"/>
</div>
<div class="lyrico-lyrics-wrapper">Numma Kudumbam Nikkudhu Roatula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Numma Kudumbam Nikkudhu Roatula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Nadakudhu Inga Enna Nadakudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Nadakudhu Inga Enna Nadakudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Nadakudhu Naatula Adha Eduthu Sollanum Paatula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Nadakudhu Naatula Adha Eduthu Sollanum Paatula"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Nadakudhu Naatula Adha Eduthu Sollanum Paatula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Nadakudhu Naatula Adha Eduthu Sollanum Paatula"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Nadakudhu Naatula Adha Eduthu Sollanum Paatula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Nadakudhu Naatula Adha Eduthu Sollanum Paatula"/>
</div>
</pre>
