---
title: "ada ada song lyrics"
album: "Mannar Vagaiyara"
artist: "Jakes Bejoy"
lyricist: "Sarathi"
director: "G. Boopathy Pandian"
path: "/albums/mannar-vagaiyara-lyrics"
song: "Ada Ada"
image: ../../images/albumart/mannar-vagaiyara.jpg
date: 2018-01-26
lang: tamil
youtubeLink: "https://www.youtube.com/embed/SdHjMYn4THA"
type: "happy"
singers:
  - Madhu Balakrishnan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">adada adada 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adada adada "/>
</div>
<div class="lyrico-lyrics-wrapper">veete kovil than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veete kovil than"/>
</div>
<div class="lyrico-lyrics-wrapper">thirunal thiname 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thirunal thiname "/>
</div>
<div class="lyrico-lyrics-wrapper">inthe valvil than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inthe valvil than"/>
</div>
<div class="lyrico-lyrics-wrapper">adada adada 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adada adada "/>
</div>
<div class="lyrico-lyrics-wrapper">veete kovil than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veete kovil than"/>
</div>
<div class="lyrico-lyrics-wrapper">thirunal thiname 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thirunal thiname "/>
</div>
<div class="lyrico-lyrics-wrapper">inthe valvil than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inthe valvil than"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oru vaalai thaar ena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru vaalai thaar ena"/>
</div>
<div class="lyrico-lyrics-wrapper">uruvana sonthame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uruvana sonthame"/>
</div>
<div class="lyrico-lyrics-wrapper">ithai thandi boomiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithai thandi boomiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">vera enna vendume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vera enna vendume"/>
</div>
<div class="lyrico-lyrics-wrapper">yarodu yar enbathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yarodu yar enbathai"/>
</div>
<div class="lyrico-lyrics-wrapper">theivangal thayangal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theivangal thayangal "/>
</div>
<div class="lyrico-lyrics-wrapper">aadi aadi serpom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aadi aadi serpom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">adada adada 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adada adada "/>
</div>
<div class="lyrico-lyrics-wrapper">veete kovil than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veete kovil than"/>
</div>
<div class="lyrico-lyrics-wrapper">thirunal thiname 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thirunal thiname "/>
</div>
<div class="lyrico-lyrics-wrapper">inthe valvil than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inthe valvil than"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">irumanam ondru sernthalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irumanam ondru sernthalum"/>
</div>
<div class="lyrico-lyrics-wrapper">oru uyir endru aanalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru uyir endru aanalum"/>
</div>
<div class="lyrico-lyrics-wrapper">ketti melam kanum yogam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ketti melam kanum yogam"/>
</div>
<div class="lyrico-lyrics-wrapper">vaikathu inge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaikathu inge"/>
</div>
<div class="lyrico-lyrics-wrapper">namakena ulla niyayangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="namakena ulla niyayangal"/>
</div>
<div class="lyrico-lyrics-wrapper">namakena ulla kaayangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="namakena ulla kaayangal"/>
</div>
<div class="lyrico-lyrics-wrapper">sonnal kooda sontha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sonnal kooda sontha"/>
</div>
<div class="lyrico-lyrics-wrapper">pantham ketkathu inge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pantham ketkathu inge"/>
</div>
<div class="lyrico-lyrics-wrapper">sila nerangal paasangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sila nerangal paasangal"/>
</div>
<div class="lyrico-lyrics-wrapper">thaalatume sila nerangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaalatume sila nerangal"/>
</div>
<div class="lyrico-lyrics-wrapper">paasangal thalai vangume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paasangal thalai vangume"/>
</div>
<div class="lyrico-lyrics-wrapper">erivom endru naam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="erivom endru naam"/>
</div>
<div class="lyrico-lyrics-wrapper">therinthe naam inge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="therinthe naam inge"/>
</div>
<div class="lyrico-lyrics-wrapper">viraganome sonthangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viraganome sonthangal"/>
</div>
<div class="lyrico-lyrics-wrapper">ondraga sera 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ondraga sera "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">adada adada 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adada adada "/>
</div>
<div class="lyrico-lyrics-wrapper">veete kovil than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veete kovil than"/>
</div>
<div class="lyrico-lyrics-wrapper">thirunal thiname 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thirunal thiname "/>
</div>
<div class="lyrico-lyrics-wrapper">inthe valvil than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inthe valvil than"/>
</div>
<div class="lyrico-lyrics-wrapper">adada adada 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adada adada "/>
</div>
<div class="lyrico-lyrics-wrapper">veete kovil than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veete kovil than"/>
</div>
<div class="lyrico-lyrics-wrapper">thirunal thiname 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thirunal thiname "/>
</div>
<div class="lyrico-lyrics-wrapper">inthe valvil than 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inthe valvil than "/>
</div>
</pre>
