---
title: "nee en nanbanae song lyrics"
album: "Raatchasi"
artist: "Sean Roldan"
lyricist: "Yugabharathi"
director: "Sy Gowthamraj"
path: "/albums/raatchasi-lyrics"
song: "Nee En Nanbanae"
image: ../../images/albumart/raatchasi.jpg
date: 2019-07-05
lang: tamil
youtubeLink: "https://www.youtube.com/embed/SjAcvpfILOs"
type: "sad"
singers:
  - Brindha Sivakumar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nee Yen Nanbanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Yen Nanbanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhvaai Nenjilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvaai Nenjilae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Yen Nanbanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Yen Nanbanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhvaai Nenjilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvaai Nenjilae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaai Kooda Pesatha Seithigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaai Kooda Pesatha Seithigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Pesa Ketanae Un magal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Pesa Ketanae Un magal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Oottiya Dhairiyam Ondru Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Oottiya Dhairiyam Ondru Dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">En Balam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Balam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanae Neeyena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanae Neeyena"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanen Naalai Thodara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanen Naalai Thodara"/>
</div>
<div class="lyrico-lyrics-wrapper">Thodara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uligal Theendamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uligal Theendamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Oor Selaiyumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oor Selaiyumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagil Thondrathae Manamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagil Thondrathae Manamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Maranam Paarkathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maranam Paarkathaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oor Uyirumae Mulumai Aagathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oor Uyirumae Mulumai Aagathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nijamae Yedhanaiyum Kelamaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijamae Yedhanaiyum Kelamaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaangi Nee Thaarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaangi Nee Thaarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Porulgaalai Pol Thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porulgaalai Pol Thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sogamum Yengeyum Naan Poga Yenninaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sogamum Yengeyum Naan Poga Yenninaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai Kaatti Nirpaye Vaasalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai Kaatti Nirpaye Vaasalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippothu Nee Ponathal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippothu Nee Ponathal"/>
</div>
<div class="lyrico-lyrics-wrapper">Aayiram Kelvigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayiram Kelvigal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaanum Yaavilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanum Yaavilum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarpen Unthan Uruvam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarpen Unthan Uruvam"/>
</div>
<div class="lyrico-lyrics-wrapper">Uruvam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uruvam"/>
</div>
</pre>
