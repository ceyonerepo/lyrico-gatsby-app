---
title: "what a man song lyrics"
album: "Vivaha Bhojanambu"
artist: "AniVee"
lyricist: "Kittu Vissapragada"
director: "Ram Abbaraju"
path: "/albums/vivaha-bhojanambu-lyrics"
song: "What A Man"
image: ../../images/albumart/vivaha-bhojanambu.jpg
date: 2021-08-27
lang: telugu
youtubeLink: "https://www.youtube.com/embed/MItmz58mw1g"
type: "happy"
singers:
  - Chowrasta Ram
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">What a man-u what a man
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="What a man-u what a man"/>
</div>
<div class="lyrico-lyrics-wrapper">What a man-u what a man
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="What a man-u what a man"/>
</div>
<div class="lyrico-lyrics-wrapper">Savings antey crazy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Savings antey crazy"/>
</div>
<div class="lyrico-lyrics-wrapper">Shaving cheyani sannasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shaving cheyani sannasi"/>
</div>
<div class="lyrico-lyrics-wrapper">What a man-u what a man
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="What a man-u what a man"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedavadam aapesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedavadam aapesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Veedu by birth ey pinaasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veedu by birth ey pinaasi"/>
</div>
<div class="lyrico-lyrics-wrapper">What a man-u what a man
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="What a man-u what a man"/>
</div>
<div class="lyrico-lyrics-wrapper">What a man-u what a man shankara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="What a man-u what a man shankara"/>
</div>
<div class="lyrico-lyrics-wrapper">Savings antey crazy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Savings antey crazy"/>
</div>
<div class="lyrico-lyrics-wrapper">Shaving cheyani sannasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shaving cheyani sannasi"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedavadam aapesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedavadam aapesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Veedu by birth ey pinaasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veedu by birth ey pinaasi"/>
</div>
<div class="lyrico-lyrics-wrapper">Who is this
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Who is this"/>
</div>
<div class="lyrico-lyrics-wrapper">Who is this Who is this man
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Who is this Who is this man"/>
</div>
<div class="lyrico-lyrics-wrapper">Under wearu bayataku vesey super man
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Under wearu bayataku vesey super man"/>
</div>
<div class="lyrico-lyrics-wrapper">What a what a chorus
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="What a what a chorus"/>
</div>
<div class="lyrico-lyrics-wrapper">What a man-u what a man
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="What a man-u what a man"/>
</div>
<div class="lyrico-lyrics-wrapper">What a man-u what a man
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="What a man-u what a man"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alcohol muttadanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alcohol muttadanta"/>
</div>
<div class="lyrico-lyrics-wrapper">K.a paul ki shishyudanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="K.a paul ki shishyudanta"/>
</div>
<div class="lyrico-lyrics-wrapper">Pulihore kalapadanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pulihore kalapadanta"/>
</div>
<div class="lyrico-lyrics-wrapper">Friendski rupai pettadanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friendski rupai pettadanta"/>
</div>
<div class="lyrico-lyrics-wrapper">Alcohol muttadanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alcohol muttadanta"/>
</div>
<div class="lyrico-lyrics-wrapper">K.a paul ki shishyudanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="K.a paul ki shishyudanta"/>
</div>
<div class="lyrico-lyrics-wrapper">Pulihore kalapadanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pulihore kalapadanta"/>
</div>
<div class="lyrico-lyrics-wrapper">Friendski rupai pettadanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friendski rupai pettadanta"/>
</div>
<div class="lyrico-lyrics-wrapper">Who is this
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Who is this"/>
</div>
<div class="lyrico-lyrics-wrapper">Who is this Who is this man
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Who is this Who is this man"/>
</div>
<div class="lyrico-lyrics-wrapper">Aha naa pellantalo Kota ki fan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aha naa pellantalo Kota ki fan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thagulkondi abbayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thagulkondi abbayi"/>
</div>
<div class="lyrico-lyrics-wrapper">What a man-u what a man
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="What a man-u what a man"/>
</div>
<div class="lyrico-lyrics-wrapper">What a man-u what a man
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="What a man-u what a man"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hotel ke velladanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hotel ke velladanta"/>
</div>
<div class="lyrico-lyrics-wrapper">Total bille kattadnata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Total bille kattadnata"/>
</div>
<div class="lyrico-lyrics-wrapper">Vocal cord vaadanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vocal cord vaadanta"/>
</div>
<div class="lyrico-lyrics-wrapper">Local brandey comfort anta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Local brandey comfort anta"/>
</div>
<div class="lyrico-lyrics-wrapper">Hotel ke velladanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hotel ke velladanta"/>
</div>
<div class="lyrico-lyrics-wrapper">Total bille kattadnata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Total bille kattadnata"/>
</div>
<div class="lyrico-lyrics-wrapper">Vocal cord vaadanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vocal cord vaadanta"/>
</div>
<div class="lyrico-lyrics-wrapper">Local brandey comfort anta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Local brandey comfort anta"/>
</div>
<div class="lyrico-lyrics-wrapper">Who is this
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Who is this"/>
</div>
<div class="lyrico-lyrics-wrapper">Who is this Who is this man
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Who is this Who is this man"/>
</div>
<div class="lyrico-lyrics-wrapper">Who is this
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Who is this"/>
</div>
<div class="lyrico-lyrics-wrapper">Who is this Who is this man
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Who is this Who is this man"/>
</div>
<div class="lyrico-lyrics-wrapper">Body building cheyyaleni salman khan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Body building cheyyaleni salman khan"/>
</div>
<div class="lyrico-lyrics-wrapper">What a… rey padara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="What a… rey padara"/>
</div>
<div class="lyrico-lyrics-wrapper">What a man-u what a man
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="What a man-u what a man"/>
</div>
<div class="lyrico-lyrics-wrapper">What a man-u what a man
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="What a man-u what a man"/>
</div>
<div class="lyrico-lyrics-wrapper">Rey lepandra konchem
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rey lepandra konchem"/>
</div>
<div class="lyrico-lyrics-wrapper">What a man-u what a man
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="What a man-u what a man"/>
</div>
<div class="lyrico-lyrics-wrapper">What a man-u what a man
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="What a man-u what a man"/>
</div>
<div class="lyrico-lyrics-wrapper">Savings antey crazy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Savings antey crazy"/>
</div>
<div class="lyrico-lyrics-wrapper">Shaving cheyani sannasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shaving cheyani sannasi"/>
</div>
</pre>
