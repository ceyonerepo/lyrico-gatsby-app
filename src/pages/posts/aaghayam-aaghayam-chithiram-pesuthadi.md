---
title: "aaghayam aaghayam song lyrics"
album: "Chithiram Pesuthadi"
artist: "Sundar C Babu"
lyricist: "Mysskin"
director: "Mysskin"
path: "/albums/chithiram-pesuthadi-lyrics"
song: "Aaghayam Aaghayam"
image: ../../images/albumart/chithiram-pesuthadi.jpg
date: 2006-02-10
lang: tamil
youtubeLink: "https://www.youtube.com/embed/aGom5WH4lm4"
type: "sad"
singers:
  - Hariharan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aagayam Aagayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagayam Aagayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Megam Baarama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megam Baarama"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Vazhvil Un Vazhvil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Vazhvil Un Vazhvil"/>
</div>
<div class="lyrico-lyrics-wrapper">Naane Sogama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naane Sogama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyire Ennai Velagaathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyire Ennai Velagaathe"/>
</div>
<div class="lyrico-lyrics-wrapper">En Nizhale Ennai Piriyaathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nizhale Ennai Piriyaathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyire Ennai Velagaathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyire Ennai Velagaathe"/>
</div>
<div class="lyrico-lyrics-wrapper">En Nizhale Ennai Piriyaathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nizhale Ennai Piriyaathe"/>
</div>
<div class="lyrico-lyrics-wrapper">En Paadal En Kadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Paadal En Kadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Endrum Thedi Varum Antha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Endrum Thedi Varum Antha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aagayam Aagayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagayam Aagayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Megam Baarama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megam Baarama"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Vazhvil Un Vazhvil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Vazhvil Un Vazhvil"/>
</div>
<div class="lyrico-lyrics-wrapper">Naane Sogama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naane Sogama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanavil Vannam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanavil Vannam"/>
</div>
<div class="lyrico-lyrics-wrapper">Malaiyil Saayam Pogumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malaiyil Saayam Pogumo"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaayin Muththam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaayin Muththam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kulainthukku Kaayam Aagumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulainthukku Kaayam Aagumo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pongaatre Poovai Kollathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pongaatre Poovai Kollathe"/>
</div>
<div class="lyrico-lyrics-wrapper">En Anbe Nenjai Killathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Anbe Nenjai Killathe"/>
</div>
<div class="lyrico-lyrics-wrapper">En Paadal En Kadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Paadal En Kadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Endrum Thedi Varum Antha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Endrum Thedi Varum Antha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aagayam Aagayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagayam Aagayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Megam Baarama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megam Baarama"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Vazhvil Un Vazhvil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Vazhvil Un Vazhvil"/>
</div>
<div class="lyrico-lyrics-wrapper">Naane Sogama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naane Sogama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nilavum Oliyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilavum Oliyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingu Pirinthu Pogumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingu Pirinthu Pogumo"/>
</div>
<div class="lyrico-lyrics-wrapper">Alaiyum Karaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alaiyum Karaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nelaiyaai Velagi Pogumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nelaiyaai Velagi Pogumo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thendrale Thalli Chellathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thendrale Thalli Chellathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhaya Kathavinai Moodikkollathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhaya Kathavinai Moodikkollathe"/>
</div>
<div class="lyrico-lyrics-wrapper">En Paadal…en Kadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Paadal…en Kadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Endrum Thedi Varum Antha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Endrum Thedi Varum Antha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aagayam Aagayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagayam Aagayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Megam Baarama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megam Baarama"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Vazhvil Un Vazhvil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Vazhvil Un Vazhvil"/>
</div>
<div class="lyrico-lyrics-wrapper">Naane Sogama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naane Sogama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyire Ennai Velagaathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyire Ennai Velagaathe"/>
</div>
<div class="lyrico-lyrics-wrapper">En Nizhale Ennai Piriyaathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nizhale Ennai Piriyaathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyire Ennai Velagaathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyire Ennai Velagaathe"/>
</div>
<div class="lyrico-lyrics-wrapper">En Nizhale Ennai Piriyaathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nizhale Ennai Piriyaathe"/>
</div>
<div class="lyrico-lyrics-wrapper">En Paadal En Kadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Paadal En Kadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Endrum Thedi Varum Antha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Endrum Thedi Varum Antha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aagayam Aagayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagayam Aagayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Megam Baarama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megam Baarama"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Vazhvil Un Vazhvil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Vazhvil Un Vazhvil"/>
</div>
<div class="lyrico-lyrics-wrapper">Naane Sogama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naane Sogama"/>
</div>
</pre>
