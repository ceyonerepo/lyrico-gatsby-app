---
title: "ennatha solla song lyrics"
album: "Kolanji"
artist: "Natarajan Sankaran"
lyricist: "Yugabharathi"
director: "Dhanaram Saravanan"
path: "/albums/kolanji-lyrics"
song: "Ennatha Solla"
image: ../../images/albumart/kolanji.jpg
date: 2019-07-26
lang: tamil
youtubeLink: "https://www.youtube.com/embed/o4l4Aa08Ujo"
type: "sad"
singers:
  - Sathyaprakash
  - Vandana Srinivasan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ennatha solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennatha solla"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan unnaala aluthen ulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan unnaala aluthen ulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Verenna naan solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verenna naan solla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennatha solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennatha solla"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan unnaala aluthen ulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan unnaala aluthen ulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Verenna naan solluven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verenna naan solluven"/>
</div>
<div class="lyrico-lyrics-wrapper">Theriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theriyala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennatha solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennatha solla"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan unnaala aluthen ulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan unnaala aluthen ulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">U and me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="U and me"/>
</div>
<div class="lyrico-lyrics-wrapper">Like a pair of dove
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Like a pair of dove"/>
</div>
<div class="lyrico-lyrics-wrapper">We choose to live
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="We choose to live"/>
</div>
<div class="lyrico-lyrics-wrapper">In the shower of love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="In the shower of love"/>
</div>
<div class="lyrico-lyrics-wrapper">But life’s a game
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="But life’s a game"/>
</div>
<div class="lyrico-lyrics-wrapper">In the course of time
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="In the course of time"/>
</div>
<div class="lyrico-lyrics-wrapper">Disaster springs
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Disaster springs"/>
</div>
<div class="lyrico-lyrics-wrapper">And tragedy strikes
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="And tragedy strikes"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hoo ooo hooo ooooo oooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo ooo hooo ooooo oooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna naanae ippothu thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna naanae ippothu thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Meiyagavae paakkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meiyagavae paakkuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna naanum ennaathathaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna naanum ennaathathaala"/>
</div>
<div class="lyrico-lyrics-wrapper">En pecha naan kekkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En pecha naan kekkuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasula neethaan neengiyathaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasula neethaan neengiyathaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaai thanthaiya nenachen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaai thanthaiya nenachen"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru vari kooda gnyabagam illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru vari kooda gnyabagam illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Un yetta naan kilichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un yetta naan kilichen"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanaa idhuthan nirantharamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanaa idhuthan nirantharamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennatha solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennatha solla"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan unnaala aluthen ulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan unnaala aluthen ulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unna paathu thoongaatha naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna paathu thoongaatha naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oiyaaramaa thoonguven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oiyaaramaa thoonguven"/>
</div>
<div class="lyrico-lyrics-wrapper">Oththa aalaa aanaalum kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oththa aalaa aanaalum kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Gethaaga naan vaazhuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gethaaga naan vaazhuven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanavula kuda nee varamaata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavula kuda nee varamaata"/>
</div>
<div class="lyrico-lyrics-wrapper">Un pera naa marappen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un pera naa marappen"/>
</div>
<div class="lyrico-lyrics-wrapper">Theruvula ninna aayiram ponnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theruvula ninna aayiram ponnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Santhosamaaa rasippen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Santhosamaaa rasippen"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanaa edhuthaan nadanthidumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanaa edhuthaan nadanthidumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennatha solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennatha solla"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan unnaala aluthen ulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan unnaala aluthen ulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Verenna naan solluven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verenna naan solluven"/>
</div>
<div class="lyrico-lyrics-wrapper">Theriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theriyala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennatha solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennatha solla"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan unnaala aluthen ulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan unnaala aluthen ulla"/>
</div>
</pre>
