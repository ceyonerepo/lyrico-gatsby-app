---
title: "dhenam dhenam song lyrics"
album: "Venghai"
artist: "Devi Sri Prasad"
lyricist: "Viveka"
director: "Hari"
path: "/albums/venghai-lyrics"
song: "Dhenam Dhenam"
image: ../../images/albumart/venghai.jpg
date: 2011-07-08
lang: tamil
youtubeLink: "https://www.youtube.com/embed/DDmHRw5s2mU"
type: "mass"
singers:
  - Benny Dayal
  - Baba Sehgal
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Sivagangai Seemai Padaiyodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sivagangai Seemai Padaiyodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sendhoor Murugan Thunaiyodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sendhoor Murugan Thunaiyodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Padhungi Paayum Thunivodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padhungi Paayum Thunivodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaengai Polae Vilaiyaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaengai Polae Vilaiyaadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhenam Dhenam Nadakkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhenam Dhenam Nadakkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Thadi Odha Odha Odha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Thadi Odha Odha Odha"/>
</div>
<div class="lyrico-lyrics-wrapper">Hei Nadungudhu Nadungudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei Nadungudhu Nadungudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhiriyin Sadha Sadha Sadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhiriyin Sadha Sadha Sadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhiradi Kedubidi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhiradi Kedubidi"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Namma Kadha Kadha Kadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Namma Kadha Kadha Kadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoi Dhairiyam Thaanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoi Dhairiyam Thaanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Ieyikkira Vedha Vedha Vedha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ieyikkira Vedha Vedha Vedha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Veeranodu Modhumbodhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Veeranodu Modhumbodhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjimela Modhadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjimela Modhadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Throghiyoda Sanda Vandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Throghiyoda Sanda Vandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjimela Midhidaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjimela Midhidaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Paamarangal Thappu Senja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Paamarangal Thappu Senja"/>
</div>
<div class="lyrico-lyrics-wrapper">Paavam Paarthu Viduda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paavam Paarthu Viduda"/>
</div>
<div class="lyrico-lyrics-wrapper">Padichavan Thappu Senjaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padichavan Thappu Senjaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Paanji Paanji Adidaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paanji Paanji Adidaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maayaandi Malaiyaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maayaandi Malaiyaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Odhaichaathaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odhaichaathaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Life Aiyum Bike Aiyum Ottida Mudiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life Aiyum Bike Aiyum Ottida Mudiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Purinjikkada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Purinjikkada"/>
</div>
<div class="lyrico-lyrics-wrapper">Veeraandi Virumaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veeraandi Virumaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Adiyum Udhaiyum Udhavuramaadhiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiyum Udhaiyum Udhavuramaadhiri"/>
</div>
<div class="lyrico-lyrics-wrapper">Annan Thambhiyum Udhavaladaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annan Thambhiyum Udhavaladaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhenam Dhenam Nadakkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhenam Dhenam Nadakkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Thadi Odha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Thadi Odha"/>
</div>
<div class="lyrico-lyrics-wrapper">Odha Odha Odha Odha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odha Odha Odha Odha"/>
</div>
<div class="lyrico-lyrics-wrapper">Hei Nadungudhu Nadungudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei Nadungudhu Nadungudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhiriyin Sadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhiriyin Sadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Sadha Sadha Sadha Sadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sadha Sadha Sadha Sadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Odha Odha Odha Odha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odha Odha Odha Odha"/>
</div>
<div class="lyrico-lyrics-wrapper">Sadha Sadha Sadha Sadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sadha Sadha Sadha Sadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Thulli Thulli Edhiriya Kuththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Thulli Thulli Edhiriya Kuththu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unadhu Kaiyil Veredhukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unadhu Kaiyil Veredhukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Veral Indha Paththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veral Indha Paththu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Idipol Madiyila Thaakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Idipol Madiyila Thaakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Adichadhula Velithalla Venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adichadhula Velithalla Venum"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan Naakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan Naakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Koottathula Sandaipoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Koottathula Sandaipoda"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaniyaa Ponaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaniyaa Ponaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Velakkennai Thaechikkittu Podaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velakkennai Thaechikkittu Podaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Selampettakaaranoda Modha Jallikkalla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Selampettakaaranoda Modha Jallikkalla"/>
</div>
<div class="lyrico-lyrics-wrapper">Allikkittu Poda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allikkittu Poda"/>
</div>
<div class="lyrico-lyrics-wrapper">Vambadiyae Sandapoda Venaam Venaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vambadiyae Sandapoda Venaam Venaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthasanda Vidavum Venaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthasanda Vidavum Venaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Venghai Pola Vegammaaga Adikkanunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venghai Pola Vegammaaga Adikkanunda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mookkaandi Muniyaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mookkaandi Muniyaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aiyo Paavam Kozhandhapola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyo Paavam Kozhandhapola"/>
</div>
<div class="lyrico-lyrics-wrapper">Mogatha Vechi Nadakkanumdaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mogatha Vechi Nadakkanumdaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adikkira Adiyila Ammiyum Kozhaviyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adikkira Adiyila Ammiyum Kozhaviyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Panjappola Parakkanundaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panjappola Parakkanundaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hei Dhenam Dhenam Nadakkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei Dhenam Dhenam Nadakkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Thadi Odha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Thadi Odha"/>
</div>
<div class="lyrico-lyrics-wrapper">Odha Odha Odha Odha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odha Odha Odha Odha"/>
</div>
<div class="lyrico-lyrics-wrapper">Hei Nadungudhu Nadungudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei Nadungudhu Nadungudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhiriyin Sadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhiriyin Sadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Sadha Sadha Sadha Sadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sadha Sadha Sadha Sadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Odha Odha Odha Odha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odha Odha Odha Odha"/>
</div>
<div class="lyrico-lyrics-wrapper">Sadha Sadha Sadha Sadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sadha Sadha Sadha Sadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Pombalainga Poduradhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Pombalainga Poduradhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Konda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konda"/>
</div>
<div class="lyrico-lyrics-wrapper">Avangalukku Poda Kaththu Tharaporen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avangalukku Poda Kaththu Tharaporen"/>
</div>
<div class="lyrico-lyrics-wrapper">Sanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Poovakkandaa Merattura Vanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Poovakkandaa Merattura Vanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Sariyaana Neram Paarthu Nimirthanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sariyaana Neram Paarthu Nimirthanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Benda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Benda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Koondhalukku Pottu Vacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Koondhalukku Pottu Vacha"/>
</div>
<div class="lyrico-lyrics-wrapper">Konda Oosi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konda Oosi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuththinaalae Kathiyaaga Maarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuththinaalae Kathiyaaga Maarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannaadi Valaiyala Odachi Pidicha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaadi Valaiyala Odachi Pidicha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaarumaaraa Keerum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaarumaaraa Keerum"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppavumae Oru Padi Melaga Thoolaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppavumae Oru Padi Melaga Thoolaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuppattaavil Mudinjittu Ponaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuppattaavil Mudinjittu Ponaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannil Thoovi Kannaamoochi Aadidanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannil Thoovi Kannaamoochi Aadidanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kamatchi Oo Meenatchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kamatchi Oo Meenatchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aruvaamanaiyum Aayudhamaagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aruvaamanaiyum Aayudhamaagum"/>
</div>
<div class="lyrico-lyrics-wrapper">Eduthaa Yevanum Therippaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eduthaa Yevanum Therippaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adiyum Udhaiyum Odhavura Maadhiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiyum Udhaiyum Odhavura Maadhiri"/>
</div>
<div class="lyrico-lyrics-wrapper">Purushanum Odhavamaattaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Purushanum Odhavamaattaanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hei Dhenam Dhenam Nadakkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei Dhenam Dhenam Nadakkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Thadi Odha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Thadi Odha"/>
</div>
<div class="lyrico-lyrics-wrapper">Odha Odha Odha Odha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odha Odha Odha Odha"/>
</div>
<div class="lyrico-lyrics-wrapper">Hei Nadungudhu Nadungudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei Nadungudhu Nadungudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhiriyin Sadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhiriyin Sadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Sadha Sadha Sadha Sadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sadha Sadha Sadha Sadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Be Ready Be Ready Be Ready To Fight
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Be Ready Be Ready Be Ready To Fight"/>
</div>
<div class="lyrico-lyrics-wrapper">Knock It Out Knock It Out Pup Pup The Might
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Knock It Out Knock It Out Pup Pup The Might"/>
</div>
<div class="lyrico-lyrics-wrapper">If Rhythm Of The Tol That Sets The Girl
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="If Rhythm Of The Tol That Sets The Girl"/>
</div>
<div class="lyrico-lyrics-wrapper">May Be Tricky Micky Ricky Sadae Thaedae Maerae Ball
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="May Be Tricky Micky Ricky Sadae Thaedae Maerae Ball"/>
</div>
<div class="lyrico-lyrics-wrapper">Be Ready Be Ready Be Ready To Fight
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Be Ready Be Ready Be Ready To Fight"/>
</div>
<div class="lyrico-lyrics-wrapper">Pup Pup The Might
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pup Pup The Might"/>
</div>
</pre>
