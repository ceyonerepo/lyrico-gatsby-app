---
title: "basanti aaj song lyrics"
album: "Suraj pe mangal bhari"
artist: "Javed - Mohsin"
lyricist: "Danish Sabri"
director: "Abhishek Sharma"
path: "/albums/suraj-pe-mangal-bhari-lyrics"
song: "Basanti Aaj"
image: ../../images/albumart/suraj-pe-mangal-bhari.jpg
date: 2020-11-15
lang: hindi
youtubeLink: "https://www.youtube.com/embed/-nBn5TqZMdo"
type: "happy"
singers:
  - Payal Dev
  - Danish Sabri
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Oh mere veeru ki jaan fasi hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh mere veeru ki jaan fasi hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Gabbar ki bhayanak hassi hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gabbar ki bhayanak hassi hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh mere veeru ki jaan fasi hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh mere veeru ki jaan fasi hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Gabbar ki bhayanak hassi hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gabbar ki bhayanak hassi hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Main to tod dungi paayal, ho jaaungi ghayal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main to tod dungi paayal, ho jaaungi ghayal"/>
</div>
<div class="lyrico-lyrics-wrapper">Chahe pairon mein chubh jaaye kaanch
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chahe pairon mein chubh jaaye kaanch"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Basanti aaj, basanti aaj, basanti aaj
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Basanti aaj, basanti aaj, basanti aaj"/>
</div>
<div class="lyrico-lyrics-wrapper">Kutto ke saamne naa naach
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kutto ke saamne naa naach"/>
</div>
<div class="lyrico-lyrics-wrapper">Basanti aaj, basanti aaj, basanti aaj
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Basanti aaj, basanti aaj, basanti aaj"/>
</div>
<div class="lyrico-lyrics-wrapper">Kutto ke saamne naa naach
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kutto ke saamne naa naach"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hmm.. Tu dekhe meri aur
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm.. Tu dekhe meri aur"/>
</div>
<div class="lyrico-lyrics-wrapper">Tujhko koyi aur
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tujhko koyi aur"/>
</div>
<div class="lyrico-lyrics-wrapper">Zulmi nazar se dekhta hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zulmi nazar se dekhta hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Haan tu dekhe meri aur
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haan tu dekhe meri aur"/>
</div>
<div class="lyrico-lyrics-wrapper">Tujhko koyi aur
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tujhko koyi aur"/>
</div>
<div class="lyrico-lyrics-wrapper">Zulmi nazar se dekhta hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zulmi nazar se dekhta hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Banke dear mera dekhe jigar mera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Banke dear mera dekhe jigar mera"/>
</div>
<div class="lyrico-lyrics-wrapper">Ankhiyon ko tu sekta hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ankhiyon ko tu sekta hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Dance aisa karungi roke se naa rukungi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dance aisa karungi roke se naa rukungi"/>
</div>
<div class="lyrico-lyrics-wrapper">Chahe subah ke baj jaaye 5(paanch)
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chahe subah ke baj jaaye 5(paanch)"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Basanti aaj, basanti aaj, basanti aaj
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Basanti aaj, basanti aaj, basanti aaj"/>
</div>
<div class="lyrico-lyrics-wrapper">Kutto ke saamne naa naach
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kutto ke saamne naa naach"/>
</div>
<div class="lyrico-lyrics-wrapper">Basanti aaj, basanti aaj, basanti aaj
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Basanti aaj, basanti aaj, basanti aaj"/>
</div>
<div class="lyrico-lyrics-wrapper">Kutto ke saamne naa naach
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kutto ke saamne naa naach"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naachungi aaj naachungi chahe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naachungi aaj naachungi chahe"/>
</div>
<div class="lyrico-lyrics-wrapper">Pairon mein chubh jaaye kaanch
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pairon mein chubh jaaye kaanch"/>
</div>
</pre>
