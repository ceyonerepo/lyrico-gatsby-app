---
title: "pushpa song lyrics"
album: "Touch Chesi Chudu"
artist: "JAM 8 - Mani Sharma"
lyricist: "Rehman"
director: "Vikram Sirikonda"
path: "/albums/touch-chesi-chudu-lyrics"
song: "Pushpa"
image: ../../images/albumart/touch-chesi-chudu.jpg
date: 2018-02-02
lang: telugu
youtubeLink: "https://www.youtube.com/embed/9D4aTnZSqxE"
type: "love"
singers:
  - Nakash Aziz
  - Dj Smash Guy
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yem Janthar Manthar Chesaav 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yem Janthar Manthar Chesaav "/>
</div>
<div class="lyrico-lyrics-wrapper">Naapai Ye Manthram Vesaav 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naapai Ye Manthram Vesaav "/>
</div>
<div class="lyrico-lyrics-wrapper">Asaledho Maaye Chesaav Uuuuuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asaledho Maaye Chesaav Uuuuuu"/>
</div>
<div class="lyrico-lyrics-wrapper">Choopultheney Chuttesaav 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choopultheney Chuttesaav "/>
</div>
<div class="lyrico-lyrics-wrapper">Navvultho Padagottesaav 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Navvultho Padagottesaav "/>
</div>
<div class="lyrico-lyrics-wrapper">Maataltho Madathettesaav Uuuuuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maataltho Madathettesaav Uuuuuu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oye Pushpaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oye Pushpaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvemo Kanney Bangaram 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvemo Kanney Bangaram "/>
</div>
<div class="lyrico-lyrics-wrapper">Thaakavo Dhummu Dhumaaram 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaakavo Dhummu Dhumaaram "/>
</div>
<div class="lyrico-lyrics-wrapper">Haa Pantham Pattesi Pai Paikochesi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa Pantham Pattesi Pai Paikochesi "/>
</div>
<div class="lyrico-lyrics-wrapper">Thappisthunnaavey Aachararam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappisthunnaavey Aachararam "/>
</div>
<div class="lyrico-lyrics-wrapper">Apachaaram Naapai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Apachaaram Naapai "/>
</div>
<div class="lyrico-lyrics-wrapper">Neekenti Adhikaaram 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neekenti Adhikaaram "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oye Pushpaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oye Pushpaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Aipoyaa Neekey Dhaasoham 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aipoyaa Neekey Dhaasoham "/>
</div>
<div class="lyrico-lyrics-wrapper">Penchoddhey Inkaa Vyamoham 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Penchoddhey Inkaa Vyamoham "/>
</div>
<div class="lyrico-lyrics-wrapper">Haa Kavvinchey Naari 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa Kavvinchey Naari "/>
</div>
<div class="lyrico-lyrics-wrapper">Kanney Godhaari 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanney Godhaari "/>
</div>
<div class="lyrico-lyrics-wrapper">Munchela Undhey Yavvaaram 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munchela Undhey Yavvaaram "/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Bhaaram Naapai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Bhaaram Naapai "/>
</div>
<div class="lyrico-lyrics-wrapper">Neekenti Adhikaaram 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neekenti Adhikaaram "/>
</div>
<div class="lyrico-lyrics-wrapper">Yem Janthar Manthar Chesaav 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yem Janthar Manthar Chesaav "/>
</div>
<div class="lyrico-lyrics-wrapper">Naapai Ye Manthram Vesaav 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naapai Ye Manthram Vesaav "/>
</div>
<div class="lyrico-lyrics-wrapper">Asaledho Maaye Chesaav Uuuuuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asaledho Maaye Chesaav Uuuuuu"/>
</div>
<div class="lyrico-lyrics-wrapper">Choopultheney Chuttesaav 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choopultheney Chuttesaav "/>
</div>
<div class="lyrico-lyrics-wrapper">Navvultho Padagottesaav 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Navvultho Padagottesaav "/>
</div>
<div class="lyrico-lyrics-wrapper">Maataltho Madathettesaav Uuuuuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maataltho Madathettesaav Uuuuuu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Itu Soodey Nee Cheyye Thagilithe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Itu Soodey Nee Cheyye Thagilithe "/>
</div>
<div class="lyrico-lyrics-wrapper">Lolona Perigenu Haaye Haaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lolona Perigenu Haaye Haaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey Haaye Arey Haaye 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey Haaye Arey Haaye "/>
</div>
<div class="lyrico-lyrics-wrapper">Zara Aagey Ee Haaye Mudirithe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zara Aagey Ee Haaye Mudirithe "/>
</div>
<div class="lyrico-lyrics-wrapper">Aapeti Veeley Ledhu Maaye Maaye 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aapeti Veeley Ledhu Maaye Maaye "/>
</div>
<div class="lyrico-lyrics-wrapper">Penu Maaye Penu Maaye 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Penu Maaye Penu Maaye "/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Gaali Sokindhantey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Gaali Sokindhantey "/>
</div>
<div class="lyrico-lyrics-wrapper">Uyyaloogey Praanam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyyaloogey Praanam "/>
</div>
<div class="lyrico-lyrics-wrapper">Ponandhi Ninney Veedi Pori 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponandhi Ninney Veedi Pori "/>
</div>
<div class="lyrico-lyrics-wrapper">Nannitta Laagesthuntey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannitta Laagesthuntey "/>
</div>
<div class="lyrico-lyrics-wrapper">Muddocchey Nee Roopam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muddocchey Nee Roopam "/>
</div>
<div class="lyrico-lyrics-wrapper">Aagedhi Yettagey Vayyaari 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagedhi Yettagey Vayyaari "/>
</div>
<div class="lyrico-lyrics-wrapper">Oye Pushpaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oye Pushpaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Saigey Megha Sandhesam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Saigey Megha Sandhesam "/>
</div>
<div class="lyrico-lyrics-wrapper">Vacchesthaa Dhaati Aakasam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vacchesthaa Dhaati Aakasam "/>
</div>
<div class="lyrico-lyrics-wrapper">Hmm Oh Oo Kaitu Laagaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm Oh Oo Kaitu Laagaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Rockettu Laagaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rockettu Laagaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Ayyavey Nuvvey Aadhaaram 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyavey Nuvvey Aadhaaram "/>
</div>
<div class="lyrico-lyrics-wrapper">O Dhaaram Naapai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Dhaaram Naapai "/>
</div>
<div class="lyrico-lyrics-wrapper">Neekenti Adhikaaram 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neekenti Adhikaaram "/>
</div>
<div class="lyrico-lyrics-wrapper">Yem Janthar Manthar Chesaav 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yem Janthar Manthar Chesaav "/>
</div>
<div class="lyrico-lyrics-wrapper">Naapai Ye Manthram Vesaav 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naapai Ye Manthram Vesaav "/>
</div>
<div class="lyrico-lyrics-wrapper">Asaledho Maaye Chesaav Uuuuuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asaledho Maaye Chesaav Uuuuuu"/>
</div>
<div class="lyrico-lyrics-wrapper">Choopultheney Chuttesaav 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choopultheney Chuttesaav "/>
</div>
<div class="lyrico-lyrics-wrapper">Navvultho Padagottesaav 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Navvultho Padagottesaav "/>
</div>
<div class="lyrico-lyrics-wrapper">Maataltho Madathettesaav Uuuuuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maataltho Madathettesaav Uuuuuu"/>
</div>
</pre>
