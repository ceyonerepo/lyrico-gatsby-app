---
title: "manasa vinava song lyrics"
album: "Nootokka Jillala Andagadu"
artist: "Shakthikanth Karthick"
lyricist: "Bhaskarabhatla"
director: "Rachakonda Vidyasagar"
path: "/albums/nootokka-jillala-andagadu-lyrics"
song: " Manasa Vinava"
image: ../../images/albumart/nootokka-jillala-andagadu.jpg
date: 2021-09-03
lang: telugu
youtubeLink: "https://www.youtube.com/embed/A6zbE99zJzQ"
type: "love"
singers:
  - Sreeram Chandra
  - Dhanya Balakrishna
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Oohalakandhani Vennela Choosaa Nee Vallegaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oohalakandhani Vennela Choosaa Nee Vallegaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasaki Rekkalu Kattuku Egiraa Aanandhangaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasaki Rekkalu Kattuku Egiraa Aanandhangaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalale Cheripee Vadhilesaavaa Nanu Ontarigaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalale Cheripee Vadhilesaavaa Nanu Ontarigaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhuke Ilaa Ilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhuke Ilaa Ilaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hayi Ni Konchem Chavi Choopinchi Tharimesaavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hayi Ni Konchem Chavi Choopinchi Tharimesaavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Veluthurunantha Neetho Theesuku Veluthunnaavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veluthurunantha Neetho Theesuku Veluthunnaavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppatilaage Kannula Mundhara Cheekatithovaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppatilaage Kannula Mundhara Cheekatithovaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhuke Ilaa Ilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhuke Ilaa Ilaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nuvu Aade Prathimaataa Nijamenani Nammesaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvu Aade Prathimaataa Nijamenani Nammesaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Emaarchaavu Andhangaa Manassey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emaarchaavu Andhangaa Manassey"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallaaraa Choosaakaa Kaadhanuko Mantaavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallaaraa Choosaakaa Kaadhanuko Mantaavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Theripinchaavu Erojey Kanuleee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theripinchaavu Erojey Kanuleee"/>
</div>
<div class="lyrico-lyrics-wrapper">Techhi Posukuntoo Nee Chuttoo Cheekatinee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Techhi Posukuntoo Nee Chuttoo Cheekatinee"/>
</div>
<div class="lyrico-lyrics-wrapper">Venneledhi Antoo Adagoddhe Raathirinee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venneledhi Antoo Adagoddhe Raathirinee"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Chethulatho Ninne Nuvvu Cheripesaakaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Chethulatho Ninne Nuvvu Cheripesaakaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannante Elaa Elaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannante Elaa Elaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanipinchani Nijamainaa Dhaachaal Anukunnaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanipinchani Nijamainaa Dhaachaal Anukunnaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Musugese Kappindhee Bhayame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Musugese Kappindhee Bhayame"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninu Chusina Prathi Saaree Adhi Naaku Tholi Saaree
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninu Chusina Prathi Saaree Adhi Naaku Tholi Saaree"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Kshaname Dhaachindhee Nijameee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Kshaname Dhaachindhee Nijameee"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanti Paapathone Kanureppe Thalapadithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanti Paapathone Kanureppe Thalapadithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Anthakanna Vere Anyaayam Undadhule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anthakanna Vere Anyaayam Undadhule"/>
</div>
<div class="lyrico-lyrics-wrapper">Chekkili Thaakani Oka Thadigeetham Vinapadaledha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chekkili Thaakani Oka Thadigeetham Vinapadaledha"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasaa Manasaa Vinavaa Vinavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasaa Manasaa Vinavaa Vinavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Na Madhiloo Maate Vinavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na Madhiloo Maate Vinavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasaa Manasaa Vinavaa Vinavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasaa Manasaa Vinavaa Vinavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Na Madhiloo Maate Vinavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na Madhiloo Maate Vinavaa"/>
</div>
</pre>
