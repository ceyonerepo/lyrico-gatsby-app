---
title: "kannale song lyrics"
album: "Market Raja MBBS"
artist: "Simon K. King"
lyricist: "Dhamayanthi"
director: "Saran"
path: "/albums/market-raja-mbbs-lyrics"
song: "Kannale"
image: ../../images/albumart/market-raja-mbbs.jpg
date: 2019-11-29
lang: tamil
youtubeLink: "https://www.youtube.com/embed/poWqju7fKqA"
type: "love"
singers:
  - Sanah Moidutty
  - Yazin Nizar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kannaalae Kannaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaalae Kannaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal Valarthen Kanavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Valarthen Kanavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaalae Unnaalae Naanum Sirithen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaalae Unnaalae Naanum Sirithen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennodu Ennodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennodu Ennodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum Irundhaal Uravae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Irundhaal Uravae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thol Meedhu Thol Saindhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thol Meedhu Thol Saindhu "/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Marappen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Marappen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Illa Nodi Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Illa Nodi Ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Nanaaga Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Nanaaga Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Arugae Irundhaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Arugae Irundhaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">En Moochaagum Kollai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Moochaagum Kollai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannaalae Kannaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaalae Kannaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaalae Unnaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaalae Unnaalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannaalae Kannaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaalae Kannaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal Valarthen Kanavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Valarthen Kanavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaalae Unnaalae Naanum Sirithen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaalae Unnaalae Naanum Sirithen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennodu Ennodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennodu Ennodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum Irundhaal Uravae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Irundhaal Uravae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thol Meedhu Thol Saindhu Ennai Marappen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thol Meedhu Thol Saindhu Ennai Marappen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu Kaadhala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Kaadhala"/>
</div>
<div class="lyrico-lyrics-wrapper">Karai Mattum Nagarum Nadhiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karai Mattum Nagarum Nadhiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanaiyamal Nanaivom Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanaiyamal Nanaivom Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayangamal Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayangamal Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Kannil Mounam Podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Kannil Mounam Podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Maru Kannil Yeno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maru Kannil Yeno"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaarthai Oor Vaarthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaarthai Oor Vaarthai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Solla"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Ketka Ketka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Ketka Ketka"/>
</div>
<div class="lyrico-lyrics-wrapper">Angae Angangae Unadharugae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Angae Angangae Unadharugae"/>
</div>
<div class="lyrico-lyrics-wrapper">Neidhal Poo Pookka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neidhal Poo Pookka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unakkaaga Naan Maayam Seiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkaaga Naan Maayam Seiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Mella Kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Mella Kaadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Kalvan Pola Koththi Poga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Kalvan Pola Koththi Poga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannaalae Kannaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaalae Kannaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal Valarthen Kanavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Valarthen Kanavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaalae Unnaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaalae Unnaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum Sirithen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum Sirithen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennodu Ennodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennodu Ennodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeyum Irundhaal Uravae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Irundhaal Uravae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thol Meedhu Thol Saindhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thol Meedhu Thol Saindhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Marappen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Marappen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu Kaadhala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Kaadhala"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Enni Yengum Kannai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Enni Yengum Kannai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pazhitheerkka Vendum Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pazhitheerkka Vendum Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Urangaamal Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urangaamal Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidiyadha Kaalai Velai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidiyadha Kaalai Velai"/>
</div>
<div class="lyrico-lyrics-wrapper">Udaiyaaga Maara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udaiyaaga Maara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vandhaai Nee Varuvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhaai Nee Varuvai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Vaadhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Vaadhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Theerka Theerka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Theerka Theerka"/>
</div>
<div class="lyrico-lyrics-wrapper">Engo Engengo Enatharugae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engo Engengo Enatharugae"/>
</div>
<div class="lyrico-lyrics-wrapper">Neithal Poo Pookka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neithal Poo Pookka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Meni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Meni"/>
</div>
<div class="lyrico-lyrics-wrapper">Varai Padamum Aaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varai Padamum Aaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Thedum Nenjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Thedum Nenjam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Mela Mela Kangal Aaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Mela Mela Kangal Aaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannaalae Kannaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaalae Kannaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal Valarthen Kanavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Valarthen Kanavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaalae Unnaalae Naanum Sirithen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaalae Unnaalae Naanum Sirithen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennodu Ennodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennodu Ennodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum Irundhaal Uravae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Irundhaal Uravae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thol Meedhu Thol Saindhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thol Meedhu Thol Saindhu "/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Marappen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Marappen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannaalae Kannaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaalae Kannaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannaalae Kannalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaalae Kannalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannaalae Kannaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaalae Kannaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannaalae Kannaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaalae Kannaalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannaalae Kannaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaalae Kannaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal Valarthen Kanavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Valarthen Kanavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennodu Ennodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennodu Ennodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum Irundhaal Uravae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Irundhaal Uravae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thol Meedhu Thol Saindhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thol Meedhu Thol Saindhu "/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Marappen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Marappen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hoo Kannaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo Kannaalae"/>
</div>
</pre>
