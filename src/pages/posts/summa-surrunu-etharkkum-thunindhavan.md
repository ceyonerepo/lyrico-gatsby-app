---
title: "summa surrunu song lyrics"
album: "Etharkkum Thunindhavan"
artist: "D. Imman"
lyricist: "Sivakarthikeyan"
director: "Pandiraj"
path: "/albums/etharkkum-thunindhavan-lyrics"
song: "Summa Surrunu"
image: ../../images/albumart/etharkkum-thunindhavan.jpg
date: 2022-02-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/AZ9gADbC-nU"
type: "love"
singers:
  - Armaan Malik
  - Nikhita Gandhi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Silukku jibba pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silukku jibba pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayakkudhu un smart u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayakkudhu un smart u"/>
</div>
<div class="lyrico-lyrics-wrapper">Vayasukkulla vacha paaru vettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayasukkulla vacha paaru vettu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vedikkidhu en heart u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedikkidhu en heart u"/>
</div>
<div class="lyrico-lyrics-wrapper">Thudikkidhu unna kettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thudikkidhu unna kettu"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasukkulla panji mitta sweat u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasukkulla panji mitta sweat u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Purusha puthusa puthusaa thedaathayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Purusha puthusa puthusaa thedaathayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasa kalachu pottu thoongaathayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasa kalachu pottu thoongaathayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhage azhaga vachu paduthaathamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhage azhaga vachu paduthaathamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee thotta ulla yerum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thotta ulla yerum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Summa surrunu summa surrunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summa surrunu summa surrunu"/>
</div>
<div class="lyrico-lyrics-wrapper">Summa surrunu summa surrunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summa surrunu summa surrunu"/>
</div>
<div class="lyrico-lyrics-wrapper">Summa summa surrunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summa summa surrunu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Silukku jibba pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silukku jibba pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayakkudhu un smart u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayakkudhu un smart u"/>
</div>
<div class="lyrico-lyrics-wrapper">Vayasukkulla vacha paaru vettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayasukkulla vacha paaru vettu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vedikkidhu en heart u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedikkidhu en heart u"/>
</div>
<div class="lyrico-lyrics-wrapper">Thudikkidhu unna kettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thudikkidhu unna kettu"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasukkulla panji mitta sweat u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasukkulla panji mitta sweat u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ginger iduppa athile madippa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ginger iduppa athile madippa"/>
</div>
<div class="lyrico-lyrics-wrapper">Asanja theraattam azhaga nadappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asanja theraattam azhaga nadappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkum enakkum makan thaan porappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkum enakkum makan thaan porappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Singam poraattam verappa iruppa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Singam poraattam verappa iruppa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adiye kitta varava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiye kitta varava"/>
</div>
<div class="lyrico-lyrics-wrapper">Poonkaatta pola thedava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poonkaatta pola thedava"/>
</div>
<div class="lyrico-lyrics-wrapper">Illa puyalukke tough tharava nee vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illa puyalukke tough tharava nee vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Medhuva nee thodava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Medhuva nee thodava"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan kaathirundhen pudhuva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan kaathirundhen pudhuva"/>
</div>
<div class="lyrico-lyrics-wrapper">Onna rough aave deal pannavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna rough aave deal pannavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neethaanadi ennoda cute queen u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethaanadi ennoda cute queen u"/>
</div>
<div class="lyrico-lyrics-wrapper">Jovunu kottura love rain u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jovunu kottura love rain u"/>
</div>
<div class="lyrico-lyrics-wrapper">Irukki anaicha oru umma tharava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irukki anaicha oru umma tharava"/>
</div>
<div class="lyrico-lyrics-wrapper">Summa surrunu summa surrunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summa surrunu summa surrunu"/>
</div>
<div class="lyrico-lyrics-wrapper">Summa surrunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summa surrunu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Silukku jibba pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silukku jibba pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayakkudhu un smart u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayakkudhu un smart u"/>
</div>
<div class="lyrico-lyrics-wrapper">Vayasukkulla vacha paaru vettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayasukkulla vacha paaru vettu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vedikkidhu en heart u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedikkidhu en heart u"/>
</div>
<div class="lyrico-lyrics-wrapper">Thudikkidhu unna kettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thudikkidhu unna kettu"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasukkulla panji mitta sweat u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasukkulla panji mitta sweat u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Purusha puthusa puthusaa thedaathayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Purusha puthusa puthusaa thedaathayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasa kalachu pottu thoongaathayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasa kalachu pottu thoongaathayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhage azhaga vachu paduthaathamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhage azhaga vachu paduthaathamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee thotta ulla yerum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thotta ulla yerum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Summa surrunu summa surrunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summa surrunu summa surrunu"/>
</div>
<div class="lyrico-lyrics-wrapper">Summa surrunu summa surrunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summa surrunu summa surrunu"/>
</div>
<div class="lyrico-lyrics-wrapper">Summa surrunu summa surrunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summa surrunu summa surrunu"/>
</div>
</pre>
