---
title: "ninnu chudagane song lyrics"
album: "Athidi Devo Bhava"
artist: "Shekar Chandra"
lyricist: "Bhaskara Bhatla"
director: "Polimera Nageshawar"
path: "/albums/athidi-devo-bhava-lyrics"
song: "Ninnu Chudagane"
image: ../../images/albumart/athidi-devo-bhava.jpg
date: 2022-01-07
lang: telugu
youtubeLink: "https://www.youtube.com/embed/ua5DeGeCHCQ"
type: "love"
singers:
  - Anurag Kulkarni
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ninnu choodagane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu choodagane"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa gunde jaarindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa gunde jaarindhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna okka praanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna okka praanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee chuttu thirigindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee chuttu thirigindhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninnu choodagane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu choodagane"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee kaalam aagindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee kaalam aagindhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnattundi lokam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnattundi lokam"/>
</div>
<div class="lyrico-lyrics-wrapper">Andhanga maarindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhanga maarindhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oka navve kadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oka navve kadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Oka choope kadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oka choope kadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Inni chithralu yento ilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inni chithralu yento ilaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nuvvele naa shwaasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvele naa shwaasa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvele naa dhyaasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvele naa dhyaasa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neetho brathakalanndhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neetho brathakalanndhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naadho chinna aasha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naadho chinna aasha"/>
</div>
<div class="lyrico-lyrics-wrapper">Neelo nannu choosa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelo nannu choosa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalo ninnu mosa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalo ninnu mosa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aunanna nuv kaadhanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aunanna nuv kaadhanna"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa manase neekicchesaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa manase neekicchesaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poota poota gurthosthunnave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poota poota gurthosthunnave"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeti meedha nadipisthunnave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeti meedha nadipisthunnave"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatuka kallathoti champesthunnave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatuka kallathoti champesthunnave"/>
</div>
<div class="lyrico-lyrics-wrapper">Nootiki noorupaallu nacchesthunnave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nootiki noorupaallu nacchesthunnave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ee janma neethone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee janma neethone"/>
</div>
<div class="lyrico-lyrics-wrapper">Maru janma neethone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maru janma neethone"/>
</div>
<div class="lyrico-lyrics-wrapper">Vachchi gundellona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vachchi gundellona"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaalipove guvvalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaalipove guvvalaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nuvvele naa aata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvele naa aata"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvele naa paata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvele naa paata"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvunte naa santhoshanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvunte naa santhoshanni"/>
</div>
<div class="lyrico-lyrics-wrapper">Veyyalenu kaata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veyyalenu kaata"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nuvve unna chota
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvve unna chota"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadha poola thota
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadha poola thota"/>
</div>
<div class="lyrico-lyrics-wrapper">Hrudayam motham raasichestha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hrudayam motham raasichestha"/>
</div>
<div class="lyrico-lyrics-wrapper">Naakem vodhu vaata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naakem vodhu vaata"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aagi aagi nuvve choosthonte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagi aagi nuvve choosthonte"/>
</div>
<div class="lyrico-lyrics-wrapper">Gunde aagi aagi kottukontunde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gunde aagi aagi kottukontunde"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaajulu gallumani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaajulu gallumani"/>
</div>
<div class="lyrico-lyrics-wrapper">Chappudu chesthunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chappudu chesthunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Gayathri manthramedho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gayathri manthramedho"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinipisthu undhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinipisthu undhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ee haayi nee valle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee haayi nee valle"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee maaya nee valle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee maaya nee valle"/>
</div>
<div class="lyrico-lyrics-wrapper">Neelo nenu kalisipotha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelo nenu kalisipotha"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaalilona themelle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaalilona themelle"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thindi nidhara leka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thindi nidhara leka"/>
</div>
<div class="lyrico-lyrics-wrapper">Raasha prema lekha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raasha prema lekha"/>
</div>
<div class="lyrico-lyrics-wrapper">Enthandhagna vesesaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enthandhagna vesesaave"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasulona geetha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasulona geetha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandha yella dhaaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandha yella dhaaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Nene neeku thoka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nene neeku thoka"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvunte naa life ki inka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvunte naa life ki inka"/>
</div>
<div class="lyrico-lyrics-wrapper">Lene ledhu dhoka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lene ledhu dhoka"/>
</div>
</pre>
