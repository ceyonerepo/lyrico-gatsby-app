---
title: "kannadi kanavugale song lyrics"
album: "Nan Avalai Sandhitha Pothu"
artist: "Hithesh Murugavel- Jai"
lyricist: "Arivumathi"
director: "L G Ravichnadar"
path: "/albums/nan-avalai-sandhitha-pothu-lyrics"
song: "Kannadi Kanavugale"
image: ../../images/albumart/nan-avalai-sandhitha-pothu.jpg
date: 2019-12-27
lang: tamil
youtubeLink: "https://www.youtube.com/embed/tM4UhIaZ3mQ"
type: "sad"
singers:
  - Vijay Yesudas
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">kannadi kanavugale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannadi kanavugale"/>
</div>
<div class="lyrico-lyrics-wrapper">ennai kai veesi udaithathada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennai kai veesi udaithathada"/>
</div>
<div class="lyrico-lyrics-wrapper">munnera thudithavanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munnera thudithavanai"/>
</div>
<div class="lyrico-lyrics-wrapper">vidhi mun vanthu thaduthathada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidhi mun vanthu thaduthathada"/>
</div>
<div class="lyrico-lyrics-wrapper">en moochai nan iluka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en moochai nan iluka"/>
</div>
<div class="lyrico-lyrics-wrapper">viral kenji kenji elupathada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viral kenji kenji elupathada"/>
</div>
<div class="lyrico-lyrics-wrapper">mun baaram athikarika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mun baaram athikarika"/>
</div>
<div class="lyrico-lyrics-wrapper">vaalkai moochiraika padukuthada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalkai moochiraika padukuthada"/>
</div>
<div class="lyrico-lyrics-wrapper">velli thirai aala vanthen athanal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="velli thirai aala vanthen athanal"/>
</div>
<div class="lyrico-lyrics-wrapper">vethanaiyai meeri vanthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vethanaiyai meeri vanthen"/>
</div>
<div class="lyrico-lyrics-wrapper">chennai yai nan thedi vanthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chennai yai nan thedi vanthen"/>
</div>
<div class="lyrico-lyrics-wrapper">ada nan theekuliyil veelthu nindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada nan theekuliyil veelthu nindren"/>
</div>
<div class="lyrico-lyrics-wrapper">barathi yanen mothida enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="barathi yanen mothida enna"/>
</div>
<div class="lyrico-lyrics-wrapper">veelthiduven endru ennathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veelthiduven endru ennathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vetriku poradum manithan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetriku poradum manithan"/>
</div>
<div class="lyrico-lyrics-wrapper">endrume thorpathu illaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="endrume thorpathu illaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">ada nenjukul nambikai vaithavan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada nenjukul nambikai vaithavan "/>
</div>
<div class="lyrico-lyrics-wrapper">jothidam parpathilaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jothidam parpathilaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">vetriku poradum manithan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetriku poradum manithan"/>
</div>
<div class="lyrico-lyrics-wrapper">endrume thorpathu illaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="endrume thorpathu illaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">ada nenjukul nambikai vaithavan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada nenjukul nambikai vaithavan "/>
</div>
<div class="lyrico-lyrics-wrapper">jothidam parpathilaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jothidam parpathilaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kannadi kanavugale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannadi kanavugale"/>
</div>
<div class="lyrico-lyrics-wrapper">ennai kai veesi udaithathada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennai kai veesi udaithathada"/>
</div>
<div class="lyrico-lyrics-wrapper">munnera thudithavanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munnera thudithavanai"/>
</div>
<div class="lyrico-lyrics-wrapper">vidhi mun vanthu thaduthathada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidhi mun vanthu thaduthathada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oodi oodi vasal thorum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oodi oodi vasal thorum"/>
</div>
<div class="lyrico-lyrics-wrapper">vaipugal ketu nindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaipugal ketu nindren"/>
</div>
<div class="lyrico-lyrics-wrapper">en kilakai kodungal endren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kilakai kodungal endren"/>
</div>
<div class="lyrico-lyrics-wrapper">thedi thedi thethi thalkalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thedi thedi thethi thalkalil"/>
</div>
<div class="lyrico-lyrics-wrapper">nane mutti konden ada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nane mutti konden ada"/>
</div>
<div class="lyrico-lyrics-wrapper">kanavugal kotti thindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavugal kotti thindren"/>
</div>
<div class="lyrico-lyrics-wrapper">paarai tookum kunivai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paarai tookum kunivai"/>
</div>
<div class="lyrico-lyrics-wrapper">pole ennai kandene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pole ennai kandene"/>
</div>
<div class="lyrico-lyrics-wrapper">kodampakkam thagam eduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kodampakkam thagam eduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">megam thotene kalaiyin magan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="megam thotene kalaiyin magan"/>
</div>
<div class="lyrico-lyrics-wrapper">vegam thotene vaalkai nathiyin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vegam thotene vaalkai nathiyin"/>
</div>
<div class="lyrico-lyrics-wrapper">sulalil mati kondene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sulalil mati kondene"/>
</div>
<div class="lyrico-lyrics-wrapper">vasamaga mati kondene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vasamaga mati kondene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kannadi kanavugale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannadi kanavugale"/>
</div>
<div class="lyrico-lyrics-wrapper">ennai kai veesi udaithathada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennai kai veesi udaithathada"/>
</div>
<div class="lyrico-lyrics-wrapper">munnera thudithavanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munnera thudithavanai"/>
</div>
<div class="lyrico-lyrics-wrapper">vidhi mun vanthu thaduthathada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidhi mun vanthu thaduthathada"/>
</div>
<div class="lyrico-lyrics-wrapper">en moochai nan iluka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en moochai nan iluka"/>
</div>
<div class="lyrico-lyrics-wrapper">viral kenji kenji elupathada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viral kenji kenji elupathada"/>
</div>
<div class="lyrico-lyrics-wrapper">mun baaram athikarika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mun baaram athikarika"/>
</div>
<div class="lyrico-lyrics-wrapper">vaalkai moochiraika padukuthada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalkai moochiraika padukuthada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thaka thaka sogam thaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaka thaka sogam thaka"/>
</div>
<div class="lyrico-lyrics-wrapper">mounam pooti konden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mounam pooti konden"/>
</div>
<div class="lyrico-lyrics-wrapper">en ethiril maranam kanden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en ethiril maranam kanden"/>
</div>
<div class="lyrico-lyrics-wrapper">parka parka neela parka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="parka parka neela parka"/>
</div>
<div class="lyrico-lyrics-wrapper">vaasal ellai kanden ada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaasal ellai kanden ada"/>
</div>
<div class="lyrico-lyrics-wrapper">irulil aluthu konden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irulil aluthu konden"/>
</div>
<div class="lyrico-lyrics-wrapper">yeno yeno yeno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yeno yeno yeno"/>
</div>
<div class="lyrico-lyrics-wrapper">nitham sethu kondene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nitham sethu kondene"/>
</div>
<div class="lyrico-lyrics-wrapper">vanam thandi poga thane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanam thandi poga thane"/>
</div>
<div class="lyrico-lyrics-wrapper">aasai patene siru thimiril
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aasai patene siru thimiril"/>
</div>
<div class="lyrico-lyrics-wrapper">meesai thotene 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meesai thotene "/>
</div>
<div class="lyrico-lyrics-wrapper">thoondil kadiyil puluvil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoondil kadiyil puluvil"/>
</div>
<div class="lyrico-lyrics-wrapper">meena sikki kondene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meena sikki kondene"/>
</div>
<div class="lyrico-lyrics-wrapper">sariya sikki kondene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sariya sikki kondene"/>
</div>
</pre>
