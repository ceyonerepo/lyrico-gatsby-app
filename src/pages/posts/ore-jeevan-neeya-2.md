---
title: "ore jeevan song lyrics"
album: "Neeya 2"
artist: "Shabir"
lyricist: "Kannadasan"
director: "L. Suresh"
path: "/albums/neeya-2-lyrics"
song: "Ore Jeevan"
image: ../../images/albumart/neeya-2.jpg
date: 2019-05-24
lang: tamil
youtubeLink: "https://www.youtube.com/embed/OwllVc1yki8"
type: "happy"
singers:
  - Naresh Iyer
  - Rita
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aaah aaa aaa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaah aaa aaa aaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ahaa aa  aaa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahaa aa  aaa aaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ore jeevan ondrae ullam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ore jeevan ondrae ullam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaraai kannaa oh oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaraai kannaa oh oo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ore jeevan ondrae ullam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ore jeevan ondrae ullam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaraai kannaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaraai kannaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Orae poovil ondrae thendral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orae poovil ondrae thendral"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaraai kannaa aa aa haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaraai kannaa aa aa haa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ore jeevan ondrae ullam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ore jeevan ondrae ullam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaraai kannae oh ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaraai kannae oh ooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ore jeevan ondrae ullam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ore jeevan ondrae ullam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaraai kannae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaraai kannae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Orae poovil ondrae thendral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orae poovil ondrae thendral"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaraai kannae ae ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaraai kannae ae ae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ore jeevan ondrae ullam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ore jeevan ondrae ullam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaraai kannae oh ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaraai kannae oh ooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ore jeevan ondrae ullam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ore jeevan ondrae ullam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaraai kannae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaraai kannae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ingae vinmeengal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingae vinmeengal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannaagi paarkindrana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaagi paarkindrana"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan verum koyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan verum koyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Aagamal kaappom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagamal kaappom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unthan kanmeengal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unthan kanmeengal"/>
</div>
<div class="lyrico-lyrics-wrapper">En meethu vilaiyaadattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En meethu vilaiyaadattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha vinmeengal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha vinmeengal"/>
</div>
<div class="lyrico-lyrics-wrapper">Suvaiyaaga paaarkka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suvaiyaaga paaarkka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaer kondu vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaer kondu vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannan vanthu geetham sonnaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannan vanthu geetham sonnaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan aaduven ennnn
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan aaduven ennnn"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ore jeevan ondrae ullam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ore jeevan ondrae ullam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaraai kannae oh ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaraai kannae oh ooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ore jeevan ondrae ullam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ore jeevan ondrae ullam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaraai kannae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaraai kannae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Antha manichangin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha manichangin"/>
</div>
<div class="lyrico-lyrics-wrapper">Oli kettu naan aaduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oli kettu naan aaduven"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha mazhai megam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha mazhai megam"/>
</div>
<div class="lyrico-lyrics-wrapper">Un meethu aadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un meethu aadum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Antha manichangin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha manichangin"/>
</div>
<div class="lyrico-lyrics-wrapper">Oli kettu naan aaduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oli kettu naan aaduven"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha mazhai megam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha mazhai megam"/>
</div>
<div class="lyrico-lyrics-wrapper">Un meethu aadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un meethu aadum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vanna pazhaththodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanna pazhaththodum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mugaththodum nee koodalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mugaththodum nee koodalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha pazhaththottam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha pazhaththottam"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnodu koodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnodu koodum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Puthu vellamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthu vellamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Orae sorgam enthan pakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orae sorgam enthan pakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ver illaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ver illaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ore jeevan ondrae ullam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ore jeevan ondrae ullam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaraai kannaa oh oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaraai kannaa oh oo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ore jeevan ondrae ullam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ore jeevan ondrae ullam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaraai kannaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaraai kannaa"/>
</div>
</pre>
