---
title: "adi avara song lyrics"
album: "Veerapandiyapuram"
artist: "Jai"
lyricist: "Ekadesi"
director: "Suseenthiran"
path: "/albums/veerapandiyapuram-lyrics"
song: "Adi Avara"
image: ../../images/albumart/veerapandiyapuram.jpg
date: 2022-02-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/n7Ps2qZiWOg"
type: "happy"
singers:
  - Mahalingam
  - Jai
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Adi avare en thovare parikkava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi avare en thovare parikkava"/>
</div>
<div class="lyrico-lyrics-wrapper">En veetukkulla velakku venum sirikkava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En veetukkulla velakku venum sirikkava"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kirukkil nan murikki thiriyeran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kirukkil nan murikki thiriyeran"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi unnala uppu kalla karaiuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi unnala uppu kalla karaiuren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannalam kattikittu rend perum pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannalam kattikittu rend perum pona"/>
</div>
<div class="lyrico-lyrics-wrapper">Kondata oorusanam varum thana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondata oorusanam varum thana"/>
</div>
<div class="lyrico-lyrics-wrapper">Santhosama valavaippen santhegam vena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Santhosama valavaippen santhegam vena"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandali unna matten yenna vittu pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandali unna matten yenna vittu pona"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un mama ullankail thankituvene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un mama ullankail thankituvene"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee ketta ooraiye vankituvene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee ketta ooraiye vankituvene"/>
</div>
<div class="lyrico-lyrics-wrapper">Vitinchale unna pakka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vitinchale unna pakka "/>
</div>
<div class="lyrico-lyrics-wrapper">pakkathula vachikkuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pakkathula vachikkuven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi avare en thovare parikkava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi avare en thovare parikkava"/>
</div>
<div class="lyrico-lyrics-wrapper">En veetukkulla velakku venum sirikkava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En veetukkulla velakku venum sirikkava"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kirukkil nan murikki thiriyeran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kirukkil nan murikki thiriyeran"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi unnala uppu kalla karaiuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi unnala uppu kalla karaiuren"/>
</div>
</pre>
