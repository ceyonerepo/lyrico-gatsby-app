---
title: "pattuku pattu song lyrics"
album: "Virudhu"
artist: "Dhina "
lyricist: "Athavan"
director: "Athavan"
path: "/albums/virudhu-lyrics"
song: "Pattuku Pattu"
image: ../../images/albumart/virudhu.jpg
date: 2019-12-20
lang: tamil
youtubeLink: "https://www.youtube.com/embed/rq7mS4RkV00"
type: "happy"
singers:
  - Mukesh
  - Dhina
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">thanana hey hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanana hey hey hey"/>
</div>
<div class="lyrico-lyrics-wrapper">thanana hey hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanana hey hey hey"/>
</div>
<div class="lyrico-lyrics-wrapper">thanana hey hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanana hey hey hey"/>
</div>
<div class="lyrico-lyrics-wrapper">thanana na hey hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanana na hey hey hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">paatuku pattu edupom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paatuku pattu edupom"/>
</div>
<div class="lyrico-lyrics-wrapper">enga patanoda peru edupom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga patanoda peru edupom"/>
</div>
<div class="lyrico-lyrics-wrapper">petha thai thanthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="petha thai thanthai"/>
</div>
<div class="lyrico-lyrics-wrapper">sollai ketpom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sollai ketpom"/>
</div>
<div class="lyrico-lyrics-wrapper">yei engaloda manna kapom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yei engaloda manna kapom"/>
</div>
<div class="lyrico-lyrics-wrapper">oh padika than palli koodam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oh padika than palli koodam"/>
</div>
<div class="lyrico-lyrics-wrapper">ponathu illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponathu illa"/>
</div>
<div class="lyrico-lyrics-wrapper">tamil lana nan eluthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tamil lana nan eluthu"/>
</div>
<div class="lyrico-lyrics-wrapper">kotti paduchathu illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kotti paduchathu illa"/>
</div>
<div class="lyrico-lyrics-wrapper">aadu maadu meikirathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aadu maadu meikirathu"/>
</div>
<div class="lyrico-lyrics-wrapper">enga vela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga vela"/>
</div>
<div class="lyrico-lyrics-wrapper">antha andavan than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antha andavan than"/>
</div>
<div class="lyrico-lyrics-wrapper">kakka venum enga valva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kakka venum enga valva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">paatuku pattu edupom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paatuku pattu edupom"/>
</div>
<div class="lyrico-lyrics-wrapper">enga patanoda peru edupom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga patanoda peru edupom"/>
</div>
<div class="lyrico-lyrics-wrapper">petha thai thanthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="petha thai thanthai"/>
</div>
<div class="lyrico-lyrics-wrapper">sollai ketpom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sollai ketpom"/>
</div>
<div class="lyrico-lyrics-wrapper">yei engaloda manna kapom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yei engaloda manna kapom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thanthane na 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanthane na "/>
</div>
<div class="lyrico-lyrics-wrapper">thanthane na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanthane na"/>
</div>
<div class="lyrico-lyrics-wrapper">oh oh oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oh oh oh"/>
</div>
<div class="lyrico-lyrics-wrapper">thanthane na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanthane na"/>
</div>
<div class="lyrico-lyrics-wrapper">thanthane na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanthane na"/>
</div>
<div class="lyrico-lyrics-wrapper">oh oh oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oh oh oh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">hey nallavanga manasula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey nallavanga manasula"/>
</div>
<div class="lyrico-lyrics-wrapper">than nanga thangi irupom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="than nanga thangi irupom"/>
</div>
<div class="lyrico-lyrics-wrapper">intha naada enga tholila 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha naada enga tholila "/>
</div>
<div class="lyrico-lyrics-wrapper">than thooki nanga nadapom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="than thooki nanga nadapom"/>
</div>
<div class="lyrico-lyrics-wrapper">kan moodi thoonga matom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kan moodi thoonga matom"/>
</div>
<div class="lyrico-lyrics-wrapper">nanga karanam than solla matom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanga karanam than solla matom"/>
</div>
<div class="lyrico-lyrics-wrapper">kanda edathula nika matom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanda edathula nika matom"/>
</div>
<div class="lyrico-lyrics-wrapper">nanga nalla valiyila thanga nadapom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanga nalla valiyila thanga nadapom"/>
</div>
<div class="lyrico-lyrics-wrapper">kathi sanda kuthu sanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathi sanda kuthu sanda"/>
</div>
<div class="lyrico-lyrics-wrapper">kathi sanda kuthu sanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathi sanda kuthu sanda"/>
</div>
<div class="lyrico-lyrics-wrapper">engaluku theriyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engaluku theriyum"/>
</div>
<div class="lyrico-lyrics-wrapper">vambu sanda vambu sanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vambu sanda vambu sanda"/>
</div>
<div class="lyrico-lyrics-wrapper">pota than puriyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pota than puriyum"/>
</div>
<div class="lyrico-lyrics-wrapper">chinna chinna sigaram pothom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chinna chinna sigaram pothom"/>
</div>
<div class="lyrico-lyrics-wrapper">sirikarama kadaiya potom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sirikarama kadaiya potom"/>
</div>
<div class="lyrico-lyrics-wrapper">sillu sillu thooral pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sillu sillu thooral pola"/>
</div>
<div class="lyrico-lyrics-wrapper">kutralathil aruvi kota pathom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kutralathil aruvi kota pathom"/>
</div>
<div class="lyrico-lyrics-wrapper">uyaramana imaya malaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyaramana imaya malaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">pathathu illa nanga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathathu illa nanga "/>
</div>
<div class="lyrico-lyrics-wrapper">manasu vacha pathuduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasu vacha pathuduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">thooram illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thooram illa"/>
</div>
<div class="lyrico-lyrics-wrapper">engaloda idhayam thanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engaloda idhayam thanga"/>
</div>
<div class="lyrico-lyrics-wrapper">imaya malai athu engaloda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="imaya malai athu engaloda"/>
</div>
<div class="lyrico-lyrics-wrapper">poti potta chinna malai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poti potta chinna malai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">paatuku pattu edupom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paatuku pattu edupom"/>
</div>
<div class="lyrico-lyrics-wrapper">enga patanoda peru edupom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga patanoda peru edupom"/>
</div>
<div class="lyrico-lyrics-wrapper">petha thai thanthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="petha thai thanthai"/>
</div>
<div class="lyrico-lyrics-wrapper">sollai ketpom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sollai ketpom"/>
</div>
<div class="lyrico-lyrics-wrapper">yei engaloda manna kapom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yei engaloda manna kapom"/>
</div>
<div class="lyrico-lyrics-wrapper">oh padika than palli koodam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oh padika than palli koodam"/>
</div>
<div class="lyrico-lyrics-wrapper">ponathu illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponathu illa"/>
</div>
<div class="lyrico-lyrics-wrapper">tamil lana nan eluthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tamil lana nan eluthu"/>
</div>
<div class="lyrico-lyrics-wrapper">kotti paduchathu illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kotti paduchathu illa"/>
</div>
<div class="lyrico-lyrics-wrapper">aadu maadu meikirathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aadu maadu meikirathu"/>
</div>
<div class="lyrico-lyrics-wrapper">enga vela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga vela"/>
</div>
<div class="lyrico-lyrics-wrapper">antha andavan than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antha andavan than"/>
</div>
<div class="lyrico-lyrics-wrapper">kakka venum enga valva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kakka venum enga valva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">paatuku pattu edupom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paatuku pattu edupom"/>
</div>
<div class="lyrico-lyrics-wrapper">enga patanoda peru edupom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga patanoda peru edupom"/>
</div>
<div class="lyrico-lyrics-wrapper">petha thai thanthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="petha thai thanthai"/>
</div>
<div class="lyrico-lyrics-wrapper">sollai ketpom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sollai ketpom"/>
</div>
<div class="lyrico-lyrics-wrapper">yei engaloda manna kapom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yei engaloda manna kapom"/>
</div>
</pre>
