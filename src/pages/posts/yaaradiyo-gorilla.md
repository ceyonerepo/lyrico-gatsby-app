---
title: "yaaradiyo song lyrics"
album: "Gorilla"
artist: "Sam C.S."
lyricist: "Sam C.S."
director: "Don Sandy"
path: "/albums/gorilla-lyrics"
song: "Yaaradiyo"
image: ../../images/albumart/gorilla.jpg
date: 2019-07-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/jdeObM_g_Qc"
type: "love"
singers:
  - Sid Sriram
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yaaradiyo Azhagathan verporulai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaradiyo Azhagathan verporulai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulaginil nee piranthaai Saki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulaginil nee piranthaai Saki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paal mazhaiyin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paal mazhaiyin"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuliyadi nee nilavin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuliyadi nee nilavin"/>
</div>
<div class="lyrico-lyrics-wrapper">Nagaladi vaa enathul Saki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nagaladi vaa enathul Saki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaala neram paakkaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaala neram paakkaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Thikku yethum nokkaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thikku yethum nokkaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanmani yeppavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanmani yeppavum"/>
</div>
<div class="lyrico-lyrics-wrapper">Enthan kaigal korthu kondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enthan kaigal korthu kondu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee venum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennirunthu neengaathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennirunthu neengaathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennuyirum thaangathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennuyirum thaangathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhvatho theivatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhvatho theivatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum naanum sernthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum naanum sernthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhum vazhka pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhum vazhka pothum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaal nee Irul pizhai naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaal nee Irul pizhai naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaan nee Oru kodi naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaan nee Oru kodi naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaan nee Oru mugil naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaan nee Oru mugil naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennil servaaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennil servaaya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poi naan Meiyadi nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poi naan Meiyadi nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Kan naan Imaiyadi nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan naan Imaiyadi nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Udal than naan Uyiradi nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udal than naan Uyiradi nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennil servaaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennil servaaya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaaradiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaradiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagathan verporulai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagathan verporulai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulaginil nee piranthaai Saki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulaginil nee piranthaai Saki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pudhu vithamaai ninaivalaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu vithamaai ninaivalaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Unathuruvaai thinam varuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unathuruvaai thinam varuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pennae unnai kaanum munbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pennae unnai kaanum munbae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhkai enbathae veen endren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhkai enbathae veen endren"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai naanum kanda pinnae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai naanum kanda pinnae"/>
</div>
<div class="lyrico-lyrics-wrapper">Jenma mukthigal than konden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jenma mukthigal than konden"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ini vazhum ovvoru nimidam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini vazhum ovvoru nimidam"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkaga thudichidum idhayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkaga thudichidum idhayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasallithaadi pennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasallithaadi pennae"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum naanum sernthu vazhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum naanum sernthu vazhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhkka pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhkka pothum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaala neram paakkaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaala neram paakkaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Thikku yethum nokkaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thikku yethum nokkaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanmani eppavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanmani eppavum"/>
</div>
<div class="lyrico-lyrics-wrapper">Enthan kaigal korthu kondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enthan kaigal korthu kondu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee venum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennirunthu neengaathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennirunthu neengaathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennuyirum thaangathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennuyirum thaangathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhvatho theivatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhvatho theivatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum naanum sernthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum naanum sernthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhum vazhka pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhum vazhka pothum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaal nee Irul pizhai naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaal nee Irul pizhai naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaan nee Oru kodi naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaan nee Oru kodi naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaan nee Oru mugil naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaan nee Oru mugil naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennil servaaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennil servaaya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poi naan Meiyadi nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poi naan Meiyadi nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Kan naan Imaiyadi nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan naan Imaiyadi nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Udal than naan Uyiradi nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udal than naan Uyiradi nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennil servaaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennil servaaya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeyum naanum sernthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum naanum sernthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhum vazhka pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhum vazhka pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thum thum thum thum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thum thum thum thum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhkka pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhkka pothum"/>
</div>
</pre>
