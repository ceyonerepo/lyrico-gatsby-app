---
title: "thandaane thandaane song lyrics"
album: "Vinaya Vidheya Rama"
artist: "Devi Sri Prasad"
lyricist: "Sri Mani"
director: "Boyapati Srinu"
path: "/albums/vinaya-vidheya-rama-lyrics"
song: "Thandaane Thandaane"
image: ../../images/albumart/vinaya-vidheya-rama.jpg
date: 2019-01-11
lang: telugu
youtubeLink: "https://www.youtube.com/embed/GKrpi9NX6LY"
type: "happy"
singers:
  - M.L.R. Karthikeyan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Thandaane thandaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandaane thandaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Thandaane thandaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandaane thandaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Choosara ye chotaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choosara ye chotaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha aanandhaanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha aanandhaanne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thandaane thandaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandaane thandaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Thandaane thandaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandaane thandaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannaara yevaraina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaara yevaraina"/>
</div>
<div class="lyrico-lyrics-wrapper">Prathiroju pandagane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prathiroju pandagane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye thiyyadanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye thiyyadanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasupai raasindho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasupai raasindho"/>
</div>
<div class="lyrico-lyrics-wrapper">Yentho andhanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yentho andhanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee thala raathalane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee thala raathalane"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye chirunavvu runapaduthu geesindho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye chirunavvu runapaduthu geesindho"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanake roopanga ee bommalane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanake roopanga ee bommalane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oka chethiloni geethale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oka chethiloni geethale"/>
</div>
<div class="lyrico-lyrics-wrapper">Oka theeruga kalisundave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oka theeruga kalisundave"/>
</div>
<div class="lyrico-lyrics-wrapper">Oka veli mudhralo polike
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oka veli mudhralo polike"/>
</div>
<div class="lyrico-lyrics-wrapper">Maroka velilo kanipinchave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maroka velilo kanipinchave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yekkada puttina vaallo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yekkada puttina vaallo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye dhikkuna modhalainollo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye dhikkuna modhalainollo"/>
</div>
<div class="lyrico-lyrics-wrapper">Oka gundeku chappudu ayyarugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oka gundeku chappudu ayyarugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye ningina gaali pataalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye ningina gaali pataalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye thotana virisina poolo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye thotana virisina poolo"/>
</div>
<div class="lyrico-lyrics-wrapper">Oka vaakita okatai unnaarugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oka vaakita okatai unnaarugaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thandaane thandaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandaane thandaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Thandaane thandaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandaane thandaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Choosara ye chotaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choosara ye chotaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha aanandhaanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha aanandhaanne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thandaane thandaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandaane thandaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Thandaane thandaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandaane thandaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannaara yevaraina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaara yevaraina"/>
</div>
<div class="lyrico-lyrics-wrapper">Prathiroju pandagane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prathiroju pandagane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ee intlilona irukundadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee intlilona irukundadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Prathi manasulona chotundhile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prathi manasulona chotundhile"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee nadakakepudu alupundadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee nadakakepudu alupundadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Gelipinchu aduge thodundhile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gelipinchu aduge thodundhile"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidi vidigaa veellu padhaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidi vidigaa veellu padhaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Okatayyina vaakyamalle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okatayyina vaakyamalle"/>
</div>
<div class="lyrico-lyrics-wrapper">Oka thiyyati artham chepparugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oka thiyyati artham chepparugaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidividiga veellu swaraale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidividiga veellu swaraale"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalagalipina raagamalle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalagalipina raagamalle"/>
</div>
<div class="lyrico-lyrics-wrapper">Oka kammani paatai nilichaarugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oka kammani paatai nilichaarugaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thandaane thandaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandaane thandaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Thandaane thandaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandaane thandaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Choosara ye chotaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choosara ye chotaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha aanandhaanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha aanandhaanne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thandaane thandaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandaane thandaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Thandaane thandaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandaane thandaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannaara yevaraina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaara yevaraina"/>
</div>
<div class="lyrico-lyrics-wrapper">Prathiroju pandagane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prathiroju pandagane"/>
</div>
</pre>
