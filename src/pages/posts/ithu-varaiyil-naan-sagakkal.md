---
title: "ithu varaiyil naan song lyrics"
album: "Sagakkal"
artist: "Thayarathnam"
lyricist: "Daya Rathnam"
director: "L. Muthukumaraswamy"
path: "/albums/sagakkal-lyrics"
song: "Ithu Varaiyil Naan"
image: ../../images/albumart/sagakkal.jpg
date: 2011-08-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Fl8t_4k0r34"
type: "love"
singers:
  - S.P. Balasubrahmanyam
  - K.S. Chithra
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Idhuvaraiyil naan vaazhndha vaazhkkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhuvaraiyil naan vaazhndha vaazhkkai"/>
</div>
<div class="lyrico-lyrics-wrapper">kaatchi poandradho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatchi poandradho"/>
</div>
<div class="lyrico-lyrics-wrapper">oru nodiyil un vaasal koadu koodi vandhatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru nodiyil un vaasal koadu koodi vandhatho"/>
</div>
<div class="lyrico-lyrics-wrapper">yeno indru naan maarinen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yeno indru naan maarinen"/>
</div>
<div class="lyrico-lyrics-wrapper">ennoaduthaan pesinen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennoaduthaan pesinen"/>
</div>
<div class="lyrico-lyrics-wrapper">nee ingu en paadhiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee ingu en paadhiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">thaalaattum en annaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaalaattum en annaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">palagoadi koadi inbam thandhavaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="palagoadi koadi inbam thandhavaney"/>
</div>
<div class="lyrico-lyrics-wrapper">vittupoagumboadhu poagum en uyirey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vittupoagumboadhu poagum en uyirey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhuvaraiyil naan vaazhndha vaazhkkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhuvaraiyil naan vaazhndha vaazhkkai"/>
</div>
<div class="lyrico-lyrics-wrapper">kaatchi poandradho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatchi poandradho"/>
</div>
<div class="lyrico-lyrics-wrapper">oru nodiyil un vaasal koadu koodi vandhatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru nodiyil un vaasal koadu koodi vandhatho"/>
</div>
<div class="lyrico-lyrics-wrapper">yeno indru naan maarinen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yeno indru naan maarinen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Netruvaraikkum kaadhal enbadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netruvaraikkum kaadhal enbadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">thavarendru ninaitheney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thavarendru ninaitheney"/>
</div>
<div class="lyrico-lyrics-wrapper">vizhi vaasalvaraikkum vandha kanaavai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vizhi vaasalvaraikkum vandha kanaavai"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjikkul adaitheney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjikkul adaitheney"/>
</div>
<div class="lyrico-lyrics-wrapper">yaarendru ethaiyum paaraamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaarendru ethaiyum paaraamal"/>
</div>
<div class="lyrico-lyrics-wrapper">en thandhaiyin uyirkkaathaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en thandhaiyin uyirkkaathaai"/>
</div>
<div class="lyrico-lyrics-wrapper">indru enakkumkooda theriyaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indru enakkumkooda theriyaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">vandhu idhayathil karai eduthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vandhu idhayathil karai eduthaai"/>
</div>
<div class="lyrico-lyrics-wrapper">pala enna thadaigalai thaandi naan unnul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pala enna thadaigalai thaandi naan unnul"/>
</div>
<div class="lyrico-lyrics-wrapper">mudigira vaanam aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudigira vaanam aa"/>
</div>
<div class="lyrico-lyrics-wrapper">pala enna thadaigalai thaandi naan unnul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pala enna thadaigalai thaandi naan unnul"/>
</div>
<div class="lyrico-lyrics-wrapper">mudigira vaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudigira vaanam"/>
</div>
<div class="lyrico-lyrics-wrapper">nee illaamal sellaadhu vaazhum manithuli ovvondrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee illaamal sellaadhu vaazhum manithuli ovvondrum"/>
</div>
<div class="lyrico-lyrics-wrapper">palagoadi koadi inbam thandhavaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="palagoadi koadi inbam thandhavaney"/>
</div>
<div class="lyrico-lyrics-wrapper">vittupoagumboadhu poagum en uyirey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vittupoagumboadhu poagum en uyirey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhuvaraiyil naan vaazhndha vaazhkkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhuvaraiyil naan vaazhndha vaazhkkai"/>
</div>
<div class="lyrico-lyrics-wrapper">kaatchi poandradho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatchi poandradho"/>
</div>
<div class="lyrico-lyrics-wrapper">oru nodiyil un vaasal koadu koodi vandhatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru nodiyil un vaasal koadu koodi vandhatho"/>
</div>
<div class="lyrico-lyrics-wrapper">yeno indru naan maarinen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yeno indru naan maarinen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nanbargal pesum kaadhalaikkooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanbargal pesum kaadhalaikkooda"/>
</div>
<div class="lyrico-lyrics-wrapper">vedikkaiyaai ninaithen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vedikkaiyaai ninaithen"/>
</div>
<div class="lyrico-lyrics-wrapper">indru unvizhi jaadaiyin arthangal thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indru unvizhi jaadaiyin arthangal thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">ulagathai marandhuvitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulagathai marandhuvitten"/>
</div>
<div class="lyrico-lyrics-wrapper">saalakkum theruvukkum veesiya kaatrai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saalakkum theruvukkum veesiya kaatrai"/>
</div>
<div class="lyrico-lyrics-wrapper">moongilukkul adaithaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moongilukkul adaithaai"/>
</div>
<div class="lyrico-lyrics-wrapper">un sadugudu pechai ketpadharkaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un sadugudu pechai ketpadharkaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">marubadi pirakkavaithaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="marubadi pirakkavaithaai"/>
</div>
<div class="lyrico-lyrics-wrapper">vilaiyaadi thirindhavan ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vilaiyaadi thirindhavan ennai"/>
</div>
<div class="lyrico-lyrics-wrapper">kalavaadi poanaval neeyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalavaadi poanaval neeyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">vilaiyaadi thirindhavan ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vilaiyaadi thirindhavan ennai"/>
</div>
<div class="lyrico-lyrics-wrapper">kalavaadi poanaval neeyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalavaadi poanaval neeyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">ada eppoadhu enveedu varuvaai enbadhai solvaayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada eppoadhu enveedu varuvaai enbadhai solvaayaa"/>
</div>
<div class="lyrico-lyrics-wrapper">palagoadi koadi inbam thandhavaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="palagoadi koadi inbam thandhavaney"/>
</div>
<div class="lyrico-lyrics-wrapper">vittupoagumboadhu poagum en uyirey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vittupoagumboadhu poagum en uyirey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhuvaraiyil naan vaazhndha vaazhkkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhuvaraiyil naan vaazhndha vaazhkkai"/>
</div>
<div class="lyrico-lyrics-wrapper">kaatchi poandradho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatchi poandradho"/>
</div>
<div class="lyrico-lyrics-wrapper">oru nodiyil un vaasal koadu koodi vandhatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru nodiyil un vaasal koadu koodi vandhatho"/>
</div>
<div class="lyrico-lyrics-wrapper">yeno indru naan maarinen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yeno indru naan maarinen"/>
</div>
</pre>
