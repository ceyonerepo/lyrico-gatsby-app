---
title: "ulagil yentha kadhal song lyrics"
album: "Nadodigal"
artist: "Sundar C Babu"
lyricist: "Kavignar Vaalee"
director: "Samuthrakani"
path: "/albums/nadodigal-lyrics"
song: "Ulagil Yentha Kadhal"
image: ../../images/albumart/nadodigal.jpg
date: 2009-06-26
lang: tamil
youtubeLink: "https://www.youtube.com/embed/_Rdck07saTA"
type: "Sad"
singers:
  - Hariharan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ulagil Yentha Kathal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagil Yentha Kathal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Udane Jeithadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udane Jeithadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Valigal Thaangum Kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valigal Thaangum Kaadhal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Migavum Valiyadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Migavum Valiyadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhal Thotrathaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Thotrathaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kathaigal Yedhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathaigal Yedhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thotraal Thotradhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thotraal Thotradhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal Aagaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Aagaadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ellaame Sandharppam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaame Sandharppam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karppikkum Thappartham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karppikkum Thappartham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ulagil Endha Kadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagil Endha Kadhal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Udane Jeiththadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udane Jeiththadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Valigal Thaangum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valigal Thaangum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhal Migavum Valiyadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Migavum Valiyadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninaivugalaale Nichayathaartham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaivugalaale Nichayathaartham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nadandhadhu Avanodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadandhadhu Avanodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avanai Allaadhu Aduthavan Maalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avanai Allaadhu Aduthavan Maalai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yerppadhu Perumpaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yerppadhu Perumpaadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oruppuram Thalaivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oruppuram Thalaivan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maruppuram Thagappan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maruppuram Thagappan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irukkolli Erumbaanaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irukkolli Erumbaanaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paasaththukkaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paasaththukkaaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhalai Tholaithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalai Tholaithu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aalaiyil Karumbaanaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalaiyil Karumbaanaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaar Kaaranam Yaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar Kaaranam Yaar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaar Paavam Yaaraicherum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar Paavam Yaaraicherum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaarthaan Solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarthaan Solla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanneer Vaarthaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneer Vaarthaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanneer Aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneer Aanen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sutram Seidha Kutram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sutram Seidha Kutram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyiril Pookkum Kaathal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyiril Pookkum Kaathal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unarvin Aazhnilai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unarvin Aazhnilai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unarvai Paarppadhedhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unarvai Paarppadhedhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uravin Soozhnilai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uravin Soozhnilai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manam Ennum Kulaththil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam Ennum Kulaththil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhiyennum Kallai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhiyennum Kallai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mudhan Mudhal Erindhaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhan Mudhal Erindhaalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alai Alaiyaaga Aasaigal Ezhumba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alai Alaiyaaga Aasaigal Ezhumba"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aval Vasam Vizhundhaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aval Vasam Vizhundhaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nadhi Vazhipponaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadhi Vazhipponaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karaivarakkoodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karaivarakkoodum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidhi Vazhipponaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidhi Vazhipponaanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidhai Ondru Poda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidhai Ondru Poda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Verondru Mulaiththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verondru Mulaiththa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhai Endru Aanaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhai Endru Aanaanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Solvadhu En Solvadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Solvadhu En Solvadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaan Konda Natpukkaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaan Konda Natpukkaaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaane Thaeindhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaane Thaeindhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karpai Pole Natpai Kaaththaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karpai Pole Natpai Kaaththaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal Thorkum Endraa Paarthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Thorkum Endraa Paarthaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ulagil Yentha Kathal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagil Yentha Kathal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Udane Jeithadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udane Jeithadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Valigal Thaangum Kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valigal Thaangum Kaadhal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Migavum Valiyadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Migavum Valiyadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhal Thotrathaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Thotrathaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kathaigal Yedhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathaigal Yedhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thotraal Thotradhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thotraal Thotradhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal Aagaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Aagaadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ellaame Sandharppam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaame Sandharppam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karppikkum Thappartham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karppikkum Thappartham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ulagil Endha Kadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagil Endha Kadhal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Udane Jeiththadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udane Jeiththadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Valigal Thaangum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valigal Thaangum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhal Migavum Valiyadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Migavum Valiyadhu"/>
</div>
</pre>
