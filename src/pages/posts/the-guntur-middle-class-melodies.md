---
title: "the guntur song lyrics"
album: "Middle Class Melodies"
artist: "Sweekar Agasthi"
lyricist: "Kittu Vissapragada"
director: "Vinod Anantoju"
path: "/albums/middle-class-melodies-lyrics"
song: "The Guntur"
image: ../../images/albumart/middle-class-melodies.jpg
date: 2020-11-20
lang: telugu
youtubeLink: "https://www.youtube.com/embed/6fbLyVaPstY"
type: "happy"
singers:
  - Anurag Kulkarni
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Thellaare ooranthaa thayyaare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thellaare ooranthaa thayyaare"/>
</div>
<div class="lyrico-lyrics-wrapper">Musthaabai pilichindhi guntur ye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Musthaabai pilichindhi guntur ye"/>
</div>
<div class="lyrico-lyrics-wrapper">Raddheelo yuddhaale modhalaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raddheelo yuddhaale modhalaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaggedhe ledhante prathivaade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaggedhe ledhante prathivaade"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Marupe raani oore guntur ye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marupe raani oore guntur ye"/>
</div>
<div class="lyrico-lyrics-wrapper">Alupantu ledhante sooride
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alupantu ledhante sooride"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagalanthaa thadisele sokkale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagalanthaa thadisele sokkale"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennenno saradhaale koluvunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennenno saradhaale koluvunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaaraale nooredhi antaare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaraale nooredhi antaare"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dialougues
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dialougues"/>
</div>
<div class="lyrico-lyrics-wrapper">Rey rey wrong route bey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rey rey wrong route bey"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhi guntur ra idhi anthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhi guntur ra idhi anthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Ikkada babai le wrong route lo yeltharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ikkada babai le wrong route lo yeltharu"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhi yentha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhi yentha"/>
</div>
<div class="lyrico-lyrics-wrapper">Niyamma guntur lo manushulakante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niyamma guntur lo manushulakante"/>
</div>
<div class="lyrico-lyrics-wrapper">Auto le ekkuva unnai ra babu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Auto le ekkuva unnai ra babu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Beram saaram saage dhaarullona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Beram saaram saage dhaarullona"/>
</div>
<div class="lyrico-lyrics-wrapper">Noroorinche mirchi bajji thagile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Noroorinche mirchi bajji thagile"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaaram nunchi saare seerala dhaaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaaram nunchi saare seerala dhaaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaalam esi patnam bazaaru pilise
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaalam esi patnam bazaaru pilise"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye pulihora dosa brodipeta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye pulihora dosa brodipeta"/>
</div>
<div class="lyrico-lyrics-wrapper">Biryanikaithe subhani maama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Biryanikaithe subhani maama"/>
</div>
<div class="lyrico-lyrics-wrapper">Vankaaya bajji aaro line
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vankaaya bajji aaro line"/>
</div>
<div class="lyrico-lyrics-wrapper">Gongura chicken brindavanamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gongura chicken brindavanamu"/>
</div>
<div class="lyrico-lyrics-wrapper">Masaala muntha sangadi gunta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Masaala muntha sangadi gunta"/>
</div>
<div class="lyrico-lyrics-wrapper">Maal poori kothapeta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maal poori kothapeta"/>
</div>
<div class="lyrico-lyrics-wrapper">Chitti idly lakshmipuramu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chitti idly lakshmipuramu"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey chekka pakodi moodonthenalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey chekka pakodi moodonthenalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gutake padaka kadupe thidithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gutake padaka kadupe thidithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Sabjaa ginjala soda bussandhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sabjaa ginjala soda bussandhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Podikaaram neyyesi peduthunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podikaaram neyyesi peduthunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Poga choore dhaarullo noroore
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poga choore dhaarullo noroore"/>
</div>
<div class="lyrico-lyrics-wrapper">Adigindhe thaduvantaa edhainaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adigindhe thaduvantaa edhainaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ledhannaa maatantu raadhantaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ledhannaa maatantu raadhantaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarada padithe podhaam guntur ye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarada padithe podhaam guntur ye"/>
</div>
</pre>
