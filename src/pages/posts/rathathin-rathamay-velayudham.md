---
title: "rathathin rathamay song lyrics"
album: "Velayudham"
artist: "Vijay Antony"
lyricist: "Annamalai"
director: "M. Raja"
path: "/albums/velayudham-lyrics"
song: "Rathathin Rathamay"
image: ../../images/albumart/velayudham.jpg
date: 2011-10-26
lang: tamil
youtubeLink: "https://www.youtube.com/embed/6ISXAshOje0"
type: "affection"
singers:
  - Haricharan
  - Srimathumitha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Rathathin Rathamae En Iniya Udan Pirapae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rathathin Rathamae En Iniya Udan Pirapae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sonthathin Sonthamae Naan Iyangum Uyri Thudipae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sonthathin Sonthamae Naan Iyangum Uyri Thudipae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ammavum Appavum Ellamae Neethaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammavum Appavum Ellamae Neethaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">En Vaazhkai Unnakallavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Vaazhkai Unnakallavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sethaalum Puthaithaalum Sediyaaga Mulaithaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sethaalum Puthaithaalum Sediyaaga Mulaithaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">En Vaasam Unnakallavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Vaasam Unnakallavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rathathin Rathamae En Iniya Udan Pirapae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rathathin Rathamae En Iniya Udan Pirapae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbendra Otrai Sollai Pol Ondru Veru Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbendra Otrai Sollai Pol Ondru Veru Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Kaatum Paasathuku Deivangal Eedu Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kaatum Paasathuku Deivangal Eedu Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">En Nenjam Unnai Matum Kadikara Mullaai Sutrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nenjam Unnai Matum Kadikara Mullaai Sutrum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nodi Neram Nee Pirinthaal Ammadi Uyirae Pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nodi Neram Nee Pirinthaal Ammadi Uyirae Pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Sonnaal Ethayo Seiven Thalai Aatum Bommai Aaven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Sonnaal Ethayo Seiven Thalai Aatum Bommai Aaven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sethaalum Puthaithaalum Sediyaaga Mulaithaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sethaalum Puthaithaalum Sediyaaga Mulaithaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">En Vaasam Unnakallavaa Ah Ah Oh Hoo Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Vaasam Unnakallavaa Ah Ah Oh Hoo Oh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tajmahal Unaku Thangathil Katta Poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tajmahal Unaku Thangathil Katta Poren"/>
</div>
<div class="lyrico-lyrics-wrapper">Megathil Nool Eduthu Selaithaan Nenju Thaaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megathil Nool Eduthu Selaithaan Nenju Thaaren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennodu Nee Irunthaal Verethum Eedagumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennodu Nee Irunthaal Verethum Eedagumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kandaangi Sela Pothum Verethum Naan Kepena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandaangi Sela Pothum Verethum Naan Kepena"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanathil Neelam Polae Boomikul Eeram Polae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanathil Neelam Polae Boomikul Eeram Polae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irutaalum Eriyathu Mudinthaalum Mudiyaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irutaalum Eriyathu Mudinthaalum Mudiyaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naam Konda Uravallavaa Ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam Konda Uravallavaa Ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aah Aa Oh Hoo Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aah Aa Oh Hoo Oh"/>
</div>
</pre>
