---
title: "ore oar ooril song lyrics"
album: "Baahubali 2"
artist: "M.M. Keeravani"
lyricist: "Madhan Karky"
director: "S.S. Rajamouli"
path: "/albums/baahubali-2-song-lyrics"
song: "ore oar ooril"
image: ../../images/albumart/baahubali-2.jpg
date: 2017-04-09
lang: tamil
youtubeLink: "https://www.youtube.com/embed/-PFTaQJhLTY"
type: "Love"
singers:
  - Mohana
  - Deepu
---

<pre class="lyrics-native">
  </pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">orae oar ooril 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="orae oar ooril "/>
</div>
<div class="lyrico-lyrics-wrapper">orae oar raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="orae oar raja"/>
</div>
<div class="lyrico-lyrics-wrapper">orae oar ooril 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="orae oar ooril "/>
</div>
<div class="lyrico-lyrics-wrapper">orae oar raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="orae oar raja"/>
</div>
<div class="lyrico-lyrics-wrapper">en kaathil kaathal solluvaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kaathil kaathal solluvaana"/>
</div>
<div class="lyrico-lyrics-wrapper">orae oar aattril
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="orae oar aattril"/>
</div>
<div class="lyrico-lyrics-wrapper">orae oar oodam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="orae oar oodam"/>
</div>
<div class="lyrico-lyrics-wrapper">thalladum ennai thanguvaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalladum ennai thanguvaanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vaa endru kattalai itaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaa endru kattalai itaana"/>
</div>
<div class="lyrico-lyrics-wrapper">muthathil kai vilangittaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muthathil kai vilangittaana"/>
</div>
<div class="lyrico-lyrics-wrapper">kaithaaginaal deva senaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaithaaginaal deva senaa"/>
</div>
<div class="lyrico-lyrics-wrapper">than porkalamaai en maarbil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="than porkalamaai en maarbil"/>
</div>
<div class="lyrico-lyrics-wrapper">yaeri poridum mei veeranaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaeri poridum mei veeranaa"/>
</div>
<div class="lyrico-lyrics-wrapper">enthan kodiyai meleri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enthan kodiyai meleri"/>
</div>
<div class="lyrico-lyrics-wrapper">naattavaa mohana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naattavaa mohana"/>
</div>
<div class="lyrico-lyrics-wrapper">vaalin munaiyil engengo mutham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalin munaiyil engengo mutham"/>
</div>
<div class="lyrico-lyrics-wrapper">vaithidum arakkanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaithidum arakkanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">vaayin munaiyil maayangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaayin munaiyil maayangal"/>
</div>
<div class="lyrico-lyrics-wrapper">laattavaa kaaminaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="laattavaa kaaminaa"/>
</div>
<div class="lyrico-lyrics-wrapper">oho yaegaantha kaalam maatrinaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oho yaegaantha kaalam maatrinaanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">thee pol ne meethu pattrinaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thee pol ne meethu pattrinaanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">thee kolamaai devasena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thee kolamaai devasena"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">orae oar ooril
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="orae oar ooril"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">orae oar ooril 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="orae oar ooril "/>
</div>
<div class="lyrico-lyrics-wrapper">orae oar raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="orae oar raja"/>
</div>
<div class="lyrico-lyrics-wrapper">en kaathil kaathal solluvaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kaathil kaathal solluvaana"/>
</div>
<div class="lyrico-lyrics-wrapper">orae oar aatril 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="orae oar aatril "/>
</div>
<div class="lyrico-lyrics-wrapper">orae oar oadam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="orae oar oadam"/>
</div>
<div class="lyrico-lyrics-wrapper">thalladum ennai thanguvaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalladum ennai thanguvaana"/>
</div>
<div class="lyrico-lyrics-wrapper">en nenjil ambu eigiraanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en nenjil ambu eigiraanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">kann indri naanum selgirenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kann indri naanum selgirenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">pachindriye deva sena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pachindriye deva sena"/>
</div>
</pre>
