---
title: "endhuku song lyrics"
album: "lakshmis ntr"
artist: "Kalyani Malik"
lyricist: "Sira Sri"
director: "Ram Gopal Varma - Agasthya Manju"
path: "/albums/lakshmi's-ntr-lyrics"
song: "Endhuku"
image: ../../images/albumart/lakshmis-ntr.jpg
date: 2019-03-29
lang: telugu
youtubeLink: "https://www.youtube.com/embed/oL3qA7A3Nbo"
type: "mass"
singers:
  - Kalyani Malik
  - Sri Krishna
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ntr Ntr Ntr
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ntr Ntr Ntr"/>
</div>
<div class="lyrico-lyrics-wrapper">Jayasudha Jayapradha Sridevi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jayasudha Jayapradha Sridevi"/>
</div>
<div class="lyrico-lyrics-wrapper">Krishna Kumari Savithri Anjali Devi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Krishna Kumari Savithri Anjali Devi"/>
</div>
<div class="lyrico-lyrics-wrapper">Veellandharini Vadhili Aa Lakshmi Parvathini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veellandharini Vadhili Aa Lakshmi Parvathini"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhuku Endhuku Endhuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhuku Endhuku Endhuku"/>
</div>
<div class="lyrico-lyrics-wrapper">Ntr Ntr
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ntr Ntr"/>
</div>
<div class="lyrico-lyrics-wrapper">Cbn Nbk Daggupati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cbn Nbk Daggupati"/>
</div>
<div class="lyrico-lyrics-wrapper">Hari Krishna Chinnamma Bhuvaneshwari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hari Krishna Chinnamma Bhuvaneshwari"/>
</div>
<div class="lyrico-lyrics-wrapper">Veellandharini Kaadhani Lakshmi Parvathi Ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veellandharini Kaadhani Lakshmi Parvathi Ni"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhuku Endhuku Endhuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhuku Endhuku Endhuku"/>
</div>
<div class="lyrico-lyrics-wrapper">Ntr Ntr
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ntr Ntr"/>
</div>
<div class="lyrico-lyrics-wrapper">Major Chandrakanth 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Major Chandrakanth "/>
</div>
<div class="lyrico-lyrics-wrapper">Vijayotsava Vedhika Midhaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vijayotsava Vedhika Midhaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Aamenendhuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aamenendhuku"/>
</div>
<div class="lyrico-lyrics-wrapper">Viceroy Godava Aamevalle Jariginappudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viceroy Godava Aamevalle Jariginappudu"/>
</div>
<div class="lyrico-lyrics-wrapper">Akkadiki Kuda Aamenendhuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Akkadiki Kuda Aamenendhuku"/>
</div>
<div class="lyrico-lyrics-wrapper">Aameki Mundhe Pellei Unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aameki Mundhe Pellei Unna"/>
</div>
<div class="lyrico-lyrics-wrapper">Yendhuguthunna Koduku Unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yendhuguthunna Koduku Unna"/>
</div>
<div class="lyrico-lyrics-wrapper">Bharya Ga Aamenendhuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bharya Ga Aamenendhuku"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodukulu Kodallu Allullu Kuthullu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodukulu Kodallu Allullu Kuthullu"/>
</div>
<div class="lyrico-lyrics-wrapper">Voddhu Voddhanna Aamenendhuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Voddhu Voddhanna Aamenendhuku"/>
</div>
<div class="lyrico-lyrics-wrapper">Ntr Ntr
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ntr Ntr"/>
</div>
<div class="lyrico-lyrics-wrapper">Ramudantu Intinta Kolichinanaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ramudantu Intinta Kolichinanaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Krishnudantu Veynolla Pogidinaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Krishnudantu Veynolla Pogidinaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Keerthi Aame Valla Pothundhani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Keerthi Aame Valla Pothundhani"/>
</div>
<div class="lyrico-lyrics-wrapper">Telsina Aamenendhuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Telsina Aamenendhuku"/>
</div>
<div class="lyrico-lyrics-wrapper">Okka Aamenendhuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okka Aamenendhuku"/>
</div>
<div class="lyrico-lyrics-wrapper">Venakana Lakshaladhi Pasupu Sainyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venakana Lakshaladhi Pasupu Sainyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Mundhu Chusthe Kotla Janala Samooham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mundhu Chusthe Kotla Janala Samooham"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaallandharini Vadhili Lakshmi Parvathi Ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaallandharini Vadhili Lakshmi Parvathi Ni"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhuku Endhuku Endhuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhuku Endhuku Endhuku"/>
</div>
<div class="lyrico-lyrics-wrapper">Maatakunna Viluvani Baatakunna Charithani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maatakunna Viluvani Baatakunna Charithani"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiruguleni Shakthi Ni Raajakeeya Yukthini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiruguleni Shakthi Ni Raajakeeya Yukthini"/>
</div>
<div class="lyrico-lyrics-wrapper">Annitini Thulabharamlo Odinchindhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annitini Thulabharamlo Odinchindhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aame Mahatyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aame Mahatyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Paruvu Oka Vaipu Laksmi Parvathi Oka Vaipu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paruvu Oka Vaipu Laksmi Parvathi Oka Vaipu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarithoochamante Manasu Origindha Aame Vaipu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarithoochamante Manasu Origindha Aame Vaipu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oka Saadha Seedha Vanitha Kosam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oka Saadha Seedha Vanitha Kosam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarvasam Vadhilina Theguva Kadha Idhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarvasam Vadhilina Theguva Kadha Idhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nidana Bhathike Aadadhani Kosam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nidana Bhathike Aadadhani Kosam"/>
</div>
<div class="lyrico-lyrics-wrapper">Raaje Rajyamunodhilina Charitha Idhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raaje Rajyamunodhilina Charitha Idhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhi Charithaku Grahanam Pattina Prema
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhi Charithaku Grahanam Pattina Prema"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhi Munine Moorkuni Chesina Prema
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhi Munine Moorkuni Chesina Prema"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhi Buddhuni Gaddi Thinipinchina Prema
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhi Buddhuni Gaddi Thinipinchina Prema"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rgv Dialogue
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rgv Dialogue"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Paataloni Prashnala Venuka Abaddhaluga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Paataloni Prashnala Venuka Abaddhaluga "/>
</div>
<div class="lyrico-lyrics-wrapper">Chalamani Avthunna Nijalani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chalamani Avthunna Nijalani"/>
</div>
<div class="lyrico-lyrics-wrapper">Nijamga Masipoosukunna Abaddhalani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijamga Masipoosukunna Abaddhalani"/>
</div>
<div class="lyrico-lyrics-wrapper">Bandakesi Vuthiki Areyyatame 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bandakesi Vuthiki Areyyatame "/>
</div>
<div class="lyrico-lyrics-wrapper">Lakshmi’S Ntr Dhyeyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lakshmi’S Ntr Dhyeyam"/>
</div>
20 <div class="lyrico-lyrics-wrapper">Samvatsaralaki Paiga Nijaniki 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samvatsaralaki Paiga Nijaniki "/>
</div>
<div class="lyrico-lyrics-wrapper">Abaddhamane Battalu Thodigi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Abaddhamane Battalu Thodigi "/>
</div>
<div class="lyrico-lyrics-wrapper">Veeddhulenta Thipputhunna Vennupotu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veeddhulenta Thipputhunna Vennupotu "/>
</div>
<div class="lyrico-lyrics-wrapper">Dharlandharni Prajalandhari Mundhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dharlandharni Prajalandhari Mundhu "/>
</div>
<div class="lyrico-lyrics-wrapper">Battalu Chimpi Avathala Paresi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Battalu Chimpi Avathala Paresi "/>
</div>
<div class="lyrico-lyrics-wrapper">Nijam Battalni Okkokkatiga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijam Battalni Okkokkatiga "/>
</div>
<div class="lyrico-lyrics-wrapper">Mellaga Vippi Dhanni Malli 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mellaga Vippi Dhanni Malli "/>
</div>
<div class="lyrico-lyrics-wrapper">Poorthi Nagnam Ga Chupinchatame 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poorthi Nagnam Ga Chupinchatame "/>
</div>
<div class="lyrico-lyrics-wrapper">Lakshmi’S Ntr Uddhesham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lakshmi’S Ntr Uddhesham"/>
</div>
</pre>
