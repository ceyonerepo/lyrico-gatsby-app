---
title: "jil jil singari song lyrics"
album: "Koottali"
artist: "Britto Michael"
lyricist: "Viveka"
director: "SK Mathi"
path: "/albums/koottali-song-lyrics"
song: "Jil Jil Singari"
image: ../../images/albumart/koottali.jpg
date: 2018-02-22
lang: tamil
youtubeLink: "https://www.youtube.com/embed/mCs7et7B2Sc"
type: "happy"
singers:
  - Ananya
  - Roshine
  - Shakthi
  - Britto
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">jil jil jiluku singari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jil jil jiluku singari"/>
</div>
<div class="lyrico-lyrics-wrapper">nan pala palakum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan pala palakum"/>
</div>
<div class="lyrico-lyrics-wrapper">sevatha oyari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sevatha oyari"/>
</div>
<div class="lyrico-lyrics-wrapper">jil jil jiluku singari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jil jil jiluku singari"/>
</div>
<div class="lyrico-lyrics-wrapper">nan pala palakum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan pala palakum"/>
</div>
<div class="lyrico-lyrics-wrapper">sevatha oyari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sevatha oyari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">antha vannara petaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antha vannara petaya"/>
</div>
<div class="lyrico-lyrics-wrapper">unnoda kootayum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnoda kootayum "/>
</div>
<div class="lyrico-lyrics-wrapper">kannala katti ilupen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannala katti ilupen"/>
</div>
<div class="lyrico-lyrics-wrapper">oh un joru aasaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oh un joru aasaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">muneru meesaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muneru meesaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">thimurala nattu morapen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thimurala nattu morapen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oh varatha puyala pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oh varatha puyala pola"/>
</div>
<div class="lyrico-lyrics-wrapper">iva paraka viduva aala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iva paraka viduva aala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vendai vennai vada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vendai vennai vada"/>
</div>
<div class="lyrico-lyrics-wrapper">nan poothi vacha soda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan poothi vacha soda"/>
</div>
<div class="lyrico-lyrics-wrapper">thagatha thalli podatha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thagatha thalli podatha "/>
</div>
<div class="lyrico-lyrics-wrapper">toto very heart to
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="toto very heart to"/>
</div>
<div class="lyrico-lyrics-wrapper">mothal ellam late
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mothal ellam late"/>
</div>
<div class="lyrico-lyrics-wrapper">mohatha moodi vaikatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mohatha moodi vaikatha"/>
</div>
<div class="lyrico-lyrics-wrapper">harmona thoondi viduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="harmona thoondi viduven"/>
</div>
<div class="lyrico-lyrics-wrapper">ara loosa mathi viduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ara loosa mathi viduven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">un pesa kangalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un pesa kangalum"/>
</div>
<div class="lyrico-lyrics-wrapper">nasa vingalum lasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nasa vingalum lasa"/>
</div>
<div class="lyrico-lyrics-wrapper">pathale nanum gaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathale nanum gaali"/>
</div>
<div class="lyrico-lyrics-wrapper">yei loosa mathum rosa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yei loosa mathum rosa"/>
</div>
<div class="lyrico-lyrics-wrapper">nee than kooda aadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee than kooda aadu"/>
</div>
<div class="lyrico-lyrics-wrapper">naan koottali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan koottali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nan luck illa kayal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan luck illa kayal"/>
</div>
<div class="lyrico-lyrics-wrapper">nan 24 carrot kutchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan 24 carrot kutchi"/>
</div>
<div class="lyrico-lyrics-wrapper">idai koovi alaikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idai koovi alaikum"/>
</div>
<div class="lyrico-lyrics-wrapper">echil illa mooju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="echil illa mooju"/>
</div>
<div class="lyrico-lyrics-wrapper">weight 50 kg pathale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="weight 50 kg pathale"/>
</div>
<div class="lyrico-lyrics-wrapper">pulb eriyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pulb eriyum"/>
</div>
<div class="lyrico-lyrics-wrapper">tamil nade echi milungum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tamil nade echi milungum"/>
</div>
<div class="lyrico-lyrics-wrapper">en peril katchi thodangum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en peril katchi thodangum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">jil jil jiluku singari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jil jil jiluku singari"/>
</div>
<div class="lyrico-lyrics-wrapper">nan pala palakum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan pala palakum"/>
</div>
<div class="lyrico-lyrics-wrapper">sevatha oyari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sevatha oyari"/>
</div>
<div class="lyrico-lyrics-wrapper">jil jil jiluku singari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jil jil jiluku singari"/>
</div>
<div class="lyrico-lyrics-wrapper">nan pala palakum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan pala palakum"/>
</div>
<div class="lyrico-lyrics-wrapper">sevatha oyari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sevatha oyari"/>
</div>
</pre>
