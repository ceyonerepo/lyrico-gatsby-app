---
title: "mudhal murai song lyrics"
album: "Avan Ivan"
artist: "Yuvan Shankar Raja"
lyricist: "Na. Muthukumar"
director: "Bala"
path: "/albums/avan-ivan-lyrics"
song: "Mudhal Murai"
image: ../../images/albumart/avan-ivan.jpg
date: 2011-06-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Q4szq4xk-OU"
type: "sad"
singers:
  - Vijay Prakash
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Muthal Murai En Vaazhvil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthal Murai En Vaazhvil "/>
</div>
<div class="lyrico-lyrics-wrapper">Maranaththai Paarkiraen 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maranaththai Paarkiraen "/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavudan Soothaadi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavudan Soothaadi "/>
</div>
<div class="lyrico-lyrics-wrapper">Kadaisiyil Thoarkiraen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadaisiyil Thoarkiraen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pazhadaintha Veedaaga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pazhadaintha Veedaaga "/>
</div>
<div class="lyrico-lyrics-wrapper">Puzhuthiyil Vazhukiraen 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puzhuthiyil Vazhukiraen "/>
</div>
<div class="lyrico-lyrics-wrapper">Paavi Intha Vithiyalae 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paavi Intha Vithiyalae "/>
</div>
<div class="lyrico-lyrics-wrapper">Azhuthingu Saakiraen 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhuthingu Saakiraen "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vittu Vittu Po Endru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vittu Vittu Po Endru "/>
</div>
<div class="lyrico-lyrics-wrapper">Vaethanaigal Solluthey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaethanaigal Solluthey "/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthu Vidu Vaa Endru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthu Vidu Vaa Endru "/>
</div>
<div class="lyrico-lyrics-wrapper">Nyabagangal Kolluthey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nyabagangal Kolluthey "/>
</div>
<div class="lyrico-lyrics-wrapper">Kannavillai Ninjam Endru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannavillai Ninjam Endru "/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Killi Paarkiraen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Killi Paarkiraen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engu Ini Naan Poaga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engu Ini Naan Poaga "/>
</div>
<div class="lyrico-lyrics-wrapper">Paathaiyinai Marakkiraen 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paathaiyinai Marakkiraen "/>
</div>
<div class="lyrico-lyrics-wrapper">Indru Vantha Pinnaallum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indru Vantha Pinnaallum "/>
</div>
<div class="lyrico-lyrics-wrapper">Netru Sendru Midhakkiraen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netru Sendru Midhakkiraen"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathal Ennum Naadagaththai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathal Ennum Naadagaththai "/>
</div>
<div class="lyrico-lyrics-wrapper">Kanda Pinbu Azhugiraen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanda Pinbu Azhugiraen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muthal Murai En Vaazhvil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthal Murai En Vaazhvil "/>
</div>
<div class="lyrico-lyrics-wrapper">Maranaththai Paarkiraen 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maranaththai Paarkiraen "/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavudan Soothaadi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavudan Soothaadi "/>
</div>
<div class="lyrico-lyrics-wrapper">Kadaisiyil Thoarkiraen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadaisiyil Thoarkiraen"/>
</div>
</pre>
