---
title: "aadyamorilam song lyrics"
album: "Varane Avashyamund"
artist: "Alphons Joseph"
lyricist: "Santhosh Varma"
director: "Anoop Sathyan"
path: "/albums/varane-avashyamund-lyrics"
song: "Aadyamorilam"
image: ../../images/albumart/varane-avashyamund.jpg
date: 2020-02-07
lang: malayalam
youtubeLink: "https://www.youtube.com/embed/TnQ7G54_66E"
type: "melody"
singers:
  - Ann Amie
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aadyamorillam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadyamorillam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalodalaayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalodalaayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kunju Nerukil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kunju Nerukil"/>
</div>
<div class="lyrico-lyrics-wrapper">Pathinju Njaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathinju Njaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Manju Raavil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manju Raavil"/>
</div>
<div class="lyrico-lyrics-wrapper">Viriyaay Ninte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viriyaay Ninte"/>
</div>
<div class="lyrico-lyrics-wrapper">Mey Pothinjathu Njan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mey Pothinjathu Njan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Innolam Kaavalirunnu Njan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innolam Kaavalirunnu Njan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nizhalaay Nin Chaare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nizhalaay Nin Chaare"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyariyaathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyariyaathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaathorthaal Nee Kelkk
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathorthaal Nee Kelkk"/>
</div>
<div class="lyrico-lyrics-wrapper">Shwasangal Ninnamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shwasangal Ninnamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Thedum Ammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Thedum Ammaa"/>
</div>
</pre>
