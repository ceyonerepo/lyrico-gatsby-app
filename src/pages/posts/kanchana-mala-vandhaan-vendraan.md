---
title: "kanchana mala song lyrics"
album: "Vandhaan Vendraan"
artist: "Thaman"
lyricist: "Thamarai"
director: "R. Kannan"
path: "/albums/vandhaan-vendraan-lyrics"
song: "Kanchana Mala"
image: ../../images/albumart/vandhaan-vendraan.jpg
date: 2011-09-16
lang: tamil
youtubeLink: "https://www.youtube.com/embed/koDshbx58yU"
type: "love"
singers:
  - Karthik
  - Naveen Madhav 
  - Priya Himesh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Mayil thogai ondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayil thogai ondru"/>
</div>
<div class="lyrico-lyrics-wrapper">Madiyil vanthu sainthukolla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madiyil vanthu sainthukolla"/>
</div>
<div class="lyrico-lyrics-wrapper">Manapadam seitha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manapadam seitha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaarathai ellam thondai killa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaarathai ellam thondai killa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nodi neram naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nodi neram naanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai vittu thalli sella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai vittu thalli sella"/>
</div>
<div class="lyrico-lyrics-wrapper">Sella sella sella sella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sella sella sella sella"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanchana maala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanchana maala"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanchana maala kanchana maala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanchana maala kanchana maala"/>
</div>
<div class="lyrico-lyrics-wrapper">Kollaamal kollum kanenna velaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kollaamal kollum kanenna velaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Malayaala manmelae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malayaala manmelae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kaalgal nadakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kaalgal nadakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Aarezhu pandhaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aarezhu pandhaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">En nejam kudhikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En nejam kudhikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanchana maala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanchana maala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbae en ullangalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbae en ullangalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Minsarangal oduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minsarangal oduthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Urchagam vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urchagam vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Uchanthalai yerudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uchanthalai yerudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maaligai polae veedugal katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaligai polae veedugal katti"/>
</div>
<div class="lyrico-lyrics-wrapper">Margali naalil naan tharavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Margali naalil naan tharavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ovvoru naalum ovvoru veetil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ovvoru naalum ovvoru veetil"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai naanum thaedatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai naanum thaedatta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Minnale minnalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minnale minnalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sanjalam konda kangalai moodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sanjalam konda kangalai moodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Chandran kaana kaathirundhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chandran kaana kaathirundhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ne varavillai nee varavillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne varavillai nee varavillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidinjae pochae en seiven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidinjae pochae en seiven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanchana maala kanchana maala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanchana maala kanchana maala"/>
</div>
<div class="lyrico-lyrics-wrapper">Kollaamal kollum kanenna velaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kollaamal kollum kanenna velaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanchana maala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanchana maala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pogum thooram enna solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogum thooram enna solla"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanam vaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam vaanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum varen konjam nillu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum varen konjam nillu"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethaan megam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethaan megam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee thaeda sollum kaada naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thaeda sollum kaada naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaedi paathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaedi paathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ne thoonga seiyum vedanaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne thoonga seiyum vedanaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbae en ullangalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbae en ullangalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Minsarangal oduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minsarangal oduthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey urchagam vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey urchagam vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Uchanthalai yerudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uchanthalai yerudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaligai polae veedugal katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaligai polae veedugal katti"/>
</div>
<div class="lyrico-lyrics-wrapper">Margali naalil naan tharavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Margali naalil naan tharavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ovvoru naalum ovvoru veetil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ovvoru naalum ovvoru veetil"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai naanum thaedatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai naanum thaedatta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Minnale minnalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minnale minnalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sanjalam konda kangalai moodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sanjalam konda kangalai moodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Chandran kaana kaathirundhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chandran kaana kaathirundhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ne varavillai nee varavillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne varavillai nee varavillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidinjae pochae en seiven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidinjae pochae en seiven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thannanaana nana nannaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannanaana nana nannaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannaanannaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannaanannaanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannanaana nana nannaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannanaana nana nannaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannaanannaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannaanannaanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanchana maala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanchana maala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kallam ondrai solli thanthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallam ondrai solli thanthai"/>
</div>
<div class="lyrico-lyrics-wrapper">Katru kondaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katru kondaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kaanum bothae katril vanthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kaanum bothae katril vanthaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kandu kondaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandu kondaai"/>
</div>
<div class="lyrico-lyrics-wrapper">En aatril odum deepam nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En aatril odum deepam nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Karai serven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karai serven"/>
</div>
<div class="lyrico-lyrics-wrapper">En ullankayil veppam nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En ullankayil veppam nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbae en ullangalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbae en ullangalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Minsarangal oduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minsarangal oduthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Urchagam vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urchagam vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Uchanthalai yerudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uchanthalai yerudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maaligai polae veedugal katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaligai polae veedugal katti"/>
</div>
<div class="lyrico-lyrics-wrapper">Margali naalil naan tharavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Margali naalil naan tharavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ovvoru naalum ovvoru veetil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ovvoru naalum ovvoru veetil"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai naanum thaedatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai naanum thaedatta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Minnale minnalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minnale minnalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ooo ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooo ooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sanjalam konda kangalai moodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sanjalam konda kangalai moodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Chandran kaana kaathirundhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chandran kaana kaathirundhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ne varavillai nee varavillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne varavillai nee varavillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidinjae pochae en seiven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidinjae pochae en seiven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanchana maala kanchana maala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanchana maala kanchana maala"/>
</div>
<div class="lyrico-lyrics-wrapper">Kollaamal kollum kanenna velaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kollaamal kollum kanenna velaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanchana maala 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanchana maala "/>
</div>
</pre>
