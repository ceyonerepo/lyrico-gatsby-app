---
title: "vaa jikki song lyrics"
album: "Kadaikutty Singam"
artist: "D. Imman"
lyricist: "Yugabharathi"
director: "Pandiraj"
path: "/albums/kadaikutty-singam-lyrics"
song: "Vaa Jikki"
image: ../../images/albumart/kadaikutty-singam.jpg
date: 2018-07-13
lang: tamil
youtubeLink: "https://www.youtube.com/embed/EESoPICvQts"
type: "happy"
singers:
  - D Imman
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vaa jikki vaa jikki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa jikki vaa jikki"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa jikki vaa jikki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa jikki vaa jikki"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa jikki vaa jikki vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa jikki vaa jikki vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mela vaa jikki vaa jikki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mela vaa jikki vaa jikki"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa jikki vaa jikki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa jikki vaa jikki"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa jikki vaa jikki vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa jikki vaa jikki vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mela vaa jikki vaa jikki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mela vaa jikki vaa jikki"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa jikki vaa jikki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa jikki vaa jikki"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa jikki vaa jikki vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa jikki vaa jikki vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naama odanum velaikki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naama odanum velaikki"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhanum asaikki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhanum asaikki"/>
</div>
<div class="lyrico-lyrics-wrapper">Kotaikki vettaikki vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kotaikki vettaikki vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naamagiri pettaiyoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naamagiri pettaiyoda"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadhaswaram oodhuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadhaswaram oodhuda"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum neeyum onna ninna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum neeyum onna ninna"/>
</div>
<div class="lyrico-lyrics-wrapper">Sethammingae yethuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sethammingae yethuda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaathu oothu athanaikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathu oothu athanaikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Veli illa paaruda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veli illa paaruda"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadum meedum un kaiyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadum meedum un kaiyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavalai enna kooruda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavalai enna kooruda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pattam pola nitham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattam pola nitham"/>
</div>
<div class="lyrico-lyrics-wrapper">Uchi uchi vaanil yeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uchi uchi vaanil yeru"/>
</div>
<div class="lyrico-lyrics-wrapper">Satta thittam pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Satta thittam pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna vella inga yaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna vella inga yaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa jikki vaa jikki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa jikki vaa jikki"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa jikki vaa jikki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa jikki vaa jikki"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa jikki vaa jikki vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa jikki vaa jikki vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mela vaa jikki vaa jikki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mela vaa jikki vaa jikki"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa jikki vaa jikki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa jikki vaa jikki"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa jikki vaa jikki vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa jikki vaa jikki vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mela vaa jikki vaa jikki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mela vaa jikki vaa jikki"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa jikki vaa jikki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa jikki vaa jikki"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa jikki vaa jikki vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa jikki vaa jikki vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naama odanum velaikki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naama odanum velaikki"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhanum asaikki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhanum asaikki"/>
</div>
<div class="lyrico-lyrics-wrapper">Kotaikki vettaikki vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kotaikki vettaikki vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa jikki vaa jikki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa jikki vaa jikki"/>
</div>
<div class="lyrico-lyrics-wrapper">Jikki jikki jikki 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jikki jikki jikki "/>
</div>
<div class="lyrico-lyrics-wrapper">jikki jikki jikki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jikki jikki jikki"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa jikki vaa jikki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa jikki vaa jikki"/>
</div>
<div class="lyrico-lyrics-wrapper">Duuurrr
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Duuurrr"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oorukellaam soru poda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorukellaam soru poda"/>
</div>
<div class="lyrico-lyrics-wrapper">Vivasayam nadakkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vivasayam nadakkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vervaiyila nellumani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vervaiyila nellumani"/>
</div>
<div class="lyrico-lyrics-wrapper">Poova pola sirikkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poova pola sirikkuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Erumbum vaazha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Erumbum vaazha"/>
</div>
<div class="lyrico-lyrics-wrapper">Pacharisi kolam podum orruda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pacharisi kolam podum orruda"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhai yaarum illaiyinu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhai yaarum illaiyinu"/>
</div>
<div class="lyrico-lyrics-wrapper">Eduthu kaattum yeruda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eduthu kaattum yeruda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaiya kaala nambi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiya kaala nambi"/>
</div>
<div class="lyrico-lyrics-wrapper">Innum innum vaangu pera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum innum vaangu pera"/>
</div>
<div class="lyrico-lyrics-wrapper">Moththa oorum onna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moththa oorum onna"/>
</div>
<div class="lyrico-lyrics-wrapper">Solla solla kaalam maara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solla solla kaalam maara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa jikki vaa jikki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa jikki vaa jikki"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa jikki vaa jikki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa jikki vaa jikki"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa jikki vaa jikki vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa jikki vaa jikki vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mela vaa jikki vaa jikki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mela vaa jikki vaa jikki"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa jikki vaa jikki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa jikki vaa jikki"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa jikki vaa jikki vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa jikki vaa jikki vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jhum tha thakkitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jhum tha thakkitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakkitta thaga thaga thaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakkitta thaga thaga thaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Jhum tha thakkitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jhum tha thakkitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakkitta thaga thaga thaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakkitta thaga thaga thaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Jhum thaaku thaga thaga thaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jhum thaaku thaga thaga thaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Jhum thaaku thaga thaga thaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jhum thaaku thaga thaga thaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakita thakita thaga thaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakita thakita thaga thaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaga thaga thaga thaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaga thaga thaga thaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa jikki vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa jikki vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Duuurrrr durr durr
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Duuurrrr durr durr"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kodi kodi kaasa sertha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodi kodi kaasa sertha"/>
</div>
<div class="lyrico-lyrics-wrapper">Varuma selvaakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varuma selvaakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Odi adi velai senju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odi adi velai senju"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai nee thooku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai nee thooku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi vangum pothum neengidatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi vangum pothum neengidatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholu parayae paattaagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholu parayae paattaagum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vedi pola neeyum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedi pola neeyum "/>
</div>
<div class="lyrico-lyrics-wrapper">vegam kaatum pothae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vegam kaatum pothae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naadae koottaagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naadae koottaagum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kodi pola naama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodi pola naama"/>
</div>
<div class="lyrico-lyrics-wrapper">Mela mela yera yera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mela mela yera yera"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorae kaiya thattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorae kaiya thattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa jikki vaa jikki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa jikki vaa jikki"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa jikki vaa jikki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa jikki vaa jikki"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa jikki vaa jikki vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa jikki vaa jikki vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mela vaa jikki vaa jikki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mela vaa jikki vaa jikki"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa jikki vaa jikki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa jikki vaa jikki"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa jikki vaa jikki vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa jikki vaa jikki vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mela vaa jikki vaa jikki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mela vaa jikki vaa jikki"/>
</div>
<div class="lyrico-lyrics-wrapper">vaa jikki vaa jikki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaa jikki vaa jikki"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa jikki vaa jikki vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa jikki vaa jikki vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naama odanum velaikki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naama odanum velaikki"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhanum asaikki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhanum asaikki"/>
</div>
<div class="lyrico-lyrics-wrapper">Kotaikki vettaikki vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kotaikki vettaikki vaa"/>
</div>
</pre>
