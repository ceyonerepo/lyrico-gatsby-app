---
title: "kalala katha song lyrics"
album: "Idhe Maa Katha"
artist: "Sunil Kashyup"
lyricist: "Mangu Balaji"
director: "Guru Pawan"
path: "/albums/idhe-maa-katha-lyrics"
song: "Kalala Katha"
image: ../../images/albumart/idhe-maa-katha.jpg
date: 2021-10-02
lang: telugu
youtubeLink: "https://www.youtube.com/embed/J_AfpznmCwU"
type: "love"
singers:
  - Vishnu Priya
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kalala katha modhalavuthondi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalala katha modhalavuthondi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ala la yedha eguruthu undhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ala la yedha eguruthu undhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Manase ninnu kalavaka mundhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manase ninnu kalavaka mundhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedho thondhara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedho thondhara"/>
</div>
<div class="lyrico-lyrics-wrapper">Kshaname tega nasa peduthundi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kshaname tega nasa peduthundi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aduge ninu kalavamanandhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aduge ninu kalavamanandhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadichina prathi daari needhira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadichina prathi daari needhira"/>
</div>
<div class="lyrico-lyrics-wrapper">Emo emo em chesavu gundenu kalabadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emo emo em chesavu gundenu kalabadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalo ninne oohinchanu ontiga nilabadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalo ninne oohinchanu ontiga nilabadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee premante sandhramantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee premante sandhramantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasemo muthymantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasemo muthymantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Itu daachukundho vintha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Itu daachukundho vintha"/>
</div>
<div class="lyrico-lyrics-wrapper">Koraaka ninnu jatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koraaka ninnu jatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chuputho pampana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chuputho pampana"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallalo premani mouname nimpana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallalo premani mouname nimpana"/>
</div>
<div class="lyrico-lyrics-wrapper">Maatale neevani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maatale neevani"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiduthu theeyaga padamantava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiduthu theeyaga padamantava"/>
</div>
<div class="lyrico-lyrics-wrapper">Alakalu poyina brathimalalika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alakalu poyina brathimalalika"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee premante sandhramantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee premante sandhramantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasemo muthymantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasemo muthymantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Itu daachukundho vintha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Itu daachukundho vintha"/>
</div>
<div class="lyrico-lyrics-wrapper">Koraaka ninnu jatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koraaka ninnu jatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kasirina kosuruthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kasirina kosuruthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaburule cheppaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaburule cheppaku"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappune chesina needhani oppuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappune chesina needhani oppuko"/>
</div>
<div class="lyrico-lyrics-wrapper">Thodai nedaga naatho undipo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodai nedaga naatho undipo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenem chesina neela chusuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenem chesina neela chusuko"/>
</div>
</pre>
