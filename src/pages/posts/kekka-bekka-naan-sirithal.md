---
title: 'keka beka song lyrics'
album: 'Naan Sirithal'
artist: 'Hiphop Tamizha'
lyricist: 'Hiphop Tamizha'
director: 'Raana'
path: '/albums/naan-sirithal-song-lyrics'
song: 'Keka Beka'
image: ../../images/albumart/naan-sirithal.jpg  
date: 2019-07-19
lang: tamil
singers: 
- Hiphop Tamizha
- Rajan Chelliah
youtubeLink: "https://www.youtube.com/embed/vDuYGZqjqbw"
type: 'funny'
---

<pre class="lyrics-native">
    Coming Soon
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Sirikkira maadhiri irukkurenga
<input type="checkbox" class="lyrico-select-lyric-line" value="Sirikkira maadhiri irukkurenga"/>
</div>
<div class="lyrico-lyrics-wrapper">Saathiyama naan ippa sirikalanga
<input type="checkbox" class="lyrico-select-lyric-line" value="Saathiyama naan ippa sirikalanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakkoru maadhiri irukkudhunaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Enakkoru maadhiri irukkudhunaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai mattum yen neenga moraikkiringa
<input type="checkbox" class="lyrico-select-lyric-line" value="Ennai mattum yen neenga moraikkiringa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Sirikkira maadhiri irukkurenga
<input type="checkbox" class="lyrico-select-lyric-line" value="Sirikkira maadhiri irukkurenga"/>
</div>
<div class="lyrico-lyrics-wrapper">Saathiyama naan ippa sirikalanga
<input type="checkbox" class="lyrico-select-lyric-line" value="Saathiyama naan ippa sirikalanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakkoru maadhiri irukkudhunaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Enakkoru maadhiri irukkudhunaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai mattum yen ellam moraikkiringa
<input type="checkbox" class="lyrico-select-lyric-line" value="Ennai mattum yen ellam moraikkiringa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Keka beka keka beka nu
<input type="checkbox" class="lyrico-select-lyric-line" value="Keka beka keka beka nu"/>
</div>
  <div class="lyrico-lyrics-wrapper">Yen (3 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Yen"/></div>
</div>
<div class="lyrico-lyrics-wrapper">Enna paathu sirukkudhu life-u
<input type="checkbox" class="lyrico-select-lyric-line" value="Enna paathu sirukkudhu life-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Keka beka keka beka nu
<input type="checkbox" class="lyrico-select-lyric-line" value="Keka beka keka beka nu"/>
</div>
  <div class="lyrico-lyrics-wrapper">Yen (3 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Yen"/></div>
</div>
<div class="lyrico-lyrics-wrapper">Enna paathu sirukkudhu life-u
<input type="checkbox" class="lyrico-select-lyric-line" value="Enna paathu sirukkudhu life-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Sari sari siri siri
<input type="checkbox" class="lyrico-select-lyric-line" value="Sari sari siri siri"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Siri please to meet you
<input type="checkbox" class="lyrico-select-lyric-line" value="Siri please to meet you"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Haiyo siri
<input type="checkbox" class="lyrico-select-lyric-line" value="Haiyo siri"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Sari sari siri siri
<input type="checkbox" class="lyrico-select-lyric-line" value="Sari sari siri siri"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">I am siri
<input type="checkbox" class="lyrico-select-lyric-line" value="I am siri"/>
</div>
<div class="lyrico-lyrics-wrapper">Need to help you
<input type="checkbox" class="lyrico-select-lyric-line" value="Need to help you"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Haiyo siri
<input type="checkbox" class="lyrico-select-lyric-line" value="Haiyo siri"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Enna paathu sirichavana
<input type="checkbox" class="lyrico-select-lyric-line" value="Enna paathu sirichavana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirumba paathu sirikkiren naan
<input type="checkbox" class="lyrico-select-lyric-line" value="Thirumba paathu sirikkiren naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna paathu sirichavana
<input type="checkbox" class="lyrico-select-lyric-line" value="Enna paathu sirichavana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirumba paathu sirikkiren naan
<input type="checkbox" class="lyrico-select-lyric-line" value="Thirumba paathu sirikkiren naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kastam vandhalum
<input type="checkbox" class="lyrico-select-lyric-line" value="Kastam vandhalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavala vandhalum
<input type="checkbox" class="lyrico-select-lyric-line" value="Kavala vandhalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Siri siri   Siri siri
<input type="checkbox" class="lyrico-select-lyric-line" value="Siri siri   Siri siri"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Enna nadanthaalum
<input type="checkbox" class="lyrico-select-lyric-line" value="Enna nadanthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paathukalam machaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Paathukalam machaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Siri siri   Siri siri
<input type="checkbox" class="lyrico-select-lyric-line" value="Siri siri   Siri siri"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kastam vandhalum
<input type="checkbox" class="lyrico-select-lyric-line" value="Kastam vandhalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavala vandhalum
<input type="checkbox" class="lyrico-select-lyric-line" value="Kavala vandhalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Siri siri   Siri siri
<input type="checkbox" class="lyrico-select-lyric-line" value="Siri siri   Siri siri"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Enna nadanthaalum
<input type="checkbox" class="lyrico-select-lyric-line" value="Enna nadanthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paathukalam machaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Paathukalam machaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Siri siri   Siri siri
<input type="checkbox" class="lyrico-select-lyric-line" value="Siri siri   Siri siri"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Hahahahahahaha
<input type="checkbox" class="lyrico-select-lyric-line" value="Hahahahahahaha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Keka beka keka beka nu
<input type="checkbox" class="lyrico-select-lyric-line" value="Keka beka keka beka nu"/>
</div>
  <div class="lyrico-lyrics-wrapper">Yen (3 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Yen"/></div>
</div>
<div class="lyrico-lyrics-wrapper">Enna paathu sirukkudhu life-u
<input type="checkbox" class="lyrico-select-lyric-line" value="Enna paathu sirukkudhu life-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Keka beka keka beka nu
<input type="checkbox" class="lyrico-select-lyric-line" value="Keka beka keka beka nu"/>
</div>
  <div class="lyrico-lyrics-wrapper">Yen (3 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Yen"/></div>
</div>
<div class="lyrico-lyrics-wrapper">Enna paathu sirukkudhu life-u
<input type="checkbox" class="lyrico-select-lyric-line" value="Enna paathu sirukkudhu life-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Enna paathu sirichavana
<input type="checkbox" class="lyrico-select-lyric-line" value="Enna paathu sirichavana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirumba paathu sirikkiren naan
<input type="checkbox" class="lyrico-select-lyric-line" value="Thirumba paathu sirikkiren naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">En love-u
<input type="checkbox" class="lyrico-select-lyric-line" value="En love-u"/>
</div>
  <div class="lyrico-lyrics-wrapper">Keka beka keka beka
<input type="checkbox" class="lyrico-select-lyric-line" value="Keka beka keka beka"/>
</div>
  <div class="lyrico-lyrics-wrapper">En life-u
<input type="checkbox" class="lyrico-select-lyric-line" value="En life-u"/>
</div>
  <div class="lyrico-lyrics-wrapper">Keka beka keka beka
<input type="checkbox" class="lyrico-select-lyric-line" value="Keka beka keka beka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Day and night-u
<input type="checkbox" class="lyrico-select-lyric-line" value="Day and night-u"/>
</div>
  <div class="lyrico-lyrics-wrapper">Keka beka keka beka
<input type="checkbox" class="lyrico-select-lyric-line" value="Keka beka keka beka"/>
</div>
  <div class="lyrico-lyrics-wrapper">Dhinamum fight-uh
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhinamum fight-uh"/>
</div>
  <div class="lyrico-lyrics-wrapper">Keka beka keka beka
<input type="checkbox" class="lyrico-select-lyric-line" value="Keka beka keka beka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Naan siricha
<input type="checkbox" class="lyrico-select-lyric-line" value="Naan siricha"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha boomi adharum
<input type="checkbox" class="lyrico-select-lyric-line" value="Indha boomi adharum"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha saami meralum
<input type="checkbox" class="lyrico-select-lyric-line" value="Andha saami meralum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sadhi sanamellam
<input type="checkbox" class="lyrico-select-lyric-line" value="Sadhi sanamellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sedharum kadharum
<input type="checkbox" class="lyrico-select-lyric-line" value="Sedharum kadharum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan siricha…aaa…
<input type="checkbox" class="lyrico-select-lyric-line" value="Naan siricha…aaa…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">En love-u
<input type="checkbox" class="lyrico-select-lyric-line" value="En love-u"/>
</div>
  <div class="lyrico-lyrics-wrapper">Keka beka keka beka
<input type="checkbox" class="lyrico-select-lyric-line" value="Keka beka keka beka"/>
</div>
  <div class="lyrico-lyrics-wrapper">En life-u
<input type="checkbox" class="lyrico-select-lyric-line" value="En life-u"/>
</div>
  <div class="lyrico-lyrics-wrapper">Keka beka keka beka
<input type="checkbox" class="lyrico-select-lyric-line" value="Keka beka keka beka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Day and night-u
<input type="checkbox" class="lyrico-select-lyric-line" value="Day and night-u"/>
</div>
  <div class="lyrico-lyrics-wrapper">Keka beka keka beka
<input type="checkbox" class="lyrico-select-lyric-line" value="Keka beka keka beka"/>
</div>
  <div class="lyrico-lyrics-wrapper">Dhinamum fight-uh
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhinamum fight-uh"/>
</div>
  <div class="lyrico-lyrics-wrapper">Keka beka keka beka
<input type="checkbox" class="lyrico-select-lyric-line" value="Keka beka keka beka"/>
</div>
</pre>