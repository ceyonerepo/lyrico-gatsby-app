---
title: "mahi mera ranjha song lyrics"
album: "The Girl On The Train"
artist: "Sunny Inder"
lyricist: "Kumaar"
director: "Ribhu Dasgupta"
path: "/albums/the-girl-on-the-train-lyrics"
song: "Mahi Mera Ranjha"
image: ../../images/albumart/the-girl-on-the-train.jpg
date: 2021-02-26
lang: hindi
youtubeLink: "https://www.youtube.com/embed/NEkDRSw21ls"
type: "celebration"
singers:
  - Navraj Hans
  - Jonita Gandhi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Pardesi raja badi dooron hai aaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pardesi raja badi dooron hai aaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Pardesi raja badi dooron hai aaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pardesi raja badi dooron hai aaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Sakhiyon saja ke doli naal le aaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sakhiyon saja ke doli naal le aaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Sakhiyon saja ke doli naal le aaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sakhiyon saja ke doli naal le aaya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pardesi raja badi dooron hai aaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pardesi raja badi dooron hai aaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Pardesi raja sohna pardesi raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pardesi raja sohna pardesi raja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tu meri rani main tera king oye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu meri rani main tera king oye"/>
</div>
<div class="lyrico-lyrics-wrapper">Main tera king oye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main tera king oye"/>
</div>
<div class="lyrico-lyrics-wrapper">Leke main aaya japani ring oye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Leke main aaya japani ring oye"/>
</div>
<div class="lyrico-lyrics-wrapper">Japani ring oye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Japani ring oye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tu meri rani main tera king oye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu meri rani main tera king oye"/>
</div>
<div class="lyrico-lyrics-wrapper">Leke main aaya japani ring oye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Leke main aaya japani ring oye"/>
</div>
<div class="lyrico-lyrics-wrapper">Dil di main keh daan zara na sharmawan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil di main keh daan zara na sharmawan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ajj desi dholak te folk song gaawan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ajj desi dholak te folk song gaawan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mahi mera ranjha main
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mahi mera ranjha main"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho! Ho! Ho! Ho!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho! Ho! Ho! Ho!"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mahi mera ranjha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mahi mera ranjha"/>
</div>
<div class="lyrico-lyrics-wrapper">Main heer bann jaawan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main heer bann jaawan"/>
</div>
<div class="lyrico-lyrics-wrapper">Main heer bann jaawan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main heer bann jaawan"/>
</div>
<div class="lyrico-lyrics-wrapper">Bana ke paranda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bana ke paranda"/>
</div>
<div class="lyrico-lyrics-wrapper">Main gutt vich laawan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main gutt vich laawan"/>
</div>
<div class="lyrico-lyrics-wrapper">Te nachdi hi jaawan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Te nachdi hi jaawan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mahi mera ranjha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mahi mera ranjha"/>
</div>
<div class="lyrico-lyrics-wrapper">Main heer bann jaawan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main heer bann jaawan"/>
</div>
<div class="lyrico-lyrics-wrapper">Main heer bann jaawan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main heer bann jaawan"/>
</div>
<div class="lyrico-lyrics-wrapper">Bana ke paranda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bana ke paranda"/>
</div>
<div class="lyrico-lyrics-wrapper">Main gutt vich laawan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main gutt vich laawan"/>
</div>
<div class="lyrico-lyrics-wrapper">Te nachdi hi jaawan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Te nachdi hi jaawan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tim tim taareyan naal raat nachdi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tim tim taareyan naal raat nachdi"/>
</div>
<div class="lyrico-lyrics-wrapper">Haan raat nachdi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haan raat nachdi"/>
</div>
<div class="lyrico-lyrics-wrapper">Jo vi dekhe kehnda saadi jodi jachdi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jo vi dekhe kehnda saadi jodi jachdi"/>
</div>
<div class="lyrico-lyrics-wrapper">Haan jodi jachdi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haan jodi jachdi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tim tim taareyan naal raat nachdi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tim tim taareyan naal raat nachdi"/>
</div>
<div class="lyrico-lyrics-wrapper">Haan raat nachdi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haan raat nachdi"/>
</div>
<div class="lyrico-lyrics-wrapper">Jo vi dekhe kehnda saadi jodi jachdi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jo vi dekhe kehnda saadi jodi jachdi"/>
</div>
<div class="lyrico-lyrics-wrapper">Haan jodi jachdi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haan jodi jachdi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanna di baali ch tainu latkawaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna di baali ch tainu latkawaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kajle di jagah tainu ankhiyan ch paawan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kajle di jagah tainu ankhiyan ch paawan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haan mahi mera ranjha main
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haan mahi mera ranjha main"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho! Ho! Ho! Ho!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho! Ho! Ho! Ho!"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mahi mera ranjha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mahi mera ranjha"/>
</div>
<div class="lyrico-lyrics-wrapper">Main heer bann jaawan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main heer bann jaawan"/>
</div>
<div class="lyrico-lyrics-wrapper">Main heer bann jaawan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main heer bann jaawan"/>
</div>
<div class="lyrico-lyrics-wrapper">Bana ke paranda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bana ke paranda"/>
</div>
<div class="lyrico-lyrics-wrapper">Main gutt vich laawan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main gutt vich laawan"/>
</div>
<div class="lyrico-lyrics-wrapper">Te nachdi hi jaawan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Te nachdi hi jaawan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mahi mera ranjha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mahi mera ranjha"/>
</div>
<div class="lyrico-lyrics-wrapper">Main heer bann jaawan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main heer bann jaawan"/>
</div>
<div class="lyrico-lyrics-wrapper">Main heer bann jaawan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main heer bann jaawan"/>
</div>
<div class="lyrico-lyrics-wrapper">Bana ke paranda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bana ke paranda"/>
</div>
<div class="lyrico-lyrics-wrapper">Main gutt vich laawan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main gutt vich laawan"/>
</div>
<div class="lyrico-lyrics-wrapper">Te nachdi hi jaawan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Te nachdi hi jaawan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mahi tera nakhreela hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mahi tera nakhreela hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nakhre karda 100
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nakhre karda 100"/>
</div>
<div class="lyrico-lyrics-wrapper">Haan nakhre karda 100
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haan nakhre karda 100"/>
</div>
<div class="lyrico-lyrics-wrapper">Mahi tera nakhreela hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mahi tera nakhreela hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nakhre karda 100
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nakhre karda 100"/>
</div>
<div class="lyrico-lyrics-wrapper">Hajje vi haiga time tere kol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hajje vi haiga time tere kol"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudiye karde no, kudiye karde no
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudiye karde no, kudiye karde no"/>
</div>
<div class="lyrico-lyrics-wrapper">Haan kudiye karde no
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haan kudiye karde no"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dil di main keh daan zara na sharmawan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil di main keh daan zara na sharmawan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ajj desi dholak te folk song gaawan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ajj desi dholak te folk song gaawan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haan mahi mera ranjha main
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haan mahi mera ranjha main"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho! Ho! Ho! Ho!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho! Ho! Ho! Ho!"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mahi mera ranjha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mahi mera ranjha"/>
</div>
<div class="lyrico-lyrics-wrapper">Main heer bann jaawan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main heer bann jaawan"/>
</div>
<div class="lyrico-lyrics-wrapper">Main heer bann jaawan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main heer bann jaawan"/>
</div>
<div class="lyrico-lyrics-wrapper">Bana ke paranda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bana ke paranda"/>
</div>
<div class="lyrico-lyrics-wrapper">Main gutt vich laawan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main gutt vich laawan"/>
</div>
<div class="lyrico-lyrics-wrapper">Te nachdi hi jaawan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Te nachdi hi jaawan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mahi mera ranjha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mahi mera ranjha"/>
</div>
<div class="lyrico-lyrics-wrapper">Main heer bann jaawan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main heer bann jaawan"/>
</div>
<div class="lyrico-lyrics-wrapper">Main heer bann jaawan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main heer bann jaawan"/>
</div>
<div class="lyrico-lyrics-wrapper">Bana ke paranda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bana ke paranda"/>
</div>
<div class="lyrico-lyrics-wrapper">Main gutt vich laawan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main gutt vich laawan"/>
</div>
<div class="lyrico-lyrics-wrapper">Te nachdi hi jaawan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Te nachdi hi jaawan"/>
</div>
</pre>
