---
title: "chinna pillai poley song lyrics"
album: "Biskoth"
artist: "Radhan"
lyricist: "Radhan"
director: "R. Kannan"
path: "/albums/biskoth-lyrics"
song: "Chinna Pillai Poley"
image: ../../images/albumart/biskoth.jpg
date: 2020-11-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ojLHNImCDwY"
type: "happy"
singers:
  - Poornima
  - Mujib Rahman
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ha ha ha ha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ha ha ha ha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ha ha ha ha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ha ha ha ha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ha ha ha ha ha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ha ha ha ha ha "/>
</div>
<div class="lyrico-lyrics-wrapper">hahah hahah haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hahah hahah haa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ha ha ha ha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ha ha ha ha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ha ha ha ha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ha ha ha ha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ha ha ha ha ha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ha ha ha ha ha "/>
</div>
<div class="lyrico-lyrics-wrapper">hahah hahah haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hahah hahah haa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chinna pillai polae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna pillai polae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naam suththi thirivom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam suththi thirivom"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattampoochi polae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattampoochi polae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vattam adippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vattam adippom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ootanda soltu vantiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ootanda soltu vantiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey chinna paiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey chinna paiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandiya paathu ootuyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandiya paathu ootuyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullara old-u beauty-ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullara old-u beauty-ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada thanga paiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada thanga paiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Old-aana gold-u party-ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Old-aana gold-u party-ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Biskothu pullingo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Biskothu pullingo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kan ingae illengo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan ingae illengo"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbaaga pesa thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbaaga pesa thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarumae illaingo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarumae illaingo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Peraandi sirichaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peraandi sirichaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Tension-eh illaingo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tension-eh illaingo"/>
</div>
<div class="lyrico-lyrics-wrapper">Vayasana ennango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayasana ennango"/>
</div>
<div class="lyrico-lyrics-wrapper">Enjoy-u pannungo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enjoy-u pannungo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pulippu mittaai pulikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pulippu mittaai pulikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Panju mittaai inikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panju mittaai inikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oora suththi paakanum-na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oora suththi paakanum-na"/>
</div>
<div class="lyrico-lyrics-wrapper">Engalukku kasakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engalukku kasakkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pakkam unna odhaikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkam unna odhaikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaandi paaru jeyikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaandi paaru jeyikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga age-u vandhakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga age-u vandhakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Ungalukkum poraikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ungalukkum poraikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chinna pillai polae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna pillai polae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naam suththi thirivom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam suththi thirivom"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattampoochi polae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattampoochi polae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vattam adippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vattam adippom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ootanda soltu vantiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ootanda soltu vantiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey chinna paiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey chinna paiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandiya paathu ootuya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandiya paathu ootuya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Advice-u sonna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Advice-u sonna "/>
</div>
<div class="lyrico-lyrics-wrapper">pulla kandukadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pulla kandukadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha vaandungellam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha vaandungellam "/>
</div>
<div class="lyrico-lyrics-wrapper">konjam kooda madhikadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konjam kooda madhikadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma perusunga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma perusunga "/>
</div>
<div class="lyrico-lyrics-wrapper">sollum anubavam dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sollum anubavam dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Unga computer 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unga computer "/>
</div>
<div class="lyrico-lyrics-wrapper">knowledge-la kedaikkadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="knowledge-la kedaikkadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anadhai illai aana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anadhai illai aana"/>
</div>
<div class="lyrico-lyrics-wrapper">Ondiya vaazhurom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondiya vaazhurom"/>
</div>
<div class="lyrico-lyrics-wrapper">Anba mattum thaan inga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anba mattum thaan inga"/>
</div>
<div class="lyrico-lyrics-wrapper">Romba romba thedurom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba romba thedurom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Biskothu Pullingo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Biskothu Pullingo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kan ingae Illengo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan ingae Illengo"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbaaga pesa thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbaaga pesa thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarumae illaingo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarumae illaingo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Peraandi sirichaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peraandi sirichaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Tension-eh illaingo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tension-eh illaingo"/>
</div>
<div class="lyrico-lyrics-wrapper">Vayasana ennango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayasana ennango"/>
</div>
<div class="lyrico-lyrics-wrapper">Enjoy-u pannungo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enjoy-u pannungo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pulippu mittaai pulikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pulippu mittaai pulikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Panju mittaai inikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panju mittaai inikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oora suththi paakanum-na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oora suththi paakanum-na"/>
</div>
<div class="lyrico-lyrics-wrapper">Engalukku kasakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engalukku kasakkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pakkam unna odhaikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkam unna odhaikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaandi paaru jeyikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaandi paaru jeyikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga age-u vandhakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga age-u vandhakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Ungalukkum poraikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ungalukkum poraikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chinna pillai polae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna pillai polae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naam suththi thirivom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam suththi thirivom"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattampoochi polae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattampoochi polae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vattam adippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vattam adippom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ootanda soltu vantiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ootanda soltu vantiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey chinna paiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey chinna paiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandiya paathu ootuya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandiya paathu ootuya"/>
</div>
</pre>
