---
title: "oggu katha song lyrics"
album: "Pressure Cooker"
artist: "Smaran"
lyricist: "Oggu Ellaiah - Oggu Shivaiah"
director: "Sujoi Karampuri - Sushil Karampuri"
path: "/albums/pressure-cooker-lyrics"
song: "Oggu Katha"
image: ../../images/albumart/pressure-cooker.jpg
date: 2020-02-21
lang: telugu
youtubeLink: "https://www.youtube.com/embed/vk0Kb0RYgOA"
type: "title track"
singers:
  - Oggu Ellaiah
  - Oggu Shivaiah
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hyderabad edisi Amaerica 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hyderabad edisi Amaerica "/>
</div>
<div class="lyrico-lyrics-wrapper">la Kalupedithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="la Kalupedithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Naaperu Kishoru kadanukuntundu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naaperu Kishoru kadanukuntundu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maa nakkanna koduku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa nakkanna koduku"/>
</div>
<div class="lyrico-lyrics-wrapper">mari nenu Hydarabad idisi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mari nenu Hydarabad idisi "/>
</div>
<div class="lyrico-lyrics-wrapper">america thapakunda ponani cheppi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="america thapakunda ponani cheppi"/>
</div>
<div class="lyrico-lyrics-wrapper">Hyderabadlona Thiruguthundurooo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hyderabadlona Thiruguthundurooo "/>
</div>
<div class="lyrico-lyrics-wrapper">kishora koduku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kishora koduku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ori kumara kishora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ori kumara kishora"/>
</div>
<div class="lyrico-lyrics-wrapper">nenu enthasepu cheppina 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenu enthasepu cheppina "/>
</div>
<div class="lyrico-lyrics-wrapper">pothani nee notlakosthaledu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pothani nee notlakosthaledu"/>
</div>
<div class="lyrico-lyrics-wrapper">neekishtamleni pani 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neekishtamleni pani "/>
</div>
<div class="lyrico-lyrics-wrapper">chesthunnavraa koduka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chesthunnavraa koduka"/>
</div>
<div class="lyrico-lyrics-wrapper">nenu cheputhunna oka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenu cheputhunna oka "/>
</div>
<div class="lyrico-lyrics-wrapper">mata chivulallapettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mata chivulallapettu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">koduka odigali sudigali ramorama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koduka odigali sudigali ramorama"/>
</div>
<div class="lyrico-lyrics-wrapper">galiki thirigavuro ramirama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="galiki thirigavuro ramirama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">koduka odigali sudigali ramorama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koduka odigali sudigali ramorama"/>
</div>
<div class="lyrico-lyrics-wrapper">galiki thirigavuro ramirama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="galiki thirigavuro ramirama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">koduka odigali sudigali ramorama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koduka odigali sudigali ramorama"/>
</div>
<div class="lyrico-lyrics-wrapper">galiki thirigavuro ramirama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="galiki thirigavuro ramirama"/>
</div>
</pre>
