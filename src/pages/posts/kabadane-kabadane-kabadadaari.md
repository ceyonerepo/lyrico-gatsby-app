---
title: "kabadane kabadane song lyrics"
album: "Kabadadaari"
artist: "Simon K.King"
lyricist: "Arun Bharathi"
director: "Pradeep Krishnamoorthy"
path: "/albums/kabadadaari-song-lyrics"
song: "Kabadane Kabadane"
image: ../../images/albumart/kabadadaari.jpg
date: 2021-01-28
lang: tamil
youtubeLink: "https://www.youtube.com/embed/U57H2wxgnBI"
type: "Motivational"
singers:
  - Sibi Sathyaraj
  - Swetha Nanditha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">vanjam karvam kollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanjam karvam kollum"/>
</div>
<div class="lyrico-lyrics-wrapper">dharmam mounam kollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dharmam mounam kollum"/>
</div>
<div class="lyrico-lyrics-wrapper">ellam undhan kaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellam undhan kaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">kabadane kabadane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kabadane kabadane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">niyayam kaigal kattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="niyayam kaigal kattum"/>
</div>
<div class="lyrico-lyrics-wrapper">nyanam irunil suttrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nyanam irunil suttrum"/>
</div>
<div class="lyrico-lyrics-wrapper">niyayam unnai pottrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="niyayam unnai pottrum"/>
</div>
<div class="lyrico-lyrics-wrapper">kabadane kabadane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kabadane kabadane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aakkam mathiyin uccham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aakkam mathiyin uccham"/>
</div>
<div class="lyrico-lyrics-wrapper">aattam sathiyin uccham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aattam sathiyin uccham"/>
</div>
<div class="lyrico-lyrics-wrapper">koottam unnai mecchum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koottam unnai mecchum"/>
</div>
<div class="lyrico-lyrics-wrapper">kabadane kabadane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kabadane kabadane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">deepam thiriyin uccham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="deepam thiriyin uccham"/>
</div>
<div class="lyrico-lyrics-wrapper">mocham uyirin uccham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mocham uyirin uccham"/>
</div>
<div class="lyrico-lyrics-wrapper">neero veriyin miccham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neero veriyin miccham"/>
</div>
<div class="lyrico-lyrics-wrapper">kabadane kabadane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kabadane kabadane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vedane oooo vedane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vedane oooo vedane"/>
</div>
<div class="lyrico-lyrics-wrapper">oyuma vedhalame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oyuma vedhalame"/>
</div>
<div class="lyrico-lyrics-wrapper">vesame oooo vesame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vesame oooo vesame"/>
</div>
<div class="lyrico-lyrics-wrapper">un idame serum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un idame serum"/>
</div>
<div class="lyrico-lyrics-wrapper">undhan vinai thane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="undhan vinai thane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">paavam vidhaigal veesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paavam vidhaigal veesum"/>
</div>
<div class="lyrico-lyrics-wrapper">vaanam thuligal veesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaanam thuligal veesum"/>
</div>
<div class="lyrico-lyrics-wrapper">kaalam medhuvai peesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaalam medhuvai peesum"/>
</div>
<div class="lyrico-lyrics-wrapper">kabadane kabadane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kabadane kabadane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">theemai mudhalil vellum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theemai mudhalil vellum"/>
</div>
<div class="lyrico-lyrics-wrapper">vaaimai mudivil vellum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaaimai mudivil vellum"/>
</div>
<div class="lyrico-lyrics-wrapper">oolin vallimai kollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oolin vallimai kollum"/>
</div>
<div class="lyrico-lyrics-wrapper">kabadane kabadane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kabadane kabadane"/>
</div>
</pre>
