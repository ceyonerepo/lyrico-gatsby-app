---
title: "naa sinni ramayya song lyrics"
album: "Thupaki Ramudu"
artist: "T Prabhakar "
lyricist: "Abhinaya Srinivas"
director: "T Prabhakar"
path: "/albums/thupaki-ramudu-lyrics"
song: "Naa Sinni Ramayya"
image: ../../images/albumart/thupaki-ramudu.jpg
date: 2019-10-25
lang: telugu
youtubeLink: "https://www.youtube.com/embed/TVf1kxnAEUA"
type: "happy"
singers:
  - Gayatri
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">naa sinni ramayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa sinni ramayya"/>
</div>
<div class="lyrico-lyrics-wrapper">koduka ammanaithira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koduka ammanaithira"/>
</div>
<div class="lyrico-lyrics-wrapper">ye thalli gannadhira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ye thalli gannadhira"/>
</div>
<div class="lyrico-lyrics-wrapper">ninnu naaku thoduga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninnu naaku thoduga"/>
</div>
<div class="lyrico-lyrics-wrapper">gudise gundelo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gudise gundelo"/>
</div>
<div class="lyrico-lyrics-wrapper">velugai nindinaavura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="velugai nindinaavura"/>
</div>
<div class="lyrico-lyrics-wrapper">pegu bandhame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pegu bandhame"/>
</div>
<div class="lyrico-lyrics-wrapper">leni prema manadhira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="leni prema manadhira"/>
</div>
<div class="lyrico-lyrics-wrapper">dhikku leni naa bathuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhikku leni naa bathuku"/>
</div>
<div class="lyrico-lyrics-wrapper">dheera nuvvura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dheera nuvvura"/>
</div>
<div class="lyrico-lyrics-wrapper">nadipe dhaari nuvvura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nadipe dhaari nuvvura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">naa sinni ramayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa sinni ramayya"/>
</div>
<div class="lyrico-lyrics-wrapper">koduka ammanaithira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koduka ammanaithira"/>
</div>
<div class="lyrico-lyrics-wrapper">ye thalli gannadhira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ye thalli gannadhira"/>
</div>
<div class="lyrico-lyrics-wrapper">ninnu naaku thoduga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninnu naaku thoduga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">guna guna nadaka joosi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="guna guna nadaka joosi"/>
</div>
<div class="lyrico-lyrics-wrapper">gundelu uppongenura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gundelu uppongenura"/>
</div>
<div class="lyrico-lyrics-wrapper">kila kila navvu chusi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kila kila navvu chusi"/>
</div>
<div class="lyrico-lyrics-wrapper">kadupu nindipoyerura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadupu nindipoyerura"/>
</div>
<div class="lyrico-lyrics-wrapper">amma ani ante 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="amma ani ante "/>
</div>
<div class="lyrico-lyrics-wrapper">yennupoosa kadhilenura 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yennupoosa kadhilenura "/>
</div>
<div class="lyrico-lyrics-wrapper">yenno aasalatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yenno aasalatho"/>
</div>
<div class="lyrico-lyrics-wrapper">ninnu saadhu kondhunura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninnu saadhu kondhunura"/>
</div>
<div class="lyrico-lyrics-wrapper">jolanu katti neeku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jolanu katti neeku "/>
</div>
<div class="lyrico-lyrics-wrapper">jola paadana kantiki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jola paadana kantiki"/>
</div>
<div class="lyrico-lyrics-wrapper">reppa vole kaavalundana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="reppa vole kaavalundana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">naa sinni ramayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa sinni ramayya"/>
</div>
<div class="lyrico-lyrics-wrapper">koduka ammanaithira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koduka ammanaithira"/>
</div>
<div class="lyrico-lyrics-wrapper">ye thalli gannadhira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ye thalli gannadhira"/>
</div>
<div class="lyrico-lyrics-wrapper">ninnu naaku thoduga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninnu naaku thoduga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">palaana poradante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="palaana poradante"/>
</div>
<div class="lyrico-lyrics-wrapper">padhimandiki teluvaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="padhimandiki teluvaale"/>
</div>
<div class="lyrico-lyrics-wrapper">tupaki ramudante 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tupaki ramudante "/>
</div>
<div class="lyrico-lyrics-wrapper">lokamantha mechchale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lokamantha mechchale"/>
</div>
<div class="lyrico-lyrics-wrapper">ooriki naa koduku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooriki naa koduku"/>
</div>
<div class="lyrico-lyrics-wrapper">yenu karrai niluvali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yenu karrai niluvali"/>
</div>
<div class="lyrico-lyrics-wrapper">prathi okkari ninda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="prathi okkari ninda"/>
</div>
<div class="lyrico-lyrics-wrapper">dheevenardhi pondhali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dheevenardhi pondhali"/>
</div>
<div class="lyrico-lyrics-wrapper">naa aayussu kuda 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa aayussu kuda "/>
</div>
<div class="lyrico-lyrics-wrapper">neeku poyana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeku poyana"/>
</div>
<div class="lyrico-lyrics-wrapper">sallaga noorendlu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sallaga noorendlu"/>
</div>
<div class="lyrico-lyrics-wrapper">brathuku nayana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="brathuku nayana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">naa sinni ramayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa sinni ramayya"/>
</div>
<div class="lyrico-lyrics-wrapper">koduka ammanaithira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koduka ammanaithira"/>
</div>
<div class="lyrico-lyrics-wrapper">ye thalli gannadhira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ye thalli gannadhira"/>
</div>
<div class="lyrico-lyrics-wrapper">ninnu naaku thoduga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninnu naaku thoduga"/>
</div>
</pre>
