---
title: 'dhom dhom song lyrics'
album: 'Naan Sirithal'
artist: 'Hiphop Tamizha'
lyricist: 'Hiphop Tamizha'
director: 'Raana'
path: '/albums/naan-sirithal-song-lyrics'
song: 'Dhom Dhom'
image: ../../images/albumart/naan-sirithal.jpg  
date: 2019-07-19
lang: tamil
singers: 
- Hiphop Tamizha
- Sanjith Hegde
youtubeLink: "https://www.youtube.com/embed/29FUG72LVFE"
type: 'love'
---

<pre class="lyrics-native">
    Coming Soon
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">hooo hooo hoo oooo hoo hooo
<input type="checkbox" class="lyrico-select-lyric-line" value="hooo hooo hoo oooo hoo hooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Dhom dhom thana
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhom dhom thana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thira nana thira nana
<input type="checkbox" class="lyrico-select-lyric-line" value="Thira nana thira nana"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhom dhom
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhom dhom"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana nagarthana nana nana
<input type="checkbox" class="lyrico-select-lyric-line" value="Thana nagarthana nana nana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Dhom dhom thana
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhom dhom thana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thira nana thira nana
<input type="checkbox" class="lyrico-select-lyric-line" value="Thira nana thira nana"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhom dhom
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhom dhom"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana nagarthana nana nana
<input type="checkbox" class="lyrico-select-lyric-line" value="Thana nagarthana nana nana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Un paarvai un pechu
<input type="checkbox" class="lyrico-select-lyric-line" value="Un paarvai un pechu"/>
</div>
<div class="lyrico-lyrics-wrapper">Selai kattiya silaiyae vaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Selai kattiya silaiyae vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinam dhorum vegu dhooram
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhinam dhorum vegu dhooram"/>
</div>
<div class="lyrico-lyrics-wrapper">Un pinnaal naan varava
<input type="checkbox" class="lyrico-select-lyric-line" value="Un pinnaal naan varava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Marudhani polae
<input type="checkbox" class="lyrico-select-lyric-line" value="Marudhani polae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sivandha kanangal
<input type="checkbox" class="lyrico-select-lyric-line" value="Sivandha kanangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Punnagai theanaai vadiyum
<input type="checkbox" class="lyrico-select-lyric-line" value="Punnagai theanaai vadiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanga kinnangal
<input type="checkbox" class="lyrico-select-lyric-line" value="Thanga kinnangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Aval kuzhal adhil adhil
<input type="checkbox" class="lyrico-select-lyric-line" value="Aval kuzhal adhil adhil"/>
</div>
<div class="lyrico-lyrics-wrapper">Medhuvaai en manadhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Medhuvaai en manadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee suzhalaadhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee suzhalaadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Oh oh oh oh…
<input type="checkbox" class="lyrico-select-lyric-line" value="Oh oh oh oh…"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanga thaer polae
<input type="checkbox" class="lyrico-select-lyric-line" value="Thanga thaer polae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Dhom dhom thana
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhom dhom thana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thira nana thira nana
<input type="checkbox" class="lyrico-select-lyric-line" value="Thira nana thira nana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Oh oh oh oh…
<input type="checkbox" class="lyrico-select-lyric-line" value="Oh oh oh oh…"/>
</div>
<div class="lyrico-lyrics-wrapper">Minnum ivalaalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Minnum ivalaalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Dhom dhom
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhom dhom"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana nagarthana nana nana
<input type="checkbox" class="lyrico-select-lyric-line" value="Thana nagarthana nana nana"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhom dhom thana
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhom dhom thana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thira nana thira nana
<input type="checkbox" class="lyrico-select-lyric-line" value="Thira nana thira nana"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhom dhom
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhom dhom"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana nagarthana nana nana
<input type="checkbox" class="lyrico-select-lyric-line" value="Thana nagarthana nana nana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Un sirippu bright
<input type="checkbox" class="lyrico-select-lyric-line" value="Un sirippu bright"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee sonna ellaam right
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee sonna ellaam right"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kaadhal thaan en life
<input type="checkbox" class="lyrico-select-lyric-line" value="Un kaadhal thaan en life"/>
</div>
<div class="lyrico-lyrics-wrapper">Aprom needhan baby life
<input type="checkbox" class="lyrico-select-lyric-line" value="Aprom needhan baby life"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thousand butterflies
<input type="checkbox" class="lyrico-select-lyric-line" value="Thousand butterflies"/>
</div>
<div class="lyrico-lyrics-wrapper">En nenjikulla fly
<input type="checkbox" class="lyrico-select-lyric-line" value="En nenjikulla fly"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanna moodi love pandren
<input type="checkbox" class="lyrico-select-lyric-line" value="Kanna moodi love pandren"/>
</div>
<div class="lyrico-lyrics-wrapper">Unmaiyil hypnotised
<input type="checkbox" class="lyrico-select-lyric-line" value="Unmaiyil hypnotised"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Un pinnala pinnala
<input type="checkbox" class="lyrico-select-lyric-line" value="Un pinnala pinnala"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannala thannala
<input type="checkbox" class="lyrico-select-lyric-line" value="Thannala thannala"/>
</div>
<div class="lyrico-lyrics-wrapper">Poguren di naanum
<input type="checkbox" class="lyrico-select-lyric-line" value="Poguren di naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vera yaarum thevai illai
<input type="checkbox" class="lyrico-select-lyric-line" value="Vera yaarum thevai illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee mattum thaan venum
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee mattum thaan venum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkaaga enna vena
<input type="checkbox" class="lyrico-select-lyric-line" value="Unakkaaga enna vena"/>
</div>
<div class="lyrico-lyrics-wrapper">Pannuven di naanum
<input type="checkbox" class="lyrico-select-lyric-line" value="Pannuven di naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha feeling inga kandupudikka
<input type="checkbox" class="lyrico-select-lyric-line" value="Indha feeling inga kandupudikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudiyadhu vinyanam
<input type="checkbox" class="lyrico-select-lyric-line" value="Mudiyadhu vinyanam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Haa….aaa….aaa…
<input type="checkbox" class="lyrico-select-lyric-line" value="Haa….aaa….aaa…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Hay hey ok….
<input type="checkbox" class="lyrico-select-lyric-line" value="Hay hey ok…."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Aval aval ponmagal
<input type="checkbox" class="lyrico-select-lyric-line" value="Aval aval ponmagal"/>
</div>
<div class="lyrico-lyrics-wrapper">Entdan manam kavarndhaval
<input type="checkbox" class="lyrico-select-lyric-line" value="Entdan manam kavarndhaval"/>
</div>
<div class="lyrico-lyrics-wrapper">Karam karam patriyae
<input type="checkbox" class="lyrico-select-lyric-line" value="Karam karam patriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinam unai sutruven
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhinam unai sutruven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Un pinnae pennae
<input type="checkbox" class="lyrico-select-lyric-line" value="Un pinnae pennae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan varuvenae
<input type="checkbox" class="lyrico-select-lyric-line" value="Naan varuvenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kelaamal ennai tharuvenae
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee kelaamal ennai tharuvenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Oh oh oh oh…
<input type="checkbox" class="lyrico-select-lyric-line" value="Oh oh oh oh…"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanga thaer polae
<input type="checkbox" class="lyrico-select-lyric-line" value="Thanga thaer polae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Dhom dhom thana
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhom dhom thana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thira nana thira nana
<input type="checkbox" class="lyrico-select-lyric-line" value="Thira nana thira nana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Oh oh oh oh…
<input type="checkbox" class="lyrico-select-lyric-line" value="Oh oh oh oh…"/>
</div>
<div class="lyrico-lyrics-wrapper">Minnum ivalaalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Minnum ivalaalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Dhom dhom
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhom dhom"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana nagarthana nana nana
<input type="checkbox" class="lyrico-select-lyric-line" value="Thana nagarthana nana nana"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhom dhom thana
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhom dhom thana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thira nana thira nana
<input type="checkbox" class="lyrico-select-lyric-line" value="Thira nana thira nana"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhom dhom
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhom dhom"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana nagarthana nana nana
<input type="checkbox" class="lyrico-select-lyric-line" value="Thana nagarthana nana nana"/>
</div>
</pre>