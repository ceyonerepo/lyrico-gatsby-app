---
title: "osthaadhu song lyrics"
album: "Madurai Manikuravar"
artist: "Isaignani Ilaiyaraaja"
lyricist: "Kavignar Muthulingam"
director: "K Raajarishi"
path: "/albums/madurai-manikuravar-lyrics"
song: "Osthaadhu"
image: ../../images/albumart/madurai-manikuravar.jpg
date: 2021-12-31
lang: tamil
youtubeLink: 
type: "happy"
singers:
  - Nincy
  - Mukesh
  - Velmurugan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">unna ethuthu munne nikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna ethuthu munne nikka"/>
</div>
<div class="lyrico-lyrics-wrapper">ipo inga yaru inga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ipo inga yaru inga"/>
</div>
<div class="lyrico-lyrics-wrapper">osthadu osthadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="osthadu osthadu"/>
</div>
<div class="lyrico-lyrics-wrapper">ipo osthadu osthadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ipo osthadu osthadu"/>
</div>
<div class="lyrico-lyrics-wrapper">osthadu osthadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="osthadu osthadu"/>
</div>
<div class="lyrico-lyrics-wrapper">inga osthadu osthadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inga osthadu osthadu"/>
</div>
<div class="lyrico-lyrics-wrapper">eduthu unga peruma solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eduthu unga peruma solla"/>
</div>
<div class="lyrico-lyrics-wrapper">enaku therunja solle ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enaku therunja solle ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">pathathu pathathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathathu pathathu"/>
</div>
<div class="lyrico-lyrics-wrapper">ada pathathu pathathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada pathathu pathathu"/>
</div>
<div class="lyrico-lyrics-wrapper">pathathu pathathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathathu pathathu"/>
</div>
<div class="lyrico-lyrics-wrapper">ada pathathu pathathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada pathathu pathathu"/>
</div>
<div class="lyrico-lyrics-wrapper">usar aiya usar konjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="usar aiya usar konjam"/>
</div>
<div class="lyrico-lyrics-wrapper">oram poi ukaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oram poi ukaru"/>
</div>
<div class="lyrico-lyrics-wrapper">kuthatam than nee aadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuthatam than nee aadu"/>
</div>
<div class="lyrico-lyrics-wrapper">ipo uttom aparam varathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ipo uttom aparam varathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kulla nari thanthirathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulla nari thanthirathil"/>
</div>
<div class="lyrico-lyrics-wrapper">ketti karan nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ketti karan nee"/>
</div>
<div class="lyrico-lyrics-wrapper">kuthu vetti potavanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuthu vetti potavanga"/>
</div>
<div class="lyrico-lyrics-wrapper">pakka rowdy nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pakka rowdy nee"/>
</div>
<div class="lyrico-lyrics-wrapper">epothum thapathu amma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="epothum thapathu amma"/>
</div>
<div class="lyrico-lyrics-wrapper">ennudaya kanaku than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennudaya kanaku than"/>
</div>
<div class="lyrico-lyrics-wrapper">solli solli adikatuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="solli solli adikatuma"/>
</div>
<div class="lyrico-lyrics-wrapper">eduvapavana valachu than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eduvapavana valachu than"/>
</div>
<div class="lyrico-lyrics-wrapper">ethi ethi edupa valachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethi ethi edupa valachu"/>
</div>
<div class="lyrico-lyrics-wrapper">vitha kaatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vitha kaatu"/>
</div>
<div class="lyrico-lyrics-wrapper">eppa veetu kekuratha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eppa veetu kekuratha"/>
</div>
<div class="lyrico-lyrics-wrapper">natathi kaatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="natathi kaatu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">unna ethuthu munne nikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna ethuthu munne nikka"/>
</div>
<div class="lyrico-lyrics-wrapper">ipo inga yaru inga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ipo inga yaru inga"/>
</div>
<div class="lyrico-lyrics-wrapper">osthadu osthadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="osthadu osthadu"/>
</div>
<div class="lyrico-lyrics-wrapper">ipo osthadu osthadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ipo osthadu osthadu"/>
</div>
<div class="lyrico-lyrics-wrapper">osthadu osthadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="osthadu osthadu"/>
</div>
<div class="lyrico-lyrics-wrapper">inga osthadu osthadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inga osthadu osthadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">chuthi chuthi oodum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chuthi chuthi oodum "/>
</div>
<div class="lyrico-lyrics-wrapper">pothu ellam aduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pothu ellam aduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">thotu thotu parka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thotu thotu parka "/>
</div>
<div class="lyrico-lyrics-wrapper">solli nenjam thavuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="solli nenjam thavuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">thakali thotathula korangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thakali thotathula korangu"/>
</div>
<div class="lyrico-lyrics-wrapper">pola thullathe munthana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pola thullathe munthana"/>
</div>
<div class="lyrico-lyrics-wrapper">muduchu avuka munne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muduchu avuka munne"/>
</div>
<div class="lyrico-lyrics-wrapper">vanthu thavathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanthu thavathe"/>
</div>
<div class="lyrico-lyrics-wrapper">muttatha jalli kattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muttatha jalli kattu"/>
</div>
<div class="lyrico-lyrics-wrapper">kaala nane muttama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaala nane muttama"/>
</div>
<div class="lyrico-lyrics-wrapper">mutikalam vaadi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mutikalam vaadi "/>
</div>
<div class="lyrico-lyrics-wrapper">inga poomane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inga poomane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">unna ethuthu munne nikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna ethuthu munne nikka"/>
</div>
<div class="lyrico-lyrics-wrapper">ipo inga yaru inga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ipo inga yaru inga"/>
</div>
<div class="lyrico-lyrics-wrapper">osthadu osthadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="osthadu osthadu"/>
</div>
<div class="lyrico-lyrics-wrapper">ipo osthadu osthadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ipo osthadu osthadu"/>
</div>
<div class="lyrico-lyrics-wrapper">osthadu osthadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="osthadu osthadu"/>
</div>
<div class="lyrico-lyrics-wrapper">inga osthadu osthadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inga osthadu osthadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">eduthu unga peruma solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eduthu unga peruma solla"/>
</div>
<div class="lyrico-lyrics-wrapper">enaku therunja solle ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enaku therunja solle ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">pathathu pathathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathathu pathathu"/>
</div>
<div class="lyrico-lyrics-wrapper">ada pathathu pathathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada pathathu pathathu"/>
</div>
<div class="lyrico-lyrics-wrapper">pathathu pathathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathathu pathathu"/>
</div>
<div class="lyrico-lyrics-wrapper">ada pathathu pathathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada pathathu pathathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">usar aiya usar konjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="usar aiya usar konjam"/>
</div>
<div class="lyrico-lyrics-wrapper">oram poi ukaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oram poi ukaru"/>
</div>
<div class="lyrico-lyrics-wrapper">kuthatam than nee aadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuthatam than nee aadu"/>
</div>
<div class="lyrico-lyrics-wrapper">ipo uttom aparam varathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ipo uttom aparam varathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">usar aiya usar konjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="usar aiya usar konjam"/>
</div>
<div class="lyrico-lyrics-wrapper">oram poi ukaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oram poi ukaru"/>
</div>
<div class="lyrico-lyrics-wrapper">kuthatam than nee aadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuthatam than nee aadu"/>
</div>
<div class="lyrico-lyrics-wrapper">ipo uttom aparam varathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ipo uttom aparam varathu"/>
</div>
</pre>
