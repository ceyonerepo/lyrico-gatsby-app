---
title: "andavane thunai song lyrics"
album: "Kolaigaran"
artist: "Simon K. King"
lyricist: "Arun Bharathi"
director: "Andrew Louis"
path: "/albums/kolaigaran-song-lyrics"
song: "Andavane Thunai"
image: ../../images/albumart/kolaigaran.jpg
date: 2019-06-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/VvOMpB0ktqs"
type: "mass"
singers:
  - Sivam
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Andavane Thunaiyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andavane Thunaiyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhirum Por Padaiyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhirum Por Padaiyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aayutham Yendhiduvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayutham Yendhiduvaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhagalam Purindhiduvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhagalam Purindhiduvaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andavane Thunaiyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andavane Thunaiyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhirum Por Padaiyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhirum Por Padaiyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aayutham Yendhiduvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayutham Yendhiduvaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhagalam Purindhiduvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhagalam Purindhiduvaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhigal Rendum Velaagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhigal Rendum Velaagum"/>
</div>
<div class="lyrico-lyrics-wrapper">Viralgal Paththum Melaagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viralgal Paththum Melaagum"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhiri Padaigal Thoolaagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhiri Padaigal Thoolaagum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan Modhave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Modhave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Narigal Kootam Naaraagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Narigal Kootam Naaraagum"/>
</div>
<div class="lyrico-lyrics-wrapper">Valaindha Kodu Neraagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valaindha Kodu Neraagum"/>
</div>
<div class="lyrico-lyrics-wrapper">Irudhi Varaikkum Poraadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irudhi Varaikkum Poraadum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan Vegame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Vegame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Heyy Oonaaigal Vaazhum Kootathile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy Oonaaigal Vaazhum Kootathile"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooyaadhu Ivanin Vettaigale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooyaadhu Ivanin Vettaigale"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappaamal Thappai Thaduthiduvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappaamal Thappai Thaduthiduvaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nyaayangal Meeraamale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nyaayangal Meeraamale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andavane Thunaiyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andavane Thunaiyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhirum Por Padaiyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhirum Por Padaiyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aayutham Yendhiduvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayutham Yendhiduvaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhagalam Purindhiduvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhagalam Purindhiduvaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andavane Thunaiyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andavane Thunaiyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhirum Por Padaiyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhirum Por Padaiyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aayutham Yendhiduvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayutham Yendhiduvaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhagalam Purindhiduvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhagalam Purindhiduvaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edhirum Puthirum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhirum Puthirum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan Thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Thaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhirum Seyalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhirum Seyalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan Thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Thaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Padhungi Paaindhu Vilaiyaadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padhungi Paaindhu Vilaiyaadum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan Aattame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Aattame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thadaigal Kodi Vanthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadaigal Kodi Vanthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Padaigal Nooru Nindraalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padaigal Nooru Nindraalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagadai Pottu Munnerum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagadai Pottu Munnerum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan Seetrame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Seetrame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karuvukkul Nee Poi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuvukkul Nee Poi"/>
</div>
<div class="lyrico-lyrics-wrapper">Olinthaalume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Olinthaalume"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalugaagi Thedum Ivan Kangale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalugaagi Thedum Ivan Kangale"/>
</div>
<div class="lyrico-lyrics-wrapper">Yemanoodum Ivan Sendru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yemanoodum Ivan Sendru"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilaiyaaduvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilaiyaaduvaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadum Puli Aattame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadum Puli Aattame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andavane Thunaiyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andavane Thunaiyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhirum Por Padaiyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhirum Por Padaiyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aayutham Yendhiduvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayutham Yendhiduvaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhagalam Purindhiduvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhagalam Purindhiduvaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andavane Thunaiyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andavane Thunaiyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunaiyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunaiyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhirum Por Padaiyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhirum Por Padaiyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Padaiyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padaiyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aayutham Yendhiduvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayutham Yendhiduvaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yendhiduvaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yendhiduvaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhagalam Purindhiduvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhagalam Purindhiduvaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andavane Thunaiyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andavane Thunaiyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhirum Por Padaiyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhirum Por Padaiyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aayutham Yendhiduvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayutham Yendhiduvaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhagalam Purindhiduvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhagalam Purindhiduvaan"/>
</div>
</pre>
