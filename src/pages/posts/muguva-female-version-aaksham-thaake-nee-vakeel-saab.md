---
title: "muguva female version song lyrics"
album: "Vakeel Saab"
artist: "S. Thaman"
lyricist: "Ramajogayya Sastry"
director: "Venu Sriram"
path: "/albums/vakeel-saab-lyrics"
song: "Muguva - Female Version - Aaksham Thaake Nee"
image: ../../images/albumart/vakeel-saab.jpg
date: 2021-04-09
lang: telugu
youtubeLink: "https://www.youtube.com/embed/cXH2hpEr1BI"
type: "melody"
singers:
  - Mohana Bhogaraju
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aaksham thaake nee aakrndhanalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaksham thaake nee aakrndhanalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasaara vinuvaarevaru…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasaara vinuvaarevaru…"/>
</div>
<div class="lyrico-lyrics-wrapper">Nitturpuna nalige nee gundela dhigulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nitturpuna nalige nee gundela dhigulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Savarinche manavaarevaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Savarinche manavaarevaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Kala maaruthunna jeevitham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kala maaruthunna jeevitham"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalathaloki jaarenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalathaloki jaarenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalaluganna kanulaku neeti chemma thgilena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalaluganna kanulaku neeti chemma thgilena"/>
</div>
<div class="lyrico-lyrics-wrapper">Veluthuraina prathi dhinam chuputhondha vedhana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veluthuraina prathi dhinam chuputhondha vedhana"/>
</div>
<div class="lyrico-lyrics-wrapper">Andhamaina bathukuna alajadi chelaregena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhamaina bathukuna alajadi chelaregena"/>
</div>
<div class="lyrico-lyrics-wrapper">Emiti nee paapam emiti nee neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emiti nee paapam emiti nee neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheekati musirindhe chitikellona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheekati musirindhe chitikellona"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeradhu nee shokam maaradhu ee lokam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeradhu nee shokam maaradhu ee lokam"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharamulu ennaina nee katha inthena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharamulu ennaina nee katha inthena"/>
</div>
<div class="lyrico-lyrics-wrapper">Maguva maguva nee manasuku ledha ye viluva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maguva maguva nee manasuku ledha ye viluva"/>
</div>
<div class="lyrico-lyrics-wrapper">Maguva maguva nee thalaraathalo chirunavvulu kalava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maguva maguva nee thalaraathalo chirunavvulu kalava"/>
</div>
<div class="lyrico-lyrics-wrapper">Alusuga chustharu lokuva chestharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alusuga chustharu lokuva chestharu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aandhi kaalanga abalave nuvvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aandhi kaalanga abalave nuvvu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nindhalu vestharu ninu veli vestharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nindhalu vestharu ninu veli vestharu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadadhigaa nuvvu porabadi puttavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadadhigaa nuvvu porabadi puttavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maguva maguva nee manasuku ledha ye viluva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maguva maguva nee manasuku ledha ye viluva"/>
</div>
<div class="lyrico-lyrics-wrapper">Maguva maguva nee thalaraathalo chirunavvulu kalava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maguva maguva nee thalaraathalo chirunavvulu kalava"/>
</div>
</pre>
