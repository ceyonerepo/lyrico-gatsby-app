---
title: "kaatril eeram song lyrics"
album: "Veppam"
artist: "Joshua Sridhar"
lyricist: "Na. Muthukumar"
director: "Anjana"
path: "/albums/veppam-lyrics"
song: "Kaatril Eeram"
image: ../../images/albumart/veppam.jpg
date: 2011-07-29
lang: tamil
youtubeLink: "https://www.youtube.com/embed/WC8qRHU9MKY"
type: "happy"
singers:
  - Karthik
  - Sricharan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kaattril Eeram Adhai Yaar Thanthadho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattril Eeram Adhai Yaar Thanthadho"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalgal Nadanam Ida Yaar Sonnadho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalgal Nadanam Ida Yaar Sonnadho"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhoomi Mudiyum Andha Ellai Varai Povomaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhoomi Mudiyum Andha Ellai Varai Povomaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Netru Naalai Adhu Poiyaanadho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netru Naalai Adhu Poiyaanadho"/>
</div>
<div class="lyrico-lyrics-wrapper">Indru Mattum Ingu Mei Aanadho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indru Mattum Ingu Mei Aanadho"/>
</div>
<div class="lyrico-lyrics-wrapper">Megam Alaiyum Andha Vaanam Varai Povomaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megam Alaiyum Andha Vaanam Varai Povomaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indrenna Ithanai Inbam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indrenna Ithanai Inbam"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhaya Koottil Neendhiduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhaya Koottil Neendhiduthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadai Paathai Pookkal Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadai Paathai Pookkal Ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaigal Neettiduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaigal Neettiduthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Neengaatha Punnagai Ondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neengaatha Punnagai Ondru"/>
</div>
<div class="lyrico-lyrics-wrapper">Udhattin Meelae Poothiduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udhattin Meelae Poothiduthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhkaiyai Pidikkirathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkaiyai Pidikkirathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaattril Eeram Adhai Yaar Thanthadho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattril Eeram Adhai Yaar Thanthadho"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalgal Nadanam Ida Yaar Sonnadho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalgal Nadanam Ida Yaar Sonnadho"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhoomi Mudiyum Andha Ellai Varai Povomaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhoomi Mudiyum Andha Ellai Varai Povomaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Netru Naalai Adhu Poiyaanadho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netru Naalai Adhu Poiyaanadho"/>
</div>
<div class="lyrico-lyrics-wrapper">Indru Mattum Ingu Mei Aanadho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indru Mattum Ingu Mei Aanadho"/>
</div>
<div class="lyrico-lyrics-wrapper">Megam Alaiyum Andha Vaanam Varai Povomaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megam Alaiyum Andha Vaanam Varai Povomaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Oru Naal Indha Oru Naal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Oru Naal Indha Oru Naal"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirodu Irundhaalum Vaazhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirodu Irundhaalum Vaazhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Payanam Indha Payanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Payanam Indha Payanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu Thodarnthida Vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu Thodarnthida Vendum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arugil Unadharugil Naan Vaazhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arugil Unadharugil Naan Vaazhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nigazh Kaalam Podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nigazh Kaalam Podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nimidam Indha Nimidam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nimidam Indha Nimidam"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Uraindhida Vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Uraindhida Vendum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hmm Mounathil Sila Neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm Mounathil Sila Neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayakkathil Sila Neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayakkathil Sila Neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Thayakkathil Sila Neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thayakkathil Sila Neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Ennavo Pudhu Unarvingae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Ennavo Pudhu Unarvingae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannarugil Sila Thooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannarugil Sila Thooram"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai Arugil Sila Thooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai Arugil Sila Thooram"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhi Thunaiyai Kaetkirathae Vaa Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi Thunaiyai Kaetkirathae Vaa Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Nam Nenjathin Oram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Nam Nenjathin Oram"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Ingu Ithanai Eeram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Ingu Ithanai Eeram"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Nam Kangalil Oram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Nam Kangalil Oram"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa Pudhu Kanavugal Oorum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Pudhu Kanavugal Oorum"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Enna Idhu Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Enna Idhu Enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha Naal Thaan Thirunaalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Naal Thaan Thirunaalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idharkaaga Idharkkaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idharkaaga Idharkkaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathirunthom Vegu Naalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathirunthom Vegu Naalaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indrenna Ithanai Inbam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indrenna Ithanai Inbam"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhaya Koottil Neendhiduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhaya Koottil Neendhiduthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadai Paathai Pookkal Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadai Paathai Pookkal Ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaigal Neettiduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaigal Neettiduthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Neengaatha Punnagai Ondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neengaatha Punnagai Ondru"/>
</div>
<div class="lyrico-lyrics-wrapper">Udhattin Meelae Poothiduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udhattin Meelae Poothiduthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhkaiyai Pidikkirathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkaiyai Pidikkirathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaattril Eeram Adhai Yaar Thanthadho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattril Eeram Adhai Yaar Thanthadho"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalgal Nadanam Ida Yaar Sonnadho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalgal Nadanam Ida Yaar Sonnadho"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhoomi Mudiyum Andha Ellai Varai Povomaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhoomi Mudiyum Andha Ellai Varai Povomaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Netru Naalai Adhu Poiyaanadho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netru Naalai Adhu Poiyaanadho"/>
</div>
<div class="lyrico-lyrics-wrapper">Indru Mattum Ingu Mei Aanadho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indru Mattum Ingu Mei Aanadho"/>
</div>
<div class="lyrico-lyrics-wrapper">Megam Alaiyum Andha Vaanam Varai Povomaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megam Alaiyum Andha Vaanam Varai Povomaa"/>
</div>
</pre>
