---
title: "aathadi song lyrics"
album: "Sagaa"
artist: "Shabir"
lyricist: "Shabir"
director: "Murugesh"
path: "/albums/sagaa-lyrics"
song: "Aathadi"
image: ../../images/albumart/sagaa.jpg
date: 2019-02-01
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Rg2_IapyHro"
type: "love"
singers:
  - Shabir
  - Rita
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aathadi Enna Maayamadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aathadi Enna Maayamadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Koothadi Manam Paayuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koothadi Manam Paayuthadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thannikkulla Thavazhura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannikkulla Thavazhura"/>
</div>
<div class="lyrico-lyrics-wrapper">Meena Tholanjan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meena Tholanjan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nethiyila Vervaiya Naanae Karainjen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nethiyila Vervaiya Naanae Karainjen"/>
</div>
<div class="lyrico-lyrics-wrapper">Othayadi Paathayil Thaana Nadanthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Othayadi Paathayil Thaana Nadanthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathirikki Nadavula Noola Kedanthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathirikki Nadavula Noola Kedanthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kattikida Kathuthara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattikida Kathuthara"/>
</div>
<div class="lyrico-lyrics-wrapper">Naana Varava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naana Varava"/>
</div>
<div class="lyrico-lyrics-wrapper">Thotta Idam Muthamida Aasai Varutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thotta Idam Muthamida Aasai Varutha"/>
</div>
<div class="lyrico-lyrics-wrapper">Muthamitta Pathikkumae Poda Badava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthamitta Pathikkumae Poda Badava"/>
</div>
<div class="lyrico-lyrics-wrapper">Uchamthala Ullukulla Sooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uchamthala Ullukulla Sooda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Kaadhal Seiya Vandhalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Kaadhal Seiya Vandhalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Koochamellam Koodathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koochamellam Koodathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kekkum Munnae Thanthalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kekkum Munnae Thanthalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yekkam Ellam Paravaiya Parakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yekkam Ellam Paravaiya Parakkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Ennamo Manthiram Senjayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Ennamo Manthiram Senjayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennamo Thanthiram Senjayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennamo Thanthiram Senjayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nethili Pola Thullurandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nethili Pola Thullurandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoondil Pottu Pidikka Nee Vaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoondil Pottu Pidikka Nee Vaadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aathadi Enna Maayamadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aathadi Enna Maayamadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Koothadi Manam Paayuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koothadi Manam Paayuthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aathadi Enna Maayamadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aathadi Enna Maayamadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Koothadi Manam Paayuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koothadi Manam Paayuthadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mkkum Mkkum Mkkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mkkum Mkkum Mkkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mkkum Mkkum Mkkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mkkum Mkkum Mkkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mkkum Mkkum Mkkumha Ha Ha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mkkum Mkkum Mkkumha Ha Ha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Iduppu Azhaga Kaatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Iduppu Azhaga Kaatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha Oodhari Paya Manasa Kedutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Oodhari Paya Manasa Kedutha"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Killanum Killanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Killanum Killanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Illa Allanum Allanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illa Allanum Allanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Hahaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hahaha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Poda Poda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Poda Poda"/>
</div>
<div class="lyrico-lyrics-wrapper">Paya Buththi Ippadithanae Pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paya Buththi Ippadithanae Pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam Adanga Maatiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Adanga Maatiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konjam Adakki Kaattudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Adakki Kaattudi"/>
</div>
<div class="lyrico-lyrics-wrapper">En Iduppa Suthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Iduppa Suthi"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kaiyi Povathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kaiyi Povathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Alanthu Vechathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alanthu Vechathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha Andavan Thappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Andavan Thappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Uthatta Suthi Un Kannu Povathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uthatta Suthi Un Kannu Povathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Venaandi Ithu Vambaagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venaandi Ithu Vambaagum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aarapodu Thaangathamma Aarapodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aarapodu Thaangathamma Aarapodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Thoondil Pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Thoondil Pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Izhukka Nee Vaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Izhukka Nee Vaadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aathadi Enna Maayamadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aathadi Enna Maayamadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Koothadi Manam Paayuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koothadi Manam Paayuthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aathadi Enna Maayamadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aathadi Enna Maayamadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Koothadi Manam Paayuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koothadi Manam Paayuthadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaaaaaaaaaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaaaaaaaaaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaaaaaaaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaaaaaaaaaaa"/>
</div>
</pre>
