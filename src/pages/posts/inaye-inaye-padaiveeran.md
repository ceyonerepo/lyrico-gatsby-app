---
title: "inaye inaye song lyrics"
album: "Padaiveeran"
artist: "Karthik Raja"
lyricist: "Dhana Sekaran"
director: "Dhana Sekaran"
path: "/albums/padaiveeran-lyrics"
song: "Inaye Inaye"
image: ../../images/albumart/padaiveeran.jpg
date: 2018-02-02
lang: tamil
youtubeLink: "https://www.youtube.com/embed/DSgxTYWOr-U"
type: "sad"
singers:
  - Vijay Yesudas
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Inaiyae inaiyae ini neethaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inaiyae inaiyae ini neethaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai nee anaithaai un thogaiyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai nee anaithaai un thogaiyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kaadhalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kaadhalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un paarvai theendavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un paarvai theendavae"/>
</div>
<div class="lyrico-lyrics-wrapper">O ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O ooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Indru meendum piranthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indru meendum piranthen"/>
</div>
<div class="lyrico-lyrics-wrapper">En thaayin madiyil thavazhnthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En thaayin madiyil thavazhnthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakae enakaeUnai thaaraaiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakae enakaeUnai thaaraaiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidiyum thisaiyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidiyum thisaiyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee vaarayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee vaarayo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un tholil saaigiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un tholil saaigiren"/>
</div>
<div class="lyrico-lyrics-wrapper">En vaazhvai vaazhgiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En vaazhvai vaazhgiren"/>
</div>
<div class="lyrico-lyrics-wrapper">O oooo ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O oooo ooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Indru meendum piranthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indru meendum piranthen"/>
</div>
<div class="lyrico-lyrics-wrapper">En thaayin madiyil thavazhnthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En thaayin madiyil thavazhnthen"/>
</div>
</pre>
