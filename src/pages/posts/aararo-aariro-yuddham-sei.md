---
title: "aararo aariro song lyrics"
album: "Yuddham Sei"
artist: "Krishnakumar"
lyricist: "Kabilan"
director: "Mysskin"
path: "/albums/yuddham-sei-lyrics"
song: "Aararo Aariro"
image: ../../images/albumart/yuddham-sei.jpg
date: 2011-02-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/DL9UkJZ5HCM"
type: "melody"
singers:
  - Mysskin
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aaraaro aariro nee vero naan vero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraaro aariro nee vero naan vero"/>
</div>
<div class="lyrico-lyrics-wrapper">kannukkul kai veesum aanandhakkanneero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannukkul kai veesum aanandhakkanneero"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaraaro aariro nee vero naan vero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraaro aariro nee vero naan vero"/>
</div>
<div class="lyrico-lyrics-wrapper">kannukkul kai veesum aanandhakkanneero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannukkul kai veesum aanandhakkanneero"/>
</div>
<div class="lyrico-lyrics-wrapper">poovin nizhalo pullaanguzhalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poovin nizhalo pullaanguzhalo"/>
</div>
<div class="lyrico-lyrics-wrapper">thandhai thoalil neer thoongum thaaley thaalelo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thandhai thoalil neer thoongum thaaley thaalelo"/>
</div>
<div class="lyrico-lyrics-wrapper">minnal melivo annal mozhiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="minnal melivo annal mozhiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">kannukkulley aanandham pongum thaen mazhaiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannukkulley aanandham pongum thaen mazhaiyo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaraaro aariro nee vero naan vero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraaro aariro nee vero naan vero"/>
</div>
<div class="lyrico-lyrics-wrapper">kannukkul kai veesum aanandhakkanneero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannukkul kai veesum aanandhakkanneero"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaraaro aariro nee vero naan vero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraaro aariro nee vero naan vero"/>
</div>
<div class="lyrico-lyrics-wrapper">kannukkul kai veesum aanandhakkanneero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannukkul kai veesum aanandhakkanneero"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">O...... aaroara neeroada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O...... aaroara neeroada"/>
</div>
<div class="lyrico-lyrics-wrapper">aanandha thaer aada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aanandha thaer aada"/>
</div>
<div class="lyrico-lyrics-wrapper">pen poovai pootti vaippen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pen poovai pootti vaippen"/>
</div>
<div class="lyrico-lyrics-wrapper">kannukkulley veroada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannukkulley veroada"/>
</div>
<div class="lyrico-lyrics-wrapper">neeppesum pechellaam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeppesum pechellaam "/>
</div>
<div class="lyrico-lyrics-wrapper">thaai pesum pechammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaai pesum pechammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">unnai nan paarkkaavittaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai nan paarkkaavittaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Odippoagaum moochamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odippoagaum moochamma"/>
</div>
<div class="lyrico-lyrics-wrapper">neeppOagum idamellaam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeppOagum idamellaam "/>
</div>
<div class="lyrico-lyrics-wrapper">nizhalaaga varuveney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nizhalaaga varuveney"/>
</div>
<div class="lyrico-lyrics-wrapper">nee ketkum ulagaththai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee ketkum ulagaththai "/>
</div>
<div class="lyrico-lyrics-wrapper">naan vaangiththaruveney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan vaangiththaruveney"/>
</div>
<div class="lyrico-lyrics-wrapper">ulalodu uyiraaga unnai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulalodu uyiraaga unnai "/>
</div>
<div class="lyrico-lyrics-wrapper">poarththi vaippeney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poarththi vaippeney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">poovin nizhalo pullaanguzhalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poovin nizhalo pullaanguzhalo"/>
</div>
<div class="lyrico-lyrics-wrapper">thandhai thoalil neer thoongum thaaley thaalelo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thandhai thoalil neer thoongum thaaley thaalelo"/>
</div>
<div class="lyrico-lyrics-wrapper">minnal melivo annal mozhiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="minnal melivo annal mozhiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">kannukkulley aanandham pongum thaen mazhaiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannukkulley aanandham pongum thaen mazhaiyo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaraaro aariro nee vero naan vero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraaro aariro nee vero naan vero"/>
</div>
<div class="lyrico-lyrics-wrapper">kannukkul kai veesum aanandhakkanneero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannukkul kai veesum aanandhakkanneero"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaraaro aariro nee vero naan vero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraaro aariro nee vero naan vero"/>
</div>
<div class="lyrico-lyrics-wrapper">kannukkul kai veesum aanandhakkanneero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannukkul kai veesum aanandhakkanneero"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaiveesu kaiveesu kaatraippoal kaiveesu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiveesu kaiveesu kaatraippoal kaiveesu"/>
</div>
<div class="lyrico-lyrics-wrapper">theroadum veedhiyengum theivam pola neeppesu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theroadum veedhiyengum theivam pola neeppesu"/>
</div>
<div class="lyrico-lyrics-wrapper">annaandhu paarththaaley aagaaya poo nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="annaandhu paarththaaley aagaaya poo nee"/>
</div>
<div class="lyrico-lyrics-wrapper">man meedhu paarkkumbOadhu manjal nilaa nee thaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="man meedhu paarkkumbOadhu manjal nilaa nee thaaney"/>
</div>
<div class="lyrico-lyrics-wrapper">thaayillaa kurai theera unai naanum valartheney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaayillaa kurai theera unai naanum valartheney"/>
</div>
<div class="lyrico-lyrics-wrapper">neeyillaa idamellaam nenjukkul azhuveney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeyillaa idamellaam nenjukkul azhuveney"/>
</div>
<div class="lyrico-lyrics-wrapper">enakkaaga pirandhaaye enna thavam seitheno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enakkaaga pirandhaaye enna thavam seitheno"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">poovin nizhalo pullaanguzhalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poovin nizhalo pullaanguzhalo"/>
</div>
<div class="lyrico-lyrics-wrapper">thandhai thoalil neer thoongum thaaley thaalelo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thandhai thoalil neer thoongum thaaley thaalelo"/>
</div>
<div class="lyrico-lyrics-wrapper">minnal melivo annal mozhiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="minnal melivo annal mozhiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">kannukkulley aanandham pongum thaen mazhaiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannukkulley aanandham pongum thaen mazhaiyo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaraaro aariro nee vero naan vero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraaro aariro nee vero naan vero"/>
</div>
<div class="lyrico-lyrics-wrapper">kannukkul kai veesum aanandhakkanneero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannukkul kai veesum aanandhakkanneero"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaraaro aariro nee vero naan vero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraaro aariro nee vero naan vero"/>
</div>
<div class="lyrico-lyrics-wrapper">kannukkul kai veesum aanandhakkanneero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannukkul kai veesum aanandhakkanneero"/>
</div>
</pre>
