---
title: "suthi irukku song lyrics"
album: "Kannakkol"
artist: "Bobby"
lyricist: "Muthuvijayan"
director: "V.A.Kumaresan"
path: "/albums/kannakkol-lyrics"
song: "Suthi Irukku"
image: ../../images/albumart/kannakkol.jpg
date: 2018-06-15
lang: tamil
youtubeLink: "https://www.youtube.com/embed/yq2UmHZojaU"
type: "sad"
singers:
  - Pavan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">suthi irukku ettu thisa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suthi irukku ettu thisa"/>
</div>
<div class="lyrico-lyrics-wrapper">than thisa than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="than thisa than"/>
</div>
<div class="lyrico-lyrics-wrapper">entha thisaiyila nadaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="entha thisaiyila nadaka"/>
</div>
<div class="lyrico-lyrics-wrapper">thembu irukku nenju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thembu irukku nenju"/>
</div>
<div class="lyrico-lyrics-wrapper">vasam than vasam than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vasam than vasam than"/>
</div>
<div class="lyrico-lyrics-wrapper">antha vaanatha valaika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antha vaanatha valaika"/>
</div>
<div class="lyrico-lyrics-wrapper">kaatukulle koodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatukulle koodu"/>
</div>
<div class="lyrico-lyrics-wrapper">katuvathu illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="katuvathu illai"/>
</div>
<div class="lyrico-lyrics-wrapper">engum thiriyum athu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engum thiriyum athu"/>
</div>
<div class="lyrico-lyrics-wrapper">thanguvathu illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanguvathu illa"/>
</div>
<div class="lyrico-lyrics-wrapper">nadaka nadaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nadaka nadaka"/>
</div>
<div class="lyrico-lyrics-wrapper">patha porakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="patha porakkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">suthi irukku ettu thisa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suthi irukku ettu thisa"/>
</div>
<div class="lyrico-lyrics-wrapper">than thisa than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="than thisa than"/>
</div>
<div class="lyrico-lyrics-wrapper">entha thisaiyila nadaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="entha thisaiyila nadaka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">eeyum erumbum vaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eeyum erumbum vaala"/>
</div>
<div class="lyrico-lyrics-wrapper">intha mannil edam irum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha mannil edam irum"/>
</div>
<div class="lyrico-lyrics-wrapper">rendu ullam kaalam thalla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rendu ullam kaalam thalla"/>
</div>
<div class="lyrico-lyrics-wrapper">edam irukaatha inga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edam irukaatha inga"/>
</div>
<div class="lyrico-lyrics-wrapper">kaaka kuruvi thinga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaaka kuruvi thinga"/>
</div>
<div class="lyrico-lyrics-wrapper">kambu solam vilanjuruka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kambu solam vilanjuruka"/>
</div>
<div class="lyrico-lyrics-wrapper">aela patta jeevanuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aela patta jeevanuku"/>
</div>
<div class="lyrico-lyrics-wrapper">pathar irukatha inga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathar irukatha inga"/>
</div>
<div class="lyrico-lyrics-wrapper">kaasuku than panjam undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaasuku than panjam undu"/>
</div>
<div class="lyrico-lyrics-wrapper">paasathuku illaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paasathuku illaye"/>
</div>
<div class="lyrico-lyrics-wrapper">anba thavira veru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anba thavira veru"/>
</div>
<div class="lyrico-lyrics-wrapper">sothu pathum illaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sothu pathum illaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">adada adada ithu kaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adada adada ithu kaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">vaakil pogum vaalgaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaakil pogum vaalgaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">suthi irukku ettu thisa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suthi irukku ettu thisa"/>
</div>
<div class="lyrico-lyrics-wrapper">than thisa than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="than thisa than"/>
</div>
<div class="lyrico-lyrics-wrapper">entha thisaiyila nadaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="entha thisaiyila nadaka"/>
</div>
<div class="lyrico-lyrics-wrapper">thembu irukku nenju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thembu irukku nenju"/>
</div>
<div class="lyrico-lyrics-wrapper">vasam than vasam than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vasam than vasam than"/>
</div>
<div class="lyrico-lyrics-wrapper">antha vaanatha valaika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antha vaanatha valaika"/>
</div>
</pre>
