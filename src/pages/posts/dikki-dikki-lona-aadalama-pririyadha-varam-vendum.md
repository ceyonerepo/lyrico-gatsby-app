---
title: "dikki dikki lona aadalama song lyrics"
album: "Pririyadha Varam Vendum"
artist: "S. A. Rajkumar"
lyricist: "Pa. Vijay"
director: "Kamal"
path: "/albums/pririyadha-varam-vendum-lyrics"
song: "Dikki Dikki Lona Aadalama"
image: ../../images/albumart/piriyadha-varam-vendum.jpg
date: 2001-02-16
lang: tamil
youtubeLink: "https://www.youtube.com/embed/EQhsaxqP1X8"
type: "celebration"
singers:
  - Mano
  - Sujatha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">dikki dikki lona aadalama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dikki dikki lona aadalama"/>
</div>
<div class="lyrico-lyrics-wrapper">thillai kani gana padalama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thillai kani gana padalama"/>
</div>
<div class="lyrico-lyrics-wrapper">dik dik love ah solalama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dik dik love ah solalama"/>
</div>
<div class="lyrico-lyrics-wrapper">teenage ponna velalama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="teenage ponna velalama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">lkg paiyan bsc ponna look onnu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lkg paiyan bsc ponna look onnu "/>
</div>
<div class="lyrico-lyrics-wrapper">vidura  kaalam idhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidura  kaalam idhu"/>
</div>
<div class="lyrico-lyrics-wrapper">ponnoda manasa aaraichi panna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponnoda manasa aaraichi panna"/>
</div>
<div class="lyrico-lyrics-wrapper">computer kekum kaalam idhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="computer kekum kaalam idhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">biscut vennila scut pottu than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="biscut vennila scut pottu than"/>
</div>
<div class="lyrico-lyrics-wrapper">rocket parvaigal veesiyathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rocket parvaigal veesiyathu"/>
</div>
<div class="lyrico-lyrics-wrapper">laali maamiku madisaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="laali maamiku madisaru"/>
</div>
<div class="lyrico-lyrics-wrapper">namma mami ponnuku suditharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="namma mami ponnuku suditharu"/>
</div>
<div class="lyrico-lyrics-wrapper">laali maamiku madisaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="laali maamiku madisaru"/>
</div>
<div class="lyrico-lyrics-wrapper">namma mami ponnuku suditharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="namma mami ponnuku suditharu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">lkg paiyan bsc ponna look onnu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lkg paiyan bsc ponna look onnu "/>
</div>
<div class="lyrico-lyrics-wrapper">vidura  kaalam idhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidura  kaalam idhu"/>
</div>
<div class="lyrico-lyrics-wrapper">ponnoda manasa aaraichi panna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponnoda manasa aaraichi panna"/>
</div>
<div class="lyrico-lyrics-wrapper">computer kekum kaalam idhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="computer kekum kaalam idhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">dikki dikki lona aadalama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dikki dikki lona aadalama"/>
</div>
<div class="lyrico-lyrics-wrapper">thillai kani gana padalama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thillai kani gana padalama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">cut aduchu pona pothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cut aduchu pona pothu"/>
</div>
<div class="lyrico-lyrics-wrapper">attanance sollum enga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="attanance sollum enga"/>
</div>
<div class="lyrico-lyrics-wrapper">natpu thane hero aaguthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="natpu thane hero aaguthu"/>
</div>
<div class="lyrico-lyrics-wrapper">loverku kammal vangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="loverku kammal vangi"/>
</div>
<div class="lyrico-lyrics-wrapper">picharuku ticket vangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="picharuku ticket vangi"/>
</div>
<div class="lyrico-lyrics-wrapper">enga appan kaasu poguthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga appan kaasu poguthu"/>
</div>
<div class="lyrico-lyrics-wrapper">kalyana naal maranthalume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalyana naal maranthalume"/>
</div>
<div class="lyrico-lyrics-wrapper">kalyana naal maranthalume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalyana naal maranthalume"/>
</div>
<div class="lyrico-lyrics-wrapper">kalloori naal marakathamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalloori naal marakathamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kalyana naal maranthalume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalyana naal maranthalume"/>
</div>
<div class="lyrico-lyrics-wrapper">kalloori naal marakathamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalloori naal marakathamma"/>
</div>
<div class="lyrico-lyrics-wrapper">deepavali kottattamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="deepavali kottattamum"/>
</div>
<div class="lyrico-lyrics-wrapper">college day pol aaguma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="college day pol aaguma"/>
</div>
<div class="lyrico-lyrics-wrapper">thaniyaga oor ulagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaniyaga oor ulagam"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu thane nam sorgam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu thane nam sorgam"/>
</div>
<div class="lyrico-lyrics-wrapper">jaathi ethuvum ingu illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jaathi ethuvum ingu illa"/>
</div>
<div class="lyrico-lyrics-wrapper">ada indhu muslim christhu illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada indhu muslim christhu illa"/>
</div>
<div class="lyrico-lyrics-wrapper">jaathi ethuvum ingu illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jaathi ethuvum ingu illa"/>
</div>
<div class="lyrico-lyrics-wrapper">ada indhu muslim christhu illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada indhu muslim christhu illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">lkg paiyan bsc ponna look onnu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lkg paiyan bsc ponna look onnu "/>
</div>
<div class="lyrico-lyrics-wrapper">vidura  kaalam idhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidura  kaalam idhu"/>
</div>
<div class="lyrico-lyrics-wrapper">ponnoda manasa aaraichi panna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponnoda manasa aaraichi panna"/>
</div>
<div class="lyrico-lyrics-wrapper">computer kekum kaalam idhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="computer kekum kaalam idhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">mickdisan ponnuku oru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mickdisan ponnuku oru "/>
</div>
<div class="lyrico-lyrics-wrapper">love letter eleuthira 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="love letter eleuthira "/>
</div>
<div class="lyrico-lyrics-wrapper">dhill iruntha super than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhill iruntha super than"/>
</div>
<div class="lyrico-lyrics-wrapper">english professor ah 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="english professor ah "/>
</div>
<div class="lyrico-lyrics-wrapper">thirukural solla solli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thirukural solla solli"/>
</div>
<div class="lyrico-lyrics-wrapper">strike panna super than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="strike panna super than"/>
</div>
<div class="lyrico-lyrics-wrapper">ladies hostel munnala than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ladies hostel munnala than"/>
</div>
<div class="lyrico-lyrics-wrapper">room iruntha super than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="room iruntha super than"/>
</div>
<div class="lyrico-lyrics-wrapper">lover ku thangai unda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lover ku thangai unda"/>
</div>
<div class="lyrico-lyrics-wrapper">life oh endrum super than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="life oh endrum super than"/>
</div>
<div class="lyrico-lyrics-wrapper">college a joke ellame super than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="college a joke ellame super than"/>
</div>
<div class="lyrico-lyrics-wrapper">college life oru taste
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="college life oru taste"/>
</div>
<div class="lyrico-lyrics-wrapper">ada kalyana life athu waste
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada kalyana life athu waste"/>
</div>
<div class="lyrico-lyrics-wrapper">college life oru taste
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="college life oru taste"/>
</div>
<div class="lyrico-lyrics-wrapper">ada kalyana life athu waste
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada kalyana life athu waste"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">lkg paiyan bsc ponna look onnu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lkg paiyan bsc ponna look onnu "/>
</div>
<div class="lyrico-lyrics-wrapper">vidura  kaalam idhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidura  kaalam idhu"/>
</div>
<div class="lyrico-lyrics-wrapper">ponnoda manasa aaraichi panna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponnoda manasa aaraichi panna"/>
</div>
<div class="lyrico-lyrics-wrapper">computer kekum kaalam idhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="computer kekum kaalam idhu"/>
</div>
<div class="lyrico-lyrics-wrapper">biscut vennila scut pottu than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="biscut vennila scut pottu than"/>
</div>
<div class="lyrico-lyrics-wrapper">rocket parvaigal veesiyathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rocket parvaigal veesiyathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">laali maamiku madisaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="laali maamiku madisaru"/>
</div>
<div class="lyrico-lyrics-wrapper">namma mami ponnuku suditharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="namma mami ponnuku suditharu"/>
</div>
<div class="lyrico-lyrics-wrapper">laali maamiku madisaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="laali maamiku madisaru"/>
</div>
<div class="lyrico-lyrics-wrapper">namma mami ponnuku suditharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="namma mami ponnuku suditharu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">dikki dikki lona aadalama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dikki dikki lona aadalama"/>
</div>
<div class="lyrico-lyrics-wrapper">thillai kani gana padalama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thillai kani gana padalama"/>
</div>
<div class="lyrico-lyrics-wrapper">dikki dikki lona aadalama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dikki dikki lona aadalama"/>
</div>
<div class="lyrico-lyrics-wrapper">thillai kani gana padalama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thillai kani gana padalama"/>
</div>
</pre>
