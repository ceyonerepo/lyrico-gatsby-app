---
title: "kala kala kala song lyrics"
album: "Marudhavelu"
artist: "James Vasanthan"
lyricist: "Na. Muthukumar"
director: "R.K.R. Aathimoolam"
path: "/albums/marudhavelu-lyrics"
song: "Kala Kala Kala"
image: ../../images/albumart/marudhavelu.jpg
date: 2011-11-18
lang: tamil
youtubeLink: "https://www.youtube.com/embed/5CXjV2p9JDU"
type: "happy"
singers:
  - Sunanthan
  - Sharanya Srinivas
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">etturu ketti melam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="etturu ketti melam"/>
</div>
<div class="lyrico-lyrics-wrapper">enga oora vanthu serum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga oora vanthu serum"/>
</div>
<div class="lyrico-lyrics-wrapper">kala kala kala 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kala kala kala "/>
</div>
<div class="lyrico-lyrics-wrapper">kalakura vidam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalakura vidam"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu kalangu adikira 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu kalangu adikira "/>
</div>
<div class="lyrico-lyrics-wrapper">kalangu adikira pulam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalangu adikira pulam"/>
</div>
<div class="lyrico-lyrics-wrapper">vela velakura vela 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vela velakura vela "/>
</div>
<div class="lyrico-lyrics-wrapper">velakura palam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="velakura palam"/>
</div>
<div class="lyrico-lyrics-wrapper">ne pagalil kooda 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ne pagalil kooda "/>
</div>
<div class="lyrico-lyrics-wrapper">pala palakura nilam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pala palakura nilam"/>
</div>
<div class="lyrico-lyrics-wrapper">en per velu oore kelu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en per velu oore kelu "/>
</div>
<div class="lyrico-lyrics-wrapper">ponna pathu saaya aalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponna pathu saaya aalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kala kala kala 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kala kala kala "/>
</div>
<div class="lyrico-lyrics-wrapper">kalakura kalla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalakura kalla"/>
</div>
<div class="lyrico-lyrics-wrapper">nan kalangu adikira 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan kalangu adikira "/>
</div>
<div class="lyrico-lyrics-wrapper">kalangu adikira pala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalangu adikira pala"/>
</div>
<div class="lyrico-lyrics-wrapper">vala valakuthu vala 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vala valakuthu vala "/>
</div>
<div class="lyrico-lyrics-wrapper">valukuthu sura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valukuthu sura"/>
</div>
<div class="lyrico-lyrics-wrapper">nan pagalil kooda pala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan pagalil kooda pala"/>
</div>
<div class="lyrico-lyrics-wrapper">palakura nila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="palakura nila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pakura thee kuchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pakura thee kuchi"/>
</div>
<div class="lyrico-lyrics-wrapper">kanna vachu pakura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanna vachu pakura"/>
</div>
<div class="lyrico-lyrics-wrapper">theeyuru nenju ipo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theeyuru nenju ipo"/>
</div>
<div class="lyrico-lyrics-wrapper">ne alaika alaika thee 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ne alaika alaika thee "/>
</div>
<div class="lyrico-lyrics-wrapper">anikum vandi venume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anikum vandi venume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">yengura koochatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yengura koochatha"/>
</div>
<div class="lyrico-lyrics-wrapper">vitu thalli vangura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vitu thalli vangura"/>
</div>
<div class="lyrico-lyrics-wrapper">ethatha pakum pothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethatha pakum pothu"/>
</div>
<div class="lyrico-lyrics-wrapper">ne anaika ne anaika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ne anaika ne anaika"/>
</div>
<div class="lyrico-lyrics-wrapper">ambulance vandi venume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ambulance vandi venume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">rendu kaalu man kuttiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rendu kaalu man kuttiye"/>
</div>
<div class="lyrico-lyrics-wrapper">un kan rendum meen kuttiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kan rendum meen kuttiye"/>
</div>
<div class="lyrico-lyrics-wrapper">rendu kaalu man kuttiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rendu kaalu man kuttiye"/>
</div>
<div class="lyrico-lyrics-wrapper">un kan rendum meen kuttiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kan rendum meen kuttiye"/>
</div>
<div class="lyrico-lyrics-wrapper">yentha ooru ponnu ne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yentha ooru ponnu ne"/>
</div>
<div class="lyrico-lyrics-wrapper">solli putu othingi nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="solli putu othingi nee"/>
</div>
<div class="lyrico-lyrics-wrapper">intha oru naalu un 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha oru naalu un "/>
</div>
<div class="lyrico-lyrics-wrapper">ooru thirunalu than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooru thirunalu than"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kala kala kala 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kala kala kala "/>
</div>
<div class="lyrico-lyrics-wrapper">kalakura vidam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalakura vidam"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu kalangu adikira 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu kalangu adikira "/>
</div>
<div class="lyrico-lyrics-wrapper">kalangu adikira pulam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalangu adikira pulam"/>
</div>
<div class="lyrico-lyrics-wrapper">vala valakuthu vala 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vala valakuthu vala "/>
</div>
<div class="lyrico-lyrics-wrapper">valukuthu sura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valukuthu sura"/>
</div>
<div class="lyrico-lyrics-wrapper">nan pagalil kooda pala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan pagalil kooda pala"/>
</div>
<div class="lyrico-lyrics-wrapper">palakura nila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="palakura nila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kannoda nellu vaipom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannoda nellu vaipom"/>
</div>
<div class="lyrico-lyrics-wrapper">nellu vaipom nellu vaipom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nellu vaipom nellu vaipom"/>
</div>
<div class="lyrico-lyrics-wrapper">malaiyodu karumbu vaipom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malaiyodu karumbu vaipom"/>
</div>
<div class="lyrico-lyrics-wrapper">karumbu vaipom karumbu vaipom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karumbu vaipom karumbu vaipom"/>
</div>
<div class="lyrico-lyrics-wrapper">kandiyoda vaala vaipom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kandiyoda vaala vaipom"/>
</div>
<div class="lyrico-lyrics-wrapper">vaala vaipom vaala vaipom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaala vaipom vaala vaipom"/>
</div>
<div class="lyrico-lyrics-wrapper">theroda thennai vaipom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theroda thennai vaipom"/>
</div>
<div class="lyrico-lyrics-wrapper">thennai vaipom thennai vaipom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thennai vaipom thennai vaipom"/>
</div>
<div class="lyrico-lyrics-wrapper">mupogam velaiyum ooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mupogam velaiyum ooru"/>
</div>
<div class="lyrico-lyrics-wrapper">epothum sirikum paru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="epothum sirikum paru"/>
</div>
<div class="lyrico-lyrics-wrapper">mupogam velaiyum ooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mupogam velaiyum ooru"/>
</div>
<div class="lyrico-lyrics-wrapper">epothum sirikum paru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="epothum sirikum paru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vaaduren aasayil pathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaaduren aasayil pathi"/>
</div>
<div class="lyrico-lyrics-wrapper">katti paduren nalaiki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="katti paduren nalaiki"/>
</div>
<div class="lyrico-lyrics-wrapper">vera ooru nan unaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vera ooru nan unaku"/>
</div>
<div class="lyrico-lyrics-wrapper">venumuna vandi katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="venumuna vandi katti"/>
</div>
<div class="lyrico-lyrics-wrapper">theda venume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theda venume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">paduren rasava pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paduren rasava pola"/>
</div>
<div class="lyrico-lyrics-wrapper">nanum valuren rapagal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanum valuren rapagal"/>
</div>
<div class="lyrico-lyrics-wrapper">betham indri nan kidaika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="betham indri nan kidaika"/>
</div>
<div class="lyrico-lyrics-wrapper">nan kidaika neyum thavam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan kidaika neyum thavam"/>
</div>
<div class="lyrico-lyrics-wrapper">seiya venume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seiya venume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">minar vetu rasa rasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="minar vetu rasa rasa"/>
</div>
<div class="lyrico-lyrics-wrapper">nan oru naalu pookum rosa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan oru naalu pookum rosa"/>
</div>
<div class="lyrico-lyrics-wrapper">minar vetu rasa rasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="minar vetu rasa rasa"/>
</div>
<div class="lyrico-lyrics-wrapper">nan oru naalu pookum rosa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan oru naalu pookum rosa"/>
</div>
<div class="lyrico-lyrics-wrapper">en nilala pidika than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en nilala pidika than"/>
</div>
<div class="lyrico-lyrics-wrapper">ambalaikul sanda than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ambalaikul sanda than"/>
</div>
<div class="lyrico-lyrics-wrapper">ooru thirunalu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooru thirunalu "/>
</div>
<div class="lyrico-lyrics-wrapper">vendame thagararu than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vendame thagararu than"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kala kala kala 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kala kala kala "/>
</div>
<div class="lyrico-lyrics-wrapper">kalakura vidam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalakura vidam"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu kalangu adikira 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu kalangu adikira "/>
</div>
<div class="lyrico-lyrics-wrapper">kalangu adikira pulam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalangu adikira pulam"/>
</div>
</pre>
