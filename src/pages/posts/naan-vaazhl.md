---
title: "naan song lyrics"
album: "Vaazhl"
artist: "Pradeep Kumar"
lyricist: "Kutti Revathi"
director: "Arun Prabu Purushothaman"
path: "/albums/vaazhl-lyrics"
song: "Naan"
image: ../../images/albumart/vaazhl.jpg
date: 2021-07-16
lang: tamil
youtubeLink: "https://www.youtube.com/embed/8_PruIXWWy4"
type: "happy"
singers:
  - Pradeep Kumar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">naan verum elumbu than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan verum elumbu than"/>
</div>
<div class="lyrico-lyrics-wrapper">un kooda aadi paada 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kooda aadi paada "/>
</div>
<div class="lyrico-lyrics-wrapper">oda varuvana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oda varuvana"/>
</div>
<div class="lyrico-lyrics-wrapper">varuvana varuvana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varuvana varuvana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">naan varum irumbu than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan varum irumbu than"/>
</div>
<div class="lyrico-lyrics-wrapper">un kaandha mogam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kaandha mogam"/>
</div>
<div class="lyrico-lyrics-wrapper">thedi otikiruvana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thedi otikiruvana"/>
</div>
<div class="lyrico-lyrics-wrapper">kiruvana kiruvana kiruvana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kiruvana kiruvana kiruvana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">paathaiya  maranthu than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paathaiya  maranthu than"/>
</div>
<div class="lyrico-lyrics-wrapper">un podha theera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un podha theera"/>
</div>
<div class="lyrico-lyrics-wrapper">eeri meeri eluvana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eeri meeri eluvana"/>
</div>
<div class="lyrico-lyrics-wrapper">eluvana eluvana eluvana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eluvana eluvana eluvana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaadhaiyum kodanji than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadhaiyum kodanji than"/>
</div>
<div class="lyrico-lyrics-wrapper">un moolaikulla panji
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un moolaikulla panji"/>
</div>
<div class="lyrico-lyrics-wrapper">olinju kidapana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="olinju kidapana"/>
</div>
<div class="lyrico-lyrics-wrapper">kidapana kidapana kidapana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kidapana kidapana kidapana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">manthaiyil or aadu than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manthaiyil or aadu than"/>
</div>
<div class="lyrico-lyrics-wrapper">un pandha paasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un pandha paasam"/>
</div>
<div class="lyrico-lyrics-wrapper">thinnu eppam vuduvana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thinnu eppam vuduvana"/>
</div>
<div class="lyrico-lyrics-wrapper">vuduvana vuduvana vuduvana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vuduvana vuduvana vuduvana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sinthaiyum kalanguna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sinthaiyum kalanguna"/>
</div>
<div class="lyrico-lyrics-wrapper">un mandai mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un mandai mela"/>
</div>
<div class="lyrico-lyrics-wrapper">kondaiyaga irupana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kondaiyaga irupana"/>
</div>
<div class="lyrico-lyrics-wrapper">irupana irupana irupana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irupana irupana irupana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sethula senthamarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sethula senthamarai"/>
</div>
<div class="lyrico-lyrics-wrapper">un pecha konjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un pecha konjam"/>
</div>
<div class="lyrico-lyrics-wrapper">mathu romba palasuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mathu romba palasuna"/>
</div>
<div class="lyrico-lyrics-wrapper">palasuna aratha palasuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="palasuna aratha palasuna"/>
</div>
<div class="lyrico-lyrics-wrapper">palasuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="palasuna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thedalum un kadhalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thedalum un kadhalum"/>
</div>
<div class="lyrico-lyrics-wrapper">en paatu kulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en paatu kulla"/>
</div>
<div class="lyrico-lyrics-wrapper">maatikichu puthusuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maatikichu puthusuna"/>
</div>
<div class="lyrico-lyrics-wrapper">puthusuna romba puthusuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthusuna romba puthusuna"/>
</div>
<div class="lyrico-lyrics-wrapper">latest na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="latest na"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">poovaiyum urinjidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poovaiyum urinjidum"/>
</div>
<div class="lyrico-lyrics-wrapper">senthenil oorum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="senthenil oorum"/>
</div>
<div class="lyrico-lyrics-wrapper">theni vaayin kodukuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theni vaayin kodukuna"/>
</div>
<div class="lyrico-lyrics-wrapper">kodukuna kodukuna kodukuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kodukuna kodukuna kodukuna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">saavaiyum kadanthu than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saavaiyum kadanthu than"/>
</div>
<div class="lyrico-lyrics-wrapper">un paadi mela 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un paadi mela "/>
</div>
<div class="lyrico-lyrics-wrapper">moodi potu parapana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moodi potu parapana"/>
</div>
<div class="lyrico-lyrics-wrapper">parapana parapana parapana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="parapana parapana parapana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vaalvaiyum purinju than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalvaiyum purinju than"/>
</div>
<div class="lyrico-lyrics-wrapper">ne ah ha ha ha ha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ne ah ha ha ha ha "/>
</div>
<div class="lyrico-lyrics-wrapper">ah ah ah ha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ah ah ah ha "/>
</div>
<div class="lyrico-lyrics-wrapper">hi hi hi hee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hi hi hi hee"/>
</div>
<div class="lyrico-lyrics-wrapper">hoo hoo hoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hoo hoo hoo"/>
</div>
<div class="lyrico-lyrics-wrapper">he he he 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="he he he "/>
</div>
</pre>
