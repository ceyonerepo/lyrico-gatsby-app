---
title: "poi varavaa song lyrics"
album: "Enai Noki Paayum Thota"
artist: "Darbuka Siva"
lyricist: "Madhan Karky"
director: "Gautham Vasudev Menon"
path: "/albums/enai-noki-paayum-thota-lyrics"
song: "Poi Varavaa"
image: ../../images/albumart/enai-noki-paayum-thota.jpg
date: 2019-11-29
lang: tamil
youtubeLink: "https://www.youtube.com/embed/_EVpCwmGIJ8"
type: "sad"
singers:
  - Bombay Jayashri
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">poi varava unai neengi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poi varava unai neengi"/>
</div>
<div class="lyrico-lyrics-wrapper">vidai kodu anbe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidai kodu anbe"/>
</div>
<div class="lyrico-lyrics-wrapper">thaduthida valiyai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaduthida valiyai "/>
</div>
<div class="lyrico-lyrics-wrapper">irunthu uravathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irunthu uravathe"/>
</div>
<div class="lyrico-lyrics-wrapper">kanavu katril eeri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavu katril eeri"/>
</div>
<div class="lyrico-lyrics-wrapper">nan pogiren 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan pogiren "/>
</div>
<div class="lyrico-lyrics-wrapper">thora thora thora 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thora thora thora "/>
</div>
<div class="lyrico-lyrics-wrapper">thoram nan pogiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoram nan pogiren"/>
</div>
<div class="lyrico-lyrics-wrapper">poi varava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poi varava"/>
</div>
</pre>
