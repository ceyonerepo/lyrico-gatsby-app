---
title: "pathu rooba song lyrics"
album: "Clap"
artist: "Ilaiyaraaja"
lyricist: "GKB"
director: "Prithivi Adithya"
path: "/albums/clap-lyrics"
song: "Pathu Rooba"
image: ../../images/albumart/clap.jpg
date: 2022-03-11
lang: tamil
youtubeLink: "https://www.youtube.com/embed/0edvR-Cbkik"
type: "happy"
singers:
  - Srinisha Jayaseelan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">nanana naa naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanana naa naa"/>
</div>
<div class="lyrico-lyrics-wrapper">nanana naa naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanana naa naa"/>
</div>
<div class="lyrico-lyrics-wrapper">nana nana nana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nana nana nana"/>
</div>
<div class="lyrico-lyrics-wrapper">aana aana nana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aana aana nana"/>
</div>
<div class="lyrico-lyrics-wrapper">nana nana nana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nana nana nana"/>
</div>
<div class="lyrico-lyrics-wrapper">na naana nannanana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="na naana nannanana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pathu rooba velarikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathu rooba velarikka"/>
</div>
<div class="lyrico-lyrics-wrapper">kaducha paaka kaasu irukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaducha paaka kaasu irukka"/>
</div>
<div class="lyrico-lyrics-wrapper">uppu kaaram thottu thinna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uppu kaaram thottu thinna"/>
</div>
<div class="lyrico-lyrics-wrapper">vaayil echil oorum appa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaayil echil oorum appa"/>
</div>
<div class="lyrico-lyrics-wrapper">pollatha vaalkaiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pollatha vaalkaiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">pogum idam thooramilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pogum idam thooramilla"/>
</div>
<div class="lyrico-lyrics-wrapper">rail mela aerikitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rail mela aerikitta"/>
</div>
<div class="lyrico-lyrics-wrapper">railukoru paaram illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="railukoru paaram illai"/>
</div>
<div class="lyrico-lyrics-wrapper">oorum en serum onnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oorum en serum onnu"/>
</div>
<div class="lyrico-lyrics-wrapper">rusi paarka thutta neetu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rusi paarka thutta neetu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pathu rooba velarikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathu rooba velarikka"/>
</div>
<div class="lyrico-lyrics-wrapper">kaducha paaka kaasu irukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaducha paaka kaasu irukka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">rail inge jaathi matha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rail inge jaathi matha"/>
</div>
<div class="lyrico-lyrics-wrapper">vithiyaasam parpathillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vithiyaasam parpathillai"/>
</div>
<div class="lyrico-lyrics-wrapper">mel endrum keel endrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mel endrum keel endrum"/>
</div>
<div class="lyrico-lyrics-wrapper">bethangal ethum illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bethangal ethum illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">rail inge jaathi matha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rail inge jaathi matha"/>
</div>
<div class="lyrico-lyrics-wrapper">vithiyaasam parpathillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vithiyaasam parpathillai"/>
</div>
<div class="lyrico-lyrics-wrapper">mel endrum keel endrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mel endrum keel endrum"/>
</div>
<div class="lyrico-lyrics-wrapper">bethangal ethum illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bethangal ethum illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">athu pola thaniyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu pola thaniyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">naan kodukum velrikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan kodukum velrikka"/>
</div>
<div class="lyrico-lyrics-wrapper">hey athu pola thaniyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey athu pola thaniyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">naan kodukum velrikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan kodukum velrikka"/>
</div>
<div class="lyrico-lyrics-wrapper">all paarka rusi mathathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="all paarka rusi mathathu"/>
</div>
<div class="lyrico-lyrics-wrapper">panakara selvanukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="panakara selvanukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">paadu padum aelaikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paadu padum aelaikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">mel vaguppu keel vaguppu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mel vaguppu keel vaguppu"/>
</div>
<div class="lyrico-lyrics-wrapper">rendu rusiya kaatathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rendu rusiya kaatathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pathu rooba velarikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathu rooba velarikka"/>
</div>
<div class="lyrico-lyrics-wrapper">kaducha paaka kaasu irukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaducha paaka kaasu irukka"/>
</div>
<div class="lyrico-lyrics-wrapper">uppu kaaram thottu thinna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uppu kaaram thottu thinna"/>
</div>
<div class="lyrico-lyrics-wrapper">vaayil echil oorum appa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaayil echil oorum appa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thannera paichina than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thannera paichina than"/>
</div>
<div class="lyrico-lyrics-wrapper">man mela payir valarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="man mela payir valarum"/>
</div>
<div class="lyrico-lyrics-wrapper">athu yenna velarikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu yenna velarikku"/>
</div>
<div class="lyrico-lyrics-wrapper">poo poothu kaai kaikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poo poothu kaai kaikum"/>
</div>
<div class="lyrico-lyrics-wrapper">valari pola neyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valari pola neyum"/>
</div>
<div class="lyrico-lyrics-wrapper">thaanaga valara venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaanaga valara venum"/>
</div>
<div class="lyrico-lyrics-wrapper">palamaana vilaguthal pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="palamaana vilaguthal pol"/>
</div>
<div class="lyrico-lyrics-wrapper">ketatha velakka venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ketatha velakka venum"/>
</div>
<div class="lyrico-lyrics-wrapper">pacha pinju vellari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pacha pinju vellari"/>
</div>
<div class="lyrico-lyrics-wrapper">vaadi pogum munnadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaadi pogum munnadi"/>
</div>
<div class="lyrico-lyrics-wrapper">en paata naan paadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en paata naan paadi"/>
</div>
<div class="lyrico-lyrics-wrapper">vittu pidipen ammadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vittu pidipen ammadi"/>
</div>
<div class="lyrico-lyrics-wrapper">nikkatha viyaapaaram thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nikkatha viyaapaaram thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">yarukum naan aalu illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yarukum naan aalu illa"/>
</div>
<div class="lyrico-lyrics-wrapper">enaku yarum thunai illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enaku yarum thunai illa"/>
</div>
<div class="lyrico-lyrics-wrapper">aanalum raasathi naane 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aanalum raasathi naane "/>
</div>
<div class="lyrico-lyrics-wrapper">thanee neye kelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanee neye kelu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pathu rooba velarikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathu rooba velarikka"/>
</div>
<div class="lyrico-lyrics-wrapper">kaducha paaka kaasu irukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaducha paaka kaasu irukka"/>
</div>
<div class="lyrico-lyrics-wrapper">uppu kaaram thottu thinna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uppu kaaram thottu thinna"/>
</div>
<div class="lyrico-lyrics-wrapper">vaayil echil oorum appa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaayil echil oorum appa"/>
</div>
</pre>
