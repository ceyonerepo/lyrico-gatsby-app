---
title: "manase swayanga song lyrics"
album: "Sehari"
artist: "Prashanth R Vihari"
lyricist: "Kittu Vissa Pragada"
director: "Gnanasagar Dwaraka"
path: "/albums/sehari-lyrics"
song: "Manase Swayanga"
image: ../../images/albumart/sehari.jpg
date: 2022-02-11
lang: telugu
youtubeLink: "https://www.youtube.com/embed/sx8OXijUvZI"
type: "happy"
singers:
  - Yashika Sikka
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kanulu dhaate
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanulu dhaate"/>
</div>
<div class="lyrico-lyrics-wrapper">Theguva undha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theguva undha"/>
</div>
<div class="lyrico-lyrics-wrapper">Karigipoye kalalaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karigipoye kalalaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Manavi vinani malupulenno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manavi vinani malupulenno"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadalaleni kadhalaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadalaleni kadhalaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Emo aage veeluntundha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emo aage veeluntundha"/>
</div>
<div class="lyrico-lyrics-wrapper">Emo Saage dhaaruntundhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emo Saage dhaaruntundhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalavarama idhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalavarama idhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Evevo oohallo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evevo oohallo "/>
</div>
<div class="lyrico-lyrics-wrapper">oorege dhaarullo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oorege dhaarullo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennenno swapnaale oorinchaaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennenno swapnaale oorinchaaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Theerale dhaatesi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theerale dhaatesi "/>
</div>
<div class="lyrico-lyrics-wrapper">aadinche aatallo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aadinche aatallo"/>
</div>
<div class="lyrico-lyrics-wrapper">Odinche mounaale ivaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odinche mounaale ivaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Manase swayanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manase swayanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhile bayanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhile bayanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Gathame sthirangaa ohoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gathame sthirangaa ohoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Migile nijanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Migile nijanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ekanthamlo saage 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ekanthamlo saage "/>
</div>
<div class="lyrico-lyrics-wrapper">prayanam chedhugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="prayanam chedhugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Innalluga lotemito telisindhigaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innalluga lotemito telisindhigaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa nakshatrale ennunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa nakshatrale ennunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Aakashmalo ningi jaabilli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aakashmalo ningi jaabilli"/>
</div>
<div class="lyrico-lyrics-wrapper">Saavaasaanne kore
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saavaasaanne kore"/>
</div>
<div class="lyrico-lyrics-wrapper">Chutturaa evarunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chutturaa evarunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee lokamlo gunde aashinche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee lokamlo gunde aashinche"/>
</div>
<div class="lyrico-lyrics-wrapper">Thodante okareley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodante okareley"/>
</div>
<div class="lyrico-lyrics-wrapper">Manase swayanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manase swayanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhile bayanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhile bayanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Gathame sthirangaa ohoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gathame sthirangaa ohoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Migile nijanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Migile nijanga"/>
</div>
</pre>
