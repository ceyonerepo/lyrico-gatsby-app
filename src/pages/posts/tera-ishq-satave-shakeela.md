---
title: "tera ishq satave song lyrics"
album: "Shakeela"
artist: "Meet Bros"
lyricist: "Kumaar"
director: "Indrajit Lankesh"
path: "/albums/shakeela-lyrics"
song: "Tera Ishq Satave"
image: ../../images/albumart/shakeela.jpg
date: 2020-12-25
lang: hindi
youtubeLink: "https://www.youtube.com/embed/sQHXUR4GlHI"
type: "happy"
singers:
  - Khushboo Grewal
  - Meet Bros
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Tera ishq satave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tera ishq satave"/>
</div>
<div class="lyrico-lyrics-wrapper">Kyun tu milne na aave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kyun tu milne na aave"/>
</div>
<div class="lyrico-lyrics-wrapper">Aake saamne tu behja sohniya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aake saamne tu behja sohniya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kar kar yaar udeekan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kar kar yaar udeekan"/>
</div>
<div class="lyrico-lyrics-wrapper">Langiyaan kinniyan tareekhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Langiyaan kinniyan tareekhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Aake naal apne le jaa sohniya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aake naal apne le jaa sohniya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sunn le naa mere dil de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sunn le naa mere dil de"/>
</div>
<div class="lyrico-lyrics-wrapper">Ki kehnde ne jazbaat
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ki kehnde ne jazbaat"/>
</div>
<div class="lyrico-lyrics-wrapper">Mere yaar tu mere aaj
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mere yaar tu mere aaj"/>
</div>
<div class="lyrico-lyrics-wrapper">Sunn le meri baat
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sunn le meri baat"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Love me love me love me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love me love me love me"/>
</div>
<div class="lyrico-lyrics-wrapper">Baby saari saari raat
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby saari saari raat"/>
</div>
<div class="lyrico-lyrics-wrapper">Love me love me love me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love me love me love me"/>
</div>
<div class="lyrico-lyrics-wrapper">Baby saari saari raat
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby saari saari raat"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arre you just want me main
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arre you just want me main"/>
</div>
<div class="lyrico-lyrics-wrapper">Takki jawaan raat saari raat saari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Takki jawaan raat saari raat saari"/>
</div>
<div class="lyrico-lyrics-wrapper">Gets take away
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gets take away"/>
</div>
<div class="lyrico-lyrics-wrapper">Main kosish kitti lakh vaari lakh vaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main kosish kitti lakh vaari lakh vaari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dim mein taareya di roshani ch
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dim mein taareya di roshani ch"/>
</div>
<div class="lyrico-lyrics-wrapper">Sohniya ve tera hi intezaar kardi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sohniya ve tera hi intezaar kardi"/>
</div>
<div class="lyrico-lyrics-wrapper">Mainu tainu pucheya je yes
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mainu tainu pucheya je yes"/>
</div>
<div class="lyrico-lyrics-wrapper">Main tan kardi main na inkaar kardi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main tan kardi main na inkaar kardi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kacchi se vi kacchi hegi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kacchi se vi kacchi hegi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naram kalayi meri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naram kalayi meri"/>
</div>
<div class="lyrico-lyrics-wrapper">Hauli hauli aake fadd le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hauli hauli aake fadd le"/>
</div>
<div class="lyrico-lyrics-wrapper">Haath mera fadd ke spin mainu kari jaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haath mera fadd ke spin mainu kari jaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaur saare kaam karle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaur saare kaam karle"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hoyi naa kis da vi jee karda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoyi naa kis da vi jee karda"/>
</div>
<div class="lyrico-lyrics-wrapper">Challi na deri hai kis gall di ve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Challi na deri hai kis gall di ve"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Love me love me love me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love me love me love me"/>
</div>
<div class="lyrico-lyrics-wrapper">Baby saari saari raat
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby saari saari raat"/>
</div>
<div class="lyrico-lyrics-wrapper">Love me love me love me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love me love me love me"/>
</div>
<div class="lyrico-lyrics-wrapper">Baby saari saari raat
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby saari saari raat"/>
</div>
</pre>
