---
title: "the retro song lyrics"
album: "Sammathame"
artist: "Shekar Chandra"
lyricist: "Sanapati Bharadwaj Patrudu"
director: "Gopinath Reddy"
path: "/albums/sammathame-lyrics"
song: "The Retro"
image: ../../images/albumart/sammathame.jpg
date: 2022-06-24
lang: telugu
youtubeLink: "https://www.youtube.com/embed/dPRDTSklUvI"
type: "happy"
singers:
  -	Mallikarjun
  - Malavika
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Thananam Thananam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thananam Thananam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thananam Thananam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thananam Thananam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thananam Thananam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thananam Thananam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thananam Thananam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thananam Thananam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thananam Thananam AaAa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thananam Thananam AaAa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thananam Thananam AaAa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thananam Thananam AaAa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thananam Thananam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thananam Thananam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thananam Thananam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thananam Thananam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chitapata Chinukulu Kurisenule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chitapata Chinukulu Kurisenule"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhalo Alajadi Rege
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhalo Alajadi Rege"/>
</div>
<div class="lyrico-lyrics-wrapper">Padi Padi Thapanalu Thadisenule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padi Padi Thapanalu Thadisenule"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanuve Taha Tahalade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanuve Taha Tahalade"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Emi Jarigindo Nee Jaaru Jaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emi Jarigindo Nee Jaaru Jaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Paita Jaaripothundi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paita Jaaripothundi"/>
</div>
<div class="lyrico-lyrics-wrapper">Eedu Daadullo Naa Onti Nundi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eedu Daadullo Naa Onti Nundi"/>
</div>
<div class="lyrico-lyrics-wrapper">Siggu Paaripoyindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siggu Paaripoyindhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kondallo Konallo Vaagullo Vankallo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondallo Konallo Vaagullo Vankallo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennenno Veshaale Veddaamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennenno Veshaale Veddaamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Enchakka Ipullo Thaitakka Muddullo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enchakka Ipullo Thaitakka Muddullo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooregi Aaha Andhaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooregi Aaha Andhaama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Baava Thaakithe Murise Murise
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baava Thaakithe Murise Murise"/>
</div>
<div class="lyrico-lyrics-wrapper">Letha Paruvam Merise
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Letha Paruvam Merise"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhama Kulukulu Telise Telise
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhama Kulukulu Telise Telise"/>
</div>
<div class="lyrico-lyrics-wrapper">Aganannadi Vayaase
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aganannadi Vayaase"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thananam Thananam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thananam Thananam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thananam Thananam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thananam Thananam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thananam Thananam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thananam Thananam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thananam Thananam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thananam Thananam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thananam Thananam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thananam Thananam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thananam Thananam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thananam Thananam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thananam Thananam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thananam Thananam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thananam Thananam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thananam Thananam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thananam Thananam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thananam Thananam"/>
</div>
<div class="lyrico-lyrics-wrapper">Laalalala Laa Laalalala Laa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laalalala Laa Laalalala Laa"/>
</div>
<div class="lyrico-lyrics-wrapper">Lala Laalalala Laa Lalala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lala Laalalala Laa Lalala"/>
</div>
<div class="lyrico-lyrics-wrapper">La La La LaLaLa Lalalalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="La La La LaLaLa Lalalalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jum Jum Jum Jum Jum Jum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jum Jum Jum Jum Jum Jum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maata Maata Choopu Choopu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maata Maata Choopu Choopu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ekam Chese Velallonaa Mm Mm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ekam Chese Velallonaa Mm Mm"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalakshepam Cheyodhandhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalakshepam Cheyodhandhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Konte Korika Jum Jum Jum Jum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konte Korika Jum Jum Jum Jum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raalenantu Raarammantu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raalenantu Raarammantu"/>
</div>
<div class="lyrico-lyrics-wrapper">Saigallone Sambandhaanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saigallone Sambandhaanni"/>
</div>
<div class="lyrico-lyrics-wrapper">Theliyajesthu Unna Nenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theliyajesthu Unna Nenu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hai Hai Naayakaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hai Hai Naayakaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edho Edho Chesave Magic Ye Magic Ye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edho Edho Chesave Magic Ye Magic Ye"/>
</div>
<div class="lyrico-lyrics-wrapper">Aagelaaga Lede Lolo Music Ye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagelaaga Lede Lolo Music Ye"/>
</div>
<div class="lyrico-lyrics-wrapper">Vachhaavante Vegangaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vachhaavante Vegangaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Dhikke Na Dhikke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Dhikke Na Dhikke"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayibaaboi Antha Naa Lakke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayibaaboi Antha Naa Lakke"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Baava Thaakithe Murise Murise
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baava Thaakithe Murise Murise"/>
</div>
<div class="lyrico-lyrics-wrapper">Letha Paruvam Merise
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Letha Paruvam Merise"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhama Kulukulu Telise Telise
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhama Kulukulu Telise Telise"/>
</div>
<div class="lyrico-lyrics-wrapper">Aganannadi Vayaase
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aganannadi Vayaase"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jum Jum Jum Jum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jum Jum Jum Jum"/>
</div>
<div class="lyrico-lyrics-wrapper">Jum Jum Jum Jum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jum Jum Jum Jum"/>
</div>
<div class="lyrico-lyrics-wrapper">Jum Jum Jum Jum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jum Jum Jum Jum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nidra Gidra Maakemaatram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nidra Gidra Maakemaatram"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaddhoddhantu Cheppe Kallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaddhoddhantu Cheppe Kallu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalupu Rangu Raatrilona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalupu Rangu Raatrilona"/>
</div>
<div class="lyrico-lyrics-wrapper">Erupekkaalammaa Jum Jum Jum Jum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Erupekkaalammaa Jum Jum Jum Jum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pedavi Pedavi Sunnithanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pedavi Pedavi Sunnithanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Rajookundhe Mojullona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rajookundhe Mojullona"/>
</div>
<div class="lyrico-lyrics-wrapper">Raanincheti Raaja Ninnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raanincheti Raaja Ninnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aapatharamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aapatharamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jivvu Jivvu Antundhe Lolona Lolona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jivvu Jivvu Antundhe Lolona Lolona"/>
</div>
<div class="lyrico-lyrics-wrapper">Bajjobettukova Nannu Ollona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bajjobettukova Nannu Ollona"/>
</div>
<div class="lyrico-lyrics-wrapper">Enaadaina Nee Istam Kaadhantu Unnaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enaadaina Nee Istam Kaadhantu Unnaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Uu Ante Oohu Annaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uu Ante Oohu Annaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Baava Thaakithe Murise Murise
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baava Thaakithe Murise Murise"/>
</div>
<div class="lyrico-lyrics-wrapper">Letha Paruvam Merise
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Letha Paruvam Merise"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhama Kulukulu Telise Telise
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhama Kulukulu Telise Telise"/>
</div>
<div class="lyrico-lyrics-wrapper">Aganannadi Vayaase
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aganannadi Vayaase"/>
</div>
</pre>
