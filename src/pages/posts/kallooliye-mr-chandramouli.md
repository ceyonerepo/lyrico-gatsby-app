---
title: "kallooliye song lyrics"
album: "Mr Chandramouli"
artist: "Sam CS"
lyricist: "Sam CS"
director: "Thiru"
path: "/albums/mr-chandramouli-lyrics"
song: "Kallooliye"
image: ../../images/albumart/mr-chandramouli.jpg
date: 2018-07-06
lang: tamil
youtubeLink: "https://www.youtube.com/embed/r5RAM_mBQB0"
type: "love"
singers:
  - Sam CS
  - Swagatha S Krishnan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">En kallooliyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kallooliyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un nenapputhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un nenapputhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ahaa haan haha haha haan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahaa haan haha haha haan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ahaa haan haha haha haan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahaa haan haha haha haan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En kallooliyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kallooliyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un nenapputhaan en manasula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un nenapputhaan en manasula"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai kaanamalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai kaanamalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru virisal thonuthadi usurula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru virisal thonuthadi usurula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En kallooliyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kallooliyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un nenapputhaan en manasula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un nenapputhaan en manasula"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai kaanamalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai kaanamalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru virisal thonuthadi usurula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru virisal thonuthadi usurula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En paatukku irunthanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En paatukku irunthanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal koduthu nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal koduthu nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Paada paduthura…yeno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paada paduthura…yeno"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaal pooti kedantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaal pooti kedantha"/>
</div>
<div class="lyrico-lyrics-wrapper">En ganaakootula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En ganaakootula"/>
</div>
<div class="lyrico-lyrics-wrapper">Nozhanjita nee…yeno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nozhanjita nee…yeno"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee velakkethuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee velakkethuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Una theendinen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Una theendinen"/>
</div>
<div class="lyrico-lyrics-wrapper">Siru poochiya…naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru poochiya…naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru nodi kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru nodi kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee piriyamalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee piriyamalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Udan vara enakku nee venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udan vara enakku nee venum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaatha kalakkura nenjila thannaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatha kalakkura nenjila thannaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedho nadakathu ennilla unnala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedho nadakathu ennilla unnala"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaanda thadaigala kadanthidum unnaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanda thadaigala kadanthidum unnaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Neengathu irunthidu pennae ennulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neengathu irunthidu pennae ennulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En paatukku irunthanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En paatukku irunthanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal koduthu nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal koduthu nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Paada paduthura yeno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paada paduthura yeno"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru nodi koodaNee piriyamalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru nodi koodaNee piriyamalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Udan vara enakku nee venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udan vara enakku nee venum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalam neela unadhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam neela unadhadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazha asaigal mudivilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazha asaigal mudivilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Marupadi jenmam thodarumo innum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marupadi jenmam thodarumo innum"/>
</div>
<div class="lyrico-lyrics-wrapper">Virumba thirumba nee pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Virumba thirumba nee pothum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaalam podum irudhayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaalam podum irudhayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum venumnu azhuthidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum venumnu azhuthidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagilae ingu unai vidathingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagilae ingu unai vidathingu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thodara thodara varam venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodara thodara varam venum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tharaiyil nikkama adam pidikiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharaiyil nikkama adam pidikiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Avalae pothumnu veri pidikiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avalae pothumnu veri pidikiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Arugil naan inga uyiril serth anga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arugil naan inga uyiril serth anga"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkul kalanguren naan inga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkul kalanguren naan inga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaatha kalakkura nenjila thannaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatha kalakkura nenjila thannaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedho nadakathu ennilla unnala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedho nadakathu ennilla unnala"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaanda thadaigala kadanthidum unnaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanda thadaigala kadanthidum unnaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Neengathu irunthidu pennae ennulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neengathu irunthidu pennae ennulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En paatukku irunthanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En paatukku irunthanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal koduthu nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal koduthu nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Paada paduthura yeno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paada paduthura yeno"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaal pooti kedantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaal pooti kedantha"/>
</div>
<div class="lyrico-lyrics-wrapper">En ganaakootula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En ganaakootula"/>
</div>
<div class="lyrico-lyrics-wrapper">Nozhanjita nee yeno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nozhanjita nee yeno"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee velakkethuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee velakkethuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Una theendinen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Una theendinen"/>
</div>
<div class="lyrico-lyrics-wrapper">Siru poochiya naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru poochiya naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru nodi kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru nodi kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee piriyamalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee piriyamalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Udan vara enakku nee venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udan vara enakku nee venum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En kallooliyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kallooliyae"/>
</div>
</pre>
