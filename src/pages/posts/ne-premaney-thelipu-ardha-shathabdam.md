---
title: "ne premaney song lyrics"
album: "Ardha Shathabdam"
artist: "Nawfal Raja AIS"
lyricist: "Rahman"
director: "Rawindra Pulle"
path: "/albums/ardha-shathabdam-lyrics"
song: "Ne Premaney Thelipu"
image: ../../images/albumart/ardha-shathabdam.jpg
date: 2021-03-05
lang: telugu
youtubeLink: "https://www.youtube.com/embed/xwtdbEel41E"
type: "love"
singers:
  - Anthonydasan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Rappappappaa Rappappa Rappappaa Rappappappaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rappappappaa Rappappa Rappappaa Rappappappaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Rappappappaa Rappappa Rappappaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rappappappaa Rappappa Rappappaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rappappappaa Rappappa Rappappaa Rappappappaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rappappappaa Rappappa Rappappaa Rappappappaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Rappappappaa Rappappa Rappappaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rappappappaa Rappappa Rappappaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Premane Thelipe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Premane Thelipe"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Puvvu Epudu Pooseno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Puvvu Epudu Pooseno"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Lokame Marichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Lokame Marichi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanulu Edhuru Chooseno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanulu Edhuru Chooseno"/>
</div>
<div class="lyrico-lyrics-wrapper">Nidhura Raadhule 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nidhura Raadhule "/>
</div>
<div class="lyrico-lyrics-wrapper">Kudhuru Ledhule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudhuru Ledhule"/>
</div>
<div class="lyrico-lyrics-wrapper">Veredhi Edhee 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veredhi Edhee "/>
</div>
<div class="lyrico-lyrics-wrapper">Guruthu Raadhule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Guruthu Raadhule"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagalu Reyilaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagalu Reyilaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Sathamathamai Ilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sathamathamai Ilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Enni Padigaapulu Kaasthunnaavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enni Padigaapulu Kaasthunnaavo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rappappappaa Rappappa Rappappaa Rappappappaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rappappappaa Rappappa Rappappaa Rappappappaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Rappappappaa Rappappa Rappappaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rappappappaa Rappappa Rappappaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rappappappaa Rappappa Rappappaa Rappappappaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rappappappaa Rappappa Rappappaa Rappappappaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Rappappappaa Rappappa Rappappaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rappappappaa Rappappa Rappappaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ee Maayalo Munigi Manasu Epudu Theleno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Maayalo Munigi Manasu Epudu Theleno"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Dhaarilo Kadhile Adugu Ye Dhari Chereno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Dhaarilo Kadhile Adugu Ye Dhari Chereno"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Puvvu Eppudraa Poochedhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Puvvu Eppudraa Poochedhi"/>
</div>
</pre>
