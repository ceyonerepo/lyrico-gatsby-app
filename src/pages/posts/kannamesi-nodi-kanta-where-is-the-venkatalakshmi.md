---
title: "kannamesi nodi kanta song lyrics"
album: "Where is the Venkatalakshmi"
artist: "Hari Gowra"
lyricist: "Suresh Banisetty"
director: "	Kishore (Ladda)"
path: "/albums/where-is-the-venkatalakshmi-lyrics"
song: "Kannamesi Nodi Kanta"
image: ../../images/albumart/where-is-the-venkatalakshmi.jpg
date: 2019-03-15
lang: telugu
youtubeLink: "https://www.youtube.com/embed/QpeKojUssDo"
type: "happy"
singers:
  - Lokeshwar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kannamesinodi kanta sunnamesi potaranta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannamesinodi kanta sunnamesi potaranta"/>
</div>
<div class="lyrico-lyrics-wrapper">Guntanakkakainagani heart attack destaranta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Guntanakkakainagani heart attack destaranta"/>
</div>
<div class="lyrico-lyrics-wrapper">Ceyyi tinnaguṇḍadaṇṭa nōru tinnaguṇḍadaṇṭā
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ceyyi tinnaguṇḍadaṇṭa nōru tinnaguṇḍadaṇṭā"/>
</div>
āḍ<div class="lyrico-lyrics-wrapper">u īḍu okkaṭaitē ūru vāḍa gattaraṇṭā
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="u īḍu okkaṭaitē ūru vāḍa gattaraṇṭā"/>
</div>
<div class="lyrico-lyrics-wrapper">lēni alavāṭu lēni nāyāḷlu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lēni alavāṭu lēni nāyāḷlu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kāni panulenno cēsē doṅga kōḷlu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kāni panulenno cēsē doṅga kōḷlu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalikeste alikesi potaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalikeste alikesi potaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Kukkatōka kanna īḷḷa bud’dhi vaṅkaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kukkatōka kanna īḷḷa bud’dhi vaṅkaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalikeste alikesi potaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalikeste alikesi potaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Kukkatōka kanna īḷḷa bud’dhi vaṅkaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kukkatōka kanna īḷḷa bud’dhi vaṅkaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannamesinodi kanta sunnamesi potaranta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannamesinodi kanta sunnamesi potaranta"/>
</div>
<div class="lyrico-lyrics-wrapper">Guntanakkakainagani heart attack destaranta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Guntanakkakainagani heart attack destaranta"/>
</div>
<div class="lyrico-lyrics-wrapper">Māyagāḷlu, jaṇṭa railu paṭṭālu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Māyagāḷlu, jaṇṭa railu paṭṭālu"/>
</div>
īḷḷ<div class="lyrico-lyrics-wrapper">a nōḷḷu polyūṣan goṭṭālu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="a nōḷḷu polyūṣan goṭṭālu"/>
</div>
<div class="lyrico-lyrics-wrapper">Saccinōḷḷu myān hōl suṭṭālu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saccinōḷḷu myān hōl suṭṭālu "/>
</div>
<div class="lyrico-lyrics-wrapper">muṭṭukuṇṭē saccipōdā ḍeṭṭol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muṭṭukuṇṭē saccipōdā ḍeṭṭol"/>
</div>
<div class="lyrico-lyrics-wrapper">sitraguptuḍeyyalēni siṭṭālu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sitraguptuḍeyyalēni siṭṭālu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sittarālu īḷḷa edava ghaṭṭālu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sittarālu īḷḷa edava ghaṭṭālu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sariḍān kē heḍēk tepistāru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sariḍān kē heḍēk tepistāru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sairen kē sirāku teppistāru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sairen kē sirāku teppistāru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paṭṭapagalē sūputāru sukkalu iṅka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paṭṭapagalē sūputāru sukkalu iṅka "/>
</div>
<div class="lyrico-lyrics-wrapper">sīkaṭaitē seppalēni lekkalu paṭṭapagalē 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sīkaṭaitē seppalēni lekkalu paṭṭapagalē "/>
</div>
<div class="lyrico-lyrics-wrapper">sūputāru sukkalu iṅka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sūputāru sukkalu iṅka "/>
</div>
<div class="lyrico-lyrics-wrapper">sīkaṭaitē seppalēni lekkalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sīkaṭaitē seppalēni lekkalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
Ū<div class="lyrico-lyrics-wrapper">rikēmō gōrusuṭṭulāṇṭōḷḷu geliki 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rikēmō gōrusuṭṭulāṇṭōḷḷu geliki "/>
</div>
<div class="lyrico-lyrics-wrapper">sūttē tēnepaṭṭulāṇṭōḷḷu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sūttē tēnepaṭṭulāṇṭōḷḷu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">hey paṭṭukuṇṭē grils kanna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey paṭṭukuṇṭē grils kanna "/>
</div>
<div class="lyrico-lyrics-wrapper">jiḍḍugāḷḷu ḍusṭ bin kanna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jiḍḍugāḷḷu ḍusṭ bin kanna "/>
</div>
<div class="lyrico-lyrics-wrapper">parama vēsṭ gāḷḷu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="parama vēsṭ gāḷḷu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaḷḷamundu tirugutunna kīṭakālu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaḷḷamundu tirugutunna kīṭakālu "/>
</div>
<div class="lyrico-lyrics-wrapper">jallaḍaina paṭṭalēni nāṭakālu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jallaḍaina paṭṭalēni nāṭakālu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vīḷḷa āgaḍālakēpēru peṭṭāli 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vīḷḷa āgaḍālakēpēru peṭṭāli "/>
</div>
<div class="lyrico-lyrics-wrapper">kāgaḍālakaina semaṭalē paṭṭāli 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kāgaḍālakaina semaṭalē paṭṭāli "/>
</div>
<div class="lyrico-lyrics-wrapper">kompalaṇṭin̄caḍanlō tōpulū
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kompalaṇṭin̄caḍanlō tōpulū"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maṭṭi aṇṭakuṇḍa duluputāru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maṭṭi aṇṭakuṇḍa duluputāru "/>
</div>
<div class="lyrico-lyrics-wrapper">sētulū cāvu telivitēṭalunna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sētulū cāvu telivitēṭalunna "/>
</div>
<div class="lyrico-lyrics-wrapper">burralu īḷḷa mundu evvarainagāni gorrelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="burralu īḷḷa mundu evvarainagāni gorrelu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannamesinodi kanta sunnamesi potaranta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannamesinodi kanta sunnamesi potaranta"/>
</div>
<div class="lyrico-lyrics-wrapper">Guntanakkakainagani heart attack destaranta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Guntanakkakainagani heart attack destaranta"/>
</div>
<div class="lyrico-lyrics-wrapper">Ceyyi tinnaguṇḍadaṇṭa nōru tinnaguṇḍadaṇṭā
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ceyyi tinnaguṇḍadaṇṭa nōru tinnaguṇḍadaṇṭā"/>
</div>
āḍ<div class="lyrico-lyrics-wrapper">u īḍu okkaṭaitē ūru vāḍa gattaraṇṭā
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="u īḍu okkaṭaitē ūru vāḍa gattaraṇṭā"/>
</div>
<div class="lyrico-lyrics-wrapper">lēni alavāṭu lēni nāyāḷlu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lēni alavāṭu lēni nāyāḷlu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kāni panulenno cēsē doṅga kōḷlu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kāni panulenno cēsē doṅga kōḷlu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalikeste alikesi potaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalikeste alikesi potaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Kukkatōka kanna īḷḷa bud’dhi vaṅkaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kukkatōka kanna īḷḷa bud’dhi vaṅkaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalikeste alikesi potaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalikeste alikesi potaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Kukkatōka kanna īḷḷa bud’dhi vaṅkaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kukkatōka kanna īḷḷa bud’dhi vaṅkaru"/>
</div>
</pre>
