---
title: "thokkanam kuruvi kudu song lyrics"
album: "Marudhavelu"
artist: "James Vasanthan"
lyricist: "Mohan Raj"
director: "R.K.R. Aathimoolam"
path: "/albums/marudhavelu-lyrics"
song: "Thokkanam Kuruvi Kudu"
image: ../../images/albumart/marudhavelu.jpg
date: 2011-11-18
lang: tamil
youtubeLink: 
type: "happy"
singers:
  - Ananth
  - Sangeetha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Thookkanaan kuruvikkoodu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thookkanaan kuruvikkoodu "/>
</div>
<div class="lyrico-lyrics-wrapper">kondamela aasavachen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kondamela aasavachen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aasa vachen aasa vachen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasa vachen aasa vachen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mookkanaan gayirupoala 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mookkanaan gayirupoala "/>
</div>
<div class="lyrico-lyrics-wrapper">onnakkatta meesavachen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="onnakkatta meesavachen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meesa vachen mesa vachen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meesa vachen mesa vachen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan thannandhaniyila 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan thannandhaniyila "/>
</div>
<div class="lyrico-lyrics-wrapper">sirichen Sirichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sirichen Sirichen"/>
</div>
<div class="lyrico-lyrics-wrapper">oru sinna podiyanaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru sinna podiyanaa "/>
</div>
<div class="lyrico-lyrics-wrapper">thavichen Thavichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thavichen Thavichen"/>
</div>
<div class="lyrico-lyrics-wrapper">pattum padaama thottum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pattum padaama thottum "/>
</div>
<div class="lyrico-lyrics-wrapper">thodaama kanavula thudichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thodaama kanavula thudichen"/>
</div>
<div class="lyrico-lyrics-wrapper">un azhagula tholainjen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un azhagula tholainjen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maaman manasathirugi thalaiyapoala 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaman manasathirugi thalaiyapoala "/>
</div>
<div class="lyrico-lyrics-wrapper">maatti vachen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maatti vachen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maatti vachen maatti vachen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maatti vachen maatti vachen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raamaney niththam adhiley 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raamaney niththam adhiley "/>
</div>
<div class="lyrico-lyrics-wrapper">Or nenappu oothivachen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Or nenappu oothivachen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oothi vachen oothi vachen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oothi vachen oothi vachen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pakkam nirkkaiyil morache
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkam nirkkaiyil morache"/>
</div>
<div class="lyrico-lyrics-wrapper">Morache
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Morache"/>
</div>
<div class="lyrico-lyrics-wrapper">kooda poagaiyil thudiche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kooda poagaiyil thudiche"/>
</div>
<div class="lyrico-lyrics-wrapper">Thudiche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thudiche"/>
</div>
<div class="lyrico-lyrics-wrapper">pattum padaama vittum tharaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pattum padaama vittum tharaama"/>
</div>
<div class="lyrico-lyrics-wrapper">edhirula thavichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edhirula thavichen"/>
</div>
<div class="lyrico-lyrics-wrapper">un nezhalukku nuzhanjen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un nezhalukku nuzhanjen"/>
</div>
<div class="lyrico-lyrics-wrapper">en usurukkullathaan mudinjen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en usurukkullathaan mudinjen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uruvandhaan puliyambazham poala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uruvandhaan puliyambazham poala"/>
</div>
<div class="lyrico-lyrics-wrapper">valainjithaan usura mozhampoada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valainjithaan usura mozhampoada"/>
</div>
<div class="lyrico-lyrics-wrapper">paruvandhaan oonjal poala aadudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paruvandhaan oonjal poala aadudhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manasuthaan pazhaiya kudam poaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasuthaan pazhaiya kudam poaley"/>
</div>
<div class="lyrico-lyrics-wrapper">suzhaluthey undhan munnaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suzhaluthey undhan munnaley"/>
</div>
<div class="lyrico-lyrics-wrapper">vayasuthaan vattam poattu thedudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vayasuthaan vattam poattu thedudhey"/>
</div>
<div class="lyrico-lyrics-wrapper">gadigaara kannu rendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gadigaara kannu rendum"/>
</div>
<div class="lyrico-lyrics-wrapper">gadigaara kannu rendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gadigaara kannu rendum"/>
</div>
<div class="lyrico-lyrics-wrapper">thinandhoarum onnaa suththum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thinandhoarum onnaa suththum"/>
</div>
<div class="lyrico-lyrics-wrapper">un veettu jannal Oram thoongumey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un veettu jannal Oram thoongumey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaal kolusil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaal kolusil"/>
</div>
<div class="lyrico-lyrics-wrapper">Ozhinjikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ozhinjikkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">ada saamathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada saamathula"/>
</div>
<div class="lyrico-lyrics-wrapper">Muzhichikiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muzhichikiren"/>
</div>
<div class="lyrico-lyrics-wrapper">vidiyumvarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidiyumvarai"/>
</div>
<div class="lyrico-lyrics-wrapper">anaichikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anaichikkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">en thangathai poapponnu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en thangathai poapponnu "/>
</div>
<div class="lyrico-lyrics-wrapper">naan thitturen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan thitturen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thookkanaan kuruvikkoodu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thookkanaan kuruvikkoodu "/>
</div>
<div class="lyrico-lyrics-wrapper">kondamela aasavachen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kondamela aasavachen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aasa vachen aasa vachen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasa vachen aasa vachen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vengalichaangalla orasithaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vengalichaangalla orasithaan"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjiley vacha theeyathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjiley vacha theeyathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">meththaiyila thookkangettu veguren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meththaiyila thookkangettu veguren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vennilaa velicham en maela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vennilaa velicham en maela"/>
</div>
<div class="lyrico-lyrics-wrapper">pozhiyudhey undhan kannaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pozhiyudhey undhan kannaala"/>
</div>
<div class="lyrico-lyrics-wrapper">ummela yekkangondu saaguren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ummela yekkangondu saaguren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vivagaaram onna enni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vivagaaram onna enni"/>
</div>
<div class="lyrico-lyrics-wrapper">Vivagaaram onna enni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vivagaaram onna enni"/>
</div>
<div class="lyrico-lyrics-wrapper">korainjaaley indhakkanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="korainjaaley indhakkanni"/>
</div>
<div class="lyrico-lyrics-wrapper">kurumbaiyaadu koottam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kurumbaiyaadu koottam "/>
</div>
<div class="lyrico-lyrics-wrapper">poaley sevakkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poaley sevakkuren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaathirundhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathirundhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaal valikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaal valikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">ada kanavuleyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada kanavuleyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Padi edukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padi edukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">raappozhudhil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="raappozhudhil"/>
</div>
<div class="lyrico-lyrics-wrapper">Thael kadikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thael kadikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">en ullatha vellam poal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en ullatha vellam poal "/>
</div>
<div class="lyrico-lyrics-wrapper">thingaadha nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thingaadha nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maaman manasathirugi thalaiyapoala 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaman manasathirugi thalaiyapoala "/>
</div>
<div class="lyrico-lyrics-wrapper">maatti vachen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maatti vachen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maatti vachen maatti vachen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maatti vachen maatti vachen"/>
</div>
</pre>
