---
title: "prema janta title song song lyrics"
album: "Prema Janta"
artist: "Nikhilesh Thogari"
lyricist: "Nikhilesh Thogari"
director: "Nikhilesh Thogari"
path: "/albums/prema-janta-lyrics"
song: "Prema Janta Title Song"
image: ../../images/albumart/prema-janta.jpg
date: 2019-06-28
lang: telugu
youtubeLink: "https://www.youtube.com/embed/n_CEU1oT6VQ"
type: "title track"
singers:
  - Anurag Kulkarni
  - Nutana Mohan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">kudirindi eddari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kudirindi eddari"/>
</div>
<div class="lyrico-lyrics-wrapper">madhyana kudirindi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="madhyana kudirindi"/>
</div>
<div class="lyrico-lyrics-wrapper">mudirindi eddari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudirindi eddari"/>
</div>
<div class="lyrico-lyrics-wrapper">madhayana mudirindi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="madhayana mudirindi"/>
</div>
<div class="lyrico-lyrics-wrapper">ammaie abbai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ammaie abbai"/>
</div>
<div class="lyrico-lyrics-wrapper">chilaka gorinkaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chilaka gorinkaa"/>
</div>
<div class="lyrico-lyrics-wrapper">aa chilaka gorinka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aa chilaka gorinka"/>
</div>
<div class="lyrico-lyrics-wrapper">preminchayanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="preminchayanta"/>
</div>
<div class="lyrico-lyrics-wrapper">anagana anagana anaganaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anagana anagana anaganaga"/>
</div>
<div class="lyrico-lyrics-wrapper">oka prema janta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oka prema janta"/>
</div>
<div class="lyrico-lyrics-wrapper">atu itu urakalu vesthundi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="atu itu urakalu vesthundi"/>
</div>
<div class="lyrico-lyrics-wrapper">arre dhenikanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arre dhenikanta"/>
</div>
<div class="lyrico-lyrics-wrapper">anagana anagana anaganaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anagana anagana anaganaga"/>
</div>
<div class="lyrico-lyrics-wrapper">oka prema janta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oka prema janta"/>
</div>
<div class="lyrico-lyrics-wrapper">thika maka payagulu tisthundi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thika maka payagulu tisthundi"/>
</div>
<div class="lyrico-lyrics-wrapper">idhi emitanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhi emitanta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kalavani aaraatu kalisaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalavani aaraatu kalisaka"/>
</div>
<div class="lyrico-lyrics-wrapper">mohamataalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mohamataalu"/>
</div>
<div class="lyrico-lyrics-wrapper">parichayame lenee ee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="parichayame lenee ee"/>
</div>
<div class="lyrico-lyrics-wrapper">theere premanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theere premanta"/>
</div>
<div class="lyrico-lyrics-wrapper">pagalantha santhosalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pagalantha santhosalu"/>
</div>
<div class="lyrico-lyrics-wrapper">diyalantha sayantrallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="diyalantha sayantrallu"/>
</div>
<div class="lyrico-lyrics-wrapper">kalavarame lolo ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalavarame lolo ooo"/>
</div>
<div class="lyrico-lyrics-wrapper">ninu veedina kshanamantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninu veedina kshanamantha"/>
</div>
<div class="lyrico-lyrics-wrapper">dhevinchanee mana premani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhevinchanee mana premani"/>
</div>
<div class="lyrico-lyrics-wrapper">thilakinchi aa devullani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thilakinchi aa devullani"/>
</div>
<div class="lyrico-lyrics-wrapper">veyyelloo vardhillalaneee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veyyelloo vardhillalaneee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">anagana anagana anaganaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anagana anagana anaganaga"/>
</div>
<div class="lyrico-lyrics-wrapper">oka prema janta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oka prema janta"/>
</div>
<div class="lyrico-lyrics-wrapper">atu itu urakalu vesthundi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="atu itu urakalu vesthundi"/>
</div>
<div class="lyrico-lyrics-wrapper">arre dhenikanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arre dhenikanta"/>
</div>
<div class="lyrico-lyrics-wrapper">anagana anagana anaganaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anagana anagana anaganaga"/>
</div>
<div class="lyrico-lyrics-wrapper">oka prema janta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oka prema janta"/>
</div>
<div class="lyrico-lyrics-wrapper">thika maka payagulu tisthundi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thika maka payagulu tisthundi"/>
</div>
<div class="lyrico-lyrics-wrapper">idhi emitanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhi emitanta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kudirindi eddari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kudirindi eddari"/>
</div>
<div class="lyrico-lyrics-wrapper">madhyana kudirindi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="madhyana kudirindi"/>
</div>
<div class="lyrico-lyrics-wrapper">mudirindi eddari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudirindi eddari"/>
</div>
<div class="lyrico-lyrics-wrapper">madhayana mudirindi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="madhayana mudirindi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thelavaarithe malle madhalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thelavaarithe malle madhalu"/>
</div>
<div class="lyrico-lyrics-wrapper">manasaagathu neekey kadhulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasaagathu neekey kadhulu"/>
</div>
<div class="lyrico-lyrics-wrapper">prathi nimisham neetho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="prathi nimisham neetho"/>
</div>
<div class="lyrico-lyrics-wrapper">ooo undalanipistundhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooo undalanipistundhe"/>
</div>
<div class="lyrico-lyrics-wrapper">polamaarithe madhilo medhulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="polamaarithe madhilo medhulu"/>
</div>
<div class="lyrico-lyrics-wrapper">thalachavani neepy dhigulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalachavani neepy dhigulu"/>
</div>
<div class="lyrico-lyrics-wrapper">prathi vishayam neethoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="prathi vishayam neethoo"/>
</div>
<div class="lyrico-lyrics-wrapper">ooo cheppalanipisthundhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooo cheppalanipisthundhe"/>
</div>
<div class="lyrico-lyrics-wrapper">bangaaramaa nee ottu ra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bangaaramaa nee ottu ra"/>
</div>
<div class="lyrico-lyrics-wrapper">neneppuduoo nee thodura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neneppuduoo nee thodura"/>
</div>
<div class="lyrico-lyrics-wrapper">naa preme saksham neekaneee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa preme saksham neekaneee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">anagana anagana anaganaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anagana anagana anaganaga"/>
</div>
<div class="lyrico-lyrics-wrapper">oka prema janta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oka prema janta"/>
</div>
<div class="lyrico-lyrics-wrapper">atu itu urakalu vesthundi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="atu itu urakalu vesthundi"/>
</div>
<div class="lyrico-lyrics-wrapper">arre dhenikanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arre dhenikanta"/>
</div>
<div class="lyrico-lyrics-wrapper">anagana anagana anaganaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anagana anagana anaganaga"/>
</div>
<div class="lyrico-lyrics-wrapper">oka prema janta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oka prema janta"/>
</div>
<div class="lyrico-lyrics-wrapper">thika maka payagulu tisthundi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thika maka payagulu tisthundi"/>
</div>
<div class="lyrico-lyrics-wrapper">idhi emitanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhi emitanta"/>
</div>
</pre>
