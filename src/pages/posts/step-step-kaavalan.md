---
title: "step step song lyrics"
album: "Kaavalan"
artist: "Vidyasagar"
lyricist: "Viveka"
director: "Siddique"
path: "/albums/kaavalan-lyrics"
song: "Step Step"
image: ../../images/albumart/kaavalan.jpg
date: 2011-01-15
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Nll4OtXiArI"
type: "happy"
singers:
  - Benny Dayal
  - Megha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">You There Come On
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You There Come On"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lets Do This
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lets Do This"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Step It Up One Two Three
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Step It Up One Two Three"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Step Step Step It Up
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Step Step Step It Up"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ilamai Alaikudhu Step It Up
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilamai Alaikudhu Step It Up"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhayam Parakudhu Step It Up
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam Parakudhu Step It Up"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Step Step Step It Up
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Step Step Step It Up"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnul Minnal Wake It Up
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnul Minnal Wake It Up"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ucham Varaikum Keep It Up
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ucham Varaikum Keep It Up"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Step It Up
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Step It Up"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uttru Paar Ulagil Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uttru Paar Ulagil Ellaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhagin Naattiyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagin Naattiyam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyirukul Adhuvey Inba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirukul Adhuvey Inba"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theeyai Mootidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeyai Mootidum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Nee Maranthae Aadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Nee Maranthae Aadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Motchum Saathiyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Motchum Saathiyam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">One Two Three Four
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="One Two Three Four"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yemmaamo Yemaamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yemmaamo Yemaamo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Five Six Seven Eight
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Five Six Seven Eight"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yemmaamo Yemaamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yemmaamo Yemaamo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">One Two Three Four
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="One Two Three Four"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yemmaamo Yemaamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yemmaamo Yemaamo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Five Six Seven Eight
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Five Six Seven Eight"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yemmaamo Yemaamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yemmaamo Yemaamo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Step Step Step It Up
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Step Step Step It Up"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ilamai Alaikuthu Step It Up
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilamai Alaikuthu Step It Up"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhayam Parakkuthu Step It Up
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam Parakkuthu Step It Up"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaal Sattai Mel Sattai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaal Sattai Mel Sattai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Loose Aga Potukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Loose Aga Potukka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Natpaaga Yarodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natpaaga Yarodum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saera Maruthutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saera Maruthutha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ippodhu Vin Mutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippodhu Vin Mutta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pudhusaga Ezunthuta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhusaga Ezunthuta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thonuthu Kai Thatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thonuthu Kai Thatta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Da Asaththitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Da Asaththitta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aattril Aadum Meen Adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aattril Aadum Meen Adi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kattil Thullum Maan Adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattil Thullum Maan Adi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engum Edhilum Naan Ready
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engum Edhilum Naan Ready"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paaradi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaradi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pudhu Vesam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu Vesam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pudhu Vegam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu Vegam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pudhthithana Aaaluthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhthithana Aaaluthaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
1, 2, 3, 4
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yemmaamo Yemaamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yemmaamo Yemaamo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
5, 6, 7, 8
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yemmaamo Yemaamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yemmaamo Yemaamo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
1, 2, 3, 4
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yemmaamo Yemaamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yemmaamo Yemaamo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
5, 6, 7, 8
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yemmaamo Yemaamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yemmaamo Yemaamo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Step Step Step It Up
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Step Step Step It Up"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ilamai Alaikudhu Step It Up
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilamai Alaikudhu Step It Up"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhayam Parakudhu Step It Up
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam Parakudhu Step It Up"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paakathaan Siru Pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paakathaan Siru Pulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalakura Payapulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalakura Payapulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ilam Pengal Nenaipulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilam Pengal Nenaipulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neethaan Mapilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethaan Mapilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yenmandha Aal Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenmandha Aal Illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Munna Pol Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Munna Pol Illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paraendi En Attam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paraendi En Attam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yarum Inai Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yarum Inai Illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dan Dan Ani Udan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dan Dan Ani Udan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athirum Kalgal Unnudan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athirum Kalgal Unnudan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aadu Konjam Ennudan Ennudan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadu Konjam Ennudan Ennudan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kai Korthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai Korthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mei Serthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mei Serthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyir Poothu Aadalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Poothu Aadalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
1, 2, 3, 4
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yemmaamo Yemaamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yemmaamo Yemaamo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
5, 6, 7, 8
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yemmaamo Yemaamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yemmaamo Yemaamo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
1, 2, 3, 4
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yemmaamo Yemaamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yemmaamo Yemaamo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
5, 6, 7, 8
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yemmaamo Yemaamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yemmaamo Yemaamo"/>
</div>
</pre>
