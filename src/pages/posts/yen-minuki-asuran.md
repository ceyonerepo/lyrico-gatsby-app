---
title: 'yen minukki song lyrics'
album: 'Asuran'
artist: 'G.V. Prakash Kumar'
lyricist: 'Eknath'
director: 'Vetrimaaran'
path: '/albums/asuran-song-lyrics'
song: 'Yen Minukki'
image: ../../images/albumart/asuran.jpg
date: 2019-10-04
lang: tamil
singers: 
- Teejay Arunasalam
- Chinmayi
youtubeLink: "https://www.youtube.com/embed/d3fJioZJcT4"
type: 'love'
---

<pre class="lyrics-native">
    Coming Soon
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Naanaanae naanaanae naanae nannae
<input type="checkbox" class="lyrico-select-lyric-line" value="Naanaanae naanaanae naanae nannae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanaanaenae naanaanaenae
<input type="checkbox" class="lyrico-select-lyric-line" value="Naanaanaenae naanaanaenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Oththa nilavai pola
<input type="checkbox" class="lyrico-select-lyric-line" value="Oththa nilavai pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuththa vacha azhaguthamla
<input type="checkbox" class="lyrico-select-lyric-line" value="Kuththa vacha azhaguthamla"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai pichi thinguthamla
<input type="checkbox" class="lyrico-select-lyric-line" value="Enai pichi thinguthamla"/>
</div>
<div class="lyrico-lyrics-wrapper">Em minukki kaathirukka
<input type="checkbox" class="lyrico-select-lyric-line" value="Em minukki kaathirukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai ulukki poothirukka
<input type="checkbox" class="lyrico-select-lyric-line" value="Enai ulukki poothirukka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Neththi vagudukulla
<input type="checkbox" class="lyrico-select-lyric-line" value="Neththi vagudukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Aambalaiya saachirukka
<input type="checkbox" class="lyrico-select-lyric-line" value="Aambalaiya saachirukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Aarappottu vachirukka
<input type="checkbox" class="lyrico-select-lyric-line" value="Aarappottu vachirukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Em minukki kaathirukka
<input type="checkbox" class="lyrico-select-lyric-line" value="Em minukki kaathirukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai ulukki poothirukka
<input type="checkbox" class="lyrico-select-lyric-line" value="Enai ulukki poothirukka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Suththi varum boomikulla
<input type="checkbox" class="lyrico-select-lyric-line" value="Suththi varum boomikulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai suththum saami pulla
<input type="checkbox" class="lyrico-select-lyric-line" value="Ennai suththum saami pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Mundhiyila mudichivaikka
<input type="checkbox" class="lyrico-select-lyric-line" value="Mundhiyila mudichivaikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Umma minukki kaathirukka
<input type="checkbox" class="lyrico-select-lyric-line" value="Umma minukki kaathirukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Umma ulukki poothirukka
<input type="checkbox" class="lyrico-select-lyric-line" value="Umma ulukki poothirukka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kammang koozha pola
<input type="checkbox" class="lyrico-select-lyric-line" value="Kammang koozha pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Karuvaattu thunda pola
<input type="checkbox" class="lyrico-select-lyric-line" value="Karuvaattu thunda pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollaama muzhungathamaya
<input type="checkbox" class="lyrico-select-lyric-line" value="Sollaama muzhungathamaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Umma minukki kaathirukka
<input type="checkbox" class="lyrico-select-lyric-line" value="Umma minukki kaathirukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Umma ulukki poothirukka
<input type="checkbox" class="lyrico-select-lyric-line" value="Umma ulukki poothirukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Umma ulukki poothirukka
<input type="checkbox" class="lyrico-select-lyric-line" value="Umma ulukki poothirukka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Yeahhh
<input type="checkbox" class="lyrico-select-lyric-line" value="Yeahhh"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanaanae naanaanae naanae nannae
<input type="checkbox" class="lyrico-select-lyric-line" value="Naanaanae naanaanae naanae nannae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanaanae naanaanae naanae nannae
<input type="checkbox" class="lyrico-select-lyric-line" value="Naanaanae naanaanae naanae nannae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeahhh
<input type="checkbox" class="lyrico-select-lyric-line" value="Yeahhh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kozhi koottukkulla
<input type="checkbox" class="lyrico-select-lyric-line" value="Kozhi koottukkulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhukku pullu vaikken naan
<input type="checkbox" class="lyrico-select-lyric-line" value="Edhukku pullu vaikken naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Maattu thozhuvula
<input type="checkbox" class="lyrico-select-lyric-line" value="Maattu thozhuvula"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhukku muttai thedudhen
<input type="checkbox" class="lyrico-select-lyric-line" value="Edhukku muttai thedudhen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Naanum kotti aayittenae
<input type="checkbox" class="lyrico-select-lyric-line" value="Naanum kotti aayittenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada idhu edhanaala
<input type="checkbox" class="lyrico-select-lyric-line" value="Ada idhu edhanaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalai mutti ooda vaikkam
<input type="checkbox" class="lyrico-select-lyric-line" value="Thalai mutti ooda vaikkam"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu andha ezhavala
<input type="checkbox" class="lyrico-select-lyric-line" value="Idhu andha ezhavala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kuvichu vacha nella pola
<input type="checkbox" class="lyrico-select-lyric-line" value="Kuvichu vacha nella pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Koorpaayum nenjaala
<input type="checkbox" class="lyrico-select-lyric-line" value="Koorpaayum nenjaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai kuththi kollathamla
<input type="checkbox" class="lyrico-select-lyric-line" value="Enai kuththi kollathamla"/>
</div>
<div class="lyrico-lyrics-wrapper">Em minukki kaathirukka
<input type="checkbox" class="lyrico-select-lyric-line" value="Em minukki kaathirukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai ulukki poothirukka
<input type="checkbox" class="lyrico-select-lyric-line" value="Enai ulukki poothirukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai ulukki poothirukka
<input type="checkbox" class="lyrico-select-lyric-line" value="Enai ulukki poothirukka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Hey nandu paadhaiyila
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey nandu paadhaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum vandiyottudhen
<input type="checkbox" class="lyrico-select-lyric-line" value="Naanum vandiyottudhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaandu pochunna
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaandu pochunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhutta ninnu pesudhen
<input type="checkbox" class="lyrico-select-lyric-line" value="Adhutta ninnu pesudhen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Panai mela kalaiyaama
<input type="checkbox" class="lyrico-select-lyric-line" value="Panai mela kalaiyaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan thongi nikkenae
<input type="checkbox" class="lyrico-select-lyric-line" value="Naan thongi nikkenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Paraikkulla isai pola
<input type="checkbox" class="lyrico-select-lyric-line" value="Paraikkulla isai pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan olinchirukkemla
<input type="checkbox" class="lyrico-select-lyric-line" value="Naan olinchirukkemla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Avan munna vandhutta
<input type="checkbox" class="lyrico-select-lyric-line" value="Avan munna vandhutta"/>
</div>
<div class="lyrico-lyrics-wrapper">Asangaama konnutta
<input type="checkbox" class="lyrico-select-lyric-line" value="Asangaama konnutta"/>
</div>
<div class="lyrico-lyrics-wrapper">Valikkaama vathaikkathamya
<input type="checkbox" class="lyrico-select-lyric-line" value="Valikkaama vathaikkathamya"/>
</div>
<div class="lyrico-lyrics-wrapper">Umma minukki kaathirukka
<input type="checkbox" class="lyrico-select-lyric-line" value="Umma minukki kaathirukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Umma ulukki poothirukka
<input type="checkbox" class="lyrico-select-lyric-line" value="Umma ulukki poothirukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Umma ulukki poothirukka
<input type="checkbox" class="lyrico-select-lyric-line" value="Umma ulukki poothirukka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Heyy oththa nilavai pola
<input type="checkbox" class="lyrico-select-lyric-line" value="Heyy oththa nilavai pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuththa vacha azhaguthamla
<input type="checkbox" class="lyrico-select-lyric-line" value="Kuththa vacha azhaguthamla"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai pichi thinguthamla
<input type="checkbox" class="lyrico-select-lyric-line" value="Enai pichi thinguthamla"/>
</div>
<div class="lyrico-lyrics-wrapper">Em minukki kaathirukka
<input type="checkbox" class="lyrico-select-lyric-line" value="Em minukki kaathirukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai ulukki poothirukka
<input type="checkbox" class="lyrico-select-lyric-line" value="Enai ulukki poothirukka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Neththi vagudukulla
<input type="checkbox" class="lyrico-select-lyric-line" value="Neththi vagudukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Aambalaiya saachirukka
<input type="checkbox" class="lyrico-select-lyric-line" value="Aambalaiya saachirukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Aarappottu vachirukka
<input type="checkbox" class="lyrico-select-lyric-line" value="Aarappottu vachirukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Em minukki kaathirukka
<input type="checkbox" class="lyrico-select-lyric-line" value="Em minukki kaathirukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai ulukki poothirukka
<input type="checkbox" class="lyrico-select-lyric-line" value="Enai ulukki poothirukka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Heyy suththi varum boomikulla
<input type="checkbox" class="lyrico-select-lyric-line" value="Heyy suththi varum boomikulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai suththum saami pulla
<input type="checkbox" class="lyrico-select-lyric-line" value="Ennai suththum saami pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Mundhiyila mudichivaikka
<input type="checkbox" class="lyrico-select-lyric-line" value="Mundhiyila mudichivaikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Umma minukki kaathirukka
<input type="checkbox" class="lyrico-select-lyric-line" value="Umma minukki kaathirukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Umma ulukki poothirukka
<input type="checkbox" class="lyrico-select-lyric-line" value="Umma ulukki poothirukka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kammang koozha pola
<input type="checkbox" class="lyrico-select-lyric-line" value="Kammang koozha pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Karuvaattu thunda pola
<input type="checkbox" class="lyrico-select-lyric-line" value="Karuvaattu thunda pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollaama muzhungathamaya
<input type="checkbox" class="lyrico-select-lyric-line" value="Sollaama muzhungathamaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Umma minukki kaathirukka
<input type="checkbox" class="lyrico-select-lyric-line" value="Umma minukki kaathirukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Umma ulukki poothirukka
<input type="checkbox" class="lyrico-select-lyric-line" value="Umma ulukki poothirukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Umma ulukki poothirukka
<input type="checkbox" class="lyrico-select-lyric-line" value="Umma ulukki poothirukka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Naanaanae naanaanae naanae nannae
<input type="checkbox" class="lyrico-select-lyric-line" value="Naanaanae naanaanae naanae nannae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanaanae naanaanae naanae nannae
<input type="checkbox" class="lyrico-select-lyric-line" value="Naanaanae naanaanae naanae nannae"/>
</div>
</pre>