---
title: "my love song lyrics"
album: "World Famous Lover"
artist: "Gopi Sundar"
lyricist: "Rehman"
director: "Kranthi Madhav"
path: "/albums/world-famous-lover-lyrics"
song: "My Love"
image: ../../images/albumart/world-famous-lover.jpg
date: 2020-02-14
lang: telugu
youtubeLink: "https://www.youtube.com/embed/FIP5HyTq_Ds"
type: "love"
singers:
  - Sri Krishna
  - Ramya Behara
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">I’m So Crazy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I’m So Crazy"/>
</div>
<div class="lyrico-lyrics-wrapper">For You Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="For You Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Life Is Singing
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life Is Singing"/>
</div>
<div class="lyrico-lyrics-wrapper">Love Melody
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love Melody"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Every Second
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Every Second"/>
</div>
<div class="lyrico-lyrics-wrapper">Feeling Awesome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Feeling Awesome"/>
</div>
<div class="lyrico-lyrics-wrapper">Heart Is Swinging
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heart Is Swinging"/>
</div>
<div class="lyrico-lyrics-wrapper">Like A Baby Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Like A Baby Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">I’m So Crazy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I’m So Crazy"/>
</div>
<div class="lyrico-lyrics-wrapper">For You Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="For You Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Life Is Singing
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life Is Singing"/>
</div>
<div class="lyrico-lyrics-wrapper">Love Melody
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love Melody"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Every Second
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Every Second"/>
</div>
<div class="lyrico-lyrics-wrapper">Feeling Awesome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Feeling Awesome"/>
</div>
<div class="lyrico-lyrics-wrapper">Heart Is Swinging
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heart Is Swinging"/>
</div>
<div class="lyrico-lyrics-wrapper">Like A Baby Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Like A Baby Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">My Love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="My Love"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasunu Meete
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasunu Meete"/>
</div>
<div class="lyrico-lyrics-wrapper">Edho Thiyyani Paatey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edho Thiyyani Paatey"/>
</div>
<div class="lyrico-lyrics-wrapper">Evaru Epudu Vinai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evaru Epudu Vinai"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhalo Jarige
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhalo Jarige"/>
</div>
<div class="lyrico-lyrics-wrapper">Symphony
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Symphony"/>
</div>
<div class="lyrico-lyrics-wrapper">I Feel now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Feel now"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">My Love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="My Love"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasunu Meete
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasunu Meete"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasunu Meete
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasunu Meete"/>
</div>
<div class="lyrico-lyrics-wrapper">Edho Thiyyani Paatey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edho Thiyyani Paatey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neetho Payanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neetho Payanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Okko Nimisham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okko Nimisham"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadiminadhile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadiminadhile"/>
</div>
<div class="lyrico-lyrics-wrapper">Thariminadhile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thariminadhile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vethike Udhayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vethike Udhayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhorike
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhorike"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalale Nijamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalale Nijamai"/>
</div>
<div class="lyrico-lyrics-wrapper">Niliche Neevalle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niliche Neevalle"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">My Love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="My Love"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasunu Meete
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasunu Meete"/>
</div>
<div class="lyrico-lyrics-wrapper">Edho… Thiyyani Paatey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edho… Thiyyani Paatey"/>
</div>
<div class="lyrico-lyrics-wrapper">Evaru… Epudu Vinai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evaru… Epudu Vinai"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhalo Jarige
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhalo Jarige"/>
</div>
<div class="lyrico-lyrics-wrapper">Symphony
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Symphony"/>
</div>
<div class="lyrico-lyrics-wrapper">I Feel Now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Feel Now"/>
</div>
</pre>
