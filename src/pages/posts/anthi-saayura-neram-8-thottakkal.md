---
title: "anthi saayura neram song lyrics"
album: "8 Thottakkal"
artist: "KS Sundaramurthy"
lyricist: "GKB"
director: "Sri Ganesh"
path: "/albums/8-thottakkal-lyrics"
song: "Anthi Saayura Neram"
image: ../../images/albumart/8-thottakkal.jpg
date: 2017-04-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/MdY_xoENDLw"
type: "happy"
singers:
  -	Yogi Sekar
  - Padmalatha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Anthi saayura neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anthi saayura neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Mandhaara chedi oram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandhaara chedi oram"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru ammavai paathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru ammavai paathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayyaa adicharaam kannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyaa adicharaam kannu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava sirichalaam ponnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava sirichalaam ponnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anthi saayura neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anthi saayura neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaikaa varapporam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaikaa varapporam"/>
</div>
<div class="lyrico-lyrics-wrapper">Eera pudavaiya paarthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eera pudavaiya paarthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaayuthu un kannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaayuthu un kannu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakkatha ninnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkatha ninnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kokkaka kokkaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kokkaka kokkaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kokkaka kovena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kokkaka kovena"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuyila pola paadinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuyila pola paadinaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kokkaka kokkaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kokkaka kokkaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kokkaka kovena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kokkaka kovena"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuyila pola paadinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuyila pola paadinaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuyila pola paadinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuyila pola paadinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava kuyila pola paadinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava kuyila pola paadinaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuyila pola paadinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuyila pola paadinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava kuyila pola paadinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava kuyila pola paadinaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kokkara kokkara ko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kokkara kokkara ko"/>
</div>
<div class="lyrico-lyrics-wrapper">Koraa kokkara kokkara ko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koraa kokkara kokkara ko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kokkara kokkara ko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kokkara kokkara ko"/>
</div>
<div class="lyrico-lyrics-wrapper">Koraa kokkara kokkara ko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koraa kokkara kokkara ko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kokkara kokkara ko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kokkara kokkara ko"/>
</div>
<div class="lyrico-lyrics-wrapper">Koraa kokkara kokkara ko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koraa kokkara kokkara ko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kokkara kokkara ko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kokkara kokkara ko"/>
</div>
<div class="lyrico-lyrics-wrapper">Koraa kokkara kokkara ko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koraa kokkara kokkara ko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pudhu sambaa nellapola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu sambaa nellapola"/>
</div>
<div class="lyrico-lyrics-wrapper">Thala thalavendru maarina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thala thalavendru maarina"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava thala thalavendru maarina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava thala thalavendru maarina"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava maama endru kooruvaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava maama endru kooruvaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meesha vacha machaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meesha vacha machaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethaan achaaram onnu podanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethaan achaaram onnu podanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada achchaaram onnu podanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada achchaaram onnu podanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pinna aattam aadi theekanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pinna aattam aadi theekanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rathinamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rathinamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Muthinamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthinamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aththai peththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aththai peththa"/>
</div>
<div class="lyrico-lyrics-wrapper">Arpudhamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arpudhamae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rathinamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rathinamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Muthinamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthinamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aththai peththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aththai peththa"/>
</div>
<div class="lyrico-lyrics-wrapper">Arpudhamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arpudhamae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kokkara kokkara ko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kokkara kokkara ko"/>
</div>
<div class="lyrico-lyrics-wrapper">Koraa kokkara kokkara ko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koraa kokkara kokkara ko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kokkara kokkara ko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kokkara kokkara ko"/>
</div>
<div class="lyrico-lyrics-wrapper">Koraa kokkara kokkara ko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koraa kokkara kokkara ko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kokkara kokkara ko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kokkara kokkara ko"/>
</div>
<div class="lyrico-lyrics-wrapper">Koraa kokkara kokkara ko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koraa kokkara kokkara ko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kokkara kokkara ko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kokkara kokkara ko"/>
</div>
<div class="lyrico-lyrics-wrapper">Koraa kokkara kokkara ko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koraa kokkara kokkara ko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anthi saayura neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anthi saayura neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Mandhaara chedi oram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandhaara chedi oram"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru ammavai paathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru ammavai paathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayyaa adicharaam kannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyaa adicharaam kannu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava sirichalaam ponnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava sirichalaam ponnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kokkaka kokkaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kokkaka kokkaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kokkaka kovena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kokkaka kovena"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuyila pola paadinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuyila pola paadinaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuyila pola paadinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuyila pola paadinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava kuyila pola paadinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava kuyila pola paadinaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuyila pola paadinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuyila pola paadinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava kuyila pola paadinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava kuyila pola paadinaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kokkara kokkara ko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kokkara kokkara ko"/>
</div>
<div class="lyrico-lyrics-wrapper">Koraa kokkara kokkara ko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koraa kokkara kokkara ko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kokkara kokkara ko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kokkara kokkara ko"/>
</div>
<div class="lyrico-lyrics-wrapper">Koraa kokkara kokkara ko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koraa kokkara kokkara ko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kokkara kokkara ko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kokkara kokkara ko"/>
</div>
<div class="lyrico-lyrics-wrapper">Koraa kokkara kokkara ko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koraa kokkara kokkara ko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kokkara kokkara ko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kokkara kokkara ko"/>
</div>
<div class="lyrico-lyrics-wrapper">Koraa kokkara kokkara ko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koraa kokkara kokkara ko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anthi saayura neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anthi saayura neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Mandhaara chedi oram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandhaara chedi oram"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru ammavai paathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru ammavai paathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayyaa adicharaam kannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyaa adicharaam kannu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava sirichalaam ponnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava sirichalaam ponnu"/>
</div>
</pre>
