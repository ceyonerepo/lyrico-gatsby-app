---
title: "saroja song lyrics"
album: "Vaanam"
artist: "Yuvan Shankar Raja"
lyricist: "Vairamuthu"
director: "Krish"
path: "/albums/vaanam-lyrics"
song: "Saroja"
image: ../../images/albumart/vaanam.jpg
date: 2011-04-29
lang: tamil
youtubeLink: "https://www.youtube.com/embed/gPkbiu1Gzr8"
type: "happy"
singers:
  - not known
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Sa Ni Sa Dha Ma Pa Ri Saa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sa Ni Sa Dha Ma Pa Ri Saa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sarojaaa Aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarojaaa Aaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sarojaa Aaa Aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarojaa Aaa Aaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rathiri Pookum Thoottam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rathiri Pookum Thoottam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ragasiyamaana Aattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ragasiyamaana Aattam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adikadi Edukkum Thagamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adikadi Edukkum Thagamum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aangalai Iyakkum Vegamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aangalai Iyakkum Vegamum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maalai Katridum Kalviyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maalai Katridum Kalviyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manasu Virumbidum Palliyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu Virumbidum Palliyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jananamum Idhu Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jananamum Idhu Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maranamum Idhu Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maranamum Idhu Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jaga Jaga Jaga Jaga Jalamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaga Jaga Jaga Jaga Jalamum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inbamum Idhu Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inbamum Idhu Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thunbamum Idhu Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunbamum Idhu Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thoda Thoda Haiyooo Tholaivadhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoda Thoda Haiyooo Tholaivadhum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu Thaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Thaaney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Elunthu Vaada Enna Venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Elunthu Vaada Enna Venum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Elunthu Vaada Enna Venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Elunthu Vaada Enna Venum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Elunthu Vaada Enna Venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Elunthu Vaada Enna Venum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha Hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Elunthu Vaada Enna Venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Elunthu Vaada Enna Venum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha Aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Aaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rathiri Pookum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rathiri Pookum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ragasiyamaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ragasiyamaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adikadi Edukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adikadi Edukkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aangalai Iyakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aangalai Iyakkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maalai Katridum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maalai Katridum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manasu Virumbidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu Virumbidum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jananamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jananamum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maranamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maranamum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jaga Jaga Jaga Jaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaga Jaga Jaga Jaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inbamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inbamum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thunbamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunbamum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tholaivadhum Idhu Thaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholaivadhum Idhu Thaaney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Elunthu Vaada Enna Venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Elunthu Vaada Enna Venum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Elunthu Vaada Enna Venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Elunthu Vaada Enna Venum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Parpadhu Thappey Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parpadhu Thappey Illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Panam Thaan Inage Ellai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panam Thaan Inage Ellai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iraviley Inba Leelai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iraviley Inba Leelai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aasaikku Vayasey Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasaikku Vayasey Illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaasai Thandhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasai Thandhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal Inthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Inthaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Melum Thantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Melum Thantha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sorghum Intha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sorghum Intha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sadu Gudu Sadu Gudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sadu Gudu Sadu Gudu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thodu Thodu Thottidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodu Thodu Thottidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thottadham Nee Moochinai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottadham Nee Moochinai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iluthu Pudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iluthu Pudi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Shiva Shivaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shiva Shivaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Elunthu Vaada Enna Venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Elunthu Vaada Enna Venum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Elunthu Vaada Enna Venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Elunthu Vaada Enna Venum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Elunthu Vaada Enna Venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Elunthu Vaada Enna Venum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Elunthu Vaada Enna Venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Elunthu Vaada Enna Venum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Elunthu Vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Elunthu Vaada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indhaa Vechikoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indhaa Vechikoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Elunthu Vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Elunthu Vaada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indhaa Vechiko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indhaa Vechiko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Elunthu Vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Elunthu Vaada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indhaa Vechiko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indhaa Vechiko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Elunthu Vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Elunthu Vaada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indhaa Vechikoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indhaa Vechikoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Elunthu Vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Elunthu Vaada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indhaa Vechikoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indhaa Vechikoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Elunthu Vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Elunthu Vaada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indhaa Vechikoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indhaa Vechikoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Elunthu Vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Elunthu Vaada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indhaa Vechikoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indhaa Vechikoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Elunthu Vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Elunthu Vaada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indhaa Vechikoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indhaa Vechikoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Elunthu Vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Elunthu Vaada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indhaa Vechikoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indhaa Vechikoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Elunthu Vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Elunthu Vaada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indhaa Vechikoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indhaa Vechikoo"/>
</div>
</pre>
