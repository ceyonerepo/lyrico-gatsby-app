---
title: "vellai pookal song lyrics"
album: "Kannathil Muthamittal"
artist: "A. R. Rahman"
lyricist: "Vairamuthu"
director: "Mani Ratnam"
path: "/albums/kannathil-muthamittal-song-lyrics"
song: "Vellai Pookal Ulagam Engum"
image: ../../images/albumart/kannathil-muthamittal.jpg
date: 2002-02-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/UGkMPEDF7U8"
type: "melody"
singers:
  - A.R. Rahmanm
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vellai Pookal Ulagam Engum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vellai Pookal Ulagam Engum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Malargavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malargavey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidiyum Bhoomi Amaidhikaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidiyum Bhoomi Amaidhikaaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidigavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidigavey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mannmel Manjal Veluchcham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannmel Manjal Veluchcham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhugavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhugavey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Malarey Sombal Muriththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malarey Sombal Muriththu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ezhugavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhugavey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuzhanthai Vizhikattumey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuzhanthai Vizhikattumey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaayin Katha Kathappil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaayin Katha Kathappil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ulagam Vidiyattumey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagam Vidiyattumey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pillayin Sirumugha Sirippil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pillayin Sirumugha Sirippil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vellai Pookal Ulagam Engum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vellai Pookal Ulagam Engum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Malargavey Malargavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malargavey Malargavey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidiyum Bhoomi Amaidhikaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidiyum Bhoomi Amaidhikaaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidigavey Vidigavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidigavey Vidigavey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mannmel Manjal Veluchcham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannmel Manjal Veluchcham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhugavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhugavey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Malarey Sombal Muriththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malarey Sombal Muriththu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ezhugavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhugavey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuzhanthai Vizhikattumey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuzhanthai Vizhikattumey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaayin Katha Kathappil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaayin Katha Kathappil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ulagam Vidiyattumey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagam Vidiyattumey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pillayin Sirumugha Sirippil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pillayin Sirumugha Sirippil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaatrin Perisaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatrin Perisaiyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mazhai Paadum Paadalgalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai Paadum Paadalgalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Maunam Pol Inbam Tharumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Maunam Pol Inbam Tharumo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kodi Keertanaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodi Keertanaiyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kavi Kortha Vaarthaigalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavi Kortha Vaarthaigalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thuli Kanner Pol Arththam Tharumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuli Kanner Pol Arththam Tharumo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vellai Pookal Ulagam Engum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vellai Pookal Ulagam Engum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Malargavey Malargavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malargavey Malargavey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidiyum Bhoomi Amaidhikaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidiyum Bhoomi Amaidhikaaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidigavey Vidigavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidigavey Vidigavey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mannmel Manjal Veluchcham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannmel Manjal Veluchcham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhugavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhugavey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Malarey Sombal Muriththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malarey Sombal Muriththu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ezhugavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhugavey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yengu Siru Kuzhanthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yengu Siru Kuzhanthai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Than Kaigal Neetidumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Than Kaigal Neetidumo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andu Thondraiyo Kollai Nilavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andu Thondraiyo Kollai Nilavey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engu Manidha Inam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engu Manidha Inam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Por Oyinthu Sayinthidumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Por Oyinthu Sayinthidumo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Angu Koovadho Vellai Kuyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Angu Koovadho Vellai Kuyile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vellai Pookal Ulagam Engum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vellai Pookal Ulagam Engum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Malargavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malargavey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidiyum Bhoomi Amaidhikaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidiyum Bhoomi Amaidhikaaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidigavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidigavey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mannmel Manjal Veluchcham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannmel Manjal Veluchcham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhugavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhugavey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Malarey Sombal Muriththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malarey Sombal Muriththu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ezhugavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhugavey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vellai Pookal Ulagam Engum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vellai Pookal Ulagam Engum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Malargavey Malargavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malargavey Malargavey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidiyum Bhoomi Amaidhikaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidiyum Bhoomi Amaidhikaaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidigavey Vidigavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidigavey Vidigavey"/>
</div>
</pre>
