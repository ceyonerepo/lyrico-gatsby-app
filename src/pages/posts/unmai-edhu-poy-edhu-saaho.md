---
title: "unmai edhu poy edhu song lyrics"
album: "Saaho"
artist: "Shankar–Ehsaan–Loy"
lyricist: "Madhan Karky"
director: "Sujeeth"
path: "/albums/saaho-lyrics"
song: "Unmai Edhu Poy Edhu"
image: ../../images/albumart/saaho.jpg
date: 2019-08-30
lang: tamil
youtubeLink: "https://www.youtube.com/embed/LMuXwm4tfMc"
type: "love"
singers:
  - Shweta Mohan
  - Shankar Mahadevan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Oru Paadhi Mei Enbadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Paadhi Mei Enbadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Maru Paadhi Poi Enbadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maru Paadhi Poi Enbadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Avai Rendum Ondrae Yena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avai Rendum Ondrae Yena"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kangal Poi Solludha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kangal Poi Solludha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanavellam Mei Enbadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavellam Mei Enbadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nijamellam Poi Enbadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijamellam Poi Enbadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Avai Rendum Ondrae Yena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avai Rendum Ondrae Yena"/>
</div>
<div class="lyrico-lyrics-wrapper">En Nenjam Mei Solludha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nenjam Mei Solludha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unmai Edhu Poi Edhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmai Edhu Poi Edhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollidu Anbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollidu Anbae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unmai Edhu Poi Edhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmai Edhu Poi Edhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unmai Edhu Poi Edhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmai Edhu Poi Edhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollidu Anbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollidu Anbae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unmai Edhu Poi Edhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmai Edhu Poi Edhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhan Munnae Kaana Vizha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhan Munnae Kaana Vizha"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholaigindren Yen Kaadhala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholaigindren Yen Kaadhala"/>
</div>
<div class="lyrico-lyrics-wrapper">Poi Ennum Vaan Meedhilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poi Ennum Vaan Meedhilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mei Endra Thondrum Nilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mei Endra Thondrum Nilaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Izha Oodha Vinnin Ozhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Izha Oodha Vinnin Ozhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhi Rendil En Kaadhali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhi Rendil En Kaadhali"/>
</div>
<div class="lyrico-lyrics-wrapper">Mei Ennum Kaar Ondrilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mei Ennum Kaar Ondrilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Poi Endra Keechum Kili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poi Endra Keechum Kili"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unmai Edhu Poi Edhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmai Edhu Poi Edhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollidu Anbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollidu Anbae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unmai Edhu Poi Edhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmai Edhu Poi Edhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unmai Edhu Poi Edhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmai Edhu Poi Edhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollidu Anbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollidu Anbae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unmai Edhu Poi Edhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmai Edhu Poi Edhu"/>
</div>
</pre>
