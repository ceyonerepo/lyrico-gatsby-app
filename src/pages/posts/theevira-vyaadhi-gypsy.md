---
title: "theevira vyaadhi song lyrics"
album: "Gypsy"
artist: "Santhosh Narayanan"
lyricist: "Arivu"
director: "Raju Murugan"
path: "/albums/gypsy-song-lyrics"
song: "Theevira Vyaadhi"
image: ../../images/albumart/gypsy.jpg
date: 2020-03-06
lang: tamil
youtubeLink: "https://www.youtube.com/embed/0wPGTAiF71g"
type: "mass"
singers:
  - Arivu
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Mani dhanai mani dhanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mani dhanai mani dhanai"/>
</div>
<div class="lyrico-lyrics-wrapper">Veruppadhai arindhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veruppadhai arindhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Pirivinai tharugira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirivinai tharugira"/>
</div>
<div class="lyrico-lyrics-wrapper">Valithanai adaindhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valithanai adaindhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Mozhi madha inam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mozhi madha inam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thani thani ena thirindhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thani thani ena thirindhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Pirikkira ula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirikkira ula"/>
</div>
<div class="lyrico-lyrics-wrapper">Ula ginai pirikkira ithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ula ginai pirikkira ithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Theevira vyaathi vyaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theevira vyaathi vyaathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vyaathi vyaathi vyaathi vyaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vyaathi vyaathi vyaathi vyaathi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koovi koovi kooraakki podum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koovi koovi kooraakki podum"/>
</div>
<div class="lyrico-lyrics-wrapper">Theevira vyaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theevira vyaathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vyaathi vyaathi vyaathi vyaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vyaathi vyaathi vyaathi vyaathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vyaathi vyaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vyaathi vyaathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadhi paadhi paazhakki podum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadhi paadhi paazhakki podum"/>
</div>
<div class="lyrico-lyrics-wrapper">Theevira vyaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theevira vyaathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vyaathi vyaathi vyaathi vyaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vyaathi vyaathi vyaathi vyaathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vyaathi vyaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vyaathi vyaathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Koovi koovi kooraakki podum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koovi koovi kooraakki podum"/>
</div>
<div class="lyrico-lyrics-wrapper">Theevira vyaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theevira vyaathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vyaathi vyaathi vyaathi vyaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vyaathi vyaathi vyaathi vyaathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vyaathi vyaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vyaathi vyaathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Desam paadhi theeradhoo intha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Desam paadhi theeradhoo intha"/>
</div>
<div class="lyrico-lyrics-wrapper">Theevira vyaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theevira vyaathi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Padhavi kaattil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padhavi kaattil"/>
</div>
<div class="lyrico-lyrics-wrapper">Vettaikalaigira vyaabaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vettaikalaigira vyaabaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannattu kudigalai vikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannattu kudigalai vikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Arasiyal oodhaari naadoodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arasiyal oodhaari naadoodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Viraiyum kodaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viraiyum kodaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Payanam unnai thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Payanam unnai thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ododi alaiyum poraali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ododi alaiyum poraali"/>
</div>
<div class="lyrico-lyrics-wrapper">Eriyum intha seidhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eriyum intha seidhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeah yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeah yeah"/>
</div>
<div class="lyrico-lyrics-wrapper">Sethathu pinangalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sethathu pinangalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Rathamum sadhaiyum uranga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rathamum sadhaiyum uranga"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeah yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeah yeah"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethanai idhangala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethanai idhangala"/>
</div>
<div class="lyrico-lyrics-wrapper">Makkali piritha madhangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Makkali piritha madhangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeah yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeah yeah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bathil adi tharuvathu yudham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bathil adi tharuvathu yudham"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesina vaaigalai suduvadhu kuttram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesina vaaigalai suduvadhu kuttram"/>
</div>
<div class="lyrico-lyrics-wrapper">Adikkina aayudham motham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adikkina aayudham motham"/>
</div>
<div class="lyrico-lyrics-wrapper">Maruthuvamanaiyil vedikkira satham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maruthuvamanaiyil vedikkira satham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theevira theevira theevira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theevira theevira theevira"/>
</div>
<div class="lyrico-lyrics-wrapper">Vyaathi vyaathi vyaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vyaathi vyaathi vyaathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Theevira theevira theevira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theevira theevira theevira"/>
</div>
<div class="lyrico-lyrics-wrapper">Vyaathi vyaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vyaathi vyaathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Theevira theevira theevira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theevira theevira theevira"/>
</div>
<div class="lyrico-lyrics-wrapper">Vyaathi vyaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vyaathi vyaathi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theevira theevira theevira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theevira theevira theevira"/>
</div>
<div class="lyrico-lyrics-wrapper">Vyaathi vyaathi vyaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vyaathi vyaathi vyaathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Illai intha mannil ellai kodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illai intha mannil ellai kodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannai thonda kodi mandai oodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannai thonda kodi mandai oodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Illai intha mannil ellai kodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illai intha mannil ellai kodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannai thonda thonda thonda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannai thonda thonda thonda"/>
</div>
<div class="lyrico-lyrics-wrapper">Mandai oodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandai oodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Purappatten varuven munnae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Purappatten varuven munnae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalagathaal kadiven unnai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalagathaal kadiven unnai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vedigundu thiriyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedigundu thiriyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru pirivinai muttrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru pirivinai muttrum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pala thalaimurai seththum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala thalaimurai seththum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalavaram ena suththum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalavaram ena suththum"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharai vizhuvadhu moththam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharai vizhuvadhu moththam"/>
</div>
<div class="lyrico-lyrics-wrapper">Namm thandhai ratham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namm thandhai ratham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theevira theevira theevira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theevira theevira theevira"/>
</div>
<div class="lyrico-lyrics-wrapper">Theevira vyaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theevira vyaathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Theevira theevira theevira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theevira theevira theevira"/>
</div>
<div class="lyrico-lyrics-wrapper">Theevira vyaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theevira vyaathi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vyaathi vyaathi vyaathi vyaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vyaathi vyaathi vyaathi vyaathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vyaathi vyaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vyaathi vyaathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Koovi koovi kooraakki podum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koovi koovi kooraakki podum"/>
</div>
<div class="lyrico-lyrics-wrapper">Theevira vyaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theevira vyaathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vyaathi vyaathi vyaathi vyaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vyaathi vyaathi vyaathi vyaathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vyaathi vyaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vyaathi vyaathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadhi paadhi paazhakki podum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadhi paadhi paazhakki podum"/>
</div>
<div class="lyrico-lyrics-wrapper">Theevira vyaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theevira vyaathi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theevira theevira theevira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theevira theevira theevira"/>
</div>
<div class="lyrico-lyrics-wrapper">Vyaathi vyaathi vyaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vyaathi vyaathi vyaathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Theevira theevira theevira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theevira theevira theevira"/>
</div>
<div class="lyrico-lyrics-wrapper">Vyaathi vyaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vyaathi vyaathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Theevira theevira theevira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theevira theevira theevira"/>
</div>
<div class="lyrico-lyrics-wrapper">Vyaathi vyaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vyaathi vyaathi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theevira theevira theevira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theevira theevira theevira"/>
</div>
<div class="lyrico-lyrics-wrapper">Pennai tholugira mannil erigira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pennai tholugira mannil erigira"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaama kolaiveri ooyaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaama kolaiveri ooyaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Innum pirivinai solli thirigira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum pirivinai solli thirigira"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna thimiradhu maaratha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna thimiradhu maaratha"/>
</div>
<div class="lyrico-lyrics-wrapper">Pennai tholugira mannil erigira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pennai tholugira mannil erigira"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaama kolaiveri ooyaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaama kolaiveri ooyaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Innum pirivinai solli thirigira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum pirivinai solli thirigira"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna thimiradhu maaratha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna thimiradhu maaratha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unavukku urimaikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unavukku urimaikku"/>
</div>
<div class="lyrico-lyrics-wrapper">Uravai ketkkira mazhalaikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uravai ketkkira mazhalaikku"/>
</div>
<div class="lyrico-lyrics-wrapper">Varumaikku verumaikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varumaikku verumaikku"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirai saaikkira mirugathukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirai saaikkira mirugathukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vyaathi vyaathi vyaathi vyaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vyaathi vyaathi vyaathi vyaathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vyaathi vyaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vyaathi vyaathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Koovi koovi kooraakki podum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koovi koovi kooraakki podum"/>
</div>
<div class="lyrico-lyrics-wrapper">Theevira vyaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theevira vyaathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vyaathi vyaathi vyaathi vyaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vyaathi vyaathi vyaathi vyaathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vyaathi vyaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vyaathi vyaathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadhi paadhi paazhakki podum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadhi paadhi paazhakki podum"/>
</div>
<div class="lyrico-lyrics-wrapper">Theevira vyaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theevira vyaathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vyaathi vyaathi vyaathi vyaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vyaathi vyaathi vyaathi vyaathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vyaathi vyaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vyaathi vyaathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Desam paadhi theeradhoo intha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Desam paadhi theeradhoo intha"/>
</div>
<div class="lyrico-lyrics-wrapper">Theevira vyaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theevira vyaathi"/>
</div>
</pre>
