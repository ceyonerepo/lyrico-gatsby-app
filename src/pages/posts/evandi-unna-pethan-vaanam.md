---
title: "evandi unna pethan song lyrics"
album: "Vaanam"
artist: "Yuvan Shankar Raja"
lyricist: "Silambarasan - Yuvan Shankar Raja"
director: "Krish"
path: "/albums/vaanam-lyrics"
song: "Evandi Unna Pethan"
image: ../../images/albumart/vaanam.jpg
date: 2011-04-29
lang: tamil
youtubeLink: "https://www.youtube.com/embed/XIVL-cHbFd4"
type: "happy"
singers:
  - Silambarasan
  - Yuvan Shankar Raja
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Oh Baby I Feel Like Flying
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Baby I Feel Like Flying"/>
</div>
<div class="lyrico-lyrics-wrapper">Flying Above Up In The Air
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Flying Above Up In The Air"/>
</div>
<div class="lyrico-lyrics-wrapper">When I Look At You
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="When I Look At You"/>
</div>
<div class="lyrico-lyrics-wrapper">Looking And Me Like
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Looking And Me Like"/>
</div>
<div class="lyrico-lyrics-wrapper">You Wanna Make Love To Me Then
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Wanna Make Love To Me Then"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unna Partha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Partha"/>
</div>
<div class="lyrico-lyrics-wrapper">First Secondla Enna Kannom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="First Secondla Enna Kannom"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedi Parkuren Kandapadi Naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedi Parkuren Kandapadi Naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Partha First Secondla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Partha First Secondla"/>
</div>
<div class="lyrico-lyrics-wrapper">Irundhae Enna Kaanom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irundhae Enna Kaanom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thedi Parkuren Kandapadi Naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedi Parkuren Kandapadi Naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Saththiyama Ennaku Venaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saththiyama Ennaku Venaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kandeeppa Ennaku Naan Venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandeeppa Ennaku Naan Venum"/>
</div>
<div class="lyrico-lyrics-wrapper">Saththiyama Ennaku Venaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saththiyama Ennaku Venaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kandeeppa Ennaku Naan Venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandeeppa Ennaku Naan Venum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Onna Enna Kandupidichu Kudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna Enna Kandupidichu Kudu"/>
</div>
<div class="lyrico-lyrics-wrapper">Illa Romba Simple Unna Enaku Kudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illa Romba Simple Unna Enaku Kudu"/>
</div>
<div class="lyrico-lyrics-wrapper">Illa Thayavu Senchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illa Thayavu Senchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Gun Eduthu Enna Sudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Gun Eduthu Enna Sudu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Evandi Unna Pethan Pethan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evandi Unna Pethan Pethan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pethan Pethan Pethan Pethan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pethan Pethan Pethan Pethan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiyila Kedacha Sethan Sethan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyila Kedacha Sethan Sethan"/>
</div>
<div class="lyrico-lyrics-wrapper">Sethan Sethan Sethan Sethan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sethan Sethan Sethan Sethan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Evandi Unna Pethan Pethan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evandi Unna Pethan Pethan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pethan Pethan Pethan Pethan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pethan Pethan Pethan Pethan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiyila Kedacha Sethan Sethan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyila Kedacha Sethan Sethan"/>
</div>
<div class="lyrico-lyrics-wrapper">Sethan Sethan Sethan Sethan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sethan Sethan Sethan Sethan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Move Your Body Girl
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Move Your Body Girl"/>
</div>
<div class="lyrico-lyrics-wrapper">Move Your Body
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Move Your Body"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Facebook Statusum Neethan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Facebook Statusum Neethan"/>
</div>
<div class="lyrico-lyrics-wrapper">En Twitter Tweetingum Neethan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Twitter Tweetingum Neethan"/>
</div>
<div class="lyrico-lyrics-wrapper">En Skype Callum Neethan Neethan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Skype Callum Neethan Neethan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En BBMum Neethan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En BBMum Neethan"/>
</div>
<div class="lyrico-lyrics-wrapper">En Facetimum Neethan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Facetimum Neethan"/>
</div>
<div class="lyrico-lyrics-wrapper">En Iphone Ipad Ellamey Neethan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Iphone Ipad Ellamey Neethan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Itune Playlistum Neethan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Itune Playlistum Neethan"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhil Love Songsum Neethan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhil Love Songsum Neethan"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhil Play Agura Spaeker Neethan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhil Play Agura Spaeker Neethan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Appaavum Neethan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Appaavum Neethan"/>
</div>
<div class="lyrico-lyrics-wrapper">En Ammaavum Neethan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Ammaavum Neethan"/>
</div>
<div class="lyrico-lyrics-wrapper">En Sothu Sugam Ellamey Neethan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Sothu Sugam Ellamey Neethan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Kadavulum Neethan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kadavulum Neethan"/>
</div>
<div class="lyrico-lyrics-wrapper">En Uyirum Neethan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Uyirum Neethan"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakellaamae Neethan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakellaamae Neethan"/>
</div>
<div class="lyrico-lyrics-wrapper">Naathan Neethan Neethan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naathan Neethan Neethan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Move Your Body Girl
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Move Your Body Girl"/>
</div>
<div class="lyrico-lyrics-wrapper">Move Your Body
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Move Your Body"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Evandi Unna Pethan Peththaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evandi Unna Pethan Peththaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pethan Pethan Pethan Peththaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pethan Pethan Pethan Peththaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiyila Kedacha Sethan Seththaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyila Kedacha Sethan Seththaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Sethan Sethan Sethan Seththaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sethan Sethan Sethan Seththaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Evandi Unna Pethan Peththaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evandi Unna Pethan Peththaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pethan Pethan Pethan Peththaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pethan Pethan Pethan Peththaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiyila Kedacha Sethan Seththaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyila Kedacha Sethan Seththaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Sethan Sethan Sethan Seththaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sethan Sethan Sethan Seththaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Move Your Body Girl
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Move Your Body Girl"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Baby I Feel Like Flying High
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Baby I Feel Like Flying High"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh High Baby Girl
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh High Baby Girl"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">You Know Make Go So Wild
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Know Make Go So Wild"/>
</div>
<div class="lyrico-lyrics-wrapper">O Wild Baby Girl
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Wild Baby Girl"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Baby I Feel Like Flying
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Baby I Feel Like Flying"/>
</div>
<div class="lyrico-lyrics-wrapper">Flying Above Up In The Air
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Flying Above Up In The Air"/>
</div>
<div class="lyrico-lyrics-wrapper">When I Look At You
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="When I Look At You"/>
</div>
<div class="lyrico-lyrics-wrapper">Looking And Me Like
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Looking And Me Like"/>
</div>
<div class="lyrico-lyrics-wrapper">You Wanna Make Love To Me Then
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Wanna Make Love To Me Then"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Paste Brushum Naanthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Paste Brushum Naanthan"/>
</div>
<div class="lyrico-lyrics-wrapper">Un  Shower Gellum Naanthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un  Shower Gellum Naanthan"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Manam Kakira Melaadai Naanthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Manam Kakira Melaadai Naanthan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Lip Glossum Naanthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Lip Glossum Naanthan"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Eye Liner Naanthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Eye Liner Naanthan"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Azhagu Koottura Make Up Pena Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Azhagu Koottura Make Up Pena Naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Teddy Bear Naanthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Teddy Bear Naanthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Bed And Pillow Naanthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Bed And Pillow Naanthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Veettoda Night Watchman Naanthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Veettoda Night Watchman Naanthan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Nagamum Sadhaiyum Naanthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Nagamum Sadhaiyum Naanthan"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Ezhumbum Naranbum Naanthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Ezhumbum Naranbum Naanthan"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhil Ulla Odum Rathamum Naanthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhil Ulla Odum Rathamum Naanthan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Friendum Nanthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Friendum Nanthan"/>
</div>
<div class="lyrico-lyrics-wrapper">Boy Friendum Nanthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boy Friendum Nanthan"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaku Ellamey Naanthan Naanthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaku Ellamey Naanthan Naanthan"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanthan Naanthan Naanthan Naanthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanthan Naanthan Naanthan Naanthan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Who The F Is Your Daddy Daddy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Who The F Is Your Daddy Daddy"/>
</div>
<div class="lyrico-lyrics-wrapper">Daddy Daddy Daddy Daddy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daddy Daddy Daddy Daddy"/>
</div>
<div class="lyrico-lyrics-wrapper">If I See Him He’s A Dead Body Body
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="If I See Him He’s A Dead Body Body"/>
</div>
<div class="lyrico-lyrics-wrapper">Body Body Body Body
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Body Body Body Body"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Who The F Is Your Daddy Daddy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Who The F Is Your Daddy Daddy"/>
</div>
<div class="lyrico-lyrics-wrapper">Daddy Daddy Daddy Daddy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daddy Daddy Daddy Daddy"/>
</div>
<div class="lyrico-lyrics-wrapper">If I See Him He’s A Dead Body Body
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="If I See Him He’s A Dead Body Body"/>
</div>
<div class="lyrico-lyrics-wrapper">Body Body Body Body
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Body Body Body Body"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Evandi Unna Pethan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evandi Unna Pethan"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan Kaiyila Kedacha Sethan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan Kaiyila Kedacha Sethan"/>
</div>
<div class="lyrico-lyrics-wrapper">Evandi Unna Pethan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evandi Unna Pethan"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan Kaiyila Kedacha Sethan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan Kaiyila Kedacha Sethan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Evandi Unna Pethaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evandi Unna Pethaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan Kaiyila Kedacha Sethaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan Kaiyila Kedacha Sethaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Evandi Unna Pethaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evandi Unna Pethaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan Kaiyila Kedacha Sethaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan Kaiyila Kedacha Sethaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Baby I Feel Like Flying High
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Baby I Feel Like Flying High"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh High Baby Girl
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh High Baby Girl"/>
</div>
<div class="lyrico-lyrics-wrapper">You Know Make Go So Wild
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Know Make Go So Wild"/>
</div>
<div class="lyrico-lyrics-wrapper">O Wild Baby Girl
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Wild Baby Girl"/>
</div>
</pre>
