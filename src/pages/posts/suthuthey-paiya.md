---
title: 'suthudhae suthudhae boomi song lyrics'
album: 'Paiya'
artist: 'Yuvan Shankar Raja'
lyricist: 'Na Muthukumar'
director: 'N.Lingusamy'
path: '/albums/paiya-song-lyrics'
song: 'Suthudhae Suthudhae Boomi'
image: ../../images/albumart/paiya.jpg
date: 2010-04-02
lang: tamil
singers: 
- Karthik
- Sunitha Sarathy
youtubeLink: "https://www.youtube.com/embed/FdPTVt3J4OQ"
type: 'love'
---

<pre class="lyrics-native">
Coming Soon
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Suthudhae suthudhae boomi
<input type="checkbox" class="lyrico-select-lyric-line" value="Suthudhae suthudhae boomi"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu podhumadaa podhumadaa saami
<input type="checkbox" class="lyrico-select-lyric-line" value="Idhu podhumadaa podhumadaa saami"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Hey hey hey hiya hiya hiya
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey hey hey hiya hiya hiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa naa naa naaa naaa naa
<input type="checkbox" class="lyrico-select-lyric-line" value="Naa naa naa naaa naaa naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naaa naaa naa naaa naa haa aaa aaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Naaa naaa naa naaa naa haa aaa aaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hiya hiya hiya yeah heyy…
<input type="checkbox" class="lyrico-select-lyric-line" value="Hiya hiya hiya yeah heyy…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Suthudhae suthudhae boomi
<input type="checkbox" class="lyrico-select-lyric-line" value="Suthudhae suthudhae boomi"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu podhumadaa podhumadaa saami
<input type="checkbox" class="lyrico-select-lyric-line" value="Idhu podhumadaa podhumadaa saami"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey suthudhae suthudhae boomi
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey suthudhae suthudhae boomi"/>
</div>
<div class="lyrico-lyrics-wrapper">Raa raa raa raadhae raadhae raadhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Raa raa raa raadhae raadhae raadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagiya raadhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Azhagiya raadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarvaiyil pesi pesi pesi
<input type="checkbox" class="lyrico-select-lyric-line" value="Paarvaiyil pesi pesi pesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pazhagiya raadhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Pazhagiya raadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Edhanaalae indha maatram
<input type="checkbox" class="lyrico-select-lyric-line" value="Edhanaalae indha maatram"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasukkul edho maaya thotram
<input type="checkbox" class="lyrico-select-lyric-line" value="Manasukkul edho maaya thotram"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhanaalae indha aattam
<input type="checkbox" class="lyrico-select-lyric-line" value="Edhanaalae indha aattam"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayathil nindru oonjal aattam
<input type="checkbox" class="lyrico-select-lyric-line" value="Idhayathil nindru oonjal aattam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Suthudhae suthudhae boomi
<input type="checkbox" class="lyrico-select-lyric-line" value="Suthudhae suthudhae boomi"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu podhumadaa podhumadaa saami..eee…
<input type="checkbox" class="lyrico-select-lyric-line" value="Idhu podhumadaa podhumadaa saami..eee…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Than naana na ta ra ra ra
<input type="checkbox" class="lyrico-select-lyric-line" value="Than naana na ta ra ra ra"/>
</div>
<div class="lyrico-lyrics-wrapper">Than naana na ta ra ra ra ra
<input type="checkbox" class="lyrico-select-lyric-line" value="Than naana na ta ra ra ra ra"/>
</div>
<div class="lyrico-lyrics-wrapper">Ahaii eiii woah aha eeiii…
<input type="checkbox" class="lyrico-select-lyric-line" value="Ahaii eiii woah aha eeiii…"/>
</div>
<div class="lyrico-lyrics-wrapper">Oooo..aheiii…aaa….aaa…
<input type="checkbox" class="lyrico-select-lyric-line" value="Oooo..aheiii…aaa….aaa…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Sirithu sirithuthaan pesumbodhilae
<input type="checkbox" class="lyrico-select-lyric-line" value="Sirithu sirithuthaan pesumbodhilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Valaigalai nee virikiraai
<input type="checkbox" class="lyrico-select-lyric-line" value="Valaigalai nee virikiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Saivam endruthaan solli kondu
<input type="checkbox" class="lyrico-select-lyric-line" value="Saivam endruthaan solli kondu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kolaigalai yen seigiraai
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee kolaigalai yen seigiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Angum ingum ennai
<input type="checkbox" class="lyrico-select-lyric-line" value="Angum ingum ennai"/>
</div>
<div class="lyrico-lyrics-wrapper">Virattum paravaiyae
<input type="checkbox" class="lyrico-select-lyric-line" value="Virattum paravaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna solla undhan
<input type="checkbox" class="lyrico-select-lyric-line" value="Enna solla undhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Mirattum azhagaiyae
<input type="checkbox" class="lyrico-select-lyric-line" value="Mirattum azhagaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetta veli naduvae
<input type="checkbox" class="lyrico-select-lyric-line" value="Vetta veli naduvae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada kotta kotta
<input type="checkbox" class="lyrico-select-lyric-line" value="Ada kotta kotta"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhithae thudikkiren…oh..
<input type="checkbox" class="lyrico-select-lyric-line" value="Vizhithae thudikkiren…oh.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Suthudhae suthudhae boomi
<input type="checkbox" class="lyrico-select-lyric-line" value="Suthudhae suthudhae boomi"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu podhumadaa podhumadaa saami (2 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Idhu podhumadaa podhumadaa saami"/></div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Idhayam urugithaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Idhayam urugithaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Karaindhu povadhai paarkiren
<input type="checkbox" class="lyrico-select-lyric-line" value="Karaindhu povadhai paarkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan paarkkiren
<input type="checkbox" class="lyrico-select-lyric-line" value="Naan paarkkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha nimidam thaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Indha nimidam thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Innum thodarumaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Innum thodarumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ketkiren unai ketkiren
<input type="checkbox" class="lyrico-select-lyric-line" value="Ketkiren unai ketkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Idhu enna indru vasandha kaalamaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Idhu enna indru vasandha kaalamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idaiveli innum kuraindhu pogumaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Idaiveli innum kuraindhu pogumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippadi orr iravu
<input type="checkbox" class="lyrico-select-lyric-line" value="Ippadi orr iravu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada ingu vandha ninaivum marakkumaa..hey..
<input type="checkbox" class="lyrico-select-lyric-line" value="Ada ingu vandha ninaivum marakkumaa..hey.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Suthudhae suthudhae boomi
<input type="checkbox" class="lyrico-select-lyric-line" value="Suthudhae suthudhae boomi"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu podhumadaa podhumadaa saami (2 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Idhu podhumadaa podhumadaa saami"/></div>
</div>
<div class="lyrico-lyrics-wrapper">Raa raa raa raadhae raadhae raadhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Raa raa raa raadhae raadhae raadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagiya raadhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Azhagiya raadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarvaiyil pesi pesi pesi
<input type="checkbox" class="lyrico-select-lyric-line" value="Paarvaiyil pesi pesi pesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pazhagiya raadhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Pazhagiya raadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Un azhagai vennil vizhunthu
<input type="checkbox" class="lyrico-select-lyric-line" value="Un azhagai vennil vizhunthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Etti etti nilavu paartha rasikkum
<input type="checkbox" class="lyrico-select-lyric-line" value="Etti etti nilavu paartha rasikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kolusil vanthu vasikka
<input type="checkbox" class="lyrico-select-lyric-line" value="Un kolusil vanthu vasikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kutti natchathirangal mannil kuthikum
<input type="checkbox" class="lyrico-select-lyric-line" value="Kutti natchathirangal mannil kuthikum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Hey hey hey hiya hiya hiya
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey hey hey hiya hiya hiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa naa naa naaa naaa naa
<input type="checkbox" class="lyrico-select-lyric-line" value="Naa naa naa naaa naaa naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naaa naaa naa naaa naa haa aaa aaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Naaa naaa naa naaa naa haa aaa aaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hiya hiya hiya yeah heyy…
<input type="checkbox" class="lyrico-select-lyric-line" value="Hiya hiya hiya yeah heyy…"/>
</div>
</div>
</pre>