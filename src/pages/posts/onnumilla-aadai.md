---
title: 'onnumilla song lyrics'
album: 'Aadai'
artist: 'Oorka'
lyricist: 'Bharath Sankar, Sivam'
director: 'Rathnakumar'
path: '/albums/aadai-song-lyrics'
song: 'Onnumilla'
image: ../../images/albumart/aadai.jpg
date: 2019-07-19
lang: tamil
singers: 
- Bharath Sankar
youtubeLink: "https://www.youtube.com/embed/yJetRY3jZ4E"
type: 'philosophy'
---

<pre class="lyrics-native">
    Coming Soon
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Poranthu eranthum
<input type="checkbox" class="lyrico-select-lyric-line" value="Poranthu eranthum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru unmai velangaadha
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru unmai velangaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirisa nenaikkum
<input type="checkbox" class="lyrico-select-lyric-line" value="Sirisa nenaikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sindhanaiyum maaraadha
<input type="checkbox" class="lyrico-select-lyric-line" value="Sindhanaiyum maaraadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kaana onna
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaana onna"/>
</div>
<div class="lyrico-lyrics-wrapper">Karuppunnu nenaikaadha
<input type="checkbox" class="lyrico-select-lyric-line" value="Karuppunnu nenaikaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Manushan ariva
<input type="checkbox" class="lyrico-select-lyric-line" value="Manushan ariva"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuzhi pottu thondaadha
<input type="checkbox" class="lyrico-select-lyric-line" value="Kuzhi pottu thondaadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Yen naa…
<input type="checkbox" class="lyrico-select-lyric-line" value="Yen naa…"/>
</div>
<div class="lyrico-lyrics-wrapper">Onnum illa onnum illa
<input type="checkbox" class="lyrico-select-lyric-line" value="Onnum illa onnum illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kaiyila onnumilla
<input type="checkbox" class="lyrico-select-lyric-line" value="Un kaiyila onnumilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Onnum illa onnum illa
<input type="checkbox" class="lyrico-select-lyric-line" value="Onnum illa onnum illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kaiyila onnumilla
<input type="checkbox" class="lyrico-select-lyric-line" value="Un kaiyila onnumilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Aattruvan aattivacha
<input type="checkbox" class="lyrico-select-lyric-line" value="Aattruvan aattivacha"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaduravan aadanum
<input type="checkbox" class="lyrico-select-lyric-line" value="Aaduravan aadanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Hey…hey…hey…hey
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey…hey…hey…hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kaalam neram poi-u
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaalam neram poi-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kanava mattum vai-u
<input type="checkbox" class="lyrico-select-lyric-line" value="Un kanava mattum vai-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Bayamo pirivo vendaam
<input type="checkbox" class="lyrico-select-lyric-line" value="Bayamo pirivo vendaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Un paasam parivae podhum
<input type="checkbox" class="lyrico-select-lyric-line" value="Un paasam parivae podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna manushanaaga maathidum
<input type="checkbox" class="lyrico-select-lyric-line" value="Unna manushanaaga maathidum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kalavu poga
<input type="checkbox" class="lyrico-select-lyric-line" value="Kalavu poga"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilaiyum poi thaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Nilaiyum poi thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavu kaana
<input type="checkbox" class="lyrico-select-lyric-line" value="Kanavu kaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Muzhichen seri thaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Muzhichen seri thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Uyirum maraiyum
<input type="checkbox" class="lyrico-select-lyric-line" value="Uyirum maraiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavalai poi thaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Kavalai poi thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathira karaikkum
<input type="checkbox" class="lyrico-select-lyric-line" value="Kathira karaikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilavum meithaan…hey
<input type="checkbox" class="lyrico-select-lyric-line" value="Nilavum meithaan…hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Nelama seriya
<input type="checkbox" class="lyrico-select-lyric-line" value="Nelama seriya"/>
</div>
<div class="lyrico-lyrics-wrapper">Irukkum varaikkum
<input type="checkbox" class="lyrico-select-lyric-line" value="Irukkum varaikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilavu meiyaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Nilavu meiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Velicham thandhaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Velicham thandhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhenamum kaalam
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhenamum kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Maarum nilaiyil
<input type="checkbox" class="lyrico-select-lyric-line" value="Maarum nilaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilavu poiyaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Nilavu poiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Manushan ketta
<input type="checkbox" class="lyrico-select-lyric-line" value="Manushan ketta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thappu illai thappu illai
<input type="checkbox" class="lyrico-select-lyric-line" value="Thappu illai thappu illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Inga onnum thappu illai
<input type="checkbox" class="lyrico-select-lyric-line" value="Inga onnum thappu illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappu illai thappu illai
<input type="checkbox" class="lyrico-select-lyric-line" value="Thappu illai thappu illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Inga onnum thappu illai
<input type="checkbox" class="lyrico-select-lyric-line" value="Inga onnum thappu illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aattruvan aattivacha
<input type="checkbox" class="lyrico-select-lyric-line" value="Aattruvan aattivacha"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaduravan aadanum
<input type="checkbox" class="lyrico-select-lyric-line" value="Aaduravan aadanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Hey…hey…hey…hey
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey…hey…hey…hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kaalam neram poi-u
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaalam neram poi-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kanava mattum vai-u
<input type="checkbox" class="lyrico-select-lyric-line" value="Un kanava mattum vai-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Bayamo pirivo vendaam
<input type="checkbox" class="lyrico-select-lyric-line" value="Bayamo pirivo vendaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Un paasam parivae podhum
<input type="checkbox" class="lyrico-select-lyric-line" value="Un paasam parivae podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna manushanaaga maathidum
<input type="checkbox" class="lyrico-select-lyric-line" value="Unna manushanaaga maathidum"/>
</div>
</pre>