---
title: "ariyatha vayasu song lyrics"
album: "Paruthiveeran"
artist: "Yuvan Shankar Raja"
lyricist: "Snehan"
director: "Ameer"
path: "/albums/paruthiveeran-lyrics"
song: "Ariyatha Vayasu"
image: ../../images/albumart/paruthiveeran.jpg
date: 2007-02-23
lang: tamil
youtubeLink: "https://www.youtube.com/embed/x8k5n3vuvpQ"
type: "Affection"
singers:
  - Ilaiyaraaja
  - Derrick
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aariyatha Vayasu Puriyatha Manasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aariyatha Vayasu Puriyatha Manasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendum Inge Kaathal Seiyum Neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendum Inge Kaathal Seiyum Neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Aathi Rendum Parakkuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Aathi Rendum Parakkuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Sedi Pola Aasa Molaikkuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sedi Pola Aasa Molaikkuthey"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendum Inge Kaathal Seiyum Neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendum Inge Kaathal Seiyum Neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Vettaveli Pottalile Mazhavantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vettaveli Pottalile Mazhavantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini Kottangkoochi Kodaiyaga Maaridum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Kottangkoochi Kodaiyaga Maaridum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thattampoochi Vandiyilae Seervantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thattampoochi Vandiyilae Seervantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Inge Pattampoochi Vandiyile Oorvarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inge Pattampoochi Vandiyile Oorvarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Ho.. Aariyatha Vayasu Puriyatha Manasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Ho.. Aariyatha Vayasu Puriyatha Manasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendum Inge Kaathal Seiyum Neram..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendum Inge Kaathal Seiyum Neram.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pallikkudathila Paadam Nadathala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pallikkudathila Paadam Nadathala"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarum Manakattu Padikkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarum Manakattu Padikkala"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha Kezhaviyum Sonna Kathaiyula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Kezhaviyum Sonna Kathaiyula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
 <div class="lyrico-lyrics-wrapper">Kattula Metula Kathula Kalanthathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattula Metula Kathula Kalanthathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oravukku Ithuthan Thalama,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oravukku Ithuthan Thalama,"/>
</div>
<div class="lyrico-lyrics-wrapper">Itha Usuraa Nenaikkum Elama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Itha Usuraa Nenaikkum Elama"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathale Kadavulin Aana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathale Kadavulin Aana"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan Bhoomikku Thottuvecha Sena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan Bhoomikku Thottuvecha Sena"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodamaathi Nadamaathi Adi Aathi Intha Vasula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodamaathi Nadamaathi Adi Aathi Intha Vasula"/>
</div>
<div class="lyrico-lyrics-wrapper">Aariyatha Vayasu Puriyatha Manasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aariyatha Vayasu Puriyatha Manasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendum Inge Kaathal Seiyum Neram..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendum Inge Kaathal Seiyum Neram.."/>
</div>
<div class="lyrico-lyrics-wrapper">Karantha Palaye Kambil Puguthida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karantha Palaye Kambil Puguthida"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanakku Poduthey Rendumthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanakku Poduthey Rendumthan"/>
</div>
<div class="lyrico-lyrics-wrapper">Køra Pullila Metti Šenjuthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Køra Pullila Metti Šenjuthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalula Mattuthu Thølula Šaayuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalula Mattuthu Thølula Šaayuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oøraiyum Oravaiyum Maranthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oøraiyum Oravaiyum Maranthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadukkattula Nadakkuthe Virunthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadukkattula Nadakkuthe Virunthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Natha Køøtila Pugunthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natha Køøtila Pugunthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini Kudithanam Nadathuma Šenthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Kudithanam Nadathuma Šenthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Aathi.. Adi Aathi..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Aathi.. Adi Aathi.."/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Aathi Intha Vayasula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Aathi Intha Vayasula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aariyatha Vayasu Puriyatha Manasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aariyatha Vayasu Puriyatha Manasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendum Inge Kaathal Šeiyum Neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendum Inge Kaathal Šeiyum Neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Aathi Rendum Parakkuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Aathi Rendum Parakkuthe"/>
</div>
Š<div class="lyrico-lyrics-wrapper">edi Pøla Aasa Mølaikkuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edi Pøla Aasa Mølaikkuthey"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendum Inge Kaathal Šeiyum Neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendum Inge Kaathal Šeiyum Neram"/>
</div>
</pre>
