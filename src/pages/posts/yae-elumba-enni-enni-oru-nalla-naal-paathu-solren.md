---
title: "yae elumba enni enni song lyrics"
album: "Oru Nalla Naal Paathu Solren"
artist: "Justin Prabhakaran"
lyricist: "Karthik Netha"
director: "Arumuga Kumar"
path: "/albums/oru-nalla-naal-paathu-solren-lyrics"
song: "Yae Elumba Enni Enni"
image: ../../images/albumart/oru-nalla-naal-paathu-solren.jpg
date: 2018-02-02
lang: tamil
youtubeLink: "https://www.youtube.com/embed/xPeX-5kEVg8"
type: "happy"
singers:
  - A.C.S. Ravichandran
  - Sharanya Gopinath
  - Dr. Narayanan
  - MC Vickey
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yae elumba enni enni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yae elumba enni enni"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaayam aadum manna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaayam aadum manna"/>
</div>
<div class="lyrico-lyrics-wrapper">Yae padaiyal kanji ootha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yae padaiyal kanji ootha"/>
</div>
<div class="lyrico-lyrics-wrapper">Manda otta kondaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manda otta kondaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye elumba enni enni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye elumba enni enni"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaayam aadum manna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaayam aadum manna"/>
</div>
<div class="lyrico-lyrics-wrapper">Yae padaiyal kanji ootha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yae padaiyal kanji ootha"/>
</div>
<div class="lyrico-lyrics-wrapper">Manda otta kondaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manda otta kondaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yae morattu meniyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yae morattu meniyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiruttu gnyaniyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiruttu gnyaniyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Arakka yazhiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arakka yazhiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Karuppu jothiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuppu jothiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yae attu kutti muthal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yae attu kutti muthal"/>
</div>
<div class="lyrico-lyrics-wrapper">Aeroplane vara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aeroplane vara"/>
</div>
<div class="lyrico-lyrics-wrapper">Attaya poduvom da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Attaya poduvom da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neenga sataiya kalathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neenga sataiya kalathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Maattum nerathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maattum nerathula"/>
</div>
<div class="lyrico-lyrics-wrapper">Veetaiyae thookkuvom da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veetaiyae thookkuvom da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yae inky pinky ponky
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yae inky pinky ponky"/>
</div>
<div class="lyrico-lyrics-wrapper">Pottu mudichuttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pottu mudichuttu"/>
</div>
<div class="lyrico-lyrics-wrapper">Bank ah thiruduvum da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bank ah thiruduvum da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vijay mallaiya panatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vijay mallaiya panatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kollaya adichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kollaya adichi"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorukku poduvom da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorukku poduvom da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hahaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hahaha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yae elumba enni enni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yae elumba enni enni"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaayam aadum manna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaayam aadum manna"/>
</div>
<div class="lyrico-lyrics-wrapper">Yae padaiyal kanji ootha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yae padaiyal kanji ootha"/>
</div>
<div class="lyrico-lyrics-wrapper">Manda otta kondaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manda otta kondaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Offer pala kuduthu pudungum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Offer pala kuduthu pudungum"/>
</div>
<div class="lyrico-lyrics-wrapper">Corporate thirudanga naanga illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Corporate thirudanga naanga illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pallikoodam collegenu pervachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pallikoodam collegenu pervachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kollaiadikuravanga naanga illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kollaiadikuravanga naanga illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vella vetti vella sattu pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vella vetti vella sattu pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Votu thirudura koottam illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Votu thirudura koottam illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sattathila ottaiya pottu sotha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sattathila ottaiya pottu sotha"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudungura koottam illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudungura koottam illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanga ketta nallavanunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga ketta nallavanunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kettu ponna nalla kettuvanunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kettu ponna nalla kettuvanunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Mothathila suthumana kolgaiulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mothathila suthumana kolgaiulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Nermaiyana thirudangada naanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nermaiyana thirudangada naanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hulk da naanga hulk da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hulk da naanga hulk da"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga manasu ennaikkum milk da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga manasu ennaikkum milk da"/>
</div>
<div class="lyrico-lyrics-wrapper">Party da enga party da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Party da enga party da"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu thirudar munnetra party da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu thirudar munnetra party da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nithiya thirudan pathiya thirudan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nithiya thirudan pathiya thirudan"/>
</div>
<div class="lyrico-lyrics-wrapper">Sathiya thirudan mukhtiya thirudan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sathiya thirudan mukhtiya thirudan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattali thirudan koottali thirudan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattali thirudan koottali thirudan"/>
</div>
<div class="lyrico-lyrics-wrapper">Innaikku thirudan ennaikum thirudan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innaikku thirudan ennaikum thirudan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thingal sevva puthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thingal sevva puthan"/>
</div>
<div class="lyrico-lyrics-wrapper">Viyazhan velli sani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viyazhan velli sani"/>
</div>
<div class="lyrico-lyrics-wrapper">Sunday kollai adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sunday kollai adi"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma kolgai padi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma kolgai padi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vanthidu vanthidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthidu vanthidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pangidu pangidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pangidu pangidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kumbittu kumbittu kondadidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kumbittu kumbittu kondadidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye vatta nilavula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye vatta nilavula"/>
</div>
<div class="lyrico-lyrics-wrapper">Paatti sutta vada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paatti sutta vada"/>
</div>
<div class="lyrico-lyrics-wrapper">Thookunathu nari than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thookunathu nari than"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha kullanariyavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha kullanariyavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kollai adichathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kollai adichathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Engaloda thala than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engaloda thala than"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yae elumba enni enni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yae elumba enni enni"/>
</div>
<div class="lyrico-lyrics-wrapper">Yae padaiya kanji ootha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yae padaiya kanji ootha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yae kaatha thirudavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yae kaatha thirudavum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaatha thirudavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaatha thirudavum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aalu undu gavani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalu undu gavani"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada ponna kariyatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada ponna kariyatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Senju mudipomae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senju mudipomae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nangayellam rajini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nangayellam rajini"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ha ha ha ha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ha ha ha ha"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoi hoi hoi hoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoi hoi hoi hoi"/>
</div>
</pre>
