---
title: "kaadhoram song lyrics"
album: "Kee"
artist: "Vishal Chandrasekhar"
lyricist: "Thamarai"
director: "Kalees"
path: "/albums/kee-lyrics"
song: "Kaadhoram"
image: ../../images/albumart/kee.jpg
date: 2019-05-10
lang: tamil
youtubeLink: "https://www.youtube.com/embed/AxwrzIgBHg0"
type: "love"
singers:
  - Vijay Prakash
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kadhooram moochu katru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhooram moochu katru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ondru reengaram Seiyudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondru reengaram Seiyudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannooram soppanangal sindhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannooram soppanangal sindhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Poongaatril serudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poongaatril serudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alaadhiyaana thotram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alaadhiyaana thotram"/>
</div>
<div class="lyrico-lyrics-wrapper">Am mookinmaelae seetram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Am mookinmaelae seetram"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinodhamaga yennai maatrudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinodhamaga yennai maatrudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aagadhae kannai kollai kondu pogadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagadhae kannai kollai kondu pogadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Munbilae Thaangaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munbilae Thaangaadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai parka venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai parka venum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vendaamaa kangalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vendaamaa kangalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meeraadha yellai meeri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meeraadha yellai meeri"/>
</div>
<div class="lyrico-lyrics-wrapper">Kooraana sollai koori
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooraana sollai koori"/>
</div>
<div class="lyrico-lyrics-wrapper">Neraaga unnai sera paarkkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neraaga unnai sera paarkkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Puyal mazhaiyinil siru ilaiyena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puyal mazhaiyinil siru ilaiyena"/>
</div>
<div class="lyrico-lyrics-wrapper">Padhari nagarum ila manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padhari nagarum ila manam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sari thavarena irupuram manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sari thavarena irupuram manam"/>
</div>
<div class="lyrico-lyrics-wrapper">Savati yeduthu magizhndhidhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Savati yeduthu magizhndhidhum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru kudaiyinil iru nadaiyena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru kudaiyinil iru nadaiyena"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadakkavaikka mazhaivarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadakkavaikka mazhaivarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vali orupuram sugam orupuram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vali orupuram sugam orupuram"/>
</div>
<div class="lyrico-lyrics-wrapper">Idaiyinil agam midhipadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idaiyinil agam midhipadum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee thedavendum tholaindhu pogavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thedavendum tholaindhu pogavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kannin oram kanavaagavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kannin oram kanavaagavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee korum vannam pennaaga maaravaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee korum vannam pennaaga maaravaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Maatraththil ondraai madi seravaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maatraththil ondraai madi seravaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannadi mun nindru nindru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannadi mun nindru nindru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai koorndhu paarkindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai koorndhu paarkindren"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan illaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan illaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennachudaa endru ennai kelvi ketkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennachudaa endru ennai kelvi ketkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetkapattu verkkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetkapattu verkkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Puyal mazhaiyinil siru ilaiyena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puyal mazhaiyinil siru ilaiyena"/>
</div>
<div class="lyrico-lyrics-wrapper">Padhari nagarum ila manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padhari nagarum ila manam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sari thavarena irupuram manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sari thavarena irupuram manam"/>
</div>
<div class="lyrico-lyrics-wrapper">Savati yeduthu magizhndhidhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Savati yeduthu magizhndhidhum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru kudaiyinil iru nadaiyena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru kudaiyinil iru nadaiyena"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadakkavaikka mazhaivarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadakkavaikka mazhaivarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vali orupuram sugam orupuram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vali orupuram sugam orupuram"/>
</div>
<div class="lyrico-lyrics-wrapper">Idaiyinil agam midhipadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idaiyinil agam midhipadum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Venmegam ondru vidhaanam maarudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venmegam ondru vidhaanam maarudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanaththai vittu tharai serudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanaththai vittu tharai serudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Neerootru ondru nidhaanam aagudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neerootru ondru nidhaanam aagudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholmeedhu saaindhu anai podhuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholmeedhu saaindhu anai podhuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engae endhan vaanavillai kaanavillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engae endhan vaanavillai kaanavillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Endraengi vaan thedudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endraengi vaan thedudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee ingudhaan ulla sedhi kandu kondadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee ingudhaan ulla sedhi kandu kondadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vittu vittu sendradhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vittu vittu sendradhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Puyal mazhaiyinil siru ilaiyena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puyal mazhaiyinil siru ilaiyena"/>
</div>
<div class="lyrico-lyrics-wrapper">Padhari nagarum ila manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padhari nagarum ila manam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sari thavarena irupuram manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sari thavarena irupuram manam"/>
</div>
<div class="lyrico-lyrics-wrapper">Savati yeduthu magizhndhidhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Savati yeduthu magizhndhidhum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru kudaiyinil iru nadaiyena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru kudaiyinil iru nadaiyena"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadakkavaikka mazhaivarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadakkavaikka mazhaivarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vali orupuram sugam orupuram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vali orupuram sugam orupuram"/>
</div>
<div class="lyrico-lyrics-wrapper">Idaiyinil agam midhipadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idaiyinil agam midhipadum"/>
</div>
</pre>
