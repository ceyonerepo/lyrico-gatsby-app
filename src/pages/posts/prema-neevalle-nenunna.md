---
title: "prema song lyrics"
album: "Nevalle Nenunna"
artist: "Siddarth Sadasivuni"
lyricist: "Krishna kanth"
director: "Saibaba. M"
path: "/albums/neevalle-nenunna-lyrics"
song: "Prema"
image: ../../images/albumart/neevalle-nenunna.jpg
date: 2020-02-07
lang: telugu
youtubeLink: "https://www.youtube.com/embed/xxdoQwKIyDA"
type: "love"
singers:
  - Satya Prakash
  - MM Manasi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Pedhave Niliche Kadhalakane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pedhave Niliche Kadhalakane"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanule Mariche Klalasale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanule Mariche Klalasale"/>
</div>
<div class="lyrico-lyrics-wrapper">Manase Yegire Melakuvane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manase Yegire Melakuvane"/>
</div>
<div class="lyrico-lyrics-wrapper">Jarige Mari Edho Maaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jarige Mari Edho Maaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nilabadithe Thagilenule Merupulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilabadithe Thagilenule Merupulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thala Nimire Thadimenule Ningee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thala Nimire Thadimenule Ningee"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholakarilo Madhi Thodige Chigurulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholakarilo Madhi Thodige Chigurulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hrudhayamu Pai Virisenule Poole
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hrudhayamu Pai Virisenule Poole"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Preema Tholi Preema
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Preema Tholi Preema"/>
</div>
<div class="lyrico-lyrics-wrapper">Brathukantha Vente Unde Preema
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Brathukantha Vente Unde Preema"/>
</div>
<div class="lyrico-lyrics-wrapper">Preema Tholi Preema
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Preema Tholi Preema"/>
</div>
<div class="lyrico-lyrics-wrapper">Preema Saripoodhe Unna Inko Janma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Preema Saripoodhe Unna Inko Janma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naalane Nene Unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalane Nene Unna"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalone Lena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalone Lena"/>
</div>
<div class="lyrico-lyrics-wrapper">Mari Nee Kalalo Unnanemo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mari Nee Kalalo Unnanemo"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Idhe Nenu Kaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Idhe Nenu Kaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Niluvana Nijamula Ney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niluvana Nijamula Ney"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilavaguna Neelone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilavaguna Neelone"/>
</div>
<div class="lyrico-lyrics-wrapper">Selavane Kalalane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Selavane Kalalane"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalavagaa Raana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalavagaa Raana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pehdve Niliche Kadhalakane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pehdve Niliche Kadhalakane"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanule Mariche Kalalasale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanule Mariche Kalalasale"/>
</div>
<div class="lyrico-lyrics-wrapper">Manase Yegire Melakuvane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manase Yegire Melakuvane"/>
</div>
<div class="lyrico-lyrics-wrapper">Jarige Mari Edho Maaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jarige Mari Edho Maaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nilabadithe Thagilenule Merupulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilabadithe Thagilenule Merupulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thala Nimire Thadimenule Ningee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thala Nimire Thadimenule Ningee"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholakarilo Madhi Thodige Chigurulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholakarilo Madhi Thodige Chigurulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hrudhayamu Pai Virisenule Poole
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hrudhayamu Pai Virisenule Poole"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Preema Tholi Preema
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Preema Tholi Preema"/>
</div>
<div class="lyrico-lyrics-wrapper">Preema Brathukantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Preema Brathukantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vente Unde Prema
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vente Unde Prema"/>
</div>
<div class="lyrico-lyrics-wrapper">Preema Tholi Preema
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Preema Tholi Preema"/>
</div>
<div class="lyrico-lyrics-wrapper">Saripodhe Unna Inko Janma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saripodhe Unna Inko Janma"/>
</div>
</pre>
