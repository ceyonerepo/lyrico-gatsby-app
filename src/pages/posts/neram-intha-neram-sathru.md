---
title: "neram indha neram song lyrics"
album: "Sathru"
artist: "Amresh Ganesh - Surya Prasadh R"
lyricist: "Kabilan - Karky - Sorko"
director: "Naveen Nanjundan"
path: "/albums/sathru-lyrics"
song: "Neram Indha Neram"
image: ../../images/albumart/sathru.jpg
date: 2019-03-08
lang: tamil
youtubeLink: "https://www.youtube.com/embed/eFvFGhs60fk"
type: "love"
singers:
  - Tippu
  - Suchithra
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ada Neram Indha Neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Neram Indha Neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Haiyo Ennenamo Nerum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haiyo Ennenamo Nerum"/>
</div>
<div class="lyrico-lyrics-wrapper">En Narambukullae Kaigal Thattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Narambukullae Kaigal Thattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal Reengaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Reengaaram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho Oo Hoh Hoh Hoh Hoh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho Oo Hoh Hoh Hoh Hoh"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho Oo Hoh Hoh Hoh Hoh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho Oo Hoh Hoh Hoh Hoh"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho Oo Hoh Hoh Hoh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho Oo Hoh Hoh Hoh"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoh Hoh Hoh Hoo Ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoh Hoh Hoh Hoo Ooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Maarum Yaavum Maarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Maarum Yaavum Maarum"/>
</div>
<div class="lyrico-lyrics-wrapper">En Mounam Pogum Dhooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Mounam Pogum Dhooram"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kanavukullae Undhan Osai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kanavukullae Undhan Osai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal Kadigaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Kadigaaram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho Oo Hoh Hoh Hoh Hoh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho Oo Hoh Hoh Hoh Hoh"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho Oo Hoh Hoh Hoh Hoh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho Oo Hoh Hoh Hoh Hoh"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho Oo Hoh Hoh Hoh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho Oo Hoh Hoh Hoh"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoh Hoh Hoh Hoo Ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoh Hoh Hoh Hoo Ooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mazhai Illa Megam Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai Illa Megam Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Irundhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Irundhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadhi Pola Ennai Neeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadhi Pola Ennai Neeyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Maatrinaayaeae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maatrinaayaeae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidai Illaa Kelvi Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidai Illaa Kelvi Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Irundhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Irundhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilaiyadum Kuzhandhai Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilaiyadum Kuzhandhai Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Maatrinaayaeae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maatrinaayaeae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pazhagadha Idam Ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pazhagadha Idam Ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Paranthu Odi Rasippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paranthu Odi Rasippom"/>
</div>
<div class="lyrico-lyrics-wrapper">Anal Kaatru Varum Bothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anal Kaatru Varum Bothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Alaiyodu Vasippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alaiyodu Vasippom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi Unnalae En Vaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Unnalae En Vaanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhooram Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhooram Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pennae Nee Indri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pennae Nee Indri"/>
</div>
<div class="lyrico-lyrics-wrapper">En Vaazhvil Yaarum Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Vaazhvil Yaarum Illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho Oo Hoh Hoh Hoh Hoh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho Oo Hoh Hoh Hoh Hoh"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho Oo Hoh Hoh Hoh Hoh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho Oo Hoh Hoh Hoh Hoh"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho Oo Hoh Hoh Hoh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho Oo Hoh Hoh Hoh"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoh Hoh Hoh Hoo Ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoh Hoh Hoh Hoo Ooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho Oo Hoh Hoh Hoh Hoh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho Oo Hoh Hoh Hoh Hoh"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho Oo Hoh Hoh Hoh Hoh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho Oo Hoh Hoh Hoh Hoh"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho Oo Hoh Hoh Hoh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho Oo Hoh Hoh Hoh"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoh Hoh Hoh Hoo Ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoh Hoh Hoh Hoo Ooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hoo Ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo Ooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidhai Thedum Boomi Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidhai Thedum Boomi Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Irundhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Irundhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhiyodu Ennai Nee Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhiyodu Ennai Nee Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nattu Vaithaaiaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nattu Vaithaaiaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ilaipaara Koodu Thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilaipaara Koodu Thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Alaindhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Alaindhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhazhaalae Netri Meedhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhazhaalae Netri Meedhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pottu Veithaaiaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pottu Veithaaiaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iravodu Nilavaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravodu Nilavaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Anaiyaamal Iruppom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anaiyaamal Iruppom"/>
</div>
<div class="lyrico-lyrics-wrapper">Imaiyodu Imaikkorthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imaiyodu Imaikkorthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilaiyaaga Parappom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilaiyaaga Parappom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Kannalae En Kannil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kannalae En Kannil"/>
</div>
<div class="lyrico-lyrics-wrapper">Eeram Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eeram Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Innum Nee Kettaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum Nee Kettaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Solla Neram Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Solla Neram Illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho Oo Hoh Hoh Hoh Hoh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho Oo Hoh Hoh Hoh Hoh"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho Oo Hoh Hoh Hoh Hoh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho Oo Hoh Hoh Hoh Hoh"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho Oo Hoh Hoh Hoh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho Oo Hoh Hoh Hoh"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoh Hoh Hoh Hoo Ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoh Hoh Hoh Hoo Ooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Neram Indha Neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Neram Indha Neram"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kanna Kuzhi Oram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kanna Kuzhi Oram"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Muzhugi Vandhu Mutheduthaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Muzhugi Vandhu Mutheduthaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal Munnerum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Munnerum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Maarum Yaavum Maarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Maarum Yaavum Maarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Nerungi Vantha Neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Nerungi Vantha Neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Idhazhgal Thantha Unavu Thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Idhazhgal Thantha Unavu Thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal Aagaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Aagaaram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho Oo Hoh Hoh Hoh Hoh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho Oo Hoh Hoh Hoh Hoh"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho Oo Hoh Hoh Hoh Hoh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho Oo Hoh Hoh Hoh Hoh"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho Oo Hoh Hoh Hoh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho Oo Hoh Hoh Hoh"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoh Hoh Hoh Hoo Ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoh Hoh Hoh Hoo Ooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hooooohooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hooooohooooo"/>
</div>
</pre>
