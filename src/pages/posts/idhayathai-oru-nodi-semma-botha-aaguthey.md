---
title: "idhayathai oru nodi song lyrics"
album: "Semma Botha Aaguthey"
artist: "Yuvan Shankar Raja"
lyricist: "Niranjan Bharathi"
director: "Badri Venkatesh"
path: "/albums/semma-botha-aaguthey-lyrics"
song: "Idhayathai Oru Nodi"
image: ../../images/albumart/semma-botha-aaguthey.jpg
date: 2018-06-29
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Jza7VAMSZ7E"
type: "love"
singers:
  - Yuvan Shankar Raja
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Idhayathai Oru Nodi Niruthinaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayathai Oru Nodi Niruthinaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Atharkulle Unnaiye Poruthinaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atharkulle Unnaiye Poruthinaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhil Kadhal Koduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhil Kadhal Koduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mathini Neeyum Virainthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mathini Neeyum Virainthaai"/>
</div>
<div class="lyrico-lyrics-wrapper">En Dhegam Kulira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Dhegam Kulira"/>
</div>
<div class="lyrico-lyrics-wrapper">Manathile Paravasam Tharugiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manathile Paravasam Tharugiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhayathai Oru Nodi Niruthinaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayathai Oru Nodi Niruthinaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Atharkulle Unnaiye Poruthinaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atharkulle Unnaiye Poruthinaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Punnagaiyil Ennai Alli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Punnagaiyil Ennai Alli"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Nooru Murai Kolgindraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Nooru Murai Kolgindraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pothathu Ena Meendum Meendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pothathu Ena Meendum Meendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Seikindraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seikindraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Iragaai Ennai Thodugindraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iragaai Ennai Thodugindraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagaai Imsai Seigindraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaai Imsai Seigindraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Sugamaai Nenjil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sugamaai Nenjil "/>
</div>
<div class="lyrico-lyrics-wrapper">Baarangal Tharugindraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baarangal Tharugindraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Vizhigal Ennum Kadigaarathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Vizhigal Ennum Kadigaarathil"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kadhalinai Parkindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kadhalinai Parkindren"/>
</div>
<div class="lyrico-lyrics-wrapper">Kooraana Un Imaigal Rendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooraana Un Imaigal Rendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Multhaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Multhaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Parkum Pothellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Parkum Pothellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam Ingu Odathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam Ingu Odathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Mutkal Ennai Kuthathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mutkal Ennai Kuthathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Peranbe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peranbe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhil Kadhal Koduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhil Kadhal Koduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mathini Neeyum Virainthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mathini Neeyum Virainthaai"/>
</div>
<div class="lyrico-lyrics-wrapper">En Dhegam Kulira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Dhegam Kulira"/>
</div>
<div class="lyrico-lyrics-wrapper">Manathile Paravasam Tharugiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manathile Paravasam Tharugiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhayathai Oru Nodi Niruthinaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayathai Oru Nodi Niruthinaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Atharkulle Unnaiye Poruthinaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atharkulle Unnaiye Poruthinaai"/>
</div>
</pre>
