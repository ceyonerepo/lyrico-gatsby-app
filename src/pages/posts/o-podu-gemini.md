---
title: "O Podu song lyrics"
album: "Gemini"
artist: "Bharathwaj"
lyricist: "Vairamuthu"
director: "Saran"
path: "/albums/gemini-lyrics"
song: "O Podu"
image: ../../images/albumart/gemini.jpg
date: 2002-04-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/1cSKMQpU9Mw"
type: "happy"
singers:
  - S. P. Balasubrahmanyam
  - Anuradha Sriram
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nenji Thudikkidhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenji Thudikkidhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gemini Gemini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gemini Gemini"/>
</div>
<div class="lyrico-lyrics-wrapper">Neththi Kodhikkidhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neththi Kodhikkidhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gemini Gemini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gemini Gemini"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil Kaadhil Vetchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil Kaadhil Vetchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Gavani Gavani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gavani Gavani"/>
</div>
<div class="lyrico-lyrics-wrapper">Gemini Gemini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gemini Gemini"/>
</div>
<div class="lyrico-lyrics-wrapper">Gavani Gavani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gavani Gavani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manda Vedikkidhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manda Vedikkidhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gemini Gemini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gemini Gemini"/>
</div>
<div class="lyrico-lyrics-wrapper">Maarbu Thudikkidhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maarbu Thudikkidhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gemini Gemini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gemini Gemini"/>
</div>
<div class="lyrico-lyrics-wrapper">Matcham Adikkidhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Matcham Adikkidhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gemini Gemini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gemini Gemini"/>
</div>
<div class="lyrico-lyrics-wrapper">Gemini Gemini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gemini Gemini"/>
</div>
<div class="lyrico-lyrics-wrapper">Gavani Gavani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gavani Gavani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gemini Gemini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gemini Gemini"/>
</div>
<div class="lyrico-lyrics-wrapper">Gemini Gemini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gemini Gemini"/>
</div>
<div class="lyrico-lyrics-wrapper">Gavani Gavani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gavani Gavani"/>
</div>
<div class="lyrico-lyrics-wrapper">Gavani Gavani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gavani Gavani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gemini Gemini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gemini Gemini"/>
</div>
<div class="lyrico-lyrics-wrapper">Gemini Gemini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gemini Gemini"/>
</div>
<div class="lyrico-lyrics-wrapper">Gavani Gavani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gavani Gavani"/>
</div>
<div class="lyrico-lyrics-wrapper">Gavani Gavani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gavani Gavani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhalikka Naan Irukken
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalikka Naan Irukken"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavalaiyellaam Vittuppudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavalaiyellaam Vittuppudu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pookkalakku Sulukkedukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pookkalakku Sulukkedukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vithai Yellaam Kaththu Kudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vithai Yellaam Kaththu Kudu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Podu Oh Podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Podu Oh Podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Podu Aah Oh Podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Podu Aah Oh Podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Podu Oh Podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Podu Oh Podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Podu Oh Podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Podu Oh Podu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenji Thudikkidhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenji Thudikkidhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gemini Gemini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gemini Gemini"/>
</div>
<div class="lyrico-lyrics-wrapper">Neththi Kodhikkidhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neththi Kodhikkidhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gemini Gemini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gemini Gemini"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil Kaadhil Vetchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil Kaadhil Vetchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Gavani Gavani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gavani Gavani"/>
</div>
<div class="lyrico-lyrics-wrapper">Gemini Gemini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gemini Gemini"/>
</div>
<div class="lyrico-lyrics-wrapper">Gavani Gavani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gavani Gavani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gemini Gemini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gemini Gemini"/>
</div>
<div class="lyrico-lyrics-wrapper">Gemini Gemini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gemini Gemini"/>
</div>
<div class="lyrico-lyrics-wrapper">Gavani Gavani Gavani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gavani Gavani Gavani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meen Thodaadha Poonaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meen Thodaadha Poonaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Then Thodaadha Theniyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Then Thodaadha Theniyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aan Thodaadha Penmai Ya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aan Thodaadha Penmai Ya"/>
</div>
<div class="lyrico-lyrics-wrapper">Alli Thinnaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alli Thinnaiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Latchum Pennil Ulladhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Latchum Pennil Ulladhu"/>
</div>
<div class="lyrico-lyrics-wrapper">En Moththam Thannil Ulladhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Moththam Thannil Ulladhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Muththam Moththam Ethanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muththam Moththam Ethanai"/>
</div>
<div class="lyrico-lyrics-wrapper">Enni Chollaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enni Chollaiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaaghamminnu Vandhu Puttaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaghamminnu Vandhu Puttaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanniyila Bedham Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanniyila Bedham Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mogaminnu Vandhu Puttaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mogaminnu Vandhu Puttaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mugavariye Thevai Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mugavariye Thevai Illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thottaachi Thottaachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottaachi Thottaachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thodaadhadhu Ellam Thottaachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodaadhadhu Ellam Thottaachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Hit Aachi Hit Aachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hit Aachi Hit Aachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Thottadhellaam Hit Aachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Thottadhellaam Hit Aachi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alli Madal Meniyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alli Madal Meniyile"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalla Yedam Kannduvidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla Yedam Kannduvidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yendha Yedam Azhagu Adhigam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yendha Yedam Azhagu Adhigam"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha Yedam Kollaiyidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Yedam Kollaiyidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Podu Oh Podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Podu Oh Podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Podu Oh Podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Podu Oh Podu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eh Nenji Thudikkidhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh Nenji Thudikkidhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gemini Gemini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gemini Gemini"/>
</div>
<div class="lyrico-lyrics-wrapper">Neththi Kodhikkidhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neththi Kodhikkidhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gemini Gemini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gemini Gemini"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil Kaadhil Vetchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil Kaadhil Vetchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Gavani Gavani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gavani Gavani"/>
</div>
<div class="lyrico-lyrics-wrapper">Gemini Gemini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gemini Gemini"/>
</div>
<div class="lyrico-lyrics-wrapper">Gavani Gavani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gavani Gavani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gemini Gemini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gemini Gemini"/>
</div>
<div class="lyrico-lyrics-wrapper">Gemini Gemini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gemini Gemini"/>
</div>
<div class="lyrico-lyrics-wrapper">Gavani Gavani Gavani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gavani Gavani Gavani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sellarikkum Dhegathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sellarikkum Dhegathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Pullarikkum Aasaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pullarikkum Aasaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullirukkum Varayila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullirukkum Varayila"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagam Unnadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagam Unnadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaaththadaitha Paiyadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaththadaitha Paiyadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattil Inbam Poiyadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattil Inbam Poiyadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasai Theerntha Ponavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai Theerntha Ponavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Andru Sonnadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andru Sonnadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pasi Yedukkum Kaalam Mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pasi Yedukkum Kaalam Mattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaithu Kulle Sikkal Ille
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaithu Kulle Sikkal Ille"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalavi Ulla Kaalam Mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalavi Ulla Kaalam Mattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Udambu Kulla Sikkal Ille
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udambu Kulla Sikkal Ille"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yennaachi Yennaachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yennaachi Yennaachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Izhuththa Izhuppu Yennaachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Izhuththa Izhuppu Yennaachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Onnaachi Onnaachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnaachi Onnaachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Odhadum Naanum Onnaachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odhadum Naanum Onnaachi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Utchan Thalai Kaayuthadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Utchan Thalai Kaayuthadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Itchu Mazhai Ittuvidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Itchu Mazhai Ittuvidu"/>
</div>
<div class="lyrico-lyrics-wrapper">En Azhagai Thottu Vidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Azhagai Thottu Vidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kodiyai Nattu Vidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kodiyai Nattu Vidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Podu Oh Podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Podu Oh Podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Podu Oh Podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Podu Oh Podu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenji Thudikkidhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenji Thudikkidhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gemini Gemini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gemini Gemini"/>
</div>
<div class="lyrico-lyrics-wrapper">Neththi Kodhikkidhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neththi Kodhikkidhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gemini Gemini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gemini Gemini"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil Kaadhil Vetchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil Kaadhil Vetchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Gavani Gavani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gavani Gavani"/>
</div>
<div class="lyrico-lyrics-wrapper">Gemini Gemini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gemini Gemini"/>
</div>
<div class="lyrico-lyrics-wrapper">Gavani Gavani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gavani Gavani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manda Vedikkidhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manda Vedikkidhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gemini Gemini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gemini Gemini"/>
</div>
<div class="lyrico-lyrics-wrapper">Maarbu Thudikkidhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maarbu Thudikkidhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gemini Gemini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gemini Gemini"/>
</div>
<div class="lyrico-lyrics-wrapper">Matcham Adikkidhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Matcham Adikkidhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gemini Gemini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gemini Gemini"/>
</div>
<div class="lyrico-lyrics-wrapper">Gemini Gemini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gemini Gemini"/>
</div>
<div class="lyrico-lyrics-wrapper">Gavani Gavani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gavani Gavani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gemini Gemini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gemini Gemini"/>
</div>
<div class="lyrico-lyrics-wrapper">Gemini Gemini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gemini Gemini"/>
</div>
<div class="lyrico-lyrics-wrapper">Gavani Gavani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gavani Gavani"/>
</div>
<div class="lyrico-lyrics-wrapper">Gavani Gavani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gavani Gavani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gemini Gemini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gemini Gemini"/>
</div>
<div class="lyrico-lyrics-wrapper">Gemini Gemini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gemini Gemini"/>
</div>
<div class="lyrico-lyrics-wrapper">Gavani Gavani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gavani Gavani"/>
</div>
<div class="lyrico-lyrics-wrapper">Gavani Gavani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gavani Gavani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhalikka Naan Irukken
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalikka Naan Irukken"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavalaiyellaam Vittuppudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavalaiyellaam Vittuppudu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pookkalakku Sulukkedukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pookkalakku Sulukkedukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vithai Yellaam Kaththu Kudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vithai Yellaam Kaththu Kudu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gemini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gemini"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Podu Oh Podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Podu Oh Podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Podu Aah oh Podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Podu Aah oh Podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Podu Oh Podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Podu Oh Podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Podu Oh Podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Podu Oh Podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Podai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Podai"/>
</div>
</pre>
