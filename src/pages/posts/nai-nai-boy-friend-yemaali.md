---
title: "nai nai boy friend song lyrics"
album: "Yemaali"
artist: "Sam D. Raj"
lyricist: "V.Z. Durai"
director: "V.Z. Durai"
path: "/albums/yemaali-lyrics"
song: "Nai Nai Boy Friend"
image: ../../images/albumart/yemaali.jpg
date: 2018-02-02
lang: tamil
youtubeLink: "https://www.youtube.com/embed/_8-GRgAY1pQ"
type: "happy"
singers:
  - Nincy
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nai Nai Boyfriend'U Alwa Kodupaan Jaakiradha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nai Nai Boyfriend'U Alwa Kodupaan Jaakiradha"/>
</div>
<div class="lyrico-lyrics-wrapper">Sei Sei Seiyata Sokka Kodupaan Parrota
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sei Sei Seiyata Sokka Kodupaan Parrota"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai Kai Vaikata Maanam Ketu Nikkata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai Kai Vaikata Maanam Ketu Nikkata"/>
</div>
<div class="lyrico-lyrics-wrapper">Tring Tring Edukata Torture Kodupaan Parupata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tring Tring Edukata Torture Kodupaan Parupata"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">How Can I Love You? How Can I Miss You?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="How Can I Love You? How Can I Miss You?"/>
</div>
<div class="lyrico-lyrics-wrapper">How Can I Trust You Now?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="How Can I Trust You Now?"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini En Kannil Kadhal Endrum Illaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini En Kannil Kadhal Endrum Illaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">I Wanna Kick You I Wanna Hurt You
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Wanna Kick You I Wanna Hurt You"/>
</div>
<div class="lyrico-lyrics-wrapper">I Wanna Kill You Now Sema Fun Fun Fun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Wanna Kill You Now Sema Fun Fun Fun"/>
</div>
<div class="lyrico-lyrics-wrapper">Break Up Oru Party Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Break Up Oru Party Dhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nai Nai Boyfriend'U Alwa Kodupaan Jaakiradha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nai Nai Boyfriend'U Alwa Kodupaan Jaakiradha"/>
</div>
<div class="lyrico-lyrics-wrapper">Sei Sei Seiyata Sokka Kodupaan Parrota
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sei Sei Seiyata Sokka Kodupaan Parrota"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai Kai Vaikata Maanam Ketu Nikkata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai Kai Vaikata Maanam Ketu Nikkata"/>
</div>
<div class="lyrico-lyrics-wrapper">Tring Tring Edukata Torture Kodupaan Parupata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tring Tring Edukata Torture Kodupaan Parupata"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Whatsapp'La Check Up Seivaan Second Line'La
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Whatsapp'La Check Up Seivaan Second Line'La"/>
</div>
<div class="lyrico-lyrics-wrapper">Tension Aavaan Ai Yai Yo Kelvi Ketu Kolvaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tension Aavaan Ai Yai Yo Kelvi Ketu Kolvaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Bathroom Poga Cellphone Pesa Cinema Poga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bathroom Poga Cellphone Pesa Cinema Poga"/>
</div>
<div class="lyrico-lyrics-wrapper">Chepal Vaanga Koi Yaala Ok Vaanga Vendume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chepal Vaanga Koi Yaala Ok Vaanga Vendume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paatha Doubt'U Pesuna Doubt'U Keta Doubt'U
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paatha Doubt'U Pesuna Doubt'U Keta Doubt'U"/>
</div>
<div class="lyrico-lyrics-wrapper">Siricha Doubt'U Moracha Doubt'U Vandha Doubt'U
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siricha Doubt'U Moracha Doubt'U Vandha Doubt'U"/>
</div>
<div class="lyrico-lyrics-wrapper">Pona Doubt'U Doubt'U Doubt'U Doubt'U Doubt'U
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pona Doubt'U Doubt'U Doubt'U Doubt'U Doubt'U"/>
</div>
<div class="lyrico-lyrics-wrapper">Doubt'U Doubt'U Doubt'U Doubt'U Doubt'U
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Doubt'U Doubt'U Doubt'U Doubt'U Doubt'U"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">How Can I Love You? How Can I Miss You?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="How Can I Love You? How Can I Miss You?"/>
</div>
<div class="lyrico-lyrics-wrapper">How Can I Trust You Now?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="How Can I Trust You Now?"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini En Kannil Kadhal Endrum Illaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini En Kannil Kadhal Endrum Illaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">I Wanna Kick You I Wanna Hurt You
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Wanna Kick You I Wanna Hurt You"/>
</div>
<div class="lyrico-lyrics-wrapper">I Wanna Kill You Now Sema Fun Fun Fun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Wanna Kill You Now Sema Fun Fun Fun"/>
</div>
<div class="lyrico-lyrics-wrapper">Break Up Oru Party Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Break Up Oru Party Dhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">No More Chat No More War It'S My Turn
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="No More Chat No More War It'S My Turn"/>
</div>
<div class="lyrico-lyrics-wrapper">Shut The Door
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shut The Door"/>
</div>
<div class="lyrico-lyrics-wrapper">Birthday'La Newyear Time'La Success Meet'La
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Birthday'La Newyear Time'La Success Meet'La"/>
</div>
<div class="lyrico-lyrics-wrapper">Lover'S Day'La Wish Panna Marandhuvitta Sethene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lover'S Day'La Wish Panna Marandhuvitta Sethene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Virumbi Vandha Kaamam Enbaan Vilagi Pona Waste'U Enbaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Virumbi Vandha Kaamam Enbaan Vilagi Pona Waste'U Enbaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh My God Boys'U Ellam Siko Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh My God Boys'U Ellam Siko Dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Saree Konjam Velagunaa Pochu Pidicha Paata Keta Gaandu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saree Konjam Velagunaa Pochu Pidicha Paata Keta Gaandu"/>
</div>
<div class="lyrico-lyrics-wrapper">Silly Kobam Always Koochal Partime Liar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silly Kobam Always Koochal Partime Liar"/>
</div>
<div class="lyrico-lyrics-wrapper">Fulltime Kutham Kutham Kutham Kutham Kutham Kutham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fulltime Kutham Kutham Kutham Kutham Kutham Kutham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nai Nai Boyfriend'U Alwa Kodupaan Jaakiradha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nai Nai Boyfriend'U Alwa Kodupaan Jaakiradha"/>
</div>
<div class="lyrico-lyrics-wrapper">Sei Sei Seiyata Sokka Kodupaan Parrota
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sei Sei Seiyata Sokka Kodupaan Parrota"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai Kai Vaikata Maanam Ketu Nikkata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai Kai Vaikata Maanam Ketu Nikkata"/>
</div>
<div class="lyrico-lyrics-wrapper">Tring Tring Edukata Torture Kodupaan Parupata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tring Tring Edukata Torture Kodupaan Parupata"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">How Can I Love You? How Can I Miss You?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="How Can I Love You? How Can I Miss You?"/>
</div>
<div class="lyrico-lyrics-wrapper">How Can I Trust You Now?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="How Can I Trust You Now?"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini En Kannil Kadhal Endrum Illaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini En Kannil Kadhal Endrum Illaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">I Wanna Kick You I Wanna Hurt You
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Wanna Kick You I Wanna Hurt You"/>
</div>
<div class="lyrico-lyrics-wrapper">I Wanna Kill You Now Sema Fun Fun Fun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Wanna Kill You Now Sema Fun Fun Fun"/>
</div>
<div class="lyrico-lyrics-wrapper">Break Up Oru Party Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Break Up Oru Party Dhaan"/>
</div>
</pre>
