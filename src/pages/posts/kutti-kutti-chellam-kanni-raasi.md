---
title: "kutti kutti chellam song lyrics"
album: "Kanni Raasi"
artist: "Vishal Chandrasekhar"
lyricist: "Yugabharathi"
director: "Muthukumaran"
path: "/albums/kanni-raasi-lyrics"
song: "Kutti Kutti Chellam"
image: ../../images/albumart/kanni-raasi.jpg
date: 2020-12-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/jWVMnUn3RrM"
type: "love"
singers:
  - Velu
  - Dhivya pbs
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">kutti kutti chellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kutti kutti chellam"/>
</div>
<div class="lyrico-lyrics-wrapper">ennai kolathadi kolathadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennai kolathadi kolathadi"/>
</div>
<div class="lyrico-lyrics-wrapper">kattikitu naama onna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kattikitu naama onna"/>
</div>
<div class="lyrico-lyrics-wrapper">valvom vaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valvom vaadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">munnalagil muttaathadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munnalagil muttaathadi"/>
</div>
<div class="lyrico-lyrics-wrapper">pinnalagil vettathadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pinnalagil vettathadi"/>
</div>
<div class="lyrico-lyrics-wrapper">unnudaya kadhal munadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnudaya kadhal munadi"/>
</div>
<div class="lyrico-lyrics-wrapper">chella chella nanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chella chella nanum"/>
</div>
<div class="lyrico-lyrics-wrapper">vanthuduven pinnadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanthuduven pinnadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kutti kutti chellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kutti kutti chellam"/>
</div>
<div class="lyrico-lyrics-wrapper">ennai kolathadi kolathadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennai kolathadi kolathadi"/>
</div>
<div class="lyrico-lyrics-wrapper">kattikitu naama onna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kattikitu naama onna"/>
</div>
<div class="lyrico-lyrics-wrapper">valvom vaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valvom vaadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">yen oonjal aatura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yen oonjal aatura"/>
</div>
<div class="lyrico-lyrics-wrapper">unnudaiya pechula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnudaiya pechula"/>
</div>
<div class="lyrico-lyrics-wrapper">kannu muli pidunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannu muli pidunga"/>
</div>
<div class="lyrico-lyrics-wrapper">nannum karagam aaduren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nannum karagam aaduren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aalam paakura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aalam paakura"/>
</div>
<div class="lyrico-lyrics-wrapper">unnudaiya aasaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnudaiya aasaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">alaga valuki vilaporen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alaga valuki vilaporen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaanatha ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaanatha ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">ne kaatura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ne kaatura"/>
</div>
<div class="lyrico-lyrics-wrapper">un kaiyoda than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kaiyoda than"/>
</div>
<div class="lyrico-lyrics-wrapper">veti enna thalla ennatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veti enna thalla ennatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kutti kutti chellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kutti kutti chellam"/>
</div>
<div class="lyrico-lyrics-wrapper">ennai kolathadi kolathadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennai kolathadi kolathadi"/>
</div>
<div class="lyrico-lyrics-wrapper">kattikitu naama onna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kattikitu naama onna"/>
</div>
<div class="lyrico-lyrics-wrapper">valvom vaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valvom vaadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">munnalagil muttaathadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munnalagil muttaathadi"/>
</div>
<div class="lyrico-lyrics-wrapper">pinnalagil vettathadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pinnalagil vettathadi"/>
</div>
<div class="lyrico-lyrics-wrapper">unnudaya kadhal munadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnudaya kadhal munadi"/>
</div>
<div class="lyrico-lyrics-wrapper">chella chella nanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chella chella nanum"/>
</div>
<div class="lyrico-lyrics-wrapper">vanthuduven pinnadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanthuduven pinnadi"/>
</div>
</pre>
