---
title: "boss maama song lyrics"
album: "Thagite Thandana"
artist: "Shravan Bharadwaj"
lyricist: "Ramanjaneyulu - Gosala Rambabu"
director: "Srinath Badineni"
path: "/albums/thagite-thandana-lyrics"
song: "Boss Maama"
image: ../../images/albumart/thagite-thandana.jpg
date: 2020-01-22
lang: telugu
youtubeLink: "https://www.youtube.com/embed/KjtkRaxTSKQ"
type: "happy"
singers:
  - Rahul Sipligunj
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">salary la aasa choopi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="salary la aasa choopi"/>
</div>
<div class="lyrico-lyrics-wrapper">sampuku thintaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sampuku thintaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">bharyala kante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bharyala kante"/>
</div>
<div class="lyrico-lyrics-wrapper">ekkuva baadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ekkuva baadha"/>
</div>
<div class="lyrico-lyrics-wrapper">peduthu untaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="peduthu untaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">evadaina navvuthunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="evadaina navvuthunte"/>
</div>
<div class="lyrico-lyrics-wrapper">lolopala kullu kuntaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lolopala kullu kuntaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">evaru brother vaallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="evaru brother vaallu"/>
</div>
<div class="lyrico-lyrics-wrapper">boos lu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="boos lu"/>
</div>
<div class="lyrico-lyrics-wrapper">kaadhu kaadhu kaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadhu kaadhu kaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">badhmashulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="badhmashulu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">boss lu kaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="boss lu kaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">veellu badhmashulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veellu badhmashulu"/>
</div>
<div class="lyrico-lyrics-wrapper">chacchi malli puttinatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chacchi malli puttinatti"/>
</div>
<div class="lyrico-lyrics-wrapper">raakasulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="raakasulu"/>
</div>
<div class="lyrico-lyrics-wrapper">pagabatti pani cheppe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pagabatti pani cheppe"/>
</div>
<div class="lyrico-lyrics-wrapper">thella thraachulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thella thraachulu"/>
</div>
<div class="lyrico-lyrics-wrapper">vinukora chebuthaanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vinukora chebuthaanu"/>
</div>
<div class="lyrico-lyrics-wrapper">vaalla oosulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalla oosulu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">burrani kobbari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="burrani kobbari"/>
</div>
<div class="lyrico-lyrics-wrapper">bondam chesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bondam chesi"/>
</div>
<div class="lyrico-lyrics-wrapper">straw tho pelchesthaaruro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="straw tho pelchesthaaruro"/>
</div>
<div class="lyrico-lyrics-wrapper">life nu rabbaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="life nu rabbaru"/>
</div>
<div class="lyrico-lyrics-wrapper">banthiga maarchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="banthiga maarchi"/>
</div>
<div class="lyrico-lyrics-wrapper">football aadesthaaruro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="football aadesthaaruro"/>
</div>
<div class="lyrico-lyrics-wrapper">thaathe chachipoyinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaathe chachipoyinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">veellabba leavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veellabba leavey"/>
</div>
<div class="lyrico-lyrics-wrapper">ivvanantaaruraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ivvanantaaruraa"/>
</div>
<div class="lyrico-lyrics-wrapper">sentiments levuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sentiments levuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">vallavi pakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vallavi pakka"/>
</div>
<div class="lyrico-lyrics-wrapper">psycho mind uraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="psycho mind uraa"/>
</div>
<div class="lyrico-lyrics-wrapper">narakamlo sikshalennunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="narakamlo sikshalennunna"/>
</div>
<div class="lyrico-lyrics-wrapper">avi beatu chese torture
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avi beatu chese torture"/>
</div>
<div class="lyrico-lyrics-wrapper">boss uraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="boss uraa"/>
</div>
<div class="lyrico-lyrics-wrapper">boss maama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="boss maama"/>
</div>
<div class="lyrico-lyrics-wrapper">arey boss maama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arey boss maama"/>
</div>
<div class="lyrico-lyrics-wrapper">nuvvu sadist uraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvvu sadist uraa"/>
</div>
<div class="lyrico-lyrics-wrapper">boss maama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="boss maama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">boss maama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="boss maama"/>
</div>
<div class="lyrico-lyrics-wrapper">boss maama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="boss maama"/>
</div>
<div class="lyrico-lyrics-wrapper">neekanna laden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neekanna laden"/>
</div>
<div class="lyrico-lyrics-wrapper">best maama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="best maama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">boss lu kaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="boss lu kaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">veellu badhmashulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veellu badhmashulu"/>
</div>
<div class="lyrico-lyrics-wrapper">chacchi malli puttinatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chacchi malli puttinatti"/>
</div>
<div class="lyrico-lyrics-wrapper">raakasulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="raakasulu"/>
</div>
<div class="lyrico-lyrics-wrapper">boss lu kaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="boss lu kaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">veellu badhmashulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veellu badhmashulu"/>
</div>
<div class="lyrico-lyrics-wrapper">chacchi malli puttinatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chacchi malli puttinatti"/>
</div>
<div class="lyrico-lyrics-wrapper">raakasulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="raakasulu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">dollar drams ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dollar drams ni"/>
</div>
<div class="lyrico-lyrics-wrapper">kalla mundhu chupithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalla mundhu chupithe"/>
</div>
<div class="lyrico-lyrics-wrapper">ice aipokoy nayana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ice aipokoy nayana"/>
</div>
<div class="lyrico-lyrics-wrapper">files ye thechi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="files ye thechi"/>
</div>
<div class="lyrico-lyrics-wrapper">mundharesi vellithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mundharesi vellithe"/>
</div>
<div class="lyrico-lyrics-wrapper">piles ye unna thappunaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="piles ye unna thappunaa"/>
</div>
<div class="lyrico-lyrics-wrapper">full ye thaagina bar ulone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="full ye thaagina bar ulone"/>
</div>
<div class="lyrico-lyrics-wrapper">dhigi podha boss le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhigi podha boss le"/>
</div>
<div class="lyrico-lyrics-wrapper">gurthu kosthe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gurthu kosthe "/>
</div>
<div class="lyrico-lyrics-wrapper">kalle mossina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalle mossina"/>
</div>
<div class="lyrico-lyrics-wrapper">dream ulone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dream ulone"/>
</div>
<div class="lyrico-lyrics-wrapper">bhayapette face le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bhayapette face le"/>
</div>
<div class="lyrico-lyrics-wrapper">boss vele 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="boss vele "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">boss maama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="boss maama"/>
</div>
<div class="lyrico-lyrics-wrapper">boss maama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="boss maama"/>
</div>
<div class="lyrico-lyrics-wrapper">nuvvu fascistu raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvvu fascistu raa"/>
</div>
<div class="lyrico-lyrics-wrapper">boss maama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="boss maama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">boss maama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="boss maama"/>
</div>
<div class="lyrico-lyrics-wrapper">boss maama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="boss maama"/>
</div>
<div class="lyrico-lyrics-wrapper">neekanna laden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neekanna laden"/>
</div>
<div class="lyrico-lyrics-wrapper">best maama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="best maama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaalikesthe velikesthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaalikesthe velikesthu"/>
</div>
<div class="lyrico-lyrics-wrapper">velikesthe kaalikesthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="velikesthe kaalikesthu"/>
</div>
<div class="lyrico-lyrics-wrapper">asthamanam classu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="asthamanam classu"/>
</div>
<div class="lyrico-lyrics-wrapper">peeku tharuro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="peeku tharuro"/>
</div>
<div class="lyrico-lyrics-wrapper">aggi meedha guggilamlaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aggi meedha guggilamlaa"/>
</div>
<div class="lyrico-lyrics-wrapper">egasipaduthu rankelese
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="egasipaduthu rankelese"/>
</div>
<div class="lyrico-lyrics-wrapper">jaali leni aambothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jaali leni aambothu"/>
</div>
<div class="lyrico-lyrics-wrapper">caalluro 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="caalluro "/>
</div>
<div class="lyrico-lyrics-wrapper">point balnk ulo gun nupetti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="point balnk ulo gun nupetti"/>
</div>
<div class="lyrico-lyrics-wrapper">saradhaga navvuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saradhaga navvuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">kannu kotti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannu kotti"/>
</div>
<div class="lyrico-lyrics-wrapper">freedom annadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="freedom annadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">paathipetti 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paathipetti "/>
</div>
<div class="lyrico-lyrics-wrapper">pareshaane cheyyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pareshaane cheyyada"/>
</div>
<div class="lyrico-lyrics-wrapper">kaksha gatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaksha gatti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">bad maama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bad maama"/>
</div>
<div class="lyrico-lyrics-wrapper">time bad maama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="time bad maama"/>
</div>
<div class="lyrico-lyrics-wrapper">mana raathalinthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mana raathalinthe"/>
</div>
<div class="lyrico-lyrics-wrapper">very sad maama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="very sad maama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">boss maama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="boss maama"/>
</div>
<div class="lyrico-lyrics-wrapper">boss maama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="boss maama"/>
</div>
<div class="lyrico-lyrics-wrapper">arey neekanna laden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arey neekanna laden"/>
</div>
<div class="lyrico-lyrics-wrapper">best maama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="best maama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">boss lu kaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="boss lu kaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">veellu badhmashulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veellu badhmashulu"/>
</div>
<div class="lyrico-lyrics-wrapper">chacchi malli puttinatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chacchi malli puttinatti"/>
</div>
<div class="lyrico-lyrics-wrapper">raakasulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="raakasulu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">boss lu kaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="boss lu kaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">veellu badhmashulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veellu badhmashulu"/>
</div>
<div class="lyrico-lyrics-wrapper">chacchi malli puttinatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chacchi malli puttinatti"/>
</div>
<div class="lyrico-lyrics-wrapper">raakasulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="raakasulu"/>
</div>
</pre>
