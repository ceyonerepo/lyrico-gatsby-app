---
title: "walking miracle song lyrics"
album: "Punya Paap"
artist: "iLL Wayno"
lyricist: "DIVINE - Nas - Cocoa Sarai"
director: "Gil Green"
path: "/albums/punya-paap-lyrics"
song: "Walking Miracle"
image: ../../images/albumart/punya-paap.jpg
date: 2020-12-03
lang: hindi
youtubeLink: "https://www.youtube.com/embed/dzKPbU7JKyk"
type: "happy"
singers:
  - DIVINE
  - Nas
  - Cocoa Sarai
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Mass apeal india!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mass apeal india!"/>
</div>
<div class="lyrico-lyrics-wrapper">Sunn!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sunn!"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">I could take the sun and
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I could take the sun and"/>
</div>
<div class="lyrico-lyrics-wrapper">I could swallow it whole
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I could swallow it whole"/>
</div>
<div class="lyrico-lyrics-wrapper">Turn my body inside out of
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Turn my body inside out of"/>
</div>
<div class="lyrico-lyrics-wrapper">You’d be facing gold but
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You’d be facing gold but"/>
</div>
<div class="lyrico-lyrics-wrapper">You don't believe me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You don't believe me"/>
</div>
<div class="lyrico-lyrics-wrapper">When I say
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="When I say"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">I could take a cup of water
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I could take a cup of water"/>
</div>
<div class="lyrico-lyrics-wrapper">Turn it into a sea
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Turn it into a sea"/>
</div>
<div class="lyrico-lyrics-wrapper">I'm a fu*king walking miracle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I'm a fu*king walking miracle"/>
</div>
<div class="lyrico-lyrics-wrapper">That's why they believe it
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="That's why they believe it"/>
</div>
<div class="lyrico-lyrics-wrapper">So when you greet me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="So when you greet me"/>
</div>
<div class="lyrico-lyrics-wrapper">Gully gang
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gully gang"/>
</div>
<div class="lyrico-lyrics-wrapper">You've been addressed by a king
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You've been addressed by a king"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pehle se yeh likha tha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pehle se yeh likha tha"/>
</div>
<div class="lyrico-lyrics-wrapper">Hath mein mere mic jab se chipka tha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hath mein mere mic jab se chipka tha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nas mera bhai door ka woh rishtedar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nas mera bhai door ka woh rishtedar"/>
</div>
<div class="lyrico-lyrics-wrapper">Maa meri church mein sab mast hain haan bismillah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa meri church mein sab mast hain haan bismillah"/>
</div>
<div class="lyrico-lyrics-wrapper">Man rakhte saaf saaf kapde mein hoon narsimha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Man rakhte saaf saaf kapde mein hoon narsimha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aasmaan ki aur jab bhi bolun ghar chala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasmaan ki aur jab bhi bolun ghar chala"/>
</div>
<div class="lyrico-lyrics-wrapper">Apne paison se ghar chala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Apne paison se ghar chala"/>
</div>
<div class="lyrico-lyrics-wrapper">Balballa, ganda par dhanda yeh flow
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Balballa, ganda par dhanda yeh flow"/>
</div>
<div class="lyrico-lyrics-wrapper">Chal tu sar se naha, barse na barsega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chal tu sar se naha, barse na barsega"/>
</div>
<div class="lyrico-lyrics-wrapper">Namaste hum raste pe jachte na, hasne ka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namaste hum raste pe jachte na, hasne ka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Soche mazaak sapnon ko apnon ne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soche mazaak sapnon ko apnon ne"/>
</div>
<div class="lyrico-lyrics-wrapper">Hasi mazaak mein thode dil saare apnon ne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hasi mazaak mein thode dil saare apnon ne"/>
</div>
<div class="lyrico-lyrics-wrapper">Rishte kharab vishwas nahi kasmon mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rishte kharab vishwas nahi kasmon mein"/>
</div>
<div class="lyrico-lyrics-wrapper">Samandar paar khada abhi bhi main chappalon main
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samandar paar khada abhi bhi main chappalon main"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ujala surya ko bech daale waisa hustle hun main
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ujala surya ko bech daale waisa hustle hun main"/>
</div>
<div class="lyrico-lyrics-wrapper">Sleep is the cousin of death aur mein sapnon mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sleep is the cousin of death aur mein sapnon mein"/>
</div>
<div class="lyrico-lyrics-wrapper">Karta vaada jeena raja jab tak nahi dafan hun main
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karta vaada jeena raja jab tak nahi dafan hun main"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">I could take the sun and
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I could take the sun and"/>
</div>
<div class="lyrico-lyrics-wrapper">I could swallow it whole
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I could swallow it whole"/>
</div>
<div class="lyrico-lyrics-wrapper">Turn my body inside out of
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Turn my body inside out of"/>
</div>
<div class="lyrico-lyrics-wrapper">You'd be facing gold but
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You'd be facing gold but"/>
</div>
<div class="lyrico-lyrics-wrapper">You don't believe me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You don't believe me"/>
</div>
<div class="lyrico-lyrics-wrapper">When I say
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="When I say"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">I could take a cup of water
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I could take a cup of water"/>
</div>
<div class="lyrico-lyrics-wrapper">Turn it into a sea
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Turn it into a sea"/>
</div>
<div class="lyrico-lyrics-wrapper">I'm a fu*king walking miracle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I'm a fu*king walking miracle"/>
</div>
<div class="lyrico-lyrics-wrapper">That's why they believe it
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="That's why they believe it"/>
</div>
<div class="lyrico-lyrics-wrapper">So when you greet me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="So when you greet me"/>
</div>
<div class="lyrico-lyrics-wrapper">You've been addressed by a king
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You've been addressed by a king"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Take care of your temple
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Take care of your temple"/>
</div>
<div class="lyrico-lyrics-wrapper">We only here temporarily
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="We only here temporarily"/>
</div>
<div class="lyrico-lyrics-wrapper">Heavy on the king treatment
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heavy on the king treatment"/>
</div>
<div class="lyrico-lyrics-wrapper">Constant steadily
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Constant steadily"/>
</div>
<div class="lyrico-lyrics-wrapper">Feeling like the 80’s, 20-20 residuals
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Feeling like the 80’s, 20-20 residuals"/>
</div>
<div class="lyrico-lyrics-wrapper">Brother we chosen, it was written
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Brother we chosen, it was written"/>
</div>
<div class="lyrico-lyrics-wrapper">Whoever think that it's not
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Whoever think that it's not"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Don't believe it's supposed to be
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Don't believe it's supposed to be"/>
</div>
<div class="lyrico-lyrics-wrapper">They like imagination, so ultimately
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="They like imagination, so ultimately"/>
</div>
<div class="lyrico-lyrics-wrapper">They fall to their knees but this is prophecy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="They fall to their knees but this is prophecy"/>
</div>
<div class="lyrico-lyrics-wrapper">The real do real things that is the policy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="The real do real things that is the policy"/>
</div>
<div class="lyrico-lyrics-wrapper">Nas divine ny to mumbai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nas divine ny to mumbai"/>
</div>
<div class="lyrico-lyrics-wrapper">Say a prayer through your ears
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Say a prayer through your ears"/>
</div>
<div class="lyrico-lyrics-wrapper">Open your eyes caught y'all slipping
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Open your eyes caught y'all slipping"/>
</div>
<div class="lyrico-lyrics-wrapper">He put together a pity party army
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="He put together a pity party army"/>
</div>
<div class="lyrico-lyrics-wrapper">He acting like the whole world against him
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="He acting like the whole world against him"/>
</div>
<div class="lyrico-lyrics-wrapper">I’m a victim, I write a letter to life
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I’m a victim, I write a letter to life"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dear life please let these little kids be right
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dear life please let these little kids be right"/>
</div>
<div class="lyrico-lyrics-wrapper">And to the vatican I'm asking them
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="And to the vatican I'm asking them"/>
</div>
<div class="lyrico-lyrics-wrapper">Play the song real loud
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Play the song real loud"/>
</div>
<div class="lyrico-lyrics-wrapper">Until the sky start cracking in
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Until the sky start cracking in"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Matlab shah jahan ka, talwar tipu sultan ka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Matlab shah jahan ka, talwar tipu sultan ka"/>
</div>
<div class="lyrico-lyrics-wrapper">Yuvraj bayein hath se, maar nahi khana vaar ka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yuvraj bayein hath se, maar nahi khana vaar ka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudrat hai mera dost, aur main mausam ko bigadta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudrat hai mera dost, aur main mausam ko bigadta"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhuan basa hai fefdon mein, saansein mere pahaad ka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhuan basa hai fefdon mein, saansein mere pahaad ka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hum ladte tu laadka teri phat'ti isliye phadta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hum ladte tu laadka teri phat'ti isliye phadta"/>
</div>
<div class="lyrico-lyrics-wrapper">Mehfil mein jo bhi baithe sabko sar mein bhukar sa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mehfil mein jo bhi baithe sabko sar mein bhukar sa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalam se main kalaam sa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalam se main kalaam sa"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu manzil tak na pohnche tu bhagta inzamam sa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu manzil tak na pohnche tu bhagta inzamam sa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bachon pe willy wonka main
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bachon pe willy wonka main"/>
</div>
<div class="lyrico-lyrics-wrapper">Hum pahaad gira dete
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hum pahaad gira dete"/>
</div>
<div class="lyrico-lyrics-wrapper">Aur ghar hai tera kaanch ka be
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aur ghar hai tera kaanch ka be"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeh rap khel mere ungliyon pe nachta be
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh rap khel mere ungliyon pe nachta be"/>
</div>
<div class="lyrico-lyrics-wrapper">Main aur nas mil gaye yeh to vaastav hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main aur nas mil gaye yeh to vaastav hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Gully gang khuchal dale ya to raasta de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gully gang khuchal dale ya to raasta de"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Labour day jab bhi mic pe hum thookte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Labour day jab bhi mic pe hum thookte"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazdooron wale pair ab bas bolne pe nahi rukte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazdooron wale pair ab bas bolne pe nahi rukte"/>
</div>
<div class="lyrico-lyrics-wrapper">Vash mein jo lele phir woh hum se na chhuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vash mein jo lele phir woh hum se na chhuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaadugar hai jaadu kare flow mera tujhpe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaadugar hai jaadu kare flow mera tujhpe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">I could take the sun and
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I could take the sun and"/>
</div>
<div class="lyrico-lyrics-wrapper">I could swallow it whole
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I could swallow it whole"/>
</div>
<div class="lyrico-lyrics-wrapper">Turn my body inside out of
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Turn my body inside out of"/>
</div>
<div class="lyrico-lyrics-wrapper">You'd be facing gold but
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You'd be facing gold but"/>
</div>
<div class="lyrico-lyrics-wrapper">You don't believe me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You don't believe me"/>
</div>
<div class="lyrico-lyrics-wrapper">When I say
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="When I say"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">I could take a cup of water
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I could take a cup of water"/>
</div>
<div class="lyrico-lyrics-wrapper">Turn it into a sea
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Turn it into a sea"/>
</div>
<div class="lyrico-lyrics-wrapper">I'm a fu*king walking miracle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I'm a fu*king walking miracle"/>
</div>
<div class="lyrico-lyrics-wrapper">That's why they believe it
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="That's why they believe it"/>
</div>
<div class="lyrico-lyrics-wrapper">So when you greet me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="So when you greet me"/>
</div>
<div class="lyrico-lyrics-wrapper">You've been addressed by a king
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You've been addressed by a king"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Punya paap punya paap
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Punya paap punya paap"/>
</div>
<div class="lyrico-lyrics-wrapper">Meri doosri yeh kitaab
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meri doosri yeh kitaab"/>
</div>
<div class="lyrico-lyrics-wrapper">Pehle din phir hai raat
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pehle din phir hai raat"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Punya paap punya paap
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Punya paap punya paap"/>
</div>
<div class="lyrico-lyrics-wrapper">Punya paap punya paap
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Punya paap punya paap"/>
</div>
<div class="lyrico-lyrics-wrapper">Meri doosri yeh kitaab
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meri doosri yeh kitaab"/>
</div>
<div class="lyrico-lyrics-wrapper">Pehle din phir hai raat
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pehle din phir hai raat"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">King jab tak bante nahi lash
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="King jab tak bante nahi lash"/>
</div>
<div class="lyrico-lyrics-wrapper">Attention attention the whole world
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Attention attention the whole world"/>
</div>
<div class="lyrico-lyrics-wrapper">We here! All across the world
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="We here! All across the world"/>
</div>
<div class="lyrico-lyrics-wrapper">We going in
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="We going in"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Divine!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Divine!"/>
</div>
<div class="lyrico-lyrics-wrapper">Nas!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nas!"/>
</div>
</pre>
