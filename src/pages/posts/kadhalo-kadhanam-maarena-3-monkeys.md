---
title: "kadhalo kadhanam song lyrics"
album: "3 Monkeys"
artist: "Anil kumar G"
lyricist: "Anil Kumar G"
director: "Anil Kumar G"
path: "/albums/3-monkeys-lyrics"
song: "Kadhalo Kadhanam Maarena"
image: ../../images/albumart/3-monkeys.jpg
date: 2020-02-07
lang: telugu
youtubeLink: "https://www.youtube.com/embed/dgdKy28gVOs"
type: "melody"
singers:
  - Madhu Balakrishnan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kathalo Kathanam Maarenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathalo Kathanam Maarenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhalo Mathanam Aagunaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhalo Mathanam Aagunaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kathalo Kathanam Maarenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathalo Kathanam Maarenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhalo Mathanam Aagunaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhalo Mathanam Aagunaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Katha Nuvve Raashavulee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Katha Nuvve Raashavulee"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Katha Paivaadu Rasadulee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Katha Paivaadu Rasadulee"/>
</div>
<div class="lyrico-lyrics-wrapper">Brathukee Vethike Ammagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Brathukee Vethike Ammagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ammeshaavee Nee Nammakam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammeshaavee Nee Nammakam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kathalo Kathanam Maarenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathalo Kathanam Maarenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhalo Mathanam Aagunaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhalo Mathanam Aagunaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anukundhi Jaragadhulee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anukundhi Jaragadhulee"/>
</div>
<div class="lyrico-lyrics-wrapper">Anukokundaa Jarugunee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anukokundaa Jarugunee"/>
</div>
<div class="lyrico-lyrics-wrapper">Porapaate Grahapatai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porapaate Grahapatai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Gamananne Marchenee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Gamananne Marchenee"/>
</div>
<div class="lyrico-lyrics-wrapper">Neelo Vunna Aashale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelo Vunna Aashale"/>
</div>
<div class="lyrico-lyrics-wrapper">Champeeshavee Lopalee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Champeeshavee Lopalee"/>
</div>
<div class="lyrico-lyrics-wrapper">Yadhalooo Ee Gaayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yadhalooo Ee Gaayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thudhimeyanee Praanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thudhimeyanee Praanam"/>
</div>
</pre>
