---
title: "theengu thaakka song lyrics"
album: "Jagame Thandhiram"
artist: "Santhosh Narayanan"
lyricist: "Arivu"
director: "Karthik Subbaraj"
path: "/albums/jagame-thandhiram-song-lyrics"
song: "Theengu Thaakka"
image: ../../images/albumart/jagame-thanthiram.jpg
date: 2021-06-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/2Eycu_9XAaM"
type: "mass"
singers:
  - Arivu
  - GKB
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Theengu thaakka thaakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theengu thaakka thaakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoondi pottu thookka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoondi pottu thookka"/>
</div>
<div class="lyrico-lyrics-wrapper">Aantha paakka paakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aantha paakka paakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Aattam kaattum venga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aattam kaattum venga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veembu kaatta kaatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veembu kaatta kaatta"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaeli thaandi nokka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaeli thaandi nokka"/>
</div>
<div class="lyrico-lyrics-wrapper">Vevu paakka paakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vevu paakka paakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Vettaiyaadum venga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vettaiyaadum venga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanthiri kottaikkulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanthiri kottaikkulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Raja thanthiri ronthu paaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja thanthiri ronthu paaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Vantha manthiri undu sangathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vantha manthiri undu sangathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kazhugu paatha sundeli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kazhugu paatha sundeli"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathirukku sangu di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathirukku sangu di"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattam kattiyaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattam kattiyaachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna verarukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna verarukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Neram ippo vanthiruchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neram ippo vanthiruchu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theengu thaakka thaakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theengu thaakka thaakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoondi pottu thookka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoondi pottu thookka"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthiruchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthiruchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aantha paakka paakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aantha paakka paakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Aattam kaattum venga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aattam kaattum venga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thappu panren avlothaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappu panren avlothaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Perusu sirusunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perusu sirusunu"/>
</div>
<div class="lyrico-lyrics-wrapper">Alavellam kidaiyathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alavellam kidaiyathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Otranukku matra yaarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Otranukku matra yaarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Theva illa da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theva illa da"/>
</div>
<div class="lyrico-lyrics-wrapper">Chumma vittuputtu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chumma vittuputtu"/>
</div>
<div class="lyrico-lyrics-wrapper">Porathippo arththamilla da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porathippo arththamilla da"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaththiraththa kaatta thaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaththiraththa kaatta thaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaaththirikkirom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaththirikkirom"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna pottathukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna pottathukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Pinna thaanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pinna thaanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Rest edukkurom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rest edukkurom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theengu thaakka thaakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theengu thaakka thaakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoondi pottu thookka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoondi pottu thookka"/>
</div>
<div class="lyrico-lyrics-wrapper">Aantha paakka paakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aantha paakka paakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Aattam kaattum venga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aattam kaattum venga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veembu kaatta kaatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veembu kaatta kaatta"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaeli thaandi nokka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaeli thaandi nokka"/>
</div>
<div class="lyrico-lyrics-wrapper">Vevu paakka paakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vevu paakka paakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Vettaiyaadum venga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vettaiyaadum venga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Check-u vachitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Check-u vachitten"/>
</div>
<div class="lyrico-lyrics-wrapper">Pangu patha vachitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pangu patha vachitten"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna sketch pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna sketch pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulla vandhu sikka vachitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulla vandhu sikka vachitten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannu vachitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu vachitten"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaila gun-u vachitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaila gun-u vachitten"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna mannu thondi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna mannu thondi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolli poda solli vachitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolli poda solli vachitten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varudhu kaataru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varudhu kaataru"/>
</div>
<div class="lyrico-lyrics-wrapper">Pottiya thookinu odu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pottiya thookinu odu"/>
</div>
<div class="lyrico-lyrics-wrapper">Uruthi kaatadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uruthi kaatadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un usura edukkum pothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un usura edukkum pothu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kathara katharathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathara katharathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethiri kottai aadumda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethiri kottai aadumda"/>
</div>
<div class="lyrico-lyrics-wrapper">Sithari sithari thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sithari sithari thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Narigal ottam odumda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Narigal ottam odumda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kazhugu kazhuguthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kazhugu kazhuguthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Paranthu nottam kaanumda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paranthu nottam kaanumda"/>
</div>
<div class="lyrico-lyrics-wrapper">Samayam paathu saaikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samayam paathu saaikum"/>
</div>
<div class="lyrico-lyrics-wrapper">Siruthai palluda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siruthai palluda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theengu thaakka thaakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theengu thaakka thaakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoondi pottu thookka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoondi pottu thookka"/>
</div>
<div class="lyrico-lyrics-wrapper">Aantha paakka paakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aantha paakka paakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Aattam kaattum venga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aattam kaattum venga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veembu kaatta kaatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veembu kaatta kaatta"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaeli thaandi nokka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaeli thaandi nokka"/>
</div>
<div class="lyrico-lyrics-wrapper">Vevu paakka paakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vevu paakka paakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Vettaiyaadum venga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vettaiyaadum venga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theengu thaakka thaakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theengu thaakka thaakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoondi pottu thookka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoondi pottu thookka"/>
</div>
<div class="lyrico-lyrics-wrapper">Aantha paakka paakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aantha paakka paakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Aattam kaattum venga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aattam kaattum venga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veembu kaatta kaatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veembu kaatta kaatta"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaeli thaandi nokka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaeli thaandi nokka"/>
</div>
<div class="lyrico-lyrics-wrapper">Vevu paakka paakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vevu paakka paakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Vettaiyaadum venga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vettaiyaadum venga"/>
</div>
</pre>
