---
title: "kannaadi maalai song lyrics"
album: "Amutha"
artist: "Arun Gopan"
lyricist: "G. Ra"
director: "P.S. Arjun"
path: "/albums/amutha-lyrics"
song: "Kannaadi Maalai"
image: ../../images/albumart/amutha.jpg
date: 2018-05-16
lang: tamil
youtubeLink: "https://www.youtube.com/embed/W--4M20QAe0"
type: "sad"
singers:
  - P. Jayachandran
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kannaadi Maalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaadi Maalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Maarodu Serththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maarodu Serththu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanneeril Thallaaduvean 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneeril Thallaaduvean "/>
</div>
<div class="lyrico-lyrics-wrapper">Paattondru Kettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paattondru Kettu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannaadi Maalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaadi Maalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Maarodu Serththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maarodu Serththu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanneeril Thallaaduvean 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneeril Thallaaduvean "/>
</div>
<div class="lyrico-lyrics-wrapper">Paattondru Kettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paattondru Kettu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Odaathey Aadaathey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odaathey Aadaathey "/>
</div>
<div class="lyrico-lyrics-wrapper">Kaivittu Thedaathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaivittu Thedaathey"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogaatha Dhooram 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogaatha Dhooram "/>
</div>
<div class="lyrico-lyrics-wrapper">Namma Poka Venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma Poka Venum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Odaathey Aadaathey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odaathey Aadaathey "/>
</div>
<div class="lyrico-lyrics-wrapper">Kaivittu Thedaathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaivittu Thedaathey"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogaatha Dhooram 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogaatha Dhooram "/>
</div>
<div class="lyrico-lyrics-wrapper">Namma Poka Venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma Poka Venum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thuvaatha Vaanam Engum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuvaatha Vaanam Engum"/>
</div>
<div class="lyrico-lyrics-wrapper">Seeraana Megham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seeraana Megham"/>
</div>
<div class="lyrico-lyrics-wrapper">Odaama Maele Ninnu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odaama Maele Ninnu "/>
</div>
<div class="lyrico-lyrics-wrapper">Thaangaama Eangum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaangaama Eangum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inneram Unnaala 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inneram Unnaala "/>
</div>
<div class="lyrico-lyrics-wrapper">Ullaasam Aakuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullaasam Aakuthey"/>
</div>
<div class="lyrico-lyrics-wrapper">Paaraatta Seeraatta 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaraatta Seeraatta "/>
</div>
<div class="lyrico-lyrics-wrapper">Thaayaakava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaayaakava"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarijaatha Povai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarijaatha Povai "/>
</div>
<div class="lyrico-lyrics-wrapper">Pola Paakkum Pothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pola Paakkum Pothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Eari Mela Odam Pola 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eari Mela Odam Pola "/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Thaangava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Thaangava"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadhiya Kadala Ada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadhiya Kadala Ada"/>
</div>
<div class="lyrico-lyrics-wrapper">Neenthi Paayalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neenthi Paayalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannaadi Maalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaadi Maalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Maarodu Serththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maarodu Serththu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanneeril Thallaaduvean 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneeril Thallaaduvean "/>
</div>
<div class="lyrico-lyrics-wrapper">Paattondru Kettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paattondru Kettu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannaadi Maalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaadi Maalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Maarodu Serththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maarodu Serththu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanneeril Thallaaduvean 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneeril Thallaaduvean "/>
</div>
<div class="lyrico-lyrics-wrapper">Paattondru Kettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paattondru Kettu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Odaathey Aadaathey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odaathey Aadaathey "/>
</div>
<div class="lyrico-lyrics-wrapper">Kaivittu Thedaathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaivittu Thedaathey"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogaatha Dhooram 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogaatha Dhooram "/>
</div>
<div class="lyrico-lyrics-wrapper">Namma Poka Venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma Poka Venum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Odaathey Aadaathey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odaathey Aadaathey "/>
</div>
<div class="lyrico-lyrics-wrapper">Kaivittu Thedaathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaivittu Thedaathey"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogaatha Dhooram 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogaatha Dhooram "/>
</div>
<div class="lyrico-lyrics-wrapper">Namma Poka Venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma Poka Venum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thuvaatha Vaanam Engum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuvaatha Vaanam Engum"/>
</div>
<div class="lyrico-lyrics-wrapper">Seeraana Megham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seeraana Megham"/>
</div>
<div class="lyrico-lyrics-wrapper">Odaama Maele Ninnu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odaama Maele Ninnu "/>
</div>
<div class="lyrico-lyrics-wrapper">Thaangaama Eangum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaangaama Eangum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollaamaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollaamaley"/>
</div>
</pre>
