---
title: "oh dil hanjuaan song lyrics"
album: "Tuesdays And Fridays"
artist: "Tony Kakkar"
lyricist: "Kumaar"
director: "Taranveer Singh"
path: "/albums/tuesdays-and-fridays-lyrics"
song: "Oh Dil Hanjuaan"
image: ../../images/albumart/tuesdays-and-fridays.jpg
date: 2021-02-19
lang: hindi
youtubeLink: "https://www.youtube.com/embed/wJzUf1v6LDY"
type: "love"
singers:
  - Shreya Ghoshal
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Jaan leke jaan giyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaan leke jaan giyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Teriyan judaiyan ve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teriyan judaiyan ve"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu gaya baad tere
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu gaya baad tere"/>
</div>
<div class="lyrico-lyrics-wrapper">Neendran na aaiyan ve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neendran na aaiyan ve"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho jaan leke jaan giyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho jaan leke jaan giyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Teriyan judaiyan ve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teriyan judaiyan ve"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu gaya baad tere
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu gaya baad tere"/>
</div>
<div class="lyrico-lyrics-wrapper">Neendran na aaiyan ve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neendran na aaiyan ve"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raat raat jagun main
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raat raat jagun main"/>
</div>
<div class="lyrico-lyrics-wrapper">Jag soyi soyi jaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jag soyi soyi jaave"/>
</div>
<div class="lyrico-lyrics-wrapper">Dil hanjuaan bina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil hanjuaan bina"/>
</div>
<div class="lyrico-lyrics-wrapper">Royi royi jaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Royi royi jaave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh dil hanjuaan bina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh dil hanjuaan bina"/>
</div>
<div class="lyrico-lyrics-wrapper">Royi royi jaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Royi royi jaave"/>
</div>
<div class="lyrico-lyrics-wrapper">Haan royi royi jaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haan royi royi jaave"/>
</div>
<div class="lyrico-lyrics-wrapper">Royi jaave, royi royi jaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Royi jaave, royi royi jaave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh chann de musafiran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh chann de musafiran"/>
</div>
<div class="lyrico-lyrics-wrapper">Kyun ban gaya kafiran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kyun ban gaya kafiran"/>
</div>
<div class="lyrico-lyrics-wrapper">Ve rull gayi chandani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ve rull gayi chandani"/>
</div>
<div class="lyrico-lyrics-wrapper">Le dooba tera aasra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Le dooba tera aasra"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho chann de musafiran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho chann de musafiran"/>
</div>
<div class="lyrico-lyrics-wrapper">Kyun ban gaya kafiran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kyun ban gaya kafiran"/>
</div>
<div class="lyrico-lyrics-wrapper">Ve rull gayi chandani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ve rull gayi chandani"/>
</div>
<div class="lyrico-lyrics-wrapper">Le dooba tera aasra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Le dooba tera aasra"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeh faasle nahi hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh faasle nahi hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Teri meri marzi ke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teri meri marzi ke"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haan..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haan.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeh faasle nahi hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh faasle nahi hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Teri meri marzi ke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teri meri marzi ke"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeh toh bahane hai bas
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh toh bahane hai bas"/>
</div>
<div class="lyrico-lyrics-wrapper">Khud ki khudgarzi ke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khud ki khudgarzi ke"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iss baat ka hai gham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iss baat ka hai gham"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaise sunaye hum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaise sunaye hum"/>
</div>
<div class="lyrico-lyrics-wrapper">Lafz dil ki arzi ke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lafz dil ki arzi ke"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Zind jaan tere bina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zind jaan tere bina"/>
</div>
<div class="lyrico-lyrics-wrapper">Moyi moyi jaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moyi moyi jaave"/>
</div>
<div class="lyrico-lyrics-wrapper">Dil hanjuaan bina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil hanjuaan bina"/>
</div>
<div class="lyrico-lyrics-wrapper">Royi royi jaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Royi royi jaave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dil hanjuaan bina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil hanjuaan bina"/>
</div>
<div class="lyrico-lyrics-wrapper">Royi royi jaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Royi royi jaave"/>
</div>
<div class="lyrico-lyrics-wrapper">Haan royi royi jaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haan royi royi jaave"/>
</div>
<div class="lyrico-lyrics-wrapper">Royi jaave, royi royi jaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Royi jaave, royi royi jaave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho mere paas main nahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho mere paas main nahi"/>
</div>
<div class="lyrico-lyrics-wrapper">Tere sath reh gayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere sath reh gayi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haan..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haan.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mere paas main nahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mere paas main nahi"/>
</div>
<div class="lyrico-lyrics-wrapper">Tere sath reh gayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere sath reh gayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Iss baar jo hai aayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iss baar jo hai aayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Barsaat beh gayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Barsaat beh gayi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aadhi adhoori si
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadhi adhoori si"/>
</div>
<div class="lyrico-lyrics-wrapper">Jo thhi zaroori si
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jo thhi zaroori si"/>
</div>
<div class="lyrico-lyrics-wrapper">Wahi baat keh gayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wahi baat keh gayi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paane se jyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paane se jyada"/>
</div>
<div class="lyrico-lyrics-wrapper">Tujhe khoyi khoyi jaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tujhe khoyi khoyi jaave"/>
</div>
<div class="lyrico-lyrics-wrapper">Dil hanjuaan bina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil hanjuaan bina"/>
</div>
<div class="lyrico-lyrics-wrapper">Royi royi jaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Royi royi jaave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh dil hanjuaan bina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh dil hanjuaan bina"/>
</div>
<div class="lyrico-lyrics-wrapper">Royi royi jaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Royi royi jaave"/>
</div>
<div class="lyrico-lyrics-wrapper">Haan royi royi jaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haan royi royi jaave"/>
</div>
<div class="lyrico-lyrics-wrapper">Royi jaave, royi royi jaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Royi jaave, royi royi jaave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh chann de musafiran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh chann de musafiran"/>
</div>
<div class="lyrico-lyrics-wrapper">Kyun ban gaya kafiran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kyun ban gaya kafiran"/>
</div>
<div class="lyrico-lyrics-wrapper">Ve rull gayi chandani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ve rull gayi chandani"/>
</div>
<div class="lyrico-lyrics-wrapper">Le dooba tera aasra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Le dooba tera aasra"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho chann de musafiran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho chann de musafiran"/>
</div>
<div class="lyrico-lyrics-wrapper">Kyun ban gaya kafiran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kyun ban gaya kafiran"/>
</div>
<div class="lyrico-lyrics-wrapper">Ve rull gayi chandani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ve rull gayi chandani"/>
</div>
<div class="lyrico-lyrics-wrapper">Le dooba tera aasra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Le dooba tera aasra"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haaye royi royi jaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haaye royi royi jaave"/>
</div>
</pre>
