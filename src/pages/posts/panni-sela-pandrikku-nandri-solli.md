---
title: "panni sela song lyrics"
album: "Pandrikku Nandri Solli"
artist: "Suren Vikhash"
lyricist: "Suren Vikhash"
director: "Bala Aran"
path: "/albums/pandrikku-nandri-solli-lyrics"
song: "Panni Sela"
image: ../../images/albumart/pandrikku-nandri-solli.jpg
date: 2022-02-04
lang: tamil
youtubeLink: 
type: "happy"
singers:
  - Benny Dayal
  - Sofia Ashraf
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">panni sela paanaikulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="panni sela paanaikulla"/>
</div>
<div class="lyrico-lyrics-wrapper">kena paya kaatukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kena paya kaatukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">kerallava thandiyachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kerallava thandiyachu"/>
</div>
<div class="lyrico-lyrics-wrapper">kedi payala maariyachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kedi payala maariyachu"/>
</div>
<div class="lyrico-lyrics-wrapper">saamiyarum maaya jaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saamiyarum maaya jaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">vilagi pochu raagu kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vilagi pochu raagu kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">nethu semba inime komba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nethu semba inime komba"/>
</div>
<div class="lyrico-lyrics-wrapper">kanna moodi kathula parapen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanna moodi kathula parapen"/>
</div>
<div class="lyrico-lyrics-wrapper">thee potikulla thee kuchi illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thee potikulla thee kuchi illa"/>
</div>
<div class="lyrico-lyrics-wrapper">beedi kooda ipo patha villa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="beedi kooda ipo patha villa"/>
</div>
<div class="lyrico-lyrics-wrapper">thee potikulla thee kuchi illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thee potikulla thee kuchi illa"/>
</div>
<div class="lyrico-lyrics-wrapper">beedi kooda ipo pathave villa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="beedi kooda ipo pathave villa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">suthu potu madaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suthu potu madaku"/>
</div>
<div class="lyrico-lyrics-wrapper">kozhi vachu adaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kozhi vachu adaku"/>
</div>
<div class="lyrico-lyrics-wrapper">kaiya vacha kadipa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaiya vacha kadipa"/>
</div>
<div class="lyrico-lyrics-wrapper">kallan pola nadipan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kallan pola nadipan"/>
</div>
<div class="lyrico-lyrics-wrapper">antha vada vayaloda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antha vada vayaloda"/>
</div>
<div class="lyrico-lyrics-wrapper">kooda sella ada iva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kooda sella ada iva"/>
</div>
<div class="lyrico-lyrics-wrapper">aaga maata pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaga maata pola"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thodi thodi katha kathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thodi thodi katha kathu"/>
</div>
<div class="lyrico-lyrics-wrapper">kuninju maranju kalanju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuninju maranju kalanju"/>
</div>
<div class="lyrico-lyrics-wrapper">tholanju vagama enga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tholanju vagama enga"/>
</div>
<div class="lyrico-lyrics-wrapper">nee pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee pora"/>
</div>
<div class="lyrico-lyrics-wrapper">nenachu nenachu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenachu nenachu "/>
</div>
<div class="lyrico-lyrics-wrapper">thapichu thapichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thapichu thapichu"/>
</div>
<div class="lyrico-lyrics-wrapper">kalaichu kudichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalaichu kudichu"/>
</div>
<div class="lyrico-lyrics-wrapper">thavichu thavichu ipo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thavichu thavichu ipo"/>
</div>
<div class="lyrico-lyrics-wrapper">nee engiruthu vaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee engiruthu vaara"/>
</div>
<div class="lyrico-lyrics-wrapper">enga pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga pora"/>
</div>
<div class="lyrico-lyrics-wrapper">enga iruntha vaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga iruntha vaara"/>
</div>
<div class="lyrico-lyrics-wrapper">enga pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga pora"/>
</div>
<div class="lyrico-lyrics-wrapper">enga iruntha vaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga iruntha vaara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thee potikulla thee kuchi illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thee potikulla thee kuchi illa"/>
</div>
<div class="lyrico-lyrics-wrapper">beedi kooda ipo patha villa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="beedi kooda ipo patha villa"/>
</div>
<div class="lyrico-lyrics-wrapper">thee potikulla thee kuchi illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thee potikulla thee kuchi illa"/>
</div>
<div class="lyrico-lyrics-wrapper">beedi kooda ipo pathave villa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="beedi kooda ipo pathave villa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nethu pota sokka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nethu pota sokka"/>
</div>
<div class="lyrico-lyrics-wrapper">nee kadicha mokka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee kadicha mokka"/>
</div>
<div class="lyrico-lyrics-wrapper">puthusa yosika 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthusa yosika "/>
</div>
<div class="lyrico-lyrics-wrapper">moode illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moode illa"/>
</div>
<div class="lyrico-lyrics-wrapper">kooda sella vida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kooda sella vida"/>
</div>
<div class="lyrico-lyrics-wrapper">somberi illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="somberi illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nee paanaikulla kedantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee paanaikulla kedantha"/>
</div>
<div class="lyrico-lyrics-wrapper">kedantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kedantha"/>
</div>
<div class="lyrico-lyrics-wrapper">naan kanji vachu tharuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan kanji vachu tharuven"/>
</div>
<div class="lyrico-lyrics-wrapper">nedukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nedukka"/>
</div>
<div class="lyrico-lyrics-wrapper">un thodankitta eluntha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un thodankitta eluntha"/>
</div>
<div class="lyrico-lyrics-wrapper">poruppa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poruppa"/>
</div>
<div class="lyrico-lyrics-wrapper">unna killi vachu rasipen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna killi vachu rasipen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">panni sela paanaikulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="panni sela paanaikulla"/>
</div>
<div class="lyrico-lyrics-wrapper">kena paya kaatukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kena paya kaatukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">kerallava thandiyachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kerallava thandiyachu"/>
</div>
<div class="lyrico-lyrics-wrapper">kedi payala maariyachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kedi payala maariyachu"/>
</div>
<div class="lyrico-lyrics-wrapper">saamiyarum maaya jaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saamiyarum maaya jaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">vilagi pochu raagu kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vilagi pochu raagu kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">nethu semba inime komba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nethu semba inime komba"/>
</div>
<div class="lyrico-lyrics-wrapper">kanna moodi kathula parapen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanna moodi kathula parapen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ye kilaka merka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ye kilaka merka "/>
</div>
<div class="lyrico-lyrics-wrapper">therka vadakka thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="therka vadakka thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">pathu kandu puduchen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathu kandu puduchen"/>
</div>
<div class="lyrico-lyrics-wrapper">varaya poraya sariya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varaya poraya sariya"/>
</div>
<div class="lyrico-lyrics-wrapper">moraya sonna velaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moraya sonna velaya"/>
</div>
<div class="lyrico-lyrics-wrapper">senju mudichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="senju mudichen"/>
</div>
<div class="lyrico-lyrics-wrapper">kelakaa kelakaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kelakaa kelakaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">panni sela paanaikulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="panni sela paanaikulla"/>
</div>
<div class="lyrico-lyrics-wrapper">kena paya kaatukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kena paya kaatukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">kerallava thandiyachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kerallava thandiyachu"/>
</div>
<div class="lyrico-lyrics-wrapper">kedi payala maariyachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kedi payala maariyachu"/>
</div>
<div class="lyrico-lyrics-wrapper">saamiyarum maaya jaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saamiyarum maaya jaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">vilagi pochu raagu kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vilagi pochu raagu kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">nethu semba inime komba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nethu semba inime komba"/>
</div>
<div class="lyrico-lyrics-wrapper">kanna moodi kathula parapen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanna moodi kathula parapen"/>
</div>
</pre>
