---
title: "va pulla na mapila song lyrics"
album: "Thambikkottai"
artist: "D. Imman"
lyricist: "Viveka"
director: "R.Rahesh"
path: "/albums/thambikkottai-lyrics"
song: "Va Pulla Na Mapila"
image: ../../images/albumart/thambikkottai.jpg
date: 2011-02-11
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Ry1_-y8x8V0"
type: "love"
singers:
  - Velmurugan
  - Mukesh
  - M.L.R. Karthikeyan
  - Senthil Das
  - Rodrigo Duterte
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Bus-su saththam Reyilukkoottam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bus-su saththam Reyilukkoottam"/>
</div>
<div class="lyrico-lyrics-wrapper">koovangaaththu kosukkadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koovangaaththu kosukkadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Fasttu Food-du Power kattu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fasttu Food-du Power kattu "/>
</div>
<div class="lyrico-lyrics-wrapper">uppuththanni Traffic-ku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uppuththanni Traffic-ku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chennaikku Taataa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chennaikku Taataa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Katta vandi kammaakkarai kalaththummedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katta vandi kammaakkarai kalaththummedu"/>
</div>
<div class="lyrico-lyrics-wrapper">kammangaadey pachcha vayal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kammangaadey pachcha vayal"/>
</div>
<div class="lyrico-lyrics-wrapper">paavaadai thaavani village-kku vandhanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paavaadai thaavani village-kku vandhanam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa pulla naan un maappulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa pulla naan un maappulla"/>
</div>
<div class="lyrico-lyrics-wrapper">muththaadha vayasu paththaadha iduppu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muththaadha vayasu paththaadha iduppu"/>
</div>
<div class="lyrico-lyrics-wrapper">kuththaala sirippu maththaala odambu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuththaala sirippu maththaala odambu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa pulla naan un maappulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa pulla naan un maappulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vettaana paarvai saththaana thoatram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vettaana paarvai saththaana thoatram"/>
</div>
<div class="lyrico-lyrics-wrapper">muththaana pechu piththaakum asaivu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muththaana pechu piththaakum asaivu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa pulla naan un maappulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa pulla naan un maappulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Graamaththu koondhalukku seekkaa vaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Graamaththu koondhalukku seekkaa vaasam"/>
</div>
<div class="lyrico-lyrics-wrapper">pakkaththu ooruvarai soakkaa veesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pakkaththu ooruvarai soakkaa veesum"/>
</div>
<div class="lyrico-lyrics-wrapper">Village ponnu paarkkaadha ninnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Village ponnu paarkkaadha ninnu"/>
</div>
<div class="lyrico-lyrics-wrapper">ennoada manasu thaangaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennoada manasu thaangaadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa pulla naan un maappulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa pulla naan un maappulla"/>
</div>
<div class="lyrico-lyrics-wrapper">vaa vaa pulla nan un maamaa pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaa vaa pulla nan un maamaa pulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Venneeril vedhaiyellaam verppudikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venneeril vedhaiyellaam verppudikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">onna paarththaaley pulikkooda poopparikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="onna paarththaaley pulikkooda poopparikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa pulla naan un maappulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa pulla naan un maappulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye un perai kuyil koottam munumunukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye un perai kuyil koottam munumunukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">megam un veettil mazhaithoova adam pidikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="megam un veettil mazhaithoova adam pidikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa pulla naan un maappulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa pulla naan un maappulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye vallinam mellinam idaiyinam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye vallinam mellinam idaiyinam"/>
</div>
<div class="lyrico-lyrics-wrapper">konjamum unakku theriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konjamum unakku theriyala"/>
</div>
<div class="lyrico-lyrics-wrapper">kuraleduththu paadinaa un paattukku oru inai illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuraleduththu paadinaa un paattukku oru inai illa"/>
</div>
<div class="lyrico-lyrics-wrapper">manasu vachchaa naan maappulla maappulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasu vachchaa naan maappulla maappulla"/>
</div>
<div class="lyrico-lyrics-wrapper">illaatti en vaazhkkai vaeppilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="illaatti en vaazhkkai vaeppilla"/>
</div>
<div class="lyrico-lyrics-wrapper">ada bayapulla!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada bayapulla!"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa pulla naan un maappulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa pulla naan un maappulla"/>
</div>
<div class="lyrico-lyrics-wrapper">vaa vaa pulla nan un maamaa pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaa vaa pulla nan un maamaa pulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">muththaadha vayasu paththaadha iduppu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muththaadha vayasu paththaadha iduppu"/>
</div>
<div class="lyrico-lyrics-wrapper">kuththaala sirippu maththaala odambu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuththaala sirippu maththaala odambu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uye pappaali vedhaippoala karuththa vizhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uye pappaali vedhaippoala karuththa vizhi"/>
</div>
<div class="lyrico-lyrics-wrapper">anga pattaasu thirippoala puruvamoadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anga pattaasu thirippoala puruvamoadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa pulla naan un maappulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa pulla naan un maappulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">O semmannu nelam poala udal azhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O semmannu nelam poala udal azhagu"/>
</div>
<div class="lyrico-lyrics-wrapper">kadal seraadha nadhippoala nervagidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadal seraadha nadhippoala nervagidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa pulla naan un maappulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa pulla naan un maappulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vambugal koottam kannula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vambugal koottam kannula"/>
</div>
<div class="lyrico-lyrics-wrapper">oru sebaga thoattam nenjula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru sebaga thoattam nenjula"/>
</div>
<div class="lyrico-lyrics-wrapper">radhiye unna minjula kandaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="radhiye unna minjula kandaa "/>
</div>
<div class="lyrico-lyrics-wrapper">kaayum pazhukkum pinjula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaayum pazhukkum pinjula"/>
</div>
<div class="lyrico-lyrics-wrapper">nee manasu vachchaa naan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee manasu vachchaa naan "/>
</div>
<div class="lyrico-lyrics-wrapper">maappulla maappulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maappulla maappulla"/>
</div>
<div class="lyrico-lyrics-wrapper">illaatti en vaazhkkai vaeppilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="illaatti en vaazhkkai vaeppilla"/>
</div>
<div class="lyrico-lyrics-wrapper">ada bayapulla!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada bayapulla!"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa pulla naan un maappulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa pulla naan un maappulla"/>
</div>
<div class="lyrico-lyrics-wrapper">vaa vaa pulla nan un maamaa pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaa vaa pulla nan un maamaa pulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">muththaadha vayasu paththaadha iduppu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muththaadha vayasu paththaadha iduppu"/>
</div>
<div class="lyrico-lyrics-wrapper">kuththaala sirippu maththaala odambu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuththaala sirippu maththaala odambu"/>
</div>
</pre>
