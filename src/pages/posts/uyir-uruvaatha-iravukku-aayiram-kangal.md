---
title: "uyir uruvaatha song lyrics"
album: "Iravukku Aayiram Kangal"
artist: "Sam CS"
lyricist: "Sam CS"
director: "Mu. Maran"
path: "/albums/iravukku-aayiram-kangal-lyrics"
song: "Uyir Uruvaatha"
image: ../../images/albumart/iravukku-aayiram-kangal.jpg
date: 2018-05-11
lang: tamil
youtubeLink: "https://www.youtube.com/embed/7B_1oIBvWqI"
type: "love"
singers:
  - Sathyaprakash
  - Chinmayi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Haaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Haaaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haaaaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Haaaaaaaaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haaaaaaaaaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Haaaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haaaaaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyir uruvaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir uruvaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Urukulaikkatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urukulaikkatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennil vandhu sera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennil vandhu sera"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee yosikkaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee yosikkaadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhisai ariyaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhisai ariyaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Paravaiyapola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paravaiyapola"/>
</div>
<div class="lyrico-lyrics-wrapper">Parakkavum aasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parakkavum aasa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnodu dhoora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnodu dhoora"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhkka theera theera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkka theera theera"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaayen nizhalaa kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaayen nizhalaa kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Saagum dhooram poga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saagum dhooram poga"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunaiya neeyum theva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunaiya neeyum theva"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan un kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan un kooda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ohoo oo hoho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohoo oo hoho"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohoo oo hoho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohoo oo hoho"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoho hoho hohohoooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoho hoho hohohoooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohoo oo hoho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohoo oo hoho"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohoo oo hoho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohoo oo hoho"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoho hoho hohohoooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoho hoho hohohoooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un nenappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un nenappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjukuzhivara irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjukuzhivara irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">En ulagam muzhusum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En ulagam muzhusum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai suththi suththi kedakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai suththi suththi kedakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un nenappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un nenappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjukuzhivara irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjukuzhivara irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">En ulagam muzhusum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En ulagam muzhusum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai suththi suththi kedakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai suththi suththi kedakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manasula oruvidha validhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasula oruvidha validhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Sugama sugama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sugama sugama"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakkula urukkura unna neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkula urukkura unna neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nejama nejama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nejama nejama"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannae kannae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannae kannae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam dhorum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam dhorum"/>
</div>
<div class="lyrico-lyrics-wrapper">Enkooda nee mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enkooda nee mattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Podhum podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhum podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee naalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee naalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ohoo oo hoho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohoo oo hoho"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohoo oo hoho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohoo oo hoho"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoho hoho hohohoooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoho hoho hohohoooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohoo oo hoho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohoo oo hoho"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohoo oo hoho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohoo oo hoho"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoho hoho hohohoooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoho hoho hohohoooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan muzhusa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan muzhusa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai enakkula podhachen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai enakkula podhachen"/>
</div>
<div class="lyrico-lyrics-wrapper">En usura azhagae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En usura azhagae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna nitham nitham nenachen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna nitham nitham nenachen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan muzhusa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan muzhusa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai enakkula podhachen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai enakkula podhachen"/>
</div>
<div class="lyrico-lyrics-wrapper">En usura azhagae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En usura azhagae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna nitham nitham nenachen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna nitham nitham nenachen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ini varum jenmam mottham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini varum jenmam mottham"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum dhaan oravaa varanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum dhaan oravaa varanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Marupadi unakkena pirandhidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marupadi unakkena pirandhidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Varam naan peranum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varam naan peranum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pennae pennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pennae pennae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhka neela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhka neela"/>
</div>
<div class="lyrico-lyrics-wrapper">En kuda nee mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kuda nee mattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Podhum podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhum podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee naalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee naalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ohoo oo hoho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohoo oo hoho"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohoo oo hoho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohoo oo hoho"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoho hoho hohohoooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoho hoho hohohoooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohoo oo hoho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohoo oo hoho"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohoo oo hoho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohoo oo hoho"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoho hoho hohohoooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoho hoho hohohoooo"/>
</div>
</pre>
