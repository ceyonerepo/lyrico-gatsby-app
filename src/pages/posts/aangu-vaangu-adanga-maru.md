---
title: "aangu vaangu song lyrics"
album: "Adanga Maru"
artist: "Sam CS"
lyricist: "Logan"
director: "Karthik Thangavel"
path: "/albums/adanga-maru-lyrics"
song: "Aangu Vaangu"
image: ../../images/albumart/adanga-maru.jpg
date: 2018-12-21
lang: tamil
youtubeLink: "https://www.youtube.com/embed/MyLI8obM6x4"
type: "mass"
singers:
  - Sam C S
  - Mukesh Mohamed
  - M L R Karthikeyan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Disskiyaan owhnn
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Disskiyaan owhnn"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Disskiyaan owhnn
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Disskiyaan owhnn"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaa aaa aaa a
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaa aaa aaa a"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaangu vaangu soangu thaangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaangu vaangu soangu thaangu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sir-u ketha local song-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sir-u ketha local song-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Kooda sernthu paadlanaaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooda sernthu paadlanaaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Disskiyaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Disskiyaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jaadi jodi akkili kaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaadi jodi akkili kaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Side-u molam tiffen-u moodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Side-u molam tiffen-u moodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sir-u kooda aadlanaaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sir-u kooda aadlanaaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Disskiyaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Disskiyaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pistol kaati mersal yethum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pistol kaati mersal yethum"/>
</div>
<div class="lyrico-lyrics-wrapper">James bond-du-da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="James bond-du-da"/>
</div>
<div class="lyrico-lyrics-wrapper">Rowdy thanam panninaakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rowdy thanam panninaakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Thola uripen da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thola uripen da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Needhi nermai nyaayam paakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Needhi nermai nyaayam paakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sooran thaanada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sooran thaanada"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiruttu payala thedi pudichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiruttu payala thedi pudichu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaala odaipen da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaala odaipen da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nagaru nagaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nagaru nagaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Sir police-u pokkiri da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sir police-u pokkiri da"/>
</div>
<div class="lyrico-lyrics-wrapper">Sedhara viduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sedhara viduven"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee ozhungathaan odidu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee ozhungathaan odidu da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Disskiyaan owhnn
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Disskiyaan owhnn"/>
</div>
<div class="lyrico-lyrics-wrapper">Disskiyaan owhnn
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Disskiyaan owhnn"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Patta kaththi vechukkinu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Patta kaththi vechukkinu"/>
</div>
<div class="lyrico-lyrics-wrapper">Punch-u pesinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Punch-u pesinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nethiyila bullet vechu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nethiyila bullet vechu"/>
</div>
<div class="lyrico-lyrics-wrapper">Puncture aakkuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puncture aakkuven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Atti ulla sikkum panni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atti ulla sikkum panni"/>
</div>
<div class="lyrico-lyrics-wrapper">Sketch pottinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sketch pottinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Lathiyila laadam katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lathiyila laadam katti"/>
</div>
<div class="lyrico-lyrics-wrapper">Goondil yethuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Goondil yethuven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhaadhaavuku dhaadhaa ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaadhaavuku dhaadhaa ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sir-a kanda uchcha povaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sir-a kanda uchcha povaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Arasiyal vaadhi ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arasiyal vaadhi ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Paathathumae beedhiyaavaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paathathumae beedhiyaavaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sir romba jolly
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sir romba jolly"/>
</div>
<div class="lyrico-lyrics-wrapper">Maatnaaka gaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maatnaaka gaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Itengaaran ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Itengaaran ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Inga aavan peeli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga aavan peeli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Disskiyaan owhnn
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Disskiyaan owhnn"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaangu vaangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaangu vaangu"/>
</div>
<div class="lyrico-lyrics-wrapper">Disskiyaan owhnn
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Disskiyaan owhnn"/>
</div>
<div class="lyrico-lyrics-wrapper">Soangu thaangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soangu thaangu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaangu vaangu soangu thaangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaangu vaangu soangu thaangu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sir-u ketha local song-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sir-u ketha local song-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Kooda sernthu paadlanaaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooda sernthu paadlanaaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Disskiyaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Disskiyaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jaadi jodi akkili kaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaadi jodi akkili kaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Side-u molam tiffen-u moodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Side-u molam tiffen-u moodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sir-u kooda aadlanaaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sir-u kooda aadlanaaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Disskiyaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Disskiyaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaangu vaangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaangu vaangu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaangu vaangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaangu vaangu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaangu vaangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaangu vaangu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaangu vaangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaangu vaangu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tonto dodoin toin toin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tonto dodoin toin toin"/>
</div>
<div class="lyrico-lyrics-wrapper">Tonto dodoin toin toin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tonto dodoin toin toin"/>
</div>
<div class="lyrico-lyrics-wrapper">Tonto dodoin toin toin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tonto dodoin toin toin"/>
</div>
<div class="lyrico-lyrics-wrapper">Tara tara tara tara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tara tara tara tara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Studentuku prechana na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Studentuku prechana na"/>
</div>
<div class="lyrico-lyrics-wrapper">Munna vanthu irupenda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munna vanthu irupenda"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponnungala kindal panna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponnungala kindal panna"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiya kaala odaipen da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiya kaala odaipen da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaaki sattai podum munnae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaki sattai podum munnae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum oru student-u thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum oru student-u thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Student ellam onna aana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Student ellam onna aana"/>
</div>
<div class="lyrico-lyrics-wrapper">Padharidum government-u thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padharidum government-u thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaaval thurai nanbanennu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaval thurai nanbanennu"/>
</div>
<div class="lyrico-lyrics-wrapper">Othukkanga onna ninnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Othukkanga onna ninnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum ippo unga friend-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum ippo unga friend-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha anbukilla end-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha anbukilla end-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Salute-u adida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Salute-u adida"/>
</div>
<div class="lyrico-lyrics-wrapper">Sir-a thaan pudidaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sir-a thaan pudidaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellarum onnunu thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellarum onnunu thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Selfie eduda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Selfie eduda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaangu vaangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaangu vaangu"/>
</div>
<div class="lyrico-lyrics-wrapper">Soangu thaangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soangu thaangu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaangu vaangu soangu thaangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaangu vaangu soangu thaangu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sir-u ketha local song-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sir-u ketha local song-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Kooda sernthu paadlanaaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooda sernthu paadlanaaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Disskiyaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Disskiyaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jaadi jodi akkili kaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaadi jodi akkili kaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Side-u molam tiffen-u moodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Side-u molam tiffen-u moodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sir-u kooda aadlanaaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sir-u kooda aadlanaaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Disskiyaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Disskiyaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Disskiyaan owhnn
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Disskiyaan owhnn"/>
</div>
</pre>
