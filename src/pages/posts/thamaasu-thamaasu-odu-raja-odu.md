---
title: "thamaasu thamaasu song lyrics"
album: "Odu Raja Odu"
artist: "Tosh Nanda"
lyricist: "Parinaman"
director: "Nishanth Ravindaran - Jathin Sanker Raj"
path: "/albums/odu-raja-odu-lyrics"
song: "Thamaasu Thamaasu"
image: ../../images/albumart/odu-raja-odu.jpg
date: 2018-08-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/0X_R9s8uMWc"
type: "happy"
singers:
  - Samana Raja
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Giri Giri Giri Giri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Giri Giri Giri Giri"/>
</div>
<div class="lyrico-lyrics-wrapper">Giri Giri Giri Giri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Giri Giri Giri Giri"/>
</div>
<div class="lyrico-lyrics-wrapper">Thamaasho Thamaash
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thamaasho Thamaash"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thamaasu Thamaasu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thamaasu Thamaasu "/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagame Thamaashu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagame Thamaashu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vedikkai Nee Paaru Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedikkai Nee Paaru Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu Sariya Thavara 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu Sariya Thavara "/>
</div>
<div class="lyrico-lyrics-wrapper">Unakku Enna Kavalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakku Enna Kavalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vedichidu Pataasu Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedichidu Pataasu Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Unaku Enna Image
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unaku Enna Image"/>
</div>
<div class="lyrico-lyrics-wrapper">Vittu Kadaasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vittu Kadaasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Putta Case Aagi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Putta Case Aagi "/>
</div>
<div class="lyrico-lyrics-wrapper">Poanalum Sabaashu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poanalum Sabaashu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Keelu Mel-Aagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Keelu Mel-Aagum"/>
</div>
<div class="lyrico-lyrics-wrapper">Melu Keel-Aagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Melu Keel-Aagum"/>
</div>
<div class="lyrico-lyrics-wrapper">Indru Nirantharam Naalai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indru Nirantharam Naalai "/>
</div>
<div class="lyrico-lyrics-wrapper">Iruppavan Yaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruppavan Yaaro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Progress Enna Da Loss
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Progress Enna Da Loss"/>
</div>
<div class="lyrico-lyrics-wrapper">Nijamum Nizhalum Ore Thamaashu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijamum Nizhalum Ore Thamaashu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Minus Enna Da Plus
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Minus Enna Da Plus"/>
</div>
<div class="lyrico-lyrics-wrapper">Life-eh Kaasu Maasu Dhoosu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life-eh Kaasu Maasu Dhoosu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thana Nana Na Thana Nana Na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana Nana Na Thana Nana Na"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana Nana Na Ne Thandhane Na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana Nana Na Ne Thandhane Na"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana Nana Na Thana Nana Na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana Nana Na Thana Nana Na"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana Nana Na Ne Thandhane Na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana Nana Na Ne Thandhane Na"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thoduvaanam Vazhkai Thoda Thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoduvaanam Vazhkai Thoda Thaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Yellaarukum Aasai Thodu Thodu Machi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yellaarukum Aasai Thodu Thodu Machi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thodavum Mudiyaathe Un Pirappe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodavum Mudiyaathe Un Pirappe"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannirin Mele Yezhuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannirin Mele Yezhuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Giri Giri Giri Giri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Giri Giri Giri Giri"/>
</div>
<div class="lyrico-lyrics-wrapper">Giri Giri Giri Giri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Giri Giri Giri Giri"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadigara Vazhvu Semma Bore
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadigara Vazhvu Semma Bore"/>
</div>
<div class="lyrico-lyrics-wrapper">Iravu Pagalaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravu Pagalaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilayattu Vazhkai Vilayaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilayattu Vazhkai Vilayaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vettri Tholvi Ethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vettri Tholvi Ethu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eppozhuthum Santosha Thee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppozhuthum Santosha Thee"/>
</div>
<div class="lyrico-lyrics-wrapper">Pathanom Da Nenjukkule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathanom Da Nenjukkule"/>
</div>
<div class="lyrico-lyrics-wrapper">Othakaalu Pambarama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Othakaalu Pambarama"/>
</div>
<div class="lyrico-lyrics-wrapper">Suthanom Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suthanom Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">White Blackishu Black Whitishu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="White Blackishu Black Whitishu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nijamum Nizhalum Ore Thamaashu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijamum Nizhalum Ore Thamaashu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Minus Enna Da Plus
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Minus Enna Da Plus"/>
</div>
<div class="lyrico-lyrics-wrapper">Life-eh Kaasu Maasu Dhoosu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life-eh Kaasu Maasu Dhoosu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thamaasu Thamaasu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thamaasu Thamaasu "/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagame Thamaashu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagame Thamaashu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vedikkai Nee Paaru Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedikkai Nee Paaru Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu Sariya Thavara 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu Sariya Thavara "/>
</div>
<div class="lyrico-lyrics-wrapper">Unakku Enna Kavalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakku Enna Kavalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vedichidu Pataasu Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedichidu Pataasu Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Unaku Enna Image
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unaku Enna Image"/>
</div>
<div class="lyrico-lyrics-wrapper">Vittu Kadaasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vittu Kadaasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Putta Case Aagi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Putta Case Aagi "/>
</div>
<div class="lyrico-lyrics-wrapper">Poanalum Sabaashu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poanalum Sabaashu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Keelu Mel-Aagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Keelu Mel-Aagum"/>
</div>
<div class="lyrico-lyrics-wrapper">Melu Keel-Aagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Melu Keel-Aagum"/>
</div>
<div class="lyrico-lyrics-wrapper">Indru Nirantharam Naalai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indru Nirantharam Naalai "/>
</div>
<div class="lyrico-lyrics-wrapper">Iruppavan Yaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruppavan Yaaro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dei Ramesh Dei Suresh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dei Ramesh Dei Suresh"/>
</div>
<div class="lyrico-lyrics-wrapper">Nijamum Nizhalum Ore Thamaashu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijamum Nizhalum Ore Thamaashu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Minus Enna Da Plus
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Minus Enna Da Plus"/>
</div>
<div class="lyrico-lyrics-wrapper">Life-eh Kaasu Maasu Dhoosu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life-eh Kaasu Maasu Dhoosu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thana Nana Na Thana Nana Na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana Nana Na Thana Nana Na"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana Nana Na Ne Thandhane Na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana Nana Na Ne Thandhane Na"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana Nana Na Thana Nana Na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana Nana Na Thana Nana Na"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana Nana Na Ne Thandhane Na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana Nana Na Ne Thandhane Na"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidayum Theriyatha Puthiranom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidayum Theriyatha Puthiranom"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppo Vilagum Marmam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppo Vilagum Marmam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sol Sol Sol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sol Sol Sol"/>
</div>
<div class="lyrico-lyrics-wrapper">Varathukulle Saabham Olinjirukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varathukulle Saabham Olinjirukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Theriyalenna Thuyaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theriyalenna Thuyaram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Giri Giri Giri Giri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Giri Giri Giri Giri"/>
</div>
<div class="lyrico-lyrics-wrapper">Giri Giri Giri Giri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Giri Giri Giri Giri"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Kaasu Thaane Rendu Sideum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Kaasu Thaane Rendu Sideum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sundivitta Theriyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sundivitta Theriyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Virumbiyathu Poove Sandhegam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Virumbiyathu Poove Sandhegam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhunthidumo Thalaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhunthidumo Thalaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vettri Poovizha Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vettri Poovizha Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasa Pattu Naan Thudichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasa Pattu Naan Thudichen"/>
</div>
<div class="lyrico-lyrics-wrapper">Aiaiaiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiaiaiyo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tension Aagi Naan Kanna Mooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tension Aagi Naan Kanna Mooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Sound Ketu Na Mulichi Paakuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sound Ketu Na Mulichi Paakuren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Suzhalura Kaasula Thala Mattum Theriyuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suzhalura Kaasula Thala Mattum Theriyuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Progress Enna Da Loss
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Progress Enna Da Loss"/>
</div>
<div class="lyrico-lyrics-wrapper">Nijamum Nizhalum Ore Thamaashu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijamum Nizhalum Ore Thamaashu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Minus Enna Da Plus
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Minus Enna Da Plus"/>
</div>
<div class="lyrico-lyrics-wrapper">Life-eh Kaasu Maasu Dhoosu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life-eh Kaasu Maasu Dhoosu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">White Blackishu Black Whitishu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="White Blackishu Black Whitishu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nijamum Nizhalum Ore Thamaashu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijamum Nizhalum Ore Thamaashu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Minus Enna Da Plus
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Minus Enna Da Plus"/>
</div>
<div class="lyrico-lyrics-wrapper">Life-eh Kaasu Maasu Dhoosu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life-eh Kaasu Maasu Dhoosu"/>
</div>
</pre>
