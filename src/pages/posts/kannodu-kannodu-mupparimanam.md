---
title: "kannodu kannodu song lyrics"
album: "Mupparimanam"
artist: "G V Prakash Kumar"
lyricist: "Kabilan"
director: "Adhiroopan"
path: "/albums/mupparimanam-lyrics"
song: "Kannodu Kannodu"
image: ../../images/albumart/mupparimanam.jpg
date: 2017-03-03
lang: tamil
youtubeLink: "https://www.youtube.com/embed/a-3XF2Ti69E"
type: "love"
singers:
  -	Sathyaprakash
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kannodu kannodu kalandhuvitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannodu kannodu kalandhuvitten"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnodu unnodu tholaindhuvitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnodu unnodu tholaindhuvitten"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinnodu vinnodu parandhuvitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinnodu vinnodu parandhuvitten"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhan kannodu ottikonda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan kannodu ottikonda"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal kaatchiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal kaatchiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee dhaanae ohoo vooh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee dhaanae ohoo vooh"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen thedal ohoo vooh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen thedal ohoo vooh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un aasai un paarvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un aasai un paarvai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un menii un serkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un menii un serkai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un konjal naan parthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un konjal naan parthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedho aanenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedho aanenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai vittal nenjukullae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai vittal nenjukullae"/>
</div>
<div class="lyrico-lyrics-wrapper">Onnum thonamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnum thonamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithanai naal kaathirundhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithanai naal kaathirundhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai kaanamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai kaanamal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thulli thulli odukindra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thulli thulli odukindra"/>
</div>
<div class="lyrico-lyrics-wrapper">Pillai kandenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pillai kandenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thundil kannal aiyoo indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thundil kannal aiyoo indru"/>
</div>
<div class="lyrico-lyrics-wrapper">Maati kondenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maati kondenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vanna vennila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanna vennila"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhu ponadhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhu ponadhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thookam kannilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thookam kannilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu thooram ponadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu thooram ponadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaneer sirppamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaneer sirppamai"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai kandenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai kandenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalli nindrae thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalli nindrae thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan dhagham kondenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan dhagham kondenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un aasai un paarvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un aasai un paarvai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un menii un serkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un menii un serkai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un konjal naan parthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un konjal naan parthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedho aanenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedho aanenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Palli kudathin udaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palli kudathin udaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai kandenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai kandenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal paadam kangalaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal paadam kangalaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Katru kondenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katru kondenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ottrai kaalil pandi aadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ottrai kaalil pandi aadum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattam boochiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattam boochiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai endrum vazha vaikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai endrum vazha vaikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan moochiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan moochiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saavi illamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saavi illamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadum bommai nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadum bommai nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalli chendralum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalli chendralum"/>
</div>
<div class="lyrico-lyrics-wrapper">En thaayin vennmai nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En thaayin vennmai nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mottai maadi mel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mottai maadi mel"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanna vennila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanna vennila"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai kandaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai kandaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanai vittu odaadho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanai vittu odaadho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En veetu jannalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En veetu jannalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kaatru pattalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kaatru pattalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thee kuda thithikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thee kuda thithikum"/>
</div>
<div class="lyrico-lyrics-wrapper">Poovae unnalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovae unnalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannodu kannodu kalandhuvitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannodu kannodu kalandhuvitten"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnodu unnodu tholaindhuvitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnodu unnodu tholaindhuvitten"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinnodu vinnodu parandhuvitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinnodu vinnodu parandhuvitten"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhan kannodu ottikonda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan kannodu ottikonda"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal kaatchiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal kaatchiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee dhaanae ohoo vooh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee dhaanae ohoo vooh"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen thedal ohoo vooh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen thedal ohoo vooh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un aasai un paarvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un aasai un paarvai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un menii un serkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un menii un serkai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un konjal naan parthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un konjal naan parthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedho aanenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedho aanenae"/>
</div>
</pre>
