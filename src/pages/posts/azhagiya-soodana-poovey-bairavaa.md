---
title: "azhagiya soodana poovey song lyrics"
album: "Bairavaa"
artist: "Santhosh Narayanan"
lyricist: "Vairamuthu"
director: "Bharathan"
path: "/albums/bairavaa-lyrics"
song: "Azhagiya Soodana Poovey"
image: ../../images/albumart/bairavaa.jpg
date: 2017-01-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/gcUNHK5CeZY"
type: "love"
singers:
  - Vijaynarain
  - Darshana KT
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Poo poo punnagaiyaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poo poo punnagaiyaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee protein tharuvaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee protein tharuvaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa vaa varthaigalaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa vaa varthaigalaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee vitamin tharuvaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee vitamin tharuvaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeyo saadhanai chelvan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyo saadhanai chelvan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pennin kangalin kalvan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pennin kangalin kalvan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kadhalil komban
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kadhalil komban"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanal kadavulin nanban
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanal kadavulin nanban"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Undhan vegamae kanden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan vegamae kanden"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyo minnalin pillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyo minnalin pillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada minnalai vetta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada minnalai vetta"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru vettu kathi illaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru vettu kathi illaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhagiya soodana poovae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagiya soodana poovae"/>
</div>
<div class="lyrico-lyrics-wrapper">En sonthamaana theevae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En sonthamaana theevae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai paartha pothae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai paartha pothae"/>
</div>
<div class="lyrico-lyrics-wrapper">En uyaram koodi ponen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En uyaram koodi ponen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhuvarai kaanadha pennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhuvarai kaanadha pennae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilakiya kavidhai kaattum kannae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilakiya kavidhai kaattum kannae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai thol meedhu yetri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai thol meedhu yetri"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu koal kondu serpen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu koal kondu serpen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thoda thoonduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoda thoonduthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoonduthae nilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoonduthae nilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohh unai theendinaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohh unai theendinaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Yenadi thada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenadi thada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En nenjilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En nenjilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Muttuthae kida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muttuthae kida"/>
</div>
<div class="lyrico-lyrics-wrapper">En achamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En achamum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanamum vida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanamum vida"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vellai pon meniyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vellai pon meniyai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kollai kolla pogiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kollai kolla pogiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Mella poi theendinaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mella poi theendinaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanae kollai pogiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanae kollai pogiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Munnae nee vandathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munnae nee vandathum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudhugu thandil mazhaiyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhugu thandil mazhaiyada"/>
</div>
<div class="lyrico-lyrics-wrapper">Inba thalaiva idai thoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inba thalaiva idai thoda"/>
</div>
<div class="lyrico-lyrics-wrapper">Idaiveli yen un
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idaiveli yen un"/>
</div>
<div class="lyrico-lyrics-wrapper">Anaipinil narambugal noranguttum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anaipinil narambugal noranguttum"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagiya soodana poovae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagiya soodana poovae"/>
</div>
<div class="lyrico-lyrics-wrapper">En sonthamaana theevae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En sonthamaana theevae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai paartha pothae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai paartha pothae"/>
</div>
<div class="lyrico-lyrics-wrapper">En uyaram koodi ponen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En uyaram koodi ponen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhuvarai kaanadha pennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhuvarai kaanadha pennae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilakiya kavidhai kaattum kannae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilakiya kavidhai kaattum kannae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai thol meedhu yetri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai thol meedhu yetri"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu koal kondu serpen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu koal kondu serpen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enai mathalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai mathalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Mathalam adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mathalam adi"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhal puthagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhal puthagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Puthagam padi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthagam padi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oo vizhum muthamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo vizhum muthamo"/>
</div>
<div class="lyrico-lyrics-wrapper">Muthamo idi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthamo idi"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhai mothamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai mothamai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaangumo kodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaangumo kodi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naattukul vanmurai vendam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naattukul vanmurai vendam"/>
</div>
<div class="lyrico-lyrics-wrapper">Enbathu unmayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enbathu unmayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattil mel vanmurai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattil mel vanmurai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vendum enbathu penmaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vendum enbathu penmaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Imbam poloru thumbama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imbam poloru thumbama"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunbam poloru inbama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunbam poloru inbama"/>
</div>
<div class="lyrico-lyrics-wrapper">Yezhel piraviyin sugangalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yezhel piraviyin sugangalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Indrae vazhangidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indrae vazhangidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirai thatti thatti thirandidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirai thatti thatti thirandidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhagiya soodana poovae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagiya soodana poovae"/>
</div>
<div class="lyrico-lyrics-wrapper">En sonthamaana theevae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En sonthamaana theevae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai paartha pothae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai paartha pothae"/>
</div>
<div class="lyrico-lyrics-wrapper">En uyaram koodi ponen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En uyaram koodi ponen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhuvarai kaanadha pennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhuvarai kaanadha pennae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilakiya kavidhai kaattum kannae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilakiya kavidhai kaattum kannae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai thol meedhu yetri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai thol meedhu yetri"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu koal kondu serpen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu koal kondu serpen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhuvarai kaanadha pennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhuvarai kaanadha pennae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilakiya kavidhai kaattum kannae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilakiya kavidhai kaattum kannae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai thol meedhu yetri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai thol meedhu yetri"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu koal kondu serpen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu koal kondu serpen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En kannae Azhagiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kannae Azhagiya"/>
</div>
<div class="lyrico-lyrics-wrapper">En kannae Azhagiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kannae Azhagiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagiyaazhagiyaazhagiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagiyaazhagiyaazhagiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Thol meedhu yetri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thol meedhu yetri"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu koal kondu serpen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu koal kondu serpen"/>
</div>
</pre>
