---
title: "the karma theme song lyrics"
album: "U Turn"
artist: "Poornachandra Tejaswi - Anirudh Ravichander"
lyricist: "Subu"
director: "Pawan Kumar"
path: "/albums/u-turn-lyrics"
song: "The Karma Theme"
image: ../../images/albumart/u-turn.jpg
date: 2018-09-13
lang: tamil
youtubeLink: "https://www.youtube.com/embed/pnWvZCAvv9Y"
type: "theme"
singers:
  - Anirudh Ravichander
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Thisai thirumbi naalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thisai thirumbi naalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadal malai kadanthu ponaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadal malai kadanthu ponaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam thirunthi naalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam thirunthi naalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhi ranam maranthu ponaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi ranam maranthu ponaalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sagaaa aaaaaa aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sagaaa aaaaaa aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam thooram 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam thooram "/>
</div>
<div class="lyrico-lyrics-wrapper">engae sendraalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engae sendraalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sagaaa aaa aaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sagaaa aaa aaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinai unai thodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinai unai thodum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un sirippukku vithai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un sirippukku vithai"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethan"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kanneerikkum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kanneerikkum "/>
</div>
<div class="lyrico-lyrics-wrapper">ore vinai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ore vinai"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ambu vitta 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ambu vitta "/>
</div>
<div class="lyrico-lyrics-wrapper">velanneethan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="velanneethan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ambai thaanga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ambai thaanga "/>
</div>
<div class="lyrico-lyrics-wrapper">pogum nenjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pogum nenjam"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manam thirunthi naalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam thirunthi naalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanaa kalaintha pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanaa kalaintha pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">Yetho vinaa enai panthadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yetho vinaa enai panthadum"/>
</div>
<div class="lyrico-lyrics-wrapper">Yevan varaintha maayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yevan varaintha maayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ival ivan verum athyaiyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ival ivan verum athyaiyam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sagaaa aaaaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sagaaa aaaaaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vegam odum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vegam odum "/>
</div>
<div class="lyrico-lyrics-wrapper">kaalam theeratha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaalam theeratha"/>
</div>
<div class="lyrico-lyrics-wrapper">Sagaaa aa aaaa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sagaaa aa aaaa aaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellai illaathathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellai illaathathaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedi chellum kangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedi chellum kangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannil kaanum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannil kaanum "/>
</div>
<div class="lyrico-lyrics-wrapper">bimbam yethum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bimbam yethum"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethan"/>
</div>
<div class="lyrico-lyrics-wrapper">Bimbam kaattum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bimbam kaattum "/>
</div>
<div class="lyrico-lyrics-wrapper">paathai neethan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paathai neethan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pathai sendru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathai sendru "/>
</div>
<div class="lyrico-lyrics-wrapper">serum idam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="serum idam"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yevan varaintha maayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yevan varaintha maayam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeyae vithai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyae vithai"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyae mazhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyae mazhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Maram nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maram nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyae kizhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyae kizhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyae kani marupadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyae kani marupadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeyae vithai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyae vithai"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyae mazhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyae mazhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Maram nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maram nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyae kizhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyae kizhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyae kani marupadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyae kani marupadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sagaaaaaaaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sagaaaaaaaaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam thooram 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam thooram "/>
</div>
<div class="lyrico-lyrics-wrapper">engae sendraalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engae sendraalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sagaaa aaaaaa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sagaaa aaaaaa aaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinai unai thodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinai unai thodum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manam thirunthi naalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam thirunthi naalum"/>
</div>
</pre>
