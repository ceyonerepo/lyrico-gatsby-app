---
title: "edo jarige song lyrics"
album: "Needi Naadi Oke Katha"
artist: "Suresh Bobbili"
lyricist: "Kandikonda"
director: "Venu Udugula"
path: "/albums/needi-naadi-oke-katha-lyrics"
song: "Edo Jarige"
image: ../../images/albumart/needi-naadi-oke-katha.jpg
date: 2018-03-23
lang: telugu
youtubeLink: "https://www.youtube.com/embed/svwt03pMhh8"
type: "love"
singers:
  -	Chinmayi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Edo jarige edo jarige
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edo jarige edo jarige"/>
</div>
<div class="lyrico-lyrics-wrapper">Edo teliyanidi jarugutonde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edo teliyanidi jarugutonde"/>
</div>
<div class="lyrico-lyrics-wrapper">Ento kalige ento kalige
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ento kalige ento kalige"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppudoo eruganidi kalugutonde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppudoo eruganidi kalugutonde"/>
</div>
<div class="lyrico-lyrics-wrapper">Mande endallo chali vestondee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mande endallo chali vestondee"/>
</div>
<div class="lyrico-lyrics-wrapper">Challani chalilonaa chematadutonde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Challani chalilonaa chematadutonde"/>
</div>
<div class="lyrico-lyrics-wrapper">Madilo o varsham modalayyinde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madilo o varsham modalayyinde"/>
</div>
<div class="lyrico-lyrics-wrapper">Praanam poyettunde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Praanam poyettunde"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tiyyani gaayam chesenu praayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tiyyani gaayam chesenu praayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Biggaragaa nannu bigisina pranayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Biggaragaa nannu bigisina pranayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Entee moham valapula taapam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entee moham valapula taapam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandram taagina teerani daaham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandram taagina teerani daaham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edo jarige edo jarige
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edo jarige edo jarige"/>
</div>
<div class="lyrico-lyrics-wrapper">Edo teliyanidi jarugutonde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edo teliyanidi jarugutonde"/>
</div>
<div class="lyrico-lyrics-wrapper">Ento kalige ento kalige
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ento kalige ento kalige"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppudoo eruganidi kalugutonde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppudoo eruganidi kalugutonde"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalale chere kanulaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalale chere kanulaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Pakshula gumpulugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakshula gumpulugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Alalai nannu munche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alalai nannu munche"/>
</div>
<div class="lyrico-lyrics-wrapper">Lakshala oohalilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lakshala oohalilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gunde virahamto mandutuvunde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gunde virahamto mandutuvunde"/>
</div>
<div class="lyrico-lyrics-wrapper">Tanuvemo o todu korutuvunde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tanuvemo o todu korutuvunde"/>
</div>
<div class="lyrico-lyrics-wrapper">Atade kaavalantoo adugutuvunde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atade kaavalantoo adugutuvunde"/>
</div>
<div class="lyrico-lyrics-wrapper">Hrudayam ee roje
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hrudayam ee roje"/>
</div>
<div class="lyrico-lyrics-wrapper">Emiti chitram okate aatram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emiti chitram okate aatram"/>
</div>
<div class="lyrico-lyrics-wrapper">Naato naakayyenu chilipiga yuddham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naato naakayyenu chilipiga yuddham"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnati saantam ayyenu antam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnati saantam ayyenu antam"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalo regenu chiru bhookampam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalo regenu chiru bhookampam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edo jarige edo jarige
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edo jarige edo jarige"/>
</div>
<div class="lyrico-lyrics-wrapper">Edo teliyanidi jarugutonde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edo teliyanidi jarugutonde"/>
</div>
<div class="lyrico-lyrics-wrapper">Ento kalige ento kalige
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ento kalige ento kalige"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppudoo eruganidi kalugutonde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppudoo eruganidi kalugutonde"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kallolam aanandam okatai egasinde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallolam aanandam okatai egasinde"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanneeru panneeroo varadai munchinde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneeru panneeroo varadai munchinde"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa deham naadasalu kaanattunde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa deham naadasalu kaanattunde"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnalle ee roju lenattunde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnalle ee roju lenattunde"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenasalu nenenaa anipistonde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenasalu nenenaa anipistonde"/>
</div>
<div class="lyrico-lyrics-wrapper">Maikam kammesinde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maikam kammesinde"/>
</div>
<div class="lyrico-lyrics-wrapper">Nimisham nimisham tiyyani narakam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nimisham nimisham tiyyani narakam"/>
</div>
<div class="lyrico-lyrics-wrapper">Baadhalo choostunna nootana swargam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baadhalo choostunna nootana swargam"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhuram madhuram marigenu rudhiram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhuram madhuram marigenu rudhiram"/>
</div>
<div class="lyrico-lyrics-wrapper">Sannaga onikenu yerrani adharam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sannaga onikenu yerrani adharam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edo jarige edo jarige
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edo jarige edo jarige"/>
</div>
<div class="lyrico-lyrics-wrapper">Edo teliyanidi jarugutonde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edo teliyanidi jarugutonde"/>
</div>
<div class="lyrico-lyrics-wrapper">Ento kalige ento kalige
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ento kalige ento kalige"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppudoo eruganidi kalugutonde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppudoo eruganidi kalugutonde"/>
</div>
</pre>
