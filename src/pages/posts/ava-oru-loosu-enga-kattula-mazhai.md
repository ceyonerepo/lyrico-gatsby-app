---
title: "ava oru loosu song lyrics"
album: "Enga Kattula Mazhai"
artist: "Srivijay"
lyricist: "Bramma"
director: "Sri Balaji"
path: "/albums/enga-kattula-mazhai-lyrics"
song: "Ava Oru Loosu"
image: ../../images/albumart/enga-kattula-mazhai.jpg
date: 2018-08-03
lang: tamil
youtubeLink: "https://www.youtube.com/embed/4q1-7yEXQsI"
type: "happy"
singers:
  - Gana Bala
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ava Oru Looseu Mattina Closeu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Oru Looseu Mattina Closeu "/>
</div>
<div class="lyrico-lyrics-wrapper">Veena poidum Namma lifeu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veena poidum Namma lifeu "/>
</div>
<div class="lyrico-lyrics-wrapper">vickuna thanni chikuna janni 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vickuna thanni chikuna janni "/>
</div>
<div class="lyrico-lyrics-wrapper">sachidum aala uthidum paala 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sachidum aala uthidum paala "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kokuku pola otha kaalil ninuputa da 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kokuku pola otha kaalil ninuputa da "/>
</div>
<div class="lyrico-lyrics-wrapper">ava kanda meena kadachavudana parathuputa da 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ava kanda meena kadachavudana parathuputa da "/>
</div>
<div class="lyrico-lyrics-wrapper">kadhaliya muja vechee appu adicha da 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhaliya muja vechee appu adicha da "/>
</div>
<div class="lyrico-lyrics-wrapper">appaviya velai solli vettu vecha da 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="appaviya velai solli vettu vecha da "/>
</div>
<div class="lyrico-lyrics-wrapper">anju kula naatu vediye patha vechan da 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anju kula naatu vediye patha vechan da "/>
</div>
<div class="lyrico-lyrics-wrapper">en vetti kula onanathan vittuputen da 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en vetti kula onanathan vittuputen da "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ava Oru Looseu Mattina Closeu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Oru Looseu Mattina Closeu "/>
</div>
<div class="lyrico-lyrics-wrapper">Veena poidum Namma lifeu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veena poidum Namma lifeu "/>
</div>
<div class="lyrico-lyrics-wrapper">vickuna thanni chikuna janni 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vickuna thanni chikuna janni "/>
</div>
<div class="lyrico-lyrics-wrapper">sachidum aala uthidum paala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sachidum aala uthidum paala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nuulu kuda illama patam viduva 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuulu kuda illama patam viduva "/>
</div>
<div class="lyrico-lyrics-wrapper">suthi kuda illama aani adipa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suthi kuda illama aani adipa "/>
</div>
<div class="lyrico-lyrics-wrapper">aama da aama da aama da 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aama da aama da aama da "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kathi mela kala vechan kezhichurichu da 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathi mela kala vechan kezhichurichu da "/>
</div>
<div class="lyrico-lyrics-wrapper">en bhuthikula avala vechan valichurichu da 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en bhuthikula avala vechan valichurichu da "/>
</div>
<div class="lyrico-lyrics-wrapper">kadhal sollaama kaka vechuta 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhal sollaama kaka vechuta "/>
</div>
<div class="lyrico-lyrics-wrapper">avala nambithan necham karukipochu da 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avala nambithan necham karukipochu da "/>
</div>
<div class="lyrico-lyrics-wrapper">aama 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aama "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kadhal seiyama ponnu irutha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhal seiyama ponnu irutha "/>
</div>
<div class="lyrico-lyrics-wrapper">ava kaala thotuthan dhinam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ava kaala thotuthan dhinam "/>
</div>
<div class="lyrico-lyrics-wrapper">pooja seyiven da 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pooja seyiven da "/>
</div>
<div class="lyrico-lyrics-wrapper">antha sami kannathan thorakuma thorakuma 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antha sami kannathan thorakuma thorakuma "/>
</div>
<div class="lyrico-lyrics-wrapper">intha kadhal tholviyathan thadukuma thadukuma 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha kadhal tholviyathan thadukuma thadukuma "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pudi da saaba iraiva thadi yeppa da 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pudi da saaba iraiva thadi yeppa da "/>
</div>
<div class="lyrico-lyrics-wrapper">roadla nipa da aama da aama da 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="roadla nipa da aama da aama da "/>
</div>
<div class="lyrico-lyrics-wrapper">sathiyama nipa da 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sathiyama nipa da "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ava Oru Looseu Mattina Closeu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Oru Looseu Mattina Closeu "/>
</div>
<div class="lyrico-lyrics-wrapper">Veena poidum Namma lifeu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veena poidum Namma lifeu "/>
</div>
<div class="lyrico-lyrics-wrapper">vickuna thanni chikuna janni 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vickuna thanni chikuna janni "/>
</div>
<div class="lyrico-lyrics-wrapper">sachidum aala uthidum paala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sachidum aala uthidum paala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aazham pathu kaala vitten 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aazham pathu kaala vitten "/>
</div>
<div class="lyrico-lyrics-wrapper">saakadaila kadhal pannum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saakadaila kadhal pannum "/>
</div>
<div class="lyrico-lyrics-wrapper">ellarumey poo kadaila aama da 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellarumey poo kadaila aama da "/>
</div>
<div class="lyrico-lyrics-wrapper">aama da aama da aama da 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aama da aama da aama da "/>
</div>
<div class="lyrico-lyrics-wrapper">veru kadalai pola aalu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veru kadalai pola aalu "/>
</div>
<div class="lyrico-lyrics-wrapper">thinuduva da paarukadala pola 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thinuduva da paarukadala pola "/>
</div>
<div class="lyrico-lyrics-wrapper">nammala kadachuduva da 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nammala kadachuduva da "/>
</div>
<div class="lyrico-lyrics-wrapper">kanni thivuthan ponnu manasu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanni thivuthan ponnu manasu "/>
</div>
<div class="lyrico-lyrics-wrapper">romba neelamanathu mudivu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="romba neelamanathu mudivu "/>
</div>
<div class="lyrico-lyrics-wrapper">sogamanathu sollikolama odipuduva 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sogamanathu sollikolama odipuduva "/>
</div>
<div class="lyrico-lyrics-wrapper">jallikallatum nenja norikipoduva 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jallikallatum nenja norikipoduva "/>
</div>
<div class="lyrico-lyrics-wrapper">pulivaala pudichu kuda paakalaam paakalaam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pulivaala pudichu kuda paakalaam paakalaam "/>
</div>
<div class="lyrico-lyrics-wrapper">ada vazhuva saavanu therichudum therichudum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada vazhuva saavanu therichudum therichudum "/>
</div>
<div class="lyrico-lyrics-wrapper">ithuga manasu puriyala solla 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithuga manasu puriyala solla "/>
</div>
<div class="lyrico-lyrics-wrapper">theriyala mella mudiyala aama da aama da 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theriyala mella mudiyala aama da aama da "/>
</div>
<div class="lyrico-lyrics-wrapper">onnumey puriyala 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="onnumey puriyala "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ava Oru Looseu Mattina Closeu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Oru Looseu Mattina Closeu "/>
</div>
<div class="lyrico-lyrics-wrapper">Veena poidum Namma lifeu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veena poidum Namma lifeu "/>
</div>
<div class="lyrico-lyrics-wrapper">vickuna thanni chikuna janni 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vickuna thanni chikuna janni "/>
</div>
<div class="lyrico-lyrics-wrapper">sachidum aala uthidum paala 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sachidum aala uthidum paala "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">venan da kadhal venan da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="venan da kadhal venan da"/>
</div>
<div class="lyrico-lyrics-wrapper">venan da kadhal venan da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="venan da kadhal venan da"/>
</div>
<div class="lyrico-lyrics-wrapper">venan da kadhal venan da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="venan da kadhal venan da"/>
</div>
<div class="lyrico-lyrics-wrapper">venan da kadhal venan da 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="venan da kadhal venan da "/>
</div>
</pre>
