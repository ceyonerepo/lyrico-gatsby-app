---
title: "vaanaville vaanaville song lyrics"
album: "Ramanaa"
artist: "Ilaiyaraja"
lyricist: "Pazhani Barathi - M Metha"
director: "A.R. Murugadoss"
path: "/albums/ramanaa-lyrics"
song: "Vaanaville Vaanaville"
image: ../../images/albumart/ramanaa.jpg
date: 2002-11-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/uIuUy06DBiM"
type: "happy"
singers:
  - Hariharan
  - Sadhana Sargam
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vaanaville Vaanaville
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanaville Vaanaville"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthathenna Ippodhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthathenna Ippodhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Alli Vantha Vannangalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alli Vantha Vannangalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Engal Nenjil Nee Thoovu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engal Nenjil Nee Thoovu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chinna Paravaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna Paravaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Konji Parakuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konji Parakuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattu Siragile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattu Siragile"/>
</div>
<div class="lyrico-lyrics-wrapper">Pani Thelikuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pani Thelikuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Thaai Thendrale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Thaai Thendrale"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthu Nee Paadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthu Nee Paadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaraaroooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraaroooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanaville Vaanaville
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanaville Vaanaville"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthathenna Ippodhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthathenna Ippodhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Entha Naattu Kuyilin Koottamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entha Naattu Kuyilin Koottamum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadum Paadal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadum Paadal"/>
</div>
<div class="lyrico-lyrics-wrapper">Entha Naattu Kiligal Pechilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entha Naattu Kiligal Pechilum"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjum Malalai Undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjum Malalai Undu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jaadhi Enna Kettu Vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaadhi Enna Kettu Vittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thendral Nammai Thodumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thendral Nammai Thodumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhesam Ethu Paarthuvittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhesam Ethu Paarthuvittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannil Malai Varumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannil Malai Varumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnodu Naanum Elloorum Oar Sontham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnodu Naanum Elloorum Oar Sontham"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbulla Ullathile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbulla Ullathile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanaville Vaanaville
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanaville Vaanaville"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthathenna Ippodhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthathenna Ippodhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Alli Vantha Vannangalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alli Vantha Vannangalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Engal Nenjil Nee Thoovu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engal Nenjil Nee Thoovu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engirundhu Sondham Vandhadhoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engirundhu Sondham Vandhadhoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjam Vedanthaangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjam Vedanthaangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha Kootil Naanum Vaazhave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Kootil Naanum Vaazhave"/>
</div>
<div class="lyrico-lyrics-wrapper">Ketkka Vendum Neengal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketkka Vendum Neengal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaai Paravai Seigalukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaai Paravai Seigalukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Ootugindra Uravu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ootugindra Uravu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhil Thaane Vaazhgiradhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhil Thaane Vaazhgiradhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirgalin Azhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirgalin Azhagu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnodu Naanum Elloorum Oar Sontham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnodu Naanum Elloorum Oar Sontham"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbulla Ullathile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbulla Ullathile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanaville Vaanaville
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanaville Vaanaville"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthathenna Ippodhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthathenna Ippodhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Alli Vantha Vannangalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alli Vantha Vannangalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Engal Nenjil Nee Thoovu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engal Nenjil Nee Thoovu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chinna Paravaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna Paravaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Konji Parakuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konji Parakuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattu Siragile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattu Siragile"/>
</div>
<div class="lyrico-lyrics-wrapper">Pani Thelikuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pani Thelikuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Thaai Thendrale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Thaai Thendrale"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthu Nee Paadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthu Nee Paadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaraaroooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraaroooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanaville Vaanaville
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanaville Vaanaville"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthathenna Ippodhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthathenna Ippodhu"/>
</div>
</pre>
