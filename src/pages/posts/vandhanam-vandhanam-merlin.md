---
title: "vandhanam vandhanam song lyrics"
album: "Merlin"
artist: "Ganesh Raghavendra"
lyricist: "Ku. Karthick"
director: "Keera"
path: "/albums/merlin-song-lyrics"
song: "Vandhanam Vandhanam"
image: ../../images/albumart/merlin.jpg
date: 2018-02-23
lang: tamil
youtubeLink: "https://www.youtube.com/embed/jsqZjcJjI4Q"
type: "gana"
singers:
  - Marana Gana Viji
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">vella puravum setharkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vella puravum setharkum"/>
</div>
<div class="lyrico-lyrics-wrapper">vella pura jaburkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vella pura jaburkam"/>
</div>
<div class="lyrico-lyrics-wrapper">athu adikum paru karanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu adikum paru karanam"/>
</div>
<div class="lyrico-lyrics-wrapper">intha gana va kekurave 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha gana va kekurave "/>
</div>
<div class="lyrico-lyrics-wrapper">poda venum saranam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poda venum saranam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">hey vandhanam vandhanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey vandhanam vandhanam"/>
</div>
<div class="lyrico-lyrics-wrapper">vandhanam ma vandhanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vandhanam ma vandhanam"/>
</div>
<div class="lyrico-lyrics-wrapper">kundhanum kundhanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kundhanum kundhanum"/>
</div>
<div class="lyrico-lyrics-wrapper">vandha janam kundhanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vandha janam kundhanum"/>
</div>
<div class="lyrico-lyrics-wrapper">ettu thisaiyilayum padunavan da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ettu thisaiyilayum padunavan da"/>
</div>
<div class="lyrico-lyrics-wrapper">gana viji kita katathe banda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gana viji kita katathe banda"/>
</div>
<div class="lyrico-lyrics-wrapper">ettu thisaiyilayum padunavan da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ettu thisaiyilayum padunavan da"/>
</div>
<div class="lyrico-lyrics-wrapper">gana viji kita katathe banda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gana viji kita katathe banda"/>
</div>
<div class="lyrico-lyrics-wrapper">kadala paru kadala paru da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadala paru kadala paru da"/>
</div>
<div class="lyrico-lyrics-wrapper">antha kadalu mela kapalu paru da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antha kadalu mela kapalu paru da"/>
</div>
<div class="lyrico-lyrics-wrapper">dolu dolu digiree dolu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dolu dolu digiree dolu da"/>
</div>
<div class="lyrico-lyrics-wrapper">viji gana padum metta kelu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viji gana padum metta kelu da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">hey vandhanam vandhanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey vandhanam vandhanam"/>
</div>
<div class="lyrico-lyrics-wrapper">vandhanam ma vandhanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vandhanam ma vandhanam"/>
</div>
<div class="lyrico-lyrics-wrapper">kundhanum kundhanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kundhanum kundhanum"/>
</div>
<div class="lyrico-lyrics-wrapper">vandha janam kundhanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vandha janam kundhanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">alagu ponnu da colour kannu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alagu ponnu da colour kannu da"/>
</div>
<div class="lyrico-lyrics-wrapper">paiyan vaaikiran poya thaikiran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paiyan vaaikiran poya thaikiran"/>
</div>
<div class="lyrico-lyrics-wrapper">appa pecha kekamune katikinu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="appa pecha kekamune katikinu"/>
</div>
<div class="lyrico-lyrics-wrapper">oru packet neraya manna vari kotikinu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru packet neraya manna vari kotikinu"/>
</div>
<div class="lyrico-lyrics-wrapper">anbana ponnu da pasamana kannu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anbana ponnu da pasamana kannu da"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjula nesam na pesatha mosan ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjula nesam na pesatha mosan ah"/>
</div>
<div class="lyrico-lyrics-wrapper">amma pola kathiduva ponnu ava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="amma pola kathiduva ponnu ava"/>
</div>
<div class="lyrico-lyrics-wrapper">vidha vidhama aaki vaipa thinnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidha vidhama aaki vaipa thinnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kadala paru kadala paru da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadala paru kadala paru da"/>
</div>
<div class="lyrico-lyrics-wrapper">antha kadalu mela kapalu paru da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antha kadalu mela kapalu paru da"/>
</div>
<div class="lyrico-lyrics-wrapper">dolu dolu digiree dolu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dolu dolu digiree dolu da"/>
</div>
<div class="lyrico-lyrics-wrapper">viji gana padum metta kelu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viji gana padum metta kelu da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">koovam karaiyila thondriya gana than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koovam karaiyila thondriya gana than"/>
</div>
<div class="lyrico-lyrics-wrapper">nanban james la karaiyum thena than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanban james la karaiyum thena than"/>
</div>
<div class="lyrico-lyrics-wrapper">chella thai pethu edutha pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chella thai pethu edutha pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">enna ingana vanthu gana pada solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna ingana vanthu gana pada solla"/>
</div>
<div class="lyrico-lyrics-wrapper">kathaya pesanum vasanam veesanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathaya pesanum vasanam veesanum"/>
</div>
<div class="lyrico-lyrics-wrapper">light ah pidikanum padatha edukanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="light ah pidikanum padatha edukanum"/>
</div>
<div class="lyrico-lyrics-wrapper">bioscope pota inthe cinema ne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bioscope pota inthe cinema ne"/>
</div>
<div class="lyrico-lyrics-wrapper">sokku kaati suthinu iruntha varuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sokku kaati suthinu iruntha varuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kadala paru kadala paru da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadala paru kadala paru da"/>
</div>
<div class="lyrico-lyrics-wrapper">antha kadalu mela kapalu paru da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antha kadalu mela kapalu paru da"/>
</div>
<div class="lyrico-lyrics-wrapper">dolu dolu digiree dolu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dolu dolu digiree dolu da"/>
</div>
<div class="lyrico-lyrics-wrapper">viji gana padum metta kelu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viji gana padum metta kelu da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ajili pulijili amma amsamana ponnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ajili pulijili amma amsamana ponnu"/>
</div>
<div class="lyrico-lyrics-wrapper">ne egiri egiri aatam podu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ne egiri egiri aatam podu "/>
</div>
<div class="lyrico-lyrics-wrapper">aaya kada bannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaya kada bannu"/>
</div>
<div class="lyrico-lyrics-wrapper">ne kaiya moodi moodikita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ne kaiya moodi moodikita"/>
</div>
<div class="lyrico-lyrics-wrapper">moodikum da kannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moodikum da kannu"/>
</div>
<div class="lyrico-lyrics-wrapper">dommako domaako 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dommako domaako "/>
</div>
<div class="lyrico-lyrics-wrapper">donkanaka domaako
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="donkanaka domaako"/>
</div>
<div class="lyrico-lyrics-wrapper">nan dolu adikira aalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan dolu adikira aalu"/>
</div>
<div class="lyrico-lyrics-wrapper">ne gana onnu kelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ne gana onnu kelu"/>
</div>
<div class="lyrico-lyrics-wrapper">ne count down pesina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ne count down pesina"/>
</div>
<div class="lyrico-lyrics-wrapper">pigini paru sevulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pigini paru sevulu"/>
</div>
<div class="lyrico-lyrics-wrapper">nan deal vitu epam vita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan deal vitu epam vita"/>
</div>
<div class="lyrico-lyrics-wrapper">arunthidum da vaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arunthidum da vaalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aiya vandhanam vandhanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aiya vandhanam vandhanam"/>
</div>
<div class="lyrico-lyrics-wrapper">vandhanam ma vandhanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vandhanam ma vandhanam"/>
</div>
</pre>
