---
title: "porkkalam song lyrics"
album: "Aadukalam"
artist: "G. V. Prakash Kumar"
lyricist: "Yugabharathi"
director: "Vetrimaran"
path: "/albums/aadukalam-lyrics"
song: "Porkkalam"
image: ../../images/albumart/aadukalam.jpg
date: 2011-01-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/N2d8zYPgk7o"
type: "mass"
singers:
  - Alwa Vasu
  - Yogi B
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vaazhkkai Oru Porkkalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkkai Oru Porkkalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vettaiyaadi Paarkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vettaiyaadi Paarkanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poraadi Velladaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraadi Velladaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Potti Potttu Kolladaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Potti Potttu Kolladaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adakuthalai Mudakkuthalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adakuthalai Mudakkuthalai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Verraruppom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verraruppom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uridhi Valaiyil Poorippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uridhi Valaiyil Poorippom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Patta Kathi Vaithidungal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Patta Kathi Vaithidungal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Po Po Po Ranagala Nodigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Po Po Po Ranagala Nodigal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edhilumey Tholvi Koodaathadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhilumey Tholvi Koodaathadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Emanaiyum Vetri Nee Kolladaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emanaiyum Vetri Nee Kolladaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saadhanaiyile Vedhanaigal Mudiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saadhanaiyile Vedhanaigal Mudiyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varum Thalaimurai En Peyar Aanduvidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varum Thalaimurai En Peyar Aanduvidum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Velvomey Veezhaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velvomey Veezhaamal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Velvomey Veezhaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velvomey Veezhaamal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poradiva Idhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poradiva Idhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aadukalamum Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadukalamum Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koondodu Karuvaruppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koondodu Karuvaruppen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Porin Mudivil Koothaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porin Mudivil Koothaadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vali Rusippen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vali Rusippen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pagai Mutrayil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagai Mutrayil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Yedhirinil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Yedhirinil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edhirigal Olipadavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhirigal Olipadavey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ini Aethumillai Vazhimurai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Aethumillai Vazhimurai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhithidavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhithidavey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Veeram Unnai Veraruththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Veeram Unnai Veraruththu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kolliveikkumey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolliveikkumey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thalaigal Sidharum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaigal Sidharum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu Pagaivanai Aruthidum Aruvadai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Pagaivanai Aruthidum Aruvadai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sinathaal Serukkai Thudai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sinathaal Serukkai Thudai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhisai Ettum Naam Serpom Kootamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhisai Ettum Naam Serpom Kootamey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Parandhodidum Vaattamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parandhodidum Vaattamey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhu Sariththiram Padaiththidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu Sariththiram Padaiththidum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karum Padai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karum Padai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Elundhaal Norungum Thadai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Elundhaal Norungum Thadai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Va Uyir Vettum Namakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Va Uyir Vettum Namakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kilaipum Maanamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kilaipum Maanamey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kai Koodidum Kaalamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai Koodidum Kaalamey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aadukalam Kai Koodidum Kaalamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadukalam Kai Koodidum Kaalamey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aadukalam Kai Koodidum Kaalamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadukalam Kai Koodidum Kaalamey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aadukalam Kai Koodidum Kaalamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadukalam Kai Koodidum Kaalamey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aadukalam Kai Koodidum Kaalamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadukalam Kai Koodidum Kaalamey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha Vizhavirkku Vandhu Sir
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Vizhavirkku Vandhu Sir"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sirappu Thandhamaikkaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirappu Thandhamaikkaaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ungal Anaivarukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ungal Anaivarukkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vettaikaaran Party Saarbaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vettaikaaran Party Saarbaaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engaludaiya Nenjaarndha Nandriyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engaludaiya Nenjaarndha Nandriyai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theriviththu Kolgirom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theriviththu Kolgirom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poraadinaal Naam Vellalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraadinaal Naam Vellalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaan Veedhiyil Kaal Veikkalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaan Veedhiyil Kaal Veikkalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Boologamey Paer Sollalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boologamey Paer Sollalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saagaamaley Naam Vaazhalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saagaamaley Naam Vaazhalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poraadinaal Naam Vellalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraadinaal Naam Vellalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaan Veedhiyil Kaal Veikkalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaan Veedhiyil Kaal Veikkalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bhoologamey Paer Sollalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhoologamey Paer Sollalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saagaamaley Naam Vaazhalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saagaamaley Naam Vaazhalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maayaval Muga Perumai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maayaval Muga Perumai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adaindhidum Manadhil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adaindhidum Manadhil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pudhiya Oli Paravum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhiya Oli Paravum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kavalai Parandhidumey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavalai Parandhidumey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vendren Ippodhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vendren Ippodhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Velagidu Nee Inimel Ennai Thodathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velagidu Nee Inimel Ennai Thodathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Goyyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Goyyala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Kaiyil Karisoru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Kaiyil Karisoru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Marukaiyil Tharamaana Beeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marukaiyil Tharamaana Beeru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karai Oram Thani Veedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karai Oram Thani Veedu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhaipesuven En Jodiyodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhaipesuven En Jodiyodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Aanaiyida Maaridumey Adada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Aanaiyida Maaridumey Adada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nadai Paadhaiyil Malar Thoovida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadai Paadhaiyil Malar Thoovida"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inai Yaar Ena Pugazh Paadidada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inai Yaar Ena Pugazh Paadidada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kai Kolladha Kaasadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai Kolladha Kaasadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varalaatril Vaithiduvom Thadamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varalaatril Vaithiduvom Thadamey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thayangaamal Yedhaiyum Tharuvom Naame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thayangaamal Yedhaiyum Tharuvom Naame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avaludan En Kaadhalai Paaradaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avaludan En Kaadhalai Paaradaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Nokhi Pen Sorgam Idhu Pothumada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Nokhi Pen Sorgam Idhu Pothumada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Podhumadaa Podhumadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhumadaa Podhumadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poraadinaal Naam Vellalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraadinaal Naam Vellalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaan Veedhiyil Kaal Veikkalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaan Veedhiyil Kaal Veikkalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Boologamey Paer Sollalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boologamey Paer Sollalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saagaamaley Naam Vaazhalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saagaamaley Naam Vaazhalaam"/>
</div>
</pre>
