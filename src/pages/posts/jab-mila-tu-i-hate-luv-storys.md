---
title: "jab mila tu song lyrics"
album: "I Hate Luv Storys"
artist: "Vishal Shekhar"
lyricist: "Anvita Dutt Guptan"
director: "Punit Malhotra"
path: "/albums/i-hate-luv-storys-lyrics"
song: "jab mila tu"
image: ../../images/albumart/i-hate-luv-storys.jpg
date: 2010-07-02
lang: hindi
youtubeLink: "https://www.youtube.com/embed/5mlAMz7DM2g"
type: "love"
singers:
  - Vishal Dadlani
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Jaise ghadi ki girah se waqt kahin pe gira ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaise ghadi ki girah se waqt kahin pe gira ho"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaise roshni subah se ho jaaye juda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaise roshni subah se ho jaaye juda"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaise gaane aur nazam se sur koi chura le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaise gaane aur nazam se sur koi chura le"/>
</div>
<div class="lyrico-lyrics-wrapper">Waise aadha adhoora main hoon tere bina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Waise aadha adhoora main hoon tere bina"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jab mila tu.. ru tu ru tu ru.. tu ru tu... o o..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jab mila tu.. ru tu ru tu ru.. tu ru tu... o o.."/>
</div>
<div class="lyrico-lyrics-wrapper">Na thi kami na justuzu ru tu ru tu.. o o..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na thi kami na justuzu ru tu ru tu.. o o.."/>
</div>
<div class="lyrico-lyrics-wrapper">Aur aadhe aadhe pal hue phir poore yun o o..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aur aadhe aadhe pal hue phir poore yun o o.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jab mila tu.. ru tu ru tu ru.. tu ru tu... o o..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jab mila tu.. ru tu ru tu ru.. tu ru tu... o o.."/>
</div>
<div class="lyrico-lyrics-wrapper">Khaali jo raat ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khaali jo raat ho"/>
</div>
<div class="lyrico-lyrics-wrapper">Main khwaab chaand taaron se woh poori bhar doon
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main khwaab chaand taaron se woh poori bhar doon"/>
</div>
<div class="lyrico-lyrics-wrapper">Teri jo baat ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teri jo baat ho"/>
</div>
<div class="lyrico-lyrics-wrapper">Min bin kahe hi aadhi pauni poori kar doon
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Min bin kahe hi aadhi pauni poori kar doon"/>
</div>
<div class="lyrico-lyrics-wrapper">Jo aadhe se hum hain, woh poore hon tumse
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jo aadhe se hum hain, woh poore hon tumse"/>
</div>
<div class="lyrico-lyrics-wrapper">Na jaane yeh sauda bhi kab tay hua
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na jaane yeh sauda bhi kab tay hua"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhoore se kisse, baraabar se hisse
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhoore se kisse, baraabar se hisse"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tu dil to main dua
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu dil to main dua"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jab mila tu.. ru tu ru tu ru.. tu ru tu... o o..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jab mila tu.. ru tu ru tu ru.. tu ru tu... o o.."/>
</div>
<div class="lyrico-lyrics-wrapper">Na thi kami na justuzu ru tu ru tu.. o o..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na thi kami na justuzu ru tu ru tu.. o o.."/>
</div>
<div class="lyrico-lyrics-wrapper">Aur aadhe aadhe pal hue phir poore yun o o..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aur aadhe aadhe pal hue phir poore yun o o.."/>
</div>
<div class="lyrico-lyrics-wrapper">Jab mila tu.. ru tu ru tu ru.. tu ru tu... o o..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jab mila tu.. ru tu ru tu ru.. tu ru tu... o o.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kal tu jo khoi ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kal tu jo khoi ho"/>
</div>
<div class="lyrico-lyrics-wrapper">To gumshuda yeh zindagi basar kar doon
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="To gumshuda yeh zindagi basar kar doon"/>
</div>
<div class="lyrico-lyrics-wrapper">Manzil jo soi ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manzil jo soi ho"/>
</div>
<div class="lyrico-lyrics-wrapper">To khwaab saare tere hi nazar kar doon
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="To khwaab saare tere hi nazar kar doon"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu aadhe se dil ko jo laayegi bhi to
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu aadhe se dil ko jo laayegi bhi to"/>
</div>
<div class="lyrico-lyrics-wrapper">Main sau khwaahishon se hi bhar dunga woh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main sau khwaahishon se hi bhar dunga woh"/>
</div>
<div class="lyrico-lyrics-wrapper">Kare na yakeen tu abhi ke abhi tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kare na yakeen tu abhi ke abhi tu"/>
</div>
<div class="lyrico-lyrics-wrapper">To aake aazama..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="To aake aazama.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jab mila tu.. ru tu ru tu ru.. tu ru tu... o o..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jab mila tu.. ru tu ru tu ru.. tu ru tu... o o.."/>
</div>
<div class="lyrico-lyrics-wrapper">Na thi kami na justuzu ru tu ru tu.. o o..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na thi kami na justuzu ru tu ru tu.. o o.."/>
</div>
<div class="lyrico-lyrics-wrapper">Aur aadhe aadhe pal hue phir poore yun o o..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aur aadhe aadhe pal hue phir poore yun o o.."/>
</div>
<div class="lyrico-lyrics-wrapper">Jab mila tu.. ru tu ru tu ru.. tu ru tu... o o..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jab mila tu.. ru tu ru tu ru.. tu ru tu... o o.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jaise ghadi ki girah se waqt kahin pe gira ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaise ghadi ki girah se waqt kahin pe gira ho"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaise roshni subah se ho jaaye juda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaise roshni subah se ho jaaye juda"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaise gaane aur nazam se sur koi chura le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaise gaane aur nazam se sur koi chura le"/>
</div>
<div class="lyrico-lyrics-wrapper">Waise aadha adhoora main hoon tere bina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Waise aadha adhoora main hoon tere bina"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jab mila tu.. ru tu ru tu ru.. tu ru tu... o o..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jab mila tu.. ru tu ru tu ru.. tu ru tu... o o.."/>
</div>
<div class="lyrico-lyrics-wrapper">Na thi kami na justuzu ru tu ru tu.. o o..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na thi kami na justuzu ru tu ru tu.. o o.."/>
</div>
<div class="lyrico-lyrics-wrapper">Aur aadhe aadhe pal hue phir poore yun o o..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aur aadhe aadhe pal hue phir poore yun o o.."/>
</div>
<div class="lyrico-lyrics-wrapper">Jab mila tu.. ru tu ru tu ru.. tu ru tu... o o..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jab mila tu.. ru tu ru tu ru.. tu ru tu... o o.."/>
</div>
<div class="lyrico-lyrics-wrapper">Jab mila tu!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jab mila tu!"/>
</div>
</pre>
