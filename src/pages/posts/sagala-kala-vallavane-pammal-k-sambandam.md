---
title: "sagala kala vallavane song lyrics"
album: "Pammal K Sambandam"
artist: "Deva"
lyricist: "Kabilan"
director: "Moulee"
path: "/albums/pammal-k-sambandam-lyrics"
song: "Sagala Kala Vallavane"
image: ../../images/albumart/pammal-k-sambandam.jpg
date: 2002-01-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Zr-i8YYhwjw"
type: "love"
singers:
  - Hariharan
  - Sujatha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Sagala Kala Vallavane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sagala Kala Vallavane"/>
</div>
<div class="lyrico-lyrics-wrapper">Salavai Seitha Chandhirane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Salavai Seitha Chandhirane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sagala Kala Vallavane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sagala Kala Vallavane"/>
</div>
<div class="lyrico-lyrics-wrapper">Salavai Seitha Chandhirane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Salavai Seitha Chandhirane"/>
</div>
<div class="lyrico-lyrics-wrapper">Tennavane Chinnavane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tennavane Chinnavane"/>
</div>
<div class="lyrico-lyrics-wrapper">Devathaiyin Mannavane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Devathaiyin Mannavane"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan Paruvathai Anaikindra Podhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Paruvathai Anaikindra Podhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pathu Viral Pathaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathu Viral Pathaathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanava Ival Kaadhaliya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanava Ival Kaadhaliya"/>
</div>
<div class="lyrico-lyrics-wrapper">Manadhai Killum Manaviya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manadhai Killum Manaviya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal Otrai Kannil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Otrai Kannil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaamam Otrai Kannil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaamam Otrai Kannil"/>
</div>
<div class="lyrico-lyrics-wrapper">Entha Kannaal Ennai Paarkiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entha Kannaal Ennai Paarkiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanna Kanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna Kanna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaamam Kaadhal Rendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaamam Kaadhal Rendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhan Kannil Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan Kannil Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangal Moodi Unnai Kaangiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangal Moodi Unnai Kaangiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanne Kanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanne Kanne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Veru Naan Veru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Veru Naan Veru"/>
</div>
<div class="lyrico-lyrics-wrapper">Naam Veru Poovum Aavom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam Veru Poovum Aavom"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Ennai Valaikaadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Ennai Valaikaadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Kelvikuri Aaghi Povene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Kelvikuri Aaghi Povene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sirpam Pola Vaazhnthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirpam Pola Vaazhnthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Sedhukka Vanthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Sedhukka Vanthaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Meendum Paarai Aaven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meendum Paarai Aaven"/>
</div>
<div class="lyrico-lyrics-wrapper">Nyaayama Kaadhal Penne Penne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nyaayama Kaadhal Penne Penne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thottil Chedi Aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottil Chedi Aanen"/>
</div>
<div class="lyrico-lyrics-wrapper">Thottam Vanthu Sernthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottam Vanthu Sernthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaambai Theendum Velai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaambai Theendum Velai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaigalil Vizhunthen Kanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaigalil Vizhunthen Kanna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Vaayal En Perai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Vaayal En Perai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nan Ucharikka Vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nan Ucharikka Vendum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Theeyal En Selai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Theeyal En Selai"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinam Theekulikka Vendum Vendume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam Theekulikka Vendum Vendume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sagala Kala Vallavane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sagala Kala Vallavane"/>
</div>
<div class="lyrico-lyrics-wrapper">Salavai Seitha Chandhirane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Salavai Seitha Chandhirane"/>
</div>
<div class="lyrico-lyrics-wrapper">Tennavane Chinnavane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tennavane Chinnavane"/>
</div>
<div class="lyrico-lyrics-wrapper">Devathaiyin Mannavane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Devathaiyin Mannavane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Poovukul Nee Pootum Velai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Poovukul Nee Pootum Velai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pothum Pothum Un Leelai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pothum Pothum Un Leelai"/>
</div>
</pre>
