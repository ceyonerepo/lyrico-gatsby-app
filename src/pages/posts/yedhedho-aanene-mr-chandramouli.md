---
title: "yedhedho aanene song lyrics"
album: "Mr Chandramouli"
artist: "Sam CS"
lyricist: "Vivek"
director: "Thiru"
path: "/albums/mr-chandramouli-lyrics"
song: "Yedhedho Aanene"
image: ../../images/albumart/mr-chandramouli.jpg
date: 2018-07-06
lang: tamil
youtubeLink: "https://www.youtube.com/embed/BZy7kg42uPo"
type: "love"
singers:
  - Sam CS
  - Chinmayi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vannakuyilae vannakuyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vannakuyilae vannakuyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vannakuyilae vannakuyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vannakuyilae vannakuyilae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yedhedho aanenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhedho aanenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellamae neethanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellamae neethanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vannakuyilae vannakuyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vannakuyilae vannakuyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakulla kelakatti 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakulla kelakatti "/>
</div>
<div class="lyrico-lyrics-wrapper">enga nee thangura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga nee thangura"/>
</div>
<div class="lyrico-lyrics-wrapper">Vannakuyilae vannakuyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vannakuyilae vannakuyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanapula ada katti 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanapula ada katti "/>
</div>
<div class="lyrico-lyrics-wrapper">muthama thingura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muthama thingura"/>
</div>
<div class="lyrico-lyrics-wrapper">Vannakuyilae vannakuyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vannakuyilae vannakuyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vannakuyilae vannakuyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vannakuyilae vannakuyilae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee kannamoochi kaatathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kannamoochi kaatathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattampoochi yeripora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattampoochi yeripora"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeno…yeno yeno oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeno…yeno yeno oo"/>
</div>
<div class="lyrico-lyrics-wrapper">Un vekkam sedharum vattathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un vekkam sedharum vattathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Nikka vechu pora enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikka vechu pora enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeno yeno yeno oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeno yeno yeno oo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aatha kadaledukum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aatha kadaledukum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mainaava maram tholil podum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mainaava maram tholil podum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathu madalodikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathu madalodikum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaenthatu vandaara cherum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaenthatu vandaara cherum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Megam nelavidikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megam nelavidikum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nandoda ala aadi theerkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nandoda ala aadi theerkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal nedi adikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal nedi adikum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnodu naan serum podhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnodu naan serum podhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yedhedho aanenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhedho aanenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellamae neethanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellamae neethanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhedho aanenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhedho aanenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellamae neethanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellamae neethanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maramchedi kodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maramchedi kodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Manamthava madi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manamthava madi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadichukka veralae podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadichukka veralae podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sinungura dhesaKelambuthu yesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sinungura dhesaKelambuthu yesa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava mazha koralae odhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava mazha koralae odhum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ullaara killum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullaara killum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un valloora paarva undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un valloora paarva undu"/>
</div>
<div class="lyrico-lyrics-wrapper">Manmetha pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manmetha pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Alai neettidum porva rendu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alai neettidum porva rendu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarum thedaa oru theevil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarum thedaa oru theevil"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazha vaa vaa nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazha vaa vaa nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yedhedho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhedho"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellamae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhiravan podi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhiravan podi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalavara vedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalavara vedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Mogamena amanja yogam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mogamena amanja yogam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Masangura kalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Masangura kalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Magarantha malar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Magarantha malar"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu ava azhagu thegam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu ava azhagu thegam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnoda kannil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnoda kannil"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan ennaiyae paaka venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan ennaiyae paaka venum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un anba solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un anba solla"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan pudhu vaarthai kokka venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan pudhu vaarthai kokka venum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanae venam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanae venam"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir vaazha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir vaazha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee mattum venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee mattum venum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yedhedho aanenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhedho aanenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellamae neethanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellamae neethanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vannakuyilae vannakuyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vannakuyilae vannakuyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakulla kelakatti 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakulla kelakatti "/>
</div>
<div class="lyrico-lyrics-wrapper">enga nee thangura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga nee thangura"/>
</div>
<div class="lyrico-lyrics-wrapper">Vannakuyilae vannakuyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vannakuyilae vannakuyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanapula ada katti 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanapula ada katti "/>
</div>
<div class="lyrico-lyrics-wrapper">muthama thingura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muthama thingura"/>
</div>
<div class="lyrico-lyrics-wrapper">Vannakuyilae vannakuyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vannakuyilae vannakuyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vannakuyilae vannakuyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vannakuyilae vannakuyilae"/>
</div>
</pre>
