---
title: "gaana of republic song lyrics"
album: "Republic"
artist: "Mani Sharma"
lyricist: "Rahman"
director: "Deva Katta"
path: "/albums/republic-lyrics"
song: "Gaana of Republic"
image: ../../images/albumart/republic.jpg
date: 2021-10-01
lang: telugu
youtubeLink: "https://www.youtube.com/embed/DAPsrjU27dk"
type: "mass"
singers:
  - Anurag Kulkarni
  - Dhanunjay
  - Hymath Mohammed
  - Aditya Iyengar
  - Prudhvi Chandra
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aye raaro Aye raaro Aye raaro Aye ro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aye raaro Aye raaro Aye raaro Aye ro"/>
</div>
<div class="lyrico-lyrics-wrapper">Aye raaro Aye raaro Aye raaro Aye ro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aye raaro Aye raaro Aye raaro Aye ro"/>
</div>
<div class="lyrico-lyrics-wrapper">Na pranamloni pranam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na pranamloni pranam"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa dehamloni daham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa dehamloni daham"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa mounam paade ganam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa mounam paade ganam"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa prashna samadhanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa prashna samadhanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhi andhamaina andharani kannera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhi andhamaina andharani kannera"/>
</div>
<div class="lyrico-lyrics-wrapper">Laksha aksharalu rayaleni kavitharaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laksha aksharalu rayaleni kavitharaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee prapanchame korukune athivara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee prapanchame korukune athivara"/>
</div>
<div class="lyrico-lyrics-wrapper">Penu viplavala viswa kanya swchhara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Penu viplavala viswa kanya swchhara"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa kallalona rangula kala raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa kallalona rangula kala raa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa kallalona rangula kala raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa kallalona rangula kala raa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa oohalake unike thanuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa oohalake unike thanuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa brathukulona bagam kadharaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa brathukulona bagam kadharaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa oopirike artham thaanu raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa oopirike artham thaanu raa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aye raaro Aye raaro Aye raaro Aye ro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aye raaro Aye raaro Aye raaro Aye ro"/>
</div>
<div class="lyrico-lyrics-wrapper">Aye raaro Aye raaro Aye raaro Aye ro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aye raaro Aye raaro Aye raaro Aye ro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thellavaadinedhirinchi nallani cheekatla nunchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thellavaadinedhirinchi nallani cheekatla nunchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pillanu vidipinchi techhi sambaralu chesukunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pillanu vidipinchi techhi sambaralu chesukunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Anthalone telisindhani mayamaipoyindhani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anthalone telisindhani mayamaipoyindhani"/>
</div>
<div class="lyrico-lyrics-wrapper">Mundhukanna muppuvunna panjarana unnadhani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mundhukanna muppuvunna panjarana unnadhani"/>
</div>
<div class="lyrico-lyrics-wrapper">Asalekkadundho teliyakundhi chudara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asalekkadundho teliyakundhi chudara"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhi leka manishikinka viluvedhiraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhi leka manishikinka viluvedhiraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye poratamtho daanini cheraliraa aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye poratamtho daanini cheraliraa aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye ayudham tho daanini gelavalira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye ayudham tho daanini gelavalira"/>
</div>
<div class="lyrico-lyrics-wrapper">Aye raaro Aye raaro Aye raaro Aye ro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aye raaro Aye raaro Aye raaro Aye ro"/>
</div>
<div class="lyrico-lyrics-wrapper">Aye raaro Aye raaro Aye raaro Aye ro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aye raaro Aye raaro Aye raaro Aye ro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anadhigaa evado okadu adhi nake sonthamantu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anadhigaa evado okadu adhi nake sonthamantu"/>
</div>
<div class="lyrico-lyrics-wrapper">Niyanthalai nirantharam cheralo bandhincharu uu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niyanthalai nirantharam cheralo bandhincharu uu"/>
</div>
<div class="lyrico-lyrics-wrapper">Rekkalani virichesi hakkulanu cheripesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rekkalani virichesi hakkulanu cheripesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Adigey prathi okkadini anichi anichi vesinaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adigey prathi okkadini anichi anichi vesinaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Narajathi charitralo naligipoyeraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Narajathi charitralo naligipoyeraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Challarani swatantrya kanksha swechhara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Challarani swatantrya kanksha swechhara"/>
</div>
<div class="lyrico-lyrics-wrapper">Nara narallo pravahinche aarthy raa aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nara narallo pravahinche aarthy raa aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanipinchaka nadipinche kanthiraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanipinchaka nadipinche kanthiraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aye raaro Aye raaro Aye raaro Aye ro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aye raaro Aye raaro Aye raaro Aye ro"/>
</div>
<div class="lyrico-lyrics-wrapper">Aye raaro Aye raaro Aye raaro Aye ro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aye raaro Aye raaro Aye raaro Aye ro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aye raaro Aye raaro Aye raaro Aye ro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aye raaro Aye raaro Aye raaro Aye ro"/>
</div>
<div class="lyrico-lyrics-wrapper">Aye raaro Aye raaro Aye raaro Aye ro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aye raaro Aye raaro Aye raaro Aye ro"/>
</div>
</pre>
