---
title: "kolame en kolame song lyrics"
album: "penguin"
artist: "Santhosh Narayanan"
lyricist: "Vivek"
director: "Eashvar Karthic"
path: "/albums/penguin-lyrics"
song: "Kolame En Kolame"
image: ../../images/albumart/penguin.jpg
date: 2020-06-19
lang: tamil
youtubeLink: "https://www.youtube.com/embed/q0oAKNIUrrg"
type: "Melody"
singers:
  - Susha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kolame En Kolame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolame En Kolame"/>
</div>
<div class="lyrico-lyrics-wrapper">Puzhudhiyil Poovin Vannam Seidhomae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puzhudhiyil Poovin Vannam Seidhomae"/>
</div>
<div class="lyrico-lyrics-wrapper">Podhume Nee Eppodhume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhume Nee Eppodhume"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Vandhaale Meendum Kaiyil Ellaame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vandhaale Meendum Kaiyil Ellaame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu Nijamaa Pudhu Niramaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Nijamaa Pudhu Niramaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kirukkalukkul Nindra Oviyamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kirukkalukkul Nindra Oviyamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mazhai Vizhum Paadhai Endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai Vizhum Paadhai Endru"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhalaiyin Paarvai Ondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhalaiyin Paarvai Ondru"/>
</div>
<div class="lyrico-lyrics-wrapper">Manadhinai Thaenaakkumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manadhinai Thaenaakkumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Podhumae Nee Eppodhumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhumae Nee Eppodhumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Vandhaale Meendum Kaiyil Ellaamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vandhaale Meendum Kaiyil Ellaamae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kulaavum Ezhisaiyum Kodi Poo Dhisaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulaavum Ezhisaiyum Kodi Poo Dhisaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadi Nee Asaiya Kaangiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadi Nee Asaiya Kaangiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Paal Nilaa Pisaiyum Paarvaiyin Vasiyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paal Nilaa Pisaiyum Paarvaiyin Vasiyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Paaraiyum Kasiyum Engiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaraiyum Kasiyum Engiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thadaagamo Kalaabamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadaagamo Kalaabamo"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kai Pada Uruvaagumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kai Pada Uruvaagumo"/>
</div>
<div class="lyrico-lyrics-wrapper">Maada Maaligaiyum Jaadaiyaal Pozhiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maada Maaligaiyum Jaadaiyaal Pozhiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Pol Suvaiyum Aagumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Pol Suvaiyum Aagumo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pirai Naan Thaan Nirai Neethaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirai Naan Thaan Nirai Neethaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enai Thodum Adheedhamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai Thodum Adheedhamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Orey Murai Alai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orey Murai Alai"/>
</div>
<div class="lyrico-lyrics-wrapper">Idam Tharum Un Nooru Thoorigai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idam Tharum Un Nooru Thoorigai"/>
</div>
<div class="lyrico-lyrics-wrapper">Padaamal Povadhae Pizhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padaamal Povadhae Pizhai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alai Neethaan Anai Naanthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alai Neethaan Anai Naanthaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Vandhaalae Meendum Kaiyil Ellaame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vandhaalae Meendum Kaiyil Ellaame"/>
</div>
</pre>
