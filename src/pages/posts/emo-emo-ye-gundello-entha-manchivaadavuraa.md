---
title: "emo emo ye gundello song lyrics"
album: "Entha Manchivaadavuraa"
artist: "Gopi Sundar"
lyricist: "Ramajogayya Sastry"
director: "Satish Vegesna"
path: "/albums/entha-manchivaadavuraa-lyrics"
song: "Emo Emo Ye Gundello"
image: ../../images/albumart/entha-manchivaadavuraa.jpg
date: 2020-01-15
lang: telugu
youtubeLink: "https://www.youtube.com/embed/RNNTngghBCU"
type: "love"
singers:
  - S.P. Balasubramaniam
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Emo emo ye gundello ye bhadha undho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emo emo ye gundello ye bhadha undho"/>
</div>
<div class="lyrico-lyrics-wrapper">O koncham paalu panchukundham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O koncham paalu panchukundham"/>
</div>
<div class="lyrico-lyrics-wrapper">Emo emo ye dhaarullo ye bhandhamundho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emo emo ye dhaarullo ye bhandhamundho"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhandhuvula sankya penchukundham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhandhuvula sankya penchukundham"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheyyandhukundham chigurantha dhairyamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheyyandhukundham chigurantha dhairyamai"/>
</div>
<div class="lyrico-lyrics-wrapper">Bharosanidham padha maro balamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bharosanidham padha maro balamai"/>
</div>
<div class="lyrico-lyrics-wrapper">Manushulam manandharam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manushulam manandharam"/>
</div>
<div class="lyrico-lyrics-wrapper">Yekaakulam kaadhe evvaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yekaakulam kaadhe evvaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Manchi thanam mana gunam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manchi thanam mana gunam"/>
</div>
<div class="lyrico-lyrics-wrapper">Parasparam saaayam kaagalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parasparam saaayam kaagalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Emo emo ye gundello ye bhadha undho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emo emo ye gundello ye bhadha undho"/>
</div>
<div class="lyrico-lyrics-wrapper">O koncham paalu panchukundham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O koncham paalu panchukundham"/>
</div>
<div class="lyrico-lyrics-wrapper">Emo emo ye dhaarullo ye bhandhamundho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emo emo ye dhaarullo ye bhandhamundho"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhandhuvula sankya penchukundham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhandhuvula sankya penchukundham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye raktha bhandham lekunna gaani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye raktha bhandham lekunna gaani"/>
</div>
<div class="lyrico-lyrics-wrapper">Spandinchagaligina snehithulam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Spandinchagaligina snehithulam"/>
</div>
<div class="lyrico-lyrics-wrapper">Eechoti prema yechotikaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eechoti prema yechotikaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Andhinchagaligina vaaradhulam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhinchagaligina vaaradhulam"/>
</div>
<div class="lyrico-lyrics-wrapper">O gunde nippunu aarpadam aapadam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O gunde nippunu aarpadam aapadam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadha upakaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadha upakaaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Verevari haayiko jolaali paadadam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verevari haayiko jolaali paadadam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaaha yentha varam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaha yentha varam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Emo emo ye gundello ye bhadha undho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emo emo ye gundello ye bhadha undho"/>
</div>
<div class="lyrico-lyrics-wrapper">O koncham paalu panchukundham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O koncham paalu panchukundham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Khaaleelennenno puttinchesthundi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khaaleelennenno puttinchesthundi"/>
</div>
<div class="lyrico-lyrics-wrapper">Khaaleega undaleni kaalamidhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khaaleega undaleni kaalamidhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasainadhaanni maayam chesthundi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasainadhaanni maayam chesthundi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappinchukoleni jaalamidhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappinchukoleni jaalamidhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aalotu theerchaga ipudu epudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalotu theerchaga ipudu epudu"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam mundhundhaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam mundhundhaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kashtaala baruvunu thelikapariche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kashtaala baruvunu thelikapariche"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhujam manamaudhaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhujam manamaudhaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Emo emo ye gundello ye bhadha undho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emo emo ye gundello ye bhadha undho"/>
</div>
<div class="lyrico-lyrics-wrapper">O koncham paalu panchukundham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O koncham paalu panchukundham"/>
</div>
<div class="lyrico-lyrics-wrapper">Emo emo ye dhaarullo ye bhandhamundho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emo emo ye dhaarullo ye bhandhamundho"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhandhuvula sankya penchukundham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhandhuvula sankya penchukundham"/>
</div>
</pre>
