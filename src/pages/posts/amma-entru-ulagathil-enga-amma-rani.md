---
title: "amma entru ulagathil song lyrics"
album: "Enga Amma Rani"
artist: "Ilaiyaraaja"
lyricist: "Unknown"
director: "S Bani"
path: "/albums/enga-amma-rani-lyrics"
song: "Amma Entru Ulagathil"
image: ../../images/albumart/enga-amma-rani.jpg
date: 2017-05-05
lang: tamil
youtubeLink: "https://www.youtube.com/embed/8gq2uREAieM"
type: "affection"
singers:
  -	Anitha Karthikeyan
  - Aala
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ammavendru Ulagathil Illatta 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammavendru Ulagathil Illatta "/>
</div>
<div class="lyrico-lyrics-wrapper">Ennagum Ennagum Sollaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennagum Ennagum Sollaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ammavendru Ulagathil Illatta 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammavendru Ulagathil Illatta "/>
</div>
<div class="lyrico-lyrics-wrapper">Ennagum Ennagum Sollaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennagum Ennagum Sollaatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaththalikkum Bodhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaththalikkum Bodhu "/>
</div>
<div class="lyrico-lyrics-wrapper">Thaangikkolla Yaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaangikkolla Yaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Thookam Varum Bodhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thookam Varum Bodhu "/>
</div>
<div class="lyrico-lyrics-wrapper">Thoongavaikka Yaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoongavaikka Yaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaringu Sollakoodumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaringu Sollakoodumo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ammamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ammavendru Ulagathil Illatta 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammavendru Ulagathil Illatta "/>
</div>
<div class="lyrico-lyrics-wrapper">Ennagum Ennagum Sollaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennagum Ennagum Sollaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnalum Ennalum Aagadhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnalum Ennalum Aagadhu "/>
</div>
<div class="lyrico-lyrics-wrapper">Ennenna Sonnalum Theeradhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennenna Sonnalum Theeradhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Suttithanam Seikindra Podhilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suttithanam Seikindra Podhilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sella Thattu Thatti Mutham Thandhadhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sella Thattu Thatti Mutham Thandhadhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Potti Yittu Sandai Idum Podhilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Potti Yittu Sandai Idum Podhilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thatti Kettu Mutti Poda Cheivadhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thatti Kettu Mutti Poda Cheivadhum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unadhiru Kangal Thoongadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unadhiru Kangal Thoongadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unadhiru Kangal Thoongadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unadhiru Kangal Thoongadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Engalin Kanavai Nee Sumandhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engalin Kanavai Nee Sumandhaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkena Yaedhum Thedamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkena Yaedhum Thedamal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engalin Thedalil Nee Nadandhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engalin Thedalil Nee Nadandhaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnidathil Kaettal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnidathil Kaettal "/>
</div>
<div class="lyrico-lyrics-wrapper">Kettapadi Tharuvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kettapadi Tharuvai"/>
</div>
<div class="lyrico-lyrics-wrapper">Illai Endru Sonnadhilaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illai Endru Sonnadhilaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ammavendru Ulagathil Illatta 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammavendru Ulagathil Illatta "/>
</div>
<div class="lyrico-lyrics-wrapper">Ennagum Ennagum Sollaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennagum Ennagum Sollaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnalum Ennalum Aagadhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnalum Ennalum Aagadhu "/>
</div>
<div class="lyrico-lyrics-wrapper">Ennenna Sonnalum Theeradhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennenna Sonnalum Theeradhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unn Mugathai Nitham Nitham Paarthaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unn Mugathai Nitham Nitham Paarthaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaagam Pasi Ennavendu Theriyadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaagam Pasi Ennavendu Theriyadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Alli Tharum Anbu Mozhi Ketalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alli Tharum Anbu Mozhi Ketalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Neram Kalam Povadhondrum Theriyadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neram Kalam Povadhondrum Theriyadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Siragugal Veesum Poongiliyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siragugal Veesum Poongiliyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Siragugal Veesum Poongiliyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siragugal Veesum Poongiliyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholgalil Nindru Aadugirom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholgalil Nindru Aadugirom"/>
</div>
<div class="lyrico-lyrics-wrapper">Thazhuvidum Unadhu Thaaimaiyilae 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thazhuvidum Unadhu Thaaimaiyilae "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pudhu Pudhu Vaanam Thedukirom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu Pudhu Vaanam Thedukirom"/>
</div>
<div class="lyrico-lyrics-wrapper">Unn Nenjam Illai Ingae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unn Nenjam Illai Ingae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhuvidu Anbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhuvidu Anbae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakena Naangal Illaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakena Naangal Illaiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ammavendru Ulagathil Illatta 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammavendru Ulagathil Illatta "/>
</div>
<div class="lyrico-lyrics-wrapper">Ennagum Ennagum Sollaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennagum Ennagum Sollaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ammavendru Ulagathil Illatta 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammavendru Ulagathil Illatta "/>
</div>
<div class="lyrico-lyrics-wrapper">Ennagum Ennagum Sollaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennagum Ennagum Sollaatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaththalikkum Bodhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaththalikkum Bodhu "/>
</div>
<div class="lyrico-lyrics-wrapper">Thaangikkolla Yaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaangikkolla Yaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Thookam Varum Bodhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thookam Varum Bodhu "/>
</div>
<div class="lyrico-lyrics-wrapper">Thoongavaikka Yaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoongavaikka Yaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaringu Sollakoodumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaringu Sollakoodumo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ammamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ammavendru Ulagathil Illatta 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammavendru Ulagathil Illatta "/>
</div>
<div class="lyrico-lyrics-wrapper">Ennagum Ennagum Sollaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennagum Ennagum Sollaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnalum Ennalum Aagadhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnalum Ennalum Aagadhu "/>
</div>
<div class="lyrico-lyrics-wrapper">Ennenna Sonnalum Theeradhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennenna Sonnalum Theeradhu"/>
</div>
</pre>
