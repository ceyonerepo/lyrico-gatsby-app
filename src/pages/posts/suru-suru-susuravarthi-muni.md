---
title: "suru suru susuravarthi song lyrics"
album: "Muni"
artist: "Bharathwaj"
lyricist: "Pa. Vijay - Raghava Lawrence"
director: "Raghava Lawrence"
path: "/albums/muni-lyrics"
song: "Suru Suru Susuravarthi"
image: ../../images/albumart/muni.jpg
date: 2007-03-09
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Co3Ndb3xv8M"
type: "Love"
singers:
  - Ranjith
  - Anuradha Sriram
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Surusuru susuravaththi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Surusuru susuravaththi"/>
</div>
<div class="lyrico-lyrics-wrapper">Summaa iruntha vaththikkuchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summaa iruntha vaththikkuchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendum ippo paththikkichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendum ippo paththikkichu"/>
</div>
<div class="lyrico-lyrics-wrapper">Auto bommu vedichchiruchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Auto bommu vedichchiruchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Damaar yei dammaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Damaar yei dammaar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sara sara sara sangu sakkaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sara sara sara sangu sakkaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Summaa iruntha bhusvaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summaa iruntha bhusvaanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendum ippo paththikkichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendum ippo paththikkichu"/>
</div>
<div class="lyrico-lyrics-wrapper">Auto bommu vedichchiruchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Auto bommu vedichchiruchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Damaar yei dammaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Damaar yei dammaar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sangu sakkaram suththunaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sangu sakkaram suththunaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaala thookkuvaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaala thookkuvaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Lashmi vedi vedichaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lashmi vedi vedichaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadha pothuvaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadha pothuvaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Rocket onnu vittaakkaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rocket onnu vittaakkaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mela paappaa….
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mela paappaa…."/>
</div>
<div class="lyrico-lyrics-wrapper">Rocket onnu vittaakkaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rocket onnu vittaakkaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mela paappaa….
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mela paappaa…."/>
</div>
<div class="lyrico-lyrics-wrapper">Vaadi vaadi kitta vaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadi vaadi kitta vaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kitta vanthu muttikkodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kitta vanthu muttikkodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Muttikkodi muttikkodi dididi didi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muttikkodi muttikkodi dididi didi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Deebaavali deebaavali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Deebaavali deebaavali"/>
</div>
<div class="lyrico-lyrics-wrapper">Happy happy deebaavali…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Happy happy deebaavali…"/>
</div>
<div class="lyrico-lyrics-wrapper">Deebaavali deebaavali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Deebaavali deebaavali"/>
</div>
<div class="lyrico-lyrics-wrapper">Happy happy deebaavali…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Happy happy deebaavali…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Suudu parakkum deebaavali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suudu parakkum deebaavali"/>
</div>
<div class="lyrico-lyrics-wrapper">Deebaavali ..suru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Deebaavali ..suru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Surusuru susuravaththi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Surusuru susuravaththi"/>
</div>
<div class="lyrico-lyrics-wrapper">Summaa iruntha vaththikkuchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summaa iruntha vaththikkuchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendum ippo paththikkichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendum ippo paththikkichu"/>
</div>
<div class="lyrico-lyrics-wrapper">Auto bommu vedichchiruchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Auto bommu vedichchiruchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Damaar yei dammaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Damaar yei dammaar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paambu vedi pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paambu vedi pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum nazhuvappaakkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum nazhuvappaakkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Kitta vanthu ninnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kitta vanthu ninnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhusshu sollura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhusshu sollura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oosi vedi pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oosi vedi pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum orasappaakkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum orasappaakkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam thalli ninnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam thalli ninnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaduppaagura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaduppaagura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aamaandi aamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aamaandi aamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Koranji poguren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koranji poguren"/>
</div>
<div class="lyrico-lyrics-wrapper">Athiradiyaa naanu erangappogiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athiradiyaa naanu erangappogiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ammaadi ammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammaadi ammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholla pannura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholla pannura"/>
</div>
<div class="lyrico-lyrics-wrapper">Aththaadi aaththaa aala kavukkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aththaadi aaththaa aala kavukkura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Summaa iruntha sanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summaa iruntha sanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Oothikkedukkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oothikkedukkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalam pochchu neram pochchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalam pochchu neram pochchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaam pochchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaam pochchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalam pochchu neram pochchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalam pochchu neram pochchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaam pochchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaam pochchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Damaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Damaar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yei damaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei damaar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Surusuru susuravaththi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Surusuru susuravaththi"/>
</div>
<div class="lyrico-lyrics-wrapper">Summaa iruntha vaththikkuchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summaa iruntha vaththikkuchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendum ippo paththikkichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendum ippo paththikkichu"/>
</div>
<div class="lyrico-lyrics-wrapper">Auto bommu vedichchiruchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Auto bommu vedichchiruchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Damaar yei dammaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Damaar yei dammaar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hahhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hahhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuruvi vediya pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuruvi vediya pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanu koththa paakkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanu koththa paakkuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Kitta vanthaa neethaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kitta vanthaa neethaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Paranthu pogura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paranthu pogura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye saravedi pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye saravedi pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum setharappaakkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum setharappaakkuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanni ooththi naanthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanni ooththi naanthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Anaikkappaakkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anaikkappaakkuren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vendaandi chellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vendaandi chellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanni ooththaathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanni ooththaathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pasiyoda irukkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pasiyoda irukkuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattini podaathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattini podaathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee jega jaalakkillaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee jega jaalakkillaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesi mayakkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesi mayakkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Pooppola ponna katti izhukkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pooppola ponna katti izhukkura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yei summaa iruntha sanga..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei summaa iruntha sanga.."/>
</div>
<div class="lyrico-lyrics-wrapper">Oothikkedukkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oothikkedukkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalam pochchu neram pochchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalam pochchu neram pochchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaam pochchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaam pochchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalam pochchu neram pochchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalam pochchu neram pochchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaam pochchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaam pochchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Damaar yei damaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Damaar yei damaar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Surusuru susuravaththi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Surusuru susuravaththi"/>
</div>
<div class="lyrico-lyrics-wrapper">Summaa iruntha vaththikkuchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summaa iruntha vaththikkuchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendum ippo paththikkichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendum ippo paththikkichu"/>
</div>
<div class="lyrico-lyrics-wrapper">Auto bommu vedichchiruchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Auto bommu vedichchiruchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Damaar yei dammaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Damaar yei dammaar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sara sara sara sangu sakkaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sara sara sara sangu sakkaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Summaa iruntha bhusvaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summaa iruntha bhusvaanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendum ippo paththikkichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendum ippo paththikkichu"/>
</div>
<div class="lyrico-lyrics-wrapper">Auto bommu vedichchiruchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Auto bommu vedichchiruchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Damaar yei dammaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Damaar yei dammaar"/>
</div>
</pre>
