---
title: "oorkaran song lyrics"
album: "Selfie"
artist: "GV Prakash Kumar"
lyricist: "Arivu"
director: "Mathi Maran"
path: "/albums/selfie-lyrics"
song: "Oorkaran"
image: ../../images/albumart/selfie.jpg
date: 2022-04-01
lang: tamil
youtubeLink: "https://www.youtube.com/embed/RfOetkazBsg"
type: "mass"
singers:
  - Arivu
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Usuru Enakkuthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usuru Enakkuthan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kosuru Kanakkuthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kosuru Kanakkuthan"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudivu Eduthutaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudivu Eduthutaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorkaaran Oorkaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorkaaran Oorkaran"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pattamum Padikkathan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattamum Padikkathan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pethavan Anuppitaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pethavan Anuppitaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Mathavan Per Vaikkuraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mathavan Per Vaikkuraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorkaaran Oorkaaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorkaaran Oorkaaran"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kashtapattu Mela Mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kashtapattu Mela Mela"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandha Velai Kidaikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandha Velai Kidaikum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ishtathukku Kanda Kanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishtathukku Kanda Kanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Naaiyum Yeri Nerikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naaiyum Yeri Nerikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Patti Kaatanukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Patti Kaatanukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattanathu Kaathu Adikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattanathu Kaathu Adikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thokka Maatikkitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thokka Maatikkitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattam Katti Seettedukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattam Katti Seettedukkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pattikaattan Oornaattan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattikaattan Oornaattan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pangu Kettaan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pangu Kettaan "/>
</div>
<div class="lyrico-lyrics-wrapper">Panga Panga Panga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panga Panga Panga"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattikaattan Oornaattan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattikaattan Oornaattan"/>
</div>
<div class="lyrico-lyrics-wrapper">Sanda Pottaan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sanda Pottaan "/>
</div>
<div class="lyrico-lyrics-wrapper">Sanda Sanda Sanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sanda Sanda Sanda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaaran Kaaran Oorkaaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaran Kaaran Oorkaaran"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnathaan Oorukaaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnathaan Oorukaaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Nakkiputtu Poporaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nakkiputtu Poporaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaraan Vaaraan Oorkaaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaraan Vaaraan Oorkaaran"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennathaan Evan Ingu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennathaan Evan Ingu "/>
</div>
<div class="lyrico-lyrics-wrapper">Thaduppaan Paakuran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaduppaan Paakuran"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaaran Kaaran Oorkaaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaran Kaaran Oorkaaran"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnathaan Oorukaaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnathaan Oorukaaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Nakkiputtu Poporaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nakkiputtu Poporaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaraan Vaaraan Oorkaaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaraan Vaaraan Oorkaaran"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennathaan Evan Ingu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennathaan Evan Ingu "/>
</div>
<div class="lyrico-lyrics-wrapper">Thaduppaan Paakuran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaduppaan Paakuran"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Panam Vanganum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panam Vanganum "/>
</div>
<div class="lyrico-lyrics-wrapper">Pesu Pesu Pesu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesu Pesu Pesu"/>
</div>
<div class="lyrico-lyrics-wrapper">Education Um 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Education Um "/>
</div>
<div class="lyrico-lyrics-wrapper">Kaasu Kaasu Kaasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasu Kaasu Kaasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhusa Varaan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhusa Varaan "/>
</div>
<div class="lyrico-lyrics-wrapper">Thooku Thooku Thooku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thooku Thooku Thooku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neet Aanaalum Poduvom 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neet Aanaalum Poduvom "/>
</div>
<div class="lyrico-lyrics-wrapper">Ji Naanga Seattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ji Naanga Seattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalla Padinga Thambi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla Padinga Thambi"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakku Irukudhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakku Irukudhu "/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Nalla Edhirkaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Nalla Edhirkaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalla Iru Thambi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla Iru Thambi"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha Naatukku Neenga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Naatukku Neenga "/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaan Varungaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaan Varungaalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaaran Kaaran Oorkaaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaran Kaaran Oorkaaran"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnathaan Oorukaaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnathaan Oorukaaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Nakkiputtu Poporaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nakkiputtu Poporaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaraan Vaaraan Oorkaaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaraan Vaaraan Oorkaaran"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennathaan Evan Ingu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennathaan Evan Ingu "/>
</div>
<div class="lyrico-lyrics-wrapper">Thaduppaan Paakuran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaduppaan Paakuran"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaaran Kaaran Oorkaaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaran Kaaran Oorkaaran"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnathaan Oorukaaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnathaan Oorukaaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Nakkiputtu Poporaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nakkiputtu Poporaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaraan Vaaraan Oorkaaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaraan Vaaraan Oorkaaran"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennathaan Evan Ingu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennathaan Evan Ingu "/>
</div>
<div class="lyrico-lyrics-wrapper">Thaduppaan Paakuran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaduppaan Paakuran"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Patti Kaatula Gang Gang Gangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Patti Kaatula Gang Gang Gangu"/>
</div>
<div class="lyrico-lyrics-wrapper">City Kottaiyil Naan Thaan Strongu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="City Kottaiyil Naan Thaan Strongu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ejamaan Ini Naan Than Wrongu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ejamaan Ini Naan Than Wrongu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pisagathuda Pasi God Du Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pisagathuda Pasi God Du Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Patti Kaatula Gang Gang Gangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Patti Kaatula Gang Gang Gangu"/>
</div>
<div class="lyrico-lyrics-wrapper">City Kottaiyil Naan Thaan Strongu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="City Kottaiyil Naan Thaan Strongu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ejamaan Ini Naan Than Wrongu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ejamaan Ini Naan Than Wrongu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pisagathuda Pasi God Du Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pisagathuda Pasi God Du Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Appan Padikka Vachaan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appan Padikka Vachaan "/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhu Padichom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhu Padichom"/>
</div>
<div class="lyrico-lyrics-wrapper">Sotha Adagu Vachu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sotha Adagu Vachu "/>
</div>
<div class="lyrico-lyrics-wrapper">Sendhu Tholachom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sendhu Tholachom"/>
</div>
<div class="lyrico-lyrics-wrapper">Ketta Payakittalaam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketta Payakittalaam "/>
</div>
<div class="lyrico-lyrics-wrapper">Paadam Padichom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadam Padichom"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattam Kodukkum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattam Kodukkum "/>
</div>
<div class="lyrico-lyrics-wrapper">Munna Seattu Pudichom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munna Seattu Pudichom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaaran Kaaran Oorkaaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaran Kaaran Oorkaaran"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnathaan Oorukaaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnathaan Oorukaaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Nakkiputtu Poporaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nakkiputtu Poporaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaraan Vaaraan Oorkaaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaraan Vaaraan Oorkaaran"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennathaan Evan Ingu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennathaan Evan Ingu "/>
</div>
<div class="lyrico-lyrics-wrapper">Thaduppaan Paakuran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaduppaan Paakuran"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaaran Kaaran Oorkaaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaran Kaaran Oorkaaran"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnathaan Oorukaaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnathaan Oorukaaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Nakkiputtu Poporaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nakkiputtu Poporaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaraan Vaaraan Oorkaaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaraan Vaaraan Oorkaaran"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennathaan Evan Ingu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennathaan Evan Ingu "/>
</div>
<div class="lyrico-lyrics-wrapper">Thaduppaan Paakuran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaduppaan Paakuran"/>
</div>
</pre>
