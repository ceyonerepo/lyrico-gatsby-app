---
title: "un pere theriyathu song lyrics"
album: "Engaeyum Eppothum"
artist: "C Sathya"
lyricist: "Na Muthukumar"
director: "M. Saravanan"
path: "/albums/engaeyum-eppothum-lyrics"
song: "Un Pere Theriyathu"
image: ../../images/albumart/engaeyum-eppothum.jpg
date: 2011-09-16
lang: tamil
youtubeLink: "https://www.youtube.com/embed/fgX5w8WjUzo"
type: "love"
singers:
  - Madhushree
  - Sudha Ragunathan
  - Aalap Raju
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Un perae theriyaadhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un perae theriyaadhu "/>
</div>
<div class="lyrico-lyrics-wrapper">unai kooppida mudiyaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unai kooppida mudiyaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan unakkor peyar vaithen 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan unakkor peyar vaithen "/>
</div>
<div class="lyrico-lyrics-wrapper">unakkae theriyaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unakkae theriyaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha perai ariyaadhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha perai ariyaadhu "/>
</div>
<div class="lyrico-lyrics-wrapper">ada yaarum ingethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada yaarum ingethu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhai oru murai sonnalae 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai oru murai sonnalae "/>
</div>
<div class="lyrico-lyrics-wrapper">thookkam varaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thookkam varaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada dhinam dhorum adhai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada dhinam dhorum adhai "/>
</div>
<div class="lyrico-lyrics-wrapper">cholli unnai konjuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cholli unnai konjuven"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan adangaadha anbalae 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan adangaadha anbalae "/>
</div>
<div class="lyrico-lyrics-wrapper">unnai minjuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai minjuven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">You never find a better time
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You never find a better time"/>
</div>
<div class="lyrico-lyrics-wrapper">Make a stand you ll be fine
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Make a stand you ll be fine"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho soodaana perum adhu thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho soodaana perum adhu thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Sonnavudan udhadugal kodhikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sonnavudan udhadugal kodhikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sooriyanai neeyum ninaithaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sooriyanai neeyum ninaithaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu illaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu illaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho jillendra perum adhu thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho jillendra perum adhu thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kettavudan nenjam kulirum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kettavudan nenjam kulirum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadhi endru neeyum ninaithaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadhi endru neeyum ninaithaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu illaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu illaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Silirkka vaikkum dheivam illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silirkka vaikkum dheivam illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mirala vaikkum mirugam illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mirala vaikkum mirugam illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Oli vattam therinthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oli vattam therinthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu pattap per illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu pattap per illai"/>
</div>
<div class="lyrico-lyrics-wrapper">En perin pinnaal varum per
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En perin pinnaal varum per"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan sollavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan sollavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">You never find a better time
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You never find a better time"/>
</div>
<div class="lyrico-lyrics-wrapper">Make a stand you ll be fine
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Make a stand you ll be fine"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Peridhaana perum adhu thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peridhaana perum adhu thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Solla solla moochae vaangum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solla solla moochae vaangum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethanai ezhuthukkal endraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethanai ezhuthukkal endraal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidai illaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidai illaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sirithaana perum adhu thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirithaana perum adhu thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Sattendru mudinthae pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sattendru mudinthae pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppadi solven naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppadi solven naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mozhi illaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mozhi illaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Solli vittaal udhadu ottum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solli vittaal udhadu ottum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhuthi vittaal theanum sottum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhuthi vittaal theanum sottum"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu sutha thamizh per thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu sutha thamizh per thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayal vaarthai adhil illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayal vaarthai adhil illai"/>
</div>
<div class="lyrico-lyrics-wrapper">En perin pinnaal varum perr
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En perin pinnaal varum perr"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan sollavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan sollavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un perae theriyaadhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un perae theriyaadhu "/>
</div>
<div class="lyrico-lyrics-wrapper">unai kooppida mudiyaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unai kooppida mudiyaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan unakkor peyar vaithen 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan unakkor peyar vaithen "/>
</div>
<div class="lyrico-lyrics-wrapper">unakkae theriyaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unakkae theriyaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha perai ariyaadhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha perai ariyaadhu "/>
</div>
<div class="lyrico-lyrics-wrapper">ada yaarum ingethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada yaarum ingethu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhai oru murai sonnalae 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai oru murai sonnalae "/>
</div>
<div class="lyrico-lyrics-wrapper">thookkam varaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thookkam varaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada dhinam dhorum adhai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada dhinam dhorum adhai "/>
</div>
<div class="lyrico-lyrics-wrapper">cholli unnai konjuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cholli unnai konjuven"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan adangaadha anbalae 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan adangaadha anbalae "/>
</div>
<div class="lyrico-lyrics-wrapper">unnai minjuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai minjuven"/>
</div>
</pre>
