---
title: "ennoda rasi song lyrics"
album: "Mappillai"
artist: "Mani Sharma"
lyricist: "Gangai Amaran"
director: "Suraj"
path: "/albums/mappillai-lyrics"
song: "Ennoda Rasi"
image: ../../images/albumart/mappillai.jpg
date: 2011-04-08
lang: tamil
youtubeLink: "https://www.youtube.com/embed/f1kIXfzAjoQ"
type: "love"
singers:
  - Ranjith
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ennoda Raasi Nalla Raasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennoda Raasi Nalla Raasi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athu Eppothum Periyavanga Aasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu Eppothum Periyavanga Aasi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aththamaga Raasi Atha Oormuzhukka Pesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aththamaga Raasi Atha Oormuzhukka Pesi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kottu Maelem Kotti Vaasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottu Maelem Kotti Vaasi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aththamaga Raasi Atha Oormuzhukka Pesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aththamaga Raasi Atha Oormuzhukka Pesi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kottu Maelem Kotti Vaasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottu Maelem Kotti Vaasi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennoada Raasi Nalla Raasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennoada Raasi Nalla Raasi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athu Eppodhum Periyavanga Aasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu Eppodhum Periyavanga Aasi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raasi Ulla Pakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raasi Ulla Pakkam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhinam Vetrivandhu Saerum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam Vetrivandhu Saerum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaasu Ulla Pakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasu Ulla Pakkam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Verum Thimiru Vanthu Saerum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verum Thimiru Vanthu Saerum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raasi Ulla Pakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raasi Ulla Pakkam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhinam Vetrivandhu Saerum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam Vetrivandhu Saerum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaasu Ulla Pakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasu Ulla Pakkam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Verum Thimiru Vanthu Saerum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verum Thimiru Vanthu Saerum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naeran Koodumbothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naeran Koodumbothu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Entha Oorum Unna Paadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entha Oorum Unna Paadum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjukkulla Nimmathi Varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjukkulla Nimmathi Varum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aalu Ambu Saenai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalu Ambu Saenai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Athanaiyum Koodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Athanaiyum Koodum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vittu Poana Sonthamum Varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vittu Poana Sonthamum Varum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koadiyiley Oruthanukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koadiyiley Oruthanukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raasi Uchathiley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raasi Uchathiley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Entha Kuraigalumey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entha Kuraigalumey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avangittathaan Thaedi Vanthathilley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avangittathaan Thaedi Vanthathilley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ethu Vanthaalum Ponaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethu Vanthaalum Ponaalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ottura Mannuthaan Ottumadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ottura Mannuthaan Ottumadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennoada Raasi Nalla Raasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennoada Raasi Nalla Raasi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athu Eppothum Periyavanga Aasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu Eppothum Periyavanga Aasi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athamaga Raasi Atha Oormuzhukka Paesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athamaga Raasi Atha Oormuzhukka Paesi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kottu Maelem Kotti Vaasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottu Maelem Kotti Vaasi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennoada Raasi Nalla Raasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennoada Raasi Nalla Raasi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athu Eppothum Periyavanga Aasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu Eppothum Periyavanga Aasi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maappullannaa Maappulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maappullannaa Maappulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaasal Karuvaepilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaasal Karuvaepilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maappullannaa Maappulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maappullannaa Maappulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaasal Karuvaeppilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaasal Karuvaeppilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theeppidicha Paeigala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeppidicha Paeigala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oattividum Vaeppilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oattividum Vaeppilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theeppidicha Paeigala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeppidicha Paeigala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oattividum Vaeppilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oattividum Vaeppilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ava Sirippula Oru Veruppilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Sirippula Oru Veruppilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avaru Kurumbathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avaru Kurumbathaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaaru Rasikkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaru Rasikkala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hei Diyaandakkaan Diyaandakkaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei Diyaandakkaan Diyaandakkaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Diyaandakkaan Dangaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Diyaandakkaan Dangaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ooru Vanba Paesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooru Vanba Paesum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Unmai Solla Koosum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Unmai Solla Koosum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Podum Nooru Vesham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podum Nooru Vesham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhinam Poiya Solli Yesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam Poiya Solli Yesum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ooru Vanba Paesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooru Vanba Paesum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Unmai Solla Koosum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Unmai Solla Koosum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poadum Nooru Vesam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poadum Nooru Vesam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thinam Poiya Solli Yesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thinam Poiya Solli Yesum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thillaattaangu Taangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thillaattaangu Taangu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Enna Unga Bongu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Enna Unga Bongu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aendiyamma Intha Raangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aendiyamma Intha Raangu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nallaa Illa Pokku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nallaa Illa Pokku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Sonnen Oru Vaakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Sonnen Oru Vaakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vethalaikku Kottappaakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vethalaikku Kottappaakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raaniyamma Manasuvacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raaniyamma Manasuvacha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nanmai Undaagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanmai Undaagum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nallapechu Ketkalanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nallapechu Ketkalanna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veedu Rendaagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veedu Rendaagum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Aththaachi Piththaachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Aththaachi Piththaachi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athana Vithaiyum Sollungamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athana Vithaiyum Sollungamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennoada Raasi Nalla Raasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennoada Raasi Nalla Raasi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athu Eppothum Periyavanga Aasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu Eppothum Periyavanga Aasi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athamaga Raasi Atha Oormuzhukka Paesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athamaga Raasi Atha Oormuzhukka Paesi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kottu Maelem Kotti Vaasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottu Maelem Kotti Vaasi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athamaga Raasi Atha Oormuzhukka Paesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athamaga Raasi Atha Oormuzhukka Paesi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kottu Maelem Kotti Vaasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottu Maelem Kotti Vaasi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennoada Raasi Nalla Raasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennoada Raasi Nalla Raasi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athu Eppothum Periyavanga Aasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu Eppothum Periyavanga Aasi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennoada Raasi Nalla Raasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennoada Raasi Nalla Raasi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athu Eppothum Periyavanga Aasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu Eppothum Periyavanga Aasi"/>
</div>
</pre>
