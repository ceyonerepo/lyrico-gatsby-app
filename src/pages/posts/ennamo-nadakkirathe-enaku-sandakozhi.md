---
title: "ennamo nadakkirathe enaku song lyrics"
album: "Sandakozhi"
artist: "Yuvan Sankar Raja"
lyricist: "Na.Muthukumar"
director: "N. Linguswamy"
path: "/albums/sandakozhi-lyrics"
song: "Ennamo Nadakkirathe Enaku"
image: ../../images/albumart/sandakozhi.jpg
date: 2005-12-16
lang: tamil
youtubeLink: "https://www.youtube.com/embed/raxMeOdtJaU"
type: "Love"
singers:
  - 	Shaan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ennamo Nadakkirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennamo Nadakkirathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakku Pidikkirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku Pidikkirathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennamo Nadakkirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennamo Nadakkirathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ellaam Pidikkirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaam Pidikkirathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennamo Nadakkirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennamo Nadakkirathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakku Pidikkirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku Pidikkirathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennamo Nadakkirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennamo Nadakkirathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ellaam Pidikkirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaam Pidikkirathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaar Yaaro Etho Pesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar Yaaro Etho Pesa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaar Yaaro Etho Ketka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar Yaaro Etho Ketka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Kuralaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kuralaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ellaam Ketkkirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaam Ketkkirathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeah Yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeah Yeah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaar Yaaro Ethira Thondra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar Yaaro Ethira Thondra"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaar Yaaro Kadanththu Poga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar Yaaro Kadanththu Poga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Pol"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ellaam Therigirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaam Therigirathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaathal Vanthu Kannukkulle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathal Vanthu Kannukkulle"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koodu Kattiyathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koodu Kattiyathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kallai Pola Poovai Vaithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallai Pola Poovai Vaithu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veedu Kattiyathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veedu Kattiyathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaathal Seida Madaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathal Seida Madaiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endru Kaathal Thittiyathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endru Kaathal Thittiyathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kathavai Moodi Vaithapothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathavai Moodi Vaithapothum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jannal Thattiyathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jannal Thattiyathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennamo Nadakkirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennamo Nadakkirathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakku Pidikkirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku Pidikkirathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennamo Nadakkirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennamo Nadakkirathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ellaam Pidikkirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaam Pidikkirathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Paarvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Paarvai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Paarvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Paarvai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athu Malai Mele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu Malai Mele"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yhalai Keezhaai Thallum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yhalai Keezhaai Thallum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maru Paarvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maru Paarvai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maru Paarvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maru Paarvai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athu Meendum Ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu Meendum Ennai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mele Vara Chollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mele Vara Chollum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adada Ennai Maatti Vittaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adada Ennai Maatti Vittaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhagaai Aabaththil Maati Vittaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaai Aabaththil Maati Vittaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tholodu Rekkai Mulaikkuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholodu Rekkai Mulaikkuthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaal Rendum Kaathil Parakkuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaal Rendum Kaathil Parakkuthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ithu Maayam Manthiram Illaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu Maayam Manthiram Illaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Kaathal Tholllai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Kaathal Tholllai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaar Yaaro Etho Pesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar Yaaro Etho Pesa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaar Yaaro Etho Ketka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar Yaaro Etho Ketka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Kuralaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kuralaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ellaam Ketkkirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaam Ketkkirathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeah Yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeah Yeah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaar Yaaro Ethira Thondra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar Yaaro Ethira Thondra"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaar Yaaro Kadanththu Poga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar Yaaro Kadanththu Poga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Pol"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ellaam Therigirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaam Therigirathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muthal Muthalaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthal Muthalaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mella thayangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mella thayangi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nettru Poo Kadaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nettru Poo Kadaiyil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pookal Vaanga Nindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pookal Vaanga Nindren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indru Valaiyal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indru Valaiyal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadai paarthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadai paarthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unthan Kai Alavai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unthan Kai Alavai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaattril Naanum Varainthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattril Naanum Varainthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Per Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Per Enna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaaro Ennai Ketkka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaro Ennai Ketkka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Per Solli Uthattinai Kadithen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Per Solli Uthattinai Kadithen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enga naan Piranthu Vanthathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga naan Piranthu Vanthathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnoda Sernthu Vaazhava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnoda Sernthu Vaazhava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iru Ithayam Ennul Thudikkirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iru Ithayam Ennul Thudikkirathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Intha Kaathalaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Kaathalaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaar Yaaro Etho Pesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar Yaaro Etho Pesa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaar Yaaro Etho Ketka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar Yaaro Etho Ketka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Kuralaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kuralaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ellaam Ketkkirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaam Ketkkirathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeah Yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeah Yeah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaar Yaaro Ethira Thondra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar Yaaro Ethira Thondra"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaar Yaaro Kadanththu Poga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar Yaaro Kadanththu Poga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Pol"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ellaam Therigirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaam Therigirathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennamo Nadakkirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennamo Nadakkirathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakku Pidikkirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku Pidikkirathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennamo Nadakkirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennamo Nadakkirathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ellam Pidikkirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellam Pidikkirathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennamo Nadakkirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennamo Nadakkirathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakku Pidikkirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku Pidikkirathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennamo Nadakkirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennamo Nadakkirathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ellam Pidikkirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellam Pidikkirathe"/>
</div>
</pre>
