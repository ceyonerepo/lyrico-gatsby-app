---
title: "hey rambha rambha song lyrics"
album: "Maha Samudram"
artist: "Chaitan Bharadwaj"
lyricist: "BhaskaraBhatla"
director: "Ajay Bhupathi"
path: "/albums/maha-samudram-lyrics"
song: "Hey Rambha Rambha"
image: ../../images/albumart/maha-samudram.jpg
date: 2021-10-14
lang: telugu
youtubeLink: "https://www.youtube.com/embed/-W3SLhnv4iE"
type: "mass"
singers:
  - Chaitan Bharadwaj
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hey mandhe ika mandhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey mandhe ika mandhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Ishakapatnam beachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishakapatnam beachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thagochhu oogochhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thagochhu oogochhu "/>
</div>
<div class="lyrico-lyrics-wrapper">edhaina cheyochhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edhaina cheyochhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottey jai kottey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottey jai kottey"/>
</div>
<div class="lyrico-lyrics-wrapper">Manamantha rambha fansu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manamantha rambha fansu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattedham bannersu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattedham bannersu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pettedham cut out-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pettedham cut out-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Korra meenu madhiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Korra meenu madhiri"/>
</div>
<div class="lyrico-lyrics-wrapper">Varra varriguntadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varra varriguntadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kurragalla gundeki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurragalla gundeki"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaalam vesthadhiraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaalam vesthadhiraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Erra pedhavi korikithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Erra pedhavi korikithe"/>
</div>
<div class="lyrico-lyrics-wrapper">sarra sari navvithe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sarra sari navvithe "/>
</div>
<div class="lyrico-lyrics-wrapper">burra thirigipothadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="burra thirigipothadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Girra girra girra girra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Girra girra girra girra"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh rambha rambha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh rambha rambha "/>
</div>
<div class="lyrico-lyrics-wrapper">hey rambha hey rambha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey rambha hey rambha"/>
</div>
<div class="lyrico-lyrics-wrapper">Rambha rambha rambha rambha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rambha rambha rambha rambha"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey rambha hey rambha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey rambha hey rambha"/>
</div>
<div class="lyrico-lyrics-wrapper">Pandage prarambha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pandage prarambha"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey rambha hey rambha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey rambha hey rambha"/>
</div>
<div class="lyrico-lyrics-wrapper">Rambha rambha rambha rambha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rambha rambha rambha rambha"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey rambha hey rambha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey rambha hey rambha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ekkadhe gudumba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ekkadhe gudumba"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Soda ice lekunda rendu nintylu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soda ice lekunda rendu nintylu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ganuka peekamanuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ganuka peekamanuko"/>
</div>
<div class="lyrico-lyrics-wrapper">Bodylo rambha dance 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bodylo rambha dance "/>
</div>
<div class="lyrico-lyrics-wrapper">aadeddhiraa mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aadeddhiraa mama"/>
</div>
<div class="lyrico-lyrics-wrapper">Abba eela kottey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Abba eela kottey"/>
</div>
<div class="lyrico-lyrics-wrapper">Soundu pettey pettey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soundu pettey pettey"/>
</div>
<div class="lyrico-lyrics-wrapper">Dance kattey kattey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dance kattey kattey "/>
</div>
<div class="lyrico-lyrics-wrapper">dhumme regalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhumme regalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Poolu posey posey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poolu posey posey"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodi kosey kosey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodi kosey kosey"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey dheeni andam matthu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey dheeni andam matthu "/>
</div>
<div class="lyrico-lyrics-wrapper">mundu samaname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mundu samaname"/>
</div>
<div class="lyrico-lyrics-wrapper">Mungipodam dhukey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mungipodam dhukey"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheeni nadum banasancha dhukaname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheeni nadum banasancha dhukaname"/>
</div>
<div class="lyrico-lyrics-wrapper">Muttukunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muttukunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhi chala chala pramadham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhi chala chala pramadham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sakshath srikrishnude o 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sakshath srikrishnude o "/>
</div>
<div class="lyrico-lyrics-wrapper">velithoti konndanetthade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="velithoti konndanetthade"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey onti chettho anjaneyude
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey onti chettho anjaneyude"/>
</div>
<div class="lyrico-lyrics-wrapper">Mari sanjeevani etthukochhade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mari sanjeevani etthukochhade"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayyababoy manavalla kadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyababoy manavalla kadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Manamanthati goppollam kadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manamanthati goppollam kadu"/>
</div>
<div class="lyrico-lyrics-wrapper">O seesanaina etthakapothe etta mava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O seesanaina etthakapothe etta mava"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey mandhe ika mandhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey mandhe ika mandhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Ishakapatnam beachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishakapatnam beachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thagochhu oogochhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thagochhu oogochhu "/>
</div>
<div class="lyrico-lyrics-wrapper">edhaina cheyochhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edhaina cheyochhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottey jai kottey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottey jai kottey"/>
</div>
<div class="lyrico-lyrics-wrapper">Manamantha rambha fansu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manamantha rambha fansu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattedham bannersu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattedham bannersu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pettedham cut out-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pettedham cut out-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Korra meenu madhiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Korra meenu madhiri"/>
</div>
<div class="lyrico-lyrics-wrapper">Varra varriguntadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varra varriguntadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kurragalla gundeki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurragalla gundeki"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaalam vesthadhiraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaalam vesthadhiraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Erra pedhavi korikithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Erra pedhavi korikithe"/>
</div>
<div class="lyrico-lyrics-wrapper">sarra sari navvithe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sarra sari navvithe "/>
</div>
<div class="lyrico-lyrics-wrapper">burra thirigipothadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="burra thirigipothadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Girra girra girra girra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Girra girra girra girra"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh rambha rambha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh rambha rambha "/>
</div>
<div class="lyrico-lyrics-wrapper">hey rambha hey rambha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey rambha hey rambha"/>
</div>
<div class="lyrico-lyrics-wrapper">Rambha rambha rambha rambha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rambha rambha rambha rambha"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey rambha hey rambha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey rambha hey rambha"/>
</div>
<div class="lyrico-lyrics-wrapper">Pandage prarambha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pandage prarambha"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey rambha hey rambha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey rambha hey rambha"/>
</div>
<div class="lyrico-lyrics-wrapper">Rambha rambha rambha rambha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rambha rambha rambha rambha"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey rambha hey rambha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey rambha hey rambha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ekkadhe gudumba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ekkadhe gudumba"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey mandhe ika mandhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey mandhe ika mandhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Ishakapatnam beachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishakapatnam beachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thagochhu oogochhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thagochhu oogochhu "/>
</div>
<div class="lyrico-lyrics-wrapper">edhaina cheyochhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edhaina cheyochhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottey jai kottey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottey jai kottey"/>
</div>
<div class="lyrico-lyrics-wrapper">Manamantha rambha fansu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manamantha rambha fansu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattedham bannersu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattedham bannersu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pettedham cut out-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pettedham cut out-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Korra meenu madhiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Korra meenu madhiri"/>
</div>
<div class="lyrico-lyrics-wrapper">Varra varriguntadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varra varriguntadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kurragalla gundeki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurragalla gundeki"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaalam vesthadhiraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaalam vesthadhiraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Erra pedhavi korikithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Erra pedhavi korikithe"/>
</div>
<div class="lyrico-lyrics-wrapper">sarra sari navvithe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sarra sari navvithe "/>
</div>
<div class="lyrico-lyrics-wrapper">burra thirigipothadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="burra thirigipothadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Girra girra girra girra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Girra girra girra girra"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh rambha rambha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh rambha rambha "/>
</div>
<div class="lyrico-lyrics-wrapper">hey rambha hey rambha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey rambha hey rambha"/>
</div>
<div class="lyrico-lyrics-wrapper">Rambha rambha rambha rambha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rambha rambha rambha rambha"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey rambha hey rambha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey rambha hey rambha"/>
</div>
<div class="lyrico-lyrics-wrapper">Pandage prarambha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pandage prarambha"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey rambha hey rambha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey rambha hey rambha"/>
</div>
<div class="lyrico-lyrics-wrapper">Rambha rambha rambha rambha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rambha rambha rambha rambha"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey rambha hey rambha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey rambha hey rambha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ekkadhe gudumba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ekkadhe gudumba"/>
</div>
</pre>
