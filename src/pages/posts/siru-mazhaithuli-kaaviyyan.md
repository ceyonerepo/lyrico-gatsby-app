---
title: "siru mazhaithuli song lyrics"
album: "Kaaviyyan"
artist: "Syam Mohan MM"
lyricist: "Pragaa"
director: "Sarathi"
path: "/albums/kaaviyyan-lyrics"
song: "Siru Mazhaithuli"
image: ../../images/albumart/kaaviyyan.jpg
date: 2019-10-18
lang: tamil
youtubeLink: "https://www.youtube.com/embed/FBKsN98AZ28"
type: "happy"
singers:
  - Vandana Dhivyaraja
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">siru mazhaithuli serthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="siru mazhaithuli serthu"/>
</div>
<div class="lyrico-lyrics-wrapper">idi idika ohr
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idi idika ohr"/>
</div>
<div class="lyrico-lyrics-wrapper">minnalai varave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="minnalai varave"/>
</div>
<div class="lyrico-lyrics-wrapper">trending tweetai maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="trending tweetai maari"/>
</div>
<div class="lyrico-lyrics-wrapper">vazhi maranthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vazhi maranthu"/>
</div>
<div class="lyrico-lyrics-wrapper">google map-il tholaivom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="google map-il tholaivom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">siru mazhaithuli serthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="siru mazhaithuli serthu"/>
</div>
<div class="lyrico-lyrics-wrapper">idi idika ohr
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idi idika ohr"/>
</div>
<div class="lyrico-lyrics-wrapper">minnalai varave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="minnalai varave"/>
</div>
<div class="lyrico-lyrics-wrapper">trending tweetai maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="trending tweetai maari"/>
</div>
<div class="lyrico-lyrics-wrapper">vazhi maranthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vazhi maranthu"/>
</div>
<div class="lyrico-lyrics-wrapper">google map-il tholaivom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="google map-il tholaivom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">modern pennam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="modern pennam"/>
</div>
<div class="lyrico-lyrics-wrapper">thoduvaanam thooram illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoduvaanam thooram illai"/>
</div>
<div class="lyrico-lyrics-wrapper">raadhai naana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="raadhai naana"/>
</div>
<div class="lyrico-lyrics-wrapper">ada poda midhapen vinnil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada poda midhapen vinnil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">modern pennam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="modern pennam"/>
</div>
<div class="lyrico-lyrics-wrapper">thoduvaanam thooram illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoduvaanam thooram illai"/>
</div>
<div class="lyrico-lyrics-wrapper">raadhai naana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="raadhai naana"/>
</div>
<div class="lyrico-lyrics-wrapper">ada poda midhapen vinnil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada poda midhapen vinnil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">naamum ini naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naamum ini naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">ingu yaarum adimai illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ingu yaarum adimai illai"/>
</div>
<div class="lyrico-lyrics-wrapper">thadaiyo perum thadaiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thadaiyo perum thadaiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">let's brak it break it bab
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="let's brak it break it bab"/>
</div>
<div class="lyrico-lyrics-wrapper">poovom kadal thandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poovom kadal thandi"/>
</div>
<div class="lyrico-lyrics-wrapper">pudhu nation thedi vaazha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pudhu nation thedi vaazha"/>
</div>
<div class="lyrico-lyrics-wrapper">humming bird pole 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="humming bird pole "/>
</div>
<div class="lyrico-lyrics-wrapper">nedu vaanil siragadipom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nedu vaanil siragadipom"/>
</div>
<div class="lyrico-lyrics-wrapper">leteratureril new grammarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="leteratureril new grammarai"/>
</div>
<div class="lyrico-lyrics-wrapper">naam serkalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naam serkalam"/>
</div>
<div class="lyrico-lyrics-wrapper">vinmeengalai oliveesave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vinmeengalai oliveesave"/>
</div>
<div class="lyrico-lyrics-wrapper">poradalam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poradalam "/>
</div>
<div class="lyrico-lyrics-wrapper">ini endrume  
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ini endrume  "/>
</div>
<div class="lyrico-lyrics-wrapper">ramp walk naam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ramp walk naam"/>
</div>
<div class="lyrico-lyrics-wrapper">nadai poduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nadai poduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">raajali naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="raajali naan"/>
</div>
<div class="lyrico-lyrics-wrapper">veezhven ena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veezhven ena"/>
</div>
<div class="lyrico-lyrics-wrapper">ninaithaayo da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninaithaayo da"/>
</div>
<div class="lyrico-lyrics-wrapper">kaaviyyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaaviyyan"/>
</div>
<div class="lyrico-lyrics-wrapper">sarithirathai thedi par
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sarithirathai thedi par"/>
</div>
<div class="lyrico-lyrics-wrapper">safeguard
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="safeguard"/>
</div>
<div class="lyrico-lyrics-wrapper">pengalai seidhadhaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pengalai seidhadhaar"/>
</div>
<div class="lyrico-lyrics-wrapper">engum iniyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engum iniyum"/>
</div>
<div class="lyrico-lyrics-wrapper">feminism pesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="feminism pesa"/>
</div>
<div class="lyrico-lyrics-wrapper">kanda udane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanda udane"/>
</div>
<div class="lyrico-lyrics-wrapper">thurathi adika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thurathi adika"/>
</div>
<div class="lyrico-lyrics-wrapper">strangers storm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="strangers storm"/>
</div>
<div class="lyrico-lyrics-wrapper">yendragidum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yendragidum "/>
</div>
<div class="lyrico-lyrics-wrapper">idhu kaaviyyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhu kaaviyyan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">modern pennam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="modern pennam"/>
</div>
<div class="lyrico-lyrics-wrapper">thoduvaanam thooram illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoduvaanam thooram illai"/>
</div>
<div class="lyrico-lyrics-wrapper">raadhai naana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="raadhai naana"/>
</div>
<div class="lyrico-lyrics-wrapper">ada poda midhapen vinnil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada poda midhapen vinnil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">modern pennam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="modern pennam"/>
</div>
<div class="lyrico-lyrics-wrapper">thoduvaanam thooram illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoduvaanam thooram illai"/>
</div>
<div class="lyrico-lyrics-wrapper">raadhai naana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="raadhai naana"/>
</div>
<div class="lyrico-lyrics-wrapper">ada poda midhapen vinnil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada poda midhapen vinnil"/>
</div>
</pre>
