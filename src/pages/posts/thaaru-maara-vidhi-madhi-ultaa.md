---
title: "thaaru maara song lyrics"
album: "Vidhi Madhi Ultaa"
artist: "Ashwin Vinayagamoorthy"
lyricist: "Kabilan"
director: "Vijai Balaji"
path: "/albums/vidhi-madhi-ultaa-lyrics"
song: "Thaaru Maara"
image: ../../images/albumart/vidhi-madhi-ultaa.jpg
date: 2018-01-05
lang: tamil
youtubeLink: "https://www.youtube.com/embed/4sCJXvzBWWg"
type: "love"
singers:
  - G.V. Prakash Kumar
  - Kausthub Ravi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kaadhal oru koothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal oru koothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi aadu kai korthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi aadu kai korthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarum ariyaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarum ariyaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Neram paathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neram paathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey kaadhal oru koothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey kaadhal oru koothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi aadu kai korthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi aadu kai korthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarum ariyaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarum ariyaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Neram andha neram paathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neram andha neram paathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaaru maara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaru maara"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru paarva paaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru paarva paaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Nerumaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nerumaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna adichu thooka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna adichu thooka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Heythaaru maara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heythaaru maara"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru paarva paaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru paarva paaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Neerumaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neerumaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna adichu thooka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna adichu thooka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chindaripetta nandoda pudiyula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chindaripetta nandoda pudiyula"/>
</div>
<div class="lyrico-lyrics-wrapper">Maatikita maatikita machan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maatikita maatikita machan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannadi meena pinnadi ponaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannadi meena pinnadi ponaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannathula kannathula vecha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannathula kannathula vecha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chindaripetta nandoda pudiyula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chindaripetta nandoda pudiyula"/>
</div>
<div class="lyrico-lyrics-wrapper">Maatikita maatikita machan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maatikita maatikita machan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannadi meena pinnadi ponaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannadi meena pinnadi ponaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannathula kannathula vecha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannathula kannathula vecha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tholaivil needhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholaivil needhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholaindhen naandhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholaindhen naandhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nizhalaai thodava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nizhalaai thodava"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilavae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Marunaal badhilai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marunaal badhilai"/>
</div>
<div class="lyrico-lyrics-wrapper">Arindhaal indrae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arindhaal indrae"/>
</div>
<div class="lyrico-lyrics-wrapper">Alaiyaai kudhipen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alaiyaai kudhipen"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaniya naa thaniya thavikkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaniya naa thaniya thavikkuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenava un nenava kedakkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenava un nenava kedakkuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Rayila nee rayila kadakkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rayila nee rayila kadakkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Muraiyaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muraiyaadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaniya naa thaniya thavikkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaniya naa thaniya thavikkuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenava un nenava kedakkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenava un nenava kedakkuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Rayila nee rayila kadakkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rayila nee rayila kadakkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Udan vaa di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udan vaa di"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey kaadhal veetukaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey kaadhal veetukaathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiyil thookadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyil thookadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adiyae naano attakathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiyae naano attakathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan romba paavam di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan romba paavam di"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhala nenjukullae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhala nenjukullae"/>
</div>
<div class="lyrico-lyrics-wrapper">Moota kattadhaeKannakuli kullae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moota kattadhaeKannakuli kullae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai nee kappal ottadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai nee kappal ottadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaaru maara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaru maara"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru paarva paaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru paarva paaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey nerumaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey nerumaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna adichu thooka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna adichu thooka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey thaaru maara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey thaaru maara"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaaru maara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaru maara"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru paarva paaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru paarva paaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru paarva paaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru paarva paaka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey neerumaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey neerumaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Neerumaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neerumaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna adichu nanachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna adichu nanachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thovachu thooka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thovachu thooka"/>
</div>
<div class="lyrico-lyrics-wrapper">Hahaan hahaan hahaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hahaan hahaan hahaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Can I know
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Can I know"/>
</div>
<div class="lyrico-lyrics-wrapper">When you’ll call me baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="When you’ll call me baby"/>
</div>
<div class="lyrico-lyrics-wrapper">What you gonna say
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="What you gonna say"/>
</div>
<div class="lyrico-lyrics-wrapper">When you get a little crazy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="When you get a little crazy"/>
</div>
<div class="lyrico-lyrics-wrapper">You tell me what now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You tell me what now"/>
</div>
<div class="lyrico-lyrics-wrapper">Don’t hate now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Don’t hate now"/>
</div>
<div class="lyrico-lyrics-wrapper">I’m gonna break down
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I’m gonna break down"/>
</div>
<div class="lyrico-lyrics-wrapper">Just don’t
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Just don’t"/>
</div>
<div class="lyrico-lyrics-wrapper">Make a clown
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Make a clown"/>
</div>
<div class="lyrico-lyrics-wrapper">Whaat
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Whaat"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh you know
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh you know"/>
</div>
<div class="lyrico-lyrics-wrapper">You so hot now baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You so hot now baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Got nerve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Got nerve"/>
</div>
<div class="lyrico-lyrics-wrapper">But you’re so high class baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="But you’re so high class baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Much nonsense
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Much nonsense"/>
</div>
<div class="lyrico-lyrics-wrapper">It’s in my conscience
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="It’s in my conscience"/>
</div>
<div class="lyrico-lyrics-wrapper">I say
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I say"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannama kannama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannama kannama"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennama Ponnama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennama Ponnama"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhalai sollaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalai sollaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Engamma ponamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engamma ponamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal oru koothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal oru koothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi aadu kai korthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi aadu kai korthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarum ariyaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarum ariyaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Neram paathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neram paathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey kaadhal oru koothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey kaadhal oru koothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi aadu kai korthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi aadu kai korthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarum ariyaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarum ariyaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Neram andha neram paathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neram andha neram paathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaaru maara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaru maara"/>
</div>
</pre>
