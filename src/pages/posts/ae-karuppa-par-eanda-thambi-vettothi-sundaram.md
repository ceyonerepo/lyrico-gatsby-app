---
title: "ae karuppa par eanda song lyrics"
album: "Thambi Vettothi Sundaram"
artist: "Vidyasagar"
lyricist: "Vairamuthu"
director: "V.C. Vadivudaiyan"
path: "/albums/thambi-vettothi-sundaram-lyrics"
song: "Ae Karuppa Par Eanda"
image: ../../images/albumart/thambi-vettothi-sundaram.jpg
date: 2011-11-11
lang: tamil
youtubeLink: "https://www.youtube.com/embed/gKycTwEoCEk"
type: "happy"
singers:
  - Velmurugan
  - Sujithra
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yae karuppaa paaraendaa dhanushkodi lady
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yae karuppaa paaraendaa dhanushkodi lady"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kappangattum podhu en kallaappetti kodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kappangattum podhu en kallaappetti kodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ae karuppaa paaraendaa dhanushkodi lady
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae karuppaa paaraendaa dhanushkodi lady"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kappangattum podhu en kallaappetti kodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kappangattum podhu en kallaappetti kodi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yae saaraayatha vittaa oru saami illaiyadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yae saaraayatha vittaa oru saami illaiyadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandhoshaththa vittaa ingae bhoomi illaiyadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhoshaththa vittaa ingae bhoomi illaiyadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan yekki yekki paakkuradhu enna jolikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan yekki yekki paakkuradhu enna jolikku"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee yekku thappaa vachirukka ellaam jollykku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee yekku thappaa vachirukka ellaam jollykku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Note alli thandhaa naan seettu thaaraen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Note alli thandhaa naan seettu thaaraen"/>
</div>
<div class="lyrico-lyrics-wrapper">Roottellaam pottu naan kaettu thaaraen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Roottellaam pottu naan kaettu thaaraen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yae karuppaa paaraendaa dhanushkodi lady
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yae karuppaa paaraendaa dhanushkodi lady"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kappangattum podhu en kallaappetti kodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kappangattum podhu en kallaappetti kodi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanni ponnum Indha boomiyum onnu thaanunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanni ponnum Indha boomiyum onnu thaanunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Maedu pallam ada rendilum undu paarunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maedu pallam ada rendilum undu paarunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanni ponnum Indha boomiyum onnu thaanunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanni ponnum Indha boomiyum onnu thaanunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Maedu pallam ada rendilum undu paarunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maedu pallam ada rendilum undu paarunga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ponnu madiyil naan poonakkutti paaththukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponnu madiyil naan poonakkutti paaththukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Pozhudhu vidinjaa yenna puyalukkitta kaettukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pozhudhu vidinjaa yenna puyalukkitta kaettukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Guru maedu thottu paaru kurumboadu muththam podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Guru maedu thottu paaru kurumboadu muththam podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee ariyaadha thappukkellaam mariyaadhai ingae thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee ariyaadha thappukkellaam mariyaadhai ingae thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haei nottaththaan paaththaa naan nottu thaaraen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haei nottaththaan paaththaa naan nottu thaaraen"/>
</div>
<div class="lyrico-lyrics-wrapper">Pokkuthaan paaththu naan pottu thaaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pokkuthaan paaththu naan pottu thaaren"/>
</div>
<div class="lyrico-lyrics-wrapper">Allaek allaek allaek aiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allaek allaek allaek aiyo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yae karuppaa paaraendaa dhanushkodi lady
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yae karuppaa paaraendaa dhanushkodi lady"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kappangattum podhu en kallaappetti kodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kappangattum podhu en kallaappetti kodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aamaa yaei podu inthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aamaa yaei podu inthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaei appudi podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaei appudi podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aamaa adhaan aiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aamaa adhaan aiyo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maelu valichaa konjam patta thanni oothikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maelu valichaa konjam patta thanni oothikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Boadhai koranjaa enna maela keezha paaththukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boadhai koranjaa enna maela keezha paaththukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Maelu valichaa konjam patta thanni oothikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maelu valichaa konjam patta thanni oothikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Boadhai koranjaa enna maela keezha paaththukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boadhai koranjaa enna maela keezha paaththukka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sila naeram nenju thattu kettu alaiyudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sila naeram nenju thattu kettu alaiyudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponna paaththaa angae ottikkittu orangudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponna paaththaa angae ottikkittu orangudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unakkaachu yenakkaachu selavellaam varavaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkaachu yenakkaachu selavellaam varavaachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee thaalaadha thunbam ellaam thanni vittu aaththikko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thaalaadha thunbam ellaam thanni vittu aaththikko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haei nottaththaan paaththaa naan nottu thaaraen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haei nottaththaan paaththaa naan nottu thaaraen"/>
</div>
<div class="lyrico-lyrics-wrapper">Pokkuthaan paaththu naan pottu thaaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pokkuthaan paaththu naan pottu thaaren"/>
</div>
<div class="lyrico-lyrics-wrapper">Aei vaadi pulla haei chinna chittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aei vaadi pulla haei chinna chittu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hei aadhi naalil vaanam vandhadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei aadhi naalil vaanam vandhadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanam thorandhu kaaththu vandhadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam thorandhu kaaththu vandhadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaaththil irundhu neerum vandhadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaththil irundhu neerum vandhadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeril irundhu uyirum vandhadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeril irundhu uyirum vandhadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyiril irundhu korangu vandhadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyiril irundhu korangu vandhadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kurangil irundhu manushan porandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurangil irundhu manushan porandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Manushan porandhu sondham vandhadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manushan porandhu sondham vandhadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sondham porandhu sandai vandhadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sondham porandhu sandai vandhadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandai vandhadhum kaayam vandhadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandai vandhadhum kaayam vandhadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaayam vandhadhum kavalai vandhadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaayam vandhadhum kavalai vandhadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavalai vandhadhum boadhai vandhadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavalai vandhadhum boadhai vandhadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Boadhai vandhadhum buththi maarudhadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boadhai vandhadhum buththi maarudhadaa"/>
</div>
</pre>
