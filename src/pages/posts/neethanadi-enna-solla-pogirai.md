---
title: "neethanadi song lyrics"
album: "Enna Solla Pogirai"
artist: "Vivek – Mervin"
lyricist: "Prakash Francis"
director: "A. Hariharan"
path: "/albums/enna-solla-pogirai-lyrics"
song: "Neethanadi"
image: ../../images/albumart/enna-solla-pogirai.jpg
date: 2022-01-13
lang: tamil
youtubeLink: "https://www.youtube.com/embed/DGXMl3tdSFY"
type: "melody"
singers:
  - Mervin Solomon
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Unakkaaga thaane naan uyir vaazhgiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkaaga thaane naan uyir vaazhgiren"/>
</div>
<div class="lyrico-lyrics-wrapper">En uyir naadi neethaanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En uyir naadi neethaanadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee illai endral naan ennaguven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee illai endral naan ennaguven"/>
</div>
<div class="lyrico-lyrics-wrapper">En swasam neethaanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En swasam neethaanadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unakkaaga thaane naan uyir vaazhgiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkaaga thaane naan uyir vaazhgiren"/>
</div>
<div class="lyrico-lyrics-wrapper">En uyir naadi neethaanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En uyir naadi neethaanadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee illai endral naan ennaguven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee illai endral naan ennaguven"/>
</div>
<div class="lyrico-lyrics-wrapper">En swasam neethaanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En swasam neethaanadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En ullam nee vandhu udaithaalum kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En ullam nee vandhu udaithaalum kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Udayamal unnai en uyirai kaapaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udayamal unnai en uyirai kaapaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennalum nee ennai veruthalum kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennalum nee ennai veruthalum kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Neengamal nirkum un ninaivil vaazhven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neengamal nirkum un ninaivil vaazhven"/>
</div>
<div class="lyrico-lyrics-wrapper">Ketkintra isai ellaam neethaanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketkintra isai ellaam neethaanadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan parkintra thisai ellam neethanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan parkintra thisai ellam neethanadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi naan patta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi naan patta"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaayangal azhinthalume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaayangal azhinthalume"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada naan konda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada naan konda"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal azhiyaathadee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal azhiyaathadee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unakkaaga thaane naan uyir vaazhgiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkaaga thaane naan uyir vaazhgiren"/>
</div>
<div class="lyrico-lyrics-wrapper">En uyir naadi neethaanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En uyir naadi neethaanadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee illai endral naan ennaguven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee illai endral naan ennaguven"/>
</div>
<div class="lyrico-lyrics-wrapper">En swasam neethaanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En swasam neethaanadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unakkaaga thaane naan uyir vaazhgiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkaaga thaane naan uyir vaazhgiren"/>
</div>
<div class="lyrico-lyrics-wrapper">En uyir naadi neethaanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En uyir naadi neethaanadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee illai endral naan ennaguven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee illai endral naan ennaguven"/>
</div>
<div class="lyrico-lyrics-wrapper">En swasam neethaanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En swasam neethaanadi"/>
</div>
</pre>
