---
title: "azhagiya theeye song lyrics"
album: "Minnale"
artist: "Harris Jayaraj"
lyricist: "Vaali"
director: "Gautham Menon"
path: "/albums/minnale-lyrics"
song: "Azhagiya Theeye"
image: ../../images/albumart/minnale.jpg
date: 2001-02-02
lang: tamil
youtubeLink: "https://www.youtube.com/embed/d1r_edJ1xo0"
type: "love"
singers:
  - Harish Raghavendra
  - Timmy
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ae Azhagiya Theeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Azhagiya Theeye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Vaattugiraaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Vaattugiraaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Haikku Kavaithai Vizhigalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Haikku Kavaithai Vizhigalil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Paada Paada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Paada Paada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Hyper Tension Thalaikkeruthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Hyper Tension Thalaikkeruthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanum Vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum Vaada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ae Azhagiya Theeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Azhagiya Theeye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Vaattugiraaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Vaattugiraaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Haikku Kavaithai Vizhigalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Haikku Kavaithai Vizhigalil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Paada Paada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Paada Paada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Hyper Tension Thalaikkeruthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Hyper Tension Thalaikkeruthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanum Vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum Vaada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paavaigal Unakkoru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paavaigal Unakkoru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Allergy Adaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allergy Adaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avalai Paarthathum Unakkulley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avalai Paarthathum Unakkulley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Energy Adaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Energy Adaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Yetho Seithu Vittaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Yetho Seithu Vittaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Come On Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Come On Baby"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dont Do This Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dont Do This Baby"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjai Poo Pol Koithu Vittaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjai Poo Pol Koithu Vittaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjai Poo Pol Koithu Vittaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjai Poo Pol Koithu Vittaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varavey illai Urakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varavey illai Urakkam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Atharkkum illai Irakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atharkkum illai Irakkam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Imaigal Ondraaga Eppothum Seraamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imaigal Ondraaga Eppothum Seraamal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idaiyil Nindraaye Ithu Niyayamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idaiyil Nindraaye Ithu Niyayamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bp Yeri Pochchu ila Raththam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bp Yeri Pochchu ila Raththam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjil Kaargil Pola Oru Yuththam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil Kaargil Pola Oru Yuththam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi Artha Raaththiri Sammam Maathiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Artha Raaththiri Sammam Maathiri"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veppam Paarkuthadi Kanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veppam Paarkuthadi Kanne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennil Minnal Thaakkuthey Theeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennil Minnal Thaakkuthey Theeyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaatrum Ondru Sernthatho Unnil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatrum Ondru Sernthatho Unnil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Ennai Suttathum Manalil Ittathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Ennai Suttathum Manalil Ittathum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Intha Mattilum Po Po Pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Mattilum Po Po Pothum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ae Theeye Azhagiya Theeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Theeye Azhagiya Theeye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Vaattugiraaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Vaattugiraaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Haikku Kavaithai Vizhigalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Haikku Kavaithai Vizhigalil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Thaan Paada Paada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Thaan Paada Paada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Hyper Tension Thalaikkeruthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Hyper Tension Thalaikkeruthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanum Vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum Vaada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Never Do This To Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Never Do This To Me"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dont Ever Do This To Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dont Ever Do This To Me"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Peyar Solli Solli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Peyar Solli Solli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennaiye Naan Maranthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaiye Naan Maranthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Missile Paarvaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Missile Paarvaiyil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennuyir Naan Tholaiththen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennuyir Naan Tholaiththen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Peyar Solli Solli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Peyar Solli Solli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennaiye Naan Maranthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaiye Naan Maranthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Missile Paarvaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Missile Paarvaiyil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennuyir Naan Tholaiththen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennuyir Naan Tholaiththen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uthatttil Unthan Peyarthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uthatttil Unthan Peyarthaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Udalil Unthan Uyirthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udalil Unthan Uyirthaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nilaththil Nindraalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilaththil Nindraalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Yengu Sendraalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Yengu Sendraalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Unnai Thodargindra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Unnai Thodargindra"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nizhal Allavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nizhal Allavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhal Piththu Yeri Manam Kaththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Piththu Yeri Manam Kaththa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avalai Sekku Pola Nee Suththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avalai Sekku Pola Nee Suththa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Konjam Konjamaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Konjam Konjamaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thundam Thundamaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thundam Thundamaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kondru Pottaathu Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondru Pottaathu Enna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kodi Minnal Thakkiya Dhegam Yaavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodi Minnal Thakkiya Dhegam Yaavum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Minnal Polave Minna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minnal Polave Minna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Ennai Ennidam illai Endruthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Ennai Ennidam illai Endruthaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Penne Unnidam Anbai Theda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Penne Unnidam Anbai Theda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ae Theeye Azhagiya Theeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Theeye Azhagiya Theeye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Vaatugiraaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Vaatugiraaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Haikku Kavaithai Vizhigalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Haikku Kavaithai Vizhigalil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Thaan Paada Paada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Thaan Paada Paada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Hyper Tension Thalaikkeruthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Hyper Tension Thalaikkeruthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanum Vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum Vaada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paavaigal Unakkoru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paavaigal Unakkoru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Allergy Adaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allergy Adaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avalai Paarthathum Unakkulley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avalai Paarthathum Unakkulley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Energy Adaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Energy Adaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Yetho Seithu Vittaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Yetho Seithu Vittaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Come On Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Come On Baby"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dont Do This Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dont Do This Baby"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjai Poo Pol Koithu Vittaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjai Poo Pol Koithu Vittaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjai Poo Pol Koithu Vittaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjai Poo Pol Koithu Vittaal"/>
</div>
</pre>
