---
title: "pattu pattu song lyrics"
album: "Pei Vaala Pudicha Kadha"
artist: "Arun Prasath"
lyricist: "Raasi Manivasakam "
director: "Raasi Manivasakam "
path: "/albums/pei-vaala-pudicha-kadha-lyrics"
song: "Pattu Pattu"
image: ../../images/albumart/pei-vaala-pudicha-kadha.jpg
date: 2019-11-22
lang: tamil
youtubeLink: "https://www.youtube.com/embed/xaB2oaRVM3I"
type: "happy"
singers:
  - VM Mahalingam
  - Lincy
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">pattu pattu meni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pattu pattu meni"/>
</div>
<div class="lyrico-lyrics-wrapper">nan paraseega rani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan paraseega rani"/>
</div>
<div class="lyrico-lyrics-wrapper">thottu pesa kitta vantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thottu pesa kitta vantha"/>
</div>
<div class="lyrico-lyrics-wrapper">kotugira theni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kotugira theni"/>
</div>
<div class="lyrico-lyrics-wrapper">thunija vaa nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thunija vaa nee"/>
</div>
<div class="lyrico-lyrics-wrapper">kethu kethu than di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kethu kethu than di"/>
</div>
<div class="lyrico-lyrics-wrapper">en sothu romba than di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en sothu romba than di"/>
</div>
<div class="lyrico-lyrics-wrapper">thottu pesa kitta vantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thottu pesa kitta vantha"/>
</div>
<div class="lyrico-lyrics-wrapper">alli tharuven di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alli tharuven di"/>
</div>
<div class="lyrico-lyrics-wrapper">solli tharuven di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="solli tharuven di"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">margali na kulir adikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="margali na kulir adikum"/>
</div>
<div class="lyrico-lyrics-wrapper">kodai yina verthu irukum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kodai yina verthu irukum"/>
</div>
<div class="lyrico-lyrics-wrapper">rendum onna sernthavada nanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rendum onna sernthavada nanu"/>
</div>
<div class="lyrico-lyrics-wrapper">en thoondilukum thevaiyada meenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en thoondilukum thevaiyada meenu"/>
</div>
<div class="lyrico-lyrics-wrapper">neeyum veenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeyum veenu"/>
</div>
<div class="lyrico-lyrics-wrapper">margali na kulir adikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="margali na kulir adikum"/>
</div>
<div class="lyrico-lyrics-wrapper">kodai yina verthu irukum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kodai yina verthu irukum"/>
</div>
<div class="lyrico-lyrics-wrapper">rendum onna sernthavada nanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rendum onna sernthavada nanu"/>
</div>
<div class="lyrico-lyrics-wrapper">en thoondilukum thevaiyada meenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en thoondilukum thevaiyada meenu"/>
</div>
<div class="lyrico-lyrics-wrapper">neeyum veenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeyum veenu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thanthoori chicken vanjara meenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanthoori chicken vanjara meenu"/>
</div>
<div class="lyrico-lyrics-wrapper">munthiri parupu patham pista 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munthiri parupu patham pista "/>
</div>
<div class="lyrico-lyrics-wrapper">kaada kari pran fry 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaada kari pran fry "/>
</div>
<div class="lyrico-lyrics-wrapper">side dish ellam konda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="side dish ellam konda"/>
</div>
1000 <div class="lyrico-lyrics-wrapper">rs tips tharen indha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rs tips tharen indha"/>
</div>
<div class="lyrico-lyrics-wrapper">mathathu unda athaiyum konda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mathathu unda athaiyum konda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ye thanthoori chicken vanjara meenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ye thanthoori chicken vanjara meenu"/>
</div>
<div class="lyrico-lyrics-wrapper">munthiri parupu patham pista 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munthiri parupu patham pista "/>
</div>
<div class="lyrico-lyrics-wrapper">kaada kari pran fry 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaada kari pran fry "/>
</div>
<div class="lyrico-lyrics-wrapper">side dish ellam konda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="side dish ellam konda"/>
</div>
1000 <div class="lyrico-lyrics-wrapper">rs tips tharen indha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rs tips tharen indha"/>
</div>
<div class="lyrico-lyrics-wrapper">mathathu unda athaiyum konda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mathathu unda athaiyum konda"/>
</div>
<div class="lyrico-lyrics-wrapper">athaiyum konda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athaiyum konda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">seesava thooki eri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seesava thooki eri"/>
</div>
<div class="lyrico-lyrics-wrapper">pesama sapudu ne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pesama sapudu ne"/>
</div>
<div class="lyrico-lyrics-wrapper">common common paiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="common common paiya"/>
</div>
<div class="lyrico-lyrics-wrapper">nan kanni ponnu meiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan kanni ponnu meiya"/>
</div>
<div class="lyrico-lyrics-wrapper">nattu nala therntha vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nattu nala therntha vaada"/>
</div>
<div class="lyrico-lyrics-wrapper">pottu poova killi poda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pottu poova killi poda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ettu poda route thadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ettu poda route thadi"/>
</div>
<div class="lyrico-lyrics-wrapper">left um right um kooti podi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="left um right um kooti podi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nan kathu tharen 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan kathu tharen "/>
</div>
<div class="lyrico-lyrics-wrapper">kathu tharen puthusa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathu tharen puthusa"/>
</div>
<div class="lyrico-lyrics-wrapper">vanthu kathuka da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanthu kathuka da"/>
</div>
<div class="lyrico-lyrics-wrapper">kathuka da thinusa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathuka da thinusa"/>
</div>
<div class="lyrico-lyrics-wrapper">nan kathu tharen 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan kathu tharen "/>
</div>
<div class="lyrico-lyrics-wrapper">kathu tharen puthusa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathu tharen puthusa"/>
</div>
<div class="lyrico-lyrics-wrapper">vanthu kathuka da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanthu kathuka da"/>
</div>
<div class="lyrico-lyrics-wrapper">kathuka da thinusa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathuka da thinusa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">common common paiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="common common paiya"/>
</div>
<div class="lyrico-lyrics-wrapper">nan kanni ponnu meiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan kanni ponnu meiya"/>
</div>
<div class="lyrico-lyrics-wrapper">common common paiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="common common paiya"/>
</div>
<div class="lyrico-lyrics-wrapper">nan kanni ponnu meiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan kanni ponnu meiya"/>
</div>
<div class="lyrico-lyrics-wrapper">common common paiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="common common paiya"/>
</div>
<div class="lyrico-lyrics-wrapper">nan kanni ponnu meiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan kanni ponnu meiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">rupaya vangika di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rupaya vangika di"/>
</div>
<div class="lyrico-lyrics-wrapper">machana thangi pudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="machana thangi pudi"/>
</div>
<div class="lyrico-lyrics-wrapper">intha intha note
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha intha note"/>
</div>
<div class="lyrico-lyrics-wrapper">nee vitha ellam kaatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee vitha ellam kaatu"/>
</div>
<div class="lyrico-lyrics-wrapper">katta bomma meesa kaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="katta bomma meesa kaara"/>
</div>
<div class="lyrico-lyrics-wrapper">kutti rani katti poda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kutti rani katti poda"/>
</div>
<div class="lyrico-lyrics-wrapper">katta bomma meesa kaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="katta bomma meesa kaara"/>
</div>
<div class="lyrico-lyrics-wrapper">kutti rani katti poda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kutti rani katti poda"/>
</div>
<div class="lyrico-lyrics-wrapper">en aasa ellam aasa ellam perusa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en aasa ellam aasa ellam perusa"/>
</div>
<div class="lyrico-lyrics-wrapper">ena thooki poda thooki poda mulusa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ena thooki poda thooki poda mulusa"/>
</div>
<div class="lyrico-lyrics-wrapper">en aasa ellam aasa ellam perusa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en aasa ellam aasa ellam perusa"/>
</div>
<div class="lyrico-lyrics-wrapper">ena thooki poda thooki poda mulusa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ena thooki poda thooki poda mulusa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thotta petta pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thotta petta pola"/>
</div>
<div class="lyrico-lyrics-wrapper">ne thottu puta thala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ne thottu puta thala"/>
</div>
<div class="lyrico-lyrics-wrapper">antha thotta petta pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antha thotta petta pola"/>
</div>
<div class="lyrico-lyrics-wrapper">ne thottu puta thala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ne thottu puta thala"/>
</div>
<div class="lyrico-lyrics-wrapper">antha thotta petta pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antha thotta petta pola"/>
</div>
<div class="lyrico-lyrics-wrapper">ne thottu puta thala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ne thottu puta thala"/>
</div>
<div class="lyrico-lyrics-wrapper">atta poochi pola nan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="atta poochi pola nan"/>
</div>
<div class="lyrico-lyrics-wrapper">otikiten mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="otikiten mela"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pattu pattu meni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pattu pattu meni"/>
</div>
<div class="lyrico-lyrics-wrapper">nan paraseega rani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan paraseega rani"/>
</div>
<div class="lyrico-lyrics-wrapper">thottu pesa kitta vantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thottu pesa kitta vantha"/>
</div>
<div class="lyrico-lyrics-wrapper">kotugira theni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kotugira theni"/>
</div>
<div class="lyrico-lyrics-wrapper">thunija vaa nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thunija vaa nee"/>
</div>
<div class="lyrico-lyrics-wrapper">kethu kethu than di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kethu kethu than di"/>
</div>
<div class="lyrico-lyrics-wrapper">en sothu romba than di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en sothu romba than di"/>
</div>
<div class="lyrico-lyrics-wrapper">thottu pesa kitta vantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thottu pesa kitta vantha"/>
</div>
<div class="lyrico-lyrics-wrapper">alli tharuven di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alli tharuven di"/>
</div>
<div class="lyrico-lyrics-wrapper">solli tharuven di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="solli tharuven di"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">margali na kulir adikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="margali na kulir adikum"/>
</div>
<div class="lyrico-lyrics-wrapper">kodai yina verthu irukum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kodai yina verthu irukum"/>
</div>
<div class="lyrico-lyrics-wrapper">rendum onna sernthavada nanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rendum onna sernthavada nanu"/>
</div>
<div class="lyrico-lyrics-wrapper">en thoondilukum thevaiyada meenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en thoondilukum thevaiyada meenu"/>
</div>
<div class="lyrico-lyrics-wrapper">neeyum veenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeyum veenu"/>
</div>
<div class="lyrico-lyrics-wrapper">margali na kulir adikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="margali na kulir adikum"/>
</div>
<div class="lyrico-lyrics-wrapper">kodai yina verthu irukum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kodai yina verthu irukum"/>
</div>
<div class="lyrico-lyrics-wrapper">rendum onna sernthavada nanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rendum onna sernthavada nanu"/>
</div>
<div class="lyrico-lyrics-wrapper">en thoondilukum thevaiyada meenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en thoondilukum thevaiyada meenu"/>
</div>
<div class="lyrico-lyrics-wrapper">podatha seenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="podatha seenu"/>
</div>
</pre>
