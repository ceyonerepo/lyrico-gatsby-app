---
title: "nilavu paatu nilavu paatu song lyrics"
album: "Kannukkul Nilavu"
artist: "Ilaiyaraaja"
lyricist: "Palani Bharathi"
director: "Fazil"
path: "/albums/kannukkul-nilavu-lyrics"
song: "Nilavu Paatu Nilavu Paatu"
image: ../../images/albumart/kannukkul-nilavu.jpg
date: 2000-01-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Dg-bcMI7rpY"
type: "melody"
singers:
  - Hariharan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nilavu Paattu Nilavu Paattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilavu Paattu Nilavu Paattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oar Naal Ketten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oar Naal Ketten"/>
</div>
<div class="lyrico-lyrics-wrapper">Moongil Kaattil Moongil Kaattil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moongil Kaattil Moongil Kaattil"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalum Padiththen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalum Padiththen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nilavu Paattu Nilavu Paattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilavu Paattu Nilavu Paattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oar Naal Ketten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oar Naal Ketten"/>
</div>
<div class="lyrico-lyrics-wrapper">Moongil Kaattil Moongil Kaattil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moongil Kaattil Moongil Kaattil"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalum Padiththen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalum Padiththen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nilavu Paattu Nilavu Paattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilavu Paattu Nilavu Paattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oar Naal Ketten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oar Naal Ketten"/>
</div>
<div class="lyrico-lyrics-wrapper">Moongil Kaattil Moongil Kaattil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moongil Kaattil Moongil Kaattil"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalum Padiththen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalum Padiththen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andha Isaiyin Ragasiyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Isaiyin Ragasiyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Iru Uyirukku Purindhadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iru Uyirukku Purindhadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Iru Uyirukku Purindhadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iru Uyirukku Purindhadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingu Yaarukkuth Therindhadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingu Yaarukkuth Therindhadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Isaiyil Kalandhu Midhakkum Thendrale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Isaiyil Kalandhu Midhakkum Thendrale"/>
</div>
<div class="lyrico-lyrics-wrapper">Isaiyin Magalai Paarththadhillaiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Isaiyin Magalai Paarththadhillaiyo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nilavu Paattu Nilavu Paattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilavu Paattu Nilavu Paattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oar Naal Ketten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oar Naal Ketten"/>
</div>
<div class="lyrico-lyrics-wrapper">Moongil Kaattil Moongil Kaattil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moongil Kaattil Moongil Kaattil"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalum Padiththen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalum Padiththen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanavugal Varuvadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavugal Varuvadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhigalin Viruppamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhigalin Viruppamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavidhaigal Varuvadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavidhaigal Varuvadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavinjanin Viruppamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavinjanin Viruppamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuyilgalin Iruppidam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuyilgalin Iruppidam"/>
</div>
<div class="lyrico-lyrics-wrapper">Isaiyaal Ariyalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Isaiyaal Ariyalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Malarndhidum Malargalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malarndhidum Malargalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaasanai Sollalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaasanai Sollalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuyilgalum Malargalum Adhisayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuyilgalum Malargalum Adhisayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavugal Kavidhaigal Ragasiyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavugal Kavidhaigal Ragasiyam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nilavu Paattu Nilavu Paattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilavu Paattu Nilavu Paattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oar Naal Ketten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oar Naal Ketten"/>
</div>
<div class="lyrico-lyrics-wrapper">Moongil Kaattil Moongil Kaattil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moongil Kaattil Moongil Kaattil"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalum Padiththen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalum Padiththen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nilavondru Nadandhadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilavondru Nadandhadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Suvadugal Manadhile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suvadugal Manadhile"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhai Vandhu Nanaiththadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai Vandhu Nanaiththadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Isai Yennai Seviyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Isai Yennai Seviyile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kolusugal Keerththanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolusugal Keerththanai"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarandha Dhevadhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarandha Dhevadhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhigalil Virigiraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhigalil Virigiraal"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarandha Thaamarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarandha Thaamarai"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Oru Pudhuvidha Paravasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Oru Pudhuvidha Paravasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayakkudhu Isaiyennum Adhisayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayakkudhu Isaiyennum Adhisayam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nilavu Paattu Nilavu Paattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilavu Paattu Nilavu Paattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oar Naal Ketten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oar Naal Ketten"/>
</div>
<div class="lyrico-lyrics-wrapper">Moongil Kaattil Moongil Kaattil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moongil Kaattil Moongil Kaattil"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalum Padiththen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalum Padiththen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andha Isaiyin Ragasiyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Isaiyin Ragasiyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Iru Uyirukku Purindhadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iru Uyirukku Purindhadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Iru Uyirukku Purindhadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iru Uyirukku Purindhadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingu Yaarukkuth Therindhadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingu Yaarukkuth Therindhadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Isaiyil Kalandhu Midhakkum Thendrale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Isaiyil Kalandhu Midhakkum Thendrale"/>
</div>
<div class="lyrico-lyrics-wrapper">Isaiyin Magalai Paarththadhillaiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Isaiyin Magalai Paarththadhillaiyo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nilavu Paattu Nilavu Paattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilavu Paattu Nilavu Paattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oar Naal Ketten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oar Naal Ketten"/>
</div>
<div class="lyrico-lyrics-wrapper">Moongil Kaattil Moongil Kaattil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moongil Kaattil Moongil Kaattil"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalum Padiththen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalum Padiththen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nilavu Paattu Nilavu Paattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilavu Paattu Nilavu Paattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oar Naal Ketten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oar Naal Ketten"/>
</div>
<div class="lyrico-lyrics-wrapper">Moongil Kaattil Moongil Kaattil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moongil Kaattil Moongil Kaattil"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalum Padiththen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalum Padiththen"/>
</div>
</pre>
