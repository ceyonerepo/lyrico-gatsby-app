---
title: "vaada bin laada song lyrics"
album: "Mankatha"
artist: "Yuvan Shankar Raja"
lyricist: "Vaali"
director: "Venkat Prabhu"
path: "/albums/mankatha-lyrics"
song: "Vaada Bin Laada"
image: ../../images/albumart/mankatha.jpg
date: 2011-08-31
lang: tamil
youtubeLink: "https://www.youtube.com/embed/TAv0fApr1js"
type: "love"
singers:
  - Krish
  - Suchitra
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vaada Bin Laada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada Bin Laada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oliyaadhe Acchoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oliyaadhe Acchoda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Twin Tower Endru Thoduda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Twin Tower Endru Thoduda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Japan In Hykoova
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Japan In Hykoova"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Russia vin Vodhkaava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Russia vin Vodhkaava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Ennnul Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Ennnul Enna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kandupidida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandupidida"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Noolaadai Nikaatha Idupu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Noolaadai Nikaatha Idupu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh O Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh O Oh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Thaane En
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Thaane En"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thothaana Uduppu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thothaana Uduppu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh O Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh O Oh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Thodamale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Thodamale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Summa Soodethura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summa Soodethura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naana Naana Vandhu Modhuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naana Naana Vandhu Modhuren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaaada Bin Laada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaada Bin Laada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oliyaadhe Acchoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oliyaadhe Acchoda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Twin Tower Endru Thoduda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Twin Tower Endru Thoduda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Japan In Hykoova
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Japan In Hykoova"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Russia vin Vodhkaava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Russia vin Vodhkaava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Ennnul Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Ennnul Enna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kandupidida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandupidida"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mathu Kadaivathu Thayirathan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mathu Kadaivathu Thayirathan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maiyal Kadaivathu Uyirathaan Endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maiyal Kadaivathu Uyirathaan Endru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhu Edho Adhu Edho Ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu Edho Adhu Edho Ennai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaatudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaatudhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pandri Kaaichal Mathiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pandri Kaaichal Mathiri"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paruva Kaichal Thaanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paruva Kaichal Thaanadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Udhadu Otthadam Udambu Muzhukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udhadu Otthadam Udambu Muzhukka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ketkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paduka Kuthuthu Rathiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paduka Kuthuthu Rathiri"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Purandu Kathuthu Poongili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Purandu Kathuthu Poongili"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nilavu Suttadhum Narambil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilavu Suttadhum Narambil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pattadhu Theepori
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattadhu Theepori"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Othungi Ninnathu Kaalathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Othungi Ninnathu Kaalathaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Urasi Vandhathu Karava Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urasi Vandhathu Karava Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manasum Kettadhu Mayangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasum Kettadhu Mayangi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vittathu Emma Emma Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vittathu Emma Emma Enna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Summa Summa Ninna thaakkuva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summa Summa Ninna thaakkuva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Cricket Enbathu Fixingthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cricket Enbathu Fixingthaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal Enbathu Mixingthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Enbathu Mixingthaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ingu Bet Mela Bet Katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingu Bet Mela Bet Katti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhinam Aadalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam Aadalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pandha Kandathum Catch -u Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pandha Kandathum Catch -u Dhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pidichu Jaipa Matchdhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pidichu Jaipa Matchdhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidiya Mattilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidiya Mattilum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veluthi Kattuva Batting
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veluthi Kattuva Batting"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakku Vachathu Pitch-u Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku Vachathu Pitch-u Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unakku Vaikkanum Ichu Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakku Vaikkanum Ichu Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ilamaiku Verum 20 Over Podhuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilamaiku Verum 20 Over Podhuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adichu Aadura Dhoni Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adichu Aadura Dhoni Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athukku Yengra Menithaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athukku Yengra Menithaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Viragam Enbathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viragam Enbathum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naragam Enbathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naragam Enbathum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Onnu Onnu Chinna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnu Onnu Chinna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ponnu Ponnu Unna Kooduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponnu Ponnu Unna Kooduthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Onna Ononnah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna Ononnah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nan Sonna Summa Sonna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nan Sonna Summa Sonna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhu Seivadhu Un Duty Yadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu Seivadhu Un Duty Yadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ai Emma Emmemmma Summa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ai Emma Emmemmma Summa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Summa Ishtam Pola Lootiyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Summa Ishtam Pola Lootiyadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nooladai Nikkatha Idupu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nooladai Nikkatha Idupu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh O Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh O Oh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Thaane En Thothaana Uduppu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Thaane En Thothaana Uduppu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh O Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh O Oh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Thodamale summa Soodethura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Thodamale summa Soodethura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naana Naana Vandhu Modhuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naana Naana Vandhu Modhuren"/>
</div>
</pre>
