---
title: "munimaa song lyrics"
album: "Aaruthra"
artist: "Vidyasagar"
lyricist: "Pa Vijay"
director: "Pa Vijay"
path: "/albums/aaruthra-lyrics"
song: "Munimaa"
image: ../../images/albumart/aaruthra.jpg
date: 2018-08-31
lang: tamil
youtubeLink: "https://www.youtube.com/embed/XIeekA1nmWY"
type: "happy"
singers:
  - Sanjana Kalmanje
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">maa maa mama 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maa maa mama "/>
</div>
<div class="lyrico-lyrics-wrapper">i am your munimaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="i am your munimaa"/>
</div>
<div class="lyrico-lyrics-wrapper">oh maa maa mama 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oh maa maa mama "/>
</div>
<div class="lyrico-lyrics-wrapper">i am your munimaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="i am your munimaa"/>
</div>
<div class="lyrico-lyrics-wrapper">maa maa mama 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maa maa mama "/>
</div>
<div class="lyrico-lyrics-wrapper">i am your munimaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="i am your munimaa"/>
</div>
<div class="lyrico-lyrics-wrapper">oh maa maa mama 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oh maa maa mama "/>
</div>
<div class="lyrico-lyrics-wrapper">i am your munimaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="i am your munimaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sokka keeran paiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sokka keeran paiyan"/>
</div>
<div class="lyrico-lyrics-wrapper">sokka aetho seiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sokka aetho seiya"/>
</div>
<div class="lyrico-lyrics-wrapper">sekka keeran paiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sekka keeran paiyan"/>
</div>
<div class="lyrico-lyrics-wrapper">nekka aetho seiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nekka aetho seiya"/>
</div>
<div class="lyrico-lyrics-wrapper">selfi eduthu potta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="selfi eduthu potta"/>
</div>
<div class="lyrico-lyrics-wrapper">moonji pottan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moonji pottan"/>
</div>
<div class="lyrico-lyrics-wrapper">whatsuppil vaa mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="whatsuppil vaa mama"/>
</div>
<div class="lyrico-lyrics-wrapper">ne varan kati nan vuda maten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ne varan kati nan vuda maten"/>
</div>
<div class="lyrico-lyrics-wrapper">mama ne bemani thana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mama ne bemani thana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">maa maa mama 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maa maa mama "/>
</div>
<div class="lyrico-lyrics-wrapper">i am your munimaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="i am your munimaa"/>
</div>
<div class="lyrico-lyrics-wrapper">oh maa maa mama 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oh maa maa mama "/>
</div>
<div class="lyrico-lyrics-wrapper">i am your munimaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="i am your munimaa"/>
</div>
<div class="lyrico-lyrics-wrapper">maa maa mama 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maa maa mama "/>
</div>
<div class="lyrico-lyrics-wrapper">i am your munimaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="i am your munimaa"/>
</div>
<div class="lyrico-lyrics-wrapper">oh maa maa mama 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oh maa maa mama "/>
</div>
<div class="lyrico-lyrics-wrapper">i am your munimaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="i am your munimaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ennoda fittness pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennoda fittness pochu"/>
</div>
<div class="lyrico-lyrics-wrapper">unnala reading pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnala reading pochu"/>
</div>
<div class="lyrico-lyrics-wrapper">crush matum pothuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="crush matum pothuma"/>
</div>
<div class="lyrico-lyrics-wrapper">vaa mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaa mama"/>
</div>
<div class="lyrico-lyrics-wrapper">instagram kan mulichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="instagram kan mulichi"/>
</div>
<div class="lyrico-lyrics-wrapper">twiter la kai kulukki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="twiter la kai kulukki"/>
</div>
<div class="lyrico-lyrics-wrapper">fb la feel panna polama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="fb la feel panna polama"/>
</div>
<div class="lyrico-lyrics-wrapper">haalokirama potu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="haalokirama potu"/>
</div>
<div class="lyrico-lyrics-wrapper">heart jammuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="heart jammuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">aaliv palam taste panna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaliv palam taste panna"/>
</div>
<div class="lyrico-lyrics-wrapper">aala theduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aala theduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">parmaa kadaiyila sutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="parmaa kadaiyila sutta"/>
</div>
<div class="lyrico-lyrics-wrapper">aththo vaaduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aththo vaaduthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">mama ne bemani thana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mama ne bemani thana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">maa maa mama 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maa maa mama "/>
</div>
<div class="lyrico-lyrics-wrapper">i am your munimaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="i am your munimaa"/>
</div>
<div class="lyrico-lyrics-wrapper">oh maa maa mama 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oh maa maa mama "/>
</div>
<div class="lyrico-lyrics-wrapper">i am your munimaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="i am your munimaa"/>
</div>
<div class="lyrico-lyrics-wrapper">maa maa mama 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maa maa mama "/>
</div>
<div class="lyrico-lyrics-wrapper">i am your munimaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="i am your munimaa"/>
</div>
<div class="lyrico-lyrics-wrapper">oh maa maa mama 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oh maa maa mama "/>
</div>
<div class="lyrico-lyrics-wrapper">i am your munimaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="i am your munimaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sokka keeran paiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sokka keeran paiyan"/>
</div>
<div class="lyrico-lyrics-wrapper">sokka aetho seiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sokka aetho seiya"/>
</div>
</pre>
