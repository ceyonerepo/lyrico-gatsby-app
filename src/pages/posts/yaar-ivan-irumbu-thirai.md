---
title: "yaar ivan song lyrics"
album: "Irumbu Thirai"
artist: "Yuvan Shankar Raja"
lyricist: "Kaber Vasuki"
director: "P.S. Mithran"
path: "/albums/irumbu-thirai-song-lyrics"
song: "Yaar Ivan"
image: ../../images/albumart/irumbu-thirai.jpg
date: 2018-05-11
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Ds5fu8AxqqY"
type: "mass"
singers:
  - Kaber Vasuki
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yaar ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar ivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoonilum thurumbilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoonilum thurumbilum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangal vaithu kondu paarpavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangal vaithu kondu paarpavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaan kadal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaan kadal"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannilum manadhilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannilum manadhilum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhai vaaithukondu ketpavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhai vaaithukondu ketpavan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhoram vaai vachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhoram vaai vachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathoda nee pesu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathoda nee pesu"/>
</div>
<div class="lyrico-lyrics-wrapper">Satham potta vaarthai yellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Satham potta vaarthai yellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan siraiyila sethidichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan siraiyila sethidichu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enga poi olinjaaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga poi olinjaaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedho onnu paakkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedho onnu paakkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarukitta pesinaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarukitta pesinaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedho onnu ketkkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedho onnu ketkkudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adimaiyaka vongirundha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adimaiyaka vongirundha"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhidhadi nu yerangalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhidhadi nu yerangalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasayathaan kaati ippo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasayathaan kaati ippo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaala vaari vetturaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaala vaari vetturaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaar ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar ivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoonilum thurumbilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoonilum thurumbilum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangal vaithu kondu paarpavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangal vaithu kondu paarpavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaan kadal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaan kadal"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannilum manadhilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannilum manadhilum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhai vaaithukondu ketpavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhai vaaithukondu ketpavan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veshatha izhandhaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veshatha izhandhaachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanam paathu kaiya kattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam paathu kaiya kattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Uthaman dhaan illaiyinu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uthaman dhaan illaiyinu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vangiyila vaddi kattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vangiyila vaddi kattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eppapayo senjadhellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppapayo senjadhellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Engengeyo padhiyidhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engengeyo padhiyidhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kota vittu thotha pindhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kota vittu thotha pindhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Aatamae puriyidhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aatamae puriyidhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engirundhu adikiraan-nu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engirundhu adikiraan-nu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedi paakka poyithaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedi paakka poyithaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Uruvamillaa mirugamaathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uruvamillaa mirugamaathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Karpanai-la sirikiraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karpanai-la sirikiraan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaar ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar ivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar ivan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karuviya kaiyila pidichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuviya kaiyila pidichu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavu koocham kaasaiyellam podhachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavu koocham kaasaiyellam podhachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ragasiyam pootithaan adachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ragasiyam pootithaan adachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarukum theriyaama marachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarukum theriyaama marachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ilavasam vilambaraminu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilavasam vilambaraminu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhan vasam izhuthichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhan vasam izhuthichu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellai illa aasapadu-nu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellai illa aasapadu-nu"/>
</div>
<div class="lyrico-lyrics-wrapper">Koochamillama sollichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koochamillama sollichu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saaku ippo irukudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saaku ippo irukudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Saavi innum porundhudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saavi innum porundhudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pootiruka pootiyiruka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pootiruka pootiyiruka"/>
</div>
<div class="lyrico-lyrics-wrapper">Vacha saraku mattum karaiyidhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vacha saraku mattum karaiyidhu"/>
</div>
</pre>
