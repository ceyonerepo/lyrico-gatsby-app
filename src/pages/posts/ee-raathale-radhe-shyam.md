---
title: "ee raathale song lyrics"
album: "Radhe Shyam"
artist: "Justin Prabhakaran"
lyricist: "Krishna Kanth"
director: "Radha Krishna Kumar"
path: "/albums/radhe-shyam-lyrics"
song: "Ee Raathale"
image: ../../images/albumart/radhe-shyam.jpg
date: 2022-01-14
lang: telugu
youtubeLink: "https://www.youtube.com/embed/vHuBCcm7KC8"
type: "love"
singers:
  - Yuvan Shankar Raja
  - Harini Ivaturi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Evaaro veerevaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evaaro veerevaaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalavani iruu premikulaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalavani iruu premikulaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Evaaro veerevaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evaaro veerevaaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidiponi yathrikulaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidiponi yathrikulaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veeri dhaarokate
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veeri dhaarokate"/>
</div>
<div class="lyrico-lyrics-wrapper">Maari dhikkule verule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maari dhikkule verule"/>
</div>
<div class="lyrico-lyrics-wrapper">Oopirokatele oka swasala niswaasala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oopirokatele oka swasala niswaasala"/>
</div>
<div class="lyrico-lyrics-wrapper">Aataade vidhe idhaa idhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aataade vidhe idhaa idhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Padhe padhe kalavadam elaa elaa kaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padhe padhe kalavadam elaa elaa kaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Raase undhaa, raase undhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raase undhaa, raase undhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ee raathale dhoboochule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee raathale dhoboochule"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee raathale doboochule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee raathale doboochule"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Evaaro veerevaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evaaro veerevaaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalavani iruu premikulaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalavani iruu premikulaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Evaaro veerevaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evaaro veerevaaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidiponi yathrikulaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidiponi yathrikulaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Khaale khaaligunna uttharamedho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khaale khaaligunna uttharamedho"/>
</div>
<div class="lyrico-lyrics-wrapper">Naatho edho katha cheppaalantondhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naatho edho katha cheppaalantondhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye goodachaaro gaadanga nanney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye goodachaaro gaadanga nanney"/>
</div>
<div class="lyrico-lyrics-wrapper">Ventaadenu enduko emo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ventaadenu enduko emo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalam manchu katthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam manchu katthi"/>
</div>
<div class="lyrico-lyrics-wrapper">Gundello guchhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundello guchhey"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaayam ledhu gaani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaayam ledhu gaani"/>
</div>
<div class="lyrico-lyrics-wrapper">Daadentho nachhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daadentho nachhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aa maaye evare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa maaye evare"/>
</div>
<div class="lyrico-lyrics-wrapper">Raadaa edhurey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raadaa edhurey"/>
</div>
<div class="lyrico-lyrics-wrapper">Theleekane thaha thaha perige
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theleekane thaha thaha perige"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nijama bhrama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijama bhrama"/>
</div>
<div class="lyrico-lyrics-wrapper">Baagundhi yaathaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baagundhi yaathaney"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalatho kalo gadavani guruthuley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalatho kalo gadavani guruthuley"/>
</div>
<div class="lyrico-lyrics-wrapper">Edho janma baadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edho janma baadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Podhe premai radhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhe premai radhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ee raathale dhoboochule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee raathale dhoboochule"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee raathale dhoboochule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee raathale dhoboochule"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee raathale dhoboochule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee raathale dhoboochule"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye goodachaaro gaadanga nanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye goodachaaro gaadanga nanne"/>
</div>
<div class="lyrico-lyrics-wrapper">Ventaadenu enduko emo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ventaadenu enduko emo"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa maaye evaare raadaa edhure
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa maaye evaare raadaa edhure"/>
</div>
<div class="lyrico-lyrics-wrapper">Theleekane thaha thaha perige
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theleekane thaha thaha perige"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Evaaro veerevaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evaaro veerevaaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalavani iru premikulaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalavani iru premikulaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Evaaro veerevaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evaaro veerevaaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidiponi yathrikulaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidiponi yathrikulaa"/>
</div>
</pre>
