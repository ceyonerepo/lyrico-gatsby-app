---
title: "konjam chellama song lyrics"
album: "Clap"
artist: "Ilaiyaraaja"
lyricist: "Pa Vijay"
director: "Prithivi Adithya"
path: "/albums/clap-lyrics"
song: "Konjam Chellama"
image: ../../images/albumart/clap.jpg
date: 2022-03-11
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Cg1NfH9ZHdY"
type: "love"
singers:
  - Shaan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">konjam chellamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konjam chellamma"/>
</div>
<div class="lyrico-lyrics-wrapper">i love you sollamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="i love you sollamma"/>
</div>
<div class="lyrico-lyrics-wrapper">konjam chellamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konjam chellamma"/>
</div>
<div class="lyrico-lyrics-wrapper">i love you sollamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="i love you sollamma"/>
</div>
<div class="lyrico-lyrics-wrapper">mella mellamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mella mellamma"/>
</div>
<div class="lyrico-lyrics-wrapper">i miss you sollamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="i miss you sollamma"/>
</div>
<div class="lyrico-lyrics-wrapper">raja kaiya vacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="raja kaiya vacha"/>
</div>
<div class="lyrico-lyrics-wrapper">wrong aaguma yemma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="wrong aaguma yemma"/>
</div>
<div class="lyrico-lyrics-wrapper">kekum satham ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kekum satham ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">song aaguma yemma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="song aaguma yemma"/>
</div>
<div class="lyrico-lyrics-wrapper">istam pola kathal solli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="istam pola kathal solli"/>
</div>
<div class="lyrico-lyrics-wrapper">alli unna nenjil vacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alli unna nenjil vacha"/>
</div>
<div class="lyrico-lyrics-wrapper">konjum manasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konjum manasu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">konjam chellamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konjam chellamma"/>
</div>
<div class="lyrico-lyrics-wrapper">i love you sollamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="i love you sollamma"/>
</div>
<div class="lyrico-lyrics-wrapper">mella mellamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mella mellamma"/>
</div>
<div class="lyrico-lyrics-wrapper">i miss you sollamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="i miss you sollamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">konjam neyum thaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konjam neyum thaa"/>
</div>
<div class="lyrico-lyrics-wrapper">un nenjathukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un nenjathukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">antha nodi naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antha nodi naan"/>
</div>
<div class="lyrico-lyrics-wrapper">un vizhi mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un vizhi mela"/>
</div>
<div class="lyrico-lyrics-wrapper">innum innum alagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="innum innum alagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">imsa pannathe nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="imsa pannathe nee"/>
</div>
<div class="lyrico-lyrics-wrapper">unnoda kadhalathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnoda kadhalathil"/>
</div>
<div class="lyrico-lyrics-wrapper">ennoda raani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennoda raani"/>
</div>
<div class="lyrico-lyrics-wrapper">satrum munbu maiyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="satrum munbu maiyam"/>
</div>
<div class="lyrico-lyrics-wrapper">kolla kathal puyal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kolla kathal puyal"/>
</div>
<div class="lyrico-lyrics-wrapper">natpu enum nenju tharaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="natpu enum nenju tharaiyil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">konjam chellamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konjam chellamma"/>
</div>
<div class="lyrico-lyrics-wrapper">i love you sollavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="i love you sollavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">mella mellamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mella mellamma"/>
</div>
<div class="lyrico-lyrics-wrapper">i miss you sollavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="i miss you sollavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">enna senja nee thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna senja nee thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">mun pol nanum illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mun pol nanum illa"/>
</div>
<div class="lyrico-lyrics-wrapper">unmai sonna un pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unmai sonna un pol"/>
</div>
<div class="lyrico-lyrics-wrapper">yarum inge illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yarum inge illai"/>
</div>
<div class="lyrico-lyrics-wrapper">kathal enum roja vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathal enum roja vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">nera vanthu kai puduchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nera vanthu kai puduchu"/>
</div>
<div class="lyrico-lyrics-wrapper">kai kulukuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kai kulukuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">konjam chellamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konjam chellamma"/>
</div>
<div class="lyrico-lyrics-wrapper">i love you sollavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="i love you sollavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">mella mellamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mella mellamma"/>
</div>
<div class="lyrico-lyrics-wrapper">i miss you sollavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="i miss you sollavaa"/>
</div>
</pre>
