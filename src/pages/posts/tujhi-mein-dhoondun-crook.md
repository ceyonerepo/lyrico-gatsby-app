---
title: "tujhi mein song lyrics"
album: "Crook"
artist: "Pritam - Babbu Maan"
lyricist: "Kumaar"
director: "Mohit Suri"
path: "/albums/crook-lyrics"
song: "Tujhi Mein Dhoondun"
image: ../../images/albumart/crook.jpg
date: 2010-10-08
lang: hindi
youtubeLink: "https://www.youtube.com/embed/f0dxu8rPTRg"
type: "love"
singers:
  - KK
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Tujhi mein dhoondun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tujhi mein dhoondun"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuchh der jeene ka aasra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuchh der jeene ka aasra"/>
</div>
<div class="lyrico-lyrics-wrapper">Tujhi se poonchun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tujhi se poonchun"/>
</div>
<div class="lyrico-lyrics-wrapper">Khud se hi milne ka raasta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khud se hi milne ka raasta"/>
</div>
<div class="lyrico-lyrics-wrapper">Tujhi mein dhoondun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tujhi mein dhoondun"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuchh der jeene ka aasra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuchh der jeene ka aasra"/>
</div>
<div class="lyrico-lyrics-wrapper">Tujhi se poonchun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tujhi se poonchun"/>
</div>
<div class="lyrico-lyrics-wrapper">Khud se hi milne ka raasta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khud se hi milne ka raasta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mujhe teri panahein milein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mujhe teri panahein milein"/>
</div>
<div class="lyrico-lyrics-wrapper">Rehne ko teri baahein milein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rehne ko teri baahein milein"/>
</div>
<div class="lyrico-lyrics-wrapper">Jana hai kahan tere siwa wo ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jana hai kahan tere siwa wo ho"/>
</div>
<div class="lyrico-lyrics-wrapper">Mujhe teri panahein milein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mujhe teri panahein milein"/>
</div>
<div class="lyrico-lyrics-wrapper">Rehne ko teri baahein milein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rehne ko teri baahein milein"/>
</div>
<div class="lyrico-lyrics-wrapper">Jana hai kahan tere siwa wo ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jana hai kahan tere siwa wo ho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Toone diye din mujhe raat mein haan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Toone diye din mujhe raat mein haan"/>
</div>
<div class="lyrico-lyrics-wrapper">Saansein chali ab tere sath mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saansein chali ab tere sath mein"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mehsoos karne laga apni kismat tere hath mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mehsoos karne laga apni kismat tere hath mein"/>
</div>
<div class="lyrico-lyrics-wrapper">Fizaaon mein hain mausam khile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fizaaon mein hain mausam khile"/>
</div>
<div class="lyrico-lyrics-wrapper">Labon se gaye saare gile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Labon se gaye saare gile"/>
</div>
<div class="lyrico-lyrics-wrapper">Jana hai kahan tere siwa wo ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jana hai kahan tere siwa wo ho"/>
</div>
<div class="lyrico-lyrics-wrapper">Mujhe teri panahein milein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mujhe teri panahein milein"/>
</div>
<div class="lyrico-lyrics-wrapper">Rehne ko teri baahein milein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rehne ko teri baahein milein"/>
</div>
<div class="lyrico-lyrics-wrapper">Jana hai kahan tere siwa wo ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jana hai kahan tere siwa wo ho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jab maine paya tujhe kho diya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jab maine paya tujhe kho diya"/>
</div>
<div class="lyrico-lyrics-wrapper">Dil aasuon ke bina ro diya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil aasuon ke bina ro diya"/>
</div>
<div class="lyrico-lyrics-wrapper">Din khwaabon aankhon mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Din khwaabon aankhon mein"/>
</div>
<div class="lyrico-lyrics-wrapper">Aate hue door yun ho gaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aate hue door yun ho gaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Ke tere bina jeena khale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ke tere bina jeena khale"/>
</div>
<div class="lyrico-lyrics-wrapper">Seene mein koi rang sa chale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seene mein koi rang sa chale"/>
</div>
<div class="lyrico-lyrics-wrapper">Jana hai kahan haan tere siwa wo o
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jana hai kahan haan tere siwa wo o"/>
</div>
<div class="lyrico-lyrics-wrapper">Mujhe teri panahein milein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mujhe teri panahein milein"/>
</div>
<div class="lyrico-lyrics-wrapper">Rehne ko teri baahein milein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rehne ko teri baahein milein"/>
</div>
<div class="lyrico-lyrics-wrapper">Jana hai kahan tere siwa wo ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jana hai kahan tere siwa wo ho"/>
</div>
</pre>
