---
title: "uyir suzhaluthey song lyrics"
album: "Theeni"
artist: "Rajesh Murugesan"
lyricist: "Ko Sesha"
director: "Ani.I.V.Sasi"
path: "/albums/theeni-lyrics"
song: "Uyir Suzhaluthey"
image: ../../images/albumart/theeni.jpg
date: 2021-02-26
lang: tamil
youtubeLink: "https://www.youtube.com/embed/r2XxxWnmvEU"
type: "Love"
singers:
  - Yazin Nizar
  - Kalyani Nair
  - Rajesh Murugesan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">uyire suzhaludhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyire suzhaludhe"/>
</div>
<div class="lyrico-lyrics-wrapper">valigal maraiyudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valigal maraiyudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">un maayangalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un maayangalil"/>
</div>
<div class="lyrico-lyrics-wrapper">en aagayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en aagayam"/>
</div>
<div class="lyrico-lyrics-wrapper">engengum pon minnale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engengum pon minnale"/>
</div>
<div class="lyrico-lyrics-wrapper">kaalam uraindhadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaalam uraindhadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">kanavum azhaithadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavum azhaithadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">un vaarthaigalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un vaarthaigalil"/>
</div>
<div class="lyrico-lyrics-wrapper">en boologam engengum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en boologam engengum"/>
</div>
<div class="lyrico-lyrics-wrapper">poo pookudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poo pookudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">enakkul puriyadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enakkul puriyadha"/>
</div>
<div class="lyrico-lyrics-wrapper">sandhosame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sandhosame"/>
</div>
<div class="lyrico-lyrics-wrapper">un ennangal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un ennangal "/>
</div>
<div class="lyrico-lyrics-wrapper">thoondi selludhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoondi selludhe"/>
</div>
<div class="lyrico-lyrics-wrapper">en vaazhvile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en vaazhvile"/>
</div>
<div class="lyrico-lyrics-wrapper">artham serudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="artham serudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">unnale indha yeatrame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnale indha yeatrame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">en vaanile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en vaanile"/>
</div>
<div class="lyrico-lyrics-wrapper">pala vannam serudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pala vannam serudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">unnale pudhu maatrame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnale pudhu maatrame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nenjil thirakudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjil thirakudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">anbendra jannale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anbendra jannale"/>
</div>
<div class="lyrico-lyrics-wrapper">un kaadhoramaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kaadhoramaai"/>
</div>
<div class="lyrico-lyrics-wrapper">en aasaigal pesave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en aasaigal pesave"/>
</div>
<div class="lyrico-lyrics-wrapper">yeakkam thaakudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yeakkam thaakudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">kannangal sivakkudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannangal sivakkudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">indha yeakanthane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indha yeakanthane"/>
</div>
<div class="lyrico-lyrics-wrapper">pudhu ellaigal thaanduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pudhu ellaigal thaanduthe"/>
</div>
<div class="lyrico-lyrics-wrapper">enakkul naan pesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enakkul naan pesum"/>
</div>
<div class="lyrico-lyrics-wrapper">sallabame un ennangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sallabame un ennangal"/>
</div>
<div class="lyrico-lyrics-wrapper">thoondi selludhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoondi selludhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">en vaazhvile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en vaazhvile"/>
</div>
<div class="lyrico-lyrics-wrapper">artham serudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="artham serudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">unnale indha yeatrame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnale indha yeatrame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">en vaanile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en vaanile"/>
</div>
<div class="lyrico-lyrics-wrapper">pala vannam serudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pala vannam serudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">unnale pudhu maatrame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnale pudhu maatrame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">en vaazhvile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en vaazhvile"/>
</div>
<div class="lyrico-lyrics-wrapper">artham serudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="artham serudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">unnale indha yeatrame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnale indha yeatrame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">en vaanile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en vaanile"/>
</div>
<div class="lyrico-lyrics-wrapper">pala vannam serudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pala vannam serudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">unnale pudhu maatrame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnale pudhu maatrame"/>
</div>
</pre>
