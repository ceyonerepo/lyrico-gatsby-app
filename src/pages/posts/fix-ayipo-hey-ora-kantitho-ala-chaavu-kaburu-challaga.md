---
title: "fix ayipo song lyrics"
album: "Chaavu Kaburu Challaga"
artist: "Jakes Bejoy"
lyricist: "Koushik Pegallapati - Sanare"
director: "Pegallapati Koushik"
path: "/albums/chaavu-kaburu-challaga-lyrics"
song: "Fox Ayipo - Hey Ora Kantitho Ala"
image: ../../images/albumart/chaavu-kaburu-challaga.jpg
date: 2021-03-19
lang: telugu
youtubeLink: "https://www.youtube.com/embed/F95kc4eoVBg"
type: "love"
singers:
  - Rahul Sipligunj
  - Aditya Tadepalli
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hey ora kantitho ala chusthave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey ora kantitho ala chusthave"/>
</div>
<div class="lyrico-lyrics-wrapper">Burra vedekkipoyela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Burra vedekkipoyela"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey dhoora dhooranga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey dhoora dhooranga"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanne thosthaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanne thosthaave"/>
</div>
<div class="lyrico-lyrics-wrapper">Sara sarra mandela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sara sarra mandela"/>
</div>
<div class="lyrico-lyrics-wrapper">Pora pommanna sigge lekunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pora pommanna sigge lekunda"/>
</div>
<div class="lyrico-lyrics-wrapper">Vente vasthane jada koppula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vente vasthane jada koppula"/>
</div>
<div class="lyrico-lyrics-wrapper">Basthi gaadnaina baane untaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Basthi gaadnaina baane untaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Sariga soode pilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sariga soode pilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey ammo ammo ammo ammo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey ammo ammo ammo ammo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ammo ammo ammaayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammo ammo ammaayo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kurradiko kompundhammaayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurradiko kompundhammaayo"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey ammo ammo ammo ammo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey ammo ammo ammo ammo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ammo ammo ammaayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammo ammo ammaayo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudi kaaletteyyammo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudi kaaletteyyammo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Fix aipo fix aipo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fix aipo fix aipo"/>
</div>
<div class="lyrico-lyrics-wrapper">Fix aipo fix aipo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fix aipo fix aipo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee janme naakantu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee janme naakantu"/>
</div>
<div class="lyrico-lyrics-wrapper">Fix aipo fix aipo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fix aipo fix aipo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee rajuki raanai fix aipo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee rajuki raanai fix aipo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey ora kantitho ala chusthave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey ora kantitho ala chusthave"/>
</div>
<div class="lyrico-lyrics-wrapper">Burra vedekkipoyela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Burra vedekkipoyela"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey dhoora dhooranga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey dhoora dhooranga"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanne thosthaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanne thosthaave"/>
</div>
<div class="lyrico-lyrics-wrapper">Sara sarra mandela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sara sarra mandela"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saavu melalae dholu sannayay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saavu melalae dholu sannayay"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinipisthu unnaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinipisthu unnaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaati kanneelle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaati kanneelle"/>
</div>
<div class="lyrico-lyrics-wrapper">Pelli sandhallai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pelli sandhallai"/>
</div>
<div class="lyrico-lyrics-wrapper">Muripisthu unnaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muripisthu unnaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Valapula nadhilona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valapula nadhilona"/>
</div>
<div class="lyrico-lyrics-wrapper">Munigina nadhini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munigina nadhini"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanikarame soopinchi kaapadave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanikarame soopinchi kaapadave"/>
</div>
<div class="lyrico-lyrics-wrapper">Palakave ikanaina alakanu vadhili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palakave ikanaina alakanu vadhili"/>
</div>
<div class="lyrico-lyrics-wrapper">Narakame soopithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Narakame soopithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenemai povaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenemai povaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vedekki pothaande
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedekki pothaande"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa gunde nee gaaliki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa gunde nee gaaliki"/>
</div>
<div class="lyrico-lyrics-wrapper">Ammo ammo ammo ammo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammo ammo ammo ammo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ammo ammo ammaayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammo ammo ammaayo"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadekki pothunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadekki pothunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Lesthaale nee soopiki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lesthaale nee soopiki"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey ammo ammo ammo ammo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey ammo ammo ammo ammo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ammo ammo ammaayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammo ammo ammaayo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Seppedhinkemunde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seppedhinkemunde"/>
</div>
<div class="lyrico-lyrics-wrapper">Fix aipo fix aipo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fix aipo fix aipo"/>
</div>
<div class="lyrico-lyrics-wrapper">Fix aipo fix aipo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fix aipo fix aipo"/>
</div>
<div class="lyrico-lyrics-wrapper">Gangamma kodaligaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gangamma kodaligaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Fix aipo fix aipo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fix aipo fix aipo"/>
</div>
<div class="lyrico-lyrics-wrapper">Are mandhuki sindhai mix aipo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Are mandhuki sindhai mix aipo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey ora kantitho ala chusthave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey ora kantitho ala chusthave"/>
</div>
<div class="lyrico-lyrics-wrapper">Burra vedekkipoyela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Burra vedekkipoyela"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey dhoora dhooranga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey dhoora dhooranga"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanne thosthaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanne thosthaave"/>
</div>
<div class="lyrico-lyrics-wrapper">Sara sarra mandela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sara sarra mandela"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey ammo ammo ammo ammo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey ammo ammo ammo ammo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ammo ammo ammaayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammo ammo ammaayo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kurradiko kompundhammaayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurradiko kompundhammaayo"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey ammo ammo ammo ammo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey ammo ammo ammo ammo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ammo ammo ammaayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammo ammo ammaayo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudi kaaletteyyammo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudi kaaletteyyammo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Fix aipo fix aipo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fix aipo fix aipo"/>
</div>
<div class="lyrico-lyrics-wrapper">Fix aipo fix aipo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fix aipo fix aipo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee janme naakantu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee janme naakantu"/>
</div>
<div class="lyrico-lyrics-wrapper">Fix aipo fix aipo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fix aipo fix aipo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee rajuki raanai fix aipo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee rajuki raanai fix aipo"/>
</div>
</pre>
