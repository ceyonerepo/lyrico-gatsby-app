---
title: "mayilaanjiye song lyrics"
album: "Sivappu Manjal Pachai"
artist: "Siddhu Kumar"
lyricist: "Dhamayanthi"
director: "Sasi"
path: "/albums/sivappu-manjal-pachai-lyrics"
song: "Mayilaanjiye"
image: ../../images/albumart/sivappu-manjal-pachai.jpg
date: 2019-09-06
lang: tamil
youtubeLink: "https://www.youtube.com/embed/2fIk8K5FKAk"
type: "love"
singers:
  - Anand Aravindakshan
  - Shashaa Tirupati
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aathi Yaaradi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aathi Yaaradi"/>
</div>
<div class="lyrico-lyrics-wrapper">Yekkam Thaaradi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yekkam Thaaradi"/>
</div>
<div class="lyrico-lyrics-wrapper">koththu Koththaa Aasa Vechi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koththu Koththaa Aasa Vechi"/>
</div>
<div class="lyrico-lyrics-wrapper">Mothamaaga Thooki Poradi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mothamaaga Thooki Poradi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saanji Paaradi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saanji Paaradi"/>
</div>
<div class="lyrico-lyrics-wrapper">Saanjae Ponnendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saanjae Ponnendi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannathoda Kannam Vechi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannathoda Kannam Vechi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kichu Kichu Mootti Poradi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kichu Kichu Mootti Poradi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Sonna Vaartha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Sonna Vaartha"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Serthu Vechen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Serthu Vechen"/>
</div>
<div class="lyrico-lyrics-wrapper">En Moochu Kaaththaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Moochu Kaaththaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Maathi Vechen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Maathi Vechen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enj Siru Mailaanjiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enj Siru Mailaanjiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayangidu Nenjukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayangidu Nenjukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Enj Siru Kallaliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enj Siru Kallaliye"/>
</div>
<div class="lyrico-lyrics-wrapper">Thavikkudhu Ullukkulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thavikkudhu Ullukkulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakkena Nee Iruntha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkena Nee Iruntha"/>
</div>
<div class="lyrico-lyrics-wrapper">Orutharum Theva Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orutharum Theva Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Madiyila Thoonga Senja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madiyila Thoonga Senja"/>
</div>
<div class="lyrico-lyrics-wrapper">Saagavum Bayamey Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saagavum Bayamey Illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aathi Yaaradi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aathi Yaaradi"/>
</div>
<div class="lyrico-lyrics-wrapper">Yekkam Thaaradi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yekkam Thaaradi"/>
</div>
<div class="lyrico-lyrics-wrapper">koththu Koththaa Aasa Vechi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koththu Koththaa Aasa Vechi"/>
</div>
<div class="lyrico-lyrics-wrapper">Mothamaaga Thooki Poradi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mothamaaga Thooki Poradi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ullangaikulla Mugam Vechi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullangaikulla Mugam Vechi"/>
</div>
<div class="lyrico-lyrics-wrapper">Otti Kolla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Otti Kolla"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam Poora Nee Venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam Poora Nee Venum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Paakaatha Otha Nodi Nenjukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Paakaatha Otha Nodi Nenjukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeno Kaneera Maari Therikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeno Kaneera Maari Therikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeyilla Naan Thaan Na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyilla Naan Thaan Na"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenachi Naan Paakala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenachi Naan Paakala"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaigal Kokkanum Dhenam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaigal Kokkanum Dhenam"/>
</div>
<div class="lyrico-lyrics-wrapper">Moochatha Motha Mora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moochatha Motha Mora"/>
</div>
<div class="lyrico-lyrics-wrapper">Viralula Paakuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viralula Paakuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Koosutha Nadungutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koosutha Nadungutha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Minjiya Maata Un Kaal Eduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minjiya Maata Un Kaal Eduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithama Nenjodu Vaippenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithama Nenjodu Vaippenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjiya Thozhula Sanjiruppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjiya Thozhula Sanjiruppen"/>
</div>
<div class="lyrico-lyrics-wrapper">Usure En Thaaram Vaa Nee Thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usure En Thaaram Vaa Nee Thaanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enj Siru Mailaanjiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enj Siru Mailaanjiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayangidu Nenjukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayangidu Nenjukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Enj Siru Kallaliyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enj Siru Kallaliyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thavikkuthu Ullukkulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thavikkuthu Ullukkulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakkena Nee Iruntha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkena Nee Iruntha"/>
</div>
<div class="lyrico-lyrics-wrapper">Orutharum Theva Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orutharum Theva Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Madiyila Thoonga Senja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madiyila Thoonga Senja"/>
</div>
<div class="lyrico-lyrics-wrapper">Saagavum Bayamey Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saagavum Bayamey Illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aathi Yaaradi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aathi Yaaradi"/>
</div>
<div class="lyrico-lyrics-wrapper">Yekkam Thaaradi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yekkam Thaaradi"/>
</div>
<div class="lyrico-lyrics-wrapper">koththu Koththa Aasa Vechi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koththu Koththa Aasa Vechi"/>
</div>
<div class="lyrico-lyrics-wrapper">Mothamaaga Thooki Poradi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mothamaaga Thooki Poradi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koththu Koththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koththu Koththa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasa Vechi Mothamaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasa Vechi Mothamaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Thooki Poradi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thooki Poradi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thooki Poradi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thooki Poradi"/>
</div>
</pre>
