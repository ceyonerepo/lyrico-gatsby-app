---
title: "yendaro monisa song lyrics"
album: "Chaavu Kaburu Challaga"
artist: "Jakes Bejoy"
lyricist: "Sanare"
director: "Pegallapati Koushik"
path: "/albums/chaavu-kaburu-challaga-lyrics"
song: "Yendaro Monisa Sundara Bhaavamu"
image: ../../images/albumart/chaavu-kaburu-challaga.jpg
date: 2021-03-19
lang: telugu
youtubeLink: "https://www.youtube.com/embed/T9u_CVhX-hs"
type: "love"
singers:
 - Deepika V
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Endharo mosina sundhara bhaavamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endharo mosina sundhara bhaavamu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sughunabi raamuni sonthamaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sughunabi raamuni sonthamaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Sambara veedhina aathani hrudhayamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sambara veedhina aathani hrudhayamu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chalamutho thakadhimi naatyamaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chalamutho thakadhimi naatyamaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannula mundhara devatha roopamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannula mundhara devatha roopamu"/>
</div>
<div class="lyrico-lyrics-wrapper">Choosedi bhaagyamu dhorikinadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choosedi bhaagyamu dhorikinadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappani theluputhu dhaivamu dhigina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappani theluputhu dhaivamu dhigina"/>
</div>
<div class="lyrico-lyrics-wrapper">Aapithe aagani varusa idhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aapithe aagani varusa idhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endharo mosina sundhara bhaavamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endharo mosina sundhara bhaavamu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sughunabi raamuni sonthamaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sughunabi raamuni sonthamaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adharaala yerupuki neeraajanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adharaala yerupuki neeraajanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Jalajaakshi momuki neeraajanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jalajaakshi momuki neeraajanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Adharaala yerupuki neeraajanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adharaala yerupuki neeraajanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Jalajaakshi momuki neeraajanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jalajaakshi momuki neeraajanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aliveni thurumuki apuroopa sogasuki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aliveni thurumuki apuroopa sogasuki"/>
</div>
<div class="lyrico-lyrics-wrapper">Aliveni thurumuki apuroopa sogasuki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aliveni thurumuki apuroopa sogasuki"/>
</div>
<div class="lyrico-lyrics-wrapper">Hrudhaya tharamu nundi neerajanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hrudhaya tharamu nundi neerajanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Prema neerajanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prema neerajanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Endharo mosina sundhara bhaavamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endharo mosina sundhara bhaavamu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sughunabi raamuni sonthamaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sughunabi raamuni sonthamaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Makutamu leni elikasaani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Makutamu leni elikasaani"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasuni kadhipina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasuni kadhipina"/>
</div>
<div class="lyrico-lyrics-wrapper">Moskha pradhaayini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moskha pradhaayini"/>
</div>
<div class="lyrico-lyrics-wrapper">Vadhanamu choodaga maate raani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadhanamu choodaga maate raani"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaaradamunnadha nayanamuloni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaaradamunnadha nayanamuloni"/>
</div>
<div class="lyrico-lyrics-wrapper">Adagakane madhi sumadhura ramanini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adagakane madhi sumadhura ramanini"/>
</div>
<div class="lyrico-lyrics-wrapper">Choopina kshanamuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choopina kshanamuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Vadhilaa thanuvuni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadhilaa thanuvuni"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalisaa valupuni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalisaa valupuni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endharo mosina sundhara bhaavamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endharo mosina sundhara bhaavamu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sughunabi raamuni sonthamaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sughunabi raamuni sonthamaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Sambara veedhina aathani hrudhayamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sambara veedhina aathani hrudhayamu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chalamutho thakadhimi naatyamaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chalamutho thakadhimi naatyamaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannula mundhara devatha roopamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannula mundhara devatha roopamu"/>
</div>
<div class="lyrico-lyrics-wrapper">Choosedi bhaagyamu dhorikinadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choosedi bhaagyamu dhorikinadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappani theluputhu dhaivamu dhigina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappani theluputhu dhaivamu dhigina"/>
</div>
<div class="lyrico-lyrics-wrapper">Aapithe aagani varusa idhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aapithe aagani varusa idhi"/>
</div>
</pre>
