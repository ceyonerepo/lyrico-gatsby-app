---
title: "nigazhe sadhaa song lyrics"
album: "Kathir"
artist: "Prashant Pillai"
lyricist: "Karthik Netha"
director: "Dhinesh Palanivel"
path: "/albums/kathir-lyrics"
song: "Nigazhe Sadhaa"
image: ../../images/albumart/kathir.jpg
date: 2022-04-29
lang: tamil
youtubeLink: "https://www.youtube.com/embed/DvtDpHP-fyM"
type: "happy"
singers:
  - Sreekanth Hariharan
  - Preeti Pillai
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">nigale sadha en 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nigale sadha en "/>
</div>
<div class="lyrico-lyrics-wrapper">thunai ena varum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thunai ena varum "/>
</div>
<div class="lyrico-lyrics-wrapper">athuve sadhaa or
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athuve sadhaa or"/>
</div>
<div class="lyrico-lyrics-wrapper">anubava suvadu ena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anubava suvadu ena"/>
</div>
<div class="lyrico-lyrics-wrapper">pogum vazhi yaavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pogum vazhi yaavum"/>
</div>
<div class="lyrico-lyrics-wrapper">ethirkaalam vaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethirkaalam vaa "/>
</div>
<div class="lyrico-lyrics-wrapper">vaa engume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaa engume"/>
</div>
<div class="lyrico-lyrics-wrapper">maala athu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maala athu"/>
</div>
<div class="lyrico-lyrics-wrapper">mei vetkai ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mei vetkai ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">thedi sendraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thedi sendraal"/>
</div>
<div class="lyrico-lyrics-wrapper">thedi tharume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thedi tharume"/>
</div>
<div class="lyrico-lyrics-wrapper">oyathe urchagam ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oyathe urchagam ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">peratral maayaa vaalathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="peratral maayaa vaalathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">netru indru noolil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="netru indru noolil"/>
</div>
<div class="lyrico-lyrics-wrapper">pattam aagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pattam aagum"/>
</div>
<div class="lyrico-lyrics-wrapper">indru ellame naalai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indru ellame naalai "/>
</div>
<div class="lyrico-lyrics-wrapper">endra vaanam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="endra vaanam "/>
</div>
<div class="lyrico-lyrics-wrapper">tharume sadhaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tharume sadhaa "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pooti kondu vaalkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pooti kondu vaalkai"/>
</div>
<div class="lyrico-lyrics-wrapper">etti paarkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="etti paarkum"/>
</div>
<div class="lyrico-lyrics-wrapper">kan munnale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kan munnale"/>
</div>
<div class="lyrico-lyrics-wrapper">kaalam vanthu vaasam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaalam vanthu vaasam "/>
</div>
<div class="lyrico-lyrics-wrapper">thatume sadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thatume sadhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vaa oli mele 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaa oli mele "/>
</div>
<div class="lyrico-lyrics-wrapper">vizhum thooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vizhum thooram"/>
</div>
<div class="lyrico-lyrics-wrapper">vilaamal veelamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vilaamal veelamal"/>
</div>
<div class="lyrico-lyrics-wrapper">oor sellume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oor sellume"/>
</div>
<div class="lyrico-lyrics-wrapper">pani ondrin ulle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pani ondrin ulle"/>
</div>
<div class="lyrico-lyrics-wrapper">mazhai mella aadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mazhai mella aadum"/>
</div>
<div class="lyrico-lyrics-wrapper">vaa parada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaa parada"/>
</div>
</pre>
