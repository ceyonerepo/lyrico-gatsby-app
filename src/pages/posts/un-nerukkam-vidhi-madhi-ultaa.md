---
title: "un nerukkam song lyrics"
album: "Vidhi Madhi Ultaa"
artist: "Ashwin Vinayagamoorthy"
lyricist: "Kabilan"
director: "Vijai Balaji"
path: "/albums/vidhi-madhi-ultaa-lyrics"
song: "Un Nerukkam"
image: ../../images/albumart/vidhi-madhi-ultaa.jpg
date: 2018-01-05
lang: tamil
youtubeLink: "https://www.youtube.com/embed/5v6Id4cTD38"
type: "love"
singers:
  - Sid Sriram
  - Chinmayi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Oooooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oooooooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooouh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooouh"/>
</div>
<div class="lyrico-lyrics-wrapper">Oooooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oooooooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooouh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooouh"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohaaaaaah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohaaaaaah"/>
</div>
<div class="lyrico-lyrics-wrapper">Oooooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oooooooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooouh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooouh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un nerukkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un nerukkam"/>
</div>
<div class="lyrico-lyrics-wrapper">Un nerukkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un nerukkam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhum varaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhum varaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil irukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil irukkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un mayakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un mayakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan mayakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan mayakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullukullae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullukullae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirai iyakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirai iyakkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un madiyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un madiyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">En iravai serpen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En iravai serpen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Andhiravilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhiravilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nam idhayam korpen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam idhayam korpen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poovin magal neeyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovin magal neeyadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatrin magan naanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatrin magan naanadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Serndhae kadhai pesalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Serndhae kadhai pesalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan siragugal ini ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan siragugal ini ivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaaimadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaimadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poovin magal neeyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovin magal neeyadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatrin magan naanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatrin magan naanadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Serndhae kadhai pesalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Serndhae kadhai pesalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan siragugal ini ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan siragugal ini ivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaaimadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaimadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooo ooo aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooo ooo aaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooo oooooo ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooo oooooo ooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaar pirippaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar pirippaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Naam namai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam namai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhalae dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalae dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thozhamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozhamai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakkena nee dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkena nee dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Irukkum pozhudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irukkum pozhudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Iruvarum tholaithom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruvarum tholaithom"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanimaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanimaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Idam porul paarthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idam porul paarthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pazhagum pozhudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pazhagum pozhudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhalgalil edho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhalgalil edho"/>
</div>
<div class="lyrico-lyrics-wrapper">Inimaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inimaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naatkurippil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naatkurippil"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan ezhudhum kavidhai nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan ezhudhum kavidhai nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ooo oooooo ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooo oooooo ooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooo oooooo ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooo oooooo ooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooo oooooo ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooo oooooo ooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un nerukkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un nerukkam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nerukkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nerukkam"/>
</div>
<div class="lyrico-lyrics-wrapper">Un nerukkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un nerukkam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhum varaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhum varaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil irukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil irukkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un mayakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un mayakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan mayakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan mayakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullukullae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullukullae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirai iyakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirai iyakkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un madiyilae ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un madiyilae ae"/>
</div>
<div class="lyrico-lyrics-wrapper">En iravai serpen ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En iravai serpen ae"/>
</div>
<div class="lyrico-lyrics-wrapper">Andhiravile ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhiravile ae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nam idhayam korpen ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam idhayam korpen ae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poovin magal neeyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovin magal neeyadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatrin magan naanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatrin magan naanadi"/>
</div>
<div class="lyrico-lyrics-wrapper">yeh yeh yeh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yeh yeh yeh"/>
</div>
<div class="lyrico-lyrics-wrapper">Serndhae kadhai pesalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Serndhae kadhai pesalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan siragugal ini ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan siragugal ini ivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaaimadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaimadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmmmmmmm mmm mm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmmmmmmm mmm mm"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poovin magal neeyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovin magal neeyadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatrin magan naanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatrin magan naanadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Serndhae kadhai pesalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Serndhae kadhai pesalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan siragugal ini ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan siragugal ini ivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaaimadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaimadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmmm mm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmmm mm"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaaa aaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaa aaaaaa"/>
</div>
</pre>
