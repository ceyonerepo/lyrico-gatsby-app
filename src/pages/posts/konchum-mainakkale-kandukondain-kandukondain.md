---
title: "konchum mainakkale song lyrics"
album: "Kandukondain Kandukondain"
artist: "A. R. Rahman"
lyricist: "Vairamuthu"
director: "Rajiv Menon"
path: "/albums/kandukondain-kandukondain-lyrics"
song: "Konchum Mainakkale"
image: ../../images/albumart/kandukondain-kandukondain.jpg
date: 2000-05-05
lang: tamil
youtubeLink: "https://www.youtube.com/embed/DjvSyq8-Kdk"
type: "intro song"
singers:
  - Sadhana Sargam
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Konjum Mainakkale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjum Mainakkale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konjum Mainakaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjum Mainakaley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Kural Kettu Ondru Koodungal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kural Kettu Ondru Koodungal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konjum Mainakaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjum Mainakaley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konjum Mainakaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjum Mainakaley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Kural Kettu Ondru Koodungal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kural Kettu Ondru Koodungal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Indrey Varavendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Indrey Varavendum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Deepaavali Pandigai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Deepaavali Pandigai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indrey Varavendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indrey Varavendum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Deepaavali Pandigai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Deepaavali Pandigai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naalai Verum Kanavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalai Verum Kanavu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhai Yen Nammbanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai Yen Nammbanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naam Nattadhum Roja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam Nattadhum Roja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indrey Pookanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indrey Pookanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konjum Mainaakalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjum Mainaakalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konjum Mainaakalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjum Mainaakalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Kural Kettu Ondru Koodungal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kural Kettu Ondru Koodungal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pagalil Oru Vennilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagalil Oru Vennilaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pagalil Oru Vennilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagalil Oru Vennilaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vandhaal Paavamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhaal Paavamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iravil Oru Vaanavil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravil Oru Vaanavil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vandhaal Kutramaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhaal Kutramaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidai Sol Sol Sol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidai Sol Sol Sol"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manasukul Jal Jal Jal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasukul Jal Jal Jal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidai Sol Sol Sol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidai Sol Sol Sol"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manasukul Jal Jal Jal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasukul Jal Jal Jal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konjam Aasai Konjam Kanavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Aasai Konjam Kanavu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivai Illama Vaazhkaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivai Illama Vaazhkaiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nooru Kanavugal Kandaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nooru Kanavugal Kandaley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaru Kanavugal Palikaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaru Kanavugal Palikaadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanavey Kaiseru Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavey Kaiseru Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konjum Mainakaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjum Mainakaley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konjum Mainakaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjum Mainakaley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Kural Kettu Ondru Koodungal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kural Kettu Ondru Koodungal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Perai Solliyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Perai Solliyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuyilgal Koovattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuyilgal Koovattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enaketra Maathiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enaketra Maathiri"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paruvam Maaratum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paruvam Maaratum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Barudham Dham Dham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Barudham Dham Dham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manasukul Dhaam Dhoom Dheem
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasukul Dhaam Dhoom Dheem"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Barutham Dham Dham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Barutham Dham Dham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manasukul Dhaam Dhoom Dheem
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasukul Dhaam Dhoom Dheem"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poongaatrae Konjam Kiliththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poongaatrae Konjam Kiliththu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engal Mugavervai Pokidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engal Mugavervai Pokidum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naalai Enbadhu Kadavuluku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalai Enbadhu Kadavuluku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indru Enbadhu Manidharuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indru Enbadhu Manidharuku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhvey Vaazbavarkku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvey Vaazbavarkku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konjum Mainaakale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjum Mainaakale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konjum Mainaakale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjum Mainaakale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Kural Kettu Ondru Koodungal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kural Kettu Ondru Koodungal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Indrae Varavendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Indrae Varavendum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Deepaavali Pandigai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Deepaavali Pandigai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indreay Varavendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indreay Varavendum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Deepaavali Pandigai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Deepaavali Pandigai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naalai Vaerum Kanavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalai Vaerum Kanavu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhai Yen Nambanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai Yen Nambanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naam Nattathum Roja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam Nattathum Roja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indrae Pookanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indrae Pookanum"/>
</div>
</pre>
