---
title: "kothagaa kothagaa song lyrics"
album: "Miss India"
artist: "S. Thaman"
lyricist: "Kalyan Chakravarthi"
director: "Narendra Nath"
path: "/albums/miss-india-lyrics"
song: "Kothagaa Kothagaa"
image: ../../images/albumart/miss-india.jpg
date: 2020-11-04
lang: telugu
youtubeLink: "https://www.youtube.com/embed/9XBS6VXihMc"
type: "happy"
singers:
  - Shreya Ghoshal
  - S. Thaman
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kotthaga Kotthaga Kotthaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kotthaga Kotthaga Kotthaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Rangu Le Ningilo Ponge Saarangamayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangu Le Ningilo Ponge Saarangamayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Lipthalo Kshiptame Kaanani Kaalame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lipthalo Kshiptame Kaanani Kaalame"/>
</div>
<div class="lyrico-lyrics-wrapper">Molakale Vese Naa Sonthamayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Molakale Vese Naa Sonthamayi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninnalo Unna Neeti Chaaruni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnalo Unna Neeti Chaaruni"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannule Thongi Choosukovani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannule Thongi Choosukovani"/>
</div>
<div class="lyrico-lyrics-wrapper">Andukoleni Anthu Ledani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andukoleni Anthu Ledani"/>
</div>
<div class="lyrico-lyrics-wrapper">Anthataa Santagam Undani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anthataa Santagam Undani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Daarige Ye Maari Poindani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daarige Ye Maari Poindani"/>
</div>
<div class="lyrico-lyrics-wrapper">Daagipoledu Ga Aamani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daagipoledu Ga Aamani"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheyi Chaastunna Ee Chelimini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheyi Chaastunna Ee Chelimini"/>
</div>
<div class="lyrico-lyrics-wrapper">Chhodani Kotthaga Kotthani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chhodani Kotthaga Kotthani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sa Re Ga Ma Ni Ge Re Sa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sa Re Ga Ma Ni Ge Re Sa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sa Re Ga Ma Ni Ge Re Sa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sa Re Ga Ma Ni Ge Re Sa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sa Re Ga Ma Ni Ge Re Sa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sa Re Ga Ma Ni Ge Re Sa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sa Ge Re Ni Sa Ni Sa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sa Ge Re Ni Sa Ni Sa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koraboina Veraina Terupayi Poyena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koraboina Veraina Terupayi Poyena"/>
</div>
<div class="lyrico-lyrics-wrapper">Guruthainadi Chedaina Marupayi Neelona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Guruthainadi Chedaina Marupayi Neelona"/>
</div>
<div class="lyrico-lyrics-wrapper">Haan, Vedurulona Madhura Gaaname Vintu Unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haan, Vedurulona Madhura Gaaname Vintu Unna"/>
</div>
<div class="lyrico-lyrics-wrapper">Parusavedi Manasu Koname Choostu Unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parusavedi Manasu Koname Choostu Unna"/>
</div>
<div class="lyrico-lyrics-wrapper">Karusuleni Nagavu Bandhanaalu Teestu Unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karusuleni Nagavu Bandhanaalu Teestu Unna"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalona Ne Leni Ee Velana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalona Ne Leni Ee Velana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Toorupayi Unna Cheekatlani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Toorupayi Unna Cheekatlani"/>
</div>
<div class="lyrico-lyrics-wrapper">Wekuve Veru Chestundani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wekuve Veru Chestundani"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheruvoutunna Duralalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheruvoutunna Duralalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Chudana Velugulo Vedini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chudana Velugulo Vedini"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sa Re Ga Ma Ni Ge Re Sa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sa Re Ga Ma Ni Ge Re Sa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sa Re Ga Ma Ni Ge Re Sa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sa Re Ga Ma Ni Ge Re Sa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sa Re Ga Ma Ni Ge Re Sa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sa Re Ga Ma Ni Ge Re Sa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sa Ge Re Ni Sa Ni Sa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sa Ge Re Ni Sa Ni Sa"/>
</div>
</pre>
