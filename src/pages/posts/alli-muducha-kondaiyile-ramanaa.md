---
title: "alli muducha kondaiyile song lyrics"
album: "Ramanaa"
artist: "Ilaiyaraja"
lyricist: "Pazhani Barathi - M Metha"
director: "A.R. Murugadoss"
path: "/albums/ramanaa-lyrics"
song: "Alli Muducha Kondaiyile"
image: ../../images/albumart/ramanaa.jpg
date: 2002-11-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/rkkmfWEk0Fo"
type: "celebration"
singers:
  - Pushpavanam Kuppusamy
  - Swarnalatha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">alli muducha kondaiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alli muducha kondaiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">ada en manasa sorugi vacha pen mayile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada en manasa sorugi vacha pen mayile"/>
</div>
<div class="lyrico-lyrics-wrapper">ne enaku oru pathila sollu unmaiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ne enaku oru pathila sollu unmaiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">paaya virichu vachu padutha thookam ille
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paaya virichu vachu padutha thookam ille"/>
</div>
<div class="lyrico-lyrics-wrapper">paduthum unna nenacha rathiri mudiyavillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paduthum unna nenacha rathiri mudiyavillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sorugi vacha manasa nee avuthuthadi mayile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sorugi vacha manasa nee avuthuthadi mayile"/>
</div>
<div class="lyrico-lyrics-wrapper">aaah marachu vaikira kiliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaah marachu vaikira kiliye"/>
</div>
<div class="lyrico-lyrics-wrapper">un manasa solladi veliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un manasa solladi veliye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pipe adika pogayile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pipe adika pogayile"/>
</div>
<div class="lyrico-lyrics-wrapper">enna site adichu nisu pannum machane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna site adichu nisu pannum machane"/>
</div>
<div class="lyrico-lyrics-wrapper">vittane manmadhanum unna eavi vittane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vittane manmadhanum unna eavi vittane"/>
</div>
<div class="lyrico-lyrics-wrapper">theruvula pipe aducha thaalam pottu pakure
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theruvula pipe aducha thaalam pottu pakure"/>
</div>
<div class="lyrico-lyrics-wrapper">kodathula manasu vachu konjam konjam valiyire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kodathula manasu vachu konjam konjam valiyire"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">en manasu ipo emdi nee thooka tharen mamdi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en manasu ipo emdi nee thooka tharen mamdi"/>
</div>
<div class="lyrico-lyrics-wrapper">ne vera aala thedu ipa mathi mathi podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ne vera aala thedu ipa mathi mathi podu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aaha eduppa irukuthunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaha eduppa irukuthunu"/>
</div>
<div class="lyrico-lyrics-wrapper">idupa valaika venuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idupa valaika venuma"/>
</div>
<div class="lyrico-lyrics-wrapper">odiyuthu ila manasu othukuraye niyayama he
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="odiyuthu ila manasu othukuraye niyayama he"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">alaga pesikitu aala amukka pakure
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alaga pesikitu aala amukka pakure"/>
</div>
<div class="lyrico-lyrics-wrapper">adikadi jollu vitu application podure
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adikadi jollu vitu application podure"/>
</div>
<div class="lyrico-lyrics-wrapper">othaiyile othakada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="othaiyile othakada"/>
</div>
<div class="lyrico-lyrics-wrapper">pakkam nanum pogaiyile he
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pakkam nanum pogaiyile he"/>
</div>
<div class="lyrico-lyrics-wrapper">atha maga varaliyanu avanavanum kekurane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="atha maga varaliyanu avanavanum kekurane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kekatha kelvi yellam kekuraye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kekatha kelvi yellam kekuraye"/>
</div>
<div class="lyrico-lyrics-wrapper">nadaya kattunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nadaya kattunga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">en manasu ipo emdi nee thooka tharen mamdi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en manasu ipo emdi nee thooka tharen mamdi"/>
</div>
<div class="lyrico-lyrics-wrapper">ne vera aala thedu ipa mathi mathi podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ne vera aala thedu ipa mathi mathi podu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">alli muducha kondaiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alli muducha kondaiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">ada en manasa sorugi vacha pen mayile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada en manasa sorugi vacha pen mayile"/>
</div>
<div class="lyrico-lyrics-wrapper">ne enaku oru pathila sollu unmaiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ne enaku oru pathila sollu unmaiyile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">puducha puliyankomba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puducha puliyankomba"/>
</div>
<div class="lyrico-lyrics-wrapper">pudikanum nu nenaikiran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pudikanum nu nenaikiran"/>
</div>
<div class="lyrico-lyrics-wrapper">iducha puliya pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iducha puliya pola"/>
</div>
<div class="lyrico-lyrics-wrapper">ipo ethuku moraikire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ipo ethuku moraikire"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">puducha vituduvena pombala ne nambala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puducha vituduvena pombala ne nambala"/>
</div>
<div class="lyrico-lyrics-wrapper">ethayo puduchukitu koranga pola thongura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethayo puduchukitu koranga pola thongura"/>
</div>
<div class="lyrico-lyrics-wrapper">pichi poova vangikitu pichi pichi otharuriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pichi poova vangikitu pichi pichi otharuriye"/>
</div>
<div class="lyrico-lyrics-wrapper">achu vellam pacharisi sethidika thavuriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="achu vellam pacharisi sethidika thavuriye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">enna yengura unna thangurare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna yengura unna thangurare"/>
</div>
<div class="lyrico-lyrics-wrapper">ada ettu patiyum koti mulanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada ettu patiyum koti mulanga"/>
</div>
<div class="lyrico-lyrics-wrapper">kanaduchu jaada kaatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanaduchu jaada kaatu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sorugi vacha manasa nee avuthuthadi mayile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sorugi vacha manasa nee avuthuthadi mayile"/>
</div>
<div class="lyrico-lyrics-wrapper">aaah marachu vaikira kiliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaah marachu vaikira kiliye"/>
</div>
<div class="lyrico-lyrics-wrapper">un manasa solladi veliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un manasa solladi veliye"/>
</div>
</pre>
