---
title: "vegadha veyilula song lyrics"
album: "Adutha Saattai"
artist: "Justin Prabhakaran"
lyricist: "Yugabharathi"
director: "M. Anbazhagan"
path: "/albums/adutha-saattai-lyrics"
song: "Vegadha Veyilula"
image: ../../images/albumart/adutha-saattai.jpg
date: 2019-11-29
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ugeNQoH5q1c"
type: "happy"
singers:
  - Kanchi B. Rajeswari
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vegaatha Veyilula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vegaatha Veyilula"/>
</div>
<div class="lyrico-lyrics-wrapper">Venthaviyum Pottalula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venthaviyum Pottalula"/>
</div>
<div class="lyrico-lyrics-wrapper">Vegaatha Veyilula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vegaatha Veyilula"/>
</div>
<div class="lyrico-lyrics-wrapper">Venthaviyum Pottalula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venthaviyum Pottalula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uzhacha Appaa Ammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uzhacha Appaa Ammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vervai Intha Padippu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vervai Intha Padippu"/>
</div>
<div class="lyrico-lyrics-wrapper">Athil Velanjathintha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athil Velanjathintha"/>
</div>
<div class="lyrico-lyrics-wrapper">Mathippu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mathippu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aasaiyila Peththaeduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasaiyila Peththaeduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Anuthinam Paadupattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anuthinam Paadupattu"/>
</div>
<div class="lyrico-lyrics-wrapper">College Anuppi Vecha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="College Anuppi Vecha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaasu Illa Perusu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasu Illa Perusu"/>
</div>
<div class="lyrico-lyrics-wrapper">Avunga Kanavu Romba Perusu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avunga Kanavu Romba Perusu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ethetho Paadangala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethetho Paadangala"/>
</div>
<div class="lyrico-lyrics-wrapper">Pulla Vanthu Sollayila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pulla Vanthu Sollayila"/>
</div>
<div class="lyrico-lyrics-wrapper">Nethiyila Muththam Vachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nethiyila Muththam Vachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Neranji Pogum Kudumbam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neranji Pogum Kudumbam"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha Nenappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Nenappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhula Adangum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhula Adangum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eraatha English’a
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eraatha English’a"/>
</div>
<div class="lyrico-lyrics-wrapper">Empulla Pesuthunnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Empulla Pesuthunnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Eraatha English’a
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eraatha English’a"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Empulla Pesuthunnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Empulla Pesuthunnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Uraara Kootti Vachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uraara Kootti Vachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Peruma Pesi Kedakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peruma Pesi Kedakkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Antha Pechila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Pechila"/>
</div>
<div class="lyrico-lyrics-wrapper">Vethana Marakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vethana Marakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha Pechila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Pechila"/>
</div>
<div class="lyrico-lyrics-wrapper">Vethana Marakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vethana Marakkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vegaatha Veyilula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vegaatha Veyilula"/>
</div>
<div class="lyrico-lyrics-wrapper">Venthaviyum Pottalula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venthaviyum Pottalula"/>
</div>
<div class="lyrico-lyrics-wrapper">Vegaatha Veyilulaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vegaatha Veyilulaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Joraana Sattaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Joraana Sattaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Suththi Vara Venumunnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suththi Vara Venumunnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maatha Seettup Pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maatha Seettup Pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuni Eduththu Kodukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuni Eduththu Kodukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Athilum Varuma Vaasam Adikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athilum Varuma Vaasam Adikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kola Saami Koyilukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kola Saami Koyilukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Thala Mudiya Thaarennu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thala Mudiya Thaarennu"/>
</div>
<div class="lyrico-lyrics-wrapper">Parichaikku Anupi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parichaikku Anupi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vachcha Paasam Koduththa Maarkku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vachcha Paasam Koduththa Maarkku"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha Payanam Ethana Perukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Payanam Ethana Perukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vela Onnu Vathathunnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vela Onnu Vathathunnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vellaama Kaatta Yellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vellaama Kaatta Yellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vela Onnu Vathathunnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vela Onnu Vathathunnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vellaama Kaatta Yellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vellaama Kaatta Yellaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meettida Maattomaannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meettida Maattomaannu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thavichchi Kedakkum Oravu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thavichchi Kedakkum Oravu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayyaa Venum Onga Thayavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyaa Venum Onga Thayavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayyaa Venum Unga Thayavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyaa Venum Unga Thayavu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vegaatha Veyilula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vegaatha Veyilula"/>
</div>
<div class="lyrico-lyrics-wrapper">Venthaviyum Pottalula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venthaviyum Pottalula"/>
</div>
<div class="lyrico-lyrics-wrapper">Vegaatha Veyilula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vegaatha Veyilula"/>
</div>
<div class="lyrico-lyrics-wrapper">Venthaviyum Pottalula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venthaviyum Pottalula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uzhacha Appaa Ammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uzhacha Appaa Ammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vervai Intha Padippu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vervai Intha Padippu"/>
</div>
<div class="lyrico-lyrics-wrapper">Athil Velanjathintha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athil Velanjathintha"/>
</div>
<div class="lyrico-lyrics-wrapper">Mathippu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mathippu"/>
</div>
</pre>
