---
title: "voice of unity song lyrics"
album: "Maanaadu"
artist: "Yuvan Shankar Raja"
lyricist: "Arivu"
director: "Venkat Prabhu"
path: "/albums/maanaadu-lyrics"
song: "Voice Of Unity"
image: ../../images/albumart/maanaadu.jpg
date: 2021-06-21
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Nag7tYultXQ"
type: "Mass"
singers:
  - Silambarasan
  - Arivu
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Oru Naadidhu Endraalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Naadidhu Endraalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pala Naadugalin Koodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala Naadugalin Koodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirupanmaigal Illamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirupanmaigal Illamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Perumbanmaigal Ingedhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perumbanmaigal Ingedhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nadhi Neeradhu Nilladhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadhi Neeradhu Nilladhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Anaiyo Thadai Solladhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anaiyo Thadai Solladhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Madha Megangal Ingedhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madha Megangal Ingedhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Podhuvaanadhu Nam Naadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhuvaanadhu Nam Naadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jananaayagam Illadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jananaayagam Illadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nam Thaayagam Velladhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam Thaayagam Velladhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Iru Naanayathin Pakkam!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iru Naanayathin Pakkam!"/>
</div>
<div class="lyrico-lyrics-wrapper">Aranaaga Mozhi Nikkum!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aranaaga Mozhi Nikkum!"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hindu Muslim Christ-uh..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hindu Muslim Christ-uh.."/>
</div>
<div class="lyrico-lyrics-wrapper">Namma Poorvakudi First-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma Poorvakudi First-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Vandhadhamma Twist-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Vandhadhamma Twist-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma Sandhadhikae Stress-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma Sandhadhikae Stress-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Veraai Naanum Veraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Veraai Naanum Veraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naam Aanom Naangu Peraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam Aanom Naangu Peraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaraaro Aandu Kolla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaraaro Aandu Kolla"/>
</div>
<div class="lyrico-lyrics-wrapper">Veeraadhi Veeram Solla!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veeraadhi Veeram Solla!"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aagaayam Yerum Kaalam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagaayam Yerum Kaalam "/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanaalum Oorin Oaram 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanaalum Oorin Oaram "/>
</div>
<div class="lyrico-lyrics-wrapper">Oaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeraalam Kodi Pergal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeraalam Kodi Pergal "/>
</div>
<div class="lyrico-lyrics-wrapper">Pergal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pergal"/>
</div>
<div class="lyrico-lyrics-wrapper">Seramal Vazhum Kolam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seramal Vazhum Kolam "/>
</div>
<div class="lyrico-lyrics-wrapper">Kolam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Madhamaaya Vittadhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhamaaya Vittadhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Samiudhayam Munnerum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samiudhayam Munnerum"/>
</div>
<div class="lyrico-lyrics-wrapper">Adaiyaalam Paakama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adaiyaalam Paakama"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellam Onnavom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellam Onnavom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sirupanmai Illaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirupanmai Illaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Perumbaanmai Vaazhadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perumbaanmai Vaazhadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sama Needhi Thandhalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sama Needhi Thandhalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sanda Varadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sanda Varadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kizhakula Adicha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kizhakula Adicha"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu Valikalayae Vadakkuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu Valikalayae Vadakkuku"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarithiram Padicha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarithiram Padicha"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhil Edamillayae Madhathukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhil Edamillayae Madhathukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadavula Padaichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadavula Padaichu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sada Sadangaiyellam Nadathittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sada Sadangaiyellam Nadathittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Manushana Verutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manushana Verutha"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu Varam Tharuma Janathuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu Varam Tharuma Janathuku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Veraai Naanum Veraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Veraai Naanum Veraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naam Aanom Naangu Peraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam Aanom Naangu Peraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaraaro Aandu Kolla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaraaro Aandu Kolla"/>
</div>
<div class="lyrico-lyrics-wrapper">Veeraadhi Veeram Solla!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veeraadhi Veeram Solla!"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aagaayam Yerum Kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagaayam Yerum Kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanaalum Oorin Oaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanaalum Oorin Oaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeraalam Kodi Pergal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeraalam Kodi Pergal"/>
</div>
<div class="lyrico-lyrics-wrapper">Seramal Vazhum Kolam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seramal Vazhum Kolam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Madhamaaya Vittadhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhamaaya Vittadhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Samiudhayam Munnerum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samiudhayam Munnerum"/>
</div>
<div class="lyrico-lyrics-wrapper">Adaiyaalam Paakama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adaiyaalam Paakama"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellam Onnavom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellam Onnavom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sirupanmai Illaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirupanmai Illaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Perumbaanmai Vaazhadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perumbaanmai Vaazhadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sama Needhi Thandhalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sama Needhi Thandhalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sanda Varadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sanda Varadhu"/>
</div>
</pre>
