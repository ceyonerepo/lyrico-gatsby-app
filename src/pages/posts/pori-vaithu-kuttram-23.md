---
title: "pori vaithu song lyrics"
album: "Kuttram 23"
artist: "Vishal Chandrasekhar"
lyricist: "Viveka"
director: "Arivazhagan Venkatachalam"
path: "/albums/kuttram-23-lyrics"
song: "Pori Vaithu"
image: ../../images/albumart/kuttram-23.jpg
date: 2017-03-03
lang: tamil
youtubeLink: "https://www.youtube.com/embed/RtvFrRlj-o4"
type: "love"
singers:
  - Vijay Prakash
  - Shweta Mohan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Pori vaithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pori vaithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaidhu seiyyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaidhu seiyyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pennin kannilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pennin kannilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Puriyaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puriyaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Maattikondenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maattikondenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iduppuku keezhe soodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iduppuku keezhe soodum"/>
</div>
<div class="lyrico-lyrics-wrapper">Irakkam kooda illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irakkam kooda illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayathil kaayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayathil kaayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kandenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Midukkaagha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Midukkaagha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirigindra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirigindra"/>
</div>
<div class="lyrico-lyrics-wrapper">En thimirai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En thimirai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thindraalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thindraalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thudukkaagha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thudukkaagha"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir melae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir melae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuppaakki 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuppaakki "/>
</div>
<div class="lyrico-lyrics-wrapper">vaithaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaithaalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kumilullae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kumilullae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirai sendra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirai sendra"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaattraaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattraaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalithenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalithenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aagaayam enai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagaayam enai"/>
</div>
<div class="lyrico-lyrics-wrapper">Theendudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theendudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pori vaithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pori vaithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaidhu seiyyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaidhu seiyyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pennin kannilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pennin kannilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Puriyaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puriyaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Maattikondenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maattikondenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iduppuku keezhe soodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iduppuku keezhe soodum"/>
</div>
<div class="lyrico-lyrics-wrapper">Irakkam kooda illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irakkam kooda illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayathil kaayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayathil kaayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kandenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Seerudai pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seerudai pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadandhavanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadandhavanai"/>
</div>
<div class="lyrico-lyrics-wrapper">Siragugal pootti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siragugal pootti"/>
</div>
<div class="lyrico-lyrics-wrapper">Parakka vaiththaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parakka vaiththaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Udaigalil thaiththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udaigalil thaiththa"/>
</div>
<div class="lyrico-lyrics-wrapper">Natchathiram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natchathiram"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyara vaiththaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyara vaiththaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodimaram pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodimaram pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Nindravanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nindravanai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nodimaram pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nodimaram pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Suruttivittaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suruttivittaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idi vilundhaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idi vilundhaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Siriththavanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siriththavanai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilaga vaithaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilaga vaithaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nimidam illaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nimidam illaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Sulandru vandhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sulandru vandhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Nirandharamaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nirandharamaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Abagarithaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Abagarithaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadamai kellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadamai kellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidumuraigal podavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidumuraigal podavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai paniththaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai paniththaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Blue skies make way 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Blue skies make way "/>
</div>
<div class="lyrico-lyrics-wrapper">for rainbows 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="for rainbows "/>
</div>
<div class="lyrico-lyrics-wrapper">and butterflies
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="and butterflies"/>
</div>
<div class="lyrico-lyrics-wrapper">Sunshine brings 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sunshine brings "/>
</div>
<div class="lyrico-lyrics-wrapper">in a warm 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="in a warm "/>
</div>
<div class="lyrico-lyrics-wrapper">new paradise
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="new paradise"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Blue skies make way 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Blue skies make way "/>
</div>
<div class="lyrico-lyrics-wrapper">for rainbows 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="for rainbows "/>
</div>
<div class="lyrico-lyrics-wrapper">and butterflies
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="and butterflies"/>
</div>
<div class="lyrico-lyrics-wrapper">Sunshine brings 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sunshine brings "/>
</div>
<div class="lyrico-lyrics-wrapper">in a warm 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="in a warm "/>
</div>
<div class="lyrico-lyrics-wrapper">new paradise
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="new paradise"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Padigalai thaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padigalai thaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee nuzhaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee nuzhaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Paravasamachu en veedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paravasamachu en veedu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nodigalum kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nodigalum kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Nazhuvamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nazhuvamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhugiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhugiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kudai pidithu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudai pidithu "/>
</div>
<div class="lyrico-lyrics-wrapper">oodum unarughae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oodum unarughae"/>
</div>
<div class="lyrico-lyrics-wrapper">Eda othukeedu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eda othukeedu "/>
</div>
<div class="lyrico-lyrics-wrapper">peruvatharkkae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="peruvatharkkae"/>
</div>
<div class="lyrico-lyrics-wrapper">Muyandridum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muyandridum "/>
</div>
<div class="lyrico-lyrics-wrapper">eera kaatroodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eera kaatroodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Modhugiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Modhugiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadalinil sernthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadalinil sernthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neelathai pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelathai pol"/>
</div>
<div class="lyrico-lyrics-wrapper">Uravughaloodu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uravughaloodu "/>
</div>
<div class="lyrico-lyrics-wrapper">kalanthuvittai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalanthuvittai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhaigalil ketta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhaigalil ketta"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhkkaiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhkkaiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennakku nee 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennakku nee "/>
</div>
<div class="lyrico-lyrics-wrapper">kaatti vittai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatti vittai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pori vaithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pori vaithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaidhu seiyyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaidhu seiyyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pennin kannilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pennin kannilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Puriyaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puriyaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Maattikondenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maattikondenae"/>
</div>
</pre>
