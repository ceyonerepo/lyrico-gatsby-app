---
title: "paavadai song lyrics"
album: "Aaruthra"
artist: "Vidyasagar"
lyricist: "Pa Vijay"
director: "Pa Vijay"
path: "/albums/aaruthra-lyrics"
song: "Paavadai"
image: ../../images/albumart/aaruthra.jpg
date: 2018-08-31
lang: tamil
youtubeLink: "https://www.youtube.com/embed/jYkmVOz5UiA"
type: "happy"
singers:
  - Sanjana Kalmanje
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">paavaada thavaniyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paavaada thavaniyil"/>
</div>
<div class="lyrico-lyrics-wrapper">paarthathu illaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paarthathu illaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">intha maanada mayilaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha maanada mayilaada"/>
</div>
<div class="lyrico-lyrics-wrapper">aasai illaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aasai illaiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">paavaada thavaniyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paavaada thavaniyil"/>
</div>
<div class="lyrico-lyrics-wrapper">paarthathu illaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paarthathu illaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">intha maanada mayilaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha maanada mayilaada"/>
</div>
<div class="lyrico-lyrics-wrapper">aasai illaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aasai illaiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ithu velli nila thulli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu velli nila thulli"/>
</div>
<div class="lyrico-lyrics-wrapper">varum velai illaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varum velai illaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">muthu pallakula aeri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muthu pallakula aeri"/>
</div>
<div class="lyrico-lyrics-wrapper">vantha raani illaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vantha raani illaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">enna venamunnu sonna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna venamunnu sonna"/>
</div>
<div class="lyrico-lyrics-wrapper">athu vesamillaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu vesamillaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">mosamillaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mosamillaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">sirappu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sirappu "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">paavaada thavaniyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paavaada thavaniyil"/>
</div>
<div class="lyrico-lyrics-wrapper">paarthathu illaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paarthathu illaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">intha maanada mayilaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha maanada mayilaada"/>
</div>
<div class="lyrico-lyrics-wrapper">aasai illaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aasai illaiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">maligai poo vacha than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maligai poo vacha than"/>
</div>
<div class="lyrico-lyrics-wrapper">koonthalukku sirappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koonthalukku sirappu"/>
</div>
<div class="lyrico-lyrics-wrapper">en machan than vachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en machan than vachu"/>
</div>
<div class="lyrico-lyrics-wrapper">vitta maligaike sirappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vitta maligaike sirappu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">maligai poo vacha than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maligai poo vacha than"/>
</div>
<div class="lyrico-lyrics-wrapper">koonthalukku sirappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koonthalukku sirappu"/>
</div>
<div class="lyrico-lyrics-wrapper">en machan than vachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en machan than vachu"/>
</div>
<div class="lyrico-lyrics-wrapper">vitta maligaike sirappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vitta maligaike sirappu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaal kolusu katina than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaal kolusu katina than"/>
</div>
<div class="lyrico-lyrics-wrapper">kaalgalukku sirappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaalgalukku sirappu"/>
</div>
<div class="lyrico-lyrics-wrapper">kaal kolusu katina than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaal kolusu katina than"/>
</div>
<div class="lyrico-lyrics-wrapper">kaalgalukku sirappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaalgalukku sirappu"/>
</div>
<div class="lyrico-lyrics-wrapper">athu katilile ketta thanya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu katilile ketta thanya"/>
</div>
<div class="lyrico-lyrics-wrapper">kacherike sirappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kacherike sirappu"/>
</div>
<div class="lyrico-lyrics-wrapper">kacherike sirappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kacherike sirappu"/>
</div>
<div class="lyrico-lyrics-wrapper">sirappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sirappu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">paavaada thavaniyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paavaada thavaniyil"/>
</div>
<div class="lyrico-lyrics-wrapper">paarthathu illaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paarthathu illaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">intha maanada mayilaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha maanada mayilaada"/>
</div>
<div class="lyrico-lyrics-wrapper">aasai illaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aasai illaiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">mookuthiyum kuthikitaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mookuthiyum kuthikitaa"/>
</div>
<div class="lyrico-lyrics-wrapper">mugathuku sirappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mugathuku sirappu"/>
</div>
<div class="lyrico-lyrics-wrapper">athu maaman nenjil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu maaman nenjil"/>
</div>
<div class="lyrico-lyrics-wrapper">kuthum bothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuthum bothu"/>
</div>
<div class="lyrico-lyrics-wrapper">mookuthike sirappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mookuthike sirappu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">mookuthiyum kuthikitaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mookuthiyum kuthikitaa"/>
</div>
<div class="lyrico-lyrics-wrapper">mugathuku sirappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mugathuku sirappu"/>
</div>
<div class="lyrico-lyrics-wrapper">athu maaman nenjil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu maaman nenjil"/>
</div>
<div class="lyrico-lyrics-wrapper">kuthum bothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuthum bothu"/>
</div>
<div class="lyrico-lyrics-wrapper">mookuthike sirappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mookuthike sirappu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nethi pottu vachu vitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nethi pottu vachu vitta"/>
</div>
<div class="lyrico-lyrics-wrapper">nesaththuku sirappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nesaththuku sirappu"/>
</div>
<div class="lyrico-lyrics-wrapper">athu mutham pattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu mutham pattu"/>
</div>
<div class="lyrico-lyrics-wrapper">thenjaa thaanyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thenjaa thaanyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">mothathukkum sirappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mothathukkum sirappu"/>
</div>
<div class="lyrico-lyrics-wrapper">mothathukkum sirappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mothathukkum sirappu"/>
</div>
<div class="lyrico-lyrics-wrapper">sirappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sirappu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">paavaada thavaniyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paavaada thavaniyil"/>
</div>
<div class="lyrico-lyrics-wrapper">paarthathu illaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paarthathu illaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">intha maanada mayilaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha maanada mayilaada"/>
</div>
<div class="lyrico-lyrics-wrapper">aasai illaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aasai illaiya"/>
</div>
</pre>
