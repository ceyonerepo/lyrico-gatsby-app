---
title: "letha letha song lyrics"
album: "Master"
artist: "Anirudh Ravichander"
lyricist: "Krishna Kanth"
director: "Lokesh Kanakaraj"
path: "/albums/master-lyrics"
song: "Letha Letha Gundelu - Manasula Karagani"
image: ../../images/albumart/master.jpg
date: 2021-01-13
lang: telugu
youtubeLink: "https://www.youtube.com/embed/ADQG-gSCDH8"
type: "sad"
singers:
  - Mayukh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Manasule karagani lokame lokama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasule karagani lokame lokama"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasule karagani lokame lokama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasule karagani lokame lokama"/>
</div>
<div class="lyrico-lyrics-wrapper">Iruku gadhulalo are mogge bathukule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruku gadhulalo are mogge bathukule"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee thirigi egaraga konchem aase kaligile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thirigi egaraga konchem aase kaligile"/>
</div>
<div class="lyrico-lyrics-wrapper">Velugu virisile ningi vollu virichele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velugu virisile ningi vollu virichele"/>
</div>
<div class="lyrico-lyrics-wrapper">Mari rekkalegarale gadhi thalupu viragale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mari rekkalegarale gadhi thalupu viragale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Letha letha gundelemo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Letha letha gundelemo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooprigaagi poyena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooprigaagi poyena"/>
</div>
<div class="lyrico-lyrics-wrapper">Ikkadunna kaatinunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ikkadunna kaatinunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendu okate aayena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendu okate aayena"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannerantu pongithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannerantu pongithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvve thuduchuko eeda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvve thuduchuko eeda"/>
</div>
<div class="lyrico-lyrics-wrapper">Amma naanna evaru leru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma naanna evaru leru"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhadhe anuchuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhadhe anuchuko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pothe poniraa chache bathuku maadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pothe poniraa chache bathuku maadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudhute padavule arupulika mave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudhute padavule arupulika mave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manasule karagani lokame lokama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasule karagani lokame lokama"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasule karagani lokame lokama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasule karagani lokame lokama"/>
</div>
</pre>
