---
title: "hola amigo song lyrics"
album: "Rum"
artist: "Anirudh Ravichander"
lyricist: "Madhan Karky - Balan Kashmir"
director: "Sai Bharath"
path: "/albums/rum-lyrics"
song: "Hola Amigo"
image: ../../images/albumart/rum.jpg
date: 2017-02-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/MX_olljGHUA"
type: "happy"
singers:
  - Anirudh Ravichander
  - Balan Kashmir
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hola Amigo Hola Amigo Hola Senorita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hola Amigo Hola Amigo Hola Senorita"/>
</div>
<div class="lyrico-lyrics-wrapper">Hola Amigo Hola Amigo Hola Senorita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hola Amigo Hola Amigo Hola Senorita"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adutha Theruvil Irukkum Kuppaiku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adutha Theruvil Irukkum Kuppaiku"/>
</div>
<div class="lyrico-lyrics-wrapper">Mooka Nee Mooduriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mooka Nee Mooduriye"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkul Kuvinju Kidakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkul Kuvinju Kidakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuppaiye Marachu Ooduriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuppaiye Marachu Ooduriye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnoda Aaloda Azhaga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnoda Aaloda Azhaga "/>
</div>
<div class="lyrico-lyrics-wrapper">Azhaga Izhuthu Mooduriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhaga Izhuthu Mooduriye"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennoda Aaloda Azhaga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennoda Aaloda Azhaga "/>
</div>
<div class="lyrico-lyrics-wrapper">Rasikka Paakura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rasikka Paakura"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum Ozhungilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Ozhungilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum Ozhungilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum Ozhungilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorum Ozhungilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorum Ozhungilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarum Ozhungilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarum Ozhungilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Vela Unakilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vela Unakilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Neram Enakilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neram Enakilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Adichu Thiruthida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adichu Thiruthida"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum Kanakilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum Kanakilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hola Hola Amigo Hola Senorita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hola Hola Amigo Hola Senorita"/>
</div>
<div class="lyrico-lyrics-wrapper">Hola Hola Amigo Hola Senorita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hola Hola Amigo Hola Senorita"/>
</div>
<div class="lyrico-lyrics-wrapper">Hola Hola Amigo Hola Senorita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hola Hola Amigo Hola Senorita"/>
</div>
<div class="lyrico-lyrics-wrapper">Hola Hola Amigo Senorita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hola Hola Amigo Senorita"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hola Hola Amigo Hola Senorita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hola Hola Amigo Hola Senorita"/>
</div>
<div class="lyrico-lyrics-wrapper">Hola Hola Amigo Hola Senorita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hola Hola Amigo Hola Senorita"/>
</div>
<div class="lyrico-lyrics-wrapper">Hola Hola Amigo Hola Senorita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hola Hola Amigo Hola Senorita"/>
</div>
<div class="lyrico-lyrics-wrapper">Hola Hola Amigo Hola Senorita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hola Hola Amigo Hola Senorita"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaasukaa Avan Oozhal Senje Thinnuvaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasukaa Avan Oozhal Senje Thinnuvaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanaalum Nee Vote Pote Yemathuviye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanaalum Nee Vote Pote Yemathuviye"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaasukaa Avan Match Fix Pannuvaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasukaa Avan Match Fix Pannuvaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanalum Nee TV Paathu Kathuviye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanalum Nee TV Paathu Kathuviye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lanjam Vaangum Cop-ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lanjam Vaangum Cop-ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Helmet Illa Top-ah?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Helmet Illa Top-ah?"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Sollu Yethu Wrong-nu?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Sollu Yethu Wrong-nu?"/>
</div>
<div class="lyrico-lyrics-wrapper">Beep Song ah? Kettu Aadum Gang-ah?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Beep Song ah? Kettu Aadum Gang-ah?"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Sollu Yethu Thappu-nu?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Sollu Yethu Thappu-nu?"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hola Hola Amigo Hola Senorita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hola Hola Amigo Hola Senorita"/>
</div>
<div class="lyrico-lyrics-wrapper">Hola Hola Amigo Hola Senorita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hola Hola Amigo Hola Senorita"/>
</div>
<div class="lyrico-lyrics-wrapper">Hola Hola Amigo Hola Senorita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hola Hola Amigo Hola Senorita"/>
</div>
<div class="lyrico-lyrics-wrapper">Hola Hola Amigo Senorita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hola Hola Amigo Senorita"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hola Hola Amigo Hola Senorita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hola Hola Amigo Hola Senorita"/>
</div>
<div class="lyrico-lyrics-wrapper">Hola Hola Amigo Hola Senorita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hola Hola Amigo Hola Senorita"/>
</div>
<div class="lyrico-lyrics-wrapper">Hola Hola Amigo Hola Senorita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hola Hola Amigo Hola Senorita"/>
</div>
<div class="lyrico-lyrics-wrapper">Hola Hola Amigo Hola Senorita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hola Hola Amigo Hola Senorita"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">See I Came From 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="See I Came From "/>
</div>
<div class="lyrico-lyrics-wrapper">The Bottom Boy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="The Bottom Boy"/>
</div>
<div class="lyrico-lyrics-wrapper">Not Big Not King 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Not Big Not King "/>
</div>
<div class="lyrico-lyrics-wrapper">Just Call Me A Dreamer
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Just Call Me A Dreamer"/>
</div>
<div class="lyrico-lyrics-wrapper">And I Rise At The Sun Boy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="And I Rise At The Sun Boy"/>
</div>
<div class="lyrico-lyrics-wrapper">Never Been Fit Cuz 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Never Been Fit Cuz "/>
</div>
<div class="lyrico-lyrics-wrapper">Then You Erase The Leader
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Then You Erase The Leader"/>
</div>
<div class="lyrico-lyrics-wrapper">So Who Wanna Stop Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="So Who Wanna Stop Me"/>
</div>
<div class="lyrico-lyrics-wrapper">Please Dont Try Me!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Please Dont Try Me!"/>
</div>
<div class="lyrico-lyrics-wrapper">I Feel Nothing
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Feel Nothing"/>
</div>
<div class="lyrico-lyrics-wrapper">Only God Can Judge Me!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Only God Can Judge Me!"/>
</div>
<div class="lyrico-lyrics-wrapper">Please Ask Around
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Please Ask Around"/>
</div>
<div class="lyrico-lyrics-wrapper">I Dont Need The Crown
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Dont Need The Crown"/>
</div>
<div class="lyrico-lyrics-wrapper">Do Tell My Enemies
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Do Tell My Enemies"/>
</div>
<div class="lyrico-lyrics-wrapper">I Wont Stay Down
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Wont Stay Down"/>
</div>
<div class="lyrico-lyrics-wrapper">And I Wont Stay Down
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="And I Wont Stay Down"/>
</div>
<div class="lyrico-lyrics-wrapper">And I Can’t Give Up
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="And I Can’t Give Up"/>
</div>
<div class="lyrico-lyrics-wrapper">And If I Fall
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="And If I Fall"/>
</div>
<div class="lyrico-lyrics-wrapper">Do Raise Me Up!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Do Raise Me Up!"/>
</div>
<div class="lyrico-lyrics-wrapper">And I Wanna Live It Up!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="And I Wanna Live It Up!"/>
</div>
<div class="lyrico-lyrics-wrapper">And I Wanna Raise It Up!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="And I Wanna Raise It Up!"/>
</div>
<div class="lyrico-lyrics-wrapper">And I Wanna Shut It Down
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="And I Wanna Shut It Down"/>
</div>
<div class="lyrico-lyrics-wrapper">Tell ’em
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tell ’em"/>
</div>
<div class="lyrico-lyrics-wrapper">Till My People Live It Up
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Till My People Live It Up"/>
</div>
<div class="lyrico-lyrics-wrapper">Till My Girls Raise It Up
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Till My Girls Raise It Up"/>
</div>
<div class="lyrico-lyrics-wrapper">Coz I Wanna Put It Down
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Coz I Wanna Put It Down"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hola Hola Amigo Hola Senorita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hola Hola Amigo Hola Senorita"/>
</div>
<div class="lyrico-lyrics-wrapper">Hola Hola Amigo Hola Senorita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hola Hola Amigo Hola Senorita"/>
</div>
<div class="lyrico-lyrics-wrapper">Hola Hola Amigo Hola Senorita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hola Hola Amigo Hola Senorita"/>
</div>
<div class="lyrico-lyrics-wrapper">Hola Hola Amigo Hola Senorita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hola Hola Amigo Hola Senorita"/>
</div>
</pre>
