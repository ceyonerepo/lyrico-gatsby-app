---
title: "epodhum unmael nyabagam song lyrics"
album: "Nimir"
artist: "Darbuka Siva - B. Ajaneesh Loknath"
lyricist: "Thamarai"
director: "Priyadarshan"
path: "/albums/nimir-lyrics"
song: "Epodhum Unmael Nyabagam"
image: ../../images/albumart/nimir.jpg
date: 2018-01-26
lang: tamil
youtubeLink: "https://www.youtube.com/embed/btgjtnhHWoE"
type: "love"
singers:
  - Vijay Prakash
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Epodhum unmel nyabagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Epodhum unmel nyabagam"/>
</div>
<div class="lyrico-lyrics-wrapper">En ullathai eertha poomugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En ullathai eertha poomugam"/>
</div>
<div class="lyrico-lyrics-wrapper">Andraadum unnai paarkkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andraadum unnai paarkkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">En anbellaam kotti theerkkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En anbellaam kotti theerkkanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arugil vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arugil vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ena azhaikka vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ena azhaikka vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Viral korkkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viral korkkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thozh saaikkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozh saaikkanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Epodhum unmel nyabagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Epodhum unmel nyabagam"/>
</div>
<div class="lyrico-lyrics-wrapper">En ullathai eertha poomugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En ullathai eertha poomugam"/>
</div>
<div class="lyrico-lyrics-wrapper">Andraadum unnai paarkkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andraadum unnai paarkkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">En anbellaam kotti theerkkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En anbellaam kotti theerkkanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andhi veyil adangayil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhi veyil adangayil"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum neeyum pesanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum neeyum pesanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sathamidum valayalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sathamidum valayalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Sangam vaithu theerkkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sangam vaithu theerkkanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pakkathilae vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkathilae vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vekkathiyum thandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vekkathiyum thandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sokka vaithaai ennai andru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sokka vaithaai ennai andru"/>
</div>
<div class="lyrico-lyrics-wrapper">Selai pola naanum thozhil sariyanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Selai pola naanum thozhil sariyanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Epodhum unmel nyabagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Epodhum unmel nyabagam"/>
</div>
<div class="lyrico-lyrics-wrapper">En ullathai eertha poomugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En ullathai eertha poomugam"/>
</div>
<div class="lyrico-lyrics-wrapper">Andraadum unnai paarkkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andraadum unnai paarkkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">En anbellaam kotti theerkkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En anbellaam kotti theerkkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Arugil vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arugil vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ena azhaikka vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ena azhaikka vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Viral korkkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viral korkkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thozh saaikkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozh saaikkanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna idhu uyirilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna idhu uyirilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oonjal ondru aadudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oonjal ondru aadudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Engirindho udalaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engirindho udalaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaichal vandhu moodudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaichal vandhu moodudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pathu viral theyyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathu viral theyyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nithiraiyum pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nithiraiyum pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Yethanai naal indha vaaathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethanai naal indha vaaathai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thendral kooda karunai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thendral kooda karunai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kondu thazhuvudheheyyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondu thazhuvudheheyyy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Epodhum unmel nyabagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Epodhum unmel nyabagam"/>
</div>
<div class="lyrico-lyrics-wrapper">En ullathai eertha poomugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En ullathai eertha poomugam"/>
</div>
<div class="lyrico-lyrics-wrapper">Andraadum unnai paarkkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andraadum unnai paarkkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">En anbellaam kotti theerkkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En anbellaam kotti theerkkanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arugil vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arugil vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ena azhaikka vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ena azhaikka vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Viral korkkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viral korkkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thozh saaikkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozh saaikkanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Epodhum unmel nyabagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Epodhum unmel nyabagam"/>
</div>
<div class="lyrico-lyrics-wrapper">En ullathai eertha poomugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En ullathai eertha poomugam"/>
</div>
<div class="lyrico-lyrics-wrapper">Andraadum unnai paarkkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andraadum unnai paarkkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">En anbellaam kotti theerkkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En anbellaam kotti theerkkanum"/>
</div>
</pre>
