---
title: "zombie reddy theme song lyrics"
album: "Zombie Reddy"
artist: "Mark K. Robin"
lyricist: "Mama Singh"
director: "Prasanth Varma"
path: "/albums/zombie-reddy-lyrics"
song: "Zombie Reddy Theme - Kallu Erraga"
image: ../../images/albumart/zombie-reddy.jpg
date: 2021-02-05
lang: telugu
youtubeLink: "https://www.youtube.com/embed/bmr0PVRHDMI"
type: "theme song"
singers:
  - Mama Sing
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kallu erraga gollu nallaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallu erraga gollu nallaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ollu sallaga saavu mellaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ollu sallaga saavu mellaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Aakali aagaka ooge naluka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aakali aagaka ooge naluka"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanivi theeraga ninnu korukutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanivi theeraga ninnu korukutha"/>
</div>
<div class="lyrico-lyrics-wrapper">Koruku koruku koruku koruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koruku koruku koruku koruku"/>
</div>
<div class="lyrico-lyrics-wrapper">Narukuthunna korukutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Narukuthunna korukutha"/>
</div>
<div class="lyrico-lyrics-wrapper">Uruku uruku uruku uruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uruku uruku uruku uruku"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhorike varaku urukutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhorike varaku urukutha"/>
</div>
<div class="lyrico-lyrics-wrapper">Koruku koruku koruku koruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koruku koruku koruku koruku"/>
</div>
<div class="lyrico-lyrics-wrapper">Narukuthunna korukutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Narukuthunna korukutha"/>
</div>
<div class="lyrico-lyrics-wrapper">Uruku uruku uruku uruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uruku uruku uruku uruku"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhorike varaku urukutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhorike varaku urukutha"/>
</div>
<div class="lyrico-lyrics-wrapper">Manishi karichene medhadu dhobbene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manishi karichene medhadu dhobbene"/>
</div>
<div class="lyrico-lyrics-wrapper">Zombie reddy Zombie reddy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zombie reddy Zombie reddy"/>
</div>
<div class="lyrico-lyrics-wrapper">Marige dahame raktha dahame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marige dahame raktha dahame"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaavi varusa lene ledu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaavi varusa lene ledu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vasanosthe valla kadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vasanosthe valla kadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Zombie reddy Zombie reddy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zombie reddy Zombie reddy"/>
</div>
<div class="lyrico-lyrics-wrapper">Ratri pagalu theda ledu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ratri pagalu theda ledu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooru vaada valla kaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooru vaada valla kaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Zombie reddy Zombie reddy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zombie reddy Zombie reddy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nannu podichina noppi ledhule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannu podichina noppi ledhule"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaallu narikina aadugu aagadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaallu narikina aadugu aagadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Pichhi kukkalaa ninnu tharumutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pichhi kukkalaa ninnu tharumutha"/>
</div>
<div class="lyrico-lyrics-wrapper">Narula jathine roopu maaputha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Narula jathine roopu maaputha"/>
</div>
<div class="lyrico-lyrics-wrapper">Uruku uruku uruku uruku uruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uruku uruku uruku uruku uruku"/>
</div>
<div class="lyrico-lyrics-wrapper">Zombie reddy Zombie reddy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zombie reddy Zombie reddy"/>
</div>
<div class="lyrico-lyrics-wrapper">Koruku koruku koruku koruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koruku koruku koruku koruku"/>
</div>
<div class="lyrico-lyrics-wrapper">Zombie reddy Zombie reddy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zombie reddy Zombie reddy"/>
</div>
<div class="lyrico-lyrics-wrapper">Thodalu kotte veerulaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodalu kotte veerulaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Medhadu korukuthanu nenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Medhadu korukuthanu nenu"/>
</div>
<div class="lyrico-lyrics-wrapper">Zombie reddy Zombie reddy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zombie reddy Zombie reddy"/>
</div>
<div class="lyrico-lyrics-wrapper">Kali kalamandu nene raju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kali kalamandu nene raju"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaala yamudu mogudu nenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaala yamudu mogudu nenu"/>
</div>
<div class="lyrico-lyrics-wrapper">Zombie reddy Zombie reddy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zombie reddy Zombie reddy"/>
</div>
</pre>
