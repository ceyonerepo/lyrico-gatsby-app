---
title: "oh saaiyaan oh saaiyaan song lyrics"
album: "The Power"
artist: "Salim - Sulaiman"
lyricist: "Kumaar"
director: "Mahesh Manjrekar"
path: "/albums/the-power-lyrics"
song: "Oh Saaiyaan Oh Saaiyaan"
image: ../../images/albumart/the-power.jpg
date: 2021-01-14
lang: hindi
youtubeLink: "https://www.youtube.com/embed/zG_T0wXEbYE"
type: "love"
singers:
  - Arijit Singh
  - Raj Pandit
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Mere haathon mein hai ik dua
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mere haathon mein hai ik dua"/>
</div>
<div class="lyrico-lyrics-wrapper">Usse sun raha hai woh khuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usse sun raha hai woh khuda"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho mere haathon mein hai ik dua
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho mere haathon mein hai ik dua"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho usse sun raha hai woh khuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho usse sun raha hai woh khuda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaade mere pakke ve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaade mere pakke ve"/>
</div>
<div class="lyrico-lyrics-wrapper">Tere liye rakhe ve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere liye rakhe ve"/>
</div>
<div class="lyrico-lyrics-wrapper">Umar bhar nibhawan changiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Umar bhar nibhawan changiyan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh saaiyaan, oh saaiyaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh saaiyaan, oh saaiyaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Sun le tu meri arziyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sun le tu meri arziyan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tere naal mausam saare vekh laan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere naal mausam saare vekh laan"/>
</div>
<div class="lyrico-lyrics-wrapper">Haye.. Dhoop hove, hove chahe baarishan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haye.. Dhoop hove, hove chahe baarishan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho rakhi bas mere te yakeen tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho rakhi bas mere te yakeen tu"/>
</div>
<div class="lyrico-lyrics-wrapper">Haye aihi bas karda ve guzarishan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haye aihi bas karda ve guzarishan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho jitni lakeerein hathon mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho jitni lakeerein hathon mein"/>
</div>
<div class="lyrico-lyrics-wrapper">Tere rangan vich main taan rangiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere rangan vich main taan rangiyan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh saaiyaan, oh saaiyaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh saaiyaan, oh saaiyaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Sun le tu meri arziyan haaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sun le tu meri arziyan haaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh saaiyaan, oh saaiyaan ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh saaiyaan, oh saaiyaan ho"/>
</div>
<div class="lyrico-lyrics-wrapper">Sun le tu meri arziyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sun le tu meri arziyan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kahe ankhiyan, kahe ankhiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kahe ankhiyan, kahe ankhiyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Tere khwabon sang yaariyan hai pakkiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere khwabon sang yaariyan hai pakkiyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ik tu hi mera saaiyaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ik tu hi mera saaiyaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Maine to umeedan tujh se hi rakhiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maine to umeedan tujh se hi rakhiyan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho saaiyaan, oh saaiyaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho saaiyaan, oh saaiyaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Sun le tu meri arziyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sun le tu meri arziyan"/>
</div>
</pre>
