---
title: "sanjaaram sanjaaram song lyrics"
album: "Anbirkiniyal"
artist: "Javed Riaz"
lyricist: "Lalithanand"
director: "Gokul"
path: "/albums/anbirkiniyal-lyrics"
song: "Sanjaaram Sanjaaram"
image: ../../images/albumart/anbirkiniyal.jpg
date: 2021-03-05
lang: tamil
youtubeLink: "https://www.youtube.com/embed/YfY9vcppwY4"
type: "Love"
singers:
  - Sid Sriram 
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Sanjaaram sanjaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sanjaaram sanjaaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Sanjaaram vaazhve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sanjaaram vaazhve"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanjaaram vanjaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanjaaram vanjaaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanjaaram poove
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanjaaram poove"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nerungi paarkuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nerungi paarkuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Nerungi paarkuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nerungi paarkuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayame idhayame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayame idhayame"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinam pazhagi pogalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam pazhagi pogalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhagi poguma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhagi poguma"/>
</div>
<div class="lyrico-lyrics-wrapper">Uravugal ethuvume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uravugal ethuvume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sanjaaram sanjaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sanjaaram sanjaaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Sanjaaram vaazhve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sanjaaram vaazhve"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanjaaram vanjaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanjaaram vanjaaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanjaaram poove
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanjaaram poove"/>
</div>
<div class="lyrico-lyrics-wrapper">Sanjaaram sanjaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sanjaaram sanjaaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Sanjaaram vaazhve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sanjaaram vaazhve"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanjaaram vanjaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanjaaram vanjaaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanjaaram poove
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanjaaram poove"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nerungi paarkuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nerungi paarkuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Nerungi paarkuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nerungi paarkuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayame idhayame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayame idhayame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa vaa valarnthu ponalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa vaa valarnthu ponalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee nee kuzhanthai pol vaazhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee nee kuzhanthai pol vaazhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Penne pesum kanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Penne pesum kanne"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee pesum pechai kettu rasikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee pesum pechai kettu rasikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar kaathum badhil rasikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar kaathum badhil rasikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Natpa nanba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natpa nanba"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee paarthidum paarvaiyal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee paarthidum paarvaiyal"/>
</div>
<div class="lyrico-lyrics-wrapper">Paalargal aagave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paalargal aagave"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarai ondru eeram kondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarai ondru eeram kondu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paasam kandu yengatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paasam kandu yengatho"/>
</div>
</pre>
