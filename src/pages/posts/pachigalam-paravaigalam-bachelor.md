---
title: "pachigalam paravaigalam song lyrics"
album: "Bachelor"
artist: "G.V. Prakash Kumar"
lyricist: "Navakkarai Naveen Prabanjam"
director: "Sathish Selvakumar"
path: "/albums/bachelor-lyrics"
song: "Pachigalam Paravaigalam"
image: ../../images/albumart/bachelor.jpg
date: 2021-12-03
lang: tamil
youtubeLink: "https://www.youtube.com/embed/H98LeGKjcag"
type: "mass"
singers:
  - Navakkarai Naveen Prabanjam
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Pachigalam ammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pachigalam ammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Paravaikkellaam thathinam they they
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paravaikkellaam thathinam they they"/>
</div>
<div class="lyrico-lyrics-wrapper">Paravaikalaam sollammaa sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paravaikalaam sollammaa sollu"/>
</div>
<div class="lyrico-lyrics-wrapper">Palavithamaay vandhu iralkuthammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palavithamaay vandhu iralkuthammaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ichai illaa rendu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ichai illaa rendu"/>
</div>
<div class="lyrico-lyrics-wrapper">Manippuraavum thathinam they they
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manippuraavum thathinam they they"/>
</div>
<div class="lyrico-lyrics-wrapper">Manippuraavum sollammaa sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manippuraavum sollammaa sollu"/>
</div>
<div class="lyrico-lyrics-wrapper">Imbamura kavaraa cheyyuthammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imbamura kavaraa cheyyuthammaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maadappura kanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maadappura kanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayilinangal thathinam they they
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayilinangal thathinam they they"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayilinangal sollammaa sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayilinangal sollammaa sollu"/>
</div>
</pre>
