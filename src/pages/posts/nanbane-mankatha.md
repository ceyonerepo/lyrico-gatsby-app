---
title: "nanbane song lyrics"
album: "Mankatha"
artist: "Yuvan Shankar Raja"
lyricist: "Vaali"
director: "Venkat Prabhu"
path: "/albums/mankatha-lyrics"
song: "Nanbane"
image: ../../images/albumart/mankatha.jpg
date: 2011-08-31
lang: tamil
youtubeLink: "https://www.youtube.com/embed/vlzqE-JbLcw"
type: "sad"
singers:
  - Madhushree
  - Yuvan Shankar Raja
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">En Nanbane Ennai Eithaai Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nanbane Ennai Eithaai Oh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Paavamaai Vandhu Vaaithaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Paavamaai Vandhu Vaaithaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Polave Nalla Nadigan Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Polave Nalla Nadigan Oh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oor Engilum Illai Oruvan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oor Engilum Illai Oruvan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nallavargal Yaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nallavargal Yaaro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theeyavargal Yaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeyavargal Yaaro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kandukondu Kanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandukondu Kanni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhal Seivadhillaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Seivadhillaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gangai Nadhiyellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gangai Nadhiyellam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaanal Nathi Endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanal Nathi Endru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pirpaadu Gnyanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirpaadu Gnyanam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vanthu Laabam Ennavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthu Laabam Ennavo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhal Enbathu Kanavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Enbathu Kanavu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maligai Purinthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maligai Purinthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kolladi En Thozhiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolladi En Thozhiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unmai Kaadhalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmai Kaadhalai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nan Thedi Paarkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nan Thedi Paarkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaanavillaiye En
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanavillaiye En"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thozhiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozhiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Valakaiyai Pidithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valakaiyai Pidithu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
 <div class="lyrico-lyrics-wrapper">Valakaiyil Vizhunthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valakaiyil Vizhunthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Valakaram Pidithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valakaram Pidithu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Valam Vara Ninaithen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valam Vara Ninaithen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uravennum Kavithai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uravennum Kavithai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyirinil Varainthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirinil Varainthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ezhuthiya Kavithai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhuthiya Kavithai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen Mudhal Bari Mudhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Mudhal Bari Mudhal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muzhuvadhum Pizhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muzhuvadhum Pizhai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhigalin Vazhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhigalin Vazhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhundathu Mazhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhundathu Mazhai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ellam Unnal Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellam Unnal Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhuva Undhan Gnyangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhuva Undhan Gnyangal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enaken Indha Kayangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enaken Indha Kayangal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kizhithaai Oru Kaadhal Oviyam Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kizhithaai Oru Kaadhal Oviyam Oh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Murugan Mugam Aaru Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Murugan Mugam Aaru Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manithan Mugam Nooru Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manithan Mugam Nooru Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ovvondrum Veru Veru Niramo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ovvondrum Veru Veru Niramo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Nanbane Ennai Eithaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nanbane Ennai Eithaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal Velluma Kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Velluma Kaadhal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thorkuma Yarum Arindadillaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thorkuma Yarum Arindadillaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Thozhiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Thozhiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal Oviyam Kizhinthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Oviyam Kizhinthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ponatha Kavalai Yenadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponatha Kavalai Yenadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhuvum Kadandhidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhuvum Kadandhidum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adikadi Enai Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adikadi Enai Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anaithathai Ariven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anaithathai Ariven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbenum Vilakkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbenum Vilakkai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anaithadhu Ariyen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anaithadhu Ariyen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Puyal Bandhu Saaitha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puyal Bandhu Saaitha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maram Oru Viragu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maram Oru Viragu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unakena Theriyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakena Theriyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Idhayathil Vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Idhayathil Vandhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhunthadhu Idi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhunthadhu Idi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ela Manam Engum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ela Manam Engum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irundhathu Vali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irundhathu Vali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yamma Yamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yamma Yamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ulagil Ulla Pengale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagil Ulla Pengale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uraipen Oru Ponmozhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uraipen Oru Ponmozhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal Oru Kanavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Oru Kanavu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maligai Oh Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maligai Oh Oh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edhuvum Angu Mayamthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhuvum Angu Mayamthaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ellam Varna Jaalam Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellam Varna Jaalam Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nambamal Vazhvadhendrum Nalame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nambamal Vazhvadhendrum Nalame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhal Enbathu Kanavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Enbathu Kanavu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maligai Purinthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maligai Purinthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kolladi En Thozhiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolladi En Thozhiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unmai Kaadhalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmai Kaadhalai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nan Thedi Paarkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nan Thedi Paarkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaanavillaiye En
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanavillaiye En"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thozhiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozhiye"/>
</div>
</pre>
