---
title: "6 feet tall song lyrics"
album: "Voter"
artist: "S. Thaman"
lyricist: "Ramajogaiah Sastry"
director: "G.S. Karthik"
path: "/albums/voter-lyrics"
song: "6 Feet Tall"
image: ../../images/albumart/voter.jpg
date: 2019-06-21
lang: telugu
youtubeLink: "https://www.youtube.com/embed/2bOmXNHKkDY"
type: "love"
singers:
  - Rahul Nambiar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hey Tell Me Tell Me How
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Tell Me Tell Me How"/>
</div>
<div class="lyrico-lyrics-wrapper">Oka Doubt-Ye Undhi Now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oka Doubt-Ye Undhi Now"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeha Yeda Peda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeha Yeda Peda"/>
</div>
<div class="lyrico-lyrics-wrapper">Inthandhanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inthandhanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Yetta Puttesav
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yetta Puttesav"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Kantlo Kochesav
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Kantlo Kochesav"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Kallo Tentesav
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Kallo Tentesav"/>
</div>
<div class="lyrico-lyrics-wrapper">Ne Chusi Chudani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne Chusi Chudani"/>
</div>
<div class="lyrico-lyrics-wrapper">Timey Chusi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Timey Chusi"/>
</div>
<div class="lyrico-lyrics-wrapper">Heart-Ye Kottesav
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heart-Ye Kottesav"/>
</div>
<div class="lyrico-lyrics-wrapper">Yendhuko Nachesav
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yendhuko Nachesav"/>
</div>
<div class="lyrico-lyrics-wrapper">Yekkadao Touch Chesav
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yekkadao Touch Chesav"/>
</div>
<div class="lyrico-lyrics-wrapper">Love You Baby Ante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love You Baby Ante"/>
</div>
<div class="lyrico-lyrics-wrapper">Naku No Cheppesav
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naku No Cheppesav"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappulo Kaalesav
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappulo Kaalesav"/>
</div>
<div class="lyrico-lyrics-wrapper">Nippulo Velesav
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nippulo Velesav"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannu Gani Miss Ayithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannu Gani Miss Ayithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Repu Nuvve Feel Avuthav
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Repu Nuvve Feel Avuthav"/>
</div>
<div class="lyrico-lyrics-wrapper">Iam Six Feetu Tall-Uu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iam Six Feetu Tall-Uu"/>
</div>
<div class="lyrico-lyrics-wrapper">Iam Falling On Your Kaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iam Falling On Your Kaalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Iam Waiting For Our Call-Uu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iam Waiting For Our Call-Uu"/>
</div>
<div class="lyrico-lyrics-wrapper">Just Be My Be My Aalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Just Be My Be My Aalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Iam Six Feetu Tall-Uu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iam Six Feetu Tall-Uu"/>
</div>
<div class="lyrico-lyrics-wrapper">Iam Falling On Your Kaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iam Falling On Your Kaalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Iam Waiting For Our Call-Uu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iam Waiting For Our Call-Uu"/>
</div>
<div class="lyrico-lyrics-wrapper">Just Be My Be My Aalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Just Be My Be My Aalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yendhulo Thakkuva Nenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yendhulo Thakkuva Nenu"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheyyake Too Much Scene-Uu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheyyake Too Much Scene-Uu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Modern Kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Modern Kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ramudlanti Pilladu Nenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ramudlanti Pilladu Nenu"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey Gentleman Ye Nenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey Gentleman Ye Nenu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sight Kotte Thuntari Kanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sight Kotte Thuntari Kanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Naughty
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Naughty"/>
</div>
<div class="lyrico-lyrics-wrapper">Fruity Navvuni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fruity Navvuni"/>
</div>
<div class="lyrico-lyrics-wrapper">Chusi Flat Ayipoyanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chusi Flat Ayipoyanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Coffee,Beedi Panu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Coffee,Beedi Panu"/>
</div>
<div class="lyrico-lyrics-wrapper">Bad Habitle Levantanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bad Habitle Levantanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Extra Sugar Complan Thaage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Extra Sugar Complan Thaage"/>
</div>
<div class="lyrico-lyrics-wrapper">Bujjodu Nenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bujjodu Nenu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvante Padichasthanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvante Padichasthanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeku Pakka Match Avuthanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeku Pakka Match Avuthanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Dollarlanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Dollarlanni"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhandalu Guchi Neeke Vesthanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhandalu Guchi Neeke Vesthanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Iam Six Feetu Tall-Uu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iam Six Feetu Tall-Uu"/>
</div>
<div class="lyrico-lyrics-wrapper">Iam Falling On Your Kaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iam Falling On Your Kaalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Iam Waiting For Our Call-Uu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iam Waiting For Our Call-Uu"/>
</div>
<div class="lyrico-lyrics-wrapper">Just Be My Be My Aalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Just Be My Be My Aalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Iam Six Feetu Tall-Uu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iam Six Feetu Tall-Uu"/>
</div>
<div class="lyrico-lyrics-wrapper">Iam Falling On Your Kaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iam Falling On Your Kaalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Iam Waiting For Our Call-Uu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iam Waiting For Our Call-Uu"/>
</div>
<div class="lyrico-lyrics-wrapper">Just Be My Be My Aalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Just Be My Be My Aalu"/>
</div>
</pre>
