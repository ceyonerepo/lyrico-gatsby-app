---
title: "yevaru chesina papam song lyrics"
album: "Satya Gang"
artist: "Prabhas"
lyricist: "Chandrabose"
director: "Nimmala Prabhas"
path: "/albums/satya-gang-lyrics"
song: "Yevaru Chesina Papam"
image: ../../images/albumart/satya-gang.jpg
date: 2018-04-06
lang: telugu
youtubeLink: "https://www.youtube.com/embed/l59TLSN7O0A"
type: "sad"
singers:
  -	Vijay Yesudas
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">evaru chesina papamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="evaru chesina papamo"/>
</div>
<div class="lyrico-lyrics-wrapper">ee gaaali deepalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ee gaaali deepalu"/>
</div>
<div class="lyrico-lyrics-wrapper">evaru chesina drohamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="evaru chesina drohamo"/>
</div>
<div class="lyrico-lyrics-wrapper">ee baala rupaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ee baala rupaalu"/>
</div>
<div class="lyrico-lyrics-wrapper">yengilaku vetalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yengilaku vetalo"/>
</div>
<div class="lyrico-lyrics-wrapper">ee rangu velasina puvula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ee rangu velasina puvula"/>
</div>
<div class="lyrico-lyrics-wrapper">chetta kuppala chatulo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chetta kuppala chatulo"/>
</div>
<div class="lyrico-lyrics-wrapper">nilu vethu gayalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nilu vethu gayalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">evaru chesina papamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="evaru chesina papamo"/>
</div>
<div class="lyrico-lyrics-wrapper">ee gaaali deepalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ee gaaali deepalu"/>
</div>
<div class="lyrico-lyrics-wrapper">evaru chesina drohamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="evaru chesina drohamo"/>
</div>
<div class="lyrico-lyrics-wrapper">ee baala rupaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ee baala rupaalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">amma teliyadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="amma teliyadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">naana teliyathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naana teliyathu"/>
</div>
<div class="lyrico-lyrics-wrapper">thelisinathu avamanamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thelisinathu avamanamo"/>
</div>
<div class="lyrico-lyrics-wrapper">gudu dhorakathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gudu dhorakathu"/>
</div>
<div class="lyrico-lyrics-wrapper">needa dhorakadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="needa dhorakadu"/>
</div>
<div class="lyrico-lyrics-wrapper">dorikinadhi kuradrustamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dorikinadhi kuradrustamu"/>
</div>
<div class="lyrico-lyrics-wrapper">nestam evarum leni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nestam evarum leni"/>
</div>
<div class="lyrico-lyrics-wrapper">velalo veethi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="velalo veethi "/>
</div>
<div class="lyrico-lyrics-wrapper">kukalu dosthulu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kukalu dosthulu "/>
</div>
<div class="lyrico-lyrics-wrapper">laali paatalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="laali paatalu"/>
</div>
<div class="lyrico-lyrics-wrapper">leni brathukulo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="leni brathukulo"/>
</div>
<div class="lyrico-lyrics-wrapper">doma padenu joolalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="doma padenu joolalu"/>
</div>
<div class="lyrico-lyrics-wrapper">yethi thagithe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yethi thagithe "/>
</div>
<div class="lyrico-lyrics-wrapper">kadupu nindeno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadupu nindeno"/>
</div>
<div class="lyrico-lyrics-wrapper">pampu neellu kannillu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pampu neellu kannillu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">evaru chesina papamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="evaru chesina papamo"/>
</div>
<div class="lyrico-lyrics-wrapper">ee gaaali deepalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ee gaaali deepalu"/>
</div>
<div class="lyrico-lyrics-wrapper">evaru chesina drohamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="evaru chesina drohamo"/>
</div>
<div class="lyrico-lyrics-wrapper">ee baala rupaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ee baala rupaalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">daari poruguna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="daari poruguna"/>
</div>
<div class="lyrico-lyrics-wrapper">bicham adigae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bicham adigae"/>
</div>
<div class="lyrico-lyrics-wrapper">dhana karnuni thammulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhana karnuni thammulu"/>
</div>
<div class="lyrico-lyrics-wrapper">inupa mukkalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inupa mukkalu"/>
</div>
<div class="lyrico-lyrics-wrapper">eri barathike
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eri barathike"/>
</div>
<div class="lyrico-lyrics-wrapper">swarna bharathe bourulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="swarna bharathe bourulu"/>
</div>
<div class="lyrico-lyrics-wrapper">goni sanchi mosi alasey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="goni sanchi mosi alasey"/>
</div>
<div class="lyrico-lyrics-wrapper">gandhi thatha manavalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gandhi thatha manavalu"/>
</div>
<div class="lyrico-lyrics-wrapper">chaya kappulu kadigi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chaya kappulu kadigi"/>
</div>
<div class="lyrico-lyrics-wrapper">karige chandra gupthuni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karige chandra gupthuni"/>
</div>
<div class="lyrico-lyrics-wrapper">guruthulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="guruthulu"/>
</div>
<div class="lyrico-lyrics-wrapper">mosam merugaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mosam merugaka"/>
</div>
<div class="lyrico-lyrics-wrapper">neram erugaka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neram erugaka "/>
</div>
<div class="lyrico-lyrics-wrapper">siluva mosae 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="siluva mosae "/>
</div>
<div class="lyrico-lyrics-wrapper">chinni yesulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chinni yesulu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">evaru chesina papamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="evaru chesina papamo"/>
</div>
<div class="lyrico-lyrics-wrapper">ee gaaali deepalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ee gaaali deepalu"/>
</div>
<div class="lyrico-lyrics-wrapper">evaru chesina drohamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="evaru chesina drohamo"/>
</div>
<div class="lyrico-lyrics-wrapper">ee baala rupaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ee baala rupaalu"/>
</div>
</pre>
