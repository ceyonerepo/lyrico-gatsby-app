---
title: "vanga machan vanga song lyrics"
album: "Vantha Rajava Than Varuven"
artist: "Hiphop Tamizha"
lyricist: "Thanjai N. Ramaiah Dass - Remixed :Hip Hop Tamizha"
director: "Sundar C"
path: "/albums/vantha-rajava-than-varuven-lyrics"
song: "Vanga Machan Vanga"
image: ../../images/albumart/vantha-rajava-than-varuven.jpg
date: 2019-02-01
lang: tamil
youtubeLink: "https://www.youtube.com/embed/gQojuXfzL9w"
type: "happy"
singers:
  - Kaushik Krish
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aiyaa Maana Thedi Vantharaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyaa Maana Thedi Vantharaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thambi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thambi"/>
</div>
<div class="lyrico-lyrics-wrapper">Akka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Akka"/>
</div>
<div class="lyrico-lyrics-wrapper">Thambi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thambi"/>
</div>
<div class="lyrico-lyrics-wrapper">Akka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Akka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vanga Machaan Vanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanga Machaan Vanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Vantha Vazhiya Paathu Ponga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vantha Vazhiya Paathu Ponga"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanga Machaan Vanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanga Machaan Vanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Vantha Vazhiya Paathu Ponga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vantha Vazhiya Paathu Ponga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yengi Yengi Neenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yengi Yengi Neenga"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Ippadi Paakkuringa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Ippadi Paakkuringa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yengi Yengi Neenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yengi Yengi Neenga"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Ippadi Paakkuringa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Ippadi Paakkuringa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vanga Machaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanga Machaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Summa Vanga Machaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summa Vanga Machaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kitta Vanga Machaan Vanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kitta Vanga Machaan Vanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Vantha Vazhiya Paathu Ponga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vantha Vazhiya Paathu Ponga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thambi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thambi"/>
</div>
<div class="lyrico-lyrics-wrapper">Akka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Akka"/>
</div>
<div class="lyrico-lyrics-wrapper">Thambi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thambi"/>
</div>
<div class="lyrico-lyrics-wrapper">Akka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Akka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thambi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thambi"/>
</div>
<div class="lyrico-lyrics-wrapper">Akka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Akka"/>
</div>
<div class="lyrico-lyrics-wrapper">Thambi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thambi"/>
</div>
<div class="lyrico-lyrics-wrapper">Akka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Akka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aththai Magane Aththaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aththai Magane Aththaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Gethu Thaan Soththunu Nikkurane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gethu Thaan Soththunu Nikkurane"/>
</div>
<div class="lyrico-lyrics-wrapper">Aththai Magane Aththaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aththai Magane Aththaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Gethu Thaan Soththunu Nikkurane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gethu Thaan Soththunu Nikkurane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vantha Rajavathaan Varuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vantha Rajavathaan Varuven"/>
</div>
<div class="lyrico-lyrics-wrapper">Vantha Raja Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vantha Raja Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Vantha Rajavathaan Varuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Vantha Rajavathaan Varuven"/>
</div>
<div class="lyrico-lyrics-wrapper">Vantha Raja Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vantha Raja Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vantha Rajavathaan Varuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vantha Rajavathaan Varuven"/>
</div>
<div class="lyrico-lyrics-wrapper">Vantha Raja Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vantha Raja Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Vantha Rajavathaan Varuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Vantha Rajavathaan Varuven"/>
</div>
<div class="lyrico-lyrics-wrapper">Vantha Raja Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vantha Raja Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ponga Machaan Ponga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponga Machaan Ponga"/>
</div>
<div class="lyrico-lyrics-wrapper">Summa Pothikittu Ponga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summa Pothikittu Ponga"/>
</div>
<div class="lyrico-lyrics-wrapper">Scene-u Geenuu Vuttaaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Scene-u Geenuu Vuttaaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Senjiduvom Naanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senjiduvom Naanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ponga Machaan Ponga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponga Machaan Ponga"/>
</div>
<div class="lyrico-lyrics-wrapper">Summa Pothikittu Ponga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summa Pothikittu Ponga"/>
</div>
<div class="lyrico-lyrics-wrapper">Scene-u Geenuu Vuttaaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Scene-u Geenuu Vuttaaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Senjiduvom Naanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senjiduvom Naanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manmadhandhaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manmadhandhaanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Aana Vallavandhaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aana Vallavandhaanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kettavandhaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kettavandhaanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Aana Nallavandhaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aana Nallavandhaanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manmadhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manmadhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Vallavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Vallavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kettavan Aana Nallavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kettavan Aana Nallavan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivan Manmadhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Manmadhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan Vallavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Vallavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan Kettavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Kettavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Aana Nallavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aana Nallavan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vanga Machaan Vanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanga Machaan Vanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Vantha Vazhiya Paathu Ponga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vantha Vazhiya Paathu Ponga"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanga Machaan Vanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanga Machaan Vanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Vantha Vazhiya Paathu Ponga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vantha Vazhiya Paathu Ponga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yengi Yengi Neenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yengi Yengi Neenga"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Ippadi Paakkuringa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Ippadi Paakkuringa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yengi Yengi Neenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yengi Yengi Neenga"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Ippadi Paakkuringa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Ippadi Paakkuringa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vantha Rajavathaan Varuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vantha Rajavathaan Varuven"/>
</div>
<div class="lyrico-lyrics-wrapper">Vantha Raja Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vantha Raja Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Vantha Rajavathaan Varuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Vantha Rajavathaan Varuven"/>
</div>
<div class="lyrico-lyrics-wrapper">Vantha Raja Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vantha Raja Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vantha Rajavathaan Varuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vantha Rajavathaan Varuven"/>
</div>
<div class="lyrico-lyrics-wrapper">Vantha Raja Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vantha Raja Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Vantha Rajavathaan Varuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Vantha Rajavathaan Varuven"/>
</div>
<div class="lyrico-lyrics-wrapper">Vantha Raja Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vantha Raja Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aththai Magane Aththaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aththai Magane Aththaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Gethu Thaan Soththunu Nikkurane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gethu Thaan Soththunu Nikkurane"/>
</div>
<div class="lyrico-lyrics-wrapper">Aththai Magane Aththaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aththai Magane Aththaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Gethu Thaan Soththunu Nikkurane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gethu Thaan Soththunu Nikkurane"/>
</div>
</pre>
