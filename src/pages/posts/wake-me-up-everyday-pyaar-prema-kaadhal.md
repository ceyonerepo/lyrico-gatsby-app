---
title: "wake me up everyday song lyrics"
album: "Pyaar Prema Kaadhal"
artist: "Yuvan Shankar Raja"
lyricist: "Elan"
director: "Elan"
path: "/albums/pyaar-prema-kaadhal-lyrics"
song: "Wake Me Up Everyday"
image: ../../images/albumart/pyaar-prema-kaadhal.jpg
date: 2018-08-10
lang: tamil
youtubeLink: "https://www.youtube.com/embed/JXuqrOzBxaE"
type: "love"
singers:
  - Teejay Arunachalam
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yeno minnalaai ennullae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeno minnalaai ennullae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthaval nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthaval nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan swasippen nesippenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan swasippen nesippenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Theera kaadhalai nenjilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theera kaadhalai nenjilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanthaval nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanthaval nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan vaazhgiren maaigirenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan vaazhgiren maaigirenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pala megangal soodiya vaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala megangal soodiya vaanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Athil vannamaai sernthavalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athil vannamaai sernthavalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pala raagangal koodiya gaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala raagangal koodiya gaanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Athil un isai serthavalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athil un isai serthavalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kutti kutti kathaigal pesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kutti kutti kathaigal pesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Chella chella vithigal meeri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chella chella vithigal meeri"/>
</div>
<div class="lyrico-lyrics-wrapper">Mella mella purithal kolvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mella mella purithal kolvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai naanum thaangi paarkka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai naanum thaangi paarkka"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai neeyum konji theerkka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai neeyum konji theerkka"/>
</div>
<div class="lyrico-lyrics-wrapper">Nammai naamae thooki velvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nammai naamae thooki velvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhvilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvilae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Puthu vaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthu vaasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Poovondru thanthathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovondru thanthathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaen thaen unnai thottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaen thaen unnai thottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thithithaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thithithaen"/>
</div>
<div class="lyrico-lyrics-wrapper">En kaadhal neethaan kanmani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kaadhal neethaan kanmani"/>
</div>
<div class="lyrico-lyrics-wrapper">Kol kol ennai neeyum konjam kolladi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kol kol ennai neeyum konjam kolladi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pala megangal soodiya vaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala megangal soodiya vaanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Athil vannamaai sernthavalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athil vannamaai sernthavalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pala raagangal koodiya gaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala raagangal koodiya gaanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Athil un isai serthavalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athil un isai serthavalae"/>
</div>
</pre>
