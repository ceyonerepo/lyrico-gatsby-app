---
title: "savaal oyaathey song lyrics"
album: "kanaa"
artist: "Dhibu Ninan Thomas"
lyricist: "Mohan Rajan - Rabbit Mac"
director: " Arunraja Kamaraj"
path: "/albums/kanaa-lyrics"
song: "Savaal Oyaathey"
image: ../../images/albumart/kanaa.jpg
date: 2018-12-21
lang: tamil
youtubeLink: "https://www.youtube.com/embed/4yiwEm_8tJY"
type: "Motivational"
singers:
  - Dhibu Ninan Thomas
  - Arunraja Kamaraj
  - Rabbit Mac
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Savaal Oyaadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Savaal Oyaadhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Look Up Fly Like a Plane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Look Up Fly Like a Plane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">We’re Bout to Make It Rain
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="We’re Bout to Make It Rain"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Savaal Theyaadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Savaal Theyaadhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bout Spark All the Flames
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bout Spark All the Flames"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">We are Back Again
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="We are Back Again"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sakka Podu Podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sakka Podu Podu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Achchangal Thevai illa Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achchangal Thevai illa Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vetri Nadai Podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetri Nadai Podu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uchchangal Dhooramilla Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uchchangal Dhooramilla Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Neeye Thedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Neeye Thedu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Indri Naalai llai Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Indri Naalai llai Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nambi Mutti Modhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nambi Mutti Modhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa Vaa Savaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Vaa Savaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Savaal Oyaadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Savaal Oyaadhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Savaal Theyaadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Savaal Theyaadhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">You Can Say Whatever You Want
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Can Say Whatever You Want"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">But We Don’t Fret
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="But We Don’t Fret"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">No Who You Calling a Looser Boy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="No Who You Calling a Looser Boy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">We Champ You Know
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="We Champ You Know"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">We Got a Dream
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="We Got a Dream"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Like Martin Luther King
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Like Martin Luther King"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Light Speed Yeah We Beam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Light Speed Yeah We Beam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">All We All We Do is Win
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="All We All We Do is Win"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">We About to Burn Up
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="We About to Burn Up"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Put Your Heads Down
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Put Your Heads Down"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">When You Come Around the King
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="When You Come Around the King"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">You Better Learn to Bow Down
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Better Learn to Bow Down"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">When We Get to the Ground Y All Better Run
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="When We Get to the Ground Y All Better Run"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Coming for the First Spot Yeah We Gonna Be Number 1
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Coming for the First Spot Yeah We Gonna Be Number 1"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Who Won Go Figure Look Who’s Got Bigger
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Who Won Go Figure Look Who’s Got Bigger"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa Vaa Savaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Vaa Savaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaraadha Kaayam Illaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraadha Kaayam Illaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vetri Veri Rendum Otrai Soldhaan Thikkaathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetri Veri Rendum Otrai Soldhaan Thikkaathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ezhuvathai Pola Veeram Illaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhuvathai Pola Veeram Illaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Pinne Nee Irundhaal Unnai Veezhtha Mudiyaathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Pinne Nee Irundhaal Unnai Veezhtha Mudiyaathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanavugal Kai Serume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavugal Kai Serume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maatraththai Maatri Kaattum Aatral Oorum Unnaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maatraththai Maatri Kaattum Aatral Oorum Unnaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Odu Dhinam Dhinam Vaazhvu Raname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odu Dhinam Dhinam Vaazhvu Raname"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aatralai Aakka Alikka Endrum Inge Mudiyaathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aatralai Aakka Alikka Endrum Inge Mudiyaathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Fire On Fire Making All of your Vibes Expire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fire On Fire Making All of your Vibes Expire"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Burnin Up All Losers and Haters
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Burnin Up All Losers and Haters"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Man in the Mirror Took Me Fly Higher
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Man in the Mirror Took Me Fly Higher"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Felt Like the Whole World on My Shoulder
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Felt Like the Whole World on My Shoulder"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Then You Point Your Trigger Finger
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Then You Point Your Trigger Finger"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">If This Your War Man I’m the Soldier
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="If This Your War Man I’m the Soldier"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Show Me My Flag I’m Flame Up
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Show Me My Flag I’m Flame Up"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalangaathey Evanum Ingey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalangaathey Evanum Ingey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avan Avan Azhavukku Puyal Thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan Avan Azhavukku Puyal Thaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pudhu Varalaaru Ezhutha Inge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu Varalaaru Ezhutha Inge"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa Vaa Needhaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Vaa Needhaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa Indha Nodithaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Indha Nodithaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Vaazhvai Maatrum Nodi Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Vaazhvai Maatrum Nodi Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha Valithaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Valithaanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Vegam Yetrum Vazhi Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Vegam Yetrum Vazhi Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Patri Oodagam Poatti Podum Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Patri Oodagam Poatti Podum Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vendru Kaattu Podhum Vaa Vaa Savaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vendru Kaattu Podhum Vaa Vaa Savaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Savaal Oyaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Savaal Oyaadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muttukattai Ellaam Muttu Tharum Munneru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muttukattai Ellaam Muttu Tharum Munneru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Savaal Theyaathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Savaal Theyaathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Look Up Fly Like a Plane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Look Up Fly Like a Plane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">We’re Bout to Make It Rain
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="We’re Bout to Make It Rain"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Savvaal Oyaadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Savvaal Oyaadhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Savaal Theyaathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Savaal Theyaathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kangal Sirpi Aanaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangal Sirpi Aanaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanneer Kuda Muththaagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneer Kuda Muththaagum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Savaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Savaal"/>
</div>
</pre>
