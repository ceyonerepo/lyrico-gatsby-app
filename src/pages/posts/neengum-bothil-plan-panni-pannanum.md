---
title: "neengum bothil song lyrics"
album: "Plan Panni Pannanum"
artist: "Yuvan Shankar Raja"
lyricist: "Niranjan Bharathi"
director: "Badri Venkatesh"
path: "/albums/plan-panni-pannanum-lyrics"
song: "Neengum Bothil"
image: ../../images/albumart/plan-panni-pannanum.jpg
date: 2021-12-30
lang: tamil
youtubeLink: "https://www.youtube.com/embed/M-RCvPppQr4"
type: "happy"
singers:
  - Yuvan Shankar Raja
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Neengum Podhil Yengum Nenjil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neengum Podhil Yengum Nenjil"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedho Saaralo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedho Saaralo"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedho Saaralo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedho Saaralo"/>
</div>
<div class="lyrico-lyrics-wrapper">Moongil Modhi Thendral Pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moongil Modhi Thendral Pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Podhu Paadalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhu Paadalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Podhu Paadalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhu Paadalo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhooram Kooda Baaram Yera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhooram Kooda Baaram Yera"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatril Mounamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatril Mounamo"/>
</div>
<div class="lyrico-lyrics-wrapper">Otrai Vaarthai Sollum Munbu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Otrai Vaarthai Sollum Munbu"/>
</div>
<div class="lyrico-lyrics-wrapper">Neram Mudiyumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neram Mudiyumo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nerungidum Ennam Varugaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nerungidum Ennam Varugaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu Vilagidum Vindhai Yenadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu Vilagidum Vindhai Yenadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaigalil Yaavum Tharugaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaigalil Yaavum Tharugaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhai Izhanthidum Nilaiyil Naanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai Izhanthidum Nilaiyil Naanadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Poovil Vaazhnthidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Poovil Vaazhnthidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaasanai Maraiyudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaasanai Maraiyudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">En Uyiril Oru Vali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Uyiril Oru Vali"/>
</div>
<div class="lyrico-lyrics-wrapper">Menmaiyaai Niraiyudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Menmaiyaai Niraiyudhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaanum Kanavugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanum Kanavugal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vegamaai Kalaiyudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vegamaai Kalaiyudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Sugamaana Payanangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sugamaana Payanangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Mutkalaai Kuthudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mutkalaai Kuthudhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aruginil Neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aruginil Neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Irukkaiyilum Nenjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irukkaiyilum Nenjam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanimaiyai Unarudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanimaiyai Unarudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Pirinthidum Endrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirinthidum Endrum"/>
</div>
<div class="lyrico-lyrics-wrapper">Therinthidum Uravin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Therinthidum Uravin"/>
</div>
<div class="lyrico-lyrics-wrapper">Arimugam Kodumaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arimugam Kodumaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pazhagiya Naatkal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pazhagiya Naatkal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thandha Inbamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandha Inbamo"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagiya Vaanavillai Polave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagiya Vaanavillai Polave"/>
</div>
<div class="lyrico-lyrics-wrapper">Neengidum Andha Neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neengidum Andha Neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhadhaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhadhaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Suvadugal Indri Neengidumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suvadugal Indri Neengidumo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poomazhai Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poomazhai Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan Gnyabagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan Gnyabagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Theemazhai Aaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theemazhai Aaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Kaaranam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Kaaranam"/>
</div>
<div class="lyrico-lyrics-wrapper">Inivarum Kaalam Mella Thengumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inivarum Kaalam Mella Thengumo"/>
</div>
<div class="lyrico-lyrics-wrapper">Anubavam Ondru Kadandhu Pogumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anubavam Ondru Kadandhu Pogumo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pogum Paadhai Pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogum Paadhai Pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aana Podhum Vendume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aana Podhum Vendume"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai Piriyum Ellai Thondrum Varai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai Piriyum Ellai Thondrum Varai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Paarvai Neelume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Paarvai Neelume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thirumbaamal Sendridum Podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirumbaamal Sendridum Podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Ninaivugal Thirumbume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Ninaivugal Thirumbume"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Illaadha Podhum Unnai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Illaadha Podhum Unnai"/>
</div>
<div class="lyrico-lyrics-wrapper">Endrum Virumbume Virumbume Virumbume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endrum Virumbume Virumbume Virumbume"/>
</div>
</pre>
