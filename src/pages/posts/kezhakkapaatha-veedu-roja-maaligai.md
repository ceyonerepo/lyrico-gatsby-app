---
title: "kezhakkapaatha veedu song lyrics"
album: "Roja Maaligai"
artist: "Leo"
lyricist: "Sundar"
director: "Goutham"
path: "/albums/roja-maaligai-lyrics"
song: "Kezhakkapaatha Veedu"
image: ../../images/albumart/roja-maaligai.jpg
date: 2018-07-06
lang: tamil
youtubeLink: "https://www.youtube.com/embed/78LIo5wF5yU"
type: "happy"
singers:
  - Leo
  - Hema
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">kezhakka patha veedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kezhakka patha veedu"/>
</div>
<div class="lyrico-lyrics-wrapper">nee saraka pottu aadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee saraka pottu aadu"/>
</div>
<div class="lyrico-lyrics-wrapper">kezhakka patha veedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kezhakka patha veedu"/>
</div>
<div class="lyrico-lyrics-wrapper">saraka pottu aadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saraka pottu aadu"/>
</div>
<div class="lyrico-lyrics-wrapper">unakum enakum vaasthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unakum enakum vaasthu"/>
</div>
<div class="lyrico-lyrics-wrapper">nee kanaka muducha thosthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee kanaka muducha thosthu"/>
</div>
<div class="lyrico-lyrics-wrapper">unakum enakum vaasthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unakum enakum vaasthu"/>
</div>
<div class="lyrico-lyrics-wrapper">nee kanaka muducha thosthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee kanaka muducha thosthu"/>
</div>
<div class="lyrico-lyrics-wrapper">kathava thorantha kaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathava thorantha kaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">nee kanna moodi yethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee kanna moodi yethu"/>
</div>
<div class="lyrico-lyrics-wrapper">vidiya vidiya pootu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidiya vidiya pootu"/>
</div>
<div class="lyrico-lyrics-wrapper">nee veedu vaangi sethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee veedu vaangi sethu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kezhakka patha veedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kezhakka patha veedu"/>
</div>
<div class="lyrico-lyrics-wrapper">nee saraka pottu aadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee saraka pottu aadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ethuku unaku doubtu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethuku unaku doubtu"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu thaa namaku right
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu thaa namaku right"/>
</div>
<div class="lyrico-lyrics-wrapper">right right right
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="right right right"/>
</div>
<div class="lyrico-lyrics-wrapper">innum ethana round
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="innum ethana round"/>
</div>
<div class="lyrico-lyrics-wrapper">paduthu kidaka ground
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paduthu kidaka ground"/>
</div>
<div class="lyrico-lyrics-wrapper">thanni potta friend
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanni potta friend"/>
</div>
<div class="lyrico-lyrics-wrapper">foreign saraku trend
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="foreign saraku trend"/>
</div>
<div class="lyrico-lyrics-wrapper">fulla iruku rendu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="fulla iruku rendu"/>
</div>
<div class="lyrico-lyrics-wrapper">pathu ninna mandu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathu ninna mandu"/>
</div>
<div class="lyrico-lyrics-wrapper">thanni potta friend
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanni potta friend"/>
</div>
<div class="lyrico-lyrics-wrapper">foreign saraku trend
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="foreign saraku trend"/>
</div>
<div class="lyrico-lyrics-wrapper">fulla iruku rendu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="fulla iruku rendu"/>
</div>
<div class="lyrico-lyrics-wrapper">pathu ninna mandu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathu ninna mandu"/>
</div>
<div class="lyrico-lyrics-wrapper">thuttu eduthu kaatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thuttu eduthu kaatu"/>
</div>
<div class="lyrico-lyrics-wrapper">kattu katta neetu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kattu katta neetu"/>
</div>
<div class="lyrico-lyrics-wrapper">renta maadi bangala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="renta maadi bangala"/>
</div>
<div class="lyrico-lyrics-wrapper">motta maadi single ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="motta maadi single ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kezhakka patha veedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kezhakka patha veedu"/>
</div>
<div class="lyrico-lyrics-wrapper">nee saraka pottu aadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee saraka pottu aadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">veetu kulla kootam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veetu kulla kootam"/>
</div>
<div class="lyrico-lyrics-wrapper">veliyil iruku thotam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veliyil iruku thotam"/>
</div>
<div class="lyrico-lyrics-wrapper">thotam thotam thotam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thotam thotam thotam"/>
</div>
<div class="lyrico-lyrics-wrapper">hey vedala pulla aatam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey vedala pulla aatam"/>
</div>
<div class="lyrico-lyrics-wrapper">vidama thorathum notam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidama thorathum notam"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu rose gardern house
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu rose gardern house"/>
</div>
<div class="lyrico-lyrics-wrapper">nan tasmac rouse
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan tasmac rouse"/>
</div>
<div class="lyrico-lyrics-wrapper">oru full glass ah kavuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru full glass ah kavuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">ipadi suitcase ah navuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ipadi suitcase ah navuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">hey rose gardern house
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey rose gardern house"/>
</div>
<div class="lyrico-lyrics-wrapper">nan tasmac rouse
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan tasmac rouse"/>
</div>
<div class="lyrico-lyrics-wrapper">oru full glass ah kavuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru full glass ah kavuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">ipadi suitcase ah navuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ipadi suitcase ah navuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu 20 20 hall nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu 20 20 hall nee"/>
</div>
<div class="lyrico-lyrics-wrapper">nee 90 90 aalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee 90 90 aalu"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu patta potta veedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu patta potta veedu"/>
</div>
<div class="lyrico-lyrics-wrapper">nee kita vanthu aadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee kita vanthu aadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vadakka patha veedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vadakka patha veedu"/>
</div>
<div class="lyrico-lyrics-wrapper">saraka pottu aadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saraka pottu aadu"/>
</div>
<div class="lyrico-lyrics-wrapper">unakum enakum vaasthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unakum enakum vaasthu"/>
</div>
<div class="lyrico-lyrics-wrapper">nee kanaka muducha thosthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee kanaka muducha thosthu"/>
</div>
<div class="lyrico-lyrics-wrapper">unakum enakum vaasthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unakum enakum vaasthu"/>
</div>
<div class="lyrico-lyrics-wrapper">nee kanaka muducha thosthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee kanaka muducha thosthu"/>
</div>
<div class="lyrico-lyrics-wrapper">kathava thorantha kaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathava thorantha kaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">nee kanna moodi yethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee kanna moodi yethu"/>
</div>
<div class="lyrico-lyrics-wrapper">vidiya vidiya pootu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidiya vidiya pootu"/>
</div>
<div class="lyrico-lyrics-wrapper">nee veedu vaangi sethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee veedu vaangi sethu"/>
</div>
</pre>
