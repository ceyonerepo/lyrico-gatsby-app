---
title: "ippadi or inbam song lyrics"
album: "Capmaari"
artist: "Siddharth Vipin"
lyricist: "Mohan Rajan"
director: "S.A. Chandrasekhar"
path: "/albums/capmaari-lyrics"
song: "Ippadi Or Inbam"
image: ../../images/albumart/capmaari.jpg
date: 2019-12-13
lang: tamil
youtubeLink: 
type: "love"
singers:
  - Neha Nair
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ippadi Oor Inbam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippadi Oor Inbam"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhuvarai Naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhuvarai Naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kandadhillai Engum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandadhillai Engum"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinam Nandri Sollum Nenjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam Nandri Sollum Nenjam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Moochum Moochum Konjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moochum Moochum Konjam"/>
</div>
<div class="lyrico-lyrics-wrapper">Mutti Kondu Konjum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mutti Kondu Konjum"/>
</div>
<div class="lyrico-lyrics-wrapper">Patri Kollum Manjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Patri Kollum Manjam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Kaattu Theeyai Minjum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Kaattu Theeyai Minjum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paarvai Ondrae Podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarvai Ondrae Podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paasam Konjom Podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paasam Konjom Podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Idhu Idhu Podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Idhu Idhu Podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Inimel Eppodhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Inimel Eppodhum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adadaa Un Thozhilae Saindhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adadaa Un Thozhilae Saindhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kai Viralai Pidithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kai Viralai Pidithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidigira Varai Naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidigira Varai Naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilaiyattil Vizhunthida Vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilaiyattil Vizhunthida Vendum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ippadi Oor Inbam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippadi Oor Inbam"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhuvarai Naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhuvarai Naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kandadhillai Engum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandadhillai Engum"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinam Nandri Sollum Nenjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam Nandri Sollum Nenjam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Moochum Moochum Konjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moochum Moochum Konjam"/>
</div>
<div class="lyrico-lyrics-wrapper">Mutti Kondu Konjum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mutti Kondu Konjum"/>
</div>
<div class="lyrico-lyrics-wrapper">Patri Kollum Manjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Patri Kollum Manjam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Kaattu Theeyai Minjum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Kaattu Theeyai Minjum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unmel Ada Unmel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmel Ada Unmel"/>
</div>
<div class="lyrico-lyrics-wrapper">En Uyirum Saayuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Uyirum Saayuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">En Pol En Uyirum Unnai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Pol En Uyirum Unnai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal Seiyuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Seiyuthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mudhal Murai Enai Killi Paarthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhal Murai Enai Killi Paarthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Peyarai Adhai Solli Paarthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Peyarai Adhai Solli Paarthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjam Verthaen Naan Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjam Verthaen Naan Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Sariyo Thavaro Solvaaiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sariyo Thavaro Solvaaiyo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha Vendaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Vendaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha Nila Vendam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Nila Vendam"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha Vali Vendaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Vali Vendaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vendaamaae Vendaamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vendaamaae Vendaamae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unmai Kaadhal Vendaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmai Kaadhal Vendaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Polambalgal Vendaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Polambalgal Vendaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thavippugal Vendam Vendaamaeae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thavippugal Vendam Vendaamaeae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vendaamaeaehaaaaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vendaamaeaehaaaaaaaaa"/>
</div>
</pre>
