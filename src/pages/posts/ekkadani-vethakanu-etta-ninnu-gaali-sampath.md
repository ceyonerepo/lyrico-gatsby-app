---
title: "ekkadani vethakanu song lyrics"
album: "Gaali Sampath"
artist: "Achu"
lyricist: "Ramajogayya Sastry"
director: "Anish Krishna"
path: "/albums/gaali-sampath-lyrics"
song: "Ekkadani Vethakanu Etta Ninnu"
image: ../../images/albumart/gaali-sampath.jpg
date: 2021-03-11
lang: telugu
youtubeLink: "https://www.youtube.com/embed/XpS4xyUXfZY"
type: "sad"
singers:
 - Kaala Bhairava
 - Ramajogayya Sastry
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hey unnapaatunu cheekatochi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey unnapaatunu cheekatochi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalla rathiri shunyam vachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla rathiri shunyam vachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kantipapaku Sudhi guchhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kantipapaku Sudhi guchhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanaraani thutlu podisene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanaraani thutlu podisene"/>
</div>
<div class="lyrico-lyrics-wrapper">Ekkadani vethakanu etta ninnu vethakanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ekkadani vethakanu etta ninnu vethakanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu leni masakalo etta nenenu bathakanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu leni masakalo etta nenenu bathakanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnalaga lenu nanna maaripoyanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnalaga lenu nanna maaripoyanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeti chemmalaga chempa jaari poyenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeti chemmalaga chempa jaari poyenu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu kanaleni onti dhaaraipoyanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu kanaleni onti dhaaraipoyanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yellipoye yellipoye allarantha yellipoye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yellipoye yellipoye allarantha yellipoye"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu leka navvu leka ooru motthama sallabadipoye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu leka navvu leka ooru motthama sallabadipoye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Buddhileni moddhunu nenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Buddhileni moddhunu nenu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mabbulona unnanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mabbulona unnanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Assalaina neelo ninnu sudalekapoyanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Assalaina neelo ninnu sudalekapoyanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalam yentha maayalamaari kallu teripinchindi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalam yentha maayalamaari kallu teripinchindi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannu terichi susey lope ninnu mayam chesindi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu terichi susey lope ninnu mayam chesindi"/>
</div>
<div class="lyrico-lyrics-wrapper">Oopinantha oopiri gaali suttupakkale undi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oopinantha oopiri gaali suttupakkale undi"/>
</div>
<div class="lyrico-lyrics-wrapper">Gayamaina gundeku matram oopirandhnantundi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gayamaina gundeku matram oopirandhnantundi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvuleni illu choodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvuleni illu choodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Boruborumantundi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boruborumantundi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu gaani thekapothe nannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu gaani thekapothe nannu"/>
</div>
<div class="lyrico-lyrics-wrapper">kudakuda ravoddhantundi Nanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kudakuda ravoddhantundi Nanna"/>
</div>
</pre>
