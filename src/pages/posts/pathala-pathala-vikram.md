---
title: "pathala pathala song lyrics"
album: "Vikram"
artist: "Anirudh Ravichander"
lyricist: "Kamal Haasan"
director: "Lokesh Kanagaraj"
path: "/albums/vikram-lyrics"
song: "Pathala Pathala"
image: ../../images/albumart/vikram.jpg
date: 2022-06-03
lang: tamil
youtubeLink: "https://www.youtube.com/embed/9VpeTiz81gc"
type: "happy"
singers:
  - Kamal Haasan
  - Anirudh Ravichander
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Bass
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bass"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pathala Pathala Kuttiyam Pathala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathala Pathala Kuttiyam Pathala"/>
</div>
<div class="lyrico-lyrics-wrapper">Puttiyam Pathala Mathalam Adra Dai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puttiyam Pathala Mathalam Adra Dai"/>
</div>
<div class="lyrico-lyrics-wrapper">Sutha Mathalam Adra Dai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sutha Mathalam Adra Dai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adra"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adha Soilini Irukara Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adha Soilini Irukara Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Andamrae Nee Ethi Padu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andamrae Nee Ethi Padu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dho Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dho Paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuthura Gumman Kuthula Gomma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuthura Gumman Kuthula Gomma"/>
</div>
<div class="lyrico-lyrics-wrapper">Petha Pulla Nee Sethuruva Da Dai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Petha Pulla Nee Sethuruva Da Dai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Udhar Udadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Udhar Udadhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mavane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mavane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey What Language Is This
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey What Language Is This"/>
</div>
<div class="lyrico-lyrics-wrapper">What You Talking About Man
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="What You Talking About Man"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivan Uttalakid John’u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Uttalakid John’u"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Mudichamiki Prem’u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Mudichamiki Prem’u"/>
</div>
<div class="lyrico-lyrics-wrapper">Onna Number Sokka Thirudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna Number Sokka Thirudi"/>
</div>
<div class="lyrico-lyrics-wrapper">Bladeu Pakkiri Mame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bladeu Pakkiri Mame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhi Sarakku Adikum Somu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhi Sarakku Adikum Somu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan Sundi Soru Seen’u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Sundi Soru Seen’u"/>
</div>
<div class="lyrico-lyrics-wrapper">Vella Powder Koadu Pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vella Powder Koadu Pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mookurinjum Team’u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mookurinjum Team’u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dai Patti Tinkering Seiyadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dai Patti Tinkering Seiyadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ketta Pombalaiya Nambi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketta Pombalaiya Nambi"/>
</div>
<div class="lyrico-lyrics-wrapper">Yemandhu Pudadhe Dharandhu Pudadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yemandhu Pudadhe Dharandhu Pudadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Idha Nakinu Kudra Dai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idha Nakinu Kudra Dai"/>
</div>
<div class="lyrico-lyrics-wrapper">Adha Thuttu Nu Thundai Dai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adha Thuttu Nu Thundai Dai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Ithini Kudichalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Ithini Kudichalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Inga Pattini Koodadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga Pattini Koodadhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bass
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bass"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gajjanaale Kaasille
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gajjanaale Kaasille"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallalaiyum Kaasille
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallalaiyum Kaasille"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaaichal Joram Neraiya Varudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaichal Joram Neraiya Varudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thillalangadi Thillale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thillalangadi Thillale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ondriyathin Thapaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondriyathin Thapaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Onniyum Illa Ippaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onniyum Illa Ippaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Saavi Ipo Thirudan Kaila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saavi Ipo Thirudan Kaila"/>
</div>
<div class="lyrico-lyrics-wrapper">Thillalangadi Thillale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thillalangadi Thillale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eri Kolam Nadhiya Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eri Kolam Nadhiya Kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Plotu Pottu Vithaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Plotu Pottu Vithaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Naari Poodum Ooru Janam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naari Poodum Ooru Janam"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinna Mazha Vandhaaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna Mazha Vandhaaka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oyyarama Taluka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oyyarama Taluka"/>
</div>
<div class="lyrico-lyrics-wrapper">Odhungi Pora Kannala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odhungi Pora Kannala"/>
</div>
<div class="lyrico-lyrics-wrapper">Erangi Vandhu Vela Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Erangi Vandhu Vela Paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadu Maarum Thannala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadu Maarum Thannala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kulla Nari Mamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulla Nari Mamu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kedupadh Ivan Game’u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kedupadh Ivan Game’u"/>
</div>
<div class="lyrico-lyrics-wrapper">Kulam Irundhum Valaithalathila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulam Irundhum Valaithalathila"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaadhi Pesum Meme’u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaadhi Pesum Meme’u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oosi Podu Mame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oosi Podu Mame"/>
</div>
<div class="lyrico-lyrics-wrapper">Veengikichu Bum-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veengikichu Bum-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Balla Balle Balla Balle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Balla Balle Balla Balle"/>
</div>
<div class="lyrico-lyrics-wrapper">Balla Balle Bomb’u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Balla Balle Bomb’u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dai Adha Uttu Ozhi’da Dai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dai Adha Uttu Ozhi’da Dai"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Kuthu Uddu Da Dai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Kuthu Uddu Da Dai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pataaevan Ethithalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pataaevan Ethithalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Getha Etti Midha Dai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Getha Etti Midha Dai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idha Nakinu Kudra Dai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idha Nakinu Kudra Dai"/>
</div>
<div class="lyrico-lyrics-wrapper">Adha Thottu Nu Thunda Dai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adha Thottu Nu Thunda Dai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Ethini Kudichalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Ethini Kudichalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Inga Pattini Koodadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga Pattini Koodadhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bass
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bass"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Va Ma Janaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Va Ma Janaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Koovu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koovu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pathala Pathala Kuttiyam Pathala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathala Pathala Kuttiyam Pathala"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuthura Gumman Kuthula Gomma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuthura Gumman Kuthula Gomma"/>
</div>
<div class="lyrico-lyrics-wrapper">Petha Pulla Nee Sethuruva Da Dai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Petha Pulla Nee Sethuruva Da Dai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Udhar Udadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Udhar Udadhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mavane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mavane"/>
</div>
</pre>
