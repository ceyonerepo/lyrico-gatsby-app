---
title: 'donu donu donu song lyrics'
album: 'Maari'
artist: 'Anirudh Ravichander'
lyricist: 'Dhanush'
director: 'Balaji Mohan'
path: '/albums/maari-song-lyrics'
song: 'Donu Donu Donu'
image: ../../images/albumart/Maari.jpg
date: 2015-07-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/A_z5g0_hJN8"
type: 'love'
singers: 
- Anirudh Ravichander
- Alisha Thomas
---

<pre class="lyrics-native">
    Coming Soon
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Donu donu donu
<input type="checkbox" class="lyrico-select-lyric-line" value="Donu donu donu"/>
</div>
  <div class="lyrico-lyrics-wrapper">Naa unnoda gold meenu
<input type="checkbox" class="lyrico-select-lyric-line" value="Naa unnoda gold meenu"/>
</div>
  <div class="lyrico-lyrics-wrapper">Sceneu sceneu sceneu
<input type="checkbox" class="lyrico-select-lyric-line" value="Sceneu sceneu sceneu"/>
</div>
  <div class="lyrico-lyrics-wrapper">Nee thotathellam sceneu
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee thotathellam sceneu"/>
</div>
  <div class="lyrico-lyrics-wrapper">Maanu maanu maanu
<input type="checkbox" class="lyrico-select-lyric-line" value="Maanu maanu maanu"/>
</div>
  <div class="lyrico-lyrics-wrapper">Naan unnoda caffineu
<input type="checkbox" class="lyrico-select-lyric-line" value="Naan unnoda caffineu"/>
</div>
  <div class="lyrico-lyrics-wrapper">Goneu goneu goneu
<input type="checkbox" class="lyrico-select-lyric-line" value="Goneu goneu goneu"/>
</div>
  <div class="lyrico-lyrics-wrapper">Nee paathale naan goneu
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee paathale naan goneu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Gundaana kanaala kuthama kuthatha
<input type="checkbox" class="lyrico-select-lyric-line" value="Gundaana kanaala kuthama kuthatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee unna thandhalum pathaathadi
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee unna thandhalum pathaathadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Roja poo dhegathaal raja naan saanjuten
<input type="checkbox" class="lyrico-select-lyric-line" value="Roja poo dhegathaal raja naan saanjuten"/>
</div>
<div class="lyrico-lyrics-wrapper">Un munne en gethu nikkadhadi
<input type="checkbox" class="lyrico-select-lyric-line" value="Un munne en gethu nikkadhadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Uruguthae unthan aanmai paarthu
<input type="checkbox" class="lyrico-select-lyric-line" value="Uruguthae unthan aanmai paarthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayanguthae indha paavai thaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Mayanguthae indha paavai thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thavikuthae yellai thaandi paarkka
<input type="checkbox" class="lyrico-select-lyric-line" value="Thavikuthae yellai thaandi paarkka"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadukuthae pen naanam thaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Thadukuthae pen naanam thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uruguthae.. mayanguthae..
<input type="checkbox" class="lyrico-select-lyric-line" value="Uruguthae.. mayanguthae.."/>
</div>
<div class="lyrico-lyrics-wrapper">Thavikuthae.. thadukuthae pen naanam thaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Thavikuthae.. thadukuthae pen naanam thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">I’m a loyal husband
<input type="checkbox" class="lyrico-select-lyric-line" value="I’m a loyal husband"/>
</div>
<div class="lyrico-lyrics-wrapper">Giving you royal treatment
<input type="checkbox" class="lyrico-select-lyric-line" value="Giving you royal treatment"/>
</div>
<div class="lyrico-lyrics-wrapper">And your daily shopping is guarenteed
<input type="checkbox" class="lyrico-select-lyric-line" value="And your daily shopping is guarenteed"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">You’re ma meesai maama
<input type="checkbox" class="lyrico-select-lyric-line" value="You’re ma meesai maama"/>
</div>
<div class="lyrico-lyrics-wrapper">I’m your naughty maami
<input type="checkbox" class="lyrico-select-lyric-line" value="I’m your naughty maami"/>
</div>
<div class="lyrico-lyrics-wrapper">Giving you a life time service warranty (2 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Giving you a life time service warranty"/></div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Hey vaa maa twinkleu.. come and mingleu
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey vaa maa twinkleu.. come and mingleu"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma kaatula naan thaan kingu
<input type="checkbox" class="lyrico-select-lyric-line" value="Namma kaatula naan thaan kingu"/>
</div>
  <div class="lyrico-lyrics-wrapper">Modern manmadha.. aasa koodudha
<input type="checkbox" class="lyrico-select-lyric-line" value="Modern manmadha.. aasa koodudha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanna kattudha.. give me ringu
<input type="checkbox" class="lyrico-select-lyric-line" value="Kanna kattudha.. give me ringu"/>
</div>
  <div class="lyrico-lyrics-wrapper">Naan sokkuren di.. vikkuren di
<input type="checkbox" class="lyrico-select-lyric-line" value="Naan sokkuren di.. vikkuren di"/>
</div>
<div class="lyrico-lyrics-wrapper">Othaiyila nikkurendi
<input type="checkbox" class="lyrico-select-lyric-line" value="Othaiyila nikkurendi"/>
</div>
  <div class="lyrico-lyrics-wrapper">Naan thithikavaa.. pathikavaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Naan thithikavaa.. pathikavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oththaikkotha aada ready
<input type="checkbox" class="lyrico-select-lyric-line" value="Oththaikkotha aada ready"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Uruguthae.. mayanguthae..
<input type="checkbox" class="lyrico-select-lyric-line" value="Uruguthae.. mayanguthae.."/>
</div>
<div class="lyrico-lyrics-wrapper">Ehh thavikuthae. ehh. thadukuthae pen naanam thaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Ehh thavikuthae. ehh. thadukuthae pen naanam thaan"/>
</div>
  <div class="lyrico-lyrics-wrapper">Uruguthae unthan aanmai paarthu
<input type="checkbox" class="lyrico-select-lyric-line" value="Uruguthae unthan aanmai paarthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayanguthae indha paavai thaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Mayanguthae indha paavai thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thavikuthae yellai thaandi paarkka
<input type="checkbox" class="lyrico-select-lyric-line" value="Thavikuthae yellai thaandi paarkka"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadukuthae pen naanam thaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Thadukuthae pen naanam thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Donu donu donu
<input type="checkbox" class="lyrico-select-lyric-line" value="Donu donu donu"/>
</div>
  <div class="lyrico-lyrics-wrapper">Naa unnoda gold meenu
<input type="checkbox" class="lyrico-select-lyric-line" value="Naa unnoda gold meenu"/>
</div>
  <div class="lyrico-lyrics-wrapper">Sceneu sceneu sceneu
<input type="checkbox" class="lyrico-select-lyric-line" value="Sceneu sceneu sceneu"/>
</div>
  <div class="lyrico-lyrics-wrapper">Nee thotathellam sceneu
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee thotathellam sceneu"/>
</div>
  <div class="lyrico-lyrics-wrapper">Maanu maanu maanu
<input type="checkbox" class="lyrico-select-lyric-line" value="Maanu maanu maanu"/>
</div>
  <div class="lyrico-lyrics-wrapper">Naan unnoda caffineu
<input type="checkbox" class="lyrico-select-lyric-line" value="Naan unnoda caffineu"/>
</div>
  <div class="lyrico-lyrics-wrapper">Goneu goneu goneu
<input type="checkbox" class="lyrico-select-lyric-line" value="Goneu goneu goneu"/>
</div>
  <div class="lyrico-lyrics-wrapper">Nee paathale naan goneu
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee paathale naan goneu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Gudnaana kanaala kuthama kuthatha
<input type="checkbox" class="lyrico-select-lyric-line" value="Gudnaana kanaala kuthama kuthatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee unna thandhalum pathaathadi
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee unna thandhalum pathaathadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Roja poo dhegathaal raja naan saanjuten
<input type="checkbox" class="lyrico-select-lyric-line" value="Roja poo dhegathaal raja naan saanjuten"/>
</div>
<div class="lyrico-lyrics-wrapper">Un munne en gethu nikkadhadi
<input type="checkbox" class="lyrico-select-lyric-line" value="Un munne en gethu nikkadhadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">I’m a loyal husband
<input type="checkbox" class="lyrico-select-lyric-line" value="I’m a loyal husband"/>
</div>
<div class="lyrico-lyrics-wrapper">Giving you royal treatment
<input type="checkbox" class="lyrico-select-lyric-line" value="Giving you royal treatment"/>
</div>
<div class="lyrico-lyrics-wrapper">And your daily shopping is guarenteed
<input type="checkbox" class="lyrico-select-lyric-line" value="And your daily shopping is guarenteed"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">You’re ma meesai maama
<input type="checkbox" class="lyrico-select-lyric-line" value="You’re ma meesai maama"/>
</div>
<div class="lyrico-lyrics-wrapper">I’m your naughty maami
<input type="checkbox" class="lyrico-select-lyric-line" value="I’m your naughty maami"/>
</div>
<div class="lyrico-lyrics-wrapper">Giving you a life time service warranty(2 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Giving you a life time service warranty"/></div>
</div>
</pre>