---
title: "mapila vandha song lyrics"
album: "Rajavamsam"
artist: "Sam C.S"
lyricist: "Sam C.S"
director: "K.V. Kathirvelu"
path: "/albums/rajavamsam-lyrics"
song: "Maane Unna"
image: ../../images/albumart/rajavamsam.jpg
date: 2021-11-26
lang: tamil
youtubeLink: "https://www.youtube.com/embed/s1i2dHsWFHs"
type: "happy"
singers:
  - Mukesh
  - Sam CS
  - Sindu Sampath
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Mapila vandhaan mapila vandhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mapila vandhaan mapila vandhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarattu vandiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarattu vandiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaali katti thaan koottipoga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaali katti thaan koottipoga"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha sarattu vandiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha sarattu vandiyila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada vekkatha paaru vekkatha paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada vekkatha paaru vekkatha paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Athana paakkayila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athana paakkayila"/>
</div>
<div class="lyrico-lyrics-wrapper">Moochi pechithan pochu pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moochi pechithan pochu pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava athaana paakkayila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava athaana paakkayila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mapila vandhaan mapila vandhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mapila vandhaan mapila vandhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarattu vandiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarattu vandiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaali katti thaan koottipoga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaali katti thaan koottipoga"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha sarattu vandiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha sarattu vandiyila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada vekkatha paaru vekkatha paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada vekkatha paaru vekkatha paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Athana paakkayila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athana paakkayila"/>
</div>
<div class="lyrico-lyrics-wrapper">Moochi pechithan pochu pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moochi pechithan pochu pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava athaana paakkayila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava athaana paakkayila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avan sirippazhagu sikka vachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan sirippazhagu sikka vachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaiyun thaan sokka vaikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaiyun thaan sokka vaikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mani kanakka pesa solli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mani kanakka pesa solli"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasai vanthu nacharikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai vanthu nacharikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada ithukaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada ithukaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakkam vanthu ninnaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkam vanthu ninnaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenju pullarikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenju pullarikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan uyir kuduppan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan uyir kuduppan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhum vaazha aasai koodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhum vaazha aasai koodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Oothedukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oothedukkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nachinu nachinu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nachinu nachinu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mechura gunathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mechura gunathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Devathai pol oru seethaiyamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Devathai pol oru seethaiyamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Maapila unakku yogam thaan irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maapila unakku yogam thaan irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Sernthutta unnoda baakiyammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sernthutta unnoda baakiyammaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nachinu nachinu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nachinu nachinu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mechura gunathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mechura gunathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Devathai pol oru seethaiyamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Devathai pol oru seethaiyamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Maapila unakku yogam thaan irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maapila unakku yogam thaan irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Sernthutta unnoda baakiyammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sernthutta unnoda baakiyammaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Siru vattama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru vattama"/>
</div>
<div class="lyrico-lyrics-wrapper">Sozhalum ulagam inithaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sozhalum ulagam inithaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava mattum thaan iruppa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava mattum thaan iruppa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulaga inithaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulaga inithaanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeh kanmani ivan thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh kanmani ivan thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini un oru paathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini un oru paathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Akkaraiya ivana paathukko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Akkaraiya ivana paathukko"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyira nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyira nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaai maman ingae vaarandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaai maman ingae vaarandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanga sangili kondu thaarandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanga sangili kondu thaarandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha vaazhkai ellaam kaavalaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha vaazhkai ellaam kaavalaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam pora kaappandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam pora kaappandi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaai maman ingae vaarandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaai maman ingae vaarandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanga sangili kondu thaarandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanga sangili kondu thaarandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha vaazhkai ellaam kaavalaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha vaazhkai ellaam kaavalaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam pora kaappandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam pora kaappandi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yei maaple maple
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei maaple maple"/>
</div>
<div class="lyrico-lyrics-wrapper">Maja maja maja maaple
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maja maja maja maaple"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nachinu nachinu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nachinu nachinu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mechura gunathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mechura gunathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Devathai pol oru seethaiyamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Devathai pol oru seethaiyamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Maapila unakku yogam thaan irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maapila unakku yogam thaan irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Sernthutta unnoda baakiyammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sernthutta unnoda baakiyammaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nachinu nachinu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nachinu nachinu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mechura gunathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mechura gunathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Devathai pol oru seethaiyamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Devathai pol oru seethaiyamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Maapila unakku yogam thaan irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maapila unakku yogam thaan irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Sernthutta unnoda baakiyammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sernthutta unnoda baakiyammaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Soodhuvadhu therinji nadanthuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soodhuvadhu therinji nadanthuko"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini thaan nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini thaan nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Mundhanaiyil mudinju vechuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mundhanaiyil mudinju vechuko"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini nee nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini nee nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan bathirama avala paathukirom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan bathirama avala paathukirom"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidumaa nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidumaa nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Sondhagalaam irukkom kavalaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sondhagalaam irukkom kavalaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidu nee nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidu nee nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andha vaazha pola thaaroda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha vaazha pola thaaroda"/>
</div>
<div class="lyrico-lyrics-wrapper">Un vaazhka maarum seeroda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un vaazhka maarum seeroda"/>
</div>
<div class="lyrico-lyrics-wrapper">Aazham viludhaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aazham viludhaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbu neezhum aayul seeraaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbu neezhum aayul seeraaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paasam vaikka aalundu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paasam vaikka aalundu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaazhattida thaayundu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaazhattida thaayundu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pugundha veeda ennadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pugundha veeda ennadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Porandha veedu thaan idhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porandha veedu thaan idhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yei maaple maaple
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei maaple maaple"/>
</div>
<div class="lyrico-lyrics-wrapper">Maja maja maja maaple
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maja maja maja maaple"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nachinu nachinu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nachinu nachinu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mechura gunathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mechura gunathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Devathai pol oru seethaiyamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Devathai pol oru seethaiyamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Maapila unakku yogam thaan irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maapila unakku yogam thaan irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Sernthutta unnoda baakiyammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sernthutta unnoda baakiyammaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nachinu nachinu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nachinu nachinu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mechura gunathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mechura gunathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Devathai pol oru seethaiyamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Devathai pol oru seethaiyamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Maapila unakku yogam thaan irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maapila unakku yogam thaan irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Sernthutta unnoda baakiyammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sernthutta unnoda baakiyammaa"/>
</div>
</pre>
