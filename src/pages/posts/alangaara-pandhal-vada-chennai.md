---
title: "alangaara pandhal song lyrics"
album: "Vada Chennai"
artist: "Santhosh Narayanan"
lyricist: "Dholak Jegan"
director: "Vetrimaaran"
path: "/albums/vada-chennai-lyrics"
song: "Alangaara Pandhal"
image: ../../images/albumart/vada-chennai.jpg
date: 2018-10-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/fOCCf3R9j5g"
type: "sad"
singers:
  - Dholak Jegan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Alangaara pandhalilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alangaara pandhalilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Alangaara pandhalilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alangaara pandhalilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagaaga padukka vaithom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaaga padukka vaithom"/>
</div>
<div class="lyrico-lyrics-wrapper">Engal nanban 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engal nanban "/>
</div>
<div class="lyrico-lyrics-wrapper">indru maraindha naal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indru maraindha naal"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbu nanban 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbu nanban "/>
</div>
<div class="lyrico-lyrics-wrapper">indru maanda naal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indru maanda naal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alangaara pandhalilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alangaara pandhalilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Alangaara pandhalilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alangaara pandhalilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagaaga padukka vaithom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaaga padukka vaithom"/>
</div>
<div class="lyrico-lyrics-wrapper">Engal nanban 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engal nanban "/>
</div>
<div class="lyrico-lyrics-wrapper">indru maraindha naal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indru maraindha naal"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbu nanban 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbu nanban "/>
</div>
<div class="lyrico-lyrics-wrapper">indru maanda naal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indru maanda naal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaaa aaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaa aaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooooooooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooooooooooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaaaaaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaaaaaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooo oooooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooo oooooooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chennai maanagarinilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chennai maanagarinilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaasi maettu oorinilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasi maettu oorinilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Seerodu vaazhndha engal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seerodu vaazhndha engal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanbar maraindhaarae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanbar maraindhaarae"/>
</div>
<div class="lyrico-lyrics-wrapper">Seerodu vaazhndha engal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seerodu vaazhndha engal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanbar maraindhaarae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanbar maraindhaarae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paasathudan pazhagivittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paasathudan pazhagivittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadhiyilae pirindhaarae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadhiyilae pirindhaarae"/>
</div>
<div class="lyrico-lyrics-wrapper">Paasathudan pazhagivittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paasathudan pazhagivittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadhiyile pirindhaarae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadhiyile pirindhaarae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhum chinna vayadhinilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhum chinna vayadhinilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhaamal maraindhaarae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhaamal maraindhaarae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhum chinna vayadhinilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhum chinna vayadhinilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhaamal maraindhaarae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhaamal maraindhaarae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alangaara pandhalilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alangaara pandhalilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Alangaara pandhalilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alangaara pandhalilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagaaga padukka vaithom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaaga padukka vaithom"/>
</div>
<div class="lyrico-lyrics-wrapper">Engal nanban indru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engal nanban indru "/>
</div>
<div class="lyrico-lyrics-wrapper">maraindha naal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maraindha naal"/>
</div>
<div class="lyrico-lyrics-wrapper">Aruyir nanban 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aruyir nanban "/>
</div>
<div class="lyrico-lyrics-wrapper">engalai marandha naal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engalai marandha naal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhthida unnai ingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhthida unnai ingu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaarthaigalum illai nanbaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaarthaigalum illai nanbaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhthida unnai ingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhthida unnai ingu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaarthaigalum illai nanbaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaarthaigalum illai nanbaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhndhadhu podhumendru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhndhadhu podhumendru"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadhiyil pirindhaayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadhiyil pirindhaayaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhndhadhu podhumendru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhndhadhu podhumendru"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadhiyil pirindhaayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadhiyil pirindhaayaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sondhangalai marandhuvittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sondhangalai marandhuvittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanimaiyilae uranga sendraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanimaiyilae uranga sendraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Sondhangalai marandhuvittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sondhangalai marandhuvittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanimaiyilae uranga sendraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanimaiyilae uranga sendraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Paasathudan pazhagivittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paasathudan pazhagivittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadhiyilae pirindhuvittaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadhiyilae pirindhuvittaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Paasathudan pazhagivittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paasathudan pazhagivittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadhiyilae pirindhuvittaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadhiyilae pirindhuvittaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alangaara pandhalilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alangaara pandhalilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Alangaara pandhalilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alangaara pandhalilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagaaga padukka vaithom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaaga padukka vaithom"/>
</div>
<div class="lyrico-lyrics-wrapper">Engal nanban 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engal nanban "/>
</div>
<div class="lyrico-lyrics-wrapper">indru maanda naal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indru maanda naal"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbu nanban 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbu nanban "/>
</div>
<div class="lyrico-lyrics-wrapper">indru maraindha naal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indru maraindha naal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaaa aaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaa aaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooooooooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooooooooooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaaaaaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaaaaaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooo oooooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooo oooooooo"/>
</div>
</pre>
