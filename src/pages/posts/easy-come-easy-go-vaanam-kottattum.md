---
title: "easy come easy go song lyrics"
album: "Vaanam Kottattum"
artist: "Sid Sriram"
lyricist: "Siva Ananth"
director: "Dhana"
path: "/albums/vaanam-kottattum-lyrics"
song: "Easy Come Easy Go"
image: ../../images/albumart/vaanam-kottattum.jpg
date: 2020-02-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/EA-Cx2wNrL8"
type: "Enjoyment"
singers:
  - Sid Sriram
  - Sanjeev T
  - MADM
  - Tapass Naresh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Sevvizhi Ven Kadal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sevvizhi Ven Kadal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Minminum Vaanavil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minminum Vaanavil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ponnoliyo Paalveli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponnoliyo Paalveli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ellaam Ullangaiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaam Ullangaiyile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indhiran Suriyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indhiran Suriyan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thullidum Thaaragai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thullidum Thaaragai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mannile Oh Poo Mazhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannile Oh Poo Mazhai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ellaam Mangai Nenjile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaam Mangai Nenjile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Kai Pidithu Naam Nadakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Kai Pidithu Naam Nadakka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oor Ulagam Othungiduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oor Ulagam Othungiduvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa Vaa Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Vaa Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veesi Varum Per Alaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veesi Varum Per Alaiyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pathungi Ninnu Thazhuvidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathungi Ninnu Thazhuvidum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa Vaa Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Vaa Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanavukku Vilaiyenna Sol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavukku Vilaiyenna Sol"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaattu Puli Manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattu Puli Manam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ettu Thisaiyilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettu Thisaiyilum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaththi Thaavi Pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaththi Thaavi Pogum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhooram Enna Kettu Sol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhooram Enna Kettu Sol"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poothidum Paadhai Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poothidum Paadhai Ellaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Budhi Sidhthi Mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Budhi Sidhthi Mattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Otti Kooda Vara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Otti Kooda Vara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Katti Pottu Vaikka Yarumilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katti Pottu Vaikka Yarumilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Etti Thalli Podu Kavalaigala Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Etti Thalli Podu Kavalaigala Oh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Easy Come Easy Go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Easy Come Easy Go"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Venum Ketukko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Venum Ketukko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Easy Come Easy Go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Easy Come Easy Go"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ishatam Patta Vechchikko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishatam Patta Vechchikko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Easy Come Easy Go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Easy Come Easy Go"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Venum Ketukko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Venum Ketukko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Easy Come Easy Go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Easy Come Easy Go"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ishtam Pola Paadikko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishtam Pola Paadikko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sevvizhi Ven Kadal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sevvizhi Ven Kadal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Minminum Vaanavil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minminum Vaanavil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ponnoliyo Paalveli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponnoliyo Paalveli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ellaam Ullangaiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaam Ullangaiyile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chinnathaai Aasaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinnathaai Aasaigal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thevaigal Oh Ennile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thevaigal Oh Ennile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unmaiyaai Naan Solgiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmaiyaai Naan Solgiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Ennai Kanden Vinnile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Ennai Kanden Vinnile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Odum Megamellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odum Megamellaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nam Per Solli Koovaatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam Per Solli Koovaatho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ini Endrum Eppodhum Porkkalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Endrum Eppodhum Porkkalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa Vaa Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Vaa Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Easy Come Easy Go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Easy Come Easy Go"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Venum Ketukko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Venum Ketukko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Easy Come Easy Go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Easy Come Easy Go"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ishtam Patta Vechchikko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishtam Patta Vechchikko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Easy Come Easy Go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Easy Come Easy Go"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Venum Ketukko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Venum Ketukko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Easy Come Easy Go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Easy Come Easy Go"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ishtam Patta Vechchikko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishtam Patta Vechchikko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Easy Come Easy Go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Easy Come Easy Go"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Venum Ketukko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Venum Ketukko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Easy Come Easy Go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Easy Come Easy Go"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ishtam Pola Paadikko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishtam Pola Paadikko"/>
</div>
</pre>
