---
title: "gun in kadhal song lyrics"
album: "Kolamaavu Kokila"
artist: "Anirudh Ravichander"
lyricist: "Vignesh Shivan"
director: "Nelson Dilipkumar"
path: "/albums/kolamaavu-kokila-lyrics"
song: "Gun In Kadhal"
image: ../../images/albumart/kolamaavu-kokila.jpg
date: 2018-08-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/b4HJh7EuxR4"
type: "happy"
singers:
  - Vijay Yesudas
  - Arunraja Kamaraj
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nenjae vendam nadungadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjae vendam nadungadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un azhagana kaigal kondu nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un azhagana kaigal kondu nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbodu ennai aluthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbodu ennai aluthadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannae vendam kalangadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannae vendam kalangadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan uyirodu unnai meetkavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan uyirodu unnai meetkavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un viralalae ennai meetadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un viralalae ennai meetadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thotaakalai thotaakalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thotaakalai thotaakalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathirundhen kaadhaludanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathirundhen kaadhaludanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thotaakalai thotaakalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thotaakalai thotaakalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Modhiduven mogathudanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Modhiduven mogathudanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai undhan kayil vaithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai undhan kayil vaithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kondaal podhum anbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondaal podhum anbae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan pothumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan pothumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada aangal pola illaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada aangal pola illaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnakagavae naan iruppen anbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnakagavae naan iruppen anbae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan pothumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan pothumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Noor aatkal balamullavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Noor aatkal balamullavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kaiyilae endrum iruppen anbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kaiyilae endrum iruppen anbae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan pothumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan pothumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada aangal pola illaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada aangal pola illaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnakagavae naan iruppen anbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnakagavae naan iruppen anbae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan pothumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan pothumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Noor aatkal balamullavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Noor aatkal balamullavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kaiyilae endrum iruppen anbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kaiyilae endrum iruppen anbae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thotaakalai thotaakalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thotaakalai thotaakalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathirundhen kaadhaludanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathirundhen kaadhaludanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thotaakalai thotaakalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thotaakalai thotaakalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Modhiduven mogathudanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Modhiduven mogathudanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai undhan kayil vaithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai undhan kayil vaithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kondaal podhum anbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondaal podhum anbae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kabiskabaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabiskabaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kabiskabaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabiskabaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kabiskabaa Coco
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabiskabaa Coco"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kabiskabaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabiskabaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kabiskabaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabiskabaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kabiskabaa Coco
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabiskabaa Coco"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tig gag ku da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tig gag ku da"/>
</div>
<div class="lyrico-lyrics-wrapper">Tig gag ku da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tig gag ku da"/>
</div>
<div class="lyrico-lyrics-wrapper">Tig gag ku da Coco
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tig gag ku da Coco"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tig gag ku da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tig gag ku da"/>
</div>
<div class="lyrico-lyrics-wrapper">Tig gag ku da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tig gag ku da"/>
</div>
<div class="lyrico-lyrics-wrapper">Tig gag ku da Coco 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tig gag ku da Coco "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Coco Coco Coco Coco
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Coco Coco Coco Coco"/>
</div>
</pre>
