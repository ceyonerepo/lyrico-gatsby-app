---
title: "rane radhe song lyrics"
album: "Sashi"
artist: "Arun Chiluveru"
lyricist: "Vengi"
director: "Srinivas Naidu Nadikatla"
path: "/albums/sashi-lyrics"
song: "Rane Radhe Viluvaina Jeevitham"
image: ../../images/albumart/sashi.jpg
date: 2021-03-19
lang: telugu
youtubeLink: "https://www.youtube.com/embed/1ONQUTnRhRg"
type: "love"
singers:
  - Chowrastha Band
  - Aditi Bhavaraju
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Rane radhe viluvaina jeevitham pothe raadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rane radhe viluvaina jeevitham pothe raadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Pone podhe hrudhayamlo vedhane ponandhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pone podhe hrudhayamlo vedhane ponandhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Rane radhe viluvaina jeevitham pothe raadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rane radhe viluvaina jeevitham pothe raadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Pone podhe hrudhayamlo vedhane ponandhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pone podhe hrudhayamlo vedhane ponandhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manasu cheppe badhalanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu cheppe badhalanni"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinna chinnavanta vadhiley vadhiley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna chinnavanta vadhiley vadhiley"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalisi vachhe anandhale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalisi vachhe anandhale"/>
</div>
<div class="lyrico-lyrics-wrapper">Haddhulenivanta adugey adugey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haddhulenivanta adugey adugey"/>
</div>
<div class="lyrico-lyrics-wrapper">Daggaravuthayi dhuram avuthayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daggaravuthayi dhuram avuthayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Okka kougilintha valachey valachey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okka kougilintha valachey valachey"/>
</div>
<div class="lyrico-lyrics-wrapper">Mullu untayi raallu untayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mullu untayi raallu untayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Rahadarulanni gelichey gelichey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rahadarulanni gelichey gelichey"/>
</div>
<div class="lyrico-lyrics-wrapper">Needhe ee istam kastam nastam edhemaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Needhe ee istam kastam nastam edhemaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Needhe adhrustam maate maarale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Needhe adhrustam maate maarale"/>
</div>
<div class="lyrico-lyrics-wrapper">Needhe ee lokam mottham anukuntene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Needhe ee lokam mottham anukuntene"/>
</div>
<div class="lyrico-lyrics-wrapper">Ika pai needhe needhele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ika pai needhe needhele"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pallavinche konte ala padi lesthe andham ho…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pallavinche konte ala padi lesthe andham ho…"/>
</div>
<div class="lyrico-lyrics-wrapper">Panchukunte navvunila manadhe anubandham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panchukunte navvunila manadhe anubandham"/>
</div>
<div class="lyrico-lyrics-wrapper">Pone podhe hrudhayamlo vedhane ponandhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pone podhe hrudhayamlo vedhane ponandhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Thullipade kurrathanam theeramekkadi chuddam ho…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thullipade kurrathanam theeramekkadi chuddam ho…"/>
</div>
<div class="lyrico-lyrics-wrapper">Thellavare thurupinta tholi velugavudham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thellavare thurupinta tholi velugavudham"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninna monnavanni gadichenu vadhiley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninna monnavanni gadichenu vadhiley"/>
</div>
<div class="lyrico-lyrics-wrapper">Paatha rojulanni gathamegaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paatha rojulanni gathamegaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu nenu antha sawartham vidichey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu nenu antha sawartham vidichey"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinni chethivandhe hithamegaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinni chethivandhe hithamegaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Swargamunnadhinka ekkado ledhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Swargamunnadhinka ekkado ledhu"/>
</div>
<div class="lyrico-lyrics-wrapper">swapnamai undhi swathahaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="swapnamai undhi swathahaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Saahasalu chese satthuva manaku sonthamega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saahasalu chese satthuva manaku sonthamega"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaare ledhani thudhi varaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaare ledhani thudhi varaku"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhari lene ledhani thadabadaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhari lene ledhani thadabadaku"/>
</div>
<div class="lyrico-lyrics-wrapper">Theere maaradhu ani anaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theere maaradhu ani anaku"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee theeram dhuram cheru varaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee theeram dhuram cheru varaku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rane radhe viluvaina jeevitham pothe raadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rane radhe viluvaina jeevitham pothe raadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Pone podhe hrudhayamlo vedhane ponandhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pone podhe hrudhayamlo vedhane ponandhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manasu cheppe badhalanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu cheppe badhalanni"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinna chinnavanta vadhiley vadhiley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna chinnavanta vadhiley vadhiley"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalisi vachhe anandhale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalisi vachhe anandhale"/>
</div>
<div class="lyrico-lyrics-wrapper">Haddhulenivanta adugey adugey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haddhulenivanta adugey adugey"/>
</div>
<div class="lyrico-lyrics-wrapper">Daggaravuthayi dhuram avuthayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daggaravuthayi dhuram avuthayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Okka kougilintha valachey valachey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okka kougilintha valachey valachey"/>
</div>
<div class="lyrico-lyrics-wrapper">Mullu untayi raallu untayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mullu untayi raallu untayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Rahadarulanni gelichey gelichey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rahadarulanni gelichey gelichey"/>
</div>
<div class="lyrico-lyrics-wrapper">Needhe ee istam kastam nastam edhemaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Needhe ee istam kastam nastam edhemaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Needhe adhrustam maate maarale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Needhe adhrustam maate maarale"/>
</div>
<div class="lyrico-lyrics-wrapper">Needhe ee lokam mottham anukuntene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Needhe ee lokam mottham anukuntene"/>
</div>
<div class="lyrico-lyrics-wrapper">Ika pai needhe needhele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ika pai needhe needhele"/>
</div>
</pre>
