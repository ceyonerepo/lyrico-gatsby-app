---
title: "naattu koothu song lyrics"
album: "RRR Tamil"
artist: "Maragathamani"
lyricist: "Madhan Karky"
director: "S.S. Rajamouli "
path: "/albums/rrr-tamil-lyrics"
song: "Naattu Koothu"
image: ../../images/albumart/rrr-tamil.jpg
date: 2022-03-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/1ADj7ziy18M"
type: "happy"
singers:
  - Rahul Sipligunj
  - Yazin Nizar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Karuntholu kumbalodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuntholu kumbalodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattikkaattu kootha kaattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattikkaattu kootha kaattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Podu namma thaalam onnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podu namma thaalam onnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pottu naattu kootha kaattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pottu naattu kootha kaattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chilambaattam suthi kaattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chilambaattam suthi kaattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatha renda vetti kaattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatha renda vetti kaattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Jallikkattu kaaliyaattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jallikkattu kaaliyaattam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kooru kombil kuthi kaattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooru kombil kuthi kaattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalu kaalu naalu tholum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalu kaalu naalu tholum"/>
</div>
<div class="lyrico-lyrics-wrapper">Miratti dhoola kelappi kattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Miratti dhoola kelappi kattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En paattunkoothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En paattunkoothu"/>
</div>
<div class="lyrico-lyrics-wrapper">En paattunkoothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En paattunkoothu"/>
</div>
<div class="lyrico-lyrics-wrapper">En paattunkoothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En paattunkoothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naattu naattu naattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naattu naattu naattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naattu naattu naattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naattu naattu naattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kootha kaattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kootha kaattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naattu naattu naattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naattu naattu naattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naattu naattu vetta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naattu naattu vetta"/>
</div>
<div class="lyrico-lyrics-wrapper">Kootha kaattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kootha kaattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naattu naattu naattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naattu naattu naattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paattu padichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paattu padichu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappadichu kaattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappadichu kaattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naattu naattu naattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naattu naattu naattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vettrikodiya naatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vettrikodiya naatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Veeran kaattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veeran kaattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rendu idhayam onnaakki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendu idhayam onnaakki"/>
</div>
<div class="lyrico-lyrics-wrapper">Dandanakkannu molam kottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dandanakkannu molam kottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kiliyum kuyilum paattu katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kiliyum kuyilum paattu katti"/>
</div>
<div class="lyrico-lyrics-wrapper">Keechikkittu koovikittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Keechikkittu koovikittu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kayyi sodakkum thaalathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kayyi sodakkum thaalathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Sevvaanam saaichu kattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sevvaanam saaichu kattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalu thattum thaalathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalu thattum thaalathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilamellaam athiravittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilamellaam athiravittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chottu chottu vervai kottum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chottu chottu vervai kottum"/>
</div>
<div class="lyrico-lyrics-wrapper">Chathanthaan kaithattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chathanthaan kaithattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En paattunkoothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En paattunkoothu"/>
</div>
<div class="lyrico-lyrics-wrapper">En paattunkoothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En paattunkoothu"/>
</div>
<div class="lyrico-lyrics-wrapper">En paattunkoothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En paattunkoothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naattu naattu naattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naattu naattu naattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naattu naattu naattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naattu naattu naattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kootha kaattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kootha kaattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naattu naattu naattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naattu naattu naattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naattu naattu vetta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naattu naattu vetta"/>
</div>
<div class="lyrico-lyrics-wrapper">Kootha kaattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kootha kaattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naattu naattu naattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naattu naattu naattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallu podhai aattam aadi kaattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallu podhai aattam aadi kaattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naattu naattu naattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naattu naattu naattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kotta mela vettrikodi naattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kotta mela vettrikodi naattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bhoomi aadi nadunga thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhoomi aadi nadunga thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vegam yethi adiya maathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vegam yethi adiya maathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pinna vechu munna vechu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pinna vechu munna vechu"/>
</div>
<div class="lyrico-lyrics-wrapper">Egirithaan yekkaa yekkaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Egirithaan yekkaa yekkaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naattu kootha kaattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naattu kootha kaattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhum dhum thudippellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhum dhum thudippellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Veliya vittu ulla vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veliya vittu ulla vittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhammu dhammu kattikkittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhammu dhammu kattikkittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thullithaan yekkaa yekkaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thullithaan yekkaa yekkaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naattu koothu kaattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naattu koothu kaattu"/>
</div>
</pre>
