---
title: "roja poonthottam song lyrics"
album: "Kannukkul Nilavu"
artist: "Ilaiyaraaja"
lyricist: "Palani Bharathi"
director: "Fazil"
path: "/albums/kannukkul-nilavu-lyrics"
song: "Roja Poonthottam"
image: ../../images/albumart/kannukkul-nilavu.jpg
date: 2000-01-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/VzCtcOGv7_0"
type: "love"
singers:
  - P. Unni Krishnan
  - Anuradha Sriram
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Roja Poonthottam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Roja Poonthottam "/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal Vaasam Kadhal Vaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Vaasam Kadhal Vaasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Roja Poonthottam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Roja Poonthottam "/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal Vaasam Kadhal Vaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Vaasam Kadhal Vaasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Poovin Idhazh Ellam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovin Idhazh Ellam "/>
</div>
<div class="lyrico-lyrics-wrapper">Mouna Raagam Mouna Raagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mouna Raagam Mouna Raagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ovvoru Ilaiyilum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ovvoru Ilaiyilum "/>
</div>
<div class="lyrico-lyrics-wrapper">Then Thuli Aadudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Then Thuli Aadudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Poovellam Poovellam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovellam Poovellam "/>
</div>
<div class="lyrico-lyrics-wrapper">Pani Mazhai Thedudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pani Mazhai Thedudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Nam Kadhal Kadhaiyai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam Kadhal Kadhaiyai "/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam Sol Sol Sol Endradhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Sol Sol Sol Endradhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Roja Poonthottam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Roja Poonthottam "/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal Vaasam Kadhal Vaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Vaasam Kadhal Vaasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Poovin Idhazh Ellam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovin Idhazh Ellam "/>
</div>
<div class="lyrico-lyrics-wrapper">Mouna Raagam Mouna Raagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mouna Raagam Mouna Raagam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhiyasaivil Un Idhazh Asaivil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhiyasaivil Un Idhazh Asaivil"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayathile Indru Oru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayathile Indru Oru "/>
</div>
<div class="lyrico-lyrics-wrapper">Isai Thattu Suzhaluthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Isai Thattu Suzhaluthadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pudhiya Isai Oru Pudhiya Dhisai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhiya Isai Oru Pudhiya Dhisai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu Idhayam Indru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu Idhayam Indru "/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kadhalil Kidaithadhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kadhalil Kidaithadhadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhalai Naan Thandhen 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhalai Naan Thandhen "/>
</div>
<div class="lyrico-lyrics-wrapper">Vetkathai Nee Thandhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetkathai Nee Thandhaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhalai Naan Thandhen 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhalai Naan Thandhen "/>
</div>
<div class="lyrico-lyrics-wrapper">Vetkathai Nee Thandhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetkathai Nee Thandhaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Nerunginaal Nerunginaal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Nerunginaal Nerunginaal "/>
</div>
<div class="lyrico-lyrics-wrapper">Ilamai Sudugiradhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilamai Sudugiradhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Roja Poonthottam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Roja Poonthottam "/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal Vaasam Kadhal Vaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Vaasam Kadhal Vaasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Poovin Idhazh Ellam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovin Idhazh Ellam "/>
</div>
<div class="lyrico-lyrics-wrapper">Mouna Raagam Mouna Raagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mouna Raagam Mouna Raagam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Ninaithu Naan Vizhithirundhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Ninaithu Naan Vizhithirundhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Iravugalil Dhinam Vanna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravugalil Dhinam Vanna "/>
</div>
<div class="lyrico-lyrics-wrapper">Nilavuku Thunai Irundhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilavuku Thunai Irundhen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nilavadikum Konjam Veyil Adikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilavadikum Konjam Veyil Adikum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paruva Nilai Adhil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paruva Nilai Adhil "/>
</div>
<div class="lyrico-lyrics-wrapper">En Malarudal Silirthirundhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Malarudal Silirthirundhen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sooriyan Oru Kannil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sooriyan Oru Kannil "/>
</div>
<div class="lyrico-lyrics-wrapper">Vennila Maru Kannil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vennila Maru Kannil"/>
</div>
<div class="lyrico-lyrics-wrapper">Sooriyan Oru Kannil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sooriyan Oru Kannil "/>
</div>
<div class="lyrico-lyrics-wrapper">Vennila Maru Kannil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vennila Maru Kannil"/>
</div>
<div class="lyrico-lyrics-wrapper">En Iravaiyum Pagalaiyum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Iravaiyum Pagalaiyum "/>
</div>
<div class="lyrico-lyrics-wrapper">Unadhu Vizhiyil Kanden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unadhu Vizhiyil Kanden"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Roja Poonthottam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Roja Poonthottam "/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal Vaasam Kadhal Vaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Vaasam Kadhal Vaasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Poovin Idhazh Ellam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovin Idhazh Ellam "/>
</div>
<div class="lyrico-lyrics-wrapper">Mouna Raagam Mouna Raagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mouna Raagam Mouna Raagam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ovvoru Ilaiyilum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ovvoru Ilaiyilum "/>
</div>
<div class="lyrico-lyrics-wrapper">Then Thuli Aadudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Then Thuli Aadudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Poovellam Poovellam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovellam Poovellam "/>
</div>
<div class="lyrico-lyrics-wrapper">Pani Mazhai Thedudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pani Mazhai Thedudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Nam Kadhal Kadhaiyai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam Kadhal Kadhaiyai "/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam Sol Sol Sol Endradhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Sol Sol Sol Endradhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Roja Poonthottam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Roja Poonthottam "/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal Vaasam Kadhal Vaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Vaasam Kadhal Vaasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Poovin Idhazh Ellam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovin Idhazh Ellam "/>
</div>
<div class="lyrico-lyrics-wrapper">Mouna Raagam Mouna Raagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mouna Raagam Mouna Raagam"/>
</div>
</pre>
