---
title: "azhaikkatuma song lyrics"
album: "Kanne Kalaimane"
artist: "Yuvan Shankar Raja"
lyricist: "Vairamuthu"
director: "Seenu Ramasamy"
path: "/albums/kanne-kalaimane-lyrics"
song: "Azhaikkatuma"
image: ../../images/albumart/kanne-kalaimane.jpg
date: 2019-02-22
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ulWiD4cxjOY"
type: "happy"
singers:
  - Mathichiyam Bala
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Pachai Vaya Kaattukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pachai Vaya Kaattukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Punga Maram Poothurukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Punga Maram Poothurukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Naal Vazhi Salaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naal Vazhi Salaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Puli Maram Kaluma Poothurukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puli Maram Kaluma Poothurukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Etthu Thikkum Engum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Etthu Thikkum Engum"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga Veppa Maramum Poothurukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Veppa Maramum Poothurukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Vare Pogum Neram Paathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vare Pogum Neram Paathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pootha Poovellam Kan Mulaichu Kaathurukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pootha Poovellam Kan Mulaichu Kaathurukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aathula Vanthavale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aathula Vanthavale"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaigai Aathula Vanthavale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaigai Aathula Vanthavale"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga Solavanthaan Mariyamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Solavanthaan Mariyamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Anbodu Azhaikkatuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Anbodu Azhaikkatuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbodu Azhaikkatuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbodu Azhaikkatuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhaikkatuma Thaaiye Azhaikkatuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhaikkatuma Thaaiye Azhaikkatuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha Arugam Pullu Maalaikaari Azhaikkatuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Arugam Pullu Maalaikaari Azhaikkatuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhaikkatuma Thaaiye Azhaikkatuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhaikkatuma Thaaiye Azhaikkatuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma Veyilu Vantha Mariyamma Azhaikkatuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma Veyilu Vantha Mariyamma Azhaikkatuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Odi Vaa Karuppa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odi Vaa Karuppa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vanni Maram Pilanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanni Maram Pilanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">En Aathavukku Vanni Onnu Maram Pilanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Aathavukku Vanni Onnu Maram Pilanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">En Thaaiye Kondu Sella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Thaaiye Kondu Sella"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaagana Petti Senjan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaagana Petti Senjan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anthavana Petti Kulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anthavana Petti Kulla"/>
</div>
<div class="lyrico-lyrics-wrapper">En Anjana Maikaari Aei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Anjana Maikaari Aei"/>
</div>
<div class="lyrico-lyrics-wrapper">Anga Adangatha Kovakkari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anga Adangatha Kovakkari"/>
</div>
<div class="lyrico-lyrics-wrapper">Engal Solavanthan Mariyammane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engal Solavanthan Mariyammane"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppadi Adachan Theriyuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppadi Adachan Theriyuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anga Manjanama Poosakkari Azhaikkatuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anga Manjanama Poosakkari Azhaikkatuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha Marikozhundhu Vaadakaari Azhaikkatuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Marikozhundhu Vaadakaari Azhaikkatuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Anga Seppu Nalla Kalaiyalagi Azhaikkatuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anga Seppu Nalla Kalaiyalagi Azhaikkatuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha Singara Udaiyazhagi Azhaikkatuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Singara Udaiyazhagi Azhaikkatuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Odi Vaa Karuppa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odi Vaa Karuppa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naattula Panjam Pasinu Yethum Illame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naattula Panjam Pasinu Yethum Illame"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattiniya Kedanthurukkum Velaiyula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattiniya Kedanthurukkum Velaiyula"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha Makkal Yellam Koodi Nikkum Bothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Makkal Yellam Koodi Nikkum Bothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Seivom Endru Solli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Seivom Endru Solli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Antha Yosithu Paarkum Bothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Yosithu Paarkum Bothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha Anantha Malai Maayakannan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Anantha Malai Maayakannan"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha Varnabagavana Azhaichi Dey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Varnabagavana Azhaichi Dey"/>
</div>
<div class="lyrico-lyrics-wrapper">Adichi Peida Malaiyinnu Soldran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adichi Peida Malaiyinnu Soldran"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ango Adichathu Or Puyal Malaiyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ango Adichathu Or Puyal Malaiyam"/>
</div>
<div class="lyrico-lyrics-wrapper">En Aatha Petti Mel Kilamba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Aatha Petti Mel Kilamba"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha Paalathu Thanniyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Paalathu Thanniyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha Theanathu Thanniyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Theanathu Thanniyila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Namma Solavanthan Aathukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma Solavanthan Aathukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Pettiyume Othunguthama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Pettiyume Othunguthama"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Ithu Petti Endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Ithu Petti Endru"/>
</div>
<div class="lyrico-lyrics-wrapper">Yerittu Paarkum Bothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yerittu Paarkum Bothu"/>
</div>
<div class="lyrico-lyrics-wrapper">En Chella Maga Mariyamma Pettiyamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Chella Maga Mariyamma Pettiyamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pettiyila Vanthala Azhaikkathuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pettiyila Vanthala Azhaikkathuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Anga Perum Pugazh Petravale Azhaikkatuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anga Perum Pugazh Petravale Azhaikkatuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Pettiyila Vanthala Azhaikkathuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pettiyila Vanthala Azhaikkathuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Anga Perum Pugazh Petravale Azhaikkatuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anga Perum Pugazh Petravale Azhaikkatuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enga Solavanthan Mariyamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Solavanthan Mariyamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Adra Dei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adra Dei"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enga Solavanthan Mariyamma Azhaikkatuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Solavanthan Mariyamma Azhaikkatuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga Sornamuthu Mariyammana Azhaikkatuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Sornamuthu Mariyammana Azhaikkatuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga Sornamuthu Mariyammana Azhaikkatuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Sornamuthu Mariyammana Azhaikkatuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Petti Vanthuruchapa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Petti Vanthuruchapa"/>
</div>
<div class="lyrico-lyrics-wrapper">En Makkal Ellam Koodi Ninnuruchapa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Makkal Ellam Koodi Ninnuruchapa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Ithu Petti Endru Solli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Ithu Petti Endru Solli"/>
</div>
<div class="lyrico-lyrics-wrapper">Parke Bayama Irukku Solli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parke Bayama Irukku Solli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yosichi Parkum Bothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yosichi Parkum Bothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Haha Pettila Thangam Irukkuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haha Pettila Thangam Irukkuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Illai Vairam Irukkuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illai Vairam Irukkuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Illai Yetho Irukkume Solli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illai Yetho Irukkume Solli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yosithu Parkum Bothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yosithu Parkum Bothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma Thaaiyu Petti Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma Thaaiyu Petti Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma Solavanthan Kaatha Maari Petti Da Sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma Solavanthan Kaatha Maari Petti Da Sollu"/>
</div>
<div class="lyrico-lyrics-wrapper">Edutha Namma Makkal Ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edutha Namma Makkal Ellam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saathi Sanamellam Koodi Vechu Parkum Bothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saathi Sanamellam Koodi Vechu Parkum Bothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Varusham Varusham Kelarikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varusham Varusham Kelarikku"/>
</div>
<div class="lyrico-lyrics-wrapper">Aathalukku Koluvaga Amarnthukku Velaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aathalukku Koluvaga Amarnthukku Velaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha Makkalukku Lam Eppadi Kaachi Thaara Theriyuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Makkalukku Lam Eppadi Kaachi Thaara Theriyuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avalukku 21 Deivangalum Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avalukku 21 Deivangalum Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Anga 61 Panthigalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anga 61 Panthigalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Athanaikkum Kaavalkaaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athanaikkum Kaavalkaaran"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga Pathunattam Karuppan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Pathunattam Karuppan"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppadi Kaava Kaakuran Theriyuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppadi Kaava Kaakuran Theriyuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anga Parka Bayama Irukkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anga Parka Bayama Irukkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Karuppan Samy Pakkam Vantha Pul Arikuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuppan Samy Pakkam Vantha Pul Arikuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Parka Bayama Irukkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parka Bayama Irukkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Karuppan Samy Pakkam Vantha Pul Arikuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuppan Samy Pakkam Vantha Pul Arikuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vellai Nellai Kutharaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vellai Nellai Kutharaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Veechu Aruva Kondukittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veechu Aruva Kondukittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vellai Nellai Kutharaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vellai Nellai Kutharaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Veechu Aruva Kondukittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veechu Aruva Kondukittu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vellai Nellai Kutharaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vellai Nellai Kutharaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Veechu Aruva Kondukittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veechu Aruva Kondukittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilaiyadi Aadi Varan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilaiyadi Aadi Varan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilaiyadi Aadi Varan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilaiyadi Aadi Varan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kottai Karuppan Samy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottai Karuppan Samy"/>
</div>
<div class="lyrico-lyrics-wrapper">Parka Parka Parka Parka Bayama Irukkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parka Parka Parka Parka Bayama Irukkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Karuppan Samy Pakka Vantha Pul Arikuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuppan Samy Pakka Vantha Pul Arikuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Odi Vaa Karuppa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odi Vaa Karuppa"/>
</div>
</pre>
