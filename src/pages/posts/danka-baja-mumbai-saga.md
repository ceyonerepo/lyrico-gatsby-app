---
title: "danka baja song lyrics"
album: "Mumbai Saga"
artist: "Payal Dev"
lyricist: "Prashant Ingole"
director: "Sanjay Gupta"
path: "/albums/mumbai-saga-lyrics"
song: "Danka Baja"
image: ../../images/albumart/mumbai-saga.jpg
date: 2021-03-19
lang: hindi
youtubeLink: "https://www.youtube.com/embed/dsl8UKnLlQM"
type: "happy"
singers:
  - Dev Negi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Gajanana gajanana gajanana gajanana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gajanana gajanana gajanana gajanana"/>
</div>
<div class="lyrico-lyrics-wrapper">Gajanana gajanana gajanana gajanana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gajanana gajanana gajanana gajanana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bhakt tera aaya re dekh ganraya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhakt tera aaya re dekh ganraya"/>
</div>
<div class="lyrico-lyrics-wrapper">Jiska ik saaya re tu hi ganesha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jiska ik saaya re tu hi ganesha"/>
</div>
<div class="lyrico-lyrics-wrapper">Hath jod aaya re dil hai bhar laaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hath jod aaya re dil hai bhar laaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Usme jo samaya re tu hi ganesha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usme jo samaya re tu hi ganesha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ganpati bappa morya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ganpati bappa morya"/>
</div>
<div class="lyrico-lyrics-wrapper">Mangal murti moreya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mangal murti moreya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bhakt tera aaya re dekh ganraya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhakt tera aaya re dekh ganraya"/>
</div>
<div class="lyrico-lyrics-wrapper">Jiska ik saaya re tu hi ganesha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jiska ik saaya re tu hi ganesha"/>
</div>
<div class="lyrico-lyrics-wrapper">Hath jod aaya re dil hai bhar laaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hath jod aaya re dil hai bhar laaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Usme jo samaya re tu hi ganesha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usme jo samaya re tu hi ganesha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jo kare teri bhakti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jo kare teri bhakti"/>
</div>
<div class="lyrico-lyrics-wrapper">Woh jaane teri shakti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Woh jaane teri shakti"/>
</div>
<div class="lyrico-lyrics-wrapper">Har soch se tu mukti dila de re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Har soch se tu mukti dila de re"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arre danka baja de re tu zoreya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arre danka baja de re tu zoreya"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaya re morya bappa hey moarya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaya re morya bappa hey moarya"/>
</div>
<div class="lyrico-lyrics-wrapper">Jo bhi chup hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jo bhi chup hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Woh dhadkan ko khanka zara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Woh dhadkan ko khanka zara"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaya re morya bappa hey moraya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaya re morya bappa hey moraya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Charo disha mein hai naam tera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Charo disha mein hai naam tera"/>
</div>
<div class="lyrico-lyrics-wrapper">Har dil kare hai gungaan tera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Har dil kare hai gungaan tera"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaya jo bhi sharan tere
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaya jo bhi sharan tere"/>
</div>
<div class="lyrico-lyrics-wrapper">Bharam se hai woh mukt hua
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bharam se hai woh mukt hua"/>
</div>
<div class="lyrico-lyrics-wrapper">Har kisi dharam se ne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Har kisi dharam se ne"/>
</div>
<div class="lyrico-lyrics-wrapper">Karam se ne woh bhakt hua
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karam se ne woh bhakt hua"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aankhon meri teri dikhti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aankhon meri teri dikhti"/>
</div>
<div class="lyrico-lyrics-wrapper">Prachand teri shakti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prachand teri shakti"/>
</div>
<div class="lyrico-lyrics-wrapper">Dukhon se tujhe mukti dilane re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dukhon se tujhe mukti dilane re"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arre danka baja de re tu zoreya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arre danka baja de re tu zoreya"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaya re morya bappa hey moreya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaya re morya bappa hey moreya"/>
</div>
<div class="lyrico-lyrics-wrapper">Arre danka baja de re tu zoreya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arre danka baja de re tu zoreya"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaya re morya bappa hey moreya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaya re morya bappa hey moreya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jo bhi chup hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jo bhi chup hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Woh dhadkan ko khanka zara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Woh dhadkan ko khanka zara"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaya re morya bappa hey moreya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaya re morya bappa hey moreya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jai jai jai jai jai jai jai jai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jai jai jai jai jai jai jai jai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ganpapati bappa morya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ganpapati bappa morya"/>
</div>
<div class="lyrico-lyrics-wrapper">Jai jai jai jai jai jai jai jai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jai jai jai jai jai jai jai jai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mangal murti moreya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mangal murti moreya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ganpapati bappa morya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ganpapati bappa morya"/>
</div>
<div class="lyrico-lyrics-wrapper">Mangal murti moreya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mangal murti moreya"/>
</div>
<div class="lyrico-lyrics-wrapper">Ganpapati bappa morya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ganpapati bappa morya"/>
</div>
<div class="lyrico-lyrics-wrapper">Mangal murti moreya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mangal murti moreya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jai jai jai jai jai jai jai jai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jai jai jai jai jai jai jai jai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ganpapati bappa morya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ganpapati bappa morya"/>
</div>
<div class="lyrico-lyrics-wrapper">Mangal murti moreya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mangal murti moreya"/>
</div>
<div class="lyrico-lyrics-wrapper">Mangal murti moreya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mangal murti moreya"/>
</div>
</pre>
