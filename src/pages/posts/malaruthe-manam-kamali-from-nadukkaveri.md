---
title: "malaruthe manam song lyrics"
album: "Kamali from Nadukkaveri"
artist: "Dheena Dhayalan"
lyricist: "Madhan Karky"
director: "Rajasekar Duraisamy"
path: "/albums/kamali-from-nadukkaveri-lyrics"
song: "Malaruthe Manam"
image: ../../images/albumart/kamali-from-nadukkaveri.jpg
date: 2021-02-19
lang: tamil
youtubeLink: "https://www.youtube.com/embed/0W1t1q4zf68"
type: "Love"
singers:
  - Shakthisree Gopalan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">malaruthe manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malaruthe manam"/>
</div>
<div class="lyrico-lyrics-wrapper">ohohoh uyirellam un swasame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ohohoh uyirellam un swasame"/>
</div>
<div class="lyrico-lyrics-wrapper">ninaivile thinam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninaivile thinam"/>
</div>
<div class="lyrico-lyrics-wrapper">ohohoh  nanaiyuthe en kaanale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ohohoh  nanaiyuthe en kaanale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">iniya thooral neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iniya thooral neram"/>
</div>
<div class="lyrico-lyrics-wrapper">manathil aadum naalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manathil aadum naalam"/>
</div>
<div class="lyrico-lyrics-wrapper">ilaigalil thuligalai miliruthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ilaigalil thuligalai miliruthe"/>
</div>
<div class="lyrico-lyrics-wrapper">paravai thoovi poogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paravai thoovi poogum"/>
</div>
<div class="lyrico-lyrics-wrapper">iragin eera vaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iragin eera vaasam"/>
</div>
<div class="lyrico-lyrics-wrapper">uyirile mulumaiyai valiyuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyirile mulumaiyai valiyuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">kanaavile un paarvaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanaavile un paarvaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">vinmeengalai vilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vinmeengalai vilum"/>
</div>
<div class="lyrico-lyrics-wrapper">alaaviye un maarbile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alaaviye un maarbile"/>
</div>
<div class="lyrico-lyrics-wrapper">kan moodinal varam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kan moodinal varam"/>
</div>
<div class="lyrico-lyrics-wrapper">varam varam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varam varam "/>
</div>
<div class="lyrico-lyrics-wrapper">vaarthai illa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaarthai illa "/>
</div>
<div class="lyrico-lyrics-wrapper">bashaigal pesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bashaigal pesi"/>
</div>
<div class="lyrico-lyrics-wrapper">valginrathe en thevaiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valginrathe en thevaiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">nanum illa kaalathil aeno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanum illa kaalathil aeno"/>
</div>
<div class="lyrico-lyrics-wrapper">sergindrathe un paarvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sergindrathe un paarvai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">malaruthe manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malaruthe manam"/>
</div>
<div class="lyrico-lyrics-wrapper">ohohoh uyirellam un swasame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ohohoh uyirellam un swasame"/>
</div>
<div class="lyrico-lyrics-wrapper">ninaivile thinam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninaivile thinam"/>
</div>
<div class="lyrico-lyrics-wrapper">ohohoh  nanaiyuthe en kaanale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ohohoh  nanaiyuthe en kaanale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aetho ondrai aagiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aetho ondrai aagiren"/>
</div>
<div class="lyrico-lyrics-wrapper">nana endre ketkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nana endre ketkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">poongaatrile pogiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poongaatrile pogiren"/>
</div>
<div class="lyrico-lyrics-wrapper">un moochu than sergiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un moochu than sergiren"/>
</div>
<div class="lyrico-lyrics-wrapper">ne ilam paathaigale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ne ilam paathaigale"/>
</div>
<div class="lyrico-lyrics-wrapper">moolum vaathaigale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moolum vaathaigale"/>
</div>
<div class="lyrico-lyrics-wrapper">eera paarvaigalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eera paarvaigalai"/>
</div>
<div class="lyrico-lyrics-wrapper">koodum sovaigale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koodum sovaigale"/>
</div>
<div class="lyrico-lyrics-wrapper">tholai thooram neyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tholai thooram neyum"/>
</div>
<div class="lyrico-lyrics-wrapper">vaalum pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalum pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">en kadhal unai serume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kadhal unai serume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">malaruthe manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malaruthe manam"/>
</div>
<div class="lyrico-lyrics-wrapper">ohohoh uyirellam un swasame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ohohoh uyirellam un swasame"/>
</div>
<div class="lyrico-lyrics-wrapper">ninaivile thinam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninaivile thinam"/>
</div>
<div class="lyrico-lyrics-wrapper">ohohoh  nanaiyuthe en kaanale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ohohoh  nanaiyuthe en kaanale"/>
</div>
</pre>
