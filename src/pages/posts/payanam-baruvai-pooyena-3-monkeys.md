---
title: "payanam baruvai song lyrics"
album: "3 Monkeys"
artist: "Anil kumar G"
lyricist: "Anil Kumar G"
director: "Anil Kumar G"
path: "/albums/3-monkeys-lyrics"
song: "Payanam Baruvai Pooyena"
image: ../../images/albumart/3-monkeys.jpg
date: 2020-02-07
lang: telugu
youtubeLink: "https://www.youtube.com/embed/KoAWvqkHBqc"
type: "sad"
singers:
  - Madhu Balakrishnan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Payanam Baruvai Pooyenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Payanam Baruvai Pooyenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nayanam Neerai Nindenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nayanam Neerai Nindenaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Payanam Baruvai Pooyenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Payanam Baruvai Pooyenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nayanam Neerai Nindenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nayanam Neerai Nindenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidhi Ninnalaa Ventaadenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidhi Ninnalaa Ventaadenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalarathanee Maarchesenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalarathanee Maarchesenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhigulee Vidadee Enduku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhigulee Vidadee Enduku"/>
</div>
<div class="lyrico-lyrics-wrapper">Adugee Padadee Munduku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adugee Padadee Munduku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Payanam Baruvai Pooyenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Payanam Baruvai Pooyenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nayanam Neerai Nindenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nayanam Neerai Nindenaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kshanakaalam Kshanikaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kshanakaalam Kshanikaale"/>
</div>
<div class="lyrico-lyrics-wrapper">cherasaalai pooyenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cherasaalai pooyenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Nimisham Aandham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Nimisham Aandham"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippudayyenaa Nee Gatham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippudayyenaa Nee Gatham"/>
</div>
<div class="lyrico-lyrics-wrapper">Minne Thaake Aashalee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minne Thaake Aashalee"/>
</div>
<div class="lyrico-lyrics-wrapper">Needaipooye Adiyaashalee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Needaipooye Adiyaashalee"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhalo Ee Gaayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhalo Ee Gaayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thudhilo Nee Praanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thudhilo Nee Praanam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Payanam Baruvai Pooyenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Payanam Baruvai Pooyenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nayanam Neerai Nindenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nayanam Neerai Nindenaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Payanam Baruvai Pooyenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Payanam Baruvai Pooyenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nayanam Neerai Nindenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nayanam Neerai Nindenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidhi ninnalaa Ventaadenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidhi ninnalaa Ventaadenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalarathanee Maarchesenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalarathanee Maarchesenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhigulee Vidadee enduku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhigulee Vidadee enduku"/>
</div>
<div class="lyrico-lyrics-wrapper">Adugee Padadee Munduku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adugee Padadee Munduku"/>
</div>
</pre>
