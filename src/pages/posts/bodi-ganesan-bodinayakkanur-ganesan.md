---
title: "bodi ganesan song lyrics"
album: "Bodinayakkanur Ganesan"
artist: "John Peter"
lyricist: "Ilaya Kamban"
director: "O. Gnanam"
path: "/albums/bodinayakkanur-ganesan-lyrics"
song: "Bodi Ganesan"
image: ../../images/albumart/bodinayakkanur-ganesan.jpg
date: 2011-07-29
lang: tamil
youtubeLink: "https://www.youtube.com/embed/uXQKqV-bUyU"
type: "happy"
singers:
  - Velmurugan
  - Chinnaponnu
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hey niruthappa naanum paarkkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey niruthappa naanum paarkkuren"/>
</div>
<div class="lyrico-lyrics-wrapper">aathularundhu Town-varaikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aathularundhu Town-varaikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">moththa gumbalayum vachu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moththa gumbalayum vachu "/>
</div>
<div class="lyrico-lyrics-wrapper">summa gulugulunnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="summa gulugulunnu"/>
</div>
<div class="lyrico-lyrics-wrapper">aatti vandhittirukkeenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aatti vandhittirukkeenga"/>
</div>
<div class="lyrico-lyrics-wrapper">mechaa vesam poattavanakkaanala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mechaa vesam poattavanakkaanala"/>
</div>
<div class="lyrico-lyrics-wrapper">bappunu vesham poattavankkaanala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bappunu vesham poattavankkaanala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idho vandhuttaangalla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idho vandhuttaangalla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey avinga illappa ivinga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey avinga illappa ivinga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yei yaaraa irundhaa unakkenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei yaaraa irundhaa unakkenna"/>
</div>
<div class="lyrico-lyrics-wrapper">unga aaluga sarakka poattuttu mattaiyaayittaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unga aaluga sarakka poattuttu mattaiyaayittaanga"/>
</div>
<div class="lyrico-lyrics-wrapper">naanga moodukku Form aayitoamlla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naanga moodukku Form aayitoamlla"/>
</div>
<div class="lyrico-lyrics-wrapper">onakku thevai aattandhaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="onakku thevai aattandhaana"/>
</div>
<div class="lyrico-lyrics-wrapper">nee adichi norukku naanga aadi kizhikkiroam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee adichi norukku naanga aadi kizhikkiroam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bodi Ganesan godhaavil kudhichivarendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bodi Ganesan godhaavil kudhichivarendi"/>
</div>
<div class="lyrico-lyrics-wrapper">un naadi naramba pirichi meyappoarendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un naadi naramba pirichi meyappoarendi"/>
</div>
<div class="lyrico-lyrics-wrapper">hei Theni Jillava maaman kalakkapporendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hei Theni Jillava maaman kalakkapporendi"/>
</div>
<div class="lyrico-lyrics-wrapper">un thenaavattathaan adakki aalapporendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un thenaavattathaan adakki aalapporendi"/>
</div>
<div class="lyrico-lyrics-wrapper">Odi udaigiren moadhimidhakkirendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odi udaigiren moadhimidhakkirendi"/>
</div>
<div class="lyrico-lyrics-wrapper">vettaruvaa meesakkaarandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vettaruvaa meesakkaarandi"/>
</div>
<div class="lyrico-lyrics-wrapper">indha Ganesan ondiveeran saamiperandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indha Ganesan ondiveeran saamiperandi"/>
</div>
<div class="lyrico-lyrics-wrapper">aadi maasakkaalathula soodam yeththum aaludi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aadi maasakkaalathula soodam yeththum aaludi"/>
</div>
<div class="lyrico-lyrics-wrapper">aaradiyil singakkuttidi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaradiyil singakkuttidi"/>
</div>
<div class="lyrico-lyrics-wrapper">indha Ganesan anbuvachaa thangakkattidi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indha Ganesan anbuvachaa thangakkattidi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bodi Ganesan godhaavil kudhichivarendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bodi Ganesan godhaavil kudhichivarendi"/>
</div>
<div class="lyrico-lyrics-wrapper">un naadi naramba pirichi meyappoarendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un naadi naramba pirichi meyappoarendi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan soraanganni ponnudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan soraanganni ponnudaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu kodakambi kannudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu kodakambi kannudaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">OhO
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="OhO"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karagaattam aadura kanni iva jinnudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karagaattam aadura kanni iva jinnudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">moottu illaa menu indha pulla menidaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moottu illaa menu indha pulla menidaa"/>
</div>
<div class="lyrico-lyrics-wrapper">annaa gnaanidaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="annaa gnaanidaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannam senja malaiya naan kodaiyuvendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannam senja malaiya naan kodaiyuvendi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannam senja malaiya naan kodaiyuvendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannam senja malaiya naan kodaiyuvendi"/>
</div>
<div class="lyrico-lyrics-wrapper">soorakkaathulaye vennaiyathaan kadaiyuvendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="soorakkaathulaye vennaiyathaan kadaiyuvendi"/>
</div>
<div class="lyrico-lyrics-wrapper">engittathaan aattaadha un vaaladi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engittathaan aattaadha un vaaladi"/>
</div>
<div class="lyrico-lyrics-wrapper">enakku eppavumey yerumugamthaanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enakku eppavumey yerumugamthaanadi"/>
</div>
<div class="lyrico-lyrics-wrapper">solluradha kaadhil neeyum keludi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="solluradha kaadhil neeyum keludi"/>
</div>
<div class="lyrico-lyrics-wrapper">naan sunaamiyil neechalidum aaludi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan sunaamiyil neechalidum aaludi"/>
</div>
<div class="lyrico-lyrics-wrapper">nee koththu poada veraalappaarudi paarudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee koththu poada veraalappaarudi paarudi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bodi Ganesan godhaavil kudhichivarendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bodi Ganesan godhaavil kudhichivarendi"/>
</div>
<div class="lyrico-lyrics-wrapper">un naadi naramba pirichi meyappoarendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un naadi naramba pirichi meyappoarendi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Puliyamboovu lavukkakkaari 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puliyamboovu lavukkakkaari "/>
</div>
<div class="lyrico-lyrics-wrapper">puliyappoala paachalkkaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puliyappoala paachalkkaari"/>
</div>
<div class="lyrico-lyrics-wrapper">urumbimela iduppukkaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="urumbimela iduppukkaari"/>
</div>
<div class="lyrico-lyrics-wrapper">uppu maangaa udhattukkaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uppu maangaa udhattukkaari"/>
</div>
<div class="lyrico-lyrics-wrapper">goarimettu viththakkaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="goarimettu viththakkaari"/>
</div>
<div class="lyrico-lyrics-wrapper">aavipparakkum muththakkaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aavipparakkum muththakkaari"/>
</div>
<div class="lyrico-lyrics-wrapper">kallacharakku kannukkaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kallacharakku kannukkaari"/>
</div>
<div class="lyrico-lyrics-wrapper">kanjaa poadhai sirippukkaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanjaa poadhai sirippukkaari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhu naanthaandaa enna thookkikkadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu naanthaandaa enna thookkikkadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">un kaaichalathaan konjam poththikkadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kaaichalathaan konjam poththikkadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">kaathadikkum oorey thoothikkadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaathadikkum oorey thoothikkadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">nee kuliradichaa enna thoothikkadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee kuliradichaa enna thoothikkadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan palapera paarthava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan palapera paarthava"/>
</div>
<div class="lyrico-lyrics-wrapper">Aamaa aamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aamaa aamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">un paarvaikki yethava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un paarvaikki yethava"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Urvasi Rambaikki oru vayasu moothava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urvasi Rambaikki oru vayasu moothava"/>
</div>
<div class="lyrico-lyrics-wrapper">summaa naanum thottupputtaa pachakkuthuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="summaa naanum thottupputtaa pachakkuthuven"/>
</div>
<div class="lyrico-lyrics-wrapper">paal sombathedum poonaiyaattam enna suththuva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paal sombathedum poonaiyaattam enna suththuva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kottakkudi aathula naan kulichavandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottakkudi aathula naan kulichavandi"/>
</div>
<div class="lyrico-lyrics-wrapper">kottakkudi aathula naan kulichavandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kottakkudi aathula naan kulichavandi"/>
</div>
<div class="lyrico-lyrics-wrapper">andha thoalukku maalaiyil kodi yeththuvandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="andha thoalukku maalaiyil kodi yeththuvandi"/>
</div>
<div class="lyrico-lyrics-wrapper">ye vaadippatti thappaipoala irukkura nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ye vaadippatti thappaipoala irukkura nee"/>
</div>
<div class="lyrico-lyrics-wrapper">vadugappatti poondu poala ilikkira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vadugappatti poondu poala ilikkira"/>
</div>
<div class="lyrico-lyrics-wrapper">Bodi mettu yelakkaayaa manakkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bodi mettu yelakkaayaa manakkura"/>
</div>
<div class="lyrico-lyrics-wrapper">enna porivachi pudikkathaan ninaikira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna porivachi pudikkathaan ninaikira"/>
</div>
<div class="lyrico-lyrics-wrapper">sela veesi singathathaan pudikkira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sela veesi singathathaan pudikkira"/>
</div>
<div class="lyrico-lyrics-wrapper">sela veesi singathathaan pudikkira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sela veesi singathathaan pudikkira"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bodi Ganesan godhaavil kudhichivarendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bodi Ganesan godhaavil kudhichivarendi"/>
</div>
<div class="lyrico-lyrics-wrapper">un naadi naramba pirichi meyappoarendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un naadi naramba pirichi meyappoarendi"/>
</div>
</pre>
