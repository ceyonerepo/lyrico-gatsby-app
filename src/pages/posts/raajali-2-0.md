---
title: 'raajali song lyrics'
album: '2.0'
artist: 'A R Rahman'
lyricist: 'Madhan Karky'
director: 'Shankar'
path: '/albums/2.0-song-lyrics'
song: 'Raajali'
image: ../../images/albumart/2-0.jpg
date: 2017-10-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/CHYfO--S40g"
type: 'Love'
singers: 
- Blaaze
- Arjun Chandy
- Sid Sriram
---

<pre class="lyrics-native">
  </pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Issac Asimo Peran Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Issac Asimo Peran Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Sundakka size Sooran Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sundakka size Sooran Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Issac Asimo Peran Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Issac Asimo Peran Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Sundakka size Sooran Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sundakka size Sooran Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raajali Nee Gaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raajali Nee Gaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Innaiku Engalukku Diwali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innaiku Engalukku Diwali"/>
</div>
<div class="lyrico-lyrics-wrapper">Raajali Semma Jolly
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raajali Semma Jolly"/>
</div>
<div class="lyrico-lyrics-wrapper">Naragathukku Nee Virundhaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naragathukku Nee Virundhaali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Massu Naan Kodi Maasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Massu Naan Kodi Maasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vedichaka Boom Pattasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedichaka Boom Pattasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Boss-eh Naan Kutta Bossu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boss-eh Naan Kutta Bossu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maatikitta Machan Nee Poota Case-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maatikitta Machan Nee Poota Case-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naga Naga Naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naga Naga Naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aale Ambu Beerangi Nee Mullangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aale Ambu Beerangi Nee Mullangi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naga Naga Naa Thaan Iyangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naga Naga Naa Thaan Iyangi"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kaadhula Vachen Sambagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kaadhula Vachen Sambagi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naga Naga Naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naga Naga Naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aale Ambu Beerangi Nee Mullangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aale Ambu Beerangi Nee Mullangi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naga Naga Naa Thaan Iyangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naga Naga Naa Thaan Iyangi"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kaadhula Vachen Sambagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kaadhula Vachen Sambagi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naga Naga Naga Ranguski
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naga Naga Naga Ranguski"/>
</div>
<div class="lyrico-lyrics-wrapper">Unaku Udha Vandhen Sanguskki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unaku Udha Vandhen Sanguskki"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudi Pudi Pudi da Mooka Pudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudi Pudi Pudi da Mooka Pudi"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Mookula Poonthen Thaaku Pudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Mookula Poonthen Thaaku Pudi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raajali Nee Gaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raajali Nee Gaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Innaiku Engalukku Diwali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innaiku Engalukku Diwali"/>
</div>
<div class="lyrico-lyrics-wrapper">Raajali Semma Jolly
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raajali Semma Jolly"/>
</div>
<div class="lyrico-lyrics-wrapper">Naragathukku Nee Virundhaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naragathukku Nee Virundhaali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Massu Naan Kodi Maasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Massu Naan Kodi Maasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vedichaka Boom Pattasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedichaka Boom Pattasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Boss-eh Naan Kutta Bossu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boss-eh Naan Kutta Bossu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maatikitta Machan Nee Poota Case-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maatikitta Machan Nee Poota Case-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Machi Sikkikicho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machi Sikkikicho"/>
</div>
<div class="lyrico-lyrics-wrapper">Rekka Pichikicho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rekka Pichikicho"/>
</div>
<div class="lyrico-lyrics-wrapper">Achchoo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achchoo "/>
</div>
</pre>