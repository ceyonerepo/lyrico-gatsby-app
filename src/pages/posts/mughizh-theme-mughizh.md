---
title: "mughizh theme song lyrics"
album: "Mughizh"
artist: "Revaa"
lyricist: "Balaji Tharaneetharan"
director: "Karthik Swaminathan"
path: "/albums/mughizh-lyrics"
song: "Mughizh Theme"
image: ../../images/albumart/mughizh.jpg
date: 2021-10-08
lang: tamil
youtubeLink: "https://www.youtube.com/embed/nJkchEkkciE"
type: "happy"
singers:
  - Pradeep Kumar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Rapappaara raa raa raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rapappaara raa raa raa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh rapappaara raa raa raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh rapappaara raa raa raa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oyaadha alai poley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oyaadha alai poley"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhvu theeraatha kadal poley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvu theeraatha kadal poley"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethu vandha podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethu vandha podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalangal neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalangal neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbaandrai kai enthu vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbaandrai kai enthu vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyir vaazhum vazhiyengum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir vaazhum vazhiyengum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oli veesi vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oli veesi vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai serkka vaa vaa vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai serkka vaa vaa vaa vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oyaadha alai poley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oyaadha alai poley"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhvu theeraatha kadal poley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvu theeraatha kadal poley"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethu vandha podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethu vandha podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalangal neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalangal neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbaandrai kai enthu vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbaandrai kai enthu vaa"/>
</div>
</pre>
