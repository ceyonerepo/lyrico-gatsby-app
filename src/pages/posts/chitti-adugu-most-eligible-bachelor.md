---
title: "chitti adugu song lyrics"
album: "Most Eligible Bachelor"
artist: "Gopi Sundar"
lyricist: "Sirivennela Seetharama Sastry"
director: "Bommarillu Bhaskar"
path: "/albums/most-eligible-bachelor-lyrics"
song: "Chitti Adugu"
image: ../../images/albumart/most-eligible-bachelor.jpg
date: 2021-10-15
lang: telugu
youtubeLink: "https://www.youtube.com/embed/qD8sPWTekDQ"
type: "love"
singers:
  - Zia Ul Haq
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Arareyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arareyy"/>
</div>
<div class="lyrico-lyrics-wrapper">Evaru em cheppaledha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evaru em cheppaledha"/>
</div>
<div class="lyrico-lyrics-wrapper">Okka saari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okka saari"/>
</div>
<div class="lyrico-lyrics-wrapper">Innaallu gaaliloney theliyaade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innaallu gaaliloney theliyaade"/>
</div>
<div class="lyrico-lyrics-wrapper">Chitti aduga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chitti aduga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sarleyy ippudainaa thlisindhigaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarleyy ippudainaa thlisindhigaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholi saari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholi saari"/>
</div>
<div class="lyrico-lyrics-wrapper">Ikanainaa nelaa thaaki nerchukovey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ikanainaa nelaa thaaki nerchukovey"/>
</div>
<div class="lyrico-lyrics-wrapper">Kotha nadaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kotha nadaka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Innaallu ninnethukuni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innaallu ninnethukuni"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooreginchina ee lokaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooreginchina ee lokaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaana bharavu thaaney saariga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaana bharavu thaaney saariga"/>
</div>
<div class="lyrico-lyrics-wrapper">Moyaleni ooh maalokam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moyaleni ooh maalokam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Innaallu gaalilone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innaallu gaalilone"/>
</div>
<div class="lyrico-lyrics-wrapper">Theliyaade chitti adugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theliyaade chitti adugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ikanaina nela thaaki nerchukove
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ikanaina nela thaaki nerchukove"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottha nadaaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottha nadaaka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Shilalaanti ninnu ilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shilalaanti ninnu ilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Shilpanga malichindhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shilpanga malichindhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aah navvulo churakaleyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aah navvulo churakaleyy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee sontha kaala laaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee sontha kaala laaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kanta nilichindhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kanta nilichindhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aah divvelo merupuleyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aah divvelo merupuleyy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Achchangaa thanala undha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achchangaa thanala undha"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhdham choopey nee roopam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhdham choopey nee roopam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee sontha chirunamala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee sontha chirunamala"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanipisthondha ee malupu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanipisthondha ee malupu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Innaallu gaalilone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innaallu gaalilone"/>
</div>
<div class="lyrico-lyrics-wrapper">Theliyaade chitti adugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theliyaade chitti adugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ikanaina nela thaaki nerchukove
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ikanaina nela thaaki nerchukove"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottha nadaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottha nadaka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enneni janmalaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enneni janmalaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Thegiponi bandham edho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thegiponi bandham edho"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhuraindhi nee dharilo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhuraindhi nee dharilo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maatalaki andhani bhaavam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maatalaki andhani bhaavam"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasela gurthisthondho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasela gurthisthondho"/>
</div>
<div class="lyrico-lyrics-wrapper">Thlisindhi aa chelimitho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thlisindhi aa chelimitho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inkevari kallo choosey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inkevari kallo choosey"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalavey nuvvu innaallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalavey nuvvu innaallu"/>
</div>
<div class="lyrico-lyrics-wrapper">Entha baagundho choodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entha baagundho choodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee tholi vekuva eenaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee tholi vekuva eenaadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Innaallu gaalilone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innaallu gaalilone"/>
</div>
<div class="lyrico-lyrics-wrapper">Theliyaade chitti adugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theliyaade chitti adugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ikanaina nela thaaki nerchukove
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ikanaina nela thaaki nerchukove"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottha nadaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottha nadaka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arareyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arareyy"/>
</div>
<div class="lyrico-lyrics-wrapper">Evaru em cheppaledha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evaru em cheppaledha"/>
</div>
<div class="lyrico-lyrics-wrapper">Okka saari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okka saari"/>
</div>
<div class="lyrico-lyrics-wrapper">Innaallu gaaliloney theliyaade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innaallu gaaliloney theliyaade"/>
</div>
<div class="lyrico-lyrics-wrapper">Chitti aduga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chitti aduga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sarleyy ippudainaa thlisindhigaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarleyy ippudainaa thlisindhigaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholi saari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholi saari"/>
</div>
<div class="lyrico-lyrics-wrapper">Ikanainaa nela thaaki nerchukoveyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ikanainaa nela thaaki nerchukoveyy"/>
</div>
<div class="lyrico-lyrics-wrapper">Kotha nadaaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kotha nadaaka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Innaallu ninnethukuni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innaallu ninnethukuni"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooreginchina ee lokam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooreginchina ee lokam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana bharavu thaaney saarigaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana bharavu thaaney saarigaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Moyaleni ooh maalokam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moyaleni ooh maalokam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Innaallu gaalilone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innaallu gaalilone"/>
</div>
<div class="lyrico-lyrics-wrapper">Theliyaade chitti adugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theliyaade chitti adugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ikanaina nela thaaki nerchukove
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ikanaina nela thaaki nerchukove"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottha nadaaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottha nadaaka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Innaallu gaalilone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innaallu gaalilone"/>
</div>
<div class="lyrico-lyrics-wrapper">Theliyaade chitti adugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theliyaade chitti adugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ikanaina nela thaaki nerchukove
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ikanaina nela thaaki nerchukove"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottha nadaaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottha nadaaka"/>
</div>
</pre>
