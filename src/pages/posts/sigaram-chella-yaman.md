---
title: "sigaram chella song lyrics"
album: "Yaman"
artist: "Vijay Antony"
lyricist: "Eknath"
director: "Jeeva Shankar"
path: "/albums/yaman-lyrics"
song: "Sigaram Chella"
image: ../../images/albumart/yaman.jpg
date: 2017-02-24
lang: tamil
youtubeLink: "https://www.youtube.com/embed/S-KdklfeBfw"
type: "mass"
singers:
  - Jagadeesh Kumar
  - Ranjith Unni
  - Velmurugan
  - Ayappan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Sigaram Chella Uyaram Thulla 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sigaram Chella Uyaram Thulla "/>
</div>
<div class="lyrico-lyrics-wrapper">Valiyai Pazhagi Vaithudu Kurukki 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valiyai Pazhagi Vaithudu Kurukki "/>
</div>
<div class="lyrico-lyrics-wrapper">Melae Ponal Kettavan Yellam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Melae Ponal Kettavan Yellam "/>
</div>
<div class="lyrico-lyrics-wrapper">Bali Bali Bali 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bali Bali Bali "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tharaigal Undu Thandi Chela 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharaigal Undu Thandi Chela "/>
</div>
<div class="lyrico-lyrics-wrapper">Pazhiyai Maatra Padam Padiththu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pazhiyai Maatra Padam Padiththu "/>
</div>
<div class="lyrico-lyrics-wrapper">Kadavul Pillai Kavalai Illai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadavul Pillai Kavalai Illai "/>
</div>
<div class="lyrico-lyrics-wrapper">Theri Theri Theri 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theri Theri Theri "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha Vazhve Porkalam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Vazhve Porkalam "/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Vazhndhe Thirkanum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vazhndhe Thirkanum "/>
</div>
<div class="lyrico-lyrics-wrapper">Yendrum Vizhipaai Irukanum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yendrum Vizhipaai Irukanum "/>
</div>
<div class="lyrico-lyrics-wrapper">Manidhargal Uruvathil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manidhargal Uruvathil "/>
</div>
<div class="lyrico-lyrics-wrapper">Mirugamgal Nee Kaanuvaai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mirugamgal Nee Kaanuvaai "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poiye Ulagam Potti Nilavum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poiye Ulagam Potti Nilavum "/>
</div>
<div class="lyrico-lyrics-wrapper">Mannil Kaayum Raththa Thuli 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannil Kaayum Raththa Thuli "/>
</div>
<div class="lyrico-lyrics-wrapper">Thappai Kandaal Thooki Potu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappai Kandaal Thooki Potu "/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Adi Adi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Adi Adi "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pin Theeyin Pillai Thimirin Ellai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pin Theeyin Pillai Thimirin Ellai "/>
</div>
<div class="lyrico-lyrics-wrapper">Yamane Undhan Thaayin Madi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yamane Undhan Thaayin Madi "/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhiri Yendru Yevanum Vandhaal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhiri Yendru Yevanum Vandhaal "/>
</div>
<div class="lyrico-lyrics-wrapper">Mudi Mudi Mudi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudi Mudi Mudi "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oodadhe Thurathidum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oodadhe Thurathidum "/>
</div>
<div class="lyrico-lyrics-wrapper">Odhungaadhe Miratidum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odhungaadhe Miratidum "/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhir Nindral Mirandidum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhir Nindral Mirandidum "/>
</div>
<div class="lyrico-lyrics-wrapper">Manidhargal Uruvathil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manidhargal Uruvathil "/>
</div>
<div class="lyrico-lyrics-wrapper">Mirugamgal Nee Kaanuvaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mirugamgal Nee Kaanuvaai"/>
</div>
</pre>
