---
title: "assah pussah love panna song lyrics"
album: "Muni"
artist: "Bharathwaj"
lyricist: "Pa. Vijay - Raghava Lawrence"
director: "Raghava Lawrence"
path: "/albums/muni-lyrics"
song: "Assah Pussah Love Panna"
image: ../../images/albumart/muni.jpg
date: 2007-03-09
lang: tamil
youtubeLink: "https://www.youtube.com/embed/shcvhAFMi9g"
type: "Love"
singers:
  - Karthik
  - Priya
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Assha bussha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Assha bussha"/>
</div>
<div class="lyrico-lyrics-wrapper">Lovvu panna polaamaa…..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lovvu panna polaamaa….."/>
</div>
<div class="lyrico-lyrics-wrapper">Haan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Assha bussha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Assha bussha"/>
</div>
<div class="lyrico-lyrics-wrapper">Lovvu panna polaamaa…..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lovvu panna polaamaa….."/>
</div>
<div class="lyrico-lyrics-wrapper">Assha bussha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Assha bussha"/>
</div>
<div class="lyrico-lyrics-wrapper">Lovvu panna polaamaa…..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lovvu panna polaamaa….."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paarthalae leasaa moraikkiraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarthalae leasaa moraikkiraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi nenja ice-ah karaikkiraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi nenja ice-ah karaikkiraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nikkaama nikkuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikkaama nikkuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadakkaame nadakkuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadakkaame nadakkuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pinnaalae suththuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pinnaalae suththuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannaalae kuththuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaalae kuththuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennammaa chinnammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennammaa chinnammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee thaan en kaadhal kannammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thaan en kaadhal kannammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ammammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ammammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammammaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Assha bussha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Assha bussha"/>
</div>
<div class="lyrico-lyrics-wrapper">Lovvu panna polaamaa…..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lovvu panna polaamaa….."/>
</div>
<div class="lyrico-lyrics-wrapper">Assha bussha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Assha bussha"/>
</div>
<div class="lyrico-lyrics-wrapper">Lovvu panna polaamaa…..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lovvu panna polaamaa….."/>
</div>
<div class="lyrico-lyrics-wrapper">Polamaa…ooo hooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Polamaa…ooo hooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bumshalakadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bumshalakadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Bumshalakadi hahaan hahaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bumshalakadi hahaan hahaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Bumshalakadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bumshalakadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Bumshalakadi hahaan hahaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bumshalakadi hahaan hahaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Bum sha lakadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bum sha lakadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Bum sha lakadi hah aan hah aan heii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bum sha lakadi hah aan hah aan heii"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Orappaarva paakkuriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orappaarva paakkuriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nearaa paathaa ennammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nearaa paathaa ennammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Assha pusshaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Assha pusshaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullukkulla kaadhala maraichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullukkulla kaadhala maraichu"/>
</div>
<div class="lyrico-lyrics-wrapper">Veliyea nadikkum kannammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veliyea nadikkum kannammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhusshaa asshaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhusshaa asshaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Onga appaavukkum sammathanthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onga appaavukkum sammathanthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Onga ammaavukkum sammathanthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onga ammaavukkum sammathanthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">On annanukkum sammathanthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="On annanukkum sammathanthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">On thangachchikkum sammathanthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="On thangachchikkum sammathanthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaru sonnaa enn
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaru sonnaa enn"/>
</div>
<div class="lyrico-lyrics-wrapper">Kettaa enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kettaa enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan thaandaa ok sollanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan thaandaa ok sollanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollammaa chellamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollammaa chellamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Assha bussha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Assha bussha"/>
</div>
<div class="lyrico-lyrics-wrapper">Lovvu panna polaamaa…..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lovvu panna polaamaa….."/>
</div>
<div class="lyrico-lyrics-wrapper">Assha bussha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Assha bussha"/>
</div>
<div class="lyrico-lyrics-wrapper">Lovvu panna polaamaa…..aaa…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lovvu panna polaamaa…..aaa…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nini sa sa ni ni sa sa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nini sa sa ni ni sa sa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nini sa sa ri ma ga ri nisa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nini sa sa ri ma ga ri nisa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ashha bushhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ashha bushhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Re re pa re re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Re re pa re re"/>
</div>
<div class="lyrico-lyrics-wrapper">Nirigamamamani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nirigamamamani"/>
</div>
<div class="lyrico-lyrics-wrapper">Ashha bushhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ashha bushhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Paamagare gamapapa magare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paamagare gamapapa magare"/>
</div>
<div class="lyrico-lyrics-wrapper">Ashha bushhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ashha bushhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ashha bushhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ashha bushhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaalameenukkum hahaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaalameenukkum hahaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Velangu meenukkum…..hahaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velangu meenukkum…..hahaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaalameenukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaalameenukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Velangu meenukkum..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velangu meenukkum.."/>
</div>
<div class="lyrico-lyrics-wrapper">Kalyaanamaam kalyaanamaam….
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalyaanamaam kalyaanamaam…."/>
</div>
<div class="lyrico-lyrics-wrapper">Kalyaanamaam kalyaanamaam….
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalyaanamaam kalyaanamaam…."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeyum naanum kalyaananthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum naanum kalyaananthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pannikkittaa thappillae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pannikkittaa thappillae"/>
</div>
<div class="lyrico-lyrics-wrapper">Asshaa bhussaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asshaa bhussaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Black and white colorula thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Black and white colorula thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pulla porakkum thappilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pulla porakkum thappilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhusshaa asshaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhusshaa asshaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Asshaa bhusshaa sollikkittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asshaa bhusshaa sollikkittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathal pichcha kettukkittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathal pichcha kettukkittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna mattum suththikkittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna mattum suththikkittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaathaiyum vittupputten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaathaiyum vittupputten"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada kannaa pinnaa kanaakkandu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada kannaa pinnaa kanaakkandu"/>
</div>
<div class="lyrico-lyrics-wrapper">Olaraatha summaa kadhathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Olaraatha summaa kadhathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Poyendaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poyendaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Poreandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poreandi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Assha bussha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Assha bussha"/>
</div>
<div class="lyrico-lyrics-wrapper">Lovvu panna polaamaa…..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lovvu panna polaamaa….."/>
</div>
<div class="lyrico-lyrics-wrapper">Assha bussha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Assha bussha"/>
</div>
<div class="lyrico-lyrics-wrapper">Lovvu panna polaamaa…..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lovvu panna polaamaa….."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paarthalae leasaa moraikkiraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarthalae leasaa moraikkiraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi nenja ice-ah karaikkiraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi nenja ice-ah karaikkiraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nikkaama nikkuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikkaama nikkuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadakkaame nadakkuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadakkaame nadakkuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pinnaalae suththuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pinnaalae suththuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannaalae kuththuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaalae kuththuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennammaa chinnammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennammaa chinnammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee thaan en kaadhal kannammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thaan en kaadhal kannammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ammammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ammammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammammaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Assha bussha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Assha bussha"/>
</div>
<div class="lyrico-lyrics-wrapper">Lovvu panna polaamaa…..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lovvu panna polaamaa….."/>
</div>
<div class="lyrico-lyrics-wrapper">Assha bussha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Assha bussha"/>
</div>
<div class="lyrico-lyrics-wrapper">Lovvu panna polaamaa…..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lovvu panna polaamaa….."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Assha bussha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Assha bussha"/>
</div>
<div class="lyrico-lyrics-wrapper">Bussha assha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bussha assha"/>
</div>
<div class="lyrico-lyrics-wrapper">Assha bussha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Assha bussha"/>
</div>
<div class="lyrico-lyrics-wrapper">Bussha assha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bussha assha"/>
</div>
</pre>
