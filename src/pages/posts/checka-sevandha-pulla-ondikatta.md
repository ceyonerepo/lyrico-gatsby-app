---
title: "checka sevandha pulla song lyrics"
album: "Ondikatta"
artist: "Bharani"
lyricist: "Bharani"
director: "Bharani"
path: "/albums/ondikatta-lyrics"
song: "Checka Sevandha Pulla"
image: ../../images/albumart/ondikatta.jpg
date: 2018-07-20
lang: tamil
youtubeLink: "https://www.youtube.com/embed/I4rDKJ2wWrk"
type: "sad"
singers:
  - Bharani
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">sekka sevantha pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sekka sevantha pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">senni malai thoppu kulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="senni malai thoppu kulla"/>
</div>
<div class="lyrico-lyrics-wrapper">kooda thalaiya vachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kooda thalaiya vachu"/>
</div>
<div class="lyrico-lyrics-wrapper">vanthu puttu pora pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanthu puttu pora pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">unna nenaikirene mane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna nenaikirene mane"/>
</div>
<div class="lyrico-lyrics-wrapper">enna arivamani enge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna arivamani enge"/>
</div>
<div class="lyrico-lyrics-wrapper">nanum aagurene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanum aagurene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sekka sevantha pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sekka sevantha pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">senni malai thoppu kulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="senni malai thoppu kulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">majal kulikirathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="majal kulikirathum"/>
</div>
<div class="lyrico-lyrics-wrapper">maarappu mathunathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maarappu mathunathum"/>
</div>
<div class="lyrico-lyrics-wrapper">patha orakannu venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="patha orakannu venum"/>
</div>
<div class="lyrico-lyrics-wrapper">nenju porukuthu illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenju porukuthu illa"/>
</div>
<div class="lyrico-lyrics-wrapper">mangi sirikirathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mangi sirikirathum"/>
</div>
<div class="lyrico-lyrics-wrapper">maata vachu nepunathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maata vachu nepunathum"/>
</div>
<div class="lyrico-lyrics-wrapper">patha urakkam illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="patha urakkam illa"/>
</div>
<div class="lyrico-lyrics-wrapper">nenju vedikuthu la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenju vedikuthu la"/>
</div>
<div class="lyrico-lyrics-wrapper">kandaangi sela kaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kandaangi sela kaari"/>
</div>
<div class="lyrico-lyrics-wrapper">kandaangi sela kaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kandaangi sela kaari"/>
</div>
<div class="lyrico-lyrics-wrapper">senkaanthu pottu kari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="senkaanthu pottu kari"/>
</div>
<div class="lyrico-lyrics-wrapper">kodaani konda kaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kodaani konda kaari"/>
</div>
<div class="lyrico-lyrics-wrapper">nee thaan di nee than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee thaan di nee than"/>
</div>
<div class="lyrico-lyrics-wrapper">unna ninaikiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna ninaikiren"/>
</div>
<div class="lyrico-lyrics-wrapper">en manam veguthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en manam veguthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">enna aruvamani aaguthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna aruvamani aaguthadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sekka sevantha pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sekka sevantha pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">senni malai thoppu kulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="senni malai thoppu kulla"/>
</div>
</pre>
