---
title: "tharagathi gadhi song lyrics"
album: "Colour Photo"
artist: "Kaala Bhairava"
lyricist: "Kittu Vissapragada"
director: "Vijay Kumar Konda"
path: "/albums/colour-photo-lyrics"
song: "Tharagathi Gadhi"
image: ../../images/albumart/colour-photo.jpg
date: 2020-10-23
lang: telugu
youtubeLink: "https://www.youtube.com/embed/cqwc0_C_Yt0"
type: "love"
singers:
  - Kaala Bhairava
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Tholi palukulathone karigina manasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholi palukulathone karigina manasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chiru chinukula lage jaarey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chiru chinukula lage jaarey"/>
</div>
<div class="lyrico-lyrics-wrapper">Gusa gusalanu vintu alaluga vayasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gusa gusalanu vintu alaluga vayasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Padha padhamani theeram cheerey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padha padhamani theeram cheerey"/>
</div>
<div class="lyrico-lyrics-wrapper">Yee pani paata leni ee challa gaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yee pani paata leni ee challa gaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohh… sagam chote kori mee… kathey vindha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohh… sagam chote kori mee… kathey vindha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooru peru leni ooha lokana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooru peru leni ooha lokana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaara theeram dhaati sagindhaa premaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaara theeram dhaati sagindhaa premaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharagathi gadhi dhaati tharalina kathaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharagathi gadhi dhaati tharalina kathaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Teliyani teguvedho cheerey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teliyani teguvedho cheerey"/>
</div>
<div class="lyrico-lyrics-wrapper">Adugulu padaleni tholi thapanalaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adugulu padaleni tholi thapanalaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Iruvuri mohamaataale dhuram poye neede
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruvuri mohamaataale dhuram poye neede"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raane geetha daate vidhe maare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raane geetha daate vidhe maare"/>
</div>
<div class="lyrico-lyrics-wrapper">Thane thota maale dhare cherey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thane thota maale dhare cherey"/>
</div>
<div class="lyrico-lyrics-wrapper">Velugu needalle kalisey sayantram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velugu needalle kalisey sayantram"/>
</div>
<div class="lyrico-lyrics-wrapper">Rangey lekunda saage chadarangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangey lekunda saage chadarangam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandhramlo nadhila jantavvalantu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhramlo nadhila jantavvalantu"/>
</div>
<div class="lyrico-lyrics-wrapper">raasaro ledo aa devudu gaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="raasaro ledo aa devudu gaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharagathi gadhi dhaati tharalina kathaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharagathi gadhi dhaati tharalina kathaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Teliyani teguvedho cheerey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teliyani teguvedho cheerey"/>
</div>
<div class="lyrico-lyrics-wrapper">Adugulu padaleni tholi thapanalaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adugulu padaleni tholi thapanalaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Iruvuri mohamaataale dhuram poye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruvuri mohamaataale dhuram poye"/>
</div>
</pre>
