---
title: "kamala song lyrics"
album: "Sangathamizhan"
artist: "	Vivek - Mervin"
lyricist: "Ku. Karthik"
director: "Vijay Chandar"
path: "/albums/sangathamizhan-lyrics"
song: "Kamala"
image: ../../images/albumart/sangathamizhan.jpg
date: 2019-11-15
lang: tamil
youtubeLink: "https://www.youtube.com/embed/QobS-iBjKHQ"
type: "happy"
singers:
  - Vivek Siva
  - Sanjana Kalmanje
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kamala Kalaasaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kamala Kalaasaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Apuna Apuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Apuna Apuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayyayo Galatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyayo Galatta"/>
</div>
<div class="lyrico-lyrics-wrapper">Appuna Kamalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appuna Kamalaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kamala Kalaasaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kamala Kalaasaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Apuna Apuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Apuna Apuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayyayo Galatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyayo Galatta"/>
</div>
<div class="lyrico-lyrics-wrapper">Appuna Kamalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appuna Kamalaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kamala Kalaasaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kamala Kalaasaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Apuna Mere Takkaru Sundhariyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Apuna Mere Takkaru Sundhariyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Samujha Pannikkama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samujha Pannikkama"/>
</div>
<div class="lyrico-lyrics-wrapper">Gammunu Nikkuriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gammunu Nikkuriyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kamala Kalaasaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kamala Kalaasaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Apunnaa Mere Takkaru Sundhariyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Apunnaa Mere Takkaru Sundhariyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Samujha Pannikkama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samujha Pannikkama"/>
</div>
<div class="lyrico-lyrics-wrapper">Gammunu Nikkuriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gammunu Nikkuriyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yengayo Vikkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yengayo Vikkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yennamo Sikkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yennamo Sikkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kitta Nee Vandhuta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kitta Nee Vandhuta"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhedho Kolaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhedho Kolaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Status-u Single-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Status-u Single-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Ready To Mingle-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ready To Mingle-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Love-u Nu Vandhuta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love-u Nu Vandhuta"/>
</div>
<div class="lyrico-lyrics-wrapper">Full Time-u Nee Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Full Time-u Nee Paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kamala Kamala Kamala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kamala Kamala Kamala"/>
</div>
<div class="lyrico-lyrics-wrapper">Kamala Kamala Kamala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kamala Kamala Kamala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kamala Kalaasaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kamala Kalaasaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Apuna Mere Takkaru Sundhariyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Apuna Mere Takkaru Sundhariyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Samujha Pannikkama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samujha Pannikkama"/>
</div>
<div class="lyrico-lyrics-wrapper">Gammunu Nikkuriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gammunu Nikkuriyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kamala Kalaasaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kamala Kalaasaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Apuna Mere Takkaru Sundhariyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Apuna Mere Takkaru Sundhariyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Samujha Pannikkama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samujha Pannikkama"/>
</div>
<div class="lyrico-lyrics-wrapper">Gammunu Nikkuriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gammunu Nikkuriyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pathikkucha Pathikkucha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathikkucha Pathikkucha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjula Fire-u Fire-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjula Fire-u Fire-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Vechirukka Vechirukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vechirukka Vechirukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannula Current-u Wire-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannula Current-u Wire-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pathikkucha Pathikkucha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathikkucha Pathikkucha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjula Fire-u Fire-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjula Fire-u Fire-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Vechirukka Vechirukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vechirukka Vechirukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannula Current-u Wire-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannula Current-u Wire-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Current-u Kambi Mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Current-u Kambi Mela"/>
</div>
<div class="lyrico-lyrics-wrapper">Thothi Nikkum Maina Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thothi Nikkum Maina Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Heart-a Odhara Vechaayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heart-a Odhara Vechaayae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Morattu Single Mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Morattu Single Mela"/>
</div>
<div class="lyrico-lyrics-wrapper">Love Rocket-a Yevi Vuttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love Rocket-a Yevi Vuttu"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasa Kadhara Vutaayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasa Kadhara Vutaayae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa I Mac-u Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa I Mac-u Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee High Volt-u Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee High Volt-u Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulla Power Ethadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulla Power Ethadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenja Terror Akathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenja Terror Akathaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Eye Ball-u La
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Eye Ball-u La"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Game Aadura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Game Aadura"/>
</div>
<div class="lyrico-lyrics-wrapper">En Brain-u Kulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Brain-u Kulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Yeppodhum Thagaraaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Yeppodhum Thagaraaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kamala Kalaasaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kamala Kalaasaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Apuna Apuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Apuna Apuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayyayo Galatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyayo Galatta"/>
</div>
<div class="lyrico-lyrics-wrapper">Appuna Kamalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appuna Kamalaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kamala Kalaasaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kamala Kalaasaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Apuna Apuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Apuna Apuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayyayo Galatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyayo Galatta"/>
</div>
<div class="lyrico-lyrics-wrapper">Appuna Kamalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appuna Kamalaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kamala Kalaasaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kamala Kalaasaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Apunnaa Mere Takkaru Sundhariyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Apunnaa Mere Takkaru Sundhariyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Samujha Pannikkama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samujha Pannikkama"/>
</div>
<div class="lyrico-lyrics-wrapper">Gammunu Nikkuriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gammunu Nikkuriyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kamala Kalaasaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kamala Kalaasaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Apunnaa Mere Takkaru Sundhariyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Apunnaa Mere Takkaru Sundhariyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Samujha Pannikkama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samujha Pannikkama"/>
</div>
<div class="lyrico-lyrics-wrapper">Gammunu Nikkuriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gammunu Nikkuriyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pathikkucha Pathikkucha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathikkucha Pathikkucha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjula Fire-u Fire-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjula Fire-u Fire-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Vechirukka Vechirukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vechirukka Vechirukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannula Current-u Wire-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannula Current-u Wire-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pathikkucha Pathikkucha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathikkucha Pathikkucha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjula Fire-u Fire-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjula Fire-u Fire-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Vechirukka Vechirukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vechirukka Vechirukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannula Current-u Wire-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannula Current-u Wire-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kamala Kamala Kamala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kamala Kamala Kamala"/>
</div>
</pre>
