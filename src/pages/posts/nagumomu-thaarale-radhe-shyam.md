---
title: "nagumomu thaarale song lyrics"
album: "Radhe Shyam"
artist: "Justin Prabhakaran"
lyricist: "Krishna Kanth"
director: "Radha Krishna Kumar"
path: "/albums/radhe-shyam-lyrics"
song: "Nagumomu Thaarale"
image: ../../images/albumart/radhe-shyam.jpg
date: 2022-01-14
lang: telugu
youtubeLink: "https://www.youtube.com/embed/DCRqeCH21Qo"
type: "love"
singers:
  - Sid Sriram
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nagumomu thaarale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nagumomu thaarale"/>
</div>
<div class="lyrico-lyrics-wrapper">Thegi raale nelaake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thegi raale nelaake"/>
</div>
<div class="lyrico-lyrics-wrapper">Okataithe meerila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okataithe meerila"/>
</div>
<div class="lyrico-lyrics-wrapper">Choodalane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choodalane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sagamaye prayame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sagamaye prayame"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhilenu paadhame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhilenu paadhame"/>
</div>
<div class="lyrico-lyrics-wrapper">Padasaage praaname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padasaage praaname"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana venake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana venake"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mohaalane meerenthala ilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mohaalane meerenthala ilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Momaatame ika veedenule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Momaatame ika veedenule"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ippude ekam ayye ee radhe shyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippude ekam ayye ee radhe shyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Iddharo lokam ayye ee radhe shyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iddharo lokam ayye ee radhe shyam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhaladame marichenuga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhaladame marichenuga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalalu ninne choosi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalalu ninne choosi"/>
</div>
<div class="lyrico-lyrics-wrapper">Anakuvaga nilichenuga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anakuvaga nilichenuga"/>
</div>
<div class="lyrico-lyrics-wrapper">Vegaalu thaalalesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vegaalu thaalalesi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hechataku yemo teliyaduga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hechataku yemo teliyaduga"/>
</div>
<div class="lyrico-lyrics-wrapper">Adaganeleni chelimidiga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adaganeleni chelimidiga"/>
</div>
<div class="lyrico-lyrics-wrapper">Pedavulakemo adhe paniga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pedavulakemo adhe paniga"/>
</div>
<div class="lyrico-lyrics-wrapper">Nimishamu leve vidividiga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nimishamu leve vidividiga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Samayalake selave ikaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samayalake selave ikaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Perulenidhi prema kaanidhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perulenidhi prema kaanidhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo kathe idhe kadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo kathe idhe kadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ippude ekam ayye ee radhe shyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippude ekam ayye ee radhe shyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Iddharo lokam ayye ee radhe shyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iddharo lokam ayye ee radhe shyam"/>
</div>
</pre>
