---
title: "aaliyah aaliyah un munne song lyrics"
album: "Dagaalty"
artist: "Vijay Narain"
lyricist: "Madhan Karky"
director: "Vijay Anand"
path: "/albums/dagaalty-song-lyrics"
song: "Aaliyah Aaliyah Un Munne"
image: ../../images/albumart/dagaalty.jpg
date: 2020-01-31
lang: tamil
youtubeLink: "https://www.youtube.com/embed/VapesRRVr20"
type: "Item Song"
singers:
  - Harshitha Krishnan
  - Gowtham Bharadwaj
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aaliyah Aaliyah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaliyah Aaliyah"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Munne Vandhaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Munne Vandhaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Then Mutham Venaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Then Mutham Venaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannadi Kannaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannadi Kannaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaliyah Aaliyah Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaliyah Aaliyah Oh"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannadi Kannaala Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannadi Kannaala Oh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pala Pala Heartah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala Pala Heartah"/>
</div>
<div class="lyrico-lyrics-wrapper">Odachi Pekkurenda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odachi Pekkurenda"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Heartah Kunjom Thaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Heartah Kunjom Thaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Chellama Thadavi Paarthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chellama Thadavi Paarthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chill Aakkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chill Aakkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Udaikaama Kiss Pannura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udaikaama Kiss Pannura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Heyyaa Aaliyah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyyaa Aaliyah"/>
</div>
<div class="lyrico-lyrics-wrapper">Hyp Hypnotic Theeyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hyp Hypnotic Theeyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Hypnotic Theeyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Hypnotic Theeyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaliyah Aaliyah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaliyah Aaliyah"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Munne Vandhaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Munne Vandhaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Then Mutham Venaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Then Mutham Venaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannadi Kannaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannadi Kannaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaliyah Aaliyah Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaliyah Aaliyah Oh"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannadi Kannaala Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannadi Kannaala Oh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nalla Pannum Unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla Pannum Unna"/>
</div>
<div class="lyrico-lyrics-wrapper">Free-ah Pannadhennu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Free-ah Pannadhennu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sonnadhu Jokeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sonnadhu Jokeru"/>
</div>
<div class="lyrico-lyrics-wrapper">Batman Ellaam Zero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Batman Ellaam Zero"/>
</div>
<div class="lyrico-lyrics-wrapper">Joker Dhaan En Hero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Joker Dhaan En Hero"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaasu Dhaan Matteru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasu Dhaan Matteru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhenam Dhenam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhenam Dhenam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu Oru Vesham Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu Oru Vesham Dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Odurom Odurom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odurom Odurom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veshamellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veshamellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalachittu Vaariyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalachittu Vaariyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaru Nee Theduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaru Nee Theduvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pala Pala Vesham Kalanchitte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala Pala Vesham Kalanchitte"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa Pakkam Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Pakkam Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Aadai Neekka Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Aadai Neekka Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thangama Orasi Paakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangama Orasi Paakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Nenja Thaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Nenja Thaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Uruthama Tharam Paakkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uruthama Tharam Paakkuren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Heyyaa Aaliyah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyyaa Aaliyah"/>
</div>
<div class="lyrico-lyrics-wrapper">Hyp Hypnotic Theeyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hyp Hypnotic Theeyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Hypnotic Theeyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Hypnotic Theeyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Hypnotic Theeyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Hypnotic Theeyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Hypnotic Theeyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Hypnotic Theeyaa"/>
</div>
</pre>
