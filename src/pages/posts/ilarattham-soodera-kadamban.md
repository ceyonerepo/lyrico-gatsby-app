---
title: "ilarattham soodera song lyrics"
album: "Kadamban"
artist: "Yuvan Shankar Raja"
lyricist: "Yugabharathi"
director: "Ragava"
path: "/albums/kadamban-lyrics"
song: "Ilarattham Soodera"
image: ../../images/albumart/kadamban.jpg
date: 2017-04-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/F5HY8jI5D1M"
type: "mass"
singers:
  -	M L R Karthikeyan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ila ratham soodera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ila ratham soodera"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhesai ettum thoolaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhesai ettum thoolaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagai illai illai kaigal sera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagai illai illai kaigal sera"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru yuddham eedera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru yuddham eedera"/>
</div>
<div class="lyrico-lyrics-wrapper">Bayam illai poraada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bayam illai poraada"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidi velli enghal perai koora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidi velli enghal perai koora"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethiraali yaarena nangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethiraali yaarena nangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Arivomae manmele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arivomae manmele"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru pothum tholvigal illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru pothum tholvigal illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadapomae munnalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadapomae munnalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ila ratham soodera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ila ratham soodera"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhesai ettum thoolaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhesai ettum thoolaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagai illai illai kaigal sera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagai illai illai kaigal sera"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru yuddham eedera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru yuddham eedera"/>
</div>
<div class="lyrico-lyrics-wrapper">Bayam illai poraada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bayam illai poraada"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidi velli enghal perai koora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidi velli enghal perai koora"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ohh kanneer enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohh kanneer enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanneer enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneer enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendil ondrai indrae seivom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendil ondrai indrae seivom"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Engae engae kuttram engae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engae engae kuttram engae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedamal theerathae sogangalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedamal theerathae sogangalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pollatha paathaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pollatha paathaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogum kaalagalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogum kaalagalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeyilae vaatuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeyilae vaatuvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Sellathu kaasu pol emmai aakiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sellathu kaasu pol emmai aakiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Soozhalai maatruvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soozhalai maatruvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaiyai meeruvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaiyai meeruvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Innumae seeruvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innumae seeruvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Naangalum yaarena kaatuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naangalum yaarena kaatuvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ila ratham soodera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ila ratham soodera"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhesai ettum thoolaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhesai ettum thoolaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagai illai illai kaigal sera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagai illai illai kaigal sera"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru yuddham eedera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru yuddham eedera"/>
</div>
<div class="lyrico-lyrics-wrapper">Bayam illai poraada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bayam illai poraada"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidi velli enghal perai koora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidi velli enghal perai koora"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hmm ellorukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm ellorukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellam ingae sonthama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellam ingae sonthama"/>
</div>
<div class="lyrico-lyrics-wrapper">Sevvanam thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sevvanam thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Mul velikkul thanguma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mul velikkul thanguma"/>
</div>
<div class="lyrico-lyrics-wrapper">Annai thanthai endrum engal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annai thanthai endrum engal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadendru vazhnthomae neengamalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadendru vazhnthomae neengamalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ponnana kaadithai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponnana kaadithai"/>
</div>
<div class="lyrico-lyrics-wrapper">Sooraiyadinal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sooraiyadinal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vettaiyum aaduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vettaiyum aaduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennalum veli pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennalum veli pol"/>
</div>
<div class="lyrico-lyrics-wrapper">Naangal engalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naangal engalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaavalai poduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaavalai poduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Aayutham thookkuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayutham thookkuvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Vethanai pokkuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vethanai pokkuvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetriyin uchiyil yeruvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetriyin uchiyil yeruvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ila ratham soodera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ila ratham soodera"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhesai ettum thoolaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhesai ettum thoolaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagai illai illai kaigal sera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagai illai illai kaigal sera"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru yuddham eedera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru yuddham eedera"/>
</div>
<div class="lyrico-lyrics-wrapper">Bayam illai poraada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bayam illai poraada"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidi velli enghal perai koora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidi velli enghal perai koora"/>
</div>
</pre>
