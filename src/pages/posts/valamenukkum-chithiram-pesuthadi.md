---
title: "valamenukkum song lyrics"
album: "Chithiram Pesuthadi"
artist: "Sundar C Babu"
lyricist: "Gana Ulaganathan"
director: "Mysskin"
path: "/albums/chithiram-pesuthadi-lyrics"
song: "Valamenukkum"
image: ../../images/albumart/chithiram-pesuthadi.jpg
date: 2006-02-10
lang: tamil
youtubeLink: "https://www.youtube.com/embed/62Wxy3hpkes"
type: "happy"
singers:
  - Gana Ulaganathan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vaalai Meenukum Vilangu Meenukum Kalyaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaalai Meenukum Vilangu Meenukum Kalyaanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha Sennakuni Kootamellam Oorkolam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Sennakuni Kootamellam Oorkolam"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha Nadukadalil Nadakuthaiya Thirumanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Nadukadalil Nadakuthaiya Thirumanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha Asurakodi Aalukellam Kummaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Asurakodi Aalukellam Kummaalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalyaanamaam Kalyaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalyaanamaam Kalyaanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalyaanamaam Kalyaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalyaanamaam Kalyaanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalyaanamaam Kalyaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalyaanamaam Kalyaanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalyaanamaam Kalyaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalyaanamaam Kalyaanam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaalai Meenukum Vilangu Meenukum Kalyaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaalai Meenukum Vilangu Meenukum Kalyaanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha Sennakuni Kootamellam Oorkolam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Sennakuni Kootamellam Oorkolam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oorvalathil Aadi Varum Nandu Thaane Naatiyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorvalathil Aadi Varum Nandu Thaane Naatiyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye Mele Thaalam Muzhangi Varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Mele Thaalam Muzhangi Varum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanjira Meenu Vaathiyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanjira Meenu Vaathiyam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oorvalathil Aadi Varum Nandu Thaane Naatiyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorvalathil Aadi Varum Nandu Thaane Naatiyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye Mele Thaalam Muzhangi Varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Mele Thaalam Muzhangi Varum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanjira Meenu Vaathiyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanjira Meenu Vaathiyam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paarai Meenu Nadathi Varraar Partyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarai Meenu Nadathi Varraar Partyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Namba Paarai Meenu Nadathi Varraar Partyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namba Paarai Meenu Nadathi Varraar Partyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Angu Ther Pole Poguthaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Angu Ther Pole Poguthaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Oor Kola Kaatchiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oor Kola Kaatchiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oor Kola Kaatchiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oor Kola Kaatchiyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaalai Meenukum Vilangu Meenukum Kalyaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaalai Meenukum Vilangu Meenukum Kalyaanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha Sennakuni Kootamellam Oorkolam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Sennakuni Kootamellam Oorkolam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koovam Aaru Kadalil Serum Andha Idathil Lovevunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koovam Aaru Kadalil Serum Andha Idathil Lovevunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Idha Paarthu Vitta Uluva Meenu Vachathaiya Vattingo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idha Paarthu Vitta Uluva Meenu Vachathaiya Vattingo"/>
</div>
<div class="lyrico-lyrics-wrapper">Koovam Aaru Kadalil Serum Andha Idathil Lovevunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koovam Aaru Kadalil Serum Andha Idathil Lovevunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Idha Paarthu Vitta Uluva Meenu Vachathaiya Vattingo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idha Paarthu Vitta Uluva Meenu Vachathaiya Vattingo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Panchaayathu Thalaivaraana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panchaayathu Thalaivaraana"/>
</div>
<div class="lyrico-lyrics-wrapper">Suraa Meenu Thaanungo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suraa Meenu Thaanungo"/>
</div>
<div class="lyrico-lyrics-wrapper">Panchaayathu Thalaivaraana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panchaayathu Thalaivaraana"/>
</div>
<div class="lyrico-lyrics-wrapper">Suraa Meenu Thaanungo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suraa Meenu Thaanungo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avar Sonnapadi Iruvarukum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avar Sonnapadi Iruvarukum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nichayathaartham Thaanungo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nichayathaartham Thaanungo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalyaanam Nadanthu Varudhu Paarungo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalyaanam Nadanthu Varudhu Paarungo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaalai Meenukum Vilangu Meenukum Kalyaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaalai Meenukum Vilangu Meenukum Kalyaanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha Sennakuni Kootamellam Oorkolam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Sennakuni Kootamellam Oorkolam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maapilla Sondha Bandham Meesakaara Yeraango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maapilla Sondha Bandham Meesakaara Yeraango"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha Nethili Podiyum Kaara Podiyum Kalakalanu Irukudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Nethili Podiyum Kaara Podiyum Kalakalanu Irukudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maapilla Sondha Bandham Meesakaara Yeraango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maapilla Sondha Bandham Meesakaara Yeraango"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha Nethili Podiyum Kaara Podiyum Kalakalanu Irukudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Nethili Podiyum Kaara Podiyum Kalakalanu Irukudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pennuku Sondha Bandham Meesa Kaara Kadumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pennuku Sondha Bandham Meesa Kaara Kadumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pennuku Sondha Bandham Meesa Kaara Kadumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pennuku Sondha Bandham Meesa Kaara Kadumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha Sankara Meenum Vavvalu Meenum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Sankara Meenum Vavvalu Meenum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vara Vazhaipa Tharugudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vara Vazhaipa Tharugudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vara Vazhaipa Tharugudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vara Vazhaipa Tharugudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaalai Meenukum Vilangu Meenukum Kalyaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaalai Meenukum Vilangu Meenukum Kalyaanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha Sennakuni Kootamellam Oorkolam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Sennakuni Kootamellam Oorkolam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maapilla Vaalai Meenu Pazhavarkaadu Thaanungo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maapilla Vaalai Meenu Pazhavarkaadu Thaanungo"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha Manaponnu Vilangu Meenu Meenjooru Thaanungo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Manaponnu Vilangu Meenu Meenjooru Thaanungo"/>
</div>
<div class="lyrico-lyrics-wrapper">Maapilla Vaalai Meenu Pazhavarkaadu Thaanungo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maapilla Vaalai Meenu Pazhavarkaadu Thaanungo"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha Manaponnu Vilangu Meenu Meenjooru Thaanungo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Manaponnu Vilangu Meenu Meenjooru Thaanungo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha Thirumanathai Nadathi Vaikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Thirumanathai Nadathi Vaikum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirukaamalu Annango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirukaamalu Annango"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha Thirumanathai Nadathi Vaikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Thirumanathai Nadathi Vaikum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirukaamalu Annango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirukaamalu Annango"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha Manamakkalai Vaazhthukindra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Manamakkalai Vaazhthukindra"/>
</div>
<div class="lyrico-lyrics-wrapper">Periya Manusan Yaarungo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Periya Manusan Yaarungo"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalaivaru Thimingalam Thaanungo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaivaru Thimingalam Thaanungo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaalai Meenukum Vilangu Meenukum Kalyaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaalai Meenukum Vilangu Meenukum Kalyaanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha Sennakuni Kootamellam Oorkolam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Sennakuni Kootamellam Oorkolam"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha Nadukadalil Nadakuthaiya Thirumanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Nadukadalil Nadakuthaiya Thirumanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha Asurakodi Aalukellam Kummaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Asurakodi Aalukellam Kummaalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaala Meenukum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaala Meenukum"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha Sennakuni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Sennakuni"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadukadalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadukadalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha Asurakodi Aalukellam Kummaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Asurakodi Aalukellam Kummaalam"/>
</div>
</pre>
