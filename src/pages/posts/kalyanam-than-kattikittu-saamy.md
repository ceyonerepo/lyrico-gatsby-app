---
title: "kalyanam than kattikittu song lyrics"
album: "Saamy"
artist: "Harris Jayaraj "
lyricist: "Snehan"
director: "Hari"
path: "/albums/saamy-song-lyrics"
song: "Kalyanam Than Kattikittu"
image: ../../images/albumart/saamy.jpg
date: 2003-05-01
lang: tamil
youtubeLink: "https://www.youtube.com/embed/PbSvVe8XPQk"
type: "Love"
singers:
  - KK
  - Yugendran
  - Srilekha Parthasarathy
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kalyanam Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalyanam Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattikitu Odi Polama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattikitu Odi Polama"/>
</div>
<div class="lyrico-lyrics-wrapper">Illa Odi Poyi Kalyanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illa Odi Poyi Kalyanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaan Katti Kalamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaan Katti Kalamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalyanam Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalyanam Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattikitu Odi Polama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattikitu Odi Polama"/>
</div>
<div class="lyrico-lyrics-wrapper">Illa Odi Poyi Kalyanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illa Odi Poyi Kalyanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaan Katti Kalamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaan Katti Kalamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaliya Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaliya Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattikitu Pethukalama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattikitu Pethukalama"/>
</div>
<div class="lyrico-lyrics-wrapper">Illa Pulla Kutti Pethukitu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illa Pulla Kutti Pethukitu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattikalama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattikalama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sambirani Vaasathila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sambirani Vaasathila"/>
</div>
<div class="lyrico-lyrics-wrapper">Valarntha Siriki Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valarntha Siriki Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Sagunam Paarthu Sadangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sagunam Paarthu Sadangu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarthu Siricha Kiruki Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarthu Siricha Kiruki Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Samikellam Sagunam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samikellam Sagunam"/>
</div>
<div class="lyrico-lyrics-wrapper">Yethum Thevaiyillaye Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethum Thevaiyillaye Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Saaku Poku Solvathellam Niyamillaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saaku Poku Solvathellam Niyamillaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adiye Kalyanam Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiye Kalyanam Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattikitu Odi Polama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattikitu Odi Polama"/>
</div>
<div class="lyrico-lyrics-wrapper">Illa Odi Poyi Kalyanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illa Odi Poyi Kalyanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaan Katti Kalamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaan Katti Kalamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaliya Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaliya Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattikitu Pethukalama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattikitu Pethukalama"/>
</div>
<div class="lyrico-lyrics-wrapper">Illa Pulla Kutti Pethukitu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illa Pulla Kutti Pethukitu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattikalama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattikalama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rusiya Pesura Rusiya Paarkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rusiya Pesura Rusiya Paarkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Rusiya Samayala Seiviya Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rusiya Samayala Seiviya Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pasum Paal Neiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pasum Paal Neiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuvaram Parupa Kadanju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuvaram Parupa Kadanju"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalichu Kodukatuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalichu Kodukatuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pathiya Sora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathiya Sora"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Unna Keten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Unna Keten"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaaram Saarama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaram Saarama"/>
</div>
<div class="lyrico-lyrics-wrapper">Unaku Samaika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unaku Samaika"/>
</div>
<div class="lyrico-lyrics-wrapper">Theriyuma Yamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theriyuma Yamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Milagula Rasama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Milagula Rasama"/>
</div>
<div class="lyrico-lyrics-wrapper">Molaga Thokka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Molaga Thokka"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalla Seiyuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla Seiyuven"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Saapitu Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Saapitu Paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nandu Varuka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nandu Varuka"/>
</div>
<div class="lyrico-lyrics-wrapper">Theriyuma Kozhi Porika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theriyuma Kozhi Porika"/>
</div>
<div class="lyrico-lyrics-wrapper">Theriyuma Aatu Kaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theriyuma Aatu Kaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Nasuki Potu Soopu Vaika Theriyuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nasuki Potu Soopu Vaika Theriyuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Idupu Orama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Idupu Orama"/>
</div>
<div class="lyrico-lyrics-wrapper">Irukuthaiya Karama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irukuthaiya Karama"/>
</div>
<div class="lyrico-lyrics-wrapper">Kandu Neeyum Pudichuta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandu Neeyum Pudichuta"/>
</div>
<div class="lyrico-lyrics-wrapper">Eduthukaya Thaaralama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eduthukaya Thaaralama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalyanam Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalyanam Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattikitu Odi Polama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattikitu Odi Polama"/>
</div>
<div class="lyrico-lyrics-wrapper">Illa Odi Poyi Kalyanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illa Odi Poyi Kalyanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaan Katti Kalamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaan Katti Kalamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaliya Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaliya Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattikitu Pethukalama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattikitu Pethukalama"/>
</div>
<div class="lyrico-lyrics-wrapper">Illa Pulla Kutti Pethukitu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illa Pulla Kutti Pethukitu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattikalama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattikalama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ooru Orama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooru Orama"/>
</div>
<div class="lyrico-lyrics-wrapper">Aiyanaar Pole Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyanaar Pole Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Meesaiya Kaati Meraturiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meesaiya Kaati Meraturiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Kunchi Porichidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Kunchi Porichidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kozhiya Pole Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kozhiya Pole Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Veliya Thaanda Anjuriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veliya Thaanda Anjuriye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thethiya Vachuthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thethiya Vachuthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Paakaiyum Mathunaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paakaiyum Mathunaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thethiya Vachuthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thethiya Vachuthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Paakaiyum Mathunaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paakaiyum Mathunaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaiya Thaanduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaiya Thaanduven"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Ishtam Pole Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Ishtam Pole Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Panguni Varatum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panguni Varatum"/>
</div>
<div class="lyrico-lyrics-wrapper">Parisam Thaaren Athuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parisam Thaaren Athuku"/>
</div>
<div class="lyrico-lyrics-wrapper">Munnala Konjam Adjust Pannudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munnala Konjam Adjust Pannudi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manja Poosi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manja Poosi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kulichuta Manasukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulichuta Manasukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Verkuthu Unna Naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verkuthu Unna Naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarthuta Udambu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarthuta Udambu"/>
</div>
<div class="lyrico-lyrics-wrapper">Muzhuka Koosuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muzhuka Koosuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Venthayatha Arachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venthayatha Arachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaan Thechu Vida Varatuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaan Thechu Vida Varatuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidiya Vidiya Unaku Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidiya Vidiya Unaku Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalaiyanaya Irukatuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaiyanaya Irukatuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalyanam Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalyanam Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattikitu Odi Polama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattikitu Odi Polama"/>
</div>
<div class="lyrico-lyrics-wrapper">Illa Odi Poyi Kalyanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illa Odi Poyi Kalyanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaan Katti Kalamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaan Katti Kalamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aa Aa Thaliya Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Aa Thaliya Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattikitu Pethukalama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattikitu Pethukalama"/>
</div>
<div class="lyrico-lyrics-wrapper">Illa Pulla Kutti Pethukitu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illa Pulla Kutti Pethukitu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattikalama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattikalama"/>
</div>
</pre>
