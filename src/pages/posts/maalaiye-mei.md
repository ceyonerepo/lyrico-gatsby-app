---
title: "maalaiye song lyrics"
album: "Mei"
artist: "Prithvi Kumar"
lyricist: "Christoper Pradeep"
director: "SA Baskaran"
path: "/albums/mei-lyrics"
song: "Maalaiye"
image: ../../images/albumart/mei.jpg
date: 2019-08-23
lang: tamil
youtubeLink: "https://www.youtube.com/embed/8H8ufYBHAJ8"
type: "love"
singers:
  - Prithvi Kumar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Maalaiyae Pon Maalaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maalaiyae Pon Maalaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Saalai Oram Pen Poo Saaralae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saalai Oram Pen Poo Saaralae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasaiyae Yen Aasaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasaiyae Yen Aasaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinam Dhorum Nee Pudhu Aasaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam Dhorum Nee Pudhu Aasaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaatrilae Swaasamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatrilae Swaasamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhalin Vaasamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalin Vaasamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Thedinen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Thedinen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naadagam Aayiram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naadagam Aayiram"/>
</div>
<div class="lyrico-lyrics-wrapper">Arangeridum Nayagan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arangeridum Nayagan"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kaadhalan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kaadhalan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maalaiyae Pon Maalaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maalaiyae Pon Maalaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Saalai Oram Pen Poo Saaralae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saalai Oram Pen Poo Saaralae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasaiyae Yen Aasaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasaiyae Yen Aasaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinam Dhorum Nee Pudhu Aasaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam Dhorum Nee Pudhu Aasaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ora Paarvai Paarkumpodhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ora Paarvai Paarkumpodhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kan Simitta Vaikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan Simitta Vaikum"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan Punnagaigalo Azhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan Punnagaigalo Azhagu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neram Sendrum Kaalam Sendrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neram Sendrum Kaalam Sendrum"/>
</div>
<div class="lyrico-lyrics-wrapper">Yennamgalin Thooral Sindum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yennamgalin Thooral Sindum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nyabagangalo Azhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nyabagangalo Azhagu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yedhedho Dhinam Dhinam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhedho Dhinam Dhinam"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeneno Maatrangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeneno Maatrangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar Seidha Thavarugalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar Seidha Thavarugalo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ini Nee Sellum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Nee Sellum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu Pudhu Paadhaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu Pudhu Paadhaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Yenge Serum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenge Serum"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar Kanda Kanavugalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar Kanda Kanavugalo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maalaiyae Pon Maalaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maalaiyae Pon Maalaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Saalai Oram Pen Poo Saaralae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saalai Oram Pen Poo Saaralae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasaiyae Yen Aasaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasaiyae Yen Aasaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinam Dhorum Nee Pudhu Aasaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam Dhorum Nee Pudhu Aasaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nedunaalaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nedunaalaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil Vaitha Kadhalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil Vaitha Kadhalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalli Pogathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalli Pogathey"/>
</div>
<div class="lyrico-lyrics-wrapper">Neengum Indha Kaayamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neengum Indha Kaayamae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unmai Sonnen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmai Sonnen"/>
</div>
<div class="lyrico-lyrics-wrapper">Valibam Odidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valibam Odidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanavil Theindhidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanavil Theindhidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadumaaridum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadumaaridum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhkaiyae Inbamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkaiyae Inbamo"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudhal Kaadhalae Thunbamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhal Kaadhalae Thunbamo"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Nyaayamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Nyaayamo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maalaiyae Pon Maalaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maalaiyae Pon Maalaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Saalai Oram Pen Poo Saaralae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saalai Oram Pen Poo Saaralae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasaiyae Yen Aasaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasaiyae Yen Aasaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinam Dhorum Nee Pudhu Aasaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam Dhorum Nee Pudhu Aasaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Yen Kaadhalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Yen Kaadhalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Yen Kaadhalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Yen Kaadhalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Yen Kaadhalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Yen Kaadhalae"/>
</div>
</pre>
