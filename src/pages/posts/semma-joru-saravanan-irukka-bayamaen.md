---
title: "semma joru song lyrics"
album: "Saravanan Irukka Bayamaen"
artist: "D Imman"
lyricist: "Yugabharathi"
director: "Ezhil"
path: "/albums/saravanan-irukka-bayamaen-lyrics"
song: "Semma Joru"
image: ../../images/albumart/saravanan-irukka-bayamaen.jpg
date: 2017-05-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Hf1YD_nbkAc"
type: "love"
singers:
  -	Vishal Dadlani
  - Maria Kavita Thomas
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Semma joru joru joru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Semma joru joru joru"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna nadandhuchu theriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna nadandhuchu theriyala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avan yaaru yaaru yaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan yaaru yaaru yaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollu theriyattum veliyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollu theriyattum veliyila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avan yaaru yaaru yaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan yaaru yaaru yaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollu theriyattum veliyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollu theriyattum veliyila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhagaaga nee maariya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaaga nee maariya"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaaranam kooradi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaranam kooradi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mana maalaiyai soodidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana maalaiyai soodidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Maamanum yaaradi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maamanum yaaradi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unai aasaiyodu serndhu vaazha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai aasaiyodu serndhu vaazha"/>
</div>
<div class="lyrico-lyrics-wrapper">Varum avan sugam pera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varum avan sugam pera"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Semma joru joru joru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Semma joru joru joru"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna nadandhuchu theriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna nadandhuchu theriyala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avan yaaru yaaru yaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan yaaru yaaru yaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollu theriyattum veliyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollu theriyattum veliyila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avan yaaru yaaru yaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan yaaru yaaru yaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollu theriyattum veliyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollu theriyattum veliyila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ippodhu nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippodhu nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan avan avan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan avan avan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kondadidum peru naladi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondadidum peru naladi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennalumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennalumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunaivarum avan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunaivarum avan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjodu nee kudiyeradi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjodu nee kudiyeradi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yarodum pesamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yarodum pesamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanam ennadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanam ennadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesamal ponalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesamal ponalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattum kannadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattum kannadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Polladha neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Polladha neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">AiiyodiKalyanam ana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="AiiyodiKalyanam ana"/>
</div>
<div class="lyrico-lyrics-wrapper">Pinnadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pinnadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pazhagiya emmai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pazhagiya emmai"/>
</div>
<div class="lyrico-lyrics-wrapper">Theriyalai endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theriyalai endru"/>
</div>
<div class="lyrico-lyrics-wrapper">Odhungi nee nadanthida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odhungi nee nadanthida"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Semma joru joru joru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Semma joru joru joru"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna nadandhuchu theriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna nadandhuchu theriyala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avan yaaru yaaru yaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan yaaru yaaru yaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollu theriyattum veliyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollu theriyattum veliyila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pennana nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pennana nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Valam nalam pera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valam nalam pera"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandhoshamae kuraiyadhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhoshamae kuraiyadhadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ellamumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellamumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thara varum avan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thara varum avan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannadipol therivanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannadipol therivanadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalalae kolangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalalae kolangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Pottal eppadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pottal eppadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannalan ketpaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannalan ketpaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal solladi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal solladi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Solladha neeyo killadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solladha neeyo killadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Seivayae settai ammadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seivayae settai ammadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avanadhu ullam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avanadhu ullam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini unadhillam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini unadhillam"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhai vida varam edhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai vida varam edhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Semma joru joru joru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Semma joru joru joru"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna nadandhuchu theriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna nadandhuchu theriyala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avan yaaru yaaru yaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan yaaru yaaru yaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollu theriyattum veliyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollu theriyattum veliyila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avan yaaru yaaru yaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan yaaru yaaru yaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollu theriyattum veliyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollu theriyattum veliyila"/>
</div>
</pre>
