---
title: "yaar da song lyrics"
album: "Utharavu Maharaja"
artist: "Naren Balakumar"
lyricist: "Na Muthukumar"
director: "Asif Kuraishi"
path: "/albums/utharavu-maharaja-lyrics"
song: "Yaar Da"
image: ../../images/albumart/utharavu-maharaja.jpg
date: 2018-11-16
lang: tamil
youtubeLink: 
type: "mass"
singers:
  - Diwakar
  - Naren Balakumar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">yaar ra yaar ra yaar ra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaar ra yaar ra yaar ra"/>
</div>
<div class="lyrico-lyrics-wrapper">nee yaar ra enakum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee yaar ra enakum "/>
</div>
<div class="lyrico-lyrics-wrapper">unakum thagaraara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unakum thagaraara"/>
</div>
<div class="lyrico-lyrics-wrapper">yaar ra yaar ra yaar ra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaar ra yaar ra yaar ra"/>
</div>
<div class="lyrico-lyrics-wrapper">nee yaar ra pona 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee yaar ra pona "/>
</div>
<div class="lyrico-lyrics-wrapper">jenmathin adivera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jenmathin adivera"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">yaar ra yaar ra yaar ra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaar ra yaar ra yaar ra"/>
</div>
<div class="lyrico-lyrics-wrapper">nee yaar ra enakum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee yaar ra enakum "/>
</div>
<div class="lyrico-lyrics-wrapper">unakum thagaraara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unakum thagaraara"/>
</div>
<div class="lyrico-lyrics-wrapper">yaar ra yaar ra yaar ra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaar ra yaar ra yaar ra"/>
</div>
<div class="lyrico-lyrics-wrapper">nee yaar ra pona 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee yaar ra pona "/>
</div>
<div class="lyrico-lyrics-wrapper">jenmathin adivera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jenmathin adivera"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kathukul irunthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathukul irunthu"/>
</div>
<div class="lyrico-lyrics-wrapper">kathum peya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathum peya"/>
</div>
<div class="lyrico-lyrics-wrapper">udambile irunthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="udambile irunthu"/>
</div>
<div class="lyrico-lyrics-wrapper">kollum noya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kollum noya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kathukul irunthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathukul irunthu"/>
</div>
<div class="lyrico-lyrics-wrapper">kathum peya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathum peya"/>
</div>
<div class="lyrico-lyrics-wrapper">udambile irunthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="udambile irunthu"/>
</div>
<div class="lyrico-lyrics-wrapper">kollum noya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kollum noya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ipadi torchar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ipadi torchar"/>
</div>
<div class="lyrico-lyrics-wrapper">seivathu yen yen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seivathu yen yen"/>
</div>
<div class="lyrico-lyrics-wrapper">haiyo haiyo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="haiyo haiyo "/>
</div>
<div class="lyrico-lyrics-wrapper">oodi poyen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oodi poyen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">hey ethane 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey ethane "/>
</div>
<div class="lyrico-lyrics-wrapper">odi pogave unnai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="odi pogave unnai"/>
</div>
<div class="lyrico-lyrics-wrapper">thedi vanthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thedi vanthen"/>
</div>
<div class="lyrico-lyrics-wrapper">un naadi narambukul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un naadi narambukul"/>
</div>
<div class="lyrico-lyrics-wrapper">ooduruvi unnai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooduruvi unnai"/>
</div>
<div class="lyrico-lyrics-wrapper">urukku ulaippen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="urukku ulaippen"/>
</div>
<div class="lyrico-lyrics-wrapper">nee ethisai sendralum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee ethisai sendralum"/>
</div>
<div class="lyrico-lyrics-wrapper">athisaiyil thondru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athisaiyil thondru "/>
</div>
<div class="lyrico-lyrics-wrapper">unnai vaati 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai vaati "/>
</div>
<div class="lyrico-lyrics-wrapper">vathaithiduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vathaithiduven"/>
</div>
<div class="lyrico-lyrics-wrapper">yoi yaar ra nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yoi yaar ra nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">rajathi raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rajathi raja"/>
</div>
<div class="lyrico-lyrics-wrapper">raaja kambeera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="raaja kambeera"/>
</div>
<div class="lyrico-lyrics-wrapper">raaja kula thilaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="raaja kula thilaga"/>
</div>
<div class="lyrico-lyrics-wrapper">raaja parakrama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="raaja parakrama"/>
</div>
<div class="lyrico-lyrics-wrapper">irandaam raja rajan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irandaam raja rajan"/>
</div>
<div class="lyrico-lyrics-wrapper">parak parak parak
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="parak parak parak"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vinnai kaati 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vinnai kaati "/>
</div>
<div class="lyrico-lyrics-wrapper">kaatril vittu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatril vittu "/>
</div>
<div class="lyrico-lyrics-wrapper">kanna moochi nadakirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanna moochi nadakirathe"/>
</div>
<div class="lyrico-lyrics-wrapper">ye kappam vangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ye kappam vangi"/>
</div>
<div class="lyrico-lyrics-wrapper">kadalil vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadalil vittu"/>
</div>
<div class="lyrico-lyrics-wrapper">palaivanathil kidakirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="palaivanathil kidakirathe"/>
</div>
<div class="lyrico-lyrics-wrapper">yennai sutru kaatril
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yennai sutru kaatril"/>
</div>
<div class="lyrico-lyrics-wrapper">kooda yetho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kooda yetho"/>
</div>
<div class="lyrico-lyrics-wrapper">marmam irukirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="marmam irukirathe"/>
</div>
<div class="lyrico-lyrics-wrapper">munne pinne vali 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munne pinne vali "/>
</div>
<div class="lyrico-lyrics-wrapper">illatha thadathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="illatha thadathil"/>
</div>
<div class="lyrico-lyrics-wrapper">kaalgal nadakirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaalgal nadakirathe"/>
</div>
<div class="lyrico-lyrics-wrapper">yennai sutru kaatril
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yennai sutru kaatril"/>
</div>
<div class="lyrico-lyrics-wrapper">kooda yetho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kooda yetho"/>
</div>
<div class="lyrico-lyrics-wrapper">marmam irukirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="marmam irukirathe"/>
</div>
<div class="lyrico-lyrics-wrapper">munne pinne vali 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munne pinne vali "/>
</div>
<div class="lyrico-lyrics-wrapper">illatha thadathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="illatha thadathil"/>
</div>
<div class="lyrico-lyrics-wrapper">kaalgal nadakirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaalgal nadakirathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">yei en adimaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yei en adimaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">enaku sevi saaikamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enaku sevi saaikamal"/>
</div>
<div class="lyrico-lyrics-wrapper">enna pithatri 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna pithatri "/>
</div>
<div class="lyrico-lyrics-wrapper">kondu irukiraari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kondu irukiraari"/>
</div>
<div class="lyrico-lyrics-wrapper">yov nan yar theriyuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yov nan yar theriyuma"/>
</div>
<div class="lyrico-lyrics-wrapper">nan evlo periya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan evlo periya"/>
</div>
<div class="lyrico-lyrics-wrapper">all theriyuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="all theriyuma"/>
</div>
<div class="lyrico-lyrics-wrapper">ha ha ha ha ha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ha ha ha ha ha "/>
</div>
<div class="lyrico-lyrics-wrapper">nee enna yemana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee enna yemana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ettanai podum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ettanai podum"/>
</div>
<div class="lyrico-lyrics-wrapper">raaja naan than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="raaja naan than"/>
</div>
<div class="lyrico-lyrics-wrapper">enake katalai podathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enake katalai podathe"/>
</div>
<div class="lyrico-lyrics-wrapper">etti pudikka oruvan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="etti pudikka oruvan"/>
</div>
<div class="lyrico-lyrics-wrapper">kaatu ennai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatu ennai "/>
</div>
<div class="lyrico-lyrics-wrapper">ippadi thurathathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ippadi thurathathe"/>
</div>
<div class="lyrico-lyrics-wrapper">mandaikul kodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mandaikul kodi"/>
</div>
<div class="lyrico-lyrics-wrapper">erimalai vaithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="erimalai vaithu"/>
</div>
<div class="lyrico-lyrics-wrapper">mavane neyum modathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mavane neyum modathe"/>
</div>
<div class="lyrico-lyrics-wrapper">ithanai valigal mothamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithanai valigal mothamai"/>
</div>
<div class="lyrico-lyrics-wrapper">vanthal haiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanthal haiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">ithayam thangathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithayam thangathe"/>
</div>
<div class="lyrico-lyrics-wrapper">haiyo ithayam thangathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="haiyo ithayam thangathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">yaar ra yaar ra yaar ra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaar ra yaar ra yaar ra"/>
</div>
<div class="lyrico-lyrics-wrapper">yaar ra yaar ra yaar ra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaar ra yaar ra yaar ra"/>
</div>
<div class="lyrico-lyrics-wrapper">enakum unakum thagaraara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enakum unakum thagaraara"/>
</div>
<div class="lyrico-lyrics-wrapper">yaar ra yaar ra yaar ra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaar ra yaar ra yaar ra"/>
</div>
<div class="lyrico-lyrics-wrapper">yaar ra yaar ra yaar ra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaar ra yaar ra yaar ra"/>
</div>
<div class="lyrico-lyrics-wrapper">pona jenmathin adivera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pona jenmathin adivera"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">yaar ra yaar ra yaar ra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaar ra yaar ra yaar ra"/>
</div>
<div class="lyrico-lyrics-wrapper">nee yaar ra enakum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee yaar ra enakum "/>
</div>
<div class="lyrico-lyrics-wrapper">unakum thagaraara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unakum thagaraara"/>
</div>
<div class="lyrico-lyrics-wrapper">yaar ra yaar ra yaar ra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaar ra yaar ra yaar ra"/>
</div>
<div class="lyrico-lyrics-wrapper">nee yaar ra pona 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee yaar ra pona "/>
</div>
<div class="lyrico-lyrics-wrapper">jenmathin adivera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jenmathin adivera"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">raaja maharaaja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="raaja maharaaja"/>
</div>
<div class="lyrico-lyrics-wrapper">yov enna vitruya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yov enna vitruya"/>
</div>
</pre>
