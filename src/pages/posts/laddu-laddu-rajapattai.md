---
title: "laddu laddu song lyrics"
album: "Rajapattai"
artist: "Yuvan Shankar Raja"
lyricist: "Yugabharathi"
director: "Suseenthiran"
path: "/albums/rajapattai-lyrics"
song: "Laddu Laddu"
image: ../../images/albumart/rajapattai.jpg
date: 2011-12-23
lang: tamil
youtubeLink: "https://www.youtube.com/embed/VLHQsswjJpk"
type: "love"
singers:
  - Vikram
  - Suchitra
  - Priyadarshini
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hey laddu laddu rendu laddu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey laddu laddu rendu laddu"/>
</div>
<div class="lyrico-lyrics-wrapper">Laddu laddu rendu laddu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laddu laddu rendu laddu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sernthu kedachaalae luckkae luckkae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sernthu kedachaalae luckkae luckkae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sukunaa suku malli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sukunaa suku malli"/>
</div>
<div class="lyrico-lyrics-wrapper">Sundarinaa dingaree coffee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sundarinaa dingaree coffee"/>
</div>
<div class="lyrico-lyrics-wrapper">Sooda kothikayilae soda ennathuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sooda kothikayilae soda ennathuku"/>
</div>
<div class="lyrico-lyrics-wrapper">Eduthu kudikava dhosthu dhosthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eduthu kudikava dhosthu dhosthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhuku ethuvum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhuku ethuvum"/>
</div>
<div class="lyrico-lyrics-wrapper">Illa vaasthu vaasthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illa vaasthu vaasthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuchinaa kuchi mittaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuchinaa kuchi mittaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kumarina panju mittaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kumarina panju mittaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Besha irukaiyilae pizza ennathuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Besha irukaiyilae pizza ennathuku"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha eduthuko tastetu tastetu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha eduthuko tastetu tastetu"/>
</div>
<div class="lyrico-lyrics-wrapper">Innum thayanguna wasteu wasteu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum thayanguna wasteu wasteu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Laddu laddu rendu laddu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laddu laddu rendu laddu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sernthu kedachaalae luckkae luckkae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sernthu kedachaalae luckkae luckkae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ottu ottu iruka ottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ottu ottu iruka ottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaari anachaalae kickku kickku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaari anachaalae kickku kickku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaa vadivel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaa vadivel"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanthanuku rendu peru jodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanthanuku rendu peru jodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Avanga appavukum ullathuthaan podi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avanga appavukum ullathuthaan podi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adiyae pombalaikum rendu veedu thaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiyae pombalaikum rendu veedu thaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhula enna kutham vaaya pothikodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhula enna kutham vaaya pothikodi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yea kaalu kai renduna theva theva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yea kaalu kai renduna theva theva"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattunava renduna bejaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattunava renduna bejaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Veedu vaasal renduna ok ok
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veedu vaasal renduna ok ok"/>
</div>
<div class="lyrico-lyrics-wrapper">Pondatinga rendu yen nee kooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pondatinga rendu yen nee kooru"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasai meerunaa alavae illayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai meerunaa alavae illayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhuku podura veli veli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhuku podura veli veli"/>
</div>
<div class="lyrico-lyrics-wrapper">Pambu venumaa yeni venumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pambu venumaa yeni venumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuluki podavaa soli soli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuluki podavaa soli soli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Laddu laddu rendu laddu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laddu laddu rendu laddu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sernthu kedachaalae luckkae luckkae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sernthu kedachaalae luckkae luckkae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ottu ottu iruka ottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ottu ottu iruka ottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaari anachaalae kickku kickku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaari anachaalae kickku kickku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pagalae illaiyinaa vanthiduma nightu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagalae illaiyinaa vanthiduma nightu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavala venamuna vachukanum sightu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavala venamuna vachukanum sightu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thavarae illaiyinaa onnum illa rightu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thavarae illaiyinaa onnum illa rightu"/>
</div>
<div class="lyrico-lyrics-wrapper">Jeika venumuna podanumae fightu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jeika venumuna podanumae fightu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sooriyana polae naan otha aalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sooriyana polae naan otha aalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vennilavu renduna aagadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vennilavu renduna aagadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaamaraiku thanni thaan kootu kootu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaamaraiku thanni thaan kootu kootu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanni vathi pochuna thaangadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanni vathi pochuna thaangadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ethayum thaanguven enna yethuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethayum thaanguven enna yethuko"/>
</div>
<div class="lyrico-lyrics-wrapper">Pootukaagavae saavi saavi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pootukaagavae saavi saavi"/>
</div>
<div class="lyrico-lyrics-wrapper">Koora paayava elava panjunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koora paayava elava panjunu"/>
</div>
<div class="lyrico-lyrics-wrapper">Izhuka paakura koovi koovi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Izhuka paakura koovi koovi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Laddu laddu rendu laddu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laddu laddu rendu laddu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sernthu kedachaalae luckkae luckkae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sernthu kedachaalae luckkae luckkae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ottu ottu iruka ottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ottu ottu iruka ottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaari anachaalae kickku kickku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaari anachaalae kickku kickku"/>
</div>
</pre>
