---
title: "enga anne song lyrics"
album: "namma veetu pillai"
artist: "D Imman"
lyricist: "vignesh shivan"
director: "pandiraj"
path: "/albums/namma-veetu-pillai-song-lyrics"
song: "enga anne"
image: ../../images/albumart/namma-veetu-pillai.jpg
date: 2019-09-27
lang: tamil
youtubeLink: "https://www.youtube.com/embed/cUowksRM7x0"
type: "brother sentiment"
singers:
  - Nakash Aziz
  - Sunidhi Chauhan
---

<pre class="lyrics-native">
</pre>
<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">En Thangathaan En Uyiru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Thangathaan En Uyiru"/>
</div>
<div class="lyrico-lyrics-wrapper">En Ulagame Athuthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Ulagame Athuthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu Siruchaathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu Siruchaathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum Sirippen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum Sirippen"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu Aluthaa  Aiyyaiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu Aluthaa  Aiyyaiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaala Thaangave Mudiyaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaala Thaangave Mudiyaathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Kanna Thoranthurukkom Pothella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Kanna Thoranthurukkom Pothella"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu En Munnalaiye Nikkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu En Munnalaiye Nikkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanna Moodi Iruntha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna Moodi Iruntha"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kanavula Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kanavula Kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Kala Kalanu Siruchu Velaiyadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kala Kalanu Siruchu Velaiyadum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa Vaa Dearu Brotheru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Vaa Dearu Brotheru"/>
</div>
<div class="lyrico-lyrics-wrapper">Paatha Setharum Sugaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paatha Setharum Sugaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Anne Oruthan Irunthaale Podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anne Oruthan Irunthaale Podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Athuve Thani Poweru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athuve Thani Poweru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enga Anne Enga Anne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Anne Enga Anne"/>
</div>
<div class="lyrico-lyrics-wrapper">Anba Alli Thelikkarathil Mannan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anba Alli Thelikkarathil Mannan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanga Paasathil Avanathan Aduchuka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanga Paasathil Avanathan Aduchuka"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorula Aale Kidaiyathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorula Aale Kidaiyathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yenga Annan Yenga Annan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenga Annan Yenga Annan"/>
</div>
<div class="lyrico-lyrics-wrapper">Anba Alli Thelikkarathil Mannan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anba Alli Thelikkarathil Mannan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanga Paasathil Avanathan Aduchuka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanga Paasathil Avanathan Aduchuka"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorula Aale Kidaiyathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorula Aale Kidaiyathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Veetu Thalaivi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Veetu Thalaivi"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha Jillavoda Azhagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Jillavoda Azhagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Annangaaran Andraadam Nanaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annangaaran Andraadam Nanaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbaana Aruvi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbaana Aruvi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen Thanga My Thanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Thanga My Thanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Vella Manasu Konda Nalla Thanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vella Manasu Konda Nalla Thanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Annan Paasathil Avalathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annan Paasathil Avalathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Adichukka Yaarume illaiye Inga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adichukka Yaarume illaiye Inga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen Aaramba Kaalathu Loveukellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Aaramba Kaalathu Loveukellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Anilaa Iruppaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anilaa Iruppaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Cricketu Aadaiyil Wicketu Ketta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Cricketu Aadaiyil Wicketu Ketta"/>
</div>
<div class="lyrico-lyrics-wrapper">Udane Eduppaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udane Eduppaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalakalanu Avan Iruppathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalakalanu Avan Iruppathum"/>
</div>
<div class="lyrico-lyrics-wrapper">Color Colora Avan Siripathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Color Colora Avan Siripathum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paathaale Podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paathaale Podhum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enga Anne Enga Anne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Anne Enga Anne"/>
</div>
<div class="lyrico-lyrics-wrapper">Anba Alli Thelikkarathil Mannan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anba Alli Thelikkarathil Mannan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanga Paasathil Avanathan Aduchuka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanga Paasathil Avanathan Aduchuka"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorula Aale Kidaiyathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorula Aale Kidaiyathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enga Anne Enga Anne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Anne Enga Anne"/>
</div>
<div class="lyrico-lyrics-wrapper">Anba Alli Thelikkarathil Mannan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anba Alli Thelikkarathil Mannan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanga Paasathil Avanathan Aduchuka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanga Paasathil Avanathan Aduchuka"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorula Aale Kidaiyathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorula Aale Kidaiyathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Thangathaan En Uyiru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Thangathaan En Uyiru"/>
</div>
<div class="lyrico-lyrics-wrapper">En Ulagame Athuthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Ulagame Athuthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">En Thangathaan En Uyiru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Thangathaan En Uyiru"/>
</div>
<div class="lyrico-lyrics-wrapper">En Ulagame Athuthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Ulagame Athuthaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unna Vida Enga Annanungathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Vida Enga Annanungathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku Mukkiyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku Mukkiyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Enakku Kodutha Vaazhkaiya Vida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Enakku Kodutha Vaazhkaiya Vida"/>
</div>
<div class="lyrico-lyrics-wrapper">En Annanungaloda Paasanthaanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Annanungaloda Paasanthaanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku Mukkiyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku Mukkiyam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adutha Jenmam Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adutha Jenmam Kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Annan Unakku Naanthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annan Unakku Naanthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Agreement Ah Pottu Vechukalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Agreement Ah Pottu Vechukalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inthan Jenmam Annan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inthan Jenmam Annan"/>
</div>
<div class="lyrico-lyrics-wrapper">Adutha Jenmam Appan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adutha Jenmam Appan"/>
</div>
<div class="lyrico-lyrics-wrapper">Maathi Maathi Poranthu Vaazhnthukalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maathi Maathi Poranthu Vaazhnthukalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sontha Bandha Paasamellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sontha Bandha Paasamellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanaa Pochu Enga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanaa Pochu Enga"/>
</div>
<div class="lyrico-lyrics-wrapper">Paasamalar Part 2 va Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paasamalar Part 2 va Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Paathukonga Inga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paathukonga Inga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kooda Poranthava Aasa Pattaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooda Poranthava Aasa Pattaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhoomiya Kooda Vaangi Thaadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhoomiya Kooda Vaangi Thaadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaivittu Sirikara Satham Ketta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaivittu Sirikara Satham Ketta"/>
</div>
<div class="lyrico-lyrics-wrapper">Veronum Venda Podhum Poda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veronum Venda Podhum Poda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadavl Vandhu Ketta Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadavl Vandhu Ketta Kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Tharamaatten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Tharamaatten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enga Anne Enga Anne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Anne Enga Anne"/>
</div>
<div class="lyrico-lyrics-wrapper">Anba Alli Thelikkarathil Mannan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anba Alli Thelikkarathil Mannan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanga Paasathil Avanathan Aduchuka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanga Paasathil Avanathan Aduchuka"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorula Aale Kidaiyathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorula Aale Kidaiyathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen Thanga My Thanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Thanga My Thanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Vella Manasu Konda Nalla Thanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vella Manasu Konda Nalla Thanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Annan Paasathil Avalathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annan Paasathil Avalathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Adichukka Yaarume illaiye Inga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adichukka Yaarume illaiye Inga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa Vaa Dearu Brotheru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Vaa Dearu Brotheru"/>
</div>
<div class="lyrico-lyrics-wrapper">Paatha Setharum Sugaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paatha Setharum Sugaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Anne Oruthan Irunthaale Podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anne Oruthan Irunthaale Podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Athuve Thani Poweru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athuve Thani Poweru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen Aaramba Kaalathu Loveukellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Aaramba Kaalathu Loveukellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Anilaa Iruppaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anilaa Iruppaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Cricketu Aadaiyil Wicketu Ketta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Cricketu Aadaiyil Wicketu Ketta"/>
</div>
<div class="lyrico-lyrics-wrapper">Udane Eduppaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udane Eduppaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalakalanu Avan Iruppathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalakalanu Avan Iruppathum"/>
</div>
<div class="lyrico-lyrics-wrapper">Color Colora Avan Siripathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Color Colora Avan Siripathum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paathaale Podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paathaale Podhum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enga Anne Enga Anne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Anne Enga Anne"/>
</div>
<div class="lyrico-lyrics-wrapper">Anba Alli Thelikkarathil Mannan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anba Alli Thelikkarathil Mannan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanga Paasathil Avanathan Aduchuka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanga Paasathil Avanathan Aduchuka"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorula Aale Kidaiyathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorula Aale Kidaiyathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enga Anne Enga Anne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Anne Enga Anne"/>
</div>
<div class="lyrico-lyrics-wrapper">Anba Alli Thelikkarathil Mannan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anba Alli Thelikkarathil Mannan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanga Paasathil Avanathan Aduchuka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanga Paasathil Avanathan Aduchuka"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorula Aale Kidaiyathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorula Aale Kidaiyathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Thangathaan En Uyiru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Thangathaan En Uyiru"/>
</div>
<div class="lyrico-lyrics-wrapper">En Ulagame Athuthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Ulagame Athuthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">En Thangathaan En Uyiru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Thangathaan En Uyiru"/>
</div>
<div class="lyrico-lyrics-wrapper">En Ulagame Athuthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Ulagame Athuthaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anne"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennada Thangai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennada Thangai"/>
</div>
</pre>
