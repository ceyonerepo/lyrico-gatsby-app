---
title: "va va en thalaiva song lyrics"
album: "Sandhitha Velai"
artist: "Deva"
lyricist: "Vairamuthu"
director: "Ravichandran"
path: "/albums/sandhitha-velai-lyrics"
song: "Va Va En Thalaiva"
image: ../../images/albumart/sandhitha-velai.jpg
date: 2000-04-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/rn542qP_93c"
type: "love"
singers:
  - Harini
  - P. Unnikrishnan
  - Mahanadi Shobana
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vaa vaa en thalaiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa vaa en thalaiva"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhuvidu en thalaiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhuvidu en thalaiva"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa vaa en thalaiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa vaa en thalaiva"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalaianaiyai pangidavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaianaiyai pangidavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Malligayin madalukullae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malligayin madalukullae"/>
</div>
<div class="lyrico-lyrics-wrapper">Marmakadhaidhaan irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marmakadhaidhaan irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Marmakadhai padipadharkku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marmakadhai padipadharkku"/>
</div>
<div class="lyrico-lyrics-wrapper">Iruttukullum vazhi irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruttukullum vazhi irukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poovae un kadhavugal ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovae un kadhavugal ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Poottithaan kidakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poottithaan kidakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Pathu viral saavi ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathu viral saavi ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">En vasam irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En vasam irukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa vaa en thalaiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa vaa en thalaiva"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhuvidu en thalaiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhuvidu en thalaiva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjil oru thuli idamillaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil oru thuli idamillaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyae vazhangida manamillaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyae vazhangida manamillaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Verukkum mannukkum idaiveliya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verukkum mannukkum idaiveliya"/>
</div>
<div class="lyrico-lyrics-wrapper">Urimai ennakillaiyaa .
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urimai ennakillaiyaa ."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaaigitha bhoomi naanillaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaigitha bhoomi naanillaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharugira megam neeillaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharugira megam neeillaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhoomiyin marmangal nanaithidaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhoomiyin marmangal nanaithidaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pookkal malarthidaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pookkal malarthidaiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhagin motham naan anaikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagin motham naan anaikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Aayiram kaigal vendumadi thalaivi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayiram kaigal vendumadi thalaivi"/>
</div>
<div class="lyrico-lyrics-wrapper">Suriya thaagam theerum vazhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suriya thaagam theerum vazhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sundara baanam alli kudi thalaivaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sundara baanam alli kudi thalaivaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koodalilae pennin kangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koodalilae pennin kangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Moodiyae kidakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moodiyae kidakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodugalai thaandum seigai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodugalai thaandum seigai"/>
</div>
<div class="lyrico-lyrics-wrapper">Sammadham adharkku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sammadham adharkku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaatril oligal midhakkum varai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatril oligal midhakkum varai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadalil oru thuli irukkumvarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadalil oru thuli irukkumvarai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalathin kadainodi karaiyumvarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalathin kadainodi karaiyumvarai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanna kalandhirupom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna kalandhirupom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sutrum ulagam nirkum varai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sutrum ulagam nirkum varai"/>
</div>
<div class="lyrico-lyrics-wrapper">Suriya kumuzhi udaiyum varai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suriya kumuzhi udaiyum varai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanam vayadhaagi udhirum varai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam vayadhaagi udhirum varai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannae innaindhiruppom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannae innaindhiruppom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Udhadugal kondu vetkam thudaithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udhadugal kondu vetkam thudaithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirae ennai vazhi nadathu thalaivaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirae ennai vazhi nadathu thalaivaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhil edhil sugam endru arivuruthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhil edhil sugam endru arivuruthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhi adhil kondu ennai nilai niruthu thalaivi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhi adhil kondu ennai nilai niruthu thalaivi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kattil meedhu vilakkin kangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattil meedhu vilakkin kangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Moodudhal edharkku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moodudhal edharkku"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilvazhakil saatchi illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilvazhakil saatchi illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Endru seivadharkku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endru seivadharkku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa vaa en thalaiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa vaa en thalaiva"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhuvidu en thalaiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhuvidu en thalaiva"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa vaa en thalaiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa vaa en thalaiva"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalaianaiyai pangidavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaianaiyai pangidavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Malligayin madalukullae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malligayin madalukullae"/>
</div>
<div class="lyrico-lyrics-wrapper">Marmakadhaidhaan irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marmakadhaidhaan irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Marmakadhai padipadharkku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marmakadhai padipadharkku"/>
</div>
<div class="lyrico-lyrics-wrapper">Iruttukullum vazhi irrukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruttukullum vazhi irrukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mmmmmmmmmmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mmmmmmmmmmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Mmmmmmmmmmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mmmmmmmmmmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Mmmmmmmmmmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mmmmmmmmmmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Mmmmmmmmmmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mmmmmmmmmmm"/>
</div>
</pre>
