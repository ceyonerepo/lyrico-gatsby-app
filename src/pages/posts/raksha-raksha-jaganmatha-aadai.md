---
title: 'raksha raksha jaganmatha song lyrics'
album: 'Aadai'
artist: 'Oorka'
lyricist: 'Kalpanadasan'
director: 'Rathnakumar'
path: '/albums/aadai-song-lyrics'
song: 'Raksha Raksha Jaganmatha'
image: ../../images/albumart/aadai.jpg
date: 2019-07-19
lang: tamil
singers: 
- P. Susheela
youtubeLink: "https://www.youtube.com/embed/fFoq2QJb2iw"
type: 'devotional'
---

<pre class="lyrics-native">
    
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Raksha Raksha Jagan Matha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raksha Raksha Jagan Matha"/>
</div>
<div class="lyrico-lyrics-wrapper">Raksha Raksha Jagan Matha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raksha Raksha Jagan Matha"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarva Sakthi Jaya Durga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarva Sakthi Jaya Durga"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarva Sakthi Jaya Durga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarva Sakthi Jaya Durga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raksha Raksha Jagan Matha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raksha Raksha Jagan Matha"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarva Sakthi Jaya Durga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarva Sakthi Jaya Durga"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarva Sakthi Jaya Durga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarva Sakthi Jaya Durga"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarva Sakthi Jaya Durga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarva Sakthi Jaya Durga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raksha Raksha Jagan Matha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raksha Raksha Jagan Matha"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarva Sakthi Jaya Durga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarva Sakthi Jaya Durga"/>
</div>
<div class="lyrico-lyrics-wrapper">Raksha Raksha Jagan Matha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raksha Raksha Jagan Matha"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarva Sakthi Jaya Durga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarva Sakthi Jaya Durga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raksha Raksha Jagan Matha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raksha Raksha Jagan Matha"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarva Sakthi Jaya Durga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarva Sakthi Jaya Durga"/>
</div>
<div class="lyrico-lyrics-wrapper">Raksha Raksha Jagan Matha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raksha Raksha Jagan Matha"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarva Sakthi Jaya Durga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarva Sakthi Jaya Durga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raksha Raksha Jagan Matha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raksha Raksha Jagan Matha"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarva Sakthi Jaya Durga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarva Sakthi Jaya Durga"/>
</div>
<div class="lyrico-lyrics-wrapper">Raksha Raksha Jagan Matha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raksha Raksha Jagan Matha"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarva Sakthi Jaya Durga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarva Sakthi Jaya Durga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jaya Jaya Shankari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaya Jaya Shankari"/>
</div>
<div class="lyrico-lyrics-wrapper">Gowri Manohari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gowri Manohari"/>
</div>
<div class="lyrico-lyrics-wrapper">Abayam Alitharul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Abayam Alitharul"/>
</div>
<div class="lyrico-lyrics-wrapper">Ambigai Bairavi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ambigai Bairavi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jaya Jaya Shankari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaya Jaya Shankari"/>
</div>
<div class="lyrico-lyrics-wrapper">Gowri Manohari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gowri Manohari"/>
</div>
<div class="lyrico-lyrics-wrapper">Abayam Alitharul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Abayam Alitharul"/>
</div>
<div class="lyrico-lyrics-wrapper">Ambigai Bairavi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ambigai Bairavi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Shiva Shiva Shankari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shiva Shiva Shankari"/>
</div>
<div class="lyrico-lyrics-wrapper">Shakthi Magheshwari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shakthi Magheshwari"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiruvarul Tharuvaai Devi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiruvarul Tharuvaai Devi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiruvarul Tharuvaai Devi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiruvarul Tharuvaai Devi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thiruvarul Tharuvaai Devi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiruvarul Tharuvaai Devi"/>
</div>
</pre>