---
title: "chinnataname song lyrics"
album: "Prati Roju Pandage"
artist: "S Thaman"
lyricist: "Sirivennela Seetharama Sastry"
director: "Maruthi Dasari"
path: "/albums/prati-roju-pandage-lyrics"
song: "Chinnataname"
image: ../../images/albumart/prati-roju-pandage.jpg
date: 2019-12-20
lang: telugu
youtubeLink: "https://www.youtube.com/embed/nz0WyQguG0w"
type: "sad"
singers:
  - Vijay Yesudas
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Chinnathaname Chera Rammante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinnathaname Chera Rammante"/>
</div>
<div class="lyrico-lyrics-wrapper">Praanam Ninna Vaipe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Praanam Ninna Vaipe"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaari Theesthundhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaari Theesthundhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Adugulaithe Yedharakaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adugulaithe Yedharakaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadaka Maathram Venakake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadaka Maathram Venakake"/>
</div>
<div class="lyrico-lyrics-wrapper">Gadichipoyina Gnaapakaalatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gadichipoyina Gnaapakaalatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Gathamu Yedhuravuthunnadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gathamu Yedhuravuthunnadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Cherigipone Ledhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cherigipone Ledhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Marapuraane Raadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marapuraane Raadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Chivari Malupuna Nilachi Pilichina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chivari Malupuna Nilachi Pilichina"/>
</div>
<div class="lyrico-lyrics-wrapper">Smruthula Chitikina Velu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Smruthula Chitikina Velu "/>
</div>
<div class="lyrico-lyrics-wrapper">Vadhalani Chelimiga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadhalani Chelimiga"/>
</div>
<div class="lyrico-lyrics-wrapper">Oohale Upponguthunnavilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oohale Upponguthunnavilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mugiyani Kathalatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mugiyani Kathalatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhi Melukunnadhilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhi Melukunnadhilaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaathaga Thala Pandinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaathaga Thala Pandinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thandri Thaname Yendunaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandri Thaname Yendunaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Odini Dhigi Kodukedhiginaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odini Dhigi Kodukedhiginaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanna Muripemu Theerunaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanna Muripemu Theerunaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vayasu Vaalina Sandhe Vaaluna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayasu Vaalina Sandhe Vaaluna"/>
</div>
<div class="lyrico-lyrics-wrapper">Chethikandhina Priya Varam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chethikandhina Priya Varam"/>
</div>
<div class="lyrico-lyrics-wrapper">Manavadai Thana Pasithanammunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manavadai Thana Pasithanammunu"/>
</div>
<div class="lyrico-lyrics-wrapper">Venta Techina Sambaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venta Techina Sambaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottha Oopiri Kaagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottha Oopiri Kaagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasu Ooyalalooga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu Ooyalalooga"/>
</div>
<div class="lyrico-lyrics-wrapper">Thara Tharammula Paatu Inkani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thara Tharammula Paatu Inkani"/>
</div>
<div class="lyrico-lyrics-wrapper">Vamsha Dhaaraga Maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vamsha Dhaaraga Maari"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadalini Kaliyani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadalini Kaliyani"/>
</div>
<div class="lyrico-lyrics-wrapper">Jeevanadhigaa Paaruthundhi Kadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jeevanadhigaa Paaruthundhi Kadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanchiki Cheru Kathaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanchiki Cheru Kathaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Mugisipodhu Kadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mugisipodhu Kadhaa"/>
</div>
</pre>
