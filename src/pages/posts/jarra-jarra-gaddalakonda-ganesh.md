---
title: "jarra jarra song lyrics"
album: "Gaddalakonda Ganesh"
artist: "Mickey J. Meyer"
lyricist: "Bhaskarbhatla Ravikumar"
director: "Harish Shankar"
path: "/albums/gaddalakonda-ganesh-lyrics"
song: "Jarra Jarra"
image: ../../images/albumart/gaddalakonda-ganesh.jpg
date: 2019-09-20
lang: telugu
youtubeLink: "https://www.youtube.com/embed/4ZjZHqgD6YQ"
type: "happy"
singers:
  - Anurag Kulkarni
  - Uma Neha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Jarra Jarra Acha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jarra Jarra Acha"/>
</div>
<div class="lyrico-lyrics-wrapper">Jarra Jarra Gajja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jarra Jarra Gajja"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenu Inthe Chicha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenu Inthe Chicha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye Chandrudikaina 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Chandrudikaina "/>
</div>
<div class="lyrico-lyrics-wrapper">Leda Macha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Leda Macha"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheyyi Padithe Laksha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheyyi Padithe Laksha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalu Pedithe Racha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalu Pedithe Racha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nakaraal Jesthe Bachaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nakaraal Jesthe Bachaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ney Naaral Desetandhuke Vachaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ney Naaral Desetandhuke Vachaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sigguke Aggettei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sigguke Aggettei"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohhohho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohhohho"/>
</div>
<div class="lyrico-lyrics-wrapper">Buggaki Muddhettei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Buggaki Muddhettei"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohhohho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohhohho"/>
</div>
<div class="lyrico-lyrics-wrapper">Galagala Lade Galasuthoti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Galagala Lade Galasuthoti"/>
</div>
<div class="lyrico-lyrics-wrapper">Kulasalenno Leggottei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulasalenno Leggottei"/>
</div>
<div class="lyrico-lyrics-wrapper">Choopulu Dhiggottey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choopulu Dhiggottey"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohhohho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohhohho"/>
</div>
<div class="lyrico-lyrics-wrapper">Lekkalu Theggottey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lekkalu Theggottey"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohhohho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohhohho"/>
</div>
<div class="lyrico-lyrics-wrapper">Gudugudu Guncham Galatalona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gudugudu Guncham Galatalona"/>
</div>
<div class="lyrico-lyrics-wrapper">Manchi Chedda Moolaki Nettey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manchi Chedda Moolaki Nettey"/>
</div>
<div class="lyrico-lyrics-wrapper">Gira Gira Gira Gira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gira Gira Gira Gira"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirige Nadumidhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirige Nadumidhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kora Kora Choopuki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kora Kora Choopuki"/>
</div>
<div class="lyrico-lyrics-wrapper">Kara Kara Mannadhirooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kara Kara Mannadhirooooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Super Hittu Nee Heightu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Super Hittu Nee Heightu"/>
</div>
<div class="lyrico-lyrics-wrapper">Super Hittu Nee Routeu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Super Hittu Nee Routeu"/>
</div>
<div class="lyrico-lyrics-wrapper">Super Hittu Head Weightu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Super Hittu Head Weightu"/>
</div>
<div class="lyrico-lyrics-wrapper">Super Hittu Bomma Hittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Super Hittu Bomma Hittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Super Hittu Meesam Kattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Super Hittu Meesam Kattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Super Hittu Vibudhi Bottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Super Hittu Vibudhi Bottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Super Hittu Eela Kottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Super Hittu Eela Kottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Super Hittu Dhanchi Kottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Super Hittu Dhanchi Kottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Jarra Jarra Acha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jarra Jarra Acha"/>
</div>
<div class="lyrico-lyrics-wrapper">Jarra Jarra Gajja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jarra Jarra Gajja"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenu Inthe Chicha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenu Inthe Chicha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye Chandrudikaina 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Chandrudikaina "/>
</div>
<div class="lyrico-lyrics-wrapper">Leda Macha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Leda Macha"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheyyi Padithe Laksha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheyyi Padithe Laksha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalu Pedithe Racha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalu Pedithe Racha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nakaraal Jesthe Bachaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nakaraal Jesthe Bachaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naaral Desetandhuke Vachaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naaral Desetandhuke Vachaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kelikithe Ek Baar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelikithe Ek Baar"/>
</div>
<div class="lyrico-lyrics-wrapper">Baddale Baasingaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baddale Baasingaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhebbakee Scene Sithaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhebbakee Scene Sithaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhutodi Gundello
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhutodi Gundello"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanuku Vanuku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanuku Vanuku "/>
</div>
<div class="lyrico-lyrics-wrapper">Adhi Nee Aasthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhi Nee Aasthi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Dhamme 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Dhamme "/>
</div>
<div class="lyrico-lyrics-wrapper">Neekunna Bandhobasthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neekunna Bandhobasthi"/>
</div>
<div class="lyrico-lyrics-wrapper">Yahe Nacchindhi Yaadunnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yahe Nacchindhi Yaadunnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ek Dham Yesestha Dhasthee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ek Dham Yesestha Dhasthee"/>
</div>
<div class="lyrico-lyrics-wrapper">Super Hittu Nee Heightu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Super Hittu Nee Heightu"/>
</div>
<div class="lyrico-lyrics-wrapper">Super Hittu Nee Routeu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Super Hittu Nee Routeu"/>
</div>
<div class="lyrico-lyrics-wrapper">Super Hittu Head Weightu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Super Hittu Head Weightu"/>
</div>
<div class="lyrico-lyrics-wrapper">Super Hittu Bomma Hittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Super Hittu Bomma Hittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Super Hittu Meesam Kattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Super Hittu Meesam Kattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Super Hittu Vibudhi Bottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Super Hittu Vibudhi Bottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Super Hittu Eela Kottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Super Hittu Eela Kottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Super Hittu Dhanchi Kottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Super Hittu Dhanchi Kottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Jarra Jarra Acha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jarra Jarra Acha"/>
</div>
<div class="lyrico-lyrics-wrapper">Jarra Jarra Gajja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jarra Jarra Gajja"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenu Inthe Chicha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenu Inthe Chicha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye Chandrudikaina 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Chandrudikaina "/>
</div>
<div class="lyrico-lyrics-wrapper">Leda Macha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Leda Macha"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheyyi Padithe Laksha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheyyi Padithe Laksha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalu Pedithe Racha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalu Pedithe Racha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nakaraal Jesthe Bachaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nakaraal Jesthe Bachaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ney Naaral Desetandhuke Vachaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ney Naaral Desetandhuke Vachaa"/>
</div>
</pre>
