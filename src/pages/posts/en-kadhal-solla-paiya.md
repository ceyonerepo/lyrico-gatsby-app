---
title: 'en kaadhal solla neramillai song lyrics'
album: 'Paiya'
artist: 'Yuvan Shankar Raja'
lyricist: 'Na Muthukumar'
director: 'N.Lingusamy'
path: '/albums/paiya-song-lyrics'
song: 'En Kaadhal Solla Neramillai'
image: ../../images/albumart/paiya.jpg
date: 2010-04-02
lang: tamil
singers: 
- Yuvan Shankar Raja
youtubeLink: "https://www.youtube.com/embed/thtAxtEuX6c"
type: 'love'
---

<pre class="lyrics-native">
    Coming Soon
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">En kaadhal solla neramillai
<input type="checkbox" class="lyrico-select-lyric-line" value="En kaadhal solla neramillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kaadhal solla thevayillai
<input type="checkbox" class="lyrico-select-lyric-line" value="Un kaadhal solla thevayillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nam kaadhal solla vaarthayillai
<input type="checkbox" class="lyrico-select-lyric-line" value="Nam kaadhal solla vaarthayillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Unmai maraithaalum maraiyathadiiii..eeee
<input type="checkbox" class="lyrico-select-lyric-line" value="Unmai maraithaalum maraiyathadiiii..eeee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Un kaiyil sera yengavillai
<input type="checkbox" class="lyrico-select-lyric-line" value="Un kaiyil sera yengavillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un thozil saaya aasaiyillai
<input type="checkbox" class="lyrico-select-lyric-line" value="Un thozil saaya aasaiyillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee ponapinbu soghamillai.. endru
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee ponapinbu soghamillai.. endru"/>
</div>
<div class="lyrico-lyrics-wrapper">Poi solla theriyaadhadi
<input type="checkbox" class="lyrico-select-lyric-line" value="Poi solla theriyaadhadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Un azhagalae un azhagalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Un azhagalae un azhagalae"/>
</div>
<div class="lyrico-lyrics-wrapper">En veyil kaalam adhu mazhai kaalam
<input type="checkbox" class="lyrico-select-lyric-line" value="En veyil kaalam adhu mazhai kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kanavaalae un kanavalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Un kanavaalae un kanavalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam azhaipaayum mella kudaisaayum..
<input type="checkbox" class="lyrico-select-lyric-line" value="Manam azhaipaayum mella kudaisaayum.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">yeee heah yeahhh..
<input type="checkbox" class="lyrico-select-lyric-line" value="yeee heah yeahhh.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">En kaadhal solla neramillai
<input type="checkbox" class="lyrico-select-lyric-line" value="En kaadhal solla neramillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kaadhal solla thevayillai
<input type="checkbox" class="lyrico-select-lyric-line" value="Un kaadhal solla thevayillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nam kaadhal solla vaarthayillai
<input type="checkbox" class="lyrico-select-lyric-line" value="Nam kaadhal solla vaarthayillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Unmai maraithaalum maraiyathadiiii..eeee
<input type="checkbox" class="lyrico-select-lyric-line" value="Unmai maraithaalum maraiyathadiiii..eeee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kaatrodu kaiveesi nee pesinaal
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaatrodu kaiveesi nee pesinaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhan nenjodu puyal veesudhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Endhan nenjodu puyal veesudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vayadhodum mandhodum sollamalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Vayadhodum mandhodum sollamalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sila ennangal valai veesudhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Sila ennangal valai veesudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal vandhalae kannodudhan
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaadhal vandhalae kannodudhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallathanam vandhu kudiyerumo
<input type="checkbox" class="lyrico-select-lyric-line" value="Kallathanam vandhu kudiyerumo"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam nadithenadi konjam thudithenadi
<input type="checkbox" class="lyrico-select-lyric-line" value="Konjam nadithenadi konjam thudithenadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha velaiyaatai rasithenadi..
<input type="checkbox" class="lyrico-select-lyric-line" value="Indha velaiyaatai rasithenadi.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Un viziyalae un viziyalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Un viziyalae un viziyalae"/>
</div>
<div class="lyrico-lyrics-wrapper">En vazhimaarum kann thadumaarum
<input type="checkbox" class="lyrico-select-lyric-line" value="En vazhimaarum kann thadumaarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi idhu yeatho pudhu yeakkam
<input type="checkbox" class="lyrico-select-lyric-line" value="Adi idhu yeatho pudhu yeakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu valithaalum nenjam adhai yearkum …ehh
<input type="checkbox" class="lyrico-select-lyric-line" value="Idhu valithaalum nenjam adhai yearkum …ehh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Oru vaarthai pesamal neepaaradi
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru vaarthai pesamal neepaaradi"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan nimidangal neelatumae
<input type="checkbox" class="lyrico-select-lyric-line" value="Undhan nimidangal neelatumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ver edhum ninaikamal vizhi moodadi
<input type="checkbox" class="lyrico-select-lyric-line" value="Ver edhum ninaikamal vizhi moodadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha nerukangal thodaratumae
<input type="checkbox" class="lyrico-select-lyric-line" value="Indha nerukangal thodaratumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yarum paarkaamal yenai parkiren
<input type="checkbox" class="lyrico-select-lyric-line" value="Yarum paarkaamal yenai parkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai ariyaamal unnai parkiren
<input type="checkbox" class="lyrico-select-lyric-line" value="Ennai ariyaamal unnai parkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Siru pillai ena endhan imaigal adhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Siru pillai ena endhan imaigal adhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai kandalae kudhikindradhae …
<input type="checkbox" class="lyrico-select-lyric-line" value="Unai kandalae kudhikindradhae …"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">En adhikaalai en adhikaalai
<input type="checkbox" class="lyrico-select-lyric-line" value="En adhikaalai en adhikaalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un mugam paarthu thinam ela vendum
<input type="checkbox" class="lyrico-select-lyric-line" value="Un mugam paarthu thinam ela vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">En andhi maalai en andhi maalai
<input type="checkbox" class="lyrico-select-lyric-line" value="En andhi maalai en andhi maalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un madi saayindhu dhinam vizha vendum ..hey eh ley
<input type="checkbox" class="lyrico-select-lyric-line" value="Un madi saayindhu dhinam vizha vendum ..hey eh ley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">En kaadhal solla neramillai
<input type="checkbox" class="lyrico-select-lyric-line" value="En kaadhal solla neramillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kaadhal solla thevayillai
<input type="checkbox" class="lyrico-select-lyric-line" value="Un kaadhal solla thevayillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nam kaadhal solla vaarthayillai
<input type="checkbox" class="lyrico-select-lyric-line" value="Nam kaadhal solla vaarthayillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Unmai maraithaalum maraiyathadiiii..eeee
<input type="checkbox" class="lyrico-select-lyric-line" value="Unmai maraithaalum maraiyathadiiii..eeee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Un kaiyil sera yengavillai
<input type="checkbox" class="lyrico-select-lyric-line" value="Un kaiyil sera yengavillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un thozil saaya aasaiyillai
<input type="checkbox" class="lyrico-select-lyric-line" value="Un thozil saaya aasaiyillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee ponapinbu soghamillai.. endru
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee ponapinbu soghamillai.. endru"/>
</div>
<div class="lyrico-lyrics-wrapper">Poi solla theriyaadhadi
<input type="checkbox" class="lyrico-select-lyric-line" value="Poi solla theriyaadhadi"/>
</div>
</pre>