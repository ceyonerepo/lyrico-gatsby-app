---
title: 'kaalame song lyrics'
album: 'Bigil'
artist: 'A R Rahman'
lyricist: 'Vivek'
director: 'Atlee'
path: '/albums/bigil-song-lyrics'
song: 'Kaalame'
image: ../../images/albumart/bigil.jpg
date: 2019-10-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/5zsNbah4u54"
type: 'sad'
singers: 
- Bamba Bakiya
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">

  <div class="lyrico-lyrics-wrapper">Kaalamae kaalamae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaalamae kaalamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai engu kondu pogiraai
<input type="checkbox" class="lyrico-select-lyric-line" value="Ennai engu kondu pogiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannavan saaigiraan
<input type="checkbox" class="lyrico-select-lyric-line" value="Mannavan saaigiraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaigal katti paarkiraai
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaigal katti paarkiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vaazhkayin kaaranam
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaazhkayin kaaranam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai vittu pogudhoo
<input type="checkbox" class="lyrico-select-lyric-line" value="Ennai vittu pogudhoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Veedhiyil veera vaal
<input type="checkbox" class="lyrico-select-lyric-line" value="Veedhiyil veera vaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thee pidithu vegudhoo
<input type="checkbox" class="lyrico-select-lyric-line" value="Thee pidithu vegudhoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thirumbi vaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Thirumbi vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhundhu vaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Ezhundhu vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirumbi vaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Thirumbi vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhundhu vaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Ezhundhu vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thunayilla vaazhkayil
<input type="checkbox" class="lyrico-select-lyric-line" value="Thunayilla vaazhkayil"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunayaai un kural
<input type="checkbox" class="lyrico-select-lyric-line" value="Thunayaai un kural"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirumbi vaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Thirumbi vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilayilla koottathil
<input type="checkbox" class="lyrico-select-lyric-line" value="Nilayilla koottathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilaikkum un peyar
<input type="checkbox" class="lyrico-select-lyric-line" value="Nilaikkum un peyar"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhundhu vaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Ezhundhu vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhundhu vaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Ezhundhu vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Nee illa boomiyil
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee illa boomiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Engu naan selluven
<input type="checkbox" class="lyrico-select-lyric-line" value="Engu naan selluven"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirumbi vaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Thirumbi vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhundhu vaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Ezhundhu vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirumbi vaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Thirumbi vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhundhu vaa…aaa…aa….
<input type="checkbox" class="lyrico-select-lyric-line" value="Ezhundhu vaa…aaa…aa…."/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhundhu vaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Ezhundhu vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Malaigalum mannil
<input type="checkbox" class="lyrico-select-lyric-line" value="Malaigalum mannil"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhumpozhudhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Vizhumpozhudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Malaigalai dhaanae ezhuppidudhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Malaigalai dhaanae ezhuppidudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaengai vaazhndha kaatilae..ae..hae
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaengai vaazhndha kaatilae..ae..hae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaengai vandhu nirappidum kadhaiyo
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaengai vandhu nirappidum kadhaiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hooo hoo ooo oooo
<input type="checkbox" class="lyrico-select-lyric-line" value="Hooo hoo ooo oooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Arasan naano
<input type="checkbox" class="lyrico-select-lyric-line" value="Arasan naano"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo hoo ooo oooo hoo ho
<input type="checkbox" class="lyrico-select-lyric-line" value="Hoo hoo ooo oooo hoo ho"/>
</div>
<div class="lyrico-lyrics-wrapper">Kobam ondru eriyudho
<input type="checkbox" class="lyrico-select-lyric-line" value="Kobam ondru eriyudho"/>
</div>
<div class="lyrico-lyrics-wrapper">Muttum pagai mudiyudho
<input type="checkbox" class="lyrico-select-lyric-line" value="Muttum pagai mudiyudho"/>
</div>
<div class="lyrico-lyrics-wrapper">Vittu vaitha kalathilea
<input type="checkbox" class="lyrico-select-lyric-line" value="Vittu vaitha kalathilea"/>
</div>
<div class="lyrico-lyrics-wrapper">Singam ondru nuzhaiyudho
<input type="checkbox" class="lyrico-select-lyric-line" value="Singam ondru nuzhaiyudho"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo hoo ooo oooo hoo ho ooo ooo
<input type="checkbox" class="lyrico-select-lyric-line" value="Hoo hoo ooo oooo hoo ho ooo ooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo hoo ooo oooo hoo ho ooo ooo
<input type="checkbox" class="lyrico-select-lyric-line" value="Hoo hoo ooo oooo hoo ho ooo ooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ezhundhu vaa ..aa…
<input type="checkbox" class="lyrico-select-lyric-line" value="Ezhundhu vaa ..aa…"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhundhu vaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Ezhundhu vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhundhu vaa….aa….
<input type="checkbox" class="lyrico-select-lyric-line" value="Ezhundhu vaa….aa…."/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhundhu vaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Ezhundhu vaa"/>
</div>
</pre>