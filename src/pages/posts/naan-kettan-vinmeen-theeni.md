---
title: "naan kettan vinmeen song lyrics"
album: "Theeni"
artist: "Rajesh Murugesan"
lyricist: "Ko Sesha"
director: "Ani.I.V.Sasi"
path: "/albums/theeni-lyrics"
song: "Naan Kettan Vinmeen"
image: ../../images/albumart/theeni.jpg
date: 2021-02-26
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Gu1jirOrdZE"
type: "Melody"
singers:
  - Vijay Yesudas
  - Rajesh Murugesan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">intha alagana 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha alagana "/>
</div>
<div class="lyrico-lyrics-wrapper">london nagarathile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="london nagarathile"/>
</div>
<div class="lyrico-lyrics-wrapper">vennilavin velichathile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vennilavin velichathile"/>
</div>
<div class="lyrico-lyrics-wrapper">ungalai thaluvi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ungalai thaluvi "/>
</div>
<div class="lyrico-lyrics-wrapper">thaalati urukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaalati urukka"/>
</div>
<div class="lyrico-lyrics-wrapper">oru suvaiyana paatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru suvaiyana paatu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oh yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oh yeah"/>
</div>
<div class="lyrico-lyrics-wrapper">bass
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bass"/>
</div>
<div class="lyrico-lyrics-wrapper">keys
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="keys"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">naan ketten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan ketten"/>
</div>
<div class="lyrico-lyrics-wrapper">vinmeen baashaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vinmeen baashaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">kaadhile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadhile"/>
</div>
<div class="lyrico-lyrics-wrapper">ennodu naalum pesudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennodu naalum pesudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">raavile 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="raavile "/>
</div>
<div class="lyrico-lyrics-wrapper">en veedu bhoologam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en veedu bhoologam"/>
</div>
<div class="lyrico-lyrics-wrapper">en nanban 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en nanban "/>
</div>
<div class="lyrico-lyrics-wrapper">dhinam thol urasiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhinam thol urasiye"/>
</div>
<div class="lyrico-lyrics-wrapper">nadandhidum kaatruthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nadandhidum kaatruthaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">naan thoongi naalachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan thoongi naalachu"/>
</div>
<div class="lyrico-lyrics-wrapper">en thookam thoolachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en thookam thoolachu"/>
</div>
<div class="lyrico-lyrics-wrapper">en moochu kaatrai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en moochu kaatrai"/>
</div>
<div class="lyrico-lyrics-wrapper">pola enakkul ketkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pola enakkul ketkum"/>
</div>
<div class="lyrico-lyrics-wrapper">paadal ondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paadal ondru"/>
</div>
<div class="lyrico-lyrics-wrapper">vaanodu neer pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaanodu neer pola"/>
</div>
<div class="lyrico-lyrics-wrapper">ennodu nee endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennodu nee endru"/>
</div>
<div class="lyrico-lyrics-wrapper">naan pogum paadahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan pogum paadahi"/>
</div>
<div class="lyrico-lyrics-wrapper">yaavum kooda nagarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaavum kooda nagarum"/>
</div>
<div class="lyrico-lyrics-wrapper">nizhalum solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nizhalum solla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">iravugal mannodu thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iravugal mannodu thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">vaanil neendhum vennila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaanil neendhum vennila"/>
</div>
<div class="lyrico-lyrics-wrapper">kannoduthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannoduthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">minnal keetrena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="minnal keetrena"/>
</div>
<div class="lyrico-lyrics-wrapper">thanimaiyum nenjoduthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanimaiyum nenjoduthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">indha nilaiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indha nilaiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">ennai vaatum vazhkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennai vaatum vazhkai"/>
</div>
<div class="lyrico-lyrics-wrapper">yedagangalum nenjuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yedagangalum nenjuku"/>
</div>
<div class="lyrico-lyrics-wrapper">thevai illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thevai illai"/>
</div>
<div class="lyrico-lyrics-wrapper">thookangalum kannuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thookangalum kannuku"/>
</div>
<div class="lyrico-lyrics-wrapper">thevai illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thevai illai"/>
</div>
<div class="lyrico-lyrics-wrapper">ennul ketkum sandhosa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennul ketkum sandhosa"/>
</div>
<div class="lyrico-lyrics-wrapper">sangeetham podhum endrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sangeetham podhum endrum"/>
</div>
<div class="lyrico-lyrics-wrapper">yaarodu yaarendru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaarodu yaarendru"/>
</div>
<div class="lyrico-lyrics-wrapper">yaar kaana ulagile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaar kaana ulagile"/>
</div>
<div class="lyrico-lyrics-wrapper">alaigirom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alaigirom"/>
</div>
<div class="lyrico-lyrics-wrapper">irudhiyil maraigirom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irudhiyil maraigirom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">naan thoongi naalachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan thoongi naalachu"/>
</div>
<div class="lyrico-lyrics-wrapper">en thookam thoolachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en thookam thoolachu"/>
</div>
<div class="lyrico-lyrics-wrapper">en moochu kaatrai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en moochu kaatrai"/>
</div>
<div class="lyrico-lyrics-wrapper">pola enakkul ketkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pola enakkul ketkum"/>
</div>
<div class="lyrico-lyrics-wrapper">paadal ondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paadal ondru"/>
</div>
<div class="lyrico-lyrics-wrapper">vaanodu neer pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaanodu neer pola"/>
</div>
<div class="lyrico-lyrics-wrapper">ennodu nee endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennodu nee endru"/>
</div>
<div class="lyrico-lyrics-wrapper">naan pogum paadahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan pogum paadahi"/>
</div>
<div class="lyrico-lyrics-wrapper">yaavum kooda nagarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaavum kooda nagarum"/>
</div>
<div class="lyrico-lyrics-wrapper">nizhalum solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nizhalum solla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">naan thoongi naalachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan thoongi naalachu"/>
</div>
<div class="lyrico-lyrics-wrapper">en thookam thoolachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en thookam thoolachu"/>
</div>
<div class="lyrico-lyrics-wrapper">en moochu kaatrai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en moochu kaatrai"/>
</div>
<div class="lyrico-lyrics-wrapper">pola enakkul ketkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pola enakkul ketkum"/>
</div>
<div class="lyrico-lyrics-wrapper">paadal ondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paadal ondru"/>
</div>
<div class="lyrico-lyrics-wrapper">vaanodu neer pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaanodu neer pola"/>
</div>
<div class="lyrico-lyrics-wrapper">ennodu nee endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennodu nee endru"/>
</div>
<div class="lyrico-lyrics-wrapper">naan pogum paadahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan pogum paadahi"/>
</div>
<div class="lyrico-lyrics-wrapper">yaavum kooda nagarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaavum kooda nagarum"/>
</div>
<div class="lyrico-lyrics-wrapper">nizhalum solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nizhalum solla"/>
</div>
</pre>
