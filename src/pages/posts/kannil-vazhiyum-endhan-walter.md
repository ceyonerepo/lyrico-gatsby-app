---
title: 'kannil vazhiyum endhan song lyrics'
album: 'Walter'
artist: 'Dharma Prakash'
lyricist: 'Uma Devi'
director: 'U Anbu'
path: '/albums/walter-song-lyrics'
song: 'Kannil Vazhiyum Endhan'
image: ../../images/albumart/walter.jpg
date: 2020-03-13
lang: tamil
singers: 
- Naresh Iyer
youtubeLink: "https://www.youtube.com/embed/o7f4WM__MSQ"
type: 'friendship'
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">

  <div class="lyrico-lyrics-wrapper">kannil vazhiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannil vazhiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">endhan uyire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="endhan uyire"/>
</div>
<div class="lyrico-lyrics-wrapper">nanban unai pol yaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanban unai pol yaar"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjil vilaintha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjil vilaintha"/>
</div>
<div class="lyrico-lyrics-wrapper">enthan urave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enthan urave"/>
</div>
<div class="lyrico-lyrics-wrapper">sontham unai pol yaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sontham unai pol yaar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">malayagatha neer ellame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malayagatha neer ellame"/>
</div>
<div class="lyrico-lyrics-wrapper">kadal seruma meendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadal seruma meendum"/>
</div>
<div class="lyrico-lyrics-wrapper">siragagi thuliyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="siragagi thuliyum"/>
</div>
<div class="lyrico-lyrics-wrapper">thirumbi vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thirumbi vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">en malagalanavane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en malagalanavane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thozha annai pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thozha annai pola"/>
</div>
<div class="lyrico-lyrics-wrapper">unthan uravil anbai kanden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unthan uravil anbai kanden"/>
</div>
<div class="lyrico-lyrics-wrapper">thozha thanthai pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thozha thanthai pola"/>
</div>
<div class="lyrico-lyrics-wrapper">uyir kaaval aanaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyir kaaval aanaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">yeno alithu tholaithitene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yeno alithu tholaithitene"/>
</div>
<div class="lyrico-lyrics-wrapper">yeeri kaatil thunai thedum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yeeri kaatil thunai thedum "/>
</div>
<div class="lyrico-lyrics-wrapper">paravai pola aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paravai pola aanen"/>
</div>
<div class="lyrico-lyrics-wrapper">thinam nanum unai thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thinam nanum unai thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">kaniyanagiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaniyanagiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thozha kollum theeyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thozha kollum theeyai"/>
</div>
<div class="lyrico-lyrics-wrapper">avan uthiram sithara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avan uthiram sithara"/>
</div>
<div class="lyrico-lyrics-wrapper">alithu olithiduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alithu olithiduven"/>
</div>
<div class="lyrico-lyrics-wrapper">theera pasiyaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theera pasiyaara"/>
</div>
<div class="lyrico-lyrics-wrapper">unai alitha ethiri udalin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unai alitha ethiri udalin"/>
</div>
<div class="lyrico-lyrics-wrapper">kolaigan naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kolaigan naan"/>
</div>
</pre>