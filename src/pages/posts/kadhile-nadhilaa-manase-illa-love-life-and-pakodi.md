---
title: "kadhile nadhilaa song lyrics"
album: "Love Life And Pakodi"
artist: "Pavan"
lyricist: "Mahesh Poloju"
director: "Jayanth Gali"
path: "/albums/love-life-and-pakodi-lyrics"
song: "Kadhile Nadhilaa Manase Illa"
image: ../../images/albumart/love-life-and-pakodi.jpg
date: 2021-03-12
lang: telugu
youtubeLink: "https://www.youtube.com/embed/9DIRBEBgHNQ"
type: "love"
singers:
  - Krishna Tejaswi
  - Akanksha Bisht
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kadhile Nadhilaa Manase Ilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhile Nadhilaa Manase Ilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalale Alalai Murisindhilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalale Alalai Murisindhilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Veluge Chinukai Kurise Ilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veluge Chinukai Kurise Ilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thapane Thadisi Virisindhilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thapane Thadisi Virisindhilaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Palike Gaale Raagaalilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palike Gaale Raagaalilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Alle Chilipi Haayilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alle Chilipi Haayilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Paruge Adige Paadhaannilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paruge Adige Paadhaannilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Cherosagamai Chirakaalam Saagaalilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cherosagamai Chirakaalam Saagaalilaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalam Ilaa Kadhile Kalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam Ilaa Kadhile Kalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadham Ilaa Parugula Alaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadham Ilaa Parugula Alaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Aa Aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Aa Aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Oo oo Oo OoOo Oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Oo oo Oo OoOo Oo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paruvam Swaramai Palike Kadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paruvam Swaramai Palike Kadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Samayam Saradhaa Panche Sadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samayam Saradhaa Panche Sadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hrudhayam Vethike Kshaname Ilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hrudhayam Vethike Kshaname Ilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pranayam Payanam Okate Kadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pranayam Payanam Okate Kadhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Dhaarilo Adugesthunnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Dhaarilo Adugesthunnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Preme Ika Edhuravvadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Preme Ika Edhuravvadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Okatainaa Ekaanthaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okatainaa Ekaanthaanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Prathi Nimisham Santhosham 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prathi Nimisham Santhosham "/>
</div>
<div class="lyrico-lyrics-wrapper">Sontham Kaadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sontham Kaadhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalam Ilaa Kadhile Kalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam Ilaa Kadhile Kalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadham Ilaa Parugula Alaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadham Ilaa Parugula Alaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Aa Aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Aa Aa"/>
</div>
</pre>
