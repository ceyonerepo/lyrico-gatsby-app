---
title: "ettuthikkum oorum song lyrics"
album: "Aelay"
artist: "Kaber Vasuki - Aruldev"
lyricist: "M.K. Kadal Vendhan"
director: "Halitha Shameem"
path: "/albums/aelay-song-lyrics"
song: "Ethuthikkum Oorum"
image: ../../images/albumart/aelay.jpg
date: 2021-002-28
lang: tamil
youtubeLink: "https://www.youtube.com/embed/xMBFsVNAhmM"
type: "Emotional"
singers:
  - M.K. Kadal Vendhan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">ettuthikkum oorum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ettuthikkum oorum"/>
</div>
<div class="lyrico-lyrics-wrapper">ivan per ucharikum paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ivan per ucharikum paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">pakkathula paatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pakkathula paatha"/>
</div>
<div class="lyrico-lyrics-wrapper">ivaro pacha pulla aalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ivaro pacha pulla aalu"/>
</div>
<div class="lyrico-lyrics-wrapper">kavadu soothu theriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kavadu soothu theriya"/>
</div>
<div class="lyrico-lyrics-wrapper">paya pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paya pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">irunthum kuduppan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irunthum kuduppan"/>
</div>
<div class="lyrico-lyrics-wrapper">thinamum ivan tholla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thinamum ivan tholla"/>
</div>
<div class="lyrico-lyrics-wrapper">kodukum settai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kodukum settai "/>
</div>
<div class="lyrico-lyrics-wrapper">endrum inai illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="endrum inai illa"/>
</div>
<div class="lyrico-lyrics-wrapper">kanaka valaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanaka valaka"/>
</div>
<div class="lyrico-lyrics-wrapper">aiyo alavillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aiyo alavillai"/>
</div>
<div class="lyrico-lyrics-wrapper">ettuthikkum oorum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ettuthikkum oorum"/>
</div>
<div class="lyrico-lyrics-wrapper">ivan per ucharikum paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ivan per ucharikum paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">pakkathula paatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pakkathula paatha"/>
</div>
<div class="lyrico-lyrics-wrapper">ivaro pacha pulla aalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ivaro pacha pulla aalu"/>
</div>
<div class="lyrico-lyrics-wrapper">aluku vetti colouru satta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aluku vetti colouru satta"/>
</div>
<div class="lyrico-lyrics-wrapper">aluthi ootum bedalu katta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aluthi ootum bedalu katta"/>
</div>
<div class="lyrico-lyrics-wrapper">kudippan bodha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kudippan bodha"/>
</div>
<div class="lyrico-lyrics-wrapper">mooku mutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mooku mutta"/>
</div>
<div class="lyrico-lyrics-wrapper">ulaippan rasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulaippan rasa"/>
</div>
<div class="lyrico-lyrics-wrapper">viyarva sotta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viyarva sotta"/>
</div>
<div class="lyrico-lyrics-wrapper">oor pillai yavum thaan pillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oor pillai yavum thaan pillai"/>
</div>
<div class="lyrico-lyrics-wrapper">onnaga pappa mappila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="onnaga pappa mappila"/>
</div>
<div class="lyrico-lyrics-wrapper">eelaikum ivan mel illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eelaikum ivan mel illa"/>
</div>
<div class="lyrico-lyrics-wrapper">yarukum iya keel illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yarukum iya keel illa"/>
</div>
<div class="lyrico-lyrics-wrapper">pinju manasa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pinju manasa "/>
</div>
<div class="lyrico-lyrics-wrapper">kolla adippan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kolla adippan"/>
</div>
<div class="lyrico-lyrics-wrapper">aana veliya ivan nadippan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aana veliya ivan nadippan"/>
</div>
<div class="lyrico-lyrics-wrapper">pinju manasa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pinju manasa "/>
</div>
<div class="lyrico-lyrics-wrapper">kolla adippan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kolla adippan"/>
</div>
<div class="lyrico-lyrics-wrapper">aana veliya ivan nadippan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aana veliya ivan nadippan"/>
</div>
<div class="lyrico-lyrics-wrapper">ettuthikkum oorum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ettuthikkum oorum"/>
</div>
<div class="lyrico-lyrics-wrapper">ivan per ucharikum paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ivan per ucharikum paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">pakkathula paatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pakkathula paatha"/>
</div>
<div class="lyrico-lyrics-wrapper">ivaro pacha pulla aalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ivaro pacha pulla aalu"/>
</div>
</pre>
