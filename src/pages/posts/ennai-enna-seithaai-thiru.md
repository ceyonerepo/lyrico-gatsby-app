---
title: "ennai enna seithaai song lyrics"
album: "Thiru"
artist: "unknown"
lyricist: "unknown"
director: "Karthik Sivan"
path: "/albums/thiru-lyrics"
song: "Ennai Enna Seithaai"
image: ../../images/albumart/thiru.jpg
date: 2018-12-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/CpmW7Lbk6r0"
type: "love"
singers:
  - unknown
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">ennai enna seithaai neeyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennai enna seithaai neeyada"/>
</div>
<div class="lyrico-lyrics-wrapper">innum enna seivai koorada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="innum enna seivai koorada"/>
</div>
<div class="lyrico-lyrics-wrapper">kangalai nerai paarada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kangalai nerai paarada"/>
</div>
<div class="lyrico-lyrics-wrapper">un valaikulle meenai nanada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un valaikulle meenai nanada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">adiye adiye unnai vida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adiye adiye unnai vida"/>
</div>
<div class="lyrico-lyrics-wrapper">peralagi yaarum illai endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="peralagi yaarum illai endru"/>
</div>
<div class="lyrico-lyrics-wrapper">solla ennai vitaal veru yaradi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="solla ennai vitaal veru yaradi"/>
</div>
<div class="lyrico-lyrics-wrapper">adi ennai vitaal veru yaradi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi ennai vitaal veru yaradi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ennai enna seithaai neeyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennai enna seithaai neeyada"/>
</div>
<div class="lyrico-lyrics-wrapper">innum enna seivai kooradi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="innum enna seivai kooradi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vaasam pookalai alli tharuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaasam pookalai alli tharuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">vannam kangal alli tharutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vannam kangal alli tharutha"/>
</div>
<div class="lyrico-lyrics-wrapper">thegam vaanamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thegam vaanamai"/>
</div>
<div class="lyrico-lyrics-wrapper">illai vaanam megamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="illai vaanam megamai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pookal indri vaasam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pookal indri vaasam "/>
</div>
<div class="lyrico-lyrics-wrapper">illai kathali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="illai kathali"/>
</div>
<div class="lyrico-lyrics-wrapper">adi kangal indri 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi kangal indri "/>
</div>
<div class="lyrico-lyrics-wrapper">vannam illai kanmani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vannam illai kanmani"/>
</div>
<div class="lyrico-lyrics-wrapper">vaanam indri megam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaanam indri megam"/>
</div>
<div class="lyrico-lyrics-wrapper">illai kathali adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="illai kathali adi"/>
</div>
<div class="lyrico-lyrics-wrapper">unnai vittu nanum illaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai vittu nanum illaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">athanaal athanaal unnai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athanaal athanaal unnai"/>
</div>
<div class="lyrico-lyrics-wrapper">alli poosi konden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alli poosi konden"/>
</div>
<div class="lyrico-lyrics-wrapper">anbin vaasam vangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anbin vaasam vangi"/>
</div>
<div class="lyrico-lyrics-wrapper">konden unnai vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konden unnai vittu"/>
</div>
<div class="lyrico-lyrics-wrapper">naanum illaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naanum illaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">ini unnai vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ini unnai vittu"/>
</div>
<div class="lyrico-lyrics-wrapper">naanum illaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naanum illaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ennai enna seithaai neeyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennai enna seithaai neeyadi"/>
</div>
<div class="lyrico-lyrics-wrapper">innum enna seivai kooradi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="innum enna seivai kooradi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pookal enai vida alaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pookal enai vida alaga"/>
</div>
<div class="lyrico-lyrics-wrapper">nee sol en arumai alaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee sol en arumai alaga"/>
</div>
<div class="lyrico-lyrics-wrapper">poovum vedhama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poovum vedhama"/>
</div>
<div class="lyrico-lyrics-wrapper">athan mounama aagume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athan mounama aagume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">mounathinal pookal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mounathinal pookal"/>
</div>
<div class="lyrico-lyrics-wrapper">ellam alagadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellam alagadi"/>
</div>
<div class="lyrico-lyrics-wrapper">tamil pesum poove
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tamil pesum poove"/>
</div>
<div class="lyrico-lyrics-wrapper">peralagi neeyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="peralagi neeyadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vannathainal pookal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vannathainal pookal"/>
</div>
<div class="lyrico-lyrics-wrapper">ellam alagada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellam alagada"/>
</div>
<div class="lyrico-lyrics-wrapper">ennai nee konjum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennai nee konjum"/>
</div>
<div class="lyrico-lyrics-wrapper">pothe alagada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pothe alagada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">athanaal athanaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athanaal athanaal"/>
</div>
<div class="lyrico-lyrics-wrapper">un arivum en alagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un arivum en alagum"/>
</div>
<div class="lyrico-lyrics-wrapper">ondri ondrai sera ingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ondri ondrai sera ingu"/>
</div>
<div class="lyrico-lyrics-wrapper">kathal thane peralagu di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathal thane peralagu di"/>
</div>
<div class="lyrico-lyrics-wrapper">intha kathal thane 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha kathal thane "/>
</div>
<div class="lyrico-lyrics-wrapper">peralagu di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="peralagu di"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ennai enna seithaai neeyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennai enna seithaai neeyada"/>
</div>
<div class="lyrico-lyrics-wrapper">innum enna seivai koorada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="innum enna seivai koorada"/>
</div>
<div class="lyrico-lyrics-wrapper">kangalai nerai paarada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kangalai nerai paarada"/>
</div>
<div class="lyrico-lyrics-wrapper">un valaikulle meenai nanada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un valaikulle meenai nanada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">adiye adiye unnai vida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adiye adiye unnai vida"/>
</div>
<div class="lyrico-lyrics-wrapper">peralagi yaarum illai endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="peralagi yaarum illai endru"/>
</div>
<div class="lyrico-lyrics-wrapper">solla ennai vitaal veru yaradi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="solla ennai vitaal veru yaradi"/>
</div>
<div class="lyrico-lyrics-wrapper">adi ennai vitaal veru yaradi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi ennai vitaal veru yaradi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ennai enna seithaai neeyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennai enna seithaai neeyada"/>
</div>
<div class="lyrico-lyrics-wrapper">innum enna seivai koorada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="innum enna seivai koorada"/>
</div>
</pre>
