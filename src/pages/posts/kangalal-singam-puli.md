---
title: "kangalal song lyrics"
album: "Singam Puli"
artist: "Mani Sharma"
lyricist: "Na. Muthukumar"
director: "Sai Ramani"
path: "/albums/singam-puli-lyrics"
song: "Kangalal"
image: ../../images/albumart/singam-puli.jpg
date: 2011-03-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/0CEoxK-HaP0"
type: "love"
singers:
  - Karthik
  - Rita
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kangalaal Kaththi Sandai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangalaal Kaththi Sandai"/>
</div>
<div class="lyrico-lyrics-wrapper">Podaadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podaadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaandhame Thalli Nindru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaandhame Thalli Nindru"/>
</div>
<div class="lyrico-lyrics-wrapper">Theendaadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theendaadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Muththathaal Kuththu Sandai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muththathaal Kuththu Sandai"/>
</div>
<div class="lyrico-lyrics-wrapper">Podaadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podaadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Moochukkul Kichukkichu Moottaadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moochukkul Kichukkichu Moottaadhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbe Onnum Seiyaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbe Onnum Seiyaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Aiyo Ennai Kollaadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyo Ennai Kollaadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Adadaa Thappu Seiyaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adadaa Thappu Seiyaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal Sandai Mudiyaadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Sandai Mudiyaadhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konjamaai Nee Paarthaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjamaai Nee Paarthaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Panjile Theeppole
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panjile Theeppole"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjile Sood Yerudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjile Sood Yerudhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbe Onnum Seiyaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbe Onnum Seiyaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Aiyo Ennai Kollaadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyo Ennai Kollaadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Adadaa Thappu Seiyaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adadaa Thappu Seiyaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal Sandai Mudiyaadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Sandai Mudiyaadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Haa Aah Haa Aaa Aaah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa Aah Haa Aaa Aaah"/>
</div>
<div class="lyrico-lyrics-wrapper">Haa Aah Haa Aaa Aaah Aah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa Aah Haa Aaa Aaah Aah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Puthagaththin Ulle Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthagaththin Ulle Dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Vandhu Paarkkaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Vandhu Paarkkaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppadi Paadam Yerum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppadi Paadam Yerum"/>
</div>
<div class="lyrico-lyrics-wrapper">Gnyaanam Varum Vaa Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gnyaanam Varum Vaa Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Malligaikku Ulle Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malligaikku Ulle Dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yetti Yetti Paarkkaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yetti Yetti Paarkkaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Dhaan Undu Endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Dhaan Undu Endru"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanariven Vaa Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanariven Vaa Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaambile Vittaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaambile Vittaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Pookkal Dhaan Veenaagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pookkal Dhaan Veenaagum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattikko Maalaipole
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattikko Maalaipole"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaadaa Vaadaa Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadaa Vaadaa Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ammamma Naan Thera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammamma Naan Thera"/>
</div>
<div class="lyrico-lyrics-wrapper">Aiyaiyo Naalaagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyaiyo Naalaagum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappiche Odapporen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappiche Odapporen"/>
</div>
<div class="lyrico-lyrics-wrapper">Podi Podi Poo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podi Podi Poo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Baby Come To Me Hold Me Tight
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby Come To Me Hold Me Tight"/>
</div>
<div class="lyrico-lyrics-wrapper">Let Me Be A Girl Feel So Right
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Let Me Be A Girl Feel So Right"/>
</div>
<div class="lyrico-lyrics-wrapper">Hold And To You We So Meant To
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hold And To You We So Meant To"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbe Onnum Seiyaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbe Onnum Seiyaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Aiyo Ennai Kollaadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyo Ennai Kollaadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Adadaa Thappu Seiyaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adadaa Thappu Seiyaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal Sandai Mudiyaadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Sandai Mudiyaadhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhekko Jaane Jaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhekko Jaane Jaana"/>
</div>
<div class="lyrico-lyrics-wrapper">You Met Me Gosha Naanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Met Me Gosha Naanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Tell Me Baby Will I Ever Be Your Deewaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tell Me Baby Will I Ever Be Your Deewaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhekko Jaane Jaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhekko Jaane Jaana"/>
</div>
<div class="lyrico-lyrics-wrapper">We Married If U Wanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="We Married If U Wanna"/>
</div>
<div class="lyrico-lyrics-wrapper">Tell Me Baby Will I Ever Be Your Deewaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tell Me Baby Will I Ever Be Your Deewaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullaakki Laali Ullaali Laali Aththupa Aththupa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullaakki Laali Ullaali Laali Aththupa Aththupa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullaakki Laali Ullaali Laali Aththupa Aththupa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullaakki Laali Ullaali Laali Aththupa Aththupa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meththaiyile Eppodhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meththaiyile Eppodhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethanaiyo Sandhegam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethanaiyo Sandhegam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ketkkiren Onnu Onnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketkkiren Onnu Onnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollavenum Neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollavenum Neeyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ullam Solla Sonnaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullam Solla Sonnaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetkkam Solla Vidaadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetkkam Solla Vidaadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Achaththai Thottu Thottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achaththai Thottu Thottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pokkavenum Neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pokkavenum Neeyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaithoda Enneram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaithoda Enneram"/>
</div>
<div class="lyrico-lyrics-wrapper">Solladi Pon Neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solladi Pon Neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalaiyaa Andhi Maalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalaiyaa Andhi Maalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Moondraam Jaamamaa Hoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moondraam Jaamamaa Hoi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pakkathil Veraarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkathil Veraarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Illannaa Anneram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illannaa Anneram"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal Dhaan Kaalam Neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Dhaan Kaalam Neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedippaarkkaadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedippaarkkaadhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Baby Come To Me Hold Me Tight
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby Come To Me Hold Me Tight"/>
</div>
<div class="lyrico-lyrics-wrapper">Let Me Be A Man Feel So Right
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Let Me Be A Man Feel So Right"/>
</div>
<div class="lyrico-lyrics-wrapper">Hold And To You We So Meant To Be
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hold And To You We So Meant To Be"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbe Unnai Ippodhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbe Unnai Ippodhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Alli Thinna Porene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alli Thinna Porene"/>
</div>
<div class="lyrico-lyrics-wrapper">Adadaa Thappu Seiyaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adadaa Thappu Seiyaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal Sandai Mudiyaadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Sandai Mudiyaadhe"/>
</div>
</pre>
