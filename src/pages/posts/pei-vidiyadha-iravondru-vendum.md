---
title: "pei song lyrics"
album: "Vidiyadha Iravondru Vendum"
artist: "GKV"
lyricist: "Karuppaiyaa Murugan"
director: "Karuppaiyaa Murugan"
path: "/albums/vidiyadha-iravondru-vendum-lyrics"
song: "Pei"
image: ../../images/albumart/vidiyadha-iravondru-vendum.jpg
date: 2022-02-11
lang: tamil
youtubeLink: "https://www.youtube.com/embed/hvvO_iBopZI"
type: "thrill"
singers:
  - Jainraj Ejoumale 
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">bayam bayam pei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bayam bayam pei"/>
</div>
<div class="lyrico-lyrics-wrapper">bayamunna enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bayamunna enna"/>
</div>
<div class="lyrico-lyrics-wrapper">kannukulla vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannukulla vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">kollumo unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kollumo unna"/>
</div>
<div class="lyrico-lyrics-wrapper">senja pavam ella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="senja pavam ella"/>
</div>
<div class="lyrico-lyrics-wrapper">nikkume munna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nikkume munna"/>
</div>
<div class="lyrico-lyrics-wrapper">thandana unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thandana unna"/>
</div>
<div class="lyrico-lyrics-wrapper">thurathume pinna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thurathume pinna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kanna uruti unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanna uruti unna"/>
</div>
<div class="lyrico-lyrics-wrapper">parthu kollum mella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="parthu kollum mella"/>
</div>
<div class="lyrico-lyrics-wrapper">pei vanthu pogum ulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pei vanthu pogum ulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kathi vachu unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathi vachu unna"/>
</div>
<div class="lyrico-lyrics-wrapper">thirvu seyyum mella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thirvu seyyum mella"/>
</div>
<div class="lyrico-lyrics-wrapper">pei vendru pogum ulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pei vendru pogum ulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">bayamunna enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bayamunna enna"/>
</div>
<div class="lyrico-lyrics-wrapper">therikuthu ulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="therikuthu ulla"/>
</div>
<div class="lyrico-lyrics-wrapper">bayamunna enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bayamunna enna"/>
</div>
<div class="lyrico-lyrics-wrapper">pei bayam enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pei bayam enna"/>
</div>
<div class="lyrico-lyrics-wrapper">bayamunna enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bayamunna enna"/>
</div>
<div class="lyrico-lyrics-wrapper">adichi kollum unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adichi kollum unna"/>
</div>
<div class="lyrico-lyrics-wrapper">bayamunna enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bayamunna enna"/>
</div>
<div class="lyrico-lyrics-wrapper">therichu odu munna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="therichu odu munna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">tharu maru vandiya otti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tharu maru vandiya otti"/>
</div>
<div class="lyrico-lyrics-wrapper">ulla rendu bottle 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulla rendu bottle "/>
</div>
<div class="lyrico-lyrics-wrapper">pranthi oothi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pranthi oothi"/>
</div>
<div class="lyrico-lyrics-wrapper">nadapathai makkal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nadapathai makkal"/>
</div>
<div class="lyrico-lyrics-wrapper">nasungana seithi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nasungana seithi"/>
</div>
<div class="lyrico-lyrics-wrapper">naadu marakkuma 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naadu marakkuma "/>
</div>
<div class="lyrico-lyrics-wrapper">ahhhhhhhh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ahhhhhhhh"/>
</div>
<div class="lyrico-lyrics-wrapper">seththa manushan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seththa manushan"/>
</div>
<div class="lyrico-lyrics-wrapper">aviya vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aviya vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">konnavanoda usura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konnavanoda usura"/>
</div>
<div class="lyrico-lyrics-wrapper">pirichi eduthutu pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pirichi eduthutu pona"/>
</div>
<div class="lyrico-lyrics-wrapper">court ethuku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="court ethuku "/>
</div>
<div class="lyrico-lyrics-wrapper">kodium ethuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kodium ethuku"/>
</div>
<div class="lyrico-lyrics-wrapper">nadunisi neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nadunisi neram"/>
</div>
<div class="lyrico-lyrics-wrapper">naaikalum thungum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naaikalum thungum"/>
</div>
<div class="lyrico-lyrics-wrapper">peinga guda rest uh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="peinga guda rest uh"/>
</div>
<div class="lyrico-lyrics-wrapper">edukum antha neramum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edukum antha neramum"/>
</div>
<div class="lyrico-lyrics-wrapper">pinchu karangala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pinchu karangala"/>
</div>
<div class="lyrico-lyrics-wrapper">engo evano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engo evano"/>
</div>
<div class="lyrico-lyrics-wrapper">naasam pannuran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naasam pannuran"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pei ah vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pei ah vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">kalutha neruchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalutha neruchi"/>
</div>
<div class="lyrico-lyrics-wrapper">mookka odachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mookka odachi"/>
</div>
<div class="lyrico-lyrics-wrapper">kanna pudungi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanna pudungi"/>
</div>
<div class="lyrico-lyrics-wrapper">usura uru theriyama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="usura uru theriyama"/>
</div>
<div class="lyrico-lyrics-wrapper">aakki puduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aakki puduven"/>
</div>
<div class="lyrico-lyrics-wrapper">madhu maadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="madhu maadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">mattumea than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mattumea than"/>
</div>
<div class="lyrico-lyrics-wrapper">intha ulagamada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha ulagamada"/>
</div>
<div class="lyrico-lyrics-wrapper">engu parthalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engu parthalum"/>
</div>
<div class="lyrico-lyrics-wrapper">kalagamada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalagamada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sattam sarilla countryila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sattam sarilla countryila"/>
</div>
<div class="lyrico-lyrics-wrapper">saga manusan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saga manusan"/>
</div>
<div class="lyrico-lyrics-wrapper">evanukum nandri illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="evanukum nandri illa"/>
</div>
<div class="lyrico-lyrics-wrapper">orunal illa orunal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="orunal illa orunal"/>
</div>
<div class="lyrico-lyrics-wrapper">kadavul avatharam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadavul avatharam"/>
</div>
<div class="lyrico-lyrics-wrapper">kan munne nikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kan munne nikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">jakkirathai jaa kki ra thai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jakkirathai jaa kki ra thai"/>
</div>
<div class="lyrico-lyrics-wrapper">jakkirathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jakkirathai"/>
</div>
</pre>
