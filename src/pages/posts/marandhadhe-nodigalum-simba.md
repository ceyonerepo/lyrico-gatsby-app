---
title: "marandhadhe nodigalum song lyrics"
album: "Simba"
artist: "Vishal Chandrasekhar"
lyricist: "Vishal Chandrashekhar"
director: "Arvind Sridhar"
path: "/albums/simba-lyrics"
song: "Marandhadhe Nodigalum"
image: ../../images/albumart/simba.jpg
date: 2019-01-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/zQNIncDkDLo"
type: "love"
singers:
  - Anirudh Ravichander
  - Tee Jay
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Marandhadhe Nodigalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marandhadhe Nodigalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Parandhadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parandhadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Mounamum Nirambiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mounamum Nirambiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan Arugile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan Arugile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Marandhadhe Nodigalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marandhadhe Nodigalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Parandhadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parandhadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Mounamum Nirambiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mounamum Nirambiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan Arugile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan Arugile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Ninaivin Paadhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Ninaivin Paadhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Meedhu Nadakkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meedhu Nadakkuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhil Mudhal Kaaladiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhil Mudhal Kaaladiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Sumakkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Sumakkuren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Nanban Podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Nanban Podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Endru Nee Korinaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endru Nee Korinaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Unthol Saaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unthol Saaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakkam Vandhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkam Vandhen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pakkam Vara Thudikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkam Vara Thudikkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakkam Vara Thudikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkam Vara Thudikkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Marandhadhe Nodigalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marandhadhe Nodigalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Parandhadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parandhadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Mounamum Nirambiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mounamum Nirambiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan Arugile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan Arugile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Marandhadhe Nodigalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marandhadhe Nodigalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Parandhadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parandhadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Mounamum Nirambiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mounamum Nirambiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan Arugile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan Arugile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanmoodum Nerathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanmoodum Nerathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanava Vandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanava Vandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavalaiya Marandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavalaiya Marandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Thoonginen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Thoonginen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thodarum Indha Kanavula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodarum Indha Kanavula"/>
</div>
<div class="lyrico-lyrics-wrapper">Nejatha Thedi Alaigiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nejatha Thedi Alaigiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Nodigal Kandthu Pogave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nodigal Kandthu Pogave"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Endhan Arugil Vendume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Endhan Arugil Vendume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Nanban Podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Nanban Podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Endru Nee Korinaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endru Nee Korinaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Unthol Saaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unthol Saaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakkam Vandhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkam Vandhen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pakkam Vara Thudikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkam Vara Thudikkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakkam Vara Thudikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkam Vara Thudikkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Marandhadhe Nodigalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marandhadhe Nodigalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Parandhadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parandhadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Mounamum Nirambiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mounamum Nirambiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan Arugile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan Arugile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Marandhadhe Nodigalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marandhadhe Nodigalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Parandhadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parandhadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Mounamum Nirambiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mounamum Nirambiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan Arugile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan Arugile"/>
</div>
</pre>
