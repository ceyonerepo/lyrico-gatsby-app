---
title: "ada paavi ada paavi song lyrics"
album: "Maanik"
artist: "Dharan Kumar"
lyricist: "Mirchi Vijay"
director: "Martyn"
path: "/albums/maanik-lyrics"
song: "Ada Paavi Ada Paavi"
image: ../../images/albumart/maanik.jpg
date: 2019-01-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/YaeQwRZcpMM"
type: "love"
singers:
  - Shashaa Tirupati
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">ada paavi ada paavi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada paavi ada paavi"/>
</div>
<div class="lyrico-lyrics-wrapper">una paaka daily nan thudichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="una paaka daily nan thudichen"/>
</div>
<div class="lyrico-lyrics-wrapper">thimir unaku romba thigam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thimir unaku romba thigam"/>
</div>
<div class="lyrico-lyrics-wrapper">that's ok nan kaikorka kaathen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="that's ok nan kaikorka kaathen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">paarthum ne pakama poriye da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paarthum ne pakama poriye da"/>
</div>
<div class="lyrico-lyrics-wrapper">un munnae nan kanal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un munnae nan kanal"/>
</div>
<div class="lyrico-lyrics-wrapper">neer aagurenda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neer aagurenda"/>
</div>
<div class="lyrico-lyrics-wrapper">cutie baby nu kopidu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cutie baby nu kopidu da"/>
</div>
<div class="lyrico-lyrics-wrapper">yen photo'ca status'ah vechikoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yen photo'ca status'ah vechikoda"/>
</div>
<div class="lyrico-lyrics-wrapper">you are my vaazhvin artham da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="you are my vaazhvin artham da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ada paavi ada paavi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada paavi ada paavi"/>
</div>
<div class="lyrico-lyrics-wrapper">una paaka daily nan thudichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="una paaka daily nan thudichen"/>
</div>
<div class="lyrico-lyrics-wrapper">thimir unaku romba thigam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thimir unaku romba thigam"/>
</div>
<div class="lyrico-lyrics-wrapper">that's ok nan kaikorka kaathen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="that's ok nan kaikorka kaathen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">karupu kannaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karupu kannaney"/>
</div>
<div class="lyrico-lyrics-wrapper">vennai thirudaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vennai thirudaney"/>
</div>
<div class="lyrico-lyrics-wrapper">come and kidnap me darling
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="come and kidnap me darling"/>
</div>
<div class="lyrico-lyrics-wrapper">venga area police never calling
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="venga area police never calling"/>
</div>
<div class="lyrico-lyrics-wrapper">thanni illa maniplanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanni illa maniplanta"/>
</div>
<div class="lyrico-lyrics-wrapper">guide illa tourista ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="guide illa tourista ah"/>
</div>
<div class="lyrico-lyrics-wrapper">subtitle illatha korean padama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="subtitle illatha korean padama"/>
</div>
<div class="lyrico-lyrics-wrapper">unna paththadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna paththadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">yen kuthama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yen kuthama"/>
</div>
<div class="lyrico-lyrics-wrapper">sollu da yen mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sollu da yen mama"/>
</div>
<div class="lyrico-lyrics-wrapper">main tum se pyaar karthi hoon
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="main tum se pyaar karthi hoon"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ada paavi ada paavi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada paavi ada paavi"/>
</div>
<div class="lyrico-lyrics-wrapper">una paaka daily nan thudichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="una paaka daily nan thudichen"/>
</div>
<div class="lyrico-lyrics-wrapper">thimir unaku romba thigam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thimir unaku romba thigam"/>
</div>
<div class="lyrico-lyrics-wrapper">that's ok nan kaikorka kaathen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="that's ok nan kaikorka kaathen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kolathukul sura meenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kolathukul sura meenu"/>
</div>
<div class="lyrico-lyrics-wrapper">en mama sokku scene'u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en mama sokku scene'u"/>
</div>
<div class="lyrico-lyrics-wrapper">night karapanpoochi tholla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="night karapanpoochi tholla"/>
</div>
<div class="lyrico-lyrics-wrapper">naa pethu tharen pathu pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa pethu tharen pathu pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">unna nenachaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna nenachaney"/>
</div>
<div class="lyrico-lyrics-wrapper">pattu padicheney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pattu padicheney"/>
</div>
<div class="lyrico-lyrics-wrapper">thangame my thangamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thangame my thangamey"/>
</div>
<div class="lyrico-lyrics-wrapper">lyrics yellam nan thirudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lyrics yellam nan thirudi"/>
</div>
<div class="lyrico-lyrics-wrapper">paataga tharen irudhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paataga tharen irudhi"/>
</div>
<div class="lyrico-lyrics-wrapper">my dear kuthichanthathan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="my dear kuthichanthathan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ada paavi ada paavi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada paavi ada paavi"/>
</div>
<div class="lyrico-lyrics-wrapper">una paaka daily nan thudichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="una paaka daily nan thudichen"/>
</div>
<div class="lyrico-lyrics-wrapper">thimir unaku romba thigam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thimir unaku romba thigam"/>
</div>
<div class="lyrico-lyrics-wrapper">that's ok nan kaikorka kaathen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="that's ok nan kaikorka kaathen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">paarthum ne pakama poriye da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paarthum ne pakama poriye da"/>
</div>
<div class="lyrico-lyrics-wrapper">un munnae nan kanal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un munnae nan kanal"/>
</div>
<div class="lyrico-lyrics-wrapper">neer aagurenda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neer aagurenda"/>
</div>
<div class="lyrico-lyrics-wrapper">cutie baby nu kopidu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cutie baby nu kopidu da"/>
</div>
<div class="lyrico-lyrics-wrapper">yen photo'ca status'ah vechikoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yen photo'ca status'ah vechikoda"/>
</div>
<div class="lyrico-lyrics-wrapper">you are my vaazhvin artham da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="you are my vaazhvin artham da"/>
</div>
</pre>
