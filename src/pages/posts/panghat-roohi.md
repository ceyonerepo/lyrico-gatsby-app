---
title: "panghat song lyrics"
album: "Roohi"
artist: "Sachin-Jigar"
lyricist: "Amitabh Bhattacharya"
director: "Hardik Mehta"
path: "/albums/roohi-lyrics"
song: "Panghat"
image: ../../images/albumart/roohi.jpg
date: 2021-03-11
lang: hindi
youtubeLink: "https://www.youtube.com/embed/5GRbUnw64W0"
type: "celebration"
singers:
  - Asees Kaur
  - Divya Kumar
  - Sachin-Jigar
  - Mellow D
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Bahut kathin hai dagar panghat ki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bahut kathin hai dagar panghat ki"/>
</div>
<div class="lyrico-lyrics-wrapper">Bahut kathin hai dagar panghat ki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bahut kathin hai dagar panghat ki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Zulmi badi aukat pe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zulmi badi aukat pe"/>
</div>
<div class="lyrico-lyrics-wrapper">Yee raat aayi hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yee raat aayi hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mann to hai mera paawan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mann to hai mera paawan"/>
</div>
<div class="lyrico-lyrics-wrapper">Magar yauvan harjayi hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Magar yauvan harjayi hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho kaise ho milan baanke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho kaise ho milan baanke"/>
</div>
<div class="lyrico-lyrics-wrapper">Piya se jaan meri atki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Piya se jaan meri atki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bahut kathin hai dagar panghat ki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bahut kathin hai dagar panghat ki"/>
</div>
<div class="lyrico-lyrics-wrapper">Pan pan pan panghat ki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pan pan pan panghat ki"/>
</div>
<div class="lyrico-lyrics-wrapper">Pan pan pan panghat ki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pan pan pan panghat ki"/>
</div>
<div class="lyrico-lyrics-wrapper">Bahut kathin hai dagar panghat ki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bahut kathin hai dagar panghat ki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Panghat pe gatt gatt karke peeti rehti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panghat pe gatt gatt karke peeti rehti"/>
</div>
<div class="lyrico-lyrics-wrapper">Peeti rehti peeti rehti peeti rehti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peeti rehti peeti rehti peeti rehti"/>
</div>
<div class="lyrico-lyrics-wrapper">Sawan ya bin sajna kyun jeeti rehti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sawan ya bin sajna kyun jeeti rehti"/>
</div>
<div class="lyrico-lyrics-wrapper">Jeeti rehti jeeti rehti jeeti rehti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jeeti rehti jeeti rehti jeeti rehti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saiyan babu ji bambaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saiyan babu ji bambaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Teri butterfalaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teri butterfalaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Kab se baithi teri raah take
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kab se baithi teri raah take"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haiyya apne pyaar ki naiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haiyya apne pyaar ki naiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Beech bhanwar mein daiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Beech bhanwar mein daiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Paar laga jogan ke bhaag jage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paar laga jogan ke bhaag jage"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho meri phhuljhadiya re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho meri phhuljhadiya re"/>
</div>
<div class="lyrico-lyrics-wrapper">Sabar zara kariyo re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sabar zara kariyo re"/>
</div>
<div class="lyrico-lyrics-wrapper">Sabar karte huye kitna bharun main
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sabar karte huye kitna bharun main"/>
</div>
<div class="lyrico-lyrics-wrapper">Hand pump se matki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hand pump se matki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bahut kathin hai dagar panghat ki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bahut kathin hai dagar panghat ki"/>
</div>
<div class="lyrico-lyrics-wrapper">Pan pan pan panghat ki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pan pan pan panghat ki"/>
</div>
<div class="lyrico-lyrics-wrapper">Pan pan pan panghat ki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pan pan pan panghat ki"/>
</div>
<div class="lyrico-lyrics-wrapper">Bahut kathin hai dagar panghat ki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bahut kathin hai dagar panghat ki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pan pan pan panghat ki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pan pan pan panghat ki"/>
</div>
<div class="lyrico-lyrics-wrapper">Pan pan pan panghat ki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pan pan pan panghat ki"/>
</div>
<div class="lyrico-lyrics-wrapper">Bahut kathin hai dagar panghat ki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bahut kathin hai dagar panghat ki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Panghat pe gatt gatt karke peeti rehti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panghat pe gatt gatt karke peeti rehti"/>
</div>
<div class="lyrico-lyrics-wrapper">Peeti rehti peeti rehti peeti rehti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peeti rehti peeti rehti peeti rehti"/>
</div>
<div class="lyrico-lyrics-wrapper">Sawan ya bin sajna kyun jeeti rehti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sawan ya bin sajna kyun jeeti rehti"/>
</div>
<div class="lyrico-lyrics-wrapper">Jeeti rehti jeeti rehti jeeti rehti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jeeti rehti jeeti rehti jeeti rehti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pan pan pan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pan pan pan"/>
</div>
<div class="lyrico-lyrics-wrapper">Bahut kathin hai dagar panghat ki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bahut kathin hai dagar panghat ki"/>
</div>
<div class="lyrico-lyrics-wrapper">Pan pan pan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pan pan pan"/>
</div>
</pre>
