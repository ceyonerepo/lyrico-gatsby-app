---
title: "oh my aadhya song lyrics"
album: "Aadavallu Meeku Johaarlu"
artist: "Devi Sri Prasad"
lyricist: "Sri Mani"
director: "Kishore Tirumala"
path: "/albums/aadavallu-meeku-johaarlu-lyrics"
song: "Oh My Aadhya"
image: ../../images/albumart/aadavallu-meeku-johaarlu.jpg
date: 2022-03-04
lang: telugu
youtubeLink: "https://www.youtube.com/embed/pR0rq3iXRws"
type: "love"
singers:
  - Yazin Nizar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Oh My Aadhya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh My Aadhya"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuv Pakkana Unte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuv Pakkana Unte"/>
</div>
<div class="lyrico-lyrics-wrapper">Car Ayina Gitaarai Mogene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Car Ayina Gitaarai Mogene"/>
</div>
<div class="lyrico-lyrics-wrapper">O Mana Madhya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Mana Madhya"/>
</div>
<div class="lyrico-lyrics-wrapper">Distance Ye Thaggi Gear Ayina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Distance Ye Thaggi Gear Ayina"/>
</div>
<div class="lyrico-lyrics-wrapper">Pyaar Antu Palikene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pyaar Antu Palikene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oo Oo Tere Jaisa Koyi Nahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Oo Tere Jaisa Koyi Nahi"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Oo Mere Jaisa Diwana Nahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Oo Mere Jaisa Diwana Nahi"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Oo Route Ye Geesaa Prayaanaaniki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Oo Route Ye Geesaa Prayaanaaniki"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu Nenu Maathram Unde Chotuki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu Nenu Maathram Unde Chotuki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh My Aadhya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh My Aadhya"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuv Pakkana Unte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuv Pakkana Unte"/>
</div>
<div class="lyrico-lyrics-wrapper">Car Ayina Gitaarai Mogene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Car Ayina Gitaarai Mogene"/>
</div>
<div class="lyrico-lyrics-wrapper">O Mana Madhya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Mana Madhya"/>
</div>
<div class="lyrico-lyrics-wrapper">Distance Ye Thaggi Gear Ayina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Distance Ye Thaggi Gear Ayina"/>
</div>
<div class="lyrico-lyrics-wrapper">Pyaar Antu Palikene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pyaar Antu Palikene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Google Map Ke Dorakani Chotuke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Google Map Ke Dorakani Chotuke"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadavani Bandine Manatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadavani Bandine Manatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Week Day Saturday Bedhame Teliyani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Week Day Saturday Bedhame Teliyani"/>
</div>
<div class="lyrico-lyrics-wrapper">PlaceU Ne Vethakani Neetho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="PlaceU Ne Vethakani Neetho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saradaga Shikaaru Antu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saradaga Shikaaru Antu"/>
</div>
<div class="lyrico-lyrics-wrapper">Colombus Ye Kadhilaade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Colombus Ye Kadhilaade"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Desham Aa Desham Antu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Desham Aa Desham Antu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enno Kanipettaade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enno Kanipettaade"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanugondham Manamee Journey Lo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanugondham Manamee Journey Lo"/>
</div>
<div class="lyrico-lyrics-wrapper">O Love Desam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Love Desam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh My Aadhya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh My Aadhya"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuv Pakkana Unte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuv Pakkana Unte"/>
</div>
<div class="lyrico-lyrics-wrapper">Car Ayina Gitaarai Mogene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Car Ayina Gitaarai Mogene"/>
</div>
<div class="lyrico-lyrics-wrapper">O Mana Madhya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Mana Madhya"/>
</div>
<div class="lyrico-lyrics-wrapper">Distance Ye Thaggi Gear Ayina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Distance Ye Thaggi Gear Ayina"/>
</div>
<div class="lyrico-lyrics-wrapper">Pyaar Antu Palikene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pyaar Antu Palikene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vemana Padyame 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vemana Padyame "/>
</div>
<div class="lyrico-lyrics-wrapper">Shakespear Kaavyame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shakespear Kaavyame"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvu Em Cheppina Kavithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvu Em Cheppina Kavithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Last Ball Sixer 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Last Ball Sixer "/>
</div>
<div class="lyrico-lyrics-wrapper">Ye Shoor Shot Hitture
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Shoor Shot Hitture"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu Em Chesina Gelupe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu Em Chesina Gelupe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andamga Untaamantu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andamga Untaamantu "/>
</div>
<div class="lyrico-lyrics-wrapper">Evarevaro Antaare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evarevaro Antaare"/>
</div>
<div class="lyrico-lyrics-wrapper">Andampai Raasina Haiku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andampai Raasina Haiku "/>
</div>
<div class="lyrico-lyrics-wrapper">Lennenno Chadivaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lennenno Chadivaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Asalandam Ivaala Choosaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asalandam Ivaala Choosaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhi Nee Navve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhi Nee Navve"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh My Aadhya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh My Aadhya"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuv Pakkana Unte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuv Pakkana Unte"/>
</div>
<div class="lyrico-lyrics-wrapper">Car Ayina Gitaarai Mogene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Car Ayina Gitaarai Mogene"/>
</div>
<div class="lyrico-lyrics-wrapper">O Mana Madhya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Mana Madhya"/>
</div>
<div class="lyrico-lyrics-wrapper">Distance Ye Thaggi Gear Ayina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Distance Ye Thaggi Gear Ayina"/>
</div>
<div class="lyrico-lyrics-wrapper">Pyaar Antu Palikene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pyaar Antu Palikene"/>
</div>
</pre>
