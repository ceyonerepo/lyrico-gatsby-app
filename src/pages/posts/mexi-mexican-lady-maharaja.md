---
title: "mexi mexican lady song lyrics"
album: "Maharaja"
artist: "D. Imman"
lyricist: "Na. Muthukumar"
director: "D. Manoharan"
path: "/albums/maharaja-lyrics"
song: "Mexi Mexican Lady"
image: ../../images/albumart/maharaja.jpg
date: 2011-12-30
lang: tamil
youtubeLink: "https://www.youtube.com/embed/QxWELz__o10"
type: "happy"
singers:
  - Hariharan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Mexi mexikon lady
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mexi mexikon lady"/>
</div>
<div class="lyrico-lyrics-wrapper">en nenjikkulla nee vaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en nenjikkulla nee vaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">unnaal thaan en 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnaal thaan en "/>
</div>
<div class="lyrico-lyrics-wrapper">idhayam thudikkumadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhayam thudikkumadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mexi mexikon lady
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mexi mexikon lady"/>
</div>
<div class="lyrico-lyrics-wrapper">en moochikkulla nee vaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en moochikkulla nee vaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">unnaal thaan en ulagam suzhalumadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnaal thaan en ulagam suzhalumadi"/>
</div>
<div class="lyrico-lyrics-wrapper">un azhaginil imaigal gudhikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un azhaginil imaigal gudhikka"/>
</div>
<div class="lyrico-lyrics-wrapper">un aruginil nimidam inikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un aruginil nimidam inikka"/>
</div>
<div class="lyrico-lyrics-wrapper">thaniyaai thavippadhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaniyaai thavippadhu "/>
</div>
<div class="lyrico-lyrics-wrapper">kaadhalil kodumaiyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadhalil kodumaiyadi"/>
</div>
<div class="lyrico-lyrics-wrapper">en aasaigal unnidam muraikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en aasaigal unnidam muraikka"/>
</div>
<div class="lyrico-lyrics-wrapper">varum vaarthaigal vazhiyil vazhukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varum vaarthaigal vazhiyil vazhukka"/>
</div>
<div class="lyrico-lyrics-wrapper">mounam thaaney kaadhalin maranamadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mounam thaaney kaadhalin maranamadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mexi mexikon lady
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mexi mexikon lady"/>
</div>
<div class="lyrico-lyrics-wrapper">en nenjikkulla nee vaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en nenjikkulla nee vaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">unnaal thaan en 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnaal thaan en "/>
</div>
<div class="lyrico-lyrics-wrapper">idhayam thudikkumadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhayam thudikkumadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un pechai ketkathaan nenjam yengum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un pechai ketkathaan nenjam yengum"/>
</div>
<div class="lyrico-lyrics-wrapper">ennathaan seivadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennathaan seivadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">un paadhai thedi en kaalgal Odum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un paadhai thedi en kaalgal Odum"/>
</div>
<div class="lyrico-lyrics-wrapper">ennathaan seivadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennathaan seivadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">un moochukkaatru en aayul thedum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un moochukkaatru en aayul thedum"/>
</div>
<div class="lyrico-lyrics-wrapper">ennathaan seivadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennathaan seivadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">nee paarthum paarkkaamal poanaaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee paarthum paarkkaamal poanaaley"/>
</div>
<div class="lyrico-lyrics-wrapper">naan ennathaan seivadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan ennathaan seivadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">kattippoattu vaithaalkooda 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kattippoattu vaithaalkooda "/>
</div>
<div class="lyrico-lyrics-wrapper">manasu ketkavillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasu ketkavillai"/>
</div>
<div class="lyrico-lyrics-wrapper">vettivittu poagathaaney 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vettivittu poagathaaney "/>
</div>
<div class="lyrico-lyrics-wrapper">kaadhal nagamum illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadhal nagamum illai"/>
</div>
<div class="lyrico-lyrics-wrapper">poovai veroadu killaadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poovai veroadu killaadhey"/>
</div>
<div class="lyrico-lyrics-wrapper">ennai uyiroadu kollaadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennai uyiroadu kollaadhey"/>
</div>
<div class="lyrico-lyrics-wrapper">Odi ozhindhaalum naan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odi ozhindhaalum naan "/>
</div>
<div class="lyrico-lyrics-wrapper">undhan pinnaal varuveney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="undhan pinnaal varuveney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mexi mexikon lady
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mexi mexikon lady"/>
</div>
<div class="lyrico-lyrics-wrapper">en nenjikkulla nee vaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en nenjikkulla nee vaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">unnaal thaan en 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnaal thaan en "/>
</div>
<div class="lyrico-lyrics-wrapper">idhayam thudikkumadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhayam thudikkumadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yehey hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yehey hey hey"/>
</div>
<div class="lyrico-lyrics-wrapper">unnai tholaivil paarthaaley uyirin ulley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai tholaivil paarthaaley uyirin ulley"/>
</div>
<div class="lyrico-lyrics-wrapper">ennavo aanadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennavo aanadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">un thoalil saaindhaaley ninaivin ulley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un thoalil saaindhaaley ninaivin ulley"/>
</div>
<div class="lyrico-lyrics-wrapper">ennavo aanadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennavo aanadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">en kaadhal yekkangal paadhiyil penney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kaadhal yekkangal paadhiyil penney"/>
</div>
<div class="lyrico-lyrics-wrapper">ennavo aanadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennavo aanadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">adhu unnai kaangindra kanavippoala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adhu unnai kaangindra kanavippoala"/>
</div>
<div class="lyrico-lyrics-wrapper">ennavo aanadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennavo aanadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">unnai paarthaal pinnaal thaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai paarthaal pinnaal thaaney"/>
</div>
<div class="lyrico-lyrics-wrapper">naanum maarippoanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naanum maarippoanen"/>
</div>
<div class="lyrico-lyrics-wrapper">undhan idhayam mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="undhan idhayam mattum"/>
</div>
<div class="lyrico-lyrics-wrapper">yeno maaravillai penney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yeno maaravillai penney"/>
</div>
<div class="lyrico-lyrics-wrapper">maatram illaadha aal endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maatram illaadha aal endru"/>
</div>
<div class="lyrico-lyrics-wrapper">mannil yaarumthaan kidaiyaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mannil yaarumthaan kidaiyaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">undhan manam maarum naalaithaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="undhan manam maarum naalaithaan"/>
</div>
<div class="lyrico-lyrics-wrapper">endru enni kaathirundhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="endru enni kaathirundhen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mexi mexikon lady
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mexi mexikon lady"/>
</div>
<div class="lyrico-lyrics-wrapper">en nenjikkulla nee vaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en nenjikkulla nee vaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">unnaal thaan en 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnaal thaan en "/>
</div>
<div class="lyrico-lyrics-wrapper">idhayam thudikkumadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhayam thudikkumadi"/>
</div>
</pre>
