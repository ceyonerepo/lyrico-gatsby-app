---
title: "hey abbayi song lyrics"
album: "Sreekaram"
artist: "Mickey J. Meyer"
lyricist: "Krishna Kanth"
director: "Kishor B"
path: "/albums/sreekaram-lyrics"
song: "Hey abbayi - No No Vaddanna"
image: ../../images/albumart/sreekaram.jpg
date: 2021-03-11
lang: telugu
youtubeLink: "https://www.youtube.com/embed/bGSerzhd3QA"
type: "love"
singers:
  - Hymath
  - Nutana Mohan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">No no vaddhanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="No no vaddhanna"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninu follow chesthunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninu follow chesthunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedho roju yes antaavani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedho roju yes antaavani"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhure choosthunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhure choosthunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey po po pommanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey po po pommanna"/>
</div>
<div class="lyrico-lyrics-wrapper">Padigaape kaasthunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padigaape kaasthunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Girlfriend ayye moment kosam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Girlfriend ayye moment kosam"/>
</div>
<div class="lyrico-lyrics-wrapper">Plan ye vesthunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Plan ye vesthunna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sorry anna kshamisthaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sorry anna kshamisthaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Ney vintaana vastha emaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ney vintaana vastha emaina"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey abbai hey hey abbai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey abbai hey hey abbai"/>
</div>
<div class="lyrico-lyrics-wrapper">Inka posulu chaloyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inka posulu chaloyi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaastha itepu choodoyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaastha itepu choodoyi"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey abbai hey hey abbai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey abbai hey hey abbai"/>
</div>
<div class="lyrico-lyrics-wrapper">Siggento abbayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siggento abbayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeke muddhotichi pogotteyna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeke muddhotichi pogotteyna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Abbai, hey hey abbai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Abbai, hey hey abbai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenu chusthunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenu chusthunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Paruve theesesthunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paruve theesesthunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Poni papam vadilesthu unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poni papam vadilesthu unna"/>
</div>
<div class="lyrico-lyrics-wrapper">Needhe thappunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Needhe thappunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Innaallu thaggunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innaallu thaggunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Padane nenu vadhiley nannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padane nenu vadhiley nannu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aapey antunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aapey antunna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nuvvemanna vasthaananna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvemanna vasthaananna"/>
</div>
<div class="lyrico-lyrics-wrapper">Ney vintaana buddhiga aagamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ney vintaana buddhiga aagamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey ammayi hey hey ammayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey ammayi hey hey ammayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aapesey golantu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aapesey golantu"/>
</div>
<div class="lyrico-lyrics-wrapper">Inka elaaga cheppali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inka elaaga cheppali"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey ammayi hey hey ammayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey ammayi hey hey ammayi"/>
</div>
<div class="lyrico-lyrics-wrapper">O meedhe padipoyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O meedhe padipoyi"/>
</div>
<div class="lyrico-lyrics-wrapper">Itta colouring isthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Itta colouring isthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Cut chesey naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cut chesey naa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thege prema unna neepaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thege prema unna neepaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheap ayyaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheap ayyaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholichoopullone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholichoopullone"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasu needhe telusukunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu needhe telusukunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Ika appatninche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ika appatninche"/>
</div>
<div class="lyrico-lyrics-wrapper">Emaina neetho unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emaina neetho unna"/>
</div>
<div class="lyrico-lyrics-wrapper">Oka ninne ninne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oka ninne ninne"/>
</div>
<div class="lyrico-lyrics-wrapper">Thagina jodane oohisthunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thagina jodane oohisthunna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh nedani repani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh nedani repani"/>
</div>
<div class="lyrico-lyrics-wrapper">Entha kaalame aina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entha kaalame aina"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhi choodaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhi choodaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Okka matapai nenunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okka matapai nenunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Aina neekidhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aina neekidhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Artham ainanu kaakunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Artham ainanu kaakunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Asale ninnu vadhileponu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asale ninnu vadhileponu"/>
</div>
<div class="lyrico-lyrics-wrapper">Neetho paate nenunta hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neetho paate nenunta hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey abbai hey hey abbai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey abbai hey hey abbai"/>
</div>
<div class="lyrico-lyrics-wrapper">Inka posulu chaloyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inka posulu chaloyi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaastha itepu choodoyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaastha itepu choodoyi"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey abbai hey hey abbai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey abbai hey hey abbai"/>
</div>
<div class="lyrico-lyrics-wrapper">Siggento abbayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siggento abbayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeke muddhotichi pogotteyna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeke muddhotichi pogotteyna"/>
</div>
</pre>
