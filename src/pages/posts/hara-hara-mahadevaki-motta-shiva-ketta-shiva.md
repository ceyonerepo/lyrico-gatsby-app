---
title: "hara hara mahadevaki song lyrics"
album: "Motta Shiva Ketta Shiva"
artist: "Amresh Ganesh"
lyricist: "Sorkko"
director: "Sai Ramani"
path: "/albums/motta-shiva-ketta-shiva-lyrics"
song: "Hara Hara Mahadevaki"
image: ../../images/albumart/motta-shiva-ketta-shiva.jpg
date: 2017-03-09
lang: tamil
youtubeLink: "https://www.youtube.com/embed/yxaieZxwYzs"
type: "love"
singers:
  -	Amresh Ganesh
  - Padmalatha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hara hara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hara hara"/>
</div>
<div class="lyrico-lyrics-wrapper">Mahadevaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mahadevaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Swamyae cycle la poguthaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Swamyae cycle la poguthaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Poosarikku bullet ketkuthaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poosarikku bullet ketkuthaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Athavathu sollavarathu ennanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athavathu sollavarathu ennanna"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru nalla figure-ah madakanum na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru nalla figure-ah madakanum na"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru nalla figure-ah kuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru nalla figure-ah kuda"/>
</div>
<div class="lyrico-lyrics-wrapper">Close-ah palaguna thaan mudiyum ( Dialogue)
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Close-ah palaguna thaan mudiyum ( Dialogue)"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nethiyil patta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nethiyil patta"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaluthil kotta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaluthil kotta"/>
</div>
<div class="lyrico-lyrics-wrapper">Veluthu katta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veluthu katta"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaadi kitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadi kitta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pudhusaa style-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhusaa style-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Parakkudhu rail-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parakkudhu rail-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Adikkudhu veyilu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adikkudhu veyilu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaadi en kuyilu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadi en kuyilu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hara hara mahadevaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hara hara mahadevaki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hara hara mahadevaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hara hara mahadevaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Hara hara mahadevaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hara hara mahadevaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Hara hara mahadevaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hara hara mahadevaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Hara hara mahadevaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hara hara mahadevaki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hara hara mahadevaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hara hara mahadevaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Hara hara mahadevaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hara hara mahadevaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Hara hara mahadevaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hara hara mahadevaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Hara hara mahadevaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hara hara mahadevaki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey pajunu pa pa pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey pajunu pa pa pa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pajunu pa pa pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pajunu pa pa pa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pajunu pa pa pa pajnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pajunu pa pa pa pajnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ha pajunu pa pa pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ha pajunu pa pa pa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pajunu pa pa pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pajunu pa pa pa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pajunu pajunu  pajunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pajunu pajunu  pajunu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nethiyil patta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nethiyil patta"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaluthil kotta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaluthil kotta"/>
</div>
<div class="lyrico-lyrics-wrapper">Veluthu katta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veluthu katta"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaadi kitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadi kitta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pudhusaa style-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhusaa style-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Parakkudhu rail-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parakkudhu rail-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Adikkudhu veyilu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adikkudhu veyilu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaadi en kuyilu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadi en kuyilu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hara hara mahadevaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hara hara mahadevaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Hara hara mahadevaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hara hara mahadevaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Hara hara mahadevaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hara hara mahadevaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Hara hara mahadevaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hara hara mahadevaki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hara hara mahadevaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hara hara mahadevaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Hara hara mahadevaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hara hara mahadevaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Hara hara mahadevaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hara hara mahadevaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Hara hara mahadevaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hara hara mahadevaki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kokkarako
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kokkarako"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan sevakozhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan sevakozhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee pakkam vandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee pakkam vandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Semma jolly
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Semma jolly"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manna kindum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manna kindum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kozhi pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kozhi pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethaan enna kinda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethaan enna kinda"/>
</div>
<div class="lyrico-lyrics-wrapper">Paakkuriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paakkuriyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ayyayoo thudikkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyayoo thudikkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">En meesai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En meesai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuravikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuravikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Varum aasai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varum aasai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Michatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Michatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Uchchatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uchchatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalla sirippula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalla sirippula"/>
</div>
<div class="lyrico-lyrics-wrapper">Alli kodukkura 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alli kodukkura "/>
</div>
<div class="lyrico-lyrics-wrapper">yeh yeh yeh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yeh yeh yeh"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeh yeh yeh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh yeh yeh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hara hara mahadevaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hara hara mahadevaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Hara hara mahadevaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hara hara mahadevaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Hara hara mahadevaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hara hara mahadevaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Hara hara mahadevaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hara hara mahadevaki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hara hara mahadevaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hara hara mahadevaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Hara hara mahadevaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hara hara mahadevaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Hara hara mahadevaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hara hara mahadevaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Hara hara mahadevaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hara hara mahadevaki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey pajunu pa pa pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey pajunu pa pa pa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pajunu pa pa pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pajunu pa pa pa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pajunu pa pa pa pajnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pajunu pa pa pa pajnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ha pajunu pa pa pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ha pajunu pa pa pa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pajunu pa pa pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pajunu pa pa pa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pajunu pajunu pajunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pajunu pajunu pajunu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hoi hoi hoi hoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoi hoi hoi hoi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iruttu kadai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruttu kadai"/>
</div>
<div class="lyrico-lyrics-wrapper">Halwa di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Halwa di"/>
</div>
<div class="lyrico-lyrics-wrapper">Un iduppulathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un iduppulathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Irukkuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irukkuthadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Murattu paiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Murattu paiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Murukkaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Murukkaadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiruttu paiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiruttu paiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirudaathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirudaathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aiyayyo pournamiyin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyayyo pournamiyin"/>
</div>
<div class="lyrico-lyrics-wrapper">Vellai nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vellai nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakisthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakisthan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellai nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellai nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhiradiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhiradiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Saravediyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saravediyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adichi norukki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adichi norukki"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattaya kelappura 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattaya kelappura "/>
</div>
<div class="lyrico-lyrics-wrapper">ra ra ra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ra ra ra"/>
</div>
<div class="lyrico-lyrics-wrapper">Ra ra ra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ra ra ra"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hara hara mahadevaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hara hara mahadevaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Hara hara mahadevaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hara hara mahadevaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Hara hara mahadevaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hara hara mahadevaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Hara hara mahadevaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hara hara mahadevaki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hara hara mahadevaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hara hara mahadevaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Hara hara mahadevaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hara hara mahadevaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Hara hara mahadevaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hara hara mahadevaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Hara hara mahadevaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hara hara mahadevaki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey pajunu pa pa pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey pajunu pa pa pa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pajunu pa pa pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pajunu pa pa pa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pajunu pa pa pa pajnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pajunu pa pa pa pajnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ha pajunu pa pa pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ha pajunu pa pa pa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pajunu pa pa pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pajunu pa pa pa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pajunu pajunu  pajunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pajunu pajunu  pajunu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nethiyil patta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nethiyil patta"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaluthil kotta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaluthil kotta"/>
</div>
<div class="lyrico-lyrics-wrapper">Veluthu katta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veluthu katta"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaadi kitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadi kitta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pudhusaa style-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhusaa style-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Parakkudhu rail-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parakkudhu rail-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Adikkudhu veyilu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adikkudhu veyilu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaadi en kuyilu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadi en kuyilu"/>
</div>
<div class="lyrico-lyrics-wrapper">Le le le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Le le le"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hara hara mahadevaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hara hara mahadevaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Hara hara mahadevaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hara hara mahadevaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Hara hara mahadevaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hara hara mahadevaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Hara hara mahadevaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hara hara mahadevaki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey pajunu pa pa pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey pajunu pa pa pa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pajunu pa pa pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pajunu pa pa pa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pajunu pa pa pa pajnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pajunu pa pa pa pajnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ha pajunu pa pa pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ha pajunu pa pa pa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pajunu pa pa pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pajunu pa pa pa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pajunu pajunu  pajunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pajunu pajunu  pajunu"/>
</div>
</pre>
