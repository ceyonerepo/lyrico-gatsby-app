---
title: "voda voda dhooram korayala song lyrics"
album: "Mayakkam Enna"
artist: "G.V. Prakash Kumar"
lyricist: "Selvaraghavan - Dhanush"
director: "Selvaraghavan"
path: "/albums/mayakkam-enna-lyrics"
song: "Voda Voda Dhooram Korayala"
image: ../../images/albumart/mayakkam-enna.jpg
date: 2011-11-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/FWj7CCqb454"
type: "happy"
singers:
  - Dhanush
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Oda Oda Oda Dhooram Kuraiyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oda Oda Oda Dhooram Kuraiyala"/>
</div>
<div class="lyrico-lyrics-wrapper">Paada Paada Paada Paattum Mudiyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paada Paada Paada Paattum Mudiyala"/>
</div>
<div class="lyrico-lyrics-wrapper">Poga Poga Poga Onnum Puriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poga Poga Poga Onnum Puriyala"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaga Motham Onnum Velangala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaga Motham Onnum Velangala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Free-Ah Suthum Pothu Figure Illiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Free-Ah Suthum Pothu Figure Illiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudicha Figure-Um Ippo Free-Ah Illiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudicha Figure-Um Ippo Free-Ah Illiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaila Bat Irukku Bal-Lu Illiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaila Bat Irukku Bal-Lu Illiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Life Poora Indha Thollaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life Poora Indha Thollaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ulagamae Speed-Ah Odi Poguthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagamae Speed-Ah Odi Poguthu"/>
</div>
<div class="lyrico-lyrics-wrapper">En Vandi Puncture Aayi Nikkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Vandi Puncture Aayi Nikkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mokka Piece-Um Kooda Kindal Pannuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mokka Piece-Um Kooda Kindal Pannuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Saami Enna Bangam Pannuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saami Enna Bangam Pannuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Crack-Ah Maaritten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Crack-Ah Maaritten"/>
</div>
<div class="lyrico-lyrics-wrapper">Joker Aayitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Joker Aayitten"/>
</div>
<div class="lyrico-lyrics-wrapper">Gundu Chattiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundu Chattiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendu Kuthura Vandi Otturen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendu Kuthura Vandi Otturen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Beachul-A Thaniya Alanjen Alanjen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Beachul-A Thaniya Alanjen Alanjen"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadu Rottula Azhuthen Puranden Kizhinjen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadu Rottula Azhuthen Puranden Kizhinjen"/>
</div>
<div class="lyrico-lyrics-wrapper">Bharam Thangala Thangala Kalutha Nan Illiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bharam Thangala Thangala Kalutha Nan Illiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaanum Yerala Yerala Molama Sarukuranae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaanum Yerala Yerala Molama Sarukuranae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Crack-Ah Maaritten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Crack-Ah Maaritten"/>
</div>
<div class="lyrico-lyrics-wrapper">Joker Ayitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Joker Ayitten"/>
</div>
<div class="lyrico-lyrics-wrapper">Fuse-Uu Pona Pinn
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fuse-Uu Pona Pinn"/>
</div>
<div class="lyrico-lyrics-wrapper">Bulb-Bu Kana Switch-A Theduren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bulb-Bu Kana Switch-A Theduren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oda Oda Oda Dhooram Kuraiyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oda Oda Oda Dhooram Kuraiyala"/>
</div>
<div class="lyrico-lyrics-wrapper">Paada Paada Paada Paattum Mudiyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paada Paada Paada Paattum Mudiyala"/>
</div>
<div class="lyrico-lyrics-wrapper">Poga Poga Poga Onnum Puriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poga Poga Poga Onnum Puriyala"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaga Motham Onnum Velangala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaga Motham Onnum Velangala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
 <div class="lyrico-lyrics-wrapper">Free-Ah Suthum Pothu Figure Illiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Free-Ah Suthum Pothu Figure Illiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudicha Figure-Um Ippo Free-Ah Illiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudicha Figure-Um Ippo Free-Ah Illiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaila Bat Irukku Bal-Lu Illiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaila Bat Irukku Bal-Lu Illiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Life Poora Indha Thollaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life Poora Indha Thollaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
 <div class="lyrico-lyrics-wrapper">Nadu Raathiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadu Raathiri"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhunthen Paduthen Ezhunthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhunthen Paduthen Ezhunthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Maadhiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Maadhiri"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirichen Aluthen Sirichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirichen Aluthen Sirichen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meenaa Neendhuren Neendhuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meenaa Neendhuren Neendhuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadalum Seralayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadalum Seralayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Padaga Poguren Poguren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padaga Poguren Poguren"/>
</div>
<div class="lyrico-lyrics-wrapper">Karaiyum Seralayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karaiyum Seralayae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Crack-Ah Maaritten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Crack-Ah Maaritten"/>
</div>
<div class="lyrico-lyrics-wrapper">Joker Aayiten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Joker Aayiten"/>
</div>
<div class="lyrico-lyrics-wrapper">Kelvi Kettu Kettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelvi Kettu Kettu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kelvi Kuri Pola Nikkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelvi Kuri Pola Nikkuren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oda Oda Oda Dhooram Kuraiyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oda Oda Oda Dhooram Kuraiyala"/>
</div>
<div class="lyrico-lyrics-wrapper">Paada Paada Paada Paattum Mudiyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paada Paada Paada Paattum Mudiyala"/>
</div>
<div class="lyrico-lyrics-wrapper">Poga Poga Poga Onnum Puriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poga Poga Poga Onnum Puriyala"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaga Motham Onnum Velangala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaga Motham Onnum Velangala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Free-Ah Suthum Pothu Figure Illiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Free-Ah Suthum Pothu Figure Illiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudicha Figure-Um Ippo Free-Ah Illiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudicha Figure-Um Ippo Free-Ah Illiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaila Bat Irukku Bal-Lu Illiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaila Bat Irukku Bal-Lu Illiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Life Poora Indha Thollaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life Poora Indha Thollaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ulagamae Speed-Ah Odi Poguthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagamae Speed-Ah Odi Poguthu"/>
</div>
<div class="lyrico-lyrics-wrapper">En Vandi Puncture Aayi Nikkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Vandi Puncture Aayi Nikkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mokka Piece-Um Kooda Kindal Pannuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mokka Piece-Um Kooda Kindal Pannuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Saami Enna Bangam Pannuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saami Enna Bangam Pannuthu"/>
</div>
</pre>
