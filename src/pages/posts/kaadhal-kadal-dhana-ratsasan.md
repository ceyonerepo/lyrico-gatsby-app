---
title: "kaadhal kadal dhana song lyrics"
album: "Ratsasan"
artist: "Ghibran"
lyricist: "Umadevi"
director: "Ram Kumar"
path: "/albums/ratsasan-lyrics"
song: "Kaadhal Kadal Dhana"
image: ../../images/albumart/ratsasan.jpg
date: 2018-10-05
lang: tamil
youtubeLink: "https://www.youtube.com/embed/btmRyqGKF70"
type: "love"
singers:
  - Sathyaprakash
  - Chaitra Ambadipudi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kaadhal kadal dhana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal kadal dhana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuli dhana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuli dhana"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal vazhi dhana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal vazhi dhana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunai dhana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunai dhana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannoli polae enai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannoli polae enai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannimai polae anai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannimai polae anai"/>
</div>
<div class="lyrico-lyrics-wrapper">Annaiyai polae enai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annaiyai polae enai"/>
</div>
<div class="lyrico-lyrics-wrapper">Madiyanai melae pudhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madiyanai melae pudhai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal kadal dhana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal kadal dhana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuli dhana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuli dhana"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal vazhi dhana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal vazhi dhana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunai dhana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunai dhana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mmm mmmmmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mmm mmmmmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Mmmmmm mmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mmmmmm mmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaa aaa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaa aaa aaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaaa aaa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaa aaa aaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naam vazhavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam vazhavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Orr kaadhalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orr kaadhalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodukiren padaikiren oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodukiren padaikiren oh"/>
</div>
<div class="lyrico-lyrics-wrapper">Por kaalathin vaal polavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Por kaalathin vaal polavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thudikkiren thavikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thudikkiren thavikkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">En kaadhalai un kovathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kaadhalai un kovathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Rasikkiren rusikkiren oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rasikkiren rusikkiren oh"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee sayavae en tholgalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee sayavae en tholgalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Inaikkiren ninaikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inaikkiren ninaikkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aalilaa vaanil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalilaa vaanil"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanae nilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanae nilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee pogum paadhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee pogum paadhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Poven adaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poven adaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa ozhiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa ozhiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee dhaan vazhiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee dhaan vazhiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaalae naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaalae naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnodu naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnodu naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal kadal dhana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal kadal dhana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuli dhana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuli dhana"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal vazhi dhana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal vazhi dhana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunai dhana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunai dhana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannoli polae enai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannoli polae enai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannimai polae anai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannimai polae anai"/>
</div>
<div class="lyrico-lyrics-wrapper">Annaiyai polae enai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annaiyai polae enai"/>
</div>
<div class="lyrico-lyrics-wrapper">Madiyanai melae pudhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madiyanai melae pudhai"/>
</div>
</pre>
