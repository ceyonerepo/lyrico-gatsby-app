---
title: "inna mylu song lyrics"
album: "Lift"
artist: "Britto Michael"
lyricist: "Nishanth"
director: "Vineeth Varaprasad"
path: "/albums/lift-lyrics"
song: "Inna Mylu"
image: ../../images/albumart/lift.jpg
date: 2021-10-01
lang: tamil
youtubeLink: "https://www.youtube.com/embed/yoaUIyfudOw"
type: "happy"
singers:
  - Rajesh
  - Sivakarthikeyan
  - Poovaiyar
  - Kamala Kannan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Inna mylu Yaara paathuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inna mylu Yaara paathuu"/>
</div>
<div class="lyrico-lyrics-wrapper">Scene’a podu off avvadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Scene’a podu off avvadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Inna mayilae sirichikuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inna mayilae sirichikuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Inga vanthu orsikina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga vanthu orsikina"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaru kaila vechikina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaru kaila vechikina"/>
</div>
<div class="lyrico-lyrics-wrapper">Bangama maatniya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bangama maatniya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inna mayilae sirichikuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inna mayilae sirichikuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Inga vanthu orsikina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga vanthu orsikina"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaara paathu morchikina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaara paathu morchikina"/>
</div>
<div class="lyrico-lyrics-wrapper">Inga vathu olijikina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga vathu olijikina"/>
</div>
<div class="lyrico-lyrics-wrapper">Inna mylu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inna mylu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inna mayilae sirichikuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inna mayilae sirichikuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Annan pakkam orsikina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annan pakkam orsikina"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaara paathu morchikina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaara paathu morchikina"/>
</div>
<div class="lyrico-lyrics-wrapper">Inga vanthu olinjikuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga vanthu olinjikuna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inna mayilae sirichikuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inna mayilae sirichikuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Annan pakkam orsikina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annan pakkam orsikina"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaara paathu morchikina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaara paathu morchikina"/>
</div>
<div class="lyrico-lyrics-wrapper">Inga vanthu olinjikuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga vanthu olinjikuna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Please close the door
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Please close the door"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum avanum friendumba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum avanum friendumba"/>
</div>
<div class="lyrico-lyrics-wrapper">Roatulla paathaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Roatulla paathaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Time ilenba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Time ilenba"/>
</div>
<div class="lyrico-lyrics-wrapper">Miss you poduva statusla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Miss you poduva statusla"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaeta off late idhudhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaeta off late idhudhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Trendumba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Trendumba"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paadhi vaazhka phonukkulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadhi vaazhka phonukkulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Meedhi vaazhkai loanukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meedhi vaazhkai loanukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee podra scene over ah illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee podra scene over ah illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee vaangi potadellam EMI la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee vaangi potadellam EMI la"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inna mayile EMI La
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inna mayile EMI La"/>
</div>
<div class="lyrico-lyrics-wrapper">Inna mayile EMI La
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inna mayile EMI La"/>
</div>
<div class="lyrico-lyrics-wrapper">Inna mayile EMI La
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inna mayile EMI La"/>
</div>
<div class="lyrico-lyrics-wrapper">Car EMI Phone EMI
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Car EMI Phone EMI"/>
</div>
<div class="lyrico-lyrics-wrapper">Vitu EMI cheddi kuda endhilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vitu EMI cheddi kuda endhilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Aana onnuda velaiyuna gunnuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aana onnuda velaiyuna gunnuda"/>
</div>
<div class="lyrico-lyrics-wrapper">Suthi noru ponnuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suthi noru ponnuda"/>
</div>
<div class="lyrico-lyrics-wrapper">Maman mela kannuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maman mela kannuda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aayiram friends online la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayiram friends online la"/>
</div>
<div class="lyrico-lyrics-wrapper">Odhavinu koopta oruthanula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odhavinu koopta oruthanula"/>
</div>
<div class="lyrico-lyrics-wrapper">Kootunu varta naan pullingala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kootunu varta naan pullingala"/>
</div>
<div class="lyrico-lyrics-wrapper">Maasu kaatlama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maasu kaatlama"/>
</div>
<div class="lyrico-lyrics-wrapper">Friendship-la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friendship-la"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gili gili gili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gili gili gili"/>
</div>
<div class="lyrico-lyrics-wrapper">Gujili gujili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gujili gujili"/>
</div>
<div class="lyrico-lyrics-wrapper">Gili gili gili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gili gili gili"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gili gili gili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gili gili gili"/>
</div>
<div class="lyrico-lyrics-wrapper">Gujili gujili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gujili gujili"/>
</div>
<div class="lyrico-lyrics-wrapper">Gili gili gili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gili gili gili"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gili gili gili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gili gili gili"/>
</div>
<div class="lyrico-lyrics-wrapper">Gujili gujili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gujili gujili"/>
</div>
<div class="lyrico-lyrics-wrapper">Gili gili gili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gili gili gili"/>
</div>
<div class="lyrico-lyrics-wrapper">Gujili gujili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gujili gujili"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adinga vaaya moodra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adinga vaaya moodra"/>
</div>
<div class="lyrico-lyrics-wrapper">En vaanaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En vaanaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee romba gunjaaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee romba gunjaaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Goonuzhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Goonuzhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nyaayama pesanumba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nyaayama pesanumba"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaranaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaranaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Dai neeyella naatama-na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dai neeyella naatama-na"/>
</div>
<div class="lyrico-lyrics-wrapper">Daara keenjurum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daara keenjurum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey neranketta nerathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey neranketta nerathula"/>
</div>
<div class="lyrico-lyrics-wrapper">Like’a potu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Like’a potu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee number mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee number mattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaangu machi noolavittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaangu machi noolavittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava nambitaana paathikaati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava nambitaana paathikaati"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalikattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalikattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeye aapula yeri ukkandhuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeye aapula yeri ukkandhuko"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasapattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasapattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey pakkathootu panjayathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey pakkathootu panjayathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Orangkattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orangkattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee ottukekka nikkathada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee ottukekka nikkathada"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetkamkettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetkamkettu"/>
</div>
<div class="lyrico-lyrics-wrapper">Polambaadha panjagatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Polambaadha panjagatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattikittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattikittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ipo maatikita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ipo maatikita"/>
</div>
<div class="lyrico-lyrics-wrapper">Sethula dhaan kaalavittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sethula dhaan kaalavittu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maaman machan rendu perum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaman machan rendu perum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pechuna puyalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pechuna puyalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaya vechu vadasuttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaya vechu vadasuttu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ootuvaanuva railu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ootuvaanuva railu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thamizhumnnu pechedutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thamizhumnnu pechedutha"/>
</div>
<div class="lyrico-lyrics-wrapper">Parakkudhu analu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parakkudhu analu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ungokka petha pulla rendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ungokka petha pulla rendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Tamil ah failu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tamil ah failu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inna mylu tamil-ah failu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inna mylu tamil-ah failu"/>
</div>
<div class="lyrico-lyrics-wrapper">Inna mylu ata tamil-ah failu abba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inna mylu ata tamil-ah failu abba"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inna mayilae sirichikuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inna mayilae sirichikuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Annan pakkam orsikina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annan pakkam orsikina"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaara paathu morchikina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaara paathu morchikina"/>
</div>
<div class="lyrico-lyrics-wrapper">Inga vanthu olinjikuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga vanthu olinjikuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Inna mylu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inna mylu"/>
</div>
</pre>
