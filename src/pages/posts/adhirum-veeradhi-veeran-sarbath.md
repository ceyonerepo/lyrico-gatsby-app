---
title: "adhirum veeradhi veeran song lyrics"
album: "Sarbath"
artist: "Ajesh"
lyricist: "Muthamil"
director: "Prabhakaran"
path: "/albums/sarbath-song-lyrics"
song: "Adhirum Veeradhi Veeran"
image: ../../images/albumart/sarbath.jpg
date: 2021-04-11
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Zq2qSMtpBeo"
type: "Introduction"
singers:
  - Ajesh
  - Mahalingam
  - Diwakar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Adhirum veeradhi veeran vandhane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhirum veeradhi veeran vandhane"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudhura thozh mela yeri vandhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudhura thozh mela yeri vandhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Aruma seerellam yendhi ninnane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aruma seerellam yendhi ninnane"/>
</div>
<div class="lyrico-lyrics-wrapper">Perumai serthu thaan vaangi thandhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perumai serthu thaan vaangi thandhan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Annaadam paaraattuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annaadam paaraattuvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbaale aaraattuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbaale aaraattuvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Angali pangaliya anjama thaan kekkurom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Angali pangaliya anjama thaan kekkurom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ooru velainjidunum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooru velainjidunum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naadu sezhichudanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naadu sezhichudanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Perum perugidanum aara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perum perugidanum aara"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalum mana urudhi thaana thara venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalum mana urudhi thaana thara venum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aramaga varamaga nalamum valamum tharanum tharanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aramaga varamaga nalamum valamum tharanum tharanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Annaadam paaraattuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annaadam paaraattuvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbaale aaraattuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbaale aaraattuvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Angali pangaliya anjama thaan kekkurom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Angali pangaliya anjama thaan kekkurom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Murukku meesai adha surutti thaan thookki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Murukku meesai adha surutti thaan thookki"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai verichuthaan paakkume vedala kaalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai verichuthaan paakkume vedala kaalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Adakki vecha pala aasaiya serthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adakki vecha pala aasaiya serthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu kettu thaan verattu aala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu kettu thaan verattu aala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vayasu pulla vai peche illama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayasu pulla vai peche illama"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru kannala kanjaada kaatti pesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru kannala kanjaada kaatti pesum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhiya kaatti muttayathaan kettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhiya kaatti muttayathaan kettu"/>
</div>
<div class="lyrico-lyrics-wrapper">Siru vaandellam vandhu nikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru vaandellam vandhu nikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjodu nambikkai nee oottanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjodu nambikkai nee oottanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Panjatha nee pokkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panjatha nee pokkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannellam ponnaga nee aakkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannellam ponnaga nee aakkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Makkala thaan kaakkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Makkala thaan kaakkanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unmaikku per sollum oora aaganum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmaikku per sollum oora aaganum"/>
</div>
<div class="lyrico-lyrics-wrapper">Onnaga thaan vaazhanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnaga thaan vaazhanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Onnaga ellame undaaganum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnaga ellame undaaganum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorukku nalvaaku nee kooranum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorukku nalvaaku nee kooranum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ooru velainjidunum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooru velainjidunum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naadu sezhichudanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naadu sezhichudanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Perum perugidanum aara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perum perugidanum aara"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalum mana urudhi thaana thara venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalum mana urudhi thaana thara venum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aramaga varamaga nalamum valamum tharanum tharanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aramaga varamaga nalamum valamum tharanum tharanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Annaadam paaraattuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annaadam paaraattuvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbaale aaraattuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbaale aaraattuvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Angali pangaliya anjama thaan kekkurom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Angali pangaliya anjama thaan kekkurom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Annaadam paaraattuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annaadam paaraattuvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbaale aaraattuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbaale aaraattuvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Angali pangaliya anjama thaan kekkurom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Angali pangaliya anjama thaan kekkurom"/>
</div>
</pre>
