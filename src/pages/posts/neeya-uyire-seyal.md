---
title: "neeya uyire song lyrics"
album: "Seyal"
artist: "Siddharth Vipin"
lyricist: "Siddharth Vipin"
director: "Ravi Abbulu"
path: "/albums/seyal-lyrics"
song: "Neeya Uyire"
image: ../../images/albumart/seyal.jpg
date: 2018-05-18
lang: tamil
youtubeLink: "https://www.youtube.com/embed/s2npOcaPLLE"
type: "love"
singers:
  - Chinmayi
  - Karthik
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">neeya uyire uyir 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeya uyire uyir "/>
</div>
<div class="lyrico-lyrics-wrapper">thedum uyir neeya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thedum uyir neeya "/>
</div>
<div class="lyrico-lyrics-wrapper">neeya nilave tholaivil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeya nilave tholaivil"/>
</div>
<div class="lyrico-lyrics-wrapper">poyum thoduvaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poyum thoduvaya"/>
</div>
<div class="lyrico-lyrics-wrapper">neeya veesum kaatru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeya veesum kaatru"/>
</div>
<div class="lyrico-lyrics-wrapper">vaasam nee thanana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaasam nee thanana"/>
</div>
<div class="lyrico-lyrics-wrapper">pesum kannil vaarthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pesum kannil vaarthai"/>
</div>
<div class="lyrico-lyrics-wrapper">nee thaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee thaana"/>
</div>
<div class="lyrico-lyrics-wrapper">pesathirunthal mounam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pesathirunthal mounam"/>
</div>
<div class="lyrico-lyrics-wrapper">nee thana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee thana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">imaiyoram nee vizhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="imaiyoram nee vizhi"/>
</div>
<div class="lyrico-lyrics-wrapper">imaithidum pothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="imaithidum pothu"/>
</div>
<div class="lyrico-lyrics-wrapper">inaipiriyamal irukum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inaipiriyamal irukum"/>
</div>
<div class="lyrico-lyrics-wrapper">kaatchi neeya en
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatchi neeya en"/>
</div>
<div class="lyrico-lyrics-wrapper">idhayathin ooram 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhayathin ooram "/>
</div>
<div class="lyrico-lyrics-wrapper">thaatu paarkum isai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaatu paarkum isai"/>
</div>
<div class="lyrico-lyrics-wrapper">oosai neeya un parvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oosai neeya un parvai"/>
</div>
<div class="lyrico-lyrics-wrapper">konda porvai aagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konda porvai aagum"/>
</div>
<div class="lyrico-lyrics-wrapper">kulir kaatru neeya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulir kaatru neeya"/>
</div>
<div class="lyrico-lyrics-wrapper">en uthatin meethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en uthatin meethu"/>
</div>
<div class="lyrico-lyrics-wrapper">ukarthirukum pani neeya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ukarthirukum pani neeya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kadhal thee athu neeya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhal thee athu neeya"/>
</div>
<div class="lyrico-lyrics-wrapper">neeya theenda theenda 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeya theenda theenda "/>
</div>
<div class="lyrico-lyrics-wrapper">kadhal nee thana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhal nee thana"/>
</div>
<div class="lyrico-lyrics-wrapper">imai modum pagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="imai modum pagam"/>
</div>
<div class="lyrico-lyrics-wrapper">nee thana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee thana"/>
</div>
<div class="lyrico-lyrics-wrapper">thisaiye ellam theriyuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thisaiye ellam theriyuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">nee thana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee thana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ninaivellam neeya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninaivellam neeya"/>
</div>
<div class="lyrico-lyrics-wrapper">nan partha naalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan partha naalum"/>
</div>
<div class="lyrico-lyrics-wrapper">alagiya naale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alagiya naale"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhal kaalam neeya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhal kaalam neeya"/>
</div>
<div class="lyrico-lyrics-wrapper">en ulagathil oram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en ulagathil oram"/>
</div>
<div class="lyrico-lyrics-wrapper">ellai thandi nulaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellai thandi nulaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">kaalgal neeya en
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaalgal neeya en"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhil vanthu kadhaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhil vanthu kadhaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">sollum siriposai neeya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sollum siriposai neeya"/>
</div>
<div class="lyrico-lyrics-wrapper">en iravil kooda ennodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en iravil kooda ennodu"/>
</div>
<div class="lyrico-lyrics-wrapper">irukum udal neeya neeya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irukum udal neeya neeya"/>
</div>
<div class="lyrico-lyrics-wrapper">pagal neeya irul neeya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pagal neeya irul neeya"/>
</div>
<div class="lyrico-lyrics-wrapper">neeya muthin ulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeya muthin ulla"/>
</div>
<div class="lyrico-lyrics-wrapper">venmeen nee thana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="venmeen nee thana"/>
</div>
<div class="lyrico-lyrics-wrapper">ennai theendum megam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennai theendum megam"/>
</div>
<div class="lyrico-lyrics-wrapper">nee thana ennai thottal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee thana ennai thottal"/>
</div>
<div class="lyrico-lyrics-wrapper">ellam nee thane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellam nee thane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">neeya uyire uyir 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeya uyire uyir "/>
</div>
<div class="lyrico-lyrics-wrapper">thedum uyir neeya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thedum uyir neeya "/>
</div>
</pre>
