---
title: "nee jathai song lyrics"
album: "Amaram Akhilam Prema"
artist: "Radhan"
lyricist: "Rehman"
director: "Jonathan Edwards"
path: "/albums/amaram-akhilam-prema-lyrics"
song: "Nee Jathai"
image: ../../images/albumart/amaram-akhilam-prema.jpg
date: 2020-09-18
lang: telugu
youtubeLink: "https://www.youtube.com/embed/uts0mKs5nVs"
type: "love"
singers:
  - Babo Sashi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">nee jathai nadichina dharule ika mugisina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee jathai nadichina dharule ika mugisina"/>
</div>
<div class="lyrico-lyrics-wrapper">pakkake jarigene ikkade ika nilichina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pakkake jarigene ikkade ika nilichina"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nalo nindi unna nee guruthule naake kotha nesthaaluga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nalo nindi unna nee guruthule naake kotha nesthaaluga"/>
</div>
<div class="lyrico-lyrics-wrapper">ila undiponi o lokamai nalo nannu nee guruthuga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ila undiponi o lokamai nalo nannu nee guruthuga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">edhuruga lekunna evariki varaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edhuruga lekunna evariki varaina"/>
</div>
<div class="lyrico-lyrics-wrapper">manasuna nee sthanam mayam kaadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasuna nee sthanam mayam kaadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">thelupadhu neekaina maruvadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thelupadhu neekaina maruvadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">lolona perigina ee dhuram baram kaadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lolona perigina ee dhuram baram kaadhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nuvvu leni okka kshaname 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvvu leni okka kshaname "/>
</div>
<div class="lyrico-lyrics-wrapper">gundekalavatu cheyyanikapai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gundekalavatu cheyyanikapai"/>
</div>
<div class="lyrico-lyrics-wrapper">nenu malle nannu nake 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenu malle nannu nake "/>
</div>
<div class="lyrico-lyrics-wrapper">patti nerpani jeevithamidhani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="patti nerpani jeevithamidhani"/>
</div>
<div class="lyrico-lyrics-wrapper">e alalaina jada leni 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="e alalaina jada leni "/>
</div>
<div class="lyrico-lyrics-wrapper">sandramalle lolopala nenu odhiga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sandramalle lolopala nenu odhiga"/>
</div>
<div class="lyrico-lyrics-wrapper">chenthaunna andhaleni ninna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chenthaunna andhaleni ninna "/>
</div>
<div class="lyrico-lyrics-wrapper">naku thodugane gadipesthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naku thodugane gadipesthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">edhuruga lekunna evariki varaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edhuruga lekunna evariki varaina"/>
</div>
<div class="lyrico-lyrics-wrapper">manasuna nee sthanam mayam kaadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasuna nee sthanam mayam kaadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">thelupadhu neekaina maruvadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thelupadhu neekaina maruvadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">lolona perigina ee dhuram baram kaadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lolona perigina ee dhuram baram kaadhe"/>
</div>
</pre>