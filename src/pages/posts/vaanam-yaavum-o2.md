---
title: "vaanam yaavum song lyrics"
album: "O2"
artist: "Vishal Chandrasekar"
lyricist: "Dharan KR"
director: "GS Viknesh"
path: "/albums/o2-lyrics"
song: "Vaanam Yaavum"
image: ../../images/albumart/o2.jpg
date: 2022-06-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/OXggGgN1wlU"
type: "melody"
singers:
  - Pradeep Kumar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">vaanam yaavum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaanam yaavum "/>
</div>
<div class="lyrico-lyrics-wrapper">vaasal vandhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaasal vandhu "/>
</div>
<div class="lyrico-lyrics-wrapper">vaasam veesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaasam veesi"/>
</div>
<div class="lyrico-lyrics-wrapper">azhaikirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="azhaikirathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaatrin mele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatrin mele"/>
</div>
<div class="lyrico-lyrics-wrapper">paravai pole
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paravai pole"/>
</div>
<div class="lyrico-lyrics-wrapper">ithayam sernthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithayam sernthu"/>
</div>
<div class="lyrico-lyrics-wrapper">parakirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="parakirathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">irul irul ullarathan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irul irul ullarathan"/>
</div>
<div class="lyrico-lyrics-wrapper">oli onnu thangirukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oli onnu thangirukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">mella mella paathaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mella mella paathaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">thaanaga thondrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaanaga thondrum"/>
</div>
<div class="lyrico-lyrics-wrapper">nedham nedham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nedham nedham"/>
</div>
<div class="lyrico-lyrics-wrapper">santhosatha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="santhosatha "/>
</div>
<div class="lyrico-lyrics-wrapper">kotti kotti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kotti kotti"/>
</div>
<div class="lyrico-lyrics-wrapper">kondaduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kondaduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">pattam poochi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pattam poochi"/>
</div>
<div class="lyrico-lyrics-wrapper">mul meedhum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mul meedhum "/>
</div>
<div class="lyrico-lyrics-wrapper">ekkaalam podum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ekkaalam podum"/>
</div>
<div class="lyrico-lyrics-wrapper">neelum paadhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neelum paadhai"/>
</div>
<div class="lyrico-lyrics-wrapper">veliya nilama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veliya nilama"/>
</div>
<div class="lyrico-lyrics-wrapper">kaatra kadala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatra kadala"/>
</div>
<div class="lyrico-lyrics-wrapper">theriyathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theriyathe"/>
</div>
<div class="lyrico-lyrics-wrapper">pogum payanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pogum payanam"/>
</div>
<div class="lyrico-lyrics-wrapper">pogum pokkil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pogum pokkil"/>
</div>
<div class="lyrico-lyrics-wrapper">artham thanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="artham thanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">anaikirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anaikirathe"/>
</div>
<div class="lyrico-lyrics-wrapper">thaanai oru maatram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaanai oru maatram"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjil pirakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjil pirakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">thiravaa thisaiyellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thiravaa thisaiyellam"/>
</div>
<div class="lyrico-lyrics-wrapper">anbil thirakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anbil thirakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">thirakkum nadakum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thirakkum nadakum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaathodu dhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaathodu dhan"/>
</div>
<div class="lyrico-lyrics-wrapper">kaathaga medhapom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaathaga medhapom"/>
</div>
<div class="lyrico-lyrics-wrapper">neerodu than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neerodu than"/>
</div>
<div class="lyrico-lyrics-wrapper">neeraaga kalapom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeraaga kalapom"/>
</div>
<div class="lyrico-lyrics-wrapper">iyakaiyin madiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iyakaiyin madiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">konjam vaa sombal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konjam vaa sombal"/>
</div>
<div class="lyrico-lyrics-wrapper">muripom 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muripom "/>
</div>
<div class="lyrico-lyrics-wrapper">malaithuliyo suduveyilo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malaithuliyo suduveyilo"/>
</div>
<div class="lyrico-lyrics-wrapper">suvaikira nenjam pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suvaikira nenjam pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">payanam uyirootuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="payanam uyirootuthey"/>
</div>
<div class="lyrico-lyrics-wrapper">kaalin idukile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaalin idukile"/>
</div>
<div class="lyrico-lyrics-wrapper">kaalam oduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaalam oduthe"/>
</div>
<div class="lyrico-lyrics-wrapper">anbin nilalile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anbin nilalile"/>
</div>
<div class="lyrico-lyrics-wrapper">vaazhvu neeluthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaazhvu neeluthe"/>
</div>
<div class="lyrico-lyrics-wrapper">thaanai oru maatram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaanai oru maatram"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjil pirakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjil pirakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">thiravaa thisaiyellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thiravaa thisaiyellam"/>
</div>
<div class="lyrico-lyrics-wrapper">anbil thirakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anbil thirakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">thirakkum nadakum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thirakkum nadakum"/>
</div>
</pre>
