---
title: "aaru padayappaa song lyrics"
album: "Sagakkal"
artist: "Thayarathnam"
lyricist: "Yugabharathi"
director: "L. Muthukumaraswamy"
path: "/albums/sagakkal-lyrics"
song: "Aaru Padayappaa"
image: ../../images/albumart/sagakkal.jpg
date: 2011-08-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Dhrhn_5Pcfg"
type: "happy"
singers:
  - Haricharan
  - Mukesh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aarubadaiyappa anbaaley ulagai aalbavare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aarubadaiyappa anbaaley ulagai aalbavare"/>
</div>
<div class="lyrico-lyrics-wrapper">pazhaniyappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pazhaniyappa"/>
</div>
<div class="lyrico-lyrics-wrapper">pazhamudhirum soalai nadandhuvandhoam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pazhamudhirum soalai nadandhuvandhoam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aarubadaiyappa anbaaley ulagai aalbavare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aarubadaiyappa anbaaley ulagai aalbavare"/>
</div>
<div class="lyrico-lyrics-wrapper">pazhaniyappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pazhaniyappa"/>
</div>
<div class="lyrico-lyrics-wrapper">pazhamudhirum soalai nadandhuvandhoam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pazhamudhirum soalai nadandhuvandhoam"/>
</div>
<div class="lyrico-lyrics-wrapper">vinaiyellaam nodiyil theerikkum ulagin mudhalvaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vinaiyellaam nodiyil theerikkum ulagin mudhalvaa"/>
</div>
<div class="lyrico-lyrics-wrapper">unaikkaana udaney serndhu varuvoam kuzhuvaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unaikkaana udaney serndhu varuvoam kuzhuvaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Sivan maganey arogaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sivan maganey arogaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Shanmuganey arogaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shanmuganey arogaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nagaimuganey arogaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nagaimuganey arogaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thavayugane arogaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thavayugane arogaraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aarubadaiyappa anbaaley ulagai aalbavare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aarubadaiyappa anbaaley ulagai aalbavare"/>
</div>
<div class="lyrico-lyrics-wrapper">pazhaniyappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pazhaniyappa"/>
</div>
<div class="lyrico-lyrics-wrapper">pazhamudhirum soalai nadandhuvandhoam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pazhamudhirum soalai nadandhuvandhoam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Malaiyeri varuvoarkkellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malaiyeri varuvoarkkellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">maravaamal tharuvaai nanmai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maravaamal tharuvaai nanmai"/>
</div>
<div class="lyrico-lyrics-wrapper">adhai naangal arivoamadaa murugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adhai naangal arivoamadaa murugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">manadhodu nuzhaindha kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manadhodu nuzhaindha kaadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">maraiyaamal inidhey sera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maraiyaamal inidhey sera"/>
</div>
<div class="lyrico-lyrics-wrapper">arul neeyum purivaayadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arul neeyum purivaayadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">thiruthani malaiyaa chinnappayalgal solluvadhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thiruthani malaiyaa chinnappayalgal solluvadhai"/>
</div>
<div class="lyrico-lyrics-wrapper">nee seviyil ketkaadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee seviyil ketkaadhey"/>
</div>
<div class="lyrico-lyrics-wrapper">unmai illaa kaadhal edhaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unmai illaa kaadhal edhaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">aravey serkkaadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aravey serkkaadhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sivan maganey arogaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sivan maganey arogaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Shanmuganey arogaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shanmuganey arogaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nagaimuganey arogaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nagaimuganey arogaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thavayugane arogaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thavayugane arogaraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aarubadaiyappa anbaaley ulagai aalbavare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aarubadaiyappa anbaaley ulagai aalbavare"/>
</div>
<div class="lyrico-lyrics-wrapper">pazhaniyappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pazhaniyappa"/>
</div>
<div class="lyrico-lyrics-wrapper">pazhamudhirum soalai nadandhuvandhoam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pazhamudhirum soalai nadandhuvandhoam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thiruneerai iduvoarkkellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiruneerai iduvoarkkellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">panam koadi tharuvaai endrey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="panam koadi tharuvaai endrey"/>
</div>
<div class="lyrico-lyrics-wrapper">unaithedi adaivoamadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unaithedi adaivoamadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">kumara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kumara"/>
</div>
<div class="lyrico-lyrics-wrapper">uyirkkaadhal uravai neekki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyirkkaadhal uravai neekki"/>
</div>
<div class="lyrico-lyrics-wrapper">udal serndha natpai poatrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="udal serndha natpai poatrum"/>
</div>
<div class="lyrico-lyrics-wrapper">ivan soagamthanai theeradaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ivan soagamthanai theeradaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paththu malai murugaa sollaal thuyaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paththu malai murugaa sollaal thuyaram"/>
</div>
<div class="lyrico-lyrics-wrapper">idhayathukkulley anindhen pala vesham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhayathukkulley anindhen pala vesham"/>
</div>
<div class="lyrico-lyrics-wrapper">illaa uravai inaiyakkettu iduvaar karagoasham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="illaa uravai inaiyakkettu iduvaar karagoasham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sivan maganey arogaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sivan maganey arogaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Shanmuganey arogaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shanmuganey arogaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nagaimuganey arogaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nagaimuganey arogaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thavayugane arogaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thavayugane arogaraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aarubadaiyappa anbaaley ulagai aalbavare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aarubadaiyappa anbaaley ulagai aalbavare"/>
</div>
<div class="lyrico-lyrics-wrapper">pazhaniyappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pazhaniyappa"/>
</div>
<div class="lyrico-lyrics-wrapper">pazhamudhirum soalai nadandhuvandhoam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pazhamudhirum soalai nadandhuvandhoam"/>
</div>
</pre>
