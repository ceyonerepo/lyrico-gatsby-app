---
title: "tholiprema song lyrics"
album: "Tholi Prema"
artist: "S Thaman"
lyricist: "Sri Mani"
director: "Venky Atluri"
path: "/albums/tholi-prema-lyrics"
song: "Tholiprema"
image: ../../images/albumart/tholi-prema.jpg
date: 2018-02-10
lang: telugu
youtubeLink: "https://www.youtube.com/embed/TQ-B5df2Le8"
type: "melody"
singers:
  -	Kaala Bhairava
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nijamenaa Nijamena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijamenaa Nijamena"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana Katha Mugisena 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana Katha Mugisena "/>
</div>
<div class="lyrico-lyrics-wrapper">Cheekatilo Ontarigaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheekatilo Ontarigaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Madhi Migilenaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Madhi Migilenaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Gathamu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Gathamu "/>
</div>
<div class="lyrico-lyrics-wrapper">Nene Vadulukunnaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nene Vadulukunnaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Nannu Vadalade 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Nannu Vadalade "/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Guruthulanni 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Guruthulanni "/>
</div>
<div class="lyrico-lyrics-wrapper">Cherapamannaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cherapamannaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Hrudayam Cherapade 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hrudayam Cherapade "/>
</div>
<div class="lyrico-lyrics-wrapper">Ye Ninna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Ninna "/>
</div>
<div class="lyrico-lyrics-wrapper">Thappo Netikedurai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappo Netikedurai "/>
</div>
<div class="lyrico-lyrics-wrapper">Nanu Niladeesene 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanu Niladeesene "/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Maruvaleni 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Maruvaleni "/>
</div>
<div class="lyrico-lyrics-wrapper">Gnaapakaale 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gnaapakaale "/>
</div>
<div class="lyrico-lyrics-wrapper">Nanu Velivesene 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanu Velivesene "/>
</div>
<div class="lyrico-lyrics-wrapper">Tholiprema
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholiprema"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Gundelo Gaayamaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Gundelo Gaayamaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Tholiprema 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholiprema "/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Valle Anakumaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Valle Anakumaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholiprema 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholiprema "/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Gundelo Gaayamaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Gundelo Gaayamaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Tholiprema 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholiprema "/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Valle Anakumaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Valle Anakumaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nijamenaa Nijamena 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijamenaa Nijamena "/>
</div>
<div class="lyrico-lyrics-wrapper">Mana Katha Mugisena 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana Katha Mugisena "/>
</div>
<div class="lyrico-lyrics-wrapper">Cheekatilo Ontarigaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheekatilo Ontarigaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Madhi Migilenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Madhi Migilenaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nerame Evarido 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nerame Evarido "/>
</div>
<div class="lyrico-lyrics-wrapper">Theladugaa Thelchavugaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theladugaa Thelchavugaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Panthame Enduko 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panthame Enduko "/>
</div>
<div class="lyrico-lyrics-wrapper">Adagavugaa Vidavavugaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adagavugaa Vidavavugaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Nene Oopiri Panchinaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nene Oopiri Panchinaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Nene Kaadani Thenchinaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nene Kaadani Thenchinaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Nene Kori Nene Veedi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nene Kori Nene Veedi "/>
</div>
<div class="lyrico-lyrics-wrapper">Nilakada Marichinaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilakada Marichinaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Raaka Malli 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Raaka Malli "/>
</div>
<div class="lyrico-lyrics-wrapper">Nidurapoye Kalalanu Pilichanene 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nidurapoye Kalalanu Pilichanene "/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Veedukole Entha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Veedukole Entha "/>
</div>
<div class="lyrico-lyrics-wrapper">Baadho Nede Thelisene 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baadho Nede Thelisene "/>
</div>
<div class="lyrico-lyrics-wrapper">Tholiprema
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholiprema"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Gundelo Gaayamaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Gundelo Gaayamaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Tholiprema 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholiprema "/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Valle Anakumaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Valle Anakumaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholiprema 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholiprema "/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Gundelo Gaayamaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Gundelo Gaayamaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Tholiprema 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholiprema "/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Valle Anakumaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Valle Anakumaaa"/>
</div>
</pre>
