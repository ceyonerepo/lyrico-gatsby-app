---
title: "thandhom song lyrics"
album: "Vellai Yaanai"
artist: "Santhosh Narayanan"
lyricist: "Anthony Daasan"
director: "Subramaniam Siva"
path: "/albums/vellai-yaanai-lyrics"
song: "Thandhom"
image: ../../images/albumart/vellai-yaanai.jpg
date: 2021-07-11
lang: tamil
youtubeLink: "https://www.youtube.com/embed/pypiS4GXCk0"
type: "happy"
singers:
  - Mukesh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">thandhom thana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thandhom thana"/>
</div>
<div class="lyrico-lyrics-wrapper">thandhom thana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thandhom thana"/>
</div>
<div class="lyrico-lyrics-wrapper">thandhom thana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thandhom thana"/>
</div>
<div class="lyrico-lyrics-wrapper">thana thandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thana thandha"/>
</div>
<div class="lyrico-lyrics-wrapper">enga sonthamellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga sonthamellam"/>
</div>
<div class="lyrico-lyrics-wrapper">koodi mazhila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koodi mazhila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thandhom thana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thandhom thana"/>
</div>
<div class="lyrico-lyrics-wrapper">thandhom thana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thandhom thana"/>
</div>
<div class="lyrico-lyrics-wrapper">thandhom thana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thandhom thana"/>
</div>
<div class="lyrico-lyrics-wrapper">thana thandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thana thandha"/>
</div>
<div class="lyrico-lyrics-wrapper">enga sandhosama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga sandhosama"/>
</div>
<div class="lyrico-lyrics-wrapper">aadi mazhila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aadi mazhila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vanthachu thirunaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanthachu thirunaalu"/>
</div>
<div class="lyrico-lyrics-wrapper">kondatam palanooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kondatam palanooru"/>
</div>
<div class="lyrico-lyrics-wrapper">theruvellam palacoloru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theruvellam palacoloru"/>
</div>
<div class="lyrico-lyrics-wrapper">veedelaam pudhu varavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veedelaam pudhu varavu"/>
</div>
<div class="lyrico-lyrics-wrapper">oorellam thoranam katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oorellam thoranam katti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">hey putham pudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey putham pudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">aadaigala maati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aadaigala maati"/>
</div>
<div class="lyrico-lyrics-wrapper">naanga kanduvarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naanga kanduvarum"/>
</div>
<div class="lyrico-lyrics-wrapper">kastangala otti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kastangala otti"/>
</div>
<div class="lyrico-lyrics-wrapper">hey pala ooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey pala ooru"/>
</div>
<div class="lyrico-lyrics-wrapper">pala desam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pala desam"/>
</div>
<div class="lyrico-lyrics-wrapper">parandhodi thirinjome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="parandhodi thirinjome"/>
</div>
<div class="lyrico-lyrics-wrapper">iraitheda kadal thaandum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iraitheda kadal thaandum"/>
</div>
<div class="lyrico-lyrics-wrapper">parava pola parandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="parava pola parandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">aanaalum or naalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aanaalum or naalil"/>
</div>
<div class="lyrico-lyrics-wrapper">enankaalu virundhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enankaalu virundhu"/>
</div>
<div class="lyrico-lyrics-wrapper">innaale uzhaikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="innaale uzhaikum"/>
</div>
<div class="lyrico-lyrics-wrapper">makkaluku arumarundhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="makkaluku arumarundhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kondaatam gumalam than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kondaatam gumalam than"/>
</div>
<div class="lyrico-lyrics-wrapper">engengum urchagam than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engengum urchagam than"/>
</div>
<div class="lyrico-lyrics-wrapper">enga munnorgal panpadu than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga munnorgal panpadu than"/>
</div>
<div class="lyrico-lyrics-wrapper">adha vaazhvome thembodu than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adha vaazhvome thembodu than"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">manna nee kaati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manna nee kaati"/>
</div>
<div class="lyrico-lyrics-wrapper">ponna adha maatri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponna adha maatri"/>
</div>
<div class="lyrico-lyrics-wrapper">pooncholaiya aakuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pooncholaiya aakuvom"/>
</div>
<div class="lyrico-lyrics-wrapper">naanga vandha idam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naanga vandha idam"/>
</div>
<div class="lyrico-lyrics-wrapper">adhu valamaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adhu valamaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">pona idam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pona idam"/>
</div>
<div class="lyrico-lyrics-wrapper">poovanama maara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poovanama maara"/>
</div>
<div class="lyrico-lyrics-wrapper">ada aaru oram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada aaru oram"/>
</div>
<div class="lyrico-lyrics-wrapper">vandha naagareegam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vandha naagareegam"/>
</div>
<div class="lyrico-lyrics-wrapper">adha kaaka venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adha kaaka venum"/>
</div>
<div class="lyrico-lyrics-wrapper">endha naalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="endha naalum"/>
</div>
<div class="lyrico-lyrics-wrapper">aadhaaram bhoomi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aadhaaram bhoomi"/>
</div>
<div class="lyrico-lyrics-wrapper">ava dhaane saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ava dhaane saami"/>
</div>
<div class="lyrico-lyrics-wrapper">idhupola sandhosham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhupola sandhosham"/>
</div>
<div class="lyrico-lyrics-wrapper">engalukku kuru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engalukku kuru"/>
</div>
<div class="lyrico-lyrics-wrapper">nalvaram vaangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nalvaram vaangi"/>
</div>
<div class="lyrico-lyrics-wrapper">vandhom paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vandhom paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kondaatam gumalam than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kondaatam gumalam than"/>
</div>
<div class="lyrico-lyrics-wrapper">engengum urchagam than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engengum urchagam than"/>
</div>
<div class="lyrico-lyrics-wrapper">enga munnorgal panpadu than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga munnorgal panpadu than"/>
</div>
<div class="lyrico-lyrics-wrapper">adha vaazhvome thembodu than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adha vaazhvome thembodu than"/>
</div>
</pre>
