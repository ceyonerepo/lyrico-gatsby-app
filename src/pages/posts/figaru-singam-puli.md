---
title: "figaru song lyrics"
album: "Singam Puli"
artist: "Mani Sharma"
lyricist: "Viveka"
director: "Sai Ramani"
path: "/albums/singam-puli-lyrics"
song: "Figaru"
image: ../../images/albumart/singam-puli.jpg
date: 2011-03-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/IvRg3W-knB8"
type: "mass"
singers:
  - Naveen
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Figure Ru Kidaikkudhunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Figure Ru Kidaikkudhunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Koosaama Poi Sol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koosaama Poi Sol"/>
</div>
<div class="lyrico-lyrics-wrapper">Bypass U Route Tu Pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bypass U Route Tu Pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Munneri Nee Sel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munneri Nee Sel"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalaikkoru Pen Irundhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalaikkoru Pen Irundhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Life E Happy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life E Happy"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalu Love Vu Panni Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalu Love Vu Panni Paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Varaadhe Bp
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varaadhe Bp"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Figure Ru Kidaikkudhunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Figure Ru Kidaikkudhunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Koosaama Poi Sol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koosaama Poi Sol"/>
</div>
<div class="lyrico-lyrics-wrapper">Bypass-u Route-u Pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bypass-u Route-u Pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Munneri Nee Sel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munneri Nee Sel"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edhuvume Thavar Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhuvume Thavar Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Evanume Seri Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evanume Seri Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivanukku Bayamilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivanukku Bayamilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Bayam Vandhaa Jayam Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bayam Vandhaa Jayam Illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Needhi Nerma Nyaayam Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Needhi Nerma Nyaayam Ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vendaam Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vendaam Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Machchi Machchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machchi Machchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Bokka Vaayan Veedhiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bokka Vaayan Veedhiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Vikkaadha Pal Kutchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vikkaadha Pal Kutchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Mela Vandhaa Aalil Palar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mela Vandhaa Aalil Palar"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullukkul Romba Mosam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullukkul Romba Mosam"/>
</div>
<div class="lyrico-lyrics-wrapper">Raavananaa Irundhaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raavananaa Irundhaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Raaman Vesham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raaman Vesham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Figure Ru Kidaikkudhunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Figure Ru Kidaikkudhunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Koosaama Poi Sol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koosaama Poi Sol"/>
</div>
<div class="lyrico-lyrics-wrapper">Bypass-u Route-u Pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bypass-u Route-u Pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Munneri Nee Sel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munneri Nee Sel"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edhuvume Thavar Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhuvume Thavar Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Evanume Seri Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evanume Seri Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivanukku Bayamilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivanukku Bayamilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Bayam Vandhaa Jayam Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bayam Vandhaa Jayam Illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Needhi Nerma Nyaayam Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Needhi Nerma Nyaayam Ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vendaam Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vendaam Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Machchi Machchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machchi Machchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Bokka Vaayan Veedhiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bokka Vaayan Veedhiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Vikkaadha Pal Kutchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vikkaadha Pal Kutchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Mela Vandhaa Aalil Palar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mela Vandhaa Aalil Palar"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullukkul Romba Mosam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullukkul Romba Mosam"/>
</div>
<div class="lyrico-lyrics-wrapper">Raavananaa Irundhaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raavananaa Irundhaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Raaman Vesham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raaman Vesham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhaghi Paarthaalum Sirippen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhaghi Paarthaalum Sirippen"/>
</div>
<div class="lyrico-lyrics-wrapper">Aunty Paarthaalum Rasippen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aunty Paarthaalum Rasippen"/>
</div>
<div class="lyrico-lyrics-wrapper">Cigarette Kedaichaalum Adippen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cigarette Kedaichaalum Adippen"/>
</div>
<div class="lyrico-lyrics-wrapper">Thundu Beediyum Kudippen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thundu Beediyum Kudippen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Thedi Thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Thedi Thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Maan Vettai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Maan Vettai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadathuven Nadathuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadathuven Nadathuven"/>
</div>
<div class="lyrico-lyrics-wrapper">Oon Urakkam Marandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oon Urakkam Marandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aval Um Sollum Varaiyilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aval Um Sollum Varaiyilum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoraththuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoraththuven"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Dhinam Thodarudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Dhinam Thodarudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meesai Vellai Aagum Munne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meesai Vellai Aagum Munne"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasaigal Theerthu Kollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasaigal Theerthu Kollu"/>
</div>
<div class="lyrico-lyrics-wrapper">Motcham Mele Undenbadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Motcham Mele Undenbadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Perusungalaal Tholla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perusungalaal Tholla"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathu Pona Tubeu Naala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathu Pona Tubeu Naala"/>
</div>
<div class="lyrico-lyrics-wrapper">Payanam Dhaan Illa Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Payanam Dhaan Illa Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Karanji Pona Soappu Meendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karanji Pona Soappu Meendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Payan Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Payan Illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Coffee Tea Pola Figure Ai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Coffee Tea Pola Figure Ai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal Tha Endru Keppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Tha Endru Keppen"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalai Sheelaavai Rasippen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalai Sheelaavai Rasippen"/>
</div>
<div class="lyrico-lyrics-wrapper">Maalai Maalaavai Rusippen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maalai Maalaavai Rusippen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poo Ondru Venaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poo Ondru Venaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Poo Kaadu Enakkenna Irukkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Poo Kaadu Enakkenna Irukkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Then Sottu Venaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Then Sottu Venaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Then Koodai Enakkena Kalaikkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Then Koodai Enakkena Kalaikkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Pudhu Anubhavam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Pudhu Anubhavam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ketta Vazhi Ellaathaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketta Vazhi Ellaathaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vovonnaa Kathukkitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vovonnaa Kathukkitten"/>
</div>
<div class="lyrico-lyrics-wrapper">Kettavana Vaazhuradha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kettavana Vaazhuradha"/>
</div>
<div class="lyrico-lyrics-wrapper">Latchchiyama Aakitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Latchchiyama Aakitten"/>
</div>
<div class="lyrico-lyrics-wrapper">Mollamaari Endru Oorum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mollamaari Endru Oorum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thittunadhaan Kandukkaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thittunadhaan Kandukkaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Moodikkollu Moodikkollu Rendu Kaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moodikkollu Moodikkollu Rendu Kaadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhuvarai Irundhadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhuvarai Irundhadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thavarena Purindhadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thavarena Purindhadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Inivarum Pozhudhughal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inivarum Pozhudhughal"/>
</div>
<div class="lyrico-lyrics-wrapper">Iniththida Varam Kodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iniththida Varam Kodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poove Unakkaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poove Unakkaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyiraiyum Koduppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyiraiyum Koduppen"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannil Podhaindhaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannil Podhaindhaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Ninaivil Iruppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Ninaivil Iruppen"/>
</div>
</pre>
