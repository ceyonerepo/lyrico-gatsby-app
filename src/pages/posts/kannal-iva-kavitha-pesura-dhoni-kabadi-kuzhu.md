---
title: "kannal iva kavitha pesura song lyrics"
album: "Dhoni Kabadi Kuzhu"
artist: "Roshan Joseph"
lyricist: "N Rasa"
director: "P Iyyappan"
path: "/albums/dhoni-kabadi-kuzhu-lyrics"
song: "Kannal Iva Kavitha Pesura"
image: ../../images/albumart/dhoni-kabadi-kuzhu.jpg
date: 2018-12-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/tEaYLIlpI7k"
type: "love"
singers:
  - Gunasekaran
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">kannal iva 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannal iva "/>
</div>
<div class="lyrico-lyrics-wrapper">kavitha pesura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kavitha pesura"/>
</div>
<div class="lyrico-lyrics-wrapper">emmela thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="emmela thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">kaththi veesura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaththi veesura"/>
</div>
<div class="lyrico-lyrics-wrapper">kanda padi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanda padi"/>
</div>
<div class="lyrico-lyrics-wrapper">enna thaakuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna thaakuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">kanakku panni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanakku panni"/>
</div>
<div class="lyrico-lyrics-wrapper">enna thookuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna thookuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">iva anaipula naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iva anaipula naan"/>
</div>
<div class="lyrico-lyrics-wrapper">marupadi purakuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="marupadi purakuren"/>
</div>
<div class="lyrico-lyrics-wrapper">iva siripula naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iva siripula naan"/>
</div>
<div class="lyrico-lyrics-wrapper">enaiye marakuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enaiye marakuren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kannal iva 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannal iva "/>
</div>
<div class="lyrico-lyrics-wrapper">kavitha pesura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kavitha pesura"/>
</div>
<div class="lyrico-lyrics-wrapper">emmela thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="emmela thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">kaththi veesura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaththi veesura"/>
</div>
<div class="lyrico-lyrics-wrapper">kanda padi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanda padi"/>
</div>
<div class="lyrico-lyrics-wrapper">enna thaakuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna thaakuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">kanakku panni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanakku panni"/>
</div>
<div class="lyrico-lyrics-wrapper">enna thookuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna thookuraa"/>
</div>
</pre>