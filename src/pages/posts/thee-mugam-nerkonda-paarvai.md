---
title: 'thee mugam dhaan song lyrics'
album: 'Nerkonda Paarvai'
artist: 'Yuvan Shankar Raja'
lyricist: 'Pa.Vijay'
director: 'H.Vinoth'
path: '/albums/comali-song-lyrics'
song: 'Thee Mugam Dhaan'
image: ../../images/albumart/nerkonda-paarvai.jpg
date: 2019-08-08
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Ar8GWKv3Kng"
type: 'mass'
singers: 
- Sathyan
- Senthil Dass
- Sarath Santhosh 
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Thee mugam dhaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Thee mugam dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar ivan dhaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Yaar ivan dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Orr adi dhaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Orr adi dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Paar idi dhaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Paar idi dhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Nee edhiri-ah uthiryaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee edhiri-ah uthiryaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Padhariyae vaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Padhariyae vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Imaippadhum vedi
<input type="checkbox" class="lyrico-select-lyric-line" value="Imaippadhum vedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan nerukkadi
<input type="checkbox" class="lyrico-select-lyric-line" value="Ivan nerukkadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vaa moodhi paaru
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaa moodhi paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Adichu midhichu
<input type="checkbox" class="lyrico-select-lyric-line" value="Adichu midhichu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aattam mudikka
<input type="checkbox" class="lyrico-select-lyric-line" value="Aattam mudikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa vettaiyaadu
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaa vettaiyaadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vella thaadi velicham adikka
<input type="checkbox" class="lyrico-select-lyric-line" value="Vella thaadi velicham adikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Poyi ennai paaru
<input type="checkbox" class="lyrico-select-lyric-line" value="Poyi ennai paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Udhachcha udhaiyil udanja elumba
<input type="checkbox" class="lyrico-select-lyric-line" value="Udhachcha udhaiyil udanja elumba"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar indha aalu
<input type="checkbox" class="lyrico-select-lyric-line" value="Yaar indha aalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Irangi pudipaan edhiri narmaba
<input type="checkbox" class="lyrico-select-lyric-line" value="Irangi pudipaan edhiri narmaba"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Pirichu pirichu meiyuraan
<input type="checkbox" class="lyrico-select-lyric-line" value="Pirichu pirichu meiyuraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thorathi thorathi velukkuraan
<input type="checkbox" class="lyrico-select-lyric-line" value="Thorathi thorathi velukkuraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulla kodhikkum neruppathaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Ulla kodhikkum neruppathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Urichi urichi edukkuraan
<input type="checkbox" class="lyrico-select-lyric-line" value="Urichi urichi edukkuraan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Adanga adanga marukkuraan
<input type="checkbox" class="lyrico-select-lyric-line" value="Adanga adanga marukkuraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Alanga kalanga midhikiraan
<input type="checkbox" class="lyrico-select-lyric-line" value="Alanga kalanga midhikiraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Poratti poratti edukkuraan
<input type="checkbox" class="lyrico-select-lyric-line" value="Poratti poratti edukkuraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Paiyil puyala adaikkiraan
<input type="checkbox" class="lyrico-select-lyric-line" value="Paiyil puyala adaikkiraan"/>
</div>
</pre>