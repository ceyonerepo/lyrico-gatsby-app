---
title: "raja garu bayatakosthe song lyrics"
album: "Raja Vikramarka"
artist: "Prasanth R Vihari"
lyricist: "Krishnakant"
director: "Sri Saripalli"
path: "/albums/raja-vikramarka-lyrics"
song: "Raja Garu Bayatakosthe"
image: ../../images/albumart/raja-vikramarka.jpg
date: 2021-11-12
lang: telugu
youtubeLink: "https://www.youtube.com/embed/sgoTWbLMTi0"
type: "mass"
singers:
  - David Simon
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Raja Garu Bayatikosthe Pramadhame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja Garu Bayatikosthe Pramadhame"/>
</div>
<div class="lyrico-lyrics-wrapper">Prayaasatho Paraaru Anthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prayaasatho Paraaru Anthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Raja Garu Vetakosthe Bhujaalapai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja Garu Vetakosthe Bhujaalapai"/>
</div>
<div class="lyrico-lyrics-wrapper">Shikhaarule Kharaaru Anthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shikhaarule Kharaaru Anthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhirele Inchainaa Thaggadhinkaa Theevi Needhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhirele Inchainaa Thaggadhinkaa Theevi Needhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhirele Kangaaru Machhukainaa Lene Ledhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhirele Kangaaru Machhukainaa Lene Ledhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhirele Ponchunna Gudachari Aanavaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhirele Ponchunna Gudachari Aanavaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye Alupu Dhigulu Padane Padani Narudu Veede
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Alupu Dhigulu Padane Padani Narudu Veede"/>
</div>
<div class="lyrico-lyrics-wrapper">Merupulaa Malupulaa Dhaare Pattaade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Merupulaa Malupulaa Dhaare Pattaade"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raja Garu Bayatikosthe Pramadhame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja Garu Bayatikosthe Pramadhame"/>
</div>
<div class="lyrico-lyrics-wrapper">Prayaasatho Paraaru Anthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prayaasatho Paraaru Anthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Raja Garu Vetakosthe Bhujaalapai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja Garu Vetakosthe Bhujaalapai"/>
</div>
<div class="lyrico-lyrics-wrapper">Shikhaarule Kharaaru Anthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shikhaarule Kharaaru Anthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bulletainaa Adhiri Jarigi Jarigi Podhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bulletainaa Adhiri Jarigi Jarigi Podhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mruthyuvaina Bedhiri Hadali Hadali Podhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mruthyuvaina Bedhiri Hadali Hadali Podhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Shathruvaithe Thanani 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shathruvaithe Thanani "/>
</div>
<div class="lyrico-lyrics-wrapper">Vadhaladhasalu Bang Bang
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadhaladhasalu Bang Bang"/>
</div>
<div class="lyrico-lyrics-wrapper">Vadhaladhasalu Bang Bang
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadhaladhasalu Bang Bang"/>
</div>
<div class="lyrico-lyrics-wrapper">Vadhaladhasalu Bang Bang
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadhaladhasalu Bang Bang"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vetaade Kannullo Ennenno Thanthraalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetaade Kannullo Ennenno Thanthraalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhethaalunne Veedu Prashnisthaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhethaalunne Veedu Prashnisthaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Raaboye Vidhwamsam Ye Kantaa Choosthaado
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raaboye Vidhwamsam Ye Kantaa Choosthaado"/>
</div>
<div class="lyrico-lyrics-wrapper">Eelope Pannaagam Pannesthaade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eelope Pannaagam Pannesthaade"/>
</div>
<div class="lyrico-lyrics-wrapper">Merupulaa Malupulaa Dhaare Pattaade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Merupulaa Malupulaa Dhaare Pattaade"/>
</div>
</pre>
