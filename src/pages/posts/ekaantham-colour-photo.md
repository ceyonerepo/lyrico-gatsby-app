---
title: "ekaantham song lyrics"
album: "Colour Photo"
artist: "Kaala Bhairava"
lyricist: "Krishna Chaitanya"
director: "Vijay Kumar Konda"
path: "/albums/colour-photo-lyrics"
song: "Ekaantham"
image: ../../images/albumart/colour-photo.jpg
date: 2020-10-23
lang: telugu
youtubeLink: "https://www.youtube.com/embed/v-E6TTa5KJQ"
type: "love"
singers:
  - Ramya Behara
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kannelu konnellu ga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannelu konnellu ga"/>
</div>
<div class="lyrico-lyrics-wrapper">Raananna raledhuga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raananna raledhuga"/>
</div>
<div class="lyrico-lyrics-wrapper">Gathamundhi le nindu ga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gathamundhi le nindu ga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kada varaku thodundagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kada varaku thodundagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa kopam na dwesham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa kopam na dwesham"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa thode raavemo naatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa thode raavemo naatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Naathoney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naathoney"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa gayam naa saayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa gayam naa saayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa thode raavemo naatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa thode raavemo naatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Naathoney…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naathoney…"/>
</div>
<div class="lyrico-lyrics-wrapper">Ekaantham lene ledhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ekaantham lene ledhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayina manase urukodhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayina manase urukodhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nishabdham kottha kadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nishabdham kottha kadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa yuddham naathone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa yuddham naathone"/>
</div>
<div class="lyrico-lyrics-wrapper">Ekaantham lene ledhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ekaantham lene ledhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nishabdham kotthemi kadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nishabdham kotthemi kadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasemo urukodhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasemo urukodhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa yuddham naathone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa yuddham naathone"/>
</div>
<div class="lyrico-lyrics-wrapper">O... Ne moyaleni porapatulenno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O... Ne moyaleni porapatulenno"/>
</div>
<div class="lyrico-lyrics-wrapper">Enno ennenno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enno ennenno"/>
</div>
<div class="lyrico-lyrics-wrapper">Ne dhaataleni aa maatalenno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne dhaataleni aa maatalenno"/>
</div>
<div class="lyrico-lyrics-wrapper">Enno ennenno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enno ennenno"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannelu konnellu ga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannelu konnellu ga"/>
</div>
<div class="lyrico-lyrics-wrapper">Raananna raledhuga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raananna raledhuga"/>
</div>
<div class="lyrico-lyrics-wrapper">Gathamundhi le nindu ga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gathamundhi le nindu ga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kada varaku thodundagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kada varaku thodundagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ne moyaleni porapatulenno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne moyaleni porapatulenno"/>
</div>
<div class="lyrico-lyrics-wrapper">Enno ennenno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enno ennenno"/>
</div>
<div class="lyrico-lyrics-wrapper">Ne dhaataleni aa maatalenno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne dhaataleni aa maatalenno"/>
</div>
<div class="lyrico-lyrics-wrapper">Enno ennenno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enno ennenno"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannelu konnellu ga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannelu konnellu ga"/>
</div>
<div class="lyrico-lyrics-wrapper">Raananna raledhuga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raananna raledhuga"/>
</div>
</pre>
