---
title: "maehaveli song lyrics"
album: "WWW"
artist: "Simon K King "
lyricist: "Madhan Karky"
director: "K V Guhan"
path: "/albums/www-lyrics"
song: "Maehaveli"
image: ../../images/albumart/www.jpg
date: 2021-12-24
lang: telugu
youtubeLink: "https://www.youtube.com/embed/--RVsZCqm0k"
type: "love"
singers:
  - Sid Sriram
  - Kalyani Nair
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Maehaveli yaavum yaavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maehaveli yaavum yaavum"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan peyarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan peyarai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuligalena thoorudhaadee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuligalena thoorudhaadee"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhaloli paayum paayum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhaloli paayum paayum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kurundhirai en ulaham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurundhirai en ulaham"/>
</div>
<div class="lyrico-lyrics-wrapper">Ena maarudhadee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ena maarudhadee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aruhe nee nindrum unnai theendenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aruhe nee nindrum unnai theendenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanini thirai chiraiyai thaandenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanini thirai chiraiyai thaandenaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inaiyam vazhiye pozhiyum mazhaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inaiyam vazhiye pozhiyum mazhaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Engum undhan kaanoliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engum undhan kaanoliye"/>
</div>
<div class="lyrico-lyrics-wrapper">Alaiyin oliyaay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alaiyin oliyaay"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilaiyin mozhiyaay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilaiyin mozhiyaay"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadholi karuviyil pesidu kaadhaliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadholi karuviyil pesidu kaadhaliye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maehaveli yaavum yaavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maehaveli yaavum yaavum"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan peyarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan peyarai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuligalena thoorudhaadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuligalena thoorudhaadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhaloli paayum paayum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhaloli paayum paayum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kurundhirai en ulaham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurundhirai en ulaham"/>
</div>
<div class="lyrico-lyrics-wrapper">Ena maarudhadee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ena maarudhadee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endhan aluval ellaame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan aluval ellaame"/>
</div>
<div class="lyrico-lyrics-wrapper">En irikkiradhoa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En irikkiradhoa"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhan visaippalagai mele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan visaippalagai mele"/>
</div>
<div class="lyrico-lyrics-wrapper">Poo malarhiradho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poo malarhiradho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Minsuvaronru nam munnae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minsuvaronru nam munnae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee ingae naan angae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee ingae naan angae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinnil minnidum meenellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinnil minnidum meenellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennip paarthe theerndhaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennip paarthe theerndhaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennimanaaya muthangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennimanaaya muthangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirndhaepoagaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirndhaepoagaadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maehaveli yaavum yaavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maehaveli yaavum yaavum"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan peyarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan peyarai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuligalena thoorudhadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuligalena thoorudhadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhaloli Aa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhaloli Aa aaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhaloli paayum paayum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhaloli paayum paayum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kurundhirai en ulaham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurundhirai en ulaham"/>
</div>
<div class="lyrico-lyrics-wrapper">Ena maarudhadee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ena maarudhadee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nedundholaivenumbo adhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nedundholaivenumbo adhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilavoli nijamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilavoli nijamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholaithodarbenumbo adhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholaithodarbenumbo adhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Varum kural varamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varum kural varamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aerpaaya nee maattaayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aerpaaya nee maattaayaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naetrellaam achathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naetrellaam achathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Aetrukkonda pinnaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aetrukkonda pinnaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinnaith thaandi uchathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinnaith thaandi uchathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Narahamum inbam unnoadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Narahamum inbam unnoadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan vazhum patchathil oo oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan vazhum patchathil oo oo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maehaveli yaavum yaavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maehaveli yaavum yaavum"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan peyarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan peyarai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuligalena thoorudhaadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuligalena thoorudhaadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhaloli paayum paayum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhaloli paayum paayum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kurundhirai en ulaham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurundhirai en ulaham"/>
</div>
<div class="lyrico-lyrics-wrapper">Ena maarudhadee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ena maarudhadee"/>
</div>
</pre>
