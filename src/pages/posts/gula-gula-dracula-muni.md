---
title: "gula gula dracula song lyrics"
album: "Muni"
artist: "Bharathwaj"
lyricist: "Pa. Vijay - Raghava Lawrence"
director: "Raghava Lawrence"
path: "/albums/muni-lyrics"
song: "Gula Gula Dracula"
image: ../../images/albumart/muni.jpg
date: 2007-03-09
lang: tamil
youtubeLink: "https://www.youtube.com/embed/FUnpyF67yJA"
type: "Love"
singers:
  - Sathyan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Gullaa gullaa dragullaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gullaa gullaa dragullaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yei peigal aadum oonjalaa…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei peigal aadum oonjalaa…"/>
</div>
<div class="lyrico-lyrics-wrapper">Gullaa gullaa dragullaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gullaa gullaa dragullaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yei peigal aadum oonjalaa…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei peigal aadum oonjalaa…"/>
</div>
<div class="lyrico-lyrics-wrapper">Saaththaan naanthaan sagalagalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saaththaan naanthaan sagalagalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Watch me now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Watch me now"/>
</div>
<div class="lyrico-lyrics-wrapper">Watch me now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Watch me now"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maayaajaala manithanadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maayaajaala manithanadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Raththam ketkum uruvamadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raththam ketkum uruvamadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aavigal aadum aattamadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aavigal aadum aattamadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Watch me now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Watch me now"/>
</div>
<div class="lyrico-lyrics-wrapper">Watch me now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Watch me now"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En ullukkulla thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En ullukkulla thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pala sakthi irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala sakthi irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">En kannukkulla thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kannukkulla thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pala viththai irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala viththai irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Maayaajaala aattam vanthaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maayaajaala aattam vanthaachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yoo yoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yoo yoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Maayan vanthaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maayan vanthaachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yoo yoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yoo yoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam vanthaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam vanthaachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yoo yoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yoo yoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Neram vanthaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neram vanthaachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hahahahaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hahahahaha"/>
</div>
<div class="lyrico-lyrics-wrapper">This is my time man
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="This is my time man"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gullaa gullaa dragullaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gullaa gullaa dragullaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yei peigal aadum oonjalaa…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei peigal aadum oonjalaa…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan maayaavi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan maayaavi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan avan aavi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan avan aavi"/>
</div>
<div class="lyrico-lyrics-wrapper">En kan munnae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kan munnae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadal neeraavi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadal neeraavi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada naankuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada naankuda"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeah"/>
</div>
<div class="lyrico-lyrics-wrapper">Puthu avathaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthu avathaaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeh yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh yeah"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini naalthorum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini naalthorum"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeah"/>
</div>
<div class="lyrico-lyrics-wrapper">Pala samhaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala samhaaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koodu vittu innoru koodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koodu vittu innoru koodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paayum neram vanthaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paayum neram vanthaachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeyai vaithu theeyai vella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeyai vaithu theeyai vella"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeyae aayutham aayaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeyae aayutham aayaachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gnaayam ketkka peigal vanthaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gnaayam ketkka peigal vanthaachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yoo yoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yoo yoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Maayan vanthaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maayan vanthaachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yoo yoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yoo yoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam vanthaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam vanthaachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yoo yoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yoo yoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Neram vanthaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neram vanthaachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hahahahaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hahahahaha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gullaa gullaa dragullaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gullaa gullaa dragullaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yei peigal aadum oonjalaa…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei peigal aadum oonjalaa…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada neeyaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada neeyaachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini naanaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini naanaachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha yutham thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha yutham thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Indru mudivaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indru mudivaachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pazhi theerkkathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pazhi theerkkathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Theerkkathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theerkkathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Padai eduthaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padai eduthaachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Eduthaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eduthaachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir vaangathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir vaangathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vangathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vangathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir undaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir undaachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Undaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undaachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalikaalam mutrippoga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalikaalam mutrippoga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadavul seitha ruthran naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadavul seitha ruthran naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanavangal adi ver saaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanavangal adi ver saaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Aayutham yenthum asuran naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayutham yenthum asuran naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Narasimmam meendum vandhaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Narasimmam meendum vandhaachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yoo yoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yoo yoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Maayan vanthaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maayan vanthaachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yoo yoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yoo yoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam vanthaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam vanthaachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yoo yoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yoo yoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Neram vanthaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neram vanthaachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hahahahaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hahahahaha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gullaa gullaa dragullaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gullaa gullaa dragullaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yei peigal aadum oonjalaa…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei peigal aadum oonjalaa…"/>
</div>
<div class="lyrico-lyrics-wrapper">Saaththaan naanthaan sagalagalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saaththaan naanthaan sagalagalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Watch me now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Watch me now"/>
</div>
<div class="lyrico-lyrics-wrapper">Watch me now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Watch me now"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maayaajaala manithanadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maayaajaala manithanadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Raththam ketkum uruvamadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raththam ketkum uruvamadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aavigal aadum aattamadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aavigal aadum aattamadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Watch me now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Watch me now"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En ullukkulla thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En ullukkulla thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pala sakthi irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala sakthi irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">En kannukkulla thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kannukkulla thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pala viththai irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala viththai irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Maayaajaala aattam vanthaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maayaajaala aattam vanthaachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yoo yoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yoo yoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Maayan vanthaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maayan vanthaachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yoo yoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yoo yoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam vanthaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam vanthaachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yoo yoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yoo yoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Neram vanthaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neram vanthaachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hahahahaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hahahahaha"/>
</div>
<div class="lyrico-lyrics-wrapper">This is my time man
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="This is my time man"/>
</div>
</pre>
