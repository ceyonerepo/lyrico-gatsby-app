---
title: "dooram nillu modhathe song lyrics"
album: "Vellaiilla Pattadhari 2"
artist: "Sean Roldan"
lyricist: "Dhanush"
director: "Soundarya Rajinikanth"
path: "/albums/vellaiilla-pattadhari-2-lyrics"
song: "Dooram Nillu Modhathe"
image: ../../images/albumart/vellaiilla-pattadhari-2.jpg
date: 2017-11-08
lang: tamil
youtubeLink: "https://www.youtube.com/embed/dncVK-Z4rSA"
type: "Motivational"
singers:
  - Dhanush
  - Shakthisree Gopalan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Dooram Nillu Modhadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dooram Nillu Modhadhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pen Singam Ennai Seendathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pen Singam Ennai Seendathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aatam Ellam Aadathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aatam Ellam Aadathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Thittam Ellam Theradhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Thittam Ellam Theradhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ullukulla Ullukulla Thunivundu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullukulla Ullukulla Thunivundu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjukulla Nenjukulla Panivundu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjukulla Nenjukulla Panivundu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sathiyatha Muttiyila Vechi Iruken Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sathiyatha Muttiyila Vechi Iruken Naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nambivandha Manasukku Thanniundu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nambivandha Manasukku Thanniundu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kettathayum Mannikira Arivundu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kettathayum Mannikira Arivundu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thuyarayum Thodachittu Sirippavan Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuyarayum Thodachittu Sirippavan Naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pudhusa Uruvanavan Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhusa Uruvanavan Illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ulaipaal Uyar Erpen Solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulaipaal Uyar Erpen Solla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Velvene Muraiyaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velvene Muraiyaaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaayundu Thunaiyaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaayundu Thunaiyaaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Aarudhal Thevayum Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Aarudhal Thevayum Illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Aan Thunai Thevaiyum Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Aan Thunai Thevaiyum Illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanaththu Nilavaaga Jolippene Thaniaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanaththu Nilavaaga Jolippene Thaniaaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Vaazhkaiye Poraatam Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Vaazhkaiye Poraatam Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paakadha Thadai Illa  Eraalamthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paakadha Thadai Illa  Eraalamthaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thuthilla Naan Puyalpolathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuthilla Naan Puyalpolathaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nadandhaale Uruvaagum Boogambamthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadandhaale Uruvaagum Boogambamthaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Aanin Thimirungal Adangattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Aanin Thimirungal Adangattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Pennen Sirappugal Vilangattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Pennen Sirappugal Vilangattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bring It On Ready Naan Nenacha Mudippen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bring It On Ready Naan Nenacha Mudippen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Game On
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Game On"/>
</div>
</pre>
