---
title: "eyy beta idhu en patta song lyrics"
album: "Pushpa The Rise"
artist: "Devi Sri Prasad"
lyricist: "Viveka"
director: "Sukumar"
path: "/albums/pushpa-the-rise-song-lyrics"
song: "Eyy Beta Idhu En Patta"
image: ../../images/albumart/pushpa-the-rise.jpg
date: 2021-12-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/XNR-sHIQ89c"
type: "mass"
singers:
  - Nakash Aziz
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vallap pakkam naane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vallap pakkam naane"/>
</div>
<div class="lyrico-lyrics-wrapper">Ida pakkam naane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ida pakkam naane"/>
</div>
<div class="lyrico-lyrics-wrapper">Thala mela aagaayam mottham naane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thala mela aagaayam mottham naane"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada thappum naane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada thappum naane"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye right-um naane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye right-um naane"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappukkum right-kum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappukkum right-kum"/>
</div>
<div class="lyrico-lyrics-wrapper">Appen naane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appen naane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennoda moodhi vella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennoda moodhi vella"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhoomi mela yaarumilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhoomi mela yaarumilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Sooraadhi sooran ingu naane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sooraadhi sooran ingu naane"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen mele kaiyyavekka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen mele kaiyyavekka"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarum porakka villa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarum porakka villa"/>
</div>
<div class="lyrico-lyrics-wrapper">Porandhaaka andha aalum naane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porandhaaka andha aalum naane"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennoda meesa rendu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennoda meesa rendu"/>
</div>
<div class="lyrico-lyrics-wrapper">Theetti veccha kodari da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theetti veccha kodari da"/>
</div>
<div class="lyrico-lyrics-wrapper">Rattham varum madndayila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rattham varum madndayila"/>
</div>
<div class="lyrico-lyrics-wrapper">Raatchasan naan sandayila vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raatchasan naan sandayila vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dei beta idhu en patta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dei beta idhu en patta"/>
</div>
<div class="lyrico-lyrics-wrapper">Dei beta idhu en patta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dei beta idhu en patta"/>
</div>
<div class="lyrico-lyrics-wrapper">Dei beta idhu en patta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dei beta idhu en patta"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey Dei beta idhu en patta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey Dei beta idhu en patta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unna kadalula pottaakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna kadalula pottaakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Meena kavvikittu varuvenda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meena kavvikittu varuvenda"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna kaaththula yerunjaaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna kaaththula yerunjaaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan kaathaadi aavenda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan kaathaadi aavenda"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada mannukulla unna vecchu podhacchaaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada mannukulla unna vecchu podhacchaaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa vairamaaga maari vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa vairamaaga maari vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Getha jolippen da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Getha jolippen da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dei beta idhu en patta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dei beta idhu en patta"/>
</div>
<div class="lyrico-lyrics-wrapper">Dei beta idhu en patta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dei beta idhu en patta"/>
</div>
<div class="lyrico-lyrics-wrapper">Dei beta idhu en patta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dei beta idhu en patta"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey Dei beta idhu en patta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey Dei beta idhu en patta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee yevanda yevanda sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee yevanda yevanda sollu"/>
</div>
<div class="lyrico-lyrics-wrapper">Irumba pola naan dhillu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irumba pola naan dhillu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada valacchaalum vaalaaven thallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada valacchaalum vaalaaven thallu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee yevanda yevanda sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee yevanda yevanda sollu"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeppori pola naan dhillu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeppori pola naan dhillu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna theenduna kaattu thee thallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna theenduna kaattu thee thallu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee yevanda yevanda sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee yevanda yevanda sollu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paaraya pola naan dhillu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaraya pola naan dhillu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vediya odaichaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vediya odaichaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Uliya setchecchaalum vanagura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uliya setchecchaalum vanagura"/>
</div>
<div class="lyrico-lyrics-wrapper">Silayaaven naanu naanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silayaaven naanu naanu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dei beta idhu en patta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dei beta idhu en patta"/>
</div>
<div class="lyrico-lyrics-wrapper">Dei beta idhu en patta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dei beta idhu en patta"/>
</div>
<div class="lyrico-lyrics-wrapper">Dei beta idhu en patta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dei beta idhu en patta"/>
</div>
<div class="lyrico-lyrics-wrapper">Le le thaggedhele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Le le thaggedhele"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey Dei beta idhu en patta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey Dei beta idhu en patta"/>
</div>
<div class="lyrico-lyrics-wrapper">Le le thaggedhele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Le le thaggedhele"/>
</div>
</pre>
