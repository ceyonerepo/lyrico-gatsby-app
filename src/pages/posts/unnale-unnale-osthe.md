---
title: "unnale unnale song lyrics"
album: "Osthe"
artist: "Sai Thaman"
lyricist: "Yugabharathi"
director: "S. Dharani"
path: "/albums/osthe-lyrics"
song: "Unnale Unnale"
image: ../../images/albumart/osthe.jpg
date: 2011-12-08
lang: tamil
youtubeLink: "https://www.youtube.com/embed/D3C66NWlqug"
type: "love"
singers:
  - S. Thaman
  - Rita
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aae unnalae unnalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aae unnalae unnalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Suthudhadi boomi pandhu thannaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suthudhadi boomi pandhu thannaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aae kannaalae kannaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aae kannaalae kannaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Ottuthadi vanavillu emmela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ottuthadi vanavillu emmela"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Devadhaiya kaanavila yenginen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Devadhaiya kaanavila yenginen"/>
</div>
<div class="lyrico-lyrics-wrapper">Appo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appo"/>
</div>
<div class="lyrico-lyrics-wrapper">Unvaliyil theerudhae yekkamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unvaliyil theerudhae yekkamum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poomazhaiyae vanamae thoorumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poomazhaiyae vanamae thoorumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Appo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appo"/>
</div>
<div class="lyrico-lyrics-wrapper">Unninaivu thuralaa serudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unninaivu thuralaa serudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aae unnalae unnalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aae unnalae unnalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Suthudhadi boomi pandhu thannaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suthudhadi boomi pandhu thannaalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivan mansasudhan perusudhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan mansasudhan perusudhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaku ivan idamundhaan kodukuraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaku ivan idamundhaan kodukuraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Un nenapudhaan kanavudhaan avana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un nenapudhaan kanavudhaan avana"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenachudhaan urugudhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenachudhaan urugudhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh aiyo aiyo theriyuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh aiyo aiyo theriyuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavilae kalaigira veshamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavilae kalaigira veshamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Inge sendru mudiyumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inge sendru mudiyumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalumbiyae vazhigira nesamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalumbiyae vazhigira nesamae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey enna neeyum paakalanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey enna neeyum paakalanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Annam illa aagaaram illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annam illa aagaaram illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna paththi pesalenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna paththi pesalenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanban illa naan kooda illavae illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanban illa naan kooda illavae illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naal mulukka oora naan suththunen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naal mulukka oora naan suththunen"/>
</div>
<div class="lyrico-lyrics-wrapper">Appo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee sirika unna naan suththaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee sirika unna naan suththaren"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maatikitaa odanae thattuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maatikitaa odanae thattuven"/>
</div>
<div class="lyrico-lyrics-wrapper">AppoKaadhal kitta maatikiten kathuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="AppoKaadhal kitta maatikiten kathuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnalae unnalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnalae unnalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh illai illai idhyamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh illai illai idhyamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thozhaindhidum dhinam unnai kaanavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozhaindhidum dhinam unnai kaanavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingae ingae edhuvumae therindhidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingae ingae edhuvumae therindhidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadavulai polavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadavulai polavae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aae enna solla yedhu solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aae enna solla yedhu solla"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaamumae neeyaagi ninnen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaamumae neeyaagi ninnen"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathiyila kaayamilla aanaalum nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathiyila kaayamilla aanaalum nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollaamal kollaamal kolla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollaamal kollaamal kolla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Devadhaiya kaanavila yenginen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Devadhaiya kaanavila yenginen"/>
</div>
<div class="lyrico-lyrics-wrapper">Appo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appo"/>
</div>
<div class="lyrico-lyrics-wrapper">Unvaliyil theerudhae yekkamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unvaliyil theerudhae yekkamum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poomazhaiyae vanamae thoorumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poomazhaiyae vanamae thoorumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Appo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appo"/>
</div>
<div class="lyrico-lyrics-wrapper">Unninaivu thuralaa serudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unninaivu thuralaa serudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naal mulukka oora naan suththunen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naal mulukka oora naan suththunen"/>
</div>
<div class="lyrico-lyrics-wrapper">Appo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee sirika unna naan suththaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee sirika unna naan suththaren"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maatikitaa odanae thattuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maatikitaa odanae thattuven"/>
</div>
<div class="lyrico-lyrics-wrapper">Appo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal kitta maatikiten kathuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal kitta maatikiten kathuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnalae unnalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnalae unnalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Suthudhadi boomi pandhu thannaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suthudhadi boomi pandhu thannaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohh kannaalae kannaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohh kannaalae kannaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Ottuthadi vanavillu emmela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ottuthadi vanavillu emmela"/>
</div>
</pre>
