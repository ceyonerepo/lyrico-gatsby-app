---
title: "sandhanatha song lyrics"
album: "Vada Chennai"
artist: "Santhosh Narayanan"
lyricist: "Gana Bala"
director: "Vetrimaaran"
path: "/albums/vada-chennai-lyrics"
song: "Sandhanatha"
image: ../../images/albumart/vada-chennai.jpg
date: 2018-10-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/VDOS9CGIUpI"
type: "happy"
singers:
  - Ka Ka Balachander
  - Gana Bala
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Sandhanatha karachi thaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhanatha karachi thaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannathula poosa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannathula poosa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thattu varusaya pirichu vaida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thattu varusaya pirichu vaida"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorae morachu paaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorae morachu paaka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thambi annan petha ponnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thambi annan petha ponnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Periya manushi aayittaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Periya manushi aayittaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nethu intha roja poovu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nethu intha roja poovu"/>
</div>
<div class="lyrico-lyrics-wrapper">Veetukulla poothutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veetukulla poothutha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaaimaaman ellaarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaimaaman ellaarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Onnathana aayitom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnathana aayitom"/>
</div>
<div class="lyrico-lyrics-wrapper">Dappaanguthu aadikinu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dappaanguthu aadikinu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oor valama kelambitom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oor valama kelambitom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sondha bandham ellathukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sondha bandham ellathukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Seidhi solli anuppuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seidhi solli anuppuda"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandhu bondhu ella edamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhu bondhu ella edamum"/>
</div>
<div class="lyrico-lyrics-wrapper">Poster adichu ottuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poster adichu ottuda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sandhanatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhanatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayiyaiyooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayiyaiyooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandhanatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhanatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sandhanataha karachi thaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhanataha karachi thaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannathula poosa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannathula poosa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thattu varusaya pirichu vaida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thattu varusaya pirichu vaida"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorae morachu paaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorae morachu paaka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Munnaatti potta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munnaatti potta"/>
</div>
<div class="lyrico-lyrics-wrapper">Pinnaatti varen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pinnaatti varen"/>
</div>
<div class="lyrico-lyrics-wrapper">Munnaatti potta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munnaatti potta"/>
</div>
<div class="lyrico-lyrics-wrapper">Pinnaatti varen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pinnaatti varen"/>
</div>
<div class="lyrico-lyrics-wrapper">Hei hei hei heihei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei hei hei heihei"/>
</div>
<div class="lyrico-lyrics-wrapper">Hei hei hei heihei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei hei hei heihei"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paavada satta potta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paavada satta potta"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattaampoochi unakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattaampoochi unakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaimaaman kondu vandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaimaaman kondu vandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanga chain-u parisu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanga chain-u parisu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aalagi vittalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalagi vittalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Akka ponnu thaan da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Akka ponnu thaan da"/>
</div>
<div class="lyrico-lyrics-wrapper">Adha kettu sandhosam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adha kettu sandhosam"/>
</div>
<div class="lyrico-lyrics-wrapper">Adanjitten naan da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adanjitten naan da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pazhaya thuni ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pazhaya thuni ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Paraniyila podu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paraniyila podu da"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattu podava vaangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattu podava vaangi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattunathaan joru da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattunathaan joru da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pavalam muthu vairam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pavalam muthu vairam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoothu povum paru da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoothu povum paru da"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivala pola azhagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivala pola azhagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorukulla yaaru da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorukulla yaaru da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazha pola valandhutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazha pola valandhutta"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinna vayasula naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna vayasula naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Paathatha pol illadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paathatha pol illadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aalae maarivittaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalae maarivittaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pazhasa ellaam maranthutaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pazhasa ellaam maranthutaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakkuvamma aayitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkuvamma aayitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Thenna ola kudisayila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thenna ola kudisayila"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga veettu thinnaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga veettu thinnaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhusaathaan poranthutta da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhusaathaan poranthutta da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sandhanatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhanatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayiyyaa Sandhanathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayiyyaa Sandhanathaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sandhanatha karachi thaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhanatha karachi thaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannathula poosa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannathula poosa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thattu varusaya pirichu vaida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thattu varusaya pirichu vaida"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorae morachu paaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorae morachu paaka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pottlamittai pottlamittai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pottlamittai pottlamittai"/>
</div>
<div class="lyrico-lyrics-wrapper">Udathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udathaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Goindhamma udatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Goindhamma udatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Udathaa majaapaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udathaa majaapaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Avala udathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avala udathaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thattivudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thattivudu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thattivudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thattivudu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalula metti katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalula metti katti"/>
</div>
<div class="lyrico-lyrics-wrapper">Paaduthamma kolusu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaduthamma kolusu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathula oonjal katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathula oonjal katti"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadutha da kammalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadutha da kammalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paathaalae minnuthamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paathaalae minnuthamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Akka ponnu azhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Akka ponnu azhagu"/>
</div>
<div class="lyrico-lyrics-wrapper">Bayanthukkinu ozhinjikichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bayanthukkinu ozhinjikichu"/>
</div>
<div class="lyrico-lyrics-wrapper">Megathula nilavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megathula nilavu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Melaada dhavani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Melaada dhavani"/>
</div>
<div class="lyrico-lyrics-wrapper">Selaiyaaga achi da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Selaiyaaga achi da"/>
</div>
<div class="lyrico-lyrics-wrapper">Mittaai kammarakattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mittaai kammarakattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasai ellaam pochu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai ellaam pochu da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raathiri ellaarukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raathiri ellaarukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kozhikari virundhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kozhikari virundhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Raava thaan oothikudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raava thaan oothikudu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nattu sarakku marundhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nattu sarakku marundhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raasathi vandhutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raasathi vandhutta"/>
</div>
<div class="lyrico-lyrics-wrapper">Munnala vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munnala vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aarathi edungada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aarathi edungada"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollama inga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollama inga"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorujanam onupadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorujanam onupadum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponnumela kannupadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponnumela kannupadum"/>
</div>
<div class="lyrico-lyrics-wrapper">Melathaalam paadivarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Melathaalam paadivarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinna pasanga aadi varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna pasanga aadi varum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mothathula thirushti padum da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mothathula thirushti padum da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sandhanatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhanatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey sandhanatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey sandhanatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sandhanatha karachi thaapa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhanatha karachi thaapa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannathula poosa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannathula poosa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thattu varusaya pirichu vaida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thattu varusaya pirichu vaida"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorae morachu paaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorae morachu paaka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hei hei hei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei hei hei"/>
</div>
<div class="lyrico-lyrics-wrapper">Udathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udathaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandhanatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhanatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandhanatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhanatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dumukkam purappukutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dumukkam purappukutha"/>
</div>
<div class="lyrico-lyrics-wrapper">Dummakuruppakku ruppukutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dummakuruppakku ruppukutha"/>
</div>
<div class="lyrico-lyrics-wrapper">Dumukkam purappukutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dumukkam purappukutha"/>
</div>
<div class="lyrico-lyrics-wrapper">Dummakuruppakku ruppukutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dummakuruppakku ruppukutha"/>
</div>
<div class="lyrico-lyrics-wrapper">Dumukkam purappukutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dumukkam purappukutha"/>
</div>
<div class="lyrico-lyrics-wrapper">Dummakuruppakku ruppukutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dummakuruppakku ruppukutha"/>
</div>
<div class="lyrico-lyrics-wrapper">Dumukkam purappukutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dumukkam purappukutha"/>
</div>
<div class="lyrico-lyrics-wrapper">Podu machi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podu machi"/>
</div>
</pre>
