---
title: "comrade anthem song lyrics"
album: "Dear Comrade"
artist: "Justin Prabhakaran"
lyricist: "Chaitanya Prasad"
director: "Bharat Kamma"
path: "/albums/dear-comrade-lyrics"
song: "Comrade Anthem"
image: ../../images/albumart/dear-comrade.jpg
date: 2019-07-26
lang: telugu
youtubeLink: "https://www.youtube.com/embed/hrVEH_YqBdA"
type: "mass"
singers:
  - Vijay Deverakonda
  - Stony Psyko
  - MC Vickey
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Telugu Prajalara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Telugu Prajalara"/>
</div>
<div class="lyrico-lyrics-wrapper">Manamika Chalu Padha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manamika Chalu Padha"/>
</div>
<div class="lyrico-lyrics-wrapper">You Know Who This Is
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Know Who This Is"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaati Matha Marachi Padha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaati Matha Marachi Padha"/>
</div>
<div class="lyrico-lyrics-wrapper">Comrade Devarakonda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Comrade Devarakonda"/>
</div>
<div class="lyrico-lyrics-wrapper">Entha Kaalam Baanisatwam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entha Kaalam Baanisatwam"/>
</div>
<div class="lyrico-lyrics-wrapper">Let’S Go Let’S Go Let’S Go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Let’S Go Let’S Go Let’S Go"/>
</div>
<div class="lyrico-lyrics-wrapper">Sontha Balam Telusuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sontha Balam Telusuko"/>
</div>
<div class="lyrico-lyrics-wrapper">Mounaminka Chaalu Padha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mounaminka Chaalu Padha"/>
</div>
<div class="lyrico-lyrics-wrapper">Pidikilettu Eeka Fight Like A
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pidikilettu Eeka Fight Like A"/>
</div>
<div class="lyrico-lyrics-wrapper">Comrade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Comrade"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaati Matha Marachi Padha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaati Matha Marachi Padha"/>
</div>
<div class="lyrico-lyrics-wrapper">Manchini Penchaga Be Like A
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manchini Penchaga Be Like A"/>
</div>
<div class="lyrico-lyrics-wrapper">Comrade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Comrade"/>
</div>
<div class="lyrico-lyrics-wrapper">Entha Kaalam Baanisatwam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entha Kaalam Baanisatwam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Bhavaaalu Maarchaga Vachaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Bhavaaalu Maarchaga Vachaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Comrade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Comrade"/>
</div>
<div class="lyrico-lyrics-wrapper">Sontha Balam Telusuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sontha Balam Telusuko"/>
</div>
<div class="lyrico-lyrics-wrapper">Andaga Vundaga Vacchinadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andaga Vundaga Vacchinadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Comrade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Comrade"/>
</div>
<div class="lyrico-lyrics-wrapper">Chepparaa Guru Idhi Manam Kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chepparaa Guru Idhi Manam Kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Never Gonna Give Up
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Never Gonna Give Up"/>
</div>
<div class="lyrico-lyrics-wrapper">Samarame Praanam Erupu Mayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samarame Praanam Erupu Mayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana Kanulalo Krodham Yegasipade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana Kanulalo Krodham Yegasipade"/>
</div>
<div class="lyrico-lyrics-wrapper">Yuva Kakinada Theeram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yuva Kakinada Theeram"/>
</div>
<div class="lyrico-lyrics-wrapper">Oovvethu Keratala Udhyama Baata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oovvethu Keratala Udhyama Baata"/>
</div>
<div class="lyrico-lyrics-wrapper">Goondaala Thandaaku Bedharamu Beta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Goondaala Thandaaku Bedharamu Beta"/>
</div>
<div class="lyrico-lyrics-wrapper">Maathoti Thodagotti Padoddhu Poti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maathoti Thodagotti Padoddhu Poti"/>
</div>
<div class="lyrico-lyrics-wrapper">Studentsu Okataithe Meekedhi Safety
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Studentsu Okataithe Meekedhi Safety"/>
</div>
<div class="lyrico-lyrics-wrapper">Poraatamagadhu Aaratamagadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraatamagadhu Aaratamagadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maarpemi Thekunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maarpemi Thekunda"/>
</div>
<div class="lyrico-lyrics-wrapper">Maa Kopamanagaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa Kopamanagaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Iquilab
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iquilab"/>
</div>
<div class="lyrico-lyrics-wrapper">Vardhillu Vardhillu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vardhillu Vardhillu"/>
</div>
<div class="lyrico-lyrics-wrapper">Live Like A Comrade!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Live Like A Comrade!"/>
</div>
<div class="lyrico-lyrics-wrapper">Bayamu Vadhilesthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bayamu Vadhilesthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Yavaraddukunnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yavaraddukunnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jayamu Needele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jayamu Needele"/>
</div>
<div class="lyrico-lyrics-wrapper">Padhamu Kadhilisthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padhamu Kadhilisthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuv Cheruthavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuv Cheruthavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gagana Shikaraale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gagana Shikaraale"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuv Chedunu Kanaradhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuv Chedunu Kanaradhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chedunu Vinaradhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chedunu Vinaradhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sookthi Vallisthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sookthi Vallisthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Saagithe Saripodhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saagithe Saripodhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Lokam Lo Yemi Jarigina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lokam Lo Yemi Jarigina"/>
</div>
<div class="lyrico-lyrics-wrapper">Naakentantu Undiporaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naakentantu Undiporaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Choopu Chacchi Maata Chacchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choopu Chacchi Maata Chacchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Chevude Vacchi Shavamu Kaaraadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chevude Vacchi Shavamu Kaaraadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ishtamaina Dhani Kosam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishtamaina Dhani Kosam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kastamaina Kalisi Poraadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kastamaina Kalisi Poraadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Bathakadamoka Hakkuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bathakadamoka Hakkuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Hakku Kosamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Hakku Kosamai"/>
</div>
<div class="lyrico-lyrics-wrapper">Galam Vippara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Galam Vippara"/>
</div>
<div class="lyrico-lyrics-wrapper">Odidhudukulu Misirina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odidhudukulu Misirina"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuv Yedhure Nilichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuv Yedhure Nilichi"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhure Nilichi Kadham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhure Nilichi Kadham"/>
</div>
<div class="lyrico-lyrics-wrapper">Thokkaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thokkaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Jeevana Samaramlo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Jeevana Samaramlo"/>
</div>
<div class="lyrico-lyrics-wrapper">Bujam Thatti Nee Dwajam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bujam Thatti Nee Dwajam"/>
</div>
<div class="lyrico-lyrics-wrapper">Patti-Ye Kastanastamulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Patti-Ye Kastanastamulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhuraina Nee Venta Vacchedhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhuraina Nee Venta Vacchedhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Comradokkade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Comradokkade"/>
</div>
<div class="lyrico-lyrics-wrapper">Bathakaku Bayapadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bathakaku Bayapadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Be Like A Comrade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Be Like A Comrade"/>
</div>
<div class="lyrico-lyrics-wrapper">Yegasina Youth Idhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yegasina Youth Idhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheyadhinka Tolerate
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheyadhinka Tolerate"/>
</div>
<div class="lyrico-lyrics-wrapper">Live Like A Comrade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Live Like A Comrade"/>
</div>
<div class="lyrico-lyrics-wrapper">Chei Sakthulani Activate
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chei Sakthulani Activate"/>
</div>
<div class="lyrico-lyrics-wrapper">Live Like A Comrade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Live Like A Comrade"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey Avvu Nuvvu Motivate
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey Avvu Nuvvu Motivate"/>
</div>
<div class="lyrico-lyrics-wrapper">Live Like A Comrade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Live Like A Comrade"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Route Mottham Separate
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Route Mottham Separate"/>
</div>
<div class="lyrico-lyrics-wrapper">Live Like A Comrade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Live Like A Comrade"/>
</div>
<div class="lyrico-lyrics-wrapper">Chei Jeevitham Liberate
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chei Jeevitham Liberate"/>
</div>
<div class="lyrico-lyrics-wrapper">Mounaminka Chaalu Padha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mounaminka Chaalu Padha"/>
</div>
<div class="lyrico-lyrics-wrapper">Pidikilettu Eeka Fight Like A
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pidikilettu Eeka Fight Like A"/>
</div>
<div class="lyrico-lyrics-wrapper">Comrade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Comrade"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaati Matha Marachi Padha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaati Matha Marachi Padha"/>
</div>
<div class="lyrico-lyrics-wrapper">Manchini Penchaga Be Like A
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manchini Penchaga Be Like A"/>
</div>
<div class="lyrico-lyrics-wrapper">Comrade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Comrade"/>
</div>
<div class="lyrico-lyrics-wrapper">Entha Kaalam Baanisatwam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entha Kaalam Baanisatwam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Bhavaaalu Maarchaga Vachaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Bhavaaalu Maarchaga Vachaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Comrade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Comrade"/>
</div>
<div class="lyrico-lyrics-wrapper">Sontha Balam Telusuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sontha Balam Telusuko"/>
</div>
</pre>
