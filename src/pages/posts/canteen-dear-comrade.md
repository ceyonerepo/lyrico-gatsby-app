---
title: "gira gira song lyrics"
album: "Dear Comrade"
artist: "Justin Prabhakaran"
lyricist: "Rehman"
director: "Bharat Kamma"
path: "/albums/dear-comrade-lyrics"
song: "Gira Gira"
image: ../../images/albumart/dear-comrade.jpg
date: 2019-07-26
lang: telugu
youtubeLink: "https://www.youtube.com/embed/0qWrtL862q8"
type: "love"
singers:
  - Gowtham Bharathwaj
  - Yamini Ghantasala
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Gira Gira Gira Thiragali Laaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gira Gira Gira Thiragali Laaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirigi Arigi Poyinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirigi Arigi Poyinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinuse Nalagaaledhule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinuse Nalagaaledhule"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoi Hoi Hoi Hoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoi Hoi Hoi Hoi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aluperagaka Thana Venakaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aluperagaka Thana Venakaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Alasi Solasi Poyinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alasi Solasi Poyinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Manase Karagaaledhule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manase Karagaaledhule"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoi Hoi Hoi Hoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoi Hoi Hoi Hoi"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinadhemo Thirige Chudadhe.
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinadhemo Thirige Chudadhe."/>
</div>
<div class="lyrico-lyrics-wrapper">Premante Asale Padadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Premante Asale Padadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoi"/>
</div>
<div class="lyrico-lyrics-wrapper">Gira Gira Gira Thiragali Laaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gira Gira Gira Thiragali Laaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirigi Arigi Poyinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirigi Arigi Poyinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinuse Nalagaaledhule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinuse Nalagaaledhule"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoi Hoi Hoi Hoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoi Hoi Hoi Hoi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aluperagaka Thana Venakaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aluperagaka Thana Venakaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Alasi Solasi Poyinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alasi Solasi Poyinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Manase Karagaaledhule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manase Karagaaledhule"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoi Hoi Hoi Hoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoi Hoi Hoi Hoi"/>
</div>
<div class="lyrico-lyrics-wrapper">Alalu Alisi Chathikilapaduna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alalu Alisi Chathikilapaduna"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalalu Nilichi Akalavara Paduna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalalu Nilichi Akalavara Paduna"/>
</div>
<div class="lyrico-lyrics-wrapper">Sahaja Gunamu Nimishamu Vidana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sahaja Gunamu Nimishamu Vidana"/>
</div>
<div class="lyrico-lyrics-wrapper">Emi Jariginaa..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emi Jariginaa.."/>
</div>
<div class="lyrico-lyrics-wrapper">Manasunepudu Viduvani Thapana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasunepudu Viduvani Thapana"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinadhu Asalu Evaremaninaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinadhu Asalu Evaremaninaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaganamorigi Thanapai Padina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaganamorigi Thanapai Padina"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasha Karugunaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasha Karugunaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Veysavilona Penuthapam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veysavilona Penuthapam"/>
</div>
<div class="lyrico-lyrics-wrapper">O Aaratam La Ningini Thaaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Aaratam La Ningini Thaaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhigi Raadha Varshamlaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhigi Raadha Varshamlaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gira Gira Gira Thiragali Laaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gira Gira Gira Thiragali Laaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirigi Arigi Poyinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirigi Arigi Poyinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinuse Nalagaaledhule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinuse Nalagaaledhule"/>
</div>
<div class="lyrico-lyrics-wrapper">Sannayi Dolu Pelli Paata Paade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sannayi Dolu Pelli Paata Paade"/>
</div>
<div class="lyrico-lyrics-wrapper">Abbayi Vorakanta Chusthunnade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Abbayi Vorakanta Chusthunnade"/>
</div>
<div class="lyrico-lyrics-wrapper">Bangaaru Bomma Thala Yetthi Choode
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bangaaru Bomma Thala Yetthi Choode"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Eedu Jode Andaala Chanduroode
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Eedu Jode Andaala Chanduroode"/>
</div>
<div class="lyrico-lyrics-wrapper">Evarikevaru Theliyadhu Munupu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evarikevaru Theliyadhu Munupu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adigi Adigi Kalagadhu Valapu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adigi Adigi Kalagadhu Valapu"/>
</div>
<div class="lyrico-lyrics-wrapper">Okarikokaru Ani Kalapanidhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okarikokaru Ani Kalapanidhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Manani Vadhulunaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manani Vadhulunaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Eduru Padina Kshanamoka Malupu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eduru Padina Kshanamoka Malupu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adugu Kalipi Kadhilithe Gelupu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adugu Kalipi Kadhilithe Gelupu"/>
</div>
<div class="lyrico-lyrics-wrapper">Disalu Rendu Verai Unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Disalu Rendu Verai Unna"/>
</div>
<div class="lyrico-lyrics-wrapper">Payanamaagunaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Payanamaagunaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenante Thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenante Thaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaane Nene Okatai Unnane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaane Nene Okatai Unnane"/>
</div>
<div class="lyrico-lyrics-wrapper">Pommanna Pone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pommanna Pone"/>
</div>
<div class="lyrico-lyrics-wrapper">Padathaane Lesthaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padathaane Lesthaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Gira Gira Gira Thiragali Laaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gira Gira Gira Thiragali Laaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirigi Arigi Poyinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirigi Arigi Poyinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinuse Nalagaaledhule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinuse Nalagaaledhule"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoi Hoi Hoi Hoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoi Hoi Hoi Hoi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aluperagaka Thana Venakaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aluperagaka Thana Venakaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Alasi Solasi Poyinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alasi Solasi Poyinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Manase Karagaaledhule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manase Karagaaledhule"/>
</div>
</pre>
