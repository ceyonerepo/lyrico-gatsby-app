---
title: "oru manam song lyrics"
album: "Dhuruva Natchathiram"
artist: "Harris Jayaraj"
lyricist: "Thamarai"
director: "Gowtham Vasudev Menan"
path: "/albums/dhuruva-natchathiram-song-lyrics"
song: "Oru Manam"
image: ../../images/albumart/Dhruva-Natchathiram.jpg
date: 2020-10-08
youtubeLink: "https://www.youtube.com/embed/8unt57KrZLI"
type: "love"
lang: tamil
singers:
  - Karthik, Shashaa Tirupati
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Oru Manam Nirkka Solluthey 
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru Manam Nirkka Solluthey "/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Manam Etti Thalluthey 
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru Manam Etti Thalluthey "/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhai Nanum Ketpadhu 
<input type="checkbox" class="lyrico-select-lyric-line" value="Yedhai Nanum Ketpadhu "/>
</div>
<div class="lyrico-lyrics-wrapper">Thadumatram Thakudhu 
<input type="checkbox" class="lyrico-select-lyric-line" value="Thadumatram Thakudhu "/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinarasri Unnai Parkavey 
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhinarasri Unnai Parkavey "/>
</div>
<div class="lyrico-lyrics-wrapper">Thirudiya Nenjai Meetkavey 
<input type="checkbox" class="lyrico-select-lyric-line" value="Thirudiya Nenjai Meetkavey "/>
</div>
<div class="lyrico-lyrics-wrapper">Un Veettai Thedava 
<input type="checkbox" class="lyrico-select-lyric-line" value="Un Veettai Thedava "/>
</div>
<div class="lyrico-lyrics-wrapper">Urangamal Theiyava 
<input type="checkbox" class="lyrico-select-lyric-line" value="Urangamal Theiyava "/>
</div>
<div class="lyrico-lyrics-wrapper">Oho Ho Ho Dhinam 
<input type="checkbox" class="lyrico-select-lyric-line" value="Oho Ho Ho Dhinam "/>
</div>
<div class="lyrico-lyrics-wrapper">Oho Ho Ho Ganam 
<input type="checkbox" class="lyrico-select-lyric-line" value="Oho Ho Ho Ganam "/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Vandha Soppanam 
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee Vandha Soppanam "/>
</div>
<div class="lyrico-lyrics-wrapper">Ninavil Narthanam 
<input type="checkbox" class="lyrico-select-lyric-line" value="Ninavil Narthanam "/>
</div>
<div class="lyrico-lyrics-wrapper">Oho Ho Ho Varum 
<input type="checkbox" class="lyrico-select-lyric-line" value="Oho Ho Ho Varum "/>
</div>
<div class="lyrico-lyrics-wrapper">Oho Ho Ho Kanam 
<input type="checkbox" class="lyrico-select-lyric-line" value="Oho Ho Ho Kanam "/>
</div>
<div class="lyrico-lyrics-wrapper">En Anbe Ariyram Dhinam Varum 
<input type="checkbox" class="lyrico-select-lyric-line" value="En Anbe Ariyram Dhinam Varum "/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Dhan Mudhal Kanam 
<input type="checkbox" class="lyrico-select-lyric-line" value="Idhu Dhan Mudhal Kanam "/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Manam Nirkka Solluthe 
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru Manam Nirkka Solluthe "/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Manam Etti Thalluthe 
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru Manam Etti Thalluthe "/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhai Nanum Ketpadhu 
<input type="checkbox" class="lyrico-select-lyric-line" value="Yedhai Nanum Ketpadhu "/>
</div>
<div class="lyrico-lyrics-wrapper">Thadumatram Thakudhu 
<input type="checkbox" class="lyrico-select-lyric-line" value="Thadumatram Thakudhu "/>
</div>
<div class="lyrico-lyrics-wrapper">Innum Enna Idaiveli 
<input type="checkbox" class="lyrico-select-lyric-line" value="Innum Enna Idaiveli "/>
</div>
<div class="lyrico-lyrics-wrapper">Dhooram Maruthali 
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhooram Maruthali "/>
</div>
<div class="lyrico-lyrics-wrapper">Pakkam Vandhal Anumadhi 
<input type="checkbox" class="lyrico-select-lyric-line" value="Pakkam Vandhal Anumadhi "/>
</div>
<div class="lyrico-lyrics-wrapper">Podhum Arai Nodi 
<input type="checkbox" class="lyrico-select-lyric-line" value="Podhum Arai Nodi "/>
</div>
<div class="lyrico-lyrics-wrapper">Ho Ennai Unnai Pirithidum 
<input type="checkbox" class="lyrico-select-lyric-line" value="Ho Ennai Unnai Pirithidum "/>
</div>
<div class="lyrico-lyrics-wrapper">Katril Kadhakali 
<input type="checkbox" class="lyrico-select-lyric-line" value="Katril Kadhakali "/>
</div>
<div class="lyrico-lyrics-wrapper">Mele Nindru Sirithidum 
<input type="checkbox" class="lyrico-select-lyric-line" value="Mele Nindru Sirithidum "/>
</div>
<div class="lyrico-lyrics-wrapper">Manjal Nilavozhi 
<input type="checkbox" class="lyrico-select-lyric-line" value="Manjal Nilavozhi "/>
</div>
<div class="lyrico-lyrics-wrapper">Haa! Thee Moottum Vanathai 
<input type="checkbox" class="lyrico-select-lyric-line" value="Haa! Thee Moottum Vanathai "/>
</div>
<div class="lyrico-lyrics-wrapper">Thitta Pogiren 
<input type="checkbox" class="lyrico-select-lyric-line" value="Thitta Pogiren "/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhai Vandhum Kaivathal 
<input type="checkbox" class="lyrico-select-lyric-line" value="Mazhai Vandhum Kaivathal "/>
</div>
<div class="lyrico-lyrics-wrapper">Mutham Thedinen 
<input type="checkbox" class="lyrico-select-lyric-line" value="Mutham Thedinen "/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Puram Nanam Killuthe 
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru Puram Nanam Killuthe "/>
</div>
<div class="lyrico-lyrics-wrapper">Marupuram Asai Thalluthe 
<input type="checkbox" class="lyrico-select-lyric-line" value="Marupuram Asai Thalluthe "/>
</div>
<div class="lyrico-lyrics-wrapper">Enai Nanum Kedpadhu 
<input type="checkbox" class="lyrico-select-lyric-line" value="Enai Nanum Kedpadhu "/>
</div>
<div class="lyrico-lyrics-wrapper">Thadumatram Thakuthu 
<input type="checkbox" class="lyrico-select-lyric-line" value="Thadumatram Thakuthu "/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinarasri Unnai Parkave 
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhinarasri Unnai Parkave "/>
</div>
<div class="lyrico-lyrics-wrapper">Thirudiya Nenjai Meetkave 
<input type="checkbox" class="lyrico-select-lyric-line" value="Thirudiya Nenjai Meetkave "/>
</div>
<div class="lyrico-lyrics-wrapper">En Veettai Thedi Va 
<input type="checkbox" class="lyrico-select-lyric-line" value="En Veettai Thedi Va "/>
</div>
<div class="lyrico-lyrics-wrapper">Urangamal Theiyava 
<input type="checkbox" class="lyrico-select-lyric-line" value="Urangamal Theiyava "/>
</div>
<div class="lyrico-lyrics-wrapper">Vanam Peiyakadavadhu 
<input type="checkbox" class="lyrico-select-lyric-line" value="Vanam Peiyakadavadhu "/>
</div>
<div class="lyrico-lyrics-wrapper">Eeram Iniyadhu 
<input type="checkbox" class="lyrico-select-lyric-line" value="Eeram Iniyadhu "/>
</div>
<div class="lyrico-lyrics-wrapper">Muththam Kondu Thudaippadhu 
<input type="checkbox" class="lyrico-select-lyric-line" value="Muththam Kondu Thudaippadhu "/>
</div>
<div class="lyrico-lyrics-wrapper">Innum Eliyadhu 
<input type="checkbox" class="lyrico-select-lyric-line" value="Innum Eliyadhu "/>
</div>
<div class="lyrico-lyrics-wrapper">Ulle Thongum Anal Idhu 
<input type="checkbox" class="lyrico-select-lyric-line" value="Ulle Thongum Anal Idhu "/>
</div>
<div class="lyrico-lyrics-wrapper">Urakkam Kalaiyudhu 
<input type="checkbox" class="lyrico-select-lyric-line" value="Urakkam Kalaiyudhu "/>
</div>
<div class="lyrico-lyrics-wrapper">Eththanai Natkal Poruppadhu 
<input type="checkbox" class="lyrico-select-lyric-line" value="Eththanai Natkal Poruppadhu "/>
</div>
<div class="lyrico-lyrics-wrapper">Yengi Thavikkudhu 
<input type="checkbox" class="lyrico-select-lyric-line" value="Yengi Thavikkudhu "/>
</div>
<div class="lyrico-lyrics-wrapper">Ho Nan Indru Nan Illai 
<input type="checkbox" class="lyrico-select-lyric-line" value="Ho Nan Indru Nan Illai "/>
</div>
<div class="lyrico-lyrics-wrapper">Nanal Agiren 
<input type="checkbox" class="lyrico-select-lyric-line" value="Nanal Agiren "/>
</div>
<div class="lyrico-lyrics-wrapper">La La Lala 
<input type="checkbox" class="lyrico-select-lyric-line" value="La La Lala "/>
</div>
<div class="lyrico-lyrics-wrapper">Nadhi Pole Nee Sendral 
<input type="checkbox" class="lyrico-select-lyric-line" value="Nadhi Pole Nee Sendral "/>
</div>
<div class="lyrico-lyrics-wrapper">Nanum Valaigiren 
<input type="checkbox" class="lyrico-select-lyric-line" value="Nanum Valaigiren "/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Manam Nirkka Solluthe 
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru Manam Nirkka Solluthe "/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Manam Etti Thalluthe 
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru Manam Etti Thalluthe "/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhai Nanum Ketpadhu 
<input type="checkbox" class="lyrico-select-lyric-line" value="Yedhai Nanum Ketpadhu "/>
</div>
<div class="lyrico-lyrics-wrapper">Thadumatram Thakudhu 
<input type="checkbox" class="lyrico-select-lyric-line" value="Thadumatram Thakudhu "/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinarasri Unnai Parkave 
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhinarasri Unnai Parkave "/>
</div>
<div class="lyrico-lyrics-wrapper">Thirudiya Nenjai Meetkave 
<input type="checkbox" class="lyrico-select-lyric-line" value="Thirudiya Nenjai Meetkave "/>
</div>
<div class="lyrico-lyrics-wrapper">En Veettai Thedi Va 
<input type="checkbox" class="lyrico-select-lyric-line" value="En Veettai Thedi Va "/>
</div>
<div class="lyrico-lyrics-wrapper">Urangamal Theiyava 
<input type="checkbox" class="lyrico-select-lyric-line" value="Urangamal Theiyava "/>
</div>
<div class="lyrico-lyrics-wrapper">Oho Ho Ho Dhinam 
<input type="checkbox" class="lyrico-select-lyric-line" value="Oho Ho Ho Dhinam "/>
</div>
<div class="lyrico-lyrics-wrapper">Oho Ho Ho Ganam 
<input type="checkbox" class="lyrico-select-lyric-line" value="Oho Ho Ho Ganam "/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Vandha Soppanam 
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee Vandha Soppanam "/>
</div>
<div class="lyrico-lyrics-wrapper">Ninavil Narthanam 
<input type="checkbox" class="lyrico-select-lyric-line" value="Ninavil Narthanam "/>
</div>
<div class="lyrico-lyrics-wrapper">Oho Ho Ho Varum 
<input type="checkbox" class="lyrico-select-lyric-line" value="Oho Ho Ho Varum "/>
</div>
<div class="lyrico-lyrics-wrapper">Oho Ho Ho Kanam 
<input type="checkbox" class="lyrico-select-lyric-line" value="Oho Ho Ho Kanam "/>
</div>
<div class="lyrico-lyrics-wrapper">En Anbe Ariyram Dhinam Varum 
<input type="checkbox" class="lyrico-select-lyric-line" value="En Anbe Ariyram Dhinam Varum "/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Dhan Mudhal Kanam.
<input type="checkbox" class="lyrico-select-lyric-line" value="Idhu Dhan Mudhal Kanam."/>
</div>
</pre>
