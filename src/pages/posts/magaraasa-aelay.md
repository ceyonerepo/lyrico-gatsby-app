---
title: "magaraasa song lyrics"
album: "Aelay"
artist: "Kaber Vasuki - Aruldev"
lyricist: "Kaber Vasuki"
director: "Halitha Shameem"
path: "/albums/aelay-song-lyrics"
song: "Magaraasa"
image: ../../images/albumart/aelay.jpg
date: 2021-002-28
lang: tamil
youtubeLink: "https://www.youtube.com/embed/gs_0uj8ZTWI"
type: "Sad"
singers:
  - Kaber Vasuki
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">unai naan veruthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unai naan veruthen"/>
</div>
<div class="lyrico-lyrics-wrapper">mulusha maruthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mulusha maruthen"/>
</div>
<div class="lyrico-lyrics-wrapper">ipo nee pona pin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ipo nee pona pin"/>
</div>
<div class="lyrico-lyrics-wrapper">nan yen odanchen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan yen odanchen"/>
</div>
<div class="lyrico-lyrics-wrapper">un ullam kaana than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un ullam kaana than"/>
</div>
<div class="lyrico-lyrics-wrapper">nanum kaaaathu irunthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanum kaaaathu irunthen"/>
</div>
<div class="lyrico-lyrics-wrapper">un unmai kanda pin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un unmai kanda pin"/>
</div>
<div class="lyrico-lyrics-wrapper">neyum karainthathu yeen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neyum karainthathu yeen"/>
</div>
<div class="lyrico-lyrics-wrapper">unai naan veruthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unai naan veruthen"/>
</div>
<div class="lyrico-lyrics-wrapper">mulusha maruthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mulusha maruthen"/>
</div>
<div class="lyrico-lyrics-wrapper">ipo nee pona pin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ipo nee pona pin"/>
</div>
<div class="lyrico-lyrics-wrapper">nan yen aluthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan yen aluthen"/>
</div>
<div class="lyrico-lyrics-wrapper">un ullam kaana thane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un ullam kaana thane"/>
</div>
<div class="lyrico-lyrics-wrapper">nanum kaaaathu irunthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanum kaaaathu irunthen"/>
</div>
<div class="lyrico-lyrics-wrapper">un unmai kanda pinne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un unmai kanda pinne"/>
</div>
<div class="lyrico-lyrics-wrapper">neyum karainthathu yeen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neyum karainthathu yeen"/>
</div>
</pre>
