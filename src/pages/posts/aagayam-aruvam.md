---
title: "aagayam song lyrics"
album: "Aruvam"
artist: "S. Thaman"
lyricist: "Madhan Karky"
director: "Sai Sekhar"
path: "/albums/aruvam-lyrics"
song: "Aagayam"
image: ../../images/albumart/aruvam.jpg
date: 2019-10-11
lang: tamil
youtubeLink: "https://www.youtube.com/embed/xPwrp1oBn-o"
type: "happy"
singers:
  - Roshini
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aagayam boomi engum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagayam boomi engum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanandha vaasam vandhadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanandha vaasam vandhadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Anumadhiyae kelaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anumadhiyae kelaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Moochu kaatril bashai vandhadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moochu kaatril bashai vandhadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kindal kaeligal nindradhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kindal kaeligal nindradhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Konda sogamum sendrathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konda sogamum sendrathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai aanandham thindrathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai aanandham thindrathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Inbam aarambam endrathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inbam aarambam endrathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thadagal meeri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadagal meeri"/>
</div>
<div class="lyrico-lyrics-wrapper">Padigal yaeri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padigal yaeri"/>
</div>
<div class="lyrico-lyrics-wrapper">Siragu indru parandhu selludhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siragu indru parandhu selludhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidhiyai vendru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidhiyai vendru"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhigal kandu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhigal kandu"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhaiyum thaangum idhayam vandhathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhaiyum thaangum idhayam vandhathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aagayam boomi engum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagayam boomi engum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanandha vaasam vandhadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanandha vaasam vandhadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Anumadhiyae kelaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anumadhiyae kelaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Moochu kaatril bashai vandhadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moochu kaatril bashai vandhadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aathaadi endhan kaalgal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aathaadi endhan kaalgal"/>
</div>
<div class="lyrico-lyrics-wrapper">Aattam poduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aattam poduthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Koothaadum kuruvi nooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koothaadum kuruvi nooru"/>
</div>
<div class="lyrico-lyrics-wrapper">Koottam kooduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koottam kooduthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Indru paadam solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indru paadam solla"/>
</div>
<div class="lyrico-lyrics-wrapper">Ppallikoodam kooda illaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ppallikoodam kooda illaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha sevai seiyum podhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha sevai seiyum podhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Inbam ellai illaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inbam ellai illaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ammaadi aanandham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammaadi aanandham"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannai kattudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannai kattudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Gummaalaam ittu aasai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gummaalaam ittu aasai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kummi kottudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kummi kottudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andraadame anbaal oru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andraadame anbaal oru"/>
</div>
<div class="lyrico-lyrics-wrapper">Sambathiyam naamum seivom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sambathiyam naamum seivom"/>
</div>
<div class="lyrico-lyrics-wrapper">Nandraagavae naam vazhavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nandraagavae naam vazhavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Narchevaigal naalum seivom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Narchevaigal naalum seivom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jarigai pattu selai pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jarigai pattu selai pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Chennai nagara saalai minnudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chennai nagara saalai minnudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Jallikattu kaalai pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jallikattu kaalai pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellai meeri ullam thulludhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellai meeri ullam thulludhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aagayam boomi engum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagayam boomi engum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanandha vaasam vandhadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanandha vaasam vandhadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Anumadhiyae kelaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anumadhiyae kelaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Moochu kaatril bashai vandhadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moochu kaatril bashai vandhadhae"/>
</div>
</pre>
