---
title: "evalthaan evalthaan song lyrics"
album: "Chasing"
artist: "Thashi"
lyricist: "S. Sivakumar"
director: "Veerakumar"
path: "/albums/chasing-song-lyrics"
song: "Evalthaan Evalthaan"
image: ../../images/albumart/chasing.jpg
date: 2021-04-16
lang: tamil
youtubeLink: "https://www.youtube.com/embed/vfG00y5nMdM"
type: "Theme Song"
singers:
  - Balaji
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">evalthaan evalthaan pennada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="evalthaan evalthaan pennada"/>
</div>
<div class="lyrico-lyrics-wrapper">irumbai pootha purachi pennada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irumbai pootha purachi pennada"/>
</div>
<div class="lyrico-lyrics-wrapper">evalthaan evalthaan pennada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="evalthaan evalthaan pennada"/>
</div>
<div class="lyrico-lyrics-wrapper">irumbai pootha purachi pennada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irumbai pootha purachi pennada"/>
</div>
<div class="lyrico-lyrics-wrapper">kambu suthuvaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kambu suthuvaal"/>
</div>
<div class="lyrico-lyrics-wrapper">surul kathiyum veesuval
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="surul kathiyum veesuval"/>
</div>
<div class="lyrico-lyrics-wrapper">kabadi aaduvaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kabadi aaduvaal"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhakaliyum aaduvaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhakaliyum aaduvaal"/>
</div>
<div class="lyrico-lyrics-wrapper">thedi thedi veetai aaduvaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thedi thedi veetai aaduvaal"/>
</div>
<div class="lyrico-lyrics-wrapper">thittam pottu theethu kattuval
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thittam pottu theethu kattuval"/>
</div>
<div class="lyrico-lyrics-wrapper">saavai kandu anja maataal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saavai kandu anja maataal"/>
</div>
<div class="lyrico-lyrics-wrapper">sadhanaigal pala senjuduvaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sadhanaigal pala senjuduvaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">evalthaan evalthaan pennada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="evalthaan evalthaan pennada"/>
</div>
<div class="lyrico-lyrics-wrapper">irumbai pootha purachi pennada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irumbai pootha purachi pennada"/>
</div>
<div class="lyrico-lyrics-wrapper">evalthaan evalthaan pennada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="evalthaan evalthaan pennada"/>
</div>
<div class="lyrico-lyrics-wrapper">irumbai pootha purachi pennada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irumbai pootha purachi pennada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaatrum neruppum engada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatrum neruppum engada"/>
</div>
<div class="lyrico-lyrics-wrapper">intha pennuku panju polada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha pennuku panju polada"/>
</div>
<div class="lyrico-lyrics-wrapper">acham ivalidam illaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="acham ivalidam illaye"/>
</div>
<div class="lyrico-lyrics-wrapper">aayutham kaiyil irukuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aayutham kaiyil irukuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">neethiyai nilaiyai kaathida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neethiyai nilaiyai kaathida"/>
</div>
<div class="lyrico-lyrics-wrapper">piranthu vantha devathaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="piranthu vantha devathaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">paarthaal mugathil kovame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paarthaal mugathil kovame"/>
</div>
<div class="lyrico-lyrics-wrapper">ethirikum thookam ponethe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethirikum thookam ponethe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">evalthaan evalthaan pennada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="evalthaan evalthaan pennada"/>
</div>
<div class="lyrico-lyrics-wrapper">irumbai pootha purachi pennada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irumbai pootha purachi pennada"/>
</div>
<div class="lyrico-lyrics-wrapper">evalthaan evalthaan pennada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="evalthaan evalthaan pennada"/>
</div>
<div class="lyrico-lyrics-wrapper">irumbai pootha purachi pennada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irumbai pootha purachi pennada"/>
</div>
<div class="lyrico-lyrics-wrapper">kambu suthuvaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kambu suthuvaal"/>
</div>
<div class="lyrico-lyrics-wrapper">surul kathiyum veesuval
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="surul kathiyum veesuval"/>
</div>
<div class="lyrico-lyrics-wrapper">kabadi aaduvaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kabadi aaduvaal"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhakaliyum aaduvaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhakaliyum aaduvaal"/>
</div>
<div class="lyrico-lyrics-wrapper">thedi thedi veetai aaduvaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thedi thedi veetai aaduvaal"/>
</div>
<div class="lyrico-lyrics-wrapper">thittam pottu theethu kattuval
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thittam pottu theethu kattuval"/>
</div>
<div class="lyrico-lyrics-wrapper">saavai kandu anja maataal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saavai kandu anja maataal"/>
</div>
<div class="lyrico-lyrics-wrapper">sadhanaigal pala senjuduvaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sadhanaigal pala senjuduvaal"/>
</div>
</pre>
