---
title: "oor aayiram vanavil song lyrics"
album: "99 Songs"
artist: "A.R. Rahman"
lyricist: "Vivek"
director: "Vishwesh Krishnamoorthy"
path: "/albums/99-songs-lyrics"
song: "Oor Aayiram Vanavil"
image: ../../images/albumart/99-songs.jpg
date: 2021-04-16
lang: tamil
youtubeLink: "https://www.youtube.com/embed/A5aQkxGiUkY"
type: "Love"
singers:
  - Vijay Yesudas
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Oor Aayiram Vaanavil Bhoomiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oor Aayiram Vaanavil Bhoomiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kangalum Theduthe Kaar Irul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kangalum Theduthe Kaar Irul"/>
</div>
<div class="lyrico-lyrics-wrapper">Poo Vaasangal Korthidum Bhoomiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poo Vaasangal Korthidum Bhoomiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Nesamum Veezhnthidum Veliyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Nesamum Veezhnthidum Veliyil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">un idhaya idhaya idhaya thiriyil peroli parava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un idhaya idhaya idhaya thiriyil peroli parava"/>
</div>
<div class="lyrico-lyrics-wrapper">nalloliyum unackul nuzhaindhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nalloliyum unackul nuzhaindhu"/>
</div>
<div class="lyrico-lyrics-wrapper">padara oor idam tharuga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="padara oor idam tharuga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oh koochale mounamai maaridu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oh koochale mounamai maaridu"/>
</div>
<div class="lyrico-lyrics-wrapper">oh vanjame nanmayai yeridu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oh vanjame nanmayai yeridu"/>
</div>
<div class="lyrico-lyrics-wrapper">perasaye thaanamai aagidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="perasaye thaanamai aagidu"/>
</div>
<div class="lyrico-lyrics-wrapper">oh inimaye ennil peidhidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oh inimaye ennil peidhidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">un idhaya idhaya idhaya thiriyil peroli parava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un idhaya idhaya idhaya thiriyil peroli parava"/>
</div>
<div class="lyrico-lyrics-wrapper">nalloliyum unackul nuzhaindhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nalloliyum unackul nuzhaindhu"/>
</div>
<div class="lyrico-lyrics-wrapper">padara oor idam tharuga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="padara oor idam tharuga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kilai veelndhinum pootharum nanmaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kilai veelndhinum pootharum nanmaram"/>
</div>
<div class="lyrico-lyrics-wrapper">mithitherinum thangidum odamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mithitherinum thangidum odamum"/>
</div>
<div class="lyrico-lyrics-wrapper">adithalume isai tharum melamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adithalume isai tharum melamum"/>
</div>
<div class="lyrico-lyrics-wrapper">ivai polave yengidum thaaimanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ivai polave yengidum thaaimanam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">un idhaya idhaya idhaya thiriyil peroli parava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un idhaya idhaya idhaya thiriyil peroli parava"/>
</div>
<div class="lyrico-lyrics-wrapper">nalloliyum unackul nuzhaindhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nalloliyum unackul nuzhaindhu"/>
</div>
<div class="lyrico-lyrics-wrapper">padara oor idam tharuga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="padara oor idam tharuga"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Idhaya Idhaya Idhaya Thiriyil Perozhi Paravum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Idhaya Idhaya Idhaya Thiriyil Perozhi Paravum"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu Ozhira Ozhira Ozhiyai Parugum Maanudam Muzhuthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu Ozhira Ozhira Ozhiyai Parugum Maanudam Muzhuthum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Amma Un Madi Podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma Un Madi Podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeye En Varamaagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeye En Varamaagum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellam Thandhai Verai Pola Nindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellam Thandhai Verai Pola Nindren"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnil Paniven Unthan Paadham Enge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnil Paniven Unthan Paadham Enge"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam Ellam Enthan Aavi Unadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam Ellam Enthan Aavi Unadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Amma Un Karunai Ethanai Koduthu Niraithiduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma Un Karunai Ethanai Koduthu Niraithiduven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oor Aayiram Vaanavil Bhoomiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oor Aayiram Vaanavil Bhoomiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kangalum Theduthe Kaar Irul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kangalum Theduthe Kaar Irul"/>
</div>
<div class="lyrico-lyrics-wrapper">Poo Vaasangal Korthidum Bhoomiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poo Vaasangal Korthidum Bhoomiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Nesamum Veezhnthidum Veliyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Nesamum Veezhnthidum Veliyil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Idhaya Idhaya Idhaya Thiriyil Perozhi Parava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Idhaya Idhaya Idhaya Thiriyil Perozhi Parava"/>
</div>
<div class="lyrico-lyrics-wrapper">Mel Ozhiyum Unakkul Nuzhainthu Padara Ooridum Tharuga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mel Ozhiyum Unakkul Nuzhainthu Padara Ooridum Tharuga"/>
</div>
</pre>
