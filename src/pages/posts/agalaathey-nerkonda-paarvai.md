---
title: 'agalaathey song lyrics'
album: 'Nerkonda Paarvai'
artist: 'Yuvan Shankar Raja'
lyricist: 'Pa.Vijay'
director: 'H.Vinoth'
path: '/albums/comali-song-lyrics'
song: 'Agalaathey'
image: ../../images/albumart/nerkonda-paarvai.jpg
date: 2019-08-08
youtubeLink: "https://www.youtube.com/embed/gcvOanDvc5U"
type: 'love'
lang: tamil
singers: 
- Yuvan Shankar Raja
- Prithivee
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Nadai paadhai poovanangal paarthu
<input type="checkbox" class="lyrico-select-lyric-line" value="Nadai paadhai poovanangal paarthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nigazhkala kanavugalil pooththu
<input type="checkbox" class="lyrico-select-lyric-line" value="Nigazhkala kanavugalil pooththu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru moochchin oosaiyilae
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru moochchin oosaiyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ondraai vazhdhiruppom… ooh..oohoo
<input type="checkbox" class="lyrico-select-lyric-line" value="Ondraai vazhdhiruppom… ooh..oohoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vaa ullankaigalai korthu
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaa ullankaigalai korthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai-regai mothamum serthu
<input type="checkbox" class="lyrico-select-lyric-line" value="Kai-regai mothamum serthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sila dhoora payangalil
<input type="checkbox" class="lyrico-select-lyric-line" value="Sila dhoora payangalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Siragaai serndhiruppom
<input type="checkbox" class="lyrico-select-lyric-line" value="Siragaai serndhiruppom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Agalaathae…. agalaathae
<input type="checkbox" class="lyrico-select-lyric-line" value="Agalaathae…. agalaathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nodikooda… nagaraadhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Nodikooda… nagaraadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sellaadhae..sellaadhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Sellaadhae..sellaadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanam thaandi pogaathae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kanam thaandi pogaathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Nagaraamal un mun nindrae
<input type="checkbox" class="lyrico-select-lyric-line" value="Nagaraamal un mun nindrae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pidivadham seiya vendum
<input type="checkbox" class="lyrico-select-lyric-line" value="Pidivadham seiya vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Asaraamal muththam thanthae
<input type="checkbox" class="lyrico-select-lyric-line" value="Asaraamal muththam thanthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Alangaaram seiya vendum
<input type="checkbox" class="lyrico-select-lyric-line" value="Alangaaram seiya vendum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Nadai paadhai poovanangal paarthu
<input type="checkbox" class="lyrico-select-lyric-line" value="Nadai paadhai poovanangal paarthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nigazhkala kanavugalil pooththu
<input type="checkbox" class="lyrico-select-lyric-line" value="Nigazhkala kanavugalil pooththu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru moochchin oosaiyilae
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru moochchin oosaiyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ondraai vazhdhiruppom… 
<input type="checkbox" class="lyrico-select-lyric-line" value="Ondraai vazhdhiruppom… "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vaa ullankaigalai korthu
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaa ullankaigalai korthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai-regai mothamum serthu
<input type="checkbox" class="lyrico-select-lyric-line" value="Kai-regai mothamum serthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sila dhoora payangalil
<input type="checkbox" class="lyrico-select-lyric-line" value="Sila dhoora payangalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Siragaai serndhiruppom
<input type="checkbox" class="lyrico-select-lyric-line" value="Siragaai serndhiruppom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Nee endhan vaazhvil maarudhal
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee endhan vaazhvil maarudhal"/>
</div>
<div class="lyrico-lyrics-wrapper">En idhayam kaetta aarudhal
<input type="checkbox" class="lyrico-select-lyric-line" value="En idhayam kaetta aarudhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Madi saayum manaiviyae
<input type="checkbox" class="lyrico-select-lyric-line" value="Madi saayum manaiviyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Poi kova-puthalviyae
<input type="checkbox" class="lyrico-select-lyric-line" value="Poi kova-puthalviyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadu vaazhvil vantha uravu nee
<input type="checkbox" class="lyrico-select-lyric-line" value="Nadu vaazhvil vantha uravu nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Nedunthooram thodarum ninaivu nee
<input type="checkbox" class="lyrico-select-lyric-line" value="Nedunthooram thodarum ninaivu nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhaiyathin thalaivi nee
<input type="checkbox" class="lyrico-select-lyric-line" value="Idhaiyathin thalaivi nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Peranbin piravi nee
<input type="checkbox" class="lyrico-select-lyric-line" value="Peranbin piravi nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">En kuraigal noorai marandhaval
<input type="checkbox" class="lyrico-select-lyric-line" value="En kuraigal noorai marandhaval"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakaaga thannai thuranthaval
<input type="checkbox" class="lyrico-select-lyric-line" value="Enakaaga thannai thuranthaval"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasalae ennai mananthaval
<input type="checkbox" class="lyrico-select-lyric-line" value="Manasalae ennai mananthaval"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbaalae uyirai alanthaval
<input type="checkbox" class="lyrico-select-lyric-line" value="Anbaalae uyirai alanthaval"/>
</div>
<div class="lyrico-lyrics-wrapper">Un varugai en varamaai aanadhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Un varugai en varamaai aanadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Nadai paadhai poovanangal paarthu
<input type="checkbox" class="lyrico-select-lyric-line" value="Nadai paadhai poovanangal paarthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nigazhkala kanavugalil pooththu
<input type="checkbox" class="lyrico-select-lyric-line" value="Nigazhkala kanavugalil pooththu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru moochchin oosaiyilae
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru moochchin oosaiyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ondraai vazhdhiruppom… ooh..oohoo
<input type="checkbox" class="lyrico-select-lyric-line" value="Ondraai vazhdhiruppom… ooh..oohoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ondraai vazhdhiruppom… 
<input type="checkbox" class="lyrico-select-lyric-line" value="Ondraai vazhdhiruppom… "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vaa ullankaigalai korthu
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaa ullankaigalai korthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai-regai mothamum serthu
<input type="checkbox" class="lyrico-select-lyric-line" value="Kai-regai mothamum serthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sila dhoora payangalil
<input type="checkbox" class="lyrico-select-lyric-line" value="Sila dhoora payangalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Siragaai serndhiruppom
<input type="checkbox" class="lyrico-select-lyric-line" value="Siragaai serndhiruppom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Siragaai serndhiruppom
<input type="checkbox" class="lyrico-select-lyric-line" value="Siragaai serndhiruppom"/>
</div>
</pre>