---
title: "vaanam adhirave song lyrics"
album: "Ramanaa"
artist: "Ilaiyaraja"
lyricist: "Pazhani Barathi - M Metha"
director: "A.R. Murugadoss"
path: "/albums/ramanaa-lyrics"
song: "Vaanam Adhirave"
image: ../../images/albumart/ramanaa.jpg
date: 2002-11-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/XvBIuP6dTBk"
type: "happy"
singers:
  - P. Unnikrishnan
  - Sadhana Sargam
  - Bhavatharini
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Padapadavena Pattasu Vedikkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padapadavena Pattasu Vedikkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirukuyil Kooduthu Gummalam Adikkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirukuyil Kooduthu Gummalam Adikkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Deepavali Deepavali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Deepavali Deepavali"/>
</div>
<div class="lyrico-lyrics-wrapper">Silu Silu Siluvenna Maththaapu Sirikkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silu Silu Siluvenna Maththaapu Sirikkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Siru Siru Kiligalin Kondaattam Porakkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru Siru Kiligalin Kondaattam Porakkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Deepavali Deepavali Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Deepavali Deepavali Hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanam Adhirave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam Adhirave"/>
</div>
<div class="lyrico-lyrics-wrapper">Baanam Vedikkalaam Rosi Rosi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baanam Vedikkalaam Rosi Rosi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhvu Mazhigave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvu Mazhigave"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadam Padikkalaam Yosi Yosi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadam Padikkalaam Yosi Yosi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanam Adhirave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam Adhirave"/>
</div>
<div class="lyrico-lyrics-wrapper">Baanam Vedikkalaam Rosi Rosi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baanam Vedikkalaam Rosi Rosi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhvu Mazhigave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvu Mazhigave"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadam Padikkalaam Yosi Yosi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadam Padikkalaam Yosi Yosi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanam Adhirave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam Adhirave"/>
</div>
<div class="lyrico-lyrics-wrapper">Baanam Vedikkalaam Rosi Rosi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baanam Vedikkalaam Rosi Rosi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhvu Mazhigave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvu Mazhigave"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadam Padikkalaam Yosi Yosi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadam Padikkalaam Yosi Yosi"/>
</div>
<div class="lyrico-lyrics-wrapper">Minminiyenna Kanmanigalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minminiyenna Kanmanigalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Olividum Olividum Minvettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Olividum Olividum Minvettu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gala Galavenna Kulingida Vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gala Galavenna Kulingida Vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaikodu Kaikodu Poonkothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaikodu Kaikodu Poonkothu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Jaanngu Jakka Chacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Jaanngu Jakka Chacha"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaka Jaanngu Jakka Chacha Hey Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaka Jaanngu Jakka Chacha Hey Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Jaanngu Jakka Chacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Jaanngu Jakka Chacha"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaka Jaanngu Jakka Chacha Hey Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaka Jaanngu Jakka Chacha Hey Hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanam Adhirave Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam Adhirave Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Baanam Vedikkalaam Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baanam Vedikkalaam Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanam Adhirave Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam Adhirave Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Baanam Vedikkalaam Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baanam Vedikkalaam Hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">FeManmele Deepangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="FeManmele Deepangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Yettrum Neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yettrum Neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasellaam Oliveesum Paarammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasellaam Oliveesum Paarammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbodu Oru Vaarthai Sonnaal Podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbodu Oru Vaarthai Sonnaal Podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagellaam Sondhangal Thaanamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagellaam Sondhangal Thaanamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">FeDeivangal Ellaam Kaikorthukolla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="FeDeivangal Ellaam Kaikorthukolla"/>
</div>
<div class="lyrico-lyrics-wrapper">Bedham Yen Nam Nenjile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bedham Yen Nam Nenjile"/>
</div>
<div class="lyrico-lyrics-wrapper">Nambikkai Kondaal Munnettram Undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nambikkai Kondaal Munnettram Undu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sorvenna Un Vaazhivile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sorvenna Un Vaazhivile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thavazhndhu Thavazhndhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thavazhndhu Thavazhndhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadhiyil Kulikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadhiyil Kulikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Malaiyaipola Thullu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malaiyaipola Thullu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathaadum Pattampola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathaadum Pattampola"/>
</div>
<div class="lyrico-lyrics-wrapper">Parandhu Parandhu Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parandhu Parandhu Paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Manikuyile Siragaai Viriththu Paadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manikuyile Siragaai Viriththu Paadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanam Adhirave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam Adhirave"/>
</div>
<div class="lyrico-lyrics-wrapper">Baanam Vedikkalaam Rosi Rosi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baanam Vedikkalaam Rosi Rosi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhvu Mazhigave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvu Mazhigave"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadam Padikkalaam Yosi Yosi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadam Padikkalaam Yosi Yosi"/>
</div>
<div class="lyrico-lyrics-wrapper">Minminiyenna Kanmanigalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minminiyenna Kanmanigalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Olividum Olividum Minvettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Olividum Olividum Minvettu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gala Galavenna Kulingida Vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gala Galavenna Kulingida Vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaikodu Kaikodu Poonkothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaikodu Kaikodu Poonkothu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Jaanngu Jakka Chacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Jaanngu Jakka Chacha"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaka Jaanngu Jakka Chacha Hey Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaka Jaanngu Jakka Chacha Hey Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Jaanngu Jakka Chacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Jaanngu Jakka Chacha"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaka Jaanngu Jakka Chacha Hey Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaka Jaanngu Jakka Chacha Hey Hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanam Adhirave Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam Adhirave Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Baanam Vedikkalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baanam Vedikkalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">FePalakaaram Paniyaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="FePalakaaram Paniyaaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Suttu Podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suttu Podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattassu Vedipaale Paatikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattassu Vedipaale Paatikku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">FeSuttalum Vedikkatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="FeSuttalum Vedikkatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanam Pole
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam Pole"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesaathe Yettikku Potikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesaathe Yettikku Potikku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ambuli Maama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ambuli Maama"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhai Sollalaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhai Sollalaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Anjali Kaikothu Vaa Maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anjali Kaikothu Vaa Maa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pinnal Illaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pinnal Illaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Jadai Podum Paapa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jadai Podum Paapa"/>
</div>
<div class="lyrico-lyrics-wrapper">Engalai Vambezhukkalaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engalai Vambezhukkalaama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Surutti Surutti Odu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Surutti Surutti Odu"/>
</div>
<div class="lyrico-lyrics-wrapper">Jolikkum Saami Yaara Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jolikkum Saami Yaara Paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Kashmiril Pattu Pudavai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kashmiril Pattu Pudavai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattum Maami Yaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattum Maami Yaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadakaiyil Thadukkum Thadukkum Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadakaiyil Thadukkum Thadukkum Paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanam Adhirave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam Adhirave"/>
</div>
<div class="lyrico-lyrics-wrapper">Baanam Vedikkalaam Rosi Rosi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baanam Vedikkalaam Rosi Rosi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhvu Mazhigave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvu Mazhigave"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadam Padikkalaam Yosi Yosi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadam Padikkalaam Yosi Yosi"/>
</div>
<div class="lyrico-lyrics-wrapper">Minminiyenna Kanmanigalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minminiyenna Kanmanigalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Olividum Olividum Minvettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Olividum Olividum Minvettu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gala Galavenna Kulingida Vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gala Galavenna Kulingida Vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaikodu Kaikodu Poonkothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaikodu Kaikodu Poonkothu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Jaanngu Jakka Chacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Jaanngu Jakka Chacha"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaka Jaanngu Jakka Chacha Hey Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaka Jaanngu Jakka Chacha Hey Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Jaanngu Jakka Chacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Jaanngu Jakka Chacha"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaka Jaanngu Jakka Chacha Hey Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaka Jaanngu Jakka Chacha Hey Hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanam Adhirave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam Adhirave"/>
</div>
<div class="lyrico-lyrics-wrapper">Baanam Vedikkalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baanam Vedikkalaam"/>
</div>
</pre>
