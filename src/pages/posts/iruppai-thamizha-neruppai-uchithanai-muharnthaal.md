---
title: "iruppai thamizha neruppai song lyrics"
album: "Uchithanai Muharnthaal"
artist: "D. Imman"
lyricist: "Kasi Anandan"
director: "Pugazhendhi Thangaraj"
path: "/albums/uchithanai-muharnthaal-lyrics"
song: "Iruppai Thamizha Neruppai"
image: ../../images/albumart/uchithanai-muharnthaal.jpg
date: 2011-12-16
lang: tamil
youtubeLink: "https://www.youtube.com/embed/D-BIbqMiH4E"
type: "sad"
singers:
  - D. Imman
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">irupai tamizha nerupai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irupai tamizha nerupai"/>
</div>
<div class="lyrico-lyrics-wrapper">irupai tamizha nerupai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irupai tamizha nerupai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">irupai tamizha nerupai nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irupai tamizha nerupai nee"/>
</div>
<div class="lyrico-lyrics-wrapper">ilivai kidaka serupa nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ilivai kidaka serupa nee"/>
</div>
<div class="lyrico-lyrics-wrapper">ongi ongi puyal adikirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ongi ongi puyal adikirathe"/>
</div>
<div class="lyrico-lyrics-wrapper">oru theepam anaiyum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru theepam anaiyum "/>
</div>
<div class="lyrico-lyrics-wrapper">munne thudikirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munne thudikirathe"/>
</div>
<div class="lyrico-lyrics-wrapper">kudithu kudithu udal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kudithu kudithu udal "/>
</div>
<div class="lyrico-lyrics-wrapper">sithaikirathe thinam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sithaikirathe thinam"/>
</div>
<div class="lyrico-lyrics-wrapper">pinangal pinangalai puthaikirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pinangal pinangalai puthaikirathe"/>
</div>
<div class="lyrico-lyrics-wrapper">en pillaiyai mannil puthaipargal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en pillaiyai mannil puthaipargal "/>
</div>
<div class="lyrico-lyrics-wrapper">avar thai mannai avargal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avar thai mannai avargal "/>
</div>
<div class="lyrico-lyrics-wrapper">enge puthaipargal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enge puthaipargal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">erimalai thaniyuma thaneeril
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="erimalai thaniyuma thaneeril"/>
</div>
<div class="lyrico-lyrics-wrapper">kadalanena karaiyuma kaneeril
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadalanena karaiyuma kaneeril"/>
</div>
<div class="lyrico-lyrics-wrapper">mulangidum sange mulangayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mulangidum sange mulangayo"/>
</div>
<div class="lyrico-lyrics-wrapper">vilangal puthaika piranthaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vilangal puthaika piranthaye"/>
</div>
<div class="lyrico-lyrics-wrapper">adimaiyai vaalum nilam ondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adimaiyai vaalum nilam ondru"/>
</div>
<div class="lyrico-lyrics-wrapper">vidiyalai kaanum kalam indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidiyalai kaanum kalam indru"/>
</div>
<div class="lyrico-lyrics-wrapper">vetta velio veedu aanathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetta velio veedu aanathu"/>
</div>
<div class="lyrico-lyrics-wrapper">patiniyo unavu aanathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="patiniyo unavu aanathu"/>
</div>
<div class="lyrico-lyrics-wrapper">tholathu nee veelvodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tholathu nee veelvodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">irupai tamizha nerupai nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irupai tamizha nerupai nee"/>
</div>
<div class="lyrico-lyrics-wrapper">ilivai kidaka serupa nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ilivai kidaka serupa nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">minnalin thodarchiye idiyaven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="minnalin thodarchiye idiyaven"/>
</div>
<div class="lyrico-lyrics-wrapper">innalin thodarchiye vidivagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="innalin thodarchiye vidivagum"/>
</div>
<div class="lyrico-lyrics-wrapper">konthalithu aram vedikatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konthalithu aram vedikatho"/>
</div>
<div class="lyrico-lyrics-wrapper">kodiyavar moochai mudikatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kodiyavar moochai mudikatho"/>
</div>
<div class="lyrico-lyrics-wrapper">aayiram malaigalai tholaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aayiram malaigalai tholaku"/>
</div>
<div class="lyrico-lyrics-wrapper">adimaiku viduthalai naalaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adimaiku viduthalai naalaku"/>
</div>
<div class="lyrico-lyrics-wrapper">maanthar uyiro nilai atrathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maanthar uyiro nilai atrathu"/>
</div>
<div class="lyrico-lyrics-wrapper">nanum thanum thanada 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanum thanum thanada "/>
</div>
<div class="lyrico-lyrics-wrapper">nigar atrathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nigar atrathu"/>
</div>
<div class="lyrico-lyrics-wrapper">poradu nee verodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poradu nee verodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">irupai tamizha nerupai nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irupai tamizha nerupai nee"/>
</div>
<div class="lyrico-lyrics-wrapper">ilivai kidaka serupa nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ilivai kidaka serupa nee"/>
</div>
<div class="lyrico-lyrics-wrapper">ongi ongi puyal adikirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ongi ongi puyal adikirathe"/>
</div>
<div class="lyrico-lyrics-wrapper">oru theepam anaiyum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru theepam anaiyum "/>
</div>
<div class="lyrico-lyrics-wrapper">munne thudikirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munne thudikirathe"/>
</div>
<div class="lyrico-lyrics-wrapper">kudithu kudithu udal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kudithu kudithu udal "/>
</div>
<div class="lyrico-lyrics-wrapper">sithaikirathe thinam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sithaikirathe thinam"/>
</div>
<div class="lyrico-lyrics-wrapper">pinangal pinangalai puthaikirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pinangal pinangalai puthaikirathe"/>
</div>
<div class="lyrico-lyrics-wrapper">en pillaiyai mannil puthaipargal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en pillaiyai mannil puthaipargal "/>
</div>
<div class="lyrico-lyrics-wrapper">avar thai mannai avargal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avar thai mannai avargal "/>
</div>
<div class="lyrico-lyrics-wrapper">enge puthaipargal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enge puthaipargal"/>
</div>
</pre>
