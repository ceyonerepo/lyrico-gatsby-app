---
title: "oh my god song lyrics"
album: "Sangathamizhan"
artist: "	Vivek - Mervin"
lyricist: "Viveka"
director: "Vijay Chandar"
path: "/albums/sangathamizhan-lyrics"
song: "Oh My God"
image: ../../images/albumart/sangathamizhan.jpg
date: 2019-11-15
lang: tamil
youtubeLink: "https://www.youtube.com/embed/-DwdsVGU_sg"
type: "happy"
singers:
  - Nakash Aziz
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Oh My God
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh My God"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh My God
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh My God"/>
</div>
<div class="lyrico-lyrics-wrapper">Pori Parakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pori Parakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Total Tamilnaad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Total Tamilnaad"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh My God
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh My God"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh My God
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh My God"/>
</div>
<div class="lyrico-lyrics-wrapper">Terinjiruchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Terinjiruchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini Namma Pakkam God
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Namma Pakkam God"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuri Paarthu Adichaakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuri Paarthu Adichaakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Stone-il
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Stone-il"/>
</div>
<div class="lyrico-lyrics-wrapper">Double Maanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Double Maanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Solrendaa One Line-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solrendaa One Line-u"/>
</div>
<div class="lyrico-lyrics-wrapper">No Pain-u No Gain-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="No Pain-u No Gain-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Heyy Talk Work More
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy Talk Work More"/>
</div>
<div class="lyrico-lyrics-wrapper">Zero To Hero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zero To Hero"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Heartu Sollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Heartu Sollum"/>
</div>
<div class="lyrico-lyrics-wrapper">Route Ah Thaedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Route Ah Thaedu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Zero To Hero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zero To Hero"/>
</div>
<div class="lyrico-lyrics-wrapper">Heyy Ooru Pecha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy Ooru Pecha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kandukkadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandukkadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Zero To Hero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zero To Hero"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Moodi Veiyu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Moodi Veiyu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rendu Kaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendu Kaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Zero To Hero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zero To Hero"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Heyy Talk Work More
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy Talk Work More"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Zero To Hero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zero To Hero"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Heartu Sollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Heartu Sollum"/>
</div>
<div class="lyrico-lyrics-wrapper">Route Ah Thaedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Route Ah Thaedu"/>
</div>
<div class="lyrico-lyrics-wrapper">Zero To Hero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zero To Hero"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Heyy Ooru Pecha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy Ooru Pecha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kandukkadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandukkadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Zero To Hero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zero To Hero"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Moodi Veiyu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Moodi Veiyu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rendu Kaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendu Kaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Zero To Hero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zero To Hero"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Less Tension More Worku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Less Tension More Worku"/>
</div>
<div class="lyrico-lyrics-wrapper">Aavaatha Nee Jerkku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aavaatha Nee Jerkku"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithuthaanda Vetrikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithuthaanda Vetrikku"/>
</div>
<div class="lyrico-lyrics-wrapper">First-u Rule-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="First-u Rule-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadamaikku Seiyaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadamaikku Seiyaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadamaiya Nee Seiyu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadamaiya Nee Seiyu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullaara Undaagum Super Feel-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullaara Undaagum Super Feel-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Heyy Theernthu Pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy Theernthu Pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhkaiyoda Data Card-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkaiyoda Data Card-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu Theerum Munnae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu Theerum Munnae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaru Neeyinnu Kaatu Dude-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaru Neeyinnu Kaatu Dude-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pala Saadhanaikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala Saadhanaikku"/>
</div>
<div class="lyrico-lyrics-wrapper">Reason Ingae Attitude-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Reason Ingae Attitude-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Keep Calm And Carry On Thalaivaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Keep Calm And Carry On Thalaivaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Heyy Talk Work More
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy Talk Work More"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Zero To Hero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zero To Hero"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Heartu Sollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Heartu Sollum"/>
</div>
<div class="lyrico-lyrics-wrapper">Route Ah Thaedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Route Ah Thaedu"/>
</div>
<div class="lyrico-lyrics-wrapper">Zero To Hero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zero To Hero"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Heyy Ooru Pecha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy Ooru Pecha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kandukkadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandukkadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Zero To Hero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zero To Hero"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Moodi Veiyu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Moodi Veiyu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rendu Kaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendu Kaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Zero To Hero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zero To Hero"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh My God
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh My God"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh My God
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh My God"/>
</div>
<div class="lyrico-lyrics-wrapper">Pori Parakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pori Parakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Total Tamilnaad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Total Tamilnaad"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh My God
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh My God"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh My God
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh My God"/>
</div>
<div class="lyrico-lyrics-wrapper">Terinjiruchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Terinjiruchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini Namma Pakkam God
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Namma Pakkam God"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuri Paarthu Adichaakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuri Paarthu Adichaakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Stone-il
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Stone-il"/>
</div>
<div class="lyrico-lyrics-wrapper">Double Maanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Double Maanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Solrendaa One Line-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solrendaa One Line-u"/>
</div>
<div class="lyrico-lyrics-wrapper">No Pain-u No Gain-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="No Pain-u No Gain-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Heyy Talk Work More
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy Talk Work More"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Zero To Hero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zero To Hero"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Heartu Sollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Heartu Sollum"/>
</div>
<div class="lyrico-lyrics-wrapper">Route Ah Thaedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Route Ah Thaedu"/>
</div>
<div class="lyrico-lyrics-wrapper">Zero To Hero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zero To Hero"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Heyy Ooru Pecha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy Ooru Pecha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kandukkadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandukkadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Zero To Hero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zero To Hero"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Moodi Veiyu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Moodi Veiyu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rendu Kaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendu Kaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Zero To Hero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zero To Hero"/>
</div>
</pre>
