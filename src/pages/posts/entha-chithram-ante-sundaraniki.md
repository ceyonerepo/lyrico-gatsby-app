---
title: "entha chithram song lyrics"
album: "Ante Sundaraniki"
artist: "Vivek Sagar"
lyricist: "Ramajogayya Sastry"
director: "Vivek Athreya"
path: "/albums/ante-sundaraniki-lyrics"
song: "Entha Chithram"
image: ../../images/albumart/ante-sundaraniki.jpg
date: 2022-06-10
lang: telugu
youtubeLink: "https://www.youtube.com/embed/kjJEx3SGSDU"
type: "love"
singers:
  -	Anurag Kulkarni
  - Keerthana Vaidyanathan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Entha chitram entha chitram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entha chitram entha chitram"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennesi gnapakaalo oopiradedhelaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennesi gnapakaalo oopiradedhelaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmm entha maatram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm entha maatram"/>
</div>
<div class="lyrico-lyrics-wrapper">Oohalo leni utsavaalalo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oohalo leni utsavaalalo "/>
</div>
<div class="lyrico-lyrics-wrapper">munigi thelaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munigi thelaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Entha chitram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entha chitram"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennesi gnapakaalo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennesi gnapakaalo "/>
</div>
<div class="lyrico-lyrics-wrapper">oopiradedhelaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oopiradedhelaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmm entha maatram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm entha maatram"/>
</div>
<div class="lyrico-lyrics-wrapper">Oohalo leni utsavaalalo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oohalo leni utsavaalalo "/>
</div>
<div class="lyrico-lyrics-wrapper">munigi thelaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munigi thelaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ollalaa viruchukuntu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ollalaa viruchukuntu"/>
</div>
<div class="lyrico-lyrics-wrapper">Roju thellavaarutondi enchetho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Roju thellavaarutondi enchetho"/>
</div>
<div class="lyrico-lyrics-wrapper">Assalem jarugutundo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Assalem jarugutundo"/>
</div>
<div class="lyrico-lyrics-wrapper">Emo emito
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emo emito"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Emani nannadigaa emayindani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emani nannadigaa emayindani"/>
</div>
<div class="lyrico-lyrics-wrapper">Aamani naa manasantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aamani naa manasantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Poolu challe rammani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poolu challe rammani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ekkado chinni aasha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ekkado chinni aasha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ekkado chinni aasha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ekkado chinni aasha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kulaasa uyalesaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulaasa uyalesaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnalo nannu teesaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnalo nannu teesaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kothaga rangulesaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kothaga rangulesaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hmm addaalake kannu kuttelaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm addaalake kannu kuttelaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Andala anandamoutunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andala anandamoutunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Emayyindemite halaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emayyindemite halaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa vennele vennu thattelaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa vennele vennu thattelaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lokanike kaanthinistunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lokanike kaanthinistunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Inthalo inni vinthala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inthalo inni vinthala"/>
</div>
<div class="lyrico-lyrics-wrapper">Phalana peru lenide
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phalana peru lenide"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullaasame naa jathainade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullaasame naa jathainade"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee gaalilo jolalilo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee gaalilo jolalilo"/>
</div>
<div class="lyrico-lyrics-wrapper">Gathala diary kaduluthondi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gathala diary kaduluthondi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennallakennallako malli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennallakennallako malli"/>
</div>
<div class="lyrico-lyrics-wrapper">Marintha naku nenu dorikane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marintha naku nenu dorikane"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalame maaya chesene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalame maaya chesene"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalame maaya chesene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalame maaya chesene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ee konnaallalaa ninnalokelli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee konnaallalaa ninnalokelli"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanaati nannu nenu kalisaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanaati nannu nenu kalisaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Ori maa chinni nayane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ori maa chinni nayane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oo sukhisukhana jeevitham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo sukhisukhana jeevitham"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorantha kerinthaladene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorantha kerinthaladene"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee konchame inkonchemai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee konchame inkonchemai"/>
</div>
<div class="lyrico-lyrics-wrapper">Etelli agutundo emo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Etelli agutundo emo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Emani nannadigaa emayindani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emani nannadigaa emayindani"/>
</div>
<div class="lyrico-lyrics-wrapper">Emani nannadigaa emayindani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emani nannadigaa emayindani"/>
</div>
<div class="lyrico-lyrics-wrapper">Aamani naa manasantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aamani naa manasantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Poolu challe rammani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poolu challe rammani"/>
</div>
</pre>
