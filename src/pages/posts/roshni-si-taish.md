---
title: "roshni si song lyrics"
album: "Taish"
artist: "Prashant Pillai"
lyricist: "Hussain Haidry"
director: "Bejoy Nambiar"
path: "/albums/taish-lyrics"
song: "Roshni Si"
image: ../../images/albumart/taish.jpg
date: 2020-10-29
lang: hindi
youtubeLink: "https://www.youtube.com/embed/jyYkdRICb_k"
type: "sad"
singers:
  - Ashwin Gopakumar
  - Preeti Pillai
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Haule haule haulee..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haule haule haulee.."/>
</div>
<div class="lyrico-lyrics-wrapper">Haule haule haulee..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haule haule haulee.."/>
</div>
<div class="lyrico-lyrics-wrapper">Haule haule haulee..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haule haule haulee.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haule se chala raaton ke pare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haule se chala raaton ke pare"/>
</div>
<div class="lyrico-lyrics-wrapper">Aankhon se juda khwabon ke pare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aankhon se juda khwabon ke pare"/>
</div>
<div class="lyrico-lyrics-wrapper">Parwazein meri jo laayi dhuaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parwazein meri jo laayi dhuaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Naya naya nasha chadhaa..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naya naya nasha chadhaa.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Roshni si hai phir
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Roshni si hai phir"/>
</div>
<div class="lyrico-lyrics-wrapper">Nayi subah hai phirr..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nayi subah hai phirr.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Darr-badar main phir raha tha jahan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Darr-badar main phir raha tha jahan"/>
</div>
<div class="lyrico-lyrics-wrapper">Na tha ghar na tha koyi apna wahan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na tha ghar na tha koyi apna wahan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sab takhti lagaye kinare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sab takhti lagaye kinare"/>
</div>
<div class="lyrico-lyrics-wrapper">Banaye khudi ke sahaare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Banaye khudi ke sahaare"/>
</div>
<div class="lyrico-lyrics-wrapper">Milengi kahin to panahein..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Milengi kahin to panahein.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Roshni si hai phir
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Roshni si hai phir"/>
</div>
<div class="lyrico-lyrics-wrapper">Nayi subah hai phir
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nayi subah hai phir"/>
</div>
</pre>
