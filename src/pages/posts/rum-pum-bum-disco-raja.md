---
title: "rum pum bum song lyrics"
album: "Disco Raja"
artist: "S. Thaman"
lyricist: "Sirivennela Seetharama Sastry"
director: "Vi Anand"
path: "/albums/disco-raja-lyrics"
song: "Rum Pum Bum"
image: ../../images/albumart/disco-raja.jpg
date: 2020-01-24
lang: telugu
youtubeLink: "https://www.youtube.com/embed/qv11uzyYMFU"
type: "happy"
singers:
  - Ravi Teja
  - Bappi Lahiri
  - Sri Krishna
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kaalam aagali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam aagali"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa kali vegam chusi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa kali vegam chusi"/>
</div>
<div class="lyrico-lyrics-wrapper">Lokam sagali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lokam sagali"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa veli saige telisi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa veli saige telisi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalam aagali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam aagali"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa kali vegam chusi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa kali vegam chusi"/>
</div>
<div class="lyrico-lyrics-wrapper">Lokam sagali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lokam sagali"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa veli saige telisi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa veli saige telisi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kondale voogi povali na joruki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondale voogi povali na joruki"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhikule paari povali na horuki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhikule paari povali na horuki"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye rangeli rangala ponge tharangalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye rangeli rangala ponge tharangalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakali aa ningiki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakali aa ningiki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rababa ra rabariba ye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rababa ra rabariba ye"/>
</div>
<div class="lyrico-lyrics-wrapper">Rababa ye rabariba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rababa ye rabariba"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye ra ye ra rabariba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye ra ye ra rabariba"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rum pum bum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rum pum bum"/>
</div>
<div class="lyrico-lyrics-wrapper">Rum pum bum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rum pum bum"/>
</div>
<div class="lyrico-lyrics-wrapper">Rum pum bum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rum pum bum"/>
</div>
<div class="lyrico-lyrics-wrapper">Rum pum bum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rum pum bum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalam aagali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam aagali"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa kali vegam chusi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa kali vegam chusi"/>
</div>
<div class="lyrico-lyrics-wrapper">Lokam sagali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lokam sagali"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa veli saige telisi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa veli saige telisi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adugu padina prathi chotta kadatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adugu padina prathi chotta kadatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenele kota
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenele kota"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadu koodadhani yevadosthado
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadu koodadhani yevadosthado"/>
</div>
<div class="lyrico-lyrics-wrapper">Raanira chustha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raanira chustha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Peta petaku rajuntada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peta petaku rajuntada"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudharadhura beta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudharadhura beta"/>
</div>
<div class="lyrico-lyrics-wrapper">Sharanu sharanu maa raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sharanu sharanu maa raja"/>
</div>
<div class="lyrico-lyrics-wrapper">Ante abhayam andhistha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ante abhayam andhistha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Janam andharu janam techchukuni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Janam andharu janam techchukuni"/>
</div>
<div class="lyrico-lyrics-wrapper">Jadustharu mana peru vinte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jadustharu mana peru vinte"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilantodu ee prapanchana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilantodu ee prapanchana"/>
</div>
<div class="lyrico-lyrics-wrapper">Mari okke okkadanthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mari okke okkadanthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Antu bejaru puttali jejelu kottali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antu bejaru puttali jejelu kottali"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee roju mana dhaatiki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee roju mana dhaatiki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rababa ra rabariba ye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rababa ra rabariba ye"/>
</div>
<div class="lyrico-lyrics-wrapper">Rababa ye rabariba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rababa ye rabariba"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye ra ye ra rabariba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye ra ye ra rabariba"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rum pum bum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rum pum bum"/>
</div>
<div class="lyrico-lyrics-wrapper">Rum pum bum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rum pum bum"/>
</div>
<div class="lyrico-lyrics-wrapper">Rum pum bum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rum pum bum"/>
</div>
<div class="lyrico-lyrics-wrapper">Rum pum bum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rum pum bum"/>
</div>
</pre>
