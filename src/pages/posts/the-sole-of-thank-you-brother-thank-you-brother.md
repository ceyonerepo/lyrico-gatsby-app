---
title: "the sole of song lyrics"
album: "Thank You Brother"
artist: "Guna Balasubramanian"
lyricist: "Lakshmi Priyanka"
director: "Ramesh Raparthi"
path: "/albums/thank-you-brother-lyrics"
song: "The Sole of Thank you Brother - Pranayam andhame"
image: ../../images/albumart/thank-you-brother.jpg
date: 2021-05-07
lang: telugu
youtubeLink: "https://www.youtube.com/embed/v8xiWmIn8A8"
type: "affection"
singers:
  - Shakthi Shree Gopalan
  - Guna Balasubramanian
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Pranayamandhame Payanamandhame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pranayamandhame Payanamandhame"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu Kalisina Kshanamu Andhame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu Kalisina Kshanamu Andhame"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalisi Nadichinaa Dhooramandhame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalisi Nadichinaa Dhooramandhame"/>
</div>
<div class="lyrico-lyrics-wrapper">Gathamu Panchinaa Guruthulandhame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gathamu Panchinaa Guruthulandhame"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu Andhame Prema Andhame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu Andhame Prema Andhame"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottha Oopirai Chere Bandhame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottha Oopirai Chere Bandhame"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu Levane Ooha Bhaarame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu Levane Ooha Bhaarame"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasulo Sadhaa Nuvvu Padhilame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasulo Sadhaa Nuvvu Padhilame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kantaneeru Chendhakundaa Kshanamantu Ledhule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kantaneeru Chendhakundaa Kshanamantu Ledhule"/>
</div>
<div class="lyrico-lyrics-wrapper">Saayamantu Pondhakunda Manishantu Ledule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saayamantu Pondhakunda Manishantu Ledule"/>
</div>
<div class="lyrico-lyrics-wrapper">Kantaneeru Chendhakundaa Kshanamantu Ledhule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kantaneeru Chendhakundaa Kshanamantu Ledhule"/>
</div>
<div class="lyrico-lyrics-wrapper">Saayamantu Pondhakunda Manishantu Ledu Bhoomipai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saayamantu Pondhakunda Manishantu Ledu Bhoomipai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naathone Nenilaa Saagaane Jantagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naathone Nenilaa Saagaane Jantagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Theliyani Oo Sankelane Manasukesine
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theliyani Oo Sankelane Manasukesine"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Bandhaalanu Dhooramgaa Unchaanule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Bandhaalanu Dhooramgaa Unchaanule"/>
</div>
<div class="lyrico-lyrics-wrapper">Porapaate Alavaatai Ontarinai Migilaane Nedu Nenilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porapaate Alavaatai Ontarinai Migilaane Nedu Nenilaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kantaneeru Chendhakundaa Kshanamantu Ledhule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kantaneeru Chendhakundaa Kshanamantu Ledhule"/>
</div>
<div class="lyrico-lyrics-wrapper">Saayamantu Pondhakunda Manishantu Ledu Bhoomipai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saayamantu Pondhakunda Manishantu Ledu Bhoomipai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanipenchi Preminchi Kanupaapai Enchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanipenchi Preminchi Kanupaapai Enchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana Lokam Thana Swaram Thana Praanam Nene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana Lokam Thana Swaram Thana Praanam Nene"/>
</div>
<div class="lyrico-lyrics-wrapper">Ani Thelise Ee Nimisham Marujanmegaa Naaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ani Thelise Ee Nimisham Marujanmegaa Naaku"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasunna Manishalle Marosaari Puttaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasunna Manishalle Marosaari Puttaanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kantaneeru Chendhakundaa Kshanamantu Ledhule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kantaneeru Chendhakundaa Kshanamantu Ledhule"/>
</div>
<div class="lyrico-lyrics-wrapper">Saayamantu Pondhakunda Manishantu Ledule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saayamantu Pondhakunda Manishantu Ledule"/>
</div>
<div class="lyrico-lyrics-wrapper">Kantaneeru Chendhakundaa Kshanamantu Ledhule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kantaneeru Chendhakundaa Kshanamantu Ledhule"/>
</div>
<div class="lyrico-lyrics-wrapper">Saayamantu Pondhakunda Manishantu Ledu Bhoomipai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saayamantu Pondhakunda Manishantu Ledu Bhoomipai"/>
</div>
</pre>
