---
title: "idi nijamena song lyrics"
album: "Jodi"
artist: "Phani Kalyan"
lyricist: "Anantha Sriram"
director: "Vishwanath Arigella"
path: "/albums/jodi-lyrics"
song: "Idi Nijamena"
image: ../../images/albumart/jodi.jpg
date: 2019-09-06
lang: telugu
youtubeLink: "https://www.youtube.com/embed/YtPhZwS7VGE"
type: "love"
singers:
  - Yazin Nizar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Idi Nijamena Nijame Antara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idi Nijamena Nijame Antara"/>
</div>
<div class="lyrico-lyrics-wrapper">Asalokaraina Thanala Untara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asalokaraina Thanala Untara"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalavamu Kadara 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalavamu Kadara "/>
</div>
<div class="lyrico-lyrics-wrapper">Thanane Kallonaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanane Kallonaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanabaduthunte Vinthe Vinthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanabaduthunte Vinthe Vinthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Dorakadu Kadara 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dorakadu Kadara "/>
</div>
<div class="lyrico-lyrics-wrapper">Thanu Ae Kavithallona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanu Ae Kavithallona"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhuravuthunte Anthe Anthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhuravuthunte Anthe Anthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallolam Kallolam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallolam Kallolam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oohallo Kallolam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oohallo Kallolam"/>
</div>
<div class="lyrico-lyrics-wrapper">Undundi Gundello
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undundi Gundello"/>
</div>
<div class="lyrico-lyrics-wrapper">Oogindhoy Bhoogolam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oogindhoy Bhoogolam "/>
</div>
<div class="lyrico-lyrics-wrapper">Bhoogolam Golam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhoogolam Golam"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhoogolam Golam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhoogolam Golam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ee Lokam Kaadaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Lokam Kaadaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Emo Thanu Mari Itu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emo Thanu Mari Itu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ae Lokam Nuncho Vachhindaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Lokam Nuncho Vachhindaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Lokam Motham 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Lokam Motham "/>
</div>
<div class="lyrico-lyrics-wrapper">Thanai Ippudika Nanu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanai Ippudika Nanu "/>
</div>
<div class="lyrico-lyrics-wrapper">Maa Lokam Lagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa Lokam Lagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Marchindaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marchindaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Emito Aa Theerutho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emito Aa Theerutho"/>
</div>
<div class="lyrico-lyrics-wrapper">Na Praanaanne Laagindaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na Praanaanne Laagindaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenane Aalochane 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenane Aalochane "/>
</div>
<div class="lyrico-lyrics-wrapper">Naalo Mayam Chesindaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalo Mayam Chesindaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhorakam Ayomayam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhorakam Ayomayam "/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Vypukae Thosindhira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Vypukae Thosindhira"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallolam Kallolam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallolam Kallolam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oohallo Kallolam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oohallo Kallolam"/>
</div>
<div class="lyrico-lyrics-wrapper">Undundi Gundello
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undundi Gundello"/>
</div>
<div class="lyrico-lyrics-wrapper">Oogindhoy Bhoogolam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oogindhoy Bhoogolam"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhoogolam Golam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhoogolam Golam"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhoogolam Golam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhoogolam Golam"/>
</div>
<div class="lyrico-lyrics-wrapper">Idi Nijamena Nijame Antara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idi Nijamena Nijame Antara"/>
</div>
<div class="lyrico-lyrics-wrapper">Asalokaraina Thanala Untara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asalokaraina Thanala Untara"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalavamu Kadara 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalavamu Kadara "/>
</div>
<div class="lyrico-lyrics-wrapper">Thanane Kallonaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanane Kallonaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanabaduthunte Vinthe Vinthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanabaduthunte Vinthe Vinthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Dorakadu Kadara 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dorakadu Kadara "/>
</div>
<div class="lyrico-lyrics-wrapper">Thanu Ae Kavithallona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanu Ae Kavithallona"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhuravuthunte Anthe Anthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhuravuthunte Anthe Anthe"/>
</div>
</pre>
