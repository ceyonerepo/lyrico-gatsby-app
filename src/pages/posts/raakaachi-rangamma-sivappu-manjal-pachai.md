---
title: "raakaachi rangamma song lyrics"
album: "Sivappu Manjal Pachai"
artist: "Siddhu Kumar"
lyricist: "Mohan Rajan"
director: "Sasi"
path: "/albums/sivappu-manjal-pachai-lyrics"
song: "Raakaachi Rangamma"
image: ../../images/albumart/sivappu-manjal-pachai.jpg
date: 2019-09-06
lang: tamil
youtubeLink: "https://www.youtube.com/embed/WKokhmgmXlM"
type: "love"
singers:
  - Anitha Karthikeyan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Raakaachi Rangamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raakaachi Rangamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Raasathi Mangamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raasathi Mangamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Darling-ah Torture-ah Paathaalama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Darling-ah Torture-ah Paathaalama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aakaatti Angamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aakaatti Angamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Agmark-u Thangamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Agmark-u Thangamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathaadi Noolaa Andhu Ponaalamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathaadi Noolaa Andhu Ponaalamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaar Saala Singam Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaar Saala Singam Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Challenge Panni Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Challenge Panni Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthaanae Tholla Thanthaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthaanae Tholla Thanthaanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ae Kolaaru Illatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Kolaaru Illatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Boologam Kaanatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boologam Kaanatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Mappilla Ketkkuranae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mappilla Ketkkuranae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paavamae Paakkama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paavamae Paakkama"/>
</div>
<div class="lyrico-lyrics-wrapper">Vechithaan Seiyiraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vechithaan Seiyiraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vedha Vedhama Photo Paarthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedha Vedhama Photo Paarthum"/>
</div>
<div class="lyrico-lyrics-wrapper">Venaanu Solluraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venaanu Solluraan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ranveerum Ranbirum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ranveerum Ranbirum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mix Aaki Ketkkuraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mix Aaki Ketkkuraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivanoda Akka Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivanoda Akka Enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Deepika Padukone-ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Deepika Padukone-ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yedhedho Tholla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhedho Tholla"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanthu Ponaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanthu Ponaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan Mela Kovam Valla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Mela Kovam Valla"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu Yennu Ivala Kelvi Ketaakaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu Yennu Ivala Kelvi Ketaakaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Gamukkama Sirippa Ulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Gamukkama Sirippa Ulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Puriyatha Onnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puriyatha Onnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Avalaiyum Ivanaiyum Poottudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avalaiyum Ivanaiyum Poottudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ariyaama Nenju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ariyaama Nenju"/>
</div>
<div class="lyrico-lyrics-wrapper">Adithadi Adithadi Pannudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adithadi Adithadi Pannudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivanoda Ullam Vella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivanoda Ullam Vella"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhunthutaa Padicha Pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhunthutaa Padicha Pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Adada Eliyum Puliyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adada Eliyum Puliyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasathaan Maathikichae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasathaan Maathikichae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raakaachi Rangamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raakaachi Rangamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Raasathi Mangamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raasathi Mangamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Darling-ah Torture-ah Paathaalama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Darling-ah Torture-ah Paathaalama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aakaatti Angamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aakaatti Angamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Agmark-u Thangamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Agmark-u Thangamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Silvanda Pola Suththi Vanthaalamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silvanda Pola Suththi Vanthaalamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Daa Potta Talent-ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daa Potta Talent-ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Di Pottaan Decent-ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Di Pottaan Decent-ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Raavellaam Whatsapp Galatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raavellaam Whatsapp Galatta"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava Thee Vecha Silent-ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Thee Vecha Silent-ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Pathithaan Vaiyen Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathithaan Vaiyen Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Naall Ellaam Pochae No End-ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naall Ellaam Pochae No End-ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pollatha Pullandaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pollatha Pullandaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nikkaama Vellaandaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikkaama Vellaandaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Raga Ragama Ragala Senji
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raga Ragama Ragala Senji"/>
</div>
<div class="lyrico-lyrics-wrapper">Iva Manasa Kalavaandaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iva Manasa Kalavaandaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Killadi Sullaandaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Killadi Sullaandaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjellaam Utkaandhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjellaam Utkaandhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Urava Thedi Vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Urava Thedi Vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu Urava Kondaanthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu Urava Kondaanthaan"/>
</div>
</pre>
