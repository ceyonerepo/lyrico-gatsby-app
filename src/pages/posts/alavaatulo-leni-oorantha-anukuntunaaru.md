---
title: "alavaatulo leni song lyrics"
album: "Oorantha Anukuntunnaru"
artist: "	KM Radha Krishnan"
lyricist: "Srihari Mangalampalli"
director: "Balaji Sanala"
path: "/albums/oorantha-anukuntunnaru-lyrics"
song: "Alavaatulo Leni"
image: ../../images/albumart/oorantha-anukuntunaaru.jpg
date: 2019-10-05
lang: telugu
youtubeLink: "https://www.youtube.com/embed/58c5IAVhJL8"
type: "love"
singers:
  - SP Balasubrahmanyam
  - Sunitha Upadrashta	
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">alavaatulo leni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alavaatulo leni"/>
</div>
<div class="lyrico-lyrics-wrapper">kotthalokaalu chooputhunnaavila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kotthalokaalu chooputhunnaavila"/>
</div>
<div class="lyrico-lyrics-wrapper">alavaatulo leni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alavaatulo leni"/>
</div>
<div class="lyrico-lyrics-wrapper">kotthalokaalu chooputhunnaavila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kotthalokaalu chooputhunnaavila"/>
</div>
<div class="lyrico-lyrics-wrapper">mannilaa choodanee vinthagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mannilaa choodanee vinthagaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ye maaya chesaavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ye maaya chesaavo"/>
</div>
<div class="lyrico-lyrics-wrapper">moyalenantha baarame premalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moyalenantha baarame premalo"/>
</div>
<div class="lyrico-lyrics-wrapper">ye maaya chesaavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ye maaya chesaavo"/>
</div>
<div class="lyrico-lyrics-wrapper">moyalenantha baarame premalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moyalenantha baarame premalo"/>
</div>
<div class="lyrico-lyrics-wrapper">yenni thullinthalo inthalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yenni thullinthalo inthalo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">manimaranchaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manimaranchaane"/>
</div>
<div class="lyrico-lyrics-wrapper">nee thalapullo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee thalapullo"/>
</div>
<div class="lyrico-lyrics-wrapper">yemarupaate leneledhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yemarupaate leneledhe"/>
</div>
<div class="lyrico-lyrics-wrapper">yugame kshanamae jaarene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yugame kshanamae jaarene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aamani kosam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aamani kosam"/>
</div>
<div class="lyrico-lyrics-wrapper">vechina praayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vechina praayam"/>
</div>
<div class="lyrico-lyrics-wrapper">chigurula thodigi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chigurula thodigi"/>
</div>
<div class="lyrico-lyrics-wrapper">nee odilone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee odilone"/>
</div>
<div class="lyrico-lyrics-wrapper">sagamai odhige
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sagamai odhige"/>
</div>
<div class="lyrico-lyrics-wrapper">samayamlo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="samayamlo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">alavaatulo leni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alavaatulo leni"/>
</div>
<div class="lyrico-lyrics-wrapper">kotthalokaalu chooputhunnaavila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kotthalokaalu chooputhunnaavila"/>
</div>
<div class="lyrico-lyrics-wrapper">yenni thullinthalo inthalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yenni thullinthalo inthalo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">parugulu these
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="parugulu these"/>
</div>
<div class="lyrico-lyrics-wrapper">paruvapu jarilo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paruvapu jarilo"/>
</div>
<div class="lyrico-lyrics-wrapper">akkuna jere
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="akkuna jere"/>
</div>
<div class="lyrico-lyrics-wrapper">priyalayalore
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="priyalayalore"/>
</div>
<div class="lyrico-lyrics-wrapper">karige vayase
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karige vayase"/>
</div>
<div class="lyrico-lyrics-wrapper">thaapamlo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaapamlo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">chalikowgililo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chalikowgililo"/>
</div>
<div class="lyrico-lyrics-wrapper">kalige dhigule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalige dhigule"/>
</div>
<div class="lyrico-lyrics-wrapper">theerindhilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theerindhilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">nee laalanalone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee laalanalone"/>
</div>
<div class="lyrico-lyrics-wrapper">nanune marichaa neelone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanune marichaa neelone"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">alavaatulo leni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alavaatulo leni"/>
</div>
<div class="lyrico-lyrics-wrapper">kotthalokaalu chooputhunnaavila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kotthalokaalu chooputhunnaavila"/>
</div>
<div class="lyrico-lyrics-wrapper">mannilaa choodanee vinthagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mannilaa choodanee vinthagaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ye maaya chesaavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ye maaya chesaavo"/>
</div>
<div class="lyrico-lyrics-wrapper">moyalenantha baarame premalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moyalenantha baarame premalo"/>
</div>
<div class="lyrico-lyrics-wrapper">yenni thullinthalo inthalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yenni thullinthalo inthalo"/>
</div>
</pre>
