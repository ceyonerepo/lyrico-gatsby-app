---
title: "kaasethan kadavul song lyrics"
album: "Chithiram Pesuthadi 2"
artist: "Sajan Madhav"
lyricist: "Saajan Madhav"
director: "Rajan Madhav"
path: "/albums/chithiram-pesuthadi-2-lyrics"
song: "Kaasethan Kadavul"
image: ../../images/albumart/chithiram-pesuthadi-2.jpg
date: 2019-02-15
lang: tamil
youtubeLink: "https://www.youtube.com/embed/rUmortu32VM"
type: "happy"
singers:
  - Saajan Madhav
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Engae Theduvan Panatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engae Theduvan Panatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Panatha Panatha Panatha Panatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panatha Panatha Panatha Panatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Engae Theduvan Panatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engae Theduvan Panatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Panatha Panatha Panatha Panatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panatha Panatha Panatha Panatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hoo Wow Hoo Oo Ho Hoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo Wow Hoo Oo Ho Hoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo Wow Hoo Oo Ho Ho Hoooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo Wow Hoo Oo Ho Ho Hoooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo Wow Hoo Oo Ho Ho Hooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo Wow Hoo Oo Ho Ho Hooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaasae Thaan Kadavul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasae Thaan Kadavul"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha Kadavula Thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Kadavula Thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Alaiyaa Alainjaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alaiyaa Alainjaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ettaadha Dhooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettaadha Dhooram"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhai Etti Pidikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai Etti Pidikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattamaa Parandhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattamaa Parandhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Panam Paththum Seiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panam Paththum Seiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha Panathukkaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Panathukkaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Pala Pala Senjaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala Pala Senjaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manithaabimaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manithaabimaanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhai Kuzhi Onnu Thondi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai Kuzhi Onnu Thondi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aazhathil Pudhaichaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aazhathil Pudhaichaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neram Kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neram Kaalam"/>
</div>
 <div class="lyrico-lyrics-wrapper">Velapadavilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velapadavilla"/>
</div>
 <div class="lyrico-lyrics-wrapper">Vazhkaikinga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhkaikinga"/>
</div>
 <div class="lyrico-lyrics-wrapper">Vazhimurai Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhimurai Illa"/>
</div>
 <div class="lyrico-lyrics-wrapper">Yerum Botha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yerum Botha"/>
</div>
<div class="lyrico-lyrics-wrapper">Yerangavae Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yerangavae Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhum Inga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhum Inga"/>
</div>
<div class="lyrico-lyrics-wrapper">Pulapada Villa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pulapada Villa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ponnukkum Ponnukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponnukkum Ponnukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Adithadi Inga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adithadi Inga"/>
</div>
 <div class="lyrico-lyrics-wrapper">Natppukkum Karppukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natppukkum Karppukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhippae Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhippae Illa"/>
</div>
 <div class="lyrico-lyrics-wrapper">Mansuhanukku Ingae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mansuhanukku Ingae"/>
</div>
 <div class="lyrico-lyrics-wrapper">Edamae Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edamae Illa"/>
</div>
 <div class="lyrico-lyrics-wrapper">Asurana Kolla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asurana Kolla"/>
</div>
 <div class="lyrico-lyrics-wrapper">Aandavan Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aandavan Illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hoo Wow Hoo Oo Ho Hoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo Wow Hoo Oo Ho Hoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo Wow Hoo Oo Ho Ho Hoooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo Wow Hoo Oo Ho Ho Hoooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo Wow Hoo Oo Ho Ho Hooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo Wow Hoo Oo Ho Ho Hooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aasaiyin Botha Uchiyil Yera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasaiyin Botha Uchiyil Yera"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhuvilum Maadhilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhuvilum Maadhilum"/>
</div>
<div class="lyrico-lyrics-wrapper">Muzhugi Theera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muzhugi Theera"/>
</div>
<div class="lyrico-lyrics-wrapper">Dharmam Nyaayam Engae Theda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dharmam Nyaayam Engae Theda"/>
</div>
<div class="lyrico-lyrics-wrapper">Panam Irunthaa Nee Raja Raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panam Irunthaa Nee Raja Raja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaasathaanda Nesikkiraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasathaanda Nesikkiraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhai Serkkathaan Yosikkiraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai Serkkathaan Yosikkiraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nachu Kaatha Swaasikkiraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nachu Kaatha Swaasikkiraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Saava Thedi Poraan Poraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saava Thedi Poraan Poraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Poraan Poraan Poraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraan Poraan Poraan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaasae Thaan Kadavul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasae Thaan Kadavul"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohh Hoo Ohhhh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohh Hoo Ohhhh"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaasae Thaan Kadavul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasae Thaan Kadavul"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohh Hoo Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohh Hoo Oh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaasae Thaan Kadavul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasae Thaan Kadavul"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha Kadavula Thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Kadavula Thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Alaiyaa Alainjaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alaiyaa Alainjaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ettaadha Dhooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettaadha Dhooram"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhai Etti Pidikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai Etti Pidikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattamaa Parandhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattamaa Parandhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Panam Paththum Seiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panam Paththum Seiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha Panathukkaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Panathukkaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Pala Pala Senjaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala Pala Senjaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manithaabimaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manithaabimaanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhai Kuzhi Onnu Thondi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai Kuzhi Onnu Thondi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aazhathil Pudhaichaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aazhathil Pudhaichaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaasae Thaan Kadavul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasae Thaan Kadavul"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaasae Thaan Kadavul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasae Thaan Kadavul"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaasae Thaan Kaasae Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasae Thaan Kaasae Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaasae Thaan Kadavul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasae Thaan Kadavul"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaasae Thaan Kadavul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasae Thaan Kadavul"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaasae Thaan Kaasae Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasae Thaan Kaasae Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaasae Thaan Kadavul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasae Thaan Kadavul"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaasae Thaan Kadavul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasae Thaan Kadavul"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaasae Thaan Kaasae Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasae Thaan Kaasae Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaasae Thaan Kadavul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasae Thaan Kadavul"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaasae Thaan Kadavul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasae Thaan Kadavul"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaasae Thaan Kaasae Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasae Thaan Kaasae Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaasae Thaan Kadavul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasae Thaan Kadavul"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaasae Thaan Kadavul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasae Thaan Kadavul"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaasae Thaan Kaasae Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasae Thaan Kaasae Thaan"/>
</div>
</pre>
