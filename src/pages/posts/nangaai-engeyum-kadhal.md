---
title: "nangaai song lyrics"
album: "Engeyum Kadhal"
artist: "Harris Jayaraj"
lyricist: "Vaali"
director: "Prabhudeva"
path: "/albums/engeyum-kadhal-lyrics"
song: "Nangaai"
image: ../../images/albumart/engeyum-kadhal.jpg
date: 2011-05-06
lang: tamil
youtubeLink: "https://www.youtube.com/embed/TA6zB49ngEM"
type: "love"
singers:
  - Richard
  - Rahul Nambiar
  - Naveen Madhav
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Valliye Chakkara Valliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valliye Chakkara Valliye"/>
</div>
<div class="lyrico-lyrics-wrapper">Malliye Santhana Malliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malliye Santhana Malliye"/>
</div>
<div class="lyrico-lyrics-wrapper">Balliye Bangana Balliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Balliye Bangana Balliye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Mama Mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Mama Mama"/>
</div>
<div class="lyrico-lyrics-wrapper">Vippaamalaamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vippaamalaamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Mama Mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Mama Mama"/>
</div>
<div class="lyrico-lyrics-wrapper">Vippak Shaamalaamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vippak Shaamalaamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Mama Mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Mama Mama"/>
</div>
<div class="lyrico-lyrics-wrapper">Vippaamalaamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vippaamalaamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Mama Mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Mama Mama"/>
</div>
<div class="lyrico-lyrics-wrapper">Vippak Shaamalaamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vippak Shaamalaamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nangaai Nilaavin Thangai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nangaai Nilaavin Thangai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mangaai Neethaane Sengaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mangaai Neethaane Sengaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Paavaai En Thozhi Aavaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paavaai En Thozhi Aavaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Poovaai Nirkaathey Theevaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovaai Nirkaathey Theevaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mandaakani Maanganani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandaakani Maanganani"/>
</div>
<div class="lyrico-lyrics-wrapper">Sindhaamani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sindhaamani"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa Vaa Amsaveni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Vaa Amsaveni"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Pournami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Pournami"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maa Rathini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa Rathini"/>
</div>
<div class="lyrico-lyrics-wrapper">Mrunaalini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mrunaalini"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee En Swapna Raani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee En Swapna Raani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nangaai Nilaavin Thangai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nangaai Nilaavin Thangai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mangaai Neethaane Sengaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mangaai Neethaane Sengaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Paavaai En Thozhi Aavaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paavaai En Thozhi Aavaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Poovaai Nirkaathey Theevaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovaai Nirkaathey Theevaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Mama Mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Mama Mama"/>
</div>
<div class="lyrico-lyrics-wrapper">Vippaamalaamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vippaamalaamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Mama Mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Mama Mama"/>
</div>
<div class="lyrico-lyrics-wrapper">Vippak Shaamalaamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vippak Shaamalaamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Mama Mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Mama Mama"/>
</div>
<div class="lyrico-lyrics-wrapper">Vippaamalaamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vippaamalaamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Mama Mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Mama Mama"/>
</div>
<div class="lyrico-lyrics-wrapper">Vippak Shaamalaamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vippak Shaamalaamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Paarthathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Paarthathum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorai Vittu Aangilam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorai Vittu Aangilam"/>
</div>
<div class="lyrico-lyrics-wrapper">Odi Ponathenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odi Ponathenna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sealight Semmozhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sealight Semmozhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Senthamizh Thaan En Mozhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senthamizh Thaan En Mozhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Endre Aanathenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endre Aanathenna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnaala Naam Jone Harry
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaala Naam Jone Harry"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaala Ippo Muththu Maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaala Ippo Muththu Maari"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaala Naam Jone Harry
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaala Naam Jone Harry"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaala Ippo Muththu Maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaala Ippo Muththu Maari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unna Na Uppu Kandamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Na Uppu Kandamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thottukka Uppu Kollaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottukka Uppu Kollaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaala Thammaa Thundaamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaala Thammaa Thundaamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Odanjen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odanjen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nangaai Nilaavin Thangai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nangaai Nilaavin Thangai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mangaai Neethaane Sengaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mangaai Neethaane Sengaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Paavaai En Thozhi Aavaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paavaai En Thozhi Aavaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Poovaai Nirkaathey Theevaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovaai Nirkaathey Theevaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Soozhan Puththiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soozhan Puththiri"/>
</div>
<div class="lyrico-lyrics-wrapper">Suttam Vizhigal Kaththiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suttam Vizhigal Kaththiri"/>
</div>
<div class="lyrico-lyrics-wrapper">Veyyil Pola Kaaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veyyil Pola Kaaya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kamban Pillaithaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kamban Pillaithaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal Ullam Vellaithaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Ullam Vellaithaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalum Venthu Saaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalum Venthu Saaya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naam Ottalaamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam Ottalaamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Engammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Aththa Thaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Aththa Thaama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naam Ottalaamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam Ottalaamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Engammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Aththa Thaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Aththa Thaama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Valliye Chakkara Valliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valliye Chakkara Valliye"/>
</div>
<div class="lyrico-lyrics-wrapper">Malliye Santhana Malliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malliye Santhana Malliye"/>
</div>
<div class="lyrico-lyrics-wrapper">Balliye Bangana Balliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Balliye Bangana Balliye"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaithaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaithaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nangaai Nilaavin Thangai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nangaai Nilaavin Thangai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mangaai Neethaane Sengaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mangaai Neethaane Sengaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Paavaai En Thozhi Aavaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paavaai En Thozhi Aavaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Poovaai Nirkaathey Theevaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovaai Nirkaathey Theevaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mandaakani Maanganani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandaakani Maanganani"/>
</div>
<div class="lyrico-lyrics-wrapper">Sindhaamani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sindhaamani"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa Vaa Amsaveni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Vaa Amsaveni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Pournami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Pournami"/>
</div>
<div class="lyrico-lyrics-wrapper">Maa Rathini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa Rathini"/>
</div>
<div class="lyrico-lyrics-wrapper">Mrunaalini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mrunaalini"/>
</div>
<div class="lyrico-lyrics-wrapper">You are My Swapna Raani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You are My Swapna Raani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Mama Mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Mama Mama"/>
</div>
<div class="lyrico-lyrics-wrapper">Vippaamalaamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vippaamalaamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Mama Mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Mama Mama"/>
</div>
<div class="lyrico-lyrics-wrapper">Vippak Shaamalaamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vippak Shaamalaamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Mama Mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Mama Mama"/>
</div>
<div class="lyrico-lyrics-wrapper">Vippaamalaamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vippaamalaamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Mama Mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Mama Mama"/>
</div>
<div class="lyrico-lyrics-wrapper">Vippak Shaamalaamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vippak Shaamalaamaa"/>
</div>
</pre>
