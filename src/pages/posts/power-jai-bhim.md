---
title: "power song lyrics"
album: "Jai Bhim"
artist: "Sean Roldan"
lyricist: "Arivu"
director: "T.J. Gnanavel"
path: "/albums/jai-bhim-lyrics"
song: "Power"
image: ../../images/albumart/jai-bhim.jpg
date: 2021-11-02
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ovHIeVFnSro"
type: "mass"
singers:
  - Arivu
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kailyila Edu Powera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kailyila Edu Powera"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuninju Edu Powera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuninju Edu Powera"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhi Edhum Ini Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi Edhum Ini Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adha Thavara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adha Thavara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meetu Edu Powera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meetu Edu Powera"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaetu Edu Powera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaetu Edu Powera"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhi Adhu Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi Adhu Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nammoda Thala Nimira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nammoda Thala Nimira"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tathagata Tathagata Tathagata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tathagata Tathagata Tathagata"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirutham Kondu Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirutham Kondu Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Tathagata Tathagata Tathagata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tathagata Tathagata Tathagata"/>
</div>
<div class="lyrico-lyrics-wrapper">Arathai Yendhi Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arathai Yendhi Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Samarasam Illai Samar Idhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samarasam Illai Samar Idhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sari Nigaraga Thuyaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sari Nigaraga Thuyaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini Edhu Thuninthu Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Edhu Thuninthu Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Tathagata Tathagata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tathagata Tathagata"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Olirittum Ini Eliyavar Vizhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Olirittum Ini Eliyavar Vizhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudivodu Thodanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudivodu Thodanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhi Edhu Muzhangu Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi Edhu Muzhangu Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Tathagata Tathagata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tathagata Tathagata"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poraadithaan Vaanganum Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraadithaan Vaanganum Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Moochayum Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moochayum Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Seraama Thaan Naam Iruntha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seraama Thaan Naam Iruntha"/>
</div>
<div class="lyrico-lyrics-wrapper">Maatram Undaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maatram Undaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kailyila Edu Powera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kailyila Edu Powera"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuninju Edu Powera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuninju Edu Powera"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhi Edhum Ini Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi Edhum Ini Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adha Thavaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adha Thavaraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meetu Edu Powera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meetu Edu Powera"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaetu Edu Powera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaetu Edu Powera"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhi Adhu Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi Adhu Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nammoda Thala Nimira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nammoda Thala Nimira"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eliyavar Kural Enavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eliyavar Kural Enavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Muzhangidu Idi Enavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muzhangidu Idi Enavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Suyanalam Ozhinthidavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suyanalam Ozhinthidavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Samam Ena Uyarnthidavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samam Ena Uyarnthidavae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaarukum Podthi Thaan Needhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarukum Podthi Thaan Needhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorukku Unarthidu Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorukku Unarthidu Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Perukku Vaazhkaya Ottama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perukku Vaazhkaya Ottama"/>
</div>
<div class="lyrico-lyrics-wrapper">Nerukku Ner Ezhu Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nerukku Ner Ezhu Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaetta Thaan Kidaikumna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaetta Thaan Kidaikumna"/>
</div>
<div class="lyrico-lyrics-wrapper">Adha Kedaikura Varaikkum Kaettukada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adha Kedaikura Varaikkum Kaettukada"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathaa Nee Parakkalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathaa Nee Parakkalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Adaikkira Unna Siraiya Udachukada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adaikkira Unna Siraiya Udachukada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Samarasam Illai Samar Idhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samarasam Illai Samar Idhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sari Nigaraga Thuyaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sari Nigaraga Thuyaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini Edhu Thuninthu Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Edhu Thuninthu Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Olirittum Ini Eliyavar Vizhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Olirittum Ini Eliyavar Vizhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudivodu Thodanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudivodu Thodanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhi Edhu Muzhangu Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi Edhu Muzhangu Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Utharavellam Podathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Utharavellam Podathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Yaaru Enakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Yaaru Enakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Nikkuravan Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikkuravan Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppothum Unmaiku Pakathile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppothum Unmaiku Pakathile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aadugal Illa Pakkatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadugal Illa Pakkatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Veenaga Mirattithan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veenaga Mirattithan"/>
</div>
<div class="lyrico-lyrics-wrapper">Aayudham Illa Anbala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayudham Illa Anbala"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadakkum Puratchi Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadakkum Puratchi Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kailyila Edu Powera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kailyila Edu Powera"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuninju Edu Powera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuninju Edu Powera"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhi Edhum Ini Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi Edhum Ini Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adha Thavaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adha Thavaraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meetu Edu Powera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meetu Edu Powera"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaetu Edu Powera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaetu Edu Powera"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhi Adhu Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi Adhu Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nammoda Thala Nimiraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nammoda Thala Nimiraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tathagata Tathagata Tathagata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tathagata Tathagata Tathagata"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirutham Kondu Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirutham Kondu Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Tathagata Tathagata Tathagata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tathagata Tathagata Tathagata"/>
</div>
<div class="lyrico-lyrics-wrapper">Arathai Yendhi Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arathai Yendhi Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kailyila Edu Poweraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kailyila Edu Poweraa"/>
</div>
</pre>
