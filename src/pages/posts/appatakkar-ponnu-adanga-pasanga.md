---
title: "appatakkar ponnu song lyrics"
album: "Adanga Pasanga"
artist: "Maneesh"
lyricist: "Jai Murusu"
director: "R. Selvanathan"
path: "/albums/adanga-pasanga-lyrics"
song: "Appatakkar Ponnu"
image: ../../images/albumart/adanga-pasanga.jpg
date: 2018-10-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ZeBi_j7FQdo"
type: "gaana"
singers:
  - Gana Bala
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">appatakkar ponnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="appatakkar ponnu"/>
</div>
<div class="lyrico-lyrics-wrapper">tubakorru illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tubakorru illa"/>
</div>
<div class="lyrico-lyrics-wrapper">love la than jeyichu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="love la than jeyichu "/>
</div>
<div class="lyrico-lyrics-wrapper">puttu aaduren da mella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puttu aaduren da mella"/>
</div>
<div class="lyrico-lyrics-wrapper">ekachekka poi athu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ekachekka poi athu"/>
</div>
<div class="lyrico-lyrics-wrapper">solli kathal sei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="solli kathal sei"/>
</div>
<div class="lyrico-lyrics-wrapper">ok aayitaka ne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ok aayitaka ne"/>
</div>
<div class="lyrico-lyrics-wrapper">urugi odu nei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="urugi odu nei"/>
</div>
<div class="lyrico-lyrics-wrapper">kozhi kunja pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kozhi kunja pola"/>
</div>
<div class="lyrico-lyrics-wrapper">kunthikunu irunthavana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kunthikunu irunthavana"/>
</div>
<div class="lyrico-lyrics-wrapper">koththum kaluga pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koththum kaluga pola"/>
</div>
<div class="lyrico-lyrics-wrapper">maathi putta antha pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maathi putta antha pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">tube light ah pola 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tube light ah pola "/>
</div>
<div class="lyrico-lyrics-wrapper">vitu vitu erinchavana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vitu vitu erinchavana"/>
</div>
<div class="lyrico-lyrics-wrapper">haaljin pulb ah pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="haaljin pulb ah pol"/>
</div>
<div class="lyrico-lyrics-wrapper">amakalama aada vitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="amakalama aada vitta"/>
</div>
<div class="lyrico-lyrics-wrapper">kathal kathal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathal kathal "/>
</div>
<div class="lyrico-lyrics-wrapper">kasakum venbu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kasakum venbu da"/>
</div>
<div class="lyrico-lyrics-wrapper">kasakki kuducha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kasakki kuducha "/>
</div>
<div class="lyrico-lyrics-wrapper">inikum karumbu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inikum karumbu da"/>
</div>
<div class="lyrico-lyrics-wrapper">kathal kathal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathal kathal "/>
</div>
<div class="lyrico-lyrics-wrapper">kasakum venbu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kasakum venbu da"/>
</div>
<div class="lyrico-lyrics-wrapper">kasakki kuducha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kasakki kuducha "/>
</div>
<div class="lyrico-lyrics-wrapper">inikum karumbu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inikum karumbu da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kathal exam il 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathal exam il "/>
</div>
<div class="lyrico-lyrics-wrapper">pass mark vanganum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pass mark vanganum"/>
</div>
<div class="lyrico-lyrics-wrapper">illana plat form il
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="illana plat form il"/>
</div>
<div class="lyrico-lyrics-wrapper">nee thoonganum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee thoonganum"/>
</div>
<div class="lyrico-lyrics-wrapper">kathal kadalil la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathal kadalil la"/>
</div>
<div class="lyrico-lyrics-wrapper">fish aaha ne neenthanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="fish aaha ne neenthanum"/>
</div>
<div class="lyrico-lyrics-wrapper">illana karuvada 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="illana karuvada "/>
</div>
<div class="lyrico-lyrics-wrapper">nee kayanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee kayanum"/>
</div>
<div class="lyrico-lyrics-wrapper">close up punnagaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="close up punnagaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">neyum than close
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neyum than close"/>
</div>
<div class="lyrico-lyrics-wrapper">aagi pogathada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aagi pogathada"/>
</div>
<div class="lyrico-lyrics-wrapper">fair & lovely face cutla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="fair & lovely face cutla"/>
</div>
<div class="lyrico-lyrics-wrapper">neyum nose cut aagathada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neyum nose cut aagathada"/>
</div>
<div class="lyrico-lyrics-wrapper">he dummy piece ah 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="he dummy piece ah "/>
</div>
<div class="lyrico-lyrics-wrapper">develop panni dayana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="develop panni dayana"/>
</div>
<div class="lyrico-lyrics-wrapper">va pakatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="va pakatha"/>
</div>
<div class="lyrico-lyrics-wrapper">mammy daddy pakkum ponna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mammy daddy pakkum ponna"/>
</div>
<div class="lyrico-lyrics-wrapper">venamunu sollatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="venamunu sollatha"/>
</div>
<div class="lyrico-lyrics-wrapper">luck vantha love varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="luck vantha love varum"/>
</div>
<div class="lyrico-lyrics-wrapper">athukulla nee odatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athukulla nee odatha"/>
</div>
<div class="lyrico-lyrics-wrapper">asalta ne love panni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="asalta ne love panni"/>
</div>
<div class="lyrico-lyrics-wrapper">alpaisula pogatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alpaisula pogatha"/>
</div>
<div class="lyrico-lyrics-wrapper">alpaisula pogatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alpaisula pogatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">intha dance pothuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha dance pothuma"/>
</div>
<div class="lyrico-lyrics-wrapper">innum konjam venuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="innum konjam venuma"/>
</div>
<div class="lyrico-lyrics-wrapper">intha dance pothuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha dance pothuma"/>
</div>
<div class="lyrico-lyrics-wrapper">innum konjam venuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="innum konjam venuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">karka kasadara karpavai katrapin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karka kasadara karpavai katrapin"/>
</div>
<div class="lyrico-lyrics-wrapper">nirka atharku thaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nirka atharku thaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kathal vanthale naanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathal vanthale naanga"/>
</div>
<div class="lyrico-lyrics-wrapper">ellam naai kuttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellam naai kuttu"/>
</div>
<div class="lyrico-lyrics-wrapper">ponna paathale vaalatuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponna paathale vaalatuvom"/>
</div>
<div class="lyrico-lyrics-wrapper">black and white than coloraga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="black and white than coloraga"/>
</div>
<div class="lyrico-lyrics-wrapper">mathi than kanavula mulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mathi than kanavula mulu"/>
</div>
<div class="lyrico-lyrics-wrapper">neela padam ootuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neela padam ootuvom"/>
</div>
<div class="lyrico-lyrics-wrapper">rotu kada purota maava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rotu kada purota maava"/>
</div>
<div class="lyrico-lyrics-wrapper">unna than pesanju appala da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna than pesanju appala da"/>
</div>
<div class="lyrico-lyrics-wrapper">saathu kudi juice ah pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saathu kudi juice ah pola"/>
</div>
<div class="lyrico-lyrics-wrapper">than manasa karachu kudipalada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="than manasa karachu kudipalada"/>
</div>
<div class="lyrico-lyrics-wrapper">pessanger illama than 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pessanger illama than "/>
</div>
<div class="lyrico-lyrics-wrapper">metro train parakuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="metro train parakuma"/>
</div>
<div class="lyrico-lyrics-wrapper">parents ah than paatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="parents ah than paatha"/>
</div>
<div class="lyrico-lyrics-wrapper">intha friendship than kedaikuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha friendship than kedaikuma"/>
</div>
<div class="lyrico-lyrics-wrapper">arranged marriage la love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arranged marriage la love"/>
</div>
<div class="lyrico-lyrics-wrapper">duet kidaikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="duet kidaikum"/>
</div>
<div class="lyrico-lyrics-wrapper">adanga pasanga pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adanga pasanga pola"/>
</div>
<div class="lyrico-lyrics-wrapper">life unaku amaiyuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="life unaku amaiyuma"/>
</div>
<div class="lyrico-lyrics-wrapper">life unaku amaiyuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="life unaku amaiyuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">appatakkar ponnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="appatakkar ponnu"/>
</div>
<div class="lyrico-lyrics-wrapper">tubakorru illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tubakorru illa"/>
</div>
<div class="lyrico-lyrics-wrapper">love la than jeyichu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="love la than jeyichu "/>
</div>
<div class="lyrico-lyrics-wrapper">puttu aaduren da mella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puttu aaduren da mella"/>
</div>
<div class="lyrico-lyrics-wrapper">ekachekka poi athu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ekachekka poi athu"/>
</div>
<div class="lyrico-lyrics-wrapper">solli kathal sei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="solli kathal sei"/>
</div>
<div class="lyrico-lyrics-wrapper">ok aayitaka ne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ok aayitaka ne"/>
</div>
<div class="lyrico-lyrics-wrapper">urugi odu nei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="urugi odu nei"/>
</div>
<div class="lyrico-lyrics-wrapper">kozhi kunja pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kozhi kunja pola"/>
</div>
<div class="lyrico-lyrics-wrapper">kunthikunu irunthavana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kunthikunu irunthavana"/>
</div>
<div class="lyrico-lyrics-wrapper">koththum kaluga pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koththum kaluga pola"/>
</div>
<div class="lyrico-lyrics-wrapper">maathi putta antha pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maathi putta antha pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">tube light ah pola 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tube light ah pola "/>
</div>
<div class="lyrico-lyrics-wrapper">vitu vitu erinchavana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vitu vitu erinchavana"/>
</div>
<div class="lyrico-lyrics-wrapper">haaljin pulb ah pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="haaljin pulb ah pol"/>
</div>
<div class="lyrico-lyrics-wrapper">amakalama aada vitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="amakalama aada vitta"/>
</div>
<div class="lyrico-lyrics-wrapper">kathal kathal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathal kathal "/>
</div>
<div class="lyrico-lyrics-wrapper">kasakum venbu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kasakum venbu da"/>
</div>
<div class="lyrico-lyrics-wrapper">kasakki kuducha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kasakki kuducha "/>
</div>
<div class="lyrico-lyrics-wrapper">inikum karumbu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inikum karumbu da"/>
</div>
<div class="lyrico-lyrics-wrapper">kathal kathal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathal kathal "/>
</div>
<div class="lyrico-lyrics-wrapper">kasakum venbu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kasakum venbu da"/>
</div>
<div class="lyrico-lyrics-wrapper">kasakki kuducha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kasakki kuducha "/>
</div>
<div class="lyrico-lyrics-wrapper">inikum karumbu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inikum karumbu da"/>
</div>
</pre>
