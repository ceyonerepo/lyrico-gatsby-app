---
title: "pottum pogattume"
album: "Think Music India"
artist: "Sathyajit Ravi - Jen Martin"
lyricist: "Vishnu Edavan"
director: "Madras Logi Vignesh"
path: "/albums/pottum-pogattume-song-lyrics"
song: "Pottum Pogattume"
image: ../../images/albumart/pottum-pogattume.jpg
date: 2021-05-20
lang: tamil
youtubeLink: "https://www.youtube.com/embed/MouGF8vG8EQ"
type: "album"
singers:
  - Jen Martin
  - Sathyajit Ravi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Un kaadhal enathendrae aanaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kaadhal enathendrae aanaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Illaamal ponaalum en vaazhkai unnaladi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illaamal ponaalum en vaazhkai unnaladi"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kaadhal enathendrae aanaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kaadhal enathendrae aanaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Illaamal ponaalum en vaazhkai unnaladi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illaamal ponaalum en vaazhkai unnaladi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnaladi vizhunthalum vizhunthezhunthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaladi vizhunthalum vizhunthezhunthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">En vaazhkai unnaladi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En vaazhkai unnaladi"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaladi vizhunthalum vizhunthezhunthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaladi vizhunthalum vizhunthezhunthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">En vaazhkai unnaladi….unnaladi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En vaazhkai unnaladi….unnaladi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pottum pogattumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pottum pogattumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee marainjaalum kaadhal thunai varumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee marainjaalum kaadhal thunai varumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Saagum naal varumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saagum naal varumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu varaikkum indha vazhi thaan uyir tharumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu varaikkum indha vazhi thaan uyir tharumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pottum pogattumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pottum pogattumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee marainjaalum kaadhal thunai varumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee marainjaalum kaadhal thunai varumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Saagum naal varumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saagum naal varumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu varaikkum indha vazhi thaan uyir tharumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu varaikkum indha vazhi thaan uyir tharumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enai nookki paayum thoottakal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai nookki paayum thoottakal"/>
</div>
<div class="lyrico-lyrics-wrapper">En netri pottai kuri vaithu paayum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En netri pottai kuri vaithu paayum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sila nodigalil maranam nigazhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sila nodigalil maranam nigazhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Theriyum nee thantha kaadhal vazhiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theriyum nee thantha kaadhal vazhiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullae yeriyum…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullae yeriyum…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnaladi vizhunthalum vizhunthezhunthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaladi vizhunthalum vizhunthezhunthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">En vaazhkai unnaladi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En vaazhkai unnaladi"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaladi vizhunthalum vizhunthezhunthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaladi vizhunthalum vizhunthezhunthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">En vaazhkai unnaladi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En vaazhkai unnaladi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pottum pogattumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pottum pogattumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee marainjaalum kaadhal thunai varumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee marainjaalum kaadhal thunai varumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Saagum naal varumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saagum naal varumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu varaikkum indha vazhi thaan uyir tharumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu varaikkum indha vazhi thaan uyir tharumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pottum pogattumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pottum pogattumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee marainjaalum kaadhal thunai varumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee marainjaalum kaadhal thunai varumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Saagum naal varumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saagum naal varumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu varaikkum indha vazhi thaan uyir tharumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu varaikkum indha vazhi thaan uyir tharumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pennae unnaladi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pennae unnaladi"/>
</div>
<div class="lyrico-lyrics-wrapper">En yetrum thazhvum unnaladi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En yetrum thazhvum unnaladi"/>
</div>
<div class="lyrico-lyrics-wrapper">En vaazhvum saavum unnaladi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En vaazhvum saavum unnaladi"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kadaisi moochai nee kadanthu sendraalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kadaisi moochai nee kadanthu sendraalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adutha vaazhvil nee endhan kaadhali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adutha vaazhvil nee endhan kaadhali"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhu jenmamum unnai kaadhalippen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhu jenmamum unnai kaadhalippen"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha ennam manadhil irundhaal podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha ennam manadhil irundhaal podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir pirinthaalum naan sirippen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir pirinthaalum naan sirippen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un kaadhal…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kaadhal…"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kaadhal enai nenje aanaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kaadhal enai nenje aanaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Illaamal ponaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illaamal ponaalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pottum pogattumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pottum pogattumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee marainjaalum kaadhal thunai varumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee marainjaalum kaadhal thunai varumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Saagum naal varumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saagum naal varumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu varaikkum indha vazhi thaan uyir tharumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu varaikkum indha vazhi thaan uyir tharumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poitu pogattumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poitu pogattumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee maranthalum kaadhal thunai varumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee maranthalum kaadhal thunai varumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhum naal varumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhum naal varumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu varaikkum indha vazhi thaan uyir
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu varaikkum indha vazhi thaan uyir"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poitu pogattumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poitu pogattumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee maranthalum kaadhal thunai varumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee maranthalum kaadhal thunai varumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhum naal varumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhum naal varumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu varaikkum indha vazhi thaan uyir
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu varaikkum indha vazhi thaan uyir"/>
</div>
</pre>
