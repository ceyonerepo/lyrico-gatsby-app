---
title: "pesugiraai pesugiraai song lyrics"
album: "Simba"
artist: "Vishal Chandrasekhar"
lyricist: "Jude Chris"
director: "Arvind Sridhar"
path: "/albums/simba-lyrics"
song: "Pesugiraai Pesugiraai"
image: ../../images/albumart/simba.jpg
date: 2019-01-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/HUj_KFBc7Dk"
type: "love"
singers:
  - Roshini
  - Sinduri Vishal
  - Aarnav
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Idhudhaan Thodakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhudhaan Thodakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">Udhadugal Thaanai Sirikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udhadugal Thaanai Sirikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sulattum Kannil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sulattum Kannil"/>
</div>
<div class="lyrico-lyrics-wrapper">Neelappokkal Malarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelappokkal Malarum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vannangal Marandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vannangal Marandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vegangal Kuraindhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vegangal Kuraindhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiyukkul Adangum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyukkul Adangum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirpikkul Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirpikkul Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vannangal Marandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vannangal Marandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vegangal Kuraindhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vegangal Kuraindhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Sedhukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Sedhukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirpiyaai Naane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirpiyaai Naane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pesugiraai Pesugiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesugiraai Pesugiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Seigayil Etho Pesugiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seigayil Etho Pesugiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaigalinaal Kaatrai Pidithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaigalinaal Kaatrai Pidithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Engengo Veesugiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engengo Veesugiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pesugiraai Pesugiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesugiraai Pesugiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Seigayil Etho Pesugiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seigayil Etho Pesugiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangalinaal Vannam Sumanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangalinaal Vannam Sumanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Imaigal Moodi Yosikkiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imaigal Moodi Yosikkiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pachai Pasum Vaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pachai Pasum Vaanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Erigindra Megam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Erigindra Megam"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhai Kondadum Dhegam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai Kondadum Dhegam"/>
</div>
<div class="lyrico-lyrics-wrapper">Angum Ingum Pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Angum Ingum Pogum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Marangalin Aatam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marangalin Aatam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattam Katti Kaatum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattam Katti Kaatum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunai Nindra Paatum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunai Nindra Paatum"/>
</div>
<div class="lyrico-lyrics-wrapper">Velai Sellamal Pootum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velai Sellamal Pootum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pugai Soolntha Araiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pugai Soolntha Araiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Odugindra Tharayil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odugindra Tharayil"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadamaadum Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadamaadum Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadumaara Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadumaara Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pesugiraai Pesugiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesugiraai Pesugiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Seigayil Etho Pesugiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seigayil Etho Pesugiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaigalinaal Kaatrai Pidithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaigalinaal Kaatrai Pidithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Engengo Veesugiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engengo Veesugiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pesugiraai Pesugiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesugiraai Pesugiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Seigayil Etho Pesugiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seigayil Etho Pesugiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangalinaal Vannam Sumanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangalinaal Vannam Sumanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Imaigal Moodi Yosikkiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imaigal Moodi Yosikkiraai"/>
</div>
</pre>
