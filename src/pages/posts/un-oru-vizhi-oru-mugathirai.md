---
title: "un oru vizhi song lyrics"
album: "Oru Mugathirai"
artist: "Premkumar Sivaperuman"
lyricist: "unknown"
director: "R Senthil Nadan"
path: "/albums/kattappava-kanom-lyrics"
song: "Un Oru Vizhi"
image: ../../images/albumart/kattappava-kanom.jpg
date: 2017-03-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/E2maNXVTiac"
type: "love"
singers:
  -	Aalap Raju
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">un oru vizhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un oru vizhi"/>
</div>
<div class="lyrico-lyrics-wrapper">eera thee mootum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eera thee mootum"/>
</div>
<div class="lyrico-lyrics-wrapper">un maru vizhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un maru vizhi"/>
</div>
<div class="lyrico-lyrics-wrapper">moga poo neetum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moga poo neetum"/>
</div>
<div class="lyrico-lyrics-wrapper">en uyir thuli kootum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en uyir thuli kootum "/>
</div>
<div class="lyrico-lyrics-wrapper">kathal vinthai thanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathal vinthai thanadi"/>
</div>
<div class="lyrico-lyrics-wrapper">katta vendiyathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="katta vendiyathu"/>
</div>
<div class="lyrico-lyrics-wrapper">idhaya kadivalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhaya kadivalam"/>
</div>
<div class="lyrico-lyrics-wrapper">pin oduthu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pin oduthu "/>
</div>
<div class="lyrico-lyrics-wrapper">paruva kadikaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paruva kadikaaram"/>
</div>
<div class="lyrico-lyrics-wrapper">nee korida nan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee korida nan"/>
</div>
<div class="lyrico-lyrics-wrapper">un kaiyil pommai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kaiyil pommai"/>
</div>
<div class="lyrico-lyrics-wrapper">thaana di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaana di"/>
</div>
<div class="lyrico-lyrics-wrapper">unnai thandi yethum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai thandi yethum"/>
</div>
<div class="lyrico-lyrics-wrapper">intha kannil thoorame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha kannil thoorame"/>
</div>
<div class="lyrico-lyrics-wrapper">unthan paatham thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unthan paatham thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">enthan paathai marume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enthan paathai marume"/>
</div>
<div class="lyrico-lyrics-wrapper">sollidu oru vaarthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sollidu oru vaarthai"/>
</div>
<div class="lyrico-lyrics-wrapper">athu pothum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu pothum "/>
</div>
<div class="lyrico-lyrics-wrapper">en udan neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en udan neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">mani yaavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mani yaavum"/>
</div>
<div class="lyrico-lyrics-wrapper">sarkarai ena inikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sarkarai ena inikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">maaya vinthai thanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maaya vinthai thanadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ulle oru mugam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulle oru mugam "/>
</div>
<div class="lyrico-lyrics-wrapper">paarkiren engum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paarkiren engum"/>
</div>
<div class="lyrico-lyrics-wrapper">un kural ketkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kural ketkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">unatharugil irukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unatharugil irukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">varai siragadipene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varai siragadipene"/>
</div>
<div class="lyrico-lyrics-wrapper">meendum siru pillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meendum siru pillai"/>
</div>
<div class="lyrico-lyrics-wrapper">aagiren unthan madi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aagiren unthan madi"/>
</div>
<div class="lyrico-lyrics-wrapper">thanai thedinen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanai thedinen"/>
</div>
<div class="lyrico-lyrics-wrapper">kanavugalil kathavugalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavugalil kathavugalai"/>
</div>
<div class="lyrico-lyrics-wrapper">thiranthu vaithene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thiranthu vaithene"/>
</div>
<div class="lyrico-lyrics-wrapper">yaarum illai enthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaarum illai enthan"/>
</div>
<div class="lyrico-lyrics-wrapper">vaalvil unnai polave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalvil unnai polave"/>
</div>
<div class="lyrico-lyrics-wrapper">yaarum thevai illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaarum thevai illai"/>
</div>
<div class="lyrico-lyrics-wrapper">unthan sparisam pothume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unthan sparisam pothume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">un oru vizhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un oru vizhi"/>
</div>
<div class="lyrico-lyrics-wrapper">eera thee mootum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eera thee mootum"/>
</div>
<div class="lyrico-lyrics-wrapper">un maru vizhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un maru vizhi"/>
</div>
<div class="lyrico-lyrics-wrapper">moga poo neetum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moga poo neetum"/>
</div>
<div class="lyrico-lyrics-wrapper">en uyir thuli kootum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en uyir thuli kootum "/>
</div>
<div class="lyrico-lyrics-wrapper">kathal vinthai thanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathal vinthai thanadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">theera aasaigal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theera aasaigal "/>
</div>
<div class="lyrico-lyrics-wrapper">sumakiren theepai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sumakiren theepai"/>
</div>
<div class="lyrico-lyrics-wrapper">ena nan parkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ena nan parkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu sariya illai thavara 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu sariya illai thavara "/>
</div>
<div class="lyrico-lyrics-wrapper">thinam thavithene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thinam thavithene"/>
</div>
<div class="lyrico-lyrics-wrapper">odum nimidangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="odum nimidangal"/>
</div>
<div class="lyrico-lyrics-wrapper">nirkume nilla 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nirkume nilla "/>
</div>
<div class="lyrico-lyrics-wrapper">innodu neelume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="innodu neelume"/>
</div>
<div class="lyrico-lyrics-wrapper">unthan ethiril intha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unthan ethiril intha"/>
</div>
<div class="lyrico-lyrics-wrapper">tharunam nilaithida keten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tharunam nilaithida keten"/>
</div>
<div class="lyrico-lyrics-wrapper">unnai thangi uyiril 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai thangi uyiril "/>
</div>
<div class="lyrico-lyrics-wrapper">yenthi vaalven naanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yenthi vaalven naanadi"/>
</div>
<div class="lyrico-lyrics-wrapper">ennai unnai pirikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennai unnai pirikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">ethaiyum alipen parudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethaiyum alipen parudi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">un oru vizhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un oru vizhi"/>
</div>
<div class="lyrico-lyrics-wrapper">eera thee mootum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eera thee mootum"/>
</div>
<div class="lyrico-lyrics-wrapper">un maru vizhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un maru vizhi"/>
</div>
<div class="lyrico-lyrics-wrapper">moga poo neetum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moga poo neetum"/>
</div>
<div class="lyrico-lyrics-wrapper">en uyir thuli kootum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en uyir thuli kootum "/>
</div>
<div class="lyrico-lyrics-wrapper">kathal vinthai thanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathal vinthai thanadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">katta vendiyathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="katta vendiyathu"/>
</div>
<div class="lyrico-lyrics-wrapper">idhaya kadivalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhaya kadivalam"/>
</div>
<div class="lyrico-lyrics-wrapper">pin oduthu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pin oduthu "/>
</div>
<div class="lyrico-lyrics-wrapper">paruva kadikaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paruva kadikaaram"/>
</div>
<div class="lyrico-lyrics-wrapper">nee korida nan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee korida nan"/>
</div>
<div class="lyrico-lyrics-wrapper">un kaiyil pommai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kaiyil pommai"/>
</div>
<div class="lyrico-lyrics-wrapper">thaana di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaana di"/>
</div>
</pre>
