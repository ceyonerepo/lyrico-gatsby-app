---
title: "kalasala kalasala song lyrics"
album: "Osthe"
artist: "Sai Thaman"
lyricist: "Vaali"
director: "S. Dharani"
path: "/albums/osthe-lyrics"
song: "Kalasala Kalasala"
image: ../../images/albumart/osthe.jpg
date: 2011-12-08
lang: tamil
youtubeLink: "https://www.youtube.com/embed/S56FUyB7aAU"
type: "happy"
singers:
  - L.R. Eswari
  - T. Rajendar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kalasala kalasala 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalasala kalasala "/>
</div>
<div class="lyrico-lyrics-wrapper">kalasala kalasala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalasala kalasala"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalaasala kalasala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalaasala kalasala"/>
</div>
<div class="lyrico-lyrics-wrapper">kalaasala kalasala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalaasala kalasala"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallaasa kalasala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallaasa kalasala"/>
</div>
<div class="lyrico-lyrics-wrapper">kalaasala kalasala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalaasala kalasala"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalaasala kalasala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalaasala kalasala"/>
</div>
<div class="lyrico-lyrics-wrapper">kalaasala kalasala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalaasala kalasala"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallaasa kalasala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallaasa kalasala"/>
</div>
<div class="lyrico-lyrics-wrapper">kalaasala kalasala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalaasala kalasala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vadakae kettu paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadakae kettu paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna pathi solluvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna pathi solluvaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Bombay halwa polae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bombay halwa polae"/>
</div>
<div class="lyrico-lyrics-wrapper">En perathaan melluvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En perathaan melluvaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalaasala kalasala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalaasala kalasala"/>
</div>
<div class="lyrico-lyrics-wrapper">kalaasala kalasala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalaasala kalasala"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallaasa kalasala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallaasa kalasala"/>
</div>
<div class="lyrico-lyrics-wrapper">kalaasala kalasala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalaasala kalasala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vadakae kettu paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadakae kettu paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna pathi solluvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna pathi solluvaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Bombay halwa polae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bombay halwa polae"/>
</div>
<div class="lyrico-lyrics-wrapper">En perathaan melluvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En perathaan melluvaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Evanum yeraalama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evanum yeraalama"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodambaakam busunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodambaakam busunu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivadhaan rajanagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivadhaan rajanagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Seeriduvaa hissunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seeriduvaa hissunu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mallika nee kadicha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mallika nee kadicha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nellika pol inipaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nellika pol inipaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Panjana nee viricha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panjana nee viricha"/>
</div>
<div class="lyrico-lyrics-wrapper">Patuthaan padichirupaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Patuthaan padichirupaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjinal konja konja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjinal konja konja"/>
</div>
<div class="lyrico-lyrics-wrapper">Konji thaen vadichirupa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konji thaen vadichirupa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pudichaa vachikaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudichaa vachikaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasula thechikaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasula thechikaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Vedicha vellarikkaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedicha vellarikkaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vendaadha aalriukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vendaadha aalriukka"/>
</div>
<div class="lyrico-lyrics-wrapper">My dear darling unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="My dear darling unna"/>
</div>
<div class="lyrico-lyrics-wrapper">Mallika koopidura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mallika koopidura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalaasala kalasala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalaasala kalasala"/>
</div>
<div class="lyrico-lyrics-wrapper">kalaasala kalasala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalaasala kalasala"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallaasa kalasala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallaasa kalasala"/>
</div>
<div class="lyrico-lyrics-wrapper">kalaasala kalasala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalaasala kalasala"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalaasala kalasala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalaasala kalasala"/>
</div>
<div class="lyrico-lyrics-wrapper">kalaasala kalasala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalaasala kalasala"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallaasa kalasala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallaasa kalasala"/>
</div>
<div class="lyrico-lyrics-wrapper">kalaasala kalasala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalaasala kalasala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pattu chittu orasu ottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattu chittu orasu ottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna thottu irukki kattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna thottu irukki kattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Irandum onnaaga oppukonda enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irandum onnaaga oppukonda enna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varudhu moodu vidumaa soodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varudhu moodu vidumaa soodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Therinjum theriyaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Therinjum theriyaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Thapputhanda panna panna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thapputhanda panna panna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee kadi kadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kadi kadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaakkaa kadi lesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaakkaa kadi lesa"/>
</div>
<div class="lyrico-lyrics-wrapper">En kannam rendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kannam rendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallirukum seesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallirukum seesa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey urumum villanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey urumum villanadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullam ullaadhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullam ullaadhadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Koduthaa vallaladi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koduthaa vallaladi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodupen vaadi vaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodupen vaadi vaadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">My dear darling unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="My dear darling unna"/>
</div>
<div class="lyrico-lyrics-wrapper">Mallika koopidura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mallika koopidura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Senthaenae enga iruka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senthaenae enga iruka"/>
</div>
<div class="lyrico-lyrics-wrapper">Osthi dhaan vandhirukaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Osthi dhaan vandhirukaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Maamae osthi maamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maamae osthi maamae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa vaa kitta vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa vaa kitta vaa vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye raa raa ikkada raa raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye raa raa ikkada raa raa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey paadu maamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey paadu maamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey podu malli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey podu malli"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey paadu maamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey paadu maamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey podu malli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey podu malli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Machan party mallika beauty
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machan party mallika beauty"/>
</div>
<div class="lyrico-lyrics-wrapper">Eda kaati nerupa mootti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eda kaati nerupa mootti"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinam kollaama kollurayae enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam kollaama kollurayae enna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnaku yetha elamai pootha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaku yetha elamai pootha"/>
</div>
<div class="lyrico-lyrics-wrapper">Oruthan vandhaachu otikittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oruthan vandhaachu otikittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nillu nillu nillu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nillu nillu nillu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ne otha singam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne otha singam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sokka thangam osthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sokka thangam osthi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thee thee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thee thee"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar unnodathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar unnodathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Poda koodum kusthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poda koodum kusthi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kulicha kuthaalandhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulicha kuthaalandhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Adicha mathalandhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adicha mathalandhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Inimel ennaalumdhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inimel ennaalumdhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Iruttil kummaalandhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruttil kummaalandhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">My dear darling unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="My dear darling unna"/>
</div>
<div class="lyrico-lyrics-wrapper">Mallika koopidura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mallika koopidura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raa raa ikkada raa raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raa raa ikkada raa raa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mallika my darling
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mallika my darling"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaamaa kalaaikalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaamaa kalaaikalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Methayil vithayalli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Methayil vithayalli"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaaliyaa jamaaikalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaaliyaa jamaaikalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennavo en manasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennavo en manasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paada maridichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paada maridichu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna naan pakayila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna naan pakayila"/>
</div>
<div class="lyrico-lyrics-wrapper">Ushnam yeridichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ushnam yeridichu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Malliga maalai ithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malliga maalai ithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Korangu kaigalila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Korangu kaigalila"/>
</div>
<div class="lyrico-lyrics-wrapper">Marbil sudikiren 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marbil sudikiren "/>
</div>
<div class="lyrico-lyrics-wrapper">vaadi velli nila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaadi velli nila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalaasala kalasala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalaasala kalasala"/>
</div>
<div class="lyrico-lyrics-wrapper">kalaasala kalasala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalaasala kalasala"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallaasa kalasala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallaasa kalasala"/>
</div>
<div class="lyrico-lyrics-wrapper">kalaasala kalasala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalaasala kalasala"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalaasala kalasala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalaasala kalasala"/>
</div>
<div class="lyrico-lyrics-wrapper">kalaasala kalasala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalaasala kalasala"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallaasa kalasala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallaasa kalasala"/>
</div>
<div class="lyrico-lyrics-wrapper">kalaasala kalasala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalaasala kalasala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Osthi maaamae yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Osthi maaamae yeah"/>
</div>
<div class="lyrico-lyrics-wrapper">Osthi maamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Osthi maamae"/>
</div>
</pre>
