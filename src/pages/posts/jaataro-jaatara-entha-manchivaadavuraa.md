---
title: "jaataro jaatara song lyrics"
album: "Entha Manchivaadavuraa"
artist: "Gopi Sundar"
lyricist: "Shreemani"
director: "Satish Vegesna"
path: "/albums/entha-manchivaadavuraa-lyrics"
song: "Jaataro Jaatara"
image: ../../images/albumart/entha-manchivaadavuraa.jpg
date: 2020-01-15
lang: telugu
youtubeLink: "https://www.youtube.com/embed/D4MeBKc31rc"
type: "happy"
singers:
  - Rahul Sipligunj
  - Sahithi Chaganti
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Jataro Jataro Nenosthe Jataro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jataro Jataro Nenosthe Jataro"/>
</div>
<div class="lyrico-lyrics-wrapper">Paruvala Motha Moganeero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paruvala Motha Moganeero"/>
</div>
<div class="lyrico-lyrics-wrapper">Putharo Putharo Bangaru Putharo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Putharo Putharo Bangaru Putharo"/>
</div>
<div class="lyrico-lyrics-wrapper">Na Meni Merupu Chusukoro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na Meni Merupu Chusukoro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Voorolla Kurrolla Vuruka Cheppunammo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Voorolla Kurrolla Vuruka Cheppunammo"/>
</div>
<div class="lyrico-lyrics-wrapper">Asalaina Pandagedho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asalaina Pandagedho"/>
</div>
<div class="lyrico-lyrics-wrapper">Nippulona Paddake Niggu Thelunammo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nippulona Paddake Niggu Thelunammo"/>
</div>
<div class="lyrico-lyrics-wrapper">Sisalaina Puttadedho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sisalaina Puttadedho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Cheruku Mukkera Pakka Na Chekkili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheruku Mukkera Pakka Na Chekkili"/>
</div>
<div class="lyrico-lyrics-wrapper">Enchakka Koriki Po Ro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enchakka Koriki Po Ro"/>
</div>
<div class="lyrico-lyrics-wrapper">Pati Kekarala Bittu Na Nadumompu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pati Kekarala Bittu Na Nadumompu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chuttu Thippuko Ro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chuttu Thippuko Ro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey! Oh Saritta Nannu Touch Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey! Oh Saritta Nannu Touch Me"/>
</div>
<div class="lyrico-lyrics-wrapper">Mallocchindhantavu Jothi Lachimi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mallocchindhantavu Jothi Lachimi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavalante Nannu Gitchhimi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavalante Nannu Gitchhimi"/>
</div>
<div class="lyrico-lyrics-wrapper">Chadhivinchu Kunnaka Sommu Lichhimi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chadhivinchu Kunnaka Sommu Lichhimi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Saritta Nannu Touch Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Saritta Nannu Touch Me"/>
</div>
<div class="lyrico-lyrics-wrapper">Mallocchindhantavu Jothi Lachchimi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mallocchindhantavu Jothi Lachchimi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavalante Nannu Gitchhimi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavalante Nannu Gitchhimi"/>
</div>
<div class="lyrico-lyrics-wrapper">Chadhivinchu Kunnaka Sommu Lichhimi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chadhivinchu Kunnaka Sommu Lichhimi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jataro Jataro Nenosthe Jataro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jataro Jataro Nenosthe Jataro"/>
</div>
<div class="lyrico-lyrics-wrapper">Paruvala Motha Moganeero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paruvala Motha Moganeero"/>
</div>
<div class="lyrico-lyrics-wrapper">Pootharo Pootharo Bangaroo Pootharo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pootharo Pootharo Bangaroo Pootharo"/>
</div>
<div class="lyrico-lyrics-wrapper">Na Meni Merupu Choosukoro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na Meni Merupu Choosukoro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kusthii Poteello Vasthadhule Endharo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kusthii Poteello Vasthadhule Endharo"/>
</div>
<div class="lyrico-lyrics-wrapper">Na Chiru Kokatho Kusthee Pattalere
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na Chiru Kokatho Kusthee Pattalere"/>
</div>
<div class="lyrico-lyrics-wrapper">Rusthum Gallane Basthi Gallu Endharo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rusthum Gallane Basthi Gallu Endharo"/>
</div>
<div class="lyrico-lyrics-wrapper">Na Chiru Muddhuke Kisthi Kattalere
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na Chiru Muddhuke Kisthi Kattalere"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pourushamunnodi Pattu Mudhara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pourushamunnodi Pattu Mudhara"/>
</div>
<div class="lyrico-lyrics-wrapper">Chitthai Podha Nee Koka Thondhara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chitthai Podha Nee Koka Thondhara"/>
</div>
<div class="lyrico-lyrics-wrapper">Pandhem Kodanti Pogaru Sundhara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pandhem Kodanti Pogaru Sundhara"/>
</div>
<div class="lyrico-lyrics-wrapper">Aithe Neeloni Padhunu Choopara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aithe Neeloni Padhunu Choopara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Saritta Nannu Touch Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Saritta Nannu Touch Me"/>
</div>
<div class="lyrico-lyrics-wrapper">Mallocchindhantavu Jothi Lachchimi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mallocchindhantavu Jothi Lachchimi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavalante Nannu Gitchhimi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavalante Nannu Gitchhimi"/>
</div>
<div class="lyrico-lyrics-wrapper">Chadhivinchu Kunnaka Sommu Lichhimi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chadhivinchu Kunnaka Sommu Lichhimi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chuttu Godare Jigelantu Vundhiga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chuttu Godare Jigelantu Vundhiga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ente Nee Jore Dhanikante Goppa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ente Nee Jore Dhanikante Goppa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dappe Kottamo Husharekki Poddhiga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dappe Kottamo Husharekki Poddhiga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ente Nee Tabala Danikante Meppa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ente Nee Tabala Danikante Meppa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Godarandhale Gattu Dhatavoye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Godarandhale Gattu Dhatavoye"/>
</div>
<div class="lyrico-lyrics-wrapper">Natho Sarasalaki Haddhu Lundavoye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natho Sarasalaki Haddhu Lundavoye"/>
</div>
<div class="lyrico-lyrics-wrapper">Dappuni Kadhura Na Lippu Thakara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dappuni Kadhura Na Lippu Thakara"/>
</div>
<div class="lyrico-lyrics-wrapper">Vurakalu Puttinche Nippu Churakara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vurakalu Puttinche Nippu Churakara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">O Saritta Nannu Touch Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Saritta Nannu Touch Me"/>
</div>
<div class="lyrico-lyrics-wrapper">Mallocchindhantavu Jothi Lachimi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mallocchindhantavu Jothi Lachimi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Yavaram Faku Lachimi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Yavaram Faku Lachimi"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethoti Settavadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethoti Settavadhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Don’t Touch Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Don’t Touch Me"/>
</div>
<div class="lyrico-lyrics-wrapper">O Saritta Nannu Touch Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Saritta Nannu Touch Me"/>
</div>
<div class="lyrico-lyrics-wrapper">Mallocchindhantavu Jothi Lachchimi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mallocchindhantavu Jothi Lachchimi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Yavaram Faku Lachchimi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Yavaram Faku Lachchimi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neethoti Settavadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethoti Settavadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Don’t Touch Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Don’t Touch Me"/>
</div>
<div class="lyrico-lyrics-wrapper">Official Music Video Of Jataro Jatara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Official Music Video Of Jataro Jatara"/>
</div>
</pre>
