---
title: "jaya jaya mahavera song lyrics"
album: "Son of India"
artist: "Ilaiyaraaja"
lyricist: "Kasarna Shyam"
director: "Diamond Ratna Babu"
path: "/albums/son-of-india-lyrics"
song: "Jaya Jaya Mahavera"
image: ../../images/albumart/son-of-india.jpg
date: 2022-02-18
lang: telugu
youtubeLink: "https://www.youtube.com/embed/ITbKSmxAV4s"
type: "happy"
singers:
  - Rahul Nambiar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Jaya Jaya Mahavera Mahadheera Dhoureyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaya Jaya Mahavera Mahadheera Dhoureyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaya Jaya Mahavera Mahadheera Dhoureyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaya Jaya Mahavera Mahadheera Dhoureyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Devasura Samara Samaya Samudhitha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Devasura Samara Samaya Samudhitha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nikhila Nirjara Nirdaaritha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikhila Nirjara Nirdaaritha "/>
</div>
<div class="lyrico-lyrics-wrapper">Niravadhika Mahatmyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niravadhika Mahatmyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhashavadhana Dhamitha Daivatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhashavadhana Dhamitha Daivatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Parishadabhyardhitha Dhasharadhi Bhaavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parishadabhyardhitha Dhasharadhi Bhaavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinakara Kula Kamala Dhivakaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinakara Kula Kamala Dhivakaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhivishadadhipathi Ranasahacharana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhivishadadhipathi Ranasahacharana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chathura Dharatha Charamaruna Vimochana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chathura Dharatha Charamaruna Vimochana"/>
</div>
<div class="lyrico-lyrics-wrapper">Kosalasutha Kumaarabhaava 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kosalasutha Kumaarabhaava "/>
</div>
<div class="lyrico-lyrics-wrapper">Kanchu Chitha Kaaranaakaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanchu Chitha Kaaranaakaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Koumaara Keli Gopaayitha Koushikaadhwara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koumaara Keli Gopaayitha Koushikaadhwara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ranaadhwara Dhurya Bhavya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ranaadhwara Dhurya Bhavya "/>
</div>
<div class="lyrico-lyrics-wrapper">Dhivyaasthra Brunda Vandhithaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhivyaasthra Brunda Vandhithaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pranatha Jana Vimatha Vimathana 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pranatha Jana Vimatha Vimathana "/>
</div>
<div class="lyrico-lyrics-wrapper">Dhurlalitha Dhorlithaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhurlalitha Dhorlithaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanuthara Vishikha Vithaadana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanuthara Vishikha Vithaadana"/>
</div>
<div class="lyrico-lyrics-wrapper">Vighatitha Visharaaru Sharaaru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vighatitha Visharaaru Sharaaru "/>
</div>
<div class="lyrico-lyrics-wrapper">Thaatakaa Thaatakeyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaatakaa Thaatakeyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jada Kirana Shakala Dharajatila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jada Kirana Shakala Dharajatila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nata Pathi Makuta Thata Natana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nata Pathi Makuta Thata Natana"/>
</div>
<div class="lyrico-lyrics-wrapper">Patu Vibhuthi Saridhathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Patu Vibhuthi Saridhathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Bahula Madhu Galana 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bahula Madhu Galana "/>
</div>
<div class="lyrico-lyrics-wrapper">Lalitha Padha Nalina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lalitha Padha Nalina"/>
</div>
<div class="lyrico-lyrics-wrapper">Raja Upa Mrudhitha Nija 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja Upa Mrudhitha Nija "/>
</div>
<div class="lyrico-lyrics-wrapper">Vrujina Jahadhupala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vrujina Jahadhupala"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanu Ruchira Parama 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanu Ruchira Parama "/>
</div>
<div class="lyrico-lyrics-wrapper">Muni Vara Yuvathi Nutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muni Vara Yuvathi Nutha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kushika Suthakathita 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kushika Suthakathita "/>
</div>
<div class="lyrico-lyrics-wrapper">Vidhitha Nava Vividha Katha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidhitha Nava Vividha Katha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mythila Nagara Sulochanaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mythila Nagara Sulochanaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Lochana Chakora Chandraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lochana Chakora Chandraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mythila Nagara Sulochanaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mythila Nagara Sulochanaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Lochana Chakora Chandraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lochana Chakora Chandraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Khanda Parashu Kodanda 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khanda Parashu Kodanda "/>
</div>
<div class="lyrico-lyrics-wrapper">Prakaanda Khandanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prakaanda Khandanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Shouda Bhuja Dhanda 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shouda Bhuja Dhanda "/>
</div>
<div class="lyrico-lyrics-wrapper">Chandakara Kirna Mandala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chandakara Kirna Mandala"/>
</div>
<div class="lyrico-lyrics-wrapper">Bodhitha Pundareeka Vana 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bodhitha Pundareeka Vana "/>
</div>
<div class="lyrico-lyrics-wrapper">Ruchi Luntaaka Lochana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ruchi Luntaaka Lochana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mochitha Janaka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mochitha Janaka "/>
</div>
<div class="lyrico-lyrics-wrapper">Hrudhaya Shankaathanka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hrudhaya Shankaathanka"/>
</div>
<div class="lyrico-lyrics-wrapper">Parihrutha Nikhila Narapathi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parihrutha Nikhila Narapathi "/>
</div>
<div class="lyrico-lyrics-wrapper">Varana Janakaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varana Janakaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhuhitha Kucha Thata 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhuhitha Kucha Thata "/>
</div>
<div class="lyrico-lyrics-wrapper">Viharana Samuchitha Karathalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viharana Samuchitha Karathalaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Shathakoti Shathaguna Kathina 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shathakoti Shathaguna Kathina "/>
</div>
<div class="lyrico-lyrics-wrapper">Parachu Dhara Munivara Kara Dhrutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parachu Dhara Munivara Kara Dhrutha"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhuravanamu Thama Nija 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhuravanamu Thama Nija "/>
</div>
<div class="lyrico-lyrics-wrapper">Dhanuraakarshana Prakaashitha Paarameshtyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhanuraakarshana Prakaashitha Paarameshtyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Krathu Hara Shikhari Kanthuka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Krathu Hara Shikhari Kanthuka "/>
</div>
<div class="lyrico-lyrics-wrapper">Vihruthyunmukha Jagadarunthudhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vihruthyunmukha Jagadarunthudhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jithaharidhanthi Dhanthadhanthuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jithaharidhanthi Dhanthadhanthuraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhasha Vadhana Dhamana Kushala Dhasha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhasha Vadhana Dhamana Kushala Dhasha"/>
</div>
<div class="lyrico-lyrics-wrapper">Shatha Bhuja Nrupathi Rudhirjhara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shatha Bhuja Nrupathi Rudhirjhara"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhara Bharitha Pruthuthara Thataaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhara Bharitha Pruthuthara Thataaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharpitha Pithruka Brugu Pathi Sugathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharpitha Pithruka Brugu Pathi Sugathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vihathi Kara Natha Parudishu Parigha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vihathi Kara Natha Parudishu Parigha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jaya Jaya Mahavera Mahadheera Dhoureyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaya Jaya Mahavera Mahadheera Dhoureyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaya Jaya Mahavera Mahadheera Dhoureyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaya Jaya Mahavera Mahadheera Dhoureyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaya Jaya Hari Govinda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaya Jaya Hari Govinda"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaya Jaya Hari Govinda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaya Jaya Hari Govinda"/>
</div>
<div class="lyrico-lyrics-wrapper">Hari Jaya Hari Govinda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hari Jaya Hari Govinda"/>
</div>
<div class="lyrico-lyrics-wrapper">Hari Jaya Hari Govinda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hari Jaya Hari Govinda"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaya Hari Jaya Govinda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaya Hari Jaya Govinda"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaya Hari Jaya Govinda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaya Hari Jaya Govinda"/>
</div>
<div class="lyrico-lyrics-wrapper">Hari Jaya Hari Govinda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hari Jaya Hari Govinda"/>
</div>
<div class="lyrico-lyrics-wrapper">Hari Jaya Hari Govinda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hari Jaya Hari Govinda"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaya Jaya Jaya Govinda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaya Jaya Jaya Govinda"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaya Jaya Jaya Govinda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaya Jaya Jaya Govinda"/>
</div>
<div class="lyrico-lyrics-wrapper">Hari Hari Govinda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hari Hari Govinda"/>
</div>
<div class="lyrico-lyrics-wrapper">Hari Hari Govinda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hari Hari Govinda"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaya Jaya Jaya Hari Hari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaya Jaya Jaya Hari Hari"/>
</div>
<div class="lyrico-lyrics-wrapper">Hari Hari Jaya Jaya Govinda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hari Hari Jaya Jaya Govinda"/>
</div>
</pre>
