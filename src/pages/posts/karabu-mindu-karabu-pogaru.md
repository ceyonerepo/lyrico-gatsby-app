---
title: "kabaru song lyrics"
album: "Pogaru"
artist: "Chandan Shetty"
lyricist: "Bhaskarabhatla"
director: "Nandha kishor"
path: "/albums/pogaru-lyrics"
song: "Kabaru Mindu Kabaru"
image: ../../images/albumart/pogaru.jpg
date: 2021-02-19
lang: telugu
youtubeLink: "https://www.youtube.com/embed/Ysf4QRrcLGM"
type: "happy"
singers:
 - Anurag Kulkarni
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Karabu mindu karabu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karabu mindu karabu"/>
</div>
<div class="lyrico-lyrics-wrapper">Merise mathaabu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Merise mathaabu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilabadi chusthava rubabu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilabadi chusthava rubabu"/>
</div>
<div class="lyrico-lyrics-wrapper">Karabu mindu karabu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karabu mindu karabu"/>
</div>
<div class="lyrico-lyrics-wrapper">Merise mathaabu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Merise mathaabu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilabadi chusthava rubabu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilabadi chusthava rubabu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Donulaina rowdylaina jadusukuntare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Donulaina rowdylaina jadusukuntare"/>
</div>
<div class="lyrico-lyrics-wrapper">Janam nenante ento telusukuntare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Janam nenante ento telusukuntare"/>
</div>
<div class="lyrico-lyrics-wrapper">Filed lona nanne kingu antaare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Filed lona nanne kingu antaare"/>
</div>
<div class="lyrico-lyrics-wrapper">Pilla naa queen nuvvaithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pilla naa queen nuvvaithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Masth untaadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Masth untaadhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaaki tho nuvvu kaburettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaki tho nuvvu kaburettu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ready ney ready
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ready ney ready"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenu maate icchanante kadhu comedy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenu maate icchanante kadhu comedy"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathi konala meedha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathi konala meedha"/>
</div>
<div class="lyrico-lyrics-wrapper">Perigaane ginne kodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perigaane ginne kodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannu kaadhante kondanaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannu kaadhante kondanaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Chestha dhooli podi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chestha dhooli podi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Deepavali ramzanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Deepavali ramzanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvve naaku oh jaanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvve naaku oh jaanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Taka taka taka taangu takara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Taka taka taka taangu takara"/>
</div>
<div class="lyrico-lyrics-wrapper">Ting taakara tikake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ting taakara tikake"/>
</div>
<div class="lyrico-lyrics-wrapper">Raanga taatakara gita gita gitake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raanga taatakara gita gita gitake"/>
</div>
<div class="lyrico-lyrics-wrapper">Atta voggesi ellipoke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atta voggesi ellipoke"/>
</div>
<div class="lyrico-lyrics-wrapper">Step yesuko naathote
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Step yesuko naathote"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
(<div class="lyrico-lyrics-wrapper">Music)
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Music)"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karaabu karaabu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karaabu karaabu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mind antha karaabu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mind antha karaabu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nippu kooda naaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nippu kooda naaku"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalakatha aaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalakatha aaku"/>
</div>
<div class="lyrico-lyrics-wrapper">Sunamike thagilistha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sunamike thagilistha"/>
</div>
<div class="lyrico-lyrics-wrapper">Current shocku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Current shocku"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheppalante by birth ye neno crack
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheppalante by birth ye neno crack"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenu start chesthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenu start chesthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhaina single take-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhaina single take-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Poraadhe ninnu unchesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraadhe ninnu unchesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Poraadhe oooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraadhe oooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nannu adigivadedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannu adigivadedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannu aapevaadedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannu aapevaadedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenu pattindhalla naadhele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenu pattindhalla naadhele"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karabu maku karabu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karabu maku karabu"/>
</div>
<div class="lyrico-lyrics-wrapper">Merise mathabu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Merise mathabu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilabadi chusthava rubabu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilabadi chusthava rubabu"/>
</div>
<div class="lyrico-lyrics-wrapper">Karabu maku karabu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karabu maku karabu"/>
</div>
<div class="lyrico-lyrics-wrapper">Merise mathabu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Merise mathabu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilabadi chusthava rubabu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilabadi chusthava rubabu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Banda boothulenno nannu thittukuntare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Banda boothulenno nannu thittukuntare"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenu chavali chavalni korukuntare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenu chavali chavalni korukuntare"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenosthe rakshasude vachadantaare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenosthe rakshasude vachadantaare"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannu chudagane bhayamthoti paaripothare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannu chudagane bhayamthoti paaripothare"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannu yenaadu adagoddu qualification
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannu yenaadu adagoddu qualification"/>
</div>
<div class="lyrico-lyrics-wrapper">Neetho kudikalu pettesey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neetho kudikalu pettesey"/>
</div>
<div class="lyrico-lyrics-wrapper">Without permission
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Without permission"/>
</div>
<div class="lyrico-lyrics-wrapper">O pilla nee navve naku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O pilla nee navve naku"/>
</div>
<div class="lyrico-lyrics-wrapper">Special occassion
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Special occassion"/>
</div>
<div class="lyrico-lyrics-wrapper">Nimpey nee mogudu shiva ani application
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nimpey nee mogudu shiva ani application"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bottle thoti vasthanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bottle thoti vasthanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Matter settle chesthanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Matter settle chesthanu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Taka taka taka taangu takara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Taka taka taka taangu takara"/>
</div>
<div class="lyrico-lyrics-wrapper">Ting taakara tikake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ting taakara tikake"/>
</div>
<div class="lyrico-lyrics-wrapper">Raanga taatakara gita gita gitake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raanga taatakara gita gita gitake"/>
</div>
<div class="lyrico-lyrics-wrapper">Atta voggesi ellipoke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atta voggesi ellipoke"/>
</div>
<div class="lyrico-lyrics-wrapper">Step yesuko naathote
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Step yesuko naathote"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mind karabu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mind karabu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mind karabu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mind karabu"/>
</div>
</pre>
