---
title: "vaaranam aayiram song lyrics"
album: "Hey Ram"
artist: "Ilaiyaraaja"
lyricist: "Narsinh Mehta - Andal - Gnanakoothan"
director: "Kamal Haasan"
path: "/albums/hey-ram-song-lyrics"
song: "Vaaranam Aayiram"
image: ../../images/albumart/hey-ram.jpg
date: 2000-02-18
lang: tamil
youtubeLink: "https://www.youtube.com/embed/KzJvxvcyU9c"
type: "melody"
singers:
  - Kanapadikal
  - Vibha Sharma
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vaishnav jan to
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaishnav jan to"/>
</div>
<div class="lyrico-lyrics-wrapper">Tene kahiye je
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tene kahiye je"/>
</div>
<div class="lyrico-lyrics-wrapper">Peed paraayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peed paraayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaanae re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaanae re"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaishnav jan to
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaishnav jan to"/>
</div>
<div class="lyrico-lyrics-wrapper">Tene kahiye je
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tene kahiye je"/>
</div>
<div class="lyrico-lyrics-wrapper">Peed paraayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peed paraayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaanae re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaanae re"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Par dukhkhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Par dukhkhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Upkaar kare toye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Upkaar kare toye"/>
</div>
<div class="lyrico-lyrics-wrapper">Man abhimaan na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Man abhimaan na"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanae re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanae re"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Par dukhkhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Par dukhkhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Upkaar kare toye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Upkaar kare toye"/>
</div>
<div class="lyrico-lyrics-wrapper">Man abhimaan na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Man abhimaan na"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanae re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanae re"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaishnav jan to
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaishnav jan to"/>
</div>
<div class="lyrico-lyrics-wrapper">Tene kahiye je
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tene kahiye je"/>
</div>
<div class="lyrico-lyrics-wrapper">Peed paraayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peed paraayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaanae re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaanae re"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaishnav jan to
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaishnav jan to"/>
</div>
<div class="lyrico-lyrics-wrapper">Tene kahiye je
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tene kahiye je"/>
</div>
<div class="lyrico-lyrics-wrapper">Peed paraayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peed paraayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaanae re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaanae re"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaaranam aayiram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaranam aayiram"/>
</div>
<div class="lyrico-lyrics-wrapper">Soozha valam seithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soozha valam seithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naarana nambi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naarana nambi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadakindraan endredhir
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadakindraan endredhir"/>
</div>
<div class="lyrico-lyrics-wrapper">Poorana porkudam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poorana porkudam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaiththu puramengum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaiththu puramengum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoranam naatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoranam naatta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kana kanden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kana kanden"/>
</div>
<div class="lyrico-lyrics-wrapper">Thozhi naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozhi naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kana kanden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kana kanden"/>
</div>
<div class="lyrico-lyrics-wrapper">Thozhi naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozhi naan"/>
</div>
</pre>
