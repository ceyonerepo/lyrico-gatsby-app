---
title: "saama kodangi song lyrics"
album: "Kadamban"
artist: "Yuvan Shankar Raja"
lyricist: "Yugabharathi"
director: "Ragava"
path: "/albums/kadamban-lyrics"
song: "Saama Kodangi"
image: ../../images/albumart/kadamban.jpg
date: 2017-04-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/RAGoT4kqOb8"
type: "happy"
singers:
  -	Anitha Karthikeyan
  - Velmurugan
  - Jayamoorthy
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ye saama kodaangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye saama kodaangi"/>
</div>
<div class="lyrico-lyrics-wrapper">Udukka pol enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udukka pol enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Poluthum ulukkuriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poluthum ulukkuriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha seema muchchudum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha seema muchchudum"/>
</div>
<div class="lyrico-lyrics-wrapper">Neranji ninnalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neranji ninnalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirichae nee enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirichae nee enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolluriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolluriyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye saama kodaangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye saama kodaangi"/>
</div>
<div class="lyrico-lyrics-wrapper">Udukka pol enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udukka pol enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Poluthum ulukkuriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poluthum ulukkuriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha seema muchchudum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha seema muchchudum"/>
</div>
<div class="lyrico-lyrics-wrapper">Neranji ninnalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neranji ninnalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirichae nee enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirichae nee enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolluriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolluriyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En muthu nava rathinamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En muthu nava rathinamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mundhi aninji
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mundhi aninji"/>
</div>
<div class="lyrico-lyrics-wrapper">Ne vanthu nikka pogudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne vanthu nikka pogudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">En kannu avinji
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kannu avinji"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanavae illadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavae illadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Karumba um pechu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karumba um pechu"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasa thithippa maathiduchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasa thithippa maathiduchi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kodi ruvaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodi ruvaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodaiyaa thandhalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodaiyaa thandhalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Odhukki vaippomungaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odhukki vaippomungaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ettu maadi veedellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettu maadi veedellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethukku venaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethukku venaanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ola kottaayae pothummunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ola kottaayae pothummunga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadu pooraavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadu pooraavum"/>
</div>
<div class="lyrico-lyrics-wrapper">Udhavum sondhandhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udhavum sondhandhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalanga maattommunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalanga maattommunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Vera aalu ullara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vera aalu ullara"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuninuchi vandhaalaeUsura 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuninuchi vandhaalaeUsura "/>
</div>
<div class="lyrico-lyrics-wrapper">thanthaachum kaappomunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanthaachum kaappomunga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey kattayila poradhu thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey kattayila poradhu thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha usuru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha usuru"/>
</div>
<div class="lyrico-lyrics-wrapper">Itha kanniyamaa vachukittaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Itha kanniyamaa vachukittaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna thavaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna thavaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edhaiyum thannulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhaiyum thannulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Marachchi vaikkaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marachchi vaikkaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadaa vaalvomae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadaa vaalvomae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga naanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga naanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aasa pattaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasa pattaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhaiyum kekkaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhaiyum kekkaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Edukka koodaadhunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edukka koodaadhunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Veli vesham podaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veli vesham podaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Nesamaa vaazhnthaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nesamaa vaazhnthaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhuvum thannala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhuvum thannala"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai koodungha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai koodungha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Verae illaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verae illaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Maramum vaazhadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maramum vaazhadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vivaram solvommunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vivaram solvommunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ventha sorae aanalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ventha sorae aanalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Uzhachchi thingaatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uzhachchi thingaatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Udambil ottaadhu kettukkanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udambil ottaadhu kettukkanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey uththamanaa vaazhum vara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey uththamanaa vaazhum vara"/>
</div>
<div class="lyrico-lyrics-wrapper">Illa kavala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illa kavala"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee unna nitham nambum vara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee unna nitham nambum vara"/>
</div>
<div class="lyrico-lyrics-wrapper">Undu ragala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undu ragala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vazhiyum maaraama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhiyum maaraama"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadandha thappilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadandha thappilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Nimundhu vaazhvomae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nimundhu vaazhvomae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorae sollaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorae sollaa"/>
</div>
</pre>
