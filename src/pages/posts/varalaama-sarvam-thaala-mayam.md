---
title: "varalaama song lyrics"
album: "Sarvam Thaala Mayam"
artist: "Rajiv Menon"
lyricist: "Madhan Karky"
director: "Rajiv Menon"
path: "/albums/sarvam-thaala-mayam-lyrics"
song: "Varalaama"
image: ../../images/albumart/sarvam-thaala-mayam.jpg
date: 2019-02-01
lang: tamil
youtubeLink: "https://www.youtube.com/embed/GfVYDSSr7Fw"
type: "sad"
singers:
  - Sriram Parthasarathy
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Varalaama Un Arugil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varalaama Un Arugil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Peralaama Un Arulai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peralaama Un Arulai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varalaamma Un Arugil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varalaamma Un Arugil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Peralaama Un Arulai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peralaama Un Arulai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thirumbaayo En Dhisaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirumbaayo En Dhisaiyil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Asaivaayo En Isaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Asaivaayo En Isaiyil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Asaivaayo En Isaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Asaivaayo En Isaiyil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhai Kaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai Kaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varalaama Un Arugil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varalaama Un Arugil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Peralaama Un Arulai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peralaama Un Arulai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thirumbaayo En Dhisaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirumbaayo En Dhisaiyil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Asaivaayo En Isaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Asaivaayo En Isaiyil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Asaivaayo En Isaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Asaivaayo En Isaiyil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhai Kaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai Kaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Puzhuthiyil Pudhainthavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puzhuthiyil Pudhainthavan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pudhithena Pirandhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhithena Pirandhen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mudhal Murai Ul Mana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhal Murai Ul Mana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhigalai Thirandhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhigalai Thirandhen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Puzhuthiyil Pudhainthavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puzhuthiyil Pudhainthavan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pudhithena Pirandhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhithena Pirandhen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mudhal Murai Ul Mana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhal Murai Ul Mana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhigalai Thirandhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhigalai Thirandhen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Viralgalai Siragena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viralgalai Siragena"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Virindhida Parandhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Virindhida Parandhen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Virindhida Parandhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Virindhida Parandhen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varalaamaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varalaamaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varalaamaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varalaamaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varalaama Un Arugil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varalaama Un Arugil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidhaiyinul Virutchamaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidhaiyinul Virutchamaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sollinul Porulaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollinul Porulaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mazhalaikkul Amizhthamaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhalaikkul Amizhthamaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kallinul Silaiyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallinul Silaiyaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidhaiyinul Virutchamaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidhaiyinul Virutchamaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sollinul Porulaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollinul Porulaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mazhalaikkul Amizhthamaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhalaikkul Amizhthamaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kallinul Silaiyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallinul Silaiyaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anuvinul Agilamaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anuvinul Agilamaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakulley Isaiyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakulley Isaiyaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unai Kandu Kalika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai Kandu Kalika"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varalaamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varalaamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varalaama Un Arugil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varalaama Un Arugil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Peralaama Un Arulai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peralaama Un Arulai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thirumbaayo En Dhisaiyill
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirumbaayo En Dhisaiyill"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Asaivaaayoo En Isaiyill
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Asaivaaayoo En Isaiyill"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Aasivaayo En Isaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Aasivaayo En Isaiyil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhai Kaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai Kaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Asaivaayo En Isaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Asaivaayo En Isaiyil"/>
</div>
</pre>
