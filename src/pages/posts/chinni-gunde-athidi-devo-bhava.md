---
title: "chinni gunde song lyrics"
album: "Athidi Devo Bhava"
artist: "Shekar Chandra"
lyricist: "Krishna Kanth"
director: "Polimera Nageshawar"
path: "/albums/athidi-devo-bhava-lyrics"
song: "Chinni Gunde"
image: ../../images/albumart/athidi-devo-bhava.jpg
date: 2022-01-07
lang: telugu
youtubeLink: "https://www.youtube.com/embed/yuY43zHYEwA"
type: "love"
singers:
  - unknown
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Chinni gunde nannila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinni gunde nannila"/>
</div>
<div class="lyrico-lyrics-wrapper">Allukundi tegala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allukundi tegala"/>
</div>
<div class="lyrico-lyrics-wrapper">Reppapatu chudakunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Reppapatu chudakunda"/>
</div>
<div class="lyrico-lyrics-wrapper">Undaleka undila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undaleka undila"/>
</div>
<div class="lyrico-lyrics-wrapper">Vellagane nuvvala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vellagane nuvvala"/>
</div>
<div class="lyrico-lyrics-wrapper">Ontarai lopala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ontarai lopala"/>
</div>
<div class="lyrico-lyrics-wrapper">Chandhamama lene neli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chandhamama lene neli"/>
</div>
<div class="lyrico-lyrics-wrapper">Ningi Naina nenila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ningi Naina nenila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuduru leka na madhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuduru leka na madhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aagi saguthunnadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagi saguthunnadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Oopirantha asha ga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oopirantha asha ga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu korukunnadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu korukunnadhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oo nimushamaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo nimushamaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Undalenu nuvvu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undalenu nuvvu "/>
</div>
<div class="lyrico-lyrics-wrapper">Leni bhavi paina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Leni bhavi paina"/>
</div>
<div class="lyrico-lyrics-wrapper">Dehamaina vidiponi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dehamaina vidiponi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu Nene nimpana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu Nene nimpana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye janmadho ee bhandamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye janmadho ee bhandamu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee janmaku na sonthamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee janmaku na sonthamu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ni shawsatho na pranamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ni shawsatho na pranamu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sagede jeevitham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sagede jeevitham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ni thodu na oopiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ni thodu na oopiri"/>
</div>
<div class="lyrico-lyrics-wrapper">Okate le adi satyamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okate le adi satyamu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye chota vadilesina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye chota vadilesina"/>
</div>
<div class="lyrico-lyrics-wrapper">Ni nidale marana nikai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ni nidale marana nikai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oo nimushamaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo nimushamaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Undalenu nuvvu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undalenu nuvvu "/>
</div>
<div class="lyrico-lyrics-wrapper">Leni bhavi paina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Leni bhavi paina"/>
</div>
<div class="lyrico-lyrics-wrapper">Dehamaina vidiponi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dehamaina vidiponi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu Nene nimpana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu Nene nimpana"/>
</div>
</pre>
