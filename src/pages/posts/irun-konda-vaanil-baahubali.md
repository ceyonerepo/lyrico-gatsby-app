---
title: 'irul konda vaanil song lyrics'
album: 'Baahubali'
artist: "M.M. Keeravani"
lyricist: 'Madhan Karky'
director: 'S.S. Rajamouli'
path: '/albums/baahubali-song-lyrics'
song: 'Irul Konda Vaanil'
image: ../../images/albumart/baahubali.jpg
date: 2015-06-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/6fQOM9pIOYM"
type: 'Love'
singers: 
- Deepika
---

<pre class="lyrics-native">
  </pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Irul konda vaanil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irul konda vaanil"/>
</div>
<div class="lyrico-lyrics-wrapper">Ival deepa oli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ival deepa oli"/>
</div>
<div class="lyrico-lyrics-wrapper">Ival madi kootil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ival madi kootil"/>
</div>
<div class="lyrico-lyrics-wrapper">Mulaikkum bahubali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mulaikkum bahubali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadaiyum indha paar kadalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadaiyum indha paar kadalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanjaa… amuthaa… mozhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanjaa… amuthaa… mozhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaan vittu magizhmathi aandidavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaan vittu magizhmathi aandidavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vantha sooriyan bahubali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vantha sooriyan bahubali"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaagaigal magudangal soodiduvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaagaigal magudangal soodiduvaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Engal naayagan bahubali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engal naayagan bahubali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadaiyum indha paar kadalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadaiyum indha paar kadalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanjaa… amuthaa… mozhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanjaa… amuthaa… mozhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ambendrum kuri maariyathillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ambendrum kuri maariyathillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaalendrum pasi aariyathillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaalendrum pasi aariyathillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudivendrum pin vaangiyathillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudivendrum pin vaangiyathillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaanae senai aavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanae senai aavan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaayae ivan deivam enbaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaayae ivan deivam enbaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thamayan ivan thozhan enbaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thamayan ivan thozhan enbaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorae than sontham enbaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorae than sontham enbaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaanae desam aavaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanae desam aavaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saasanam yethu..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saasanam yethu.."/>
</div>
<div class="lyrico-lyrics-wrapper">Sivagami sol athu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sivagami sol athu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhi ondril idhesam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhi ondril idhesam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhi ondril paasam kondae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhi ondril paasam kondae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadaiyum indha paar kadalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadaiyum indha paar kadalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanjaa… amuthaa… mozhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanjaa… amuthaa… mozhi"/>
</div>
</pre>