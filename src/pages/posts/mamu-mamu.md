---
title: "mamu mamu song lyrics"
album: "charlie chaplin 2"
artist: "amrish"
lyricist: "kadhal mathi"
director: "Sakthi chithambaram"
path: "/albums/charlie-chaplin-2-song-lyrics"
song: "mamu mamu"
image: ../../images/albumart/charlie-chaplin-2.jpg
date: 2019-01-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/NZZ_zOEqtVQ"
type: "introduction"
singers:
  - amrish
---

<pre class="lyrics-native">
</pre>
<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Mamu Mamu Mamu Mamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mamu Mamu Mamu Mamu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan Maattikittan Mamu Mamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Maattikittan Mamu Mamu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mamu Mamu Mamu Mamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mamu Mamu Mamu Mamu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan Maattikittan Mamu Mamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Maattikittan Mamu Mamu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tirupathi Malai Mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tirupathi Malai Mela"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan Mottaiya Theduranda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Mottaiya Theduranda"/>
</div>
<div class="lyrico-lyrics-wrapper">Pazhani Malai Mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pazhani Malai Mela"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan Pattaiya Theduranda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Pattaiya Theduranda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mamu Mamu Mamu Mamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mamu Mamu Mamu Mamu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan Maattikittan Mamu Mamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Maattikittan Mamu Mamu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mamu Mamu Mamu Mamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mamu Mamu Mamu Mamu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan Maattikittan Mamu Mamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Maattikittan Mamu Mamu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Pulivaalil Fevicol Ottiputtan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Pulivaalil Fevicol Ottiputtan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan Eli Porikulla Thalaiya Vittu Maattikittan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Eli Porikulla Thalaiya Vittu Maattikittan"/>
</div>
<div class="lyrico-lyrics-wrapper">Summa Kidantha Sangu Eduthu Oothiputtan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summa Kidantha Sangu Eduthu Oothiputtan"/>
</div>
<div class="lyrico-lyrics-wrapper">Sontha Selavula Sooniyam Thaan Vechikittan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sontha Selavula Sooniyam Thaan Vechikittan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idiyappa Sikkalula Sikkikittanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idiyappa Sikkalula Sikkikittanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan Yezhurukku Sms Anuppiputtanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Yezhurukku Sms Anuppiputtanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiya Kaatti Kadalkulla Kuthichiputtanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiya Kaatti Kadalkulla Kuthichiputtanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendu Kanna Katti Nadu Road-Tule Nadanthuputtanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendu Kanna Katti Nadu Road-Tule Nadanthuputtanda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Natpula Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Natpula Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Adicha Mappula Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adicha Mappula Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Senja Tappula Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senja Tappula Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Sikkikittan Mappilai Mappilai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sikkikittan Mappilai Mappilai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Natpule Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Natpule Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Adicha Mappule Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adicha Mappule Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Senja Tappule Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senja Tappule Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Sikkikittan Mappilai Mappilai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sikkikittan Mappilai Mappilai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mamu Mamu Mamu Mamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mamu Mamu Mamu Mamu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan Maattikittan Mamu Mamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Maattikittan Mamu Mamu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mamu Mamu Mamu Mamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mamu Mamu Mamu Mamu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan Maattikittan Mamu Mamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Maattikittan Mamu Mamu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tirupathi Malai Mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tirupathi Malai Mela"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan Mottaiya Theduranda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Mottaiya Theduranda"/>
</div>
<div class="lyrico-lyrics-wrapper">Pazhani Malai Mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pazhani Malai Mela"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan Pattaiya Theduranda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Pattaiya Theduranda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mamu Mamu Mamu Mamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mamu Mamu Mamu Mamu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan Maattikittan Mamu Mamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Maattikittan Mamu Mamu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mamu Mamu Mamu Mamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mamu Mamu Mamu Mamu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan Maattikittan Mamu Mamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Maattikittan Mamu Mamu"/>
</div>
</pre>
