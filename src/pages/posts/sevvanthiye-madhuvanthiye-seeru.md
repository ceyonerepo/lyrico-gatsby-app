---
title: "sevvanthiye madhuvanthiye song lyrics"
album: "Seeru"
artist: "D. Imman"
lyricist: "Parvathy"
director: "Rathnasiva"
path: "/albums/seeru-lyrics"
song: "Sevvanthiye Madhuvanthiye"
image: ../../images/albumart/seeru.jpg
date: 2020-02-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/M0F8JAEuGks"
type: "Sad"
singers:
  - Nochipatti Thirumoorthi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Sevvanthiye Mathuvanthiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sevvanthiye Mathuvanthiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivale Inimel Puviyin Raaniye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivale Inimel Puviyin Raaniye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sevvanthiye Mathuvanthiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sevvanthiye Mathuvanthiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nadakkum Pothey Parakkum Thaeniye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadakkum Pothey Parakkum Thaeniye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaniyaa Amuthaa Pasum Paal Kozhuntha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaniyaa Amuthaa Pasum Paal Kozhuntha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neruppu Thugalin Pala Naal Vizhuthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neruppu Thugalin Pala Naal Vizhuthaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalavai Pol Oru Nooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalavai Pol Oru Nooru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thani Thanmai Gundam Undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thani Thanmai Gundam Undu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivalaal Anaithum Alattidum Azhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivalaal Anaithum Alattidum Azhagu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sevvanthiye Mathuvanthiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sevvanthiye Mathuvanthiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivale Inimel Puviyin Raaniye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivale Inimel Puviyin Raaniye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sevvanthiye Mathuvanthiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sevvanthiye Mathuvanthiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nadakkum Pothey Parakkum Thaeniye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadakkum Pothey Parakkum Thaeniye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neer Veezhchiyai Veezhchi Endru Solvathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neer Veezhchiyai Veezhchi Endru Solvathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kobam Serkkume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kobam Serkkume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Illai Illai Aruvi Endru Sonnathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illai Illai Aruvi Endru Sonnathum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Kannile Anbu Pookkume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kannile Anbu Pookkume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Sol Thaan Endraalume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Sol Thaan Endraalume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanam Pondrathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam Pondrathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ena Solvaal Thozhi Neeyum Pookkalin Madhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ena Solvaal Thozhi Neeyum Pookkalin Madhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mara Paachi Bommai Pola Nerthi Unnathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mara Paachi Bommai Pola Nerthi Unnathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tirupachi Pola Koormai Pechil Ullathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tirupachi Pola Koormai Pechil Ullathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mayil Peeli Pol Idhamaanaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayil Peeli Pol Idhamaanaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Verum Thaazhai Pol Manam Kondaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verum Thaazhai Pol Manam Kondaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhinam Dhinam Kondaattamaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam Dhinam Kondaattamaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ival Aakkinaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ival Aakkinaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sevvanthiye Mathuvanthiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sevvanthiye Mathuvanthiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivale Inimel Puviyin Raaniye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivale Inimel Puviyin Raaniye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sevvanthiye Mathuvanthiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sevvanthiye Mathuvanthiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nadakkum Pothey Parakkum Thaeniye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadakkum Pothey Parakkum Thaeniye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaniyaa Amuthaa Pasum Paal Kozhuntha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaniyaa Amuthaa Pasum Paal Kozhuntha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neruppu Thugalin Pala Naal Vizhuthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neruppu Thugalin Pala Naal Vizhuthaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalavai Pol Oru Nooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalavai Pol Oru Nooru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thani Thanmai Gundam Undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thani Thanmai Gundam Undu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivalaal Anaithum Alattidum Azhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivalaal Anaithum Alattidum Azhagu"/>
</div>
</pre>
