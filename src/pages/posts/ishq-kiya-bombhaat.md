---
title: "ishq kiya song lyrics"
album: "Bombhaat"
artist: "Josh B"
lyricist: "Ramanjaneyulu"
director: "Raghavendra Varma"
path: "/albums/bombhaat-lyrics"
song: "Ishq Kiya"
image: ../../images/albumart/bombhaat.jpg
date: 2020-12-03
lang: telugu
youtubeLink: "https://www.youtube.com/embed/EwX-S1iItQE"
type: "love"
singers:
  - Sunitha Sarathy
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kallalona dhachinanu le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallalona dhachinanu le"/>
</div>
<div class="lyrico-lyrics-wrapper">Reppa dhaati polevu le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Reppa dhaati polevu le"/>
</div>
<div class="lyrico-lyrics-wrapper">kaatukaiana pettalenu le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatukaiana pettalenu le"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohh niku antukuntundhani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohh niku antukuntundhani"/>
</div>
<div class="lyrico-lyrics-wrapper">Pedhavike theepani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pedhavike theepani"/>
</div>
<div class="lyrico-lyrics-wrapper">Palike ne peruni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palike ne peruni"/>
</div>
<div class="lyrico-lyrics-wrapper">Priyatama oo priathama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Priyatama oo priathama"/>
</div>
<div class="lyrico-lyrics-wrapper">Lokame aanadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lokame aanadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maikame veedadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maikame veedadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Telusuna idhi premenani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Telusuna idhi premenani"/>
</div>
<div class="lyrico-lyrics-wrapper">Yendhukilaaa… yendhukilaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yendhukilaaa… yendhukilaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gundenila unchakila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundenila unchakila"/>
</div>
<div class="lyrico-lyrics-wrapper">Ontarigaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ontarigaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chalentha chotundhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chalentha chotundhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalona neekosam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalona neekosam"/>
</div>
<div class="lyrico-lyrics-wrapper">Dagundi ponanadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dagundi ponanadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Needega aa neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Needega aa neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Rammantu pampanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rammantu pampanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ahvanama andhenuaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahvanama andhenuaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhariki cheruvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhariki cheruvu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa ooha nindara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa ooha nindara"/>
</div>
<div class="lyrico-lyrics-wrapper">Nindindhi ni roopam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nindindhi ni roopam"/>
</div>
<div class="lyrico-lyrics-wrapper">Na oopirundanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na oopirundanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Raadandi ey dhooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raadandi ey dhooram"/>
</div>
<div class="lyrico-lyrics-wrapper">Avunantu oo maata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avunantu oo maata"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee nota cheppavuga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee nota cheppavuga"/>
</div>
<div class="lyrico-lyrics-wrapper">Naatho nuvvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naatho nuvvu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chilipiga choosina palakavem chesina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chilipiga choosina palakavem chesina"/>
</div>
<div class="lyrico-lyrics-wrapper">Parakulo padithivemo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parakulo padithivemo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanamanee na kala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanamanee na kala"/>
</div>
<div class="lyrico-lyrics-wrapper">Adigithe ninnila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adigithe ninnila"/>
</div>
<div class="lyrico-lyrics-wrapper">Nidharane cheravantoo naa bayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nidharane cheravantoo naa bayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Yendhukilaaa yendhukilaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yendhukilaaa yendhukilaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yendhukilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yendhukilaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Galullo rasanu na prema lekalne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Galullo rasanu na prema lekalne"/>
</div>
<div class="lyrico-lyrics-wrapper">Ni gundeloo cheri chappullu cheseney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ni gundeloo cheri chappullu cheseney"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinatte levinka na vanka chudavugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinatte levinka na vanka chudavugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Palakarimpu gaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palakarimpu gaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mabbullo dhachanu na prema gurthulne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mabbullo dhachanu na prema gurthulne"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa ningi anachulne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa ningi anachulne"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaatinchi chinukulne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaatinchi chinukulne"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee vaan ne paina varshinchaneeyavuga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee vaan ne paina varshinchaneeyavuga"/>
</div>
<div class="lyrico-lyrics-wrapper">Anandhanga samayame saagina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anandhanga samayame saagina"/>
</div>
<div class="lyrico-lyrics-wrapper">Hrudhayame aagina jatey padi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hrudhayame aagina jatey padi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadavamantoo parichayam cheyana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadavamantoo parichayam cheyana"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasulo dhaachina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasulo dhaachina"/>
</div>
<div class="lyrico-lyrics-wrapper">Manishine kalla mundhu Choopana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manishine kalla mundhu Choopana"/>
</div>
<div class="lyrico-lyrics-wrapper">Ishq kiya ishq kiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishq kiya ishq kiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Ishq kiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishq kiya"/>
</div>
</pre>
