---
title: "paina pataaram song lyrics"
album: "Chaavu Kaburu Challaga"
artist: "Jakes Bejoy"
lyricist: "Sanare"
director: "Pegallapati Koushik"
path: "/albums/chaavu-kaburu-challaga-lyrics"
song: "Paina Pataaram - Puttu Vela Thalliki"
image: ../../images/albumart/chaavu-kaburu-challaga.jpg
date: 2021-03-19
lang: telugu
youtubeLink: "https://www.youtube.com/embed/-P8m135HMLw"
type: "happy"
singers:
  - Mangli
  - Ram
  - Saketh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Puttu vela thalliki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puttu vela thalliki"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu puriti noppi vaithivi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu puriti noppi vaithivi"/>
</div>
<div class="lyrico-lyrics-wrapper">Gittu vela aalikemo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gittu vela aalikemo"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasu noppivaithiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu noppivaithiva"/>
</div>
<div class="lyrico-lyrics-wrapper">Gittu vela aalikemo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gittu vela aalikemo"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasu noppivaithiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu noppivaithiva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Batta maraka padithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Batta maraka padithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu kotha battalantivi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu kotha battalantivi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippudemo uthaleni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippudemo uthaleni"/>
</div>
<div class="lyrico-lyrics-wrapper">Matti batta kadithiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Matti batta kadithiva"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippudemo uthaleni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippudemo uthaleni"/>
</div>
<div class="lyrico-lyrics-wrapper">Matti batta kadithiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Matti batta kadithiva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ettagunnavayyo peter annayyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettagunnavayyo peter annayyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Needhi edhi emaina goppa savayyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Needhi edhi emaina goppa savayyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Pucchu thosi manchi vanga erinattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pucchu thosi manchi vanga erinattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Swaccha manti saami ninne korinaadayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Swaccha manti saami ninne korinaadayya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paina pataram eeda lona lotaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paina pataram eeda lona lotaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Inu bossu sebuthanee lokamevvaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inu bossu sebuthanee lokamevvaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Inu bossu sebuthanee lokamevvaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inu bossu sebuthanee lokamevvaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Paiki bangaram lona goodu gutaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paiki bangaram lona goodu gutaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Geliki soodu telisipodhi asalu bandaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Geliki soodu telisipodhi asalu bandaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Geliki soodu telisipodhi asalu bandaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Geliki soodu telisipodhi asalu bandaram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manushulu mayagallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manushulu mayagallu"/>
</div>
<div class="lyrico-lyrics-wrapper">Macchalunna ketugallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Macchalunna ketugallu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kani evarikaallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kani evarikaallu"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasulunna great gallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasulunna great gallu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naadhi naadhi anna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naadhi naadhi anna"/>
</div>
<div class="lyrico-lyrics-wrapper">Swaardhamunna seddavallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Swaardhamunna seddavallu"/>
</div>
<div class="lyrico-lyrics-wrapper">Neela brathakaleni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neela brathakaleni"/>
</div>
<div class="lyrico-lyrics-wrapper">Dabbulunna pedhavallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dabbulunna pedhavallu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naakada vandhunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naakada vandhunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa yante thirigetollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa yante thirigetollu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kaada veyyunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kaada veyyunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa paine morugutharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa paine morugutharu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandhanta pothante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhanta pothante"/>
</div>
<div class="lyrico-lyrics-wrapper">Soosi kuda palakanollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soosi kuda palakanollu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kaada sommunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kaada sommunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Inta cheri pogudutharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inta cheri pogudutharu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lokamentho lothayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lokamentho lothayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Peter annnyya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peter annnyya"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhi thovvi chudadanike
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhi thovvi chudadanike"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee jeevithamayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee jeevithamayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Thavve koddhi vasthuntaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thavve koddhi vasthuntaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninda munchi pothuntaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninda munchi pothuntaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Neetho natho unde sagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neetho natho unde sagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Dongollenayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dongollenayya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">We are very happy bossu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="We are very happy bossu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuv undedhe self-esteem case
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuv undedhe self-esteem case"/>
</div>
<div class="lyrico-lyrics-wrapper">We are very happy bossu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="We are very happy bossu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuv undedhe self-esteem case
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuv undedhe self-esteem case"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paina pataram eeda lona lotaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paina pataram eeda lona lotaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Inu bossu sebuthanee lokamevvaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inu bossu sebuthanee lokamevvaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Inu bossu sebuthanee lokamevvaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inu bossu sebuthanee lokamevvaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Paiki bangaram lona goodu gutaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paiki bangaram lona goodu gutaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Geliki soodu telisipodhi asalu bandaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Geliki soodu telisipodhi asalu bandaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Geliki soodu telisipodhi asalu bandaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Geliki soodu telisipodhi asalu bandaram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Caru bangalalu velikunna ungaralu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Caru bangalalu velikunna ungaralu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yevi ravanta sacchinaka manaventa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yevi ravanta sacchinaka manaventa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neetho unnavallu ninnu mosi kannavallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neetho unnavallu ninnu mosi kannavallu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vellipotharantha veliginaka sirimana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vellipotharantha veliginaka sirimana"/>
</div>
<div class="lyrico-lyrics-wrapper">Matti meedha nuv kalisina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Matti meedha nuv kalisina"/>
</div>
<div class="lyrico-lyrics-wrapper">Bandhalanni abaddham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bandhalanni abaddham"/>
</div>
<div class="lyrico-lyrics-wrapper">Matti lona pichi purugula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Matti lona pichi purugula"/>
</div>
<div class="lyrico-lyrics-wrapper">Jatte chivari prapancham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jatte chivari prapancham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manishi theeru maradhayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manishi theeru maradhayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Peter annayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peter annayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Andhukane sebuthunna inaraadhayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhukane sebuthunna inaraadhayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhadheleni bengeleni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhadheleni bengeleni"/>
</div>
<div class="lyrico-lyrics-wrapper">Repentanna sintheleni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Repentanna sintheleni"/>
</div>
<div class="lyrico-lyrics-wrapper">Chotedanna unnadhante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chotedanna unnadhante"/>
</div>
<div class="lyrico-lyrics-wrapper">Samasanamera, andhuke!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samasanamera, andhuke!"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">We are very happy bossu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="We are very happy bossu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuv undedhe self-esteem case
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuv undedhe self-esteem case"/>
</div>
<div class="lyrico-lyrics-wrapper">We are very happy bossu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="We are very happy bossu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuv undedhe self-esteem case
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuv undedhe self-esteem case"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paina pataram eeda lona lotaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paina pataram eeda lona lotaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Inu bossu sebuthanee lokamevvaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inu bossu sebuthanee lokamevvaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Inu bossu sebuthanee lokamevvaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inu bossu sebuthanee lokamevvaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Paiki bangaram lona goodu gutaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paiki bangaram lona goodu gutaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Geliki soodu telisipodhi asalu bandaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Geliki soodu telisipodhi asalu bandaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Geliki soodu telisipodhi asalu bandaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Geliki soodu telisipodhi asalu bandaram"/>
</div>
</pre>
