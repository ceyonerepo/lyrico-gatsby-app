---
title: "ammadi song lyrics"
album: "Rudra Thandavam"
artist: "Jubin"
lyricist: "Jubin"
director: "Mohan G Kshatriyan"
path: "/albums/rudra-thandavam-lyrics"
song: "Ammadi"
image: ../../images/albumart/rudra-thandavam.jpg
date: 2021-10-01
lang: tamil
youtubeLink: "https://www.youtube.com/embed/sAJ4XhzVL8A"
type: "sad"
singers:
  - V Prakash Kumar
  - Mookuthi Murugan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ammadi Enna Senja Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammadi Enna Senja Enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Thundu Thunda Vetti Potta Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thundu Thunda Vetti Potta Enna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Usur Arukkuthu Ullam Thudikkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usur Arukkuthu Ullam Thudikkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Norukuthu Un Solla Kettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Norukuthu Un Solla Kettu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vellam Adikkuthu Nenja Kilikuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vellam Adikkuthu Nenja Kilikuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nonthu Thavikuthu Un Solla Kettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nonthu Thavikuthu Un Solla Kettu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ne Poga Sonnaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne Poga Sonnaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Epo Enga Poven Naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Epo Enga Poven Naa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ne Poga Sonnaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne Poga Sonnaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Epo Enga Poven Naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Epo Enga Poven Naa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pinchu Mugam Enna Pakala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pinchu Mugam Enna Pakala"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha Vali Innum Pogala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Vali Innum Pogala"/>
</div>
<div class="lyrico-lyrics-wrapper">Saamy Thantha En Devathaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saamy Thantha En Devathaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaana Vazhi Kodu Kanmaniye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaana Vazhi Kodu Kanmaniye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kottum Mazhaiyila Yerikiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottum Mazhaiyila Yerikiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullukkula Naan Urukiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullukkula Naan Urukiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pacha Thanniyila Veguren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pacha Thanniyila Veguren"/>
</div>
<div class="lyrico-lyrics-wrapper">Ne Pakkam Irunthatha Ninaikuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne Pakkam Irunthatha Ninaikuren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ventha Punnula Veesura Ne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ventha Punnula Veesura Ne"/>
</div>
<div class="lyrico-lyrics-wrapper">Amba Eduthu Vegama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amba Eduthu Vegama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kollama Kolluriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kollama Kolluriye"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollula Kaththiya Kokkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollula Kaththiya Kokkura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Innum Enna Seiya Pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum Enna Seiya Pora"/>
</div>
<div class="lyrico-lyrics-wrapper">Thangura Nenjam Enakkilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangura Nenjam Enakkilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ammadi Enna Senja Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammadi Enna Senja Enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Thundu Thunda Vetti Potta Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thundu Thunda Vetti Potta Enna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Usur Arukkuthu Ullam Thudikkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usur Arukkuthu Ullam Thudikkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Norukuthu Un Solla Kettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Norukuthu Un Solla Kettu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vellam Adikkuthu Nenja Kilikuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vellam Adikkuthu Nenja Kilikuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nonthu Thavikuthu Un Solla Kettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nonthu Thavikuthu Un Solla Kettu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ne Poga Sonnaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne Poga Sonnaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Epo Enga Poven Naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Epo Enga Poven Naa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ne Poga Sonnaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne Poga Sonnaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Epo Enga Poven Naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Epo Enga Poven Naa"/>
</div>
</pre>
