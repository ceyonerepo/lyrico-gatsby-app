---
title: "pallaanguzhi song lyrics"
album: "Yennanga Sir Unga Sattam"
artist: "Guna Balasubramanian"
lyricist: "Jegan Kaviraj"
director: "Prabhu Jeyaram"
path: "/albums/yennanga-sir-unga-sattam-lyrics"
song: "Pallaanguzhi"
image: ../../images/albumart/yennanga-sir-unga-sattam.jpg
date: 2021-10-29
lang: tamil
youtubeLink: "https://www.youtube.com/embed/D8deIFd0om0"
type: "happy"
singers:
  - Malvi Sundaresan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ada pallaanguzhi venaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada pallaanguzhi venaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Un nejaanguzhi pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un nejaanguzhi pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini onnum solla venaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini onnum solla venaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha jenmam unnil podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha jenmam unnil podhum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada pallaanguzhi venaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada pallaanguzhi venaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Un nejaanguzhi pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un nejaanguzhi pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini onnum solla venaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini onnum solla venaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha jenmam unnil podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha jenmam unnil podhum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhiye nee paarkaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhiye nee paarkaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidiyadhu kanavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidiyadhu kanavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhikaalai mugam kaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhikaalai mugam kaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kulirudhu manasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulirudhu manasu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada pallaanguzhi venaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada pallaanguzhi venaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Un nejaanguzhi pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un nejaanguzhi pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini onnum solla venaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini onnum solla venaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha jenmam unnil podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha jenmam unnil podhum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nedu naalaiya perunkan avai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nedu naalaiya perunkan avai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nijamaakka vandhaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijamaakka vandhaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalamellaam kadhal pesanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalamellaam kadhal pesanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vilagaatha bandhamena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilagaatha bandhamena"/>
</div>
<div class="lyrico-lyrics-wrapper">Viral meedhu sondham kondaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viral meedhu sondham kondaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan nagaththil oodal thaniyanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan nagaththil oodal thaniyanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iraiye en iraiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iraiye en iraiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru iragai pola odi vandhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru iragai pola odi vandhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Aruge un adharuge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aruge un adharuge"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai amarthikkolvaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai amarthikkolvaaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada pallaanguzhi venaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada pallaanguzhi venaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Un nejaanguzhi pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un nejaanguzhi pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini onnum solla venaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini onnum solla venaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha jenmam unnil podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha jenmam unnil podhum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhiye nee paarkaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhiye nee paarkaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidiyadhu kanavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidiyadhu kanavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhikaalai mugam kaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhikaalai mugam kaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kulirudhu manasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulirudhu manasu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada pallaanguzhi venaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada pallaanguzhi venaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Un nejaanguzhi pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un nejaanguzhi pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini onnum solla venaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini onnum solla venaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha jenmam unnil podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha jenmam unnil podhum"/>
</div>
</pre>
