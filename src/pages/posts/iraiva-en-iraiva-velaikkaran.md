---
title: "iraiva en iraiva song lyrics"
album: "Velaikkaran"
artist: "Anirudh Ravichander"
lyricist: "Viveka"
director: "Mohan Raja"
path: "/albums/velaikkaran-lyrics"
song: "Iraiva en Iraiva"
image: ../../images/albumart/velaikkaran.jpg
date: 2017-12-22
lang: tamil
youtubeLink: "https://www.youtube.com/embed/dJY2SdxG6n4"
type: "Love"
singers:
  - Anirudh Ravichander
  - Jonita Gandhi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Iraiva En Iraiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iraiva En Iraiva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Thedi En Manam Porkalam Aanadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Thedi En Manam Porkalam Aanadhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iraiva En Iraiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iraiva En Iraiva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endhan Iru Kaalgalai Paadhaiye Meyudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan Iru Kaalgalai Paadhaiye Meyudhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enai Padaiththavan Needhaanaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai Padaiththavan Needhaanaiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyir Valarathathum Needhaanaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Valarathathum Needhaanaiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enai Sabithavan Needhaanaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai Sabithavan Needhaanaiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyir Eriththaal Thaangadhaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Eriththaal Thaangadhaiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enai Sabithavan Needhaanaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai Sabithavan Needhaanaiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyir Eriththaal Thaangadhaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Eriththaal Thaangadhaiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Vaazhavaa Naan Veezhavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Vaazhavaa Naan Veezhavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Seivadhu Nee Sollu Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Seivadhu Nee Sollu Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enai Sabithavan Needhaanaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai Sabithavan Needhaanaiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyir Eriththaal Thaangadhaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Eriththaal Thaangadhaiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Vaazhavaa Naan Veezhavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Vaazhavaa Naan Veezhavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Seivadhu Nee Sollu Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Seivadhu Nee Sollu Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa Vaa Vaa Vaa Vaaa Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Vaa Vaa Vaa Vaaa Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa Vaa Vaa Vaa  Iraivaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Vaa Vaa Vaa  Iraivaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa Vaa Vaa Vaa Vaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Vaa Vaa Vaa Vaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa Vaa Vaa Vaa Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Vaa Vaa Vaa Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyire En Urave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyire En Urave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Vittu Povadhum Saavadhum Ondrudhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Vittu Povadhum Saavadhum Ondrudhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irave En Pagale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irave En Pagale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ini Varum Naalellaam Un Vizhi Munbudhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Varum Naalellaam Un Vizhi Munbudhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pirivenum Thuyar Theendaamale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirivenum Thuyar Theendaamale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thunai Irundhidum En Kadhalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunai Irundhidum En Kadhalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ilakkanam Yedhum Paaramale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilakkanam Yedhum Paaramale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adaikkalam Naan Un Marbilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adaikkalam Naan Un Marbilae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyir Vidum Varai Unnodudhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Vidum Varai Unnodudhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unai Vittaal Udal Mannodudhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai Vittaal Udal Mannodudhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Yenbadhu Naan Mattuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Yenbadhu Naan Mattuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Koodadhaan Ododi Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Koodadhaan Ododi Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyir Vidum Varai Unnodudhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Vidum Varai Unnodudhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unai Vittaal Udal Mannodudhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai Vittaal Udal Mannodudhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Yenbadhu Naan Mattuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Yenbadhu Naan Mattuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Koodathaan Ododi Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Koodathaan Ododi Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadu Malai Thaandalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadu Malai Thaandalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalgal Ranamaagalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalgal Ranamaagalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thooya Perunkaadhalin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thooya Perunkaadhalin"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aazham Varai Pogalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aazham Varai Pogalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Virumbi Adaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Virumbi Adaiyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pon Siraiyae Siraiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pon Siraiyae Siraiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Virumbi Aniya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Virumbi Aniya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Siragae Siragae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Siragae Siragae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">O Nirandharam Yena Yedhumillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Nirandharam Yena Yedhumillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nigazhnthidum Ivai Naalai Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nigazhnthidum Ivai Naalai Illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irandhidum Varai Poraadalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irandhidum Varai Poraadalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yerimalaiyilum Neeraadalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yerimalaiyilum Neeraadalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyir Vidum Varai Unnodudhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Vidum Varai Unnodudhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unai Vittaal Udal Mannodudhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai Vittaal Udal Mannodudhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Yenbadhu Naan Mattuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Yenbadhu Naan Mattuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Koodathaan Ododi Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Koodathaan Ododi Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyir Vidum Varai Unnodudhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Vidum Varai Unnodudhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unai Vittaal Udal Mannodudhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai Vittaal Udal Mannodudhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Yenbadhu Naan Mattuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Yenbadhu Naan Mattuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Koodadhaan Ododi Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Koodadhaan Ododi Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa Vaa Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Vaa Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa Vaaa Vaa Vaa Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Vaaa Vaa Vaa Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyir Vidum Varai Uyir Vidum Varai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Vidum Varai Uyir Vidum Varai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unai Vittaal Udal Unai Vittaal Udal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai Vittaal Udal Unai Vittaal Udal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Yenbadhu Naan Mattuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Yenbadhu Naan Mattuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Koodathaan Ododi Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Koodathaan Ododi Vaa"/>
</div>
</pre>
