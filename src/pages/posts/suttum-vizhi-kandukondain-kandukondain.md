---
title: "suttum vizhi song lyrics"
album: "Kandukondain Kandukondain"
artist: "A. R. Rahman"
lyricist: "Subramania Bharati"
director: "Rajiv Menon"
path: "/albums/kandukondain-kandukondain-lyrics"
song: "Suttum Vizhi"
image: ../../images/albumart/kandukondain-kandukondain.jpg
date: 2000-05-05
lang: tamil
youtubeLink: "https://www.youtube.com/embed/189zAlxRa9U"
type: "melody"
singers:
  - Hariharan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Suttum Vizhi Chchudardhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suttum Vizhi Chchudardhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sooriya Chandhiraroo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sooriya Chandhiraroo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vatta Kariyavizhi Kannamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vatta Kariyavizhi Kannamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaana Karumai Kollo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaana Karumai Kollo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pattu Karuneela Pudavai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattu Karuneela Pudavai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Padhitha Nal Vayiram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padhitha Nal Vayiram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Natta Nadu Nisiyil Theriyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natta Nadu Nisiyil Theriyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Natchchathirangaladi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natchchathirangaladi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Solai Malar Oliyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solai Malar Oliyo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unadhu Sundhara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unadhu Sundhara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Punnagaidhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Punnagaidhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neela Kadalalaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neela Kadalalaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unadhu Nenjin Alaigaladi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unadhu Nenjin Alaigaladi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kola Kuyil Osai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kola Kuyil Osai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unadhu Kuralin Inimaiyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unadhu Kuralin Inimaiyadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaalai Kumariyadi Kannammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaalai Kumariyadi Kannammaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maruva Kadhal Konden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maruva Kadhal Konden"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saaththiram Pesugiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saaththiram Pesugiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannammaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saaththiram Yedhukkadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saaththiram Yedhukkadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaththiram Kondavarkkey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaththiram Kondavarkkey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saathiram Undodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saathiram Undodi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Moothavar Sammadhiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moothavar Sammadhiyil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vadhuvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadhuvai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muraigal Pinbu Seivom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muraigal Pinbu Seivom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaaththiruppenodi Idhupaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaththiruppenodi Idhupaar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannaththu Muththam Ondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaththu Muththam Ondru"/>
</div>
</pre>
