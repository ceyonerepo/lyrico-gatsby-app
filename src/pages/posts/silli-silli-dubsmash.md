---
title: "silli silli song lyrics"
album: "Dubsmash"
artist: "Vamssih B"
lyricist: "Balavardhan"
director: "Keshav Depur"
path: "/albums/dubsmash-lyrics"
song: "Silli Silli"
image: ../../images/albumart/dubsmash.jpg
date: 2020-01-30
lang: telugu
youtubeLink: "https://www.youtube.com/embed/ZyS-mDe_UGU"
type: "happy"
singers:
  - Lipsika
  - Vamssih
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Silli Silli Gajjala Gurram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silli Silli Gajjala Gurram"/>
</div>
<div class="lyrico-lyrics-wrapper">Sinde Sindhi Soode
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sinde Sindhi Soode"/>
</div>
<div class="lyrico-lyrics-wrapper">Palli Palli Anakaapalli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palli Palli Anakaapalli"/>
</div>
<div class="lyrico-lyrics-wrapper">Raasicchinde Aanaade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raasicchinde Aanaade"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadume Gilli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadume Gilli"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadakaluner pinchade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadakaluner pinchade"/>
</div>
<div class="lyrico-lyrics-wrapper">Sinnode
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sinnode"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Chakkara Selli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Chakkara Selli"/>
</div>
<div class="lyrico-lyrics-wrapper">Madichina Pootha Rekhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madichina Pootha Rekhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Sokhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Sokhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Nogha Nigha Lade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nogha Nigha Lade"/>
</div>
<div class="lyrico-lyrics-wrapper">Batthaayi nenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Batthaayi nenu"/>
</div>
<div class="lyrico-lyrics-wrapper">Avvaayi Chuvvvayi Lantisthade’
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avvaayi Chuvvvayi Lantisthade’"/>
</div>
<div class="lyrico-lyrics-wrapper">Bubbly Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bubbly Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Bubbly Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bubbly Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhille Naa Dhille
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhille Naa Dhille"/>
</div>
<div class="lyrico-lyrics-wrapper">Trouble Ayyindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Trouble Ayyindhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Trouble Ayyindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Trouble Ayyindhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhille Naa Dil Dil Dhille
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhille Naa Dil Dil Dhille"/>
</div>
<div class="lyrico-lyrics-wrapper">Bubbly Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bubbly Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Bubbly Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bubbly Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhille Naa Dhille
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhille Naa Dhille"/>
</div>
<div class="lyrico-lyrics-wrapper">Trouble Ayyindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Trouble Ayyindhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Trouble Ayyindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Trouble Ayyindhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhille Naa Dil Dil Dhille
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhille Naa Dil Dil Dhille"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Touching Lanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Touching Lanni"/>
</div>
<div class="lyrico-lyrics-wrapper">Gicching Layithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gicching Layithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Firing Modhale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Firing Modhale"/>
</div>
<div class="lyrico-lyrics-wrapper">Avuthundhile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avuthundhile"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Looking Lanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Looking Lanni"/>
</div>
<div class="lyrico-lyrics-wrapper">Triking Layyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Triking Layyi"/>
</div>
<div class="lyrico-lyrics-wrapper">Betting Lese
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Betting Lese"/>
</div>
<div class="lyrico-lyrics-wrapper">Bangaarine
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bangaarine"/>
</div>
<div class="lyrico-lyrics-wrapper">Heyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy"/>
</div>
<div class="lyrico-lyrics-wrapper">Maatalanni Moote Katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maatalanni Moote Katti"/>
</div>
<div class="lyrico-lyrics-wrapper">Aatalaadutharaa Neetho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aatalaadutharaa Neetho"/>
</div>
<div class="lyrico-lyrics-wrapper">Potu Gaadiveeraa Swamy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Potu Gaadiveeraa Swamy"/>
</div>
<div class="lyrico-lyrics-wrapper">Verakatthi Theeraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verakatthi Theeraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Come Come Come Come
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Come Come Come Come"/>
</div>
<div class="lyrico-lyrics-wrapper">Vere Lokam Choopistha Neeke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vere Lokam Choopistha Neeke"/>
</div>
<div class="lyrico-lyrics-wrapper">Haaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bubbly Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bubbly Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Bubbly Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bubbly Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhille Naa Dhille
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhille Naa Dhille"/>
</div>
<div class="lyrico-lyrics-wrapper">Trouble Ayyindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Trouble Ayyindhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Trouble Ayyindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Trouble Ayyindhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhille Naa Dil Dil Dhille
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhille Naa Dil Dil Dhille"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bubbly Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bubbly Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Bubbly Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bubbly Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhille Naa Dhille
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhille Naa Dhille"/>
</div>
<div class="lyrico-lyrics-wrapper">Trouble Ayyindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Trouble Ayyindhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Trouble Ayyindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Trouble Ayyindhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhille Naa Dil DilDhille
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhille Naa Dil DilDhille"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Puting Lanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puting Lanni"/>
</div>
<div class="lyrico-lyrics-wrapper">Fitting Layithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fitting Layithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Cutting Licche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cutting Licche"/>
</div>
<div class="lyrico-lyrics-wrapper">Pappaayine
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pappaayine"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Meating lanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meating lanni"/>
</div>
<div class="lyrico-lyrics-wrapper">Datingu layithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Datingu layithe"/>
</div>
<div class="lyrico-lyrics-wrapper">waiting seyanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="waiting seyanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Pootake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Pootake"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa"/>
</div>
<div class="lyrico-lyrics-wrapper">Chedhabaavilona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chedhabaavilona"/>
</div>
<div class="lyrico-lyrics-wrapper">Raaja Yenthamesukoraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raaja Yenthamesukoraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Desivaali Dhaanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Desivaali Dhaanne"/>
</div>
<div class="lyrico-lyrics-wrapper">Bajha Motha Korukodha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bajha Motha Korukodha"/>
</div>
<div class="lyrico-lyrics-wrapper">Cham Cham Cham Cham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cham Cham Cham Cham"/>
</div>
<div class="lyrico-lyrics-wrapper">Istha lancham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Istha lancham"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanne Meppisthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanne Meppisthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Silli Silli Gajjala Gurram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silli Silli Gajjala Gurram"/>
</div>
<div class="lyrico-lyrics-wrapper">Neney Haa Neney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neney Haa Neney"/>
</div>
<div class="lyrico-lyrics-wrapper">Palli PalliAnakaapalli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palli PalliAnakaapalli"/>
</div>
<div class="lyrico-lyrics-wrapper">Naadhe Ha Naadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naadhe Ha Naadhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Silli Silli Gajjala Gurram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silli Silli Gajjala Gurram"/>
</div>
<div class="lyrico-lyrics-wrapper">Neney Haa Neney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neney Haa Neney"/>
</div>
<div class="lyrico-lyrics-wrapper">Palli PalliAnakaapalli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palli PalliAnakaapalli"/>
</div>
<div class="lyrico-lyrics-wrapper">Naadhe Ha Naadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naadhe Ha Naadhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Booralaanti Buggalunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Booralaanti Buggalunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaarapusa Nuvvey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaarapusa Nuvvey"/>
</div>
<div class="lyrico-lyrics-wrapper">Thegalanti Vompulunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thegalanti Vompulunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Bondumalli Nuvvey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bondumalli Nuvvey"/>
</div>
<div class="lyrico-lyrics-wrapper">Kundhanaalu Vandhanaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kundhanaalu Vandhanaalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Prttukunta Neekeee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prttukunta Neekeee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bubbly Baby Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bubbly Baby Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Bubbly Baby Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Bubbly Baby Baby"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bubbly Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bubbly Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhille Naa Dhille
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhille Naa Dhille"/>
</div>
<div class="lyrico-lyrics-wrapper">Trouble Ayyindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Trouble Ayyindhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Trouble Ayyindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Trouble Ayyindhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhille Naa Dil Dil Dhille
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhille Naa Dil Dil Dhille"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bubbly Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bubbly Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhille Naa Dhille
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhille Naa Dhille"/>
</div>
<div class="lyrico-lyrics-wrapper">Trouble Ayyindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Trouble Ayyindhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Trouble Ayyindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Trouble Ayyindhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhille Naa Dil Dil Dhille
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhille Naa Dil Dil Dhille"/>
</div>
</pre>
