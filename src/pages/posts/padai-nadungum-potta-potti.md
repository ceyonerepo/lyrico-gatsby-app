---
title: "padai nadungum song lyrics"
album: "Potta Potti"
artist: "Aruldev"
lyricist: "Jayamurasu - Kevin Shadrach"
director: "Yuvaraj Dhayalan"
path: "/albums/potta-potti-lyrics"
song: "Padai Nadungum"
image: ../../images/albumart/potta-potti.jpg
date: 2011-08-05
lang: tamil
youtubeLink: "https://www.youtube.com/embed/st-uetN_k1c"
type: "devotional"
singers:
  - Pavan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Padai nadungum idi adhirum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padai nadungum idi adhirum"/>
</div>
<div class="lyrico-lyrics-wrapper">sidu sidunnu paarththaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sidu sidunnu paarththaa"/>
</div>
<div class="lyrico-lyrics-wrapper">thadai edhuvum velagidavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thadai edhuvum velagidavey"/>
</div>
<div class="lyrico-lyrics-wrapper">muniyavanum kaappaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muniyavanum kaappaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ellai kaakkum muniyaani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellai kaakkum muniyaani"/>
</div>
<div class="lyrico-lyrics-wrapper">thollai theerkka varuvaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thollai theerkka varuvaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellai kaakkum muniyaani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellai kaakkum muniyaani"/>
</div>
<div class="lyrico-lyrics-wrapper">thollai theerkka varuvaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thollai theerkka varuvaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">moonu kannu veththu vaettaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moonu kannu veththu vaettaa"/>
</div>
<div class="lyrico-lyrics-wrapper">mondha poatten pattaththanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mondha poatten pattaththanni"/>
</div>
<div class="lyrico-lyrics-wrapper">ye paththavachcha suruttu rendu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ye paththavachcha suruttu rendu"/>
</div>
<div class="lyrico-lyrics-wrapper">vettudhadaa rathth thundu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vettudhadaa rathth thundu"/>
</div>
<div class="lyrico-lyrics-wrapper">aththanaiyum padaiyal poattoam saamiyO
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aththanaiyum padaiyal poattoam saamiyO"/>
</div>
<div class="lyrico-lyrics-wrapper">ye maththavaala nallavazhi kaami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ye maththavaala nallavazhi kaami"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maththavaala nallavazhi kaami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maththavaala nallavazhi kaami"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sala salakkum sala salakkum kola nadungum kaettaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sala salakkum sala salakkum kola nadungum kaettaa"/>
</div>
<div class="lyrico-lyrics-wrapper">udu udukka udukka saththam urumivara kaappaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="udu udukka udukka saththam urumivara kaappaan"/>
</div>
<div class="lyrico-lyrics-wrapper">sada sada sadaa muni, sada sada sadaa muni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sada sada sadaa muni, sada sada sadaa muni"/>
</div>
<div class="lyrico-lyrics-wrapper">panjam theertha muniyaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="panjam theertha muniyaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">padaiyeduththu varuvaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="padaiyeduththu varuvaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">veru yaaru sogam paarkka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veru yaaru sogam paarkka"/>
</div>
<div class="lyrico-lyrics-wrapper">vaendivitten varangodukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaendivitten varangodukka"/>
</div>
<div class="lyrico-lyrics-wrapper">uththuraatcha kannu rendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uththuraatcha kannu rendum"/>
</div>
<div class="lyrico-lyrics-wrapper">sutterikkum theemaikkandu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sutterikkum theemaikkandu"/>
</div>
<div class="lyrico-lyrics-wrapper">kuththangorai marandhudaiyaa saamiyOv
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuththangorai marandhudaiyaa saamiyOv"/>
</div>
<div class="lyrico-lyrics-wrapper">en naethikkadan yethukkanum saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en naethikkadan yethukkanum saami"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naethikkadan yethukkanum saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naethikkadan yethukkanum saami"/>
</div>
<div class="lyrico-lyrics-wrapper">Naethikkadan yethukkanum saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naethikkadan yethukkanum saami"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kodala uruvi maalaippoattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodala uruvi maalaippoattu"/>
</div>
<div class="lyrico-lyrics-wrapper">thalaiya vetti kaiyil yendhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalaiya vetti kaiyil yendhi"/>
</div>
<div class="lyrico-lyrics-wrapper">kandandhundam aakkidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kandandhundam aakkidum"/>
</div>
<div class="lyrico-lyrics-wrapper">sada sada sada sadaa muni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sada sada sada sadaa muni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aa kaththum seval saththam kaettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa kaththum seval saththam kaettu"/>
</div>
<div class="lyrico-lyrics-wrapper">oththa mara nezhalula saththiyamaa nirkum saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oththa mara nezhalula saththiyamaa nirkum saami"/>
</div>
<div class="lyrico-lyrics-wrapper">bayabakthi nenjikkulla venummunnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bayabakthi nenjikkulla venummunnu"/>
</div>
<div class="lyrico-lyrics-wrapper">vakkirammaa vizhithirukkum engasaami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vakkirammaa vizhithirukkum engasaami"/>
</div>
<div class="lyrico-lyrics-wrapper">panja boodham aththanaiyum muni avan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="panja boodham aththanaiyum muni avan"/>
</div>
<div class="lyrico-lyrics-wrapper">kaaladiyil mandiyittuppoagum saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaaladiyil mandiyittuppoagum saami"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjikkulla nendhukkittaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjikkulla nendhukkittaa"/>
</div>
<div class="lyrico-lyrics-wrapper">vidhiyaiyum maathivachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidhiyaiyum maathivachu"/>
</div>
<div class="lyrico-lyrics-wrapper">nee tharum kulasaami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee tharum kulasaami"/>
</div>
</pre>
