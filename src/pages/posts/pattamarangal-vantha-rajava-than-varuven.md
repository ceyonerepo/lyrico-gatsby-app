---
title: "pattamarangal song lyrics"
album: "Vantha Rajava Than Varuven"
artist: "Hiphop Tamizha"
lyricist: "Hiphop Tamizha"
director: "Sundar C"
path: "/albums/vantha-rajava-than-varuven-lyrics"
song: "Pattamarangal"
image: ../../images/albumart/vantha-rajava-than-varuven.jpg
date: 2019-02-01
lang: tamil
youtubeLink: "https://www.youtube.com/embed/rr4Yb_YQg2Y"
type: "sad"
singers:
  - Sanjith Hegde
  - Rakshita Suresh
  - Srinidhi S
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">theipirai vaanil kathirena milirum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theipirai vaanil kathirena milirum"/>
</div>
<div class="lyrico-lyrics-wrapper">theemaiyai thudaithidum theeran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theemaiyai thudaithidum theeran"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">theipirai vaanil kathirena milirum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theipirai vaanil kathirena milirum"/>
</div>
<div class="lyrico-lyrics-wrapper">theemaiyai thudaithidum theeran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theemaiyai thudaithidum theeran"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">valvu thunbam varum pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valvu thunbam varum pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">nilai maari vilum pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nilai maari vilum pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">thinanthorum ela vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thinanthorum ela vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">eluvai suriyane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eluvai suriyane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oonagi uyiragi nillayo nilamaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oonagi uyiragi nillayo nilamaga"/>
</div>
<div class="lyrico-lyrics-wrapper">olivangi oliveesum nilavaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="olivangi oliveesum nilavaga"/>
</div>
<div class="lyrico-lyrics-wrapper">nilavaga eluvaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nilavaga eluvaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oonagi uyiragi nillayo nilamaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oonagi uyiragi nillayo nilamaga"/>
</div>
<div class="lyrico-lyrics-wrapper">olivangi oliveesum nilavaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="olivangi oliveesum nilavaga"/>
</div>
<div class="lyrico-lyrics-wrapper">nilavaga eluvaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nilavaga eluvaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sontha bandham yaavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sontha bandham yaavum"/>
</div>
<div class="lyrico-lyrics-wrapper">serthu vaalvathe vaalkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="serthu vaalvathe vaalkai"/>
</div>
<div class="lyrico-lyrics-wrapper">inbam thunbam yavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inbam thunbam yavum"/>
</div>
<div class="lyrico-lyrics-wrapper">serthu sethukirum vaalvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="serthu sethukirum vaalvai"/>
</div>
<div class="lyrico-lyrics-wrapper">vaalvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalvai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">theipirai vaanil kathirena milirum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theipirai vaanil kathirena milirum"/>
</div>
<div class="lyrico-lyrics-wrapper">theemaiyai thudaithidum theeran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theemaiyai thudaithidum theeran"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oonagi uyiragi nillayo nilamaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oonagi uyiragi nillayo nilamaga"/>
</div>
<div class="lyrico-lyrics-wrapper">olivangi oliveesum nilavaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="olivangi oliveesum nilavaga"/>
</div>
<div class="lyrico-lyrics-wrapper">nilavaga eluvaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nilavaga eluvaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oonagi uyiragi nillayo nilamaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oonagi uyiragi nillayo nilamaga"/>
</div>
<div class="lyrico-lyrics-wrapper">olivangi oliveesum nilavaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="olivangi oliveesum nilavaga"/>
</div>
<div class="lyrico-lyrics-wrapper">neerajiyame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neerajiyame"/>
</div>
</pre>
