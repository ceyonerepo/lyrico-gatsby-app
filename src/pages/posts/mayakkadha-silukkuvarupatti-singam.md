---
title: "mayakkadha song lyrics"
album: "Silukkuvarupatti Singam"
artist: "Leon James"
lyricist: "Madhan Karky"
director: "Chella Ayyavu"
path: "/albums/silukkuvarupatti-singam-lyrics"
song: "Mayakkadha"
image: ../../images/albumart/silukkuvarupatti-singam.jpg
date: 2018-12-21
lang: tamil
youtubeLink: "https://www.youtube.com/embed/45a7exVCt84"
type: "love"
singers:
  - Sudharshan Ashok
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Buththi kettu naan nadakken
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Buththi kettu naan nadakken"/>
</div>
<div class="lyrico-lyrics-wrapper">Suththi kittu naan kedakken
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suththi kittu naan kedakken"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru kaaichal ennil paththa vechu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru kaaichal ennil paththa vechu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponaayadii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponaayadii"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mora morappaa morappen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mora morappaa morappen"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalukku orukaa sirippen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalukku orukaa sirippen"/>
</div>
<div class="lyrico-lyrics-wrapper">En moonji fullah pallaa ippo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En moonji fullah pallaa ippo"/>
</div>
<div class="lyrico-lyrics-wrapper">Senjaayadii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senjaayadii"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ei silukkaan kayiru paarvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ei silukkaan kayiru paarvai"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna izhukka naan enna seiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna izhukka naan enna seiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Vella aatta pola un pinnaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vella aatta pola un pinnaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Odi varendii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odi varendii"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey kulfi vandiaatam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey kulfi vandiaatam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee sirichi angittu poga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee sirichi angittu poga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ara touser potta paiyanaattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ara touser potta paiyanaattam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pinna odiyaaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pinna odiyaaren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mayakkadhaaaaeheh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayakkadhaaaaeheh"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayakkadha pulla heh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayakkadha pulla heh"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayakkadha mayakkadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayakkadha mayakkadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Izhukkadha enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Izhukkadha enna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mayakkadhaaaaeheh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayakkadhaaaaeheh"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayakkadha pulla heh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayakkadha pulla heh"/>
</div>
<div class="lyrico-lyrics-wrapper">On per enna solli puttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="On per enna solli puttu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna kolladiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna kolladiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Buththi kettu naan nadakken
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Buththi kettu naan nadakken"/>
</div>
<div class="lyrico-lyrics-wrapper">Suththi kittu naan kedakken
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suththi kittu naan kedakken"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru kaaichal ennil paththa vechu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru kaaichal ennil paththa vechu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponaayadii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponaayadii"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thoonnu pinna maravaa ninnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoonnu pinna maravaa ninnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Onna mattum paarkkudhu kannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna mattum paarkkudhu kannu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thozhi kaadhil kusu kusuvunnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozhi kaadhil kusu kusuvunnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna sonna ponnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna sonna ponnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Munna pinna paarthadhilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munna pinna paarthadhilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Onna pathi kettadhilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna pathi kettadhilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Un azhaga pola oththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un azhaga pola oththa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadiga kuda illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadiga kuda illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">On varalaaru ellaam thevai illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="On varalaaru ellaam thevai illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhirkaalam nee thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhirkaalam nee thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">On neththi thodangi mookkil mudikkavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="On neththi thodangi mookkil mudikkavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Jenmam ezhu venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jenmam ezhu venum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un kudhikaalula poova pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kudhikaalula poova pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasu kedakkudhu inga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu kedakkudhu inga"/>
</div>
<div class="lyrico-lyrics-wrapper">On kuda sernthae odiyaaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="On kuda sernthae odiyaaren"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee siriya sirikkadhaaaaaaaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee siriya sirikkadhaaaaaaaaaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mayakkadhaaaaeheh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayakkadhaaaaeheh"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayakkadha pulla heh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayakkadha pulla heh"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayakkadha mayakkadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayakkadha mayakkadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Izhukkadha enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Izhukkadha enna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mayakkadhaaaaeheh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayakkadhaaaaeheh"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayakkadha pulla heh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayakkadha pulla heh"/>
</div>
<div class="lyrico-lyrics-wrapper">On per enna solli puttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="On per enna solli puttu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna kolladiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna kolladiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Buththi kettu naan nadakken
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Buththi kettu naan nadakken"/>
</div>
<div class="lyrico-lyrics-wrapper">Suththi kittu naan kedakken
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suththi kittu naan kedakken"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru kaaichal ennil paththa vechu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru kaaichal ennil paththa vechu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponaayadii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponaayadii"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mora morappaa morappen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mora morappaa morappen"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalukku orukaa sirippen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalukku orukaa sirippen"/>
</div>
<div class="lyrico-lyrics-wrapper">En moonji fullah pallaa ippo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En moonji fullah pallaa ippo"/>
</div>
<div class="lyrico-lyrics-wrapper">Senjaayadii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senjaayadii"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ei silukkaan kayiru paarvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ei silukkaan kayiru paarvai"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna izhukka naan enna seiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna izhukka naan enna seiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Vella aatta pola un pinnaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vella aatta pola un pinnaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Odi varendii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odi varendii"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey kulfi vandiaatam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey kulfi vandiaatam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee sirichi angittu poga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee sirichi angittu poga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ara touser potta paiyanaattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ara touser potta paiyanaattam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pinna odiyaaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pinna odiyaaren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mayakkadhaaaaeheh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayakkadhaaaaeheh"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayakkadha pulla heh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayakkadha pulla heh"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayakkadha mayakkadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayakkadha mayakkadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Izhukkadha enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Izhukkadha enna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mayakkadhaaaaeheh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayakkadhaaaaeheh"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayakkadha pulla heh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayakkadha pulla heh"/>
</div>
<div class="lyrico-lyrics-wrapper">On per enna solli puttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="On per enna solli puttu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna kolladiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna kolladiyae"/>
</div>
</pre>
