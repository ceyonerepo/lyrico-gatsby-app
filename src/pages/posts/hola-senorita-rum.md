---
title: "hola senorita song lyrics"
album: "Rum"
artist: "Anirudh Ravichander"
lyricist: "Madhan Karky"
director: "Sai Bharath"
path: "/albums/rum-lyrics"
song: "Hola Senorita"
image: ../../images/albumart/rum.jpg
date: 2017-02-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/qzdDkjqXSRA"
type: "happy"
singers:
  - Anirudh Ravichander
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hola Amigo Hola Amigo Hola Senorita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hola Amigo Hola Amigo Hola Senorita"/>
</div>
<div class="lyrico-lyrics-wrapper">Hola Amigo Hola Amigo Hola Senorita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hola Amigo Hola Amigo Hola Senorita"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adutha Theruvil Irukkum Kuppaiku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adutha Theruvil Irukkum Kuppaiku"/>
</div>
<div class="lyrico-lyrics-wrapper">Mooka Nee Mooduriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mooka Nee Mooduriye"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkul Kuvinju Kidakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkul Kuvinju Kidakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuppaiye Marachu Ooduriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuppaiye Marachu Ooduriye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnoda Aaloda Azhaga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnoda Aaloda Azhaga "/>
</div>
<div class="lyrico-lyrics-wrapper">Azhaga Izhuthu Mooduriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhaga Izhuthu Mooduriye"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennoda Aaloda Azhaga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennoda Aaloda Azhaga "/>
</div>
<div class="lyrico-lyrics-wrapper">Rasikka Paakura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rasikka Paakura"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum Ozhungilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Ozhungilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum Ozhungilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum Ozhungilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorum Ozhungilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorum Ozhungilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarum Ozhungilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarum Ozhungilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Vela Unakilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vela Unakilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Neram Enakilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neram Enakilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Adichu Thiruthida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adichu Thiruthida"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum Kanakilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum Kanakilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hola Hola Amigo Hola Senorita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hola Hola Amigo Hola Senorita"/>
</div>
<div class="lyrico-lyrics-wrapper">Hola Hola Amigo Hola Senorita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hola Hola Amigo Hola Senorita"/>
</div>
<div class="lyrico-lyrics-wrapper">Hola Hola Amigo Hola Senorita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hola Hola Amigo Hola Senorita"/>
</div>
<div class="lyrico-lyrics-wrapper">Hola Hola Amigo Senorita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hola Hola Amigo Senorita"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hola Hola Amigo Hola Senorita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hola Hola Amigo Hola Senorita"/>
</div>
<div class="lyrico-lyrics-wrapper">Hola Hola Amigo Hola Senorita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hola Hola Amigo Hola Senorita"/>
</div>
<div class="lyrico-lyrics-wrapper">Hola Hola Amigo Hola Senorita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hola Hola Amigo Hola Senorita"/>
</div>
<div class="lyrico-lyrics-wrapper">Hola Hola Amigo Hola Senorita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hola Hola Amigo Hola Senorita"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaasukaa Avan Oozhal Senje Thinnuvaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasukaa Avan Oozhal Senje Thinnuvaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanaalum Nee Vote Pote Yemathuviye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanaalum Nee Vote Pote Yemathuviye"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaasukaa Avan Match Fix Pannuvaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasukaa Avan Match Fix Pannuvaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanalum Nee TV Paathu Kathuviye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanalum Nee TV Paathu Kathuviye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lanjam Vaangum Cop-ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lanjam Vaangum Cop-ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Helmet Illa Top-ah?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Helmet Illa Top-ah?"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Sollu Yethu Wrong-nu?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Sollu Yethu Wrong-nu?"/>
</div>
<div class="lyrico-lyrics-wrapper">Beep Song ah? Kettu Aadum Gang-ah?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Beep Song ah? Kettu Aadum Gang-ah?"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Sollu Yethu Thappu-nu?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Sollu Yethu Thappu-nu?"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hola Hola Amigo Hola Senorita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hola Hola Amigo Hola Senorita"/>
</div>
<div class="lyrico-lyrics-wrapper">Hola Hola Amigo Hola Senorita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hola Hola Amigo Hola Senorita"/>
</div>
<div class="lyrico-lyrics-wrapper">Hola Hola Amigo Hola Senorita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hola Hola Amigo Hola Senorita"/>
</div>
<div class="lyrico-lyrics-wrapper">Hola Hola Amigo Hola Senorita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hola Hola Amigo Hola Senorita"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hola Hola Amigo Hola Senorita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hola Hola Amigo Hola Senorita"/>
</div>
<div class="lyrico-lyrics-wrapper">Hola Hola Amigo Hola Senorita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hola Hola Amigo Hola Senorita"/>
</div>
<div class="lyrico-lyrics-wrapper">Hola Hola Amigo Hola Senorita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hola Hola Amigo Hola Senorita"/>
</div>
<div class="lyrico-lyrics-wrapper">Hola Hola Amigo Hola Senorita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hola Hola Amigo Hola Senorita"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hola Senorita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hola Senorita"/>
</div>
</pre>
