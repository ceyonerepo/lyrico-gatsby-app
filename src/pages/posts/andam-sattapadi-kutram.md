---
title: "andam song lyrics"
album: "Sattapadi Kutram"
artist: "Vijay Antony"
lyricist: "S.A. Chandrasekhar"
director: "S.A. Chandrasekhar"
path: "/albums/sattapadi-kutram-lyrics"
song: "Andam"
image: ../../images/albumart/sattapadi-kutram.jpg
date: 2011-03-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/an-STiMf03U"
type: "mass"
singers:
  - Senthil
  - Anitha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Andam midhippada boomi podippada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andam midhippada boomi podippada"/>
</div>
<div class="lyrico-lyrics-wrapper">koabathil kan sivakkattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koabathil kan sivakkattum"/>
</div>
<div class="lyrico-lyrics-wrapper">paeimai azhindhida nanmai jeyithida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paeimai azhindhida nanmai jeyithida"/>
</div>
<div class="lyrico-lyrics-wrapper">youtham boomiyil kilambattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="youtham boomiyil kilambattum"/>
</div>
<div class="lyrico-lyrics-wrapper">moththa peigalum mannil saaindhida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moththa peigalum mannil saaindhida"/>
</div>
<div class="lyrico-lyrics-wrapper">vettai seekkiram thodangattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vettai seekkiram thodangattum"/>
</div>
<div class="lyrico-lyrics-wrapper">kettammanidhan nam kaiyaaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kettammanidhan nam kaiyaaley"/>
</div>
<div class="lyrico-lyrics-wrapper">indrey seththu ozhiyattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indrey seththu ozhiyattum"/>
</div>
<div class="lyrico-lyrics-wrapper">kutramillai Indhu kutramilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kutramillai Indhu kutramilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Om arimvoam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Om arimvoam"/>
</div>
<div class="lyrico-lyrics-wrapper">Om arimvoam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Om arimvoam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Murugan sooranai kondraaraam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Murugan sooranai kondraaraam"/>
</div>
<div class="lyrico-lyrics-wrapper">Manikandan magizhchiyai azhithaaraam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manikandan magizhchiyai azhithaaraam"/>
</div>
<div class="lyrico-lyrics-wrapper">indha arakkan koottathai azhippadharku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indha arakkan koottathai azhippadharku"/>
</div>
<div class="lyrico-lyrics-wrapper">andha kadavul varuvaaraam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="andha kadavul varuvaaraam"/>
</div>
<div class="lyrico-lyrics-wrapper">azhikkum vazhiyil elumbai muri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="azhikkum vazhiyil elumbai muri"/>
</div>
<div class="lyrico-lyrics-wrapper">un aduppil vaiththu avanai eri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un aduppil vaiththu avanai eri"/>
</div>
<div class="lyrico-lyrics-wrapper">kutram seidhavan kudalai uruvi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kutram seidhavan kudalai uruvi"/>
</div>
<div class="lyrico-lyrics-wrapper">veettu kooraiyil thookki eri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veettu kooraiyil thookki eri"/>
</div>
<div class="lyrico-lyrics-wrapper">kolaigaaranai kondraal kutrum illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kolaigaaranai kondraal kutrum illai"/>
</div>
<div class="lyrico-lyrics-wrapper">thappu seidhavanai thandippadhil thappey illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thappu seidhavanai thandippadhil thappey illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Om arivoam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Om arivoam"/>
</div>
<div class="lyrico-lyrics-wrapper">Om arivoam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Om arivoam"/>
</div>
</pre>
