---
title: "mozhiyellam mounam song lyrics"
album: "Yaaro"
artist: "Jose Franklin"
lyricist: "Adithya Suresh"
director: "Sandeep Sai"
path: "/albums/yaaro-lyrics"
song: "Mozhiyellam Mounam"
image: ../../images/albumart/yaaro.jpg
date: 2022-02-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/AqjaTSFWFQA"
type: "love"
singers:
  - Karthik
  - MM Manasi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">mozhiyellam mounam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mozhiyellam mounam"/>
</div>
<div class="lyrico-lyrics-wrapper">vizhi pesum naanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vizhi pesum naanam"/>
</div>
<div class="lyrico-lyrics-wrapper">kanaakal malaruthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanaakal malaruthe"/>
</div>
<div class="lyrico-lyrics-wrapper">un kathin ooram 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kathin ooram "/>
</div>
<div class="lyrico-lyrics-wrapper">naan kathai pesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan kathai pesa"/>
</div>
<div class="lyrico-lyrics-wrapper">vinaakal thodaruthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vinaakal thodaruthe"/>
</div>
<div class="lyrico-lyrics-wrapper">en uyir pallavi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en uyir pallavi"/>
</div>
<div class="lyrico-lyrics-wrapper">adiye nee thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adiye nee thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">naan yengum thaayum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan yengum thaayum"/>
</div>
<div class="lyrico-lyrics-wrapper">thai madiyum nee than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thai madiyum nee than"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjoram vaalum nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjoram vaalum nee"/>
</div>
<div class="lyrico-lyrics-wrapper">kathal thee thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathal thee thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">ival vaanil nee 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ival vaanil nee "/>
</div>
<div class="lyrico-lyrics-wrapper">matum thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="matum thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">ennavane ennavane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennavane ennavane"/>
</div>
<div class="lyrico-lyrics-wrapper">pen idai pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pen idai pol"/>
</div>
<div class="lyrico-lyrics-wrapper">alikiraai anaikiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alikiraai anaikiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">inneram thodarumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inneram thodarumo"/>
</div>
<div class="lyrico-lyrics-wrapper">ennuyire ennuyire 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennuyire ennuyire "/>
</div>
<div class="lyrico-lyrics-wrapper">vennilave oh oh 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vennilave oh oh "/>
</div>
<div class="lyrico-lyrics-wrapper">un idhalgalil vilipathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un idhalgalil vilipathai"/>
</div>
<div class="lyrico-lyrics-wrapper">un kaalai thadukumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kaalai thadukumo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">penne nee thalai saaithale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="penne nee thalai saaithale"/>
</div>
<div class="lyrico-lyrics-wrapper">koonthal nee sari seithaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koonthal nee sari seithaale"/>
</div>
<div class="lyrico-lyrics-wrapper">ennule etho maatrangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennule etho maatrangal"/>
</div>
<div class="lyrico-lyrics-wrapper">naan aetruk konden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan aetruk konden"/>
</div>
<div class="lyrico-lyrics-wrapper">selai kalainthidum pon velai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="selai kalainthidum pon velai"/>
</div>
<div class="lyrico-lyrics-wrapper">maalai poluthugal nam porvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maalai poluthugal nam porvai"/>
</div>
<div class="lyrico-lyrics-wrapper">kaalai maalai un paarvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaalai maalai un paarvai"/>
</div>
<div class="lyrico-lyrics-wrapper">nan vendum enben
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan vendum enben"/>
</div>
<div class="lyrico-lyrics-wrapper">kalliye kolluthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalliye kolluthe"/>
</div>
<div class="lyrico-lyrics-wrapper">ennai matum malai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennai matum malai"/>
</div>
<div class="lyrico-lyrics-wrapper">indru nanaikuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indru nanaikuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">kathale karaiyuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathale karaiyuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">ennai thitum pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennai thitum pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">unnai pidikuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai pidikuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">pan nathiye en rathiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pan nathiye en rathiye"/>
</div>
<div class="lyrico-lyrics-wrapper">kanaakal malaruthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanaakal malaruthe"/>
</div>
<div class="lyrico-lyrics-wrapper">manthirane mannavane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manthirane mannavane"/>
</div>
<div class="lyrico-lyrics-wrapper">vinaakal thodaruthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vinaakal thodaruthe"/>
</div>
<div class="lyrico-lyrics-wrapper">ennavane ennavane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennavane ennavane"/>
</div>
<div class="lyrico-lyrics-wrapper">pen irunthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pen irunthu"/>
</div>
<div class="lyrico-lyrics-wrapper">alaikiraai anaikiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alaikiraai anaikiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">inneram thodarumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inneram thodarumo"/>
</div>
<div class="lyrico-lyrics-wrapper">ennuyire ennuyire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennuyire ennuyire"/>
</div>
<div class="lyrico-lyrics-wrapper">vennilave oh oh oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vennilave oh oh oh"/>
</div>
<div class="lyrico-lyrics-wrapper">un ithalgalil vilipathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un ithalgalil vilipathai"/>
</div>
<div class="lyrico-lyrics-wrapper">un kaalai thadukumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kaalai thadukumo"/>
</div>
</pre>
