---
title: "chinni chinni song lyrics"
album: "Rakshasudu"
artist: "Ghibran"
lyricist: "Shree Mani"
director: "Ramesh Varma"
path: "/albums/rakshasudu-lyrics"
song: "Chinni Chinni"
image: ../../images/albumart/rakshasudu.jpg
date: 2019-08-08
lang: telugu
youtubeLink: "https://www.youtube.com/embed/lEKdHUnRHMc"
type: "happy"
singers:
  - Sid Sriram
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Chinni chinni chinukulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinni chinni chinukulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadipenu manase
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadipenu manase"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinni chinni gurthulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinni chinni gurthulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gundelona kurise
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundelona kurise"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinni chinni sangathulni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinni chinni sangathulni"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogu chese vayase
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogu chese vayase"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinni chinni parugula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinni chinni parugula"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeramedho thelise
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeramedho thelise"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa gundelo neeko gadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa gundelo neeko gadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aakashame dhaache nadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aakashame dhaache nadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa kallalo nee oohala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa kallalo nee oohala"/>
</div>
<div class="lyrico-lyrics-wrapper">Pravahamai pothunnadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pravahamai pothunnadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naadho kshanam needho kshanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naadho kshanam needho kshanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Yevaipu saagedhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yevaipu saagedhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chinni chinni chinukulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinni chinni chinukulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadipenu manase
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadipenu manase"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinni chinni gurthulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinni chinni gurthulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gundelona kurise
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundelona kurise"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vethukkannane ninna kalane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vethukkannane ninna kalane"/>
</div>
<div class="lyrico-lyrics-wrapper">Repati oohake vellaneney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Repati oohake vellaneney"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee chinni gnaapakaala varshaalalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee chinni gnaapakaala varshaalalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa gamyamemitante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa gamyamemitante"/>
</div>
<div class="lyrico-lyrics-wrapper">Yevaipu choopalile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yevaipu choopalile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chinni chinni chinukulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinni chinni chinukulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadipenu manase
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadipenu manase"/>
</div>
(<div class="lyrico-lyrics-wrapper">Chinni chinni chinukulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinni chinni chinukulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadipenu manase)
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadipenu manase)"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinni chinni gurthulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinni chinni gurthulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gundelona kurise
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundelona kurise"/>
</div>
(<div class="lyrico-lyrics-wrapper">Chinni chinni gurthulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinni chinni gurthulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gundelona kurise)
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundelona kurise)"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinni chinni sangathulni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinni chinni sangathulni"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogu chese vayase
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogu chese vayase"/>
</div>
(<div class="lyrico-lyrics-wrapper">Chinni chinni sangathulni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinni chinni sangathulni"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogu chese vayase)
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogu chese vayase)"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinni chinni parugula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinni chinni parugula"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeramedho thelise
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeramedho thelise"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa gundelo neeko gadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa gundelo neeko gadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aakashame dhaache nadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aakashame dhaache nadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa kallalo nee oohala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa kallalo nee oohala"/>
</div>
<div class="lyrico-lyrics-wrapper">Pravahamai pothunnadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pravahamai pothunnadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naadho kshanam needho kshanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naadho kshanam needho kshanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Yevaipu saagedhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yevaipu saagedhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chinni chinni chinukulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinni chinni chinukulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadipenu manase
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadipenu manase"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinni chinni gurthulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinni chinni gurthulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gundelona kurise
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundelona kurise"/>
</div>
</pre>
