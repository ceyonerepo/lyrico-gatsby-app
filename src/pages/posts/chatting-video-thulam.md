---
title: "chatting video song lyrics"
album: "Thulam"
artist: "Alex Premnath"
lyricist: "Na Muthu Kumar"
director: "Rajanagajothi"
path: "/albums/thulam-lyrics"
song: "Chatting Video"
image: ../../images/albumart/thulam.jpg
date: 2018-12-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/0x1ZLGcUHDo"
type: "love"
singers:
  - Jai Prakash
  - Suresh
  - Padma Shankar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Facebook chattingilum iruppom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Facebook chattingilum iruppom"/>
</div>
<div class="lyrico-lyrics-wrapper">Friendaai uyirai kuda koduppom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friendaai uyirai kuda koduppom"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaai pol dhesaththai madhippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaai pol dhesaththai madhippom"/>
</div>
<div class="lyrico-lyrics-wrapper">Thee pol theemaigalai erippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thee pol theemaigalai erippom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhvae oru college
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvae oru college"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharumae pala knowledge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharumae pala knowledge"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Facebook chattingilum iruppom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Facebook chattingilum iruppom"/>
</div>
<div class="lyrico-lyrics-wrapper">Friendaai uyirai kuda koduppom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friendaai uyirai kuda koduppom"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaai pol dhesaththai madhippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaai pol dhesaththai madhippom"/>
</div>
<div class="lyrico-lyrics-wrapper">Thee pol theemaigalai erippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thee pol theemaigalai erippom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pengalai deivamaai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pengalai deivamaai "/>
</div>
<div class="lyrico-lyrics-wrapper">vanangidum naadithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanangidum naadithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Eve teasing seidhida 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eve teasing seidhida "/>
</div>
<div class="lyrico-lyrics-wrapper">vida maattom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vida maattom"/>
</div>
<div class="lyrico-lyrics-wrapper">Saga thozhikku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saga thozhikku "/>
</div>
<div class="lyrico-lyrics-wrapper">vedhanai thara maattom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vedhanai thara maattom"/>
</div>
<div class="lyrico-lyrics-wrapper">Vettriyum tholvizhiyum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vettriyum tholvizhiyum "/>
</div>
<div class="lyrico-lyrics-wrapper">vaazhkkayil vendumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaazhkkayil vendumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Crickettil thoattraal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Crickettil thoattraal "/>
</div>
<div class="lyrico-lyrics-wrapper">azha maattom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="azha maattom"/>
</div>
<div class="lyrico-lyrics-wrapper">Naangal bet kattum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naangal bet kattum "/>
</div>
<div class="lyrico-lyrics-wrapper">pazhakathil vizha maattom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pazhakathil vizha maattom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theruvil kuppai poda maattom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theruvil kuppai poda maattom"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatrai pollution panna maattom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatrai pollution panna maattom"/>
</div>
<div class="lyrico-lyrics-wrapper">Lanjam kettaal thara maattom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lanjam kettaal thara maattom"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanja pugazhchi panna maattom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanja pugazhchi panna maattom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pasiyena orr uyir thudikkaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pasiyena orr uyir thudikkaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhaikku uthavida thozha vaa vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhaikku uthavida thozha vaa vaa vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunuindhae vaa ilam puyalaai vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunuindhae vaa ilam puyalaai vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Facebook chattingilum iruppom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Facebook chattingilum iruppom"/>
</div>
<div class="lyrico-lyrics-wrapper">Friendaai uyirai kuda koduppom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friendaai uyirai kuda koduppom"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaai pol dhesaththai madhippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaai pol dhesaththai madhippom"/>
</div>
<div class="lyrico-lyrics-wrapper">Thee pol theemaigalai erippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thee pol theemaigalai erippom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhvae oru college
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvae oru college"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharumae pala knowledge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharumae pala knowledge"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kangalai izhanthavar 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangalai izhanthavar "/>
</div>
<div class="lyrico-lyrics-wrapper">saalaiyai kadanthida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saalaiyai kadanthida"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai viral pidithae udhaviduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai viral pidithae udhaviduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Naangal paravaikkum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naangal paravaikkum "/>
</div>
<div class="lyrico-lyrics-wrapper">pasiththaal unaviduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pasiththaal unaviduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Kulandhaigal siruvargal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulandhaigal siruvargal "/>
</div>
<div class="lyrico-lyrics-wrapper">velaikku selvadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="velaikku selvadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Anaivarkkum kalviyai alithiduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anaivarkkum kalviyai alithiduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru kudaiyinil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru kudaiyinil "/>
</div>
<div class="lyrico-lyrics-wrapper">ulagathai adaithiduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulagathai adaithiduvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iyarkkai thaaiyai kaathiduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iyarkkai thaaiyai kaathiduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Idi mazhai puyalaa udhaviduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idi mazhai puyalaa udhaviduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilaththai virkkum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilaththai virkkum "/>
</div>
<div class="lyrico-lyrics-wrapper">nilai thodarnthaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nilai thodarnthaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallum mannum unavaagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallum mannum unavaagum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jananamum oru murai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jananamum oru murai"/>
</div>
<div class="lyrico-lyrics-wrapper">Maranamum oru murai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maranamum oru murai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhkkaiyai pala 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkkaiyai pala "/>
</div>
<div class="lyrico-lyrics-wrapper">murai vaazhnthiduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="murai vaazhnthiduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Saga uyirgalidam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saga uyirgalidam "/>
</div>
<div class="lyrico-lyrics-wrapper">anbu seluthiduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anbu seluthiduvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Facebook chattingilum iruppom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Facebook chattingilum iruppom"/>
</div>
<div class="lyrico-lyrics-wrapper">Friendaai uyirai kuda koduppom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friendaai uyirai kuda koduppom"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaai pol dhesaththai madhippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaai pol dhesaththai madhippom"/>
</div>
<div class="lyrico-lyrics-wrapper">Thee pol theemaigalai erippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thee pol theemaigalai erippom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhvae  oru college
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvae  oru college"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharumae pala knowledge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharumae pala knowledge"/>
</div>
</pre>
