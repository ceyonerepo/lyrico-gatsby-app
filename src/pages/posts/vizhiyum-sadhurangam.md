---
title: "vizhiyum song lyrics"
album: "Sadhurangam"
artist: "Vidyasagar"
lyricist: "Arivumathi"
director: "Karu Pazhaniappan"
path: "/albums/sadhurangam-lyrics"
song: "Vizhiyum"
image: ../../images/albumart/sadhurangam.jpg
date: 2011-10-08
lang: tamil
youtubeLink: "https://www.youtube.com/embed/YMgRycMSL_w"
type: "love"
singers:
  - Harini
  - Madhu Balakrishnan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">vizhiyum vizhiyum nerungum pozhuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vizhiyum vizhiyum nerungum pozhuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">valayal virumbi norungum pozhuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valayal virumbi norungum pozhuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">vasathiyaagha vasathiyaagha valainthu kodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vasathiyaagha vasathiyaagha valainthu kodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ithazhum ithazhum inayum pozhuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithazhum ithazhum inayum pozhuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">imayil nilavu nuzhayum pozhuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="imayil nilavu nuzhayum pozhuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">vasathiyaagha vasathiyaagha valainthu kodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vasathiyaagha vasathiyaagha valainthu kodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaathalinaal kaathal thottuvidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaathalinaal kaathal thottuvidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aathalinaal naanam vittu vidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aathalinaal naanam vittu vidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vizhiyum vizhiyum nerungum pozhuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vizhiyum vizhiyum nerungum pozhuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">valayal virumbi norungum pozhuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valayal virumbi norungum pozhuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vasathiyaagha vasathiyaagha valainthu kodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vasathiyaagha vasathiyaagha valainthu kodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">muththamondru thanthavudan moodikkollum kangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muththamondru thanthavudan moodikkollum kangal"/>
</div>
<div class="lyrico-lyrics-wrapper">moththamaagha koonthal alli moodikollum poikal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moththamaagha koonthal alli moodikollum poikal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">udalirangi neenthum ennai uyir izhuthu sellum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="udalirangi neenthum ennai uyir izhuthu sellum"/>
</div>
<div class="lyrico-lyrics-wrapper">oayvu thantha kaaranathaal udaikal nandri sollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oayvu thantha kaaranathaal udaikal nandri sollum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">viralum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viralum"/>
</div>
<div class="lyrico-lyrics-wrapper">viralum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viralum"/>
</div>
<div class="lyrico-lyrics-wrapper">irakum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irakum"/>
</div>
<div class="lyrico-lyrics-wrapper">pozhuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pozhuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">modhugil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="modhugil"/>
</div>
<div class="lyrico-lyrics-wrapper">suvaril
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suvaril"/>
</div>
<div class="lyrico-lyrics-wrapper">vazhiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vazhiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">vizhuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vizhuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">viralum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viralum"/>
</div>
<div class="lyrico-lyrics-wrapper">viralum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viralum"/>
</div>
<div class="lyrico-lyrics-wrapper">irakum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irakum"/>
</div>
<div class="lyrico-lyrics-wrapper">pozhuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pozhuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">modhugil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="modhugil"/>
</div>
<div class="lyrico-lyrics-wrapper">suvaril
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suvaril"/>
</div>
<div class="lyrico-lyrics-wrapper">vazhiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vazhiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">vizhuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vizhuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">urangidaamal urangidaamal kirangi vidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="urangidaamal urangidaamal kirangi vidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ithazhum ithazhum inayum pozhuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithazhum ithazhum inayum pozhuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">imayil nilavu nuzhayum pozhuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="imayil nilavu nuzhayum pozhuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vasathiyaagha vasathiyaagha valainthu kodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vasathiyaagha vasathiyaagha valainthu kodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">puyal mudinthu poana pinney kadal uranga sellum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puyal mudinthu poana pinney kadal uranga sellum"/>
</div>
<div class="lyrico-lyrics-wrapper">kan vizhitha alai thirumba kalam iranga sollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kan vizhitha alai thirumba kalam iranga sollum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">uyri anukkal koodi nindru oasai indri killum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyri anukkal koodi nindru oasai indri killum"/>
</div>
<div class="lyrico-lyrics-wrapper">oru nodikkul nooru murai meththai ingu thullum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru nodikkul nooru murai meththai ingu thullum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">imayin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="imayin"/>
</div>
<div class="lyrico-lyrics-wrapper">mudiyaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudiyaal"/>
</div>
<div class="lyrico-lyrics-wrapper">udalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="udalai"/>
</div>
<div class="lyrico-lyrics-wrapper">ulavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">ilamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ilamai"/>
</div>
<div class="lyrico-lyrics-wrapper">vayalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vayalil"/>
</div>
<div class="lyrico-lyrics-wrapper">puyalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puyalai"/>
</div>
<div class="lyrico-lyrics-wrapper">nadavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nadavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">imayin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="imayin"/>
</div>
<div class="lyrico-lyrics-wrapper">mudiyaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudiyaal"/>
</div>
<div class="lyrico-lyrics-wrapper">udalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="udalai"/>
</div>
<div class="lyrico-lyrics-wrapper">ulavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">ilamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ilamai"/>
</div>
<div class="lyrico-lyrics-wrapper">vayalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vayalil"/>
</div>
<div class="lyrico-lyrics-wrapper">puyalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puyalai"/>
</div>
<div class="lyrico-lyrics-wrapper">nadavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nadavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">isaithidaamal isaithidaamal moochu vidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="isaithidaamal isaithidaamal moochu vidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaathalinaal kaathal thottuvidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaathalinaal kaathal thottuvidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aathalinaal naanam vittu vidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aathalinaal naanam vittu vidu"/>
</div>
</pre>
