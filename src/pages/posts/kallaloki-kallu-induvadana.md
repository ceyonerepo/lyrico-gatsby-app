---
title: "kallaloki kallu song lyrics"
album: "Induvadana"
artist: "Shiva Kakani"
lyricist: "Bhaskara Bhatla"
director: "MSR"
path: "/albums/induvadana-lyrics"
song: "Kallaloki Kallu"
image: ../../images/albumart/induvadana.jpg
date: 2022-01-01
lang: telugu
youtubeLink: "https://www.youtube.com/embed/KBBflpxklWg"
type: "love"
singers:
  - SP Charan
  - Sahithi Chaganti
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kallalloki Kallu Petti Chudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallalloki Kallu Petti Chudu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gundelloki Dhooripoyi Choodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundelloki Dhooripoyi Choodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalo Ninnu Thongi Thongi Choodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalo Ninnu Thongi Thongi Choodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Haayi Haayigaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haayi Haayigaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Chempallona Siggunadigi Choodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chempallona Siggunadigi Choodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Muddhullona Vedinadigi Choodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muddhullona Vedinadigi Choodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninne Naalo Gurthupatti Choodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninne Naalo Gurthupatti Choodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanivi Theeraga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanivi Theeraga"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu Choodu Choodu Ante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu Choodu Choodu Ante"/>
</div>
<div class="lyrico-lyrics-wrapper">Manase Aagadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manase Aagadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu Choodakunda Unte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu Choodakunda Unte"/>
</div>
<div class="lyrico-lyrics-wrapper">Emi Thochadhe Asalemi Thochadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emi Thochadhe Asalemi Thochadhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kallalloki Kallu Petti Chudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallalloki Kallu Petti Chudu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gundelloki Dhooripoyi Choodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundelloki Dhooripoyi Choodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalo Ninnu Thongi Thongi Choodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalo Ninnu Thongi Thongi Choodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Haayi Haayigaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haayi Haayigaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Guruthaina Ledhu Kadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Guruthaina Ledhu Kadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu Leni Jeevitham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu Leni Jeevitham"/>
</div>
<div class="lyrico-lyrics-wrapper">Marupaina Raavu Kadaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marupaina Raavu Kadaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Okka Nimisham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okka Nimisham"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Raakathone Kadaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Raakathone Kadaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Maaripoye Jaathakam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaripoye Jaathakam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Thodulone Kadhaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Thodulone Kadhaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Nenu Navvadam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenu Navvadam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ee Prema Jeevanadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Prema Jeevanadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Iddharamu Kalisi Eedhudhaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iddharamu Kalisi Eedhudhaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye Kannu Choodaleni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Kannu Choodaleni"/>
</div>
<div class="lyrico-lyrics-wrapper">Kotthalokam Kalisi Vethukudhaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kotthalokam Kalisi Vethukudhaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Korikedho Baagunnadhi Kotthaga Unnadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Korikedho Baagunnadhi Kotthaga Unnadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pichhi Premedho Premedho 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pichhi Premedho Premedho "/>
</div>
<div class="lyrico-lyrics-wrapper">Andhutlo Dhaagunnadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhutlo Dhaagunnadhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kallalloki Kallu Petti Chudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallalloki Kallu Petti Chudu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gundelloki Dhooripoyi Choodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundelloki Dhooripoyi Choodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalo Ninnu Thongi Thongi Choodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalo Ninnu Thongi Thongi Choodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Haayi Haayigaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haayi Haayigaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aanandamekkaduna Jalladesi Pattanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanandamekkaduna Jalladesi Pattanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Kaali Muvvalaaga Thechhi Kattanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kaali Muvvalaaga Thechhi Kattanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Theneegalekkadunnaa Ventapadi Adaganaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theneegalekkadunnaa Ventapadi Adaganaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Theepi Addhukoni Muddhulettanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Theepi Addhukoni Muddhulettanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nuvvante Induvale Andhuvale Naaku Ishtame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvante Induvale Andhuvale Naaku Ishtame"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvintha Aasha Petti Champuthunte Adducheppane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvintha Aasha Petti Champuthunte Adducheppane"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannu Vachhi Allesuko Patti Laagesuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannu Vachhi Allesuko Patti Laagesuko"/>
</div>
<div class="lyrico-lyrics-wrapper">Nindi Noorellu Noorellu Neelone Dhaachesuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nindi Noorellu Noorellu Neelone Dhaachesuko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kallalloki Kallu Petti Chudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallalloki Kallu Petti Chudu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gundelloki Dhooripoyi Choodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundelloki Dhooripoyi Choodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalo Ninnu Thongi Thongi Choodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalo Ninnu Thongi Thongi Choodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Haayi Haayigaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haayi Haayigaa"/>
</div>
</pre>
