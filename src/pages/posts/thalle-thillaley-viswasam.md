---
title: 'thalle thillaaley song lyrics'
album: 'Viswasam'
artist: 'D Imman'
lyricist: 'Arunbharathi'
director: 'Siva'
path: '/albums/viswasam-song-lyrics'
song: 'Thalle Thillaaley'
image: ../../images/albumart/viswasam.jpg
date: 2019-01-10
lang: tamil
singers:
- Anthony Dasan
youtubeLink: "https://www.youtube.com/embed/_pie-kqNR2I"
type: 'folk'
---



<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">

<div class="lyrico-lyrics-wrapper">Hummchachukkum hum chikkaan</div>
<div class="lyrico-lyrics-wrapper">Hummchachukkum hum chikkaan</div>
<div class="lyrico-lyrics-wrapper">Hummchachukkum hum chikkaan</div>
<div class="lyrico-lyrics-wrapper">Hummchachukkum hum chikkaan</div>
<span class="lyrico-song-lyrics-breaker"></span>

<div class="lyrico-lyrics-wrapper">Hummchachukkum hum chikkaan</div>
<div class="lyrico-lyrics-wrapper">Hummchachukkum hum chikkaan</div>
<div class="lyrico-lyrics-wrapper">Hummchachukkum hum chikkaan</div>
<div class="lyrico-lyrics-wrapper">Hummchachukkum hum chikkaan (Overlapping)</div>
<span class="lyrico-song-lyrics-breaker"></span>

<div class="lyrico-lyrics-wrapper">Thallae thillaalae</div>
<div class="lyrico-lyrics-wrapper">Thallae thillaalae</div>
<div class="lyrico-lyrics-wrapper">Thallae thillaalae</div>
<div class="lyrico-lyrics-wrapper">Thallae thillaalae</div>
<span class="lyrico-song-lyrics-breaker"></span>

<div class="lyrico-lyrics-wrapper">Hummchachukkum hum chikkaan</div>
<div class="lyrico-lyrics-wrapper">Hummchachukkum hum chikkaan</div>
<div class="lyrico-lyrics-wrapper">Hummchachukkum hum chikkaan</div>
<div class="lyrico-lyrics-wrapper">Hummchachukkum hum chikkaan (Overlapping)</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-status-image-wrapper">
<div class="lyrico-lyrics-wrapper">
<span class="material-icons lyrico-download-statu-image">cloud_download</span>
</div>
<div class="lyrico-lyrics-wrapper">Nellu kattu sumakkum pulla</div>
<div class="lyrico-lyrics-wrapper">Nenja katti izhukkum pulla</div>
<div class="lyrico-lyrics-wrapper">Nellu kattu sumakkum pulla</div>
<div class="lyrico-lyrics-wrapper">Nenja katti izhukkum pulla</div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>

<div class="lyrico-lyrics-wrapper">Hummchachukkum hum chikkaan</div>
<div class="lyrico-lyrics-wrapper">Hummchachukkum hum chikkaan</div>
<div class="lyrico-lyrics-wrapper">Hummchachukkum hum chikkaan</div>
<div class="lyrico-lyrics-wrapper">Hummchachukkum hum chikkaan (Overlapping)</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-status-image-wrapper">
<div class="lyrico-lyrics-wrapper">
<span class="material-icons lyrico-download-statu-image">cloud_download</span>
</div>
<div class="lyrico-lyrics-wrapper">Suthi muthi yaarumilla</div>
<div class="lyrico-lyrics-wrapper">Thallae thillaalae</div>
<div class="lyrico-lyrics-wrapper">Nee soosagamaa vaadi pulla</div>
<div class="lyrico-lyrics-wrapper">Thallae thillaalae</div>
<div class="lyrico-lyrics-wrapper">Nee soosagamaa vaadi pulla</div>
<div class="lyrico-lyrics-wrapper">Thallae thillaalae</div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>

<div class="lyrico-lyrics-wrapper">Thallae thillaalae</div>
<div class="lyrico-lyrics-wrapper">Thallae thillaalae</div>
<div class="lyrico-lyrics-wrapper">Thallae thillaalae</div>
<div class="lyrico-lyrics-wrapper">Thallae thillaalae</div>
<span class="lyrico-song-lyrics-breaker"></span>

<div class="lyrico-lyrics-wrapper">Hummchachukkum hum chikkaan</div>
<div class="lyrico-lyrics-wrapper">Hummchachukkum hum chikkaan</div>
<div class="lyrico-lyrics-wrapper">Hummchachukkum hum chikkaan</div>
<div class="lyrico-lyrics-wrapper">Hummchachukkum hum chikkaan (Overlapping)</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-status-image-wrapper">
<div class="lyrico-lyrics-wrapper">
<span class="material-icons lyrico-download-statu-image">cloud_download</span>
</div>
<div class="lyrico-lyrics-wrapper">Kannangaru karutha machaan</div>
<div class="lyrico-lyrics-wrapper">Kaikku valaiyal potta machaan</div>
<div class="lyrico-lyrics-wrapper">Kannangaru karutha machaan</div>
<div class="lyrico-lyrics-wrapper">Kaikku valaiyal potta machaan</div>
<span class="lyrico-song-lyrics-breaker"></span>

<div class="lyrico-lyrics-wrapper">Panna aruva pudichirukken</div>
<div class="lyrico-lyrics-wrapper">Thallae thillaalae</div>
<div class="lyrico-lyrics-wrapper">Un paasaangu thaan palikkaathu</div>
<div class="lyrico-lyrics-wrapper">Thallae thillaalae</div>
<div class="lyrico-lyrics-wrapper">Un paasaangu thaan palikkaathu</div>
<div class="lyrico-lyrics-wrapper">Thallae thillaalae</div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>

<div class="lyrico-lyrics-wrapper">Hummchachukkum hum chikkaan</div>
<div class="lyrico-lyrics-wrapper">Hummchachukkum hum chikkaan</div>
<div class="lyrico-lyrics-wrapper">Hummchachukkum hum chikkaan</div>
<div class="lyrico-lyrics-wrapper">Hummchachukkum hum chikkaan (Overlapping)</div>
<span class="lyrico-song-lyrics-breaker"></span>

<div class="lyrico-lyrics-wrapper">Thallae thillaalae</div>
<div class="lyrico-lyrics-wrapper">Thallae thillaalae</div>
<div class="lyrico-lyrics-wrapper">Thallae thillaalae</div>
<div class="lyrico-lyrics-wrapper">Thallae thillaalae</div>
<span class="lyrico-song-lyrics-breaker"></span>

<div class="lyrico-lyrics-wrapper">Hummchachukkum hum chikkaan</div>
<div class="lyrico-lyrics-wrapper">Hummchachukkum hum chikkaan</div>
<div class="lyrico-lyrics-wrapper">Hummchachukkum hum chikkaan</div>
<div class="lyrico-lyrics-wrapper">Hummchachukkum hum chikkaan (Overlapping)</div>
<span class="lyrico-song-lyrics-breaker"></span>

<div class="lyrico-lyrics-wrapper">Kottaaru thoppukulla</div>
<div class="lyrico-lyrics-wrapper">Motor-u room-u kulla</div>
<div class="lyrico-lyrics-wrapper">Hey kottaaru thoppukulla</div>
<div class="lyrico-lyrics-wrapper">Motor-u room-u kulla</div>
<span class="lyrico-song-lyrics-breaker"></span>

<div class="lyrico-lyrics-wrapper">Hummchachukkum hum chikkaan</div>
<div class="lyrico-lyrics-wrapper">Hummchachukkum hum chikkaan</div>
<div class="lyrico-lyrics-wrapper">Hummchachukkum hum chikkaan</div>
<div class="lyrico-lyrics-wrapper">Hummchachukkum hum chikkaan (Overlapping)</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-status-image-wrapper">
<div class="lyrico-lyrics-wrapper">
<span class="material-icons lyrico-download-statu-image">cloud_download</span>
</div>
<div class="lyrico-lyrics-wrapper">Kaattaaru pola varen</div>
<div class="lyrico-lyrics-wrapper">Thallae thillaalae</div>
<div class="lyrico-lyrics-wrapper">Oru kappal ootta neeyum vaadi</div>
<div class="lyrico-lyrics-wrapper">Thallae thillaalae</div>
<div class="lyrico-lyrics-wrapper">Oru kappal ootta neeyum vaadi</div>
<div class="lyrico-lyrics-wrapper">Thallae thillaalae</div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>

<div class="lyrico-lyrics-wrapper">Hummchachukkum hum chikkaan</div>
<div class="lyrico-lyrics-wrapper">Hummchachukkum hum chikkaan</div>
<div class="lyrico-lyrics-wrapper">Hummchachukkum hum chikkaan</div>
<div class="lyrico-lyrics-wrapper">Hummchachukkum hum chikkaan (Overlapping)</div>
<span class="lyrico-song-lyrics-breaker"></span>

<div class="lyrico-lyrics-wrapper">Thallae thillaalae</div>
<div class="lyrico-lyrics-wrapper">Thallae thillaalae</div>
<div class="lyrico-lyrics-wrapper">Thallae thillaalae</div>
<div class="lyrico-lyrics-wrapper">Thallae thillaalae</div>
<span class="lyrico-song-lyrics-breaker"></span>

<div class="lyrico-lyrics-wrapper">Hummchachukkum hum chikkaan</div>
<div class="lyrico-lyrics-wrapper">Hummchachukkum hum chikkaan</div>
<div class="lyrico-lyrics-wrapper">Hummchachukkum hum chikkaan</div>
<div class="lyrico-lyrics-wrapper">Hummchachukkum hum chikkaan (Overlapping)</div>
<span class="lyrico-song-lyrics-breaker"></span>

<div class="lyrico-lyrics-wrapper">Thallae thillaalae</div>
<div class="lyrico-lyrics-wrapper">Thallae thillaalae</div>
<div class="lyrico-lyrics-wrapper">Thallae thillaalae</div>
<div class="lyrico-lyrics-wrapper">Thallae thillaalae</div>
</pre>