---
title: "veedhikor jaadhi song lyrics"
album: "Natpe Thunai"
artist: "Hiphop Tamizha"
lyricist: "Hiphop Tamizha - Arivu - Sollisai Selvandhar"
director: "D. Parthiban Desingu"
path: "/albums/natpe-thunai-lyrics"
song: "Veedhikor Jaadhi"
image: ../../images/albumart/natpe-thunai.jpg
date: 2019-04-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/iPb1Wuy3GSQ"
type: "emotional"
singers:
  - Sanjith Hegde
  - Hiphop Tamizha
  - Arivu
  - Sollisai Selvandhar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Veedhikor Jaadhiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veedhikor Jaadhiyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jadhikku Veedhiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jadhikku Veedhiyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ketkaatha Naathiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketkaatha Naathiyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kidaikkaatha Neethiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kidaikkaatha Neethiyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ellavatrukkum Oru Uchchakattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellavatrukkum Oru Uchchakattam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Thatti Kettaal Athu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Thatti Kettaal Athu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kutram Kutram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kutram Kutram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaaringe Naayagan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaringe Naayagan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaaringey Theeyavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaringey Theeyavan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oozhalil Oozhiyam Seithavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oozhalil Oozhiyam Seithavan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oothiyam Poga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oothiyam Poga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paathikku Meley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paathikku Meley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakkenna Eduthathil Thavarillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkenna Eduthathil Thavarillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engindra Mana Nilai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engindra Mana Nilai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varuvathu Ethanaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varuvathu Ethanaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Sari illai Athanaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Sari illai Athanaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaasu Vaangi Neeyum Vottu Potta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasu Vaangi Neeyum Vottu Potta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vottu Poda Neeyum Note ta Ketta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vottu Poda Neeyum Note ta Ketta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Makkalin Velaikkaran Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Makkalin Velaikkaran Naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enkitta Nee Kaasu Kettathaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enkitta Nee Kaasu Kettathaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unkitta Kudukka Engirunthu Edukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unkitta Kudukka Engirunthu Edukka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mandiri Mandiri Mandiri Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandiri Mandiri Mandiri Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raja Raja Thanthiri Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja Raja Thanthiri Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enthiri Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enthiri Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ithu En Thappu illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu En Thappu illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Thappu Maapu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Thappu Maapu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vechchuttan Aapu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vechchuttan Aapu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Common Manukkingey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Common Manukkingey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaamam Yeri Pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaamam Yeri Pochu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naattoda Maanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naattoda Maanam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vimaanam Yeri Pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vimaanam Yeri Pochu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Mattum Nallavan Pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Mattum Nallavan Pol"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irunthu Enna Aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irunthu Enna Aachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poruppathum Marappathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poruppathum Marappathum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Makkalin Maanbaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Makkalin Maanbaachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ini Ennoda Aatchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Ennoda Aatchi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Arasiyal Maatchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Arasiyal Maatchi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kan Kondu Paar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan Kondu Paar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veezhapovathu Naam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veezhapovathu Naam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anaivarumthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anaivarumthaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Panam Inam Mozhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panam Inam Mozhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Matham Privinai Sulabam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Matham Privinai Sulabam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alai Kadal Ena Thirandu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alai Kadal Ena Thirandu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakku Silai Vadithidum Padai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku Silai Vadithidum Padai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adimaigal Sudum Vadai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adimaigal Sudum Vadai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adaikkalam Antha Sirai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adaikkalam Antha Sirai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vetti Sataai Potta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetti Sataai Potta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Modern Kattai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Modern Kattai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Nambi Vote tu Potta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Nambi Vote tu Potta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naamam Pattai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naamam Pattai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naarkaali En Thaazhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naarkaali En Thaazhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kattaamal Vaazhvene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattaamal Vaazhvene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aindhaandukku Oru Murai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aindhaandukku Oru Murai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Vanthen Un Arugil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Vanthen Un Arugil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Vendaamnu Sonnaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vendaamnu Sonnaalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tharuvene Ellaam Kaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharuvene Ellaam Kaiyil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen na Naan Naatukku Raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen na Naan Naatukku Raja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yemaandhu Ponathu Neethaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yemaandhu Ponathu Neethaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen na Naan Naatukku Raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen na Naan Naatukku Raja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yemaandhu Ponathu Neethaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yemaandhu Ponathu Neethaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ozhichaachu Marachaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ozhichaachu Marachaachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuzhi Thondi Pothachaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuzhi Thondi Pothachaachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oorukku Munnaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorukku Munnaadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaai Kizhiya Siruchaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaai Kizhiya Siruchaachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pathavikku Varumbothe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathavikku Varumbothe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ezhanthaachu Manasaatchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhanthaachu Manasaatchi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Perukku Mattumthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perukku Mattumthaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Makkal Oda Aatchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Makkal Oda Aatchi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ini Ennoda Aatchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Ennoda Aatchi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Arasiyal Maatchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Arasiyal Maatchi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kan Kondu Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan Kondu Paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veezha povathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veezha povathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naam Anaivarumthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam Anaivarumthaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ini Ennoda Aatchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Ennoda Aatchi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Arasiyal Maatchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Arasiyal Maatchi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kan Kondu Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan Kondu Paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veezha povathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veezha povathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naam Anaivarumthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam Anaivarumthaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veezhaathe Veerane Veerane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veezhaathe Veerane Veerane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veezhnthaalum Vaazhum Un Peyar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veezhnthaalum Vaazhum Un Peyar"/>
</div>
</pre>
