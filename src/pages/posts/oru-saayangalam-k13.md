---
title: "oru saayangalam song lyrics"
album: "K 13"
artist: "Sam C.S."
lyricist: "Sam. C.S"
director: "Barath Neelakantan"
path: "/albums/k13-lyrics"
song: "Oru Saayangalam"
image: ../../images/albumart/k13.jpg
date: 2019-05-09
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Gw1NyChIu1o"
type: "thril"
singers:
  - Sam C.S.
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Oru Saayangalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Saayangalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sambal Aagum Neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sambal Aagum Neram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athiyaayam Onnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athiyaayam Onnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aasai Yeri Vesham Maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai Yeri Vesham Maari"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandha Vandha Oru Poochi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandha Vandha Oru Poochi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Intha Ulagam Paarkkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Ulagam Paarkkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarvaikku Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarvaikku Ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Rasanaigal Onnum Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rasanaigal Onnum Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaam Summa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaam Summa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Rasikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Rasikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Rusikka Pusikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rusikka Pusikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Paavam Paakatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paavam Paakatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam Paakatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam Paakatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Neram Paakatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neram Paakatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paavam Paakatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paavam Paakatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Paavam Paakatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paavam Paakatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Paavam Paakatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paavam Paakatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Paavam Paakatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paavam Paakatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Paavam Paakatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paavam Paakatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Paavam Paakatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paavam Paakatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Paavam Paakatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paavam Paakatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Paavam Paakatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paavam Paakatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athiyaayam Rendu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athiyaayam Rendu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thayangiye Vaazhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thayangiye Vaazhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagam Ithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagam Ithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadayangal Thedum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadayangal Thedum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagam Ithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagam Ithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanakkena Evanukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanakkena Evanukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Rasanai Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rasanai Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Rasanaiya Unarnthukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rasanaiya Unarnthukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Viruppam Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viruppam Illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Valaikulle Aval Vanthaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valaikulle Aval Vanthaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathai Mogam Kondu Ingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathai Mogam Kondu Ingu"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan Vanthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan Vanthan"/>
</div>
<div class="lyrico-lyrics-wrapper">Karaiyera Uyir Keeri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karaiyera Uyir Keeri"/>
</div>
<div class="lyrico-lyrics-wrapper">Madai Thaandi Madai Thaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madai Thaandi Madai Thaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Verikondu Verikondu Verikondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verikondu Verikondu Verikondu"/>
</div>
<div class="lyrico-lyrics-wrapper">Verikondu Verikondu Verikondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verikondu Verikondu Verikondu"/>
</div>
<div class="lyrico-lyrics-wrapper">Verikondu Verikondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verikondu Verikondu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paavam Paakatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paavam Paakatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Paavam Paakatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paavam Paakatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Paavam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paavam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhaiya Mudichaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhaiya Mudichaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadayam Azhichaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadayam Azhichaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagil Jeyichaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagil Jeyichaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhaiya Mudichaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhaiya Mudichaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadayam Azhichaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadayam Azhichaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagil Jeyichaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagil Jeyichaan"/>
</div>
</pre>
