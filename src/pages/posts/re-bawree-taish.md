---
title: "re bawree song lyrics"
album: "Taish"
artist: "Govind Vasantha"
lyricist: "Hussain Haidry"
director: "Bejoy Nambiar"
path: "/albums/taish-lyrics"
song: "Re Bawree"
image: ../../images/albumart/taish.jpg
date: 2020-10-29
lang: hindi
youtubeLink: "https://www.youtube.com/embed/-kDWwpls5-8"
type: "love"
singers:
  - Prarthna Indrajith
  - Govind Vasantha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Re Bawree, Re Bawree
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Re Bawree, Re Bawree"/>
</div>
<div class="lyrico-lyrics-wrapper">Teri Chaaya, Jane Tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teri Chaaya, Jane Tu"/>
</div>
<div class="lyrico-lyrics-wrapper">Re Bawree, Tu kya Janee, Jane Tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Re Bawree, Tu kya Janee, Jane Tu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gam naa laage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gam naa laage"/>
</div>
<div class="lyrico-lyrics-wrapper">Rah naa jaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rah naa jaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ohh Sudh-Bhud to bhul bhi to jayee Bawree
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohh Sudh-Bhud to bhul bhi to jayee Bawree"/>
</div>
<div class="lyrico-lyrics-wrapper">Baand ke tere, Pair tale, Jaye Bawree
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baand ke tere, Pair tale, Jaye Bawree"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pal-Pal pe, Rag-Rag me Hai Bawree
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pal-Pal pe, Rag-Rag me Hai Bawree"/>
</div>
<div class="lyrico-lyrics-wrapper">Re Bawree
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Re Bawree"/>
</div>
<div class="lyrico-lyrics-wrapper">Man-Tan ka, Dhan Bal ka, Bawree
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Man-Tan ka, Dhan Bal ka, Bawree"/>
</div>
<div class="lyrico-lyrics-wrapper">Re Bawree
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Re Bawree"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Re Bawree, Re Bawree
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Re Bawree, Re Bawree"/>
</div>
<div class="lyrico-lyrics-wrapper">Teri Chaaya, Jane Tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teri Chaaya, Jane Tu"/>
</div>
<div class="lyrico-lyrics-wrapper">Re Bawree, Tu kya Janee, Jane Tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Re Bawree, Tu kya Janee, Jane Tu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Re Bawree, Re Bawree
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Re Bawree, Re Bawree"/>
</div>
<div class="lyrico-lyrics-wrapper">Teri Chaaya, Jane Tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teri Chaaya, Jane Tu"/>
</div>
<div class="lyrico-lyrics-wrapper">Re Bawree, Tu kya Janee, Jane Tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Re Bawree, Tu kya Janee, Jane Tu"/>
</div>
</pre>
