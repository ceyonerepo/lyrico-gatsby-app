---
title: "suryudivo chandrudivo song lyrics"
album: "Sarileru Neekevvaru"
artist: "Devi Sri Prasad"
lyricist: "Ramajogayya Sastry"
director: "Anil Ravipudi"
path: "/albums/sarileru-neekevvaru-lyrics"
song: "Suryudivo Chandrudivo"
image: ../../images/albumart/sarileru-neekevvaru.jpg
date: 2020-01-11
lang: telugu
youtubeLink: "https://www.youtube.com/embed/v6CkUYI_J3E"
type: "happy"
singers:
  - B Praak
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Thaddi thalangu thayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaddi thalangu thayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaka thaddi thalangu thayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaka thaddi thalangu thayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasantha ee vaala aahaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasantha ee vaala aahaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Swaraala aanandhamaye hoyya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Swaraala aanandhamaye hoyya"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaddi thalangu thayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaddi thalangu thayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaka thaddi thalangu thayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaka thaddi thalangu thayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Pedavullo ee vaala yenno rakaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pedavullo ee vaala yenno rakaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Chirunavvu chere hoya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chirunavvu chere hoya"/>
</div>
<div class="lyrico-lyrics-wrapper">Suryudivo chandrudivo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suryudivo chandrudivo"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa iddari kalayikavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa iddari kalayikavo"/>
</div>
<div class="lyrico-lyrics-wrapper">Saaradhivo vaaradhivo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saaradhivo vaaradhivo"/>
</div>
<div class="lyrico-lyrics-wrapper">Maa oopirikanna kalavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa oopirikanna kalavo"/>
</div>
<div class="lyrico-lyrics-wrapper">Vishwamanthaa prema pandinchaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vishwamanthaa prema pandinchaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Puttukaina rushivo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puttukaina rushivo"/>
</div>
<div class="lyrico-lyrics-wrapper">Saativaarikai nee vantuga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saativaarikai nee vantuga"/>
</div>
<div class="lyrico-lyrics-wrapper">Udyaminchu krushivo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udyaminchu krushivo"/>
</div>
<div class="lyrico-lyrics-wrapper">Maa andarilo okadaina manishivo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa andarilo okadaina manishivo"/>
</div>
<div class="lyrico-lyrics-wrapper">Suryudivo chandrudivo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suryudivo chandrudivo"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa iddari kalayikavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa iddari kalayikavo"/>
</div>
<div class="lyrico-lyrics-wrapper">Saaradhivo vaaradhivo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saaradhivo vaaradhivo"/>
</div>
<div class="lyrico-lyrics-wrapper">Maa oopirikanna kalavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa oopirikanna kalavo"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaddi thalangu thayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaddi thalangu thayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaka thaddi thalangu thayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaka thaddi thalangu thayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasantha ee vaala aahaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasantha ee vaala aahaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Swaraala aanandhamaye hoyya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Swaraala aanandhamaye hoyya"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaddi thalangu thayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaddi thalangu thayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaka thaddi thalangu thayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaka thaddi thalangu thayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasantha ee vaala aahaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasantha ee vaala aahaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Swaraala aanandhamaye hoyya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Swaraala aanandhamaye hoyya"/>
</div>
<div class="lyrico-lyrics-wrapper">Gunde lothulo gaayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gunde lothulo gaayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu taakite maayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu taakite maayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Mandu vesavilo pandu vennelalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandu vesavilo pandu vennelalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalisindi nee sahaayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalisindi nee sahaayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Polamaare aasala kosam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Polamaare aasala kosam"/>
</div>
<div class="lyrico-lyrics-wrapper">Polimeralu daatocchavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Polimeralu daatocchavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Talaaratalu velugayyela nenunnannavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Talaaratalu velugayyela nenunnannavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adagande akkara teerche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adagande akkara teerche"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee manchini pogadalante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee manchini pogadalante"/>
</div>
<div class="lyrico-lyrics-wrapper">Malo palike maatalu chaalavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malo palike maatalu chaalavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Suryudivo chandrudivo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suryudivo chandrudivo"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa iddari kalayikavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa iddari kalayikavo"/>
</div>
<div class="lyrico-lyrics-wrapper">Saaradhivo vaaradhivo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saaradhivo vaaradhivo"/>
</div>
<div class="lyrico-lyrics-wrapper">Maa oopirikanna kalavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa oopirikanna kalavo"/>
</div>
<div class="lyrico-lyrics-wrapper">Devudekkado ledu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Devudekkado ledu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vere kotthaga raadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vere kotthaga raadu"/>
</div>
<div class="lyrico-lyrics-wrapper">manchi maanushulalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manchi maanushulalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Goppa manasu tanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Goppa manasu tanai"/>
</div>
<div class="lyrico-lyrics-wrapper">Untaadu neeku laaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Untaadu neeku laaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye loka kalyanaanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye loka kalyanaanni"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasinchi janmichchindo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasinchi janmichchindo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninu kanna thalli kadupu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninu kanna thalli kadupu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nindaara pandindi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nindaara pandindi"/>
</div>
<div class="lyrico-lyrics-wrapper">Neelanti kodukuni mose
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelanti kodukuni mose"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee bhoomi bhaarathi saitam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee bhoomi bhaarathi saitam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee payanaaniki jayaho annadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee payanaaniki jayaho annadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Suryudivo chandrudivo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suryudivo chandrudivo"/>
</div>
 <div class="lyrico-lyrics-wrapper">Aa iddari kalayikavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa iddari kalayikavo"/>
</div>
<div class="lyrico-lyrics-wrapper">Saaradhivo vaaradhivo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saaradhivo vaaradhivo"/>
</div>
<div class="lyrico-lyrics-wrapper">Maa oopirikanna kalavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa oopirikanna kalavo"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaddi thalangu thayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaddi thalangu thayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaka thaddi thalangu thayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaka thaddi thalangu thayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasantha ee vaala aahaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasantha ee vaala aahaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Swaraala aanandhamaye hoyya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Swaraala aanandhamaye hoyya"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaddi thalangu thayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaddi thalangu thayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaka thaddi thalangu thayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaka thaddi thalangu thayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasantha ee vaala aahaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasantha ee vaala aahaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Swaraala aanandhamaye hoyya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Swaraala aanandhamaye hoyya"/>
</div>
</pre>
