---
title: "soorathenga song lyrics"
album: "Koogle Kuttappa"
artist: "Ghibran"
lyricist: "Viveka"
director: "Sabari – Saravanan"
path: "/albums/koogle-kuttappa-lyrics"
song: "Soorathenga"
image: ../../images/albumart/koogle-kuttappa.jpg
date: 2022-05-06
lang: tamil
youtubeLink: "https://www.youtube.com/embed/kwyWuCgYwaM"
type: "happy"
singers:
  - Gold Devaraj
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Soorathenga Vizhi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soorathenga Vizhi "/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagi Setharavuttaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagi Setharavuttaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Uthu Paathu Pathu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uthu Paathu Pathu "/>
</div>
<div class="lyrico-lyrics-wrapper">Vayasu Koranchuputtaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayasu Koranchuputtaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Thookkam Pasi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thookkam Pasi "/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaathaiyum Thorathivuttaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaathaiyum Thorathivuttaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Thokka Enna Sukka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thokka Enna Sukka "/>
</div>
<div class="lyrico-lyrics-wrapper">Panni Kadichu Kittaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panni Kadichu Kittaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thurumbuduchu Kedantha Manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thurumbuduchu Kedantha Manam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuru Thurunu Alaiyudhappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuru Thurunu Alaiyudhappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Karunkallu Pol Irunthavanthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karunkallu Pol Irunthavanthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavithai Ellaam Ezhuthuranppa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavithai Ellaam Ezhuthuranppa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ava Pona Theruvellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Pona Theruvellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Poovaasam Veesa Vechaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovaasam Veesa Vechaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Avaloda Nenappaadhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avaloda Nenappaadhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Iravellaam Pesavechaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravellaam Pesavechaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thurumbuduchu Kedantha Manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thurumbuduchu Kedantha Manam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuru Thurunu Alaiyudhappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuru Thurunu Alaiyudhappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Karunkallu Polirunthavanthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karunkallu Polirunthavanthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavithaiyellaam Ezhuthuranppa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavithaiyellaam Ezhuthuranppa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Soorathenga Vizhi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soorathenga Vizhi "/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagi Setharavuttaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagi Setharavuttaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Uthu Paathu Pathu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uthu Paathu Pathu "/>
</div>
<div class="lyrico-lyrics-wrapper">Vayasu Koranchuputtaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayasu Koranchuputtaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Thookkam Pasi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thookkam Pasi "/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaathaiyum Thorathivuttaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaathaiyum Thorathivuttaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Thokka Enna Sukka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thokka Enna Sukka "/>
</div>
<div class="lyrico-lyrics-wrapper">Panni Kadichu Kittaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panni Kadichu Kittaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Audio Video Mangipokum Age La
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Audio Video Mangipokum Age La"/>
</div>
<div class="lyrico-lyrics-wrapper">Jodiya Ladiya Nenjam Yengudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jodiya Ladiya Nenjam Yengudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oramaa Nikkura 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oramaa Nikkura "/>
</div>
<div class="lyrico-lyrics-wrapper">Hyder-kaala Cycle Ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hyder-kaala Cycle Ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Paavi Un Paarvai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paavi Un Paarvai "/>
</div>
<div class="lyrico-lyrics-wrapper">Thaan Puncture Ottuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaan Puncture Ottuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaangeyam Kaalai Computer Mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaangeyam Kaalai Computer Mela"/>
</div>
<div class="lyrico-lyrics-wrapper">Oyaama Kedanthene Unakkaagathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oyaama Kedanthene Unakkaagathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Haikoovah Urumaara Marabukkavithai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haikoovah Urumaara Marabukkavithai"/>
</div>
<div class="lyrico-lyrics-wrapper">Onnu Marugudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnu Marugudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Soorathenga Vizhi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soorathenga Vizhi "/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagi Setharavuttaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagi Setharavuttaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Uthu Paathu Pathu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uthu Paathu Pathu "/>
</div>
<div class="lyrico-lyrics-wrapper">Vayasu Koranchuputtaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayasu Koranchuputtaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Thookkam Pasi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thookkam Pasi "/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaathaiyum Thorathivuttaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaathaiyum Thorathivuttaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Thokka Enna Sukka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thokka Enna Sukka "/>
</div>
<div class="lyrico-lyrics-wrapper">Panni Kadichu Kittaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panni Kadichu Kittaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mounamaa Mutturaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mounamaa Mutturaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Maargamaa Kaatturaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maargamaa Kaatturaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarvaiyo Seiyudhe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarvaiyo Seiyudhe "/>
</div>
<div class="lyrico-lyrics-wrapper">Panchathanthiram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panchathanthiram"/>
</div>
<div class="lyrico-lyrics-wrapper">Paalukku Yengura 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paalukku Yengura "/>
</div>
<div class="lyrico-lyrics-wrapper">Poonaiyaa Maaruren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poonaiyaa Maaruren"/>
</div>
<div class="lyrico-lyrics-wrapper">Pombala Pottathu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pombala Pottathu "/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Manthiram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Manthiram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaathaadi Suthum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathaadi Suthum "/>
</div>
<div class="lyrico-lyrics-wrapper">Ennoda Nenjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennoda Nenjam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oyaama Alipaaya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oyaama Alipaaya "/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkaagathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkaagathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ammaadi Vayasuaaiyum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammaadi Vayasuaaiyum "/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagu Kulaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagu Kulaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Silaiya Merattura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silaiya Merattura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Soorathenga Vizhi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soorathenga Vizhi "/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagi Setharavuttaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagi Setharavuttaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Uthu Paathu Pathu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uthu Paathu Pathu "/>
</div>
<div class="lyrico-lyrics-wrapper">Vayasu Koranchuputtaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayasu Koranchuputtaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Thookkam Pasi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thookkam Pasi "/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaathaiyum Thorathivuttaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaathaiyum Thorathivuttaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Thokka Enna Sukka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thokka Enna Sukka "/>
</div>
<div class="lyrico-lyrics-wrapper">Panni Kadichu Kittaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panni Kadichu Kittaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thurumbuduchu Kedantha Manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thurumbuduchu Kedantha Manam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuru Thurunu Alaiyudhappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuru Thurunu Alaiyudhappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Karunkallu Pol Irunthavanthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karunkallu Pol Irunthavanthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavithaiyellaam Ezhuthuranppa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavithaiyellaam Ezhuthuranppa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ava Pona Theruvellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Pona Theruvellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Poovaasam Veesa Vechaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovaasam Veesa Vechaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Avaloda Nenappaadhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avaloda Nenappaadhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Iravellaam Pesa Vechaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravellaam Pesa Vechaa"/>
</div>
</pre>
