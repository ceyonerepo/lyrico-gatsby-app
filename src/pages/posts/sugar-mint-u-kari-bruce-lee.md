---
title: "sugar mint-u kari song lyrics"
album: "Bruce Lee"
artist: "G V Prakash Kumar"
lyricist: "Mani Kandan"
director: "Prashanth Pandiraj"
path: "/albums/bruce-lee-lyrics"
song: "Sugar Mint-U Kari"
image: ../../images/albumart/bruce-lee.jpg
date: 2017-03-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/FD5uIf8GBjQ"
type: "love"
singers:
  -	G V Prakash Kumar
  - M M Manasi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Oooooohoo ooo ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oooooohoo ooo ooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Oooooohoo ooo ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oooooohoo ooo ooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey en sugar mint-u kaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey en sugar mint-u kaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Un jigarthanda naan di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un jigarthanda naan di"/>
</div>
<div class="lyrico-lyrics-wrapper">Color legging pant-u kaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Color legging pant-u kaari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pattu kunju nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattu kunju nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Kutti nenju nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kutti nenju nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Chella kutti nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chella kutti nee"/>
</div>
<div class="lyrico-lyrics-wrapper">En kanmani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kanmani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kooda vandhu nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooda vandhu nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Kutti mohini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kutti mohini"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudaadha thappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudaadha thappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pannendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pannendi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lady k d neeyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lady k d neeyadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Soodu ye thum theeyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soodu ye thum theeyadi"/>
</div>
<div class="lyrico-lyrics-wrapper">French made strawberry di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="French made strawberry di"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee dooppu potta peiyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee dooppu potta peiyadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Baby en heart-u beatu nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby en heart-u beatu nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethan en target di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethan en target di"/>
</div>
<div class="lyrico-lyrics-wrapper">Senti ment scene book-u di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senti ment scene book-u di"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan un modern bruce lee di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan un modern bruce lee di"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oooooohoo ooo ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oooooohoo ooo ooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Oooooohoo ooo ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oooooohoo ooo ooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pap pap paaba pap pap
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pap pap paaba pap pap"/>
</div>
<div class="lyrico-lyrics-wrapper">Paaba pap pap 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaba pap pap "/>
</div>
<div class="lyrico-lyrics-wrapper">paaba pap pap pabapaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paaba pap pap pabapaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pap pap paaba pap pap
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pap pap paaba pap pap"/>
</div>
<div class="lyrico-lyrics-wrapper">Paaba pap pap 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaba pap pap "/>
</div>
<div class="lyrico-lyrics-wrapper">paaba pap pap
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paaba pap pap"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye chutti pennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye chutti pennae"/>
</div>
<div class="lyrico-lyrics-wrapper">Little sun-ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Little sun-ae"/>
</div>
<div class="lyrico-lyrics-wrapper">En girl friend nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En girl friend nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Ketta ponnae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketta ponnae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pennae pennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pennae pennae"/>
</div>
<div class="lyrico-lyrics-wrapper">Rowdy pennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rowdy pennae"/>
</div>
<div class="lyrico-lyrics-wrapper">En feeling-a share
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En feeling-a share"/>
</div>
<div class="lyrico-lyrics-wrapper">Seiyum ginger ginnae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seiyum ginger ginnae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Silluthae megamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silluthae megamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolluthae heyy mogamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolluthae heyy mogamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey chinna chinna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey chinna chinna"/>
</div>
<div class="lyrico-lyrics-wrapper">Poikkaaranae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poikkaaranae"/>
</div>
<div class="lyrico-lyrics-wrapper">Grace time-il
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Grace time-il"/>
</div>
<div class="lyrico-lyrics-wrapper">Kiss venumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kiss venumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Seatbelt-a pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seatbelt-a pol"/>
</div>
<div class="lyrico-lyrics-wrapper">Hug podhumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hug podhumae"/>
</div>
<div class="lyrico-lyrics-wrapper">En beglinton dog ivanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En beglinton dog ivanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lady k d neeyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lady k d neeyadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Soodu ye thum theeyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soodu ye thum theeyadi"/>
</div>
<div class="lyrico-lyrics-wrapper">French made strawberry di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="French made strawberry di"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee dooppu potta peiyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee dooppu potta peiyadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Baby en heart-u beatu nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby en heart-u beatu nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethan en target di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethan en target di"/>
</div>
<div class="lyrico-lyrics-wrapper">Senti ment scene book-u di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senti ment scene book-u di"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan un modern bruce lee di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan un modern bruce lee di"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oooooohoo ooo ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oooooohoo ooo ooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaa aaaaaa haa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaa aaaaaa haa aaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye sokkupodi kannala naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye sokkupodi kannala naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Makku paiyan aanenadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Makku paiyan aanenadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Bujji kutti naan singa kutti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bujji kutti naan singa kutti"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna aakkitta naayikutti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna aakkitta naayikutti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Utchimel muththamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Utchimel muththamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vendumae heyy niththamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vendumae heyy niththamae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Suthi suthi enna suthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suthi suthi enna suthi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhaayae uchchu kotti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhaayae uchchu kotti"/>
</div>
<div class="lyrico-lyrics-wrapper">Konji konji kannaamoochi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konji konji kannaamoochi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalli po kanna katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalli po kanna katti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pap pap paap paraaparab
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pap pap paap paraaparab"/>
</div>
<div class="lyrico-lyrics-wrapper">Pap pap paap paraaparab
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pap pap paap paraaparab"/>
</div>
<div class="lyrico-lyrics-wrapper">Pap pap paap paraaparab
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pap pap paap paraaparab"/>
</div>
<div class="lyrico-lyrics-wrapper">Pabarapap pabarabapap
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pabarapap pabarabapap"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pap pap paap paraaparab
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pap pap paap paraaparab"/>
</div>
<div class="lyrico-lyrics-wrapper">Pap pap paap paraaparab
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pap pap paap paraaparab"/>
</div>
<div class="lyrico-lyrics-wrapper">Pap pap paap paraaparab
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pap pap paap paraaparab"/>
</div>
<div class="lyrico-lyrics-wrapper">Parabapap rabarap
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parabapap rabarap"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oooooohoo ooo ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oooooohoo ooo ooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Oooooohoo ooo ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oooooohoo ooo ooo"/>
</div>
</pre>
