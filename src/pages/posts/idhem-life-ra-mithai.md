---
title: "idhem life ra song lyrics"
album: "Mithai"
artist: "Vivek Sagar"
lyricist: "Kittu Vissapragada"
director: "Prashant Kumar"
path: "/albums/mithai-lyrics"
song: "Idhem Life Ra"
image: ../../images/albumart/mithai.jpg
date: 2019-02-22
lang: telugu
youtubeLink: "https://www.youtube.com/embed/qhONuvH9bL4"
type: "happy"
singers:
  - Sravya Kothalanka
  - Chorus
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Uliki Padakura Tellare 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uliki Padakura Tellare "/>
</div>
<div class="lyrico-lyrics-wrapper">Alarm Mogaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alarm Mogaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaahh Parugu Modaluga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaahh Parugu Modaluga "/>
</div>
<div class="lyrico-lyrics-wrapper">Kangaruga Kaalu Jaaradha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangaruga Kaalu Jaaradha"/>
</div>
<div class="lyrico-lyrics-wrapper">Aah Uliki Padakura 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aah Uliki Padakura "/>
</div>
<div class="lyrico-lyrics-wrapper">Tellare Alarm Mogaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tellare Alarm Mogaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaahh Parugu Modaluga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaahh Parugu Modaluga "/>
</div>
<div class="lyrico-lyrics-wrapper">Kangaruga Kaalu Jaaradha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangaruga Kaalu Jaaradha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohh Oooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohh Oooooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadalaleni Vimanam Lo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadalaleni Vimanam Lo "/>
</div>
<div class="lyrico-lyrics-wrapper">Egarani Oohalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Egarani Oohalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Padamani Those Savaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padamani Those Savaalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ohh Oooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohh Oooooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mari Mari Mari Mari Mariii Thondara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mari Mari Mari Mari Mariii Thondara"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaji Biji Gaji Biji Routiney Kada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaji Biji Gaji Biji Routiney Kada"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey Arey Arey Arey Idem Life Ra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey Arey Arey Arey Idem Life Ra"/>
</div>
<div class="lyrico-lyrics-wrapper">Pora Hahaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pora Hahaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mari Mari Mari Mari Mariii Thondara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mari Mari Mari Mari Mariii Thondara"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaji Biji Gaji Biji Routiney Kada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaji Biji Gaji Biji Routiney Kada"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey Arey Arey Arey Idem Life Ra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey Arey Arey Arey Idem Life Ra"/>
</div>
<div class="lyrico-lyrics-wrapper">Pora Pora Haaahoooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pora Pora Haaahoooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Marugunna Daari Maruthunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marugunna Daari Maruthunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Fate Maaradanaa Niraashe Ra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fate Maaradanaa Niraashe Ra"/>
</div>
<div class="lyrico-lyrics-wrapper">Kotha Paint Kotti Looku Marchukuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kotha Paint Kotti Looku Marchukuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Dust Bin Ninda Chettha Ledha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dust Bin Ninda Chettha Ledha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raju Gari Zamaanaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raju Gari Zamaanaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Rayaledhu Khazana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rayaledhu Khazana"/>
</div>
<div class="lyrico-lyrics-wrapper">Niku Dikku Thikaanaa Idekada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niku Dikku Thikaanaa Idekada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raju Gari Zamaanaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raju Gari Zamaanaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Rayaledhu Khazana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rayaledhu Khazana"/>
</div>
<div class="lyrico-lyrics-wrapper">Niku Dikku Thikaanaa Idekada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niku Dikku Thikaanaa Idekada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nizam Seku Nedu Pedda Ssam Yindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nizam Seku Nedu Pedda Ssam Yindhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Olle Hunammayi Dille Lolli Petti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Olle Hunammayi Dille Lolli Petti"/>
</div>
<div class="lyrico-lyrics-wrapper">Gussa Penchinde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gussa Penchinde"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasika Kalavara Padela Mari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasika Kalavara Padela Mari"/>
</div>
<div class="lyrico-lyrics-wrapper">Adugulu Tadabanadela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adugulu Tadabanadela"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetikina Dorakani Charithree
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetikina Dorakani Charithree"/>
</div>
<div class="lyrico-lyrics-wrapper">Neede Hey Hey Heyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neede Hey Hey Heyy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mari Mari Mari Mari Mariii Thondara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mari Mari Mari Mari Mariii Thondara"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaji Biji Gaji Biji Routiney Kada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaji Biji Gaji Biji Routiney Kada"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey Arey Arey Arey Idem Life Ra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey Arey Arey Arey Idem Life Ra"/>
</div>
<div class="lyrico-lyrics-wrapper">Pora Hahaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pora Hahaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mari Mari Mari Mari Mariii Thondara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mari Mari Mari Mari Mariii Thondara"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaji Biji Gaji Biji Routiney Kada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaji Biji Gaji Biji Routiney Kada"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey Arey Arey Arey Idem Life Ra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey Arey Arey Arey Idem Life Ra"/>
</div>
<div class="lyrico-lyrics-wrapper">Pora Pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pora Pora"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ganta Kottagane Dandamettukochi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ganta Kottagane Dandamettukochi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sittamantu Chere Bantu Lede
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sittamantu Chere Bantu Lede"/>
</div>
<div class="lyrico-lyrics-wrapper">Goti Toti Poye Dani Goddalethii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Goti Toti Poye Dani Goddalethii"/>
</div>
<div class="lyrico-lyrics-wrapper">Nariki Choosukuna Life Neede
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nariki Choosukuna Life Neede"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aasha Petti Mithai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasha Petti Mithai"/>
</div>
<div class="lyrico-lyrics-wrapper">Poye Batuku Badaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poye Batuku Badaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Rojukoka Ladaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rojukoka Ladaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Bade Miyaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bade Miyaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aasha Petti Mithai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasha Petti Mithai"/>
</div>
<div class="lyrico-lyrics-wrapper">Poye Batuku Badaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poye Batuku Badaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Rojukoka Ladaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rojukoka Ladaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Bade Miyaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bade Miyaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Simham Laga B nee Thathalu Ninnitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Simham Laga B nee Thathalu Ninnitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Gadde Pettamana Gorre 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gadde Pettamana Gorre "/>
</div>
<div class="lyrico-lyrics-wrapper">Pilla Chesi Godalu Ekkaare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pilla Chesi Godalu Ekkaare"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasika Kalavarapadela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasika Kalavarapadela"/>
</div>
<div class="lyrico-lyrics-wrapper">Mari Adugulu Tadabadamanela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mari Adugulu Tadabadamanela"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetikina Dorakani Charithre Neede
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetikina Dorakani Charithre Neede"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Hey Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Hey Hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mari Mari Mari Mari Mariii Thondara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mari Mari Mari Mari Mariii Thondara"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaji Biji Gaji Biji Routiney Kada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaji Biji Gaji Biji Routiney Kada"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey Arey Arey Arey Idem Life Ra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey Arey Arey Arey Idem Life Ra"/>
</div>
<div class="lyrico-lyrics-wrapper">Pora Hahaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pora Hahaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mari Mari Mari Mari Mariii Thondara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mari Mari Mari Mari Mariii Thondara"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaji Biji Gaji Biji Routiney Kada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaji Biji Gaji Biji Routiney Kada"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey Arey Arey Arey Idem Life Ra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey Arey Arey Arey Idem Life Ra"/>
</div>
<div class="lyrico-lyrics-wrapper">Pora Pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pora Pora"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uliki Padakura Tellare 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uliki Padakura Tellare "/>
</div>
<div class="lyrico-lyrics-wrapper">Alarm Mogaga Mogaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alarm Mogaga Mogaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaahh Parugu Modaluga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaahh Parugu Modaluga "/>
</div>
<div class="lyrico-lyrics-wrapper">Kangaruga Kaalu Jaaradha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangaruga Kaalu Jaaradha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalu Jaaradha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalu Jaaradha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadalaleni Vimanam Lo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadalaleni Vimanam Lo "/>
</div>
<div class="lyrico-lyrics-wrapper">Egarani Oohalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Egarani Oohalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Padamani Those savaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padamani Those savaalu"/>
</div>
</pre>
