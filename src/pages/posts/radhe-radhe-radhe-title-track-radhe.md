---
title: "radhe radhe radhe - title track song lyrics"
album: "Radhe"
artist: "Sajid Wajid"
lyricist: "Sajid"
director: "Prabhu Deva"
path: "/albums/radhe-lyrics"
song: "Radhe Radhe Radhe - Title Track"
image: ../../images/albumart/radhe.jpg
date: 2021-05-13
lang: hindi
youtubeLink: "https://www.youtube.com/embed/jA3XkD8k8-A"
type: "Mass"
singers:
  - Sajid
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Radhe radhe radhe radhe x((2) times)
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Radhe radhe radhe radhe x((2) times)"/>
</div>
<div class="lyrico-lyrics-wrapper">Ra ra ra ra ra ra..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ra ra ra ra ra ra.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lockdown in the city I'm a wanted
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lockdown in the city I'm a wanted"/>
</div>
<div class="lyrico-lyrics-wrapper">Saare log saala bole mujhe wanted
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saare log saala bole mujhe wanted"/>
</div>
<div class="lyrico-lyrics-wrapper">Dil se mahobbat thodi zyada mujhe wanted
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil se mahobbat thodi zyada mujhe wanted"/>
</div>
<div class="lyrico-lyrics-wrapper">Radhe radhe radhe radhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Radhe radhe radhe radhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Radhe radhe radhe ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Radhe radhe radhe ho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lockdown in the city I'm a wanted
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lockdown in the city I'm a wanted"/>
</div>
<div class="lyrico-lyrics-wrapper">Saare log saala bole mujhe wanted
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saare log saala bole mujhe wanted"/>
</div>
<div class="lyrico-lyrics-wrapper">Dil se mahobbat thodi zyada mujhe wanted
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil se mahobbat thodi zyada mujhe wanted"/>
</div>
<div class="lyrico-lyrics-wrapper">Radhe radhe radhe radhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Radhe radhe radhe radhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Radhe radhe radhe ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Radhe radhe radhe ho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jeena kunwara nahi maangta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jeena kunwara nahi maangta"/>
</div>
<div class="lyrico-lyrics-wrapper">Paisa tumhara nahi maangta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paisa tumhara nahi maangta"/>
</div>
<div class="lyrico-lyrics-wrapper">Zindagi jee li hai maine apni hi sharton pe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zindagi jee li hai maine apni hi sharton pe"/>
</div>
<div class="lyrico-lyrics-wrapper">Bol bacchan saala apun kisi ka nai maanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bol bacchan saala apun kisi ka nai maanta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kudiyan bhi mudke daubara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudiyan bhi mudke daubara"/>
</div>
<div class="lyrico-lyrics-wrapper">Dekhe main aisa hun nazara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dekhe main aisa hun nazara"/>
</div>
<div class="lyrico-lyrics-wrapper">Jis bhi gali se jaun bheed lag jaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jis bhi gali se jaun bheed lag jaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Mere ghar ka pata to yahan bacha bacha jaanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mere ghar ka pata to yahan bacha bacha jaanta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bad luck if you wanna take it granted
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bad luck if you wanna take it granted"/>
</div>
<div class="lyrico-lyrics-wrapper">Don't touch mera current high voltage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Don't touch mera current high voltage"/>
</div>
<div class="lyrico-lyrics-wrapper">Tumse mahobbat thodi zyada mujhe wanted
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tumse mahobbat thodi zyada mujhe wanted"/>
</div>
<div class="lyrico-lyrics-wrapper">Radhe radhe radhe radhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Radhe radhe radhe radhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Radhe radhe radhe ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Radhe radhe radhe ho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lockdown in the city I'm a wanted
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lockdown in the city I'm a wanted"/>
</div>
<div class="lyrico-lyrics-wrapper">Saare log saala bole mujhe wanted
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saare log saala bole mujhe wanted"/>
</div>
<div class="lyrico-lyrics-wrapper">Dil se mahobbat thodi zyada mujhe wanted
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil se mahobbat thodi zyada mujhe wanted"/>
</div>
<div class="lyrico-lyrics-wrapper">Radhe radhe radhe radhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Radhe radhe radhe radhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Radhe radhe radhe ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Radhe radhe radhe ho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sikka chale hai mere naam ka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sikka chale hai mere naam ka"/>
</div>
<div class="lyrico-lyrics-wrapper">Ikka bana dun main gulaam ka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ikka bana dun main gulaam ka"/>
</div>
<div class="lyrico-lyrics-wrapper">Koyi bane bad to main uska bhi dad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koyi bane bad to main uska bhi dad"/>
</div>
<div class="lyrico-lyrics-wrapper">Main cheez kya ab tak tu yeh nahi jaanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main cheez kya ab tak tu yeh nahi jaanta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dil mein, main sabke basa hun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil mein, main sabke basa hun"/>
</div>
<div class="lyrico-lyrics-wrapper">Jo hun duaon se bana hun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jo hun duaon se bana hun"/>
</div>
<div class="lyrico-lyrics-wrapper">Mere hai tareeke mere apne asool hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mere hai tareeke mere apne asool hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Sabko pata hai bada pakka hun zubaan ka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sabko pata hai bada pakka hun zubaan ka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bad luck if you wanna take it granted
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bad luck if you wanna take it granted"/>
</div>
<div class="lyrico-lyrics-wrapper">Don't touch mera current high voltage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Don't touch mera current high voltage"/>
</div>
<div class="lyrico-lyrics-wrapper">Tumse mahobbat thodi zyada mujhe wanted
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tumse mahobbat thodi zyada mujhe wanted"/>
</div>
<div class="lyrico-lyrics-wrapper">Radhe radhe radhe radhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Radhe radhe radhe radhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Radhe radhe radhe ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Radhe radhe radhe ho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lockdown in the city I'm a wanted
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lockdown in the city I'm a wanted"/>
</div>
<div class="lyrico-lyrics-wrapper">Saare log saala bole mujhe wanted
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saare log saala bole mujhe wanted"/>
</div>
<div class="lyrico-lyrics-wrapper">Dil se mahobbat thodi zyada mujhe wanted
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil se mahobbat thodi zyada mujhe wanted"/>
</div>
<div class="lyrico-lyrics-wrapper">Radhe radhe radhe radhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Radhe radhe radhe radhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Radhe radhe radhe ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Radhe radhe radhe ho"/>
</div>
</pre>
