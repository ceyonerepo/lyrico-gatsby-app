---
title: 'haiyo haiyo song lyrics'
album: 'Oh My Kadavule'
artist: 'Leon James'
lyricist: 'Ko Sesha'
director: 'Ashwath Marimuthu'
path: '/albums/oh-my-kadavule-song-lyrics'
song: 'Haiyo Haiyo'
image: ../../images/albumart/oh-my-kadavule.jpg
date: 2020-02-14
lang: tamil
singers: 
- Leon James
youtubeLink: 'https://www.youtube.com/embed/lJXDFmqr564'
type: 'love'
---

<pre class="lyrics-native">
    Coming Soon
</pre>

<pre class="lyrics-english">

  <div class="lyrico-lyrics-wrapper">Ava haiyo haiyo haiyo
<input type="checkbox" class="lyrico-select-lyric-line" value="Ava haiyo haiyo haiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolluraalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kolluraalae"/>
</div>
<div class="lyrico-lyrics-wrapper">1000 <div class="lyrico-lyrics-wrapper">wattu kannaala</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="wattu kannaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna mella mella mella
<input type="checkbox" class="lyrico-select-lyric-line" value="Enna mella mella mella"/>
</div>
<div class="lyrico-lyrics-wrapper">Melluraalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Melluraalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Soodaana jilebi pola…
<input type="checkbox" class="lyrico-select-lyric-line" value="Soodaana jilebi pola…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Bougainvillea rose ellaam
<input type="checkbox" class="lyrico-select-lyric-line" value="Bougainvillea rose ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Bothai yeri ketkkumaam
<input type="checkbox" class="lyrico-select-lyric-line" value="Bothai yeri ketkkumaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava pudhu poovinamaa…poovinamaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Ava pudhu poovinamaa…poovinamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Haiku limerick venbalaam
<input type="checkbox" class="lyrico-select-lyric-line" value="Haiku limerick venbalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vekka pattu ketkumaam
<input type="checkbox" class="lyrico-select-lyric-line" value="Vekka pattu ketkumaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava anju adi kavidhaiyaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Ava anju adi kavidhaiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Moodu pani neram paarthu
<input type="checkbox" class="lyrico-select-lyric-line" value="Moodu pani neram paarthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Avalodu ecr-il
<input type="checkbox" class="lyrico-select-lyric-line" value="Avalodu ecr-il"/>
</div>
<div class="lyrico-lyrics-wrapper">Long drive-u poga solla
<input type="checkbox" class="lyrico-select-lyric-line" value="Long drive-u poga solla"/>
</div>
<div class="lyrico-lyrics-wrapper">Aavalaala avasathaigal yeralamdhaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Aavalaala avasathaigal yeralamdhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Haiyo en life-ula
<input type="checkbox" class="lyrico-select-lyric-line" value="Haiyo en life-ula"/>
</div>
<div class="lyrico-lyrics-wrapper">Love moodu start aaiduchae
<input type="checkbox" class="lyrico-select-lyric-line" value="Love moodu start aaiduchae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ava haiyo haiyo haiyo
<input type="checkbox" class="lyrico-select-lyric-line" value="Ava haiyo haiyo haiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolluraalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kolluraalae"/>
</div>
<div class="lyrico-lyrics-wrapper">1000 <div class="lyrico-lyrics-wrapper">wattu kannaala</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="wattu kannaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna mella mella mella
<input type="checkbox" class="lyrico-select-lyric-line" value="Enna mella mella mella"/>
</div>
<div class="lyrico-lyrics-wrapper">Melluraalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Melluraalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Soodaana jilebi pola…
<input type="checkbox" class="lyrico-select-lyric-line" value="Soodaana jilebi pola…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ava haiyo haiyo haiyo
<input type="checkbox" class="lyrico-select-lyric-line" value="Ava haiyo haiyo haiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolluraalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kolluraalae"/>
</div>
<div class="lyrico-lyrics-wrapper">1000 <div class="lyrico-lyrics-wrapper">wattu kannaala</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="wattu kannaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna mella mella mella
<input type="checkbox" class="lyrico-select-lyric-line" value="Enna mella mella mella"/>
</div>
<div class="lyrico-lyrics-wrapper">Melluraalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Melluraalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Soodaana jilebi pola…
<input type="checkbox" class="lyrico-select-lyric-line" value="Soodaana jilebi pola…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Aaromalae baby
<input type="checkbox" class="lyrico-select-lyric-line" value="Aaromalae baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava beauty
<input type="checkbox" class="lyrico-select-lyric-line" value="Ava beauty"/>
</div>
<div class="lyrico-lyrics-wrapper">Thani class-u Thaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Thani class-u Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Hypnotic kannaala
<input type="checkbox" class="lyrico-select-lyric-line" value="Hypnotic kannaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna mayanga vechittalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Enna mayanga vechittalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Gold-il senja thaeru
<input type="checkbox" class="lyrico-select-lyric-line" value="Gold-il senja thaeru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava nadandha
<input type="checkbox" class="lyrico-select-lyric-line" value="Ava nadandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Sema maas-u dhaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Sema maas-u dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vadam pudichi pinnaal
<input type="checkbox" class="lyrico-select-lyric-line" value="Vadam pudichi pinnaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna alaya vechitaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Enna alaya vechitaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Andha kohinoor-u thiridi
<input type="checkbox" class="lyrico-select-lyric-line" value="Andha kohinoor-u thiridi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava kaal golusil maati
<input type="checkbox" class="lyrico-select-lyric-line" value="Ava kaal golusil maati"/>
</div>
<div class="lyrico-lyrics-wrapper">Adha honey moon-la dhaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Adha honey moon-la dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharuven… tharuven
<input type="checkbox" class="lyrico-select-lyric-line" value="Tharuven… tharuven"/>
</div>
<div class="lyrico-lyrics-wrapper">Onna sernthu vaazhathaanae
<input type="checkbox" class="lyrico-select-lyric-line" value="Onna sernthu vaazhathaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru loan poduvenae
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru loan poduvenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha kashmir-ula
<input type="checkbox" class="lyrico-select-lyric-line" value="Andha kashmir-ula"/>
</div>
<div class="lyrico-lyrics-wrapper">Veedu vaanguvenae..ae….
<input type="checkbox" class="lyrico-select-lyric-line" value="Veedu vaanguvenae..ae…."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ava haiyo haiyo haiyo
<input type="checkbox" class="lyrico-select-lyric-line" value="Ava haiyo haiyo haiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolluraalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kolluraalae"/>
</div>
<div class="lyrico-lyrics-wrapper">1000 <div class="lyrico-lyrics-wrapper">wattu kannaala</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="wattu kannaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna mella mella mella
<input type="checkbox" class="lyrico-select-lyric-line" value="Enna mella mella mella"/>
</div>
<div class="lyrico-lyrics-wrapper">Melluraalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Melluraalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Soodaana jilebi pola…
<input type="checkbox" class="lyrico-select-lyric-line" value="Soodaana jilebi pola…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ava haiyo haiyo haiyo
<input type="checkbox" class="lyrico-select-lyric-line" value="Ava haiyo haiyo haiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolluraalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kolluraalae"/>
</div>
<div class="lyrico-lyrics-wrapper">1000 <div class="lyrico-lyrics-wrapper">wattu kannaala</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="wattu kannaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna mella mella mella
<input type="checkbox" class="lyrico-select-lyric-line" value="Enna mella mella mella"/>
</div>
<div class="lyrico-lyrics-wrapper">Melluraalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Melluraalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Soodaana jilebi pola…
<input type="checkbox" class="lyrico-select-lyric-line" value="Soodaana jilebi pola…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ava yemma yemma kollura
<input type="checkbox" class="lyrico-select-lyric-line" value="Ava yemma yemma kollura"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna summa summa mellura
<input type="checkbox" class="lyrico-select-lyric-line" value="Enna summa summa mellura"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava kanna vechi thaakkura
<input type="checkbox" class="lyrico-select-lyric-line" value="Ava kanna vechi thaakkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna kandam thundam aakura
<input type="checkbox" class="lyrico-select-lyric-line" value="Enna kandam thundam aakura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ava yemma yemma kollura
<input type="checkbox" class="lyrico-select-lyric-line" value="Ava yemma yemma kollura"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna summa summa mellura
<input type="checkbox" class="lyrico-select-lyric-line" value="Enna summa summa mellura"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava manasa alasi thovaikura
<input type="checkbox" class="lyrico-select-lyric-line" value="Ava manasa alasi thovaikura"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna mulusa mental aakura…….
<input type="checkbox" class="lyrico-select-lyric-line" value="Enna mulusa mental aakura……."/>
</div>
</pre>