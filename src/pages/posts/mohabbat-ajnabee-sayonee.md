---
title: "mohabbat ajnabee song lyrics"
album: "Sayonee"
artist: "Anamta - Amaan"
lyricist: "Sohail Haider"
director: "Nitin Kumar Gupta - Abhay Singhal"
path: "/albums/sayonee-lyrics"
song: "Mohabbat Ajnabee"
image: ../../images/albumart/sayonee.jpg
date: 2020-12-18
lang: hindi
youtubeLink: "https://www.youtube.com/embed/KSrmqSYMUKg"
type: "love"
singers:
  - Sachet Tandon
  - Sukriti Kakar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yeh kab se thodi thodi kho rahi hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh kab se thodi thodi kho rahi hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haan.. Haan..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haan.. Haan.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeh kab se thodi thodi kho rahi hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh kab se thodi thodi kho rahi hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Khuli aankhon se yeh kaise so rahi hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khuli aankhon se yeh kaise so rahi hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeh kiska jaana aise ro rahi hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh kiska jaana aise ro rahi hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mohabbat ajnabee si ho rahi hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mohabbat ajnabee si ho rahi hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mohabbat ajnabee si ho rahi hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mohabbat ajnabee si ho rahi hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mohabbat ajnabee si ho rahi hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mohabbat ajnabee si ho rahi hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nafa nuksaan yeh kab sochti hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nafa nuksaan yeh kab sochti hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeh meethi hai magar yeh ik rog si hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh meethi hai magar yeh ik rog si hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeh zindagi teri huyi sab tera hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh zindagi teri huyi sab tera hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Tumhein paane ki khwahish ik jog si hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tumhein paane ki khwahish ik jog si hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeh doori ke beej har su bo rahi hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh doori ke beej har su bo rahi hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mohabbat ajnabee si ho rahi hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mohabbat ajnabee si ho rahi hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mohabbat ajnabee si ho rahi hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mohabbat ajnabee si ho rahi hai"/>
</div>
</pre>
