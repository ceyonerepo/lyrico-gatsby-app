---
title: "kanule munige song lyrics"
album: "Guvva Gorinka"
artist: "Bobbili Suresh"
lyricist: "Krishna Kanth"
director: "Mohan Bammidi"
path: "/albums/guvva-gorinka-lyrics"
song: "Kanule Munige"
image: ../../images/albumart/guvva-gorinka.jpg
date: 2020-12-10
lang: telugu
youtubeLink: "https://www.youtube.com/embed/iOFLBspBnI8"
type: "sad"
singers:
  - Aparna Nandan
  - Ravi Prakash Chodimalla
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kanule Munige Kaneetilona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanule Munige Kaneetilona"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam Gaayam Chesenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam Gaayam Chesenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalise Kalapai Mannesipoye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalise Kalapai Mannesipoye"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhooram Dhooram Ayyenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhooram Dhooram Ayyenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thodai Cheri Shwaasai Maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodai Cheri Shwaasai Maari"/>
</div>
<div class="lyrico-lyrics-wrapper">Verai Poye Ee Premaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verai Poye Ee Premaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mante Aari Baadhe Theere
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mante Aari Baadhe Theere"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaare Ledhaa Oo Premaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaare Ledhaa Oo Premaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaa Prapanchamai Maarenthalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaa Prapanchamai Maarenthalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Prathi Kshanam Needhe Kala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prathi Kshanam Needhe Kala"/>
</div>
<div class="lyrico-lyrics-wrapper">Ekaakilaa Chesesinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ekaakilaa Chesesinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neekosame Vechaanilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neekosame Vechaanilaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhehaale Pakkane 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhehaale Pakkane "/>
</div>
<div class="lyrico-lyrics-wrapper">Yadhele Vere Dhikkule Unnaa Dhigulenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yadhele Vere Dhikkule Unnaa Dhigulenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Enthunnaa Dhoorame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enthunnaa Dhoorame"/>
</div>
<div class="lyrico-lyrics-wrapper">Praanaalu Dhaggare Thaane Kalipene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Praanaalu Dhaggare Thaane Kalipene"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhwesham Unnaa Sarwam Thaanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhwesham Unnaa Sarwam Thaanai"/>
</div>
<div class="lyrico-lyrics-wrapper">Maare Maaye Preme Suma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maare Maaye Preme Suma"/>
</div>
<div class="lyrico-lyrics-wrapper">Pommannaamaa Neeke Lokam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pommannaamaa Neeke Lokam"/>
</div>
<div class="lyrico-lyrics-wrapper">Shoonyam Chese Pramaadhamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shoonyam Chese Pramaadhamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaa Prapanchamai Maarenthalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaa Prapanchamai Maarenthalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Prathi Kshanam Needhe Kala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prathi Kshanam Needhe Kala"/>
</div>
<div class="lyrico-lyrics-wrapper">Ekaakilaa Chesesinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ekaakilaa Chesesinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neekosame Vechaanilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neekosame Vechaanilaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bandhaale Veedani Shilalaage 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bandhaale Veedani Shilalaage "/>
</div>
<div class="lyrico-lyrics-wrapper">Oopiri Mandhe Asalundhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oopiri Mandhe Asalundhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ishtamgaa Nammithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishtamgaa Nammithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandheham Raadhule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandheham Raadhule"/>
</div>
<div class="lyrico-lyrics-wrapper">Preme Nijamaithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Preme Nijamaithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadhe Ledhu Antham Kaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadhe Ledhu Antham Kaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninge Neelo Preme Kadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninge Neelo Preme Kadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Endaa Thaane Vaana Thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endaa Thaane Vaana Thaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Roopam Leni Jeevam Kadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Roopam Leni Jeevam Kadhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oo Naa Prapanchamai Maarenthalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Naa Prapanchamai Maarenthalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Prathi Kshanam Needhe Kala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prathi Kshanam Needhe Kala"/>
</div>
<div class="lyrico-lyrics-wrapper">Ekaakilaa Chesesinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ekaakilaa Chesesinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neekosame Vechaanilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neekosame Vechaanilaa"/>
</div>
</pre>
