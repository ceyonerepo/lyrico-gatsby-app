---
title: "silu silu kids version song lyrics"
album: "Genius"
artist: "Yuvan Shankar Raja"
lyricist: "Vairamuthu"
director: "Suseenthiran"
path: "/albums/genius-lyrics"
song: "Silu Silu Kids Version"
image: ../../images/albumart/genius.jpg
date: 2018-10-26
lang: tamil
youtubeLink: "https://www.youtube.com/embed/4MN2EO-m2dE"
type: "happy"
singers:
  - V Praneeth
  - Vs Adharsh
  - Vs Akash
  - S Sindhuja
  - V Priyadharshini
  - CS Dharshini
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">lalalalalalalalalala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lalalalalalalalalala"/>
</div>
<div class="lyrico-lyrics-wrapper">lalalalalalalalalala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lalalalalalalalalala"/>
</div>
<div class="lyrico-lyrics-wrapper">lalalalalalalalalala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lalalalalalalalalala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">silu silu silu sil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="silu silu silu sil"/>
</div>
<div class="lyrico-lyrics-wrapper">kaathu veesuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaathu veesuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">sala sala sala sal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sala sala sala sal"/>
</div>
<div class="lyrico-lyrics-wrapper">oothu pesuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oothu pesuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">kala kala kala kal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kala kala kala kal"/>
</div>
<div class="lyrico-lyrics-wrapper">aaru ooduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaru ooduthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thaneeril viluntha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaneeril viluntha "/>
</div>
<div class="lyrico-lyrics-wrapper">meenaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meenaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">tharaiyil iruntha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tharaiyil iruntha "/>
</div>
<div class="lyrico-lyrics-wrapper">maanaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maanaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">paatu tha tha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paatu tha tha "/>
</div>
<div class="lyrico-lyrics-wrapper">satta maranthu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="satta maranthu "/>
</div>
<div class="lyrico-lyrics-wrapper">paada thittam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paada thittam"/>
</div>
<div class="lyrico-lyrics-wrapper">konjam maranthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konjam maranthu"/>
</div>
<div class="lyrico-lyrics-wrapper">paravai petra 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paravai petra "/>
</div>
<div class="lyrico-lyrics-wrapper">kulanthaiyai pol 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulanthaiyai pol "/>
</div>
<div class="lyrico-lyrics-wrapper">paranthu paranthu kiliyathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paranthu paranthu kiliyathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">silu silu silu sil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="silu silu silu sil"/>
</div>
<div class="lyrico-lyrics-wrapper">kaathu veesuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaathu veesuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">sala sala sala sal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sala sala sala sal"/>
</div>
<div class="lyrico-lyrics-wrapper">oothu pesuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oothu pesuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">kala kala kala kal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kala kala kala kal"/>
</div>
<div class="lyrico-lyrics-wrapper">aaru ooduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaru ooduthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">odura paamba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="odura paamba"/>
</div>
<div class="lyrico-lyrics-wrapper">mithikuthu vayasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mithikuthu vayasu"/>
</div>
<div class="lyrico-lyrics-wrapper">uchchi vaanatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uchchi vaanatha"/>
</div>
<div class="lyrico-lyrics-wrapper">ilukura vayasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ilukura vayasu"/>
</div>
<div class="lyrico-lyrics-wrapper">unna vidava 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna vidava "/>
</div>
<div class="lyrico-lyrics-wrapper">ulagam perusu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulagam perusu"/>
</div>
<div class="lyrico-lyrics-wrapper">kalla thinna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalla thinna"/>
</div>
<div class="lyrico-lyrics-wrapper">serikuthu vayasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="serikuthu vayasu"/>
</div>
<div class="lyrico-lyrics-wrapper">kaatru veliyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatru veliyil"/>
</div>
<div class="lyrico-lyrics-wrapper">mithakuthu vayasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mithakuthu vayasu"/>
</div>
<div class="lyrico-lyrics-wrapper">siraga vidava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="siraga vidava"/>
</div>
<div class="lyrico-lyrics-wrapper">vaanam perusu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaanam perusu"/>
</div>
<div class="lyrico-lyrics-wrapper">kayiru kulla 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kayiru kulla "/>
</div>
<div class="lyrico-lyrics-wrapper">muduchu viluntha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muduchu viluntha"/>
</div>
<div class="lyrico-lyrics-wrapper">avilpathu romba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avilpathu romba"/>
</div>
<div class="lyrico-lyrics-wrapper">sulabam thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sulabam thaane"/>
</div>
<div class="lyrico-lyrics-wrapper">uyirukkula muduchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyirukkula muduchu"/>
</div>
<div class="lyrico-lyrics-wrapper">viluntha avilpathu romba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viluntha avilpathu romba"/>
</div>
<div class="lyrico-lyrics-wrapper">siramam thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="siramam thaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">silu silu silu sil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="silu silu silu sil"/>
</div>
<div class="lyrico-lyrics-wrapper">kaathu veesuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaathu veesuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">sala sala sala sal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sala sala sala sal"/>
</div>
<div class="lyrico-lyrics-wrapper">oothu pesuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oothu pesuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">kala kala kala kal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kala kala kala kal"/>
</div>
<div class="lyrico-lyrics-wrapper">aaru ooduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaru ooduthe"/>
</div>
</pre>
