---
title: "the liberation song lyrics"
album: "Mithai"
artist: "Vivek Sagar"
lyricist: "Kittu Vissapragada"
director: "Prashant Kumar"
path: "/albums/mithai-lyrics"
song: "The Liberation"
image: ../../images/albumart/mithai.jpg
date: 2019-02-22
lang: telugu
youtubeLink: "https://www.youtube.com/embed/mZkk-4pahS4"
type: "happy"
singers:
  - Anurag Kulkarni
  - Sivam
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Jindhagila Paisal Kosam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jindhagila Paisal Kosam"/>
</div>
<div class="lyrico-lyrics-wrapper">Gee Naukarilalla Visigi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gee Naukarilalla Visigi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nasigi Pothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nasigi Pothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nakki Nakki Mukki Moolgina Nanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nakki Nakki Mukki Moolgina Nanna"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandhu Dorikaakda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhu Dorikaakda"/>
</div>
<div class="lyrico-lyrics-wrapper">Chekkeddhaamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chekkeddhaamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sankela Thenche Baanisalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sankela Thenche Baanisalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Bathakanu Antu Padharaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bathakanu Antu Padharaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Paka Paka Navve Vedhavalake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paka Paka Navve Vedhavalake"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhandaal Pettakuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhandaal Pettakuraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sankela Thenche Baanisalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sankela Thenche Baanisalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Bathakanu Antu Padharaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bathakanu Antu Padharaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Paka Paka Navve Vedhavalake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paka Paka Navve Vedhavalake"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhandaal Pettakuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhandaal Pettakuraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haa Eh Chuthukula Brathukula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa Eh Chuthukula Brathukula"/>
</div>
<div class="lyrico-lyrics-wrapper">Katha Antha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katha Antha"/>
</div>
<div class="lyrico-lyrics-wrapper">Haa Nuv Vethikina Dorakani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa Nuv Vethikina Dorakani"/>
</div>
<div class="lyrico-lyrics-wrapper">Gathamantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gathamantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Haa Nuv Vadhilina Viluvala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa Nuv Vadhilina Viluvala"/>
</div>
<div class="lyrico-lyrics-wrapper">Vela Entha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vela Entha"/>
</div>
<div class="lyrico-lyrics-wrapper">Adigithe Badhuluga Nuve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adigithe Badhuluga Nuve"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhi Idhi Ani Nasuguthu Migalaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhi Idhi Ani Nasuguthu Migalaku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nerpali Paatam Naatho Raara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nerpali Paatam Naatho Raara"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheppey Gunapatam Doosukuporaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheppey Gunapatam Doosukuporaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Choodali Antham Ee Pootaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choodali Antham Ee Pootaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhurusuga Kadhalara Raaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhurusuga Kadhalara Raaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhure Thirigithe Ika Mundhevaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhure Thirigithe Ika Mundhevaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Gurine Ne Pedithe Migaladu Evadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gurine Ne Pedithe Migaladu Evadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eehana Yeshana Gathamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eehana Yeshana Gathamu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hathamidhi Ee Kshana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hathamidhi Ee Kshana"/>
</div>
<div class="lyrico-lyrics-wrapper">Siksha Mokshame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siksha Mokshame"/>
</div>
<div class="lyrico-lyrics-wrapper">Eehana Yeshana Gathamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eehana Yeshana Gathamu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hathamidhi Ee Kshana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hathamidhi Ee Kshana"/>
</div>
<div class="lyrico-lyrics-wrapper">Siksha Mokshame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siksha Mokshame"/>
</div>
<div class="lyrico-lyrics-wrapper">Eehana Yeshana Gathamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eehana Yeshana Gathamu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hathamidhi Ee Kshana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hathamidhi Ee Kshana"/>
</div>
<div class="lyrico-lyrics-wrapper">Siksha Mokshame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siksha Mokshame"/>
</div>
<div class="lyrico-lyrics-wrapper">Eehana Yeshana Gathamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eehana Yeshana Gathamu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hathamidhi Ee Kshana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hathamidhi Ee Kshana"/>
</div>
<div class="lyrico-lyrics-wrapper">Siksha Mokshame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siksha Mokshame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haa Ha Haa Haahaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa Ha Haa Haahaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Haa Ha Haa Haahaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa Ha Haa Haahaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yedhirinchi Veladham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhirinchi Veladham"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhirinchi Veladham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhirinchi Veladham"/>
</div>
<div class="lyrico-lyrics-wrapper">Bedhirinchi Kodadham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bedhirinchi Kodadham"/>
</div>
<div class="lyrico-lyrics-wrapper">Bedhirinchi Kodadham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bedhirinchi Kodadham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Evadiki Thala Vanchamu Memu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evadiki Thala Vanchamu Memu"/>
</div>
<div class="lyrico-lyrics-wrapper">Evadiki Thala Vanchamu Vanchamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evadiki Thala Vanchamu Vanchamu"/>
</div>
<div class="lyrico-lyrics-wrapper">Leki Bathukulu Bathakamu Ledhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Leki Bathukulu Bathakamu Ledhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Leki Bathukulu Bathakamu Bathakamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Leki Bathukulu Bathakamu Bathakamu"/>
</div>
<div class="lyrico-lyrics-wrapper">Leki Bathukulu Bathakamu Ledhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Leki Bathukulu Bathakamu Ledhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Leki Bathukulu Bathakamu Bathakamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Leki Bathukulu Bathakamu Bathakamu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kukkala Thokalu Oopina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kukkala Thokalu Oopina"/>
</div>
<div class="lyrico-lyrics-wrapper">Rojulu Poyinay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rojulu Poyinay"/>
</div>
<div class="lyrico-lyrics-wrapper">Peekalu Korukutham Korukutham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peekalu Korukutham Korukutham"/>
</div>
<div class="lyrico-lyrics-wrapper">Kukkala Thokalu Oopina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kukkala Thokalu Oopina"/>
</div>
<div class="lyrico-lyrics-wrapper">Rojulu Poyinay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rojulu Poyinay"/>
</div>
<div class="lyrico-lyrics-wrapper">Peekalu Korukutham Korukutham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peekalu Korukutham Korukutham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haa Eh Chuthukula Brathukula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa Eh Chuthukula Brathukula"/>
</div>
<div class="lyrico-lyrics-wrapper">Katha Antha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katha Antha"/>
</div>
<div class="lyrico-lyrics-wrapper">Haa Nuv Vethikina Dorakani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa Nuv Vethikina Dorakani"/>
</div>
<div class="lyrico-lyrics-wrapper">Gathamantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gathamantha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kukkala Thokalu Oopina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kukkala Thokalu Oopina"/>
</div>
<div class="lyrico-lyrics-wrapper">Rojulu Poyinay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rojulu Poyinay"/>
</div>
<div class="lyrico-lyrics-wrapper">Peekalu Korukutham Korukutham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peekalu Korukutham Korukutham"/>
</div>
<div class="lyrico-lyrics-wrapper">Kukkala Thokalu Oopina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kukkala Thokalu Oopina"/>
</div>
<div class="lyrico-lyrics-wrapper">Rojulu Poyinay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rojulu Poyinay"/>
</div>
<div class="lyrico-lyrics-wrapper">Peekalu Korukutham Korukutham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peekalu Korukutham Korukutham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thikkalo Bokkalu Narukuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thikkalo Bokkalu Narukuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dokkalu Chinchuthu Lekkalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dokkalu Chinchuthu Lekkalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thelchutham Thelchutham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thelchutham Thelchutham"/>
</div>
<div class="lyrico-lyrics-wrapper">Thikkalo Bokkalu Narukuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thikkalo Bokkalu Narukuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dokkalu Chinchuthu Lekkalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dokkalu Chinchuthu Lekkalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thelchutham Thelchutham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thelchutham Thelchutham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kukkala Thokalu Oopina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kukkala Thokalu Oopina"/>
</div>
<div class="lyrico-lyrics-wrapper">Rojulu Poyinay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rojulu Poyinay"/>
</div>
<div class="lyrico-lyrics-wrapper">Rojulu Poyinay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rojulu Poyinay"/>
</div>
<div class="lyrico-lyrics-wrapper">Poyinay Poyinay Poyinay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poyinay Poyinay Poyinay"/>
</div>
<div class="lyrico-lyrics-wrapper">Poyinay Poyinay Poyinay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poyinay Poyinay Poyinay"/>
</div>
<div class="lyrico-lyrics-wrapper">Poyinay Poyinay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poyinay Poyinay"/>
</div>
</pre>
