---
title: "kadalai poduran song lyrics"
album: "Yuvan Yuvathi"
artist: "Vijay Antony"
lyricist: "Annamalai"
director: "G.N.R. Kumaravelan"
path: "/albums/yuvan-yuvathi-lyrics"
song: "Kadalai Poduran"
image: ../../images/albumart/yuvan-yuvathi.jpg
date: 2011-08-26
lang: tamil
youtubeLink: "https://www.youtube.com/embed/37aBEFnEKiA"
type: "love"
singers:
  - Krishnan Mahesan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kadala Poduraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadala Poduraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Paiyan Kadala Poduraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paiyan Kadala Poduraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponnu Maattikkittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponnu Maattikkittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sakka Podu Pottu Thaakkuraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sakka Podu Pottu Thaakkuraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadala Poduraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadala Poduraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Paiyan Kadala Poduraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paiyan Kadala Poduraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponnu Maattikkittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponnu Maattikkittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sakka Podu Pottu Thaakkuraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sakka Podu Pottu Thaakkuraan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannula Paakkuraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannula Paakkuraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Paiyan Kannula Paakkuraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paiyan Kannula Paakkuraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha Saakkula Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Saakkula Dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponnukkitta Kadala Poduraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponnukkitta Kadala Poduraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannula Paakkuraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannula Paakkuraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Paiyan Kannula Paakkuraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paiyan Kannula Paakkuraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha Saakkula Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Saakkula Dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponnukkitta Kadala Poduraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponnukkitta Kadala Poduraan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Killaadi Yaar Kitta Poduraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Killaadi Yaar Kitta Poduraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Avala Suthi Valachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avala Suthi Valachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ipo Avan Vattam Poduraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ipo Avan Vattam Poduraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Muthu Kannil Ora Kannil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthu Kannil Ora Kannil"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponna Paakkuraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponna Paakkuraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava Paakkirappa Ivan Ippo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Paakkirappa Ivan Ippo"/>
</div>
<div class="lyrico-lyrics-wrapper">Manna Paakkuraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manna Paakkuraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Manna Paakkuraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manna Paakkuraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Illa Ponna Paakkuraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illa Ponna Paakkuraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Manna Paakkuraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manna Paakkuraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Manna Paakkuraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manna Paakkuraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Illa Ponna Paakkuraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illa Ponna Paakkuraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nonstop-ah Pesi Pesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nonstop-ah Pesi Pesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naakka Thekkiraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naakka Thekkiraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Fullstop-eh Illaamathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fullstop-eh Illaamathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pottu Thaakkuraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pottu Thaakkuraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Full Adicha Mabula Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Full Adicha Mabula Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Beachu Kaatha Medhakkuraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Beachu Kaatha Medhakkuraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thekkiraan Thekkiraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thekkiraan Thekkiraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Naakka Thekkiraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naakka Thekkiraan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Now Hit It On The Floor Now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Now Hit It On The Floor Now"/>
</div>
<div class="lyrico-lyrics-wrapper">Hit It On The Floor Now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hit It On The Floor Now"/>
</div>
<div class="lyrico-lyrics-wrapper">Hit It On The Floor Now Slap
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hit It On The Floor Now Slap"/>
</div>
<div class="lyrico-lyrics-wrapper">Let Me Walk It Now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Let Me Walk It Now"/>
</div>
<div class="lyrico-lyrics-wrapper">Let Me Walk It Now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Let Me Walk It Now"/>
</div>
<div class="lyrico-lyrics-wrapper">Let Me Walk It Now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Let Me Walk It Now"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadala Poduraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadala Poduraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Paiyan Kadala Poduraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paiyan Kadala Poduraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponnu Maattikkittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponnu Maattikkittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sakka Podu Pottu Thaakkuraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sakka Podu Pottu Thaakkuraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadala Poduraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadala Poduraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Paiyan Kadala Poduraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paiyan Kadala Poduraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponnu Maattikkittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponnu Maattikkittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sakka Podu Pottu Thaakkuraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sakka Podu Pottu Thaakkuraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh"/>
</div>
</pre>
