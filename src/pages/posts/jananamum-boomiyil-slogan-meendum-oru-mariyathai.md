---
title: "jananamum boomiyil - slogan song lyrics"
album: "Meendum Oru Mariyathai"
artist: "N. R. Raghunanthan - Sharran Surya - Yuvan Shankar Raja"
lyricist: "Vairamuthu"
director: "Bharathiraja"
path: "/albums/meendum-oru-mariyathai-lyrics"
song: "Jananamum boomiyil - slogan"
image: ../../images/albumart/meendum-oru-mariyathai.jpg
date: 2020-02-21
lang: tamil
youtubeLink: "https://www.youtube.com/embed/EV8SLKE28uM"
type: "melody"
singers:
  - Madhu Balakrishnan
  - Parvathy
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">ohm ohm ohm ohm 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ohm ohm ohm ohm "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">jananamum boomiyil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jananamum boomiyil "/>
</div>
<div class="lyrico-lyrics-wrapper">puthiyathu illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthiyathu illai"/>
</div>
<div class="lyrico-lyrics-wrapper">maranathai pol oru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maranathai pol oru"/>
</div>
<div class="lyrico-lyrics-wrapper">palayathum illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="palayathum illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">jananamum boomiyil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jananamum boomiyil "/>
</div>
<div class="lyrico-lyrics-wrapper">puthiyathu illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthiyathu illai"/>
</div>
<div class="lyrico-lyrics-wrapper">maranathai pol oru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maranathai pol oru"/>
</div>
<div class="lyrico-lyrics-wrapper">palayathum illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="palayathum illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vedham sollathathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vedham sollathathai"/>
</div>
<div class="lyrico-lyrics-wrapper">maranangal koorum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maranangal koorum"/>
</div>
<div class="lyrico-lyrics-wrapper">vidhai ondru veelthida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidhai ondru veelthida"/>
</div>
<div class="lyrico-lyrics-wrapper">sedi vanthu serum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sedi vanthu serum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vedham sollathathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vedham sollathathai"/>
</div>
<div class="lyrico-lyrics-wrapper">maranangal koorum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maranangal koorum"/>
</div>
<div class="lyrico-lyrics-wrapper">vidhai ondru veelthida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidhai ondru veelthida"/>
</div>
<div class="lyrico-lyrics-wrapper">sedi vanthu serum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sedi vanthu serum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">maandavar swasangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maandavar swasangal"/>
</div>
<div class="lyrico-lyrics-wrapper">kaatrudan serga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatrudan serga"/>
</div>
<div class="lyrico-lyrics-wrapper">thooyavar kannoli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thooyavar kannoli"/>
</div>
<div class="lyrico-lyrics-wrapper">suriyan serga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suriyan serga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">maandavar swasangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maandavar swasangal"/>
</div>
<div class="lyrico-lyrics-wrapper">kaatrudan serga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatrudan serga"/>
</div>
<div class="lyrico-lyrics-wrapper">thooyavar kannoli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thooyavar kannoli"/>
</div>
<div class="lyrico-lyrics-wrapper">suriyan serga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suriyan serga"/>
</div>
</pre>
