---
title: "slum anthem atti song lyrics"
album: "Kodiyil Oruvan"
artist: "	Nivas K. Prasanna - Harish arjun"
lyricist: "Super Subu"
director: "Ananda Krishnan"
path: "/albums/kodiyil-oruvan-lyrics"
song: "Slum Anthem Atti"
image: ../../images/albumart/kodiyil-oruvan.jpg
date: 2021-09-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/GI7GG378gyk"
type: "mass"
singers:
  - Gautham Vasudev Menon
  - Premgi Amaren
  - Vijay Antony
  - Nivas K. Prasanna
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Atti full-ah lit achu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atti full-ah lit achu"/>
</div>
<div class="lyrico-lyrics-wrapper">City-kulla fit achu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="City-kulla fit achu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ketta koottam trip achu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketta koottam trip achu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thatti ketta flip achu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thatti ketta flip achu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paththikkichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paththikkichu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Atti full-ah lit achu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atti full-ah lit achu"/>
</div>
<div class="lyrico-lyrics-wrapper">City-kulla fit achu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="City-kulla fit achu"/>
</div>
<div class="lyrico-lyrics-wrapper">Patti thotti hip achu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Patti thotti hip achu"/>
</div>
<div class="lyrico-lyrics-wrapper">Katti potta slip aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katti potta slip aachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Padchavan sketch-u hit achu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padchavan sketch-u hit achu"/>
</div>
<div class="lyrico-lyrics-wrapper">Morchavan gang-u veth aachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Morchavan gang-u veth aachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudcha koatta geth achi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudcha koatta geth achi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tha ini koluthura vediyellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tha ini koluthura vediyellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pala thappaattam nippattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala thappaattam nippattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee erakkura idiyellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee erakkura idiyellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Arap porattam undakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arap porattam undakkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tha ini koluthura vediyellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tha ini koluthura vediyellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pala thappaattam nippattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala thappaattam nippattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee erakkura idiyellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee erakkura idiyellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Arap porattam undakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arap porattam undakkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee yaaru?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee yaaru?"/>
</div>
<div class="lyrico-lyrics-wrapper">Goppana goppanu appana paathavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Goppana goppanu appana paathavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kombannu sonnavan kombellam odchavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kombannu sonnavan kombellam odchavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vambunnu vandhadhum solli yadichavan da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vambunnu vandhadhum solli yadichavan da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee yaaru?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee yaaru?"/>
</div>
<div class="lyrico-lyrics-wrapper">Slum-nu sirchavan gammunu pothikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Slum-nu sirchavan gammunu pothikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Dham katti nerikki nambuna makkalla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dham katti nerikki nambuna makkalla"/>
</div>
<div class="lyrico-lyrics-wrapper">Gummnnu yethittu jammunnu ninnavan da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gummnnu yethittu jammunnu ninnavan da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee yaaru?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee yaaru?"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee yaaru?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee yaaru?"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Atti full-ah lit achu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atti full-ah lit achu"/>
</div>
<div class="lyrico-lyrics-wrapper">City-kulla fit achu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="City-kulla fit achu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ketta koottam trip achu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketta koottam trip achu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thatti ketta flip achu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thatti ketta flip achu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Come on put your hands in the air
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Come on put your hands in the air"/>
</div>
<div class="lyrico-lyrics-wrapper">And then wave them here to there
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="And then wave them here to there"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ah put your shades and your blazer
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ah put your shades and your blazer"/>
</div>
<div class="lyrico-lyrics-wrapper">Drink up all night naatu sarakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Drink up all night naatu sarakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Move your base to the left
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Move your base to the left"/>
</div>
<div class="lyrico-lyrics-wrapper">And then move it to your right
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="And then move it to your right"/>
</div>
<div class="lyrico-lyrics-wrapper">Move your bum up and down
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Move your bum up and down"/>
</div>
<div class="lyrico-lyrics-wrapper">And then shake it all night all night
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="And then shake it all night all night"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Move your base to the left
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Move your base to the left"/>
</div>
<div class="lyrico-lyrics-wrapper">And then move it to your right
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="And then move it to your right"/>
</div>
<div class="lyrico-lyrics-wrapper">Move your bum up and down
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Move your bum up and down"/>
</div>
<div class="lyrico-lyrics-wrapper">And then shake it all night all night long
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="And then shake it all night all night long"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pori thattichunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pori thattichunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiri paththichunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiri paththichunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuri sikkichunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuri sikkichunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Nari thappikkuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nari thappikkuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hehe thada pottanungo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hehe thada pottanungo"/>
</div>
<div class="lyrico-lyrics-wrapper">Vada suttanungo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vada suttanungo"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma onnanadhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma onnanadhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vittu essaagi ponango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vittu essaagi ponango"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanunnum neeyunnum murkinnu ninnana ka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanunnum neeyunnum murkinnu ninnana ka"/>
</div>
<div class="lyrico-lyrics-wrapper">Naayam kedachiruma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naayam kedachiruma"/>
</div>
<div class="lyrico-lyrics-wrapper">Naamellam team-u’nu game aada kathukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naamellam team-u’nu game aada kathukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalikki nammoda kodi parakkanum dei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalikki nammoda kodi parakkanum dei"/>
</div>
<div class="lyrico-lyrics-wrapper">Dei, venam dei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dei, venam dei"/>
</div>
<div class="lyrico-lyrics-wrapper">Nammaanda dei vachikkadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nammaanda dei vachikkadhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaarachum lord aattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarachum lord aattam"/>
</div>
<div class="lyrico-lyrics-wrapper">Scene-ah potta, o***la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Scene-ah potta, o***la"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma wardaanda ittunu vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma wardaanda ittunu vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yevanachum doulatha sound-u vutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yevanachum doulatha sound-u vutta"/>
</div>
<div class="lyrico-lyrics-wrapper">Avnaanda hip hop-u beat box-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avnaanda hip hop-u beat box-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Locking-u popping-u b-boy-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Locking-u popping-u b-boy-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Boxing-u styling-u painting-u kabaddi graffitti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boxing-u styling-u painting-u kabaddi graffitti"/>
</div>
<div class="lyrico-lyrics-wrapper">Foot ball-u food truck-u carrom board-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Foot ball-u food truck-u carrom board-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaana aattatha kaattinu vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaana aattatha kaattinu vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee yaaru?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee yaaru?"/>
</div>
<div class="lyrico-lyrics-wrapper">Goppana goppanu appana paathavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Goppana goppanu appana paathavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kombannu sonnavan kombellam odchavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kombannu sonnavan kombellam odchavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vambunnu vandhadhum solli yadichavan da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vambunnu vandhadhum solli yadichavan da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee yaaru?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee yaaru?"/>
</div>
<div class="lyrico-lyrics-wrapper">Slum-nu sirchavan gammunu pothikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Slum-nu sirchavan gammunu pothikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Dham katti nerikki nambuna makkalla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dham katti nerikki nambuna makkalla"/>
</div>
<div class="lyrico-lyrics-wrapper">Gummnnu yethittu jammunnu ninnavan da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gummnnu yethittu jammunnu ninnavan da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee yaaru?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee yaaru?"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Who are you?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Who are you?"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Johnny johnny yes papa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Johnny johnny yes papa"/>
</div>
<div class="lyrico-lyrics-wrapper">Attikku gate-u no papa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Attikku gate-u no papa"/>
</div>
<div class="lyrico-lyrics-wrapper">Right-ku fight-u yes papa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Right-ku fight-u yes papa"/>
</div>
<div class="lyrico-lyrics-wrapper">Night-ku tight-u no papa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Night-ku tight-u no papa"/>
</div>
<div class="lyrico-lyrics-wrapper">Koottaiku route-u yes papa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koottaiku route-u yes papa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhudhukku vote-u no papa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhudhukku vote-u no papa"/>
</div>
<div class="lyrico-lyrics-wrapper">Paththikkichu vettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paththikkichu vettu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ha ha ha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ha ha ha"/>
</div>
<div class="lyrico-lyrics-wrapper">Hee hee hee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hee hee hee"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo hoo hoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo hoo hoo"/>
</div>
</pre>
