---
title: "yevevo yevevo song lyrics"
album: "Jai Simha"
artist: "Chirantan Bhatt"
lyricist: "Bhaskarbhatla"
director: "K S Ravikumar"
path: "/albums/jai-simha-lyrics"
song: "Yevevo Yevevo"
image: ../../images/albumart/jai-simha.jpg
date: 2018-01-12
lang: telugu
youtubeLink: "https://www.youtube.com/embed/3js2KmmR0Ok"
type: "love"
singers:
  - Revanth
  - Shreya Ghoshal
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Evevo evevo cheppalanipisthundi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evevo evevo cheppalanipisthundi"/>
</div>
<div class="lyrico-lyrics-wrapper">tholisari naa manase vippalanipisthundi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tholisari naa manase vippalanipisthundi"/>
</div>
<div class="lyrico-lyrics-wrapper">Evevo evevo cheppalanipisthundi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evevo evevo cheppalanipisthundi"/>
</div>
<div class="lyrico-lyrics-wrapper">nidurinche nee kalalo raavalanipisthundi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nidurinche nee kalalo raavalanipisthundi"/>
</div>
<div class="lyrico-lyrics-wrapper">thu mera dil mera dil mera dil o mera dil mera dil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thu mera dil mera dil mera dil o mera dil mera dil"/>
</div>
<div class="lyrico-lyrics-wrapper">Evevo evevo cheppalanipisthundi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evevo evevo cheppalanipisthundi"/>
</div>
<div class="lyrico-lyrics-wrapper">tholisari naa manase vippalanipisthundi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tholisari naa manase vippalanipisthundi"/>
</div>
<div class="lyrico-lyrics-wrapper">thu mera dil mera dil mera dil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thu mera dil mera dil mera dil"/>
</div>
<div class="lyrico-lyrics-wrapper">thu mera dil mera dil mera dil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thu mera dil mera dil mera dil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thuge thuge paadam neevalle aagindi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuge thuge paadam neevalle aagindi"/>
</div>
<div class="lyrico-lyrics-wrapper">nuvve vachi cheyyandisthe paruge theesinde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvve vachi cheyyandisthe paruge theesinde"/>
</div>
<div class="lyrico-lyrics-wrapper">aage aage pranam neevalle odindi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aage aage pranam neevalle odindi"/>
</div>
<div class="lyrico-lyrics-wrapper">theerachalani nenanukunna ee runame theeranidi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theerachalani nenanukunna ee runame theeranidi"/>
</div>
<div class="lyrico-lyrics-wrapper">jeevithana mallela vaana ipude kurisinde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jeevithana mallela vaana ipude kurisinde"/>
</div>
<div class="lyrico-lyrics-wrapper">thu mera dil mera dil mera dil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thu mera dil mera dil mera dil"/>
</div>
<div class="lyrico-lyrics-wrapper">thu mera dil mera dil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thu mera dil mera dil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hoho Evvaraina chupagalara thamala praanananni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoho Evvaraina chupagalara thamala praanananni"/>
</div>
<div class="lyrico-lyrics-wrapper">ne chupisthe naalo pranam idigo nuvvu ani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ne chupisthe naalo pranam idigo nuvvu ani"/>
</div>
<div class="lyrico-lyrics-wrapper">okkaraina chudagalara thadime oopirini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="okkaraina chudagalara thadime oopirini"/>
</div>
<div class="lyrico-lyrics-wrapper">naa oopiriki rupam isthe acham nuvvanani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa oopiriki rupam isthe acham nuvvanani"/>
</div>
<div class="lyrico-lyrics-wrapper">apurupamga daachana nuvvichina bahumathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="apurupamga daachana nuvvichina bahumathi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">o mera dil mera dil mera dil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="o mera dil mera dil mera dil"/>
</div>
<div class="lyrico-lyrics-wrapper">hey thu mera dil mera dil mera dil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey thu mera dil mera dil mera dil"/>
</div>
<div class="lyrico-lyrics-wrapper">Evevo evevo cheppalanipisthundi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evevo evevo cheppalanipisthundi"/>
</div>
<div class="lyrico-lyrics-wrapper">tholisari naa manase vippalanipisthundi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tholisari naa manase vippalanipisthundi"/>
</div>
<div class="lyrico-lyrics-wrapper">thu mera dil mera dil mera dil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thu mera dil mera dil mera dil"/>
</div>
<div class="lyrico-lyrics-wrapper">thu mera dil mera dil mera dil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thu mera dil mera dil mera dil"/>
</div>
<div class="lyrico-lyrics-wrapper">thu mera dil mera dil mera dil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thu mera dil mera dil mera dil"/>
</div>
</pre>
