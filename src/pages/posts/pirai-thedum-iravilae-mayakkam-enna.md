---
title: "pirai thedum iravilae song lyrics"
album: "Mayakkam Enna"
artist: "G.V. Prakash Kumar"
lyricist: "Dhanush"
director: "Selvaraghavan"
path: "/albums/mayakkam-enna-lyrics"
song: "Pirai Thedum Iravilae"
image: ../../images/albumart/mayakkam-enna.jpg
date: 2011-11-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/QmHX0whk6Rg"
type: "melody"
singers:
  - G.V. Prakash Kumar
  - Saindhavi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Pirai Thedum Iravilae Uyirae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirai Thedum Iravilae Uyirae"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhai Thedi Alaigiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhai Thedi Alaigiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhai Solla Azhaikiren Uyirae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhai Solla Azhaikiren Uyirae"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbae Nee Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbae Nee Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irulil Kanneerum Edharku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irulil Kanneerum Edharku"/>
</div>
<div class="lyrico-lyrics-wrapper">Madiyil Kan Mooda Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madiyil Kan Mooda Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagae Intha Sogam Edharku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagae Intha Sogam Edharku"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Un Thaayum Allava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Un Thaayum Allava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unakkena Mattum Vaazhum Idhayam Adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkena Mattum Vaazhum Idhayam Adi"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir Ulla Varai Naan Un Adimaiyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Ulla Varai Naan Un Adimaiyadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pirai Thedum Iravilae Uyirae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirai Thedum Iravilae Uyirae"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhai Thedi Alaigiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhai Thedi Alaigiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhai Solla Azhaikiren Uyirae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhai Solla Azhaikiren Uyirae"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbae Nee Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbae Nee Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhuthaal Un Paarvaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhuthaal Un Paarvaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayanthaal Un Kaalgalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayanthaal Un Kaalgalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhikaalaiyin Koodalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhikaalaiyin Koodalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Sogam Theerkkum Podhuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sogam Theerkkum Podhuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nizhal Thedidum Aanmaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nizhal Thedidum Aanmaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nijam Thedidum Penmaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijam Thedidum Penmaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Porvaiyil Vaazhum Inbam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Porvaiyil Vaazhum Inbam"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheivam Thantha Sonthama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheivam Thantha Sonthama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Aayul Reghai Neeyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Aayul Reghai Neeyadi"/>
</div>
<div class="lyrico-lyrics-wrapper">En Aani Veradi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Aani Veradi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sumai Thaangum Endhan Kanmani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sumai Thaangum Endhan Kanmani"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Sudum Pani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Sudum Pani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unakkena Mattum Vaazhum Idhayamadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkena Mattum Vaazhum Idhayamadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirulla Varai Naan Un Adimaiyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirulla Varai Naan Un Adimaiyadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pirai Thedum Iravilae Uyirae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirai Thedum Iravilae Uyirae"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhai Thedi Alaigiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhai Thedi Alaigiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhai Solla Azhaikiren Uyirae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhai Solla Azhaikiren Uyirae"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbae Nee Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbae Nee Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhiyin Antha Thedalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhiyin Antha Thedalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Alaiyum Undhan Nenjamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alaiyum Undhan Nenjamum"/>
</div>
<div class="lyrico-lyrics-wrapper">Purinthalae Podhumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Purinthalae Podhumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yezhu Jenmam Thaanguven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yezhu Jenmam Thaanguven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anal Melae Vaazhgiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anal Melae Vaazhgiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadhi Polae Paaigiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadhi Polae Paaigiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Kaaranam Illaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Kaaranam Illaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Meesai Vaitha Pillaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meesai Vaitha Pillaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhai Kaadhal Endru Solvatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhai Kaadhal Endru Solvatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nizhal Kaainthu Kolvatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nizhal Kaainthu Kolvatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinam Kollum Intha Bhoomiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam Kollum Intha Bhoomiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Varam Tharum Idam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Varam Tharum Idam"/>
</div>
</pre>
