---
title: "mazhai varum neram song lyrics"
album: "Thulam"
artist: "Alex Premnath"
lyricist: "Na Muthu Kumar"
director: "Rajanagajothi"
path: "/albums/thulam-lyrics"
song: "Mazhai Varum Neram"
image: ../../images/albumart/thulam.jpg
date: 2018-12-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/463am4ldxtU"
type: "love"
singers:
  - Saindhavi
  - G V Prakash Kumar 
  - P Gopala Krishnan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Mazhai varum neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai varum neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai ninaithen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai ninaithen"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhai nindra pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai nindra pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mella nanaidhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mella nanaidhen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhayaththin arai indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayaththin arai indru"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirakkirathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirakkirathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kaaladi suvadugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kaaladi suvadugal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ketkkirathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketkkirathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyirukkul un kural
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirukkul un kural"/>
</div>
<div class="lyrico-lyrics-wrapper">Olikkirathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Olikkirathae"/>
</div>
<div class="lyrico-lyrics-wrapper">En nizhalukkum un mugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En nizhalukkum un mugam"/>
</div>
<div class="lyrico-lyrics-wrapper">Therigirathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Therigirathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaattru ulla kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattru ulla kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhuvaraikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhuvaraikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal indha mannil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal indha mannil"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilaithirukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilaithirukkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mazhai varum neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai varum neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai ninaithen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai ninaithen"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhai nindra pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai nindra pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mella nanaidhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mella nanaidhen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninaivinil neethaan naalthorum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaivinil neethaan naalthorum"/>
</div>
<div class="lyrico-lyrics-wrapper">Varugiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varugiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavilum pennae neengaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavilum pennae neengaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thodargiraaiaaaii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodargiraaiaaaii"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nerukkaththil ennai theemootta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nerukkaththil ennai theemootta"/>
</div>
<div class="lyrico-lyrics-wrapper">Varugiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varugiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Irundhum yen konjam idaiveli nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irundhum yen konjam idaiveli nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidugiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidugiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manasukkul thayakanggal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasukkul thayakanggal"/>
</div>
<div class="lyrico-lyrics-wrapper">Irukkirathae yeyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irukkirathae yeyi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thayakkaththai mayakkanggal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thayakkaththai mayakkanggal"/>
</div>
<div class="lyrico-lyrics-wrapper">Jeyikkirathaeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jeyikkirathaeye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varthaiyil mounangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varthaiyil mounangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Velgirathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velgirathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mounamum kaadhalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mounamum kaadhalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Solgirathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solgirathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanamulla kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanamulla kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhuvaraikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhuvaraikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal indha mannil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal indha mannil"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilaithirukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilaithirukkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mazhai varum neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai varum neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai ninaithen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai ninaithen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru murai ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru murai ennai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee paarththaal kothikkuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee paarththaal kothikkuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Marumurai pennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marumurai pennae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee paarththaal kuliruthae hae ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee paarththaal kuliruthae hae ae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un madi saayum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un madi saayum"/>
</div>
<div class="lyrico-lyrics-wrapper">Inneram pidikkuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inneram pidikkuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnudan pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnudan pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadhai thaan inikkudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadhai thaan inikkudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaaiyena vaazhkaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaiyena vaazhkaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhu vittaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhu vittaai"/>
</div>
<div class="lyrico-lyrics-wrapper">En thanimaikku thanimaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En thanimaikku thanimaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanthu vittaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanthu vittaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Udhiraththil muzhudhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udhiraththil muzhudhaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalandhuvittaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalandhuvittaai"/>
</div>
<div class="lyrico-lyrics-wrapper">En penmaiyai poovaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En penmaiyai poovaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Malaravittaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malaravittaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Boomi ulla kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boomi ulla kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu varaikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu varaikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal indha mannil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal indha mannil"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilaithirukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilaithirukkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mazhai varum neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai varum neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai ninaithen aen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai ninaithen aen"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhai nindra pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai nindra pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mella nanaidhenhaenn
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mella nanaidhenhaenn"/>
</div>
</pre>
