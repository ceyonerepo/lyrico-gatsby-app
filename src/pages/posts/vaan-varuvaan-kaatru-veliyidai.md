---
title: "vaan varuvaan song lyrics"
album: "Kaatru Veliyidai"
artist: "A R Rahman"
lyricist: "Vairamuthu"
director: "Mani Ratnam"
path: "/albums/kaatru-veliyidai-lyrics"
song: "Vaan Varuvaan"
image: ../../images/albumart/kaatru-veliyidai.jpg
date: 2017-04-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/2JVg8j9w7Mg"
type: "love"
singers:
  -	Shashaa Tirupati
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vaan varuvaan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaan varuvaan "/>
</div>
<div class="lyrico-lyrics-wrapper">varuvaan varuvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varuvaan varuvaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaan varuvaan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaan varuvaan "/>
</div>
<div class="lyrico-lyrics-wrapper">varuvaan varuvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varuvaan varuvaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanvaruvaan vaanvaruvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanvaruvaan vaanvaruvaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanvaruvaan vaanvaruvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanvaruvaan vaanvaruvaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaan varuvaan thoduvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaan varuvaan thoduvaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhai pol vizhuvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai pol vizhuvaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Marmam arivaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marmam arivaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennul olivaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennul olivaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Arugae nimirvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arugae nimirvaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tholaivil panivaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholaivil panivaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Garvam kondal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Garvam kondal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallai uraivaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallai uraivaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallai uraivaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallai uraivaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Uraivaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uraivaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal vandhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal vandhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaniyaai negizhvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaniyaai negizhvaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal vandhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal vandhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaniyaai negizhvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaniyaai negizhvaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En kalla kaamuganae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kalla kaamuganae"/>
</div>
<div class="lyrico-lyrics-wrapper">Avandhaan varuvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avandhaan varuvaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaan varuvaan thoduvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaan varuvaan thoduvaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhai pol vizhuvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai pol vizhuvaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Marmam arivaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marmam arivaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennul olivaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennul olivaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Arugae nimirvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arugae nimirvaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tholaivil panivaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholaivil panivaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Garvam kondal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Garvam kondal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallai uraivaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallai uraivaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallai uraivaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallai uraivaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Uraivaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uraivaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Tha tara tha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tha tara tha "/>
</div>
<div class="lyrico-lyrics-wrapper">tara tha tara-hoiii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tara tha tara-hoiii"/>
</div>
<div class="lyrico-lyrics-wrapper">Tha tara ohoo tha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tha tara ohoo tha "/>
</div>
<div class="lyrico-lyrics-wrapper">tara tha tara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tara tha tara"/>
</div>
<div class="lyrico-lyrics-wrapper">Tha tara tha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tha tara tha "/>
</div>
<div class="lyrico-lyrics-wrapper">tara tha tara-hoiii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tara tha tara-hoiii"/>
</div>
<div class="lyrico-lyrics-wrapper">Tha tara ohoo tha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tha tara ohoo tha "/>
</div>
<div class="lyrico-lyrics-wrapper">tara tha tara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tara tha tara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennodu irundhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennodu irundhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Evalo ninaivaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evalo ninaivaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Avanodu irundhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avanodu irundhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaiyae ninaivaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaiyae ninaivaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai thuravaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai thuravaan"/>
</div>
<div class="lyrico-lyrics-wrapper">En per maravaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En per maravaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai marandhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai marandhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Than uyir viduvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Than uyir viduvaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kan kavizhndhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan kavizhndhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Veli pol virivaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veli pol virivaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kan thirandhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan thirandhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanaththil karaivaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanaththil karaivaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaan varuvaan thoduvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaan varuvaan thoduvaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhai pol vizhuvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai pol vizhuvaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Marmam arivaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marmam arivaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennul olivaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennul olivaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Arugae nimirvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arugae nimirvaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tholaivil panivaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholaivil panivaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Garvam kondal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Garvam kondal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallai uraivaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallai uraivaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallai uraivaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallai uraivaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Uraivaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uraivaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal vandhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal vandhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaniyaai negizhvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaniyaai negizhvaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal vandhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal vandhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaniyaai negizhvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaniyaai negizhvaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal vandhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal vandhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaniyaai negizhvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaniyaai negizhvaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal vandhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal vandhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaniyaai negizhvaan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaniyaai negizhvaan "/>
</div>
</pre>
