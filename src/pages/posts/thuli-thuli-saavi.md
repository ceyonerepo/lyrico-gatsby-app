---
title: "thuli thuli song lyrics"
album: "Saavi"
artist: "Sathish Thaianban"
lyricist: "Arivumathi - Sathish Thaianban - Nandalala"
director: "R Subramanian"
path: "/albums/saavi-lyrics"
song: "Thuli Thuli"
image: ../../images/albumart/saavi.jpg
date: 2018-01-05
lang: tamil
youtubeLink: "https://www.youtube.com/embed/2dGdwqya91s"
type: "love"
singers:
  - Vijay Yesudas
  - Priyanka
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">thuli thuli vizhi thuli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thuli thuli vizhi thuli"/>
</div>
<div class="lyrico-lyrics-wrapper">thelithathum kojam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thelithathum kojam "/>
</div>
<div class="lyrico-lyrics-wrapper">kulirthatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulirthatho"/>
</div>
<div class="lyrico-lyrics-wrapper">vizhi vizhi aval vazhi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vizhi vizhi aval vazhi "/>
</div>
<div class="lyrico-lyrics-wrapper">azhaithathum nenjam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="azhaithathum nenjam "/>
</div>
<div class="lyrico-lyrics-wrapper">sirithatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sirithatho"/>
</div>
<div class="lyrico-lyrics-wrapper">oli thuli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oli thuli"/>
</div>
<div class="lyrico-lyrics-wrapper">imai thulaithathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="imai thulaithathum"/>
</div>
<div class="lyrico-lyrics-wrapper">nizhale indru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nizhale indru "/>
</div>
<div class="lyrico-lyrics-wrapper">nirangalil nananthatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nirangalil nananthatho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thuli thuli vizhi thuli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thuli thuli vizhi thuli"/>
</div>
<div class="lyrico-lyrics-wrapper">thelithathum kojam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thelithathum kojam "/>
</div>
<div class="lyrico-lyrics-wrapper">kulirthatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulirthatho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">en thol meethu nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en thol meethu nee"/>
</div>
<div class="lyrico-lyrics-wrapper">poomaalai sootida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poomaalai sootida"/>
</div>
<div class="lyrico-lyrics-wrapper">nee en netri mel 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee en netri mel "/>
</div>
<div class="lyrico-lyrics-wrapper">senthooram poosida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="senthooram poosida"/>
</div>
<div class="lyrico-lyrics-wrapper">mangalyam thanthai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mangalyam thanthai "/>
</div>
<div class="lyrico-lyrics-wrapper">manbura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manbura"/>
</div>
<div class="lyrico-lyrics-wrapper">oru mutham thanthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru mutham thanthen"/>
</div>
<div class="lyrico-lyrics-wrapper">nee poothida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee poothida"/>
</div>
<div class="lyrico-lyrics-wrapper">un paathathil naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un paathathil naan"/>
</div>
<div class="lyrico-lyrics-wrapper">orr metti pootida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="orr metti pootida"/>
</div>
<div class="lyrico-lyrics-wrapper">athil yena inbamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athil yena inbamo"/>
</div>
<div class="lyrico-lyrics-wrapper">unn kaal meethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unn kaal meethu"/>
</div>
<div class="lyrico-lyrics-wrapper">nan kai vaithu vendida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan kai vaithu vendida"/>
</div>
<div class="lyrico-lyrics-wrapper">imai neengum thunbamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="imai neengum thunbamo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thuli thuli vizhi thuli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thuli thuli vizhi thuli"/>
</div>
<div class="lyrico-lyrics-wrapper">thelithathum kojam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thelithathum kojam "/>
</div>
<div class="lyrico-lyrics-wrapper">kulirthatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulirthatho"/>
</div>
<div class="lyrico-lyrics-wrapper">vizhi vizhi aval vazhi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vizhi vizhi aval vazhi "/>
</div>
<div class="lyrico-lyrics-wrapper">azhaithathum nenjam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="azhaithathum nenjam "/>
</div>
<div class="lyrico-lyrics-wrapper">sirithatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sirithatho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nan un thozhil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan un thozhil "/>
</div>
<div class="lyrico-lyrics-wrapper">nee en marbil saidhida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee en marbil saidhida"/>
</div>
<div class="lyrico-lyrics-wrapper">nee en kundhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee en kundhal"/>
</div>
<div class="lyrico-lyrics-wrapper">nan un nenjil kodhida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan un nenjil kodhida"/>
</div>
<div class="lyrico-lyrics-wrapper">ekantham nalai engida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ekantham nalai engida"/>
</div>
<div class="lyrico-lyrics-wrapper">oor kantham pole
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oor kantham pole"/>
</div>
<div class="lyrico-lyrics-wrapper">ne parthida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ne parthida"/>
</div>
<div class="lyrico-lyrics-wrapper">ne engendru nan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ne engendru nan"/>
</div>
<div class="lyrico-lyrics-wrapper">angangu thedida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="angangu thedida"/>
</div>
<div class="lyrico-lyrics-wrapper">adhil enna inbamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adhil enna inbamo"/>
</div>
<div class="lyrico-lyrics-wrapper">nan thai endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan thai endru"/>
</div>
<div class="lyrico-lyrics-wrapper">nee sei endru koodida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee sei endru koodida"/>
</div>
<div class="lyrico-lyrics-wrapper">adhil theerum thunbamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adhil theerum thunbamo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thuli thuli vizhi thuli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thuli thuli vizhi thuli"/>
</div>
<div class="lyrico-lyrics-wrapper">thelithathum kadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thelithathum kadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">thulirthatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thulirthatho"/>
</div>
<div class="lyrico-lyrics-wrapper">vizhi vizhi aval vazhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vizhi vizhi aval vazhi"/>
</div>
<div class="lyrico-lyrics-wrapper">azhaithathum nenjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="azhaithathum nenjam"/>
</div>
<div class="lyrico-lyrics-wrapper">silirthatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="silirthatho"/>
</div>
<div class="lyrico-lyrics-wrapper">oli thuli imai thulaithathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oli thuli imai thulaithathum"/>
</div>
<div class="lyrico-lyrics-wrapper">pudhire indru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pudhire indru "/>
</div>
<div class="lyrico-lyrics-wrapper">vidai enna purindhatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidai enna purindhatho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thuli thuli vizhi thuli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thuli thuli vizhi thuli"/>
</div>
<div class="lyrico-lyrics-wrapper">thelithathum kadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thelithathum kadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">thulirthatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thulirthatho"/>
</div>
</pre>
