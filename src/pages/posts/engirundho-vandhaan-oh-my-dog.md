---
title: "engirundho vandhaan song lyrics"
album: "Oh My Dog"
artist: "Nivas K Prasanna"
lyricist: "Mahakavi Bharathi - A Mohan Rajan"
director: "Sarov Shanmugam"
path: "/albums/oh-my-dog-lyrics"
song: "Engirundho Vandhaan"
image: ../../images/albumart/oh-my-dog.jpg
date: 2022-04-28
lang: tamil
youtubeLink: "https://www.youtube.com/embed/IJWmkRtE4jo"
type: "happy"
singers:
  - Nivas K Prasanna
  - Pradeep Kumar
  - Brindha Sivakumar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Engiruntho vanthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engiruntho vanthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Enathuyirai ingivanai yaar pera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enathuyirai ingivanai yaar pera"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna thavam seithen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna thavam seithen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engiruntho vanthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engiruntho vanthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Enathuyirai ingivanai yaar pera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enathuyirai ingivanai yaar pera"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna thavam seithen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna thavam seithen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sonnapadi ketpaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sonnapadi ketpaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunimanikal kaathiduvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunimanikal kaathiduvaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Aattangal kaatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aattangal kaatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Alukkaamal seithiduvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alukkaamal seithiduvaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaalaattum sei neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaalaattum sei neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaalaattum en thaay neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaalaattum en thaay neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">Un nandri arivene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un nandri arivene"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai ondru naan vaazhvene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai ondru naan vaazhvene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mozhiyillaa un pechil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mozhiyillaa un pechil"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai naanum marappene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai naanum marappene"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee pesum anbaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee pesum anbaale"/>
</div>
<div class="lyrico-lyrics-wrapper">En ulakam puthithaakum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En ulakam puthithaakum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Panbile deivamaay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panbile deivamaay"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarvaile sevakanaay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarvaile sevakanaay"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanbanumaay nallaasiriyanumaay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanbanumaay nallaasiriyanumaay"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engiruntho vanthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engiruntho vanthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Enathuyire kanne imai irandum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enathuyire kanne imai irandum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaappathu pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaappathu pol"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppothum unnai naan kaathiruppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppothum unnai naan kaathiruppen"/>
</div>
</pre>
