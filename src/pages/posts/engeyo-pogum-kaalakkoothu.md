---
title: "engeyo pogum song lyrics"
album: "Kaalakkoothu"
artist: "Justin Prabhakaran"
lyricist: "Snehan"
director: "M. Nagarajan"
path: "/albums/kaalakkoothu-lyrics"
song: "Engeyo Pogum"
image: ../../images/albumart/kaalakkoothu.jpg
date: 2018-05-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/u8aLrWFGWpw"
type: "melody"
singers:
  - Sriram Parthasarathy
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aaararo aaararo aaararo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaararo aaararo aaararo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ooooo ooooo ohoooo oooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooooo ooooo ohoooo oooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engeyo pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engeyo pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Megangal polae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megangal polae"/>
</div>
<div class="lyrico-lyrics-wrapper">Payanangal povom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Payanangal povom"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha boomi meethilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha boomi meethilae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ini enna nerum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini enna nerum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhkaiku inghae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkaiku inghae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidai solvaar yaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidai solvaar yaaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Iru jeevan yenguthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iru jeevan yenguthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veyilukku nizhalaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veyilukku nizhalaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhai peithal kudaiyaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai peithal kudaiyaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar vanthu nammai servaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar vanthu nammai servaaro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thol sainthu kolla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thol sainthu kolla"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru thunai endru solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru thunai endru solla"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha bantham mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha bantham mattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakkam ullathae…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkam ullathae…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ethirpaarppu illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethirpaarppu illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Siru yemaatram illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru yemaatram illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru payanam ponaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru payanam ponaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Deivam ullathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Deivam ullathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engeyo pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engeyo pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Megangal polae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megangal polae"/>
</div>
<div class="lyrico-lyrics-wrapper">Payanangal povom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Payanangal povom"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha boomi meethilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha boomi meethilae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaararo Aaararo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaararo Aaararo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ullangkaal regai pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullangkaal regai pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Ottiyae vaazhum intha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ottiyae vaazhum intha"/>
</div>
<div class="lyrico-lyrics-wrapper">Uravukku aayul ennavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uravukku aayul ennavo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kallukkul sirpam pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallukkul sirpam pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannukkul theppam pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannukkul theppam pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Natpukkul karpam ullatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natpukkul karpam ullatho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadavulgal boomikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadavulgal boomikku"/>
</div>
<div class="lyrico-lyrics-wrapper">Varukindra oru thethi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varukindra oru thethi"/>
</div>
<div class="lyrico-lyrics-wrapper">Marakkaamal kelungal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marakkaamal kelungal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithai patri oru sethi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithai patri oru sethi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nilai illa vaazhvil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilai illa vaazhvil"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethu nilaiyaanathendru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethu nilaiyaanathendru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada yaar thaan ingae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada yaar thaan ingae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nyayam solvaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nyayam solvaaro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaanal neer polae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanal neer polae"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinam kalainthodum vaazhvil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam kalainthodum vaazhvil"/>
</div>
<div class="lyrico-lyrics-wrapper">Verum natpai kondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verum natpai kondu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhvai velvaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvai velvaaro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engeyo pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engeyo pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Megangal polae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megangal polae"/>
</div>
<div class="lyrico-lyrics-wrapper">Payanangal povom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Payanangal povom"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha boomi meethilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha boomi meethilae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oooo oooo ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oooo oooo ooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaarodu yaarum ingae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarodu yaarum ingae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunai poga nerum bothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunai poga nerum bothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Undaakum uravum ithu thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undaakum uravum ithu thaanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ezhuthatha vithigal endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhuthatha vithigal endru"/>
</div>
<div class="lyrico-lyrics-wrapper">Evar vandhu solvaar indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evar vandhu solvaar indru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu enna nizhala nijamthaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu enna nizhala nijamthaanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanneero punnagaiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneero punnagaiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellamae sari paathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellamae sari paathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar solli thanthaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar solli thanthaaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagaana puthu neethi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaana puthu neethi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru kodi sontham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru kodi sontham"/>
</div>
<div class="lyrico-lyrics-wrapper">Athil pala kodi bantham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athil pala kodi bantham"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivai ellam ingae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivai ellam ingae"/>
</div>
<div class="lyrico-lyrics-wrapper">Poiyaai ponathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poiyaai ponathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ethu vantha pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethu vantha pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nam natpondrae pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam natpondrae pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ena kangal moodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ena kangal moodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Payanam poguthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Payanam poguthae"/>
</div>
</pre>
