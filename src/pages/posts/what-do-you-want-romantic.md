---
title: "what do you want song lyrics"
album: "Romantic"
artist: "Sunil Kasyap"
lyricist: "Bhaskarabahtla"
director: "Anil Paduri"
path: "/albums/romantic-lyrics"
song: "What Do You Want"
image: ../../images/albumart/romantic.jpg
date: 2021-10-29
lang: telugu
youtubeLink: "https://www.youtube.com/embed/vZ5cQaJu_Tc"
type: "happy"
singers:
  - Mangli
  - Krishna
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">in loudonkoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="in loudonkoo"/>
</div>
<div class="lyrico-lyrics-wrapper">clarity nahi hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="clarity nahi hai"/>
</div>
<div class="lyrico-lyrics-wrapper">ham ladiky onkuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ham ladiky onkuu"/>
</div>
<div class="lyrico-lyrics-wrapper">kya chahiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kya chahiye"/>
</div>
<div class="lyrico-lyrics-wrapper">maloom nahi hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maloom nahi hai"/>
</div>
<div class="lyrico-lyrics-wrapper">hey baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey baby"/>
</div>
<div class="lyrico-lyrics-wrapper">what do you want
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="what do you want"/>
</div>
<div class="lyrico-lyrics-wrapper">hey ekkadikelthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey ekkadikelthe"/>
</div>
<div class="lyrico-lyrics-wrapper">akkadiskosthav
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="akkadiskosthav"/>
</div>
<div class="lyrico-lyrics-wrapper">rasukupusuky
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rasukupusuky"/>
</div>
<div class="lyrico-lyrics-wrapper">tirigesthuntav
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tirigesthuntav"/>
</div>
<div class="lyrico-lyrics-wrapper">kalloki kuda vachesthav
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalloki kuda vachesthav"/>
</div>
<div class="lyrico-lyrics-wrapper">nee ayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee ayya"/>
</div>
<div class="lyrico-lyrics-wrapper">what do you want
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="what do you want"/>
</div>
<div class="lyrico-lyrics-wrapper">hey what do you want
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey what do you want"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">hey naku telsu andangunta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey naku telsu andangunta"/>
</div>
<div class="lyrico-lyrics-wrapper">ayithe matram nikentantu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ayithe matram nikentantu"/>
</div>
<div class="lyrico-lyrics-wrapper">tellarithe munduntav
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tellarithe munduntav"/>
</div>
<div class="lyrico-lyrics-wrapper">nee abba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee abba"/>
</div>
<div class="lyrico-lyrics-wrapper">what do you want
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="what do you want"/>
</div>
<div class="lyrico-lyrics-wrapper">hey what do you want
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey what do you want"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vee choopule naa vipune
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vee choopule naa vipune"/>
</div>
<div class="lyrico-lyrics-wrapper">ala touch chesthu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ala touch chesthu "/>
</div>
<div class="lyrico-lyrics-wrapper">guchesthunnave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="guchesthunnave"/>
</div>
<div class="lyrico-lyrics-wrapper">ne upire na gundalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ne upire na gundalo"/>
</div>
<div class="lyrico-lyrics-wrapper">entasalu materuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="entasalu materuu"/>
</div>
<div class="lyrico-lyrics-wrapper">oyy oyy oyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oyy oyy oyy"/>
</div>
<div class="lyrico-lyrics-wrapper">datuthunde meteru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="datuthunde meteru"/>
</div>
<div class="lyrico-lyrics-wrapper">oyy oyy oyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oyy oyy oyy"/>
</div>
<div class="lyrico-lyrics-wrapper">entasalu materuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="entasalu materuu"/>
</div>
<div class="lyrico-lyrics-wrapper">datuthunde meteru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="datuthunde meteru"/>
</div>
<div class="lyrico-lyrics-wrapper">em eraganattu teliyanatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="em eraganattu teliyanatu"/>
</div>
<div class="lyrico-lyrics-wrapper">mandisthave heateruu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mandisthave heateruu"/>
</div>
<div class="lyrico-lyrics-wrapper">galu galu galupensthunavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="galu galu galupensthunavu"/>
</div>
<div class="lyrico-lyrics-wrapper">pul thote nuv lesthunavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pul thote nuv lesthunavu"/>
</div>
<div class="lyrico-lyrics-wrapper">what do you want
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="what do you want"/>
</div>
<div class="lyrico-lyrics-wrapper">hey what do you want
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey what do you want"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">laakkoleka pikkoleka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="laakkoleka pikkoleka"/>
</div>
<div class="lyrico-lyrics-wrapper">tega chasthunde 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tega chasthunde "/>
</div>
<div class="lyrico-lyrics-wrapper">naa pranam ninnu chusi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa pranam ninnu chusi"/>
</div>
<div class="lyrico-lyrics-wrapper">em cheyyalo cheppochuga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="em cheyyalo cheppochuga"/>
</div>
<div class="lyrico-lyrics-wrapper">ala mingela chustave raakasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ala mingela chustave raakasi"/>
</div>
<div class="lyrico-lyrics-wrapper">chaalu chaalu taggaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chaalu chaalu taggaro"/>
</div>
<div class="lyrico-lyrics-wrapper">oyy oyy oyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oyy oyy oyy"/>
</div>
<div class="lyrico-lyrics-wrapper">dimpamaaku muggulo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dimpamaaku muggulo"/>
</div>
<div class="lyrico-lyrics-wrapper">oyy oyy oyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oyy oyy oyy"/>
</div>
<div class="lyrico-lyrics-wrapper">chaalu chaalu taggaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chaalu chaalu taggaro"/>
</div>
<div class="lyrico-lyrics-wrapper">dimpamaaku muggulo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dimpamaaku muggulo"/>
</div>
<div class="lyrico-lyrics-wrapper">em televanatu thosinave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="em televanatu thosinave"/>
</div>
<div class="lyrico-lyrics-wrapper">andam ane aggilo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="andam ane aggilo"/>
</div>
<div class="lyrico-lyrics-wrapper">ekada ekada sei vesthunav
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ekada ekada sei vesthunav"/>
</div>
<div class="lyrico-lyrics-wrapper">epadi kepudu try chesthunav
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="epadi kepudu try chesthunav"/>
</div>
<div class="lyrico-lyrics-wrapper">rathiri igga thigalendra paagal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rathiri igga thigalendra paagal"/>
</div>
<div class="lyrico-lyrics-wrapper">what do you want
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="what do you want"/>
</div>
<div class="lyrico-lyrics-wrapper">hey what do you want
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey what do you want"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">hey ekkadikelthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey ekkadikelthe"/>
</div>
<div class="lyrico-lyrics-wrapper">akkadiskosthav
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="akkadiskosthav"/>
</div>
<div class="lyrico-lyrics-wrapper">rasukupusuky
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rasukupusuky"/>
</div>
<div class="lyrico-lyrics-wrapper">tirigesthuntav
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tirigesthuntav"/>
</div>
<div class="lyrico-lyrics-wrapper">kalloki kuda vachesthav
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalloki kuda vachesthav"/>
</div>
<div class="lyrico-lyrics-wrapper">nee ayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee ayya"/>
</div>
<div class="lyrico-lyrics-wrapper">what do you want
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="what do you want"/>
</div>
<div class="lyrico-lyrics-wrapper">hey what do you want
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey what do you want"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">hey naku telsu andangunta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey naku telsu andangunta"/>
</div>
<div class="lyrico-lyrics-wrapper">ayithe matram nikentantu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ayithe matram nikentantu"/>
</div>
<div class="lyrico-lyrics-wrapper">tellarithe munduntav
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tellarithe munduntav"/>
</div>
<div class="lyrico-lyrics-wrapper">nee abba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee abba"/>
</div>
<div class="lyrico-lyrics-wrapper">what do you want
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="what do you want"/>
</div>
<div class="lyrico-lyrics-wrapper">hey what do you want
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey what do you want"/>
</div>
</pre>
