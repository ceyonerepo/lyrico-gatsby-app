---
title: "thelaga kottudhamma song lyrics"
album: "Nanjupuram"
artist: "Raaghav"
lyricist: "Magudeshwaran"
director: "Charles"
path: "/albums/nanjupuram-lyrics"
song: "Thelaga Kottudhamma"
image: ../../images/albumart/nanjupuram.jpg
date: 2011-04-01
lang: tamil
youtubeLink: "https://www.youtube.com/embed/GcMjflIpiME"
type: "love"
singers:
  - Archith
  - Preetha S
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Thelaaga kottudhamma 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thelaaga kottudhamma "/>
</div>
<div class="lyrico-lyrics-wrapper">dhegatha kaadhal koduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhegatha kaadhal koduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">naalaaga theeraadha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naalaaga theeraadha "/>
</div>
<div class="lyrico-lyrics-wrapper">noai onnu thandhu enakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="noai onnu thandhu enakku"/>
</div>
<div class="lyrico-lyrics-wrapper">eval kolli malaiyil vilaindha mooligai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eval kolli malaiyil vilaindha mooligai"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjai kallam kabadangalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjai kallam kabadangalai"/>
</div>
<div class="lyrico-lyrics-wrapper">vella pazhagavaitha koagigai en dhevathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vella pazhagavaitha koagigai en dhevathai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thelaaga kottudhamma 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thelaaga kottudhamma "/>
</div>
<div class="lyrico-lyrics-wrapper">dhegatha kaadhal koduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhegatha kaadhal koduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">naalaaga theeraadha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naalaaga theeraadha "/>
</div>
<div class="lyrico-lyrics-wrapper">noai onnu thandhu enakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="noai onnu thandhu enakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uravenum azhagiya kadhiravan pularum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uravenum azhagiya kadhiravan pularum"/>
</div>
<div class="lyrico-lyrics-wrapper">ayirinul urangiya siru vidhai malarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ayirinul urangiya siru vidhai malarum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mangai nenjai thulaithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mangai nenjai thulaithu"/>
</div>
<div class="lyrico-lyrics-wrapper">pongum anbai jeyithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pongum anbai jeyithu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pulangalai arindhathu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pulangalai arindhathu "/>
</div>
<div class="lyrico-lyrics-wrapper">mudhal una arindhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudhal una arindhen"/>
</div>
<div class="lyrico-lyrics-wrapper">thanimaiyil irundhumey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanimaiyil irundhumey "/>
</div>
<div class="lyrico-lyrics-wrapper">amaidhiyai tholaithen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="amaidhiyai tholaithen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poadhaadhathu indha yaagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poadhaadhathu indha yaagam"/>
</div>
<div class="lyrico-lyrics-wrapper">oorppaartha pin enna koorum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oorppaartha pin enna koorum"/>
</div>
<div class="lyrico-lyrics-wrapper">seeraanadhu panikkaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seeraanadhu panikkaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">theedum karam thoadavendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theedum karam thoadavendum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu poiyaakka koodaadhu poo nenjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu poiyaakka koodaadhu poo nenjam"/>
</div>
<div class="lyrico-lyrics-wrapper">thaangaadhamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaangaadhamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yedhedho ennam anbey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhedho ennam anbey"/>
</div>
<div class="lyrico-lyrics-wrapper">naalangal thandhi adikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naalangal thandhi adikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neelaadha paadhaikkulla nam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelaadha paadhaikkulla nam "/>
</div>
<div class="lyrico-lyrics-wrapper">kaadhal mella nadakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadhal mella nadakka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thelaaga kottudhamma 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thelaaga kottudhamma "/>
</div>
<div class="lyrico-lyrics-wrapper">dhegatha kaadhal koduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhegatha kaadhal koduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">naalaaga theeraadha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naalaaga theeraadha "/>
</div>
<div class="lyrico-lyrics-wrapper">noai onnu thandhu enakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="noai onnu thandhu enakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Padugira pasiyil thalir udal vizhundhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padugira pasiyil thalir udal vizhundhen"/>
</div>
<div class="lyrico-lyrics-wrapper">vaanavil aaginen mananalam izhandhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaanavil aaginen mananalam izhandhen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu sella chinungal unnai kaadhal gunangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu sella chinungal unnai kaadhal gunangal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paruvathin pudhirgalai purindhida munaindhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paruvathin pudhirgalai purindhida munaindhen"/>
</div>
<div class="lyrico-lyrics-wrapper">paravasa chaaralil anudhinam nanaindhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paravasa chaaralil anudhinam nanaindhen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanoadu megangal yaavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanoadu megangal yaavum"/>
</div>
<div class="lyrico-lyrics-wrapper">vaazhthukkal nammeedhu paadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaazhthukkal nammeedhu paadum"/>
</div>
<div class="lyrico-lyrics-wrapper">meenoadu kadal vatrakkoodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meenoadu kadal vatrakkoodum"/>
</div>
<div class="lyrico-lyrics-wrapper">yugam thaandi nam kaadhal vaazhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yugam thaandi nam kaadhal vaazhum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha kaatraalai poovukkul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha kaatraalai poovukkul"/>
</div>
<div class="lyrico-lyrics-wrapper">karuvandu kirukki ezhudhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karuvandu kirukki ezhudhum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thelaaga kottudhamma 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thelaaga kottudhamma "/>
</div>
<div class="lyrico-lyrics-wrapper">dhegatha kaadhal koduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhegatha kaadhal koduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">naalaaga theeraadha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naalaaga theeraadha "/>
</div>
<div class="lyrico-lyrics-wrapper">noai onnu thandhu enakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="noai onnu thandhu enakku"/>
</div>
</pre>
