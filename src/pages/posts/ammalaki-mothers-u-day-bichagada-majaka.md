---
title: "ammalaki mothers u day song lyrics"
album: "Bichagada Majaka"
artist: "Sri Venkat"
lyricist: "B. Chandra Sekhar (Pedda Babu)"
director: "K.S. Nageswara Rao"
path: "/albums/bichagada-majaka-lyrics"
song: "Ammalaki Mothers U Day"
image: ../../images/albumart/bichagada-majaka.jpg
date: 2019-02-01
lang: telugu
youtubeLink: "https://www.youtube.com/embed/yKTJxK0tX9g"
type: "happy"
singers:
  - Babu Mohan
  - Venky
  - Balu Nagaraju
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">velaki velu karsupati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="velaki velu karsupati"/>
</div>
<div class="lyrico-lyrics-wrapper">bokkala pantu kontaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bokkala pantu kontaru"/>
</div>
<div class="lyrico-lyrics-wrapper">daani torni jeanfu antaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="daani torni jeanfu antaru"/>
</div>
<div class="lyrico-lyrics-wrapper">kurralu mee kurrallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kurralu mee kurrallu"/>
</div>
<div class="lyrico-lyrics-wrapper">mari memu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mari memu"/>
</div>
<div class="lyrico-lyrics-wrapper">bokkala chokka esukuni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bokkala chokka esukuni"/>
</div>
<div class="lyrico-lyrics-wrapper">dharumam babu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dharumam babu"/>
</div>
<div class="lyrico-lyrics-wrapper">dharmam antu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dharmam antu"/>
</div>
<div class="lyrico-lyrics-wrapper">velaki velu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="velaki velu"/>
</div>
<div class="lyrico-lyrics-wrapper">sampadhinche bichagaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sampadhinche bichagaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">memu bichagaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="memu bichagaalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">amalaki mothers u day
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="amalaki mothers u day"/>
</div>
<div class="lyrico-lyrics-wrapper">nannalaki fathers u day
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nannalaki fathers u day"/>
</div>
<div class="lyrico-lyrics-wrapper">premikulaki overs u day
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="premikulaki overs u day"/>
</div>
<div class="lyrico-lyrics-wrapper">maaku kaavali beggers u day
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maaku kaavali beggers u day"/>
</div>
<div class="lyrico-lyrics-wrapper">kaavali kaavali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaavali kaavali"/>
</div>
<div class="lyrico-lyrics-wrapper">beggers u day
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="beggers u day"/>
</div>
<div class="lyrico-lyrics-wrapper">kaavali beggers u day
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaavali beggers u day"/>
</div>
<div class="lyrico-lyrics-wrapper">annalaki namasthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="annalaki namasthe"/>
</div>
<div class="lyrico-lyrics-wrapper">baaba yilu namasthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="baaba yilu namasthe"/>
</div>
<div class="lyrico-lyrics-wrapper">meekandhariki namasthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meekandhariki namasthe"/>
</div>
<div class="lyrico-lyrics-wrapper">maarandi maarandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maarandi maarandi"/>
</div>
<div class="lyrico-lyrics-wrapper">meeku namasthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meeku namasthe"/>
</div>
<div class="lyrico-lyrics-wrapper">beggingu maanandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="beggingu maanandi"/>
</div>
<div class="lyrico-lyrics-wrapper">meeku namasthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meeku namasthe"/>
</div>
<div class="lyrico-lyrics-wrapper">bhavathi bikshaan dhehi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bhavathi bikshaan dhehi"/>
</div>
<div class="lyrico-lyrics-wrapper">ten rupees dharmam cheyyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ten rupees dharmam cheyyi"/>
</div>
<div class="lyrico-lyrics-wrapper">ee prapanchame maadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ee prapanchame maadi"/>
</div>
<div class="lyrico-lyrics-wrapper">memu leni place edhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="memu leni place edhi"/>
</div>
<div class="lyrico-lyrics-wrapper">pani paata cheyanollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pani paata cheyanollu"/>
</div>
<div class="lyrico-lyrics-wrapper">ee bhoomiki bharam meeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ee bhoomiki bharam meeru"/>
</div>
<div class="lyrico-lyrics-wrapper">beggerse leni desam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="beggerse leni desam"/>
</div>
<div class="lyrico-lyrics-wrapper">prapanchanike aadarsam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="prapanchanike aadarsam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sree ramunde 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sree ramunde "/>
</div>
<div class="lyrico-lyrics-wrapper">devudni chesina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="devudni chesina"/>
</div>
<div class="lyrico-lyrics-wrapper">gahnatha maadi kaadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gahnatha maadi kaadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">aa gahnatha maadi kaadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aa gahnatha maadi kaadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">a devullatho chelagatama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="a devullatho chelagatama"/>
</div>
<div class="lyrico-lyrics-wrapper">chee poraa poraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chee poraa poraa"/>
</div>
<div class="lyrico-lyrics-wrapper">ha ha ha ha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ha ha ha ha"/>
</div>
<div class="lyrico-lyrics-wrapper">chee chee chee poraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chee chee chee poraa"/>
</div>
<div class="lyrico-lyrics-wrapper">raama raavana yudamlo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="raama raavana yudamlo"/>
</div>
<div class="lyrico-lyrics-wrapper">gelupu evaridanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gelupu evaridanta"/>
</div>
<div class="lyrico-lyrics-wrapper">veera paraakarama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veera paraakarama"/>
</div>
<div class="lyrico-lyrics-wrapper">kodanda raamudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kodanda raamudu"/>
</div>
<div class="lyrico-lyrics-wrapper">geliche chivarikanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="geliche chivarikanta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">raama raavana yudamlo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="raama raavana yudamlo"/>
</div>
<div class="lyrico-lyrics-wrapper">gelupu evaridanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gelupu evaridanta"/>
</div>
<div class="lyrico-lyrics-wrapper">veera paraakarama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veera paraakarama"/>
</div>
<div class="lyrico-lyrics-wrapper">kodanda raamudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kodanda raamudu"/>
</div>
<div class="lyrico-lyrics-wrapper">geliche chivarikanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="geliche chivarikanta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">maa bichagadi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maa bichagadi "/>
</div>
<div class="lyrico-lyrics-wrapper">getaplo seetammanu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="getaplo seetammanu "/>
</div>
<div class="lyrico-lyrics-wrapper">ravanudu ettukupoye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ravanudu ettukupoye"/>
</div>
<div class="lyrico-lyrics-wrapper">bichagadi mulamgane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bichagadi mulamgane"/>
</div>
<div class="lyrico-lyrics-wrapper">raamudu devudaiphaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="raamudu devudaiphaye"/>
</div>
<div class="lyrico-lyrics-wrapper">sree raamudu devudu aypaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sree raamudu devudu aypaye"/>
</div>
<div class="lyrico-lyrics-wrapper">bhavathi bikshaan dhehi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bhavathi bikshaan dhehi"/>
</div>
<div class="lyrico-lyrics-wrapper">ten rupees dharmam cheyyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ten rupees dharmam cheyyi"/>
</div>
<div class="lyrico-lyrics-wrapper">ee prapanchame maadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ee prapanchame maadi"/>
</div>
<div class="lyrico-lyrics-wrapper">memu leni place edhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="memu leni place edhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">a kasmir to kanyakumari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="a kasmir to kanyakumari"/>
</div>
<div class="lyrico-lyrics-wrapper">railu bandi free maaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="railu bandi free maaku"/>
</div>
<div class="lyrico-lyrics-wrapper">railu bandi free maaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="railu bandi free maaku"/>
</div>
<div class="lyrico-lyrics-wrapper">a manushuluga meeru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="a manushuluga meeru "/>
</div>
<div class="lyrico-lyrics-wrapper">puttaaru gaani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puttaaru gaani"/>
</div>
<div class="lyrico-lyrics-wrapper">manishila brathakaleru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manishila brathakaleru"/>
</div>
<div class="lyrico-lyrics-wrapper">meeru manishila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meeru manishila"/>
</div>
<div class="lyrico-lyrics-wrapper">bratakaleru  
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bratakaleru  "/>
</div>
<div class="lyrico-lyrics-wrapper">aasthuleni maakunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aasthuleni maakunna"/>
</div>
<div class="lyrico-lyrics-wrapper">income tax ledanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="income tax ledanta"/>
</div>
<div class="lyrico-lyrics-wrapper">mee pai income tax ride 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mee pai income tax ride "/>
</div>
<div class="lyrico-lyrics-wrapper">maaku siggu chetanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maaku siggu chetanta"/>
</div>
<div class="lyrico-lyrics-wrapper">rupayine dharmam cheste
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rupayine dharmam cheste"/>
</div>
<div class="lyrico-lyrics-wrapper">paapaalanni pogadatham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paapaalanni pogadatham"/>
</div>
<div class="lyrico-lyrics-wrapper">papam punyam devudikeruka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="papam punyam devudikeruka"/>
</div>
<div class="lyrico-lyrics-wrapper">meeku bichamendukanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meeku bichamendukanta"/>
</div>
<div class="lyrico-lyrics-wrapper">bichagadu bichamadigithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bichagadu bichamadigithe"/>
</div>
<div class="lyrico-lyrics-wrapper">chee chee cheepo antaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chee chee cheepo antaru"/>
</div>
<div class="lyrico-lyrics-wrapper">bichagadu cinema vasthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bichagadu cinema vasthe"/>
</div>
<div class="lyrico-lyrics-wrapper">egabadi mari chustaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="egabadi mari chustaru"/>
</div>
<div class="lyrico-lyrics-wrapper">collection lu kuripistharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="collection lu kuripistharu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">bhavathi bikshaan dhehi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bhavathi bikshaan dhehi"/>
</div>
<div class="lyrico-lyrics-wrapper">ten rupees dharmam cheyyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ten rupees dharmam cheyyi"/>
</div>
<div class="lyrico-lyrics-wrapper">ee prapanchame maadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ee prapanchame maadi"/>
</div>
<div class="lyrico-lyrics-wrapper">memu leni place edhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="memu leni place edhi"/>
</div>
</pre>
