---
title: "kanne kanne song lyrics"
album: "Ayogya"
artist: "Sam C.S - S. Thaman"
lyricist: "Vivek"
director: "Venkat Mohan"
path: "/albums/ayogya-lyrics"
song: "Kanne Kanne"
image: ../../images/albumart/ayogya.jpg
date: 2019-05-11
lang: tamil
youtubeLink: "https://www.youtube.com/embed/J_IXCXqvPGI"
type: "happy"
singers:
  - Anirudh Ravichander
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kanne Kanne Unna Thookki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanne Kanne Unna Thookki"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanaa Thooram Pogattaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanaa Thooram Pogattaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaattu Jeevan Pola Thaavi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattu Jeevan Pola Thaavi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasai Ellaam Kekkattaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai Ellaam Kekkattaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanne Kanne Unna Thookki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanne Kanne Unna Thookki"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanaa Thooram Pogattaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanaa Thooram Pogattaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaattu Jeevan Pola Thaavi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattu Jeevan Pola Thaavi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasai Ellaam Kekkattaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai Ellaam Kekkattaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannae Kannae Unna Thookki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannae Kannae Unna Thookki"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanaa Dhooram Pogata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanaa Dhooram Pogata"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatu Jeevan Pola Thaavi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatu Jeevan Pola Thaavi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasai Ellaam Kekkattaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai Ellaam Kekkattaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Azhagiye Azhagiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Azhagiye Azhagiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Kannil Thinna Poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Kannil Thinna Poren"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Vedukkura Midukkula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vedukkura Midukkula"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Sinnaa Pinnam Aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Sinnaa Pinnam Aanen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ae Alagiye Alagiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Alagiye Alagiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Kannil Thinna Poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Kannil Thinna Poren"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Vedukkura Midukkula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vedukkura Midukkula"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Sinnaa Pinnam Aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Sinnaa Pinnam Aanen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennodu Nee Vanthu Seraamale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennodu Nee Vanthu Seraamale"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponakka En Nenju Thaangaathadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponakka En Nenju Thaangaathadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Yarodum Thonaatha Peraasa Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yarodum Thonaatha Peraasa Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Penne Unthan Mela Thonuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Penne Unthan Mela Thonuthadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanney Kanney Unna Thookki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanney Kanney Unna Thookki"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanaa Thooram Pogataa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanaa Thooram Pogataa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatu Jeevan Pola Thaavi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatu Jeevan Pola Thaavi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasai Ellaam Kekkattaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai Ellaam Kekkattaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanney Kanney Unna Thookki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanney Kanney Unna Thookki"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanaa Thooram Pogattaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanaa Thooram Pogattaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaattu Jeevan Pola Thaavi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattu Jeevan Pola Thaavi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasai Ellaam Kekkattaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai Ellaam Kekkattaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yela Yela Mayil Iragaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yela Yela Mayil Iragaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idha Idha Theriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idha Idha Theriyala"/>
</div>
<div class="lyrico-lyrics-wrapper">Pura Aana Puli Manasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pura Aana Puli Manasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaanu Puriyalaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaanu Puriyalaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Devathaiya Thara Irakki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Devathaiya Thara Irakki"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Pudikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Pudikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippo Nenachen Nenachen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo Nenachen Nenachen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unna Mulusa Ragasiyamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Mulusa Ragasiyamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Eduthukka Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eduthukka Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulla Thudichen Thudichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulla Thudichen Thudichen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yela Yela Mayil Iragaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yela Yela Mayil Iragaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idha Idha Theriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idha Idha Theriyala"/>
</div>
<div class="lyrico-lyrics-wrapper">Pura Aana Puli Manasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pura Aana Puli Manasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaanu Puriyalaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaanu Puriyalaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennodu Nee Vanthu Seraamalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennodu Nee Vanthu Seraamalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponaaka En Nenju Thaangaathadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponaaka En Nenju Thaangaathadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarodum Thonaatha Peraasa Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarodum Thonaatha Peraasa Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Penne Unthan Mela Thonuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Penne Unthan Mela Thonuthadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannae Kannae Unna Thookki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannae Kannae Unna Thookki"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanaa Thooram Pogattaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanaa Thooram Pogattaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaattu Jeevan Pola Thaavi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattu Jeevan Pola Thaavi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasai Ellaam Kekkattaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai Ellaam Kekkattaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannae Kannae Unna Thookki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannae Kannae Unna Thookki"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanaa Thooram Pogattaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanaa Thooram Pogattaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaattu Jeevan Pola Thaavi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattu Jeevan Pola Thaavi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasai Ellaam Kekkattaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai Ellaam Kekkattaa"/>
</div>
</pre>
