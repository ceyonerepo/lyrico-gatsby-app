---
title: "yathe yathe song lyrics"
album: "Aadukalam"
artist: "G. V. Prakash Kumar"
lyricist: "Snehan"
director: "Vetrimaran"
path: "/albums/aadukalam-lyrics"
song: "Yathe Yathe"
image: ../../images/albumart/aadukalam.jpg
date: 2011-01-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/knEAxQ00cTQ"
type: "love"
singers:
  - G. V. Prakash Kumar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yathe Yathe Yathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yathe Yathe Yathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaacho Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaacho Oh"/>
</div>
<div class="lyrico-lyrics-wrapper">Yathe Yathe Yathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yathe Yathe Yathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhacho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhacho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yathe Yathe Yathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yathe Yathe Yathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaacho Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaacho Oh"/>
</div>
<div class="lyrico-lyrics-wrapper">Yathe Yathe Yathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yathe Yathe Yathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhachoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhachoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meenkoththiya Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meenkoththiya Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Koththura Aala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Koththura Aala"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Vellaavi Vechchuthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Vellaavi Vechchuthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Veluthaangalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veluthaangalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Veyilukku Kaataama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Veyilukku Kaataama"/>
</div>
<div class="lyrico-lyrics-wrapper">Valathaangalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valathaangalaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Thalakaalu Puriyaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Thalakaalu Puriyaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharamela Nikkaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharamela Nikkaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadumaari Poneney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadumaari Poneney"/>
</div>
<div class="lyrico-lyrics-wrapper">Naaney Naaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naaney Naaney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yathe Yathe Yathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yathe Yathe Yathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennacho Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennacho Oh"/>
</div>
<div class="lyrico-lyrics-wrapper">Yathe Yathe Yathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yathe Yathe Yathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Yethachoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethachoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi Vellaavi Vechchuthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Vellaavi Vechchuthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Veluththaangalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veluththaangalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Illa Veyilukku Kaataama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illa Veyilukku Kaataama"/>
</div>
<div class="lyrico-lyrics-wrapper">Valaththaaingalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valaththaaingalaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Thalakaalu Puriyaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Thalakaalu Puriyaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharamela Nikkaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharamela Nikkaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadumaari Poneney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadumaari Poneney"/>
</div>
<div class="lyrico-lyrics-wrapper">Naaney Naaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naaney Naaney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Puyal Thotta Maramaagave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puyal Thotta Maramaagave"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalai Suththi Pogiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalai Suththi Pogiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Neerattra Nilamaagavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neerattra Nilamaagavey"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaagaththaal Kaaigiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaagaththaal Kaaigiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unai Thediye Manam Suththuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai Thediye Manam Suththuthey"/>
</div>
<div class="lyrico-lyrics-wrapper">Raa Koliyaa Thinam Kaththudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raa Koliyaa Thinam Kaththudhey"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir Naadiyil Payir Seigiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Naadiyil Payir Seigiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirupaarvaiyil Ennai Neigiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirupaarvaiyil Ennai Neigiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yathe Yathe Yathe Ennacho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yathe Yathe Yathe Ennacho"/>
</div>
<div class="lyrico-lyrics-wrapper">Yathe Yathe Yathe Edhachoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yathe Yathe Yathe Edhachoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi Sathigaari Ennadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Sathigaari Ennadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Senja Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senja Enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Sarugaagi Porene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Sarugaagi Porene"/>
</div>
<div class="lyrico-lyrics-wrapper">Paaththa Pinna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaththa Pinna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Thalakaalu Puriyaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Thalakaalu Puriyaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharamela Nikkaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharamela Nikkaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadumaari Poneney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadumaari Poneney"/>
</div>
<div class="lyrico-lyrics-wrapper">Naaney Naaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naaney Naaney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yathe Yathe Yathe Ennacho Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yathe Yathe Yathe Ennacho Oh"/>
</div>
<div class="lyrico-lyrics-wrapper">Yathey Yathey Yathe Edhachoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yathey Yathey Yathe Edhachoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi Nenju Analaagavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Nenju Analaagavey"/>
</div>
<div class="lyrico-lyrics-wrapper">Thee Alli Oothura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thee Alli Oothura"/>
</div>
<div class="lyrico-lyrics-wrapper">Nool Yethum Illamaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nool Yethum Illamaley"/>
</div>
<div class="lyrico-lyrics-wrapper">Usura Yen Kokura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usura Yen Kokura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Yenadi Vadham Seigiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Yenadi Vadham Seigiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Imai Naalilum Unai Vaikkiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imai Naalilum Unai Vaikkiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kada Vaayiley Ennai Melgiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kada Vaayiley Ennai Melgiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanjaadaiyil Ennai Kolgiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanjaadaiyil Ennai Kolgiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yathe Yathe Yathe Ennachcho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yathe Yathe Yathe Ennachcho"/>
</div>
<div class="lyrico-lyrics-wrapper">Yathe Yathe Yathe Yedhachoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yathe Yathe Yathe Yedhachoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Meenkotthiya Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meenkotthiya Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Koththura Aala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Koththura Aala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi Vellaavi Vechchuthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Vellaavi Vechchuthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Veluthaangalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veluthaangalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Illa Veyilukku Kaataama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illa Veyilukku Kaataama"/>
</div>
<div class="lyrico-lyrics-wrapper">Valathaangalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valathaangalaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Thalakaalu Puriyaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Thalakaalu Puriyaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharamela Nikkaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharamela Nikkaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadumaari Poneney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadumaari Poneney"/>
</div>
<div class="lyrico-lyrics-wrapper">Naaney Naaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naaney Naaney"/>
</div>
</pre>
