---
title: "un kannai partha song lyrics"
album: "Yuvan Yuvathi"
artist: "Vijay Antony"
lyricist: "Priyan"
director: "G.N.R. Kumaravelan"
path: "/albums/yuvan-yuvathi-lyrics"
song: "Un Kannai Partha"
image: ../../images/albumart/yuvan-yuvathi.jpg
date: 2011-08-26
lang: tamil
youtubeLink: "https://www.youtube.com/embed/m39FdWCkq_Q"
type: "love"
singers:
  - Karthik
  - Ramya NSK
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Un Kannai Partha Piragu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kannai Partha Piragu"/>
</div>
<div class="lyrico-lyrics-wrapper">En Ulle Latcham Siragu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Ulle Latcham Siragu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Ora Paarvai Asaivil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Ora Paarvai Asaivil"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam Kudai Saayum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam Kudai Saayum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Vandhu Pona Piragu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vandhu Pona Piragu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalaikelaai Maarum Ulagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaikelaai Maarum Ulagu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Illam Irukum Dhisayil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Illam Irukum Dhisayil"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiriyudhu En Paadham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiriyudhu En Paadham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanneeril Aadum Aalaiyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanneeril Aadum Aalaiyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaattrodu Midhakkum Illayaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattrodu Midhakkum Illayaai"/>
</div>
<div class="lyrico-lyrics-wrapper">En Manadhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Manadhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Maarukindradhe Unadhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maarukindradhe Unadhaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sila Neram Migavum Sugamaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sila Neram Migavum Sugamaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Sila Neram Migavum Summaiyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sila Neram Migavum Summaiyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aiyo Kaadhal Paduthu Kindradhye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyo Kaadhal Paduthu Kindradhye"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhidhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhidhaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Kannai Partha Piragu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kannai Partha Piragu"/>
</div>
<div class="lyrico-lyrics-wrapper">En Ulle Latcham Siragu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Ulle Latcham Siragu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Ora Paarvai Asaivil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Ora Paarvai Asaivil"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam Kudai Saayum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam Kudai Saayum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi Unnai Pola Pennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Unnai Pola Pennai"/>
</div>
<div class="lyrico-lyrics-wrapper">Engum Kandadhillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engum Kandadhillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Indru Varai En Manadhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indru Varai En Manadhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarume Eerthadhillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarume Eerthadhillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Wowu Wow Oh Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wowu Wow Oh Oh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Udhadu Endhan Perai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Udhadu Endhan Perai"/>
</div>
<div class="lyrico-lyrics-wrapper">Chollum Neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chollum Neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Silirkkiren Thavikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silirkkiren Thavikkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Envasam Naanum Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Envasam Naanum Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Wowu Wow Oh Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wowu Wow Oh Oh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mazhai Nindra Podhum Kilaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai Nindra Podhum Kilaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Siru Thooral Poduvadhu Pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru Thooral Poduvadhu Pol"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Kadandha Piragum Ninaivil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kadandha Piragum Ninaivil"/>
</div>
<div class="lyrico-lyrics-wrapper">Iruppaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruppaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Kannai Partha Piragu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kannai Partha Piragu"/>
</div>
<div class="lyrico-lyrics-wrapper">En Ulle Latcham Siragu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Ulle Latcham Siragu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Ora Paarvai Asaivil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Ora Paarvai Asaivil"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam Kudai Saayum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam Kudai Saayum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tat Tat Tat Tat
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tat Tat Tat Tat"/>
</div>
<div class="lyrico-lyrics-wrapper">Tat Tat Tat Tat
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tat Tat Tat Tat"/>
</div>
<div class="lyrico-lyrics-wrapper">Tat Tatat Tataara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tat Tatat Tataara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hu Paaba Paaba Paaba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hu Paaba Paaba Paaba"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Vittu Ullam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Vittu Ullam"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan Pinnaal Sellum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan Pinnaal Sellum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadukkiren Thavirkkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadukkiren Thavirkkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayamum Letkkavillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayamum Letkkavillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Wowu Wow Oh Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wowu Wow Oh Oh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Indru Pola Endrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Indru Pola Endrum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sokki Ponadhillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sokki Ponadhillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Idharkkumun Enakkindha Paravasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idharkkumun Enakkindha Paravasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Paindhadhillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paindhadhillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Wowu Wow Oh Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wowu Wow Oh Oh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Naetru Engu Irundhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Naetru Engu Irundhaai"/>
</div>
<div class="lyrico-lyrics-wrapper">En Nenjil Indru Nulaindhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nenjil Indru Nulaindhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini Naalai Enna Avasthai Purivaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Naalai Enna Avasthai Purivaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Kannai Partha Piragu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kannai Partha Piragu"/>
</div>
<div class="lyrico-lyrics-wrapper">En Ulle Latcham Siragu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Ulle Latcham Siragu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Ora Paarvai Asaivil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Ora Paarvai Asaivil"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam Kudai Saayum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam Kudai Saayum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Vandhu Pona Piragu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vandhu Pona Piragu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalaikeelaai Maarum Ulagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaikeelaai Maarum Ulagu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Illam Irukum Dhisayil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Illam Irukum Dhisayil"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiriyudhu En Paadham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiriyudhu En Paadham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanneeril Aadum Aalaiyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanneeril Aadum Aalaiyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaattrodu Midhakkum Illayaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattrodu Midhakkum Illayaai"/>
</div>
<div class="lyrico-lyrics-wrapper">En Manadhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Manadhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Maarukindradhe Unadhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maarukindradhe Unadhaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sila Neram Migavum Sugamaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sila Neram Migavum Sugamaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Sila Neram Migavum Summaiyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sila Neram Migavum Summaiyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Hey Hey Hey Hey Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Hey Hey Hey Hey Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Hey Hey Hey Hey Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Hey Hey Hey Hey Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Aiyo Kaadhal Paduthu Kindradhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyo Kaadhal Paduthu Kindradhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhidhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhidhaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Hey Hey Hey Hey Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Hey Hey Hey Hey Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Hey Hey Hey Hey Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Hey Hey Hey Hey Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Hey Hey Hey Hey Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Hey Hey Hey Hey Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Fe Un Kannai Partha Piragu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fe Un Kannai Partha Piragu"/>
</div>
<div class="lyrico-lyrics-wrapper">En Ulle Latcham Siragu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Ulle Latcham Siragu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Ora Paarvai Asaivil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Ora Paarvai Asaivil"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam Kudai Saayum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam Kudai Saayum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tat Tat Tat Tat
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tat Tat Tat Tat"/>
</div>
<div class="lyrico-lyrics-wrapper">Tat Tatat Tatara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tat Tatat Tatara"/>
</div>
<div class="lyrico-lyrics-wrapper">Tat Tat Tat Tat
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tat Tat Tat Tat"/>
</div>
<div class="lyrico-lyrics-wrapper">Tat Tatat Tatara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tat Tatat Tatara"/>
</div>
</pre>
