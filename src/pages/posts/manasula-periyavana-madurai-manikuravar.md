---
title: "manasula periyavana song lyrics"
album: "Madurai Manikuravar"
artist: "Isaignani Ilaiyaraaja"
lyricist: "Kavignar Muthulingam"
director: "K Raajarishi"
path: "/albums/madurai-manikuravar-lyrics"
song: "Manasula Periyavana"
image: ../../images/albumart/madurai-manikuravar.jpg
date: 2021-12-31
lang: tamil
youtubeLink: 
type: "happy"
singers:
  - Isaignani Dr. Ilaiyaraaja
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">manasula periyavanamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasula periyavanamma"/>
</div>
<div class="lyrico-lyrics-wrapper">madurai kaaran enaku oru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="madurai kaaran enaku oru"/>
</div>
<div class="lyrico-lyrics-wrapper">vali iruku athula poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vali iruku athula poren"/>
</div>
<div class="lyrico-lyrics-wrapper">manasula periyavanamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasula periyavanamma"/>
</div>
<div class="lyrico-lyrics-wrapper">madurai kaaran enaku oru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="madurai kaaran enaku oru"/>
</div>
<div class="lyrico-lyrics-wrapper">vali iruku athula poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vali iruku athula poren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ethilum thanuchu nikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethilum thanuchu nikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">madura madura than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="madura madura than"/>
</div>
<div class="lyrico-lyrics-wrapper">elaikinu eduthu tharum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="elaikinu eduthu tharum"/>
</div>
<div class="lyrico-lyrics-wrapper">gunathula aruma than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gunathula aruma than"/>
</div>
<div class="lyrico-lyrics-wrapper">anaikum inaikum enaikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anaikum inaikum enaikum"/>
</div>
<div class="lyrico-lyrics-wrapper">avanuku serum peruma than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avanuku serum peruma than"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">manasula periyavanamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasula periyavanamma"/>
</div>
<div class="lyrico-lyrics-wrapper">madurai kaaran enaku oru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="madurai kaaran enaku oru"/>
</div>
<div class="lyrico-lyrics-wrapper">vali iruku athula poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vali iruku athula poren"/>
</div>
<div class="lyrico-lyrics-wrapper">manasula periyavanamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasula periyavanamma"/>
</div>
<div class="lyrico-lyrics-wrapper">madurai kaaran enaku oru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="madurai kaaran enaku oru"/>
</div>
<div class="lyrico-lyrics-wrapper">vali iruku athula poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vali iruku athula poren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thodathin uyaratha than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thodathin uyaratha than"/>
</div>
<div class="lyrico-lyrics-wrapper">kopurangal kattume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kopurangal kattume"/>
</div>
<div class="lyrico-lyrics-wrapper">manathimi saalatha than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manathimi saalatha than"/>
</div>
<div class="lyrico-lyrics-wrapper">mandapangal katume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mandapangal katume"/>
</div>
<div class="lyrico-lyrics-wrapper">thodathin uyaratha than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thodathin uyaratha than"/>
</div>
<div class="lyrico-lyrics-wrapper">kopurangal kattume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kopurangal kattume"/>
</div>
<div class="lyrico-lyrics-wrapper">manathimi saalatha than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manathimi saalatha than"/>
</div>
<div class="lyrico-lyrics-wrapper">mandapangal katume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mandapangal katume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pittuku mannu sumaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pittuku mannu sumaka"/>
</div>
<div class="lyrico-lyrics-wrapper">parameswaran kaal eduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="parameswaran kaal eduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">vacha ooru da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vacha ooru da"/>
</div>
<div class="lyrico-lyrics-wrapper">kattungadangatha vellathal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kattungadangatha vellathal"/>
</div>
<div class="lyrico-lyrics-wrapper">avan aanaike kattu patta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avan aanaike kattu patta"/>
</div>
<div class="lyrico-lyrics-wrapper">vaigai aaru da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaigai aaru da"/>
</div>
<div class="lyrico-lyrics-wrapper">pittu vanicha anbukaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pittu vanicha anbukaga"/>
</div>
<div class="lyrico-lyrics-wrapper">patu pattan eesan avanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="patu pattan eesan avanum"/>
</div>
<div class="lyrico-lyrics-wrapper">avan avan avan petta pirambu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avan avan avan petta pirambu"/>
</div>
<div class="lyrico-lyrics-wrapper">adiyum sollum nalla pakthiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adiyum sollum nalla pakthiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">anbum thane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anbum thane"/>
</div>
<div class="lyrico-lyrics-wrapper">theiva sinthanaikum anthainaikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theiva sinthanaikum anthainaikum"/>
</div>
<div class="lyrico-lyrics-wrapper">vanthu nikkum makalin manneru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanthu nikkum makalin manneru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">manasula periyavanamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasula periyavanamma"/>
</div>
<div class="lyrico-lyrics-wrapper">madurai kaaran enaku oru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="madurai kaaran enaku oru"/>
</div>
<div class="lyrico-lyrics-wrapper">vali iruku athula poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vali iruku athula poren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nattu nadu nisiyinilum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nattu nadu nisiyinilum "/>
</div>
<div class="lyrico-lyrics-wrapper">idlium suttu tharum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idlium suttu tharum"/>
</div>
<div class="lyrico-lyrics-wrapper">thottu kolla nethini meenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thottu kolla nethini meenu"/>
</div>
<div class="lyrico-lyrics-wrapper">saaru vachu oothi vidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saaru vachu oothi vidum"/>
</div>
<div class="lyrico-lyrics-wrapper">vidiya vidiya muluchu irukum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidiya vidiya muluchu irukum"/>
</div>
<div class="lyrico-lyrics-wrapper">vella karan avanum kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vella karan avanum kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">thoonga nagaram nu thooki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoonga nagaram nu thooki"/>
</div>
<div class="lyrico-lyrics-wrapper">vachu ethiduvan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vachu ethiduvan"/>
</div>
<div class="lyrico-lyrics-wrapper">epavum thappu thanda 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="epavum thappu thanda "/>
</div>
<div class="lyrico-lyrics-wrapper">nadantha oorukulla thatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nadantha oorukulla thatti"/>
</div>
<div class="lyrico-lyrics-wrapper">keka munuku thane varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="keka munuku thane varum"/>
</div>
<div class="lyrico-lyrics-wrapper">pethavanga nenjatha than 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pethavanga nenjatha than "/>
</div>
<div class="lyrico-lyrics-wrapper">noga vaikum boomiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="noga vaikum boomiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">pethavala theivama 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pethavala theivama "/>
</div>
<div class="lyrico-lyrics-wrapper">than thuthikum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="than thuthikum "/>
</div>
<div class="lyrico-lyrics-wrapper">oru ealiyin vayithil aducha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru ealiyin vayithil aducha"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu ethuthu ketkum mannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu ethuthu ketkum mannu"/>
</div>
<div class="lyrico-lyrics-wrapper">aama aama aama aama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aama aama aama aama"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu vethu vettu sadam illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu vethu vettu sadam illa"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu veeram manakura mannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu veeram manakura mannu"/>
</div>
<div class="lyrico-lyrics-wrapper">solla anbum undu panbum undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="solla anbum undu panbum undu"/>
</div>
<div class="lyrico-lyrics-wrapper">andi vantha aatharavum undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="andi vantha aatharavum undu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">manasula periyavanamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasula periyavanamma"/>
</div>
<div class="lyrico-lyrics-wrapper">madurai kaaran enaku oru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="madurai kaaran enaku oru"/>
</div>
<div class="lyrico-lyrics-wrapper">vali iruku athula poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vali iruku athula poren"/>
</div>
<div class="lyrico-lyrics-wrapper">manasula periyavanamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasula periyavanamma"/>
</div>
<div class="lyrico-lyrics-wrapper">madurai kaaran enaku oru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="madurai kaaran enaku oru"/>
</div>
<div class="lyrico-lyrics-wrapper">vali iruku athula poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vali iruku athula poren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ethilum thanuchu nikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethilum thanuchu nikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">madura madura than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="madura madura than"/>
</div>
<div class="lyrico-lyrics-wrapper">ethilum thanuchu nikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethilum thanuchu nikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">madura madura than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="madura madura than"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">elaikinu eduthu tharum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="elaikinu eduthu tharum"/>
</div>
<div class="lyrico-lyrics-wrapper">gunathula aruma than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gunathula aruma than"/>
</div>
<div class="lyrico-lyrics-wrapper">anaikum inaikum enaikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anaikum inaikum enaikum"/>
</div>
<div class="lyrico-lyrics-wrapper">avanuku serum peruma than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avanuku serum peruma than"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">manasula periyavanamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasula periyavanamma"/>
</div>
<div class="lyrico-lyrics-wrapper">madurai kaaran enaku oru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="madurai kaaran enaku oru"/>
</div>
<div class="lyrico-lyrics-wrapper">vali iruku athula poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vali iruku athula poren"/>
</div>
<div class="lyrico-lyrics-wrapper">manasula periyavanamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasula periyavanamma"/>
</div>
<div class="lyrico-lyrics-wrapper">madurai kaaran enaku oru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="madurai kaaran enaku oru"/>
</div>
<div class="lyrico-lyrics-wrapper">vali iruku athula poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vali iruku athula poren"/>
</div>
</pre>
