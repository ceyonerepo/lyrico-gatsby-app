---
title: 'maatra thendral song lyrics'
album: 'Dharala Prabhu'
artist: 'Bharath Shankar'
lyricist: 'Subu'
director: 'Krishna Marimuthu'
path: '/albums/dharala-prabhu-song-lyrics'
song: 'Maatra Thendral'
image: ../../images/albumart/dharala-prabhu.jpg
date: 2020-03-13
lang: tamil
singers: 
- Pradeep Kumar
youtubeLink: "https://www.youtube.com/embed/kSvIUso8UJE"
type: 'Love'
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">

  <div class="lyrico-lyrics-wrapper">maatra thendral 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maatra thendral "/>
</div>
<div class="lyrico-lyrics-wrapper">idhamai veesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhamai veesum"/>
</div>
<div class="lyrico-lyrics-wrapper">yaar sonnalum pudhithai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaar sonnalum pudhithai"/>
</div>
<div class="lyrico-lyrics-wrapper">unthan kan paarkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unthan kan paarkum"/>
</div>
<div class="lyrico-lyrics-wrapper">etru kollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="etru kollum"/>
</div>
<div class="lyrico-lyrics-wrapper">maname podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maname podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">oor kondadum mannil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oor kondadum mannil"/>
</div>
<div class="lyrico-lyrics-wrapper">meendum poo pookum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meendum poo pookum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kollai inbam podhuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kollai inbam podhuma"/>
</div>
<div class="lyrico-lyrics-wrapper">kallum illai idhu neeluma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kallum illai idhu neeluma"/>
</div>
<div class="lyrico-lyrics-wrapper">alli konja ada 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alli konja ada "/>
</div>
<div class="lyrico-lyrics-wrapper">aasai than ooyuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aasai than ooyuma"/>
</div>
<div class="lyrico-lyrics-wrapper">anbin ellai ini venduma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anbin ellai ini venduma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">irulum megam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irulum megam"/>
</div>
<div class="lyrico-lyrics-wrapper">mannil veezhathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mannil veezhathum"/>
</div>
<div class="lyrico-lyrics-wrapper">theerum sogam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theerum sogam"/>
</div>
<div class="lyrico-lyrics-wrapper">unai therthathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unai therthathum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">yar seyalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yar seyalo"/>
</div>
<div class="lyrico-lyrics-wrapper">mazhalai mozhiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mazhalai mozhiye"/>
</div>
<div class="lyrico-lyrics-wrapper">naam magizhila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naam magizhila"/>
</div>
<div class="lyrico-lyrics-wrapper">idhuvum vazhiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhuvum vazhiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">maaridum kasappum inipum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maaridum kasappum inipum"/>
</div>
<div class="lyrico-lyrics-wrapper">rasikkum varaikum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rasikkum varaikum "/>
</div>
<div class="lyrico-lyrics-wrapper">unakum enakum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unakum enakum"/>
</div>
<div class="lyrico-lyrics-wrapper">pirakum nizhaikum punnaigaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pirakum nizhaikum punnaigaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">azhaginil uraiyava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="azhaginil uraiyava"/>
</div>
<div class="lyrico-lyrics-wrapper">mazhalayil nanaiyava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mazhalayil nanaiyava"/>
</div>
<div class="lyrico-lyrics-wrapper">pudhumaigal pazhagava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pudhumaigal pazhagava"/>
</div>
<div class="lyrico-lyrics-wrapper">kulandhiyai valarava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulandhiyai valarava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">azhaginil uraiyava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="azhaginil uraiyava"/>
</div>
<div class="lyrico-lyrics-wrapper">mazhalayil nanaiyava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mazhalayil nanaiyava"/>
</div>
<div class="lyrico-lyrics-wrapper">pudhumaigal pazhagava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pudhumaigal pazhagava"/>
</div>
<div class="lyrico-lyrics-wrapper">kulandhaiyai valara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulandhaiyai valara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vaa aruge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaa aruge"/>
</div>
<div class="lyrico-lyrics-wrapper">mazhalai mozhiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mazhalai mozhiye"/>
</div>
<div class="lyrico-lyrics-wrapper">vaa mazhaiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaa mazhaiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">manadhin inaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manadhin inaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">maaridum kasappum inipum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maaridum kasappum inipum"/>
</div>
<div class="lyrico-lyrics-wrapper">rasikkum varaikum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rasikkum varaikum "/>
</div>
<div class="lyrico-lyrics-wrapper">unakum enakum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unakum enakum"/>
</div>
<div class="lyrico-lyrics-wrapper">pirakum nizhaikum punnaigaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pirakum nizhaikum punnaigaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">varthaiyil vazhi vanthalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varthaiyil vazhi vanthalum"/>
</div>
<div class="lyrico-lyrics-wrapper">aasaiyal manam ninaiyume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aasaiyal manam ninaiyume"/>
</div>
<div class="lyrico-lyrics-wrapper">thavarugal sila nerum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thavarugal sila nerum"/>
</div>
<div class="lyrico-lyrics-wrapper">muzhuthum anbile 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muzhuthum anbile "/>
</div>
<div class="lyrico-lyrics-wrapper">athu karaiyume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu karaiyume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">naan tharuvane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan tharuvane"/>
</div>
<div class="lyrico-lyrics-wrapper">mazhalai mozhiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mazhalai mozhiye"/>
</div>
<div class="lyrico-lyrics-wrapper">nee ninaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee ninaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">ulagam iniye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulagam iniye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">maaridum kasappum inipum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maaridum kasappum inipum"/>
</div>
<div class="lyrico-lyrics-wrapper">rasikkum varaikum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rasikkum varaikum "/>
</div>
<div class="lyrico-lyrics-wrapper">unakum enakum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unakum enakum"/>
</div>
<div class="lyrico-lyrics-wrapper">pirakum nizhaikum punnaigaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pirakum nizhaikum punnaigaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">maatra thendral 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maatra thendral "/>
</div>
<div class="lyrico-lyrics-wrapper">idhamai veesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhamai veesum"/>
</div>
<div class="lyrico-lyrics-wrapper">yaar sonnalum pudhithai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaar sonnalum pudhithai"/>
</div>
<div class="lyrico-lyrics-wrapper">unthan kan paarkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unthan kan paarkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kollai inbam podhuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kollai inbam podhuma"/>
</div>
<div class="lyrico-lyrics-wrapper">kallum illai idhu neeluma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kallum illai idhu neeluma"/>
</div>
<div class="lyrico-lyrics-wrapper">alli konja ada 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alli konja ada "/>
</div>
<div class="lyrico-lyrics-wrapper">aasai than ooyuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aasai than ooyuma"/>
</div>
<div class="lyrico-lyrics-wrapper">anbin ellai ini venduma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anbin ellai ini venduma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">irulum megam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irulum megam"/>
</div>
<div class="lyrico-lyrics-wrapper">mannil veezhathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mannil veezhathum"/>
</div>
<div class="lyrico-lyrics-wrapper">theerum sogam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theerum sogam"/>
</div>
<div class="lyrico-lyrics-wrapper">unai therthathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unai therthathum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">azhaginil uraiyava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="azhaginil uraiyava"/>
</div>
<div class="lyrico-lyrics-wrapper">mazhalayil nanaiyava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mazhalayil nanaiyava"/>
</div>
<div class="lyrico-lyrics-wrapper">pudhumaigal pazhagava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pudhumaigal pazhagava"/>
</div>
<div class="lyrico-lyrics-wrapper">kulandhaiyai valara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulandhaiyai valara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">azhaginil uraiyava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="azhaginil uraiyava"/>
</div>
<div class="lyrico-lyrics-wrapper">mazhalayil nanaiyava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mazhalayil nanaiyava"/>
</div>
<div class="lyrico-lyrics-wrapper">pudhumaigal pazhagava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pudhumaigal pazhagava"/>
</div>
<div class="lyrico-lyrics-wrapper">kulandhaiyai valara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulandhaiyai valara"/>
</div>
</pre>