---
title: "alhamdulillah song lyrics"
album: "Sufiyum Sujatayum"
artist: "M. Jayachandran"
lyricist: "B K Harinarayanan"
director: "Naranipuzha Shanavas"
path: "/albums/sufiyum-sujatayum-lyrics"
song: "Alhamdulillah"
image: ../../images/albumart/sufiyum-sujatayum.jpg
date: 2020-07-03
lang: malayalam
youtubeLink: "https://www.youtube.com/embed/_hUj0w1L4L4"
type: "melody"
singers:
  - 	Sudeep Palanad
  - 	Amrutha Suresh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Alhamdulillaha Alhamdulillaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alhamdulillaha Alhamdulillaha"/>
</div>
<div class="lyrico-lyrics-wrapper">Alhamdulillaha Alhamdulillaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alhamdulillaha Alhamdulillaha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alhamdulillaha Othunnu Praanan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alhamdulillaha Othunnu Praanan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Janma Sookam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Janma Sookam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Thanna Dhanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Thanna Dhanam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mannodu Mannaayi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannodu Mannaayi "/>
</div>
<div class="lyrico-lyrics-wrapper">Cherum Varey Nin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cherum Varey Nin"/>
</div>
<div class="lyrico-lyrics-wrapper">Sangeethame Njaan Aa Aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sangeethame Njaan Aa Aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoomangalaye Paarunnitha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoomangalaye Paarunnitha "/>
</div>
<div class="lyrico-lyrics-wrapper">Njan Unmadhame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Njan Unmadhame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Noorullah Noorullah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Noorullah Noorullah"/>
</div>
<div class="lyrico-lyrics-wrapper">Noorullah Noorullah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Noorullah Noorullah"/>
</div>
<div class="lyrico-lyrics-wrapper">Noorullah Noorullah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Noorullah Noorullah"/>
</div>
<div class="lyrico-lyrics-wrapper">Noorullah Noorullah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Noorullah Noorullah"/>
</div>
<div class="lyrico-lyrics-wrapper">Noorullaha  Aa Aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Noorullaha  Aa Aa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Padi Vaathiloolum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padi Vaathiloolum"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhal Padarunna Neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhal Padarunna Neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Charadoornu Poyidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Charadoornu Poyidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Japa Malayay Njan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Japa Malayay Njan"/>
</div>
<div class="lyrico-lyrics-wrapper">Irulintey Theeyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irulintey Theeyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Mozhi Moham Alumpol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mozhi Moham Alumpol"/>
</div>
<div class="lyrico-lyrics-wrapper">Iniyengane Noore Oru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iniyengane Noore Oru "/>
</div>
<div class="lyrico-lyrics-wrapper">Nanni Othan Aa Aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanni Othan Aa Aa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Novekunnon Allaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Novekunnon Allaha"/>
</div>
<div class="lyrico-lyrics-wrapper">Novattunon Allah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Novattunon Allah"/>
</div>
<div class="lyrico-lyrics-wrapper">Eemannillam Allaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eemannillam Allaha"/>
</div>
<div class="lyrico-lyrics-wrapper">En Jannathum Allah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Jannathum Allah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thee Ekunnon Allah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thee Ekunnon Allah"/>
</div>
<div class="lyrico-lyrics-wrapper">Manjakunnon Allah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manjakunnon Allah"/>
</div>
<div class="lyrico-lyrics-wrapper">En Aanandham Allah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Aanandham Allah"/>
</div>
<div class="lyrico-lyrics-wrapper">En Aakasham Allah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Aakasham Allah"/>
</div>
<div class="lyrico-lyrics-wrapper">Allah Allah Allah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allah Allah Allah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Njaan Mayilanchi Kambai Nikkannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Njaan Mayilanchi Kambai Nikkannu"/>
</div>
<div class="lyrico-lyrics-wrapper">Neruzhuthiya Meesan Kallin Pakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neruzhuthiya Meesan Kallin Pakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyenna Suparkathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyenna Suparkathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Chayumbol Aentho Moolakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chayumbol Aentho Moolakkam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Noorullah Noorullah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Noorullah Noorullah"/>
</div>
<div class="lyrico-lyrics-wrapper">Noorullah Noorullah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Noorullah Noorullah"/>
</div>
<div class="lyrico-lyrics-wrapper">Noorullah Noorullah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Noorullah Noorullah"/>
</div>
<div class="lyrico-lyrics-wrapper">Noorullah Noorullah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Noorullah Noorullah"/>
</div>
<div class="lyrico-lyrics-wrapper">Noorullaha Aa Aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Noorullaha Aa Aa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Novekunnon Allaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Novekunnon Allaha"/>
</div>
<div class="lyrico-lyrics-wrapper">Novattunon Allah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Novattunon Allah"/>
</div>
<div class="lyrico-lyrics-wrapper">Eemannillam Allaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eemannillam Allaha"/>
</div>
<div class="lyrico-lyrics-wrapper">En Jannathum Allah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Jannathum Allah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thee Ekunnon Allah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thee Ekunnon Allah"/>
</div>
<div class="lyrico-lyrics-wrapper">Manjakunnon Allah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manjakunnon Allah"/>
</div>
<div class="lyrico-lyrics-wrapper">En Aanandham Allah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Aanandham Allah"/>
</div>
<div class="lyrico-lyrics-wrapper">En Aakasham Allah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Aakasham Allah"/>
</div>
<div class="lyrico-lyrics-wrapper">Allah Allah Allah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allah Allah Allah"/>
</div>
</pre>
