---
title: "odu odu aadu song lyrics"
album: "Pushpa The Rise"
artist: "Devi Sri Prasad"
lyricist: "Viveka"
director: "Sukumar"
path: "/albums/pushpa-the-rise-song-lyrics"
song: "Odu Odu Aadu"
image: ../../images/albumart/pushpa-the-rise.jpg
date: 2021-12-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ff9hFH1dM4A"
type: "mass"
singers:
  - Benny Dayal
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Thandhaanae thanae nanae naa nae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandhaanae thanae nanae naa nae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thandhaanae thanae nanae naa nae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandhaanae thanae nanae naa nae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaanaanae thanni nari naa nae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanaanae thanni nari naa nae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaanaanae thanni nari naa nae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanaanae thanni nari naa nae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ah ah ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ah ah ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Ah ah ah ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ah ah ah ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Ah ah ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ah ah ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Ah ah ah ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ah ah ah ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ah ah ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ah ah ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Ah ah ah ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ah ah ah ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Ah ah ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ah ah ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Ah ah ah ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ah ah ah ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ah ah ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ah ah ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Ah ah ah ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ah ah ah ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Ah ah ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ah ah ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Ah ah ah ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ah ah ah ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ah ah ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ah ah ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Ah ah ah ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ah ah ah ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Ah ah ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ah ah ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Ah ah ah ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ah ah ah ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Velichatha thinnudhu kaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velichatha thinnudhu kaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Velichatha thinnudhu kaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velichatha thinnudhu kaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaattai thinnudhu aadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattai thinnudhu aadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaattai thinnudhu aadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattai thinnudhu aadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aattai thinnudhu puli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aattai thinnudhu puli"/>
</div>
<div class="lyrico-lyrics-wrapper">Aattai thinnudhu puli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aattai thinnudhu puli"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu thaan da pasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu thaan da pasi"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu thaan da pasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu thaan da pasi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ah ah ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ah ah ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Ah ah ah ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ah ah ah ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Ah ah ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ah ah ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Ah ah ah ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ah ah ah ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Puliya thinnudhu saavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puliya thinnudhu saavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Saavai thinnudhu kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saavai thinnudhu kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalatha thunnudhu kaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalatha thunnudhu kaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu thaan maha pasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu thaan maha pasi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ah ah ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ah ah ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Ah ah ah ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ah ah ah ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Ah ah ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ah ah ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Ah ah ah ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ah ah ah ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nikkama thorathum onnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikkama thorathum onnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada sikkama parakkum onnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada sikkama parakkum onnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maattitta idhu seththuchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maattitta idhu seththuchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Maattatti pasiyila adhu seththuchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maattatti pasiyila adhu seththuchi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru jeevanukkingae pasi vandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru jeevanukkingae pasi vandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru jeevan nichayam balithaanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru jeevan nichayam balithaanda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey odu odu aaduPuli 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey odu odu aaduPuli "/>
</div>
<div class="lyrico-lyrics-wrapper">vandhakka adhirum kaaduhui
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vandhakka adhirum kaaduhui"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meenukku puzhu thaan valai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meenukku puzhu thaan valai"/>
</div>
<div class="lyrico-lyrics-wrapper">Paravaikku dhaaniyam valai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paravaikku dhaaniyam valai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naayikku elumbae valai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naayikku elumbae valai"/>
</div>
<div class="lyrico-lyrics-wrapper">Manushanukkendrum aasaiyae valai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manushanukkendrum aasaiyae valai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ah ah ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ah ah ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Ah ah ah ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ah ah ah ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Ah ah ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ah ah ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Ah ah ah ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ah ah ah ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hmmm hmmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmmm hmmm"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bannari amman koyilu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bannari amman koyilu"/>
</div>
<div class="lyrico-lyrics-wrapper">Baliya aadu kozhi kekkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baliya aadu kozhi kekkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaththiyum raththam poosudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaththiyum raththam poosudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Saamikku dhatchanai kodu varam thara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saamikku dhatchanai kodu varam thara"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu thaan vidhiyin yaathira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu thaan vidhiyin yaathira"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ah ah ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ah ah ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Ah ah ah ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ah ah ah ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Ah ah ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ah ah ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Ah ah ah ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ah ah ah ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Elaichavan paadu thindaattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Elaichavan paadu thindaattam"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu thaan ulagin vedham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu thaan ulagin vedham"/>
</div>
<div class="lyrico-lyrics-wrapper">Valuthavan paadu kondaattam enbadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valuthavan paadu kondaattam enbadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam sollum paadam aah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam sollum paadam aah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pasiyin munnae theriyadhu needhi nyaayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pasiyin munnae theriyadhu needhi nyaayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Balam irukkum aaloda kaiyil raajyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Balam irukkum aaloda kaiyil raajyam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey odu odu aadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey odu odu aadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Puli vandhakka adhirum kaadu hui
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puli vandhakka adhirum kaadu hui"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hmmm hmmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmmm hmmm"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adangi kedantha thavaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adangi kedantha thavaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Thavaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thavaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Adichavan thaane power-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adichavan thaane power-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Power-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Power-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Othaikkira vazhi thaan perusu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Othaikkira vazhi thaan perusu"/>
</div>
<div class="lyrico-lyrics-wrapper">Perusu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perusu"/>
</div>
<div class="lyrico-lyrics-wrapper">Othaikku munnaadi olagam sirusu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Othaikku munnaadi olagam sirusu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ah ah ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ah ah ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Ah ah ah ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ah ah ah ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Ah ah ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ah ah ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Ah ah ah ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ah ah ah ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaakkura aalu mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaakkura aalu mela"/>
</div>
<div class="lyrico-lyrics-wrapper">Thayangura aalu keezha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thayangura aalu keezha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuththula kedaikkira paadam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuththula kedaikkira paadam"/>
</div>
<div class="lyrico-lyrics-wrapper">Budhanum kooda sollala da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Budhanum kooda sollala da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hmmm hmmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmmm hmmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Malayalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malayalam"/>
</div>
</pre>
