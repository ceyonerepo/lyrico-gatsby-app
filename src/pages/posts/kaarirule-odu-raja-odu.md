---
title: "kaarirule song lyrics"
album: "Odu Raja Odu"
artist: "Tosh Nanda"
lyricist: "Swaminathan Dindigul"
director: "Nishanth Ravindaran - Jathin Sanker Raj"
path: "/albums/odu-raja-odu-lyrics"
song: "Kaarirule"
image: ../../images/albumart/odu-raja-odu.jpg
date: 2018-08-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/xBveuVg_pkw"
type: "melody"
singers:
  - Shaktisree Gopalan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Oh Raaa Oh Raaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Raaa Oh Raaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Raaa Oh Raaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Raaa Oh Raaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaarirule Kaarirule Thodarvaayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaarirule Kaarirule Thodarvaayo"/>
</div>
<div class="lyrico-lyrics-wrapper">Or Iravil Or Iravil Olipaayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Or Iravil Or Iravil Olipaayo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaarirule Kaarirule Thodarvaayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaarirule Kaarirule Thodarvaayo"/>
</div>
<div class="lyrico-lyrics-wrapper">Or Iravil Or Iravil Olipaayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Or Iravil Or Iravil Olipaayo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vittil Poochi Kootam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vittil Poochi Kootam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilakku Theeyil Pattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilakku Theeyil Pattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maati Konda Maayum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maati Konda Maayum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilaidhaano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilaidhaano"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mootai Mootaiaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mootai Mootaiaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasai Koodi Poga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai Koodi Poga"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhkai Indru Ondre
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhkai Indru Ondre"/>
</div>
<div class="lyrico-lyrics-wrapper">Pizhaidhaano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pizhaidhaano"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaarirule Thodarvaayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaarirule Thodarvaayo"/>
</div>
<div class="lyrico-lyrics-wrapper">Or Iravil Olipaayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Or Iravil Olipaayo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaarirule Kaarirule Thodarvaayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaarirule Kaarirule Thodarvaayo"/>
</div>
<div class="lyrico-lyrics-wrapper">Or Iravil Or Iravil Olipaayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Or Iravil Or Iravil Olipaayo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaarirule Kaarirule Thodarvaayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaarirule Kaarirule Thodarvaayo"/>
</div>
<div class="lyrico-lyrics-wrapper">Or Iravil Olipaayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Or Iravil Olipaayo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaarirule Kaarirule Thodarvaayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaarirule Kaarirule Thodarvaayo"/>
</div>
<div class="lyrico-lyrics-wrapper">Or Iravil Or Iravil Olipaayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Or Iravil Or Iravil Olipaayo"/>
</div>
</pre>
