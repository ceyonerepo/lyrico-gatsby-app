---
title: "thee yazhini song lyrics"
album: "Vanjagar Ulagam"
artist: "	Sam CS"
lyricist: "Madhan Karky"
director: "Manoj Beedha"
path: "/albums/vanjagar-ulagam-lyrics"
song: "Thee Yazhini"
image: ../../images/albumart/vanjagar-ulagam.jpg
date: 2018-09-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/K0E9r_vslJE"
type: "mass"
singers:
  - Yuvan Shankar Raja
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Thee yazhini hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thee yazhini hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Than mouna vaalai kondae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Than mouna vaalai kondae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjai konjam keerinaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjai konjam keerinaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thee yazhini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thee yazhini"/>
</div>
<div class="lyrico-lyrics-wrapper">En aanmaiyai yen korinaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En aanmaiyai yen korinaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey hey hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hey hey hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ae yeno ennai ennai erikkiraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae yeno ennai ennai erikkiraal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ae yeno thalli nindru sirikkiraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae yeno thalli nindru sirikkiraal"/>
</div>
<div class="lyrico-lyrics-wrapper">Theendaamal mogathin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theendaamal mogathin"/>
</div>
<div class="lyrico-lyrics-wrapper">Uchchathai thandhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uchchathai thandhaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ae yeno viragangal vithaikkiraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae yeno viragangal vithaikkiraal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ae yeno uyirudan puthaikkiraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae yeno uyirudan puthaikkiraal"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan kaanaa achchathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan kaanaa achchathai"/>
</div>
<div class="lyrico-lyrics-wrapper">Achchathai thandhaal aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achchathai thandhaal aaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh udaiyagi kizhiyum nenjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh udaiyagi kizhiyum nenjam"/>
</div>
<div class="lyrico-lyrics-wrapper">Idaiyodu vazhiyum paarvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idaiyodu vazhiyum paarvai"/>
</div>
<div class="lyrico-lyrics-wrapper">Veliyera mozhiyum anjum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veliyera mozhiyum anjum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kulir odum pozhiyum vervai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulir odum pozhiyum vervai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En bodhaigal yaavumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En bodhaigal yaavumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Veenena aakkinaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veenena aakkinaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kal oorum sollai kondae mayakkinaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kal oorum sollai kondae mayakkinaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey hey hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hey hey hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ae yeno ennai ennai erikkiraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae yeno ennai ennai erikkiraal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ae yeno thalli nindru sirikkiraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae yeno thalli nindru sirikkiraal"/>
</div>
<div class="lyrico-lyrics-wrapper">Theendaamal mogathin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theendaamal mogathin"/>
</div>
<div class="lyrico-lyrics-wrapper">Uchchathai thandhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uchchathai thandhaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ae yeno viragangal vithaikkiraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae yeno viragangal vithaikkiraal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ae yeno uyirudan puthaikkiraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae yeno uyirudan puthaikkiraal"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan kaanaa achchathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan kaanaa achchathai"/>
</div>
<div class="lyrico-lyrics-wrapper">Achchathai thandhaal aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achchathai thandhaal aaa"/>
</div>
</pre>
