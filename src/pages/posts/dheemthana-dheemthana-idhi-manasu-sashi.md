---
title: "dheemthana song lyrics"
album: "Sashi"
artist: "Arun Chiluveru"
lyricist: "Bhaskarbatla"
director: "Srinivas Naidu Nadikatla"
path: "/albums/sashi-lyrics"
song: "Dheemthana Dheemthana Idhi Manasu"
image: ../../images/albumart/sashi.jpg
date: 2021-03-19
lang: telugu
youtubeLink: "https://www.youtube.com/embed/D-Q61xUd3uY"
type: "love"
singers:
  - Haricharan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Dheemathana dheemathana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheemathana dheemathana"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhi manasu pandagani anukonaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhi manasu pandagani anukonaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheemathana dheemathana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheemathana dheemathana"/>
</div>
<div class="lyrico-lyrics-wrapper">Kala edhuta vaalinadhi nijamenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kala edhuta vaalinadhi nijamenaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dheemathana dheemathana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheemathana dheemathana"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhi manasu pandagani anukonaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhi manasu pandagani anukonaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheemathana dheemathana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheemathana dheemathana"/>
</div>
<div class="lyrico-lyrics-wrapper">Kala edhuta vaalinadhi nijamenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kala edhuta vaalinadhi nijamenaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanureppala kolaatamidhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanureppala kolaatamidhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Edha chappudu aaraatamidhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edha chappudu aaraatamidhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvichhina aanandhamidhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvichhina aanandhamidhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nulivechhaga baagundhidhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nulivechhaga baagundhidhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey ninninka vadhalanule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey ninninka vadhalanule"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee cheyyi vidavanule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee cheyyi vidavanule"/>
</div>
<div class="lyrico-lyrics-wrapper">Neelonchi kdhalanule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelonchi kdhalanule"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dheemathana dheemathana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheemathana dheemathana"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhi manasu pandagani anukonaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhi manasu pandagani anukonaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheemathana dheemathana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheemathana dheemathana"/>
</div>
<div class="lyrico-lyrics-wrapper">Kala edhuta vaalinadhi nijamenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kala edhuta vaalinadhi nijamenaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paala sandramla pongipothunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paala sandramla pongipothunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Paalapunthalo thelipothunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paalapunthalo thelipothunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Virise thotalanni thoonigalle tirigesthunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Virise thotalanni thoonigalle tirigesthunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Kurise tharalanni dosillalo nimpesthunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurise tharalanni dosillalo nimpesthunna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chettu kommalle oogipothunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chettu kommalle oogipothunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Kotha janmedo andhuluntunnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kotha janmedo andhuluntunnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Rekka vippukuntu guvvalanni gundeloki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rekka vippukuntu guvvalanni gundeloki"/>
</div>
<div class="lyrico-lyrics-wrapper">Dookinattu utshavalu jaruputhunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dookinattu utshavalu jaruputhunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Ningi anchumeeda rangu rangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ningi anchumeeda rangu rangu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chepaluga genthuthunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chepaluga genthuthunna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dheemathana dheemathana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheemathana dheemathana"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhi manasu pandagani anukonaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhi manasu pandagani anukonaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheemathana dheemathana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheemathana dheemathana"/>
</div>
<div class="lyrico-lyrics-wrapper">Kala edhuta vaalinadhi nijamenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kala edhuta vaalinadhi nijamenaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inthakalam ga ekkadunnave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inthakalam ga ekkadunnave"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnapalam ga oodipaddave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnapalam ga oodipaddave"/>
</div>
<div class="lyrico-lyrics-wrapper">Thelisi theliyanattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thelisi theliyanattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa manasune lagesaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa manasune lagesaave"/>
</div>
<div class="lyrico-lyrics-wrapper">Asalem eraganattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asalem eraganattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee venakane thippinchave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee venakane thippinchave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninnu chusake pranamochinde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu chusake pranamochinde"/>
</div>
<div class="lyrico-lyrics-wrapper">Vintha lokamlo kalupettindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vintha lokamlo kalupettindhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu thakuthunna gali vachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu thakuthunna gali vachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Na chempa gilluthunt
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na chempa gilluthunt"/>
</div>
<div class="lyrico-lyrics-wrapper">Inthakanna haayi undadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inthakanna haayi undadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Are ninnu thappa kannu inka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Are ninnu thappa kannu inka"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannu kuda chudanandhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannu kuda chudanandhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh O O O
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh O O O"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dheemathana dheemathana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheemathana dheemathana"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhi manasu pandagani anukonaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhi manasu pandagani anukonaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheemathana dheemathana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheemathana dheemathana"/>
</div>
<div class="lyrico-lyrics-wrapper">Kala edhuta vaalinadhi nijamenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kala edhuta vaalinadhi nijamenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanureppala kolaatamidhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanureppala kolaatamidhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Edha chappudu aaraatamidhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edha chappudu aaraatamidhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvichhina aanandhamidhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvichhina aanandhamidhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nulivechhaga baagundhidhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nulivechhaga baagundhidhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey ninninka vadhalanule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey ninninka vadhalanule"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee cheyyi vidavanule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee cheyyi vidavanule"/>
</div>
<div class="lyrico-lyrics-wrapper">Neelonchi kdhalanule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelonchi kdhalanule"/>
</div>
</pre>
