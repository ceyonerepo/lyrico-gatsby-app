---
title: "thamizh thaai vaazhthu song lyrics"
album: "LKG"
artist: "Leon James"
lyricist: "Manonmaniam Sundaram Pillai"
director: "K. R. Prabhu"
path: "/albums/lkg-lyrics"
song: "Thamizh Thaai Vaazhthu"
image: ../../images/albumart/lkg.jpg
date: 2019-02-22
lang: tamil
youtubeLink: "https://www.youtube.com/embed/5qyyQLKM_mk"
type: "anthem"
singers:
  - P. Susheela
  - L. R. Eswari
  - Vani Jayaram
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Neeraarum Kadaluduththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeraarum Kadaluduththa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilamadandhai Kezhilolugum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilamadandhai Kezhilolugum"/>
</div>
<div class="lyrico-lyrics-wrapper">Seeraarum Vadhanamena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seeraarum Vadhanamena"/>
</div>
<div class="lyrico-lyrics-wrapper">Thihazh Baradha Kandamidhil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thihazh Baradha Kandamidhil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thekkanamum Adhil Chirandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thekkanamum Adhil Chirandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Dravida Nal Thiru Naadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dravida Nal Thiru Naadum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakkasiru Pirai Nudhalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakkasiru Pirai Nudhalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thari Thanarum Thilagamume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thari Thanarum Thilagamume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aththilaga Vaasanai Pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aththilaga Vaasanai Pol"/>
</div>
<div class="lyrico-lyrics-wrapper">Anaithulagum Inbamura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anaithulagum Inbamura"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeththisayum Pugazh Manakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeththisayum Pugazh Manakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Irundha Perum Thamizhanange Thamizhanange
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irundha Perum Thamizhanange Thamizhanange"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vun Seerilamai Thiram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vun Seerilamai Thiram"/>
</div>
<div class="lyrico-lyrics-wrapper">Viyandhu Seyal Marandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viyandhu Seyal Marandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhthudhume Vazhthudhume Vazhthudhume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhthudhume Vazhthudhume Vazhthudhume"/>
</div>
</pre>
