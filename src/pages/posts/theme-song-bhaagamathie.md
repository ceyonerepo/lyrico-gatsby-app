---
title: "theme song song lyrics"
album: "Bhaagamathie"
artist: "S. Thaman"
lyricist: "Suchitra"
director: "G. Ashok"
path: "/albums/bhaagamathie-lyrics"
song: "Theme Song"
image: ../../images/albumart/bhaagamathie.jpg
date: 2018-01-26
lang: tamil
youtubeLink: "https://www.youtube.com/embed/90ckIuT06Fo"
type: "theme song"
singers:
  - Suchitra
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">You Cannot Trust What Your Eyes 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Cannot Trust What Your Eyes "/>
</div>
<div class="lyrico-lyrics-wrapper">See You Cannot Trust Your 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="See You Cannot Trust Your "/>
</div>
<div class="lyrico-lyrics-wrapper">Own Heart Beat 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Own Heart Beat "/>
</div>
<div class="lyrico-lyrics-wrapper">You Cannot Trust Anybody 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Cannot Trust Anybody "/>
</div>
<div class="lyrico-lyrics-wrapper">Let's Begin The Game 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Let's Begin The Game "/>
</div>
<div class="lyrico-lyrics-wrapper">You Cannot Trust What 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Cannot Trust What "/>
</div>
<div class="lyrico-lyrics-wrapper">Your Eyes See You Cannot 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Your Eyes See You Cannot "/>
</div>
<div class="lyrico-lyrics-wrapper">Trust Your Own Heart Beat 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Trust Your Own Heart Beat "/>
</div>
<div class="lyrico-lyrics-wrapper">You Cannot Trust Anybody 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Cannot Trust Anybody "/>
</div>
<div class="lyrico-lyrics-wrapper">War That Is The 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="War That Is The "/>
</div>
<div class="lyrico-lyrics-wrapper">Name Ba Ba Ba Ba Bha Bha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Name Ba Ba Ba Ba Bha Bha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ga Ga Ga Ga Gaa Gaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ga Ga Ga Ga Gaa Gaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ma Ma Ma Ma Maa Maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ma Ma Ma Ma Maa Maa"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhagmathi Mathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhagmathi Mathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Say Ma Ma Ma Ma Maa Maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Say Ma Ma Ma Ma Maa Maa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ga Ga Ga Ga Gaa Gaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ga Ga Ga Ga Gaa Gaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ba Ba Ba Ba Bha Bha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ba Ba Ba Ba Bha Bha"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhagmathi Mathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhagmathi Mathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhagmathi Mathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhagmathi Mathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Everyway You Turn Around 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Everyway You Turn Around "/>
</div>
<div class="lyrico-lyrics-wrapper">The Freaking Freaking Dangerous
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="The Freaking Freaking Dangerous"/>
</div>
<div class="lyrico-lyrics-wrapper">Don't You Trust The Buddy Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Don't You Trust The Buddy Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Every Once Explain That
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Every Once Explain That"/>
</div>
<div class="lyrico-lyrics-wrapper">Kicking Kicking Ass Ass
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kicking Kicking Ass Ass"/>
</div>
<div class="lyrico-lyrics-wrapper">Move It With The Sass Sass
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Move It With The Sass Sass"/>
</div>
<div class="lyrico-lyrics-wrapper">Move Your World
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Move Your World"/>
</div>
<div class="lyrico-lyrics-wrapper">Move It Around
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Move It Around"/>
</div>
<div class="lyrico-lyrics-wrapper">Move It Get It Down 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Move It Get It Down "/>
</div>
<div class="lyrico-lyrics-wrapper">Ba Ba Ba Ba Bha Bha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ba Ba Ba Ba Bha Bha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ga Ga Ga Ga Gaa Gaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ga Ga Ga Ga Gaa Gaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ma Ma Ma Ma Maa Maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ma Ma Ma Ma Maa Maa"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhagmathi Mathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhagmathi Mathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Say Ma Ma Ma Ma Maa Maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Say Ma Ma Ma Ma Maa Maa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ga Ga Ga Ga Gaa Gaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ga Ga Ga Ga Gaa Gaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ba Ba Ba Ba Bha Bha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ba Ba Ba Ba Bha Bha"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhagmathi Mathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhagmathi Mathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhaagamathie
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhaagamathie"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhaagamathie
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhaagamathie"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhaagamathie
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhaagamathie"/>
</div>
<div class="lyrico-lyrics-wrapper">Khanditha Sthambitha Bhagala 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khanditha Sthambitha Bhagala "/>
</div>
<div class="lyrico-lyrics-wrapper">Mukhi Raa Bhaagamathie 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mukhi Raa Bhaagamathie "/>
</div>
<div class="lyrico-lyrics-wrapper">Khanditha Sthambitha Bhagala 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khanditha Sthambitha Bhagala "/>
</div>
<div class="lyrico-lyrics-wrapper">Mukhi Raa Aa Aa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mukhi Raa Aa Aa "/>
</div>
<div class="lyrico-lyrics-wrapper">Ba Ba Ba Ba Bha Bha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ba Ba Ba Ba Bha Bha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ga Ga Ga Ga Gaa Gaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ga Ga Ga Ga Gaa Gaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ma Ma Ma Ma Maa Maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ma Ma Ma Ma Maa Maa"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhagmathi Mathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhagmathi Mathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Say Ma Ma Ma Ma Maa Maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Say Ma Ma Ma Ma Maa Maa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ga Ga Ga Ga Gaa Gaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ga Ga Ga Ga Gaa Gaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ba Ba Ba Ba Bha Bha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ba Ba Ba Ba Bha Bha"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhagmathi Mathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhagmathi Mathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhagmathi Mathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhagmathi Mathi"/>
</div>
<div class="lyrico-lyrics-wrapper">You Cannot Trust What 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Cannot Trust What "/>
</div>
<div class="lyrico-lyrics-wrapper">Your Eyes See You Cannot 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Your Eyes See You Cannot "/>
</div>
<div class="lyrico-lyrics-wrapper">Trust Your Own Heart Beat 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Trust Your Own Heart Beat "/>
</div>
<div class="lyrico-lyrics-wrapper">You Cannot Trust Anybody 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Cannot Trust Anybody "/>
</div>
<div class="lyrico-lyrics-wrapper">Let's Begin The Game 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Let's Begin The Game "/>
</div>
<div class="lyrico-lyrics-wrapper">You Cannot Trust What Your 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Cannot Trust What Your "/>
</div>
<div class="lyrico-lyrics-wrapper">Eyes See You Cannot Trust 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eyes See You Cannot Trust "/>
</div>
<div class="lyrico-lyrics-wrapper">Your Own Heart Beat 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Your Own Heart Beat "/>
</div>
<div class="lyrico-lyrics-wrapper">You Cannot Trust Anybody 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Cannot Trust Anybody "/>
</div>
<div class="lyrico-lyrics-wrapper">War That Is The Name 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="War That Is The Name "/>
</div>
<div class="lyrico-lyrics-wrapper">Bhaaga Bhaaga Bhaaga Bhaaga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhaaga Bhaaga Bhaaga Bhaaga "/>
</div>
<div class="lyrico-lyrics-wrapper">Bhaagamathie Khanditha Sthambitha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhaagamathie Khanditha Sthambitha "/>
</div>
<div class="lyrico-lyrics-wrapper">Bhagalamukhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhagalamukhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhagalamukhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhagalamukhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhagalamukhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhagalamukhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhagalamukhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhagalamukhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhagalamukhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhagalamukhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhagalamukhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhagalamukhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhagalamukhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhagalamukhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhagalamukhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhagalamukhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhagamathie
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhagamathie"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhagamathie
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhagamathie"/>
</div>
</pre>
