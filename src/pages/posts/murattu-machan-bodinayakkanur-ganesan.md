---
title: "murattu machan song lyrics"
album: "Bodinayakkanur Ganesan"
artist: "John Peter"
lyricist: "Nandalala"
director: "O. Gnanam"
path: "/albums/bodinayakkanur-ganesan-lyrics"
song: "Murattu Machan"
image: ../../images/albumart/bodinayakkanur-ganesan.jpg
date: 2011-07-29
lang: tamil
youtubeLink: "https://www.youtube.com/embed/aOflE0YR_WI"
type: "love"
singers:
  - S.P. Balasubrahmanyam
  - K.S. Chithra
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Morattu machaan Morattu machaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Morattu machaan Morattu machaan"/>
</div>
<div class="lyrico-lyrics-wrapper">mookku muzhiyum azhagaa vachaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mookku muzhiyum azhagaa vachaan"/>
</div>
<div class="lyrico-lyrics-wrapper">muzhusaa enna urugavachaaney hO
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muzhusaa enna urugavachaaney hO"/>
</div>
<div class="lyrico-lyrics-wrapper">morattu machaan morattu machaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="morattu machaan morattu machaan"/>
</div>
<div class="lyrico-lyrics-wrapper">mundhi sela nazhuva vachaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mundhi sela nazhuva vachaan"/>
</div>
<div class="lyrico-lyrics-wrapper">moththa udambum silirkka vachaan hO
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moththa udambum silirkka vachaan hO"/>
</div>
<div class="lyrico-lyrics-wrapper">kurumbuthanam gunathil vachaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kurumbuthanam gunathil vachaan"/>
</div>
<div class="lyrico-lyrics-wrapper">kuzhandhathanam sirippil vachaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuzhandhathanam sirippil vachaan"/>
</div>
<div class="lyrico-lyrics-wrapper">kusumbuthanam panni vachaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kusumbuthanam panni vachaaney"/>
</div>
<div class="lyrico-lyrics-wrapper">enna kuththummadhippaa mayangavachaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna kuththummadhippaa mayangavachaaney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Morattu malli Morattu malli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Morattu malli Morattu malli"/>
</div>
<div class="lyrico-lyrics-wrapper">thoraththi kannaal izhukkum kalli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoraththi kannaal izhukkum kalli"/>
</div>
<div class="lyrico-lyrics-wrapper">thoraththi enna jeyichipputtaaley hOi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoraththi enna jeyichipputtaaley hOi"/>
</div>
<div class="lyrico-lyrics-wrapper">morattu malli morattu malli 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="morattu malli morattu malli "/>
</div>
<div class="lyrico-lyrics-wrapper">mundhaanaalu samjakkalli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mundhaanaalu samjakkalli"/>
</div>
<div class="lyrico-lyrics-wrapper">moochukkulla kudippugundhaaley hO
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moochukkulla kudippugundhaaley hO"/>
</div>
<div class="lyrico-lyrics-wrapper">usurathirugi polamba vachaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="usurathirugi polamba vachaa"/>
</div>
<div class="lyrico-lyrics-wrapper">ullathaan thazhumbavachaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ullathaan thazhumbavachaa"/>
</div>
<div class="lyrico-lyrics-wrapper">oon urakkam marakkavachaaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oon urakkam marakkavachaaley"/>
</div>
<div class="lyrico-lyrics-wrapper">en baarathathaan irakkivachaaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en baarathathaan irakkivachaaley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Morattu machaan Morattu machaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Morattu machaan Morattu machaan"/>
</div>
<div class="lyrico-lyrics-wrapper">mookku muzhiyum azhagaa vachaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mookku muzhiyum azhagaa vachaan"/>
</div>
<div class="lyrico-lyrics-wrapper">muzhusaa enna urugavachaaney hO
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muzhusaa enna urugavachaaney hO"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naadodi poala naan thirinjen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naadodi poala naan thirinjen"/>
</div>
<div class="lyrico-lyrics-wrapper">Rasavaa nee than maathivacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rasavaa nee than maathivacha"/>
</div>
<div class="lyrico-lyrics-wrapper">buththikkulla poothaan pookkavacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="buththikkulla poothaan pookkavacha"/>
</div>
<div class="lyrico-lyrics-wrapper">punnagaiyil dheebam yeththi vacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="punnagaiyil dheebam yeththi vacha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thulasi poovaa poothirundhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thulasi poovaa poothirundhen"/>
</div>
<div class="lyrico-lyrics-wrapper">un kaiyil sera kaathirundhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kaiyil sera kaathirundhen"/>
</div>
<div class="lyrico-lyrics-wrapper">maalaiyaai neethaan kattivacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maalaiyaai neethaan kattivacha"/>
</div>
<div class="lyrico-lyrics-wrapper">manasukkul sarkkarai kotti vacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasukkul sarkkarai kotti vacha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Pottal kaattu boomi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Pottal kaattu boomi"/>
</div>
<div class="lyrico-lyrics-wrapper">oru poondhoattammaa aachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru poondhoattammaa aachi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thikku mukku aadi ippo vetkam vittuppoachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thikku mukku aadi ippo vetkam vittuppoachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Udamba murukkudhadi unnoada pechi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udamba murukkudhadi unnoada pechi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Morattu machaan Morattu machaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Morattu machaan Morattu machaan"/>
</div>
<div class="lyrico-lyrics-wrapper">mookku muzhiyum azhagaa vachaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mookku muzhiyum azhagaa vachaan"/>
</div>
<div class="lyrico-lyrics-wrapper">muzhusaa enna urugavachaaney hO
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muzhusaa enna urugavachaaney hO"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thevadhai onnoada Saatchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thevadhai onnoada Saatchi"/>
</div>
<div class="lyrico-lyrics-wrapper">thenmadhura muththu meenaatchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thenmadhura muththu meenaatchi"/>
</div>
<div class="lyrico-lyrics-wrapper">sondhamey onna kandukkitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sondhamey onna kandukkitten"/>
</div>
<div class="lyrico-lyrics-wrapper">sokkanaadhan poaley sokkipputten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sokkanaadhan poaley sokkipputten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey paandiyana poala roasam vacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey paandiyana poala roasam vacha"/>
</div>
<div class="lyrico-lyrics-wrapper">pachappullaiyaattam paasam vachcha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pachappullaiyaattam paasam vachcha"/>
</div>
<div class="lyrico-lyrics-wrapper">kaattaatru vellam poala vadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaattaatru vellam poala vadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">kannipponna yendaa sikkavacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannipponna yendaa sikkavacha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi pottappulla unnaala pudichiruchi kirukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi pottappulla unnaala pudichiruchi kirukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ilavambanji thannaala vedichiruchi edhukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilavambanji thannaala vedichiruchi edhukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna soodu yeththi sukku nooraakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna soodu yeththi sukku nooraakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Morattu machaan Morattu machaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Morattu machaan Morattu machaan"/>
</div>
<div class="lyrico-lyrics-wrapper">mookku muzhiyum azhagaa vachaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mookku muzhiyum azhagaa vachaan"/>
</div>
<div class="lyrico-lyrics-wrapper">muzhusaa enna urugavachaaney hO
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muzhusaa enna urugavachaaney hO"/>
</div>
</pre>
