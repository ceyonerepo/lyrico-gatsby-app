---
title: "enakku thaan song lyrics"
album: "100 percentage Kadhal"
artist: "G.V. Prakash Kumar"
lyricist: "Mohanrajan"
director: "M.M. Chandramouli"
path: "/albums/100-percentage-kadhal-lyrics"
song: "Enakku Thaan"
image: ../../images/albumart/100%-kadhal.jpg
date: 2019-10-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/oYViweRVroE"
type: "happy"
singers:
  - M.M Manasi
  - Velmurugan
  - Sinduri Vishal
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ada Ennennamo Nadakkuthunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Ennennamo Nadakkuthunga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oorukulla Aahaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorukulla Aahaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Intha Kathaigalathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Kathaigalathaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kekka inga Yaarum Illa Oho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kekka inga Yaarum Illa Oho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Antha Kaamaatchi Kadhaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Kaamaatchi Kadhaiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kelungalen Kelungalen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelungalen Kelungalen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yei Nee Adra Adra Naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei Nee Adra Adra Naa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oththakallu Mookkuthiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oththakallu Mookkuthiyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rettavadam Nekkulasum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rettavadam Nekkulasum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vatti Kadaiyil Vaangi Thanthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vatti Kadaiyil Vaangi Thanthaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakku Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Anju Rooba Saandhu Pottum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Anju Rooba Saandhu Pottum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paththu Roova Powder Dappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paththu Roova Powder Dappa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aasaiyaa Enakku Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasaiyaa Enakku Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Kannaadi Valaiyalaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Kannaadi Valaiyalaiyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalukku Kolusaiyumthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalukku Kolusaiyumthaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kachchithamma Maatti Vittaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kachchithamma Maatti Vittaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakku Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Verum Paththu Latcham Panathukkaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verum Paththu Latcham Panathukkaaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pallu pona Kilavan Kitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pallu pona Kilavan Kitta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pattaa Pottu Viththu Puttaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattaa Pottu Viththu Puttaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Purusan Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Purusan Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Mamana Oru Raathiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Mamana Oru Raathiri"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanthiyana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanthiyana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen Thaagam Theerume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Thaagam Theerume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kolla Puram Kudithanatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolla Puram Kudithanatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nadathikitta En Moodu Maarume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadathikitta En Moodu Maarume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaariya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaariya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennudaiya Maamanathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennudaiya Maamanathaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaarukkume Kudukka Maatten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarukkume Kudukka Maatten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaaya Moodikittu Podi Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaya Moodikittu Podi Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oorukulla Yethanaiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorukulla Yethanaiyo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aambalainga Irukkuraanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aambalainga Irukkuraanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ovvoru Veedu Poi Thedu Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ovvoru Veedu Poi Thedu Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athaaga Pattathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athaaga Pattathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aduththa Kadhai Enna Naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aduththa Kadhai Enna Naa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhagaana Pondatiya Kattuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaana Pondatiya Kattuna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aththana Aambala Payalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aththana Aambala Payalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sadhega Paduraanada Pondatiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sadhega Paduraanada Pondatiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athula Maattikitta Namma Sevvanthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athula Maattikitta Namma Sevvanthi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa Vanthu Sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Vanthu Sollu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Kolusu Saththam Kettaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Kolusu Saththam Kettaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Nadaiyila Sandhegam Paduvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nadaiyila Sandhegam Paduvaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Valaiyal Satham Ketta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valaiyal Satham Ketta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yevana Valaikka Pora nu Keppaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yevana Valaikka Pora nu Keppaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Kosu Vandhu Kaduchu Veengunaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Kosu Vandhu Kaduchu Veengunaalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ithu Eppo Yevan Kaduchaanu Kekkuraane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu Eppo Yevan Kaduchaanu Kekkuraane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Valiyila Poravan Sirucha Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Valiyila Poravan Sirucha Kooda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vachchurukkiyaanu Kekkuraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vachchurukkiyaanu Kekkuraan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Unnoda Mamana Kodudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Unnoda Mamana Kodudi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Munthiyaala Manthiruchu Thaaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Munthiyaala Manthiruchu Thaaren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Ingirunthu Nadaiya Nee Kattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Ingirunthu Nadaiya Nee Kattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Avanukku Samanja Sittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Avanukku Samanja Sittu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Velinaatukku Velaikku Pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velinaatukku Velaikku Pora"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pondattingaludaiya Purusan Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pondattingaludaiya Purusan Ellaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ulloorula Enna Panraanganu Theriyumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulloorula Enna Panraanganu Theriyumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thuttukkaaga Dubaiku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuttukkaaga Dubaiku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Anuppi Vechchaane Vechchaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Anuppi Vechchaane Vechchaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Fridge Tv Venumuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fridge Tv Venumuna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Panaththa Anuppi Vechchene Vechchene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panaththa Anuppi Vechchene Vechchene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sofa Settu Kattulunthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sofa Settu Kattulunthaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kettaan Vaangi Thanthene Thanthene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kettaan Vaangi Thanthene Thanthene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ECR la Veedum Kettaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ECR la Veedum Kettaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athaiyum Vaangi Thanthene Thanthene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athaiyum Vaangi Thanthene Thanthene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aasa Patta Mamanoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasa Patta Mamanoda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Settle Aaga Vandhene Vandhene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Settle Aaga Vandhene Vandhene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Vaangi Thantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Vaangi Thantha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kattil Mela Innoruthi Irunthaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattil Mela Innoruthi Irunthaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnodaiya Maamanatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnodaiya Maamanatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaarukkume Kodukkaathey Kodukkaathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarukkume Kodukkaathey Kodukkaathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Pola Neeyum Thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Pola Neeyum Thaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yemaaliya Nikkaathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yemaaliya Nikkaathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennudaiya Mamanathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennudaiya Mamanathaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaarukkume Thara Maatten  Thara Maatten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarukkume Thara Maatten  Thara Maatten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Innoruthi Vaasame Avana Nerungave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innoruthi Vaasame Avana Nerungave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Vida Maatten Vida Maatten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Vida Maatten Vida Maatten"/>
</div>
</pre>
