---
title: "en uyir kaatre song lyrics"
album: "Vaanam Kottattum"
artist: "Sid Sriram"
lyricist: "Siva Ananth"
director: "Dhana"
path: "/albums/vaanam-kottattum-lyrics"
song: "En Uyir Kaatre"
image: ../../images/albumart/vaanam-kottattum.jpg
date: 2020-02-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/oX-iP4NMpYA"
type: "Sad"
singers:
  - Sid Sriram
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kannamma Kannamma Solvaaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannamma Kannamma Solvaaya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Vasam Un Karam Thaaraaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Vasam Un Karam Thaaraaya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kan Moodiya Pothilum Minnidum Kathiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan Moodiya Pothilum Minnidum Kathiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennulle Olirvathu Yaar Solvaai Solvaaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennulle Olirvathu Yaar Solvaai Solvaaya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannamma Kannamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannamma Kannamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Yaaringey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Yaaringey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Nenjile Enthan Peyar Ennamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Nenjile Enthan Peyar Ennamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enthan Per Ennamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enthan Per Ennamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Uyir Kaatre
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Uyir Kaatre"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oranga Naadagam Sariyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oranga Naadagam Sariyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Enna Seithen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Enna Seithen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bathil Sollidamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bathil Sollidamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Uyir Kaatre
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Uyir Kaatre"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Kannil Kadhalum Varuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kannil Kadhalum Varuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaasithu Ketten Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaasithu Ketten Hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konjam Kai Kodamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Kai Kodamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kavithakalil Karaiyavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavithakalil Karaiyavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maraiyavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maraiyavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pani Nathigalil Maraiyavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pani Nathigalil Maraiyavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vazhi Kooramma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi Kooramma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vazhi Kooramma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi Kooramma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannamma Kannamma Solvaaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannamma Kannamma Solvaaya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Vasam Un Karam Thaaraaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Vasam Un Karam Thaaraaya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannamma Kannamma Solvaaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannamma Kannamma Solvaaya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Vasam Un Karam Thaaraaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Vasam Un Karam Thaaraaya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kavithakalil Karaiyavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavithakalil Karaiyavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Uyir Kaatre
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Uyir Kaatre"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maraiyavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maraiyavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pani Nathigalil Maraiyavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pani Nathigalil Maraiyavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Uyir Katre
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Uyir Katre"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vazhi Kooramma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi Kooramma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vazhi Kooramma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi Kooramma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Uyir Kaatre
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Uyir Kaatre"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Uyir Katre
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Uyir Katre"/>
</div>
</pre>
