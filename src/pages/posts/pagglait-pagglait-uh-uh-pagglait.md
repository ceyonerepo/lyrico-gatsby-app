---
title: "pagglait pagglait uh uh song lyrics"
album: "Pagglait"
artist: "Arijit Singh"
lyricist: "Raftaar"
director: "Umesh Bist"
path: "/albums/pagglait-lyrics"
song: "Pagglait Pagglait uh uh"
image: ../../images/albumart/pagglait.jpg
date: 2021-03-26
lang: hindi
youtubeLink: "https://www.youtube.com/embed/MLtOcPbjCF0"
type: "Introduction"
singers:
  - Amrita Singh
  - Raftaar
  - Arijit Singh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hogi kya kal ki subah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hogi kya kal ki subah"/>
</div>
<div class="lyrico-lyrics-wrapper">Kal ka toh kal ko pata hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kal ka toh kal ko pata hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Haan pata hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haan pata hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Hai pal do pal zindagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hai pal do pal zindagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Jo milti bas ik dafa hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jo milti bas ik dafa hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ik dafa hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ik dafa hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dil ko luta de marz mita de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil ko luta de marz mita de"/>
</div>
<div class="lyrico-lyrics-wrapper">Aatish nazarien oh tujhko pukaare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aatish nazarien oh tujhko pukaare"/>
</div>
<div class="lyrico-lyrics-wrapper">Dil ko luta de marz mita de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil ko luta de marz mita de"/>
</div>
<div class="lyrico-lyrics-wrapper">Aatish nazarien oh tujhko pukaare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aatish nazarien oh tujhko pukaare"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kya bologe deewane ko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kya bologe deewane ko"/>
</div>
<div class="lyrico-lyrics-wrapper">Do khwahishein nibhane do
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Do khwahishein nibhane do"/>
</div>
<div class="lyrico-lyrics-wrapper">Main bolungi zamane ko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main bolungi zamane ko"/>
</div>
<div class="lyrico-lyrics-wrapper">Mujhse mere fasaane lo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mujhse mere fasaane lo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kya bologe deewane ko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kya bologe deewane ko"/>
</div>
<div class="lyrico-lyrics-wrapper">Do khwahishein nibhane do
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Do khwahishein nibhane do"/>
</div>
<div class="lyrico-lyrics-wrapper">Main bolun yeh zamane ko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main bolun yeh zamane ko"/>
</div>
<div class="lyrico-lyrics-wrapper">Aur woh bole mujhe aur woh bole pagglait
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aur woh bole mujhe aur woh bole pagglait"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagglait pagglait uh uh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagglait pagglait uh uh"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagglait pagglait uh uh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagglait pagglait uh uh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saalon se ki hai jama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saalon se ki hai jama"/>
</div>
<div class="lyrico-lyrics-wrapper">Apni jo yeh shaukiyan hai, shaukiyan hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Apni jo yeh shaukiyan hai, shaukiyan hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuchh dil se lad ke kiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuchh dil se lad ke kiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuchh dil ke liye hi kiya hai, haan kiya hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuchh dil ke liye hi kiya hai, haan kiya hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dil ko luta de marz mita de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil ko luta de marz mita de"/>
</div>
<div class="lyrico-lyrics-wrapper">Aatish nazaare tujhko pukare oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aatish nazaare tujhko pukare oh"/>
</div>
<div class="lyrico-lyrics-wrapper">Dil ko luta de marz mita de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil ko luta de marz mita de"/>
</div>
<div class="lyrico-lyrics-wrapper">Aatish nazaare tujhko pukare oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aatish nazaare tujhko pukare oh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kya bologe deewane ko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kya bologe deewane ko"/>
</div>
<div class="lyrico-lyrics-wrapper">Do khwahishein nibhane do
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Do khwahishein nibhane do"/>
</div>
<div class="lyrico-lyrics-wrapper">Main bolungi zamane ko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main bolungi zamane ko"/>
</div>
<div class="lyrico-lyrics-wrapper">Mujhse mere fasaane lo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mujhse mere fasaane lo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kya bologe deewane ko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kya bologe deewane ko"/>
</div>
<div class="lyrico-lyrics-wrapper">Do khwahishein nibhane do
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Do khwahishein nibhane do"/>
</div>
<div class="lyrico-lyrics-wrapper">Main bolun yeh zamane ko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main bolun yeh zamane ko"/>
</div>
<div class="lyrico-lyrics-wrapper">Aur woh bole mujhe aur woh bole pagglait
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aur woh bole mujhe aur woh bole pagglait"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagglait, pagglait uh uh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagglait, pagglait uh uh"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagglait, pagglait uh uh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagglait, pagglait uh uh"/>
</div>
</pre>
