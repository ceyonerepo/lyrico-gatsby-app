---
title: "chitti song lyrics"
album: "Jathi Ratnalu"
artist: "Radhan"
lyricist: "Ramajogayya Sastry"
director: "Anudeep KV"
path: "/albums/jathi-ratnalu-lyrics"
song: "Chitti nee navvante"
image: ../../images/albumart/jathi-ratnalu.jpg
date: 2021-03-11
lang: telugu
youtubeLink: "https://www.youtube.com/embed/uvCbZxYdLuU"
type: "love"
singers:
  - Ram Miriyala
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Chitti nee navvante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chitti nee navvante"/>
</div>
<div class="lyrico-lyrics-wrapper">Lakshmi pataas ye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lakshmi pataas ye"/>
</div>
<div class="lyrico-lyrics-wrapper">Fattumani pelindha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fattumani pelindha"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa gunde khalass ye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa gunde khalass ye"/>
</div>
<div class="lyrico-lyrics-wrapper">Atta nuvvu girra girra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atta nuvvu girra girra"/>
</div>
<div class="lyrico-lyrics-wrapper">Melikal thirige aa oose
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Melikal thirige aa oose"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu naku set ayyavani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu naku set ayyavani"/>
</div>
<div class="lyrico-lyrics-wrapper">Signal icche breaking news ye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Signal icche breaking news ye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vachesaave line loki vachesaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vachesaave line loki vachesaave"/>
</div>
<div class="lyrico-lyrics-wrapper">Chimma cheekatigunna zindagilona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chimma cheekatigunna zindagilona"/>
</div>
<div class="lyrico-lyrics-wrapper">Flood light yesaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Flood light yesaave"/>
</div>
<div class="lyrico-lyrics-wrapper">Hattheri nachhesaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hattheri nachhesaave"/>
</div>
<div class="lyrico-lyrics-wrapper">Masthuga nachesaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Masthuga nachesaave"/>
</div>
<div class="lyrico-lyrics-wrapper">Black and white local gani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Black and white local gani"/>
</div>
<div class="lyrico-lyrics-wrapper">Lokam loni rangulu poosave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lokam loni rangulu poosave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chitti naa bul bul chitti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chitti naa bul bul chitti"/>
</div>
<div class="lyrico-lyrics-wrapper">Chiiti naa chilbul chitti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chiiti naa chilbul chitti"/>
</div>
<div class="lyrico-lyrics-wrapper">Na rendu buggalu patti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na rendu buggalu patti"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudhulu pettaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhulu pettaave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chitti naa jiljil chitti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chitti naa jiljil chitti"/>
</div>
<div class="lyrico-lyrics-wrapper">Chitti naa redbull chitti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chitti naa redbull chitti"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa facebook lo laksha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa facebook lo laksha"/>
</div>
<div class="lyrico-lyrics-wrapper">Like lu kottaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Like lu kottaave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yuddhamemi jaragale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yuddhamemi jaragale"/>
</div>
<div class="lyrico-lyrics-wrapper">Sumolevi assalu yegarale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sumolevi assalu yegarale"/>
</div>
<div class="lyrico-lyrics-wrapper">Chitikelo alaa chinna navvutho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chitikelo alaa chinna navvutho"/>
</div>
<div class="lyrico-lyrics-wrapper">Paccha janda choopinchanave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paccha janda choopinchanave"/>
</div>
<div class="lyrico-lyrics-wrapper">Madam elizabethu nee range aina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madam elizabethu nee range aina"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaadu bongaram leni awara la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaadu bongaram leni awara la"/>
</div>
<div class="lyrico-lyrics-wrapper">Nene aina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nene aina"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maassugaadi manasuke vote esaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maassugaadi manasuke vote esaave"/>
</div>
<div class="lyrico-lyrics-wrapper">Bangla nundi basthi ki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bangla nundi basthi ki"/>
</div>
<div class="lyrico-lyrics-wrapper">Flight esaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Flight esaave"/>
</div>
<div class="lyrico-lyrics-wrapper">Teenmar chinnodini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teenmar chinnodini"/>
</div>
<div class="lyrico-lyrics-wrapper">DJ steppulu aadisthive
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="DJ steppulu aadisthive"/>
</div>
<div class="lyrico-lyrics-wrapper">Naseebu bad unnodni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naseebu bad unnodni"/>
</div>
<div class="lyrico-lyrics-wrapper">Nawab chesesethive
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nawab chesesethive"/>
</div>
<div class="lyrico-lyrics-wrapper">Athiloka sundari nuvvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athiloka sundari nuvvu"/>
</div>
<div class="lyrico-lyrics-wrapper">Afterall o tappori nenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Afterall o tappori nenu"/>
</div>
<div class="lyrico-lyrics-wrapper">Google map ayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Google map ayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee gundeki cheristhive
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee gundeki cheristhive"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arere icchesaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arere icchesaave"/>
</div>
<div class="lyrico-lyrics-wrapper">Dil naaku icchesaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil naaku icchesaave"/>
</div>
<div class="lyrico-lyrics-wrapper">Mirchi bajji laanti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mirchi bajji laanti"/>
</div>
<div class="lyrico-lyrics-wrapper">Life lo nuvvu onion vesaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life lo nuvvu onion vesaave"/>
</div>
<div class="lyrico-lyrics-wrapper">Arere gucchesaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arere gucchesaave"/>
</div>
<div class="lyrico-lyrics-wrapper">Love tattoo guchchesaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love tattoo guchchesaave"/>
</div>
<div class="lyrico-lyrics-wrapper">Masthu masthu biryanilo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Masthu masthu biryanilo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nimbu chakkai hulchul chesaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nimbu chakkai hulchul chesaave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chitti naa bul bul chitti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chitti naa bul bul chitti"/>
</div>
<div class="lyrico-lyrics-wrapper">Chiiti naa chulbul chitti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chiiti naa chulbul chitti"/>
</div>
<div class="lyrico-lyrics-wrapper">Na rendu buggalu patti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na rendu buggalu patti"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudhulu pettaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhulu pettaave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chitti naa jiljil chitti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chitti naa jiljil chitti"/>
</div>
<div class="lyrico-lyrics-wrapper">Chitti naa redbull chitti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chitti naa redbull chitti"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa facebook lo laksha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa facebook lo laksha"/>
</div>
<div class="lyrico-lyrics-wrapper">Like lu kottaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Like lu kottaave"/>
</div>
</pre>
