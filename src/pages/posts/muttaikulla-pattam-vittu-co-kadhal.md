---
title: "muttaikulla pattam vittu song lyrics"
album: "C/o Kadhal"
artist: "Sweekar Agasthi"
lyricist: "Karthik Netha"
director: "Hemambar Jasti"
path: "/albums/co-kadhal-lyrics"
song: "Muttaikulla Pattam Vittu"
image: ../../images/albumart/co-kadhal.jpg
date: 2021-02-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/9dDNUet5xZk"
type: "Love"
singers:
  - Anthony dasan 
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Muttaikulla pattamvittu ninnaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muttaikulla pattamvittu ninnaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Sattaikkulla kuttakkulam kandaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sattaikkulla kuttakkulam kandaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Yellukulla vellamkilli thanthaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yellukulla vellamkilli thanthaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Mullukulla panjumethai kondaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mullukulla panjumethai kondaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Oththa pana mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oththa pana mela"/>
</div>
<div class="lyrico-lyrics-wrapper">Muthi malai pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthi malai pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Thonki thirinjaane oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thonki thirinjaane oh"/>
</div>
<div class="lyrico-lyrics-wrapper">Iththanaikum vearu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iththanaikum vearu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathalathu paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathalathu paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Sokki karanjaane oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sokki karanjaane oh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Englisu tappasu ravussu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Englisu tappasu ravussu"/>
</div>
<div class="lyrico-lyrics-wrapper">Podu thillele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podu thillele"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeh….oh…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh….oh…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thandhannaare thaare thandare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandhannaare thaare thandare"/>
</div>
<div class="lyrico-lyrics-wrapper">Thandhannaare thaare tharare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandhannaare thaare tharare"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalli chedi melae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalli chedi melae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattu pochchi polae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattu pochchi polae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanki sellum kaadhal vizhathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanki sellum kaadhal vizhathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Sangukulla kaathathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sangukulla kaathathaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vangukulla ooththaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vangukulla ooththaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pongi varum kaathal nillathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pongi varum kaathal nillathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Podani pudichchu munne thallum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podani pudichchu munne thallum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pathani kudichchu pinne thullum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathani kudichchu pinne thullum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaththu thanni polae odum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaththu thanni polae odum"/>
</div>
<div class="lyrico-lyrics-wrapper">Yethukka vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethukka vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Morappaa nikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Morappaa nikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Porukka vanthu padapadakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porukka vanthu padapadakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal saagathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal saagathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oora vittu oodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oora vittu oodum"/>
</div>
<div class="lyrico-lyrics-wrapper">Saathi matham maarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saathi matham maarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolla varum pothum azhadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolla varum pothum azhadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Puppadaiyum munne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puppadaiyum munne"/>
</div>
<div class="lyrico-lyrics-wrapper">Bokka vantha pinnae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bokka vantha pinnae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeppa venaa thonrum sollathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeppa venaa thonrum sollathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal irukka yellai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal irukka yellai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadavi thadavi ulle sellum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadavi thadavi ulle sellum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilagi vilagi veliye thallum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilagi vilagi veliye thallum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaga motham kaadhal veara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaga motham kaadhal veara"/>
</div>
<div class="lyrico-lyrics-wrapper">Uriki uriki nalla kollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uriki uriki nalla kollum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuruga vachu kooda pinnum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuruga vachu kooda pinnum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal kooththaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal kooththaadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Englisu tappasu ravussu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Englisu tappasu ravussu"/>
</div>
<div class="lyrico-lyrics-wrapper">Podu thillele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podu thillele"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeh….yeh…yeh yeh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh….yeh…yeh yeh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anga inga thedum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anga inga thedum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjukulla paadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjukulla paadum"/>
</div>
<div class="lyrico-lyrics-wrapper">Buththi ketta kaadhal vidaathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Buththi ketta kaadhal vidaathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Katta vandi oottum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katta vandi oottum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sooravali aagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sooravali aagum"/>
</div>
<div class="lyrico-lyrics-wrapper">Veshagattum kaadhal sikkaathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veshagattum kaadhal sikkaathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kenathuthukkulla nelavaasuththum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kenathuthukkulla nelavaasuththum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuralukkulla isaiyaa kaththum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuralukkulla isaiyaa kaththum"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaadikulla ooththa ooththum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaadikulla ooththa ooththum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thurivi thrivi kanavaa kottum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thurivi thrivi kanavaa kottum"/>
</div>
<div class="lyrico-lyrics-wrapper">Maruvi maruvi utchu kottum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maruvi maruvi utchu kottum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathal naadodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathal naadodi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeh Englisu tappasu ravussu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh Englisu tappasu ravussu"/>
</div>
<div class="lyrico-lyrics-wrapper">Podu thillele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podu thillele"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeh….yeh…Oh..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh….yeh…Oh.."/>
</div>
</pre>
