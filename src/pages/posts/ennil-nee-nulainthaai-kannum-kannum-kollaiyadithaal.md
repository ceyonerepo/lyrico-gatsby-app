---
title: "ennil nee nulainthaai song lyrics"
album: "Kannum Kannum Kollaiyadithaal"
artist: "Masala Coffee - Harshavardhan Rameshwar"
lyricist: "Desingh Periyasamy"
director: "Desingh Periyasamy"
path: "/albums/kannum-kannum-kollaiyadithaal-lyrics"
song: "Ennil Nee Nulainthaai"
image: ../../images/albumart/kannum-kannum-kollaiyadithaal.jpg
date: 2020-02-28
lang: tamil
youtubeLink: "https://www.youtube.com/embed/GJKpWLljcuI"
type: "Sad"
singers:
  - Janani SV
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ennil Nee Nulaindhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennil Nee Nulaindhaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enai Marandhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai Marandhen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyiraai Kalandhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyiraai Kalandhaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Ilanthene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Ilanthene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhalin Mogathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalin Mogathil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thilaithidum Velai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thilaithidum Velai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhayathai Parithu Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayathai Parithu Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thirudi Sendraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirudi Sendraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannum Kannum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannum Kannum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannum Kolliyadikudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannum Kolliyadikudhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manam Vedi Ondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam Vedi Ondru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vedithathil Setharudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedithathil Setharudhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu Uyir Varai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Uyir Varai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaedi Sendru Orasudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaedi Sendru Orasudhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannum Kannum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannum Kannum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kollaiyadikudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kollaiyadikudhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannum Kannum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannum Kannum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kollaiyadikudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kollaiyadikudhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannum Kannum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannum Kannum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannum Kollaiyadikudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannum Kollaiyadikudhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannum Kannum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannum Kannum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannum Kollaiyadikudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannum Kollaiyadikudhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannum Kannum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannum Kannum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannum Kollaiyadithaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannum Kollaiyadithaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannum Kannum Kollaiyadithaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannum Kannum Kollaiyadithaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannum Kannum Kollaiyadithaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannum Kannum Kollaiyadithaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adithaal Adithaal Adithaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adithaal Adithaal Adithaal"/>
</div>
</pre>
