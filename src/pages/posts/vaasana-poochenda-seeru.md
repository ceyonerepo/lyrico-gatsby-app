---
title: "vaasana poochenda song lyrics"
album: "Seeru"
artist: "D. Imman"
lyricist: "Parvathy"
director: "Rathnasiva"
path: "/albums/seeru-lyrics"
song: "Vaasana Poochenda"
image: ../../images/albumart/seeru.jpg
date: 2020-02-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Axi_NFlPIm8"
type: "Love"
singers:
  - Rajaganapathy
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vaasana Poochenda Pesura Kalkandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaasana Poochenda Pesura Kalkandaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Etti Ninna Innum Pakkam Vandhida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Etti Ninna Innum Pakkam Vandhida"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vazhigalthaan Undaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhigalthaan Undaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannukku Ullaara Ulladhu Pon Vandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannukku Ullaara Ulladhu Pon Vandaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iva Yaro Enna Pero Unmaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iva Yaro Enna Pero Unmaiyil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unmaiya Yar Kandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmaiya Yar Kandaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manja Saamandhi Nerathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manja Saamandhi Nerathula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sidharudhu Veesura Poongaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sidharudhu Veesura Poongaathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sillu Sillaa Therikkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sillu Sillaa Therikkuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sirippula Aambala Pammaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirippula Aambala Pammaathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu Paer thaan Edho Maya Mandhiramma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Paer thaan Edho Maya Mandhiramma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kooda Vandha Enna Aerka Sammadhamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooda Vandha Enna Aerka Sammadhamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaasana Poochenda Pesura Kalkandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaasana Poochenda Pesura Kalkandaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Etti Ninna Innum Pakkam Vandhida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Etti Ninna Innum Pakkam Vandhida"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vazhigalthaan Undaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhigalthaan Undaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannukku Ullaara Ulladhu Pon Vandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannukku Ullaara Ulladhu Pon Vandaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iva Yaro Enna Pero Unmaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iva Yaro Enna Pero Unmaiyil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unmaiya Yar Kandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmaiya Yar Kandaa"/>
</div>
</pre>
