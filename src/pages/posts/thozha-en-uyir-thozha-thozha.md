---
title: "thozha en uyir thozha song lyrics"
album: "Thozha"
artist: "Gopi Sundar"
lyricist: "Madhan Karky"
director: "Vamsi Paidipally"
path: "/albums/thozha-lyrics"
song: "Thozha en uyir Thozha"
image: ../../images/albumart/thozha.jpg
date: 2016-03-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/SvmARaZhA8M"
type: "Enjoy"
singers:
  - Shanker Mahadevan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Thozha En Uyir Thozha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozha En Uyir Thozha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhinamum Ingey Thiruvizha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinamum Ingey Thiruvizha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thozha Nirkaathae Thozha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozha Nirkaathae Thozha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Vaazhkai Undhan Thiruvizhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Vaazhkai Undhan Thiruvizhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yethanai Vannangal Immannil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethanai Vannangal Immannil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endre Nee Ennidu Hey Ennidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endre Nee Ennidu Hey Ennidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aththanai Vannamum Un
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aththanai Vannamum Un"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannil Pooti Nee Minnidu Hey Minnidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannil Pooti Nee Minnidu Hey Minnidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thozha Vaa Thozha Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozha Vaa Thozha Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thozha Vaa Thozha Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozha Vaa Thozha Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Siragai Siragai Neeti Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Siragai Siragai Neeti Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thozha Vaa Thozha Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozha Vaa Thozha Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thozha Vaa Thozha Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozha Vaa Thozha Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Kavalai Marandhu Nee Siri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Kavalai Marandhu Nee Siri"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tholaa En Uyir Tholaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholaa En Uyir Tholaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhinamum Ingae Thiruvizha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinamum Ingae Thiruvizha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tholaa Nirkkaathey Tholaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholaa Nirkkaathey Tholaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Vaazhkai Undhan Thiruvizhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Vaazhkai Undhan Thiruvizhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaanavillai Kaanavillai Ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanavillai Kaanavillai Ennai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endre Thedi Kidanthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endre Thedi Kidanthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indru Enai Naan Kandupidiththen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indru Enai Naan Kandupidiththen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennul Oru Veru Naana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennul Oru Veru Naana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aiyo Idhu Unmai Thaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyo Idhu Unmai Thaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nandri Iraiva Veru Enai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nandri Iraiva Veru Enai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Kandupidiththen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Kandupidiththen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pothum Ezhu Vaanile Vizhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pothum Ezhu Vaanile Vizhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inbam Izhu Unnai Thozhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inbam Izhu Unnai Thozhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjai Uzhu Vaanavil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjai Uzhu Vaanavil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidhaithiduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidhaithiduvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thozha Vaa Thozha Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozha Vaa Thozha Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thozha Vaa Thozha Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozha Vaa Thozha Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Siragai Siragai Neettu Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siragai Siragai Neettu Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thozha Vaa Thozha Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozha Vaa Thozha Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thozha Vaa Thozha Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozha Vaa Thozha Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kavalai Maranthu Nee Siri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavalai Maranthu Nee Siri"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thozha En Uyir Thozha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozha En Uyir Thozha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhinamum Inge Thiruvizha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinamum Inge Thiruvizha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thozha Nirkaathe Thozha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozha Nirkaathe Thozha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Vaazhkai Undhan Thiruvizhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Vaazhkai Undhan Thiruvizhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eththanai Vannangal Immannil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eththanai Vannangal Immannil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endrae Nee Ennidu Hae Ennidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endrae Nee Ennidu Hae Ennidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aththanai Vannamum Un
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aththanai Vannamum Un"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannil Pootti Nee Minnidu Hae Minnidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannil Pootti Nee Minnidu Hae Minnidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thozha Vaa Thozha Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozha Vaa Thozha Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thozha Vaa Thozha Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozha Vaa Thozha Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Siragai Siragai Neeti Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Siragai Siragai Neeti Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thozha Vaa Thozha Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozha Vaa Thozha Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thozha Vaa Thozha Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozha Vaa Thozha Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Kavalai Marandhu Nee Siri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Kavalai Marandhu Nee Siri"/>
</div>
</pre>
