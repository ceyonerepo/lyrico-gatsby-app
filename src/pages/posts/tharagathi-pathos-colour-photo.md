---
title: "tharagathi pathos song lyrics"
album: "Colour Photo"
artist: "Kaala Bhairava"
lyricist: "Kittu Vissapragada"
director: "Vijay Kumar Konda"
path: "/albums/colour-photo-lyrics"
song: "Tharagathi Pathos"
image: ../../images/albumart/colour-photo.jpg
date: 2020-10-23
lang: telugu
youtubeLink: "https://www.youtube.com/embed/7GfyQTiK8Yc"
type: "sad"
singers:
  - Kaala Bhairava
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Raane panjaraanaa kathai poye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raane panjaraanaa kathai poye"/>
</div>
<div class="lyrico-lyrics-wrapper">Raate taarumaarai gatam kore
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raate taarumaarai gatam kore"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Doorapu kondallo daagina sooridu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Doorapu kondallo daagina sooridu"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheekati kannullo chemmanu nimpaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheekati kannullo chemmanu nimpaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Taragati gadilone migilina manasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Taragati gadilone migilina manasu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Samayamu gadichenaa nedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samayamu gadichenaa nedu"/>
</div>
<div class="lyrico-lyrics-wrapper">Atu itu etu antu vetikina kanulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atu itu etu antu vetikina kanulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dorakani jata kosam choopulu vaanai jaare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dorakani jata kosam choopulu vaanai jaare"/>
</div>
</pre>
