---
title: "ini oru vidhi seivom song lyrics"
album: "LKG"
artist: "Leon James"
lyricist: "Pa. Vijay"
director: "K. R. Prabhu"
path: "/albums/lkg-lyrics"
song: "Ini Oru Vidhi Seivom"
image: ../../images/albumart/lkg.jpg
date: 2019-02-22
lang: tamil
youtubeLink: "https://www.youtube.com/embed/vcMRsdfARfI"
type: "mass"
singers:
  - D. Sathyaprakash
  - Diwakar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ini Oru Vidhi Seivom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Oru Vidhi Seivom"/>
</div>
<div class="lyrico-lyrics-wrapper">Thani Oruvanaai Velvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thani Oruvanaai Velvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetrikennada Vega Thadaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetrikennada Vega Thadaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Porr Seivom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porr Seivom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thannai Tharubavan Thalaivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannai Tharubavan Thalaivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Virpavan Vanigan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Virpavan Vanigan"/>
</div>
<div class="lyrico-lyrics-wrapper">Note-Ai Veesi Un Vote-Ai Perubavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Note-Ai Veesi Un Vote-Ai Perubavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Per Thirudan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Per Thirudan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muyalidam Aamaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muyalidam Aamaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Aarambathil Thorkkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aarambathil Thorkkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudivilae Aamai Munthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudivilae Aamai Munthi"/>
</div>
<div class="lyrico-lyrics-wrapper">Muyalai Jeyikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muyalai Jeyikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ulagamae Edhai Seithaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagamae Edhai Seithaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Elli Nagai Aadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Elli Nagai Aadum"/>
</div>
<div class="lyrico-lyrics-wrapper">Jeyithapin Thalai Mel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jeyithapin Thalai Mel"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaithu Aadidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaithu Aadidum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thalaivaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaivaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalaivaa Nee Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaivaa Nee Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Alai Kadal Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alai Kadal Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarukkum Adangaamal Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarukkum Adangaamal Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thalaivaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaivaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalaivaa Nee Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaivaa Nee Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thodum Dhoorathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodum Dhoorathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Thodu Vaanam Munneri Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodu Vaanam Munneri Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ini Oru Vidhi Seivom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Oru Vidhi Seivom"/>
</div>
<div class="lyrico-lyrics-wrapper">Thani Oruvanaai Velvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thani Oruvanaai Velvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetrikennada Vega Thadaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetrikennada Vega Thadaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Porr Seivom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porr Seivom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thannai Tharubavan Thalaivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannai Tharubavan Thalaivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Virpavan Vanigan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Virpavan Vanigan"/>
</div>
<div class="lyrico-lyrics-wrapper">Note-Ai Veesi Un Vote-Ai Perubavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Note-Ai Veesi Un Vote-Ai Perubavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Per Thirudan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Per Thirudan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hoo Oho Hoo Oho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo Oho Hoo Oho"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo Oo Hoo Ooo Oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo Oo Hoo Ooo Oo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo Oho Hoo Oho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo Oho Hoo Oho"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo Oo Hoo Ooo Oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo Oo Hoo Ooo Oo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vilum Thooral Veenaagathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilum Thooral Veenaagathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu Nathiyaagi Karai Meeri Odum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu Nathiyaagi Karai Meeri Odum"/>
</div>
<div class="lyrico-lyrics-wrapper">Avamaanam Unnai Anaikkadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avamaanam Unnai Anaikkadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Velvikku Neruppaaga Maarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Velvikku Neruppaaga Maarum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adikkum Kaatru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adikkum Kaatru"/>
</div>
<div class="lyrico-lyrics-wrapper">Paravaiyum Parakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paravaiyum Parakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paperum Parakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paperum Parakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatrodu Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatrodu Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaatru Nindraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatru Nindraal"/>
</div>
<div class="lyrico-lyrics-wrapper">Therindhidum Nanbaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Therindhidum Nanbaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Paravai Matrum Paranthu Sellum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paravai Matrum Paranthu Sellum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaagitham Mannodu Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaagitham Mannodu Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thalaivaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaivaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalaivaa Nee Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaivaa Nee Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Alai Kadal Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alai Kadal Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarukkum Adangaamal Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarukkum Adangaamal Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thalaivaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaivaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalaivaa Nee Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaivaa Nee Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thodum Dhoorathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodum Dhoorathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Thodu Vaanam Munneri Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodu Vaanam Munneri Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hoo Oho Hoo Oho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo Oho Hoo Oho"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo Oo Hoo Ooo Oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo Oo Hoo Ooo Oo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo Oho Hoo Oho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo Oho Hoo Oho"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo Oo Hoo Ooo Oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo Oo Hoo Ooo Oo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ini Oru Vidhi Seivom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Oru Vidhi Seivom"/>
</div>
<div class="lyrico-lyrics-wrapper">Thani Oruvanaai Velvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thani Oruvanaai Velvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetrikennada Vega Thadaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetrikennada Vega Thadaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Porr Seivom Ho Ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porr Seivom Ho Ooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thannai Tharubavan Thalaivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannai Tharubavan Thalaivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Virpavan Vanigan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Virpavan Vanigan"/>
</div>
<div class="lyrico-lyrics-wrapper">Note-Ai Veesi Un Vote-Ai Perubavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Note-Ai Veesi Un Vote-Ai Perubavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Per Thirudan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Per Thirudan"/>
</div>
</pre>
