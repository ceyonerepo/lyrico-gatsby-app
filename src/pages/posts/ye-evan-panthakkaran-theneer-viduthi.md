---
title: "ye evan panthakkaran song lyrics"
album: "Theneer Viduthi"
artist: "S.S. Kumaran"
lyricist: "S.S. Kumaran"
director: "S.S. Kumaran"
path: "/albums/theneer-viduthi-lyrics"
song: "Ye Evan Panthakkaran"
image: ../../images/albumart/theneer-viduthi.jpg
date: 2011-07-01
lang: tamil
youtubeLink: 
type: "happy"
singers:
  - Yash Golcha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ho evan pandhakkaaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho evan pandhakkaaran"/>
</div>
<div class="lyrico-lyrics-wrapper">Tea-kadai varum neram ravusuthaan paar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tea-kadai varum neram ravusuthaan paar"/>
</div>
<div class="lyrico-lyrics-wrapper">tharaiyil medhappaan thanniyila iruppaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tharaiyil medhappaan thanniyila iruppaan"/>
</div>
<div class="lyrico-lyrics-wrapper">kodaichalum koduppaan pandhal Rasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kodaichalum koduppaan pandhal Rasa"/>
</div>
</pre>
