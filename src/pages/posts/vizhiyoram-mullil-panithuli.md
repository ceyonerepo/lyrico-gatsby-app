---
title: "vizhiyoram song lyrics"
album: "Mullil Panithuli"
artist: "Benny Pradeep "
lyricist: "Kavingar Mouli"
director: "NM Jegan"
path: "/albums/mullil-panithuli-lyrics"
song: "Vizhiyoram"
image: ../../images/albumart/mullil-panithuli.jpg
date: 2021-10-29
lang: tamil
youtubeLink: "https://www.youtube.com/embed/wvfQlTz4jbQ"
type: "sad"
singers:
  - Asha ramanathan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">vizhiyoram yen indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vizhiyoram yen indru"/>
</div>
<div class="lyrico-lyrics-wrapper">mazhai kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mazhai kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">vithiyaale idhayathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vithiyaale idhayathil"/>
</div>
<div class="lyrico-lyrics-wrapper">padarathu thee kolam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="padarathu thee kolam"/>
</div>
<div class="lyrico-lyrics-wrapper">viligalile perukuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viligalile perukuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">neer kolam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neer kolam"/>
</div>
<div class="lyrico-lyrics-wrapper">en vaalkai malaiyil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en vaalkai malaiyil "/>
</div>
<div class="lyrico-lyrics-wrapper">itta maa kolam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="itta maa kolam"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu yar saabam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu yar saabam"/>
</div>
<div class="lyrico-lyrics-wrapper">intha pen paavam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha pen paavam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vizhiyoram yen indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vizhiyoram yen indru"/>
</div>
<div class="lyrico-lyrics-wrapper">mazhai kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mazhai kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">vithiyaale idhayathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vithiyaale idhayathil"/>
</div>
<div class="lyrico-lyrics-wrapper">padarathu thee kolam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="padarathu thee kolam"/>
</div>
<div class="lyrico-lyrics-wrapper">viligalile perukuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viligalile perukuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">neer kolam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neer kolam"/>
</div>
<div class="lyrico-lyrics-wrapper">en vaalkai malaiyil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en vaalkai malaiyil "/>
</div>
<div class="lyrico-lyrics-wrapper">itta maa kolam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="itta maa kolam"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu yar saabam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu yar saabam"/>
</div>
<div class="lyrico-lyrics-wrapper">intha pen paavam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha pen paavam"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu yar saabam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu yar saabam"/>
</div>
<div class="lyrico-lyrics-wrapper">intha pen paavam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha pen paavam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vizhigalil valiyuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vizhigalil valiyuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">perum kadalin thuli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="perum kadalin thuli"/>
</div>
<div class="lyrico-lyrics-wrapper">vidhiyadum aatathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidhiyadum aatathil"/>
</div>
<div class="lyrico-lyrics-wrapper">vidhi thaan puli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidhi thaan puli"/>
</div>
<div class="lyrico-lyrics-wrapper">naan yen pali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan yen pali"/>
</div>
<div class="lyrico-lyrics-wrapper">idhayathil vilunthathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhayathil vilunthathu"/>
</div>
<div class="lyrico-lyrics-wrapper">eri malaiyin thuli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eri malaiyin thuli"/>
</div>
<div class="lyrico-lyrics-wrapper">evarenum arivero 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="evarenum arivero "/>
</div>
<div class="lyrico-lyrics-wrapper">uyirin vali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyirin vali"/>
</div>
<div class="lyrico-lyrics-wrapper">yarenum uraipero 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yarenum uraipero "/>
</div>
<div class="lyrico-lyrics-wrapper">en vaazhvin vali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en vaazhvin vali"/>
</div>
<div class="lyrico-lyrics-wrapper">naanor mullil men
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naanor mullil men"/>
</div>
<div class="lyrico-lyrics-wrapper">paniyin thuli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paniyin thuli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">annai aval aadhiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="annai aval aadhiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">mann urangi ponale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mann urangi ponale"/>
</div>
<div class="lyrico-lyrics-wrapper">thanthai avan pathiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanthai avan pathiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">pen urangi ponane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pen urangi ponane"/>
</div>
<div class="lyrico-lyrics-wrapper">annan avan veethiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="annan avan veethiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">vitu vitu ponane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vitu vitu ponane"/>
</div>
<div class="lyrico-lyrics-wrapper">manavanum mediyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manavanum mediyile"/>
</div>
<div class="lyrico-lyrics-wrapper">vaarthaiyale konaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaarthaiyale konaane"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu vithiya mun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu vithiya mun"/>
</div>
<div class="lyrico-lyrics-wrapper">seitha vidhaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seitha vidhaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">nanum ingu enna aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanum ingu enna aanen"/>
</div>
<div class="lyrico-lyrics-wrapper">paavam oru pen aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paavam oru pen aanen"/>
</div>
<div class="lyrico-lyrics-wrapper">suruthi tholantha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suruthi tholantha "/>
</div>
<div class="lyrico-lyrics-wrapper">suram aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suram aanen"/>
</div>
<div class="lyrico-lyrics-wrapper">uyir konda savam aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyir konda savam aanen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">en vaalkai enna aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en vaalkai enna aachu"/>
</div>
<div class="lyrico-lyrics-wrapper">cherupunji vaan aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cherupunji vaan aachu"/>
</div>
<div class="lyrico-lyrics-wrapper">vaan partha man aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaan partha man aachu"/>
</div>
<div class="lyrico-lyrics-wrapper">vadam aruntha ther aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vadam aruntha ther aachu"/>
</div>
<div class="lyrico-lyrics-wrapper">intha nilai maruma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha nilai maruma"/>
</div>
<div class="lyrico-lyrics-wrapper">thear nilai seruma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thear nilai seruma"/>
</div>
<div class="lyrico-lyrics-wrapper">vizhikalil mazhai theeruma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vizhikalil mazhai theeruma"/>
</div>
<div class="lyrico-lyrics-wrapper">idhayathil thee aaruma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhayathil thee aaruma"/>
</div>
<div class="lyrico-lyrics-wrapper">en vidhi maruma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en vidhi maruma"/>
</div>
<div class="lyrico-lyrics-wrapper">padal sruthi seruma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="padal sruthi seruma"/>
</div>
<div class="lyrico-lyrics-wrapper">vaalve vina thanamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalve vina thanamma"/>
</div>
<div class="lyrico-lyrics-wrapper">neeye vidai thedumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeye vidai thedumaa"/>
</div>
</pre>
