---
title: "velli nillavae lyrics"
album: "Eeswaran"
artist: "Thaman S"
lyricist: "Yugabharathi"
director: "Susienthiran"
path: "/albums/eeswaran-song-lyrics"
song: "Velli Nillavae"
image: ../../images/albumart/eeswaran.jpg
date: 2021-01-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/-AYCQa6_J00"
type: "Family Affection"
singers:
  - ML Gayatri
  
---

<pre class="lyrics-native">
</pre>
<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Velli Nilavozhi Veedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velli Nilavozhi Veedu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhagatha Anbil Aadi Paadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhagatha Anbil Aadi Paadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sangathamizh Isaiyodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sangathamizh Isaiyodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sadhiram Aadum Sondham Theaneer Koodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sadhiram Aadum Sondham Theaneer Koodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhagiya Uravu Thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagiya Uravu Thaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Endrum Enge Anandham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endrum Enge Anandham"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavugal Kalainthidamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavugal Kalainthidamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaigal Sehra Perinbam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaigal Sehra Perinbam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vannangal Ethanai Vaanavil Sollida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vannangal Ethanai Vaanavil Sollida"/>
</div>
<div class="lyrico-lyrics-wrapper">Deivangal Moththamum Archanai Seithida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Deivangal Moththamum Archanai Seithida"/>
</div>
<div class="lyrico-lyrics-wrapper">Aththai Badhilengu Sorgam Kidaithidum Sugam Virigirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aththai Badhilengu Sorgam Kidaithidum Sugam Virigirathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Velli Nilavozhi Veedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velli Nilavozhi Veedu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhagatha Anbil Aadi Paadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhagatha Anbil Aadi Paadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sangathamizh Isaiyodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sangathamizh Isaiyodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sadhiram Aadum Sondham Theaneer Koodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sadhiram Aadum Sondham Theaneer Koodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaatril Veesum Marangal Alla Uravugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatril Veesum Marangal Alla Uravugal"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu Kalanthu Vaazhum Azhagai Kaattum Kavidhaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu Kalanthu Vaazhum Azhagai Kaattum Kavidhaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Deebam Yetrum Pozhuthe Nalla Ninaivugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Deebam Yetrum Pozhuthe Nalla Ninaivugal"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu Soodarai Maari Uyirai Neettum Inimaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu Soodarai Maari Uyirai Neettum Inimaigal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pattu Therinthidum Minnal Veezhthina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattu Therinthidum Minnal Veezhthina"/>
</div>
<div class="lyrico-lyrics-wrapper">Paasam Viralai Neettuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paasam Viralai Neettuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetta Veliyilum Muttum Malarnthida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetta Veliyilum Muttum Malarnthida"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannil Vasantham Pookkuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannil Vasantham Pookkuthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vannangal Ethanai Vaanavil Sollida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vannangal Ethanai Vaanavil Sollida"/>
</div>
<div class="lyrico-lyrics-wrapper">Deivangal Moththamum Archanai Seithida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Deivangal Moththamum Archanai Seithida"/>
</div>
<div class="lyrico-lyrics-wrapper">Aththai Badhilengu Sorgam Kidaithidum Sugam Virigirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aththai Badhilengu Sorgam Kidaithidum Sugam Virigirathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Velli Nilavozhi Veedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velli Nilavozhi Veedu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhagatha Anbil Aadi Paadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhagatha Anbil Aadi Paadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sangathamizh Isaiyodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sangathamizh Isaiyodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sadhiram Aadum Sondham Theaneer Koodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sadhiram Aadum Sondham Theaneer Koodu"/>
</div>
</pre>
