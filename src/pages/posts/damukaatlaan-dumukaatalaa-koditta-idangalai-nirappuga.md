---
title: "damukaatlaan dumukaatalaa song lyrics"
album: "Koditta Idangalai Nirappuga"
artist: "C Sathya"
lyricist: "Logan"
director: "Parthiban"
path: "/albums/koditta-idangalai-nirappuga-lyrics"
song: "Damukaatlaan Dumukaatalaa"
image: ../../images/albumart/koditta-idangalai-nirappuga.jpg
date: 2017-01-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ggbfBoxgqwQ"
type: "happy"
singers:
  - T Rajender
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Dumukaatla damukaatla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dumukaatla damukaatla"/>
</div>
<div class="lyrico-lyrics-wrapper">Damukaatla dumukaatla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Damukaatla dumukaatla"/>
</div>
<div class="lyrico-lyrics-wrapper">Dumukaatla damukaatla dumukaatla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dumukaatla damukaatla dumukaatla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oye damukaatla dumukaatla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oye damukaatla dumukaatla"/>
</div>
<div class="lyrico-lyrics-wrapper">Dumukaatla damukaatla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dumukaatla damukaatla"/>
</div>
<div class="lyrico-lyrics-wrapper">Damukaatla dumukaatla damukaatla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Damukaatla dumukaatla damukaatla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saalu savukaru acha pagaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saalu savukaru acha pagaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Seen mai they konu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seen mai they konu "/>
</div>
<div class="lyrico-lyrics-wrapper">sennla sedharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sennla sedharu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaacha vutta vutta 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaacha vutta vutta "/>
</div>
<div class="lyrico-lyrics-wrapper">lite-a thotta thotta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lite-a thotta thotta"/>
</div>
<div class="lyrico-lyrics-wrapper">Yellam attu figure-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yellam attu figure-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vazhka ponakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhka ponakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Inna irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inna irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavala vanthalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavala vanthalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhoora othukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhoora othukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponnu othukkunaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponnu othukkunaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Paiyan Kattikkina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paiyan Kattikkina"/>
</div>
<div class="lyrico-lyrics-wrapper">Veena sanda yethukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veena sanda yethukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalyaanam analum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalyaanam analum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal varunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal varunda"/>
</div>
<div class="lyrico-lyrics-wrapper">Divorce analum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Divorce analum"/>
</div>
<div class="lyrico-lyrics-wrapper">Marriage aagunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marriage aagunda"/>
</div>
<div class="lyrico-lyrics-wrapper">Husband kandukala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Husband kandukala"/>
</div>
<div class="lyrico-lyrics-wrapper">Lover kandukuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lover kandukuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal pathikkichidaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal pathikkichidaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dumukaatla damukaatla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dumukaatla damukaatla"/>
</div>
<div class="lyrico-lyrics-wrapper">Damukaatla dumukaatla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Damukaatla dumukaatla"/>
</div>
<div class="lyrico-lyrics-wrapper">Dumukaatla damukaatla dumukaatla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dumukaatla damukaatla dumukaatla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oye damukaatla dumukaatla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oye damukaatla dumukaatla"/>
</div>
<div class="lyrico-lyrics-wrapper">Dumukaatla damukaatla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dumukaatla damukaatla"/>
</div>
<div class="lyrico-lyrics-wrapper">Damukaatla dumukaatla damukaatla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Damukaatla dumukaatla damukaatla"/>
</div>
<div class="lyrico-lyrics-wrapper">Pothuva kaadhal naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pothuva kaadhal naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Innanu sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innanu sollu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ozhunga sollangatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ozhunga sollangatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottidum pallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottidum pallu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kedacha vudu jollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kedacha vudu jollu"/>
</div>
<div class="lyrico-lyrics-wrapper">Illaiya vuttu thallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illaiya vuttu thallu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhilurintha allu allu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhilurintha allu allu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thappi thavaru senja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappi thavaru senja"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappu illada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappu illada"/>
</div>
<div class="lyrico-lyrics-wrapper">Adha nee repeat senja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adha nee repeat senja"/>
</div>
<div class="lyrico-lyrics-wrapper">Reveat thanada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Reveat thanada"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalla kaadhalukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla kaadhalukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalla kaadhalukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalla kaadhalukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal kamam thanada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal kamam thanada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dumukaatla damukaatla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dumukaatla damukaatla"/>
</div>
<div class="lyrico-lyrics-wrapper">Damukaatla dumukaatla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Damukaatla dumukaatla"/>
</div>
<div class="lyrico-lyrics-wrapper">Dumukaatla damukaatla dumukaatla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dumukaatla damukaatla dumukaatla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oye damukaatla dumukaatla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oye damukaatla dumukaatla"/>
</div>
<div class="lyrico-lyrics-wrapper">Dumukaatla damukaatla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dumukaatla damukaatla"/>
</div>
<div class="lyrico-lyrics-wrapper">Damukaatla dumukaatla damukaatla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Damukaatla dumukaatla damukaatla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey thaantha naaka hey thaantha naaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey thaantha naaka hey thaantha naaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey thaantha naaka nakka naaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey thaantha naaka nakka naaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey thaantha naaka nakka nakka nakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey thaantha naaka nakka nakka nakka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hei hei heiheiii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei hei heiheiii"/>
</div>
<div class="lyrico-lyrics-wrapper">Dumukaatla damukaatla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dumukaatla damukaatla"/>
</div>
<div class="lyrico-lyrics-wrapper">Damukaatla dumukaatla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Damukaatla dumukaatla"/>
</div>
<div class="lyrico-lyrics-wrapper">Dumukaatla damukaatla dumukaatla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dumukaatla damukaatla dumukaatla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oye damukaatla dumukaatla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oye damukaatla dumukaatla"/>
</div>
<div class="lyrico-lyrics-wrapper">Dumukaatla damukaatla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dumukaatla damukaatla"/>
</div>
<div class="lyrico-lyrics-wrapper">Damukaatla dumukaatla damukaatla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Damukaatla dumukaatla damukaatla"/>
</div>
</pre>
