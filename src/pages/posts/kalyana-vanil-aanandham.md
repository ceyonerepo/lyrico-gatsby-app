---
title: "kalyana vanil song lyrics"
album: "Aanandham"
artist: "S. A. Rajkumar"
lyricist: "Tholkapiyan"
director: "N. Lingusamy"
path: "/albums/aanandham-lyrics"
song: "Kalyana Vanil"
image: ../../images/albumart/aanandham.jpg
date: 2001-05-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/j-wNFZd4jic"
type: "love"
singers:
  - Unni Menon
  - Sujatha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">hey jolika jolika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey jolika jolika"/>
</div>
<div class="lyrico-lyrics-wrapper">oru jodi pura vanthiruchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru jodi pura vanthiruchu"/>
</div>
<div class="lyrico-lyrics-wrapper">sari satha patha sari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sari satha patha sari"/>
</div>
<div class="lyrico-lyrics-wrapper">sari satha patha sari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sari satha patha sari"/>
</div>
<div class="lyrico-lyrics-wrapper">samanthi vasathil rendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="samanthi vasathil rendum"/>
</div>
<div class="lyrico-lyrics-wrapper">santhanathai poosikichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="santhanathai poosikichu"/>
</div>
<div class="lyrico-lyrics-wrapper">sari satha patha sari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sari satha patha sari"/>
</div>
<div class="lyrico-lyrics-wrapper">sari satha patha sari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sari satha patha sari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kalyana vaanil pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalyana vaanil pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">megam oorkolam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="megam oorkolam"/>
</div>
<div class="lyrico-lyrics-wrapper">kannodu thane kannadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannodu thane kannadi"/>
</div>
<div class="lyrico-lyrics-wrapper">paarkum aagayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paarkum aagayam"/>
</div>
<div class="lyrico-lyrics-wrapper">aliyathathu kadhal kolam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aliyathathu kadhal kolam"/>
</div>
<div class="lyrico-lyrics-wrapper">ooyamale ketkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooyamale ketkum"/>
</div>
<div class="lyrico-lyrics-wrapper">kaathil mela nayanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaathil mela nayanam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">neeyo etrum theepathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeyo etrum theepathil"/>
</div>
<div class="lyrico-lyrics-wrapper">kaarthikai naalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaarthikai naalum"/>
</div>
<div class="lyrico-lyrics-wrapper">veetukul vara vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veetukul vara vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">endhan thotathu poovukum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="endhan thotathu poovukum"/>
</div>
<div class="lyrico-lyrics-wrapper">pookum aasai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pookum aasai"/>
</div>
<div class="lyrico-lyrics-wrapper">unnal ela vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnal ela vendum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kalyana vaanil pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalyana vaanil pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">megam oorkolam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="megam oorkolam"/>
</div>
<div class="lyrico-lyrics-wrapper">kannodu thane kannadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannodu thane kannadi"/>
</div>
<div class="lyrico-lyrics-wrapper">paarkum aagayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paarkum aagayam"/>
</div>
<div class="lyrico-lyrics-wrapper">aliyathathu kadhal kolam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aliyathathu kadhal kolam"/>
</div>
<div class="lyrico-lyrics-wrapper">ooyamale ketkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooyamale ketkum"/>
</div>
<div class="lyrico-lyrics-wrapper">kaathil mela nayanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaathil mela nayanam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thaneer kudathil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaneer kudathil "/>
</div>
<div class="lyrico-lyrics-wrapper">nabagam nirappi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nabagam nirappi"/>
</div>
<div class="lyrico-lyrics-wrapper">vasal engum thelipene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vasal engum thelipene"/>
</div>
<div class="lyrico-lyrics-wrapper">nee itta kolam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee itta kolam "/>
</div>
<div class="lyrico-lyrics-wrapper">tholaivinil nindre
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tholaivinil nindre"/>
</div>
<div class="lyrico-lyrics-wrapper">yaro pola rasipene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaro pola rasipene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">jeeva nathi aengum aengum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jeeva nathi aengum aengum"/>
</div>
<div class="lyrico-lyrics-wrapper">adi manasu pongum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi manasu pongum"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjukuli engum engum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjukuli engum engum"/>
</div>
<div class="lyrico-lyrics-wrapper">un ninaivu thangum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un ninaivu thangum"/>
</div>
<div class="lyrico-lyrics-wrapper">nee mugam parkira kannadiku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee mugam parkira kannadiku"/>
</div>
<div class="lyrico-lyrics-wrapper">muthangalal archanai seiven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muthangalal archanai seiven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kalyana vaanil pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalyana vaanil pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">megam oorkolam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="megam oorkolam"/>
</div>
<div class="lyrico-lyrics-wrapper">kannodu thane kannadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannodu thane kannadi"/>
</div>
<div class="lyrico-lyrics-wrapper">paarkum aagayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paarkum aagayam"/>
</div>
<div class="lyrico-lyrics-wrapper">aliyathathu kadhal kolam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aliyathathu kadhal kolam"/>
</div>
<div class="lyrico-lyrics-wrapper">ooyamale ketkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooyamale ketkum"/>
</div>
<div class="lyrico-lyrics-wrapper">kaathil mela nayanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaathil mela nayanam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">manaiviyana nimisathil irunthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manaiviyana nimisathil irunthu"/>
</div>
<div class="lyrico-lyrics-wrapper">unnai per solli alaipene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai per solli alaipene"/>
</div>
<div class="lyrico-lyrics-wrapper">thoongum pothum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoongum pothum "/>
</div>
<div class="lyrico-lyrics-wrapper">un per kettal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un per kettal"/>
</div>
<div class="lyrico-lyrics-wrapper">thulli elunthu paarpene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thulli elunthu paarpene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kuliyalarai pakkam pakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuliyalarai pakkam pakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">thalai thuvata varuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalai thuvata varuven"/>
</div>
<div class="lyrico-lyrics-wrapper">padukaiyinil micham micham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="padukaiyinil micham micham"/>
</div>
<div class="lyrico-lyrics-wrapper">vaithu ange tharuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaithu ange tharuven"/>
</div>
<div class="lyrico-lyrics-wrapper">valaiyosaiyal saigai katuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valaiyosaiyal saigai katuven"/>
</div>
<div class="lyrico-lyrics-wrapper">thayangamale thevai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thayangamale thevai "/>
</div>
<div class="lyrico-lyrics-wrapper">yavum ketpen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yavum ketpen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kalyana vaanil pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalyana vaanil pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">megam oorkolam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="megam oorkolam"/>
</div>
<div class="lyrico-lyrics-wrapper">kannodu thane kannadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannodu thane kannadi"/>
</div>
<div class="lyrico-lyrics-wrapper">paarkum aagayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paarkum aagayam"/>
</div>
<div class="lyrico-lyrics-wrapper">aliyathathu kadhal kolam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aliyathathu kadhal kolam"/>
</div>
<div class="lyrico-lyrics-wrapper">ooyamale ketkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooyamale ketkum"/>
</div>
<div class="lyrico-lyrics-wrapper">kaathil mela nayanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaathil mela nayanam"/>
</div>
</pre>
