---
title: "uyirodu uyir serum song lyrics"
album: "Roja Maaligai"
artist: "Leo"
lyricist: "Nagi Prasad"
director: "Goutham"
path: "/albums/roja-maaligai-lyrics"
song: "Uyirodu Uyir Serum"
image: ../../images/albumart/roja-maaligai.jpg
date: 2018-07-06
lang: tamil
youtubeLink: "https://www.youtube.com/embed/bXf8nvadu7o"
type: "love"
singers:
  - Leo
  - Krithika
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">uyirodu uyir serum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyirodu uyir serum"/>
</div>
<div class="lyrico-lyrics-wrapper">neram ithu thaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neram ithu thaana"/>
</div>
<div class="lyrico-lyrics-wrapper">ithalodu ithal mothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithalodu ithal mothum"/>
</div>
<div class="lyrico-lyrics-wrapper">inbam ithu thaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inbam ithu thaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">uyirodu uyir serum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyirodu uyir serum"/>
</div>
<div class="lyrico-lyrics-wrapper">neram ithu thaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neram ithu thaana"/>
</div>
<div class="lyrico-lyrics-wrapper">ithalodu ithal mothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithalodu ithal mothum"/>
</div>
<div class="lyrico-lyrics-wrapper">inbam ithu thaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inbam ithu thaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sugame unai nan theendava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sugame unai nan theendava"/>
</div>
<div class="lyrico-lyrics-wrapper">ithame ellai thaan meerivaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithame ellai thaan meerivaa"/>
</div>
<div class="lyrico-lyrics-wrapper">varamaai oru naal varuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varamaai oru naal varuven"/>
</div>
<div class="lyrico-lyrics-wrapper">tharuvaai oru thaaragame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tharuvaai oru thaaragame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">uyirodu uyir serum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyirodu uyir serum"/>
</div>
<div class="lyrico-lyrics-wrapper">neram ithu thaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neram ithu thaana"/>
</div>
<div class="lyrico-lyrics-wrapper">ithalodu ithal mothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithalodu ithal mothum"/>
</div>
<div class="lyrico-lyrics-wrapper">inbam ithu thaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inbam ithu thaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ithalodu theekandu thanthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithalodu theekandu thanthai"/>
</div>
<div class="lyrico-lyrics-wrapper">udalodu por seithu vendrai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="udalodu por seithu vendrai"/>
</div>
<div class="lyrico-lyrics-wrapper">sila velai ver sendru thindrai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sila velai ver sendru thindrai"/>
</div>
<div class="lyrico-lyrics-wrapper">kulir kaalam ondraga nindrai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulir kaalam ondraga nindrai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vizhi moodinaal un
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vizhi moodinaal un"/>
</div>
<div class="lyrico-lyrics-wrapper">madi sera vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="madi sera vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">vazhi maarinal ennidam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vazhi maarinal ennidam"/>
</div>
<div class="lyrico-lyrics-wrapper">theda vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theda vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">uyir vaalvathum ennidam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyir vaalvathum ennidam"/>
</div>
<div class="lyrico-lyrics-wrapper">thorka vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thorka vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">unai thedinaal ennidam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unai thedinaal ennidam"/>
</div>
<div class="lyrico-lyrics-wrapper">paarka thondrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paarka thondrum"/>
</div>
<div class="lyrico-lyrics-wrapper">theerathadi en aasaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theerathadi en aasaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">pothathu ena por seigiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pothathu ena por seigiren"/>
</div>
<div class="lyrico-lyrics-wrapper">vidiyil vidiyil valiyum poluthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidiyil vidiyil valiyum poluthu"/>
</div>
<div class="lyrico-lyrics-wrapper">neyum nanum thedi thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neyum nanum thedi thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">mogam kolvomaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mogam kolvomaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">uyirodu uyir serum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyirodu uyir serum"/>
</div>
<div class="lyrico-lyrics-wrapper">neram ithu thaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neram ithu thaana"/>
</div>
<div class="lyrico-lyrics-wrapper">ithalodu ithal mothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithalodu ithal mothum"/>
</div>
<div class="lyrico-lyrics-wrapper">inbam ithu thaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inbam ithu thaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sumai koodinaal soladi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sumai koodinaal soladi"/>
</div>
<div class="lyrico-lyrics-wrapper">mella meendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mella meendum"/>
</div>
<div class="lyrico-lyrics-wrapper">en seedanai manmadhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en seedanai manmadhan"/>
</div>
<div class="lyrico-lyrics-wrapper">nirka vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nirka vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">pani moodiyum en udal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pani moodiyum en udal"/>
</div>
<div class="lyrico-lyrics-wrapper">verka vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="verka vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">anal vaatitum thagathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anal vaatitum thagathai"/>
</div>
<div class="lyrico-lyrics-wrapper">theerka vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theerka vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">moochaga than un thegame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moochaga than un thegame"/>
</div>
<div class="lyrico-lyrics-wrapper">en meethu than 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en meethu than "/>
</div>
<div class="lyrico-lyrics-wrapper">puyal veesuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puyal veesuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">neeril vizhiyum valiyum alagai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeril vizhiyum valiyum alagai"/>
</div>
<div class="lyrico-lyrics-wrapper">neyum nanum thedi thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neyum nanum thedi thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">mogam kolvomaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mogam kolvomaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">uyirodu uyir serum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyirodu uyir serum"/>
</div>
<div class="lyrico-lyrics-wrapper">neram ithu thaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neram ithu thaana"/>
</div>
<div class="lyrico-lyrics-wrapper">ithalodu ithal mothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithalodu ithal mothum"/>
</div>
<div class="lyrico-lyrics-wrapper">inbam ithu thaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inbam ithu thaana"/>
</div>
</pre>
