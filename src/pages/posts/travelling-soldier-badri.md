---
title: "travelling soldier song lyrics"
album: "Badri"
artist: "Ramana Gogula"
lyricist: "Palani Bharathi"
director: "P. A. Arun Prasad"
path: "/albums/badri-lyrics"
song: "Travelling Soldier"
image: ../../images/albumart/badri.jpg
date: 2001-04-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/oq-crJQKvUs"
type: "motivational"
singers:
  - Ramana Gogula
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Look At My Face In The Mirror
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Look At My Face In The Mirror"/>
</div>
<div class="lyrico-lyrics-wrapper">And I Wonder What I See
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="And I Wonder What I See"/>
</div>
<div class="lyrico-lyrics-wrapper">I’m Just A Travelling Soldier
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I’m Just A Travelling Soldier"/>
</div>
<div class="lyrico-lyrics-wrapper">And I’ll Be All I Can Be
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="And I’ll Be All I Can Be"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">But Right Now Right Now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="But Right Now Right Now"/>
</div>
<div class="lyrico-lyrics-wrapper">I Just Want To Be Free
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Just Want To Be Free"/>
</div>
<div class="lyrico-lyrics-wrapper">I Want To Be All I Can Be
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Want To Be All I Can Be"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">But Right Now Right Now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="But Right Now Right Now"/>
</div>
<div class="lyrico-lyrics-wrapper">I Just Want To Be Free
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Just Want To Be Free"/>
</div>
<div class="lyrico-lyrics-wrapper">I Want To Be All I Can Be
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Want To Be All I Can Be"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh They Say I’m A Failure
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh They Say I’m A Failure"/>
</div>
<div class="lyrico-lyrics-wrapper">Do They Know Who I Can Be?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Do They Know Who I Can Be?"/>
</div>
<div class="lyrico-lyrics-wrapper">If They Wanna Know Who I Am
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="If They Wanna Know Who I Am"/>
</div>
<div class="lyrico-lyrics-wrapper">They Just Have To Wait And See
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="They Just Have To Wait And See"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">But Right Now Right Now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="But Right Now Right Now"/>
</div>
<div class="lyrico-lyrics-wrapper">I Just Want To Be Free
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Just Want To Be Free"/>
</div>
<div class="lyrico-lyrics-wrapper">I Want To Be All I Can Be
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Want To Be All I Can Be"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Look At My Face In The Mirror
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Look At My Face In The Mirror"/>
</div>
<div class="lyrico-lyrics-wrapper">And I Wonder What I See
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="And I Wonder What I See"/>
</div>
<div class="lyrico-lyrics-wrapper">I’m Just A Travelling Soldier
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I’m Just A Travelling Soldier"/>
</div>
<div class="lyrico-lyrics-wrapper">And I’ll Be All I Can Be
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="And I’ll Be All I Can Be"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">But Right Now Right Now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="But Right Now Right Now"/>
</div>
<div class="lyrico-lyrics-wrapper">I Just Want To Be Free
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Just Want To Be Free"/>
</div>
<div class="lyrico-lyrics-wrapper">I Want To Be All I Can Be
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Want To Be All I Can Be"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">But Right Now Right Now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="But Right Now Right Now"/>
</div>
<div class="lyrico-lyrics-wrapper">I Just Want To Be Free
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Just Want To Be Free"/>
</div>
<div class="lyrico-lyrics-wrapper">I Want To Be All I Can Be
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Want To Be All I Can Be"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Hey I Wanna Be
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Hey I Wanna Be"/>
</div>
<div class="lyrico-lyrics-wrapper">All I Can Be
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="All I Can Be"/>
</div>
<div class="lyrico-lyrics-wrapper">Huh Huh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Huh Huh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">I’m Just A Travelling Soldier
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I’m Just A Travelling Soldier"/>
</div>
<div class="lyrico-lyrics-wrapper">And I’ll Be All I Can Be
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="And I’ll Be All I Can Be"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">But Right Now Right Now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="But Right Now Right Now"/>
</div>
<div class="lyrico-lyrics-wrapper">I Just Want To Be Free
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Just Want To Be Free"/>
</div>
<div class="lyrico-lyrics-wrapper">I Want To Be All I Can Be
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Want To Be All I Can Be"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">But Right Now Right Now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="But Right Now Right Now"/>
</div>
<div class="lyrico-lyrics-wrapper">I Just Want To Be Free
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Just Want To Be Free"/>
</div>
<div class="lyrico-lyrics-wrapper">I Want To Be All I Can Be
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Want To Be All I Can Be"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">I’m Just A Travelling Soldier
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I’m Just A Travelling Soldier"/>
</div>
<div class="lyrico-lyrics-wrapper">And I’ll Be All I Can Be
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="And I’ll Be All I Can Be"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">But Right Now Right Now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="But Right Now Right Now"/>
</div>
<div class="lyrico-lyrics-wrapper">I Just Want To Be Free
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Just Want To Be Free"/>
</div>
<div class="lyrico-lyrics-wrapper">I Want To Be All I Can Be
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Want To Be All I Can Be"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">But Right Now Right Now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="But Right Now Right Now"/>
</div>
<div class="lyrico-lyrics-wrapper">I Just Want To Be Free
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Just Want To Be Free"/>
</div>
<div class="lyrico-lyrics-wrapper">I Want To Be All I Can Be
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Want To Be All I Can Be"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Hey I Wanna Be
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Hey I Wanna Be"/>
</div>
<div class="lyrico-lyrics-wrapper">All I Can Be
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="All I Can Be"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Hey I Wanna Be
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Hey I Wanna Be"/>
</div>
<div class="lyrico-lyrics-wrapper">All I Can Be
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="All I Can Be"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Hey I Wanna Be
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Hey I Wanna Be"/>
</div>
<div class="lyrico-lyrics-wrapper">All I Can Be
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="All I Can Be"/>
</div>
</pre>
