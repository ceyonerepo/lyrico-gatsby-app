---
title: "aakashamantha song lyrics"
album: "Prema Katha Chitram 2"
artist: "S.B. Uddhav"
lyricist: "Purnachari"
director: "Hari Kishan"
path: "/albums/prema-katha-chitram-2-lyrics"
song: "Aakashamantha"
image: ../../images/albumart/prema-katha-chitram-2.jpg
date: 2019-04-06
lang: telugu
youtubeLink: "https://www.youtube.com/embed/6QOgrzo1EBw"
type: "love"
singers:
  - Hymath
  - Ramya Behara
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aakashamantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aakashamantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kothagunnadhantaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kothagunnadhantaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Inkedhi Chudananthagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inkedhi Chudananthagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Marindhi Antha Poorthiga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marindhi Antha Poorthiga"/>
</div>
<div class="lyrico-lyrics-wrapper">Oho Naa Gundekintha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oho Naa Gundekintha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vegamendhukanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vegamendhukanta"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanedho Chusinamdhukaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanedho Chusinamdhukaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Marinthagundi Vinthaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marinthagundi Vinthaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Galike Rekkalochinattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Galike Rekkalochinattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Eguruthundi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eguruthundi"/>
</div>
<div class="lyrico-lyrics-wrapper">Poolakemo Rangulevaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poolakemo Rangulevaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Poosinattu Undi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poosinattu Undi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninna Leni Andhamantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninna Leni Andhamantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vachi Vaalene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vachi Vaalene"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhemi Thondaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhemi Thondaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ni Kalla Mundhara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ni Kalla Mundhara"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Gundekintha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Gundekintha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vegamendhukanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vegamendhukanta"/>
</div>
<div class="lyrico-lyrics-wrapper">Inkedi Choodananthaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inkedi Choodananthaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Marindhi Antha Poorthiga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marindhi Antha Poorthiga"/>
</div>
<div class="lyrico-lyrics-wrapper">Premalo Prathi Kshanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Premalo Prathi Kshanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Emito Ayomayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emito Ayomayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivalaa Unna Nenilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivalaa Unna Nenilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnalle Lene Lenuga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnalle Lene Lenuga"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhuko Padhe Padhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhuko Padhe Padhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Oohalo Ade Pani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oohalo Ade Pani"/>
</div>
<div class="lyrico-lyrics-wrapper">Na Chupu Thotalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na Chupu Thotalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvveee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvveee"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalalni Jalli Mellega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalalni Jalli Mellega"/>
</div>
<div class="lyrico-lyrics-wrapper">Andame Andamedhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andame Andamedhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Antu Aduguthunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antu Aduguthunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannilaa Teesukochi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannilaa Teesukochi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu Poguduthunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu Poguduthunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Chandamama Andhamantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chandamama Andhamantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Neelo Chudaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelo Chudaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhemi Thondaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhemi Thondaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ni Kalla Mundhara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ni Kalla Mundhara"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Gundekintha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Gundekintha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vegamendhukanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vegamendhukanta"/>
</div>
<div class="lyrico-lyrics-wrapper">Inkedi Choodananthaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inkedi Choodananthaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Marindhi Antha Poorthiga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marindhi Antha Poorthiga"/>
</div>
<div class="lyrico-lyrics-wrapper">Oopire Hatatthuga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oopire Hatatthuga"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaginaa Anettugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaginaa Anettugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ni Dyasalo Unnadhii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ni Dyasalo Unnadhii"/>
</div>
<div class="lyrico-lyrics-wrapper">Ni Vaipu Thosthu Unnadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ni Vaipu Thosthu Unnadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Chaatuga Vunnavani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chaatuga Vunnavani"/>
</div>
<div class="lyrico-lyrics-wrapper">Matale Cheppavani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Matale Cheppavani"/>
</div>
<div class="lyrico-lyrics-wrapper">E Maaya Neede Ani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="E Maaya Neede Ani"/>
</div>
<div class="lyrico-lyrics-wrapper">Em Maya Chesaavani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Em Maya Chesaavani"/>
</div>
<div class="lyrico-lyrics-wrapper">Chukkale Ninnu Chuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chukkale Ninnu Chuda"/>
</div>
<div class="lyrico-lyrics-wrapper">Nela Jarinattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nela Jarinattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinukale Nannu Thaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinukale Nannu Thaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Muddu Petti Nattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muddu Petti Nattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinni Chinni Aashalenni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinni Chinni Aashalenni"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasu Daatenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu Daatenu"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhemi Thondaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhemi Thondaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ni Kalla Mundhara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ni Kalla Mundhara"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Gundekintha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Gundekintha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vegamendhukanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vegamendhukanta"/>
</div>
<div class="lyrico-lyrics-wrapper">Inkedi Choodananthaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inkedi Choodananthaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Marindhi Antha Poorthiga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marindhi Antha Poorthiga"/>
</div>
</pre>
