---
title: "o variya song lyrics"
album: "Iruttu"
artist: "Girishh G."
lyricist: "Mohanraja"
director: "V. Z. Durai"
path: "/albums/iruttu-lyrics"
song: "O Variya"
image: ../../images/albumart/iruttu.jpg
date: 2019-12-06
lang: tamil
youtubeLink: "https://www.youtube.com/embed/hFwwWZNxYDU"
type: "love"
singers:
  - Syan
  - Anjana Rajagopalan
  - Shilpa Natarajan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">O Variyaa O Variyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Variyaa O Variyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Thiriyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Thiriyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">En Adharangal Thee Poriyaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Adharangal Thee Poriyaaaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Karuvizhi Theda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Karuvizhi Theda"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Karuvizhi Theda Soodera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Karuvizhi Theda Soodera"/>
</div>
<div class="lyrico-lyrics-wrapper">Viral Neeludhu Pennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viral Neeludhu Pennae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Idayai Thazhuvi Kodiyaai Padara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Idayai Thazhuvi Kodiyaai Padara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Valaivugal Aada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Valaivugal Aada"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Valaivugal Aada Thadumaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Valaivugal Aada Thadumaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir Yengudhu Kannae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Yengudhu Kannae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Idhazhai Parugi Sudaraai Eriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Idhazhai Parugi Sudaraai Eriya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">O Variyaa O Variyaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Variyaa O Variyaaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Thiriyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Thiriyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Adharangal Thee Poriyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Adharangal Thee Poriyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaayodu Vaai Vaithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaayodu Vaai Vaithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaai Moodu Nee Saithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaai Moodu Nee Saithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Manjathil Machathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manjathil Machathai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Enni Paarkka Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Enni Paarkka Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Uchathin Veppathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uchathin Veppathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Naam Ottikondaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam Ottikondaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Peraasai Peyaadudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peraasai Peyaadudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Podhum Un Punnagai Podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhum Un Punnagai Podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mezhugena Vaa Urugida Vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mezhugena Vaa Urugida Vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nam Vervayin Thuliyadhu Kadalaena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam Vervayin Thuliyadhu Kadalaena"/>
</div>
<div class="lyrico-lyrics-wrapper">Thondrum Thondrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thondrum Thondrum"/>
</div>
<div class="lyrico-lyrics-wrapper">Yengum Nam Viralgalum Engum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yengum Nam Viralgalum Engum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pazhagida Manam Marubadi Yengum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pazhagida Manam Marubadi Yengum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nam Udaladhu Karayena Alayaena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam Udaladhu Karayena Alayaena"/>
</div>
<div class="lyrico-lyrics-wrapper">Modhum Modhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Modhum Modhum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">O Variyaa O Variyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Variyaa O Variyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Thiriyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Thiriyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">En Adharangal Thee Poriyaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Adharangal Thee Poriyaaaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Mutham Theeradhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Mutham Theeradhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjaadha Kenjaadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjaadha Kenjaadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Manjam Illayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Manjam Illayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Perinbam Thoongadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perinbam Thoongadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Anjaadha Minjaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anjaadha Minjaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Kaamam Illayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Kaamam Illayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnodu Serndhu Ondraga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnodu Serndhu Ondraga"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjaadha Kenjaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjaadha Kenjaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Anjaadha Minjaadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anjaadha Minjaadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nam Meththai Vetkam Kolla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam Meththai Vetkam Kolla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kooda Nam Iru Udal Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooda Nam Iru Udal Kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Nam Udayadhu Tholaivinil Theda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam Udayadhu Tholaivinil Theda"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannedhirae Kasadara Kaamam Aada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannedhirae Kasadara Kaamam Aada"/>
</div>
<div class="lyrico-lyrics-wrapper">Aada Vendaam Idhu Mudindhida Vendaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aada Vendaam Idhu Mudindhida Vendaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Iru Udalum Pirindhida Vendaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iru Udalum Pirindhida Vendaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Indreno Pudhidhaai Irukkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indreno Pudhidhaai Irukkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Inbam Pennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inbam Pennae"/>
</div>
</pre>
