---
title: "aasai song lyrics"
album: "Enna Solla Pogirai"
artist: "Vivek – Mervin"
lyricist: "Maathevan"
director: "A. Hariharan"
path: "/albums/enna-solla-pogirai-lyrics"
song: "Aasai"
image: ../../images/albumart/enna-solla-pogirai.jpg
date: 2022-01-13
lang: tamil
youtubeLink: "https://www.youtube.com/embed/r63ZrCcY9CA"
type: "happy"
singers:
  - Mervin Solomon
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aasai Nooraagi Pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai Nooraagi Pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadai Noolaagi Pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadai Noolaagi Pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanney Kannaadi Aagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanney Kannaadi Aagum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aasai Nooraagi Pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai Nooraagi Pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadai Noolaagi Pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadai Noolaagi Pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanney Kannaadi Aagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanney Kannaadi Aagum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theendal Un Saavi Endru Thedi Poga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theendal Un Saavi Endru Thedi Poga"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoondal Un Vaasam Endru Aadi Poga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoondal Un Vaasam Endru Aadi Poga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oor Kangal Romba Thollai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oor Kangal Romba Thollai"/>
</div>
<div class="lyrico-lyrics-wrapper">Endraaga Kattikondu Mutham Vaithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endraaga Kattikondu Mutham Vaithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ondraaga Kaadhalaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondraaga Kaadhalaaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen Yen Yen Thegam Mattum Paavam Ingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Yen Yen Thegam Mattum Paavam Ingu"/>
</div>
<div class="lyrico-lyrics-wrapper">Endraaga Sol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endraaga Sol"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aasai Nooraagi Poga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai Nooraagi Poga"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadai Noolaagi Poga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadai Noolaagi Poga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanney Kannaadi Aagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanney Kannaadi Aagum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nam Porvaikulle Mei Veppamaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam Porvaikulle Mei Veppamaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Nam Kaalai Velai Men Muththamaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam Kaalai Velai Men Muththamaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa Kaadhal Seivom Por Uththaamaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Kaadhal Seivom Por Uththaamaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Neengaama Iruppom Viruppamaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neengaama Iruppom Viruppamaaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eera Muththam Paayum Saththam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eera Muththam Paayum Saththam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oori Pogum Bodhai Endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oori Pogum Bodhai Endru"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaaga Pookkal Pookka Seiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaga Pookkal Pookka Seiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Latchamaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Latchamaaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aasaiyodu Nee Pathitha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasaiyodu Nee Pathitha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinsivapu Macham Ondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinsivapu Macham Ondru"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalai Kaalai Velai Endru Michamaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalai Kaalai Velai Endru Michamaaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennaalum Theeraa Kaadhaalaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaalum Theeraa Kaadhaalaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppodhum Nee Naan Pakkamaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppodhum Nee Naan Pakkamaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Endrendrum Neenga Kaatchiyaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endrendrum Neenga Kaatchiyaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellorum Ketkum Per Anbin Saatchiyaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellorum Ketkum Per Anbin Saatchiyaaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kattikkonda Muththam Vaithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattikkonda Muththam Vaithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ondraaga Kaadhalaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondraaga Kaadhalaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Yen Yen Thegam Mattum Paavam Ingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Yen Yen Thegam Mattum Paavam Ingu"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendaaga Sol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendaaga Sol"/>
</div>
</pre>
