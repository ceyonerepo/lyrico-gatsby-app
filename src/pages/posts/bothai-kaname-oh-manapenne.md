---
title: "bodhai kaname song lyrics"
album: "Oh Manapenne"
artist: "Vishal Chandrashekhar"
lyricist: "Vivek"
director: "Kaarthikk Sundar"
path: "/albums/oh-manapenne-lyrics"
song: "Bodhai Kaname"
image: ../../images/albumart/oh-manapenne.jpg
date: 2021-10-22
lang: tamil
youtubeLink: "https://www.youtube.com/embed/mfw2sMi6lrw"
type: "love"
singers:
  - Rockstar Anirudh Ravichander
  - Shashaa Tirupati
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Adho pon piraiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adho pon piraiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Udaindhidum nuraiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udaindhidum nuraiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhi en nodiyin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhi en nodiyin"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhippariyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhippariyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naalum karaiyodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalum karaiyodum"/>
</div>
<div class="lyrico-lyrics-wrapper">Alaiyodum uravaadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alaiyodum uravaadum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kilinjal pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kilinjal pol"/>
</div>
<div class="lyrico-lyrics-wrapper">En nenjam nilaiyindriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En nenjam nilaiyindriya"/>
</div>
<div class="lyrico-lyrics-wrapper">Ange tholai dhoorathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ange tholai dhoorathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Saaral mazhai kanden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saaral mazhai kanden"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan pakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan pakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">Varumbodhu sirai kambiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varumbodhu sirai kambiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thavarena paartha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thavarena paartha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kan nindru kalai seiyudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan nindru kalai seiyudhey"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharisena paartha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharisena paartha"/>
</div>
<div class="lyrico-lyrics-wrapper">Megangal kadal peiyudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megangal kadal peiyudhey"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangal kaaranam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangal kaaranam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedudhey"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai vandhu serudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai vandhu serudhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bodhai kaname kaname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bodhai kaname kaname"/>
</div>
<div class="lyrico-lyrics-wrapper">Podhaadhiru nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhaadhiru nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Bodhai kaname kaname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bodhai kaname kaname"/>
</div>
<div class="lyrico-lyrics-wrapper">Podhaadhiru nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhaadhiru nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Bodhai kaname kaname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bodhai kaname kaname"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhvaai iru nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvaai iru nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Bodhai kaname siragaagidu nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bodhai kaname siragaagidu nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nizame nizame neengadhiru nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nizame nizame neengadhiru nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Thenin dhiname dhiname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thenin dhiname dhiname"/>
</div>
<div class="lyrico-lyrics-wrapper">Thengaadhiru nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thengaadhiru nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalai inime
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalai inime"/>
</div>
<div class="lyrico-lyrics-wrapper">Analaai meley vizhundhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Analaai meley vizhundhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Bodhai kaname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bodhai kaname"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudaiyaai iru nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudaiyaai iru nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thodadha paadhaiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodadha paadhaiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai veesum aasaiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai veesum aasaiyo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Niraivadhu yen odaiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niraivadhu yen odaiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nigazhvadhu yaarin kadhaiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nigazhvadhu yaarin kadhaiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakkena neendo kilaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkena neendo kilaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuyil serudho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuyil serudho"/>
</div>
<div class="lyrico-lyrics-wrapper">Maragadha pon velaiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maragadha pon velaiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Manadhinil yaazhin mazhaiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manadhinil yaazhin mazhaiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhamai en kaalaiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhamai en kaalaiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanaavin oramaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanaavin oramaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Idadha kolamaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idadha kolamaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Maraithu vaiththa aasai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maraithu vaiththa aasai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai kaatudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai kaatudhey"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjodu aazhamaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjodu aazhamaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollamal neelamaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollamal neelamaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Suvaithirundha mounam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suvaithirundha mounam"/>
</div>
<div class="lyrico-lyrics-wrapper">Poi aaguthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poi aaguthey"/>
</div>
<div class="lyrico-lyrics-wrapper">Iru vari serndhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iru vari serndhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatrodu kuralaagudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatrodu kuralaagudhey"/>
</div>
<div class="lyrico-lyrics-wrapper">Iruvadhai thaandi edhuvo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruvadhai thaandi edhuvo"/>
</div>
<div class="lyrico-lyrics-wrapper">En viralaagudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En viralaagudhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna thoranam yerudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna thoranam yerudhey"/>
</div>
<div class="lyrico-lyrics-wrapper">Serum paalam polavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Serum paalam polavey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bodhai kaname kaname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bodhai kaname kaname"/>
</div>
<div class="lyrico-lyrics-wrapper">Podhaadhiru nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhaadhiru nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Bodhai kaname kaname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bodhai kaname kaname"/>
</div>
<div class="lyrico-lyrics-wrapper">Podhaadhiru nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhaadhiru nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Bodhai kaname kaname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bodhai kaname kaname"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhvaai iru nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvaai iru nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Bodhai kaname siragaagidu nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bodhai kaname siragaagidu nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nizame nizame neengadhiru nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nizame nizame neengadhiru nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Thenin dhiname dhiname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thenin dhiname dhiname"/>
</div>
<div class="lyrico-lyrics-wrapper">Thengaadhiru nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thengaadhiru nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalai inime
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalai inime"/>
</div>
<div class="lyrico-lyrics-wrapper">Analaai meley vizhundhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Analaai meley vizhundhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Bodhai kaname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bodhai kaname"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudaiyaai iru nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudaiyaai iru nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thodadha paadhaiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodadha paadhaiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai veesum aasaiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai veesum aasaiyo"/>
</div>
</pre>
