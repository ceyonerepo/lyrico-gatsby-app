---
title: "bhagavathi baba lyrics"
album: "Mookuthi Amman"
artist: "Girishh Gopalakrishnan"
lyricist: "Pa Vijay"
director: "RJ Balaji  & NJ Saravanan"
path: "/albums/mookuthi-amman-song-lyrics"
song: "Bhagavathi Baba"
image: ../../images/albumart/mookuthi-amman.jpg
date: 2020-11-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/0rxiJq2E2Wc"
type: "fun"
singers:
  - Anthony Daasan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Bhagavathi baba ho…
<input type="checkbox" class="lyrico-select-lyric-line" value="Bhagavathi baba ho…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Who is baba?
<input type="checkbox" class="lyrico-select-lyric-line" value="Who is baba?"/>
</div>
<div class="lyrico-lyrics-wrapper">Where is baba?
<input type="checkbox" class="lyrico-select-lyric-line" value="Where is baba?"/>
</div>
<div class="lyrico-lyrics-wrapper">What is baba?
<input type="checkbox" class="lyrico-select-lyric-line" value="What is baba?"/>
</div>
<div class="lyrico-lyrics-wrapper">When is baba?
<input type="checkbox" class="lyrico-select-lyric-line" value="When is baba?"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Tennis baba
<input type="checkbox" class="lyrico-select-lyric-line" value="Tennis baba"/>
</div>
<div class="lyrico-lyrics-wrapper">Football baba
<input type="checkbox" class="lyrico-select-lyric-line" value="Football baba"/>
</div>
<div class="lyrico-lyrics-wrapper">Cricket baba
<input type="checkbox" class="lyrico-select-lyric-line" value="Cricket baba"/>
</div>
<div class="lyrico-lyrics-wrapper">Goli baba
<input type="checkbox" class="lyrico-select-lyric-line" value="Goli baba"/>
</div>
<div class="lyrico-lyrics-wrapper">Jolly baba
<input type="checkbox" class="lyrico-select-lyric-line" value="Jolly baba"/>
</div>
<div class="lyrico-lyrics-wrapper">Julie baba
<input type="checkbox" class="lyrico-select-lyric-line" value="Julie baba"/>
</div>
<div class="lyrico-lyrics-wrapper">Jolly baba
<input type="checkbox" class="lyrico-select-lyric-line" value="Jolly baba"/>
</div>
<div class="lyrico-lyrics-wrapper">Holly baba
<input type="checkbox" class="lyrico-select-lyric-line" value="Holly baba"/>
</div>
<div class="lyrico-lyrics-wrapper">2<div class="lyrico-lyrics-wrapper">D baba</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="D baba"/>
</div>
<div class="lyrico-lyrics-wrapper">3<div class="lyrico-lyrics-wrapper">D baba</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="D baba"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoodie baba
<input type="checkbox" class="lyrico-select-lyric-line" value="Hoodie baba"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhagavathi baba
<input type="checkbox" class="lyrico-select-lyric-line" value="Bhagavathi baba"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Bhakthaasae vango
<input type="checkbox" class="lyrico-select-lyric-line" value="Bhakthaasae vango"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhakthi-ah vango
<input type="checkbox" class="lyrico-select-lyric-line" value="Bhakthi-ah vango"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhakthaasae vango
<input type="checkbox" class="lyrico-select-lyric-line" value="Bhakthaasae vango"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhakthi-ah vango
<input type="checkbox" class="lyrico-select-lyric-line" value="Bhakthi-ah vango"/>
</div>
<div class="lyrico-lyrics-wrapper">Mookthiya moonu kilo vangikungo
<input type="checkbox" class="lyrico-select-lyric-line" value="Mookthiya moonu kilo vangikungo"/>
</div>
<div class="lyrico-lyrics-wrapper">Undiyal irukkum yogavum nadakkum
<input type="checkbox" class="lyrico-select-lyric-line" value="Undiyal irukkum yogavum nadakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Think-u pannama sink aagungo
<input type="checkbox" class="lyrico-select-lyric-line" value="Think-u pannama sink aagungo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Gammunu vangamma dhiyanamthikku
<input type="checkbox" class="lyrico-select-lyric-line" value="Gammunu vangamma dhiyanamthikku"/>
</div>
<div class="lyrico-lyrics-wrapper">Chumma jammunu aagum paar unga look-u
<input type="checkbox" class="lyrico-select-lyric-line" value="Chumma jammunu aagum paar unga look-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada vinyaanam menyaanam annyaanam
<input type="checkbox" class="lyrico-select-lyric-line" value="Ada vinyaanam menyaanam annyaanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Gnani yamma
<input type="checkbox" class="lyrico-select-lyric-line" value="Gnani yamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Bhagavathi baba
<input type="checkbox" class="lyrico-select-lyric-line" value="Bhagavathi baba"/>
</div>
  <div class="lyrico-lyrics-wrapper">Corporate-u   Baba
<input type="checkbox" class="lyrico-select-lyric-line" value="Corporate-u   Baba"/>
</div>
  <div class="lyrico-lyrics-wrapper">Comrade   Baba
<input type="checkbox" class="lyrico-select-lyric-line" value="Comrade   Baba"/>
</div>
  <div class="lyrico-lyrics-wrapper">Kaal maathu unnodu vidathu
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaal maathu unnodu vidathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Baba   Samsari
<input type="checkbox" class="lyrico-select-lyric-line" value="Baba   Samsari"/>
</div>
  <div class="lyrico-lyrics-wrapper">Baba   Singari
<input type="checkbox" class="lyrico-select-lyric-line" value="Baba   Singari"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakku velai da pa
<input type="checkbox" class="lyrico-select-lyric-line" value="Unakku velai da pa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Bhagavathi baba
<input type="checkbox" class="lyrico-select-lyric-line" value="Bhagavathi baba"/>
</div>
  <div class="lyrico-lyrics-wrapper">Full mass-u   Baba
<input type="checkbox" class="lyrico-select-lyric-line" value="Full mass-u   Baba"/>
</div>
  <div class="lyrico-lyrics-wrapper">Full kaasu   Baba
<input type="checkbox" class="lyrico-select-lyric-line" value="Full kaasu   Baba"/>
</div>
  <div class="lyrico-lyrics-wrapper">Rolls Royce-ula vantharu namakkaga
<input type="checkbox" class="lyrico-select-lyric-line" value="Rolls Royce-ula vantharu namakkaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Baba   Sundrayan
<input type="checkbox" class="lyrico-select-lyric-line" value="Baba   Sundrayan"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandhosa sandai seiya vaa paa
<input type="checkbox" class="lyrico-select-lyric-line" value="Sandhosa sandai seiya vaa paa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Bhagavathi baba baba baba
<input type="checkbox" class="lyrico-select-lyric-line" value="Bhagavathi baba baba baba"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhagavathi baba bhagavathi baba
<input type="checkbox" class="lyrico-select-lyric-line" value="Bhagavathi baba bhagavathi baba"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhagavathi baba baba baba
<input type="checkbox" class="lyrico-select-lyric-line" value="Bhagavathi baba baba baba"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhagavathi baba baba ho (2 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Bhagavathi baba baba ho"></div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Pachaizhai mathiri
<input type="checkbox" class="lyrico-select-lyric-line" value="Pachaizhai mathiri"/>
</div>
<div class="lyrico-lyrics-wrapper">Echchilai thunbidum
<input type="checkbox" class="lyrico-select-lyric-line" value="Echchilai thunbidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paravasa nizhaiyai padaithavanae
<input type="checkbox" class="lyrico-select-lyric-line" value="Paravasa nizhaiyai padaithavanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Verum kallai kooda
<input type="checkbox" class="lyrico-select-lyric-line" value="Verum kallai kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Vairam kal aakki
<input type="checkbox" class="lyrico-select-lyric-line" value="Vairam kal aakki"/>
</div>
<div class="lyrico-lyrics-wrapper">Maayangal seitha magic man-eh
<input type="checkbox" class="lyrico-select-lyric-line" value="Maayangal seitha magic man-eh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Raavanan leelai-ku paththu thalai
<input type="checkbox" class="lyrico-select-lyric-line" value="Raavanan leelai-ku paththu thalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini manmadha leelaiyil ettu thalai
<input type="checkbox" class="lyrico-select-lyric-line" value="Ini manmadha leelaiyil ettu thalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethurthu nirppathu vethu velai
<input type="checkbox" class="lyrico-select-lyric-line" value="Ethurthu nirppathu vethu velai"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan kaala kazhivittu summa po lae
<input type="checkbox" class="lyrico-select-lyric-line" value="Avan kaala kazhivittu summa po lae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Appointment-u vangikittu
<input type="checkbox" class="lyrico-select-lyric-line" value="Appointment-u vangikittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Appaviya nippavarae
<input type="checkbox" class="lyrico-select-lyric-line" value="Appaviya nippavarae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ammaanu perai solli
<input type="checkbox" class="lyrico-select-lyric-line" value="Ammaanu perai solli"/>
</div>
<div class="lyrico-lyrics-wrapper">Ambaniya maathuvaru
<input type="checkbox" class="lyrico-select-lyric-line" value="Ambaniya maathuvaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Thonnaiyila sundal vechi
<input type="checkbox" class="lyrico-select-lyric-line" value="Thonnaiyila sundal vechi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanthalam andha kaalam
<input type="checkbox" class="lyrico-select-lyric-line" value="Thanthalam andha kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Uchaagam baanam thandhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Uchaagam baanam thandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Urula thaanae viduvaru
<input type="checkbox" class="lyrico-select-lyric-line" value="Urula thaanae viduvaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Happy ellorum ingae happy
<input type="checkbox" class="lyrico-select-lyric-line" value="Happy ellorum ingae happy"/>
</div>
<div class="lyrico-lyrics-wrapper">Saamy-ku carbon copy
<input type="checkbox" class="lyrico-select-lyric-line" value="Saamy-ku carbon copy"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnomae kaiya thookki
<input type="checkbox" class="lyrico-select-lyric-line" value="Ninnomae kaiya thookki"/>
</div>
<div class="lyrico-lyrics-wrapper">Jay jay jay bhagavathi paadi
<input type="checkbox" class="lyrico-select-lyric-line" value="Jay jay jay bhagavathi paadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Anandham koothu aadi
<input type="checkbox" class="lyrico-select-lyric-line" value="Anandham koothu aadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Yellow-vil saree soodi
<input type="checkbox" class="lyrico-select-lyric-line" value="Yellow-vil saree soodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Mochchamtha onna thedi
<input type="checkbox" class="lyrico-select-lyric-line" value="Mochchamtha onna thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Baba-vae enga daddy
<input type="checkbox" class="lyrico-select-lyric-line" value="Baba-vae enga daddy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Bhagavathi baba
<input type="checkbox" class="lyrico-select-lyric-line" value="Bhagavathi baba"/>
</div>
  <div class="lyrico-lyrics-wrapper">Corporate-u   Baba
<input type="checkbox" class="lyrico-select-lyric-line" value="Corporate-u   Baba"/>
</div>
  <div class="lyrico-lyrics-wrapper">Comrade   Baba
<input type="checkbox" class="lyrico-select-lyric-line" value="Comrade   Baba"/>
</div>
  <div class="lyrico-lyrics-wrapper">Kaal maathu unnodu vidathu
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaal maathu unnodu vidathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Baba   Samsari
<input type="checkbox" class="lyrico-select-lyric-line" value="Baba   Samsari"/>
</div>
  <div class="lyrico-lyrics-wrapper">Baba   Singari
<input type="checkbox" class="lyrico-select-lyric-line" value="Baba   Singari"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakku velai da pa
<input type="checkbox" class="lyrico-select-lyric-line" value="Unakku velai da pa"/>
</div>
</pre>
