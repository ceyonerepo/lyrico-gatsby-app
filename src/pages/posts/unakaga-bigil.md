---
title: 'unakaga song lyrics'
album: 'Bigil'
artist: 'A R Rahman'
lyricist: 'Vivek'
director: 'Atlee'
path: '/albums/bigil-song-lyrics'
song: 'Unakaga'
image: ../../images/albumart/bigil.jpg
date: 2019-10-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/gYZjp0RNewc"
type: 'duet'
singers: 
- Sreekanth Hariharan
- Madhura Dhara Talluri
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">

  <div class="lyrico-lyrics-wrapper">Unakaaga vaazha nenaikkiren
<input type="checkbox" class="lyrico-select-lyric-line" value="Unakaaga vaazha nenaikkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Usuroda vaasam pudikkiren
<input type="checkbox" class="lyrico-select-lyric-line" value="Usuroda vaasam pudikkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Podava madikkaiyil
<input type="checkbox" class="lyrico-select-lyric-line" value="Podava madikkaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnathaan madikkiren
<input type="checkbox" class="lyrico-select-lyric-line" value="Unnathaan madikkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru nooru varusham
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru nooru varusham"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesa nenachi
<input type="checkbox" class="lyrico-select-lyric-line" value="Pesa nenachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholil thoongiduven
<input type="checkbox" class="lyrico-select-lyric-line" value="Tholil thoongiduven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Unakaagaa…unakaagaa…
<input type="checkbox" class="lyrico-select-lyric-line" value="Unakaagaa…unakaagaa…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Unakaaga vaazha nenaikkiren
<input type="checkbox" class="lyrico-select-lyric-line" value="Unakaaga vaazha nenaikkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Usuroda vaasam pudikkiren
<input type="checkbox" class="lyrico-select-lyric-line" value="Usuroda vaasam pudikkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ahhhhh……………………….
<input type="checkbox" class="lyrico-select-lyric-line" value="Ahhhhh………………………."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Yesa ketta needhaano..ooo ooo
<input type="checkbox" class="lyrico-select-lyric-line" value="Yesa ketta needhaano..ooo ooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Neramellaam needhaanoo…ooo
<input type="checkbox" class="lyrico-select-lyric-line" value="Neramellaam needhaanoo…ooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinam nee thoongum vara dhaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhinam nee thoongum vara dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">En vaazhakaiyae vidinju un
<input type="checkbox" class="lyrico-select-lyric-line" value="En vaazhakaiyae vidinju un"/>
</div>
<div class="lyrico-lyrics-wrapper">Pecholi ketta dhaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Pecholi ketta dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Eduppen moochaiyae
<input type="checkbox" class="lyrico-select-lyric-line" value="Eduppen moochaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Unna sumakkura varamaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Unna sumakkura varamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mela nizhal vandhu vizhumaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Mela nizhal vandhu vizhumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolladhae kannin oramaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Kolladhae kannin oramaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Unakaaga vaazha nenaikkiren
<input type="checkbox" class="lyrico-select-lyric-line" value="Unakaaga vaazha nenaikkiren"/>
</div>
  <div class="lyrico-lyrics-wrapper">Unakaaga vaazha nenaikkiren
<input type="checkbox" class="lyrico-select-lyric-line" value="Unakaaga vaazha nenaikkiren"/>
</div>
  <div class="lyrico-lyrics-wrapper">Usuroda vaasam pudikkiren
<input type="checkbox" class="lyrico-select-lyric-line" value="Usuroda vaasam pudikkiren"/>
</div>
  <div class="lyrico-lyrics-wrapper">Usuroda vaasam pudikkiren
<input type="checkbox" class="lyrico-select-lyric-line" value="Usuroda vaasam pudikkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Orae mazha
<input type="checkbox" class="lyrico-select-lyric-line" value="Orae mazha"/>
</div>
<div class="lyrico-lyrics-wrapper">Alli namma pothikkanum..hoo
<input type="checkbox" class="lyrico-select-lyric-line" value="Alli namma pothikkanum..hoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiya kudu kadhavaakki saaththikkanum
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaiya kudu kadhavaakki saaththikkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Orae kulir orae mutham ootikkanum
<input type="checkbox" class="lyrico-select-lyric-line" value="Orae kulir orae mutham ootikkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna mattum usuraaga paathukkanum
<input type="checkbox" class="lyrico-select-lyric-line" value="Unna mattum usuraaga paathukkanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Nila mazha mozhi ala
<input type="checkbox" class="lyrico-select-lyric-line" value="Nila mazha mozhi ala"/>
</div>
<div class="lyrico-lyrics-wrapper">Pani irul kili kela
<input type="checkbox" class="lyrico-select-lyric-line" value="Pani irul kili kela"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum naanum
<input type="checkbox" class="lyrico-select-lyric-line" value="Neeyum naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thegatta thegatta rasikkanum
<input type="checkbox" class="lyrico-select-lyric-line" value="Thegatta thegatta rasikkanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Unakaaga vaazha nenaikkiren
<input type="checkbox" class="lyrico-select-lyric-line" value="Unakaaga vaazha nenaikkiren"/>
</div>
  <div class="lyrico-lyrics-wrapper">Usuroda vaasam pudikkiren
<input type="checkbox" class="lyrico-select-lyric-line" value="Usuroda vaasam pudikkiren"/>
</div>
  <div class="lyrico-lyrics-wrapper">Usuroda vaasam pudikkiren
<input type="checkbox" class="lyrico-select-lyric-line" value="Usuroda vaasam pudikkiren"/>
</div>
  <div class="lyrico-lyrics-wrapper">Usuroda vaasam pudikkiren
<input type="checkbox" class="lyrico-select-lyric-line" value="Usuroda vaasam pudikkiren"/>
</div>
  <div class="lyrico-lyrics-wrapper">Unakaaga vaazha nenaikkiren
<input type="checkbox" class="lyrico-select-lyric-line" value="Unakaaga vaazha nenaikkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Podava madikkaiyil
<input type="checkbox" class="lyrico-select-lyric-line" value="Podava madikkaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnathaan madikkiren
<input type="checkbox" class="lyrico-select-lyric-line" value="Unnathaan madikkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru nooru varusham
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru nooru varusham"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesa nenachi
<input type="checkbox" class="lyrico-select-lyric-line" value="Pesa nenachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholil thoongiduven
<input type="checkbox" class="lyrico-select-lyric-line" value="Tholil thoongiduven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Unakaagaa…unakaagaa…
<input type="checkbox" class="lyrico-select-lyric-line" value="Unakaagaa…unakaagaa…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Unakaaga vaazha nenaikkiren
<input type="checkbox" class="lyrico-select-lyric-line" value="Unakaaga vaazha nenaikkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Usuroda vaasam pudikkiren
<input type="checkbox" class="lyrico-select-lyric-line" value="Usuroda vaasam pudikkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Usuroda vaasam pudikkiren
<input type="checkbox" class="lyrico-select-lyric-line" value="Usuroda vaasam pudikkiren"/>
</div>
</pre>