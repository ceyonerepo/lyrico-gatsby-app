---
title: "unnai keta song lyrics"
album: "Clap"
artist: "Ilaiyaraaja"
lyricist: "Uma Devi"
director: "Prithivi Adithya"
path: "/albums/clap-lyrics"
song: "Unnai Keta"
image: ../../images/albumart/clap.jpg
date: 2022-03-11
lang: tamil
youtubeLink: "https://www.youtube.com/embed/kmwlcAKuCv8"
type: "happy"
singers:
  - Karthik
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">unnai ketal ennai ketal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai ketal ennai ketal"/>
</div>
<div class="lyrico-lyrics-wrapper">thendral vanthu thaalaatum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thendral vanthu thaalaatum"/>
</div>
<div class="lyrico-lyrics-wrapper">mannai ketal malarai ketal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mannai ketal malarai ketal"/>
</div>
<div class="lyrico-lyrics-wrapper">megam vanthu neer ootrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="megam vanthu neer ootrum"/>
</div>
<div class="lyrico-lyrics-wrapper">irulai ootum vennila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irulai ootum vennila"/>
</div>
<div class="lyrico-lyrics-wrapper">ulagukaaga thondrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulagukaaga thondrum"/>
</div>
<div class="lyrico-lyrics-wrapper">unakagave oru oli nila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unakagave oru oli nila"/>
</div>
<div class="lyrico-lyrics-wrapper">endrum unnai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="endrum unnai "/>
</div>
<div class="lyrico-lyrics-wrapper">thodarnthu varuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thodarnthu varuthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">unnai ketal ennai ketal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai ketal ennai ketal"/>
</div>
<div class="lyrico-lyrics-wrapper">thendral vanthu thaalaatum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thendral vanthu thaalaatum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">yarukum vaalkai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yarukum vaalkai "/>
</div>
<div class="lyrico-lyrics-wrapper">ninaitha padi sellathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninaitha padi sellathu"/>
</div>
<div class="lyrico-lyrics-wrapper">nee ingu ninaithathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee ingu ninaithathum"/>
</div>
<div class="lyrico-lyrics-wrapper">nadanthathum veveru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nadanthathum veveru"/>
</div>
<div class="lyrico-lyrics-wrapper">unaku or aran pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unaku or aran pola"/>
</div>
<div class="lyrico-lyrics-wrapper">poomagal vanthale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poomagal vanthale"/>
</div>
<div class="lyrico-lyrics-wrapper">unnai nee nonthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai nee nonthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">thanaiye thanthale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanaiye thanthale"/>
</div>
<div class="lyrico-lyrics-wrapper">meendu vara vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meendu vara vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">endru vendi varam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="endru vendi varam "/>
</div>
<div class="lyrico-lyrics-wrapper">vaangi kondaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaangi kondaal"/>
</div>
<div class="lyrico-lyrics-wrapper">malargal kulungum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malargal kulungum"/>
</div>
<div class="lyrico-lyrics-wrapper">poongavil otrai malar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poongavil otrai malar"/>
</div>
<div class="lyrico-lyrics-wrapper">pola nindraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pola nindraal"/>
</div>
<div class="lyrico-lyrics-wrapper">koodiye vaalvathal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koodiye vaalvathal"/>
</div>
<div class="lyrico-lyrics-wrapper">thanimaiyil nil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanimaiyil nil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">unnai ketal ennai ketal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai ketal ennai ketal"/>
</div>
<div class="lyrico-lyrics-wrapper">thendral vanthu thaalaatum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thendral vanthu thaalaatum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">veenaikul naathangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veenaikul naathangal"/>
</div>
<div class="lyrico-lyrics-wrapper">aalkadal pol thoonguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aalkadal pol thoonguthe"/>
</div>
<div class="lyrico-lyrics-wrapper">alaigal pol karai thoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alaigal pol karai thoda"/>
</div>
<div class="lyrico-lyrics-wrapper">aasaiyodu athu yenguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aasaiyodu athu yenguthe"/>
</div>
<div class="lyrico-lyrics-wrapper">vithigal nadathum vaalkaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vithigal nadathum vaalkaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">unaku yeno vithi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unaku yeno vithi"/>
</div>
<div class="lyrico-lyrics-wrapper">kalainthu pogum kanavile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalainthu pogum kanavile"/>
</div>
<div class="lyrico-lyrics-wrapper">unaku yeno kanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unaku yeno kanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">sernthu iruntha pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sernthu iruntha pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">inge neyum thani nanum thaniye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inge neyum thani nanum thaniye"/>
</div>
<div class="lyrico-lyrics-wrapper">vaanavillin serntha vannam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaanavillin serntha vannam"/>
</div>
<div class="lyrico-lyrics-wrapper">kalayum nodi pogum than valiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalayum nodi pogum than valiye"/>
</div>
<div class="lyrico-lyrics-wrapper">raagama naan vegama 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="raagama naan vegama "/>
</div>
<div class="lyrico-lyrics-wrapper">neyum sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neyum sollu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">unnai ketal ennai ketal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai ketal ennai ketal"/>
</div>
<div class="lyrico-lyrics-wrapper">thendral vanthu thaalaatum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thendral vanthu thaalaatum"/>
</div>
<div class="lyrico-lyrics-wrapper">mannai ketal malarai ketal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mannai ketal malarai ketal"/>
</div>
<div class="lyrico-lyrics-wrapper">megam vanthu neer ootrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="megam vanthu neer ootrum"/>
</div>
<div class="lyrico-lyrics-wrapper">irulai ootum vennila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irulai ootum vennila"/>
</div>
<div class="lyrico-lyrics-wrapper">ulagukaaga thondrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulagukaaga thondrum"/>
</div>
<div class="lyrico-lyrics-wrapper">unakagave oru oli nila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unakagave oru oli nila"/>
</div>
<div class="lyrico-lyrics-wrapper">endrum unnai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="endrum unnai "/>
</div>
<div class="lyrico-lyrics-wrapper">thodarnthu varuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thodarnthu varuthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">unnai ketal ennai ketal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai ketal ennai ketal"/>
</div>
<div class="lyrico-lyrics-wrapper">thendral vanthu thaalaatum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thendral vanthu thaalaatum"/>
</div>
</pre>
