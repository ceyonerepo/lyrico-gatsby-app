---
title: "kalla kalavaani song lyrics"
album: "Chekka Chivantha Vaanam"
artist: "AR Rahman"
lyricist: "Vairamuthu - Lady Kash"
director: "Mani Ratnam"
path: "/albums/chekka-chivantha-vaanam-lyrics"
song: "Kalla Kalavaani"
image: ../../images/albumart/chekka-chivantha-vaanam.jpg
date: 2018-09-27
lang: tamil
youtubeLink: "https://www.youtube.com/embed/YMuDWjxCQ9I"
type: "happy"
singers:
  - Shakthisree Gopalan
  - Lady Kash
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nee vanthu sendranai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee vanthu sendranai"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai kandu sendranai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai kandu sendranai"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir vendru sendranai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir vendru sendranai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee nee nee nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee nee nee nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalavaani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalavaani"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee nee nee nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee nee nee nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalavaani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalavaani"/>
</div>
<div class="lyrico-lyrics-wrapper">Nana na na na na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nana na na na na"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee nee nee nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee nee nee nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalavaani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalavaani"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee nee nee nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee nee nee nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalavaani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalavaani"/>
</div>
<div class="lyrico-lyrics-wrapper">Nana na na na na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nana na na na na"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee vanthu sendranai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee vanthu sendranai"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai kandu sendranai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai kandu sendranai"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir vendru sendranai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir vendru sendranai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhagiya maara vazhividu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagiya maara vazhividu"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagiya maara adithodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagiya maara adithodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagiya maara vazhipadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagiya maara vazhipadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagiya maara madithodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagiya maara madithodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeah yaa yeah yaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeah yaa yeah yaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalla kalavaani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalla kalavaani"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalla kalavaani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalla kalavaani"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey kalla kalavaani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey kalla kalavaani"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalla kalavaani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalla kalavaani"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalla kalavaani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalla kalavaani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thirantha kannilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirantha kannilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Imaiyai thirudum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imaiyai thirudum"/>
</div>
<div class="lyrico-lyrics-wrapper">Chella kalavaani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chella kalavaani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalla kalavaani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalla kalavaani"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalla kalavaani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalla kalavaani"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalla kalavaani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalla kalavaani"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalla kalla kalavaani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalla kalla kalavaani"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalla kalavaani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalla kalavaani"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalava kalavaani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalava kalavaani"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey kalla kalla kalla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey kalla kalla kalla"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalla kalla kalavaani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalla kalla kalavaani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee vanthu sendranai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee vanthu sendranai"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai kandu sendranai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai kandu sendranai"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir vendru sendranai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir vendru sendranai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhagiya maara vazhividu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagiya maara vazhividu"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagiya maara adithodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagiya maara adithodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagiya maara vazhipadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagiya maara vazhipadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagiya maara madithodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagiya maara madithodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeah yaa yeah yaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeah yaa yeah yaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sutri sutri varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sutri sutri varum"/>
</div>
<div class="lyrico-lyrics-wrapper">Soorakaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soorakaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Verum paththu viralukidaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verum paththu viralukidaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Sikkathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sikkathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nezhinjodi varum karunjaarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nezhinjodi varum karunjaarai"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu nezhivu suzhivu enna ariyaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu nezhivu suzhivu enna ariyaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Pidi pidi pidiyena thorathura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pidi pidi pidiyena thorathura"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan vidu vidu viduvena parakkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan vidu vidu viduvena parakkuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Iru iru iruvena irukkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iru iru iruvena irukkuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan vazhu vazhu vazhuvena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan vazhu vazhu vazhuvena"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhukkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhukkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee vanthu sendranai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee vanthu sendranai"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai kandu sendranai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai kandu sendranai"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir vendru sendranai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir vendru sendranai"/>
</div>
</pre>
