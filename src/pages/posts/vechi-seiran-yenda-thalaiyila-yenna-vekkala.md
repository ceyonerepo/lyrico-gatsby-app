---
title: "vechi seiran song lyrics"
album: "Yenda Thalaiyila Yenna Vekkala"
artist: "A.R. Reihana"
lyricist: "S. Madhan"
director: "Vignesh Karthick"
path: "/albums/yenda-thalaiyila-yenna-vekkala-song-lyrics"
song: "Vechi Seiran"
image: ../../images/albumart/yenda-thalaiyila-yenna-vekkala.jpg
date: 2018-02-23
lang: tamil
youtubeLink: "https://www.youtube.com/embed/mf9X-Em_Czc"
type: "melody"
singers:
  - A.R. Reihana
  - Mukesh
  - Deepika Thyagarajan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">vachu seiraan vachu seiraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vachu seiraan vachu seiraan"/>
</div>
<div class="lyrico-lyrics-wrapper">vachu vachu seiraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vachu vachu seiraan"/>
</div>
<div class="lyrico-lyrics-wrapper">en uchi mela yenna vachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en uchi mela yenna vachu"/>
</div>
<div class="lyrico-lyrics-wrapper">pichu pichu seiraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pichu pichu seiraan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vachu seiraan vachu seiraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vachu seiraan vachu seiraan"/>
</div>
<div class="lyrico-lyrics-wrapper">vachu vachu seiraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vachu vachu seiraan"/>
</div>
<div class="lyrico-lyrics-wrapper">en uchi mela yenna vachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en uchi mela yenna vachu"/>
</div>
<div class="lyrico-lyrics-wrapper">pichu pichu seiraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pichu pichu seiraan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">tarchar pandran 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tarchar pandran "/>
</div>
<div class="lyrico-lyrics-wrapper">swetcher katran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="swetcher katran"/>
</div>
<div class="lyrico-lyrics-wrapper">kuniya vachu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuniya vachu "/>
</div>
<div class="lyrico-lyrics-wrapper">back la mutran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="back la mutran"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">panda vaarthaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="panda vaarthaya"/>
</div>
<div class="lyrico-lyrics-wrapper">kaluvi kaluvi oothuran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaluvi kaluvi oothuran"/>
</div>
<div class="lyrico-lyrics-wrapper">konda pota kilavi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konda pota kilavi"/>
</div>
<div class="lyrico-lyrics-wrapper">kaathu savva kilikiran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaathu savva kilikiran"/>
</div>
<div class="lyrico-lyrics-wrapper">salt potu sapta sora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="salt potu sapta sora"/>
</div>
<div class="lyrico-lyrics-wrapper">satti paanai pola 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="satti paanai pola "/>
</div>
<div class="lyrico-lyrics-wrapper">pottu udaikiran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pottu udaikiran"/>
</div>
<div class="lyrico-lyrics-wrapper">palaguran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="palaguran"/>
</div>
<div class="lyrico-lyrics-wrapper">paste pota police pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paste pota police pola"/>
</div>
<div class="lyrico-lyrics-wrapper">latiya eduthu kathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="latiya eduthu kathula"/>
</div>
<div class="lyrico-lyrics-wrapper">laadam katuran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="laadam katuran"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vachu seiraan vachu seiraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vachu seiraan vachu seiraan"/>
</div>
<div class="lyrico-lyrics-wrapper">vachu vachu seiraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vachu vachu seiraan"/>
</div>
<div class="lyrico-lyrics-wrapper">en uchi mela yenna vachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en uchi mela yenna vachu"/>
</div>
<div class="lyrico-lyrics-wrapper">pichu pichu seiraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pichu pichu seiraan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vachu seiraan vachu seiraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vachu seiraan vachu seiraan"/>
</div>
<div class="lyrico-lyrics-wrapper">vachu vachu seiraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vachu vachu seiraan"/>
</div>
<div class="lyrico-lyrics-wrapper">en uchi mela yenna vachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en uchi mela yenna vachu"/>
</div>
<div class="lyrico-lyrics-wrapper">pichu pichu seiraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pichu pichu seiraan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">yaav vu story onna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaav vu story onna"/>
</div>
<div class="lyrico-lyrics-wrapper">aathar story aakuran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aathar story aakuran"/>
</div>
<div class="lyrico-lyrics-wrapper">kaavu kuduthu enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaavu kuduthu enna"/>
</div>
<div class="lyrico-lyrics-wrapper">follow panni suthuran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="follow panni suthuran"/>
</div>
<div class="lyrico-lyrics-wrapper">manthiricha aata pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manthiricha aata pola"/>
</div>
<div class="lyrico-lyrics-wrapper">moonchi mela manja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moonchi mela manja"/>
</div>
<div class="lyrico-lyrics-wrapper">thani oothuran oothuran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thani oothuran oothuran"/>
</div>
<div class="lyrico-lyrics-wrapper">thalaiya light ah silipunaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalaiya light ah silipunaka"/>
</div>
<div class="lyrico-lyrics-wrapper">satti mela kumbi baagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="satti mela kumbi baagam"/>
</div>
<div class="lyrico-lyrics-wrapper">potruvaan potruvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="potruvaan potruvaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vachu seiraan vachu seiraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vachu seiraan vachu seiraan"/>
</div>
<div class="lyrico-lyrics-wrapper">vachu vachu seiraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vachu vachu seiraan"/>
</div>
<div class="lyrico-lyrics-wrapper">en uchi mela yenna vachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en uchi mela yenna vachu"/>
</div>
<div class="lyrico-lyrics-wrapper">pichu pichu seiraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pichu pichu seiraan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vachu seiraan vachu seiraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vachu seiraan vachu seiraan"/>
</div>
<div class="lyrico-lyrics-wrapper">vachu vachu seiraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vachu vachu seiraan"/>
</div>
<div class="lyrico-lyrics-wrapper">en uchi mela yenna vachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en uchi mela yenna vachu"/>
</div>
<div class="lyrico-lyrics-wrapper">pichu pichu seiraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pichu pichu seiraan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">saalna oothi enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saalna oothi enna"/>
</div>
<div class="lyrico-lyrics-wrapper">side dish aakitan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="side dish aakitan"/>
</div>
<div class="lyrico-lyrics-wrapper">oota kaalna vaaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oota kaalna vaaki"/>
</div>
<div class="lyrico-lyrics-wrapper">enna ooram katitaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna ooram katitaan"/>
</div>
<div class="lyrico-lyrics-wrapper">en kannura twist vaikiraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kannura twist vaikiraan"/>
</div>
<div class="lyrico-lyrics-wrapper">oil vachu thalikiren nu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oil vachu thalikiren nu"/>
</div>
<div class="lyrico-lyrics-wrapper">ayil regai alikiran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ayil regai alikiran"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vachu seiraan vachu seiraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vachu seiraan vachu seiraan"/>
</div>
<div class="lyrico-lyrics-wrapper">vachu vachu seiraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vachu vachu seiraan"/>
</div>
<div class="lyrico-lyrics-wrapper">en uchi mela yenna vachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en uchi mela yenna vachu"/>
</div>
<div class="lyrico-lyrics-wrapper">pichu pichu seiraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pichu pichu seiraan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vachu seiraan vachu seiraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vachu seiraan vachu seiraan"/>
</div>
<div class="lyrico-lyrics-wrapper">vachu vachu vachu seiraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vachu vachu vachu seiraan"/>
</div>
<div class="lyrico-lyrics-wrapper">en uchi mela yenna vachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en uchi mela yenna vachu"/>
</div>
<div class="lyrico-lyrics-wrapper">pichu pichu seiraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pichu pichu seiraan"/>
</div>
</pre>
