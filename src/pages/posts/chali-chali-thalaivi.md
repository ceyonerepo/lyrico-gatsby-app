---
title: "chali chali song lyrics"
album: "Thalaivi"
artist: "G.V. Prakash Kumar"
lyricist: "Irshad Kamil"
director: "Vijay"
path: "/albums/thalaivi-lyrics"
song: "Chali Chali"
image: ../../images/albumart/thalaivi.jpg
date: 2021-03-23
lang: hindi
youtubeLink: "https://www.youtube.com/embed/fbWkDQw8oL4"
type: "Enjoy"
singers:
  - Saindhavi Prakash
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Chali chali haan chali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chali chali haan chali"/>
</div>
<div class="lyrico-lyrics-wrapper">Ik nayi hawa hun main
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ik nayi hawa hun main"/>
</div>
<div class="lyrico-lyrics-wrapper">Kabhi nahi dekhi jo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabhi nahi dekhi jo"/>
</div>
<div class="lyrico-lyrics-wrapper">Woh nayi disha hun main
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Woh nayi disha hun main"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chali chali haan chali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chali chali haan chali"/>
</div>
<div class="lyrico-lyrics-wrapper">Ik nayi hawa hun main
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ik nayi hawa hun main"/>
</div>
<div class="lyrico-lyrics-wrapper">Kabhi nahi dekhi jo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabhi nahi dekhi jo"/>
</div>
<div class="lyrico-lyrics-wrapper">Woh nayi disha hun main
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Woh nayi disha hun main"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Khwab ke sathiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khwab ke sathiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Dil tujhe de diya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil tujhe de diya"/>
</div>
<div class="lyrico-lyrics-wrapper">Maine jo chaha woh hai kiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maine jo chaha woh hai kiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chali chali haan chali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chali chali haan chali"/>
</div>
<div class="lyrico-lyrics-wrapper">Ik nayi hawa hun main
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ik nayi hawa hun main"/>
</div>
<div class="lyrico-lyrics-wrapper">Kabhi nahi dekhi jo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabhi nahi dekhi jo"/>
</div>
<div class="lyrico-lyrics-wrapper">Woh nayi disha hun main
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Woh nayi disha hun main"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maine kabhi jo na kahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maine kabhi jo na kahi"/>
</div>
<div class="lyrico-lyrics-wrapper">Baatein woh hothon pe aane lagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baatein woh hothon pe aane lagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pichle dino se to meri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pichle dino se to meri"/>
</div>
<div class="lyrico-lyrics-wrapper">Khamosiyan dekho gaane lagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khamosiyan dekho gaane lagi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paanv se meri chalti zameen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paanv se meri chalti zameen"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeh main jo rukun toh ruke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh main jo rukun toh ruke"/>
</div>
<div class="lyrico-lyrics-wrapper">Nindon ke baadal hai khwaab lekar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nindon ke baadal hai khwaab lekar"/>
</div>
<div class="lyrico-lyrics-wrapper">Palkon pe meri jhuke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palkon pe meri jhuke"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Khwaab ke sathiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khwaab ke sathiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Dil tujhe de diya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil tujhe de diya"/>
</div>
<div class="lyrico-lyrics-wrapper">Maine jo chaha woh hai kiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maine jo chaha woh hai kiya"/>
</div>
</pre>
