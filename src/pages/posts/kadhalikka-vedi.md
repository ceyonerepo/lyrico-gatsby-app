---
title: "kadhalikka song lyrics"
album: "Vedi"
artist: "Vijay Antony"
lyricist: "kabilan"
director: "Prabhu Deva"
path: "/albums/vedi-lyrics"
song: "Kadhalikka"
image: ../../images/albumart/vedi.jpg
date: 2011-09-30
lang: tamil
youtubeLink: "https://www.youtube.com/embed/qxqILb1MMwU"
type: "happy"
singers:
  - Emcee Jezz
  - DSP
  - Andrea Jeremiah
  - Rap Vocals Sharmila
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aahaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aahaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Common Girls Its Time To Play
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Common Girls Its Time To Play"/>
</div>
<div class="lyrico-lyrics-wrapper">Lets Have Fun In My Own Way
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lets Have Fun In My Own Way"/>
</div>
<div class="lyrico-lyrics-wrapper">Put Your Hands Up In The Air And
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Put Your Hands Up In The Air And"/>
</div>
<div class="lyrico-lyrics-wrapper">Push Yourself Lets Go Somewhere
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Push Yourself Lets Go Somewhere"/>
</div>
<div class="lyrico-lyrics-wrapper">This Is For People Out There
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="This Is For People Out There"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pull Your Leg To Have Some Fun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pull Your Leg To Have Some Fun"/>
</div>
<div class="lyrico-lyrics-wrapper">Lets Laugh Out Loud And I’ll Be Done
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lets Laugh Out Loud And I’ll Be Done"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhalika Pennoruthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalika Pennoruthi"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarthuvitenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarthuvitenae"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kangalukul Un Mugathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kangalukul Un Mugathai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naathu Nattenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naathu Nattenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaru Indha Yaaru Indha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaru Indha Yaaru Indha"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaraavaara Poo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraavaara Poo"/>
</div>
<div class="lyrico-lyrics-wrapper">En Sattayin Mel Kuthi Vacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Sattayin Mel Kuthi Vacha"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattu Roja Poo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattu Roja Poo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manmadhanin Thaaimozhi Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manmadhanin Thaaimozhi Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Meesai Illaa Minmini Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meesai Illaa Minmini Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thithithidum Theekuchi Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thithithidum Theekuchi Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thendraluku Thangachi Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thendraluku Thangachi Naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhalika Pennoruthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalika Pennoruthi"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarthuvitenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarthuvitenae"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kangalukul Un Mugathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kangalukul Un Mugathai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naathu Nattenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naathu Nattenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaru Indha Yaaru Indha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaru Indha Yaaru Indha"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaraavaara Poo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraavaara Poo"/>
</div>
<div class="lyrico-lyrics-wrapper">En Sattayin Mel Kuthi Vacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Sattayin Mel Kuthi Vacha"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattu Roja Poo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattu Roja Poo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aahaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aahaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Common Girls Its Time To Play
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Common Girls Its Time To Play"/>
</div>
<div class="lyrico-lyrics-wrapper">Lets Have Fun In My Own Way
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lets Have Fun In My Own Way"/>
</div>
<div class="lyrico-lyrics-wrapper">Put Your Hands Up In The Air And
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Put Your Hands Up In The Air And"/>
</div>
<div class="lyrico-lyrics-wrapper">Push Yourself Lets Go Somewhere
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Push Yourself Lets Go Somewhere"/>
</div>
<div class="lyrico-lyrics-wrapper">This Is For People Out There
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="This Is For People Out There"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pull Your Leg To Have Some Fun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pull Your Leg To Have Some Fun"/>
</div>
<div class="lyrico-lyrics-wrapper">Lets Laugh Out Loud And I’ll Be Done
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lets Laugh Out Loud And I’ll Be Done"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Oru Vinmeenai Kandenadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Oru Vinmeenai Kandenadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagalilohoooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagalilohoooooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Ivan Kannukul Kaithattinaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Ivan Kannukul Kaithattinaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Iravilthighu Chig
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravilthighu Chig"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koondhal Veesi Thoondil Pottal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koondhal Veesi Thoondil Pottal"/>
</div>
<div class="lyrico-lyrics-wrapper">Meesai Yaavum Meenaai Maattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meesai Yaavum Meenaai Maattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paambai Pola Paarvai Paarthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paambai Pola Paarvai Paarthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanin Nenjai Kothaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanin Nenjai Kothaadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veenai Vegam Yaanai Thandham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veenai Vegam Yaanai Thandham"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethaan Endhan Aadhi Andham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethaan Endhan Aadhi Andham"/>
</div>
<div class="lyrico-lyrics-wrapper">Velli Parkkal Vaira Karkkal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velli Parkkal Vaira Karkkal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Mendru Thinnaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Mendru Thinnaadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhalika Pennoruthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalika Pennoruthi"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarthuvitenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarthuvitenae"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kangalukul Un Mugathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kangalukul Un Mugathai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naathu Nattenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naathu Nattenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaru Indha Yaaru Indha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaru Indha Yaaru Indha"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaraavaara Poo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraavaara Poo"/>
</div>
<div class="lyrico-lyrics-wrapper">En Sattayin Mel Kuthi Vacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Sattayin Mel Kuthi Vacha"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattu Roja Poo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattu Roja Poo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaar Ival Poo Pootha Boogambam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar Ival Poo Pootha Boogambam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pol Azhaga Ahaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pol Azhaga Ahaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Ival Veppathil Vilundhenada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Ival Veppathil Vilundhenada"/>
</div>
<div class="lyrico-lyrics-wrapper">Melugaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Melugaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thighu Chig Thighu Chig
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thighu Chig Thighu Chig"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pookal Ellam Ovvor Vannam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pookal Ellam Ovvor Vannam"/>
</div>
<div class="lyrico-lyrics-wrapper">Poovae Unnil 7-Lu Vannam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovae Unnil 7-Lu Vannam"/>
</div>
<div class="lyrico-lyrics-wrapper">Killi Paarka Kaigal Neelum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Killi Paarka Kaigal Neelum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalli Thalli Chellaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalli Thalli Chellaadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanam Vittu Boomi Vandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam Vittu Boomi Vandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Aedhen Thoota Angel Neeyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aedhen Thoota Angel Neeyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadhi Kannaal Paarthu Nenjai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadhi Kannaal Paarthu Nenjai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pathavachi Kollaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathavachi Kollaadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhalika Pennoruthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalika Pennoruthi"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarthuvitenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarthuvitenae"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kangalukul Un Mugathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kangalukul Un Mugathai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naathu Nattenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naathu Nattenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaru Indha Yaaru Indha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaru Indha Yaaru Indha"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaraavaara Poo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraavaara Poo"/>
</div>
<div class="lyrico-lyrics-wrapper">En Sattayin Mel Kuthi Vacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Sattayin Mel Kuthi Vacha"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattu Roja Poo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattu Roja Poo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manmadhanin Thaaimozhi Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manmadhanin Thaaimozhi Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Meesai Illaa Minmini Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meesai Illaa Minmini Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thithithidum Theekuchi Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thithithidum Theekuchi Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thendraluku Thangachi Naan Aaann
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thendraluku Thangachi Naan Aaann"/>
</div>
</pre>
