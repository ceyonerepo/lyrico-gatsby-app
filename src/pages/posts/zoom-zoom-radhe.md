---
title: "dil nahin todna song lyrics"
album: "Sardar Ka Grandson"
artist: "Tanishk Bagchi"
lyricist: "Tanishk Bagchi"
director: "Kaashvie Nair"
path: "/albums/sardar-ka-grandson-lyrics"
song: "Dil Nahin Todna"
image: ../../images/albumart/sardar-ka-grandson.jpg
date: 2021-05-18
lang: hindi
youtubeLink: "https://www.youtube.com/embed/iF4G4KKWu3c"
type: "Love"
singers:
  - Zara Khan
  - Tanishk Bagchi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Jis cheez se pyaar karo na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jis cheez se pyaar karo na"/>
</div>
<div class="lyrico-lyrics-wrapper">Usse yaadon mein sambhal ke rakho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usse yaadon mein sambhal ke rakho"/>
</div>
<div class="lyrico-lyrics-wrapper">Aur yaadon ko dil mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aur yaadon ko dil mein"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Judaiyan manzoor judaiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Judaiyan manzoor judaiyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Manzoor na mainu teri ruswaiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manzoor na mainu teri ruswaiyan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh sajna ankhiyan kehnde kya sun zara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh sajna ankhiyan kehnde kya sun zara"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh sajna mere dil mein rehna tu sada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh sajna mere dil mein rehna tu sada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bin tere main na jee pawaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bin tere main na jee pawaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Aise hi main to mar jawaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aise hi main to mar jawaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Roothe jo tu yaar to
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Roothe jo tu yaar to"/>
</div>
<div class="lyrico-lyrics-wrapper">Main bhi tutt jawaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main bhi tutt jawaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho dil nahin todna ve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho dil nahin todna ve"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho kadi nahin chhodna ve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho kadi nahin chhodna ve"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho maahi dil tod na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho maahi dil tod na"/>
</div>
<div class="lyrico-lyrics-wrapper">Tod na, tod na ve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tod na, tod na ve"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dil nahin todna ve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil nahin todna ve"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho kadi nahin chhodna ve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho kadi nahin chhodna ve"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho maahi dil tod na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho maahi dil tod na"/>
</div>
<div class="lyrico-lyrics-wrapper">Tod na, tod na ve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tod na, tod na ve"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dass de tu bas mainu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dass de tu bas mainu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ainna pyaar tu kyun ae karda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ainna pyaar tu kyun ae karda"/>
</div>
<div class="lyrico-lyrics-wrapper">Main na koyi hoor
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main na koyi hoor"/>
</div>
<div class="lyrico-lyrics-wrapper">Meri qismat te jag yeh hassda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meri qismat te jag yeh hassda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tu na jaane tu kya mera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu na jaane tu kya mera"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaan bhi tu hai jism bhi tera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaan bhi tu hai jism bhi tera"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu jo na dhadke to
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu jo na dhadke to"/>
</div>
<div class="lyrico-lyrics-wrapper">Main bhi ruk jawaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main bhi ruk jawaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho dil nahin todna ve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho dil nahin todna ve"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho kadi nahin chhodna ve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho kadi nahin chhodna ve"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho maahi dil tod na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho maahi dil tod na"/>
</div>
<div class="lyrico-lyrics-wrapper">Tod na, tod na ve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tod na, tod na ve"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dil nahin todna ve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil nahin todna ve"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho kadi nahin chhodna ve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho kadi nahin chhodna ve"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho maahi dil tod na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho maahi dil tod na"/>
</div>
<div class="lyrico-lyrics-wrapper">Tod na, tod na ve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tod na, tod na ve"/>
</div>
</pre>
