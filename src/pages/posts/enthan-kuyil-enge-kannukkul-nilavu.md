---
title: "enthan kuyil enge song lyrics"
album: "Kannukkul Nilavu"
artist: "Ilaiyaraaja"
lyricist: "Palani Bharathi"
director: "Fazil"
path: "/albums/kannukkul-nilavu-lyrics"
song: "Enthan Kuyil Enge"
image: ../../images/albumart/kannukkul-nilavu.jpg
date: 2000-01-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/so9DTyXAyyk"
type: "love"
singers:
  - P. Unni Krishnan
  - Anuradha Sriram
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Endhan Kuyilenge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan Kuyilenge"/>
</div>
<div class="lyrico-lyrics-wrapper">Endru Paarppen Endru Paarppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endru Paarppen Endru Paarppen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endhan Kuyilenge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan Kuyilenge"/>
</div>
<div class="lyrico-lyrics-wrapper">Endru Paarppen Endru Paarppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endru Paarppen Endru Paarppen"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjum Kuyilosai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjum Kuyilosai"/>
</div>
<div class="lyrico-lyrics-wrapper">Endru Ketpen Endru Ketpen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endru Ketpen Endru Ketpen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannilorr Oviyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannilorr Oviyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjilorr Nyaabagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjilorr Nyaabagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Solvadhu Yaaridam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solvadhu Yaaridam"/>
</div>
<div class="lyrico-lyrics-wrapper">Purindhadhaa En Manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Purindhadhaa En Manam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennilaiyil Konjam Nindru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennilaiyil Konjam Nindru"/>
</div>
<div class="lyrico-lyrics-wrapper">Sol Sol Sol Kanmani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sol Sol Sol Kanmani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endhan Kuyilenge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan Kuyilenge"/>
</div>
<div class="lyrico-lyrics-wrapper">Endru Paarppen Endru Paarppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endru Paarppen Endru Paarppen"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjum Kuyilosai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjum Kuyilosai"/>
</div>
<div class="lyrico-lyrics-wrapper">Endru Ketpen Endru Ketpen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endru Ketpen Endru Ketpen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kavidhai Ondrai Naan Kandeduthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavidhai Ondrai Naan Kandeduthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Padiththidumun Adhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padiththidumun Adhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Puyal Kaatrinil Parandhadhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puyal Kaatrinil Parandhadhadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanavinilum En Ninaivinilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavinilum En Ninaivinilum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavidhaikkural Dhinam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavidhaikkural Dhinam"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai Adikkadi Azhaikkudhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai Adikkadi Azhaikkudhadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arpudham Kaanaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arpudham Kaanaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Karpanai Yen Kondaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karpanai Yen Kondaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Arpudham Kaanaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arpudham Kaanaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Karpanai Yen Kondaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karpanai Yen Kondaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa Ezhudhalaam Ezhudhalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Ezhudhalaam Ezhudhalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhiya Kavidhaigalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhiya Kavidhaigalai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endhan Kuyilenge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan Kuyilenge"/>
</div>
<div class="lyrico-lyrics-wrapper">Indru Paarthen Endru Paarthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indru Paarthen Endru Paarthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjum Kuyilosai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjum Kuyilosai"/>
</div>
<div class="lyrico-lyrics-wrapper">Indru Ketten Endru Ketten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indru Ketten Endru Ketten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannilorr Oviyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannilorr Oviyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjilorr Nyaabagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjilorr Nyaabagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Solvadhu Yaaridam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solvadhu Yaaridam"/>
</div>
<div class="lyrico-lyrics-wrapper">Purindhadhaa En Manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Purindhadhaa En Manam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennilaiyil Konjam Nindru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennilaiyil Konjam Nindru"/>
</div>
<div class="lyrico-lyrics-wrapper">Sol Sol Sol Kanmani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sol Sol Sol Kanmani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endhan Kuyilenge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan Kuyilenge"/>
</div>
<div class="lyrico-lyrics-wrapper">Endru Paarppen Endru Paarppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endru Paarppen Endru Paarppen"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjum Kuyilosai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjum Kuyilosai"/>
</div>
<div class="lyrico-lyrics-wrapper">Endru Ketpen Endru Ketpen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endru Ketpen Endru Ketpen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Malar Malarum Konjam Ilai Udhirum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malar Malarum Konjam Ilai Udhirum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavalai Vidu Idhoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavalai Vidu Idhoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ppudhu Vasandhangal Varugiradhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ppudhu Vasandhangal Varugiradhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhagazhagaai Ini Poo Malarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagazhagaai Ini Poo Malarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Rasiththirundhaal Adhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rasiththirundhaal Adhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu Vaazhkkaiyin Vaasam Tharum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu Vaazhkkaiyin Vaasam Tharum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Kadhai Nilavariyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kadhai Nilavariyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Odidum Mugilariyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odidum Mugilariyum"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kadhai Nilavariyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kadhai Nilavariyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Odidum Mugilariyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odidum Mugilariyum"/>
</div>
<div class="lyrico-lyrics-wrapper">En Vaasalin Thendrale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Vaasalin Thendrale"/>
</div>
<div class="lyrico-lyrics-wrapper">Manadhai Varudividu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manadhai Varudividu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endhan Kuyilenge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan Kuyilenge"/>
</div>
<div class="lyrico-lyrics-wrapper">Indru Paarthen Endru Paarthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indru Paarthen Endru Paarthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjum Kuyilosai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjum Kuyilosai"/>
</div>
<div class="lyrico-lyrics-wrapper">Indru Ketten Endru Ketten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indru Ketten Endru Ketten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannilorr Oviyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannilorr Oviyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjilorr Nyaabagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjilorr Nyaabagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Solvadhu Yaaridam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solvadhu Yaaridam"/>
</div>
<div class="lyrico-lyrics-wrapper">Purindhadhaa En Manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Purindhadhaa En Manam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennilaiyil Konjam Nindru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennilaiyil Konjam Nindru"/>
</div>
<div class="lyrico-lyrics-wrapper">Sol Sol Sol Kanmani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sol Sol Sol Kanmani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endhan Kuyilenge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan Kuyilenge"/>
</div>
<div class="lyrico-lyrics-wrapper">Endru Paarppen Endru Paarppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endru Paarppen Endru Paarppen"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjum Kuyilosai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjum Kuyilosai"/>
</div>
<div class="lyrico-lyrics-wrapper">Endru Ketpen Endru Ketpen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endru Ketpen Endru Ketpen"/>
</div>
</pre>
