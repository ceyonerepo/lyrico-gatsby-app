---
title: "megham tho megham song lyrics"
album: "IIT Krishnamurthy"
artist: "Naresh Kumaran"
lyricist: "Ramanjhaneyulu Sankarpu"
director: "Sree Vardhan"
path: "/albums/iit-krishnamurthy-lyrics"
song: "Megham Tho Megham"
image: ../../images/albumart/iit-krishnamurthy.jpg
date: 2020-12-10
lang: telugu
youtubeLink: "https://www.youtube.com/embed/ZWuEDOjfX5Q"
type: "love"
singers:
  - Yazin Nizar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Meghamtho Megham Murise
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meghamtho Megham Murise"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanalle Maare Varase
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanalle Maare Varase"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeramtho Kadalemo Kalise
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeramtho Kadalemo Kalise"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanandham Alalaa Egise
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanandham Alalaa Egise"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye Chota Vere Kaaleni Theere
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Chota Vere Kaaleni Theere"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Jathaga Nadiche Jagame Vidiche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Jathaga Nadiche Jagame Vidiche"/>
</div>
<div class="lyrico-lyrics-wrapper">Chuttooraa Evaru Unnaaro Kanaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chuttooraa Evaru Unnaaro Kanaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Untaare Epudu Evaru Lenattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Untaare Epudu Evaru Lenattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Are Arere Arere Arere
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Are Arere Arere Arere"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhuredhure Niliche Kanule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhuredhure Niliche Kanule"/>
</div>
<div class="lyrico-lyrics-wrapper">Are Arere Arere Arere
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Are Arere Arere Arere"/>
</div>
<div class="lyrico-lyrics-wrapper">Etuvaipo Egire Yadhale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Etuvaipo Egire Yadhale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paadham Aagindhi Emaindhani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadham Aagindhi Emaindhani"/>
</div>
<div class="lyrico-lyrics-wrapper">Gamyam Cherindho Emo Mari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gamyam Cherindho Emo Mari"/>
</div>
<div class="lyrico-lyrics-wrapper">Choosthu Choosthune Oo Maayala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choosthu Choosthune Oo Maayala"/>
</div>
<div class="lyrico-lyrics-wrapper">Choope Saagindhi Oo Needalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choope Saagindhi Oo Needalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanule Veluthunte Dhooramgaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanule Veluthunte Dhooramgaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalale Aagaayi Oo Chotane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalale Aagaayi Oo Chotane"/>
</div>
<div class="lyrico-lyrics-wrapper">Emo Emaindho Ante Mari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emo Emaindho Ante Mari"/>
</div>
<div class="lyrico-lyrics-wrapper">Badhulantu Ledhe Ee Prashnaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Badhulantu Ledhe Ee Prashnaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Are Arere Arere Arere
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Are Arere Arere Arere"/>
</div>
<div class="lyrico-lyrics-wrapper">Parichayame Ipude Modhalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parichayame Ipude Modhalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Are Arere Arere Arere
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Are Arere Arere Arere"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadavaraku Kadhile Kadhalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadavaraku Kadhile Kadhalaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anthe Lenantha Santoshame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anthe Lenantha Santoshame"/>
</div>
<div class="lyrico-lyrics-wrapper">Anthaa Ayipoye Nee Sonthame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anthaa Ayipoye Nee Sonthame"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaade Ledhante Ekanthame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaade Ledhante Ekanthame"/>
</div>
<div class="lyrico-lyrics-wrapper">Thodai Cheraaka Ee Bandhame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodai Cheraaka Ee Bandhame"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhale Verante Pai Pai Maate
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhale Verante Pai Pai Maate"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalise Ee Janta Shwaasinchane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalise Ee Janta Shwaasinchane"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhooram Raanandhi Nee Vaipuke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhooram Raanandhi Nee Vaipuke"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam Aagindhi Nee Raakake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam Aagindhi Nee Raakake"/>
</div>
<div class="lyrico-lyrics-wrapper">Are Arere Arere Arere
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Are Arere Arere Arere"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhurupade Vethike Nidhule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhurupade Vethike Nidhule"/>
</div>
<div class="lyrico-lyrics-wrapper">Are Arere Arere Arere
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Are Arere Arere Arere"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadali Ode Odhige Nadhule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadali Ode Odhige Nadhule"/>
</div>
</pre>
