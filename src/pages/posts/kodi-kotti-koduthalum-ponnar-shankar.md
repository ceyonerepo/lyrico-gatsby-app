---
title: "kodi kotti koduthalum song lyrics"
album: "Ponnar Shankar"
artist: "Ilaiyaraaja"
lyricist: "Vairamuthu"
director: "Thiagarajan"
path: "/albums/ponnar-shankar-lyrics"
song: "Kodi Kotti Koduthalum"
image: ../../images/albumart/ponnar-shankar.jpg
date: 2011-04-09
lang: tamil
youtubeLink: "https://www.youtube.com/embed/WSTYVe7vCCk"
type: "mass"
singers:
  - Haricharan
  - Aalap Raju
  - Sathyan Mahalingam
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kodi Kotti Koduthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodi Kotti Koduthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga Kongu Naattukkeededhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Kongu Naattukkeededhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadu Pattu Uzhaippadhile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadu Pattu Uzhaippadhile"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga Makkalukku Inai Yedhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Makkalukku Inai Yedhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Malai Perusaa Illai Nadhi Perusaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malai Perusaa Illai Nadhi Perusaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nelam Perusaa Alai Kadal Perusaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nelam Perusaa Alai Kadal Perusaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhai Vida Osandhadhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai Vida Osandhadhu "/>
</div>
<div class="lyrico-lyrics-wrapper">Nammoda Veeram Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nammoda Veeram Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hei Kodi Kotti Koduthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei Kodi Kotti Koduthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga Kongu Naattukkeededhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Kongu Naattukkeededhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadu Pattu Uzhaippadhile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadu Pattu Uzhaippadhile"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga Makkalukku Inai Yedhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Makkalukku Inai Yedhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pottiyinnu Vandhaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pottiyinnu Vandhaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Naam Thokkadippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam Thokkadippom"/>
</div>
<div class="lyrico-lyrics-wrapper">Thozhanendru Vandhaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozhanendru Vandhaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Naam Thol Koduppom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam Thol Koduppom"/>
</div>
<div class="lyrico-lyrics-wrapper">Kutram Endru Kandaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kutram Endru Kandaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Naam Veraruppom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam Veraruppom"/>
</div>
<div class="lyrico-lyrics-wrapper">Sutram Endru Vandhaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sutram Endru Vandhaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Naam Poo Koduppom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam Poo Koduppom"/>
</div>
<div class="lyrico-lyrics-wrapper">Annanin Vaakku Thaan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annanin Vaakku Thaan "/>
</div>
<div class="lyrico-lyrics-wrapper">Thambikkoru Vedhame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thambikkoru Vedhame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aasaanin Paasarai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasaanin Paasarai "/>
</div>
<div class="lyrico-lyrics-wrapper">Veerargal Naam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veerargal Naam"/>
</div>
<div class="lyrico-lyrics-wrapper">Engalin Rathatthil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engalin Rathatthil "/>
</div>
<div class="lyrico-lyrics-wrapper">Paasam Oori Irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paasam Oori Irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhaiyum Thaangidum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhaiyum Thaangidum "/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjam Irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjam Irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Bommalaattamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bommalaattamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiyil Noolu Asaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyil Noolu Asaiyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ponnar Sankaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponnar Sankaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiyil Vaalu Asaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyil Vaalu Asaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolai Veesi Paarthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolai Veesi Paarthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaalai Veesi Paarthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaalai Veesi Paarthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetri Maalai Engalukku Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetri Maalai Engalukku Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kodi Kotti Koduthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodi Kotti Koduthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga Kongu Naattukkeededhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Kongu Naattukkeededhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadu Pattu Uzhaippadhile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadu Pattu Uzhaippadhile"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga Makkalukku Inai Yedhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Makkalukku Inai Yedhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vella Panja Pole 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vella Panja Pole "/>
</div>
<div class="lyrico-lyrics-wrapper">Nam Nenjam Enbom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam Nenjam Enbom"/>
</div>
<div class="lyrico-lyrics-wrapper">Vettu Kathi Pole 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vettu Kathi Pole "/>
</div>
<div class="lyrico-lyrics-wrapper">Nam Kobam Enbom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam Kobam Enbom"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholin Mele Thundu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholin Mele Thundu "/>
</div>
<div class="lyrico-lyrics-wrapper">Thanmaanam Enbom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanmaanam Enbom"/>
</div>
<div class="lyrico-lyrics-wrapper">Aalukkaalu Anbil Naam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalukkaalu Anbil Naam "/>
</div>
<div class="lyrico-lyrics-wrapper">Uyarndhu Nippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyarndhu Nippom"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidhaiyai Pudhaithaal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidhaiyai Pudhaithaal "/>
</div>
<div class="lyrico-lyrics-wrapper">Mulai Vittu Ezhumbum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mulai Vittu Ezhumbum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilathai Kizhithu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilathai Kizhithu "/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjai Nimirthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjai Nimirthum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinaiyai Vidhaithaal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinaiyai Vidhaithaal "/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkenna Kidaikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkenna Kidaikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Viraindhu Unakkoru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viraindhu Unakkoru "/>
</div>
<div class="lyrico-lyrics-wrapper">Badhil Kodukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Badhil Kodukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhai Makkalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhai Makkalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaattum Ezhmai Virattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaattum Ezhmai Virattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kolaigal Illai Endru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolaigal Illai Endru "/>
</div>
<div class="lyrico-lyrics-wrapper">Tholai Uyarthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholai Uyarthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nambikkaiyil Thaan Undu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nambikkaiyil Thaan Undu "/>
</div>
<div class="lyrico-lyrics-wrapper">Nalla Nalla Vaazhvendru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla Nalla Vaazhvendru"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetri Ettu Thikkum Ettattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetri Ettu Thikkum Ettattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hei Kodi Kotti Koduthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei Kodi Kotti Koduthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga Kongu Naattukkeededhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Kongu Naattukkeededhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadu Pattu Uzhaippadhile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadu Pattu Uzhaippadhile"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga Makkalukku Inai Yedhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Makkalukku Inai Yedhu"/>
</div>
</pre>
