---
title: "dirty pondatti song lyrics"
album: "Kaatrin Mozhi"
artist: "AH Kaashif"
lyricist: "Madhan Karky"
director: "Radha Mohan"
path: "/albums/kaatrin-mozhi-lyrics"
song: "Dirty Pondatti"
image: ../../images/albumart/kaatrin-mozhi.jpg
date: 2018-11-16
lang: tamil
youtubeLink: "https://www.youtube.com/embed/uYKgtJI2n70"
type: "love"
singers:
  - 	Benny Dayal
  - Swagatha S Krishnan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ti dirty pondattiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ti dirty pondattiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ti dirty pondattiyae vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ti dirty pondattiyae vaa vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thee mootti enna kooptiyae baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thee mootti enna kooptiyae baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan vantha nee cold ah nikkatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan vantha nee cold ah nikkatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaa neeya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaa neeya"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha pechellaam keechuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha pechellaam keechuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaa neeya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaa neeya"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna ichchayil kaichina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna ichchayil kaichina"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaa neeya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaa neeya"/>
</div>
<div class="lyrico-lyrics-wrapper">Semma bodhaiya ethuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Semma bodhaiya ethuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaa neeya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaa neeya"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai yen di emathuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai yen di emathuna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh oh oh oh ohhh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh oh oh oh ohhh"/>
</div>
<div class="lyrico-lyrics-wrapper">Kettu pochu velai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kettu pochu velai"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh oh oh oh ohhh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh oh oh oh ohhh"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu unnaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu unnaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh oh oh oh ohhh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh oh oh oh ohhh"/>
</div>
<div class="lyrico-lyrics-wrapper">Patta pagalula moodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Patta pagalula moodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh oh oh oh ohhh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh oh oh oh ohhh"/>
</div>
<div class="lyrico-lyrics-wrapper">Aal illa veedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aal illa veedu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ti dirty pondattiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ti dirty pondattiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ti dirty pondattiyae vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ti dirty pondattiyae vaa vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thee mootti enna kooptiyae baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thee mootti enna kooptiyae baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan vantha nee cold ah nikkatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan vantha nee cold ah nikkatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saela kattuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saela kattuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Silaigal ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silaigal ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna paathu sirikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna paathu sirikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan vazhinju ninna kathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan vazhinju ninna kathai"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppadi naan uraikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppadi naan uraikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sillinu adikkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sillinu adikkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Kulirula thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulirula thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Verthu naanum kedakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verthu naanum kedakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kuralu kaathula paanju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kuralu kaathula paanju"/>
</div>
<div class="lyrico-lyrics-wrapper">En manasa kedukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En manasa kedukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey silky-ya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey silky-ya"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey milky-ya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey milky-ya"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee pesuna pechu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee pesuna pechu"/>
</div>
<div class="lyrico-lyrics-wrapper">Engengaiyo ennennavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engengaiyo ennennavo"/>
</div>
<div class="lyrico-lyrics-wrapper">Edakudam aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edakudam aachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey sexy-ya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey sexy-ya"/>
</div>
<div class="lyrico-lyrics-wrapper">Eh chick-siya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh chick-siya"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee koopitta pinnae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee koopitta pinnae"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey vekkama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey vekkama"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey naanama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey naanama"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha vesham enna ponnae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha vesham enna ponnae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan dirty pondattiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan dirty pondattiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan dirty pondattiya kanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan dirty pondattiya kanna"/>
</div>
<div class="lyrico-lyrics-wrapper">Thee mootti unna kooptathu naana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thee mootti unna kooptathu naana"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen appadi enna paakkura kanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen appadi enna paakkura kanna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaa naan thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaa naan thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha pechellaam pesunen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha pechellaam pesunen"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaa naan thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaa naan thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai ichchaiyil kaichinen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai ichchaiyil kaichinen"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaa naan thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaa naan thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Semma bodhaiya yethuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Semma bodhaiya yethuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaa naan thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaa naan thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai sothichu paakuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai sothichu paakuren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ohoo oo hoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohoo oo hoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ti dirty pondattiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ti dirty pondattiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohoo oo hoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohoo oo hoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ti dirty pondattiyae vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ti dirty pondattiyae vaa vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohoo oo hoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohoo oo hoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thee mootti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thee mootti"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna kooptathu naana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna kooptathu naana"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohoo oo hoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohoo oo hoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen appadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen appadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna paakkura kanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna paakkura kanna"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohoo oo hoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohoo oo hoo"/>
</div>
</pre>
