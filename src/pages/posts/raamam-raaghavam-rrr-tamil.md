---
title: "raamam raaghavam song lyrics"
album: "RRR Tamil"
artist: "Maragathamani"
lyricist: "K Shiva Dutta"
director: "S.S. Rajamouli "
path: "/albums/rrr-tamil-lyrics"
song: "Raamam Raaghavam"
image: ../../images/albumart/rrr-tamil.jpg
date: 2022-03-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/GF8aUIo6UVQ"
type: "happy"
singers:
  - Vijay Prakash
  - Chandana Bala Kalyan
  - Charu Hariharan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Oh ele ele yaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh ele ele yaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raamam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raamam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raamam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raamam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raamam raaghavam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raamam raaghavam "/>
</div>
<div class="lyrico-lyrics-wrapper">ranadheeram raajasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ranadheeram raajasam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rudhra dhanussama samana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rudhra dhanussama samana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Swadhanushtankhana bhayam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Swadhanushtankhana bhayam "/>
</div>
<div class="lyrico-lyrics-wrapper">bhrantha saathravam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bhrantha saathravam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raamam raaghavam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raamam raaghavam "/>
</div>
<div class="lyrico-lyrics-wrapper">ranadheeram raajasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ranadheeram raajasam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raamam raaghavam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raamam raaghavam "/>
</div>
<div class="lyrico-lyrics-wrapper">ranadheeram raajasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ranadheeram raajasam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ghandeeva mukhta 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ghandeeva mukhta "/>
</div>
<div class="lyrico-lyrics-wrapper">punkhanupunkaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="punkhanupunkaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sharaparamparaha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sharaparamparaha "/>
</div>
<div class="lyrico-lyrics-wrapper">davaleesatham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="davaleesatham"/>
</div>
<div class="lyrico-lyrics-wrapper">Raamam raaghavam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raamam raaghavam "/>
</div>
<div class="lyrico-lyrics-wrapper">ranadheeram raajasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ranadheeram raajasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Raamam raaghavam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raamam raaghavam "/>
</div>
<div class="lyrico-lyrics-wrapper">ranadheeram raajasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ranadheeram raajasam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hasthinapuram samasthatithasti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hasthinapuram samasthatithasti"/>
</div>
<div class="lyrico-lyrics-wrapper">Kumbhastaladi charan natarajam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kumbhastaladi charan natarajam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hasthinapuram samasthatithasti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hasthinapuram samasthatithasti"/>
</div>
<div class="lyrico-lyrics-wrapper">Kumbhastaladi charan natarajam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kumbhastaladi charan natarajam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raamam raaghavam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raamam raaghavam "/>
</div>
<div class="lyrico-lyrics-wrapper">ranadheeram raajasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ranadheeram raajasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Raamam raaghavam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raamam raaghavam "/>
</div>
<div class="lyrico-lyrics-wrapper">ranadheeram raajasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ranadheeram raajasam"/>
</div>
</pre>
