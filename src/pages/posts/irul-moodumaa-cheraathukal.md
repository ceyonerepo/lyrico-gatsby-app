---
title: "irul moodumee song lyrics"
album: "Cheraathukal"
artist: "Anand Nambiar - Pratik Abhyankar"
lyricist: "Dr. Mathew Mampra"
director: "Shanoob Karuvath "
path: "/albums/cheraathukal-lyrics"
song: "Irul Moodumee"
image: ../../images/albumart/cheraathukal.jpg
date: 2021-06-17
lang: malayalam
youtubeLink: "https://www.youtube.com/embed/Vx0_dZVYEiA"
type: "melody"
singers:
  - Kavalam Srikumar
  - Thaha Kolpad
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Irul moodumee vazhikalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irul moodumee vazhikalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Nizhal thedi nee varumoru naal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nizhal thedi nee varumoru naal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irul moodumee vazhikalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irul moodumee vazhikalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Nizhal thedi nee varumoru naal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nizhal thedi nee varumoru naal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Prakaashamaayole
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prakaashamaayole"/>
</div>
<div class="lyrico-lyrics-wrapper">Chiraathupol ennil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chiraathupol ennil"/>
</div>
<div class="lyrico-lyrics-wrapper">Aakashamaayole
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aakashamaayole"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilaavupol ullil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilaavupol ullil"/>
</div>
<div class="lyrico-lyrics-wrapper">Vannu chaare ninnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vannu chaare ninnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irul moodumee vazhikalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irul moodumee vazhikalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Nizhal thedi nee varumoru naal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nizhal thedi nee varumoru naal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaru ninne kaathirippu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaru ninne kaathirippu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponpuzha thalire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponpuzha thalire"/>
</div>
<div class="lyrico-lyrics-wrapper">Karayilere kaaryamund
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karayilere kaaryamund"/>
</div>
<div class="lyrico-lyrics-wrapper">Chollidam ponne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chollidam ponne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaaa… Aaaa….
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaa… Aaaa…."/>
</div>
<div class="lyrico-lyrics-wrapper">Karakal randum harithamaakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karakal randum harithamaakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponthalir puzhaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponthalir puzhaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathakal neerana kadhakalund
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathakal neerana kadhakalund"/>
</div>
<div class="lyrico-lyrics-wrapper">Onnu kelkkaamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnu kelkkaamo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninnidaathe enthinanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnidaathe enthinanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhoore vegam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhoore vegam"/>
</div>
</pre>
