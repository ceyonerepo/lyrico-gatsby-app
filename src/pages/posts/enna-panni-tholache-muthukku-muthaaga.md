---
title: "enna panni tholache song lyrics"
album: "Muthukku Muthaaga"
artist: "Kavi Periyathambi"
lyricist: "Na. Muthukumar - Nandalala"
director: "Rasu Madhuravan"
path: "/albums/muthukku-muthaaga-lyrics"
song: "Enna Panni Tholache"
image: ../../images/albumart/muthukku-muthaaga.jpg
date: 2011-03-18
lang: tamil
youtubeLink: "https://www.youtube.com/embed/rdgaR2CUAn4"
type: "love"
singers:
  - Vijay Yesudas
  - Chinmayi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Enna Panni Tholacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Panni Tholacha"/>
</div>
<div class="lyrico-lyrics-wrapper">En Nenjukkulla Raattinangal Suthuradhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nenjukkulla Raattinangal Suthuradhey"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Panni Tholacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Panni Tholacha"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kaadhukkulla Kaattukkuyil Kaththuradhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kaadhukkulla Kaattukkuyil Kaththuradhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Panni Tholacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Panni Tholacha"/>
</div>
<div class="lyrico-lyrics-wrapper">En Nenjukkulla Raattinangal Suthuradhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nenjukkulla Raattinangal Suthuradhey"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Panni Tholacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Panni Tholacha"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kaadhukkulla Kaattukkuyil Kaththuradhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kaadhukkulla Kaattukkuyil Kaththuradhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanimaiyila Sirikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanimaiyila Sirikkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaalaamal Thavikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaalaamal Thavikkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Raathiriyil Muzhikirean
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raathiriyil Muzhikirean"/>
</div>
<div class="lyrico-lyrics-wrapper">vidinjavudan Padukkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidinjavudan Padukkuren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakkey Theriyaama Nee Than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkey Theriyaama Nee Than"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Panni Tholacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Panni Tholacha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Beer Adikka Pudikkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Beer Adikka Pudikkala"/>
</div>
<div class="lyrico-lyrics-wrapper">beedi Kooda Pudikkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="beedi Kooda Pudikkala"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku Nee Enna Panni Tholacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku Nee Enna Panni Tholacha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho Soaru Thanni Pudikkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho Soaru Thanni Pudikkala"/>
</div>
<div class="lyrico-lyrics-wrapper">Sondhabandham Pudikkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sondhabandham Pudikkala"/>
</div>
<div class="lyrico-lyrics-wrapper">enakku Nee Enna Panni Tholacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enakku Nee Enna Panni Tholacha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Veettu Vazhi Theriyaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Veettu Vazhi Theriyaama"/>
</div>
<div class="lyrico-lyrics-wrapper">engeyo Poanen Puriyaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engeyo Poanen Puriyaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Panni Tholacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Panni Tholacha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Unnai Enni Ilachen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Unnai Enni Ilachen"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Vandhu Yedhaachum Sarippannudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vandhu Yedhaachum Sarippannudaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Panni Tholacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Panni Tholacha"/>
</div>
<div class="lyrico-lyrics-wrapper">En Nenjukkulla Raattinangal Suthuradhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nenjukkulla Raattinangal Suthuradhey"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Panni Tholacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Panni Tholacha"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kaadhukkulla Kaattukkuyil Kaththuradhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kaadhukkulla Kaattukkuyil Kaththuradhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gundu Malli Pudikkidhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundu Malli Pudikkidhu"/>
</div>
<div class="lyrico-lyrics-wrapper">kuppathotti Pudikkidhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuppathotti Pudikkidhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku Nee Enna Panni Tholacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku Nee Enna Panni Tholacha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Periyaarum Pudikkidhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Periyaarum Pudikkidhu"/>
</div>
<div class="lyrico-lyrics-wrapper">perumaalum Pudikkidhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="perumaalum Pudikkidhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku Nee Enna Panni Tholacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku Nee Enna Panni Tholacha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kudai Irundhum Adha Pudikkaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudai Irundhum Adha Pudikkaama"/>
</div>
<div class="lyrico-lyrics-wrapper">nanainjeney Naanum Puriyaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanainjeney Naanum Puriyaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Panni Tholacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Panni Tholacha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Unnai Enni Thavichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Unnai Enni Thavichen"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Vandhu Yedhaachum Sarippannudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vandhu Yedhaachum Sarippannudi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Panni Tholacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Panni Tholacha"/>
</div>
<div class="lyrico-lyrics-wrapper">En Nenjukkulla Raattinangal Suthuradhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nenjukkulla Raattinangal Suthuradhey"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Panni Tholacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Panni Tholacha"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kaadhukkulla Kaattukkuyil Kaththuradhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kaadhukkulla Kaattukkuyil Kaththuradhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanimaiyila Sirikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanimaiyila Sirikkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaalaamal Thavikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaalaamal Thavikkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Raathiriyil Muzhikirean
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raathiriyil Muzhikirean"/>
</div>
<div class="lyrico-lyrics-wrapper">vidinjavudan Padukkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidinjavudan Padukkuren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakkey Theriyaama Nee Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkey Theriyaama Nee Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Panni Tholacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Panni Tholacha"/>
</div>
</pre>
