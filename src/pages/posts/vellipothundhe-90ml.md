---
title: "vellipothundhe song lyrics"
album: "90 ML"
artist: "Anup Rubens"
lyricist: "Chandrabose"
director: "SekharReddy Yerra"
path: "/albums/90ml-lyrics"
song: "Vellipothundhe"
image: ../../images/albumart/90ml.jpg
date: 2019-12-06
lang: telugu
youtubeLink: "https://www.youtube.com/embed/thxzYbMMZsA"
type: "sad"
singers:
  - Anup Rubens
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">vellipothundhe vellipothundhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vellipothundhe vellipothundhe"/>
</div>
<div class="lyrico-lyrics-wrapper">nanne vidachi thanu veluthundheyyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanne vidachi thanu veluthundheyyy"/>
</div>
<div class="lyrico-lyrics-wrapper">vellipothundhe vellipothundhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vellipothundhe vellipothundhe"/>
</div>
<div class="lyrico-lyrics-wrapper">nanne vidachi thanu veluthundheyyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanne vidachi thanu veluthundheyyy"/>
</div>
<div class="lyrico-lyrics-wrapper">vellipothundhe vellipothundhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vellipothundhe vellipothundhe"/>
</div>
<div class="lyrico-lyrics-wrapper">kannulu vadali kala veluthundhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannulu vadali kala veluthundhe"/>
</div>
<div class="lyrico-lyrics-wrapper">gundelu vadali laya veluthundhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gundelu vadali laya veluthundhe"/>
</div>
<div class="lyrico-lyrics-wrapper">gudine vadali devatha veluthundhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gudine vadali devatha veluthundhey"/>
</div>
<div class="lyrico-lyrics-wrapper">pedavini vadali navveluthundhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pedavini vadali navveluthundhe"/>
</div>
<div class="lyrico-lyrics-wrapper">nanne vadali needeluthundhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanne vadali needeluthundhe"/>
</div>
<div class="lyrico-lyrics-wrapper">brathukunu vadali 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="brathukunu vadali "/>
</div>
<div class="lyrico-lyrics-wrapper">bandham veluthundhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bandham veluthundhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vellipothundhe vellipothundhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vellipothundhe vellipothundhe"/>
</div>
<div class="lyrico-lyrics-wrapper">nanne vidachi thanu veluthundheyyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanne vidachi thanu veluthundheyyy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kathale chebithe keratam vinatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathale chebithe keratam vinatha"/>
</div>
<div class="lyrico-lyrics-wrapper">ninge chebithe megham vinadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninge chebithe megham vinadha"/>
</div>
<div class="lyrico-lyrics-wrapper">chinuke chebithe chigure vinadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chinuke chebithe chigure vinadha"/>
</div>
<div class="lyrico-lyrics-wrapper">gaale chebithe murale vinadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gaale chebithe murale vinadha"/>
</div>
<div class="lyrico-lyrics-wrapper">cheppeyalani tondara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cheppeyalani tondara"/>
</div>
<div class="lyrico-lyrics-wrapper">naadhi vinakudadhane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naadhi vinakudadhane"/>
</div>
<div class="lyrico-lyrics-wrapper">pantham thanadhi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pantham thanadhi "/>
</div>
<div class="lyrico-lyrics-wrapper">pranamlanti preme tanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pranamlanti preme tanadi"/>
</div>
<div class="lyrico-lyrics-wrapper">pranam poye badhe naadeleyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pranam poye badhe naadeleyy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vellipothundhe vellipothundhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vellipothundhe vellipothundhe"/>
</div>
<div class="lyrico-lyrics-wrapper">nanne vidachi thanu veluthundheyyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanne vidachi thanu veluthundheyyy"/>
</div>
</pre>
