---
title: "theeraadho vali song lyrics"
album: "Mr Chandramouli"
artist: "Sam CS"
lyricist: "Sam CS"
director: "Thiru"
path: "/albums/mr-chandramouli-lyrics"
song: "Theeraadho Vali"
image: ../../images/albumart/mr-chandramouli.jpg
date: 2018-07-06
lang: tamil
youtubeLink: "https://www.youtube.com/embed/md7YyKaP7lI"
type: "sad"
singers:
  - Haricharan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">theeratho vali maaratho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theeratho vali maaratho"/>
</div>
<div class="lyrico-lyrics-wrapper">ravondru maayatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ravondru maayatho"/>
</div>
<div class="lyrico-lyrics-wrapper">theeratho vali maaratho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theeratho vali maaratho"/>
</div>
<div class="lyrico-lyrics-wrapper">ravondru maayatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ravondru maayatho"/>
</div>
<div class="lyrico-lyrics-wrapper">nee illamal ini ennalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee illamal ini ennalum"/>
</div>
<div class="lyrico-lyrics-wrapper">naan ennaveno thannala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan ennaveno thannala"/>
</div>
<div class="lyrico-lyrics-wrapper">thaai pola ethano nee thangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaai pola ethano nee thangi"/>
</div>
<div class="lyrico-lyrics-wrapper">ennai aalakki vittu thalli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennai aalakki vittu thalli"/>
</div>
<div class="lyrico-lyrics-wrapper">ipo pona yen nee pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ipo pona yen nee pona"/>
</div>
<div class="lyrico-lyrics-wrapper">yeno nee pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yeno nee pona"/>
</div>
<div class="lyrico-lyrics-wrapper">thaniya thavikka vitutu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaniya thavikka vitutu"/>
</div>
<div class="lyrico-lyrics-wrapper">pona yen nee pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pona yen nee pona"/>
</div>
<div class="lyrico-lyrics-wrapper">yeno nee pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yeno nee pona"/>
</div>
<div class="lyrico-lyrics-wrapper">kanatha unaiye 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanatha unaiye "/>
</div>
<div class="lyrico-lyrics-wrapper">vitutu pona yen 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vitutu pona yen "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kadivaalam pol unai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadivaalam pol unai"/>
</div>
<div class="lyrico-lyrics-wrapper">paarthen naan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paarthen naan "/>
</div>
<div class="lyrico-lyrics-wrapper">thinam vaalthen nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thinam vaalthen nee"/>
</div>
<div class="lyrico-lyrics-wrapper">than en paathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="than en paathai"/>
</div>
<div class="lyrico-lyrics-wrapper">maranthayo nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maranthayo nee"/>
</div>
<div class="lyrico-lyrics-wrapper">marupaayo nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="marupaayo nee"/>
</div>
<div class="lyrico-lyrics-wrapper">irupen nan ingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irupen nan ingu"/>
</div>
<div class="lyrico-lyrics-wrapper">nee illame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee illame"/>
</div>
<div class="lyrico-lyrics-wrapper">theivangal venumnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theivangal venumnu"/>
</div>
<div class="lyrico-lyrics-wrapper">epothum thonala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="epothum thonala"/>
</div>
<div class="lyrico-lyrics-wrapper">unnoda anbai meeri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnoda anbai meeri"/>
</div>
<div class="lyrico-lyrics-wrapper">paarthida ethetho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paarthida ethetho"/>
</div>
<div class="lyrico-lyrics-wrapper">theivangal ipothu iruka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theivangal ipothu iruka"/>
</div>
<div class="lyrico-lyrics-wrapper">ennoda kaneera than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennoda kaneera than"/>
</div>
<div class="lyrico-lyrics-wrapper">paarthida oh oh oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paarthida oh oh oh"/>
</div>
<div class="lyrico-lyrics-wrapper">oh oh oh oh oh oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oh oh oh oh oh oh"/>
</div>
<div class="lyrico-lyrics-wrapper">oh oh oh oh oh oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oh oh oh oh oh oh"/>
</div>
<div class="lyrico-lyrics-wrapper">oh oh oh oh oh oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oh oh oh oh oh oh"/>
</div>
</pre>
