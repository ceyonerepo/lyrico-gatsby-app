---
title: "netrikann title track song lyrics"
album: "Netrikann"
artist: "Girishh G"
lyricist: "Vignesh Shivan"
director: "Milind Rau"
path: "/albums/netrikann-song-lyrics"
song: "Netrikann Title Track"
image: ../../images/albumart/netrikann.jpg
date: 2021-08-13
lang: tamil
youtubeLink:  "https://www.youtube.com/embed/4Fny41yg9R8"
type: "title track"
singers:
  - Poorvi Koutish
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Paarthavai marandhu pogalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarthavai marandhu pogalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Padhaikum paavangal seidhorin 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padhaikum paavangal seidhorin "/>
</div>
<div class="lyrico-lyrics-wrapper">bimbangal pogadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bimbangal pogadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Paavaiyin paarvai pogalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paavaiyin paarvai pogalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Irukkum kayangal kovangal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irukkum kayangal kovangal "/>
</div>
<div class="lyrico-lyrics-wrapper">eppodhum aaradhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eppodhum aaradhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naam kann moodiyae kidappadhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam kann moodiyae kidappadhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottangal nee podadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottangal nee podadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai indru verodu thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai indru verodu thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pidunga pogindren naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pidunga pogindren naanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaanadha yaavaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanadha yaavaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kann moodi kaana 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kann moodi kaana "/>
</div>
<div class="lyrico-lyrics-wrapper">vendum enbadharkaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vendum enbadharkaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Moodamal vazhgira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moodamal vazhgira"/>
</div>
<div class="lyrico-lyrics-wrapper">Kann ondru ennidathil ulladhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kann ondru ennidathil ulladhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Netrikann eppodhum moodadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netrikann eppodhum moodadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Netrikann eppodhum saagaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netrikann eppodhum saagaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Netrikann kutrangal thaangaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netrikann kutrangal thaangaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Netrikann ennalum thorkaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netrikann ennalum thorkaadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Netrikann
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netrikann"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nerigalai murikkum mirugam ondrai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nerigalai murikkum mirugam ondrai"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhikkavae ingu evarum illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhikkavae ingu evarum illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Narigalai erikka thirakka vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Narigalai erikka thirakka vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Netrikann ondrai netrikann ondrai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netrikann ondrai netrikann ondrai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nerigalai murikkum mirugam ondrai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nerigalai murikkum mirugam ondrai"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhikkavae ingu evarum illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhikkavae ingu evarum illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Narigalai erikka thirakka vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Narigalai erikka thirakka vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Netrikann ondrai netrikann ondrai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netrikann ondrai netrikann ondrai"/>
</div>
<div class="lyrico-lyrics-wrapper">Moorgathil moozhgi ullen indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moorgathil moozhgi ullen indru"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeradha seetram innum undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeradha seetram innum undu"/>
</div>
<div class="lyrico-lyrics-wrapper">Por seiyum aavalodu ingu kaathiruppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Por seiyum aavalodu ingu kaathiruppen"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil krodham kondu indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil krodham kondu indru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aagayam indru aarvamodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagayam indru aarvamodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai thooki sendru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai thooki sendru "/>
</div>
<div class="lyrico-lyrics-wrapper">sutterikka kaathirukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sutterikka kaathirukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Boolagam thandha veerathodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boolagam thandha veerathodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai kolla naan nindrurikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai kolla naan nindrurikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaanadha yaavaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanadha yaavaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kann moodi kaana 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kann moodi kaana "/>
</div>
<div class="lyrico-lyrics-wrapper">vendum enbadharkaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vendum enbadharkaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Moodamal vazhgira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moodamal vazhgira"/>
</div>
<div class="lyrico-lyrics-wrapper">Kann ondru ennidathil ulladhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kann ondru ennidathil ulladhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Netrikann eppodhum moodaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netrikann eppodhum moodaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Netrikann edraigum saagaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netrikann edraigum saagaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Netrikann kutrangal thaangaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netrikann kutrangal thaangaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Netrikann ennalum thorkaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netrikann ennalum thorkaadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Netrikann
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netrikann"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nerigalai murikkum mirugam ondrai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nerigalai murikkum mirugam ondrai"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhikkavae ingu evarum illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhikkavae ingu evarum illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Narigalai erikka thirakka vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Narigalai erikka thirakka vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Netrikann ondrai netrikann ondrai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netrikann ondrai netrikann ondrai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nerigalai murikkum mirugam ondrai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nerigalai murikkum mirugam ondrai"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhikkavae ingu evarum illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhikkavae ingu evarum illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Narigalai erikka thirakka vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Narigalai erikka thirakka vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Netrikann ondrai netrikann ondrai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netrikann ondrai netrikann ondrai"/>
</div>
</pre>
