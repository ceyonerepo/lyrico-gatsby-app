---
title: "ennanna song lyrics"
album: "Sadhurangam"
artist: "Vidyasagar"
lyricist: "Pa. Vijay"
director: "Karu Pazhaniappan"
path: "/albums/sadhurangam-lyrics"
song: "Ennanna"
image: ../../images/albumart/sadhurangam.jpg
date: 2011-10-08
lang: tamil
youtubeLink: "https://www.youtube.com/embed/xMu2EoMfAgQ"
type: "love"
singers:
  - Sunitha Sarathy
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">ennenne Pidikkum Neeyen Seithaal Pidikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennenne Pidikkum Neeyen Seithaal Pidikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">athuvellaam Pidikkum Nee Athu Seythaal Pidikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athuvellaam Pidikkum Nee Athu Seythaal Pidikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ennenne Pidikkum Neeyen Seithaal Pidikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennenne Pidikkum Neeyen Seithaal Pidikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">athuvellaam Pidikkum Nee Athu Seythaal Pidikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athuvellaam Pidikkum Nee Athu Seythaal Pidikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">taan Taan Taan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="taan Taan Taan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ennenne Pidikkum Neeyen Seithaal Pidikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennenne Pidikkum Neeyen Seithaal Pidikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">athuvellaam Pidikkum Nee Athu Seythaal Pidikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athuvellaam Pidikkum Nee Athu Seythaal Pidikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">kuru Kuru Kuruvenum Kurumbu Paarvaikal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuru Kuru Kuruvenum Kurumbu Paarvaikal"/>
</div>
<div class="lyrico-lyrics-wrapper">pidikkum Pidikkum Athu Romba Pidikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pidikkum Pidikkum Athu Romba Pidikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">viru Viru Viruvene Uruthum Meesaikal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viru Viru Viruvene Uruthum Meesaikal"/>
</div>
<div class="lyrico-lyrics-wrapper">pidikkum Pidikkum Athu Romba Pidikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pidikkum Pidikkum Athu Romba Pidikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">olivu Maraivindri Kalanthu Urayaadal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="olivu Maraivindri Kalanthu Urayaadal"/>
</div>
<div class="lyrico-lyrics-wrapper">vayasin Thaevaiku Konjam Vilaayadal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vayasin Thaevaiku Konjam Vilaayadal"/>
</div>
<div class="lyrico-lyrics-wrapper">thoalil Saaythal Sarithal Valithal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoalil Saaythal Sarithal Valithal"/>
</div>
<div class="lyrico-lyrics-wrapper">kulaithal Thaan Pidikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulaithal Thaan Pidikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aye Ya La La Re Re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aye Ya La La Re Re"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ennenne Pidikkum Neeyen Seithaal Pidikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennenne Pidikkum Neeyen Seithaal Pidikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">athuvellaam Pidikkum Nee Athu Seythaal Pidikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athuvellaam Pidikkum Nee Athu Seythaal Pidikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ennenne Pidikkum Neeyen Seithaal Pidikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennenne Pidikkum Neeyen Seithaal Pidikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">athuvellaam Pidikkum Nee Athu Seythaal Pidikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athuvellaam Pidikkum Nee Athu Seythaal Pidikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">yay Yay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yay Yay"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">meesai Viralaal Mottu Parithaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meesai Viralaal Mottu Parithaal"/>
</div>
<div class="lyrico-lyrics-wrapper">vellai Patkalaal Kannam Korithaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vellai Patkalaal Kannam Korithaal"/>
</div>
<div class="lyrico-lyrics-wrapper">pidikkum Pidikkum Athu Romba Pidikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pidikkum Pidikkum Athu Romba Pidikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">koonthal Thulavi Koodu Pirithaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koonthal Thulavi Koodu Pirithaal"/>
</div>
<div class="lyrico-lyrics-wrapper">saayum Kazhuthil Vaasam Pidithaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saayum Kazhuthil Vaasam Pidithaal"/>
</div>
<div class="lyrico-lyrics-wrapper">pidikkum Pidikkum Athu Romba Pidikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pidikkum Pidikkum Athu Romba Pidikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">viralaal Aduthaathum Thaede
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viralaal Aduthaathum Thaede"/>
</div>
<div class="lyrico-lyrics-wrapper">ithazhaal Ithazh Rendum Moode
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithazhaal Ithazh Rendum Moode"/>
</div>
<div class="lyrico-lyrics-wrapper">uyiraal Uyiroadu Koodal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyiraal Uyiroadu Koodal"/>
</div>
<div class="lyrico-lyrics-wrapper">intha Koolam Pidikkum alangoalam Pidikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha Koolam Pidikkum alangoalam Pidikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">hey Aye Ya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey Aye Ya"/>
</div>
<div class="lyrico-lyrics-wrapper">lala Re Re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lala Re Re"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ennenne Pidikkum Neeyen Seithaal Pidikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennenne Pidikkum Neeyen Seithaal Pidikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">athuvellaam Pidikkum Nee Athu Seythaal Pidikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athuvellaam Pidikkum Nee Athu Seythaal Pidikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ennenne Pidikkum Neeyen Seithaal Pidikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennenne Pidikkum Neeyen Seithaal Pidikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">athuvellaam Pidikkum Nee Athu Seythaal Pidikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athuvellaam Pidikkum Nee Athu Seythaal Pidikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">taan Taan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="taan Taan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sella Sandaikal Seythu Nadithaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sella Sandaikal Seythu Nadithaal"/>
</div>
<div class="lyrico-lyrics-wrapper">en Chellame Endru Konji Anaithaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en Chellame Endru Konji Anaithaal"/>
</div>
<div class="lyrico-lyrics-wrapper">pidikkum Pidikkum Athu Romba Pidikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pidikkum Pidikkum Athu Romba Pidikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">thottu Thadava Kaatum Thayakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thottu Thadava Kaatum Thayakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">thottavudane Thoandrum Nadukkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thottavudane Thoandrum Nadukkam"/>
</div>
<div class="lyrico-lyrics-wrapper">pidikkum Pidikkum Athu Romba Pidikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pidikkum Pidikkum Athu Romba Pidikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">uthadai Kadikka Athu Porunthal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uthadai Kadikka Athu Porunthal"/>
</div>
<div class="lyrico-lyrics-wrapper">udayay Kasakka Athu Thazhuval
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="udayay Kasakka Athu Thazhuval"/>
</div>
<div class="lyrico-lyrics-wrapper">udalai Norukke Athu Urasal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="udalai Norukke Athu Urasal"/>
</div>
<div class="lyrico-lyrics-wrapper">ivaiyaavum Pidikkum Pidivaatham Pidikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ivaiyaavum Pidikkum Pidivaatham Pidikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ennenne Pidikkum Neeyen Seithaal Pidikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennenne Pidikkum Neeyen Seithaal Pidikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">athuvellaam Pidikkum Nee Athu Seythaal Pidikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athuvellaam Pidikkum Nee Athu Seythaal Pidikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ennenne Pidikkum Neeyen Seithaal Pidikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennenne Pidikkum Neeyen Seithaal Pidikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">athuvellaam Pidikkum Nee Athu Seythaal Pidikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athuvellaam Pidikkum Nee Athu Seythaal Pidikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kuru Kuru Kuruvenum Kurumbu Paarvaikal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuru Kuru Kuruvenum Kurumbu Paarvaikal"/>
</div>
<div class="lyrico-lyrics-wrapper">pidikkum Pidikkum Athu Romba Pidikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pidikkum Pidikkum Athu Romba Pidikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">thiru Thiru Thiruvene Uruthum Meesaikal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thiru Thiru Thiruvene Uruthum Meesaikal"/>
</div>
<div class="lyrico-lyrics-wrapper">pidikkum Pidikkum Athu Romba Pidikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pidikkum Pidikkum Athu Romba Pidikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">olivu Maraivindri Kalanthu Urayaadal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="olivu Maraivindri Kalanthu Urayaadal"/>
</div>
<div class="lyrico-lyrics-wrapper">vayasin Thaevaiku Kangal Vilaayadal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vayasin Thaevaiku Kangal Vilaayadal"/>
</div>
<div class="lyrico-lyrics-wrapper">thoalil Saaythal, Sarithal, Valithal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoalil Saaythal, Sarithal, Valithal"/>
</div>
<div class="lyrico-lyrics-wrapper">kulaithal Thaan Pidikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulaithal Thaan Pidikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aye Ya Ya La La Re Re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aye Ya Ya La La Re Re"/>
</div>
</pre>
