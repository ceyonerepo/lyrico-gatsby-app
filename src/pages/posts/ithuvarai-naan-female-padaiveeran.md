---
title: "ithuvarai naan female song lyrics"
album: "Padaiveeran"
artist: "Karthik Raja"
lyricist: "Priyan"
director: "Dhana Sekaran"
path: "/albums/padaiveeran-lyrics"
song: "Ithuvarai Naan Female"
image: ../../images/albumart/padaiveeran.jpg
date: 2018-02-02
lang: tamil
youtubeLink: "https://www.youtube.com/embed/b0_FbcrA4Zs"
type: "happy"
singers:
  - Chinmayi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ithuvarai naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithuvarai naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhupol illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhupol illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini yenna naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini yenna naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Seiven ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seiven ennai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En vasam naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En vasam naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen indru illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen indru illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadapathellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadapathellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Karpanai illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karpanai illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaar enna seithaar ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar enna seithaar ennai"/>
</div>
<div class="lyrico-lyrics-wrapper">Karanam verethum illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karanam verethum illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee vaa vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee vaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee vaaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muzhuvathumaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muzhuvathumaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai veruthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai veruthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Athil irunthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athil irunthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan unai rasithen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan unai rasithen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mugam kandaalumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mugam kandaalumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kobangal konda naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kobangal konda naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Un peyar sonaalum yen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un peyar sonaalum yen"/>
</div>
<div class="lyrico-lyrics-wrapper">Punnagai pookiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Punnagai pookiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yevar munnaalumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yevar munnaalumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanaipol nindra naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanaipol nindra naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Unidam pennaagiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unidam pennaagiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetkamen sinthinen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetkamen sinthinen"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanam thooral tharuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam thooral tharuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhum aasai varuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhum aasai varuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethaan enthan arugil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethaan enthan arugil"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa vaa nee vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa vaa nee vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muzhuvathumaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muzhuvathumaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai veruthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai veruthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Athil irunthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athil irunthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan unai rasithen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan unai rasithen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanam pogamalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanam pogamalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanimaiyil nindra naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanimaiyil nindra naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ikkanam pothaamalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ikkanam pothaamalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedal yen kolgiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedal yen kolgiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unai ennaamalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai ennaamalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilagiyae sendra naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilagiyae sendra naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Indrunai kaanumvarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indrunai kaanumvarai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippadi thavikiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippadi thavikiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nodigal theernthu pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nodigal theernthu pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai naan sera vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai naan sera vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Udanae enthan arugil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udanae enthan arugil"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa vaa nee vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa vaa nee vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muzhuvathumaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muzhuvathumaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai veruthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai veruthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Athil irunthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athil irunthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan unai rasithen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan unai rasithen"/>
</div>
</pre>
