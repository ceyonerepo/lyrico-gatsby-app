---
title: "rasathi kili song lyrics"
album: "Pillaiyar Theru Kadaisi Veedu"
artist: "Chakri"
lyricist: "Snehan"
director: "Thirumalai Kishore"
path: "/albums/pillaiyar-theru-kadaisi-veedu-lyrics"
song: "Rasathi Kili"
image: ../../images/albumart/pillaiyar-theru-kadaisi-veedu.jpg
date: 2011-06-24
lang: tamil
youtubeLink: "https://www.youtube.com/embed/b56fgBQE2Jk"
type: "happy"
singers:
  - Achu
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Raasaathi Kilikki Ennoada 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raasaathi Kilikki Ennoada "/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathal Theriyaamapoachuthada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathal Theriyaamapoachuthada"/>
</div>
<div class="lyrico-lyrics-wrapper">Naaikkutti Poala Ennoada Kaathal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naaikkutti Poala Ennoada Kaathal "/>
</div>
<div class="lyrico-lyrics-wrapper">Ava Pinnaal Oduthada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Pinnaal Oduthada"/>
</div>
<div class="lyrico-lyrics-wrapper">Onnoada Manasa Thethiko Maamu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnoada Manasa Thethiko Maamu "/>
</div>
<div class="lyrico-lyrics-wrapper">Naangallaam Irukkoamada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naangallaam Irukkoamada"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Pekka Poattaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Pekka Poattaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Thoongalaam Neethaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoongalaam Neethaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Atha Neeyum Marakkaladaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atha Neeyum Marakkaladaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattunnuthaan Paanai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattunnuthaan Paanai "/>
</div>
<div class="lyrico-lyrics-wrapper">Poala Odaichipputtaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poala Odaichipputtaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasa Purinjikkaama 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasa Purinjikkaama "/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathalathaan Azhichiputtaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathalathaan Azhichiputtaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Avala Maranthu Eppadi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avala Maranthu Eppadi "/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Vaazhapporiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vaazhapporiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Illa Ippadiye Polambi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illa Ippadiye Polambi "/>
</div>
<div class="lyrico-lyrics-wrapper">Polambi Saagapoariyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Polambi Saagapoariyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ae Ponnungala Eppoathum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Ponnungala Eppoathum "/>
</div>
<div class="lyrico-lyrics-wrapper">Nambidaatha Mannaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nambidaatha Mannaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Devathasu Annaikae 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Devathasu Annaikae "/>
</div>
<div class="lyrico-lyrics-wrapper">Advaisu Panninaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Advaisu Panninaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ae Unmaiyaana Kaathalu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Unmaiyaana Kaathalu "/>
</div>
<div class="lyrico-lyrics-wrapper">Oothikkichi Mannaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oothikkichi Mannaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponnungalellaam Poiyinnu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponnungalellaam Poiyinnu "/>
</div>
<div class="lyrico-lyrics-wrapper">Solraa Mannaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solraa Mannaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Raasaathi Kilikki Ennoada 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raasaathi Kilikki Ennoada "/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathal Theriyaamapoachuthada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathal Theriyaamapoachuthada"/>
</div>
<div class="lyrico-lyrics-wrapper">Naaikkutti Poala Ennoada Kaathal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naaikkutti Poala Ennoada Kaathal "/>
</div>
<div class="lyrico-lyrics-wrapper">Ava Pinnaal Oduthada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Pinnaal Oduthada"/>
</div>
</pre>
