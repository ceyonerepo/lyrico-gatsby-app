---
title: "mokka love song song lyrics"
album: "Iruttu Araiyil Murattu Kuththu"
artist: "Balamurali Balu"
lyricist: "Gaana Kadal - Santhosh P - Jayakumar"
director: "Uday yadav"
path: "/albums/iruttu-araiyil-murattu-kuththu-song-lyrics"
song: "Mokka Love Song"
image: ../../images/albumart/iruttu-araiyil-murattu-kuththu.jpg
date: 2018-05-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/VuuY061zsqU"
type: "love"
singers:
  - Anthony Daasan
  - Gaana Kadal
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Oho hoo hoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oho hoo hoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen yen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen yen"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna matta panna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna matta panna"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendu goli gundu kannala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendu goli gundu kannala"/>
</div>
<div class="lyrico-lyrics-wrapper">Paartha enna kaanavillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paartha enna kaanavillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mental aagurendi unnala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mental aagurendi unnala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pagal ellam iruttu aachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagal ellam iruttu aachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavellam colour aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavellam colour aachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ice aana ava pechu karanjedhaan pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ice aana ava pechu karanjedhaan pochu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oho hoo hoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oho hoo hoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nice aaga pudichenda naanum oru catch
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nice aaga pudichenda naanum oru catch"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen yen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen yen"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna matta panna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna matta panna"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendu goli gundu kannala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendu goli gundu kannala"/>
</div>
<div class="lyrico-lyrics-wrapper">Paartha enna kaanavillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paartha enna kaanavillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mental aagurendi unnala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mental aagurendi unnala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ullelae ullelae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullelae ullelae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullelae ullelae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullelae ullelae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullelae lae lae lae lae lae lae lae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullelae lae lae lae lae lae lae lae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullelae ullelae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullelae ullelae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullelae ullelae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullelae ullelae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullelae lae lae lae lae lae lae lae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullelae lae lae lae lae lae lae lae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oho hoo hoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oho hoo hoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Devasena pol oru kudhira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Devasena pol oru kudhira"/>
</div>
<div class="lyrico-lyrics-wrapper">En munnae vaara da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En munnae vaara da"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai thaandi pogirapodhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai thaandi pogirapodhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Baahubali aanen da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baahubali aanen da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Looku thakuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Looku thakuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Keela thookuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Keela thookuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Bitu paaka vaikudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bitu paaka vaikudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aduchi aduchi dhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aduchi aduchi dhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Regai theyudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Regai theyudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pimple etti paakudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pimple etti paakudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Edho aaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edho aaachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Waste-uh pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Waste-uh pochu"/>
</div>
<div class="lyrico-lyrics-wrapper">Switcha potta bulb-uh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Switcha potta bulb-uh"/>
</div>
<div class="lyrico-lyrics-wrapper">Fuse-eh pochae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fuse-eh pochae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ullelae ullelae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullelae ullelae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullelae ullelae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullelae ullelae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullelae lae lae lae lae lae lae lae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullelae lae lae lae lae lae lae lae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullelae ullelae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullelae ullelae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullelae ullelae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullelae ullelae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullelae lae lae lae lae lae lae lae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullelae lae lae lae lae lae lae lae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen yen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen yen"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna matta panna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna matta panna"/>
</div>
<div class="lyrico-lyrics-wrapper">Matta panna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Matta panna"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendu goli gundu kannala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendu goli gundu kannala"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paartha enna kaanavillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paartha enna kaanavillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanavillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanavillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mental aagurendi unnala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mental aagurendi unnala"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pagal ellam iruttu aachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagal ellam iruttu aachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavellam colour aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavellam colour aachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ice aana ava pechu karanjedhaan pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ice aana ava pechu karanjedhaan pochu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oho hoo hoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oho hoo hoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nice aaga pudichenda naanum oru catch
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nice aaga pudichenda naanum oru catch"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen yen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen yen"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna matta panna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna matta panna"/>
</div>
<div class="lyrico-lyrics-wrapper">Matta panna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Matta panna"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendu goli gundu kannala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendu goli gundu kannala"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paartha enna kaanavillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paartha enna kaanavillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanavillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanavillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mental aagurendi unnala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mental aagurendi unnala"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnala"/>
</div>
</pre>
