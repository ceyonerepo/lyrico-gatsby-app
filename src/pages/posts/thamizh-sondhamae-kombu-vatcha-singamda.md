---
title: "thamizh sondhamae song lyrics"
album: "Kombu Vatcha Singamda"
artist: "Dhibu Ninan Thomas"
lyricist: "Yugabharathi"
director: "S.R. Prabhakaran"
path: "/albums/kombu-vatcha-singamda-lyrics"
song: "Thamizh Sondhamae"
image: ../../images/albumart/kombu-vatcha-singamda.jpg
date: 2022-01-13
lang: tamil
youtubeLink: "https://www.youtube.com/embed/nKYdn41WM2s"
type: "mass"
singers:
  - Kapil Kapilan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">sattai edu sattai edu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sattai edu sattai edu"/>
</div>
<div class="lyrico-lyrics-wrapper">kai vilangu therikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kai vilangu therikka"/>
</div>
<div class="lyrico-lyrics-wrapper">potti idu potti idu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="potti idu potti idu"/>
</div>
<div class="lyrico-lyrics-wrapper">bhoomi kodi pidikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bhoomi kodi pidikka"/>
</div>
<div class="lyrico-lyrics-wrapper">yetram idhu yetram idhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yetram idhu yetram idhu"/>
</div>
<div class="lyrico-lyrics-wrapper">yar ithanai thadukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yar ithanai thadukka"/>
</div>
<div class="lyrico-lyrics-wrapper">maatram idhu maatram idhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maatram idhu maatram idhu"/>
</div>
<div class="lyrico-lyrics-wrapper">paar manidham jeika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paar manidham jeika"/>
</div>
<div class="lyrico-lyrics-wrapper">naan matum uyir vazha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan matum uyir vazha"/>
</div>
<div class="lyrico-lyrics-wrapper">ninaikatha manam vendume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninaikatha manam vendume"/>
</div>
<div class="lyrico-lyrics-wrapper">naam endru ninaithale 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naam endru ninaithale "/>
</div>
<div class="lyrico-lyrics-wrapper">varum kalam varaverkume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varum kalam varaverkume"/>
</div>
<div class="lyrico-lyrics-wrapper">yaro sila per aadukira aatam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaro sila per aadukira aatam"/>
</div>
<div class="lyrico-lyrics-wrapper">oorai ulagai oonjal ena aatum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oorai ulagai oonjal ena aatum"/>
</div>
<div class="lyrico-lyrics-wrapper">soodhai seivor vilaiyatile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="soodhai seivor vilaiyatile"/>
</div>
<div class="lyrico-lyrics-wrapper">ezhai kootam katharum man mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ezhai kootam katharum man mela"/>
</div>
<div class="lyrico-lyrics-wrapper">thamizh sondhame nee nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thamizh sondhame nee nee"/>
</div>
<div class="lyrico-lyrics-wrapper">thani illai nee orr kootam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thani illai nee orr kootam"/>
</div>
<div class="lyrico-lyrics-wrapper">bayam indriye nee nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bayam indriye nee nee"/>
</div>
<div class="lyrico-lyrics-wrapper">kalam sendru vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalam sendru vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">tamil sonthame vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tamil sonthame vaa vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">uravendru naam kai korpom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uravendru naam kai korpom"/>
</div>
<div class="lyrico-lyrics-wrapper">pagai indriye naam naam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pagai indriye naam naam"/>
</div>
<div class="lyrico-lyrics-wrapper">uyir anbile serthiduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyir anbile serthiduvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sattamoru pakkam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sattamoru pakkam "/>
</div>
<div class="lyrico-lyrics-wrapper">dharmamoru pakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dharmamoru pakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">irunthalum yematrame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irunthalum yematrame"/>
</div>
<div class="lyrico-lyrics-wrapper">unmai edhu endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unmai edhu endru"/>
</div>
<div class="lyrico-lyrics-wrapper">ullam oli kandan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ullam oli kandan"/>
</div>
<div class="lyrico-lyrics-wrapper">thodarathu thallatame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thodarathu thallatame"/>
</div>
<div class="lyrico-lyrics-wrapper">andradam katchi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="andradam katchi "/>
</div>
<div class="lyrico-lyrics-wrapper">kannerai pokka ennamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannerai pokka ennamal"/>
</div>
<div class="lyrico-lyrics-wrapper">vaalvu illaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalvu illaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">ellorum oru naal mannagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellorum oru naal mannagi"/>
</div>
<div class="lyrico-lyrics-wrapper">povom anbal inainthal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="povom anbal inainthal "/>
</div>
<div class="lyrico-lyrics-wrapper">nodiyum savillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nodiyum savillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thamizh sondhame nee nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thamizh sondhame nee nee"/>
</div>
<div class="lyrico-lyrics-wrapper">thani illai nee orr kootam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thani illai nee orr kootam"/>
</div>
<div class="lyrico-lyrics-wrapper">bayam indriye nee nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bayam indriye nee nee"/>
</div>
<div class="lyrico-lyrics-wrapper">kalam sendru vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalam sendru vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">naan matum uyir vazha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan matum uyir vazha"/>
</div>
<div class="lyrico-lyrics-wrapper">ninaikatha manam vendume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninaikatha manam vendume"/>
</div>
<div class="lyrico-lyrics-wrapper">naam endru ninaithale 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naam endru ninaithale "/>
</div>
<div class="lyrico-lyrics-wrapper">varum kalam varaverkume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varum kalam varaverkume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">saathi sagathi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saathi sagathi "/>
</div>
<div class="lyrico-lyrics-wrapper">neengi nadai podu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neengi nadai podu "/>
</div>
<div class="lyrico-lyrics-wrapper">saagum varaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saagum varaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">modhu thunivodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="modhu thunivodu"/>
</div>
<div class="lyrico-lyrics-wrapper">neethi thavarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neethi thavarum"/>
</div>
<div class="lyrico-lyrics-wrapper">padhai unarnthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="padhai unarnthe"/>
</div>
<div class="lyrico-lyrics-wrapper">kaalam maara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaalam maara"/>
</div>
<div class="lyrico-lyrics-wrapper">udane poraadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="udane poraadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thamizh sondhame nee nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thamizh sondhame nee nee"/>
</div>
<div class="lyrico-lyrics-wrapper">thani illai nee orr kootam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thani illai nee orr kootam"/>
</div>
<div class="lyrico-lyrics-wrapper">bayam indriye nee nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bayam indriye nee nee"/>
</div>
<div class="lyrico-lyrics-wrapper">kalam sendru vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalam sendru vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">tamil sonthame vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tamil sonthame vaa vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">uravendru naam kai korpom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uravendru naam kai korpom"/>
</div>
<div class="lyrico-lyrics-wrapper">pagai indriye naam naam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pagai indriye naam naam"/>
</div>
<div class="lyrico-lyrics-wrapper">uyir anbile serthiduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyir anbile serthiduvom"/>
</div>
</pre>
