---
title: "thangakathi song lyrics"
album: "Savarakathi"
artist: "Arrol Corelli"
lyricist: "Mysskin"
director: "G.R. Adithya"
path: "/albums/savarakathi-lyrics"
song: "Thangakathi"
image: ../../images/albumart/savarakathi.jpg
date: 2018-02-09
lang: tamil
youtubeLink: "https://www.youtube.com/embed/vVlVw7iDWGo"
type: "happy"
singers:
  - Mysskin
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ye thangakathi vellikathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye thangakathi vellikathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sembukathi irumbukathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sembukathi irumbukathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Savarakathi eedaagumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Savarakathi eedaagumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh koobakathi paasakathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh koobakathi paasakathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Veerakathi rosakathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veerakathi rosakathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Savarakathi kuththam seiyuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Savarakathi kuththam seiyuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veettukathi naatukathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veettukathi naatukathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorukathi uravukathi saathikathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorukathi uravukathi saathikathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vethakathi neethikathi aanalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vethakathi neethikathi aanalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Savarakathi polaagumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Savarakathi polaagumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh savarakathi polaagumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh savarakathi polaagumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kathi yethukku thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathi yethukku thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoppul kodi vettathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoppul kodi vettathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathi yethukku thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathi yethukku thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoppul kodi vettathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoppul kodi vettathaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aah haa aah haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aah haa aah haa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aha haa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aha haa aaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aah haa aah haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aah haa aah haa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aha haa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aha haa aaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aah haa aah haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aah haa aah haa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aha haa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aha haa aaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aah haa aah haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aah haa aah haa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aha haa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aha haa aaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maramirangi tharaivanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maramirangi tharaivanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kulithondi poruleduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulithondi poruleduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Theevatti pathampaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theevatti pathampaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naam aayutham senjoomae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam aayutham senjoomae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uluthu aruthu maram vetti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uluthu aruthu maram vetti"/>
</div>
<div class="lyrico-lyrics-wrapper">Kinaru thoondi kall udaichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kinaru thoondi kall udaichi"/>
</div>
<div class="lyrico-lyrics-wrapper">Koodu katti kathai solli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koodu katti kathai solli"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannamoodi irul maranthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannamoodi irul maranthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavoonu kandomae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavoonu kandomae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolai senju sirichoomae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolai senju sirichoomae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye thangakathi vellikathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye thangakathi vellikathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sembukathi irumbukathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sembukathi irumbukathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Savarakathi eedaagumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Savarakathi eedaagumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh koobakathi paasakathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh koobakathi paasakathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Veerakathi rosakathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veerakathi rosakathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Savarakathi kuththam seiyuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Savarakathi kuththam seiyuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Veettukathi naatukathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veettukathi naatukathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorukathi uravukathi saathikathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorukathi uravukathi saathikathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vethakathi neethikathi aanalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vethakathi neethikathi aanalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Savarakathi polaagumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Savarakathi polaagumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh savarakathi polaagumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh savarakathi polaagumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kathi yethukku thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathi yethukku thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoppul kodi vettathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoppul kodi vettathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathi yethukku thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathi yethukku thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoppul kodi vettathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoppul kodi vettathaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aah haa aah haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aah haa aah haa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aha haa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aha haa aaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aah haa aah haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aah haa aah haa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aha haa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aha haa aaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aah haa aah haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aah haa aah haa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aha haa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aha haa aaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aah haa aah haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aah haa aah haa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aha haa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aha haa aaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mannaasa ponnaasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannaasa ponnaasa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pennaasa peraasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pennaasa peraasa"/>
</div>
<div class="lyrico-lyrics-wrapper">Por senja idamellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Por senja idamellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Naam sogam vithaichoomae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam sogam vithaichoomae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanam paranthu mathithottom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam paranthu mathithottom"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadala kadanthu kodi nattom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadala kadanthu kodi nattom"/>
</div>
<div class="lyrico-lyrics-wrapper">Ariva kodanju verkkandom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ariva kodanju verkkandom"/>
</div>
<div class="lyrico-lyrics-wrapper">Yemena thorathi oodavittom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yemena thorathi oodavittom"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbathaan tholaichoomae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbathaan tholaichoomae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaalathaanmaranthommae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaalathaanmaranthommae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye thangakathi vellikathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye thangakathi vellikathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sembukathi irumbukathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sembukathi irumbukathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Savarakathi eedaagumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Savarakathi eedaagumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh koobakathi paasakathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh koobakathi paasakathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Veerakathi rosakathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veerakathi rosakathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Savarakathi kuththam seiyuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Savarakathi kuththam seiyuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veettukathi naatukathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veettukathi naatukathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorukathi uravukathi saathikathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorukathi uravukathi saathikathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vethakathi neethikathi aanalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vethakathi neethikathi aanalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Savarakathi polaagumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Savarakathi polaagumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh savarakathi polaagumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh savarakathi polaagumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kathi yethukku thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathi yethukku thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoppul kodi vettathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoppul kodi vettathaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kathi yethukku thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathi yethukku thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoppul kodi vettathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoppul kodi vettathaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kathi yethukku thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathi yethukku thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoppul kodi vettathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoppul kodi vettathaan"/>
</div>
</pre>
