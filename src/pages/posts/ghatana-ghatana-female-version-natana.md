---
title: "ghatana ghatana female version song lyrics"
album: "Natana"
artist: "Prabhu Praveen Lanka"
lyricist: "Bharati Babu Penupatruni"
director: "Bharati Babu Penupatruni"
path: "/albums/natana-lyrics"
song: "Ghatana Ghatana Female Version"
image: ../../images/albumart/natana.jpg
date: 2019-01-04
lang: telugu
youtubeLink: "https://www.youtube.com/embed/rrwKMmI9Q3k"
type: "devotional"
singers:
  - M.M. Srilekha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ghatana Ghatana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ghatana Ghatana"/>
</div>
<div class="lyrico-lyrics-wrapper">Sanghtana Brathukai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sanghtana Brathukai"/>
</div>
<div class="lyrico-lyrics-wrapper">Saage Jeevanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saage Jeevanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oka Natana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oka Natana"/>
</div>
<div class="lyrico-lyrics-wrapper">Janmam Gamanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Janmam Gamanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Gamyam Madhyana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gamyam Madhyana"/>
</div>
<div class="lyrico-lyrics-wrapper">Eeshudu Aade Ee Natana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eeshudu Aade Ee Natana"/>
</div>
<div class="lyrico-lyrics-wrapper">Puttukathone Bandhalanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puttukathone Bandhalanni"/>
</div>
<div class="lyrico-lyrics-wrapper">Erpadatame Kadhaa Oka Natana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Erpadatame Kadhaa Oka Natana"/>
</div>
<div class="lyrico-lyrics-wrapper">Perige Koddhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perige Koddhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Okko Bandham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okko Bandham"/>
</div>
<div class="lyrico-lyrics-wrapper">Verupadipoovadam Oka Natana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verupadipoovadam Oka Natana"/>
</div>
<div class="lyrico-lyrics-wrapper">Janminchadamee Oka Natana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Janminchadamee Oka Natana"/>
</div>
<div class="lyrico-lyrics-wrapper">Jagannatakam Oka Natana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jagannatakam Oka Natana"/>
</div>
<div class="lyrico-lyrics-wrapper">Janula Kosamee Jana Natana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Janula Kosamee Jana Natana"/>
</div>
<div class="lyrico-lyrics-wrapper">Jarigeedhantha Natanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jarigeedhantha Natanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ghatana Ghatana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ghatana Ghatana"/>
</div>
<div class="lyrico-lyrics-wrapper">Sanghtana Brathukai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sanghtana Brathukai"/>
</div>
<div class="lyrico-lyrics-wrapper">Saage Jeevanam Oka Natana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saage Jeevanam Oka Natana"/>
</div>
<div class="lyrico-lyrics-wrapper">Janmam Gamanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Janmam Gamanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Gamyam Madhyana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gamyam Madhyana"/>
</div>
<div class="lyrico-lyrics-wrapper">Eeshudu Aade Ee Natana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eeshudu Aade Ee Natana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Parichayalakai Parugulu Theese
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parichayalakai Parugulu Theese"/>
</div>
<div class="lyrico-lyrics-wrapper">Parishvanganam Oka Natana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parishvanganam Oka Natana"/>
</div>
<div class="lyrico-lyrics-wrapper">Paraayivaalla Palakarimpulo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paraayivaalla Palakarimpulo"/>
</div>
<div class="lyrico-lyrics-wrapper">Prathidhvaninche Dhoka Natana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prathidhvaninche Dhoka Natana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Prathi Vishayaniki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prathi Vishayaniki"/>
</div>
<div class="lyrico-lyrics-wrapper">Manalo Maname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manalo Maname"/>
</div>
<div class="lyrico-lyrics-wrapper">Panthamaadadame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panthamaadadame"/>
</div>
<div class="lyrico-lyrics-wrapper">Oka Natana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oka Natana"/>
</div>
<div class="lyrico-lyrics-wrapper">Prathi Roju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prathi Roju"/>
</div>
<div class="lyrico-lyrics-wrapper">Prathi Aduguku Adugu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prathi Aduguku Adugu"/>
</div>
<div class="lyrico-lyrics-wrapper">Prathi Spandhane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prathi Spandhane"/>
</div>
<div class="lyrico-lyrics-wrapper">Oka Natana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oka Natana"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirigala Vaanike
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirigala Vaanike"/>
</div>
<div class="lyrico-lyrics-wrapper">Natananu Nerpina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natananu Nerpina"/>
</div>
<div class="lyrico-lyrics-wrapper">Shivudi Leelale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shivudi Leelale"/>
</div>
<div class="lyrico-lyrics-wrapper">Oka Natana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oka Natana"/>
</div>
<div class="lyrico-lyrics-wrapper">Shiramuna Garvamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shiramuna Garvamu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ekkithe Chidhime
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ekkithe Chidhime"/>
</div>
<div class="lyrico-lyrics-wrapper">Shivudi Mundharaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shivudi Mundharaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Natana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Natana"/>
</div>
<div class="lyrico-lyrics-wrapper">Nindivunna Neelo Natana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nindivunna Neelo Natana"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninna Janmadhe Ee Natana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninna Janmadhe Ee Natana"/>
</div>
<div class="lyrico-lyrics-wrapper">Aathma Charithame Ee Natana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aathma Charithame Ee Natana"/>
</div>
<div class="lyrico-lyrics-wrapper">Paramathma Leenamai Ee Natana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paramathma Leenamai Ee Natana"/>
</div>
<div class="lyrico-lyrics-wrapper">Janinchi Jeevinchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Janinchi Jeevinchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Soodhinchi Sadhinche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soodhinchi Sadhinche"/>
</div>
<div class="lyrico-lyrics-wrapper">Natana Natana Natana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natana Natana Natana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gama gama garisa Risanidharisa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gama gama garisa Risanidharisa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sanidharidhama gamagama garisa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sanidharidhama gamagama garisa"/>
</div>
<div class="lyrico-lyrics-wrapper">gaga riri mama Gaga Mama mari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gaga riri mama Gaga Mama mari"/>
</div>
<div class="lyrico-lyrics-wrapper">Garinimaga gamanidhamaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Garinimaga gamanidhamaga"/>
</div>
<div class="lyrico-lyrics-wrapper">maagasa garisa sarisamapagama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maagasa garisa sarisamapagama"/>
</div>
<div class="lyrico-lyrics-wrapper">dhapadha panidha rigarisa rigamaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhapadha panidha rigarisa rigamaga"/>
</div>
<div class="lyrico-lyrics-wrapper">maridhapa padanisaa ga ri sa ridha ridha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maridhapa padanisaa ga ri sa ridha ridha"/>
</div>
<div class="lyrico-lyrics-wrapper">risariga risariga risariga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="risariga risariga risariga"/>
</div>
<div class="lyrico-lyrics-wrapper">sarigamapa sarigamapa rigamapa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sarigamapa sarigamapa rigamapa"/>
</div>
<div class="lyrico-lyrics-wrapper">mamagarisa ri garisa saridha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mamagarisa ri garisa saridha"/>
</div>
<div class="lyrico-lyrics-wrapper">risarisadha sari dhamapamaganisa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="risarisadha sari dhamapamaganisa"/>
</div>
<div class="lyrico-lyrics-wrapper">Natanaaa Natanaaa Natanaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natanaaa Natanaaa Natanaaa"/>
</div>
</pre>
