---
title: "unakenna venum sollu song lyrics"
album: "Yennai Arindhaal"
artist: "Harris Jayaraj"
lyricist: "Thamarai"
director: "Gautham Vasudev Menon"
path: "/albums/yennai-arindhaal-lyrics"
song: "Unakenna Venum Sollu"
image: ../../images/albumart/yennai-arindhaal.jpg
date: 2015-02-06
lang: tamil
youtubeLink: "https://www.youtube.com/embed/SdcAN3dobz4"
type: "Affection"
singers:
  - Benny Dayal
  - Mahathi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Unakkenna Venum Sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkenna Venum Sollu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ulagathai Kaatta Sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagathai Kaatta Sollu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pudhu idam Pudhu Megam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu idam Pudhu Megam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thedi Povome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedi Povome"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pidiththathai Vaanga Sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pidiththathai Vaanga Sollu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veruppadhai Neenga Sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veruppadhai Neenga Sollu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pudhu Vellam Pudhu Aaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu Vellam Pudhu Aaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neendhi Paarpome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neendhi Paarpome"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iruvarin Pagal Iravu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruvarin Pagal Iravu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Veyil Oru Nilavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Veyil Oru Nilavu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Therinthathu Theriyathathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Therinthathu Theriyathathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paarkka Porome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarkka Porome"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ulagennum Paramapatham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagennum Paramapatham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhunthapin Uyarvu Varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhunthapin Uyarvu Varum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninaithathu Ninaiyaathathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaithathu Ninaiyaathathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Serkka Porome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Serkka Porome"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Velli Kolusu Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Velli Kolusu Pola"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha Bhoomi Sinungum Keela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Bhoomi Sinungum Keela"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aniyaadha Vairam Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aniyaadha Vairam Pola"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Antha Vaanam Minungum Mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Vaanam Minungum Mela"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Velli Kolusu Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Velli Kolusu Pola"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha Bhoomi Sinungum Keela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Bhoomi Sinungum Keela"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aniyaadha Vairam Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aniyaadha Vairam Pola"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Antha Vaanam Minungum Mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Vaanam Minungum Mela"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanavugal Theinthathendru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavugal Theinthathendru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalangida Koodathendru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalangida Koodathendru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhinam Dhinam Iravu Vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam Dhinam Iravu Vandhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thoonga Solliyadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoonga Solliyadhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakkena Unnai Thanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkena Unnai Thanthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unakkiru Kannai Thanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkiru Kannai Thanthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athan Vazhi Enathu Kanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athan Vazhi Enathu Kanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaana Solliyathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaana Solliyathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Adam Pidiththaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Adam Pidiththaalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adangi Pogindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adangi Pogindren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Madi Meththai Mel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Madi Meththai Mel"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Madangi Kolgindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madangi Kolgindren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thana Thananathara Namtham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana Thananathara Namtham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thana Thananathara Namtham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana Thananathara Namtham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thana Thananathara Namtham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana Thananathara Namtham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thana Thananathara Namtham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana Thananathara Namtham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unakkenna Venum Sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkenna Venum Sollu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ulagathai Kaatta Sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagathai Kaatta Sollu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pudhu idam Pudhu Megam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu idam Pudhu Megam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thedi Povome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedi Povome"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pidiththathai Vaanga Sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pidiththathai Vaanga Sollu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veruppadhai Neenga Sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veruppadhai Neenga Sollu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pudhu Vellam Pudhu Aaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu Vellam Pudhu Aaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neendhi Paarpome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neendhi Paarpome"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paruvangal Maari Vara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paruvangal Maari Vara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varudangal Odi Vida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varudangal Odi Vida"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Izhantha En Inimaigalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Izhantha En Inimaigalai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnil Kandene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnil Kandene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eluthidum Un Viralil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eluthidum Un Viralil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sirithidum Un Idhazhil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirithidum Un Idhazhil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadantha En Kavithaigalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadantha En Kavithaigalai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kandukondene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandukondene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhuruvangal Pol Neelum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhuruvangal Pol Neelum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idaiveli Andru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idaiveli Andru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tholgalil Un Moochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholgalil Un Moochu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ilaigirathu Indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilaigirathu Indru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thana Thananathara Namtham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana Thananathara Namtham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thana Thananathara Namtham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana Thananathara Namtham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thana Thananathara Namtham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana Thananathara Namtham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thana Thananathara Namtham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana Thananathara Namtham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unakkenna Venum Sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkenna Venum Sollu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ulagathai Kaatta Sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagathai Kaatta Sollu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pudhu idam Pudhu Megam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu idam Pudhu Megam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thedi Povome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedi Povome"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pidiththathai Vaanga Sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pidiththathai Vaanga Sollu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veruppadhai Neenga Sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veruppadhai Neenga Sollu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pudhu Vellam Pudhu Aaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu Vellam Pudhu Aaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neendhi Paarpome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neendhi Paarpome"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iruvarin Pagal Iravu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruvarin Pagal Iravu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Veyil Oru Nilavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Veyil Oru Nilavu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Therinthathu Theriyathathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Therinthathu Theriyathathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paarkka Porome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarkka Porome"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ulagennum Paramapatham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagennum Paramapatham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhunthapin Uyarvu Varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhunthapin Uyarvu Varum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninaithathu Ninaiyaathathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaithathu Ninaiyaathathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Serkka Porome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Serkka Porome"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Velli Kolusu Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Velli Kolusu Pola"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha Bhoomi Sinungum Keela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Bhoomi Sinungum Keela"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aniyaadha Vairam Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aniyaadha Vairam Pola"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Antha Vaanam Minungum Mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Vaanam Minungum Mela"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Velli Kolusu Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Velli Kolusu Pola"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha Bhoomi Sinungum Keela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Bhoomi Sinungum Keela"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aniyaadha Vairam Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aniyaadha Vairam Pola"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Antha Vaanam Minungum Mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Vaanam Minungum Mela"/>
</div>
</pre>
