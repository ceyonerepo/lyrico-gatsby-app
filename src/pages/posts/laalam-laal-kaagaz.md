---
title: "laalam laal song lyrics"
album: "Kaagaz"
artist: "Pravesh Mallick"
lyricist: "Aseem Ahmed Abbasee"
director: "Satish Kaushik"
path: "/albums/kaagaz-lyrics"
song: "Laalam Laal"
image: ../../images/albumart/kaagaz.jpg
date: 2021-01-07
lang: hindi
youtubeLink: "https://www.youtube.com/embed/9w4eHIbz5Rc"
type: "celebration"
singers:
  - Rajnigandha Shekhawat
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Meharbaan Saahebaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meharbaan Saahebaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Humare Arkestra Ka Programme
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Humare Arkestra Ka Programme"/>
</div>
<div class="lyrico-lyrics-wrapper">Karwane Ke Liye Shukla Gas Agency Ko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karwane Ke Liye Shukla Gas Agency Ko"/>
</div>
<div class="lyrico-lyrics-wrapper">Tahe Dil Se Shukriya Ada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tahe Dil Se Shukriya Ada"/>
</div>
<div class="lyrico-lyrics-wrapper">Karta Hu, Karta Hu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karta Hu, Karta Hu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesh Ae Khidmat Hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesh Ae Khidmat Hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Laalam Laal Gul Anaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laalam Laal Gul Anaar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gore Gore More Se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gore Gore More Se"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaase Ke Katore Se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaase Ke Katore Se"/>
</div>
<div class="lyrico-lyrics-wrapper">Dekho Jab Jiya Lalchaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dekho Jab Jiya Lalchaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Khidki Mandeer Chadh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khidki Mandeer Chadh"/>
</div>
<div class="lyrico-lyrics-wrapper">Naino Ke Khiladi Bade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naino Ke Khiladi Bade"/>
</div>
<div class="lyrico-lyrics-wrapper">Dore Dale Baithi Rahe Haaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dore Dale Baithi Rahe Haaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saiyaan Ji Dabbangg
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saiyaan Ji Dabbangg"/>
</div>
<div class="lyrico-lyrics-wrapper">Aise Chhed Ke Palang
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aise Chhed Ke Palang"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaise Milu Jab Bichu Bich Jaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaise Milu Jab Bichu Bich Jaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Mantar Utara Karu Aara Utara Karu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mantar Utara Karu Aara Utara Karu"/>
</div>
<div class="lyrico-lyrics-wrapper">Saiyaan Ko na koi Le Jaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saiyaan Ko na koi Le Jaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Laalam Laal Gul Anaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laalam Laal Gul Anaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Saiyaan Har Dil Ke Sardaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saiyaan Har Dil Ke Sardaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Laalam Laal Gul Anaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laalam Laal Gul Anaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Saiyaan Har Dil Ke Sardaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saiyaan Har Dil Ke Sardaar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jab Tak Lelu Na Balaiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jab Tak Lelu Na Balaiyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Hargiz Ghar Se Mat Nikalna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hargiz Ghar Se Mat Nikalna"/>
</div>
<div class="lyrico-lyrics-wrapper">Saiyaan Ho Saiyaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saiyaan Ho Saiyaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saiyaan Souteno Se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saiyaan Souteno Se"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhare Saare Up Ke Bazaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhare Saare Up Ke Bazaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Saiyaan Souteno Se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saiyaan Souteno Se"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhare Saare Up Ke Bazaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhare Saare Up Ke Bazaar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saiyaan Souteno Se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saiyaan Souteno Se"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhare Saare Up Ke Bazaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhare Saare Up Ke Bazaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Saiyaan Souteno Se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saiyaan Souteno Se"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhare Saare Up Ke Bazaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhare Saare Up Ke Bazaar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saiyaan Sire Ke Glass
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saiyaan Sire Ke Glass"/>
</div>
<div class="lyrico-lyrics-wrapper">Baatein Karte First Class
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baatein Karte First Class"/>
</div>
<div class="lyrico-lyrics-wrapper">Baatein Karte First Class
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baatein Karte First Class"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saiyaan Sire Ke Glass
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saiyaan Sire Ke Glass"/>
</div>
<div class="lyrico-lyrics-wrapper">Baatein Karte First Class
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baatein Karte First Class"/>
</div>
<div class="lyrico-lyrics-wrapper">Saiyaan Ji Ke Har Lafz Mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saiyaan Ji Ke Har Lafz Mein"/>
</div>
<div class="lyrico-lyrics-wrapper">Bas Hari Ki Mithaas
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bas Hari Ki Mithaas"/>
</div>
<div class="lyrico-lyrics-wrapper">Bas Hari Ki Mithaas
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bas Hari Ki Mithaas"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saiyaan Shayar Lachedaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saiyaan Shayar Lachedaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Kurte Mein Lagte Gulzaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurte Mein Lagte Gulzaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Saiyaan Shayar Lachedaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saiyaan Shayar Lachedaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Kurte Mein Lagte Gulzaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurte Mein Lagte Gulzaar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jab Tak Lelu Na Balaiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jab Tak Lelu Na Balaiyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Hargiz Ghar Se Mat Nikalna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hargiz Ghar Se Mat Nikalna"/>
</div>
<div class="lyrico-lyrics-wrapper">Saiyaan Ho Saiyaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saiyaan Ho Saiyaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saiyaan Souteno Se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saiyaan Souteno Se"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhare Saare Up Ke Bazaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhare Saare Up Ke Bazaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Saiyaan Souteno Se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saiyaan Souteno Se"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhare Saare Up Ke Bazaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhare Saare Up Ke Bazaar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saiyaan Souteno Se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saiyaan Souteno Se"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhare Saare Up Ke Bazaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhare Saare Up Ke Bazaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Saiyaan Souteno Se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saiyaan Souteno Se"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhare Saare Up Ke Bazaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhare Saare Up Ke Bazaar"/>
</div>
</pre>
