---
title: "sendumalli song lyrics"
album: "Jai Bhim"
artist: "Sean Roldan"
lyricist: "Yugabharathi"
director: "T.J. Gnanavel"
path: "/albums/jai-bhim-lyrics"
song: "Sendumalli"
image: ../../images/albumart/jai-bhim.jpg
date: 2021-11-02
lang: tamil
youtubeLink: "https://www.youtube.com/embed/74fMMQR4puE"
type: "happy"
singers:
  - Ananthu
  - Kalyani Nair
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Sendu malliya manasula manakkura nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sendu malliya manasula manakkura nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Adikarumba usurula inikura nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adikarumba usurula inikura nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Sendu malliya manasula manakkura nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sendu malliya manasula manakkura nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Adikarumba usurula inikura nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adikarumba usurula inikura nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theruvula nooru poo kada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theruvula nooru poo kada"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaku nee vaasam serthida podhum thangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaku nee vaasam serthida podhum thangam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sendu malliya manasula manakkura nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sendu malliya manasula manakkura nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Adikarumba usurula inikura nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adikarumba usurula inikura nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanavula naanum kanda mayil thoga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavula naanum kanda mayil thoga"/>
</div>
<div class="lyrico-lyrics-wrapper">Nizhalena serndhe vandha thunaiyaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nizhalena serndhe vandha thunaiyaaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karuvelangaattu oram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuvelangaattu oram"/>
</div>
<div class="lyrico-lyrics-wrapper">Musalaga neeyum naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Musalaga neeyum naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilayaadave porandhomadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilayaadave porandhomadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Esa kaathume sugamthanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Esa kaathume sugamthanadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sendu malliya manasula manakkura nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sendu malliya manasula manakkura nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Adikarumba usurula inikura nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adikarumba usurula inikura nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iruladainja veetilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruladainja veetilum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilavoliyaa pozhangida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilavoliyaa pozhangida"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum serave kavalaai illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum serave kavalaai illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kudam kudama viyarvayil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudam kudama viyarvayil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kulikkayilum thudaithida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulikkayilum thudaithida"/>
</div>
<div class="lyrico-lyrics-wrapper">Neelum kaigalaal asadhiyilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelum kaigalaal asadhiyilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenja naan kizhicha anya nee irrupa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenja naan kizhicha anya nee irrupa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kandhal selaiyilum thangamaa jolipaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandhal selaiyilum thangamaa jolipaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adaa unai vida oru punidham edhu ulagile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adaa unai vida oru punidham edhu ulagile"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada kadavulin niram therindhidaadho kanavile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada kadavulin niram therindhidaadho kanavile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru kaambile iru thaamara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru kaambile iru thaamara"/>
</div>
<div class="lyrico-lyrics-wrapper">Konda paasamo pasumpaal nura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konda paasamo pasumpaal nura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sendu malliya manasula manakkura nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sendu malliya manasula manakkura nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Adikarumba usurula inikura nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adikarumba usurula inikura nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theruvula nooru poo kada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theruvula nooru poo kada"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaku nee vaasam serthida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaku nee vaasam serthida"/>
</div>
<div class="lyrico-lyrics-wrapper">Podhum thangam sendu malliya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhum thangam sendu malliya"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasula manakkura nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasula manakkura nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanavula kaanum kanda mayil thoga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavula kaanum kanda mayil thoga"/>
</div>
<div class="lyrico-lyrics-wrapper">Nizhalena serndhe vandha thunaiyaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nizhalena serndhe vandha thunaiyaaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karuvelangaattu oram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuvelangaattu oram"/>
</div>
<div class="lyrico-lyrics-wrapper">Musalaga neeyum naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Musalaga neeyum naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilayaadave porandhomadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilayaadave porandhomadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Esa kaathume sugamthanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Esa kaathume sugamthanadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sendu malliya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sendu malliya"/>
</div>
<div class="lyrico-lyrics-wrapper">Sendu malliya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sendu malliya"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasula nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasula nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Manakkura nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manakkura nee"/>
</div>
</pre>
