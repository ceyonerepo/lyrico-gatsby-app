---
title: 'ellu vaya pookalaye song lyrics'
album: 'Asuran'
artist: 'G.V. Prakash Kumar'
lyricist: 'Yugabharathi'
director: 'Vetrimaaran'
path: '/albums/asuran-song-lyrics'
song: 'Ellu Vaya Pookalaye'
image: ../../images/albumart/asuran.jpg
date: 2019-10-04
lang: tamil
singers: 
- Saindhavi
youtubeLink: "https://www.youtube.com/embed/t4vwdXFVKzw"
type: 'sad'
---

<pre class="lyrics-native">
    Coming Soon
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Ellu vaya pookalayae
<input type="checkbox" class="lyrico-select-lyric-line" value="Ellu vaya pookalayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yereduthum paakalayae
<input type="checkbox" class="lyrico-select-lyric-line" value="Yereduthum paakalayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aalala un sirippu koththuthaiyaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Aalala un sirippu koththuthaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Acharuntha raattinam pola suthuthaiyaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Acharuntha raattinam pola suthuthaiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kollaiyila vaazha yela
<input type="checkbox" class="lyrico-select-lyric-line" value="Kollaiyila vaazha yela"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottadiyil kozhi kunju
<input type="checkbox" class="lyrico-select-lyric-line" value="Kottadiyil kozhi kunju"/>
</div>
<div class="lyrico-lyrics-wrapper">Aththanaiyum un mugatha solluthaiyaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Aththanaiyum un mugatha solluthaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadum maadum verum vaaya melluthaiya
<input type="checkbox" class="lyrico-select-lyric-line" value="Aadum maadum verum vaaya melluthaiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kaathoda un vaasam
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaathoda un vaasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadellaam un paasam
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaadellaam un paasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oothaattam on nenappae ooruthaiyaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Oothaattam on nenappae ooruthaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Salsaappu venam vandhu nillaiyaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Salsaappu venam vandhu nillaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Saavaiyum kooru pottu kollaiyaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Saavaiyum kooru pottu kollaiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kallaaga ninnaaiyo
<input type="checkbox" class="lyrico-select-lyric-line" value="Kallaaga ninnaaiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaal nooga ninnaaiyo
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaal nooga ninnaaiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannae nee thirumbi varanum
<input type="checkbox" class="lyrico-select-lyric-line" value="Kannae nee thirumbi varanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Veettukku
<input type="checkbox" class="lyrico-select-lyric-line" value="Veettukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Mallandhu ponaalum
<input type="checkbox" class="lyrico-select-lyric-line" value="Mallandhu ponaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannodu saanjaalum
<input type="checkbox" class="lyrico-select-lyric-line" value="Mannodu saanjaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aiyaa nee perumai saadhi
<input type="checkbox" class="lyrico-select-lyric-line" value="Aiyaa nee perumai saadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sanaththukku
<input type="checkbox" class="lyrico-select-lyric-line" value="Sanaththukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thalachan pullai illaama
<input type="checkbox" class="lyrico-select-lyric-line" value="Thalachan pullai illaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarinjathu yethena aatchi
<input type="checkbox" class="lyrico-select-lyric-line" value="Sarinjathu yethena aatchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyae enga raasa vaa vaa kalaththikku
<input type="checkbox" class="lyrico-select-lyric-line" value="Neeyae enga raasa vaa vaa kalaththikku"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaaiyoda baaram maasam paththaiyaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Thaaiyoda baaram maasam paththaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaangaama neeyum ponaa thappaiyaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Thaangaama neeyum ponaa thappaiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ellu vaya pookalayae
<input type="checkbox" class="lyrico-select-lyric-line" value="Ellu vaya pookalayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yereduthum paakalayae
<input type="checkbox" class="lyrico-select-lyric-line" value="Yereduthum paakalayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aalala un sirippu koththuthaiyaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Aalala un sirippu koththuthaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Acharuntha raattinam pola suthuthaiyaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Acharuntha raattinam pola suthuthaiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vaal yendhi vanthaalum
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaal yendhi vanthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhaama seththaalum
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaazhaama seththaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Gambeeram koranjidatha
<input type="checkbox" class="lyrico-select-lyric-line" value="Gambeeram koranjidatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Neruppu nee
<input type="checkbox" class="lyrico-select-lyric-line" value="Neruppu nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Aiyonnu ponaalum
<input type="checkbox" class="lyrico-select-lyric-line" value="Aiyonnu ponaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aagasam ponaalum
<input type="checkbox" class="lyrico-select-lyric-line" value="Aagasam ponaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanneera kolaththil serkkum
<input type="checkbox" class="lyrico-select-lyric-line" value="Thanneera kolaththil serkkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Varappu nee
<input type="checkbox" class="lyrico-select-lyric-line" value="Varappu nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Uzhaikka ennura aala
<input type="checkbox" class="lyrico-select-lyric-line" value="Uzhaikka ennura aala"/>
</div>
<div class="lyrico-lyrics-wrapper">Udhaichi thallura oora
<input type="checkbox" class="lyrico-select-lyric-line" value="Udhaichi thallura oora"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiya kaala vetti veesum
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaiya kaala vetti veesum"/>
</div>
<div class="lyrico-lyrics-wrapper">Karuppu nee
<input type="checkbox" class="lyrico-select-lyric-line" value="Karuppu nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Katteri unnai kanda odaatho
<input type="checkbox" class="lyrico-select-lyric-line" value="Katteri unnai kanda odaatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaapaatha deivam vandhu seraadho
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaapaatha deivam vandhu seraadho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ellu vaya pookalayae
<input type="checkbox" class="lyrico-select-lyric-line" value="Ellu vaya pookalayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yereduthum paakalayae
<input type="checkbox" class="lyrico-select-lyric-line" value="Yereduthum paakalayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aalala un sirippu koththuthaiyaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Aalala un sirippu koththuthaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Acharuntha raattinam pola suthuthaiyaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Acharuntha raattinam pola suthuthaiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kollaiyila vaazha yela
<input type="checkbox" class="lyrico-select-lyric-line" value="Kollaiyila vaazha yela"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottadiyil kozhi kunju
<input type="checkbox" class="lyrico-select-lyric-line" value="Kottadiyil kozhi kunju"/>
</div>
<div class="lyrico-lyrics-wrapper">Aththanaiyum un mugatha solluthaiyaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Aththanaiyum un mugatha solluthaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadum maadum verum vaaya melluthaiya
<input type="checkbox" class="lyrico-select-lyric-line" value="Aadum maadum verum vaaya melluthaiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kaathoda un vaasam
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaathoda un vaasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadellaam un paasam
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaadellaam un paasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oothaattam on nenappae ooruthaiyaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Oothaattam on nenappae ooruthaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Salsaappu venam vandhu nillaiyaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Salsaappu venam vandhu nillaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Saavaiyum kooru pottu kollaiyaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Saavaiyum kooru pottu kollaiyaa"/>
</div>
</pre>