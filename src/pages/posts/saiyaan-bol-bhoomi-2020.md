---
title: "saiyaan bol song lyrics"
album: "Bhoomi 2020"
artist: "Salim Sulaiman"
lyricist: "Ustad Bade Ghulam Ali Khan Sahab"
director: "Shakti Hasija"
path: "/albums/bhoomi-2020-lyrics"
song: "Saiyaan Bol"
image: ../../images/albumart/bhoomi-2020.jpg
date: 2020-12-30
lang: hindi
youtubeLink: "https://www.youtube.com/embed/jNa-0P2uzU8"
type: "melody"
singers:
  - Kaushiki Chakraborty
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Haan..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haan.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saiyaan bol, tanik mose
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saiyaan bol, tanik mose"/>
</div>
<div class="lyrico-lyrics-wrapper">Rahiyo na jaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rahiyo na jaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Saiyaan bol, tanik mose
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saiyaan bol, tanik mose"/>
</div>
<div class="lyrico-lyrics-wrapper">Rahiyo na jaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rahiyo na jaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Saiyaan bol..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saiyaan bol.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saiyaan bol, tanik mose
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saiyaan bol, tanik mose"/>
</div>
<div class="lyrico-lyrics-wrapper">Rahiyo na jaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rahiyo na jaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Saiyaan bol, tanik mose
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saiyaan bol, tanik mose"/>
</div>
<div class="lyrico-lyrics-wrapper">Rahiyo na jaaye re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rahiyo na jaaye re"/>
</div>
<div class="lyrico-lyrics-wrapper">Saiyaan bol..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saiyaan bol.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kar has ras batiyaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kar has ras batiyaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Laago mori chhatiyaan re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laago mori chhatiyaan re"/>
</div>
<div class="lyrico-lyrics-wrapper">Kar has ras batiyaan re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kar has ras batiyaan re"/>
</div>
<div class="lyrico-lyrics-wrapper">Laago mori chhatiyaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laago mori chhatiyaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Mil prem ke ras gholo re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mil prem ke ras gholo re"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saiyaan bol, tanik mose
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saiyaan bol, tanik mose"/>
</div>
<div class="lyrico-lyrics-wrapper">Rahiyo na jaaye re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rahiyo na jaaye re"/>
</div>
<div class="lyrico-lyrics-wrapper">Saiyaan bol..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saiyaan bol.."/>
</div>
<div class="lyrico-lyrics-wrapper">Saiyaan bol haaye re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saiyaan bol haaye re"/>
</div>
<div class="lyrico-lyrics-wrapper">Saiyaan bol saiyaan bol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saiyaan bol saiyaan bol"/>
</div>
<div class="lyrico-lyrics-wrapper">Saiyaan bol saiyaan bol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saiyaan bol saiyaan bol"/>
</div>
<div class="lyrico-lyrics-wrapper">Saiyaan bol, tanik mose
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saiyaan bol, tanik mose"/>
</div>
<div class="lyrico-lyrics-wrapper">Rahiyo na jaaye,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rahiyo na jaaye,"/>
</div>
<div class="lyrico-lyrics-wrapper">Saiyaan bol, tanik mose
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saiyaan bol, tanik mose"/>
</div>
<div class="lyrico-lyrics-wrapper">Rahiyo na jaaye,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rahiyo na jaaye,"/>
</div>
<div class="lyrico-lyrics-wrapper">Saiyaan bol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saiyaan bol"/>
</div>
</pre>
