---
title: "thanga sela song lyrics"
album: "Kaala"
artist: "Santhosh Narayanan"
lyricist: "Arunraja Kamaraj"
director: "Pa. Ranjith"
path: "/albums/kaala-lyrics"
song: "Semma Weightu"
image: ../../images/albumart/kaala.jpg
date: 2018-06-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/mfC-E6oku9s"
type: "happy"
singers:
  - Shankar Mahadevan
  - Pradeep Kumar
  - Ananthu
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vaadi en thanga sela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadi en thanga sela"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee illaatti naan onnumilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee illaatti naan onnumilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Ahaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaadi en thanga sela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadi en thanga sela"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee illaatti naan onnumilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee illaatti naan onnumilla"/>
</div>
<div class="lyrico-lyrics-wrapper">En jodiya nee nikkaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En jodiya nee nikkaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Verenna venum vaazhkkaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verenna venum vaazhkkaiyila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oththala raavanan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oththala raavanan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pachappulla aavuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pachappulla aavuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Kakkathula thokkikka variyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kakkathula thokkikka variyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattaakkathi veesunen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattaakkathi veesunen"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattampoochi aakkina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattampoochi aakkina"/>
</div>
<div class="lyrico-lyrics-wrapper">Muttakkanni mayakkuna sariyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muttakkanni mayakkuna sariyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thillattaangu taangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thillattaangu taangu"/>
</div>
<div class="lyrico-lyrics-wrapper">Summa thiruppi pottu vaangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summa thiruppi pottu vaangu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thillattaangu taangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thillattaangu taangu"/>
</div>
<div class="lyrico-lyrics-wrapper">Summa thiruppi pottu vaangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summa thiruppi pottu vaangu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Re sa saa saa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Re sa saa saa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaadi en thanga sela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadi en thanga sela"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee illaatti naan onnumilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee illaatti naan onnumilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Re sa saa saa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Re sa saa saa"/>
</div>
<div class="lyrico-lyrics-wrapper">En jodiya nee nikkaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En jodiya nee nikkaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Verenna venum vaazhkkaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verenna venum vaazhkkaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Re sa ne saa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Re sa ne saa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nethi pottu mathiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nethi pottu mathiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna thottu vachavalae nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna thottu vachavalae nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Manja poosi munna vandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manja poosi munna vandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannu koosumadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu koosumadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pettaikkulla polladhavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pettaikkulla polladhavan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aei"/>
</div>
<div class="lyrico-lyrics-wrapper">Pettaikkulla polladhavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pettaikkulla polladhavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee potta kotta thandadhavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee potta kotta thandadhavan"/>
</div>
<div class="lyrico-lyrics-wrapper">En veerathellaam mottaiyaa katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En veerathellaam mottaiyaa katti"/>
</div>
<div class="lyrico-lyrics-wrapper">Un pinnadi thalladi vandhenadi oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un pinnadi thalladi vandhenadi oh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thannannannaanae naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannannannaanae naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana nannaanaenaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana nannaanaenaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Than nana nana naa naa nana nana naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Than nana nana naa naa nana nana naa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sogathellaam mootta katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sogathellaam mootta katti"/>
</div>
<div class="lyrico-lyrics-wrapper">Kondada pondaatti vandhaayadi oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondada pondaatti vandhaayadi oh"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaadi en
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadi en"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanga sela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanga sela"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaadi en thanga sela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadi en thanga sela"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee illaatti naan onnumilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee illaatti naan onnumilla"/>
</div>
<div class="lyrico-lyrics-wrapper">En jodiya nee nikkaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En jodiya nee nikkaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Verenna venum vaazhkkaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verenna venum vaazhkkaiyila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thillattaangu taangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thillattaangu taangu"/>
</div>
<div class="lyrico-lyrics-wrapper">Summa thiruppi pottu vaangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summa thiruppi pottu vaangu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannannannaanae naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannannannaanae naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thillattaangu taangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thillattaangu taangu"/>
</div>
<div class="lyrico-lyrics-wrapper">Summa thiruppi pottu vaangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summa thiruppi pottu vaangu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannannannaanae naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannannannaanae naa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thiruppi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiruppi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiruppi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiruppi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiruppi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiruppi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anba kotta natpu undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anba kotta natpu undu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paasam kotta sondham undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paasam kotta sondham undu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada ratha bandham yethumilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada ratha bandham yethumilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorae sondhamada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorae sondhamada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Settaiyellam seiyadhavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Settaiyellam seiyadhavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Settaiyellam seiyadhavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Settaiyellam seiyadhavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pala vettaikellaam sikkadhavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala vettaikellaam sikkadhavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee veettaiyellaam aalura azhagula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee veettaiyellaam aalura azhagula"/>
</div>
<div class="lyrico-lyrics-wrapper">Pennae naan thindaadi ponenadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pennae naan thindaadi ponenadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thannannannaanae naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannannannaanae naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannaananae naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannaananae naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannaananae naa thana naanaa naanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannaananae naa thana naanaa naanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aei"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottaiyellaam aalura vayasula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottaiyellaam aalura vayasula"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannae un kanjaadai podhumadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannae un kanjaadai podhumadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaadi en thanga sela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadi en thanga sela"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee illaatti naan onnumilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee illaatti naan onnumilla"/>
</div>
<div class="lyrico-lyrics-wrapper">En jodiya nee nikkaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En jodiya nee nikkaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Verenna venum vaazhkkaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verenna venum vaazhkkaiyila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oththala raavanan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oththala raavanan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pachappulla aavuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pachappulla aavuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Kakkathula thokkikka variyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kakkathula thokkikka variyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattaakkathi veesunen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattaakkathi veesunen"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattampoochi aakkina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattampoochi aakkina"/>
</div>
<div class="lyrico-lyrics-wrapper">Muttakkanni mayakkuna sariyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muttakkanni mayakkuna sariyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thotta parakkum dhoolu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thotta parakkum dhoolu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannu patta kalakkum paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu patta kalakkum paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thillattaangu taangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thillattaangu taangu"/>
</div>
<div class="lyrico-lyrics-wrapper">Summa thiruppi pottu vaangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summa thiruppi pottu vaangu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thillattaangu taangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thillattaangu taangu"/>
</div>
<div class="lyrico-lyrics-wrapper">Summa thiruppi pottu vaangu hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summa thiruppi pottu vaangu hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thillattaangu taangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thillattaangu taangu"/>
</div>
<div class="lyrico-lyrics-wrapper">Summa thiruppi pottu vaangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summa thiruppi pottu vaangu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thilla thilla thilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thilla thilla thilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Thillattaangu taangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thillattaangu taangu"/>
</div>
<div class="lyrico-lyrics-wrapper">Summa thiruppi pottu vaangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summa thiruppi pottu vaangu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thillattaangu taangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thillattaangu taangu"/>
</div>
<div class="lyrico-lyrics-wrapper">Summa thiruppi pottu vaangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summa thiruppi pottu vaangu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thillattaangu taangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thillattaangu taangu"/>
</div>
<div class="lyrico-lyrics-wrapper">Summa thiruppi pottu vaangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summa thiruppi pottu vaangu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thillattaangu taangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thillattaangu taangu"/>
</div>
<div class="lyrico-lyrics-wrapper">Summa thiruppi pottu vaangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summa thiruppi pottu vaangu"/>
</div>
</pre>
