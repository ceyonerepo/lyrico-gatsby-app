---
title: "thappa song lyrics"
album: "Stand Up Ragul"
artist: "Sweekar Agasthi"
lyricist: "Raghuram"
director: "Santo"
path: "/albums/stand-up-ragul-lyrics"
song: "Thappa"
image: ../../images/albumart/stand-up-ragul.jpg
date: 2022-03-18
lang: telugu
youtubeLink: "https://www.youtube.com/embed/Bv7NbPaDq5M"
type: "happy"
singers:
  - Benny Dayal
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Maa inti gate pakkana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa inti gate pakkana"/>
</div>
<div class="lyrico-lyrics-wrapper">Evado sports car park chesthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evado sports car park chesthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Pose lu ichi selfie kosam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pose lu ichi selfie kosam"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaniki aanukuntey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaniki aanukuntey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inthalone driver vachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inthalone driver vachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Gear vesi thokkithey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gear vesi thokkithey"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa thappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa thappa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Amaayakanga rendu peglu vesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amaayakanga rendu peglu vesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sontha street baite
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sontha street baite"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinnanati dosth thoti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinnanati dosth thoti"/>
</div>
<div class="lyrico-lyrics-wrapper">Bike ride ki velthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bike ride ki velthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pattapagati poota
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattapagati poota"/>
</div>
<div class="lyrico-lyrics-wrapper">Brunken driver 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Brunken driver "/>
</div>
<div class="lyrico-lyrics-wrapper">petti dhorikipothe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="petti dhorikipothe"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vacation eh vachesina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vacation eh vachesina"/>
</div>
<div class="lyrico-lyrics-wrapper">Location eh change avvani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Location eh change avvani"/>
</div>
<div class="lyrico-lyrics-wrapper">Position eh shapinchene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Position eh shapinchene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ayyayo yemiti zindagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyayo yemiti zindagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Destiny thappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Destiny thappa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jolly ga batch thoti 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jolly ga batch thoti "/>
</div>
<div class="lyrico-lyrics-wrapper">meda meedha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meda meedha"/>
</div>
<div class="lyrico-lyrics-wrapper">Party plan chesthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Party plan chesthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Dj petti masth masth 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dj petti masth masth "/>
</div>
<div class="lyrico-lyrics-wrapper">swing lona untey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="swing lona untey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vela kaani vela lona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vela kaani vela lona"/>
</div>
<div class="lyrico-lyrics-wrapper">Varsham vachi vekkiristhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varsham vachi vekkiristhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Online lo offer edho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Online lo offer edho"/>
</div>
<div class="lyrico-lyrics-wrapper">Chusi nenu tempt ayipoyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chusi nenu tempt ayipoyi"/>
</div>
<div class="lyrico-lyrics-wrapper">Best price thoti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Best price thoti"/>
</div>
<div class="lyrico-lyrics-wrapper">Kotha smartphone konte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kotha smartphone konte"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Just a week gap lona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Just a week gap lona"/>
</div>
<div class="lyrico-lyrics-wrapper">Next model vasthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Next model vasthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadhi thappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadhi thappa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pilla nakhu nacchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pilla nakhu nacchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Love you cheppabothe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love you cheppabothe"/>
</div>
<div class="lyrico-lyrics-wrapper">Best friend eh vachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Best friend eh vachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vadi girlfriend ante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadi girlfriend ante"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappa Thappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappa Thappa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Cellphone ninchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cellphone ninchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Paytm chesthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paytm chesthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Bill katte lopu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bill katte lopu"/>
</div>
<div class="lyrico-lyrics-wrapper">Switch off aipothe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Switch off aipothe"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappa? Naa thappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappa? Naa thappa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rent kattaleka pent house vadili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rent kattaleka pent house vadili"/>
</div>
<div class="lyrico-lyrics-wrapper">Tent vesukunte naa thappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tent vesukunte naa thappa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Janta antuleka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Janta antuleka"/>
</div>
<div class="lyrico-lyrics-wrapper">Mental ekkipoyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mental ekkipoyi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanta neeru vasthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanta neeru vasthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa thappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa thappa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Promotion eh kottesina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Promotion eh kottesina"/>
</div>
<div class="lyrico-lyrics-wrapper">Emotion eh change avvani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emotion eh change avvani"/>
</div>
<div class="lyrico-lyrics-wrapper">Condition eh vachesene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Condition eh vachesene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ayyayo yemti zindagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyayo yemti zindagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Tragedy thappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tragedy thappa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Zoom call lona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zoom call lona"/>
</div>
<div class="lyrico-lyrics-wrapper">Face kastha chupi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Face kastha chupi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nachinante naku kindha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nachinante naku kindha"/>
</div>
<div class="lyrico-lyrics-wrapper">Knicker esthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Knicker esthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa thappa Thappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa thappa Thappa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Reviewlu anni choosi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Reviewlu anni choosi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ram pam ra ra bi ri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ram pam ra ra bi ri"/>
</div>
<div class="lyrico-lyrics-wrapper">Cinema ki velthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cinema ki velthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Rating unna kani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rating unna kani"/>
</div>
<div class="lyrico-lyrics-wrapper">Rotta laga unte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rotta laga unte"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappa Naa thappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappa Naa thappa"/>
</div>
</pre>
