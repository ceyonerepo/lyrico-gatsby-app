---
title: "vaan thodave song lyrics"
album: "Boomerang"
artist: "Radhan"
lyricist: "Radhan"
director: "R. Kannan"
path: "/albums/boomerang-lyrics"
song: "Vaan Thodave"
image: ../../images/albumart/boomerang.jpg
date: 2019-03-08
lang: tamil
youtubeLink: "https://www.youtube.com/embed/K6eCERHOHeQ"
type: "sad"
singers:
  - Bobo Shashi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nam Uyir Vaaniley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam Uyir Vaaniley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nilavaai Mugam Theyumey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilavaai Mugam Theyumey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Mugam Thediye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Mugam Thediye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pudhidhaai Uyir Pookumey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhidhaai Uyir Pookumey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhundhaal Vizhundhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhundhaal Vizhundhaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mazhai Pol Vizhavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai Pol Vizhavey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vithai Pol Nee Elundhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vithai Pol Nee Elundhaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nimirndhaai Vaan Thodavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nimirndhaai Vaan Thodavey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theeyiley Pookumey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeyiley Pookumey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naalayin Oliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalayin Oliye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irul Mugam Thirappadhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irul Mugam Thirappadhaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidiyumey Vidiyumey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidiyumey Vidiyumey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyirin Uthiram Valithaaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirin Uthiram Valithaaley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyir Kangal Thirandhidumey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Kangal Thirandhidumey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nizhalin Poo Mugam Unarndhaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nizhalin Poo Mugam Unarndhaley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pudhu Nijangal Pirandhidumey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu Nijangal Pirandhidumey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhundhaal Vizhundhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhundhaal Vizhundhaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mazhai Pol Vizhavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai Pol Vizhavey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vithai Pol Nee Elundhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vithai Pol Nee Elundhaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nimirndhaai Vaan Thodavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nimirndhaai Vaan Thodavae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theeyiley Pookkumey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeyiley Pookkumey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naalayin Ozhiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalayin Ozhiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irul Mugam Thirappathaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irul Mugam Thirappathaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidiyumey Vidiyumey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidiyumey Vidiyumey"/>
</div>
</pre>
