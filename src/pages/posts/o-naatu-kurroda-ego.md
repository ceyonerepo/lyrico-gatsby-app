---
title: "o naatu kurroda song lyrics"
album: "Ego"
artist: "Sai Karthik "
lyricist: "Kasarla Shyam Kasarla"
director: "RV Subramanyam"
path: "/albums/ego-lyrics"
song: "O Naatu Kurroda"
image: ../../images/albumart/ego.jpg
date: 2018-01-05
lang: telugu
youtubeLink: "https://www.youtube.com/embed/4yGH9fFlUj0"
type: "happy"
singers:
  - Aparna Nandan
  - Prativa
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">chuttu thugo paago lo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chuttu thugo paago lo"/>
</div>
<div class="lyrico-lyrics-wrapper">firstu nuvve ego lo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="firstu nuvve ego lo"/>
</div>
<div class="lyrico-lyrics-wrapper">andra seeded nijam lo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="andra seeded nijam lo"/>
</div>
<div class="lyrico-lyrics-wrapper">lene leru nee style lo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lene leru nee style lo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">andagaada 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="andagaada "/>
</div>
<div class="lyrico-lyrics-wrapper">telisindi thoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="telisindi thoda"/>
</div>
<div class="lyrico-lyrics-wrapper">sandekaada 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sandekaada "/>
</div>
<div class="lyrico-lyrics-wrapper">nee suruku thaki 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee suruku thaki "/>
</div>
<div class="lyrico-lyrics-wrapper">silaka olley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="silaka olley"/>
</div>
<div class="lyrico-lyrics-wrapper">uduketthela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uduketthela"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">o naatu kurroda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="o naatu kurroda"/>
</div>
<div class="lyrico-lyrics-wrapper">nee vete kochhara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee vete kochhara"/>
</div>
<div class="lyrico-lyrics-wrapper">naa kota peta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa kota peta"/>
</div>
<div class="lyrico-lyrics-wrapper">idisi kobbari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idisi kobbari"/>
</div>
<div class="lyrico-lyrics-wrapper">thota kochhara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thota kochhara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">e naatu kurrode
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="e naatu kurrode"/>
</div>
<div class="lyrico-lyrics-wrapper">ninnu touch
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninnu touch"/>
</div>
<div class="lyrico-lyrics-wrapper">cheyyamantade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cheyyamantade"/>
</div>
<div class="lyrico-lyrics-wrapper">kaani ego 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaani ego "/>
</div>
<div class="lyrico-lyrics-wrapper">touchey chsavante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="touchey chsavante"/>
</div>
<div class="lyrico-lyrics-wrapper">thata theesthade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thata theesthade"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">jhalaku lichhe soku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jhalaku lichhe soku"/>
</div>
<div class="lyrico-lyrics-wrapper">ne mumbai poothareku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ne mumbai poothareku"/>
</div>
<div class="lyrico-lyrics-wrapper">o silk shokka chaaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="o silk shokka chaaku"/>
</div>
<div class="lyrico-lyrics-wrapper">nee pogaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee pogaru"/>
</div>
<div class="lyrico-lyrics-wrapper">nachhe naaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nachhe naaku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">chuttey chuttey chuttey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chuttey chuttey chuttey"/>
</div>
<div class="lyrico-lyrics-wrapper">nadum thamalapaakula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nadum thamalapaakula"/>
</div>
<div class="lyrico-lyrics-wrapper">sega pettey pattey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sega pettey pattey "/>
</div>
<div class="lyrico-lyrics-wrapper">pattey udum pattulakula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pattey udum pattulakula"/>
</div>
<div class="lyrico-lyrics-wrapper">ooruvaada 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooruvaada "/>
</div>
<div class="lyrico-lyrics-wrapper">nenate dhada dhada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenate dhada dhada"/>
</div>
<div class="lyrico-lyrics-wrapper">aada eeda 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aada eeda "/>
</div>
<div class="lyrico-lyrics-wrapper">nee choopulatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee choopulatho"/>
</div>
<div class="lyrico-lyrics-wrapper">oopulake merugethela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oopulake merugethela"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">o naatu kurroda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="o naatu kurroda"/>
</div>
<div class="lyrico-lyrics-wrapper">nee vete kochhara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee vete kochhara"/>
</div>
<div class="lyrico-lyrics-wrapper">naa kota peta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa kota peta"/>
</div>
<div class="lyrico-lyrics-wrapper">idisi kobbari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idisi kobbari"/>
</div>
<div class="lyrico-lyrics-wrapper">thota kochhara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thota kochhara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">e naatu kurrode
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="e naatu kurrode"/>
</div>
<div class="lyrico-lyrics-wrapper">ninnu touch
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninnu touch"/>
</div>
<div class="lyrico-lyrics-wrapper">cheyyamantade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cheyyamantade"/>
</div>
<div class="lyrico-lyrics-wrapper">kaani ego 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaani ego "/>
</div>
<div class="lyrico-lyrics-wrapper">touchey chsavante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="touchey chsavante"/>
</div>
<div class="lyrico-lyrics-wrapper">thata theesthade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thata theesthade"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kulukulanni thajaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulukulanni thajaa"/>
</div>
<div class="lyrico-lyrics-wrapper">idi city madatha khaaja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idi city madatha khaaja"/>
</div>
<div class="lyrico-lyrics-wrapper">saruku motham lejaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saruku motham lejaa"/>
</div>
<div class="lyrico-lyrics-wrapper">nuvvochhey bullet raaja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvvochhey bullet raaja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nanne nanne 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanne nanne "/>
</div>
<div class="lyrico-lyrics-wrapper">nanne laage 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanne laage "/>
</div>
<div class="lyrico-lyrics-wrapper">jinnu rammula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jinnu rammula"/>
</div>
<div class="lyrico-lyrics-wrapper">thega unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thega unna"/>
</div>
<div class="lyrico-lyrics-wrapper">vannelanni vaade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vannelanni vaade"/>
</div>
<div class="lyrico-lyrics-wrapper">jio sim laa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jio sim laa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">muddulaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muddulaada"/>
</div>
<div class="lyrico-lyrics-wrapper">ente nee ragada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ente nee ragada"/>
</div>
<div class="lyrico-lyrics-wrapper">chandoorooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chandoorooda"/>
</div>
<div class="lyrico-lyrics-wrapper">hey jamaku jama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey jamaku jama"/>
</div>
<div class="lyrico-lyrics-wrapper">jamaayisthe thellarala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jamaayisthe thellarala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">o naatu kurroda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="o naatu kurroda"/>
</div>
<div class="lyrico-lyrics-wrapper">nee vete kochhara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee vete kochhara"/>
</div>
<div class="lyrico-lyrics-wrapper">naa kota peta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa kota peta"/>
</div>
<div class="lyrico-lyrics-wrapper">idisi kobbari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idisi kobbari"/>
</div>
<div class="lyrico-lyrics-wrapper">thota kochhara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thota kochhara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">e naatu kurrode
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="e naatu kurrode"/>
</div>
<div class="lyrico-lyrics-wrapper">ninnu touch
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninnu touch"/>
</div>
<div class="lyrico-lyrics-wrapper">cheyyamantade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cheyyamantade"/>
</div>
<div class="lyrico-lyrics-wrapper">kaani ego 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaani ego "/>
</div>
<div class="lyrico-lyrics-wrapper">touchey chsavante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="touchey chsavante"/>
</div>
<div class="lyrico-lyrics-wrapper">thata theesthade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thata theesthade"/>
</div>
</pre>
