---
title: "thangamey thangamey song lyrics"
album: "Paava Kadhaigal"
artist: "Justin Prabhakaran"
lyricist: "Justin Prabhakaran - Shan Karuppasamy"
director: "Sudha Kongara - Vignesh Shivan - Gautham Vasudev Menon - Vetrimaaran"
path: "/albums/paava-kadhaigal-lyrics"
song: "Thangamey Thangamey"
image: ../../images/albumart/paava-kadhaigal.jpg
date: 2020-12-18
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Oh_QZDAHvzw"
type: "love"
singers:
  - Justin Prabhakaran
  - Murugavel	
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Thumbara Thumbara Thumbara Thumba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thumbara Thumbara Thumbara Thumba"/>
</div>
<div class="lyrico-lyrics-wrapper">Thumbara Thumbara Thumba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thumbara Thumbara Thumba"/>
</div>
<div class="lyrico-lyrics-wrapper">Thumbara Thumbara Thumbara Thumba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thumbara Thumbara Thumbara Thumba"/>
</div>
<div class="lyrico-lyrics-wrapper">Thumbara Thumbara Thumba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thumbara Thumbara Thumba"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanneymavarasan Thonai Venundaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneymavarasan Thonai Venundaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu Vali Paaththu Naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu Vali Paaththu Naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam Elaichenada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Elaichenada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanneyunaithaandi Enakkaaru Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneyunaithaandi Enakkaaru Daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinnu Mogam Paakka Nooru Jenmam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinnu Mogam Paakka Nooru Jenmam"/>
</div>
<div class="lyrico-lyrics-wrapper">Porakkonundaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porakkonundaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unakkaga Karaiyonum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkaga Karaiyonum"/>
</div>
<div class="lyrico-lyrics-wrapper">Melukaaga Eriyonum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Melukaaga Eriyonum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaareney Onna Thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaareney Onna Thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasai Theera Pesi Poga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai Theera Pesi Poga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thangamey Thangamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangamey Thangamey"/>
</div>
<div class="lyrico-lyrics-wrapper">Embada Thangamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Embada Thangamey"/>
</div>
<div class="lyrico-lyrics-wrapper">Thangamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangamey"/>
</div>
<div class="lyrico-lyrics-wrapper">Thangamey Thangamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangamey Thangamey"/>
</div>
<div class="lyrico-lyrics-wrapper">Embada Thangamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Embada Thangamey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thangamey Thangamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangamey Thangamey"/>
</div>
<div class="lyrico-lyrics-wrapper">Embada Thangamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Embada Thangamey"/>
</div>
<div class="lyrico-lyrics-wrapper">Thangamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangamey"/>
</div>
<div class="lyrico-lyrics-wrapper">Thangamey Thangamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangamey Thangamey"/>
</div>
<div class="lyrico-lyrics-wrapper">Embada Thangamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Embada Thangamey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanneymavarasan Thonai Venundaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneymavarasan Thonai Venundaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu Vali Paaththu Naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu Vali Paaththu Naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam Elaichenada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Elaichenada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanneyunaithaandi Enakkaaru Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneyunaithaandi Enakkaaru Daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinnu Mogam Paakka Nooru Jenmam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinnu Mogam Paakka Nooru Jenmam"/>
</div>
<div class="lyrico-lyrics-wrapper">Porakkonundaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porakkonundaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Natta Nadu Raathiriyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natta Nadu Raathiriyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Vethalaiya Pottukittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vethalaiya Pottukittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Ninaippa Melluraney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Ninaippa Melluraney"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Sugamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Sugamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Korai En Porappil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Korai En Porappil"/>
</div>
<div class="lyrico-lyrics-wrapper">Aththanaikkum Naan Poruppaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aththanaikkum Naan Poruppaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Vitta Enakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Vitta Enakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar Irukkaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar Irukkaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Onna Suthi Vaarenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna Suthi Vaarenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Onna Suthi Vaaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna Suthi Vaaren"/>
</div>
<div class="lyrico-lyrics-wrapper">Moochiraikka Naaum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moochiraikka Naaum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannaaloru Saadai Pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaaloru Saadai Pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">Usura Katti Koththaaga Thaaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usura Katti Koththaaga Thaaren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thangamey Thangamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangamey Thangamey"/>
</div>
<div class="lyrico-lyrics-wrapper">Embada Thangamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Embada Thangamey"/>
</div>
<div class="lyrico-lyrics-wrapper">Thangamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangamey"/>
</div>
<div class="lyrico-lyrics-wrapper">Thangamey Thangamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangamey Thangamey"/>
</div>
<div class="lyrico-lyrics-wrapper">Embada Thangamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Embada Thangamey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thangamey Thangamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangamey Thangamey"/>
</div>
<div class="lyrico-lyrics-wrapper">Embada Thangamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Embada Thangamey"/>
</div>
<div class="lyrico-lyrics-wrapper">Thangamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangamey"/>
</div>
<div class="lyrico-lyrics-wrapper">Thangamey Thangamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangamey Thangamey"/>
</div>
<div class="lyrico-lyrics-wrapper">Embada Thangamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Embada Thangamey"/>
</div>
</pre>
