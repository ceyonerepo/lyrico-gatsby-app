---
title: 'yela yelo song lyrics'
album: 'Mandela'
artist: 'Bharath Sankar'
lyricist: 'Arivu'
director: 'Madonne Ashwin'
path: '/albums/mandela-song-lyrics'
song: 'Yela Yelo'
image: ../../images/albumart/mandela.jpg
date: 2021-04-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Ip_A-v677cQ"
type: 'Love'

singers:
- Arivu
---

<pre class="lyrics-native">
  
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yela Yelo, Yelala Yelo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yela Yelo, Yelala Yelo"/>
</div>
<div class="lyrico-lyrics-wrapper">Yela Yelo, Yelala Yelo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yela Yelo, Yelala Yelo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paerazhagan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paerazhagan"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Pera Solli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Pera Solli"/>
</div>
<div class="lyrico-lyrics-wrapper">Poguthammov
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poguthammov"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Vaadamalli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Vaadamalli"/>
</div>
<div class="lyrico-lyrics-wrapper">Olaththatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Olaththatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Oonnjala Katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Oonnjala Katti"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoosuthatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoosuthatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudi Pugundhathammov
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudi Pugundhathammov"/>
</div>
<div class="lyrico-lyrics-wrapper">Melam Kotti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Melam Kotti"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Vethala Potti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Vethala Potti"/>
</div>
<div class="lyrico-lyrics-wrapper">Megathukke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megathukke"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanthi Adikkuthummov
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanthi Adikkuthummov"/>
</div>
<div class="lyrico-lyrics-wrapper">Savarakkathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Savarakkathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Minnukuthummov
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minnukuthummov"/>
</div>
<div class="lyrico-lyrics-wrapper">Evara Pathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evara Pathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenaikuthummov
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenaikuthummov"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaasamalli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaasamalli"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Vaarthai Solli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Vaarthai Solli"/>
</div>
<div class="lyrico-lyrics-wrapper">Vagudeduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vagudeduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mella Vaaruthammov
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mella Vaaruthammov"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathulathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathulathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Sethi Vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Sethi Vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nerthayellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nerthayellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Alli Mudinjathammov
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alli Mudinjathammov"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaattumalli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattumalli"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha Kadhaiya Solli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Kadhaiya Solli"/>
</div>
<div class="lyrico-lyrics-wrapper">Paattu Onnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paattu Onnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Inga Padikuthummov
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga Padikuthummov"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuthaala Seemaikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuthaala Seemaikku"/>
</div>
<div class="lyrico-lyrics-wrapper">Muththaada Povuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muththaada Povuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakkaana Boomikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakkaana Boomikku"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanneerum Paayudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanneerum Paayudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathaazha Poovukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathaazha Poovukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Kacheri Kooduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kacheri Kooduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mathaala Tholukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mathaala Tholukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Manjalum Seruthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manjalum Seruthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paerazhagan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paerazhagan"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Pera Solli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Pera Solli"/>
</div>
<div class="lyrico-lyrics-wrapper">Poguthammov
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poguthammov"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Ponnaanganni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Ponnaanganni"/>
</div>
<div class="lyrico-lyrics-wrapper">Aalangatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalangatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Paalam Katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Paalam Katti"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaruthalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaruthalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhu Seruthammov
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhu Seruthammov"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaayamellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaayamellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippa Aaruthammov
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippa Aaruthammov"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sithira Veyilu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sithira Veyilu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sillunnu Kaayudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sillunnu Kaayudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nethili Meenukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nethili Meenukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Nithira Povuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nithira Povuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathiri Valanju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathiri Valanju"/>
</div>
<div class="lyrico-lyrics-wrapper">Kammalum Aavathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kammalum Aavathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Koppara Vaanamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koppara Vaanamum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pothunnu Thoovuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pothunnu Thoovuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kooraiyilla Kottaikulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooraiyilla Kottaikulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Sooriyanum Sirikkuthummov
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sooriyanum Sirikkuthummov"/>
</div>
<div class="lyrico-lyrics-wrapper">Savarakathi Minukkuthummov
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Savarakathi Minukkuthummov"/>
</div>
<div class="lyrico-lyrics-wrapper">Sooriyanum Sirikkuthummov
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sooriyanum Sirikkuthummov"/>
</div>
<div class="lyrico-lyrics-wrapper">Yela Yelo, Yelala Yelo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yela Yelo, Yelala Yelo"/>
</div>
<div class="lyrico-lyrics-wrapper">Yela Yelo, Yelala Yelo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yela Yelo, Yelala Yelo"/>
</div>
</pre>