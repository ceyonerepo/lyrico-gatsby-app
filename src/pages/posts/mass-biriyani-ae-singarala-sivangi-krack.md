---
title: "mass biriyani song lyrics"
album: "Krack"
artist: "S. Thaman"
lyricist: "Kasarla Shyam"
director: "Gopichand Malineni"
path: "/albums/krack-lyrics"
song: "Mass Biriyani - Ae Singarala Sivangi"
image: ../../images/albumart/krack.jpg
date: 2021-01-09
lang: telugu
youtubeLink: "https://www.youtube.com/embed/jmHeLJmnT24"
type: "love"
singers:
  - Rahul Nambiar
  - Sahiti Chaganti
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Danda Nakra Nakara Nakara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Danda Nakra Nakara Nakara"/>
</div>
<div class="lyrico-lyrics-wrapper">Danda Nakra Nakara Nakara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Danda Nakra Nakara Nakara"/>
</div>
<div class="lyrico-lyrics-wrapper">Danda Nakra Nakara Nakara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Danda Nakra Nakara Nakara"/>
</div>
<div class="lyrico-lyrics-wrapper">Taka Taka Taka Taka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Taka Taka Taka Taka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Danda Nakra Nakara Nakara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Danda Nakra Nakara Nakara"/>
</div>
<div class="lyrico-lyrics-wrapper">Danda Nakra Nakara Nakara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Danda Nakra Nakara Nakara"/>
</div>
<div class="lyrico-lyrics-wrapper">Danda Nakra Nakara Nakara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Danda Nakra Nakara Nakara"/>
</div>
<div class="lyrico-lyrics-wrapper">Taka Taka Taka Taka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Taka Taka Taka Taka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ae Singarala Sivangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Singarala Sivangi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vayyarala Firangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayyarala Firangi"/>
</div>
<div class="lyrico-lyrics-wrapper">Konte Soopu Konangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konte Soopu Konangi"/>
</div>
<div class="lyrico-lyrics-wrapper">Paita Senge Pathangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paita Senge Pathangi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pistallage Unnade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pistallage Unnade"/>
</div>
<div class="lyrico-lyrics-wrapper">Pitta Nadumu Sampangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pitta Nadumu Sampangi"/>
</div>
<div class="lyrico-lyrics-wrapper">Bullet Daagi Unnadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bullet Daagi Unnadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Sotta Buggala Sarangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sotta Buggala Sarangi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">O Sweety Naa Duty
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Sweety Naa Duty"/>
</div>
<div class="lyrico-lyrics-wrapper">Ikapaina Intlo Neethoti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ikapaina Intlo Neethoti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Osi Na Class’u Kalyani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Osi Na Class’u Kalyani"/>
</div>
<div class="lyrico-lyrics-wrapper">Pettave Mass’u Biriyani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pettave Mass’u Biriyani"/>
</div>
<div class="lyrico-lyrics-wrapper">Biriyani Biriyani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Biriyani Biriyani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ori Naa Krack’u Maa Raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ori Naa Krack’u Maa Raja"/>
</div>
<div class="lyrico-lyrics-wrapper">Family Pack Nuvvu Leja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Family Pack Nuvvu Leja"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuv Leja Nuv Leja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuv Leja Nuv Leja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bangaramra Nee Balupu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bangaramra Nee Balupu"/>
</div>
<div class="lyrico-lyrics-wrapper">Bambula Mothey Nee Pilupu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bambula Mothey Nee Pilupu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pottelanti Nee Pogaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pottelanti Nee Pogaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Thatti Lepe Naa Figaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thatti Lepe Naa Figaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ak-47 La Dookesthara Neethodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ak-47 La Dookesthara Neethodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ekanga Ee Vantintlo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ekanga Ee Vantintlo"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheyistharaa Paredu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheyistharaa Paredu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nannu Kottu Nannu Suttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannu Kottu Nannu Suttu"/>
</div>
<div class="lyrico-lyrics-wrapper">Inninalla Aakhali Thirettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inninalla Aakhali Thirettu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Osi Na Class’u Kalyani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Osi Na Class’u Kalyani"/>
</div>
<div class="lyrico-lyrics-wrapper">Pettave Mass’u Biriyani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pettave Mass’u Biriyani"/>
</div>
<div class="lyrico-lyrics-wrapper">Biriyani Biriyani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Biriyani Biriyani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ori Naa Krack’u Maa Raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ori Naa Krack’u Maa Raja"/>
</div>
<div class="lyrico-lyrics-wrapper">Family Pack Nuvvu Leja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Family Pack Nuvvu Leja"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuv Leja Nuv Leja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuv Leja Nuv Leja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Danda Nakra Nakara Nakara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Danda Nakra Nakara Nakara"/>
</div>
<div class="lyrico-lyrics-wrapper">Danda Nakra Nakara Nakara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Danda Nakra Nakara Nakara"/>
</div>
<div class="lyrico-lyrics-wrapper">Danda Nakra Nakara Nakara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Danda Nakra Nakara Nakara"/>
</div>
<div class="lyrico-lyrics-wrapper">Taka Taka Taka Taka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Taka Taka Taka Taka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Danda Nakra Nakara Nakara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Danda Nakra Nakara Nakara"/>
</div>
<div class="lyrico-lyrics-wrapper">Danda Nakra Nakara Nakara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Danda Nakra Nakara Nakara"/>
</div>
<div class="lyrico-lyrics-wrapper">Danda Nakra Nakara Nakara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Danda Nakra Nakara Nakara"/>
</div>
<div class="lyrico-lyrics-wrapper">Taka Taka Taka Taka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Taka Taka Taka Taka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bedilu Redile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bedilu Redile"/>
</div>
<div class="lyrico-lyrics-wrapper">Jodiga Veyyale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jodiga Veyyale"/>
</div>
<div class="lyrico-lyrics-wrapper">Cherasalalundale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cherasalalundale"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheli Kougili Chale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheli Kougili Chale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Doragaru Hushaarae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Doragaru Hushaarae"/>
</div>
<div class="lyrico-lyrics-wrapper">Dorasani Thayyare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dorasani Thayyare"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chinni Gundello
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinni Gundello"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu Sirenlaa Moguthuntave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu Sirenlaa Moguthuntave"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhalo Nilichi Ila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhalo Nilichi Ila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poddu Maapullo Uniformlaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poddu Maapullo Uniformlaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Anti Untaane Neelo Odhigela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anti Untaane Neelo Odhigela"/>
</div>
<div class="lyrico-lyrics-wrapper">Andhalake Iyyala Bandobasthu Iyyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhalake Iyyala Bandobasthu Iyyala"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandamamai Tellarluu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandamamai Tellarluu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvve Gasthi Kayala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvve Gasthi Kayala"/>
</div>
<div class="lyrico-lyrics-wrapper">Nae Nacchi Ninnu Mecchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nae Nacchi Ninnu Mecchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Muddhulichhukuntaa Recchi Recchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muddhulichhukuntaa Recchi Recchi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Osi Na Class’u Kalyani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Osi Na Class’u Kalyani"/>
</div>
<div class="lyrico-lyrics-wrapper">Pettave Mass’u Biriyani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pettave Mass’u Biriyani"/>
</div>
<div class="lyrico-lyrics-wrapper">Biriyani Biriyani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Biriyani Biriyani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ori Naa Krack’u Maa Raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ori Naa Krack’u Maa Raja"/>
</div>
<div class="lyrico-lyrics-wrapper">Family Pack Nuvvu Leja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Family Pack Nuvvu Leja"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuv Leja Nuv Leja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuv Leja Nuv Leja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Danda Nakra Nakara Nakara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Danda Nakra Nakara Nakara"/>
</div>
<div class="lyrico-lyrics-wrapper">Danda Nakra Nakara Nakara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Danda Nakra Nakara Nakara"/>
</div>
<div class="lyrico-lyrics-wrapper">Danda Nakra Nakara Nakara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Danda Nakra Nakara Nakara"/>
</div>
<div class="lyrico-lyrics-wrapper">Taka Taka Taka Taka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Taka Taka Taka Taka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Danda Nakra Nakara Nakara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Danda Nakra Nakara Nakara"/>
</div>
<div class="lyrico-lyrics-wrapper">Danda Nakra Nakara Nakara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Danda Nakra Nakara Nakara"/>
</div>
<div class="lyrico-lyrics-wrapper">Danda Nakra Nakara Nakara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Danda Nakra Nakara Nakara"/>
</div>
<div class="lyrico-lyrics-wrapper">Taka Taka Taka Taka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Taka Taka Taka Taka"/>
</div>
</pre>
