---
title: "jo heelein toot gayi song lyrics"
album: "Indoo Ki Jawani"
artist: "Badshah"
lyricist: "Badshah"
director: "Abir Sengupta"
path: "/albums/indoo-ki-jawani-lyrics"
song: "Jo Heelein Toot Gayi"
image: ../../images/albumart/indoo-ki-jawani.jpg
date: 2020-12-11
lang: hindi
youtubeLink: "https://www.youtube.com/embed/ms2dXQ0gpDA"
type: "happy"
singers:
  - Badshah
  - Aastha Gill
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Chhoti chhoti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chhoti chhoti"/>
</div>
<div class="lyrico-lyrics-wrapper">Chhoti chhoti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chhoti chhoti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chhoti chhoti baaton par
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chhoti chhoti baaton par"/>
</div>
<div class="lyrico-lyrics-wrapper">Main roti nahi waise
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main roti nahi waise"/>
</div>
<div class="lyrico-lyrics-wrapper">But kal hi ki thi shopping
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="But kal hi ki thi shopping"/>
</div>
<div class="lyrico-lyrics-wrapper">Maine kharche thhe paise
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maine kharche thhe paise"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chhoti chhoti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chhoti chhoti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haan haan chhoti chhoti baaton
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haan haan chhoti chhoti baaton"/>
</div>
<div class="lyrico-lyrics-wrapper">Par main roti nahi waise
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Par main roti nahi waise"/>
</div>
<div class="lyrico-lyrics-wrapper">But kal hi ki thi shopping
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="But kal hi ki thi shopping"/>
</div>
<div class="lyrico-lyrics-wrapper">Maine kharche thhe paise
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maine kharche thhe paise"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Beat huyi jab drop
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Beat huyi jab drop"/>
</div>
<div class="lyrico-lyrics-wrapper">To baby nachi main aise
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="To baby nachi main aise"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ke heelein toot gayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ke heelein toot gayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ke heelein toot gayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ke heelein toot gayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ke heelein toot gayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ke heelein toot gayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ke heelein toot gayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ke heelein toot gayi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bas do hi kaam tere
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bas do hi kaam tere"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu jabse aayi jalandhar se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu jabse aayi jalandhar se"/>
</div>
<div class="lyrico-lyrics-wrapper">Ya to club mein ya phir
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ya to club mein ya phir"/>
</div>
<div class="lyrico-lyrics-wrapper">Nikle na tu mall ke andar se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikle na tu mall ke andar se"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho tera instagram hai famous
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho tera instagram hai famous"/>
</div>
<div class="lyrico-lyrics-wrapper">Tera swagger up to date
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tera swagger up to date"/>
</div>
<div class="lyrico-lyrics-wrapper">Baby tu karti hai chill
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby tu karti hai chill"/>
</div>
<div class="lyrico-lyrics-wrapper">Baaki kudiyan kardi hate
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baaki kudiyan kardi hate"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho baaki kudiyan ho gayi jhalli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho baaki kudiyan ho gayi jhalli"/>
</div>
<div class="lyrico-lyrics-wrapper">Tujhse sadd sadd ke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tujhse sadd sadd ke"/>
</div>
<div class="lyrico-lyrics-wrapper">Dj se apna gaana lagwati ladd ladd ke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dj se apna gaana lagwati ladd ladd ke"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho check it baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho check it baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Nach le na table pe tu chadh ke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nach le na table pe tu chadh ke"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jo heelein toot gayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jo heelein toot gayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Jo heelein toot gayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jo heelein toot gayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Jo heelein toot gayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jo heelein toot gayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Jo heelein toot gayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jo heelein toot gayi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tu phenkta lambi lambi hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu phenkta lambi lambi hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Yehi aadat teri gandi hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yehi aadat teri gandi hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ik jodi heelein dilwa de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ik jodi heelein dilwa de"/>
</div>
<div class="lyrico-lyrics-wrapper">Dikkat mein teri bandi hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dikkat mein teri bandi hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kehta tha rani bana doonga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kehta tha rani bana doonga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ik baari haan karde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ik baari haan karde"/>
</div>
<div class="lyrico-lyrics-wrapper">Le jaunga main tujhko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Le jaunga main tujhko"/>
</div>
<div class="lyrico-lyrics-wrapper">Tere ghar se bhaga kar ke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere ghar se bhaga kar ke"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ek red bottom ka joda to
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ek red bottom ka joda to"/>
</div>
<div class="lyrico-lyrics-wrapper">Dilwa do ja kar ke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dilwa do ja kar ke"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ke heelein toot gayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ke heelein toot gayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ke heelein toot gayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ke heelein toot gayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ke heelein toot gayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ke heelein toot gayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ke heelein toot gayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ke heelein toot gayi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">It's your boy badshah!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="It's your boy badshah!"/>
</div>
</pre>
