---
title: "bae song lyrics"
album: "Don"
artist: "Anirudh Ravichander"
lyricist: "Vignesh Shivan"
director: "Cibi Chakaravarthi"
path: "/albums/don-lyrics"
song: "Bae"
image: ../../images/albumart/don.jpg
date: 2022-05-13
lang: tamil
youtubeLink: "https://www.youtube.com/embed/w7Fjxf62t8E"
type: "happy"
singers:
  - RK Adithya
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Bae Kannaala Thittidaathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bae Kannaala Thittidaathey"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Naa Bae Palasellaam Paranthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Naa Bae Palasellaam Paranthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Poiye Poyaachey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poiye Poyaachey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bae Antha Sirippa Niruthidaathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bae Antha Sirippa Niruthidaathey"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Naa Bae Ini Athutha Maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Naa Bae Ini Athutha Maa"/>
</div>
<div class="lyrico-lyrics-wrapper">En Velainu Aayaachey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Velainu Aayaachey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ini Naa Unna En Kanna Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Naa Unna En Kanna Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Paathukka Pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paathukka Pora"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunaiyaa Kaaththa Antha Mazhaiya Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunaiyaa Kaaththa Antha Mazhaiya Kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Saerthukka Pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saerthukka Pora"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unakku Yethellaam Romba Pudikkumunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakku Yethellaam Romba Pudikkumunu"/>
</div>
<div class="lyrico-lyrics-wrapper">Therunchukka Pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Therunchukka Pora"/>
</div>
<div class="lyrico-lyrics-wrapper">En Bae Neethaanu Oorukkellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Bae Neethaanu Oorukkellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Therivikka Pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Therivikka Pora"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbe Anbe Neethaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbe Anbe Neethaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhan Anbe Neethaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan Anbe Neethaane"/>
</div>
<div class="lyrico-lyrics-wrapper">En Bae Endraal Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Bae Endraal Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaathukkum Mele Neethaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaathukkum Mele Neethaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Bae En Bae Neethaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Bae En Bae Neethaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Enthan Thembe Neethaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enthan Thembe Neethaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Munbe Munbe Vanthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munbe Munbe Vanthey"/>
</div>
<div class="lyrico-lyrics-wrapper">En Bae Neethaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Bae Neethaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bae Kannaala Thittidaathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bae Kannaala Thittidaathey"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Naa Bae Palasellaam Paranthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Naa Bae Palasellaam Paranthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Poiye Poyaachey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poiye Poyaachey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bae Antha Sirippa Niruthidaathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bae Antha Sirippa Niruthidaathey"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Naa Bae Ini Athutha Maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Naa Bae Ini Athutha Maa"/>
</div>
<div class="lyrico-lyrics-wrapper">En Velai Aayaachey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Velai Aayaachey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ini Naa Unna En Kanna Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Naa Unna En Kanna Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Paathukka Poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paathukka Poren"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunaiyaa Kaaththa Antha Mazhaiya Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunaiyaa Kaaththa Antha Mazhaiya Kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Saerthukka Poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saerthukka Poren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unakku Yethellaam Romba Pudikkumunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakku Yethellaam Romba Pudikkumunu"/>
</div>
<div class="lyrico-lyrics-wrapper">Therunchukka Poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Therunchukka Poren"/>
</div>
<div class="lyrico-lyrics-wrapper">En Bae Neethaan Oorukkellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Bae Neethaan Oorukkellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Theruvikka Poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theruvikka Poren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thalli Nee Pona Thedi Varuvene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalli Nee Pona Thedi Varuvene"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakka Samayathil Kaiya Tharuvene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakka Samayathil Kaiya Tharuvene"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Akkam Pakkama Aale Illaatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Akkam Pakkama Aale Illaatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakkam Varalaame Kanney Oru Vaatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkam Varalaame Kanney Oru Vaatti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Puthusaa Kaadhala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthusaa Kaadhala"/>
</div>
<div class="lyrico-lyrics-wrapper">Pazhaki Paakkuren Nalla Neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pazhaki Paakkuren Nalla Neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethukku Edanjala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethukku Edanjala"/>
</div>
<div class="lyrico-lyrics-wrapper">Mail Kanukkula Dhooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mail Kanukkula Dhooram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal Chinname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Chinname"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Paakkanamnu Kettu Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Paakkanamnu Kettu Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Inga Kondu Vanthene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga Kondu Vanthene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbe Anbe Neethaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbe Anbe Neethaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhan Anbe Neethaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan Anbe Neethaane"/>
</div>
<div class="lyrico-lyrics-wrapper">En Bae Endraal Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Bae Endraal Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaathukkum Mele Neethaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaathukkum Mele Neethaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Bae En Bae Neethaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Bae En Bae Neethaaney"/>
</div>
<div class="lyrico-lyrics-wrapper">Enthan Thenbe Neethaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enthan Thenbe Neethaaney"/>
</div>
<div class="lyrico-lyrics-wrapper">Munbe Munbe Vantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munbe Munbe Vantha"/>
</div>
<div class="lyrico-lyrics-wrapper">En Bae Neethaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Bae Neethaaney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bae Kannaala Thittidaathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bae Kannaala Thittidaathey"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Naa Bae Palasellaam Paranthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Naa Bae Palasellaam Paranthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Poiye Poyaachey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poiye Poyaachey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bae Antha Sirippa Niruthidaathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bae Antha Sirippa Niruthidaathey"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Naa Bae Ini Athutha Maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Naa Bae Ini Athutha Maa"/>
</div>
<div class="lyrico-lyrics-wrapper">En Velai Aayaachey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Velai Aayaachey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ini Naa Unna En Kanna Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Naa Unna En Kanna Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Paathukka Pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paathukka Pora"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunaiyaa Kaaththa Antha Mazhaiya Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunaiyaa Kaaththa Antha Mazhaiya Kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Saerthukka Pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saerthukka Pora"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unakku Ethellaam Romba Pudikkumunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakku Ethellaam Romba Pudikkumunu"/>
</div>
<div class="lyrico-lyrics-wrapper">Therunchukka Pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Therunchukka Pora"/>
</div>
<div class="lyrico-lyrics-wrapper">En Bae Neethaanu Oorukkellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Bae Neethaanu Oorukkellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Therivikka Pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Therivikka Pora"/>
</div>
</pre>
