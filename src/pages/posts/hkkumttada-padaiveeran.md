---
title: "hkkumttada song lyrics"
album: "Padaiveeran"
artist: "Karthik Raja"
lyricist: "Dhana Sekaran"
director: "Dhana Sekaran"
path: "/albums/padaiveeran-lyrics"
song: "Hkkumttada"
image: ../../images/albumart/padaiveeran.jpg
date: 2018-02-02
lang: tamil
youtubeLink: "https://www.youtube.com/embed/0EhfrRpCoT8"
type: "happy"
singers:
  - Vijay Yesudas
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hkkumttada hkkumttada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hkkumttada hkkumttada"/>
</div>
<div class="lyrico-lyrics-wrapper">Kettaa hkkumttada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kettaa hkkumttada"/>
</div>
<div class="lyrico-lyrics-wrapper">Yea aa aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yea aa aa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hkkumttada hkkumttada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hkkumttada hkkumttada"/>
</div>
<div class="lyrico-lyrics-wrapper">Kettaa aatumtadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kettaa aatumtadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yea aa aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yea aa aa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hkkumttada hkkumttada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hkkumttada hkkumttada"/>
</div>
<div class="lyrico-lyrics-wrapper">Kettaa hkkumttada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kettaa hkkumttada"/>
</div>
<div class="lyrico-lyrics-wrapper">Yea aa aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yea aa aa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hkkumttada hkkumttada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hkkumttada hkkumttada"/>
</div>
<div class="lyrico-lyrics-wrapper">Kettaa aatumtadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kettaa aatumtadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yea aa aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yea aa aa"/>
</div>
</pre>
