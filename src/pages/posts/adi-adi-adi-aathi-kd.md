---
title: "adi adi adi aathi song lyrics"
album: "K D"
artist: "Karthikeya Murthy"
lyricist: "Sabarivaasan Shanmugam"
director: "Madhumitha"
path: "/albums/kd-lyrics"
song: "Adi Adi Adi Aathi"
image: ../../images/albumart/kd.jpg
date: 2019-11-22
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ghNqC2MvWpg"
type: "happy"
singers:
  - Karthikeya Murthy
  - Diwakar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">adi adi adi aathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi adi adi aathi"/>
</div>
<div class="lyrico-lyrics-wrapper">aranelli pathagathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aranelli pathagathi"/>
</div>
<div class="lyrico-lyrics-wrapper">idi idi idiyappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idi idi idiyappa"/>
</div>
<div class="lyrico-lyrics-wrapper">surukulla manasu suthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="surukulla manasu suthi"/>
</div>
<div class="lyrico-lyrics-wrapper">malli poovuku yaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malli poovuku yaru"/>
</div>
<div class="lyrico-lyrics-wrapper">vella adicha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vella adicha"/>
</div>
<div class="lyrico-lyrics-wrapper">vaanavilluku vannam thelicha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaanavilluku vannam thelicha"/>
</div>
<div class="lyrico-lyrics-wrapper">kathiri kathirikolu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathiri kathirikolu"/>
</div>
<div class="lyrico-lyrics-wrapper">en kannalage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kannalage"/>
</div>
<div class="lyrico-lyrics-wrapper">mukkani mukkani saaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mukkani mukkani saaru"/>
</div>
<div class="lyrico-lyrics-wrapper">en muthalage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en muthalage"/>
</div>
<div class="lyrico-lyrics-wrapper">kathiri kathirikolu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathiri kathirikolu"/>
</div>
<div class="lyrico-lyrics-wrapper">en kannalage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kannalage"/>
</div>
<div class="lyrico-lyrics-wrapper">mukkani mukkani saaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mukkani mukkani saaru"/>
</div>
<div class="lyrico-lyrics-wrapper">en muthalage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en muthalage"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">adi adi adi aathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi adi adi aathi"/>
</div>
<div class="lyrico-lyrics-wrapper">aranelli pathagathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aranelli pathagathi"/>
</div>
<div class="lyrico-lyrics-wrapper">idi idi idiyappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idi idi idiyappa"/>
</div>
<div class="lyrico-lyrics-wrapper">surukulla manasu suthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="surukulla manasu suthi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pullada mookula puthusana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pullada mookula puthusana"/>
</div>
<div class="lyrico-lyrics-wrapper">paniyana mookuthi potathu yar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paniyana mookuthi potathu yar"/>
</div>
<div class="lyrico-lyrics-wrapper">antha kathiri veyilil kathiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antha kathiri veyilil kathiri"/>
</div>
<div class="lyrico-lyrics-wrapper">kaavuku kodai puduchathum yar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaavuku kodai puduchathum yar"/>
</div>
<div class="lyrico-lyrics-wrapper">idex kannala sticker pottala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idex kannala sticker pottala"/>
</div>
<div class="lyrico-lyrics-wrapper">attaya potathu yar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="attaya potathu yar"/>
</div>
<div class="lyrico-lyrics-wrapper">intha kanathu regaiyil thantati 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha kanathu regaiyil thantati "/>
</div>
<div class="lyrico-lyrics-wrapper">aatathil ipothu parpathum yar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aatathil ipothu parpathum yar"/>
</div>
<div class="lyrico-lyrics-wrapper">aathula malai polave oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aathula malai polave oh"/>
</div>
<div class="lyrico-lyrics-wrapper">aalamum velaiyaduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aalamum velaiyaduthe"/>
</div>
<div class="lyrico-lyrics-wrapper">en kannula neela petti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kannula neela petti"/>
</div>
<div class="lyrico-lyrics-wrapper">ada nenjula seeni katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada nenjula seeni katti"/>
</div>
<div class="lyrico-lyrics-wrapper">naan aduren vudu katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan aduren vudu katti"/>
</div>
<div class="lyrico-lyrics-wrapper">oh oh oh oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oh oh oh oh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kathiri kathirikolu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathiri kathirikolu"/>
</div>
<div class="lyrico-lyrics-wrapper">en kannalage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kannalage"/>
</div>
<div class="lyrico-lyrics-wrapper">mukkani mukkani saaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mukkani mukkani saaru"/>
</div>
<div class="lyrico-lyrics-wrapper">en muthalage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en muthalage"/>
</div>
</pre>
