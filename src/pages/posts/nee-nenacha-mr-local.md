---
title: "nee nenacha song lyrics"
album: "Mr. Local"
artist: "Hiphop Tamizha"
lyricist: "Hiphop Tamizha"
director: "M. Rajesh"
path: "/albums/mr-local-lyrics"
song: "Nee Nenacha"
image: ../../images/albumart/mr-local.jpg
date: 2019-05-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/HUwEXHPEgdM"
type: "love"
singers:
  - 	Sid Sriram
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nee Nenachaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Nenachaa"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kai Pudichaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kai Pudichaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagatha Thaandi Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagatha Thaandi Kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum Varuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum Varuven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Siricha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Siricha"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Kadhalichcha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Kadhalichcha"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakaaga Thaney Naan En
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakaaga Thaney Naan En"/>
</div>
<div class="lyrico-lyrics-wrapper">Usura Tharuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usura Tharuven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Naal Oru Naal Oru Naal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Naal Oru Naal Oru Naal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nam Vaazhkai Nizhai Maarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam Vaazhkai Nizhai Maarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha Naal Varumey Aanaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Naal Varumey Aanaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Ellaam Seri Aagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Ellaam Seri Aagum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Naal Oru Naal Oru Naal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Naal Oru Naal Oru Naal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nam Vaazhkai Nilai Maarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam Vaazhkai Nilai Maarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha Naal Varumey Aanaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Naal Varumey Aanaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Ellaam Seri Aagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Ellaam Seri Aagum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Nenacha En Kai Pudicha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Nenacha En Kai Pudicha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagaththa Thandi Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagaththa Thandi Kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum Varuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum Varuven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Pogum Paadhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Pogum Paadhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu Enakkey Theriyaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu Enakkey Theriyaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum Pinnaal Vandhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Pinnaal Vandhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaal Mudiyaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaal Mudiyaadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aiyoo Sollavum Mudiyaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyoo Sollavum Mudiyaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaal Mellavum Mudiyaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaal Mellavum Mudiyaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Vaazhuren Vaazhka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Vaazhuren Vaazhka"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarukkum Theriyaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarukkum Theriyaama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Sogam Kondaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Sogam Kondaal"/>
</div>
<div class="lyrico-lyrics-wrapper">En Nenjam Saagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nenjam Saagum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Vaangi  Vandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Vaangi  Vandha"/>
</div>
<div class="lyrico-lyrics-wrapper">En Vaazhvin Saabam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Vaazhvin Saabam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Nenacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Nenacha"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kai Pudichaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kai Pudichaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagatha Thaandi Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagatha Thaandi Kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum Varuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum Varuven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Siricha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Siricha"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Kadhalichcha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Kadhalichcha"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakaaga Thaney Naan En
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakaaga Thaney Naan En"/>
</div>
<div class="lyrico-lyrics-wrapper">Usura Tharuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usura Tharuven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadum Irul Kangalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadum Irul Kangalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Soozhndhaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soozhndhaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholviyal Thuvandey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholviyal Thuvandey"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponnalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponnalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Anjamaatteney Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anjamaatteney Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Achamillaadha Vaanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achamillaadha Vaanai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thoduveney Tholai Dhooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoduveney Tholai Dhooram"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Irundhaa Adhu Podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Irundhaa Adhu Podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nam Vaazhivinil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam Vaazhivinil"/>
</div>
<div class="lyrico-lyrics-wrapper">Sumandhidum Baaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sumandhidum Baaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaame Ini Seri Aagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaame Ini Seri Aagum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Nenachaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Nenachaa"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kai Pudichaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kai Pudichaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagatha Thaandi Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagatha Thaandi Kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum Varuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum Varuven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Siricha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Siricha"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Kadhalichcha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Kadhalichcha"/>
</div>
<div class="lyrico-lyrics-wrapper">Usuratha Naanum Unakke Tharuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usuratha Naanum Unakke Tharuven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Naal Oru Naal Oru Naal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Naal Oru Naal Oru Naal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nam Vaazhkai Nizhai Maarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam Vaazhkai Nizhai Maarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha Naal Varumey Aanaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Naal Varumey Aanaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Ellaam Seri Aagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Ellaam Seri Aagum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Naal Oru Naal Oru Naal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Naal Oru Naal Oru Naal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nam Vaazhkai Nilai Maarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam Vaazhkai Nilai Maarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha Naal Varumey Aanaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Naal Varumey Aanaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Ellaam Seri Aagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Ellaam Seri Aagum"/>
</div>
</pre>
