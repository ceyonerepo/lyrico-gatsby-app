---
title: "kangal rendum parkkum song lyrics"
album: "Nagesh Thiraiyarangam"
artist: "Srikanth Deva"
lyricist: "Thamarai"
director: "Mohamad Issack"
path: "/albums/nagesh-thiraiyarangam-lyrics"
song: "Kangal Rendum Parkkum"
image: ../../images/albumart/nagesh-thiraiyarangam.jpg
date: 2018-02-16
lang: tamil
youtubeLink: "https://www.youtube.com/embed/NIz-c1diKkU"
type: "love"
singers:
  - Jagadeesh Kumar
  - Shashaa Tirupati
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kangal Rendum Paarkkum Bodhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangal Rendum Paarkkum Bodhey"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Vittu Pőgudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Vittu Pőgudhey"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Mattum Paarthal Podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Mattum Paarthal Podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Endru Yeno Thonudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endru Yeno Thonudhey"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudhal Mudhal Varum Mazhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhal Mudhal Varum Mazhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanaikkumaa Verinvarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanaikkumaa Verinvarai"/>
</div>
<div class="lyrico-lyrics-wrapper">Inaindhidum Idhayangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inaindhidum Idhayangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Adaindhidum Monanilai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adaindhidum Monanilai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Eppo Vanthu Nenjikkula Yerinaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Eppo Vanthu Nenjikkula Yerinaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Paarkkum Munbe Naanagave Maarinai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Paarkkum Munbe Naanagave Maarinai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithuverum Kanavudhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithuverum Kanavudhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Enniye Naan Yengugiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Enniye Naan Yengugiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavu Illai Ninivena Therindha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavu Illai Ninivena Therindha "/>
</div>
<div class="lyrico-lyrics-wrapper">Pinbudhan Thoonğugiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pinbudhan Thoonğugiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Ni Illai Endral Naan Enna Seiven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ni Illai Endral Naan Enna Seiven"/>
</div>
<div class="lyrico-lyrics-wrapper">Ni Ennai Serndhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ni Ennai Serndhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Mannil Vaazhven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Mannil Vaazhven"/>
</div>
<div class="lyrico-lyrics-wrapper">Unkaiyil Serumnaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unkaiyil Serumnaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Enni Theyavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enni Theyavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Madiyil Idam Thedi Naanum Saayava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Madiyil Idam Thedi Naanum Saayava"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangal Rendum Paarkkum Bodhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangal Rendum Paarkkum Bodhey"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Vittu Pőgudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Vittu Pőgudhey"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Mattum Paarthal Podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Mattum Paarthal Podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Endru Yeno Thonudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endru Yeno Thonudhey"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayakkidum Maalai Neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayakkidum Maalai Neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogum Veenagave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogum Veenagave"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhigalo Unnai Mattum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhigalo Unnai Mattum "/>
</div>
<div class="lyrico-lyrics-wrapper">Thedum Thaanagave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedum Thaanagave"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudhal Murai Hanniye Aanidam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhal Murai Hanniye Aanidam"/>
</div>
<div class="lyrico-lyrics-wrapper">Mugavari Thaayena Ketkkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mugavari Thaayena Ketkkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhakkugal Yetrida Naalvarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhakkugal Yetrida Naalvarum"/>
</div>
<div class="lyrico-lyrics-wrapper">En Puthu Veedu Na Paarkkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Puthu Veedu Na Paarkkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Varavaa Solkanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varavaa Solkanna"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinamum Naan Pogum Paadhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinamum Naan Pogum Paadhai"/>
</div>
<div class="lyrico-lyrics-wrapper">indru Yen Maaruthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indru Yen Maaruthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhi Engum Unnai Thedi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi Engum Unnai Thedi "/>
</div>
<div class="lyrico-lyrics-wrapper">Kangal Yemaaruthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangal Yemaaruthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Malar Mugam Paarthadhum Maarinen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malar Mugam Paarthadhum Maarinen"/>
</div>
<div class="lyrico-lyrics-wrapper">Madiyinil Saaindhida Yenginen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madiyinil Saaindhida Yenginen"/>
</div>
<div class="lyrico-lyrics-wrapper">Idaivezhi Koodida Vaadinen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idaivezhi Koodida Vaadinen"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuravaram Mele Na Pogiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuravaram Mele Na Pogiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhage"/>
</div>
<div class="lyrico-lyrics-wrapper">Megam Vanthu Vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megam Vanthu Vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mella Mella Ennai Theendudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mella Mella Ennai Theendudhey"/>
</div>
<div class="lyrico-lyrics-wrapper">Megam Illai illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megam Illai illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Endru Solli Kondennanadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endru Solli Kondennanadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Osai Indri Ulle Mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Osai Indri Ulle Mattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Peyum Mazhai Kaalame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peyum Mazhai Kaalame"/>
</div>
<div class="lyrico-lyrics-wrapper">Iram Pattu Iram Pattu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iram Pattu Iram Pattu "/>
</div>
<div class="lyrico-lyrics-wrapper">Inge Kaachal Vandhadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inge Kaachal Vandhadhey"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangal Rendum Paarkkum Bodhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangal Rendum Paarkkum Bodhey"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Vittu Pőgudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Vittu Pőgudhey"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Mattum Paarthal Podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Mattum Paarthal Podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Endru Yeno Thonudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endru Yeno Thonudhey"/>
</div>
</pre>
