---
title: "thode kam ajnabi song lyrics"
album: "Pagglait"
artist: "Arijit Singh"
lyricist: "Neelesh Misra"
director: "Umesh Bist"
path: "/albums/pagglait-lyrics"
song: "Thode Kam Ajnabi"
image: ../../images/albumart/pagglait.jpg
date: 2021-03-26
lang: hindi
youtubeLink: "https://www.youtube.com/embed/jTuMsX43Ubs"
type: "Mass"
singers:
  - Himani Kapoor
  - Arijit Singh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Thode gam kam abhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thode gam kam abhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho aa thode se kam ajnabi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho aa thode se kam ajnabi"/>
</div>
<div class="lyrico-lyrics-wrapper">Mere dil ke ghar mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mere dil ke ghar mein"/>
</div>
<div class="lyrico-lyrics-wrapper">Khidki nayi hai khul gayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khidki nayi hai khul gayi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thode se kam ajnabi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thode se kam ajnabi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thode se kam ajnabi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thode se kam ajnabi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Khwahishein nayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khwahishein nayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Hothon ke munderon pe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hothon ke munderon pe"/>
</div>
<div class="lyrico-lyrics-wrapper">Chhipi hai dheron
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chhipi hai dheron"/>
</div>
<div class="lyrico-lyrics-wrapper">Chhoti chhoti si khushi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chhoti chhoti si khushi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho aa thode se kam ajnabi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho aa thode se kam ajnabi"/>
</div>
<div class="lyrico-lyrics-wrapper">Achhi si lage hai zindagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achhi si lage hai zindagi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dil chiraiya ho ho chiraiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil chiraiya ho ho chiraiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Dil chiraiya nayi baatein bole hamka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil chiraiya nayi baatein bole hamka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muskurayein hum kyun bewajah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muskurayein hum kyun bewajah"/>
</div>
<div class="lyrico-lyrics-wrapper">Taaka jhaanki toka taaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Taaka jhaanki toka taaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Karta jaaye dil zid pe ada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karta jaaye dil zid pe ada"/>
</div>
<div class="lyrico-lyrics-wrapper">Maine naa ki isne haan ki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maine naa ki isne haan ki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhoop chhanv bunte saath kabhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhoop chhanv bunte saath kabhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhool bhulaiya mein mil jo jaate
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhool bhulaiya mein mil jo jaate"/>
</div>
<div class="lyrico-lyrics-wrapper">Raste tere mere sabhi khwahishein nayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raste tere mere sabhi khwahishein nayi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Khawahishein nayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khawahishein nayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Hothon ke munderon pe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hothon ke munderon pe"/>
</div>
<div class="lyrico-lyrics-wrapper">Chhipi hai dheron
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chhipi hai dheron"/>
</div>
<div class="lyrico-lyrics-wrapper">Chhoti chhoti si khushi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chhoti chhoti si khushi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho aa thode se kam ajnabi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho aa thode se kam ajnabi"/>
</div>
<div class="lyrico-lyrics-wrapper">Achhi si lage hai zindagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achhi si lage hai zindagi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hothon ke munderon pe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hothon ke munderon pe"/>
</div>
<div class="lyrico-lyrics-wrapper">Chhipi hai dheron
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chhipi hai dheron"/>
</div>
<div class="lyrico-lyrics-wrapper">Chhoti chhoti si khushi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chhoti chhoti si khushi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho aa thode se kam ajnabi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho aa thode se kam ajnabi"/>
</div>
<div class="lyrico-lyrics-wrapper">Achhi si lage hai zindagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achhi si lage hai zindagi"/>
</div>
</pre>
