---
title: "usuraye ulukkuthe song lyrics"
album: "Thaen"
artist: "Sanath Bharadwaj"
lyricist: "S.Gnanakaravel"
director: "Ganesh Vinayakan"
path: "/albums/thaen-song-lyrics"
song: "Usuraye Ulukkuthe"
image: ../../images/albumart/thaen.jpg
date: 2021-03-19
lang: tamil
youtubeLink: "https://www.youtube.com/embed/HnFbUMWuJ6Y"
type: "Love"
singers:
  - Saindhavi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Usuraiye ulukkuthey nee enthan saamiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usuraiye ulukkuthey nee enthan saamiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenachathe nadukkuthey idhu nalla neramo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenachathe nadukkuthey idhu nalla neramo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaiya pudichi nadukkura dhisai yengo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiya pudichi nadukkura dhisai yengo"/>
</div>
<div class="lyrico-lyrics-wrapper">Gama gamanu manakkuthu man vaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gama gamanu manakkuthu man vaasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkena vaazhum bodhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkena vaazhum bodhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Iruvarum inainjathu oru usura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruvarum inainjathu oru usura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Usuraiye ulukkuthey nee enthan saamiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usuraiye ulukkuthey nee enthan saamiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenachathe nadukkuthey idhu nalla neramo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenachathe nadukkuthey idhu nalla neramo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uravaga vaazhave varam ketten saamiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uravaga vaazhave varam ketten saamiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudukkatha saamiya kurai solla thonale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudukkatha saamiya kurai solla thonale"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhamatta sadangume vaazha vachu paarkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhamatta sadangume vaazha vachu paarkala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manasoda alli vanthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasoda alli vanthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Malai thaene unnai kanden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malai thaene unnai kanden"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam thorum neeyum naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam thorum neeyum naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaattukulla malai pola nelachiruppom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattukulla malai pola nelachiruppom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Usuraiye ulukkuthey nee enthan saamiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usuraiye ulukkuthey nee enthan saamiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenachathe nadukkuthey idhu nalla neramo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenachathe nadukkuthey idhu nalla neramo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En thaaiyu thandhaiya unakkula paakuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En thaaiyu thandhaiya unakkula paakuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kooda vaazha thaan usuroda vaazhuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kooda vaazha thaan usuroda vaazhuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudusaiyum aranmanai idhu kadhal sadhanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudusaiyum aranmanai idhu kadhal sadhanai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadelam un vasanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadelam un vasanai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thinjellam un yosanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thinjellam un yosanai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatha pola kaalam thaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatha pola kaalam thaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagamtha valamvarum uravithuve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagamtha valamvarum uravithuve"/>
</div>
</pre>
