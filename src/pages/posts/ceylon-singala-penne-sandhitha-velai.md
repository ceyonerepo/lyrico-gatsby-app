---
title: "ceylon singala penne song lyrics"
album: "Sandhitha Velai"
artist: "Deva"
lyricist: "K. Subash"
director: "Ravichandran"
path: "/albums/sandhitha-velai-lyrics"
song: "Ceylon Singala Penne"
image: ../../images/albumart/sandhitha-velai.jpg
date: 2000-04-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/454ag7yz3wo"
type: "happy"
singers:
  - Sukhwinder Singh
  - Sabesh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">arithu arithu mangayarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arithu arithu mangayarai"/>
</div>
<div class="lyrico-lyrics-wrapper">pirathal arithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pirathal arithu"/>
</div>
<div class="lyrico-lyrics-wrapper">athaninum arithu alagai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athaninum arithu alagai"/>
</div>
<div class="lyrico-lyrics-wrapper">iruthal enbathu arithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iruthal enbathu arithu"/>
</div>
<div class="lyrico-lyrics-wrapper">perithu perithu aval paarvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="perithu perithu aval paarvai"/>
</div>
<div class="lyrico-lyrics-wrapper">en mel paduthal perithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en mel paduthal perithu"/>
</div>
<div class="lyrico-lyrics-wrapper">so your site is the 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="so your site is the "/>
</div>
<div class="lyrico-lyrics-wrapper">secret of my energy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="secret of my energy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">silonu singala penne sinungathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="silonu singala penne sinungathe"/>
</div>
<div class="lyrico-lyrics-wrapper">jillunu road iruntha othungathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jillunu road iruntha othungathe"/>
</div>
<div class="lyrico-lyrics-wrapper">silonu singala penne sinungathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="silonu singala penne sinungathe"/>
</div>
<div class="lyrico-lyrics-wrapper">jillunu road iruntha othungathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jillunu road iruntha othungathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">un daddy enaku alargi than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un daddy enaku alargi than"/>
</div>
<div class="lyrico-lyrics-wrapper">mammy enaku energy than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mammy enaku energy than"/>
</div>
<div class="lyrico-lyrics-wrapper">un thangai enaku gift than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un thangai enaku gift than"/>
</div>
<div class="lyrico-lyrics-wrapper">ini lifeil enaku lift than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ini lifeil enaku lift than"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">valarum ilaigan apadiye sapiduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valarum ilaigan apadiye sapiduven"/>
</div>
<div class="lyrico-lyrics-wrapper">valarum ilaigan apadiye sapiduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valarum ilaigan apadiye sapiduven"/>
</div>
<div class="lyrico-lyrics-wrapper">silonu singala penne sinungathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="silonu singala penne sinungathe"/>
</div>
<div class="lyrico-lyrics-wrapper">jillunu road iruntha othungathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jillunu road iruntha othungathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pidika pidika kolupiruke vegama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pidika pidika kolupiruke vegama"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu vera level la potu senja 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu vera level la potu senja "/>
</div>
<div class="lyrico-lyrics-wrapper">thegama thegama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thegama thegama"/>
</div>
<div class="lyrico-lyrics-wrapper">sanga renda vetti senja kannama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sanga renda vetti senja kannama"/>
</div>
<div class="lyrico-lyrics-wrapper">power kattum kooda eppodhume 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="power kattum kooda eppodhume "/>
</div>
<div class="lyrico-lyrics-wrapper">minnuma minnuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="minnuma minnuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thanni kudam pol irukum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanni kudam pol irukum"/>
</div>
<div class="lyrico-lyrics-wrapper">idupu oru thinusu than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idupu oru thinusu than"/>
</div>
<div class="lyrico-lyrics-wrapper">thariketu thudikuthamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thariketu thudikuthamma"/>
</div>
<div class="lyrico-lyrics-wrapper">ennoda ila manasu than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennoda ila manasu than"/>
</div>
<div class="lyrico-lyrics-wrapper">salvaru pota kuyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="salvaru pota kuyile"/>
</div>
<div class="lyrico-lyrics-wrapper">en madiyil vantha mayile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en madiyil vantha mayile"/>
</div>
<div class="lyrico-lyrics-wrapper">thandavaalam illamale 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thandavaalam illamale "/>
</div>
<div class="lyrico-lyrics-wrapper">parakuthu intha rayile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="parakuthu intha rayile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">un daddy enaku alargi than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un daddy enaku alargi than"/>
</div>
<div class="lyrico-lyrics-wrapper">mammy enaku energy than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mammy enaku energy than"/>
</div>
<div class="lyrico-lyrics-wrapper">un thangai enaku gift than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un thangai enaku gift than"/>
</div>
<div class="lyrico-lyrics-wrapper">ini lifeil enaku lift than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ini lifeil enaku lift than"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">valarum ilaigan apadiye sapiduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valarum ilaigan apadiye sapiduven"/>
</div>
<div class="lyrico-lyrics-wrapper">valarum ilaigan apadiye sapiduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valarum ilaigan apadiye sapiduven"/>
</div>
<div class="lyrico-lyrics-wrapper">silonu singala penne sinungathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="silonu singala penne sinungathe"/>
</div>
<div class="lyrico-lyrics-wrapper">jillunu road iruntha othungathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jillunu road iruntha othungathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thangathala sethuki vacha roobama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thangathala sethuki vacha roobama"/>
</div>
<div class="lyrico-lyrics-wrapper">kaal kolusil irunthu varuvathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaal kolusil irunthu varuvathu"/>
</div>
<div class="lyrico-lyrics-wrapper">enna ragama ragama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna ragama ragama"/>
</div>
<div class="lyrico-lyrics-wrapper">kai valaiyal kulunguthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kai valaiyal kulunguthu"/>
</div>
<div class="lyrico-lyrics-wrapper">paar thalama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paar thalama"/>
</div>
<div class="lyrico-lyrics-wrapper">ival nadai alagil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ival nadai alagil "/>
</div>
<div class="lyrico-lyrics-wrapper">valayapatti melama melama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valayapatti melama melama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">unnai thinam paarkum pothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai thinam paarkum pothu"/>
</div>
<div class="lyrico-lyrics-wrapper">kaai manau kaniyuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaai manau kaniyuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">thalladum kilavanuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalladum kilavanuku"/>
</div>
<div class="lyrico-lyrics-wrapper">pathu vayasu kuraiyuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathu vayasu kuraiyuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">adi thillalankadi kuyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi thillalankadi kuyile"/>
</div>
<div class="lyrico-lyrics-wrapper">en dindivanam mayile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en dindivanam mayile"/>
</div>
<div class="lyrico-lyrics-wrapper">ennoda aayul regai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennoda aayul regai "/>
</div>
<div class="lyrico-lyrics-wrapper">irukuthu un kaiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irukuthu un kaiyile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">un daddy enaku alargi than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un daddy enaku alargi than"/>
</div>
<div class="lyrico-lyrics-wrapper">mammy enaku energy than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mammy enaku energy than"/>
</div>
<div class="lyrico-lyrics-wrapper">un thangai enaku gift than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un thangai enaku gift than"/>
</div>
<div class="lyrico-lyrics-wrapper">ini lifeil enaku lift than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ini lifeil enaku lift than"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">valarum ilaigan apadiye sapiduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valarum ilaigan apadiye sapiduven"/>
</div>
<div class="lyrico-lyrics-wrapper">valarum ilaigan apadiye sapiduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valarum ilaigan apadiye sapiduven"/>
</div>
<div class="lyrico-lyrics-wrapper">silonu singala penne sinungathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="silonu singala penne sinungathe"/>
</div>
<div class="lyrico-lyrics-wrapper">jillunu road iruntha othungathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jillunu road iruntha othungathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">silonu singala penne sinungathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="silonu singala penne sinungathe"/>
</div>
<div class="lyrico-lyrics-wrapper">jillunu road iruntha othungathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jillunu road iruntha othungathe"/>
</div>
</pre>
