---
title: "ennulae song lyrics"
album: "Yaakkai"
artist: "Yuvan Shankar Raja"
lyricist: "Pa Vijay"
director: "Kuzhandai Velappan"
path: "/albums/yaakkai-lyrics"
song: "Ennulae"
image: ../../images/albumart/yaakkai.jpg
date: 2017-03-03
lang: tamil
youtubeLink: "https://www.youtube.com/embed/1kIb7PGaWtM"
type: "love"
singers:
  -	Tanvi Shah
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ennulae yen indha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennulae yen indha"/>
</div>
<div class="lyrico-lyrics-wrapper">Salanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Salanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Unmelae ullae orr
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmelae ullae orr"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyo naano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyo naano"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar sonnal enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar sonnal enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal ennum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal ennum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatrodu neeyum naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatrodu neeyum naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Parakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parakka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennulae yen indha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennulae yen indha"/>
</div>
<div class="lyrico-lyrics-wrapper">Salanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Salanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Unmelae ullae orr
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmelae ullae orr"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavanam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paravai siragin adiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paravai siragin adiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Pathungi kidakiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathungi kidakiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhaiyin thulikkul surundu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhaiyin thulikkul surundu"/>
</div>
<div class="lyrico-lyrics-wrapper">Madangi kidakiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madangi kidakiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai nee edhukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai nee edhukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Veliyilae vara alaikiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veliyilae vara alaikiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakkul vasikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkul vasikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal innum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal innum "/>
</div>
<div class="lyrico-lyrics-wrapper">theriyalaiyaa Alaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theriyalaiyaa Alaiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sila varudangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sila varudangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna avasiyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna avasiyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ore oru nodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ore oru nodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhigam thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhigam thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholaiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai yendha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai yendha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiyai neettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyai neettu"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayathai thirandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayathai thirandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai angu kaattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai angu kaattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Innum enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Yekka paarvaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yekka paarvaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethuvum inimel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethuvum inimel"/>
</div>
<div class="lyrico-lyrics-wrapper">Solla thevai illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solla thevai illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Puriyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puriyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aiyoda unnai thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyoda unnai thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pidikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pidikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanalum en kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanalum en kaadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadikkum"/>
</div>
</pre>
