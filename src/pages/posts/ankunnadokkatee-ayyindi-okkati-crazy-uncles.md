---
title: "ankunnadokkatee ayyindi okkati song lyrics"
album: "Crazy Uncles"
artist: "Raghu Kunche - Bhole Shavali"
lyricist: "Anand Gurram"
director: "E Sathi Babu"
path: "/albums/crazy-uncles-lyrics"
song: "Ankunnadokkatee Ayyindi Okkati"
image: ../../images/albumart/crazy-uncles.jpg
date: 2021-08-19
lang: telugu
youtubeLink: "https://www.youtube.com/embed/qBPzmgX3CpY"
type: "happy"
singers:
  - Bhole Shavali
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Anukunnadokkatee ayyindi okkati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anukunnadokkatee ayyindi okkati"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadadaaniki aashapadithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadadaaniki aashapadithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Thandaana ayyindi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandaana ayyindi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anukunnadokkatee ayyindi okkati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anukunnadokkatee ayyindi okkati"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadadaaniki aashapadithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadadaaniki aashapadithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Thandaana ayyindi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandaana ayyindi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Okka sekanu pakka sukham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okka sekanu pakka sukham"/>
</div>
<div class="lyrico-lyrics-wrapper">Anukshanam neeku narakam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anukshanam neeku narakam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chesindi okka thappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chesindi okka thappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Intikoche pedda muppu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intikoche pedda muppu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pellaalaku thelisthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pellaalaku thelisthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi kompa gaalche nippu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi kompa gaalche nippu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ayyayyo maamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyayyo maamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Endira ee kharma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endira ee kharma"/>
</div>
<div class="lyrico-lyrics-wrapper">A magavaadiki kudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="A magavaadiki kudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Raakudani badha raama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raakudani badha raama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ayyayyo maamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyayyo maamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Endira ee kharma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endira ee kharma"/>
</div>
<div class="lyrico-lyrics-wrapper">A magavaadiki kudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="A magavaadiki kudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Raakudani badha raama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raakudani badha raama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Politikallo kingainaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Politikallo kingainaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sinimaallo staratnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sinimaallo staratnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Bijinesslo thopaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bijinesslo thopaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Entha thurum khaanainaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entha thurum khaanainaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Evvadaithe nuvventy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evvadaithe nuvventy"/>
</div>
<div class="lyrico-lyrics-wrapper">Pavvarentha untenty
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pavvarentha untenty"/>
</div>
<div class="lyrico-lyrics-wrapper">Aada daani mojula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aada daani mojula"/>
</div>
<div class="lyrico-lyrics-wrapper">Padda veerulendaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padda veerulendaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Levaleka achachaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Levaleka achachaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Lempalesukunnnaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lempalesukunnnaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ayyayyo maamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyayyo maamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Endira ee kharma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endira ee kharma"/>
</div>
<div class="lyrico-lyrics-wrapper">A magavaadiki kudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="A magavaadiki kudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Raakudani badha raama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raakudani badha raama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ayyayyo maamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyayyo maamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Endira ee kharma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endira ee kharma"/>
</div>
<div class="lyrico-lyrics-wrapper">A magavaadiki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="A magavaadiki"/>
</div>
<div class="lyrico-lyrics-wrapper">A paga vaadiki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="A paga vaadiki"/>
</div>
<div class="lyrico-lyrics-wrapper">A paga vaadiki kuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="A paga vaadiki kuda"/>
</div>
<div class="lyrico-lyrics-wrapper">Raakudani badha raama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raakudani badha raama"/>
</div>
</pre>
