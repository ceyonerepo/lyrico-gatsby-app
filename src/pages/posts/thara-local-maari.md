---
title: 'thara local song lyrics'
album: 'Maari'
artist: 'Anirudh Ravichander'
lyricist: 'Dhanush'
director: 'Balaji Mohan'
path: '/albums/maari-song-lyrics'
song: 'Thara local'
image: ../../images/albumart/Maari.jpg
date: 2015-07-17
lang: tamil
youtubeLink: 'https://www.youtube.com/embed/OViH68fJUhM'
type: 'mass'
singers: 
- Dhanush
- Anirudh Ravichander
---

<pre class="lyrics-native">
    Coming Soon
</pre>

<pre class="lyrics-english">
   <div class="lyrico-lyrics-wrapper">Indha hey indha hey indha hey indha hey
<input type="checkbox" class="lyrico-select-lyric-line" value="Indha hey indha hey indha hey indha hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha hey indha hey indha hey indha hey
<input type="checkbox" class="lyrico-select-lyric-line" value="Indha hey indha hey indha hey indha hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha hey indha hey indha hey indha hey
<input type="checkbox" class="lyrico-select-lyric-line" value="Indha hey indha hey indha hey indha hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakittae thakkitae thaakitae…
<input type="checkbox" class="lyrico-select-lyric-line" value="Thakittae thakkitae thaakitae…"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha hey indha hey indha hey indha hey
<input type="checkbox" class="lyrico-select-lyric-line" value="Indha hey indha hey indha hey indha hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha hey indha hey indha hey indha hey indhaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Indha hey indha hey indha hey indha hey indhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Per perusu.. ivan aal pudhusu
<input type="checkbox" class="lyrico-select-lyric-line" value="Per perusu.. ivan aal pudhusu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhiyo dhinusu.. irukku manasu
<input type="checkbox" class="lyrico-select-lyric-line" value="Vazhiyo dhinusu.. irukku manasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey ullaara kuthamila suthamila bathamila
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey ullaara kuthamila suthamila bathamila"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuthamila suthamila.. kuthamila suthamila
<input type="checkbox" class="lyrico-select-lyric-line" value="Kuthamila suthamila.. kuthamila suthamila"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey paakaadha dhooramila
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey paakaadha dhooramila"/>
</div>
<div class="lyrico-lyrics-wrapper">Sogamilla baramila.. sogamila baramila
<input type="checkbox" class="lyrico-select-lyric-line" value="Sogamilla baramila.. sogamila baramila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Maari.. konjam nalla maari
<input type="checkbox" class="lyrico-select-lyric-line" value="Maari.. konjam nalla maari"/>
</div>
<div class="lyrico-lyrics-wrapper">Romba vera maari
<input type="checkbox" class="lyrico-select-lyric-line" value="Romba vera maari"/>
</div>
<div class="lyrico-lyrics-wrapper">Maari.. thecha thangam maari
<input type="checkbox" class="lyrico-select-lyric-line" value="Maari.. thecha thangam maari"/>
</div>
<div class="lyrico-lyrics-wrapper">Moracha singam maari (2 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Moracha singam maari"/></div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Indha hey indha hey indha hey indha hey
<input type="checkbox" class="lyrico-select-lyric-line" value="Indha hey indha hey indha hey indha hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha hey indha hey indha hey indha hey
<input type="checkbox" class="lyrico-select-lyric-line" value="Indha hey indha hey indha hey indha hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha hey indha hey indha hey indha hey
<input type="checkbox" class="lyrico-select-lyric-line" value="Indha hey indha hey indha hey indha hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakittae thakkitae thaakitae…
<input type="checkbox" class="lyrico-select-lyric-line" value="Thakittae thakkitae thaakitae…"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha hey indha hey indha hey indha hey
<input type="checkbox" class="lyrico-select-lyric-line" value="Indha hey indha hey indha hey indha hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha hey indha hey indha hey indha hey indhaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Indha hey indha hey indha hey indha hey indhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ooru olagam theriyaathu nyaayam dharmam kedayathu
<input type="checkbox" class="lyrico-select-lyric-line" value="Ooru olagam theriyaathu nyaayam dharmam kedayathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paatha maara pudikaathu paasam nesam puriyaathu
<input type="checkbox" class="lyrico-select-lyric-line" value="Paatha maara pudikaathu paasam nesam puriyaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Venaam pa.. hey poochaandi namalu inga thani party
<input type="checkbox" class="lyrico-select-lyric-line" value="Venaam pa.. hey poochaandi namalu inga thani party"/>
</div>
<div class="lyrico-lyrics-wrapper">Paakatha hey.. velayaadi kedikelaam ivan kedi
<input type="checkbox" class="lyrico-select-lyric-line" value="Paakatha hey.. velayaadi kedikelaam ivan kedi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Maari.. konjam nalla maari
<input type="checkbox" class="lyrico-select-lyric-line" value="Maari.. konjam nalla maari"/>
</div>
<div class="lyrico-lyrics-wrapper">Romba vera maari
<input type="checkbox" class="lyrico-select-lyric-line" value="Romba vera maari"/>
</div>
<div class="lyrico-lyrics-wrapper">Maari.. thecha thangam maari
<input type="checkbox" class="lyrico-select-lyric-line" value="Maari.. thecha thangam maari"/>
</div>
<div class="lyrico-lyrics-wrapper">Moracha singam maari
<input type="checkbox" class="lyrico-select-lyric-line" value="Moracha singam maari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Indha hey indha hey indha hey indha hey
<input type="checkbox" class="lyrico-select-lyric-line" value="Indha hey indha hey indha hey indha hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha hey indha hey indha hey indha hey
<input type="checkbox" class="lyrico-select-lyric-line" value="Indha hey indha hey indha hey indha hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha hey indha hey indha hey indha hey
<input type="checkbox" class="lyrico-select-lyric-line" value="Indha hey indha hey indha hey indha hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakittae thakkitae thaakitae…
<input type="checkbox" class="lyrico-select-lyric-line" value="Thakittae thakkitae thaakitae…"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha hey indha hey indha hey indha hey
<input type="checkbox" class="lyrico-select-lyric-line" value="Indha hey indha hey indha hey indha hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha hey indha hey indha hey indha hey indhaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Indha hey indha hey indha hey indha hey indhaa"/>
</div>
</pre>