---
title: "amma song lyrics"
album: "Chakra"
artist: "Yuvan Shankar Raja"
lyricist: "Karunakaran"
director: "M.S. Anandhan"
path: "/albums/chakra-song-lyrics"
song: "Amma Song"
image: ../../images/albumart/chakra.jpg
date: 2021-02-19
lang: tamil
youtubeLink: "https://www.youtube.com/embed/w0CNE5JMhY0"
type: "Amma Love"
singers:
  - Chinmayi Sripada
  - Prathana
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Unnil thodutha muthucharam naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnil thodutha muthucharam naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai petrathe enthan thavam thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai petrathe enthan thavam thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Veesum un sirippozhiyai swasan ena konden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veesum un sirippozhiyai swasan ena konden"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhum nodi ellame neethan vendume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhum nodi ellame neethan vendume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konji konji muththamittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konji konji muththamittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Uchchi mugarnthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uchchi mugarnthai"/>
</div>
<div class="lyrico-lyrics-wrapper">Enthan rani neethan endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enthan rani neethan endru"/>
</div>
<div class="lyrico-lyrics-wrapper">Mechi magizhnthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mechi magizhnthai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanne en ponnaana porkalame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanne en ponnaana porkalame"/>
</div>
<div class="lyrico-lyrics-wrapper">Un anbe ini ennaalum uyiraalume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un anbe ini ennaalum uyiraalume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbana kudilaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbana kudilaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai endrum kaathaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai endrum kaathaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan eendra en penne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan eendra en penne"/>
</div>
<div class="lyrico-lyrics-wrapper">En thaayai vanthaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En thaayai vanthaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Amma endrale adhu thean thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma endrale adhu thean thaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Amma anaithal thullum maan naane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma anaithal thullum maan naane"/>
</div>
<div class="lyrico-lyrics-wrapper">Paniyil nanaithalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paniyil nanaithalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sooriyan nadungathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sooriyan nadungathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai thavira ennai thaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai thavira ennai thaanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Thozhgal kidaiyathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozhgal kidaiyathe"/>
</div>
</pre>
