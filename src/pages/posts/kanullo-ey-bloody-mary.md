---
title: "kanullo ey song lyrics"
album: "Bloody Mary"
artist: "Kaala Bhairava"
lyricist: "Kittu Vissapragada"
director: "Chandoo Mondeti"
path: "/albums/bloody-mary-lyrics"
song: "Kanullo Ey"
image: ../../images/albumart/bloody-mary.jpg
date: 2022-04-15
lang: telugu
youtubeLink: "https://www.youtube.com/embed/P1WXj3ULtyg"
type: "sad"
singers:
  - Yamini Ghantasala
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">kanullo ey shunyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanullo ey shunyam"/>
</div>
<div class="lyrico-lyrics-wrapper">cherindho 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cherindho "/>
</div>
<div class="lyrico-lyrics-wrapper">raanunna nimishaalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="raanunna nimishaalo"/>
</div>
<div class="lyrico-lyrics-wrapper">yemundho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yemundho"/>
</div>
<div class="lyrico-lyrics-wrapper">oohinche dhaare ledhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oohinche dhaare ledhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">aadinche ee aatupotullo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aadinche ee aatupotullo"/>
</div>
<div class="lyrico-lyrics-wrapper">chepalla eedheti 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chepalla eedheti "/>
</div>
<div class="lyrico-lyrics-wrapper">sandram lo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sandram lo"/>
</div>
<div class="lyrico-lyrics-wrapper">valalone padthunnaaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valalone padthunnaaana"/>
</div>
<div class="lyrico-lyrics-wrapper">ninnati kaalam malli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninnati kaalam malli"/>
</div>
<div class="lyrico-lyrics-wrapper">raanundhaaa neduu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="raanundhaaa neduu"/>
</div>
<div class="lyrico-lyrics-wrapper">cheekatilone reyantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cheekatilone reyantha"/>
</div>
<div class="lyrico-lyrics-wrapper">chusthuvunda 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chusthuvunda "/>
</div>
<div class="lyrico-lyrics-wrapper">pai vaade geesthuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pai vaade geesthuna"/>
</div>
<div class="lyrico-lyrics-wrapper">chitram lo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chitram lo"/>
</div>
<div class="lyrico-lyrics-wrapper">ennenno saradhaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennenno saradhaalu"/>
</div>
<div class="lyrico-lyrics-wrapper">aanaadu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aanaadu "/>
</div>
<div class="lyrico-lyrics-wrapper">varnaale levu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varnaale levu"/>
</div>
<div class="lyrico-lyrics-wrapper">eenadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eenadu"/>
</div>
<div class="lyrico-lyrics-wrapper">gamyaale dhaachunche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gamyaale dhaachunche"/>
</div>
<div class="lyrico-lyrics-wrapper">daarullo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="daarullo"/>
</div>
<div class="lyrico-lyrics-wrapper">sudilone paduthunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sudilone paduthunna"/>
</div>
<div class="lyrico-lyrics-wrapper">naavallo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naavallo "/>
</div>
<div class="lyrico-lyrics-wrapper">theeramu chere 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theeramu chere "/>
</div>
<div class="lyrico-lyrics-wrapper">daare ledhaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="daare ledhaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">elaa evvarikosam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="elaa evvarikosam"/>
</div>
<div class="lyrico-lyrics-wrapper">vechundiiii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vechundiiii"/>
</div>
<div class="lyrico-lyrics-wrapper">chudaalo emooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chudaalo emooo"/>
</div>
</pre>
