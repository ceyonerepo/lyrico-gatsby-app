---
title: "yennai arindhaal - kadigaaram paarthal thavaru song lyrics"
album: "Yennai Arindhaal"
artist: "Harris Jayaraj"
lyricist: "Thamarai"
director: "Gautham Vasudev Menon"
path: "/albums/yennai-arindhaal-lyrics"
song: "Yennai Arindhaal - Kadigaaram Paarthal Thavaru"
image: ../../images/albumart/yennai-arindhaal.jpg
date: 2015-02-06
lang: tamil
youtubeLink: "https://www.youtube.com/embed/jHB8omnU0lA"
type: "Motivational"
singers:
  - Devan Ekambaram
  - Mark Thomas
  - Abhishek
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kadigaaram Paarthal Thavaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadigaaram Paarthal Thavaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nodi Mullaai Mattum Nagaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nodi Mullaai Mattum Nagaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kan Paarththu Pesa Pazhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan Paarththu Pesa Pazhagu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadamai Thaan Endrum Azhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadamai Thaan Endrum Azhagu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadigaaram Paarthal Thavaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadigaaram Paarthal Thavaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nodi Mullaai Mattum Nagaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nodi Mullaai Mattum Nagaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kan Paarththu Pesa Pazhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan Paarththu Pesa Pazhagu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadamai Thaan Endrum Azhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadamai Thaan Endrum Azhagu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Thapputhandaa Seithirunthaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Thapputhandaa Seithirunthaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Odi Poiyirudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odi Poiyirudaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Illai Netri Kanneel Nee Vizhunthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illai Netri Kanneel Nee Vizhunthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saambal Aayirudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saambal Aayirudaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Thapputhandaa Seithirunthaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Thapputhandaa Seithirunthaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Odi Poiyirudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odi Poiyirudaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Illai Netri Kanneel Nee Vizhunthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illai Netri Kanneel Nee Vizhunthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saambal Aayirudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saambal Aayirudaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Miga Paathugaappaai Veedu Selvaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Miga Paathugaappaai Veedu Selvaar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Adainthaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Adainthaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kodungkoolan Ellaam Petti Paambu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodungkoolan Ellaam Petti Paambu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yennai Arindhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yennai Arindhaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yedai Poda Kallum Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedai Poda Kallum Illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ethir Paarkkum Sollum Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethir Paarkkum Sollum Illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivan Yaaru Endre Solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Yaaru Endre Solla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyirodu Evanum illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirodu Evanum illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yedai Poda Kallum Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedai Poda Kallum Illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ethir Paarkkum Sollum Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethir Paarkkum Sollum Illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivan Yaaru Endre Solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Yaaru Endre Solla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyirodu Evanum illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirodu Evanum illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Marupakkam Marmam Nilavukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marupakkam Marmam Nilavukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mattum Illaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mattum Illaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pala Veru Varnam Vaanavillil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala Veru Varnam Vaanavillil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mattum Illaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mattum Illaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Podhum Vanthu Modhamaattaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Podhum Vanthu Modhamaattaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yennai Arindhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yennai Arindhaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Modhi Paarkka Aasai Pattaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Modhi Paarkka Aasai Pattaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ayyo Tholaindhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyo Tholaindhaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arindhaal Ariddhaal Arindhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arindhaal Ariddhaal Arindhaal"/>
</div>
</pre>
