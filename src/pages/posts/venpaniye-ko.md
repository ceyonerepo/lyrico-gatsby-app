---
title: "venpaniye song lyrics"
album: "Ko"
artist: "Harris Jayaraj"
lyricist: "Pa. Vijay"
director: "K.V. Anand"
path: "/albums/ko-lyrics"
song: "Venpaniye"
image: ../../images/albumart/ko.jpg
date: 2011-04-22
lang: tamil
youtubeLink: "https://www.youtube.com/embed/BGxzbiKIAKg"
type: "love"
singers:
  - Sriram Parthasarathy
  - Bombay Jayashree
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Everything is Chill Now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Everything is Chill Now"/>
</div>
<div class="lyrico-lyrics-wrapper">All is Gonna Be Alright
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="All is Gonna Be Alright"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh I will be There
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh I will be There"/>
</div>
<div class="lyrico-lyrics-wrapper">I Will Be There For You
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Will Be There For You"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Everything is Chill Now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Everything is Chill Now"/>
</div>
<div class="lyrico-lyrics-wrapper">Frozen in Love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Frozen in Love"/>
</div>
<div class="lyrico-lyrics-wrapper">Lets Warm and Close Around Now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lets Warm and Close Around Now"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Venpaniye Munpaniye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venpaniye Munpaniye"/>
</div>
<div class="lyrico-lyrics-wrapper">En Tholil Saainthida Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Tholil Saainthida Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Indrirave Nanpagale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indrirave Nanpagale"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kannil Tholainthida Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kannil Tholainthida Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Irul Nerangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Irul Nerangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Vizhi Eerangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Vizhi Eerangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannaale Theigirathen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaale Theigirathen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Pani Kaalangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Pani Kaalangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Pon Veyil Saaralgal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pon Veyil Saaralgal"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaal Uraigirathen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaal Uraigirathen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Venpaniye Munpaniye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venpaniye Munpaniye"/>
</div>
<div class="lyrico-lyrics-wrapper">En Tholil Saainthida Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Tholil Saainthida Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Indrirave Nanpagale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indrirave Nanpagale"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kannil Tholainthida Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kannil Tholainthida Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Irul Nerangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Irul Nerangal"/>
</div>
<div class="lyrico-lyrics-wrapper">En Vizhi Eerangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Vizhi Eerangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannaale Theigirathen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaale Theigirathen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Pani Kaalangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Pani Kaalangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Pon Veyil Saaralgal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pon Veyil Saaralgal"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaal Uraigirathen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaal Uraigirathen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Imai Kulira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Imai Kulira"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Imai Velira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Imai Velira"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakulle Uranginen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakulle Uranginen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Idhazh Malara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Idhazh Malara"/>
</div>
<div class="lyrico-lyrics-wrapper">Maru Idhazh Ulara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maru Idhazh Ulara"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai Athil Unargiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai Athil Unargiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aathalaal Pagal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aathalaal Pagal"/>
</div>
<div class="lyrico-lyrics-wrapper">Malarnthathu Kadhalaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malarnthathu Kadhalaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaithalaal Idhazh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaithalaal Idhazh"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanainthathu Thoithalaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanainthathu Thoithalaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Inaiyum Inam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inaiyum Inam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Venpaniye Munpaniye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venpaniye Munpaniye"/>
</div>
<div class="lyrico-lyrics-wrapper">En Tholil Saainthida Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Tholil Saainthida Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Indrirave Nanpagale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indrirave Nanpagale"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kannil Tholainthida Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kannil Tholainthida Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Everything is Chill Now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Everything is Chill Now"/>
</div>
<div class="lyrico-lyrics-wrapper">All is Gonna Be Alright
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="All is Gonna Be Alright"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh I will be There
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh I will be There"/>
</div>
<div class="lyrico-lyrics-wrapper">I Will Be There For You
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Will Be There For You"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Everything is Chill Now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Everything is Chill Now"/>
</div>
<div class="lyrico-lyrics-wrapper">Frozen in Love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Frozen in Love"/>
</div>
<div class="lyrico-lyrics-wrapper">Lets Warm and Close Around Now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lets Warm and Close Around Now"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Imaigalil Nanainthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imaigalil Nanainthum"/>
</div>
<div class="lyrico-lyrics-wrapper">Iru Vizhi Nulainthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iru Vizhi Nulainthum"/>
</div>
<div class="lyrico-lyrics-wrapper">Iranginaai Manathulle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iranginaai Manathulle"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muthal Nodi Maranam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthal Nodi Maranam"/>
</div>
<div class="lyrico-lyrics-wrapper">Maru Nodi Jananam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maru Nodi Jananam"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakkulle Enakkulle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkulle Enakkulle"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Evvanam Athil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evvanam Athil"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivaloru Sevvanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivaloru Sevvanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sovitham Athil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sovitham Athil"/>
</div>
<div class="lyrico-lyrics-wrapper">Alainthida Vaa Niram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alainthida Vaa Niram"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanam Kaname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanam Kaname"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Venpaniye Munpaniye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venpaniye Munpaniye"/>
</div>
<div class="lyrico-lyrics-wrapper">En Tholil Saainthida Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Tholil Saainthida Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Indrirave Nanpagale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indrirave Nanpagale"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kannil Tholainthida Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kannil Tholainthida Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Irul Nerangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Irul Nerangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Vizhi Eerangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Vizhi Eerangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannaale Theigirathen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaale Theigirathen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Pani Kaalangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Pani Kaalangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Pon Veyil Saaralgal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pon Veyil Saaralgal"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaal Uraigirathen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaal Uraigirathen"/>
</div>
</pre>
