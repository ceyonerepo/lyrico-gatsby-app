---
title: "yen monam song lyrics"
album: "Selfie"
artist: "GV Prakash Kumar"
lyricist: "Jayshree Mathi Maran"
director: "Mathi Maran"
path: "/albums/selfie-lyrics"
song: "Yen Monam"
image: ../../images/albumart/selfie.jpg
date: 2022-04-01
lang: tamil
youtubeLink: "https://www.youtube.com/embed/UnzOchGLCf0"
type: "love"
singers:
  - Kapil Kapilan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yen Monam Monam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Monam Monam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesi Theerndhe Pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesi Theerndhe Pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vegum Paarvai Engum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vegum Paarvai Engum"/>
</div>
<div class="lyrico-lyrics-wrapper">Neelum Undhan Bimbam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelum Undhan Bimbam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Piralaa En Paadham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Piralaa En Paadham"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan Pinne Nindaadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan Pinne Nindaadum"/>
</div>
<div class="lyrico-lyrics-wrapper">Piraiyodu Vaan Engum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Piraiyodu Vaan Engum "/>
</div>
<div class="lyrico-lyrics-wrapper">Pogum Megam Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogum Megam Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Seril Neeraagi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seril Neeraagi "/>
</div>
<div class="lyrico-lyrics-wrapper">Veril Uyiraagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veril Uyiraagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Meenaai Kuzhainthodinom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meenaai Kuzhainthodinom"/>
</div>
<div class="lyrico-lyrics-wrapper">Naamaaginom Uyire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naamaaginom Uyire"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ven Pani Theeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ven Pani Theeye"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennul Vizhunthaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennul Vizhunthaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Neelum Iravellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelum Iravellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeye Nirainthaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeye Nirainthaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Bommai Kannaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bommai Kannaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Panthaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Panthaadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Udaikkum Un Irukangalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udaikkum Un Irukangalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Moozhgi Pogava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Moozhgi Pogava"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhum Un Kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhum Un Kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Valigal Vara Koodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valigal Vara Koodum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunaiyaaga Ennodu Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunaiyaaga Ennodu Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Uthirvoome Vaa Uyire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uthirvoome Vaa Uyire"/>
</div>
</pre>
