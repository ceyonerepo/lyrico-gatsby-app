---
title: "chaitanya ratham song lyrics"
album: "NTR Mahanayakudu"
artist: "M.M. Keeravani"
lyricist: "Sirivennela Seetharama Sastry"
director: "Krish Jagarlamudi"
path: "/albums/ntr-mahanayakudu-lyrics"
song: "Chaitanya Ratham"
image: ../../images/albumart/ntr-mahanayakudu.jpg
date: 2019-02-22
lang: telugu
youtubeLink: "https://www.youtube.com/embed/l5qeNGey02k"
type: "happy"
singers:
  - M.M. Keeravani
  - Kaala Bhairava
  - Keerthi Sagathia
  - Sai Shivani
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Annaa Ramantu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annaa Ramantu "/>
</div>
<div class="lyrico-lyrics-wrapper">Enni Chethulo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enni Chethulo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottharatha Raayamantu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottharatha Raayamantu "/>
</div>
<div class="lyrico-lyrics-wrapper">Enni Geethalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enni Geethalo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ranunna Shakam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ranunna Shakam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ramanna Shakam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ramanna Shakam"/>
</div>
<div class="lyrico-lyrics-wrapper">Antunna Ratham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antunna Ratham"/>
</div>
<div class="lyrico-lyrics-wrapper">Chaithanya Ratham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chaithanya Ratham"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarikotthapadham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarikotthapadham"/>
</div>
<div class="lyrico-lyrics-wrapper">Janachittharatham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Janachittharatham"/>
</div>
<div class="lyrico-lyrics-wrapper">Neraverchutakai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neraverchutakai"/>
</div>
<div class="lyrico-lyrics-wrapper">Guruveshapadham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Guruveshapadham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jana Jaagruthikai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jana Jaagruthikai"/>
</div>
<div class="lyrico-lyrics-wrapper">Shankaravamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shankaravamai"/>
</div>
<div class="lyrico-lyrics-wrapper">Elugetthina Kathanapu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Elugetthina Kathanapu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuhalanadham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuhalanadham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Annaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Annaa Ramantu Enni Chethulo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annaa Ramantu Enni Chethulo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottharatha Raayamantu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottharatha Raayamantu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enni Geethalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enni Geethalo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennadu Cheragani Ghanathaga Nilichina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennadu Cheragani Ghanathaga Nilichina"/>
</div>
<div class="lyrico-lyrics-wrapper">Manakadha Appude Maricharaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manakadha Appude Maricharaa"/>
</div>
<div class="lyrico-lyrics-wrapper">O O O 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O O O "/>
</div>
<div class="lyrico-lyrics-wrapper">Telugu Vadante Yevadani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Telugu Vadante Yevadani"/>
</div>
<div class="lyrico-lyrics-wrapper">Nosalu Virusthu Alusuga Chustharaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nosalu Virusthu Alusuga Chustharaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kevalam Otese Oka Velugane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kevalam Otese Oka Velugane"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana Vunikivunnadhani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana Vunikivunnadhani"/>
</div>
<div class="lyrico-lyrics-wrapper">An Taa Raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="An Taa Raa"/>
</div>
<div class="lyrico-lyrics-wrapper">O O O
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O O O"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Charithalo Manachevadhunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Charithalo Manachevadhunu"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheripivesinaaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheripivesinaaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">O O O
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O O O"/>
</div>
<div class="lyrico-lyrics-wrapper">Evvaridhi Parihasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evvaridhi Parihasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Emaindhi Mana Rosham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emaindhi Mana Rosham"/>
</div>
<div class="lyrico-lyrics-wrapper">Ani Telugu Jaathi Kosam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ani Telugu Jaathi Kosam"/>
</div>
<div class="lyrico-lyrics-wrapper">Elugetthina Aakrosham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Elugetthina Aakrosham"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhigo Ipude Vinna Mannnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhigo Ipude Vinna Mannnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Anna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Annaa Ramantu Enni Chethulo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annaa Ramantu Enni Chethulo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottharatha Raayamantu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottharatha Raayamantu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enni Geethalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enni Geethalo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Katikarakasi Amavasakaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katikarakasi Amavasakaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Repanee Pasidi Velugundadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Repanee Pasidi Velugundadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Maro Prapanchapu Navodhayaniki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maro Prapanchapu Navodhayaniki"/>
</div>
<div class="lyrico-lyrics-wrapper">Prasudhi Noppula Prabhathama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prasudhi Noppula Prabhathama"/>
</div>
<div class="lyrico-lyrics-wrapper">Shubhala Sedhyapu Mahodhyamaniki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shubhala Sedhyapu Mahodhyamaniki"/>
</div>
<div class="lyrico-lyrics-wrapper">Halanni Etthina Prayathnamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Halanni Etthina Prayathnamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yella Kannilla KalaKallapaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yella Kannilla KalaKallapaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Nammakam Naavai Nadaparaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nammakam Naavai Nadaparaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Janabha Lekkala Janalu Tharapuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Janabha Lekkala Janalu Tharapuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Jagatthu Geliche Pathakama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jagatthu Geliche Pathakama"/>
</div>
<div class="lyrico-lyrics-wrapper">Aneka Prashnala Javabhu Cheppani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aneka Prashnala Javabhu Cheppani"/>
</div>
<div class="lyrico-lyrics-wrapper">Ahanni Anachina Prathapama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahanni Anachina Prathapama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Velasaadalitto Praveshinchunanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velasaadalitto Praveshinchunanta"/>
</div>
<div class="lyrico-lyrics-wrapper">Garibi venta Thirige Prathi Pandagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Garibi venta Thirige Prathi Pandagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">O O O
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O O O"/>
</div>
<div class="lyrico-lyrics-wrapper">Ikkada Nilabadi Dikkula Nanukouni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ikkada Nilabadi Dikkula Nanukouni"/>
</div>
<div class="lyrico-lyrics-wrapper">Dikkarinchu Ghanamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dikkarinchu Ghanamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nippula Vuppenala Nuvvu Vurimithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nippula Vuppenala Nuvvu Vurimithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Nindimpagalama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nindimpagalama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Suswagatham Sankalpama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suswagatham Sankalpama"/>
</div>
<div class="lyrico-lyrics-wrapper">Jejelivee Chaithanyama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jejelivee Chaithanyama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Annaa Ramantu Enni Chethulo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annaa Ramantu Enni Chethulo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottharatha Raayamantu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottharatha Raayamantu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enni Geethalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enni Geethalo"/>
</div>
</pre>
