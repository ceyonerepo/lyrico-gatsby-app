---
title: "local sarakka foreign sarakka song lyrics"
album: "Padaiveeran"
artist: "Karthik Raja"
lyricist: "Priyan"
director: "Dhana Sekaran"
path: "/albums/padaiveeran-lyrics"
song: "Local Sarakka Foreign Sarakka"
image: ../../images/albumart/padaiveeran.jpg
date: 2018-02-02
lang: tamil
youtubeLink: "https://www.youtube.com/embed/1bhysLFKR0o"
type: "happy"
singers:
  - Dhanush
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Mapla edhu local sarakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mapla edhu local sarakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Foreign sarakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Foreign sarakka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Local sarakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Local sarakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Foreign sarakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Foreign sarakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Othi kudicha ellam onnuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Othi kudicha ellam onnuda"/>
</div>
<div class="lyrico-lyrics-wrapper">Keppa kaliya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Keppa kaliya"/>
</div>
<div class="lyrico-lyrics-wrapper">Meenu kozhamba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meenu kozhamba"/>
</div>
<div class="lyrico-lyrics-wrapper">Sethu adicha innum sugam da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sethu adicha innum sugam da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engappan sarakkum soda colorum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engappan sarakkum soda colorum"/>
</div>
<div class="lyrico-lyrics-wrapper">Romba romba dhosthuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba romba dhosthuda"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma naaluperum onna serntha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma naaluperum onna serntha"/>
</div>
<div class="lyrico-lyrics-wrapper">Podu disco dances-u da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podu disco dances-u da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Local sarakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Local sarakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey foreign sarakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey foreign sarakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Othi kudicha ellam onnuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Othi kudicha ellam onnuda"/>
</div>
<div class="lyrico-lyrics-wrapper">Keppa kaliya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Keppa kaliya"/>
</div>
<div class="lyrico-lyrics-wrapper">Meenu kozhamba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meenu kozhamba"/>
</div>
<div class="lyrico-lyrics-wrapper">Sethu adicha innum sugam da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sethu adicha innum sugam da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mapla engala laam vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mapla engala laam vittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Epadi da irrundha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Epadi da irrundha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nimmathiya irrundhenda mapla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nimmathiya irrundhenda mapla"/>
</div>
<div class="lyrico-lyrics-wrapper">Ha aa aa aa summa oolulangattiku da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ha aa aa aa summa oolulangattiku da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey sontha manna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey sontha manna"/>
</div>
<div class="lyrico-lyrics-wrapper">Pirinju pona valikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirinju pona valikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanathula erinja kallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanathula erinja kallu"/>
</div>
<div class="lyrico-lyrics-wrapper">Angaya kedakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Angaya kedakkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yoy yoy yoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yoy yoy yoi"/>
</div>
<div class="lyrico-lyrics-wrapper">Mapla overya over
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mapla overya over"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oorukulla nozhanja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorukulla nozhanja"/>
</div>
<div class="lyrico-lyrics-wrapper">Udambu silukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udambu silukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Boomiyila vizhundha mazha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boomiyila vizhundha mazha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thullithan kuthikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thullithan kuthikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada adida thookki pudida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada adida thookki pudida"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu patti thotti paasamda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu patti thotti paasamda"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga pacha pulla kootam da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga pacha pulla kootam da"/>
</div>
<div class="lyrico-lyrics-wrapper">Pacha pullaiyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pacha pullaiyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Mogarakkattaiya paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mogarakkattaiya paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Namoorula suthikittae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namoorula suthikittae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oohonnuthan vazhanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oohonnuthan vazhanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oohonnuthan vazhanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oohonnuthan vazhanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kootathula koothadichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kootathula koothadichu"/>
</div>
<div class="lyrico-lyrics-wrapper">Disco dances-a podanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Disco dances-a podanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada disco dances-a podanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada disco dances-a podanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Namoorula suthikittae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namoorula suthikittae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oohonnuthan vazhanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oohonnuthan vazhanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oohonnuthan vazhanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oohonnuthan vazhanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kootathula koothadichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kootathula koothadichu"/>
</div>
<div class="lyrico-lyrics-wrapper">Disco dances-a podanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Disco dances-a podanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada disco dances-a podanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada disco dances-a podanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Heh heh heh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heh heh heh"/>
</div>
<div class="lyrico-lyrics-wrapper">Papae papae pappoom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Papae papae pappoom"/>
</div>
<div class="lyrico-lyrics-wrapper">Papae papae pappoom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Papae papae pappoom"/>
</div>
<div class="lyrico-lyrics-wrapper">Papae papae pappoom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Papae papae pappoom"/>
</div>
<div class="lyrico-lyrics-wrapper">Papae papae pappoom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Papae papae pappoom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konjumvala kondavala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjumvala kondavala"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan pirinjen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan pirinjen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennavala ettipudikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennavala ettipudikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaviya thavichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaviya thavichen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada vidinja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada vidinja"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava mogam therinja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava mogam therinja"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan allikittu povenda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan allikittu povenda"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava sollivandha samy da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava sollivandha samy da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Devathaiya kattikittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Devathaiya kattikittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oohonnuthan vazhanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oohonnuthan vazhanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oohonnuthan vazhanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oohonnuthan vazhanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oyiyarama sernthukittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oyiyarama sernthukittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Duetu than padanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Duetu than padanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada duetu than padanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada duetu than padanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Devathaiya kattikittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Devathaiya kattikittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oohonnuthan vazhanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oohonnuthan vazhanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oohonnuthan vazhanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oohonnuthan vazhanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oyiyarama sernthukittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oyiyarama sernthukittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Duetu than padanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Duetu than padanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada duetu than padanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada duetu than padanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Local sarakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Local sarakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Foreign sarakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Foreign sarakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Othi kudicha ellam onnuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Othi kudicha ellam onnuda"/>
</div>
<div class="lyrico-lyrics-wrapper">Keppa kaliya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Keppa kaliya"/>
</div>
<div class="lyrico-lyrics-wrapper">Meenu kozhamba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meenu kozhamba"/>
</div>
<div class="lyrico-lyrics-wrapper">Sethu adicha innum sugam da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sethu adicha innum sugam da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engappan sarakkum soda colorum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engappan sarakkum soda colorum"/>
</div>
<div class="lyrico-lyrics-wrapper">Romba romba dhosthuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba romba dhosthuda"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma naaluperum onna serntha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma naaluperum onna serntha"/>
</div>
<div class="lyrico-lyrics-wrapper">Podu disco dances-u da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podu disco dances-u da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Local sarakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Local sarakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Nana nana naaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nana nana naaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Othi kudicha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Othi kudicha"/>
</div>
<div class="lyrico-lyrics-wrapper">Jinthak thak jintha thaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jinthak thak jintha thaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Keppa kaliya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Keppa kaliya"/>
</div>
<div class="lyrico-lyrics-wrapper">Meenu kozhamba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meenu kozhamba"/>
</div>
<div class="lyrico-lyrics-wrapper">Sethu adicha innum sugam da dei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sethu adicha innum sugam da dei"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalaila sooriyan sullunnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalaila sooriyan sullunnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adikkira varaikkum thoonglamadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adikkira varaikkum thoonglamadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Chennaila vachi…senji uttangaiyya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chennaila vachi…senji uttangaiyya"/>
</div>
</pre>
