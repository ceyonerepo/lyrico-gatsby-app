---
title: "unakaana ulagam song lyrics"
album: "Thalaivii"
artist: "G.V. Prakash Kumar"
lyricist: "Madhan Karky"
director: "A.L. Vijay"
path: "/albums/thalaivii-song-lyrics"
song: "Unakaana Ulagam"
image: ../../images/albumart/thalaivii.jpg
date: 2021-09-10
lang: tamil
youtubeLink: "https://www.youtube.com/embed/WHdM-Zm9Vjg"
type: "happy"
singers:
  - K.S. Harisankar
  - Ananya Bhat
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Unakkana Ulagam Udhikindra Tharunam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkana Ulagam Udhikindra Tharunam"/>
</div>
<div class="lyrico-lyrics-wrapper">Imaikindra Nodiye Niram Maarum Muzhuthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imaikindra Nodiye Niram Maarum Muzhuthum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkana Kanavu Izhakkindra Pozhuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkana Kanavu Izhakkindra Pozhuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pirar Kaanum Kanavu Avai Yaavum Unadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirar Kaanum Kanavu Avai Yaavum Unadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu Kaalam Ezhuthidum Kanakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Kaalam Ezhuthidum Kanakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhai Maatra Thudippathu Etharkku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhai Maatra Thudippathu Etharkku"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan Kaattum Vazhi Undhan Kizhakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan Kaattum Vazhi Undhan Kizhakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Sila Kodi Idhayam Un Izhakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sila Kodi Idhayam Un Izhakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu Thaan Idhu Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Thaan Idhu Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhan Thotram Endre
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan Thotram Endre"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeyo Neero Sollathendrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeyo Neero Sollathendrum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu Thaan Idhu Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Thaan Idhu Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhan Paadhai Endre
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan Paadhai Endre"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanum Kaatrum Sellathengum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanum Kaatrum Sellathengum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thiruppangal Nigazhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiruppangal Nigazhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Irudhayam Negizhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irudhayam Negizhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sila Aayiram Siragugal Soodidadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sila Aayiram Siragugal Soodidadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu Vaanil Yeridadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu Vaanil Yeridadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhidhaga Maaridadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhidhaga Maaridadi"/>
</div>
</pre>
