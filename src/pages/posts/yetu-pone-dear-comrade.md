---
title: "yetu pone song lyrics"
album: "Dear Comrade"
artist: "Justin Prabhakaran"
lyricist: "Krishna Kanth"
director: "Bharat Kamma"
path: "/albums/dear-comrade-lyrics"
song: "Yetu Pone"
image: ../../images/albumart/dear-comrade.jpg
date: 2019-07-26
lang: telugu
youtubeLink: "https://www.youtube.com/embed/Jo0D_u-QSXs"
type: "sad"
singers:
  - Kaala Bhairava
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yetu Pone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yetu Pone"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninu Thalachi Thalachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninu Thalachi Thalachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalalu Vidichi Yetu Pone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalalu Vidichi Yetu Pone"/>
</div>
<div class="lyrico-lyrics-wrapper">Yetu Pone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yetu Pone"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhaku Yedhuru Niliche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhaku Yedhuru Niliche"/>
</div>
<div class="lyrico-lyrics-wrapper">Pilupu Vidichi Yetu Pone Ye Ye Ye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pilupu Vidichi Yetu Pone Ye Ye Ye"/>
</div>
<div class="lyrico-lyrics-wrapper">Bahudhoorapu Dhaarilo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bahudhoorapu Dhaarilo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninu Chere Malupukey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninu Chere Malupukey"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadipiche Dhikkukai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadipiche Dhikkukai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninu Vedhikaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninu Vedhikaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Theguthunna Dhaaramei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theguthunna Dhaaramei"/>
</div>
<div class="lyrico-lyrics-wrapper">Guruthulatho Neyanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Guruthulatho Neyanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana Gaali Patamune
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana Gaali Patamune"/>
</div>
<div class="lyrico-lyrics-wrapper">Ne Negareinaa Aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne Negareinaa Aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aapaleni Kopamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aapaleni Kopamey"/>
</div>
<div class="lyrico-lyrics-wrapper">Maarchaleni Lopamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maarchaleni Lopamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhupuleni Mantanu Nenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhupuleni Mantanu Nenu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vacchi Kougilinchavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vacchi Kougilinchavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Munchai Aavahinchavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munchai Aavahinchavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nidhare Raadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nidhare Raadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanneetike Adde Pade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneetike Adde Pade"/>
</div>
<div class="lyrico-lyrics-wrapper">Kala Marakale Cheragaveii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kala Marakale Cheragaveii"/>
</div>
<div class="lyrico-lyrics-wrapper">Padileche Payanaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padileche Payanaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Orpante Nerpenule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orpante Nerpenule"/>
</div>
<div class="lyrico-lyrics-wrapper">Yekantham Saayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yekantham Saayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Shanthamuke Adigithimeii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shanthamuke Adigithimeii"/>
</div>
<div class="lyrico-lyrics-wrapper">Panti Biguvuna Baadhanunche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panti Biguvuna Baadhanunche"/>
</div>
<div class="lyrico-lyrics-wrapper">Navvuthunna Ninu Thalachey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Navvuthunna Ninu Thalachey"/>
</div>
<div class="lyrico-lyrics-wrapper">Yemaina Naatho Veravani Theerokate
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yemaina Naatho Veravani Theerokate"/>
</div>
<div class="lyrico-lyrics-wrapper">Velloddhe Velloddhe Nuvve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velloddhe Velloddhe Nuvve"/>
</div>
<div class="lyrico-lyrics-wrapper">Untane Thakkunde Nene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Untane Thakkunde Nene"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Raktham Nee Vennele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Raktham Nee Vennele"/>
</div>
<div class="lyrico-lyrics-wrapper">Paduthunte Nenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paduthunte Nenu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenoka Yegase Uppena Migelene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenoka Yegase Uppena Migelene"/>
</div>
<div class="lyrico-lyrics-wrapper">Chiraakaina Raalchanu Le Raalchanu Le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chiraakaina Raalchanu Le Raalchanu Le"/>
</div>
<div class="lyrico-lyrics-wrapper">Kurulanu Saitham Baadhinchani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurulanu Saitham Baadhinchani"/>
</div>
<div class="lyrico-lyrics-wrapper">O Gaale Avanaa Avanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Gaale Avanaa Avanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedhi Leni Maasamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedhi Leni Maasamai"/>
</div>
<div class="lyrico-lyrics-wrapper">Yandamaavi Theeramai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yandamaavi Theeramai"/>
</div>
<div class="lyrico-lyrics-wrapper">Undalenu Oopiraaputhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undalenu Oopiraaputhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Inka Neeku Dhooramai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inka Neeku Dhooramai"/>
</div>
<div class="lyrico-lyrics-wrapper">Inka Neeku Dhooramai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inka Neeku Dhooramai"/>
</div>
<div class="lyrico-lyrics-wrapper">Yetu Pone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yetu Pone"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninu Thalachi Thalachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninu Thalachi Thalachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalalu Vidichi Yetu Pone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalalu Vidichi Yetu Pone"/>
</div>
<div class="lyrico-lyrics-wrapper">Yetu Pone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yetu Pone"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhaku Yedhuru Niliche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhaku Yedhuru Niliche"/>
</div>
<div class="lyrico-lyrics-wrapper">Pilupu Vidichi Yetu Pone Ye Ye Ye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pilupu Vidichi Yetu Pone Ye Ye Ye"/>
</div>
</pre>
