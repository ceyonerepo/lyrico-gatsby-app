---
title: "katravai patravai song lyrics"
album: "Kaala"
artist: "Santhosh Narayanan"
lyricist: "Kabilan"
director: "Pa. Ranjith"
path: "/albums/kaala-lyrics"
song: "Katravai Patravai"
image: ../../images/albumart/kaala.jpg
date: 2018-06-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/GG7UUQ7IJDo"
type: "happy"
singers:
  - Yogi B
  - Arunraja Kamaraj
  - Roshan Jamrock
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Haaa yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haaa yeah"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oththa thalai raavana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oththa thalai raavana"/>
</div>
<div class="lyrico-lyrics-wrapper">Paththu thalai avudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paththu thalai avudaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oththaiyila nikkura vengai da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oththaiyila nikkura vengai da"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhillirundha moththama vaangada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhillirundha moththama vaangada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eluchigalai puratchiyaaka vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eluchigalai puratchiyaaka vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Varappugalai vayalgal aaka vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varappugalai vayalgal aaka vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Varumai enum noyai theerka vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varumai enum noyai theerka vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adimaithanam neeyum poka vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adimaithanam neeyum poka vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Puzhuthu pona manasula inga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puzhuthu pona manasula inga"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudichi pona azhukunga thanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudichi pona azhukunga thanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Thee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thee"/>
</div>
<div class="lyrico-lyrics-wrapper">Veluthu vaangum manushanga enga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veluthu vaangum manushanga enga"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Onu serndhu nerikkanum sanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onu serndhu nerikkanum sanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeah"/>
</div>
<div class="lyrico-lyrics-wrapper">We coming calculating
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="We coming calculating"/>
</div>
<div class="lyrico-lyrics-wrapper">Every single moment
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Every single moment"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnidam ullathellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnidam ullathellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedi thelindhidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedi thelindhidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pack of wolves on the loose
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pack of wolves on the loose"/>
</div>
<div class="lyrico-lyrics-wrapper">And we howling at the moon
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="And we howling at the moon"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ullathil ottrumaiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullathil ottrumaiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">Koodi thirindhidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koodi thirindhidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Check mate
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Check mate"/>
</div>
<div class="lyrico-lyrics-wrapper">White facade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="White facade"/>
</div>
<div class="lyrico-lyrics-wrapper">Yo mama should have told you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yo mama should have told you"/>
</div>
<div class="lyrico-lyrics-wrapper">Not to mess with my chawl
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Not to mess with my chawl"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai veliyiduIrulai baliyidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai veliyiduIrulai baliyidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Go"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaiyum mannaiyum vendru vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaiyum mannaiyum vendru vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thee ratha or thevaiyai kondu vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thee ratha or thevaiyai kondu vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Noor aayiram aandugal podhumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Noor aayiram aandugal podhumae"/>
</div>
<div class="lyrico-lyrics-wrapper">On draagave maaruvaai seeruvaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="On draagave maaruvaai seeruvaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaala thee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaala thee"/>
</div>
<div class="lyrico-lyrics-wrapper">Thee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oththa thalai raavana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oththa thalai raavana"/>
</div>
<div class="lyrico-lyrics-wrapper">Kya re?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kya re?"/>
</div>
<div class="lyrico-lyrics-wrapper">Setting ah?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Setting ah?"/>
</div>
<div class="lyrico-lyrics-wrapper">Oththaiyila nikkura vengai da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oththaiyila nikkura vengai da"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhilluruntha mothamaa vaangalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhilluruntha mothamaa vaangalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Try-na bring me down for nothing
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Try-na bring me down for nothing"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Let me tell you something
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Let me tell you something"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">That I ain’t going down without a fight
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="That I ain’t going down without a fight"/>
</div>
<div class="lyrico-lyrics-wrapper">Be afraid of the dark black night
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Be afraid of the dark black night"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ei ei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ei ei"/>
</div>
<div class="lyrico-lyrics-wrapper">I’m the definition of great
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I’m the definition of great"/>
</div>
<div class="lyrico-lyrics-wrapper">Wait
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wait"/>
</div>
<div class="lyrico-lyrics-wrapper">Gotta stay humble
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gotta stay humble"/>
</div>
<div class="lyrico-lyrics-wrapper">But I am victorious in every single way
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="But I am victorious in every single way"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey saet
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey saet"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ethiri yaarum norungi poga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethiri yaarum norungi poga"/>
</div>
<div class="lyrico-lyrics-wrapper">Adithu norukku nodiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adithu norukku nodiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathari pathari sethari oda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathari pathari sethari oda"/>
</div>
<div class="lyrico-lyrics-wrapper">Siru narigalai oda vidu ini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru narigalai oda vidu ini"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaiyum mannaiyum vendru vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaiyum mannaiyum vendru vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thee ratha or thevaiyai kondu vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thee ratha or thevaiyai kondu vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Noor aayiram aandugal podhume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Noor aayiram aandugal podhume"/>
</div>
<div class="lyrico-lyrics-wrapper">On draagave maaruvaai seeruvaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="On draagave maaruvaai seeruvaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Katravai patravai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katravai patravai"/>
</div>
<div class="lyrico-lyrics-wrapper">Oththa thalai raavana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oththa thalai raavana"/>
</div>
<div class="lyrico-lyrics-wrapper">Oththaiyila nikkura vengai da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oththaiyila nikkura vengai da"/>
</div>
<div class="lyrico-lyrics-wrapper">Heyyyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyyyy"/>
</div>
</pre>
