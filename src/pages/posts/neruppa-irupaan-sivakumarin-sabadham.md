---
title: "neruppa irupaan song lyrics"
album: "Sivakumarin Sabadham"
artist: "Hiphop Tamizha"
lyricist: "Hiphop Tamizha"
director: "Hiphop Tamizha"
path: "/albums/sivakumarin-sabadham-lyrics"
song: "Neruppa Irupaan"
image: ../../images/albumart/sivakumarin-sabadham.jpg
date: 2021-09-30
lang: tamil
youtubeLink: "https://www.youtube.com/embed/tYfo3aSLqHs"
type: "love"
singers:
  - Padmalatha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">En Kanne Pattudunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kanne Pattudunu"/>
</div>
<div class="lyrico-lyrics-wrapper">En Aasaiya Pothi Vechen Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Aasaiya Pothi Vechen Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Neruppa Iruppan Siricha Therippan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neruppa Iruppan Siricha Therippan"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kanne Pattudunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kanne Pattudunu"/>
</div>
<div class="lyrico-lyrics-wrapper">En Aasaiya Pothi Vechen Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Aasaiya Pothi Vechen Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Neruppa Iruppan Siricha Therippan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neruppa Iruppan Siricha Therippan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Othayila Poravare Ottikolla Naanum Vaaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Othayila Poravare Ottikolla Naanum Vaaren"/>
</div>
<div class="lyrico-lyrics-wrapper">Kooda Ennai Koottikittu Pooveero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooda Ennai Koottikittu Pooveero"/>
</div>
<div class="lyrico-lyrics-wrapper">Othayila Poravare Ottikolla Naanum Vaaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Othayila Poravare Ottikolla Naanum Vaaren"/>
</div>
<div class="lyrico-lyrics-wrapper">Kooda Ennai Koottikittu Pooveero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooda Ennai Koottikittu Pooveero"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Kanne Pattudunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kanne Pattudunu"/>
</div>
<div class="lyrico-lyrics-wrapper">En Aasaiya Pothi Vechen Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Aasaiya Pothi Vechen Da"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kanne Pattudunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kanne Pattudunu"/>
</div>
<div class="lyrico-lyrics-wrapper">En Aasaiya Pothi Vechen Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Aasaiya Pothi Vechen Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Neruppa Iruppan Siricha Therippan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neruppa Iruppan Siricha Therippan"/>
</div>
<div class="lyrico-lyrics-wrapper">Neruppa Iruppan Siricha Therippan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neruppa Iruppan Siricha Therippan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avan Kannu Rendum Gadigaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan Kannu Rendum Gadigaram"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kaalam Adhula Thadumaarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kaalam Adhula Thadumaarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paathukittu Pesikittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paathukittu Pesikittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Neram Poora Pari Pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neram Poora Pari Pogum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avan Kovam Kooda Azhagagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan Kovam Kooda Azhagagum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thala Kodhum Style Lil Azhamodhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thala Kodhum Style Lil Azhamodhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayam Muzhudhum Avan Thaan Irukkan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam Muzhudhum Avan Thaan Irukkan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennatha Seiya Naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennatha Seiya Naanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanna Pinna Kadhal Irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna Pinna Kadhal Irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullam Full Ah Ve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullam Full Ah Ve"/>
</div>
<div class="lyrico-lyrics-wrapper">Adha Unakku Mattum Tharuvenda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adha Unakku Mattum Tharuvenda"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Thalla Solladhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Thalla Solladhey"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanna Kuzhiyil Valiyum Vetkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna Kuzhiyil Valiyum Vetkam"/>
</div>
<div class="lyrico-lyrics-wrapper">Killi Selladhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Killi Selladhey"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Arugil Kazhiyum Pozhuthil Sorgam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Arugil Kazhiyum Pozhuthil Sorgam"/>
</div>
<div class="lyrico-lyrics-wrapper">Needhan Ellaame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Needhan Ellaame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neruppa Iruppan Siricha Therippan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neruppa Iruppan Siricha Therippan"/>
</div>
<div class="lyrico-lyrics-wrapper">Neruppa Iruppan Siricha Therippan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neruppa Iruppan Siricha Therippan"/>
</div>
</pre>
