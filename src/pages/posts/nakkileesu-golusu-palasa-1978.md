---
title: "nakkileesu golusu song lyrics"
album: "Palasa 1978"
artist: "Raghu Kunche"
lyricist: "Uttarandhra Janapadam"
director: "Karuna Kumar"
path: "/albums/palasa-1978-lyrics"
song: "Nakkileesu Golusu"
image: ../../images/albumart/palasa-1978.jpg
date: 2020-03-06
lang: telugu
youtubeLink: "https://www.youtube.com/embed/o4ox_7oLabg"
type: "happy"
singers:
  - Raghu Kunche
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nee Pakkana Paddaadhi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Pakkana Paddaadhi "/>
</div>
<div class="lyrico-lyrics-wrapper">Ledhu Soodave Pillaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ledhu Soodave Pillaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naadhi Nakkileesu Golusu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naadhi Nakkileesu Golusu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Pakkana Paddaadhi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Pakkana Paddaadhi "/>
</div>
<div class="lyrico-lyrics-wrapper">Ledhu Soodave Pillaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ledhu Soodave Pillaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naadhi Nakkileesu Golusu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naadhi Nakkileesu Golusu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Pakkana Paddaadhi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Pakkana Paddaadhi "/>
</div>
<div class="lyrico-lyrics-wrapper">Ledhu Soodave Pillaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ledhu Soodave Pillaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naadhi Nakkileesu Golusu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naadhi Nakkileesu Golusu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Pakkana Paddaadhi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Pakkana Paddaadhi "/>
</div>
<div class="lyrico-lyrics-wrapper">Ledhu Soodave Pillaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ledhu Soodave Pillaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naadhi Nakkileesu Golusu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naadhi Nakkileesu Golusu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mee Baava Gaaru Vachheti Vela Aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mee Baava Gaaru Vachheti Vela Aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeku Banthi Poolu Thechheti Vela Aa Aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeku Banthi Poolu Thechheti Vela Aa Aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mee Baava Gaaru Vachheti Vela Aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mee Baava Gaaru Vachheti Vela Aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeku Banthi Poolu Thechheti Velaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeku Banthi Poolu Thechheti Velaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mee Maridhi Gaaru Vachheti Vela Osi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mee Maridhi Gaaru Vachheti Vela Osi"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeku Mandhaaram Thechheti Vela Aa Aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeku Mandhaaram Thechheti Vela Aa Aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mee Maridhi Gaaru Vachheti Vela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mee Maridhi Gaaru Vachheti Vela"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeku Mandhaaram Thechheti Vela Aa Aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeku Mandhaaram Thechheti Vela Aa Aa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mee Maavagaaru Aa Maavagaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mee Maavagaaru Aa Maavagaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Mee Maavagaaru Vachheti Vela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mee Maavagaaru Vachheti Vela"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeku Marumallelu Thechheti Vela Aa Aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeku Marumallelu Thechheti Vela Aa Aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mee Maavagaaru Vachheti Vela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mee Maavagaaru Vachheti Vela"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeku Marumallelu Thechheti Vela Aa Aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeku Marumallelu Thechheti Vela Aa Aa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naadhi Naadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naadhi Naadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naadhi Nakkileesu Golusu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naadhi Nakkileesu Golusu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Pakkana Paddaadhi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Pakkana Paddaadhi "/>
</div>
<div class="lyrico-lyrics-wrapper">Ledhu Soodave Pillaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ledhu Soodave Pillaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naadhi Nakkileesu Golusu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naadhi Nakkileesu Golusu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Pakkana Paddaadhi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Pakkana Paddaadhi "/>
</div>
<div class="lyrico-lyrics-wrapper">Ledhu Soodave Pillaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ledhu Soodave Pillaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naadhi Nakkileesu Golusu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naadhi Nakkileesu Golusu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeku Kadiyaalu Thechheti Velaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeku Kadiyaalu Thechheti Velaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeku Ponakammalu Thechheti Velaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeku Ponakammalu Thechheti Velaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeku Kadiyaalu Thechheti Velaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeku Kadiyaalu Thechheti Velaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeku Ponakammalu Thechheti Velaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeku Ponakammalu Thechheti Velaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeku Bottubilla Thechheti Velaa Hoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeku Bottubilla Thechheti Velaa Hoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhi Pettukuni Vachheti Velaa Chee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhi Pettukuni Vachheti Velaa Chee"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeku Bottubilla Thechheti Velaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeku Bottubilla Thechheti Velaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhi Pettukuni Vachheti Velaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhi Pettukuni Vachheti Velaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeku Pattu Cheera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeku Pattu Cheera"/>
</div>
<div class="lyrico-lyrics-wrapper">Abbabboo Pattu Cheera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Abbabboo Pattu Cheera"/>
</div>
<div class="lyrico-lyrics-wrapper">Pillaa Pattucheera Thechheti Vela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pillaa Pattucheera Thechheti Vela"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhi Kattukuni Vachheti Velaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhi Kattukuni Vachheti Velaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pillaa Pattucheera Thechheti Vela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pillaa Pattucheera Thechheti Vela"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhi Kattukuni Vachheti Velaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhi Kattukuni Vachheti Velaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naadhi Naadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naadhi Naadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naadhi Nakkileesu Golusu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naadhi Nakkileesu Golusu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Pakkana Paddaadhi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Pakkana Paddaadhi "/>
</div>
<div class="lyrico-lyrics-wrapper">Ledhu Soodave Pillaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ledhu Soodave Pillaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naadhi Nakkileesu Golusu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naadhi Nakkileesu Golusu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey..! Pakkana Paddaadhi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey..! Pakkana Paddaadhi "/>
</div>
<div class="lyrico-lyrics-wrapper">Ledhu Soodave Pillaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ledhu Soodave Pillaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naadhi Nakkileesu Golusu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naadhi Nakkileesu Golusu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Pakkana Paddaadhi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Pakkana Paddaadhi "/>
</div>
<div class="lyrico-lyrics-wrapper">Ledhu Soodave Pillaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ledhu Soodave Pillaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naadhi Nakkileesu Golusu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naadhi Nakkileesu Golusu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naadhi Naadhi Naadhi Naadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naadhi Naadhi Naadhi Naadhi"/>
</div>
</pre>
