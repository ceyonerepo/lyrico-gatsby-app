---
title: "enge enadhuyir enge song lyrics"
album: "EPCO 302"
artist: "Alex Paul"
lyricist: "Muthu Vijayan"
director: "Salangai Durai"
path: "/albums/epco-302-lyrics"
song: "Enge Enadhuyir Enge"
image: ../../images/albumart/epco-302.jpg
date: 2021-12-31
lang: tamil
youtubeLink: "https://www.youtube.com/embed/MirU7R_XBWA"
type: "sad"
singers:
  - P.N. Gireesh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">mazhai adithal malai seriyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mazhai adithal malai seriyum"/>
</div>
<div class="lyrico-lyrics-wrapper">puyal adithal maram seriyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puyal adithal maram seriyum"/>
</div>
<div class="lyrico-lyrics-wrapper">katradithal elai seriyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="katradithal elai seriyum"/>
</div>
<div class="lyrico-lyrics-wrapper">alai adithal karai seriyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alai adithal karai seriyum"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhal adithal uyire seriyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhal adithal uyire seriyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">enge enge enge enge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enge enge enge enge"/>
</div>
<div class="lyrico-lyrics-wrapper">enathuyir enge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enathuyir enge"/>
</div>
<div class="lyrico-lyrics-wrapper">enge enge enathuyir enge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enge enge enathuyir enge"/>
</div>
<div class="lyrico-lyrics-wrapper">enge enge enathuravu enge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enge enge enathuravu enge"/>
</div>
<div class="lyrico-lyrics-wrapper">ithuvarai vizhi sertha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithuvarai vizhi sertha"/>
</div>
<div class="lyrico-lyrics-wrapper">kanavu enge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavu enge"/>
</div>
<div class="lyrico-lyrics-wrapper">idhayathil idam piditha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhayathil idam piditha"/>
</div>
<div class="lyrico-lyrics-wrapper">sugam enge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sugam enge"/>
</div>
<div class="lyrico-lyrics-wrapper">ninaivu ennum nerupinile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninaivu ennum nerupinile"/>
</div>
<div class="lyrico-lyrics-wrapper">thinam erinthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thinam erinthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">enge enge enathuyir enge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enge enge enathuyir enge"/>
</div>
<div class="lyrico-lyrics-wrapper">enge enge enathuravu enge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enge enge enathuravu enge"/>
</div>
<div class="lyrico-lyrics-wrapper">ithuvarai vizhi sertha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithuvarai vizhi sertha"/>
</div>
<div class="lyrico-lyrics-wrapper">kanavu enge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavu enge"/>
</div>
<div class="lyrico-lyrics-wrapper">idhayathil idam piditha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhayathil idam piditha"/>
</div>
<div class="lyrico-lyrics-wrapper">sugam enge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sugam enge"/>
</div>
<div class="lyrico-lyrics-wrapper">ninaivu ennum nerupinile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninaivu ennum nerupinile"/>
</div>
<div class="lyrico-lyrics-wrapper">thinam erinthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thinam erinthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">evarukum manathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="evarukum manathil"/>
</div>
<div class="lyrico-lyrics-wrapper">idam illai endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idam illai endru"/>
</div>
<div class="lyrico-lyrics-wrapper">enakule vanthai poiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enakule vanthai poiya"/>
</div>
<div class="lyrico-lyrics-wrapper">iruvarin uyirum udalukul ilai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iruvarin uyirum udalukul ilai"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhalil endrai poiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhalil endrai poiya"/>
</div>
<div class="lyrico-lyrics-wrapper">adi erandhe pirantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi erandhe pirantha"/>
</div>
<div class="lyrico-lyrics-wrapper">kulanthai pole en kadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulanthai pole en kadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">sathal anathu enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sathal anathu enna"/>
</div>
<div class="lyrico-lyrics-wrapper">unnai pirintha nodiyinile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai pirintha nodiyinile"/>
</div>
<div class="lyrico-lyrics-wrapper">iranthu vitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iranthu vitten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">enge enge enathuyir enge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enge enge enathuyir enge"/>
</div>
<div class="lyrico-lyrics-wrapper">enge enge enathuravu enge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enge enge enathuravu enge"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">marathadi nizhalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="marathadi nizhalil"/>
</div>
<div class="lyrico-lyrics-wrapper">manam vitu pesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manam vitu pesi"/>
</div>
<div class="lyrico-lyrics-wrapper">sonnathu ellam poiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sonnathu ellam poiya"/>
</div>
<div class="lyrico-lyrics-wrapper">thinam iru murai than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thinam iru murai than"/>
</div>
<div class="lyrico-lyrics-wrapper">kanavinil varuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavinil varuven"/>
</div>
<div class="lyrico-lyrics-wrapper">endrathu ellam poiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="endrathu ellam poiya"/>
</div>
<div class="lyrico-lyrics-wrapper">enathu pirantha naalin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enathu pirantha naalin"/>
</div>
<div class="lyrico-lyrics-wrapper">chinna parisai ne thantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chinna parisai ne thantha"/>
</div>
<div class="lyrico-lyrics-wrapper">antha mutham poiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antha mutham poiya"/>
</div>
<div class="lyrico-lyrics-wrapper">unnai pirintha nodiyinile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai pirintha nodiyinile"/>
</div>
<div class="lyrico-lyrics-wrapper">iranthu viten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iranthu viten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">enge enge enge enge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enge enge enge enge"/>
</div>
<div class="lyrico-lyrics-wrapper">enathuyir enge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enathuyir enge"/>
</div>
<div class="lyrico-lyrics-wrapper">enge enge enathuravu enge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enge enge enathuravu enge"/>
</div>
<div class="lyrico-lyrics-wrapper">ithuvarai vizhi sertha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithuvarai vizhi sertha"/>
</div>
<div class="lyrico-lyrics-wrapper">kanavu enge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavu enge"/>
</div>
<div class="lyrico-lyrics-wrapper">idhayathil idam piditha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhayathil idam piditha"/>
</div>
<div class="lyrico-lyrics-wrapper">sugam enge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sugam enge"/>
</div>
<div class="lyrico-lyrics-wrapper">ninaivu ennum nerupinile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninaivu ennum nerupinile"/>
</div>
<div class="lyrico-lyrics-wrapper">thinam erinthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thinam erinthen"/>
</div>
</pre>
