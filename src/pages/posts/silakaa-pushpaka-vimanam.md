---
title: "silakaa song lyrics"
album: "Pushpaka Vimanam"
artist: "Ram Miriyala - Sidharth Sadasivuni - Mark K Robin - Amit N Dasani"
lyricist: "Ram Miriyala - Anand Gurram"
director: "Damodara"
path: "/albums/pushpaka-vimanam-lyrics"
song: "Silakaa"
image: ../../images/albumart/pushpaka-vimanam.jpg
date: 2021-11-12
lang: telugu
youtubeLink: "https://www.youtube.com/embed/hfIy_efBuT8?start=35"
type: "happy"
singers:
  - Ram Miriyala
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Silakaa… yegiripoyavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silakaa… yegiripoyavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Asalanni edisesi yenaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asalanni edisesi yenaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Silakaa.. sinnaboinde sittigunde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silakaa.. sinnaboinde sittigunde"/>
</div>
<div class="lyrico-lyrics-wrapper">Pitta nuvvu leka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pitta nuvvu leka"/>
</div>
<div class="lyrico-lyrics-wrapper">Bangaru silakammo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bangaru silakammo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee alaka denikammo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee alaka denikammo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee allibelli atalinka apavammo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee allibelli atalinka apavammo"/>
</div>
<div class="lyrico-lyrics-wrapper">Gundeni tappujari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundeni tappujari"/>
</div>
<div class="lyrico-lyrics-wrapper">Pandanukunnavemo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pandanukunnavemo"/>
</div>
<div class="lyrico-lyrics-wrapper">Inkentha korukuthave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inkentha korukuthave"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaali chupavammo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaali chupavammo"/>
</div>
<div class="lyrico-lyrics-wrapper">Silakaa… yegiripoyavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silakaa… yegiripoyavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Asalanni edisesi yenaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asalanni edisesi yenaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Silakaa.. yegiripoyava ye…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silakaa.. yegiripoyava ye…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninna monnadaka kulukuladinave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninna monnadaka kulukuladinave"/>
</div>
<div class="lyrico-lyrics-wrapper">Enthalone yetta jaripoinave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enthalone yetta jaripoinave"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu gurthukochi koter yesinane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu gurthukochi koter yesinane"/>
</div>
<div class="lyrico-lyrics-wrapper">Mataladaleka paata rasinane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mataladaleka paata rasinane"/>
</div>
<div class="lyrico-lyrics-wrapper">Premalona nenu devadasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Premalona nenu devadasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gullu kattaleni ramadasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gullu kattaleni ramadasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paata rasukochha first classlu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paata rasukochha first classlu"/>
</div>
<div class="lyrico-lyrics-wrapper">Padamante avutha yesudasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padamante avutha yesudasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Okka chance icchi chudave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okka chance icchi chudave"/>
</div>
<div class="lyrico-lyrics-wrapper">Inko chance adigite cheppu tiyave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inko chance adigite cheppu tiyave"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannu vidichi assalundalenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannu vidichi assalundalenu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannu virigi meda padda vadala ninne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannu virigi meda padda vadala ninne"/>
</div>
<div class="lyrico-lyrics-wrapper">Cool down my boy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cool down my boy"/>
</div>
<div class="lyrico-lyrics-wrapper">Silakaa… yegiripoyavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silakaa… yegiripoyavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Asalanni edisesi yenaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asalanni edisesi yenaka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Cheliya chelako
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheliya chelako"/>
</div>
<div class="lyrico-lyrics-wrapper">Atta techina kotha chera nachako
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atta techina kotha chera nachako"/>
</div>
<div class="lyrico-lyrics-wrapper">Bava techina mallepulu mudavako
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bava techina mallepulu mudavako"/>
</div>
<div class="lyrico-lyrics-wrapper">Burra pitta burra pitta turrumannado
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Burra pitta burra pitta turrumannado"/>
</div>
<div class="lyrico-lyrics-wrapper">Vadupelli punthallo chusa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadupelli punthallo chusa"/>
</div>
<div class="lyrico-lyrics-wrapper">Singaraye konda jatharlo vetiki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Singaraye konda jatharlo vetiki"/>
</div>
<div class="lyrico-lyrics-wrapper">Hyderabadu poye maitrivanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hyderabadu poye maitrivanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Centerlo love missing ani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Centerlo love missing ani"/>
</div>
<div class="lyrico-lyrics-wrapper">pompulatelu pancha tondaremledu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pompulatelu pancha tondaremledu"/>
</div>
<div class="lyrico-lyrics-wrapper">Time tesukoni ola ekki rave nelaveni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Time tesukoni ola ekki rave nelaveni"/>
</div>
<div class="lyrico-lyrics-wrapper">Okkasari ninnu chusukoni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okkasari ninnu chusukoni"/>
</div>
<div class="lyrico-lyrics-wrapper">yenni sarlaina sacchipove
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yenni sarlaina sacchipove"/>
</div>
<div class="lyrico-lyrics-wrapper">Kunavaram konaloki podame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kunavaram konaloki podame"/>
</div>
<div class="lyrico-lyrics-wrapper">Goru vankalalle janta kadadame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Goru vankalalle janta kadadame"/>
</div>
<div class="lyrico-lyrics-wrapper">Rella pakalu allukoni vechaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rella pakalu allukoni vechaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Malli malli okkatapodame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malli malli okkatapodame"/>
</div>
<div class="lyrico-lyrics-wrapper">Silakaa… ye… silakaa…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silakaa… ye… silakaa…"/>
</div>
<div class="lyrico-lyrics-wrapper">Silakaa… ekkadunnagani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silakaa… ekkadunnagani"/>
</div>
<div class="lyrico-lyrics-wrapper">Guthikochi valipoye silakaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Guthikochi valipoye silakaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Silakaa sinnaboindi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silakaa sinnaboindi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sitti gunde pitta nuvvu leka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sitti gunde pitta nuvvu leka"/>
</div>
</pre>
