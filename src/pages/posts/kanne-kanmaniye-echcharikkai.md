---
title: "kanne kanmaniye song lyrics"
album: "Echcharikkai"
artist: "Sundaramurthy KS"
lyricist: "Kabilan"
director: "Sarjun KM"
path: "/albums/echcharikkai-lyrics"
song: "Kanne Kanmaniye"
image: ../../images/albumart/echcharikkai.jpg
date: 2018-08-24
lang: tamil
youtubeLink: "https://www.youtube.com/embed/J8dw8UCk7XU"
type: "melody"
singers:
  - Anand Aravindakshan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kannae kanmaniyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannae kanmaniyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaro aarariro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaro aarariro"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannae kanmaniyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannae kanmaniyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaro aarariro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaro aarariro"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannae kanmaniyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannae kanmaniyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaro aarariro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaro aarariro"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannae kanmaniyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannae kanmaniyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paadhai maariyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadhai maariyae"/>
</div>
<div class="lyrico-lyrics-wrapper">En payanam ponathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En payanam ponathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unthan poomugam ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unthan poomugam ivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagam aanathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagam aanathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaniyaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaniyaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan vazhum nerathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan vazhum nerathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunaiyaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunaiyaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthaayae kaalathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthaayae kaalathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu neeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu neeyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannae kanmaniyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannae kanmaniyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaro aarariro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaro aarariro"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanna ponmayilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanna ponmayilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaro aarariro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaro aarariro"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinna punnagaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna punnagaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaro aarariro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaro aarariro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannae kanmaniyaeaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannae kanmaniyaeaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannae kanmaniyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannae kanmaniyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaro aarariro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaro aarariro"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanna ponmayilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanna ponmayilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaro aarariro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaro aarariro"/>
</div>
<div class="lyrico-lyrics-wrapper">Annai pol azhagae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annai pol azhagae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaro aarariro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaro aarariro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhkai mudiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkai mudiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Neram paarthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neram paarthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhadhae siru dhevathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhadhae siru dhevathai"/>
</div>
<div class="lyrico-lyrics-wrapper">Theivam solli ketka maaten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theivam solli ketka maaten"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai naan vittu povathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai naan vittu povathai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pesum pechu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesum pechu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pillai thamizhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pillai thamizhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Arinthenae indru naanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arinthenae indru naanadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaasam veesum vanna poovin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaasam veesum vanna poovin"/>
</div>
<div class="lyrico-lyrics-wrapper">Varisai neeyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varisai neeyadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanmaniyaeae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanmaniyaeae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannurangu kannurangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannurangu kannurangu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanmaniyaeaee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanmaniyaeaee"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaaaa nee kannurangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaaa nee kannurangu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanmaniyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanmaniyae"/>
</div>
</pre>
