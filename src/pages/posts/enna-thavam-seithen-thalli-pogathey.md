---
title: "enna thavam seithen song lyrics"
album: "Thalli Pogathey"
artist: "Gopi Sunder"
lyricist: "Kabilan Vairamuthu"
director: "R. Kannan"
path: "/albums/thalli-pogathey-song-lyrics"
song: "Enna Thavam Seithen"
image: ../../images/albumart/thalli-pogathey.jpg
date: 2021-12-24
lang: tamil
youtubeLink: "https://www.youtube.com/embed/HB-dzUl5KTE"
type: "love"
singers:
  - Sathya Prakash
  - Saindhavi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Oh ohho oo oho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh ohho oo oho"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh ohho oo oho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh ohho oo oho"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooho ooho oo oo oho ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooho ooho oo oo oho ho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh ohho oo oho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh ohho oo oho"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh ohho oo oho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh ohho oo oho"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooho ooho oo oo oho ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooho ooho oo oo oho ho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna thavam seithean
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna thavam seithean"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai thedi vanthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai thedi vanthaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aga magizhvena aagiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aga magizhvena aagiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veenai thanthi ennil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veenai thanthi ennil"/>
</div>
<div class="lyrico-lyrics-wrapper">Veesum thendral neeyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veesum thendral neeyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Isai arivithu pogiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Isai arivithu pogiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maayaa neeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maayaa neeyae"/>
</div>
<div class="lyrico-lyrics-wrapper">En idhazh puthai vinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En idhazh puthai vinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oyaa oyaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oyaa oyaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhi munai vizhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhi munai vizhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey nallavai allavai enbavai neengi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey nallavai allavai enbavai neengi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pallavi pallavi aanathu nenjae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pallavi pallavi aanathu nenjae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh yeah oh yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh yeah oh yeah"/>
</div>
<div class="lyrico-lyrics-wrapper">Pallavi pallavi aanathu nenjae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pallavi pallavi aanathu nenjae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kallena kallena ooriya ullam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallena kallena ooriya ullam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sillena sillena paayuthu pennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sillena sillena paayuthu pennae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh yeah oh yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh yeah oh yeah"/>
</div>
<div class="lyrico-lyrics-wrapper">Sillena sillena paayuthu pennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sillena sillena paayuthu pennae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna thavam seithean
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna thavam seithean"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai thedi vanthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai thedi vanthaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aga magizhvena aagiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aga magizhvena aagiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veenai thanthi ennil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veenai thanthi ennil"/>
</div>
<div class="lyrico-lyrics-wrapper">Veesum thendral neeyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veesum thendral neeyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Isai arivithu pogiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Isai arivithu pogiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un paarvai charam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un paarvai charam"/>
</div>
<div class="lyrico-lyrics-wrapper">Neengaatha varam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neengaatha varam"/>
</div>
<div class="lyrico-lyrics-wrapper">Un paatha thadam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un paatha thadam"/>
</div>
<div class="lyrico-lyrics-wrapper">En boomi padam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En boomi padam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuzhanthai ozhithu vaikkum bommayaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuzhanthai ozhithu vaikkum bommayaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal thurathi kondu paarthidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal thurathi kondu paarthidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinamum mugam maraiyum pulliyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinamum mugam maraiyum pulliyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatril narai vizhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatril narai vizhum"/>
</div>
<div class="lyrico-lyrics-wrapper">En thaabam ennai thoithidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En thaabam ennai thoithidum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey nallavai allavai enbavai neengi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey nallavai allavai enbavai neengi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pallavi pallavi aanathu nenjae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pallavi pallavi aanathu nenjae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh yeah oh yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh yeah oh yeah"/>
</div>
<div class="lyrico-lyrics-wrapper">Pallavi pallavi aanathu nenjae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pallavi pallavi aanathu nenjae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kallena kallena ooriya ullam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallena kallena ooriya ullam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sillena sillena paayuthu pennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sillena sillena paayuthu pennae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh yeah oh yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh yeah oh yeah"/>
</div>
<div class="lyrico-lyrics-wrapper">Sillena sillena paayuthu pennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sillena sillena paayuthu pennae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un perai manam reengaram idum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un perai manam reengaram idum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnalae varum singaara sinam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnalae varum singaara sinam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyiril thangi vidum yeakkangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyiril thangi vidum yeakkangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nizhalin karumai ena poothidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nizhalin karumai ena poothidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagil vinmini thodum aatralai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagil vinmini thodum aatralai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pethai puraa perum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pethai puraa perum"/>
</div>
<div class="lyrico-lyrics-wrapper">En kadhal ennai yaenthidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kadhal ennai yaenthidum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna thavam seithean
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna thavam seithean"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai thedi vanthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai thedi vanthaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aga magizhvena aagiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aga magizhvena aagiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veenai thanthi ennil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veenai thanthi ennil"/>
</div>
<div class="lyrico-lyrics-wrapper">Veesum thendral neeyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veesum thendral neeyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Isai arivithu pogiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Isai arivithu pogiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maayaaaa.neeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maayaaaa.neeyae"/>
</div>
<div class="lyrico-lyrics-wrapper">En idhazh puthai vinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En idhazh puthai vinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oyaa oyaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oyaa oyaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhi munai vizhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhi munai vizhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey nallavai allavai enbavai neengi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey nallavai allavai enbavai neengi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pallavi pallavi aanathu nenjae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pallavi pallavi aanathu nenjae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh yeah oh yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh yeah oh yeah"/>
</div>
<div class="lyrico-lyrics-wrapper">Pallavi pallavi aanathu nenjae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pallavi pallavi aanathu nenjae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kallena kallena ooriya ullam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallena kallena ooriya ullam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sillena sillena paayuthu pennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sillena sillena paayuthu pennae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh yeah oh yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh yeah oh yeah"/>
</div>
<div class="lyrico-lyrics-wrapper">Sillena sillena paayuthu pennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sillena sillena paayuthu pennae"/>
</div>
</pre>
