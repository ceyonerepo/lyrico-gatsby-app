---
title: "touch chesi chudu title song lyrics"
album: "Touch Chesi Chudu"
artist: "JAM 8 - Mani Sharma"
lyricist: "Chandrabose"
director: "Vikram Sirikonda"
path: "/albums/touch-chesi-chudu-lyrics"
song: "Touch Chesi Chudu Title"
image: ../../images/albumart/touch-chesi-chudu.jpg
date: 2018-02-02
lang: telugu
youtubeLink: "https://www.youtube.com/embed/_QD-HphScLQ"
type: "title track"
singers:
  - Brijesh Shandilya
  - Sreerama Chandra
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">I Do Solemnly Declare 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Do Solemnly Declare "/>
</div>
<div class="lyrico-lyrics-wrapper">Upon My Honour And 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Upon My Honour And "/>
</div>
<div class="lyrico-lyrics-wrapper">Conscience That I Will 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Conscience That I Will "/>
</div>
<div class="lyrico-lyrics-wrapper">Act At All Times 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Act At All Times "/>
</div>
<div class="lyrico-lyrics-wrapper">To The Best Of 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="To The Best Of "/>
</div>
<div class="lyrico-lyrics-wrapper">My Ability And Knowledge 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="My Ability And Knowledge "/>
</div>
<div class="lyrico-lyrics-wrapper">In A Manner Befitting 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="In A Manner Befitting "/>
</div>
<div class="lyrico-lyrics-wrapper">A Police Officer 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="A Police Officer "/>
</div>
<div class="lyrico-lyrics-wrapper">I Will Preserve The Dignity 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Will Preserve The Dignity "/>
</div>
<div class="lyrico-lyrics-wrapper">And Will Respect The 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="And Will Respect The "/>
</div>
<div class="lyrico-lyrics-wrapper">Rights Of All Individuals 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rights Of All Individuals "/>
</div>
<div class="lyrico-lyrics-wrapper">I Will Act Justly 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Will Act Justly "/>
</div>
<div class="lyrico-lyrics-wrapper">And Impartially 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="And Impartially "/>
</div>
<div class="lyrico-lyrics-wrapper">And With Propriety 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="And With Propriety "/>
</div>
<div class="lyrico-lyrics-wrapper">Towards My Fellow Officers 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Towards My Fellow Officers "/>
</div>
<div class="lyrico-lyrics-wrapper">I Will Constantly 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Will Constantly "/>
</div>
<div class="lyrico-lyrics-wrapper">Strive To Honour 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Strive To Honour "/>
</div>
<div class="lyrico-lyrics-wrapper">This Oath In My Service 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="This Oath In My Service "/>
</div>
<div class="lyrico-lyrics-wrapper">As A Police Officer 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="As A Police Officer "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sarru Sarruna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarru Sarruna "/>
</div>
<div class="lyrico-lyrics-wrapper">Ningi Merupulaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ningi Merupulaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Dookuthaadu Veedoo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dookuthaadu Veedoo "/>
</div>
<div class="lyrico-lyrics-wrapper">Girru Girruna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Girru Girruna "/>
</div>
<div class="lyrico-lyrics-wrapper">Nippu Varadala 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nippu Varadala "/>
</div>
<div class="lyrico-lyrics-wrapper">Ponguthaadu Veedoo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponguthaadu Veedoo "/>
</div>
<div class="lyrico-lyrics-wrapper">Gada Bida Chesthey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gada Bida Chesthey "/>
</div>
<div class="lyrico-lyrics-wrapper">Gaaliki Kooda 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaaliki Kooda "/>
</div>
<div class="lyrico-lyrics-wrapper">Sankele Vesthadoo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sankele Vesthadoo "/>
</div>
<div class="lyrico-lyrics-wrapper">Dari Thapputhuntey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dari Thapputhuntey "/>
</div>
<div class="lyrico-lyrics-wrapper">Nelanu Kooda 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nelanu Kooda "/>
</div>
<div class="lyrico-lyrics-wrapper">Cellulo Thosthadoo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cellulo Thosthadoo "/>
</div>
<div class="lyrico-lyrics-wrapper">Anytime Veedu Terror 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anytime Veedu Terror "/>
</div>
<div class="lyrico-lyrics-wrapper">AK47 Ku Veedey Ga Trigger
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="AK47 Ku Veedey Ga Trigger"/>
</div>
<div class="lyrico-lyrics-wrapper">Touch Chesi Chudu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Touch Chesi Chudu "/>
</div>
<div class="lyrico-lyrics-wrapper">Boom Boom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boom Boom"/>
</div>
<div class="lyrico-lyrics-wrapper">Touch Chesi Chudu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Touch Chesi Chudu "/>
</div>
<div class="lyrico-lyrics-wrapper">Boom Boom 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boom Boom "/>
</div>
<div class="lyrico-lyrics-wrapper">Touch Chesi Chudu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Touch Chesi Chudu "/>
</div>
<div class="lyrico-lyrics-wrapper">Boom Boom 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boom Boom "/>
</div>
<div class="lyrico-lyrics-wrapper">Touch Chesi Chudu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Touch Chesi Chudu "/>
</div>
<div class="lyrico-lyrics-wrapper">Boom Boom 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boom Boom "/>
</div>
<div class="lyrico-lyrics-wrapper">Touch Chesi Chudu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Touch Chesi Chudu "/>
</div>
<div class="lyrico-lyrics-wrapper">Veedi Adugey Police Station 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veedi Adugey Police Station "/>
</div>
<div class="lyrico-lyrics-wrapper">Veedi Choopey Sting Operation 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veedi Choopey Sting Operation "/>
</div>
24/7 <div class="lyrico-lyrics-wrapper">On Duty Veedidhey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="On Duty Veedidhey "/>
</div>
<div class="lyrico-lyrics-wrapper">Arey Rowdy Mafia 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey Rowdy Mafia "/>
</div>
<div class="lyrico-lyrics-wrapper">Wikipedia Veedey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wikipedia Veedey "/>
</div>
<div class="lyrico-lyrics-wrapper">Anni Crimulaku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anni Crimulaku "/>
</div>
<div class="lyrico-lyrics-wrapper">Anni Scamulaku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anni Scamulaku "/>
</div>
<div class="lyrico-lyrics-wrapper">Veedey Eraser 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veedey Eraser "/>
</div>
<div class="lyrico-lyrics-wrapper">Touch Chesi Chudu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Touch Chesi Chudu "/>
</div>
<div class="lyrico-lyrics-wrapper">Boom Boom 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boom Boom "/>
</div>
<div class="lyrico-lyrics-wrapper">Touch Chesi Chudu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Touch Chesi Chudu "/>
</div>
<div class="lyrico-lyrics-wrapper">Boom Boom 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boom Boom "/>
</div>
<div class="lyrico-lyrics-wrapper">Touch Chesi Chudu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Touch Chesi Chudu "/>
</div>
<div class="lyrico-lyrics-wrapper">Boom Boom 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boom Boom "/>
</div>
<div class="lyrico-lyrics-wrapper">Touch Chesi Chudu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Touch Chesi Chudu "/>
</div>
<div class="lyrico-lyrics-wrapper">Boom Boom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boom Boom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anytime Veedu Terror 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anytime Veedu Terror "/>
</div>
<div class="lyrico-lyrics-wrapper">AK47 Ku Veedey Ga Trigger
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="AK47 Ku Veedey Ga Trigger"/>
</div>
<div class="lyrico-lyrics-wrapper">Touch Chesi Chudu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Touch Chesi Chudu "/>
</div>
<div class="lyrico-lyrics-wrapper">Boom Boom 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boom Boom "/>
</div>
<div class="lyrico-lyrics-wrapper">Touch Chesi Chudu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Touch Chesi Chudu "/>
</div>
<div class="lyrico-lyrics-wrapper">Boom Boom 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boom Boom "/>
</div>
<div class="lyrico-lyrics-wrapper">Touch Chesi Chudu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Touch Chesi Chudu "/>
</div>
<div class="lyrico-lyrics-wrapper">Boom Boom 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boom Boom "/>
</div>
<div class="lyrico-lyrics-wrapper">Touch Chesi Chudu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Touch Chesi Chudu "/>
</div>
<div class="lyrico-lyrics-wrapper">Boom Boom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boom Boom"/>
</div>
<div class="lyrico-lyrics-wrapper">Holiday Vaddantaadu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Holiday Vaddantaadu "/>
</div>
<div class="lyrico-lyrics-wrapper">Weekendey Vaddantaadu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Weekendey Vaddantaadu "/>
</div>
<div class="lyrico-lyrics-wrapper">Vacation Vaddantaadu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vacation Vaddantaadu "/>
</div>
<div class="lyrico-lyrics-wrapper">Picnic Ye Vaddantaadu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Picnic Ye Vaddantaadu "/>
</div>
<div class="lyrico-lyrics-wrapper">Work Ye Worshipantaadu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Work Ye Worshipantaadu "/>
</div>
<div class="lyrico-lyrics-wrapper">Workholic Ai Untadu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Workholic Ai Untadu "/>
</div>
<div class="lyrico-lyrics-wrapper">Work Lone Relax Ai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Work Lone Relax Ai "/>
</div>
<div class="lyrico-lyrics-wrapper">Refresh Avuthoo Untaadu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Refresh Avuthoo Untaadu "/>
</div>
<div class="lyrico-lyrics-wrapper">Colorlu Enno Kanpisthunna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Colorlu Enno Kanpisthunna "/>
</div>
<div class="lyrico-lyrics-wrapper">Khaki Rangey Great Antaadu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khaki Rangey Great Antaadu "/>
</div>
<div class="lyrico-lyrics-wrapper">Kanamulloni Rakthamu Rangu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanamulloni Rakthamu Rangu "/>
</div>
<div class="lyrico-lyrics-wrapper">Khaki Rangey Antaadu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khaki Rangey Antaadu "/>
</div>
<div class="lyrico-lyrics-wrapper">Kapu Kaasthoo Untaadu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kapu Kaasthoo Untaadu "/>
</div>
<div class="lyrico-lyrics-wrapper">Cop Kaasthu Untaadu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cop Kaasthu Untaadu "/>
</div>
<div class="lyrico-lyrics-wrapper">Belt Ki Pogaru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Belt Ki Pogaru "/>
</div>
<div class="lyrico-lyrics-wrapper">Cap Ki Poweru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cap Ki Poweru "/>
</div>
<div class="lyrico-lyrics-wrapper">Badge Ki Thegimpu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Badge Ki Thegimpu "/>
</div>
<div class="lyrico-lyrics-wrapper">Boot Ki Vegimpu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boot Ki Vegimpu "/>
</div>
<div class="lyrico-lyrics-wrapper">Needaku Courage 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Needaku Courage "/>
</div>
<div class="lyrico-lyrics-wrapper">Kodithey Garage 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodithey Garage "/>
</div>
<div class="lyrico-lyrics-wrapper">Seizing Loney Rangoli 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seizing Loney Rangoli "/>
</div>
<div class="lyrico-lyrics-wrapper">Searching Loney Sankranthi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Searching Loney Sankranthi "/>
</div>
<div class="lyrico-lyrics-wrapper">Raiding Loney Ramzaanu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raiding Loney Ramzaanu "/>
</div>
<div class="lyrico-lyrics-wrapper">Encounter Lo Diwali 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Encounter Lo Diwali "/>
</div>
<div class="lyrico-lyrics-wrapper">Ata Pata Anandaalu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ata Pata Anandaalu "/>
</div>
<div class="lyrico-lyrics-wrapper">Aato Potoo Anubandhalu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aato Potoo Anubandhalu "/>
</div>
<div class="lyrico-lyrics-wrapper">Duty Loney Antaadu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Duty Loney Antaadu "/>
</div>
<div class="lyrico-lyrics-wrapper">Duty Thaaney Untaadu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Duty Thaaney Untaadu "/>
</div>
<div class="lyrico-lyrics-wrapper">Thoota La Velthuntaadey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoota La Velthuntaadey "/>
</div>
<div class="lyrico-lyrics-wrapper">Ek Bar Touch Chesi Choodu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ek Bar Touch Chesi Choodu "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manamantha Nidarothey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manamantha Nidarothey "/>
</div>
<div class="lyrico-lyrics-wrapper">Thanu Melkontaadu Ra 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanu Melkontaadu Ra "/>
</div>
<div class="lyrico-lyrics-wrapper">Mana Loni Kalalaku Thanu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana Loni Kalalaku Thanu "/>
</div>
<div class="lyrico-lyrics-wrapper">Kavali Kadara 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavali Kadara "/>
</div>
<div class="lyrico-lyrics-wrapper">Adiginicho Rakshisthey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiginicho Rakshisthey "/>
</div>
<div class="lyrico-lyrics-wrapper">Devudu Antamura 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Devudu Antamura "/>
</div>
<div class="lyrico-lyrics-wrapper">Adaganidey Rakshisthey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adaganidey Rakshisthey "/>
</div>
<div class="lyrico-lyrics-wrapper">Vaadikantey Goppara 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadikantey Goppara "/>
</div>
<div class="lyrico-lyrics-wrapper">Anytime Veedu Terror 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anytime Veedu Terror "/>
</div>
<div class="lyrico-lyrics-wrapper">AK47 Ku Veedey Ga Trigger
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="AK47 Ku Veedey Ga Trigger"/>
</div>
<div class="lyrico-lyrics-wrapper">Touch Chesi Chudu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Touch Chesi Chudu "/>
</div>
<div class="lyrico-lyrics-wrapper">Boom Boom 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boom Boom "/>
</div>
<div class="lyrico-lyrics-wrapper">Touch Chesi Chudu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Touch Chesi Chudu "/>
</div>
<div class="lyrico-lyrics-wrapper">Boom Boom 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boom Boom "/>
</div>
<div class="lyrico-lyrics-wrapper">Touch Chesi Chudu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Touch Chesi Chudu "/>
</div>
<div class="lyrico-lyrics-wrapper">Boom Boom 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boom Boom "/>
</div>
<div class="lyrico-lyrics-wrapper">Touch Chesi Chudu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Touch Chesi Chudu "/>
</div>
<div class="lyrico-lyrics-wrapper">Boom Boom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boom Boom"/>
</div>
<div class="lyrico-lyrics-wrapper">Touch Chesi Chudu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Touch Chesi Chudu "/>
</div>
<div class="lyrico-lyrics-wrapper">Boom Boom 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boom Boom "/>
</div>
<div class="lyrico-lyrics-wrapper">Touch Chesi Chudu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Touch Chesi Chudu "/>
</div>
<div class="lyrico-lyrics-wrapper">Boom Boom 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boom Boom "/>
</div>
<div class="lyrico-lyrics-wrapper">Touch Chesi Chudu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Touch Chesi Chudu "/>
</div>
<div class="lyrico-lyrics-wrapper">Boom Boom 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boom Boom "/>
</div>
<div class="lyrico-lyrics-wrapper">Touch Chesi Chudu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Touch Chesi Chudu "/>
</div>
<div class="lyrico-lyrics-wrapper">Boom Boom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boom Boom"/>
</div>
<div class="lyrico-lyrics-wrapper">Touch Chesi Chudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Touch Chesi Chudu"/>
</div>
</pre>
