---
title: "phisal jaa tu song lyrics"
album: "Haseen Dillruba"
artist: "Amit Trivedi"
lyricist: "Kshitij Patwardhan"
director: "Vinil Mathew"
path: "/albums/haseen-dillruba-lyrics"
song: "Phisal Jaa Tu"
image: ../../images/albumart/haseen-dillruba.jpg
date: 2021-07-02
lang: hindi
youtubeLink: "https://www.youtube.com/embed/NCjuB1BToqo"
type: "love"
singers:
  - Abhijeet Srivastava
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aankhon Ko Door Se Hi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aankhon Ko Door Se Hi"/>
</div>
<div class="lyrico-lyrics-wrapper">Usne Chhua Hai Re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usne Chhua Hai Re"/>
</div>
<div class="lyrico-lyrics-wrapper">Mehsoos Humko Bhi Yeh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mehsoos Humko Bhi Yeh"/>
</div>
<div class="lyrico-lyrics-wrapper">Jabse Hua Hai Re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jabse Hua Hai Re"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kurban Uspe Apna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurban Uspe Apna"/>
</div>
<div class="lyrico-lyrics-wrapper">Har Pal Hua Hai Re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Har Pal Hua Hai Re"/>
</div>
<div class="lyrico-lyrics-wrapper">Woh Hi Hai Wajah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Woh Hi Hai Wajah"/>
</div>
<div class="lyrico-lyrics-wrapper">Woh Hi Hai Jagah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Woh Hi Hai Jagah"/>
</div>
<div class="lyrico-lyrics-wrapper">Woh Hi Jaadoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Woh Hi Jaadoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ke Mere Dil Ne Kaha Mujhse
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ke Mere Dil Ne Kaha Mujhse"/>
</div>
<div class="lyrico-lyrics-wrapper">Phisal Jaa Tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phisal Jaa Tu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ke Mere Dil Ne Kaha Mujhse
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ke Mere Dil Ne Kaha Mujhse"/>
</div>
<div class="lyrico-lyrics-wrapper">Phisal Ja Tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phisal Ja Tu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ke Meri Jaan Ne Kaha Mujhse
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ke Meri Jaan Ne Kaha Mujhse"/>
</div>
<div class="lyrico-lyrics-wrapper">Nikal Jaa Tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikal Jaa Tu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho Ab Kya Sahi Na Jaanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho Ab Kya Sahi Na Jaanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Na Galat Jaanu Re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na Galat Jaanu Re"/>
</div>
<div class="lyrico-lyrics-wrapper">Woh Toh Badhta Hi Jaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Woh Toh Badhta Hi Jaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho Jaise Chadhta Ho Bukhar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho Jaise Chadhta Ho Bukhar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Ho Apna Paraya Ab Toh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Ho Apna Paraya Ab Toh"/>
</div>
<div class="lyrico-lyrics-wrapper">Main Na Pehchanu Re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main Na Pehchanu Re"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeh Hai Aisi Bahaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh Hai Aisi Bahaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Jo Karde Re Beda Paar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jo Karde Re Beda Paar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gehraiyon Mein Jaise
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gehraiyon Mein Jaise"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaane Laga Hai Woh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaane Laga Hai Woh"/>
</div>
<div class="lyrico-lyrics-wrapper">Nazdeekiyon Mein Vaise
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nazdeekiyon Mein Vaise"/>
</div>
<div class="lyrico-lyrics-wrapper">Aane Laga Hai Woh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aane Laga Hai Woh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chhune Ki Deri Hai Ji
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chhune Ki Deri Hai Ji"/>
</div>
<div class="lyrico-lyrics-wrapper">Hum Bhi Taiyaar Hain
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hum Bhi Taiyaar Hain"/>
</div>
<div class="lyrico-lyrics-wrapper">Woh Bahana Woh Thikana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Woh Bahana Woh Thikana"/>
</div>
<div class="lyrico-lyrics-wrapper">Woh Hi Harsoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Woh Hi Harsoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mere Dil Ne Kaha Mujhse
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mere Dil Ne Kaha Mujhse"/>
</div>
<div class="lyrico-lyrics-wrapper">Phisal Ja Tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phisal Ja Tu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mere Dil Ne Kaha Mujhse
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mere Dil Ne Kaha Mujhse"/>
</div>
<div class="lyrico-lyrics-wrapper">Phisal Jaa Tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phisal Jaa Tu"/>
</div>
</pre>
