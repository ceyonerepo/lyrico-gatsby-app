---
title: "kappathalli song lyrics"
album: "Dorasaani"
artist: "Prashanth R Vihari"
lyricist: "Goreti Venkanna"
director: "KVR Mahendra"
path: "/albums/dorasaani-lyrics"
song: "Kappathalli"
image: ../../images/albumart/dorasaani.jpg
date: 2019-07-12
lang: telugu
youtubeLink: "https://www.youtube.com/embed/cdabp_AXsow"
type: "happy"
singers:
  - Anurag Kulkarni
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Shitapata Shitapata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shitapata Shitapata"/>
</div>
<div class="lyrico-lyrics-wrapper">Shinukula Thaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shinukula Thaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ningini Vampina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ningini Vampina"/>
</div>
<div class="lyrico-lyrics-wrapper">Singidi Baanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Singidi Baanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Matthadi Dhunikina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Matthadi Dhunikina"/>
</div>
<div class="lyrico-lyrics-wrapper">Pongula Haaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pongula Haaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Urumulu Merupula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urumulu Merupula"/>
</div>
<div class="lyrico-lyrics-wrapper">Guppena Kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Guppena Kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kappala Ganthula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kappala Ganthula"/>
</div>
<div class="lyrico-lyrics-wrapper">Pillala Melam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pillala Melam"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey Kappala Ganthula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey Kappala Ganthula"/>
</div>
<div class="lyrico-lyrics-wrapper">Pillala Melam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pillala Melam"/>
</div>
<div class="lyrico-lyrics-wrapper">Shitapata Shitapata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shitapata Shitapata"/>
</div>
<div class="lyrico-lyrics-wrapper">Shinukula Thaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shinukula Thaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ningini Vampina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ningini Vampina"/>
</div>
<div class="lyrico-lyrics-wrapper">Singidi Baanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Singidi Baanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Matthadi Dhunikina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Matthadi Dhunikina"/>
</div>
<div class="lyrico-lyrics-wrapper">Pongula Haaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pongula Haaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Urumulu Merupula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urumulu Merupula"/>
</div>
<div class="lyrico-lyrics-wrapper">Guppena Kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Guppena Kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kappala Ganthula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kappala Ganthula"/>
</div>
<div class="lyrico-lyrics-wrapper">Pillala Melam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pillala Melam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kappala Ganthula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kappala Ganthula"/>
</div>
<div class="lyrico-lyrics-wrapper">Pillala Melam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pillala Melam"/>
</div>
<div class="lyrico-lyrics-wrapper">Gu Guggoo Kusinthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gu Guggoo Kusinthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kasindheeraa Pulisinthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kasindheeraa Pulisinthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gu Guggoo Kusinthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gu Guggoo Kusinthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kasindheeraa Pulisinthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kasindheeraa Pulisinthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gu Guggoo Kusinthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gu Guggoo Kusinthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kasindheeraa Pulisinthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kasindheeraa Pulisinthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gu Guggoo Kusinthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gu Guggoo Kusinthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kasindheeraa Pulisinthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kasindheeraa Pulisinthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kammala Katthitho Gunjithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kammala Katthitho Gunjithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Konakommala Khya Neesentha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konakommala Khya Neesentha"/>
</div>
<div class="lyrico-lyrics-wrapper">Aakula Saatuna Pindhala Kaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aakula Saatuna Pindhala Kaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kommanu Upina Koyila Kootha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kommanu Upina Koyila Kootha"/>
</div>
<div class="lyrico-lyrics-wrapper">Biggannala Rekkala Motha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Biggannala Rekkala Motha"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey Biggannala Rekkala Motha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey Biggannala Rekkala Motha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kotaku Vattina Naasu Pootha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kotaku Vattina Naasu Pootha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottha Neetivai Koiyara Kotha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottha Neetivai Koiyara Kotha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kotaku Thotaku Veiyara Baata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kotaku Thotaku Veiyara Baata"/>
</div>
<div class="lyrico-lyrics-wrapper">Kotaku Thotaku Veiyara Baata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kotaku Thotaku Veiyara Baata"/>
</div>
<div class="lyrico-lyrics-wrapper">Nimmalu Poosi Nimmalu Gaase
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nimmalu Poosi Nimmalu Gaase"/>
</div>
<div class="lyrico-lyrics-wrapper">Nimmala Mullu Ninne Joose
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nimmala Mullu Ninne Joose"/>
</div>
<div class="lyrico-lyrics-wrapper">Nimmalu Poosi Nimmalu Gaase
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nimmalu Poosi Nimmalu Gaase"/>
</div>
<div class="lyrico-lyrics-wrapper">Nimmala Mullu Ninne Joose
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nimmala Mullu Ninne Joose"/>
</div>
<div class="lyrico-lyrics-wrapper">Mamidi Poosi Mamidi Gaase
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mamidi Poosi Mamidi Gaase"/>
</div>
<div class="lyrico-lyrics-wrapper">Mamidi Vagare Kallem Vese
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mamidi Vagare Kallem Vese"/>
</div>
<div class="lyrico-lyrics-wrapper">Mamidi Poosi Mamidi Gaase
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mamidi Poosi Mamidi Gaase"/>
</div>
<div class="lyrico-lyrics-wrapper">Mamidi Vagare Kallem Vese
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mamidi Vagare Kallem Vese"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey Nimmalu Poosi Nimmalu Gaase
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey Nimmalu Poosi Nimmalu Gaase"/>
</div>
<div class="lyrico-lyrics-wrapper">Nimmala Mullu Ninne Joose
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nimmala Mullu Ninne Joose"/>
</div>
<div class="lyrico-lyrics-wrapper">Mamidi Poosi Mamidi Gaase
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mamidi Poosi Mamidi Gaase"/>
</div>
<div class="lyrico-lyrics-wrapper">Mamidi Vagare Kallem Vese
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mamidi Vagare Kallem Vese"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaamalu Poosi Jaamalu Kaase
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaamalu Poosi Jaamalu Kaase"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey Jaamalu Poosi Jaamalu Kaase
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey Jaamalu Poosi Jaamalu Kaase"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaamala Theepi Thaapam Repey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaamala Theepi Thaapam Repey"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhapuna Dhaagina Chilakanu Jera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhapuna Dhaagina Chilakanu Jera"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheepam Veluguvu Neevelera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheepam Veluguvu Neevelera"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheepam Veluguvu Neevelera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheepam Veluguvu Neevelera"/>
</div>
<div class="lyrico-lyrics-wrapper">Giri Giri Gaajula Gilaka Nuvvulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Giri Giri Gaajula Gilaka Nuvvulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Jada Kucchullo Jaaji Puvvulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jada Kucchullo Jaaji Puvvulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadumula Vampu Kodavali Anchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadumula Vampu Kodavali Anchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Biguyadha Pongulu Bangaru Bindhelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Biguyadha Pongulu Bangaru Bindhelu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadabadi Bidiyam Mudividi Pilichenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadabadi Bidiyam Mudividi Pilichenu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadabadi Bidiyam Mudividi Pilichenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadabadi Bidiyam Mudividi Pilichenu"/>
</div>
<div class="lyrico-lyrics-wrapper">Divi Yavvana Gudi Nelana Merisenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Divi Yavvana Gudi Nelana Merisenu"/>
</div>
<div class="lyrico-lyrics-wrapper">Divi Yavvana Gudi Nelana Merisenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Divi Yavvana Gudi Nelana Merisenu"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey Divi Yavvana Gudi Nelana Merisenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey Divi Yavvana Gudi Nelana Merisenu"/>
</div>
</pre>
