---
title: "yetho yetho song lyrics"
album: "Marudhavelu"
artist: "James Vasanthan"
lyricist: "Mohan Raj"
director: "R.K.R. Aathimoolam"
path: "/albums/marudhavelu-lyrics"
song: "Yetho Yetho"
image: ../../images/albumart/marudhavelu.jpg
date: 2011-11-18
lang: tamil
youtubeLink: "https://www.youtube.com/embed/YanErpqy9Lo"
type: "happy"
singers:
  - Mathangi Jagdish
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yedho yedhedho ennam vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedho yedhedho ennam vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">thoondil meenaagiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoondil meenaagiren"/>
</div>
<div class="lyrico-lyrics-wrapper">yeno yeneno thookkam ennum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yeno yeneno thookkam ennum"/>
</div>
<div class="lyrico-lyrics-wrapper">undhan bimbam kaanugindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="undhan bimbam kaanugindren"/>
</div>
<div class="lyrico-lyrics-wrapper">thottupoagum un 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thottupoagum un "/>
</div>
<div class="lyrico-lyrics-wrapper">svaasakkaatrai kattipoaduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="svaasakkaatrai kattipoaduven"/>
</div>
<div class="lyrico-lyrics-wrapper">naan vettipoattu en 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan vettipoattu en "/>
</div>
<div class="lyrico-lyrics-wrapper">sondham endru uyiriley mooduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sondham endru uyiriley mooduven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal kanthaanam seidhadhendru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal kanthaanam seidhadhendru"/>
</div>
<div class="lyrico-lyrics-wrapper">enakkul unai paarkkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enakkul unai paarkkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">thookkam indri en poarvaiyoadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thookkam indri en poarvaiyoadu"/>
</div>
<div class="lyrico-lyrics-wrapper">undhan kadhaiyai pesugiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="undhan kadhaiyai pesugiren"/>
</div>
<div class="lyrico-lyrics-wrapper">endhan kanavil nee vandhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="endhan kanavil nee vandhu "/>
</div>
<div class="lyrico-lyrics-wrapper">theenda kangal vizhikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theenda kangal vizhikkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">O naanam poaley en kannam engum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O naanam poaley en kannam engum"/>
</div>
<div class="lyrico-lyrics-wrapper">regai naan kaangiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="regai naan kaangiren"/>
</div>
<div class="lyrico-lyrics-wrapper">thooral poadum paarvai poadhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thooral poadum paarvai poadhum"/>
</div>
<div class="lyrico-lyrics-wrapper">unnai poovaiyaakki en veettil vaithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai poovaiyaakki en veettil vaithu"/>
</div>
<div class="lyrico-lyrics-wrapper">poottikkolla vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poottikkolla vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">nillaamaley kaalgal rendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nillaamaley kaalgal rendum"/>
</div>
<div class="lyrico-lyrics-wrapper">engengilum thaaviye Odudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engengilum thaaviye Odudhey"/>
</div>
<div class="lyrico-lyrics-wrapper">ennaagumo ennenna aagumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennaagumo ennenna aagumo"/>
</div>
<div class="lyrico-lyrics-wrapper">unnaal maaripoagindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnaal maaripoagindren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanum naanendru nambavillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum naanendru nambavillai"/>
</div>
<div class="lyrico-lyrics-wrapper">ennai nee maatrinaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennai nee maatrinaai"/>
</div>
<div class="lyrico-lyrics-wrapper">kaalai maalai ennulley vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaalai maalai ennulley vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">pookkal veesi Odigindraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pookkal veesi Odigindraai"/>
</div>
<div class="lyrico-lyrics-wrapper">mutrupulli illaadha aasai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mutrupulli illaadha aasai "/>
</div>
<div class="lyrico-lyrics-wrapper">nenjil poothadho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjil poothadho"/>
</div>
<div class="lyrico-lyrics-wrapper">en ellaikkoadu unnaaley 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en ellaikkoadu unnaaley "/>
</div>
<div class="lyrico-lyrics-wrapper">endru kollaipoagindradho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="endru kollaipoagindradho"/>
</div>
<div class="lyrico-lyrics-wrapper">thooral poadum paarvaipoadhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thooral poadum paarvaipoadhum"/>
</div>
<div class="lyrico-lyrics-wrapper">unnai bommaiyaakki en veettil vaithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai bommaiyaakki en veettil vaithu"/>
</div>
<div class="lyrico-lyrics-wrapper">poottikkoven unmai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poottikkoven unmai"/>
</div>
<div class="lyrico-lyrics-wrapper">nillaamaley kaalgal rendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nillaamaley kaalgal rendum"/>
</div>
<div class="lyrico-lyrics-wrapper">engengilum thaaviye Odudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engengilum thaaviye Odudhey"/>
</div>
<div class="lyrico-lyrics-wrapper">ennaagumo ennenna aagumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennaagumo ennenna aagumo"/>
</div>
<div class="lyrico-lyrics-wrapper">unnaal maarippoagindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnaal maarippoagindren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thooral poadum paarvai poadhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thooral poadum paarvai poadhum"/>
</div>
<div class="lyrico-lyrics-wrapper">unnai poovaiyaakki en veettil vaithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai poovaiyaakki en veettil vaithu"/>
</div>
<div class="lyrico-lyrics-wrapper">poottikkolla vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poottikkolla vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">nillaamaley kaalgal rendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nillaamaley kaalgal rendum"/>
</div>
<div class="lyrico-lyrics-wrapper">engengilum thaaviye Odudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engengilum thaaviye Odudhey"/>
</div>
<div class="lyrico-lyrics-wrapper">ennaagumo ennenna aagumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennaagumo ennenna aagumo"/>
</div>
<div class="lyrico-lyrics-wrapper">unnaal maaripoagindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnaal maaripoagindren"/>
</div>
</pre>
