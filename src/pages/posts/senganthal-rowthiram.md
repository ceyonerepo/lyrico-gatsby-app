---
title: "senganthal song lyrics"
album: "Rowthiram"
artist: "Prakash Nikki"
lyricist: "Lalithanand"
director: "Gokul"
path: "/albums/rowthiram-lyrics"
song: "Senganthal"
image: ../../images/albumart/rowthiram.jpg
date: 2011-08-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/KvaJNtm78Fc"
type: "love"
singers:
  - Haricharan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Senganthal Kaiyaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senganthal Kaiyaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirai Thottu Ponaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirai Thottu Ponaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Senthoora Aagaayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Senthoora Aagaayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannil Thandhu Ponaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannil Thandhu Ponaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oyaadha Poothooral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oyaadha Poothooral"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullae Ootri Ponaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullae Ootri Ponaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Aval Thozhsaaindhu Naan Pesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aval Thozhsaaindhu Naan Pesa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasai Mooti Ponaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai Mooti Ponaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Palagiya Naatkal Ellamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palagiya Naatkal Ellamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ahzgaai Maatri Ponaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahzgaai Maatri Ponaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Palamurai Avalai Paarthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palamurai Avalai Paarthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Adanga Aaval Koduthalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adanga Aaval Koduthalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eerammanadhoram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eerammanadhoram"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadhi Nadhiena Thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadhi Nadhiena Thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhaal Vandhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhaal Vandhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhooram Vegu Thooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhooram Vegu Thooram"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Azhaiena Thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Azhaiena Thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Theendi Chendraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theendi Chendraal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Senganthal Kaiyaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senganthal Kaiyaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirai Thottu Ponaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirai Thottu Ponaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Senthoora Aagaayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Senthoora Aagaayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannil Thandhu Ponaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannil Thandhu Ponaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oyaadha Poothooral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oyaadha Poothooral"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullae Ootri Ponaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullae Ootri Ponaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Aval Thozhsaaindhu Naan Pesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aval Thozhsaaindhu Naan Pesa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasai Mooti Ponaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai Mooti Ponaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edho Pola Aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edho Pola Aanen"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhanaalae Maari Ponen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhanaalae Maari Ponen"/>
</div>
<div class="lyrico-lyrics-wrapper">Eman Polae Kollum Paarvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eman Polae Kollum Paarvai"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai Urasumpodhum Uyir Vazhkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai Urasumpodhum Uyir Vazhkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Yaarai Thaandum Pozhudhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Yaarai Thaandum Pozhudhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhum Thondridathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhum Thondridathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Avalai Paarkum Pozhuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avalai Paarkum Pozhuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kaalgal Thaandi Pogaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kaalgal Thaandi Pogaathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Netru Paartha Nila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netru Paartha Nila"/>
</div>
<div class="lyrico-lyrics-wrapper">Ettaadha Thooram Thooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettaadha Thooram Thooram"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeno Yeno Indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeno Yeno Indru"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kaigal Neettum Thooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kaigal Neettum Thooram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaatril Pogum Ilai Aanenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatril Pogum Ilai Aanenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeno Naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeno Naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thooki Ponaaii Ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thooki Ponaaii Ennai"/>
</div>
<div class="lyrico-lyrics-wrapper">Engeyo Ponen Naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engeyo Ponen Naanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaedi Dhinam Thaedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaedi Dhinam Thaedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Udai Udithidum Maatram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udai Udithidum Maatram"/>
</div>
<div class="lyrico-lyrics-wrapper">Thandhaal Thandhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandhaal Thandhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Maayam Idhu Thaano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maayam Idhu Thaano"/>
</div>
<div class="lyrico-lyrics-wrapper">En Nizhalukkum Vannam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nizhalukkum Vannam"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Vandhadho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Vandhadho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Senganthal Kaiyaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senganthal Kaiyaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirai Thottu Ponaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirai Thottu Ponaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Senthoora Aagaayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Senthoora Aagaayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannil Thandhu Ponaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannil Thandhu Ponaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oyaadha Poothooral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oyaadha Poothooral"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullae Ootri Ponaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullae Ootri Ponaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Aval Thozhsaaindhu Naan Pesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aval Thozhsaaindhu Naan Pesa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasai Mooti Ponaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai Mooti Ponaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kootam Koodum Saalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kootam Koodum Saalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Viral Korthu Pogum Velai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viral Korthu Pogum Velai"/>
</div>
<div class="lyrico-lyrics-wrapper">Palar Paarthu Pogum Pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palar Paarthu Pogum Pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu Thanimai Pola Yen Thondrutho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu Thanimai Pola Yen Thondrutho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aval Koondhal Aada Thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aval Koondhal Aada Thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thozhil Medai Ketpen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozhil Medai Ketpen"/>
</div>
<div class="lyrico-lyrics-wrapper">Melidhaai Theendum Podhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Melidhaai Theendum Podhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Siridhaaga Garvam Kolvenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siridhaaga Garvam Kolvenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theeyil Seitha Kangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeyil Seitha Kangal"/>
</div>
<div class="lyrico-lyrics-wrapper">En Nenjai Etti Paarkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nenjai Etti Paarkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Poovil Neitha Kaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovil Neitha Kaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethotho Ennai Ketkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethotho Ennai Ketkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanavilin Ezhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanavilin Ezhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vannamthaan Saayam Pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vannamthaan Saayam Pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vellai Megam Aval
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vellai Megam Aval"/>
</div>
<div class="lyrico-lyrics-wrapper">Elloraa Thottru Pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Elloraa Thottru Pogum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eerammanadhoram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eerammanadhoram"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadhi Nadhiena Thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadhi Nadhiena Thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhaal Vandhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhaal Vandhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhooram Vegu Thooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhooram Vegu Thooram"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Azhaiena Thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Azhaiena Thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Theendi Chendraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theendi Chendraal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Palagiya Naatkal Ellamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palagiya Naatkal Ellamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ahzgaai Maatri Ponaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahzgaai Maatri Ponaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Palamurai Avalai Paarthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palamurai Avalai Paarthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Adanga Aaval Koduthalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adanga Aaval Koduthalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanam Adhan Thaagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam Adhan Thaagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadal Kudithidum Kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadal Kudithidum Kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhaal Vandhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhaal Vandhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Neram Veguneram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neram Veguneram"/>
</div>
<div class="lyrico-lyrics-wrapper">Udan Nadhidum Aasai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udan Nadhidum Aasai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thandhaal Avaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandhaal Avaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Senganthal Kaiyaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senganthal Kaiyaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirai Thottu Ponaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirai Thottu Ponaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Senthoora Aagaayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Senthoora Aagaayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannil Thandhu Ponaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannil Thandhu Ponaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oooyaadha Poothooral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oooyaadha Poothooral"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullae Ootri Ponaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullae Ootri Ponaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Aval Thozhsaaindhu Naan Pesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aval Thozhsaaindhu Naan Pesa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasai Mooti Ponaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai Mooti Ponaal"/>
</div>
</pre>
