---
title: "nagaruthe nagaruthe song lyrics"
album: "Vandhaan Vendraan"
artist: "Thaman"
lyricist: "Na. Muthukumar"
director: "R. Kannan"
path: "/albums/vandhaan-vendraan-lyrics"
song: "Nagaruthe Nagaruthe"
image: ../../images/albumart/vandhaan-vendraan.jpg
date: 2011-09-16
lang: tamil
youtubeLink: "https://www.youtube.com/embed/5Ko1rKUuAR4"
type: "sad"
singers:
  - Thaman
  - Gopi Sundar
  - Pepe
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nagarudhae nagarudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nagarudhae nagarudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha nimidam nagaruthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha nimidam nagaruthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Endham idhayam padharudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endham idhayam padharudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai vittuchella valikuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai vittuchella valikuthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhayathil kathiyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayathil kathiyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nulaikaathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nulaikaathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithanai idi athu porukaathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithanai idi athu porukaathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee thandha ninaivugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thandha ninaivugal"/>
</div>
<div class="lyrico-lyrics-wrapper">Marakaathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marakaathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nan irandhaal kooda irakaathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nan irandhaal kooda irakaathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhayathil kathiyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayathil kathiyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nulaikaathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nulaikaathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithanai idi athu porukaathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithanai idi athu porukaathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ne tharum pirivugal thaangathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne tharum pirivugal thaangathae"/>
</div>
<div class="lyrico-lyrics-wrapper">En uyirae uyirai vizhagaathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En uyirae uyirai vizhagaathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhevadhai unnidam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhevadhai unnidam"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna varama ketpen keladiDhevadhai unnaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna varama ketpen keladiDhevadhai unnaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Varamaai ketpen naanadai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varamaai ketpen naanadai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoonginaal thookathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoonginaal thookathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavil ketkum undhan kaaladi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavil ketkum undhan kaaladi"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakkae indha idaiveli yen adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkae indha idaiveli yen adi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhayathil kathiyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayathil kathiyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nulaikaathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nulaikaathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithanai idi athu porukaathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithanai idi athu porukaathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee thandha ninaivugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thandha ninaivugal"/>
</div>
<div class="lyrico-lyrics-wrapper">Marakaathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marakaathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nan irandhaal kooda irakaathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nan irandhaal kooda irakaathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo wow wowu ow
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo wow wowu ow"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhayathil kathiyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayathil kathiyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nulaikaathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nulaikaathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithanai idi athu porukaathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithanai idi athu porukaathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ne tharum pirivugal thaangathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne tharum pirivugal thaangathae"/>
</div>
<div class="lyrico-lyrics-wrapper">En uyirae uyirai vizhagaathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En uyirae uyirai vizhagaathae"/>
</div>
</pre>
