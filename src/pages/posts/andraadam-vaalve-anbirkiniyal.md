---
title: "andraadam vaalve song lyrics"
album: "Anbirkiniyal"
artist: "Javed Riaz"
lyricist: "Lalithanand"
director: "Gokul"
path: "/albums/anbirkiniyal-lyrics"
song: "Andraadam Vaalve"
image: ../../images/albumart/anbirkiniyal.jpg
date: 2021-03-05
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ALAIO9zpCDY"
type: "Melody"
singers:
  - Prarthana Indrajith
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Andraadam vaazhve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andraadam vaazhve"/>
</div>
<div class="lyrico-lyrics-wrapper">Kondaadum paarvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondaadum paarvai"/>
</div>
<div class="lyrico-lyrics-wrapper">Andraadam vaazhve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andraadam vaazhve"/>
</div>
<div class="lyrico-lyrics-wrapper">Kondaadum paarvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondaadum paarvai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhvai un thozhil yethi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvai un thozhil yethi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee vaanil pogum minmini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee vaanil pogum minmini"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaane oru bhoomiyagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaane oru bhoomiyagum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee pullin midhu venpani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee pullin midhu venpani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nermarai paarvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nermarai paarvai"/>
</div>
<div class="lyrico-lyrics-wrapper">Netrum indrum naalaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netrum indrum naalaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarthathum yaaraiyum eerpale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarthathum yaaraiyum eerpale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Velaiyil muzhginal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velaiyil muzhginal"/>
</div>
<div class="lyrico-lyrics-wrapper">Venil kaala vaanilai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venil kaala vaanilai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vannamai maaratho vaazh naale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vannamai maaratho vaazh naale"/>
</div>
</pre>
