---
title: "ninaithadhu ellam song lyrics"
album: "Oru Kuppai Kathai"
artist: "Joshua Sridhar"
lyricist: "Na. Muthukumar"
director: "Kaali Rangasamy"
path: "/albums/oru-kuppai-kathai-lyrics"
song: "Ninaithadhu Ellam"
image: ../../images/albumart/oru-kuppai-kathai.jpg
date: 2018-05-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/-X5x_msduuE"
type: "melody"
singers:
  - Saicharan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ninaithadhu ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaithadhu ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadikkira vaazhkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadikkira vaazhkai"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarukku kidaikirathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarukku kidaikirathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pala kanavugal ingae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala kanavugal ingae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanneer thuliyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneer thuliyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannil vazhigirathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannil vazhigirathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Viruppangal ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viruppangal ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholaindha pinnalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholaindha pinnalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vedhanai nilaikirathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedhanai nilaikirathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pala thirupangal theriyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala thirupangal theriyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadhaiyil thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadhaiyil thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Payanam nadakkirathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Payanam nadakkirathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ohoooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohoooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Manmelae naam ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manmelae naam ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nool bommai pol thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nool bommai pol thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidhi vandhu nammai izhukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidhi vandhu nammai izhukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohoo ooo naam kaanum aasaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohoo ooo naam kaanum aasaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanneeril kolangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanneeril kolangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanaalum nenjam ninaikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanaalum nenjam ninaikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninaithadhu ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaithadhu ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadikkira vaazhkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadikkira vaazhkai"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarukku kidaikirathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarukku kidaikirathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pala kanavugal ingae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala kanavugal ingae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanneer thuliyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneer thuliyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannil vazhigirathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannil vazhigirathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninaithadhu ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaithadhu ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadikkira vaazhkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadikkira vaazhkai"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarukku kidaikirathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarukku kidaikirathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pala kanavugal ingae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala kanavugal ingae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanneer thuliyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneer thuliyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannil vazhigirathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannil vazhigirathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unakkum oor kanavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkum oor kanavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada enakkum oor kanavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada enakkum oor kanavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingae kanavugal inaivathillayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingae kanavugal inaivathillayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Irulil thinam tholaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irulil thinam tholaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru nizhal polae sondham ingae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru nizhal polae sondham ingae"/>
</div>
<div class="lyrico-lyrics-wrapper">Soozhnilaigal purivathillaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soozhnilaigal purivathillaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha uravugal enbathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha uravugal enbathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidukathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidukathai"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu mudivindri thodarndhidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu mudivindri thodarndhidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thodarkathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodarkathai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ingu unarvugal enbathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingu unarvugal enbathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chitravathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chitravathai"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu thoondilil puzhuvena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu thoondilil puzhuvena"/>
</div>
<div class="lyrico-lyrics-wrapper">Sikkum vadhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sikkum vadhai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Puyal mazhai varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puyal mazhai varum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru puram veyil varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru puram veyil varum"/>
</div>
<div class="lyrico-lyrics-wrapper">Marupuram inbam thumbam irandum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marupuram inbam thumbam irandum"/>
</div>
<div class="lyrico-lyrics-wrapper">Inaindhu aattivaikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inaindhu aattivaikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhkaiyaada…aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkaiyaada…aaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nadhiyil vandhu midhakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadhiyil vandhu midhakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oor ilai polae vaazhkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oor ilai polae vaazhkai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingae sellum dhisai therivathillaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingae sellum dhisai therivathillaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Iraivan ada andrae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iraivan ada andrae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naam vazhi paadhai ezhuthi vaithaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam vazhi paadhai ezhuthi vaithaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiruppangal purivathillaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiruppangal purivathillaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha vaazhkaiyin payanamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha vaazhkaiyin payanamo"/>
</div>
<div class="lyrico-lyrics-wrapper">Siriyadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siriyadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhil kidaithidum anubavam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhil kidaithidum anubavam"/>
</div>
<div class="lyrico-lyrics-wrapper">Periyadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Periyadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ingu edhuvumae illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingu edhuvumae illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhil varuthathil thoivadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhil varuthathil thoivadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Puyal mazhai varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puyal mazhai varum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru puram veyil varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru puram veyil varum"/>
</div>
<div class="lyrico-lyrics-wrapper">Marupuram inbam thumbam irandum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marupuram inbam thumbam irandum"/>
</div>
<div class="lyrico-lyrics-wrapper">Inaindhu aattivaikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inaindhu aattivaikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhkaiyaada aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkaiyaada aaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninaithadhu ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaithadhu ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadikkira vaazhkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadikkira vaazhkai"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarukku kidaikirathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarukku kidaikirathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pala kanavugal ingae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala kanavugal ingae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanneer thuliyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneer thuliyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannil vazhigirathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannil vazhigirathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Viruppangal ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viruppangal ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholaindha pinnalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholaindha pinnalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vedhanai nilaikirathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedhanai nilaikirathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pala thirupangal theriyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala thirupangal theriyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadhaiyil thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadhaiyil thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Payanam nadakkirathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Payanam nadakkirathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ohoooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohoooooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Manmelae naam ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manmelae naam ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nool bommai pol thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nool bommai pol thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidhi vandhu nammai izhukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidhi vandhu nammai izhukkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ohoo naam kaanum aasaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohoo naam kaanum aasaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanneeril kolangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanneeril kolangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanaalum nenjam ninaikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanaalum nenjam ninaikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohooooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohooooooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninaithadhu ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaithadhu ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadikkira vaazhkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadikkira vaazhkai"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarukku kidaikirathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarukku kidaikirathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pala kanavugal ingae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala kanavugal ingae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanneer thuliyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneer thuliyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannil vazhigirathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannil vazhigirathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninaithadhu ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaithadhu ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadikkira vaazhkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadikkira vaazhkai"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarukku kidaikirathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarukku kidaikirathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pala kanavugal ingae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala kanavugal ingae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanneer thuliyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneer thuliyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannil vazhigirathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannil vazhigirathu"/>
</div>
</pre>
