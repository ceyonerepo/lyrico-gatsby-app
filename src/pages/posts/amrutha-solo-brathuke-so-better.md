---
title: "amrutha song lyrics"
album: "Solo Brathuke So Better"
artist: "S. Thaman"
lyricist: "Kasarla Shyam"
director: "Subbu"
path: "/albums/solo-brathuke-so-better-lyrics"
song: "Amrutha"
image: ../../images/albumart/solo-brathuke-so-better.jpg
date: 2020-12-25
lang: telugu
youtubeLink: "https://www.youtube.com/embed/--R7ThJH02Y"
type: "sad"
singers:
  - Nakash Aziz
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Bulb kanipettinodike
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bulb kanipettinodike"/>
</div>
<div class="lyrico-lyrics-wrapper">Bathuku simmaseekataipoindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bathuku simmaseekataipoindhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Cell phone company odike
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cell phone company odike"/>
</div>
<div class="lyrico-lyrics-wrapper">Sim card ye block ai poindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sim card ye block ai poindhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Route soope google ammane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Route soope google ammane"/>
</div>
<div class="lyrico-lyrics-wrapper">Inti route ney marchipoindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inti route ney marchipoindhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Right time cheppe watch ke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Right time cheppe watch ke"/>
</div>
<div class="lyrico-lyrics-wrapper">Bad time ye stary ai poindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bad time ye stary ai poindhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aggi pulla nene mellaga kalchukunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aggi pulla nene mellaga kalchukunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Sontha kompane full ga antukkunnadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sontha kompane full ga antukkunnadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Past life lo nenu cheppina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Past life lo nenu cheppina"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhava maate
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhava maate"/>
</div>
<div class="lyrico-lyrics-wrapper">Bright future ye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bright future ye"/>
</div>
<div class="lyrico-lyrics-wrapper">Neela thagala bettindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neela thagala bettindhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oggesi poke amrutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oggesi poke amrutha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenu thattukoka mandhu thaagutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenu thattukoka mandhu thaagutha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ottesi septhunna amrutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ottesi septhunna amrutha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuv ellipothe ontaraipotha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuv ellipothe ontaraipotha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oggesi poke amrutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oggesi poke amrutha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenu thattukoka mandhu thaagutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenu thattukoka mandhu thaagutha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ottesi septhunna amrutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ottesi septhunna amrutha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuv ellipothe ontaraipotha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuv ellipothe ontaraipotha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Five star chocolate ichi bujjagincha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Five star chocolate ichi bujjagincha"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinna pillavu kaadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna pillavu kaadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Fevicol kanna gattiga fix ayyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fevicol kanna gattiga fix ayyi"/>
</div>
<div class="lyrico-lyrics-wrapper">Chukkalu choopisthave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chukkalu choopisthave"/>
</div>
<div class="lyrico-lyrics-wrapper">Champa meedha okkatidhamante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Champa meedha okkatidhamante"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheyye ravatledhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheyye ravatledhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Hug chesukoni chepthamante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hug chesukoni chepthamante"/>
</div>
<div class="lyrico-lyrics-wrapper">Baggumantaavanna bhayame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baggumantaavanna bhayame"/>
</div>
<div class="lyrico-lyrics-wrapper">Bandaraayi lanti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bandaraayi lanti"/>
</div>
<div class="lyrico-lyrics-wrapper">Mindu settu maarchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mindu settu maarchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasu thoti linku chesthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu thoti linku chesthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Baagu padathave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baagu padathave"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee heart gate terichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee heart gate terichi"/>
</div>
<div class="lyrico-lyrics-wrapper">Neelo thongichoode
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelo thongichoode"/>
</div>
<div class="lyrico-lyrics-wrapper">Na bommane geesi undhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na bommane geesi undhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naapai love undhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naapai love undhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oggesi poke amrutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oggesi poke amrutha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenu thattukoka mandhu thaagutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenu thattukoka mandhu thaagutha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ottesi septhunna amrutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ottesi septhunna amrutha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuv ellipothe ontaraipotha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuv ellipothe ontaraipotha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oggesi poke amrutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oggesi poke amrutha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenu thattukoka mandhu thaagutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenu thattukoka mandhu thaagutha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ottesi septhunna amrutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ottesi septhunna amrutha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuv ellipothe ontaraipotha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuv ellipothe ontaraipotha"/>
</div>
</pre>
