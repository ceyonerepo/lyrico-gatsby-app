---
title: "enna petha devadhaiye song lyrics"
album: "Theal"
artist: "C. Sathya"
lyricist: "Uma Devi"
director: "Harikumar"
path: "/albums/theal-lyrics"
song: "Enna Petha Devadhaiye"
image: ../../images/albumart/theal.jpg
date: 2022-01-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/5hPB7lmwzZc"
type: "melody"
singers:
  - C. Sathya
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Enna petha devadhaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna petha devadhaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennudaye devadhaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennudaye devadhaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannukkulla oorum neer neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannukkulla oorum neer neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbukonda paarvaiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbukonda paarvaiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">Imbam kondu vandhavale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imbam kondu vandhavale"/>
</div>
<div class="lyrico-lyrics-wrapper">Mootchukkaana kaathum nee thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mootchukkaana kaathum nee thaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thani maramaa ulakil ninnene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thani maramaa ulakil ninnene"/>
</div>
<div class="lyrico-lyrics-wrapper">Aada mazhaiya unaiye thanthene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aada mazhaiya unaiye thanthene"/>
</div>
<div class="lyrico-lyrics-wrapper">Pillaikku thaayondru karuvaakuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pillaikku thaayondru karuvaakuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru thaalaattu thaimaikku uruvaakuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru thaalaattu thaimaikku uruvaakuthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sumaiyellaam tholile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sumaiyellaam tholile"/>
</div>
<div class="lyrico-lyrics-wrapper">Poo pola maaruthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poo pola maaruthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunaiyaana tholkal nee thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunaiyaana tholkal nee thaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Karayellaam neenguna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karayellaam neenguna"/>
</div>
<div class="lyrico-lyrics-wrapper">Nelavaattam maarune
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nelavaattam maarune"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhi kaattum vaanam nee thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi kaattum vaanam nee thaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjukkulla yedhumille
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjukkulla yedhumille"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenappukku yaarumille
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenappukku yaarumille"/>
</div>
<div class="lyrico-lyrics-wrapper">Oraiye unaiye manasippo marappathille
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oraiye unaiye manasippo marappathille"/>
</div>
<div class="lyrico-lyrics-wrapper">Buddhi thantha poo marame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Buddhi thantha poo marame"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna pakkam ongarame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna pakkam ongarame"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasa manasa manthirama maathuthinge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasa manasa manthirama maathuthinge"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru vali vandha thudippene naa kannaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru vali vandha thudippene naa kannaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Yengathu nee thaan maa thene ini munnaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yengathu nee thaan maa thene ini munnaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Pillaikku thaayondru karuvaakuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pillaikku thaayondru karuvaakuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru thaalaattu thaimaikku uruvaakuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru thaalaattu thaimaikku uruvaakuthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sumaiyellaam tholile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sumaiyellaam tholile"/>
</div>
<div class="lyrico-lyrics-wrapper">Poo pola maaruthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poo pola maaruthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunaiyaana tholkal nee thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunaiyaana tholkal nee thaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Karayellaam neenguna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karayellaam neenguna"/>
</div>
<div class="lyrico-lyrics-wrapper">Nelavaattam maarune
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nelavaattam maarune"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhi kaattum vaanam nee thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi kaattum vaanam nee thaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koyil kolam suthunaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koyil kolam suthunaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollaamale kooda varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollaamale kooda varum"/>
</div>
<div class="lyrico-lyrics-wrapper">Malaye malaye enna kaakkum deivam neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malaye malaye enna kaakkum deivam neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarvaiyile puriya vekkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarvaiyile puriya vekkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaarthayile valiya neekkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaarthayile valiya neekkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhaiyum veyilum un anbu koda virikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhaiyum veyilum un anbu koda virikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sirivoli naane tholithene athu unnaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirivoli naane tholithene athu unnaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannu ima pole adanjome ippo kannaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu ima pole adanjome ippo kannaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Pillaikku thaayondru karuvaakuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pillaikku thaayondru karuvaakuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru thaalaattu thaimaikku uruvaakuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru thaalaattu thaimaikku uruvaakuthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sumaiyellaam tholile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sumaiyellaam tholile"/>
</div>
<div class="lyrico-lyrics-wrapper">Poo pola maaruthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poo pola maaruthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunaiyaana tholkal nee thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunaiyaana tholkal nee thaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Karayellaam neenguna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karayellaam neenguna"/>
</div>
<div class="lyrico-lyrics-wrapper">Nelavaattam maarune
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nelavaattam maarune"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhi kaattum vaanam nee thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi kaattum vaanam nee thaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna petha devadhaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna petha devadhaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennudaye devadhaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennudaye devadhaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannukkulla oorum neer neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannukkulla oorum neer neeye"/>
</div>
</pre>
