---
title: "avunu ani kadu ani song lyrics"
album: "G Zombie"
artist: "Vinod Kumar"
lyricist: "Rambabu Gosala"
director: "Aryan & Deepu"
path: "/albums/g-zombie-lyrics"
song: "Avunu Ani Kadu Ani - "
image: ../../images/albumart/g-zombie.jpg
date: 2021-02-26
lang: telugu
youtubeLink: "https://www.youtube.com/embed/eBo-6ZkuD7I"
type: "love"
singers:
  - Saicharan Bhaskaruni
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aunani kaadhantu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aunani kaadhantu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhani aunantu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhani aunantu"/>
</div>
<div class="lyrico-lyrics-wrapper">Upire aapestu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Upire aapestu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanu champodhe maguvaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanu champodhe maguvaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rammani pommantu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rammani pommantu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pommani rammantu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pommani rammantu"/>
</div>
<div class="lyrico-lyrics-wrapper">Matale maarchesthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Matale maarchesthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhalo sadine penchodhe maguwaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhalo sadine penchodhe maguwaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nuvvani nenantu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvani nenantu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenani nuvvantu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenani nuvvantu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kshana kshanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kshana kshanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalahisthu viraham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalahisthu viraham"/>
</div>
<div class="lyrico-lyrics-wrapper">Odilo undodhe maguvaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odilo undodhe maguvaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chilipiga chiru chiru navvulathone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chilipiga chiru chiru navvulathone"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasuna alajadi penchaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasuna alajadi penchaave"/>
</div>
<div class="lyrico-lyrics-wrapper">Samayamu theliyani usulathone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samayamu theliyani usulathone"/>
</div>
<div class="lyrico-lyrics-wrapper">Jagamuna maimarapinchaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jagamuna maimarapinchaave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aunani kaadhantu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aunani kaadhantu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhani aunantu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhani aunantu"/>
</div>
<div class="lyrico-lyrics-wrapper">Upire aapestu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Upire aapestu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanu champodhe maguvaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanu champodhe maguvaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee pere japiyinche pedhavanchule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee pere japiyinche pedhavanchule"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee dhaare payaninche naa adugule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee dhaare payaninche naa adugule"/>
</div>
<div class="lyrico-lyrics-wrapper">Inallu kavvinche nee kulukule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inallu kavvinche nee kulukule"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee nimisham theereli naa ashale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee nimisham theereli naa ashale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Madhi gadhilone mellangaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhi gadhilone mellangaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pravahinchaave nadhilaagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pravahinchaave nadhilaagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Anuvanuvullo andhanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anuvanuvullo andhanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu odhigaave istangaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu odhigaave istangaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oorinchaave nee andhaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorinchaave nee andhaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Ika thelinche nanu mabbulo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ika thelinche nanu mabbulo"/>
</div>
<div class="lyrico-lyrics-wrapper">Uginche nanu hayi hayiga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uginche nanu hayi hayiga"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee uhala uyyalallo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee uhala uyyalallo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nuvvani nenantu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvani nenantu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenani nuvvantu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenani nuvvantu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kshana kshanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kshana kshanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalahisthu viraham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalahisthu viraham"/>
</div>
<div class="lyrico-lyrics-wrapper">Odilo undodhe maguvaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odilo undodhe maguvaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chilipiga chiru chiru navvulathone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chilipiga chiru chiru navvulathone"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasuna alajadi penchaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasuna alajadi penchaave"/>
</div>
<div class="lyrico-lyrics-wrapper">Samayamu theliyani usulathone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samayamu theliyani usulathone"/>
</div>
<div class="lyrico-lyrics-wrapper">Jagamuna maimarapinchaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jagamuna maimarapinchaave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aunani kaadhantu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aunani kaadhantu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhani aunantu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhani aunantu"/>
</div>
<div class="lyrico-lyrics-wrapper">Upire aapestu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Upire aapestu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanu champodhe maguvaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanu champodhe maguvaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalledhuta niluvethu sammohanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalledhuta niluvethu sammohanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kalla vaakille nakovaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kalla vaakille nakovaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Harivillu varnaale nee soyagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Harivillu varnaale nee soyagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannallu kuntundhe nee kalavaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannallu kuntundhe nee kalavaram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kala nijamavuthu kalisaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kala nijamavuthu kalisaave"/>
</div>
<div class="lyrico-lyrics-wrapper">Parimallamedho jallave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parimallamedho jallave"/>
</div>
<div class="lyrico-lyrics-wrapper">Paravashamavuthu pilichaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paravashamavuthu pilichaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Nishilo sishila merisaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nishilo sishila merisaave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee piche pattindhe naaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee piche pattindhe naaku"/>
</div>
<div class="lyrico-lyrics-wrapper">Ne puttindhe nee koraku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne puttindhe nee koraku"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoduntava naa kadavaraku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoduntava naa kadavaraku"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kougile pranam naaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kougile pranam naaku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nuvvani nenantu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvani nenantu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenani nuvvantu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenani nuvvantu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kshana kshanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kshana kshanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalahisthu viraham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalahisthu viraham"/>
</div>
<div class="lyrico-lyrics-wrapper">Odilo undodhe maguvaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odilo undodhe maguvaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chilipiga chiru chiru navvulathone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chilipiga chiru chiru navvulathone"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasuna alajadi penchaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasuna alajadi penchaave"/>
</div>
<div class="lyrico-lyrics-wrapper">Samayamu theliyani usulathone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samayamu theliyani usulathone"/>
</div>
<div class="lyrico-lyrics-wrapper">Jagamuna maimarapinchaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jagamuna maimarapinchaave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aunani kaadhantu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aunani kaadhantu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhani aunantu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhani aunantu"/>
</div>
<div class="lyrico-lyrics-wrapper">Upire aapestu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Upire aapestu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanu champodhe maguvaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanu champodhe maguvaa"/>
</div>
</pre>
