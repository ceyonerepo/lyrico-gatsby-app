---
title: "un kuthama en kuthama song lyrics"
album: "Azhagi"
artist: "Ilaiyaraaja"
lyricist: "Ilaiyaraaja"
director: "Thangar Bachchan"
path: "/albums/azhagi-lyrics"
song: "Un Kuthama En Kuthama"
image: ../../images/albumart/azhagi.jpg
date: 2002-01-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/5703mJUis_c"
type: "sad"
singers:
  - Ilaiyaraaja
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Un Kuththamaa En Kuthamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kuththamaa En Kuthamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaara Naanum Kutham Solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaara Naanum Kutham Solla"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kuththamaa En Kuthamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kuththamaa En Kuthamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaara Naanum Kutham Solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaara Naanum Kutham Solla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Kuththamaa En Kuthamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kuththamaa En Kuthamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaara Naanum Kutham Solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaara Naanum Kutham Solla"/>
</div>
<div class="lyrico-lyrics-wrapper">Pachampasum Solaiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pachampasum Solaiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadi Vantha Paynkiliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadi Vantha Paynkiliye"/>
</div>
<div class="lyrico-lyrics-wrapper">Indru Nadapaathaiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indru Nadapaathaiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhavathenna Moolaiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhavathenna Moolaiyile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koththu Nerunju Mullu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koththu Nerunju Mullu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuthuthu Nenjukkulle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuthuthu Nenjukkulle"/>
</div>
<div class="lyrico-lyrics-wrapper">Sonnaalum Sogam Amma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sonnaalum Sogam Amma"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeraatha Dhaagam Amma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeraatha Dhaagam Amma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Kuththamaa En Kuthamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kuththamaa En Kuthamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaara Naanum Kutham Solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaara Naanum Kutham Solla"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kuththamaa En Kuthamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kuththamaa En Kuthamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaara Naanum Kutham Solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaara Naanum Kutham Solla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nilavoda Mannaloda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilavoda Mannaloda"/>
</div>
<div class="lyrico-lyrics-wrapper">Therumannu Oodamboda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Therumannu Oodamboda"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilaiyaandathu Oru Kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilaiyaandathu Oru Kaalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alanjaalum Thirinjaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alanjaalum Thirinjaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Alaiyaatha Kalaiyaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alaiyaatha Kalaiyaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavaachu Ilam Kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavaachu Ilam Kaalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Edhirkaalamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Edhirkaalamo"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Edhirkaalamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Edhirkaalamo"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Pudhir Podumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Pudhir Podumo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilamaiyil Puriyaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilamaiyil Puriyaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudhimaiyil Mudiyaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhimaiyil Mudiyaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Inbathirkku Yengaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inbathirkku Yengaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilamaiyum Ingethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilamaiyum Ingethu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam Podudhu Kolangale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam Podudhu Kolangale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Kuthamaa Un Kuththamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kuthamaa Un Kuththamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaara Naanum Kutham Solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaara Naanum Kutham Solla"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu En Kuthamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu En Kuthamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pesaama Irunthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesaama Irunthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasoda Manasaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasoda Manasaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesiya Oru Kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesiya Oru Kaalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhoorathil Irunthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhoorathil Irunthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thodarnthu Unn Arugile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodarnthu Unn Arugile"/>
</div>
<div class="lyrico-lyrics-wrapper">Kulaviya Oru Kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulaviya Oru Kaalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indru Naanum Orathilll
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indru Naanum Orathilll"/>
</div>
<div class="lyrico-lyrics-wrapper">Indru Naanum Orathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indru Naanum Orathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Manathu Thoorathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Manathu Thoorathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Veedhiyil Isaithaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veedhiyil Isaithaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Veenaikku Isai Undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veenaikku Isai Undu"/>
</div>
<div class="lyrico-lyrics-wrapper">Veenaagi Pogaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veenaagi Pogaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ketkindra Nenjundu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketkindra Nenjundu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mei Kural Paaduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mei Kural Paaduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Veenaiyodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veenaiyodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Kuththamaa En Kuthamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kuththamaa En Kuthamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaara Naanum Kutham Solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaara Naanum Kutham Solla"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu Un Kuththamaa En Kuthamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu Un Kuththamaa En Kuthamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaara Naanum Kutham Solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaara Naanum Kutham Solla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pachampasum Solaiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pachampasum Solaiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadi Vantha Paynkiliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadi Vantha Paynkiliye"/>
</div>
<div class="lyrico-lyrics-wrapper">Indru Nadapaathaiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indru Nadapaathaiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhavathenna Moolaiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhavathenna Moolaiyile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koththu Nerunju Mullu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koththu Nerunju Mullu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuthuthu Nenjukkulle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuthuthu Nenjukkulle"/>
</div>
<div class="lyrico-lyrics-wrapper">Sonnaalum Sogam Amma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sonnaalum Sogam Amma"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeraatha Dhaagam Amma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeraatha Dhaagam Amma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ithu Un Kuththamaa En Kuthamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu Un Kuththamaa En Kuthamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaara Naanum Kutham Solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaara Naanum Kutham Solla"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kuththamaa En Kuthamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kuththamaa En Kuthamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaara Naanum Kutham Solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaara Naanum Kutham Solla"/>
</div>
</pre>
