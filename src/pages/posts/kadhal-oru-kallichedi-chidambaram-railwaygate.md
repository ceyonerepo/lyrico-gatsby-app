---
title: "kadhal oru kallichedi song lyrics"
album: "Chidambaram Railwaygate"
artist: "Karthik Raja"
lyricist: "Arun Bharathi"
director: "Sivabalan"
path: "/albums/chidambaram-railwaygate-lyrics"
song: "Kadhal Oru Kallichedi"
image: ../../images/albumart/chidambaram-railwaygate.jpg
date: 2021-02-05
lang: tamil
youtubeLink: "https://www.youtube.com/embed/TRJ2xtoW9N4"
type: "Sad"
singers:
  - Karthik Raja
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">kadhal oru kallichedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhal oru kallichedi"/>
</div>
<div class="lyrico-lyrics-wrapper">athu kothi sellum kandapadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu kothi sellum kandapadi"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhal dhinam kollum vali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhal dhinam kollum vali"/>
</div>
<div class="lyrico-lyrics-wrapper">athu kangal seitha kutramadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu kangal seitha kutramadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kadhal oru kallichedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhal oru kallichedi"/>
</div>
<div class="lyrico-lyrics-wrapper">athu kothi sellum kandapadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu kothi sellum kandapadi"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhal dhinam kollum vali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhal dhinam kollum vali"/>
</div>
<div class="lyrico-lyrics-wrapper">athu kangal seitha kutramadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu kangal seitha kutramadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">onnodu onnaga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="onnodu onnaga "/>
</div>
<div class="lyrico-lyrics-wrapper">undaana kadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="undaana kadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">ellame poiyaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellame poiyaga"/>
</div>
<div class="lyrico-lyrics-wrapper">pogindra kaanal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pogindra kaanal"/>
</div>
<div class="lyrico-lyrics-wrapper">puriyamal theriyamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puriyamal theriyamal"/>
</div>
<div class="lyrico-lyrics-wrapper">suthi varuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suthi varuven"/>
</div>
<div class="lyrico-lyrics-wrapper">athu kadhal thodave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu kadhal thodave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">idhayathil yeeno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhayathil yeeno"/>
</div>
<div class="lyrico-lyrics-wrapper">pala nooru kaayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pala nooru kaayam"/>
</div>
<div class="lyrico-lyrics-wrapper">ne vanthu pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ne vanthu pona"/>
</div>
<div class="lyrico-lyrics-wrapper">kannerum kaayum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannerum kaayum"/>
</div>
<div class="lyrico-lyrics-wrapper">pirivondru vanthaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pirivondru vanthaal"/>
</div>
<div class="lyrico-lyrics-wrapper">ellame maayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellame maayam"/>
</div>
<div class="lyrico-lyrics-wrapper">vidhi podum thaayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidhi podum thaayam"/>
</div>
<div class="lyrico-lyrics-wrapper">vilaiyadu neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vilaiyadu neeyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">alagana vaazhkai ondrai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alagana vaazhkai ondrai"/>
</div>
<div class="lyrico-lyrics-wrapper">thanthu vida vantha kadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanthu vida vantha kadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">aliyatha sogam onnai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aliyatha sogam onnai"/>
</div>
<div class="lyrico-lyrics-wrapper">thanthu vitu ponathenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanthu vitu ponathenna"/>
</div>
<div class="lyrico-lyrics-wrapper">ennagum ennagum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennagum ennagum "/>
</div>
<div class="lyrico-lyrics-wrapper">nenjam ketkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjam ketkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">punnagi ponalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="punnagi ponalum"/>
</div>
<div class="lyrico-lyrics-wrapper">pookal pookuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pookal pookuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ada neyum vanthudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada neyum vanthudu"/>
</div>
<div class="lyrico-lyrics-wrapper">uyir mella thanthidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyir mella thanthidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thunaiyaga vantha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thunaiyaga vantha "/>
</div>
<div class="lyrico-lyrics-wrapper">natpondru engee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="natpondru engee"/>
</div>
<div class="lyrico-lyrics-wrapper">thaniyaga vaadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaniyaga vaadum"/>
</div>
<div class="lyrico-lyrics-wrapper">uyirondru ingee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyirondru ingee"/>
</div>
<div class="lyrico-lyrics-wrapper">thuyarangal illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thuyarangal illa"/>
</div>
<div class="lyrico-lyrics-wrapper">ullagum than engee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ullagum than engee"/>
</div>
<div class="lyrico-lyrics-wrapper">idamondru vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idamondru vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">uyir vaazha angee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyir vaazha angee"/>
</div>
<div class="lyrico-lyrics-wrapper">kannadi kaatukindra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannadi kaatukindra"/>
</div>
<div class="lyrico-lyrics-wrapper">bimbam thanai pola mella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bimbam thanai pola mella"/>
</div>
<div class="lyrico-lyrics-wrapper">munadi nabagangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munadi nabagangal"/>
</div>
<div class="lyrico-lyrics-wrapper">vanthu nindral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanthu nindral"/>
</div>
<div class="lyrico-lyrics-wrapper">enna solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna solla"/>
</div>
<div class="lyrico-lyrics-wrapper">sollamal kollamal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sollamal kollamal "/>
</div>
<div class="lyrico-lyrics-wrapper">sontham vanthathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sontham vanthathu"/>
</div>
<div class="lyrico-lyrics-wrapper">illamal ipothu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="illamal ipothu "/>
</div>
<div class="lyrico-lyrics-wrapper">enge sentrathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enge sentrathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ada neyum vanthidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada neyum vanthidu"/>
</div>
<div class="lyrico-lyrics-wrapper">uyir mella thanthidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyir mella thanthidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kadhal oru kallichedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhal oru kallichedi"/>
</div>
<div class="lyrico-lyrics-wrapper">athu kothi sellum kandapadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu kothi sellum kandapadi"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhal dhinam kollum vali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhal dhinam kollum vali"/>
</div>
<div class="lyrico-lyrics-wrapper">athu kangal seitha kutramadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu kangal seitha kutramadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">onnodu onnaga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="onnodu onnaga "/>
</div>
<div class="lyrico-lyrics-wrapper">undaana kadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="undaana kadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">ellame poiyagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellame poiyagi"/>
</div>
<div class="lyrico-lyrics-wrapper">pogindra kaanal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pogindra kaanal"/>
</div>
<div class="lyrico-lyrics-wrapper">puriyamal theriyamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puriyamal theriyamal"/>
</div>
<div class="lyrico-lyrics-wrapper">suthi varuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suthi varuven"/>
</div>
<div class="lyrico-lyrics-wrapper">athu kadhal thodave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu kadhal thodave"/>
</div>
</pre>
