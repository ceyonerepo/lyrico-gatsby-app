---
title: "matta sekar song lyrics"
album: "Annanukku Jai"
artist: "Arrol Corelli"
lyricist: "Emcee Jesz"
director: "Rajkumar"
path: "/albums/annanukku-jai-lyrics"
song: "Matta Sekar"
image: ../../images/albumart/annanukku-jai.jpg
date: 2018-08-31
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ReaGqTG9G3A"
type: "happy"
singers:
  - Emcee Jesz
  - Stevie
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">En peru matta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En peru matta"/>
</div>
<div class="lyrico-lyrics-wrapper">En peru matta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En peru matta"/>
</div>
<div class="lyrico-lyrics-wrapper">En peru matta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En peru matta"/>
</div>
<div class="lyrico-lyrics-wrapper">Sekar-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sekar-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai nee thotta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai nee thotta"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai nee thotta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai nee thotta"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai nee thotta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai nee thotta"/>
</div>
<div class="lyrico-lyrics-wrapper">Danger-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Danger-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En peru matta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En peru matta"/>
</div>
<div class="lyrico-lyrics-wrapper">En peru matta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En peru matta"/>
</div>
<div class="lyrico-lyrics-wrapper">En peru matta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En peru matta"/>
</div>
<div class="lyrico-lyrics-wrapper">Sekar-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sekar-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai nee thotta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai nee thotta"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai nee thotta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai nee thotta"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai nee thotta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai nee thotta"/>
</div>
<div class="lyrico-lyrics-wrapper">Danger-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Danger-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mokka piece-u dammi piece-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mokka piece-u dammi piece-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Side-u vangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Side-u vangu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatha koduthu nalla kelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatha koduthu nalla kelu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanthan weight-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanthan weight-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Adipatta pulikitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adipatta pulikitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Palikaathu un settai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palikaathu un settai"/>
</div>
<div class="lyrico-lyrics-wrapper">Matta kitta maattuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Matta kitta maattuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee sappi potta kottai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee sappi potta kottai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En sollu mirattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En sollu mirattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Theriya therikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theriya therikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">En padai kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En padai kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Irangi nerukki seyyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irangi nerukki seyyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Meesaya murukki varen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meesaya murukki varen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai vella mudiyathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai vella mudiyathu"/>
</div>
<div class="lyrico-lyrics-wrapper">En attagasam athigaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En attagasam athigaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattaya kelappathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattaya kelappathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En peru matta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En peru matta"/>
</div>
<div class="lyrico-lyrics-wrapper">En peru matta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En peru matta"/>
</div>
<div class="lyrico-lyrics-wrapper">En peru matta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En peru matta"/>
</div>
<div class="lyrico-lyrics-wrapper">Sekar-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sekar-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai nee thotta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai nee thotta"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai nee thotta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai nee thotta"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai nee thotta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai nee thotta"/>
</div>
<div class="lyrico-lyrics-wrapper">Danger-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Danger-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En peru matta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En peru matta"/>
</div>
<div class="lyrico-lyrics-wrapper">En peru matta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En peru matta"/>
</div>
<div class="lyrico-lyrics-wrapper">En peru matta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En peru matta"/>
</div>
<div class="lyrico-lyrics-wrapper">Sekar-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sekar-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai nee thotta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai nee thotta"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai nee thotta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai nee thotta"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai nee thotta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai nee thotta"/>
</div>
<div class="lyrico-lyrics-wrapper">Danger-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Danger-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mersal illaiMersal illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mersal illaiMersal illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mersal illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mersal illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mersal enbathu illaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mersal enbathu illaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mersal illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mersal illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mersal illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mersal illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mersal illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mersal illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mersal enbathu illaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mersal enbathu illaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mersal illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mersal illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mersal illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mersal illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mersal illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mersal illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mersal enbathu illaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mersal enbathu illaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Madhayanai thotruvidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhayanai thotruvidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sinam konda singathidam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sinam konda singathidam"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai mothum padaiyellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai mothum padaiyellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Puramudhugu kaatividum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puramudhugu kaatividum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuzhi thondum pala peru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuzhi thondum pala peru"/>
</div>
<div class="lyrico-lyrics-wrapper">Varum pogum varalaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varum pogum varalaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru yutham varum podhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru yutham varum podhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sari thavaru kidaiyathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sari thavaru kidaiyathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pottu varuven raja nadai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pottu varuven raja nadai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidamal koduppen erichalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidamal koduppen erichalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nikkama nikkama thurathumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikkama nikkama thurathumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma athiri puthiri aattamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma athiri puthiri aattamae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalagam seiyurom kilambiru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalagam seiyurom kilambiru"/>
</div>
<div class="lyrico-lyrics-wrapper">Munnadi varathae vilagidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munnadi varathae vilagidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodukkura soundula 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodukkura soundula "/>
</div>
<div class="lyrico-lyrics-wrapper">kizhiyithu sevulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kizhiyithu sevulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adikkira adiyila 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adikkira adiyila "/>
</div>
<div class="lyrico-lyrics-wrapper">piriyumae javvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="piriyumae javvu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En makkal en pakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En makkal en pakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellamae en sattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellamae en sattam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aangara thaandavam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aangara thaandavam"/>
</div>
<div class="lyrico-lyrics-wrapper">Inimela aarambam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inimela aarambam"/>
</div>
<div class="lyrico-lyrics-wrapper">Bayamilla ilangandru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bayamilla ilangandru"/>
</div>
<div class="lyrico-lyrics-wrapper">Paayuthu veri kondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paayuthu veri kondu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mothavae mudiyathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mothavae mudiyathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Moodittu nee kelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moodittu nee kelu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Udai vaalai veesavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udai vaalai veesavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Athigaram pirakkuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athigaram pirakkuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethiraali maarbilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethiraali maarbilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Patham paarka thudikuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Patham paarka thudikuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adaiyalam pathikkavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adaiyalam pathikkavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadai meeri paayuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadai meeri paayuthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Annanukku jey jey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annanukku jey jey"/>
</div>
<div class="lyrico-lyrics-wrapper">Annanukku jey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annanukku jey"/>
</div>
<div class="lyrico-lyrics-wrapper">Annanukku jey jey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annanukku jey jey"/>
</div>
<div class="lyrico-lyrics-wrapper">Annanukku jey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annanukku jey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Annanukku jey jey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annanukku jey jey"/>
</div>
<div class="lyrico-lyrics-wrapper">Annanukku jey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annanukku jey"/>
</div>
<div class="lyrico-lyrics-wrapper">Annanukku jey jey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annanukku jey jey"/>
</div>
<div class="lyrico-lyrics-wrapper">Annanukku jey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annanukku jey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Annanukku jey jey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annanukku jey jey"/>
</div>
<div class="lyrico-lyrics-wrapper">Annanukku jey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annanukku jey"/>
</div>
<div class="lyrico-lyrics-wrapper">Annanukku jey jey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annanukku jey jey"/>
</div>
<div class="lyrico-lyrics-wrapper">Annanukku jey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annanukku jey"/>
</div>
</pre>
