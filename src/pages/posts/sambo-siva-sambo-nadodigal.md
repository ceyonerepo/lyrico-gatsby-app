---
title: "sambo siva sambo song lyrics"
album: "Nadodigal"
artist: "Sundar C Babu"
lyricist: "Yugabharathi"
director: "Samuthrakani"
path: "/albums/nadodigal-lyrics"
song: "Sambo Siva Sambo"
image: ../../images/albumart/nadodigal.jpg
date: 2009-06-26
lang: tamil
youtubeLink: "https://www.youtube.com/embed/nYFq-aM84_Q"
type: "Mass"
singers:
  - Vikram
  - Rita
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Sambo Siva Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sambo Siva Sambo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Siva Siva Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siva Siva Sambo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sambo Siva Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sambo Siva Sambo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Siva Siva Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siva Siva Sambo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Urangum Mirugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urangum Mirugam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ezhundhuvidattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhundhuvidattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thodangum Kalagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodangum Kalagam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thunindhuvidattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunindhuvidattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Padhungum Narigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padhungum Narigal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Madindhuvidattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madindhuvidattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tholgal Thimirattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholgal Thimirattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thudikkum Idhayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thudikkum Idhayam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kozhundhuvidattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kozhundhuvidattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Therikkum Thisaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Therikkum Thisaigal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Norungividattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Norungividattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vedikkum Pagaimai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedikkum Pagaimai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maraindhuvidattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maraindhuvidattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Natpe Jeikkattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natpe Jeikkattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sambo Siva Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sambo Siva Sambo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Siva Siva Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siva Siva Sambo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sambo Siva Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sambo Siva Sambo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Siva Siva Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siva Siva Sambo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeyenna Naanum Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyenna Naanum Enna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Beadhangal Thevaiyillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Beadhangal Thevaiyillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ellorum Urave Endraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellorum Urave Endraal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sogangal Yedhum Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sogangal Yedhum Illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sirikkindra Neram Mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirikkindra Neram Mattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Natpendru Theangidaadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natpendru Theangidaadhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhugindra Neramkooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhugindra Neramkooda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Natpunda Neengidaadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natpunda Neengidaadhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tholviye Endrum Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholviye Endrum Illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thunindha Pinbu Bayame illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunindha Pinbu Bayame illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vetriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetriyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sambo Siva Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sambo Siva Sambo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Siva Siva Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siva Siva Sambo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sambo Siva Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sambo Siva Sambo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Siva Siva Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siva Siva Sambo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Urangum Mirugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urangum Mirugam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ezhundhuvidattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhundhuvidattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thodangum Kalagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodangum Kalagam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thunindhuvidattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunindhuvidattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Padhungum Narigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padhungum Narigal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Madindhuvidattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madindhuvidattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tholgal Thimirattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholgal Thimirattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thudikkum Idhayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thudikkum Idhayam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kozhundhuvidattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kozhundhuvidattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Therikkum Thisaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Therikkum Thisaigal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Norungividattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Norungividattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vedikkum Pagaimai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedikkum Pagaimai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maraindhuvidattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maraindhuvidattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Natpe Jeyikkattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natpe Jeyikkattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sambo Siva Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sambo Siva Sambo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Siva Siva Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siva Siva Sambo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sambo Siva Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sambo Siva Sambo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Siva Siva Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siva Siva Sambo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yekkangal Theerum Mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yekkangal Theerum Mattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhvadhaa Vaazhkkaiyaagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvadhaa Vaazhkkaiyaagum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aasaikki Vaazhum Vaazhkkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasaikki Vaazhum Vaazhkkai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aatridai Kolamaagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aatridai Kolamaagum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poi Vedam Vaazhvadhillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poi Vedam Vaazhvadhillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mannodu Veezhum Veezhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannodu Veezhum Veezhum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Natpaale Oorum Ulagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natpaale Oorum Ulagum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennaalum VaazhumVaazhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaalum VaazhumVaazhum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saasthiram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saasthiram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Natpukkillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natpukkillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aasthiram Natpukkundu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasthiram Natpukkundu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaattave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sambo Siva Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sambo Siva Sambo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Siva Siva Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siva Siva Sambo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sambo Siva Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sambo Siva Sambo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Siva Siva Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siva Siva Sambo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sambo Siva Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sambo Siva Sambo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Siva Siva Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siva Siva Sambo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sambo Siva Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sambo Siva Sambo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Siva Siva Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siva Siva Sambo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eriyum Vizhigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eriyum Vizhigal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uranguvathenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uranguvathenna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theriyum Dhisaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theriyum Dhisaigal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Posunguvadhenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Posunguvadhenna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mudiyum Thuyaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudiyum Thuyaram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thimiruvadhenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thimiruvadhenna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjil Anal Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil Anal Enna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maraiyum Pozhuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maraiyum Pozhuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thirumbuvadhenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirumbuvadhenna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manadhai Bayamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manadhai Bayamum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nerungubadhenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nerungubadhenna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iniyum Iniyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iniyum Iniyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thayanguvadhenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thayanguvadhenna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Soll Soll Badhil Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soll Soll Badhil Enna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sambo Siva Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sambo Siva Sambo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Siva Siva Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siva Siva Sambo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sambo Siva Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sambo Siva Sambo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Siva Siva Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siva Siva Sambo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sambo Siva Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sambo Siva Sambo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Siva Siva Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siva Siva Sambo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sambo Siva Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sambo Siva Sambo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Siva Siva Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siva Siva Sambo"/>
</div>
</pre>
