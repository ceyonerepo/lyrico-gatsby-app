---
title: "rockstar robber song lyrics"
album: "Sindhubaadh"
artist: "Yuvan Shankar Raja"
lyricist: "Rahul Raja"
director: "S.U. Arun Kumar"
path: "/albums/sindhubaadh-lyrics"
song: "Rockstar Robber"
image: ../../images/albumart/sindhubaadh.jpg
date: 2019-06-27
lang: tamil
youtubeLink: "https://www.youtube.com/embed/tNjEcfwubOE"
type: "happy"
singers:
  - ADK
  - Shankar Raj
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Oorukulla Raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorukulla Raja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eppovumey Naan thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppovumey Naan thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oorukulla Raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorukulla Raja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eppovumey Naan thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppovumey Naan thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oorukulla Rajaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorukulla Rajaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eppovumey Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppovumey Naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Collar Ah Thaan Thookki Vittuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Collar Ah Thaan Thookki Vittuko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vettiya Dhaan Madichu Kattiko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vettiya Dhaan Madichu Kattiko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meesaiya Thaan Murikki Vittuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meesaiya Thaan Murikki Vittuko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhillaga Thiruttu Senjiko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhillaga Thiruttu Senjiko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Suttalum Athula Koodaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suttalum Athula Koodaiyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kolgaiya Maatha Mattom Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolgaiya Maatha Mattom Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Weightaana Piece Ah Parthutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Weightaana Piece Ah Parthutta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lite Ah Dhaan Kaiya Vaippom Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lite Ah Dhaan Kaiya Vaippom Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaar Ivaru Parkka Mattom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar Ivaru Parkka Mattom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tight Ah Thaan Sketch Poduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tight Ah Thaan Sketch Poduvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naekaaga Dhaan Ulla Nulanju Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naekaaga Dhaan Ulla Nulanju Dhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Note Ah Adipom Da Deiii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Note Ah Adipom Da Deiii"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanga Rockstaru Robberru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga Rockstaru Robberru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanga Rockstaru Robberru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga Rockstaru Robberru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanga Rockstaru Robberru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga Rockstaru Robberru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanga Rockstaru Robberru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga Rockstaru Robberru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ezhai Vaiyithil Kaiya Vaikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhai Vaiyithil Kaiya Vaikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mattom Mottom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mattom Mottom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Purse Ah Pudungi Thutta Nirappi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Purse Ah Pudungi Thutta Nirappi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koduppom Koduppom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koduppom Koduppom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanni Pottu Ragala Panna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanni Pottu Ragala Panna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mattom Mottom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mattom Mottom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sunday Naalum Namma Velai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sunday Naalum Namma Velai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nadakkum Nadakkum Nadakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadakkum Nadakkum Nadakkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veththalaiya Madichu Pottuka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veththalaiya Madichu Pottuka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gethaaga Nee Nadaiya Pottuka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gethaaga Nee Nadaiya Pottuka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Style Ah Thaan Unna Maththika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Style Ah Thaan Unna Maththika"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thee Aaga Velai Senjika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thee Aaga Velai Senjika"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engalukku Kalla Ullam Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engalukku Kalla Ullam Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aanaalum Nalla Ullam Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanaalum Nalla Ullam Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Puli Pola Pathunguvomada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puli Pola Pathunguvomada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Time Paarthu Paaiyivomadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Time Paarthu Paaiyivomadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Leftnaalum Ok Thaanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Leftnaalum Ok Thaanda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Right Naalum Ok Thaanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Right Naalum Ok Thaanda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nammoda Kadhaiya Kelu Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nammoda Kadhaiya Kelu Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vera Mathiri Daiii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vera Mathiri Daiii"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanga Rockstaru Robberru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga Rockstaru Robberru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanga Rockstaru Robberru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga Rockstaru Robberru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanga Rockstaru Robberru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga Rockstaru Robberru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanga Rockstaru Robberru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga Rockstaru Robberru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pappara Pappara Pappaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pappara Pappara Pappaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rockstaru Robberru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rockstaru Robberru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rockstaru Robberru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rockstaru Robberru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rockstaru Robberru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rockstaru Robberru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rockstaru Robberru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rockstaru Robberru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rockstaru Robberru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rockstaru Robberru"/>
</div>
</pre>
