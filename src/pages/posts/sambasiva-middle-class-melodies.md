---
title: "sambasiva song lyrics"
album: "Middle Class Melodies"
artist: "Sweekar Agasthi"
lyricist: "Palnadu Janapadam"
director: "Vinod Anantoju"
path: "/albums/middle-class-melodies-lyrics"
song: "Sambasiva"
image: ../../images/albumart/middle-class-melodies.jpg
date: 2020-11-20
lang: telugu
youtubeLink: "https://www.youtube.com/embed/q_b59X38kvM"
type: "happy"
singers:
  - Ram Miryala
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Samba Siva Needhu Mahima
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samba Siva Needhu Mahima"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennatiki Teliyadhaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennatiki Teliyadhaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Samba Siva Needhu Mahima
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samba Siva Needhu Mahima"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennatiki Teliyadhaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennatiki Teliyadhaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Samba Siva Needhu Mahima
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samba Siva Needhu Mahima"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennatiki Teliyadhaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennatiki Teliyadhaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hara Hara Siva Siva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hara Hara Siva Siva"/>
</div>
<div class="lyrico-lyrics-wrapper">Hara Hara Siva Siva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hara Hara Siva Siva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aa Ganga Jalam Thechi Neeku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Ganga Jalam Thechi Neeku "/>
</div>
<div class="lyrico-lyrics-wrapper">Abhishekam Settunante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Abhishekam Settunante"/>
</div>
<div class="lyrico-lyrics-wrapper">Ganga Jalam Thechi Neeku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ganga Jalam Thechi Neeku "/>
</div>
<div class="lyrico-lyrics-wrapper">Abhishekam Settunante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Abhishekam Settunante"/>
</div>
<div class="lyrico-lyrics-wrapper">Mari Ganga Jalamuna Sepakappala 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mari Ganga Jalamuna Sepakappala "/>
</div>
<div class="lyrico-lyrics-wrapper">Engilantunnavu Sambho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engilantunnavu Sambho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hara Hara Siva Siva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hara Hara Siva Siva"/>
</div>
<div class="lyrico-lyrics-wrapper">Hara Hara Siva Siva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hara Hara Siva Siva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aa Samba Siva Needhu Mahima
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Samba Siva Needhu Mahima"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennatiki Teliyadhaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennatiki Teliyadhaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hara Hara Siva Siva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hara Hara Siva Siva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aa Aavu Paalu Thechi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Aavu Paalu Thechi "/>
</div>
<div class="lyrico-lyrics-wrapper">Neeku Arpithamu Settunante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeku Arpithamu Settunante"/>
</div>
<div class="lyrico-lyrics-wrapper">Aavu Paalu Thechi Neeku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aavu Paalu Thechi Neeku "/>
</div>
<div class="lyrico-lyrics-wrapper">Arpithamu Settunante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arpithamu Settunante"/>
</div>
<div class="lyrico-lyrics-wrapper">Aavupala Legadhudala 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aavupala Legadhudala "/>
</div>
<div class="lyrico-lyrics-wrapper">Yengilantunnavu Shambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yengilantunnavu Shambo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hara Hara Oho Siva Siva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hara Hara Oho Siva Siva"/>
</div>
<div class="lyrico-lyrics-wrapper">Gattigaa Hara Hara Siva Siva Adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gattigaa Hara Hara Siva Siva Adi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Samba Siva Needhu Mahima
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samba Siva Needhu Mahima"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennatiki Teliyadhaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennatiki Teliyadhaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Samba Siva Needhu Mahima
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samba Siva Needhu Mahima"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennatiki Teliyadhaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennatiki Teliyadhaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ahaa Oho Oho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahaa Oho Oho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tummi  Poolu Tecchi Neeku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tummi  Poolu Tecchi Neeku "/>
</div>
<div class="lyrico-lyrics-wrapper">Tushtuga Poojintunante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tushtuga Poojintunante"/>
</div>
<div class="lyrico-lyrics-wrapper">Tummi Poolu Tecchi Neeku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tummi Poolu Tecchi Neeku "/>
</div>
<div class="lyrico-lyrics-wrapper">Tushtuga Poojintunante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tushtuga Poojintunante"/>
</div>
<div class="lyrico-lyrics-wrapper">Kommaa Kommana Koti 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kommaa Kommana Koti "/>
</div>
<div class="lyrico-lyrics-wrapper">Tummedala Engilantunnavu Siva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tummedala Engilantunnavu Siva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hara Hara Siva Siva Arre
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hara Hara Siva Siva Arre"/>
</div>
<div class="lyrico-lyrics-wrapper">Hara Hara Siva Siva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hara Hara Siva Siva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Samba Siva Needhu Mahima
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samba Siva Needhu Mahima"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennatiki Teliyadhaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennatiki Teliyadhaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hara Hara Gattigaa Siva Siva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hara Hara Gattigaa Siva Siva"/>
</div>
<div class="lyrico-lyrics-wrapper">Naarikelamu Tecchi Neeku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naarikelamu Tecchi Neeku "/>
</div>
<div class="lyrico-lyrics-wrapper">Naivedhyamu Settunante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naivedhyamu Settunante"/>
</div>
<div class="lyrico-lyrics-wrapper">Naarikelamu Tecchi Neeku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naarikelamu Tecchi Neeku "/>
</div>
<div class="lyrico-lyrics-wrapper">Naivedhyamu Settunante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naivedhyamu Settunante"/>
</div>
<div class="lyrico-lyrics-wrapper">Appudu Bahuishtamu Antivi Sambho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appudu Bahuishtamu Antivi Sambho"/>
</div>
<div class="lyrico-lyrics-wrapper">Saami Hara Hara Siva Siva Aaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saami Hara Hara Siva Siva Aaha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hara Hara Oho Siva Siva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hara Hara Oho Siva Siva"/>
</div>
<div class="lyrico-lyrics-wrapper">Hara Hara Siva Siva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hara Hara Siva Siva"/>
</div>
<div class="lyrico-lyrics-wrapper">Hara Hara Siva Siva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hara Hara Siva Siva"/>
</div>
</pre>
