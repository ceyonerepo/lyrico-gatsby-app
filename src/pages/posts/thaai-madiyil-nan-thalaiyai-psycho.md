---
title: "thaai madiyil nan thalaiyai song lyrics"
album: "Psycho"
artist: "Ilayaraja"
lyricist: "Mysskin"
director: "Mysskin"
path: "/albums/psycho-song-lyrics"
song: "Thaai Madiyil Nan Thalaiyai"
image: ../../images/albumart/psycho.jpg
date: 2020-01-24
lang: tamil
youtubeLink: "https://www.youtube.com/embed/5krSubVMV7w"
type: "Sentiment"
singers:
  - Kailash Kher
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Thaaimadiyil Naan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaimadiyil Naan "/>
</div>
<div class="lyrico-lyrics-wrapper">Thalaiyai Saaikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaiyai Saaikkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Thangame Nyaana Thangame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangame Nyaana Thangame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaaimadiyil Naan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaimadiyil Naan "/>
</div>
<div class="lyrico-lyrics-wrapper">Thalaiyai Saaikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaiyai Saaikkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Thangame Nyaana Thangame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangame Nyaana Thangame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Poomadi Enakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Poomadi Enakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Kidaikkavumillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kidaikkavumillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogum Vazhikku Un Ninaive Thunai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogum Vazhikku Un Ninaive Thunai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaraaro Paadu Kanmaniye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraaro Paadu Kanmaniye"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaraaro Paadu Kanmaniye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraaro Paadu Kanmaniye"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaraaro Paadu Kanmaniye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraaro Paadu Kanmaniye"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanmaniye En Ponmaniye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanmaniye En Ponmaniye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaaimadiyil Naan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaimadiyil Naan "/>
</div>
<div class="lyrico-lyrics-wrapper">Thalaiyai Saaikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaiyai Saaikkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Thangame Nyaana Thangame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangame Nyaana Thangame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sogam Thaangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sogam Thaangi"/>
</div>
<div class="lyrico-lyrics-wrapper">Baaram Irakka Yaarum Illaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baaram Irakka Yaarum Illaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaagam Theerkka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaagam Theerkka"/>
</div>
<div class="lyrico-lyrics-wrapper">Sunaiyaai Ingu Karunai Illaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sunaiyaai Ingu Karunai Illaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kobam Vaazhvil Nizhalaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kobam Vaazhvil Nizhalaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Oodi Aadi Alaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oodi Aadi Alaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Paasam Nenjil Kanalaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paasam Nenjil Kanalaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ongi Yengi Eriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ongi Yengi Eriya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaatre En
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatre En"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatre Un
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatre Un"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaalaattil Indru Thoongiduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaalaattil Indru Thoongiduven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaaimadiyil Naan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaimadiyil Naan "/>
</div>
<div class="lyrico-lyrics-wrapper">Thalaiyai Saaikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaiyai Saaikkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Thangame Nyaana Thangame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangame Nyaana Thangame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaayam Seidha Manidhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaayam Seidha Manidhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Indru Irulil Karaigiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indru Irulil Karaigiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Nyaayam Seidha Manadhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nyaayam Seidha Manadhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninaithu Ozhiyil Nanaigiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaithu Ozhiyil Nanaigiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalam Meendum Maara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam Meendum Maara"/>
</div>
<div class="lyrico-lyrics-wrapper">Maayam Kaiyil Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maayam Kaiyil Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nyaalam Meedum Maara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nyaalam Meedum Maara"/>
</div>
<div class="lyrico-lyrics-wrapper">Baaram Nenjil Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baaram Nenjil Illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaaye En
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaye En"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaaye Un
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaye Un"/>
</div>
<div class="lyrico-lyrics-wrapper">Sei Ingu Karuvil Kalanthiduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sei Ingu Karuvil Kalanthiduven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaaimadiyil Naan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaimadiyil Naan "/>
</div>
<div class="lyrico-lyrics-wrapper">Thalaiyai Saaikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaiyai Saaikkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Thangame Nyaana Thangame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangame Nyaana Thangame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Poomadi Enakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Poomadi Enakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Kidaikkavumillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kidaikkavumillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogum Vazhikku Un Ninaive Thunai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogum Vazhikku Un Ninaive Thunai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaraaro Paadu Kanmaniye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraaro Paadu Kanmaniye"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaraaro Paadu Kanmaniye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraaro Paadu Kanmaniye"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaraaro Paadu Kanmaniye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraaro Paadu Kanmaniye"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanmaniye En Ponmaniye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanmaniye En Ponmaniye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaaimadiyil Naan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaimadiyil Naan "/>
</div>
<div class="lyrico-lyrics-wrapper">Thalaiyai Saaikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaiyai Saaikkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Thangame Nyaana Thangame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangame Nyaana Thangame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaaimadiyil Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaimadiyil Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalaiyai Saaikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaiyai Saaikkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Thangame Nyaana Thangame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangame Nyaana Thangame"/>
</div>
</pre>
