---
title: "pa pa pappa song lyrics"
album: "Deiva Thirumagal"
artist: "G.V. Prakash Kumar"
lyricist: "Na. Muthukumar"
director: "A.L. Vijay"
path: "/albums/deiva-thirumagal-lyrics"
song: "Pa Pa Pappa"
image: ../../images/albumart/deiva-thirumagal.jpg
date: 2011-07-15
lang: tamil
youtubeLink: "https://www.youtube.com/embed/vL59TYrZOLM"
type: "happy"
singers:
  - Vikram
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Pa Pa Pa Pappappapa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pa Pa Pa Pappappapa"/>
</div>
<div class="lyrico-lyrics-wrapper">Varudhe Enakku Papa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varudhe Enakku Papa"/>
</div>
<div class="lyrico-lyrics-wrapper">Appappapa Appa Appa Appa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appappapa Appa Appa Appa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhusaaga Bommai Ketpa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhusaaga Bommai Ketpa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inime En
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inime En"/>
</div>
<div class="lyrico-lyrics-wrapper">Aattamum Kondaattamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aattamum Kondaattamum"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhoolu Dhoolu Thaambaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhoolu Dhoolu Thaambaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ha Ha Ha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ha Ha Ha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pa Pa Pa Pappappapa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pa Pa Pa Pappappapa"/>
</div>
<div class="lyrico-lyrics-wrapper">Varudhe Enakku Papa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varudhe Enakku Papa"/>
</div>
<div class="lyrico-lyrics-wrapper">Appappapa Appa Appa Appa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appappapa Appa Appa Appa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhusaaga Bommai Ketpa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhusaaga Bommai Ketpa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inime En
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inime En"/>
</div>
<div class="lyrico-lyrics-wrapper">Aattamum Kondaattamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aattamum Kondaattamum"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhoolu Dhoolu Thaambaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhoolu Dhoolu Thaambaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Krishna Ivlo Panam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Krishna Ivlo Panam"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Vaanga Pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Vaanga Pora"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannukku Kannukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannukku Kannukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Mai Vaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mai Vaanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalukku Kolusu Thaan Vaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalukku Kolusu Thaan Vaanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ah Appuram Yedho Sonnaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ah Appuram Yedho Sonnaanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Aiyaiyo Marandhenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyaiyo Marandhenga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hana Kai Sattai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hana Kai Sattai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanga Poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanga Poren"/>
</div>
<div class="lyrico-lyrics-wrapper">Gavunundhaan Vaanga Poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gavunundhaan Vaanga Poren"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadaitheruva Thediporen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadaitheruva Thediporen"/>
</div>
<div class="lyrico-lyrics-wrapper">Gudhurai Mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gudhurai Mela"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ellam Paappaakuthaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellam Paappaakuthaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhanukku Onnum Ilaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhanukku Onnum Ilaiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mmm Irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mmm Irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Apple Naan Vaanga Poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Apple Naan Vaanga Poren"/>
</div>
<div class="lyrico-lyrics-wrapper">Horlicksum Vaanga Poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Horlicksum Vaanga Poren"/>
</div>
<div class="lyrico-lyrics-wrapper">Avalai Naan Thaangaporen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avalai Naan Thaangaporen"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuzhandhaipola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuzhandhaipola"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kazhuthukku Kazhuthukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kazhuthukku Kazhuthukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Mani Vaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mani Vaanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Iduppukku Arunaakkodi Vaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iduppukku Arunaakkodi Vaanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Appuram Yedho Sonnaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appuram Yedho Sonnaanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ha Ha Ha Idhuthaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ha Ha Ha Idhuthaanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oho Ithuvum Bhanuvukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oho Ithuvum Bhanuvukka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ha Ha Ila Paappaavukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ha Ha Ila Paappaavukka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pa Pa Pa Pappappapa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pa Pa Pa Pappappapa"/>
</div>
<div class="lyrico-lyrics-wrapper">Varudhe Enakku Papa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varudhe Enakku Papa"/>
</div>
<div class="lyrico-lyrics-wrapper">Appappapa Appa Appa Appa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appappapa Appa Appa Appa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhusaaga Bommai Ketpa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhusaaga Bommai Ketpa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inime En
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inime En"/>
</div>
<div class="lyrico-lyrics-wrapper">Aattamum Kondaattamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aattamum Kondaattamum"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhoolu Dhoolu Thaambaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhoolu Dhoolu Thaambaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ha Ha Ha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ha Ha Ha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen Krishna Paappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Krishna Paappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Appa Maadhiri Irukkanumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appa Maadhiri Irukkanumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Amma Maadhiri Irukkanuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma Maadhiri Irukkanuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Appa Pola Venaame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appa Pola Venaame"/>
</div>
<div class="lyrico-lyrics-wrapper">Amma Pola Venaame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma Pola Venaame"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendum Kalandhu Irukkanume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendum Kalandhu Irukkanume"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhaga Sirikanume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhaga Sirikanume"/>
</div>
<div class="lyrico-lyrics-wrapper">Ha Ha Ha Ha Ha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ha Ha Ha Ha Ha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ha Ha Ha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ha Ha Ha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithaan Andha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithaan Andha"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagaana Sirippa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaana Sirippa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mmm Kuzhandhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mmm Kuzhandhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pirakka Podhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirakka Podhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Engalukkellam Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engalukkellam Enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaangitharuva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaangitharuva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chocolate-u Vaangitharuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chocolate-u Vaangitharuven"/>
</div>
<div class="lyrico-lyrics-wrapper">Oottikke Ootti Viduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oottikke Ootti Viduven"/>
</div>
<div class="lyrico-lyrics-wrapper">Verenna Vaangitharuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verenna Vaangitharuven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Apram Soldren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Apram Soldren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sari Kuzhandhaioda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sari Kuzhandhaioda"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Pannuva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Pannuva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vilaiyaada Kootti Varuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilaiyaada Kootti Varuven"/>
</div>
<div class="lyrico-lyrics-wrapper">Pazham Vittu Doovum Viduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pazham Vittu Doovum Viduven"/>
</div>
<div class="lyrico-lyrics-wrapper">Apram Naan Enna Seiven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Apram Naan Enna Seiven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Seiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Seiva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bhanuva Kettu Soldren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhanuva Kettu Soldren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aamaa Inime
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aamaa Inime"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhanuvoda Vilaiyaadamaattiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhanuvoda Vilaiyaadamaattiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paththumaasam Porukkanumaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paththumaasam Porukkanumaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Paappa Nallaa Valaranumaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paappa Nallaa Valaranumaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhuvarai Summa Irukkanumaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhuvarai Summa Irukkanumaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Doctor Sonnaan Gaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Doctor Sonnaan Gaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhu Dhu Dhudhu Dhudhu Dha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhu Dhu Dhudhu Dhudhu Dha"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhi Dha Dha Dhu Dha Thaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhi Dha Dha Dhu Dha Thaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhi Dhi Dhidhi Dhu Dhi Thaa Thi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhi Dhi Dhidhi Dhu Dhi Thaa Thi"/>
</div>
<div class="lyrico-lyrics-wrapper">Dha Dha Dha Dha Tha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dha Dha Dha Dha Tha"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhi Dhi Dhi Aatamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhi Dhi Dhi Aatamum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kondattamum Dhu Dhu Dhu Dha Dha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondattamum Dhu Dhu Dhu Dha Dha"/>
</div>
<div class="lyrico-lyrics-wrapper">Haa Ha Ha Ha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa Ha Ha Ha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pa Pa Pa Pappappapa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pa Pa Pa Pappappapa"/>
</div>
<div class="lyrico-lyrics-wrapper">Varudhe Enakku Papa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varudhe Enakku Papa"/>
</div>
<div class="lyrico-lyrics-wrapper">Appappapa Appa Appa Appa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appappapa Appa Appa Appa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhusaaga Bommai Ketpa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhusaaga Bommai Ketpa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inime En
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inime En"/>
</div>
<div class="lyrico-lyrics-wrapper">Aattamum Kondaattamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aattamum Kondaattamum"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhoolu Dhoolu Thaambaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhoolu Dhoolu Thaambaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ha Ha Ha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ha Ha Ha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pa Pa Pa Pappappapa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pa Pa Pa Pappappapa"/>
</div>
<div class="lyrico-lyrics-wrapper">Varudhe Enakku Papa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varudhe Enakku Papa"/>
</div>
<div class="lyrico-lyrics-wrapper">Appappapa Appa Appa Appa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appappapa Appa Appa Appa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhusaaga Bommai Ketpa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhusaaga Bommai Ketpa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inime En
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inime En"/>
</div>
<div class="lyrico-lyrics-wrapper">Aattamum Kondaattamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aattamum Kondaattamum"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhoolu Dhoolu Thaambaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhoolu Dhoolu Thaambaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Krishna Ivlo Panam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Krishna Ivlo Panam"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Vaanga Pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Vaanga Pora"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannukku Kannukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannukku Kannukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Mai Vaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mai Vaanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalukku Kolusu Thaan Vaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalukku Kolusu Thaan Vaanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ah Appuram Yedho Sonnaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ah Appuram Yedho Sonnaanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Aiyaiyo Marandhenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyaiyo Marandhenga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hana Kai Sattai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hana Kai Sattai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanga Poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanga Poren"/>
</div>
<div class="lyrico-lyrics-wrapper">Gavunundhaan Vaanga Poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gavunundhaan Vaanga Poren"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadaitheruva Thediporen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadaitheruva Thediporen"/>
</div>
<div class="lyrico-lyrics-wrapper">Gudhurai Mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gudhurai Mela"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ellam Paappaakuthaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellam Paappaakuthaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhanukku Onnum Ilaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhanukku Onnum Ilaiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mmm Irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mmm Irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Apple Naan Vaanga Poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Apple Naan Vaanga Poren"/>
</div>
<div class="lyrico-lyrics-wrapper">Horlicksum Vaanga Poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Horlicksum Vaanga Poren"/>
</div>
<div class="lyrico-lyrics-wrapper">Avalai Naan Thaangaporen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avalai Naan Thaangaporen"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuzhandhaipola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuzhandhaipola"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kazhuthukku Kazhuthukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kazhuthukku Kazhuthukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Mani Vaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mani Vaanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Iduppukku Arunaakkodi Vaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iduppukku Arunaakkodi Vaanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Appuram Yedho Sonnaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appuram Yedho Sonnaanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ha Ha Ha Idhuthaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ha Ha Ha Idhuthaanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oho Ithuvum Bhanuvukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oho Ithuvum Bhanuvukka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ha Ha Ila Paappaavukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ha Ha Ila Paappaavukka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pa Pa Pa Pappappapa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pa Pa Pa Pappappapa"/>
</div>
<div class="lyrico-lyrics-wrapper">Varudhe Enakku Papa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varudhe Enakku Papa"/>
</div>
<div class="lyrico-lyrics-wrapper">Appappapa Appa Appa Appa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appappapa Appa Appa Appa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhusaaga Bommai Ketpa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhusaaga Bommai Ketpa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inime En
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inime En"/>
</div>
<div class="lyrico-lyrics-wrapper">Aattamum Kondaattamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aattamum Kondaattamum"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhoolu Dhoolu Thaambaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhoolu Dhoolu Thaambaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ha Ha Ha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ha Ha Ha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen Krishna Paappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Krishna Paappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Appa Maadhiri Irukkanumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appa Maadhiri Irukkanumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Amma Maadhiri Irukkanuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma Maadhiri Irukkanuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Appa Pola Venaame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appa Pola Venaame"/>
</div>
<div class="lyrico-lyrics-wrapper">Amma Pola Venaame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma Pola Venaame"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendum Kalandhu Irukkanume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendum Kalandhu Irukkanume"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhaga Sirikanume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhaga Sirikanume"/>
</div>
<div class="lyrico-lyrics-wrapper">Ha Ha Ha Ha Ha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ha Ha Ha Ha Ha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ha Ha Ha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ha Ha Ha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithaan Andha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithaan Andha"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagaana Sirippa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaana Sirippa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mmm Kuzhandhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mmm Kuzhandhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pirakka Podhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirakka Podhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Engalukkellam Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engalukkellam Enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaangitharuva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaangitharuva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chocolate Tu Vaangitharuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chocolate Tu Vaangitharuven"/>
</div>
<div class="lyrico-lyrics-wrapper">Oottikke Ootti Viduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oottikke Ootti Viduven"/>
</div>
<div class="lyrico-lyrics-wrapper">Verenna Vaangitharuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verenna Vaangitharuven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Apram Soldren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Apram Soldren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sari Kuzhandhaioda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sari Kuzhandhaioda"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Pannuva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Pannuva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vilaiyaada Kootti Varuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilaiyaada Kootti Varuven"/>
</div>
<div class="lyrico-lyrics-wrapper">Pazham Vittu Doovum Viduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pazham Vittu Doovum Viduven"/>
</div>
<div class="lyrico-lyrics-wrapper">Apram Naan Enna Seiven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Apram Naan Enna Seiven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Seiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Seiva"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhanuva Kettu Soldren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhanuva Kettu Soldren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aamaa Inime
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aamaa Inime"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhanuvoda Vilaiyaadamaattiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhanuvoda Vilaiyaadamaattiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paththumaasam Porukkanumaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paththumaasam Porukkanumaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Paappa Nallaa Valaranumaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paappa Nallaa Valaranumaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhuvarai Summa Irukkanumaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhuvarai Summa Irukkanumaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Doctor Sonnaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Doctor Sonnaanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhu Dhu Dhudhu Dhudhu Dha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhu Dhu Dhudhu Dhudhu Dha"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhi Dha Dha Dhu Dha Thaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhi Dha Dha Dhu Dha Thaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhi Dhi Dhidhi Dhu Dhi Thaa Thi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhi Dhi Dhidhi Dhu Dhi Thaa Thi"/>
</div>
<div class="lyrico-lyrics-wrapper">Dha Dha Dha Dha Tha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dha Dha Dha Dha Tha"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhi Dhi Dhi Aatamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhi Dhi Dhi Aatamum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kondattamum Dhu Dhu Dhu Dha Dha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondattamum Dhu Dhu Dhu Dha Dha"/>
</div>
<div class="lyrico-lyrics-wrapper">Haa Ha Ha Ha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa Ha Ha Ha"/>
</div>
</pre>
