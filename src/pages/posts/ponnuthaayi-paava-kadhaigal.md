---
title: "ponnuthaayi song lyrics"
album: "Paava Kadhaigal"
artist: "Karthik"
lyricist: "Madhan Karky"
director: "Sudha Kongara - Vignesh Shivan - Gautham Vasudev Menon - Vetrimaaran"
path: "/albums/paava-kadhaigal-lyrics"
song: "Ponnuthaayi"
image: ../../images/albumart/paava-kadhaigal.jpg
date: 2020-12-18
lang: tamil
youtubeLink: "https://www.youtube.com/embed/0BZ0Ordb_9E"
type: "happy"
singers:
  - Karthik
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">ponnuthaayi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponnuthaayi "/>
</div>
<div class="lyrico-lyrics-wrapper">en pozhudhu vidiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en pozhudhu vidiya"/>
</div>
<div class="lyrico-lyrics-wrapper">un kanna thoradi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kanna thoradi"/>
</div>
<div class="lyrico-lyrics-wrapper">ponnuthaayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponnuthaayi"/>
</div>
<div class="lyrico-lyrics-wrapper">ponnuthaayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponnuthaayi"/>
</div>
<div class="lyrico-lyrics-wrapper">en usuru kosuru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en usuru kosuru"/>
</div>
<div class="lyrico-lyrics-wrapper">unkitta kedakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unkitta kedakku"/>
</div>
<div class="lyrico-lyrics-wrapper">ponnuthaayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponnuthaayi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vaya mael veesum kaathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaya mael veesum kaathaa"/>
</div>
<div class="lyrico-lyrics-wrapper">kulungi pesum naathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulungi pesum naathaa"/>
</div>
<div class="lyrico-lyrics-wrapper">pudusa pookum oothaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pudusa pookum oothaa"/>
</div>
<div class="lyrico-lyrics-wrapper">nee pesadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee pesadi"/>
</div>
<div class="lyrico-lyrics-wrapper">ponnuthaayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponnuthaayi"/>
</div>
<div class="lyrico-lyrics-wrapper">un sirippu kanakka ulagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un sirippu kanakka ulagum"/>
</div>
<div class="lyrico-lyrics-wrapper">virichu minnu thaayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="virichu minnu thaayi"/>
</div>
<div class="lyrico-lyrics-wrapper">en ponnuthaayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en ponnuthaayi"/>
</div>
<div class="lyrico-lyrics-wrapper">un kolusu maniyil manasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kolusu maniyil manasa"/>
</div>
<div class="lyrico-lyrics-wrapper">alasu ponnuthaayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alasu ponnuthaayi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vaa thaayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaa thaayi"/>
</div>
<div class="lyrico-lyrics-wrapper">megam onnu paethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="megam onnu paethu"/>
</div>
<div class="lyrico-lyrics-wrapper">rekka rendu koathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rekka rendu koathu"/>
</div>
<div class="lyrico-lyrics-wrapper">mela veesavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mela veesavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">ohh oh oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ohh oh oh"/>
</div>
<div class="lyrico-lyrics-wrapper">thaayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaayi"/>
</div>
<div class="lyrico-lyrics-wrapper">saami etti paathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saami etti paathu"/>
</div>
<div class="lyrico-lyrics-wrapper">sowkiyamaa kaettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sowkiyamaa kaettu"/>
</div>
<div class="lyrico-lyrics-wrapper">keezha oadi vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="keezha oadi vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">udhirthu vizhundha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="udhirthu vizhundha "/>
</div>
<div class="lyrico-lyrics-wrapper">malara podhachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malara podhachu"/>
</div>
<div class="lyrico-lyrics-wrapper">adhukkum azhuvura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adhukkum azhuvura"/>
</div>
<div class="lyrico-lyrics-wrapper">nee neliyum puzhuva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee neliyum puzhuva"/>
</div>
<div class="lyrico-lyrics-wrapper">viralil eduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viralil eduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">oravu pazhagura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oravu pazhagura"/>
</div>
<div class="lyrico-lyrics-wrapper">kuruvi kunjuga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuruvi kunjuga"/>
</div>
<div class="lyrico-lyrics-wrapper">pattini kedakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pattini kedakka"/>
</div>
<div class="lyrico-lyrics-wrapper">idili kodukkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idili kodukkura"/>
</div>
<div class="lyrico-lyrics-wrapper">oru erumbu varisa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru erumbu varisa"/>
</div>
<div class="lyrico-lyrics-wrapper">mudinja porave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudinja porave"/>
</div>
<div class="lyrico-lyrics-wrapper">kadandhu nadakkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadandhu nadakkura"/>
</div>
<div class="lyrico-lyrics-wrapper">nee paasathula thaavi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee paasathula thaavi"/>
</div>
<div class="lyrico-lyrics-wrapper">en meesa mudi neevi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en meesa mudi neevi"/>
</div>
<div class="lyrico-lyrics-wrapper">en nethiyila mutham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en nethiyila mutham"/>
</div>
<div class="lyrico-lyrics-wrapper">onnu veiyi thaayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="onnu veiyi thaayi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">azhagu ponnuthaayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="azhagu ponnuthaayi"/>
</div>
<div class="lyrico-lyrics-wrapper">en pozhuthu vidiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en pozhuthu vidiya"/>
</div>
<div class="lyrico-lyrics-wrapper">un kanna thoradi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kanna thoradi"/>
</div>
<div class="lyrico-lyrics-wrapper">ponnuthaayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponnuthaayi"/>
</div>
<div class="lyrico-lyrics-wrapper">ponnuthaayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponnuthaayi"/>
</div>
<div class="lyrico-lyrics-wrapper">en usura kosuru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en usura kosuru"/>
</div>
<div class="lyrico-lyrics-wrapper">unkitta kedakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unkitta kedakku"/>
</div>
<div class="lyrico-lyrics-wrapper">ponnuthaayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponnuthaayi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vaya mael veesum kaathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaya mael veesum kaathaa"/>
</div>
<div class="lyrico-lyrics-wrapper">kulungi pesum naathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulungi pesum naathaa"/>
</div>
<div class="lyrico-lyrics-wrapper">pudusa pookum oothaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pudusa pookum oothaa"/>
</div>
<div class="lyrico-lyrics-wrapper">nee pesadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee pesadi"/>
</div>
<div class="lyrico-lyrics-wrapper">ponnuthaayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponnuthaayi"/>
</div>
<div class="lyrico-lyrics-wrapper">un sirippu kanakka ulagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un sirippu kanakka ulagum"/>
</div>
<div class="lyrico-lyrics-wrapper">virichu minnu thaayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="virichu minnu thaayi"/>
</div>
<div class="lyrico-lyrics-wrapper">en ponnuthaayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en ponnuthaayi"/>
</div>
<div class="lyrico-lyrics-wrapper">un kolusu maniyil manasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kolusu maniyil manasa"/>
</div>
<div class="lyrico-lyrics-wrapper">alasu ponnuthaayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alasu ponnuthaayi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vaa thaayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaa thaayi"/>
</div>
<div class="lyrico-lyrics-wrapper">megam onnu paethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="megam onnu paethu"/>
</div>
<div class="lyrico-lyrics-wrapper">rekka rendu koathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rekka rendu koathu"/>
</div>
<div class="lyrico-lyrics-wrapper">mela veesavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mela veesavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">ohh oh oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ohh oh oh"/>
</div>
<div class="lyrico-lyrics-wrapper">thaayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaayi"/>
</div>
<div class="lyrico-lyrics-wrapper">saami etti paathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saami etti paathu"/>
</div>
<div class="lyrico-lyrics-wrapper">sowkiyamaa kaettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sowkiyamaa kaettu"/>
</div>
<div class="lyrico-lyrics-wrapper">keezha oadi vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="keezha oadi vaa"/>
</div>
</pre>
