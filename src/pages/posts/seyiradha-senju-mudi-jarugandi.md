---
title: "seyiradha senju mudi song lyrics"
album: "Jarugandi"
artist: "Bobo Shashi"
lyricist: "Gangai Amaren"
director: "AN Pitchumani"
path: "/albums/jarugandi-lyrics"
song: "Seyiradha Senju Mudi"
image: ../../images/albumart/jarugandi.jpg
date: 2018-10-26
lang: tamil
youtubeLink: "https://www.youtube.com/embed/zKR0bP5PlMc"
type: "happy"
singers:
  - Jai
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Seyiradha senju mudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seyiradha senju mudi"/>
</div>
<div class="lyrico-lyrics-wrapper">Time appadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Time appadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu kaiyil vanthu sernthathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu kaiyil vanthu sernthathum"/>
</div>
<div class="lyrico-lyrics-wrapper">Irukki pudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irukki pudi"/>
</div>
<div class="lyrico-lyrics-wrapper">Poiya kooda solli padi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poiya kooda solli padi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuralai padi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuralai padi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee pora vazhi nalla vazhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee pora vazhi nalla vazhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhuthu appadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhuthu appadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yerura vazhi ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yerura vazhi ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeri varanum varum padi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeri varanum varum padi"/>
</div>
<div class="lyrico-lyrics-wrapper">Yetham irukkuthuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yetham irukkuthuda"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethukku unakku kozhappadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethukku unakku kozhappadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu thaan nadakkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu thaan nadakkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhuthi mudicha kadhai padi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhuthi mudicha kadhai padi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ooru unakku pinnadi thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooru unakku pinnadi thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagam pugazhum munnadi thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagam pugazhum munnadi thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Peru vilangum eppothum thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peru vilangum eppothum thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Perumai niraiyathan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perumai niraiyathan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeri varnum selvaakku thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeri varnum selvaakku thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathil vizhanum nalvaakku thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathil vizhanum nalvaakku thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Car-u vasathi munnetram thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Car-u vasathi munnetram thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanakkil varavu thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanakkil varavu thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Porathu pazhasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porathu pazhasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaarathu puthusu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaarathu puthusu"/>
</div>
<div class="lyrico-lyrics-wrapper">Annaikku sirusu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annaikku sirusu"/>
</div>
<div class="lyrico-lyrics-wrapper">Innaikku perusu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innaikku perusu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kettathukkulla thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kettathukkulla thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nallathum irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nallathum irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Nallathukkullayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nallathukkullayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kettathum kedakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kettathum kedakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaarathellam nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaarathellam nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Vittu vidatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vittu vidatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhkai atha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhkai atha"/>
</div>
<div class="lyrico-lyrics-wrapper">Veenakki vidatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veenakki vidatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">First-u route-eh podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="First-u route-eh podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Next-u road-ta podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Next-u road-ta podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Doubt-ta thookki pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Doubt-ta thookki pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Podu sakkapodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podu sakkapodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varaathu varavuthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varaathu varavuthan"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppothumae nikkathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppothumae nikkathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Varumpadi kanakkula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varumpadi kanakkula"/>
</div>
<div class="lyrico-lyrics-wrapper">Vatti muthalu thappathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vatti muthalu thappathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma luck-ku thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma luck-ku thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Erangi engayum odathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Erangi engayum odathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ooru unakku pinnadithan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooru unakku pinnadithan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagam pugazhum munnadithan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagam pugazhum munnadithan"/>
</div>
<div class="lyrico-lyrics-wrapper">Peru vilangum eppothumthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peru vilangum eppothumthan"/>
</div>
<div class="lyrico-lyrics-wrapper">Perumai niraiyathan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perumai niraiyathan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeri varnum selvaakkuthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeri varnum selvaakkuthan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathil vizhanum nalvaakkuthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathil vizhanum nalvaakkuthan"/>
</div>
<div class="lyrico-lyrics-wrapper">Car-u vasathi munnetramthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Car-u vasathi munnetramthan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanakkil varavuthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanakkil varavuthan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi dhool
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi dhool"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ullathu varaikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullathu varaikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorellaam koodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorellaam koodum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kitta nice-ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kitta nice-ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Sangaathi paadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sangaathi paadum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vanthathu varaikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthathu varaikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vechukka nekka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vechukka nekka"/>
</div>
<div class="lyrico-lyrics-wrapper">Nerathai paathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nerathai paathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhnthukka shokka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhnthukka shokka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Namma vazhkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma vazhkai"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu namma kaiyula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu namma kaiyula"/>
</div>
<div class="lyrico-lyrics-wrapper">Nambungada verai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nambungada verai"/>
</div>
<div class="lyrico-lyrics-wrapper">Engayum illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engayum illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thappa nee sonnalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappa nee sonnalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalaiya aatum koottam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaiya aatum koottam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuttethum illenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuttethum illenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thookki keela podum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thookki keela podum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uzhaicha rikshawkaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uzhaicha rikshawkaran"/>
</div>
<div class="lyrico-lyrics-wrapper">Kooda rich-ah maaralaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooda rich-ah maaralaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Edutha eduppulayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edutha eduppulayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aeroplane-u yeralaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aeroplane-u yeralaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Over night-layum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Over night-layum"/>
</div>
<div class="lyrico-lyrics-wrapper">Obamavum aavalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Obamavum aavalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Seyiradha senju mudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seyiradha senju mudi"/>
</div>
<div class="lyrico-lyrics-wrapper">Time appadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Time appadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu kaiyil vanthu sernthathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu kaiyil vanthu sernthathum"/>
</div>
<div class="lyrico-lyrics-wrapper">Irukki pudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irukki pudi"/>
</div>
<div class="lyrico-lyrics-wrapper">irukkipudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irukkipudi"/>
</div>
<div class="lyrico-lyrics-wrapper">Poiya kooda solli padi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poiya kooda solli padi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuralai padi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuralai padi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee pora vazhi nalla vazhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee pora vazhi nalla vazhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhuthu appadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhuthu appadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ooru unakku pinnadi thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooru unakku pinnadi thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagam pugazhum munnadi thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagam pugazhum munnadi thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Peru vilangum eppothum thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peru vilangum eppothum thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Perumai niraiyathan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perumai niraiyathan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeri varnum selvaakku thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeri varnum selvaakku thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathil vizhanum nalvaakku thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathil vizhanum nalvaakku thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Car-u vasathi munnetram thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Car-u vasathi munnetram thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanakkil varavu thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanakkil varavu thaan"/>
</div>
</pre>
