---
title: "tholi tholi song lyrics"
album: "Amaram Akhilam Prema"
artist: "Radhan"
lyricist: "Rehman"
director: "Jonathan Edwards"
path: "/albums/amaram-akhilam-prema-lyrics"
song: "Tholi Tholi"
image: ../../images/albumart/amaram-akhilam-prema.jpg
date: 2020-09-18
lang: telugu
youtubeLink: "https://www.youtube.com/embed/7JaHgn85TL8"
type: "love"
singers:
  - Anurag Kulkarni
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kadhilaane neevaipe neevalle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhilaane neevaipe neevalle"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanu nene vadhilesi neekai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanu nene vadhilesi neekai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanugonna tholisaari ivvaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanugonna tholisaari ivvaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalaganna naa theeram nuvve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalaganna naa theeram nuvve"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sandheham ledhule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandheham ledhule"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee samayam kalisochhindhile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee samayam kalisochhindhile"/>
</div>
<div class="lyrico-lyrics-wrapper">Santhosham rangule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Santhosham rangule"/>
</div>
<div class="lyrico-lyrics-wrapper">Parichesindhi prathi dhaarile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parichesindhi prathi dhaarile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sandheham ledhule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandheham ledhule"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee samayam kalisochhindhile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee samayam kalisochhindhile"/>
</div>
<div class="lyrico-lyrics-wrapper">Santhosham rangule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Santhosham rangule"/>
</div>
<div class="lyrico-lyrics-wrapper">Parichesindhi prathi dhaarile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parichesindhi prathi dhaarile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tholi tholi tholi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholi tholi tholi"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholakari chinukalle ne jaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholakari chinukalle ne jaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Vadhalanu ika nimishamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadhalanu ika nimishamu"/>
</div>
<div class="lyrico-lyrics-wrapper">Varadhalle nee dhaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varadhalle nee dhaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhuruga nilavani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhuruga nilavani"/>
</div>
<div class="lyrico-lyrics-wrapper">Oka merupalle nee maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oka merupalle nee maari"/>
</div>
<div class="lyrico-lyrics-wrapper">Adugaduguna velugai vasthaale dheveri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adugaduguna velugai vasthaale dheveri"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thelusaa neevalane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thelusaa neevalane"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa varase maaripoye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa varase maaripoye"/>
</div>
<div class="lyrico-lyrics-wrapper">Manase nee vasamai chejaaripoye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manase nee vasamai chejaaripoye"/>
</div>
<div class="lyrico-lyrics-wrapper">Yepudu vinani sarikotha vinthagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yepudu vinani sarikotha vinthagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa gundelo nee vegame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa gundelo nee vegame"/>
</div>
<div class="lyrico-lyrics-wrapper">Cherindhi cheppakunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cherindhi cheppakunda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa prathi swaashanu thadime
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa prathi swaashanu thadime"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee gaali gandhame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee gaali gandhame"/>
</div>
<div class="lyrico-lyrics-wrapper">Ikapai nanu brathikincheti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ikapai nanu brathikincheti"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo praanabhandhame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo praanabhandhame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sandheham ledhule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandheham ledhule"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee samayam kalisochindhile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee samayam kalisochindhile"/>
</div>
<div class="lyrico-lyrics-wrapper">Santhosham rangule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Santhosham rangule"/>
</div>
<div class="lyrico-lyrics-wrapper">Parichesindhi prathi dhaarile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parichesindhi prathi dhaarile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tholi tholi tholi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholi tholi tholi"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholakari chinukalle ne jaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholakari chinukalle ne jaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Vadhalanu ika nimishamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadhalanu ika nimishamu"/>
</div>
<div class="lyrico-lyrics-wrapper">Varadhalle nee dhaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varadhalle nee dhaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhuruga nilavani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhuruga nilavani"/>
</div>
<div class="lyrico-lyrics-wrapper">Oka merupalle ne maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oka merupalle ne maari"/>
</div>
<div class="lyrico-lyrics-wrapper">Adugaduguna velugai vasthaale dheveri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adugaduguna velugai vasthaale dheveri"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Udhayam ee hrudhayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udhayam ee hrudhayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee korake melukundhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee korake melukundhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanulu ee kalalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanulu ee kalalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee sonthamele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee sonthamele"/>
</div>
<div class="lyrico-lyrics-wrapper">Avunaa nijamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avunaa nijamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Anipisthu vunnadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anipisthu vunnadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanandhame aakaashamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanandhame aakaashamai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naapaina vaaluthunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naapaina vaaluthunte"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ee prathi kshanamoka varamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee prathi kshanamoka varamai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadipindhi kaalame bahushaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadipindhi kaalame bahushaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu kalapadame thana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu kalapadame thana"/>
</div>
<div class="lyrico-lyrics-wrapper">Mundhunna dhyeyame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mundhunna dhyeyame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sandheham ledhule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandheham ledhule"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee samayam kalisochindhile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee samayam kalisochindhile"/>
</div>
<div class="lyrico-lyrics-wrapper">Santhosham rangule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Santhosham rangule"/>
</div>
<div class="lyrico-lyrics-wrapper">Parichesindhi prathi dhaarile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parichesindhi prathi dhaarile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tholi tholi tholi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholi tholi tholi"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholakari chinukalle ne jaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholakari chinukalle ne jaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Vadhalanu ika nimishamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadhalanu ika nimishamu"/>
</div>
<div class="lyrico-lyrics-wrapper">Varadhalle nee dhaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varadhalle nee dhaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhuruga nilavani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhuruga nilavani"/>
</div>
<div class="lyrico-lyrics-wrapper">Oka merupalle ne maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oka merupalle ne maari"/>
</div>
<div class="lyrico-lyrics-wrapper">Adugaduguna velugai vasthale dheveri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adugaduguna velugai vasthale dheveri"/>
</div>
</pre>