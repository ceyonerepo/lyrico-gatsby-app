---
title: "kilimanjaro song lyrics"
album: "Enthiran"
artist: "A.R. Rahman"
lyricist: "Pa Vijay"
director: "S. Shankar"
path: "/albums/enthiran-lyrics"
song: "Kilimanjaro"
image: ../../images/albumart/enthiran.jpg
date: 2010-07-31
lang: tamil
youtubeLink: "https://www.youtube.com/embed/E21CJi7R9JM"
type: "Love"
singers:
  - Javed Ali
  - Chinmayi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kilimanjaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kilimanjaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Malai Kani Manjaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malai Kani Manjaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanna Kuzhi Manjaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna Kuzhi Manjaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaro Yaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaro Yaro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Moganjadharo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moganjadharo"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnil Nozhanjadharo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnil Nozhanjadharo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiya Kozhanjatharo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiya Kozhanjatharo"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaro Yaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaro Yaro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaattuvaasi Kaattuvaasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattuvaasi Kaattuvaasi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pachaiyaaga Kadiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pachaiyaaga Kadiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Muththathaale Vega Vechchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muththathaale Vega Vechchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Singapallil Uriyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Singapallil Uriyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mala Paambu Pola Vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mala Paambu Pola Vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maan Kuttiya Pudiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maan Kuttiya Pudiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sukku Milagu Thatti Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sukku Milagu Thatti Enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Souppu Vechchu Kudiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Souppu Vechchu Kudiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Evaalukku Thangachiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evaalukku Thangachiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Engudathaan Irukkaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engudathaan Irukkaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaluyara Olive Pazham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaluyara Olive Pazham"/>
</div>
<div class="lyrico-lyrics-wrapper">Appadiye Enakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appadiye Enakka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Akkakko Adi Kinni Kozhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Akkakko Adi Kinni Kozhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Appappo Enna Pinnikkodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appappo Enna Pinnikkodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippappo Muththam Ennikodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippappo Muththam Ennikodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippappp Ennikko Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippappp Ennikko Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kilimanjaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kilimanjaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Malai Kani Manjaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malai Kani Manjaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanna Kuzhi Manjaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna Kuzhi Manjaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaro Yaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaro Yaro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Moganjadharo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moganjadharo"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnil Nozhanjadharo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnil Nozhanjadharo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiya Kozhanjatharo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiya Kozhanjatharo"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaro Yaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaro Yaro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kodi Pachaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodi Pachaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Elumichaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Elumichaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Unmel Unmel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmel Unmel"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir Ichchaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Ichchaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Nooru Kodi Thasai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Nooru Kodi Thasai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ovvondrilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ovvondrilum"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan Pere Isai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan Pere Isai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inisakkere Adichakare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inisakkere Adichakare"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasa Renda Madichikara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasa Renda Madichikara"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Oora Vaitha Kani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Oora Vaitha Kani"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Mella Aara Vaithu Kadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Mella Aara Vaithu Kadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vervai Nuzhaiyum Veyilum Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vervai Nuzhaiyum Veyilum Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Ilathirai Yen Ittaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Ilathirai Yen Ittaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Udhattaiyum Udhattaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udhattaiyum Udhattaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Poottikkondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poottikkondu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Yugam Mudithu Thira Enbaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Yugam Mudithu Thira Enbaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Akkakko Adi Kinni Kozhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Akkakko Adi Kinni Kozhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Appappo Enna Pinnikkodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appappo Enna Pinnikkodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippappo Muththam Ennikodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippappo Muththam Ennikodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippappp Ennikko Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippappp Ennikko Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kilimanjaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kilimanjaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Malai Kani Manjaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malai Kani Manjaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanna Kuzhi Manjaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna Kuzhi Manjaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaro Yaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaro Yaro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Moganjadharo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moganjadharo"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnil Nozhanjadharo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnil Nozhanjadharo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiya Kozhanjatharo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiya Kozhanjatharo"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaro Yaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaro Yaro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sunaivaasiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sunaivaasiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Sugavaasiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sugavaasiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Thol Karuvi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thol Karuvi"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai Vaasiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai Vaasiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Tholkuthadha Pala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Tholkuthadha Pala"/>
</div>
<div class="lyrico-lyrics-wrapper">Rekkai Katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rekkai Katti"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaal Kondaadum Nilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaal Kondaadum Nilaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mara Dhegam Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mara Dhegam Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Marangothi Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marangothi Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaana Desam Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaana Desam Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Athil Vaasam Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athil Vaasam Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Nooru Gram thaan Idai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Nooru Gram thaan Idai"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakku Ini Yaru Naanthaan Udai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakku Ini Yaru Naanthaan Udai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ainthadi Valarntha Aattu Chedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ainthadi Valarntha Aattu Chedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Meinthuvidu Moththam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Meinthuvidu Moththam"/>
</div>
<div class="lyrico-lyrics-wrapper">Paachai Pasumpul Neeyaanaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paachai Pasumpul Neeyaanaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Puli Pul Thinnume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puli Pul Thinnume"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Kuththam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Kuththam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Akkakko Adi Kinni Kozhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Akkakko Adi Kinni Kozhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Appappo Enna Pinnikkodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appappo Enna Pinnikkodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippappo Muththam Ennikodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippappo Muththam Ennikodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippappp Ennikko Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippappp Ennikko Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Akkakko Adi Kinni Kozhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Akkakko Adi Kinni Kozhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Appappo Enna Pinnikkodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appappo Enna Pinnikkodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippappo Muththam Ennikodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippappo Muththam Ennikodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippappp Ennikko Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippappp Ennikko Nee"/>
</div>
</pre>
