---
title: "the panchakattu song lyrics"
album: "Ante Sundaraniki"
artist: "Vivek Sagar"
lyricist: "Hasith Goli"
director: "Vivek Athreya"
path: "/albums/ante-sundaraniki-lyrics"
song: "The Panchakattu"
image: ../../images/albumart/ante-sundaraniki.jpg
date: 2022-06-10
lang: telugu
youtubeLink: "https://www.youtube.com/embed/mu4ejLfhXHI"
type: "love"
singers:
  -	Aruna Sairam
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Saaroru fade ayipoye freedom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saaroru fade ayipoye freedom"/>
</div>
<div class="lyrico-lyrics-wrapper">Meedinkaa ehe meedinkaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meedinkaa ehe meedinkaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Saaroru dupe e lekundaa freedom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saaroru dupe e lekundaa freedom"/>
</div>
<div class="lyrico-lyrics-wrapper">Fightinkaa meetho meekinkaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fightinkaa meetho meekinkaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aa phoje biginchi yetu cheraaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa phoje biginchi yetu cheraaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Moje varinchee saaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moje varinchee saaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey saaram guninchi bari daataaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey saaram guninchi bari daataaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Mari moge mugimpo maaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mari moge mugimpo maaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tangamlo dunkaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tangamlo dunkaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhale andamgaa maastaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhale andamgaa maastaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey saradaalaa sarukee meeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey saradaalaa sarukee meeru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sayyantuu dunkaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sayyantuu dunkaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ika sundaru maastaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ika sundaru maastaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey saradaake surakesaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey saradaake surakesaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnadanta maayeleraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnadanta maayeleraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Endukinka beraalee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endukinka beraalee"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnadanta beraalera endukinka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnadanta beraalera endukinka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Telanandaa soku daagi daaganandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Telanandaa soku daagi daaganandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Teeranandaa daahaala ee yedaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teeranandaa daahaala ee yedaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa aaganantu aageti baataloneee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa aaganantu aageti baataloneee"/>
</div>
<div class="lyrico-lyrics-wrapper">Saagamantu pecheene thodayindaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saagamantu pecheene thodayindaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anthe gaaranga veganga duuranga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anthe gaaranga veganga duuranga"/>
</div>
<div class="lyrico-lyrics-wrapper">Maare mee gaadha oo vinthale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maare mee gaadha oo vinthale"/>
</div>
<div class="lyrico-lyrics-wrapper">Anthe leenantha raanantha koorindaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anthe leenantha raanantha koorindaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Tuge ee muga meelaalanee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tuge ee muga meelaalanee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rangamlo dunkaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangamlo dunkaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Andamgaa maastaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andamgaa maastaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Saradaalaa sarukee meeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saradaalaa sarukee meeru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sayyantuu dunkaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sayyantuu dunkaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ika sundaru maastaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ika sundaru maastaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey saradaake surakesaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey saradaake surakesaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Rangam lo dunkar saarooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangam lo dunkar saarooru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ika sundaru mastaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ika sundaru mastaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pani ni ni pani ni ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pani ni ni pani ni ni"/>
</div>
<div class="lyrico-lyrics-wrapper">Pani ni ni pani ni ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pani ni ni pani ni ni"/>
</div>
<div class="lyrico-lyrics-wrapper">Sayyantuu dunkaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sayyantuu dunkaru"/>
</div>
<div class="lyrico-lyrics-wrapper">nisa nisa nisa pani pani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nisa nisa nisa pani pani"/>
</div>
<div class="lyrico-lyrics-wrapper">Pani pani mapa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pani pani mapa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Antham ga mastaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antham ga mastaru"/>
</div>
<div class="lyrico-lyrics-wrapper">mapa nisa mapa nisa mapa nisa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mapa nisa mapa nisa mapa nisa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ni ga saa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ni ga saa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saaroru fade ayipoye freedom meedantaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saaroru fade ayipoye freedom meedantaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Saaroru dupelekundaa fight ye meedantaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saaroru dupelekundaa fight ye meedantaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aa phoje Biginchi yetu cheraaru cheraaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa phoje Biginchi yetu cheraaru cheraaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Moje varinchee saaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moje varinchee saaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey saaram guninchi bari daataaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey saaram guninchi bari daataaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Mari moge mugimpo maaru saaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mari moge mugimpo maaru saaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnadanta maayeleraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnadanta maayeleraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Endukinka beraalee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endukinka beraalee"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnadanta beraalee endukinka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnadanta beraalee endukinka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnadanta maayeleraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnadanta maayeleraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Endukinka beraalee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endukinka beraalee"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnadanta beraalee endukinka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnadanta beraalee endukinka"/>
</div>
</pre>
