---
title: "kya kiya hai tune song lyrics"
album: "Broken But Beautiful Season 3"
artist: "Amaal Mallik"
lyricist: "Rashmi Virag"
director: "Priyanka Ghose"
path: "/albums/broken-but-beautiful-season3-lyrics"
song: "Kya Kiya Hai Tune"
image: ../../images/albumart/broken-but-beautiful-season3.jpg
date: 2021-05-28
lang: hindi
youtubeLink: "https://www.youtube.com/embed/zRA24lGKAXk"
type: "love"
singers:
  - Armaan Malik
  - Palak Muchhal
  - Amaal Mallik
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kholi hai khidki maine
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kholi hai khidki maine"/>
</div>
<div class="lyrico-lyrics-wrapper">Iss naazuk se dil ki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iss naazuk se dil ki"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaage hain dekho phir se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaage hain dekho phir se"/>
</div>
<div class="lyrico-lyrics-wrapper">Khwab hazaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khwab hazaar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Banke lehar tu aayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Banke lehar tu aayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Rooh se yun takrayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rooh se yun takrayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sadiyon se tha is pal ke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sadiyon se tha is pal ke"/>
</div>
<div class="lyrico-lyrics-wrapper">Liye bekaraar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Liye bekaraar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kya kiya hai tune
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kya kiya hai tune"/>
</div>
<div class="lyrico-lyrics-wrapper">Kya kiya hai tune
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kya kiya hai tune"/>
</div>
<div class="lyrico-lyrics-wrapper">Kya kiya hai tune mere yaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kya kiya hai tune mere yaar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jo jeene laga hun phir se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jo jeene laga hun phir se"/>
</div>
<div class="lyrico-lyrics-wrapper">Khone laga hun khud se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khone laga hun khud se"/>
</div>
<div class="lyrico-lyrics-wrapper">Kya kiya hai tune mere yaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kya kiya hai tune mere yaar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Shayad khatam hai ya phir bacha hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shayad khatam hai ya phir bacha hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Dil mein kahi pe tumhara intezar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil mein kahi pe tumhara intezar"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu saamne se aise hai guzra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu saamne se aise hai guzra"/>
</div>
<div class="lyrico-lyrics-wrapper">Hone laga hai humko tumse hi pyaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hone laga hai humko tumse hi pyaar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sauda yeh do dil'on ka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sauda yeh do dil'on ka"/>
</div>
<div class="lyrico-lyrics-wrapper">Lagta hai kuch palon ka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lagta hai kuch palon ka"/>
</div>
<div class="lyrico-lyrics-wrapper">Na hoga yeh humse baar baar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na hoga yeh humse baar baar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kya kiya hai tune
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kya kiya hai tune"/>
</div>
<div class="lyrico-lyrics-wrapper">Kya kiya hai tune
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kya kiya hai tune"/>
</div>
<div class="lyrico-lyrics-wrapper">Kya kiya hai tune mere yaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kya kiya hai tune mere yaar"/>
</div>
</pre>
