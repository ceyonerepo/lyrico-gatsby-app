---
title: "atta sudake song lyrics"
album: "Khiladi"
artist: "Devi Sri Prasad"
lyricist: "Shree Mani"
director: "Ramesh Varma"
path: "/albums/khiladi-lyrics"
song: "Atta Sudake"
image: ../../images/albumart/khiladi.jpg
date: 2022-02-11
lang: telugu
youtubeLink: "https://www.youtube.com/embed/SPoE3UWVDlI"
type: "happy"
singers:
  - Devi Sri Prasad
  - Sameera Bharadwaj
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Atta soodake matthekthaandi eeduke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atta soodake matthekthaandi eeduke"/>
</div>
<div class="lyrico-lyrics-wrapper">Vontlo vedike picchekthandi naadike
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vontlo vedike picchekthandi naadike"/>
</div>
<div class="lyrico-lyrics-wrapper">Neelo speedke oopekthandi mooduke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelo speedke oopekthandi mooduke"/>
</div>
<div class="lyrico-lyrics-wrapper">Sigge side ke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sigge side ke"/>
</div>
<div class="lyrico-lyrics-wrapper">Atakekthandi chhod ke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atakekthandi chhod ke"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana selfie theesi poster vesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana selfie theesi poster vesa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa gunde godake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa gunde godake"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Atta sudake matthekthaandi eeduke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atta sudake matthekthaandi eeduke"/>
</div>
<div class="lyrico-lyrics-wrapper">Vontlo vedike picchekthandi naadike
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vontlo vedike picchekthandi naadike"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">O bottle lona samudram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O bottle lona samudram"/>
</div>
<div class="lyrico-lyrics-wrapper">Oka bodylona inthandham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oka bodylona inthandham"/>
</div>
<div class="lyrico-lyrics-wrapper">Packing cheydam asaadyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Packing cheydam asaadyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa god ki vandhanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa god ki vandhanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Super hero imageu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Super hero imageu"/>
</div>
<div class="lyrico-lyrics-wrapper">Neekiccharo packageu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neekiccharo packageu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee valle naaki courageu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee valle naaki courageu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvve naa indhanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvve naa indhanam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dooram penchake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dooram penchake"/>
</div>
<div class="lyrico-lyrics-wrapper">Mentalkethandhi mind ke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mentalkethandhi mind ke"/>
</div>
<div class="lyrico-lyrics-wrapper">Neetho bond ke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neetho bond ke"/>
</div>
<div class="lyrico-lyrics-wrapper">Tempt ekthandi gunde ke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tempt ekthandi gunde ke"/>
</div>
<div class="lyrico-lyrics-wrapper">Oka DJ mix ye modhalayindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oka DJ mix ye modhalayindhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalo mass gaadike
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalo mass gaadike"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Atta soodake matthekthaandi eeduke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atta soodake matthekthaandi eeduke"/>
</div>
<div class="lyrico-lyrics-wrapper">Vontlo vedike picchekthandi naadike
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vontlo vedike picchekthandi naadike"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nuv switch ye leni currentu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuv switch ye leni currentu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aipoya neeke connectu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aipoya neeke connectu"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeke iccha na remoteu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeke iccha na remoteu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee ishtam aaduko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee ishtam aaduko"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee picture kaamudi cut outu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee picture kaamudi cut outu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee structure love ki layout
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee structure love ki layout"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee range ki thagga contentu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee range ki thagga contentu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa glamour choosuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa glamour choosuko"/>
</div>
<div class="lyrico-lyrics-wrapper">Raave trade ke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raave trade ke"/>
</div>
<div class="lyrico-lyrics-wrapper">O truckudu love load ke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O truckudu love load ke"/>
</div>
<div class="lyrico-lyrics-wrapper">Vastha thoduke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vastha thoduke"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuv ekkadikelthe aadike
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuv ekkadikelthe aadike"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana scene gaani chusthe shocke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana scene gaani chusthe shocke"/>
</div>
<div class="lyrico-lyrics-wrapper">Censor board ke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Censor board ke"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Atta soodake matthekthaandi eeduke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atta soodake matthekthaandi eeduke"/>
</div>
<div class="lyrico-lyrics-wrapper">Vontlo vedike picchekthandi naadike
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vontlo vedike picchekthandi naadike"/>
</div>
</pre>
