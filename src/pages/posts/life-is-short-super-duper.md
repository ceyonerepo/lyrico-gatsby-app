---
title: "life is short song lyrics"
album: "Super Duper"
artist: "Divakara Thiyagarajan"
lyricist: "AK"
director: "Arun Karthik"
path: "/albums/super-duper-lyrics"
song: "Life Is Short"
image: ../../images/albumart/super-duper.jpg
date: 2019-09-20
lang: tamil
youtubeLink: "https://www.youtube.com/embed/AthOeuW1Uf8"
type: "philosophy"
singers:
  - Ranjith Govind
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Darling Monaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Darling Monaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nina Gone-aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nina Gone-aaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Feeling Venaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Feeling Venaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Time For Gaanaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Time For Gaanaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Life Is Short’u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life Is Short’u"/>
</div>
<div class="lyrico-lyrics-wrapper">Death Is The Dot’u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Death Is The Dot’u"/>
</div>
<div class="lyrico-lyrics-wrapper">Singing My Paatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Singing My Paatu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dance Aadu Kettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dance Aadu Kettu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">English-u Daddy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="English-u Daddy"/>
</div>
<div class="lyrico-lyrics-wrapper">Now Dead – u Body
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Now Dead – u Body"/>
</div>
<div class="lyrico-lyrics-wrapper">What To Do Lady
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="What To Do Lady"/>
</div>
<div class="lyrico-lyrics-wrapper">Fate Is A Baddie
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fate Is A Baddie"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Life is Short
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life is Short"/>
</div>
<div class="lyrico-lyrics-wrapper">Go Go Go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Go Go Go"/>
</div>
<div class="lyrico-lyrics-wrapper">You Can’t Say
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Can’t Say"/>
</div>
<div class="lyrico-lyrics-wrapper">No No No
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="No No No"/>
</div>
<div class="lyrico-lyrics-wrapper">Let’s Chill Out
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Let’s Chill Out"/>
</div>
<div class="lyrico-lyrics-wrapper">Always Happy Aaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Always Happy Aaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Setha Get Out’u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Setha Get Out’u"/>
</div>
<div class="lyrico-lyrics-wrapper">Geth ah Cut Out’u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Geth ah Cut Out’u"/>
</div>
<div class="lyrico-lyrics-wrapper">Vecha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vecha"/>
</div>
<div class="lyrico-lyrics-wrapper">Adha Nee Paapiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adha Nee Paapiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Life Says
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life Says"/>
</div>
<div class="lyrico-lyrics-wrapper">Go Go Go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Go Go Go"/>
</div>
<div class="lyrico-lyrics-wrapper">You Can’t Say
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Can’t Say"/>
</div>
<div class="lyrico-lyrics-wrapper">No No No
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="No No No"/>
</div>
<div class="lyrico-lyrics-wrapper">Let’s Chill Out
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Let’s Chill Out"/>
</div>
<div class="lyrico-lyrics-wrapper">Always Happy Aaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Always Happy Aaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Setha Get Out’u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Setha Get Out’u"/>
</div>
<div class="lyrico-lyrics-wrapper">Geth ah Cut Out’u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Geth ah Cut Out’u"/>
</div>
<div class="lyrico-lyrics-wrapper">Vecha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vecha"/>
</div>
<div class="lyrico-lyrics-wrapper">Adha Nee Paapiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adha Nee Paapiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh God’u Iva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh God’u Iva"/>
</div>
<div class="lyrico-lyrics-wrapper">Vera Oru Vera Oru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vera Oru Vera Oru"/>
</div>
<div class="lyrico-lyrics-wrapper">Naadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Onnu Sera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnu Sera"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Plan Irundha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Plan Irundha"/>
</div>
<div class="lyrico-lyrics-wrapper">Melirundhu Podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Melirundhu Podu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">A For Apple Adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="A For Apple Adi"/>
</div>
<div class="lyrico-lyrics-wrapper">B For Bottle Adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="B For Bottle Adi"/>
</div>
<div class="lyrico-lyrics-wrapper">Safe Ah Paathukuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Safe Ah Paathukuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Ok nu Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ok nu Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollu Di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollu Di"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ayyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Raasaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raasaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Poitiyae Poitiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poitiyae Poitiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Raasaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raasaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Poitiyae Poitiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poitiyae Poitiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ayyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Raasaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raasaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Poitiyae Poitiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poitiyae Poitiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Raasaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raasaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Poitiyae Poitiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poitiyae Poitiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ayyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Raasaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raasaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Poitiyae Poitiyae Poitiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poitiyae Poitiyae Poitiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ABCD
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ABCD"/>
</div>
<div class="lyrico-lyrics-wrapper">Paavam Daddy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paavam Daddy"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaitaan Body
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaitaan Body"/>
</div>
<div class="lyrico-lyrics-wrapper">Potare Di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Potare Di"/>
</div>
<div class="lyrico-lyrics-wrapper">ABCD
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ABCD"/>
</div>
<div class="lyrico-lyrics-wrapper">Paavam Daddy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paavam Daddy"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaitaan Body
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaitaan Body"/>
</div>
<div class="lyrico-lyrics-wrapper">Potare Di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Potare Di"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ABCD
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ABCD"/>
</div>
<div class="lyrico-lyrics-wrapper">Paavam Daddy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paavam Daddy"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaitaan Body
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaitaan Body"/>
</div>
<div class="lyrico-lyrics-wrapper">Potare Di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Potare Di"/>
</div>
<div class="lyrico-lyrics-wrapper">ABCD
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ABCD"/>
</div>
<div class="lyrico-lyrics-wrapper">Paavam Daddy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paavam Daddy"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaitaan Body
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaitaan Body"/>
</div>
<div class="lyrico-lyrics-wrapper">Potare Di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Potare Di"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ey Ennadiyae Ennadiyae Mayilu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ey Ennadiyae Ennadiyae Mayilu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhuku Indha Trial’u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhuku Indha Trial’u"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Gandam Vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Gandam Vittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gandam Vandha Kuyilu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gandam Vandha Kuyilu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathukodi En Thamizhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathukodi En Thamizhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Mass Ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Mass Ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Paduvanae Paaduvanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Paduvanae Paaduvanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaanaa..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaanaa.."/>
</div>
<div class="lyrico-lyrics-wrapper">Adha Paada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adha Paada"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Class-u Yedhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Class-u Yedhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Class-u Yedhum Venaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Class-u Yedhum Venaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Serndhae Paadikalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Serndhae Paadikalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Serndhe Vazhunhukalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Serndhe Vazhunhukalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Viduthala Venaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viduthala Venaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vellakaari Nee Aalalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vellakaari Nee Aalalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Set Aachu Deal’u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Set Aachu Deal’u"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennoda Aalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennoda Aalu"/>
</div>
<div class="lyrico-lyrics-wrapper">White – ana Girl’u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="White – ana Girl’u"/>
</div>
<div class="lyrico-lyrics-wrapper">Weight Aana Aalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Weight Aana Aalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Life Is Short’u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life Is Short’u"/>
</div>
<div class="lyrico-lyrics-wrapper">Death Is The Dot’u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Death Is The Dot’u"/>
</div>
<div class="lyrico-lyrics-wrapper">Singing My Paatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Singing My Paatu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dance Aadu Kettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dance Aadu Kettu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">My Dear World’u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="My Dear World’u"/>
</div>
<div class="lyrico-lyrics-wrapper">Western is Old’u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Western is Old’u"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaanavil Paadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaanavil Paadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Step Up The Mood’u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Step Up The Mood’u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam"/>
</div>
<div class="lyrico-lyrics-wrapper">Step Up The Mood’u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Step Up The Mood’u"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Step Up The Mood’u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Step Up The Mood’u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam"/>
</div>
<div class="lyrico-lyrics-wrapper">Step Up The Mood’u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Step Up The Mood’u"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Step Up The Mood’u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Step Up The Mood’u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Step Up The
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Step Up The"/>
</div>
<div class="lyrico-lyrics-wrapper">Step Up The
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Step Up The"/>
</div>
<div class="lyrico-lyrics-wrapper">Step Up The
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Step Up The"/>
</div>
<div class="lyrico-lyrics-wrapper">Step Up The
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Step Up The"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ayyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Raasaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raasaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Poitiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poitiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Raasaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raasaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Poitiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poitiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ayyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Raasaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raasaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Poitiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poitiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Raasaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raasaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Poitiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poitiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ayyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Raasaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raasaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Poitiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poitiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Raasaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raasaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Poitiyae Poitiyae Poitiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poitiyae Poitiyae Poitiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Life Is Short’u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life Is Short’u"/>
</div>
<div class="lyrico-lyrics-wrapper">Death Is The Dot’u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Death Is The Dot’u"/>
</div>
<div class="lyrico-lyrics-wrapper">Singing My Paatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Singing My Paatu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dance Aadu Kettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dance Aadu Kettu"/>
</div>
</pre>
