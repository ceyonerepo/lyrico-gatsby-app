---
title: "ettu kangalukkum song lyrics"
album: "Thoonga Nagaram"
artist: "Sundar C Babu"
lyricist: "S Gnanakaravel"
director: "Gaurav Narayanan"
path: "/albums/thoonga-nagaram-lyrics"
song: "Ettu Kangalukkum"
image: ../../images/albumart/thoonga-nagaram.jpg
date: 2011-02-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/UaDLqA9VXgU"
type: "friendship"
singers:
  - Madhu Balakrishnan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ettu kangalukkum otrai paarvaitharum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettu kangalukkum otrai paarvaitharum "/>
</div>
<div class="lyrico-lyrics-wrapper">natpai thaippen kannoadu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="natpai thaippen kannoadu "/>
</div>
<div class="lyrico-lyrics-wrapper">sattaippaigalukkul natpai segariththu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sattaippaigalukkul natpai segariththu "/>
</div>
<div class="lyrico-lyrics-wrapper">poththikkolvoam nenjoadu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poththikkolvoam nenjoadu "/>
</div>
<div class="lyrico-lyrics-wrapper">naangu idhayathirkum serndhu thudippadharku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naangu idhayathirkum serndhu thudippadharku "/>
</div>
<div class="lyrico-lyrics-wrapper">natpin idhayam thayangaadhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="natpin idhayam thayangaadhu "/>
</div>
<div class="lyrico-lyrics-wrapper">varangal yedhumindri yougangal vaazhvadharku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varangal yedhumindri yougangal vaazhvadharku "/>
</div>
<div class="lyrico-lyrics-wrapper">natpai vittaal uravedhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="natpai vittaal uravedhu "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pirivedhu piri...vedhu.... 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pirivedhu piri...vedhu.... "/>
</div>
<div class="lyrico-lyrics-wrapper">natpukkul pirivedu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="natpukkul pirivedu "/>
</div>
<div class="lyrico-lyrics-wrapper">ninaivennum sol kooda 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninaivennum sol kooda "/>
</div>
<div class="lyrico-lyrics-wrapper">natpukku pidikkaadhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="natpukku pidikkaadhu "/>
</div>
<div class="lyrico-lyrics-wrapper">thoorathil irundhaalum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoorathil irundhaalum "/>
</div>
<div class="lyrico-lyrics-wrapper">nam natpu kuraiyaadhey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nam natpu kuraiyaadhey "/>
</div>
<div class="lyrico-lyrics-wrapper">thookkaththil irundhaalum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thookkaththil irundhaalum "/>
</div>
<div class="lyrico-lyrics-wrapper">natpennam urangaadhey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="natpennam urangaadhey "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ettu kangalukkum otrai paarvai tharum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettu kangalukkum otrai paarvai tharum "/>
</div>
<div class="lyrico-lyrics-wrapper">natpai thaithen kannoadu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="natpai thaithen kannoadu "/>
</div>
<div class="lyrico-lyrics-wrapper">sattaippaigalukkul natpai segariththu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sattaippaigalukkul natpai segariththu "/>
</div>
<div class="lyrico-lyrics-wrapper">poththikkondom nenjoadu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poththikkondom nenjoadu "/>
</div>
<div class="lyrico-lyrics-wrapper">pirivedhu pirivedhu..... 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pirivedhu pirivedhu..... "/>
</div>
<div class="lyrico-lyrics-wrapper">natpukkul pirivedhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="natpukkul pirivedhu "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaaimugam ninaithaal nanbanin mugam edhirinil varumey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaimugam ninaithaal nanbanin mugam edhirinil varumey "/>
</div>
<div class="lyrico-lyrics-wrapper">sila neram noai vandhu thavithaal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sila neram noai vandhu thavithaal "/>
</div>
<div class="lyrico-lyrics-wrapper">avan vaarthaiyil maruthuva kumaney 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avan vaarthaiyil maruthuva kumaney "/>
</div>
<div class="lyrico-lyrics-wrapper">varudangal aindho paththo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varudangal aindho paththo "/>
</div>
<div class="lyrico-lyrics-wrapper">kanakku poattu pazhagavillai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanakku poattu pazhagavillai "/>
</div>
<div class="lyrico-lyrics-wrapper">aanaalum nooru jenma ninaivugal serndhoamey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aanaalum nooru jenma ninaivugal serndhoamey "/>
</div>
<div class="lyrico-lyrics-wrapper">kuladheiva koayilukku nerththikadan nerndhukkittu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuladheiva koayilukku nerththikadan nerndhukkittu "/>
</div>
<div class="lyrico-lyrics-wrapper">nanbanin peyarai naangal pillaikku vaippoamey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanbanin peyarai naangal pillaikku vaippoamey "/>
</div>
<div class="lyrico-lyrics-wrapper">mana kashttathil varum kaayathai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mana kashttathil varum kaayathai "/>
</div>
<div class="lyrico-lyrics-wrapper">nam kanavukkul varum viruppathai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nam kanavukkul varum viruppathai "/>
</div>
<div class="lyrico-lyrics-wrapper">udan theerppaaney uyir nanban mattum thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="udan theerppaaney uyir nanban mattum thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ettu kangalukkum otrai paarvaitharum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettu kangalukkum otrai paarvaitharum "/>
</div>
<div class="lyrico-lyrics-wrapper">natpai thaippen kannoadu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="natpai thaippen kannoadu "/>
</div>
<div class="lyrico-lyrics-wrapper">sattaippaigalukkul natpai segariththu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sattaippaigalukkul natpai segariththu "/>
</div>
<div class="lyrico-lyrics-wrapper">poththikkolvoam nenjoadu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poththikkolvoam nenjoadu "/>
</div>
<div class="lyrico-lyrics-wrapper">naangu idhayathirkum serndhu thudippadharku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naangu idhayathirkum serndhu thudippadharku "/>
</div>
<div class="lyrico-lyrics-wrapper">natpin idhayam thayangaadhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="natpin idhayam thayangaadhu "/>
</div>
<div class="lyrico-lyrics-wrapper">varangal yedhumindri yougangal vaazhvadharku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varangal yedhumindri yougangal vaazhvadharku "/>
</div>
<div class="lyrico-lyrics-wrapper">natpai vittaal uravedhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="natpai vittaal uravedhu "/>
</div>

</pre>
