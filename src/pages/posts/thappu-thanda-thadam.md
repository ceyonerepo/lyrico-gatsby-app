---
title: "thappu thanda song lyrics"
album: "Thadam"
artist: "Arun Raj"
lyricist: "Eknath"
director: "Magizh Thirumeni"
path: "/albums/thadam-lyrics"
song: "Thappu Thanda"
image: ../../images/albumart/thadam.jpg
date: 2019-03-01
lang: tamil
youtubeLink: "https://www.youtube.com/embed/DVVTub2-qJc"
type: "happy"
singers:
  - V. M. Mahalingam
  - Arun Raj
  - Rohit Sridhar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ye Thappu Thanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Thappu Thanda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thappu Thaanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappu Thaanda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ellarume Pannuromey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellarume Pannuromey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edho Oru Thappu Thanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edho Oru Thappu Thanda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thundu Thundaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thundu Thundaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhammaa Thundaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhammaa Thundaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ellarayum Aatudhadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellarayum Aatudhadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edho Oru Bodhai Dhaanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edho Oru Bodhai Dhaanda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aasai Venumda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai Venumda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ayul Koodum Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayul Koodum Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thevaikku Poi Sollu Thappillada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thevaikku Poi Sollu Thappillada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeyum Kaasu Pannuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Kaasu Pannuda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Currency Ennuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Currency Ennuda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pootidhaan Veikkaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pootidhaan Veikkaadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaangaadhudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaangaadhudaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inga Sutthamilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga Sutthamilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kutthamilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kutthamilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kelvikekka Yarumilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelvikekka Yarumilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kallamattum Rombuthadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallamattum Rombuthadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sathamillaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sathamillaama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vera Michamilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vera Michamilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sochchamilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sochchamilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sethunaanga Vechchadhilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sethunaanga Vechchadhilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjam Thembaa Nikkudhadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjam Thembaa Nikkudhadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Achchamillaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achchamillaama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inga Suthamilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga Suthamilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kutthamilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kutthamilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kelvikeka Yaarumilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelvikeka Yaarumilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalla Mattum Rombudhadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalla Mattum Rombudhadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sathamillaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sathamillaama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vera Michamilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vera Michamilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sochamilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sochamilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Serthu Naanga Vechadhilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Serthu Naanga Vechadhilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjam Thembaa Nikkudhadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjam Thembaa Nikkudhadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Achchamillaaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achchamillaaama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Aadum Varai Aattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Aadum Varai Aattam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oodum Varai Ottam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oodum Varai Ottam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yerithaana Erangumadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yerithaana Erangumadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neerin Mattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neerin Mattam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Buththisaali Pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Buththisaali Pulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pala Vitthai Undu Ulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala Vitthai Undu Ulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaiya Vechchaa Therakkaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiya Vechchaa Therakkaadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pootumilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pootumilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Nenjukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nenjukulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irukkudhadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irukkudhadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aasai Mottham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai Mottham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhu Nenjathaandi Podudhadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu Nenjathaandi Podudhadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yedho Saththam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedho Saththam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neram Varumbodhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neram Varumbodhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaandaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Seval Katthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seval Katthum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Namma Ellarukkum Irukudhadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma Ellarukkum Irukudhadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yedho Dhukkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedho Dhukkam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inga Suththamilla Kuththamilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga Suththamilla Kuththamilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kelvikekka Yaarumilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelvikekka Yaarumilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kallamattum Rombudhadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallamattum Rombudhadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saththamillaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saththamillaama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vera Michchamilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vera Michchamilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sochchamilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sochchamilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Serthunaanga Vechadhilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Serthunaanga Vechadhilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjam Themba Nikkudhadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjam Themba Nikkudhadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Achchamillaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achchamillaama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aiyoo Thaakuraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyoo Thaakuraan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vekkam Kekkuraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vekkam Kekkuraan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ora Kannaala Paathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ora Kannaala Paathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Thookuraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Thookuraan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aiyoo Thaakuraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyoo Thaakuraan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vekkam Kekkuraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vekkam Kekkuraan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ora Kannaala Paathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ora Kannaala Paathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Thookuraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Thookuraan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thalli Nikkuraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalli Nikkuraan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pulli Veikkuraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pulli Veikkuraan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhooram Ninnudhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhooram Ninnudhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eerapaarvai Thinguraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eerapaarvai Thinguraan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nambikkai Unmela Nee Veiyadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nambikkai Unmela Nee Veiyadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avan Nambikkai Vandhaaley Thee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan Nambikkai Vandhaaley Thee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veiyadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veiyadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaanaaga Edhuvumey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanaaga Edhuvumey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kidaikkaadhada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kidaikkaadhada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeyum Mutti Modhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Mutti Modhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninnupaaru Gethudhaanada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnupaaru Gethudhaanada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inga Suthamilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga Suthamilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuthamilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuthamilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kelvikekka Yaarumilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelvikekka Yaarumilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kallamattum Rombudhadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallamattum Rombudhadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sathamillaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sathamillaama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vera Michamilla Sochamilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vera Michamilla Sochamilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Serthunaanga Vechadhilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Serthunaanga Vechadhilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjam Themba Nikkudhadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjam Themba Nikkudhadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Achamillaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achamillaama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inga Suthamilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga Suthamilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuthamilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuthamilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kelvikekka Yaarumilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelvikekka Yaarumilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kallamattum Rombudhadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallamattum Rombudhadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sathamillaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sathamillaama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vera Michamilla Sochamilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vera Michamilla Sochamilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Serthunaanga Vechadhilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Serthunaanga Vechadhilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjam Themba Nikkudhadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjam Themba Nikkudhadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Achamillaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achamillaama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inga Suthamilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga Suthamilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuthamilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuthamilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kelvikekka Yaarumilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelvikekka Yaarumilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kallamattum Rombudhadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallamattum Rombudhadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sathamillaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sathamillaama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vera Michamilla Sochamilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vera Michamilla Sochamilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Serthunaanga Vechadhilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Serthunaanga Vechadhilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjam Themba Nikkudhadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjam Themba Nikkudhadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Achamillaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achamillaama"/>
</div>
</pre>
