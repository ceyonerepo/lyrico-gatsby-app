---
title: "thalatu song lyrics"
album: "Enna Thavam Seitheno"
artist: "Dev Guru"
lyricist: "Venkatesh Prabhakar"
director: "Murabasalan"
path: "/albums/enna-thavam-seitheno-lyrics"
song: "Thalatu"
image: ../../images/albumart/enna-thavam-seitheno.jpg
date: 2018-06-22
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Fw5kiS72aKA"
type: "happy"
singers:
  - Malavika
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">thalatu ketka entham manam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalatu ketka entham manam "/>
</div>
<div class="lyrico-lyrics-wrapper">yenga thol sainthu kolla 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yenga thol sainthu kolla "/>
</div>
<div class="lyrico-lyrics-wrapper">la la la la la la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="la la la la la la"/>
</div>
<div class="lyrico-lyrics-wrapper">ennai vaithu sumakum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennai vaithu sumakum"/>
</div>
<div class="lyrico-lyrics-wrapper">vali unaku illai enai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vali unaku illai enai"/>
</div>
<div class="lyrico-lyrics-wrapper">nanjil sumantha vali thanapa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanjil sumantha vali thanapa"/>
</div>
<div class="lyrico-lyrics-wrapper">naan thadumarinen 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan thadumarinen "/>
</div>
<div class="lyrico-lyrics-wrapper">thadai nee neekinai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thadai nee neekinai"/>
</div>
<div class="lyrico-lyrics-wrapper">naan thadam maarinen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan thadam maarinen"/>
</div>
<div class="lyrico-lyrics-wrapper">enai nee meerkiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enai nee meerkiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">un uyirai vaalthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un uyirai vaalthen"/>
</div>
<div class="lyrico-lyrics-wrapper">en nilalai nindraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en nilalai nindraai"/>
</div>
<div class="lyrico-lyrics-wrapper">andru nilal thedinen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="andru nilal thedinen"/>
</div>
<div class="lyrico-lyrics-wrapper">indru nijamaguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indru nijamaguthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thirumanam enbathu ithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thirumanam enbathu ithu"/>
</div>
<div class="lyrico-lyrics-wrapper">puthu uravu iravan thantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthu uravu iravan thantha"/>
</div>
<div class="lyrico-lyrics-wrapper">nalvaravu illaram enbathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nalvaravu illaram enbathu"/>
</div>
<div class="lyrico-lyrics-wrapper">nam puthu unarvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nam puthu unarvu"/>
</div>
<div class="lyrico-lyrics-wrapper">santhosamai kadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="santhosamai kadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">seriyane vali theriyathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seriyane vali theriyathu"/>
</div>
<div class="lyrico-lyrics-wrapper">athan vidai than nam kulanthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athan vidai than nam kulanthai"/>
</div>
<div class="lyrico-lyrics-wrapper">kulanthaiyin sinungal kathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulanthaiyin sinungal kathil"/>
</div>
<div class="lyrico-lyrics-wrapper">olikkum inimaiyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="olikkum inimaiyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">en thathi vantha ninaivum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en thathi vantha ninaivum"/>
</div>
<div class="lyrico-lyrics-wrapper">kai vanthathe athu thudithida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kai vanthathe athu thudithida"/>
</div>
<div class="lyrico-lyrics-wrapper">avar manam thudikirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avar manam thudikirathe"/>
</div>
<div class="lyrico-lyrics-wrapper">en thanthai munbu maalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en thanthai munbu maalai"/>
</div>
<div class="lyrico-lyrics-wrapper">maatri manam mudipen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maatri manam mudipen"/>
</div>
<div class="lyrico-lyrics-wrapper">naan vaalthu vitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan vaalthu vitten"/>
</div>
<div class="lyrico-lyrics-wrapper">ini kanavilum ninaivilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ini kanavilum ninaivilum"/>
</div>
<div class="lyrico-lyrics-wrapper">sugam vanthathe ungal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sugam vanthathe ungal"/>
</div>
<div class="lyrico-lyrics-wrapper">anbu mattum pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anbu mattum pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">en vaalvinile ini mudivugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en vaalvinile ini mudivugal"/>
</div>
<div class="lyrico-lyrics-wrapper">thuyarangal enaku illaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thuyarangal enaku illaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">endrum santhosame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="endrum santhosame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thalatu ketka entham manam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalatu ketka entham manam "/>
</div>
<div class="lyrico-lyrics-wrapper">yenga thol sainthu kolla 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yenga thol sainthu kolla "/>
</div>
<div class="lyrico-lyrics-wrapper">la la la la la la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="la la la la la la"/>
</div>
<div class="lyrico-lyrics-wrapper">ennai vaithu sumakum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennai vaithu sumakum"/>
</div>
<div class="lyrico-lyrics-wrapper">vali unaku illai enai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vali unaku illai enai"/>
</div>
<div class="lyrico-lyrics-wrapper">nanjil sumantha vali thanapa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanjil sumantha vali thanapa"/>
</div>
<div class="lyrico-lyrics-wrapper">naan thadumarinen 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan thadumarinen "/>
</div>
<div class="lyrico-lyrics-wrapper">thadai nee neekinai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thadai nee neekinai"/>
</div>
<div class="lyrico-lyrics-wrapper">naan thadam maarinen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan thadam maarinen"/>
</div>
<div class="lyrico-lyrics-wrapper">enai nee meerkiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enai nee meerkiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">un uyirai vaalthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un uyirai vaalthen"/>
</div>
<div class="lyrico-lyrics-wrapper">en nilalai nindraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en nilalai nindraai"/>
</div>
<div class="lyrico-lyrics-wrapper">andru nilal thedinen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="andru nilal thedinen"/>
</div>
<div class="lyrico-lyrics-wrapper">indru nijamaguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indru nijamaguthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thalatu ketka entham manam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalatu ketka entham manam "/>
</div>
<div class="lyrico-lyrics-wrapper">yenga thol sainthu kolla 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yenga thol sainthu kolla "/>
</div>
<div class="lyrico-lyrics-wrapper">la la la la la la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="la la la la la la"/>
</div>
<div class="lyrico-lyrics-wrapper">ennai vaithu sumakum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennai vaithu sumakum"/>
</div>
<div class="lyrico-lyrics-wrapper">vali unaku illai enai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vali unaku illai enai"/>
</div>
<div class="lyrico-lyrics-wrapper">nanjil sumantha vali thanapa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanjil sumantha vali thanapa"/>
</div>
</pre>
