---
title: "kadikkum joku onnu sollu song lyrics"
album: "Good Luck"
artist: "Manoj Bhatnaghar"
lyricist: "Vairamuthu"
director: "Manoj Bhatnaghar"
path: "/albums/good-luck-song-lyrics"
song: "Kadikkum Joku Onnu Sollu"
image: ../../images/albumart/good-luck.jpg
date: 2000-02-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/3yI1qdK66TE"
type: "happy"
singers:
  - S.N. Surendar
  - K. Prabhakaran
  - Subha
  - Bhuvana
  - Chorus
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kadikkum joke-ku onnu sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadikkum joke-ku onnu sollu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirikkum pennai kannam killu hei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirikkum pennai kannam killu hei"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadikkum joke-ku onnu sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadikkum joke-ku onnu sollu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirikkum pennai kannam killu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirikkum pennai kannam killu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mullaavin joke-kum undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mullaavin joke-kum undu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mexico joke-kum undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mexico joke-kum undu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaethaachchum avuththu vidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaethaachchum avuththu vidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ae….joke solluvathendraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae….joke solluvathendraal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaarukkum solla venaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaarukkum solla venaam"/>
</div>
<div class="lyrico-lyrics-wrapper">En kaadhil azhuththi vidu everybody
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kaadhil azhuththi vidu everybody"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaai vittu siruchchaal noi vittu pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaai vittu siruchchaal noi vittu pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaraachum joke-ku sollunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaraachum joke-ku sollunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaai vittu siruchchaal noi vittu pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaai vittu siruchchaal noi vittu pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaraachum joke-ku sollunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaraachum joke-ku sollunga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadikkum joke-ku onnu sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadikkum joke-ku onnu sollu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirikkum pennai kannam killu…ouch….
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirikkum pennai kannam killu…ouch…."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hei…..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei….."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanjan maaperum kanjan yaarendru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanjan maaperum kanjan yaarendru"/>
</div>
<div class="lyrico-lyrics-wrapper">Potti oruvan vaiththaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Potti oruvan vaiththaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Honey moon pogavum thaniyaai poe vantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Honey moon pogavum thaniyaai poe vantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanjan parisu petraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanjan parisu petraan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kurattai vidukindra kanavanai manaivi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurattai vidukindra kanavanai manaivi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulukki ezhuppi vittaal…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulukki ezhuppi vittaal…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aa….
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa…."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ezhuppiya kaaranam yeanendru ezhnthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhuppiya kaaranam yeanendru ezhnthaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thookka maaththirai kodukka maranthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thookka maaththirai kodukka maranthaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Manaivi padhi sonnaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manaivi padhi sonnaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aa aahaan…..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa aahaan….."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ithuthaandaa nalla joke-ku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithuthaandaa nalla joke-ku"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaarum thinnum kekku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaarum thinnum kekku"/>
</div>
<div class="lyrico-lyrics-wrapper">Silaikooda siriththu vidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silaikooda siriththu vidum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ithuthaandaa nalla joke-ku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithuthaandaa nalla joke-ku"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaarum thinnum kekku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaarum thinnum kekku"/>
</div>
<div class="lyrico-lyrics-wrapper">Silaikooda siriththu vidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silaikooda siriththu vidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Join us…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Join us…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaai vittu siruchchaal noi vittu pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaai vittu siruchchaal noi vittu pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaraachum joke-ku sollunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaraachum joke-ku sollunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaai vittu siruchchaal noi vittu pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaai vittu siruchchaal noi vittu pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaraachum joke-ku sollunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaraachum joke-ku sollunga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadikkum joke-ku onnu sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadikkum joke-ku onnu sollu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirikkum pennai kannam killu…ae….
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirikkum pennai kannam killu…ae…."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enga akkaa sirichaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga akkaa sirichaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannaththil kizhi vizhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaththil kizhi vizhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Endrou siruvan sonnaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endrou siruvan sonnaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enga paatti kannaththil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga paatti kannaththil"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirikkaamal kuzhi vizhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirikkaamal kuzhi vizhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Endraan Innoruvan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endraan Innoruvan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Marumagal oruththi doctor-rai paarththaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marumagal oruththi doctor-rai paarththaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Theal udhattil kottiyathendru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theal udhattil kottiyathendru"/>
</div>
<div class="lyrico-lyrics-wrapper">Udhattil thealkadi eppadi endru doctor ketka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udhattil thealkadi eppadi endru doctor ketka"/>
</div>
<div class="lyrico-lyrics-wrapper">Enathu maamiyaarai kadiththa theal endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enathu maamiyaarai kadiththa theal endru"/>
</div>
<div class="lyrico-lyrics-wrapper">Eduththu mutham thanthean
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eduththu mutham thanthean"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aiyyo….
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyyo…."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pollaatha theal enthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pollaatha theal enthan"/>
</div>
<div class="lyrico-lyrics-wrapper">Udhattil kadiththathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udhattil kadiththathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Udhadum thadithathendraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udhadum thadithathendraal"/>
</div>
<div class="lyrico-lyrics-wrapper">Pollaatha theal enthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pollaatha theal enthan"/>
</div>
<div class="lyrico-lyrics-wrapper">Udhattil kadiththathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udhattil kadiththathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Udhadum thadithathendraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udhadum thadithathendraal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaai vittu siruchchaal noi vittu pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaai vittu siruchchaal noi vittu pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaraachum joke-ku sollunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaraachum joke-ku sollunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaai vittu siruchchaal noi vittu pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaai vittu siruchchaal noi vittu pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaraachum joke-ku sollunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaraachum joke-ku sollunga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadikkum joke-ku onnu sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadikkum joke-ku onnu sollu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirikkum pennai kannam killu…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirikkum pennai kannam killu…"/>
</div>
</pre>
