---
title: "tere siva song lyrics"
album: "Coolie No1"
artist: "Tanishk Bagchi"
lyricist: "Rashmi - Virag"
director: "David Dhawan"
path: "/albums/coolie-no1-lyrics"
song: "Tere Siva"
image: ../../images/albumart/coolie-no1.jpg
date: 2020-12-25
lang: hindi
youtubeLink: "https://www.youtube.com/embed/eCAeknLQuEM"
type: "love"
singers:
  - Renessa Das
  - Ash King
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Dil ko mere yeh kya hua hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil ko mere yeh kya hua hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mil ke tujhe udne laga hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mil ke tujhe udne laga hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sunta nahi ab yeh meri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sunta nahi ab yeh meri"/>
</div>
<div class="lyrico-lyrics-wrapper">Dekho zara bhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dekho zara bhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Khwaabon mein yeh jaake tujhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khwaabon mein yeh jaake tujhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Milne laga hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Milne laga hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tere siva main kuch bhi jaanu na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere siva main kuch bhi jaanu na"/>
</div>
<div class="lyrico-lyrics-wrapper">Bas itna jaanu re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bas itna jaanu re"/>
</div>
<div class="lyrico-lyrics-wrapper">Iss jahaan mein sab kuch chhod ke main
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iss jahaan mein sab kuch chhod ke main"/>
</div>
<div class="lyrico-lyrics-wrapper">Tujhe apna maanu re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tujhe apna maanu re"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tere siva main kuch bhi jaanu na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere siva main kuch bhi jaanu na"/>
</div>
<div class="lyrico-lyrics-wrapper">Bas itna jaanu re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bas itna jaanu re"/>
</div>
<div class="lyrico-lyrics-wrapper">Iss jahaan mein sab kuch chhod ke main
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iss jahaan mein sab kuch chhod ke main"/>
</div>
<div class="lyrico-lyrics-wrapper">Tujhe apna maanu re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tujhe apna maanu re"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tere siva, tere siva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere siva, tere siva"/>
</div>
<div class="lyrico-lyrics-wrapper">Tere si tere si tere si tere siva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere si tere si tere si tere siva"/>
</div>
<div class="lyrico-lyrics-wrapper">Tere si tere si tere si tere siva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere si tere si tere si tere siva"/>
</div>
<div class="lyrico-lyrics-wrapper">Tere si tere si tere si tere siva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere si tere si tere si tere siva"/>
</div>
<div class="lyrico-lyrics-wrapper">Tere si tere si tere si
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere si tere si tere si"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tera mera rishta yeh kya hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tera mera rishta yeh kya hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Khushbu ke sang jaise hawa hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khushbu ke sang jaise hawa hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Hum tum alag lekin magan hai ek jaise
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hum tum alag lekin magan hai ek jaise"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeh ishq ab mujhko samajh aane laga hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh ishq ab mujhko samajh aane laga hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tere siva main kuch bhi mangu na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere siva main kuch bhi mangu na"/>
</div>
<div class="lyrico-lyrics-wrapper">Bas tujhko mangu re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bas tujhko mangu re"/>
</div>
<div class="lyrico-lyrics-wrapper">Iss jahaan mein sab kuch chhod ke main
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iss jahaan mein sab kuch chhod ke main"/>
</div>
<div class="lyrico-lyrics-wrapper">Tujhe apna maanu re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tujhe apna maanu re"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tere siva main kuch bhi mangu na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere siva main kuch bhi mangu na"/>
</div>
<div class="lyrico-lyrics-wrapper">Bas tujhko mangu re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bas tujhko mangu re"/>
</div>
<div class="lyrico-lyrics-wrapper">Iss jahaan mein sab kuch chhod ke main
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iss jahaan mein sab kuch chhod ke main"/>
</div>
<div class="lyrico-lyrics-wrapper">Tujhe apna maanu re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tujhe apna maanu re"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tere siva, tere siva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere siva, tere siva"/>
</div>
<div class="lyrico-lyrics-wrapper">Tere siva, tere siva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere siva, tere siva"/>
</div>
</pre>
