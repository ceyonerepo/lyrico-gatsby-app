---
title: "thittam poda theriyala song lyrics"
album: "Kolamaavu Kokila"
artist: "Anirudh Ravichander"
lyricist: "Vignesh Shivan"
director: "Nelson Dilipkumar"
path: "/albums/kolamaavu-kokila-lyrics"
song: "Thittam Poda Theriyala"
image: ../../images/albumart/kolamaavu-kokila.jpg
date: 2018-08-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/cQ_wKOpz6rQ"
type: "happy"
singers:
  - Anirudh Ravichander
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Thittam poda theriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thittam poda theriyala"/>
</div>
<div class="lyrico-lyrics-wrapper">Bayapada pudikkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bayapada pudikkala"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeyinu therinjum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeyinu therinjum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalli poga mudiyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalli poga mudiyala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vera vazhi theriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vera vazhi theriyala"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalla vazhi kidaikala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla vazhi kidaikala"/>
</div>
<div class="lyrico-lyrics-wrapper">Avalukku onnu na thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avalukku onnu na thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappum thappilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappum thappilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanavellaam varavilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavellaam varavilla"/>
</div>
<div class="lyrico-lyrics-wrapper">En kanna mooda thunivilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kanna mooda thunivilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadavula tholla panni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadavula tholla panni"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhara theriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhara theriyala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edhu sari puriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhu sari puriyala"/>
</div>
<div class="lyrico-lyrics-wrapper">Inga thappu edhu theriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga thappu edhu theriyala"/>
</div>
<div class="lyrico-lyrics-wrapper">Valaiyura neliyura aala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valaiyura neliyura aala"/>
</div>
<div class="lyrico-lyrics-wrapper">Porandhu tholayuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porandhu tholayuren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vali thaangala adhanaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vali thaangala adhanaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Vera vazhiyae illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vera vazhiyae illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vali thaangala adhanaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vali thaangala adhanaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Vera vazhiyae illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vera vazhiyae illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vali thaangala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vali thaangala"/>
</div>
<div class="lyrico-lyrics-wrapper">Vali thaanga ini thembae illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vali thaanga ini thembae illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Avalukku onnu na thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avalukku onnu na thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappum thappilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappum thappilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyir pogala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir pogala"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu irukkum varaikkum kavala illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu irukkum varaikkum kavala illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir pogala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir pogala"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu irukkum varaikkum kavala illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu irukkum varaikkum kavala illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyir pogala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir pogala"/>
</div>
<div class="lyrico-lyrics-wrapper">Adha thavara vera theva illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adha thavara vera theva illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Valaiyura neliyura aala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valaiyura neliyura aala"/>
</div>
<div class="lyrico-lyrics-wrapper">Porandhu tholayuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porandhu tholayuren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaarumae pogatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarumae pogatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thooramae theriyadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thooramae theriyadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Otrai adi padhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Otrai adi padhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Onnu therndheduthenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnu therndheduthenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Moodavae mudiyatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moodavae mudiyatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Aalamum theriyadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalamum theriyadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuli onnil ennai naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuli onnil ennai naanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalli vittenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalli vittenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ellarukkum vaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellarukkum vaanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nallarukkum bothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nallarukkum bothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellarukkum vaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellarukkum vaanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nallarukkum bothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nallarukkum bothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan paakumbothu mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan paakumbothu mattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Karuthu pogudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuthu pogudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mazhai kooda venam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai kooda venam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sinna thooral podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sinna thooral podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Edho oru velicham thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edho oru velicham thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Muzhichi irukkenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muzhichi irukkenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vali thaangala adhanaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vali thaangala adhanaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Vera vazhiyae illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vera vazhiyae illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vali thaangala adhanaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vali thaangala adhanaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Vera vazhiyae illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vera vazhiyae illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vali thaangala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vali thaangala"/>
</div>
<div class="lyrico-lyrics-wrapper">Vali thaanga ini thembae illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vali thaanga ini thembae illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Avalukku onnu na thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avalukku onnu na thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappum thappilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappum thappilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyir pogala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir pogala"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu irukkum varaikkum kavala illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu irukkum varaikkum kavala illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir pogala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir pogala"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu irukkum varaikkum kavala illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu irukkum varaikkum kavala illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyir pogala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir pogala"/>
</div>
<div class="lyrico-lyrics-wrapper">Adha thavara vera theva illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adha thavara vera theva illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Valaiyura neliyura aala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valaiyura neliyura aala"/>
</div>
<div class="lyrico-lyrics-wrapper">Porandhu tholayuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porandhu tholayuren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thittam poda theriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thittam poda theriyala"/>
</div>
<div class="lyrico-lyrics-wrapper">Bayapada pudikkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bayapada pudikkala"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeyinu therinjum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeyinu therinjum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalli poga mudiyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalli poga mudiyala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vera vazhi theriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vera vazhi theriyala"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalla vazhi kidaikala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla vazhi kidaikala"/>
</div>
<div class="lyrico-lyrics-wrapper">Avalukku onnu na thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avalukku onnu na thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappum thappilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappum thappilla"/>
</div>
</pre>
