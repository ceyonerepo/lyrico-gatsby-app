---
title: "thedi thedi song lyrics"
album: "60 Vayadu Maaniram"
artist: "Ilaiyaraaja"
lyricist: "Palani Bharathi"
director: "Radha Mohan"
path: "/albums/60-vayadu-maaniram-lyrics"
song: "Thedi Thedi"
image: ../../images/albumart/60-vayadu-maaniram.jpg
date: 2018-08-31
lang: tamil
youtubeLink: "https://www.youtube.com/embed/G560oUY3UYY"
type: "melody"
singers:
  - Benny Dayal
  - Vibhavari
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Thedi thedi odum kangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedi thedi odum kangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedum uyirai paaraatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedum uyirai paaraatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedi thedi odum kaalgal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedi thedi odum kaalgal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedum idathai saeraatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedum idathai saeraatho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vinnai thedi pogum megam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinnai thedi pogum megam"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannai thedi sindhum vannam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannai thedi sindhum vannam"/>
</div>
<div class="lyrico-lyrics-wrapper">Puthirallavaaaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthirallavaaaaaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thedi thedi odum kangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedi thedi odum kangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedi thedi eee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedi thedi eee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thedaamal ingae yedhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedaamal ingae yedhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhkai ondrum illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkai ondrum illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedaamal theda thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedaamal theda thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthom naam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthom naam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Boomikkul neerai thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boomikkul neerai thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Odum verai polae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odum verai polae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yethyetho paadhai maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethyetho paadhai maari"/>
</div>
<div class="lyrico-lyrics-wrapper">Sendrom naam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sendrom naam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaarai thedi ingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarai thedi ingu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaro yaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaro yaaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Veesum kaatru adhai koorathoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veesum kaatru adhai koorathoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Poovin koottam sollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovin koottam sollum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aari raaroo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aari raaroo"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedum inbam innum koodaathoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedum inbam innum koodaathoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanil ulla vannam ezhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanil ulla vannam ezhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pothaathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pothaathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedalgal theerathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedalgal theerathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thedi thedi odum kangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedi thedi odum kangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedum uyirai paaraatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedum uyirai paaraatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedi thedi odum kaalgal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedi thedi odum kaalgal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedum idathai saeraatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedum idathai saeraatho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pookkal mel odi odi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pookkal mel odi odi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedi vantha kaatru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedi vantha kaatru"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaasathin vaarthai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaasathin vaarthai "/>
</div>
<div class="lyrico-lyrics-wrapper">ondrai sollaatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ondrai sollaatho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engeyo moongil kaatil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engeyo moongil kaatil"/>
</div>
<div class="lyrico-lyrics-wrapper">Ketkkum antha paatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketkkum antha paatu"/>
</div>
<div class="lyrico-lyrics-wrapper">En nenjin thedal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En nenjin thedal "/>
</div>
<div class="lyrico-lyrics-wrapper">enna sollaatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna sollaatho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koondukullae vaazhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koondukullae vaazhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naththai nenjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naththai nenjam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedal kondaal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedal kondaal "/>
</div>
<div class="lyrico-lyrics-wrapper">vinnai thaandaatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vinnai thaandaatho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Suzhal kaatril
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suzhal kaatril"/>
</div>
<div class="lyrico-lyrics-wrapper">Sutri vizhum pookkal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sutri vizhum pookkal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nesam kondu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nesam kondu "/>
</div>
<div class="lyrico-lyrics-wrapper">kaigal thaangaatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaigal thaangaatho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thedal konda nenjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedal konda nenjam"/>
</div>
<div class="lyrico-lyrics-wrapper">Endrum ooyathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endrum ooyathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedalgal theeraathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedalgal theeraathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thedi thedi odum kangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedi thedi odum kangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedum uyirai paaraatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedum uyirai paaraatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedi thedi odum kaalgal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedi thedi odum kaalgal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedum idathai saeraatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedum idathai saeraatho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vinnai thedi pogum megam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinnai thedi pogum megam"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannai thedi sindhum vannam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannai thedi sindhum vannam"/>
</div>
<div class="lyrico-lyrics-wrapper">Puthirallavaaaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthirallavaaaaaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thedi thedi odum kangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedi thedi odum kangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedi thedi eee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedi thedi eee"/>
</div>
</pre>
