---
title: "arere song lyrics"
album: "Mismatch"
artist: "Gifton Elias"
lyricist: "Shreshta"
director: "N V Nirmal Kumar"
path: "/albums/mismatch-lyrics"
song: "Arere"
image: ../../images/albumart/mismatch.jpg
date: 2019-12-06
lang: telugu
youtubeLink: "https://www.youtube.com/embed/FPpMt_VW6-I"
type: "love"
singers:
  - M M Manasi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">arere arere ee pilladu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arere arere ee pilladu"/>
</div>
<div class="lyrico-lyrics-wrapper">edo maaye chesaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edo maaye chesaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">arere arere ee pilladu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arere arere ee pilladu"/>
</div>
<div class="lyrico-lyrics-wrapper">naalo nanne gilladu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naalo nanne gilladu"/>
</div>
<div class="lyrico-lyrics-wrapper">o nimishamlo em chesaado
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="o nimishamlo em chesaado"/>
</div>
<div class="lyrico-lyrics-wrapper">naa manasantha laagesaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa manasantha laagesaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">ee kasthlone elaago
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ee kasthlone elaago"/>
</div>
<div class="lyrico-lyrics-wrapper">mathi dochele maayagaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mathi dochele maayagaadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">arere arere ee pilladu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arere arere ee pilladu"/>
</div>
<div class="lyrico-lyrics-wrapper">edo maaye chesaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edo maaye chesaadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kanureppakemaindo emo mari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanureppakemaindo emo mari"/>
</div>
<div class="lyrico-lyrics-wrapper">kasthaina kanu mooyadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kasthaina kanu mooyadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">kanuchoopulo ninnnu bandhinchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanuchoopulo ninnnu bandhinchi"/>
</div>
<div class="lyrico-lyrics-wrapper">kalalenno kantunnadey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalalenno kantunnadey"/>
</div>
<div class="lyrico-lyrics-wrapper">nee navvulo emunnado
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee navvulo emunnado"/>
</div>
<div class="lyrico-lyrics-wrapper">naa navvulo maareney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa navvulo maareney"/>
</div>
<div class="lyrico-lyrics-wrapper">nee choopule emaunnavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee choopule emaunnavo"/>
</div>
<div class="lyrico-lyrics-wrapper">naa manasulo nuvvuley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa manasulo nuvvuley"/>
</div>
<div class="lyrico-lyrics-wrapper">chiru chiru chiru navuula chiru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chiru chiru chiru navuula chiru"/>
</div>
<div class="lyrico-lyrics-wrapper">jallulalo virisenuley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jallulalo virisenuley"/>
</div>
<div class="lyrico-lyrics-wrapper">ee chelimi harivvilluley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ee chelimi harivvilluley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">arere arere ee pilladu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arere arere ee pilladu"/>
</div>
<div class="lyrico-lyrics-wrapper">edo maaye chesaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edo maaye chesaadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">naa gunde lothullo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa gunde lothullo"/>
</div>
<div class="lyrico-lyrics-wrapper">gammattugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gammattugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">vinipinche nee maatalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vinipinche nee maatalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">naa chentha daagundi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa chentha daagundi"/>
</div>
<div class="lyrico-lyrics-wrapper">o dongala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="o dongala"/>
</div>
<div class="lyrico-lyrics-wrapper">choosthondi nuvva kalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="choosthondi nuvva kalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">nee oosula ee velluva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee oosula ee velluva"/>
</div>
<div class="lyrico-lyrics-wrapper">maha mudduga vundiga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maha mudduga vundiga"/>
</div>
<div class="lyrico-lyrics-wrapper">naa theerilaa ee theerugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa theerilaa ee theerugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">nee vallaney maaraga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee vallaney maaraga"/>
</div>
<div class="lyrico-lyrics-wrapper">gili gili gili ginthala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gili gili gili ginthala"/>
</div>
<div class="lyrico-lyrics-wrapper">kavinthala kerinthalalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kavinthala kerinthalalo"/>
</div>
<div class="lyrico-lyrics-wrapper">enneni pulakinthalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enneni pulakinthalo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">arere arere ee pilladu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arere arere ee pilladu"/>
</div>
<div class="lyrico-lyrics-wrapper">edo maaye chesaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edo maaye chesaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">arere arere ee pilladu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arere arere ee pilladu"/>
</div>
<div class="lyrico-lyrics-wrapper">naalo nanne gilladu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naalo nanne gilladu"/>
</div>
<div class="lyrico-lyrics-wrapper">o nimishamlo em chesaado
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="o nimishamlo em chesaado"/>
</div>
<div class="lyrico-lyrics-wrapper">naa manasantha laagesaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa manasantha laagesaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">ee kasthlone elaago
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ee kasthlone elaago"/>
</div>
<div class="lyrico-lyrics-wrapper">mathi dochele maayagaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mathi dochele maayagaadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">arere arere ee pilladu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arere arere ee pilladu"/>
</div>
<div class="lyrico-lyrics-wrapper">edo maaye chesaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edo maaye chesaadu"/>
</div>
</pre>
