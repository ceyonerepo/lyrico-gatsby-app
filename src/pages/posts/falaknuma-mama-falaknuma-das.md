---
title: "falaknuma mama song lyrics"
album: "Falaknuma Das"
artist: "Vivek Sagar"
lyricist: "Kittu Vissapragada"
director: "Vishwak Sen"
path: "/albums/falaknuma-das-lyrics"
song: "Falaknuma Mama"
image: ../../images/albumart/falaknuma-das.jpg
date: 2019-05-31
lang: telugu
youtubeLink: "https://www.youtube.com/embed/1w5WAbe6En0"
type: "happy"
singers:
  - Jassie Gift
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Gundellona Halla Gulla Perige
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundellona Halla Gulla Perige"/>
</div>
<div class="lyrico-lyrics-wrapper">Daari Tappi Patangula Tirige
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daari Tappi Patangula Tirige"/>
</div>
<div class="lyrico-lyrics-wrapper">Paani Puri Lo Kotha Ghaatu Laa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paani Puri Lo Kotha Ghaatu Laa"/>
</div>
<div class="lyrico-lyrics-wrapper">Manta Puttina Malli Adigeti Masala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manta Puttina Malli Adigeti Masala"/>
</div>
<div class="lyrico-lyrics-wrapper">Charminaruu Pai Ekki Mike Lo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Charminaruu Pai Ekki Mike Lo "/>
</div>
<div class="lyrico-lyrics-wrapper">Shalibanda Mogalanta Sarada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shalibanda Mogalanta Sarada"/>
</div>
<div class="lyrico-lyrics-wrapper">Falaknuma Mama Prema Ani Pilavaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Falaknuma Mama Prema Ani Pilavaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Eh Mama Malli Gunde Nindi Poye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh Mama Malli Gunde Nindi Poye"/>
</div>
<div class="lyrico-lyrics-wrapper">Falaknuma Mama Prema Ani Pilavaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Falaknuma Mama Prema Ani Pilavaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Eh Mama Malli Gunde Nindi Poye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh Mama Malli Gunde Nindi Poye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Galla Patti Gunjindo Galli Galli Tippindo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Galla Patti Gunjindo Galli Galli Tippindo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanne Pilla Oosedo Oosedo Oosedo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanne Pilla Oosedo Oosedo Oosedo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalle Tippi Chusindo Saigalevo Chesindo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalle Tippi Chusindo Saigalevo Chesindo"/>
</div>
<div class="lyrico-lyrics-wrapper">Maanja Kathi Chupedo Chupedo Chupedo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maanja Kathi Chupedo Chupedo Chupedo"/>
</div>
<div class="lyrico-lyrics-wrapper">Benchi Ekki Gantulesina Kothula Mookale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Benchi Ekki Gantulesina Kothula Mookale"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey Okko Naram Onti Lopala Lolledo Pettinde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey Okko Naram Onti Lopala Lolledo Pettinde"/>
</div>
<div class="lyrico-lyrics-wrapper">Ori Teere Maari Daari Tappina Budhe Maarinde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ori Teere Maari Daari Tappina Budhe Maarinde"/>
</div>
<div class="lyrico-lyrics-wrapper">Dillu Horahori Poru Penchina Porini Korinde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dillu Horahori Poru Penchina Porini Korinde"/>
</div>
<div class="lyrico-lyrics-wrapper">Shehar Chusi Raavale Padamani Jashnmanale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shehar Chusi Raavale Padamani Jashnmanale"/>
</div>
<div class="lyrico-lyrics-wrapper">Atharu Poosukunna Sherwani Jigel Anale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atharu Poosukunna Sherwani Jigel Anale"/>
</div>
<div class="lyrico-lyrics-wrapper">Chaduvula Lona Tadapadina 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chaduvula Lona Tadapadina "/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu Nadipedi Note Booku Kaadu Noteu Kattara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu Nadipedi Note Booku Kaadu Noteu Kattara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Salam Kotti Kaburedo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Salam Kotti Kaburedo "/>
</div>
<div class="lyrico-lyrics-wrapper">Haleem Petti Chebutondi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haleem Petti Chebutondi"/>
</div>
<div class="lyrico-lyrics-wrapper">Salam Kotti Kaburedo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Salam Kotti Kaburedo "/>
</div>
<div class="lyrico-lyrics-wrapper">Haleem Petti Chebutondi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haleem Petti Chebutondi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nawaabu Teeru Baitedo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nawaabu Teeru Baitedo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nizam Raajuvantundi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nizam Raajuvantundi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nawaabu Teeru Baitedo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nawaabu Teeru Baitedo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nizam Raajuvantundi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nizam Raajuvantundi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalisipoye Teerunte Dildaar Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalisipoye Teerunte Dildaar Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalisina Raanu Ante Dildaar Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalisina Raanu Ante Dildaar Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Har Din Sabko Aayegi Aayegi Aayegi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Har Din Sabko Aayegi Aayegi Aayegi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kismath Vahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kismath Vahi"/>
</div>
<div class="lyrico-lyrics-wrapper">Chudi Bajaar Ratri Velalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chudi Bajaar Ratri Velalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ramzan Rojullo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ramzan Rojullo "/>
</div>
<div class="lyrico-lyrics-wrapper">Masthu Raddi Lekka Gundelopala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Masthu Raddi Lekka Gundelopala"/>
</div>
<div class="lyrico-lyrics-wrapper">Goledo Pedutunde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Goledo Pedutunde"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey Ghulam Ali Golconda Lo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey Ghulam Ali Golconda Lo"/>
</div>
<div class="lyrico-lyrics-wrapper">Paade Teerale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paade Teerale"/>
</div>
<div class="lyrico-lyrics-wrapper">Kotha Kavvalitho Vintha Shayari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kotha Kavvalitho Vintha Shayari"/>
</div>
<div class="lyrico-lyrics-wrapper">Dilledo Raasinde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dilledo Raasinde"/>
</div>
<div class="lyrico-lyrics-wrapper">Shehar Chusi Raavale Padamani Jashnmanale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shehar Chusi Raavale Padamani Jashnmanale"/>
</div>
<div class="lyrico-lyrics-wrapper">Atharu Poosukunna Sherwani Jigel Anale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atharu Poosukunna Sherwani Jigel Anale"/>
</div>
<div class="lyrico-lyrics-wrapper">Chaduvula Lona Tadapadina 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chaduvula Lona Tadapadina "/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu Nadipedi Note Booku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu Nadipedi Note Booku "/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadu Noteu Kattara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadu Noteu Kattara"/>
</div>
<div class="lyrico-lyrics-wrapper">Falaknuma Mama Prema Ani Pilavaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Falaknuma Mama Prema Ani Pilavaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Eh Mama Malli Gunde Nindi Poye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh Mama Malli Gunde Nindi Poye"/>
</div>
<div class="lyrico-lyrics-wrapper">Falaknuma Mama Prema Ani Pilavaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Falaknuma Mama Prema Ani Pilavaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Eh Mama Malli Gunde Nindi Poye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh Mama Malli Gunde Nindi Poye"/>
</div>
<div class="lyrico-lyrics-wrapper">Falaknuma Mama Prema Ani Pilavaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Falaknuma Mama Prema Ani Pilavaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Eh Mama Malli Gunde Nindi Poye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh Mama Malli Gunde Nindi Poye"/>
</div>
</pre>
