---
title: "mastiya song lyrics"
album: "Kuriyan Jawan Bapu Preshaan"
artist: "Laddi Gill"
lyricist: "Happy Raikoti"
director: "Avtar Singh"
path: "/albums/kuriyan-jawan-bapu-preshaan-lyrics"
song: "Mastiya"
image: ../../images/albumart/kuriyan-jawan-bapu-preshaan.jpg
date: 2021-04-16
lang: panjabi
youtubeLink: "https://www.youtube.com/embed/FBfxVAF7GQQ"
type: "Enjoy"
singers:
  - Kamal Khan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Mastiya mastiya mastiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mastiya mastiya mastiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Kar le maza mastiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kar le maza mastiyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Hastiyan hastiyan hastiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hastiyan hastiyan hastiyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhull jaaye sab hastiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhull jaaye sab hastiyan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mastiya mastiya mastiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mastiya mastiya mastiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Kar le maza mastiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kar le maza mastiyan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Apne treeke naal zindagi nu jee le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Apne treeke naal zindagi nu jee le"/>
</div>
<div class="lyrico-lyrics-wrapper">Hanju te gam saare hasse naal pee le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hanju te gam saare hasse naal pee le"/>
</div>
<div class="lyrico-lyrics-wrapper">Apne treeke naal zindagi nu jee le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Apne treeke naal zindagi nu jee le"/>
</div>
<div class="lyrico-lyrics-wrapper">Hanju te gam saare hasse naal pee le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hanju te gam saare hasse naal pee le"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sastiyan sastiyan sastiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sastiyan sastiyan sastiyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Eh khushiyan nahi sastiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh khushiyan nahi sastiyan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mastiya mastiya mastiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mastiya mastiya mastiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Kar le maza mastiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kar le maza mastiyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Mastiya mastiya mastiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mastiya mastiya mastiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Kar le maza mastiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kar le maza mastiyan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eh ishqe de bin kithe milda aaram ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh ishqe de bin kithe milda aaram ae"/>
</div>
<div class="lyrico-lyrics-wrapper">Eh zindagi swarg'an da hi dooja naam ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh zindagi swarg'an da hi dooja naam ae"/>
</div>
<div class="lyrico-lyrics-wrapper">Eh ishqe de bin kithe milda aaram ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh ishqe de bin kithe milda aaram ae"/>
</div>
<div class="lyrico-lyrics-wrapper">Eh zindagi swarg'an da hi dooja naam ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh zindagi swarg'an da hi dooja naam ae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bastiyan bastiyan bastiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bastiyan bastiyan bastiyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Eh ishqe diyan bastiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh ishqe diyan bastiyan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mastiya mastiya mastiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mastiya mastiya mastiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Kar le maza mastiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kar le maza mastiyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Mastiya mastiya mastiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mastiya mastiya mastiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Kar le maza mastiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kar le maza mastiyan"/>
</div>
</pre>
