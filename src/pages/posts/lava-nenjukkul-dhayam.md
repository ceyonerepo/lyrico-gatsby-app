---
title: "lava nenjukkul song lyrics"
album: "Dhayam"
artist: "Sathish Selvam"
lyricist: "Arun Raja K"
director: "Kannan Rangaswamy"
path: "/albums/dhayam-lyrics"
song: "Lava Nenjukkul"
image: ../../images/albumart/dhayam.jpg
date: 2017-03-24
lang: tamil
youtubeLink: "https://www.youtube.com/embed/f799Fe0dJ0I"
type: "mass"
singers:
  - Alphonse Joseph
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Laava Nenjukkul Theedhan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laava Nenjukkul Theedhan "/>
</div>
<div class="lyrico-lyrics-wrapper">Kannukkul Minnal Pol 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannukkul Minnal Pol "/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum Vandhaayadaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Vandhaayadaa "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saayam Poanaalum Kaayam Aanaalum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saayam Poanaalum Kaayam Aanaalum "/>
</div>
<div class="lyrico-lyrics-wrapper">Dhayam Ondragum Kan Koodada 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhayam Ondragum Kan Koodada "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Vagai Pagaiyudan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Vagai Pagaiyudan "/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkul Oori  
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkul Oori  "/>
</div>
<div class="lyrico-lyrics-wrapper">Varai Murai Edhuvumae 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varai Murai Edhuvumae "/>
</div>
<div class="lyrico-lyrics-wrapper">Irundhaal Meeri 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irundhaal Meeri "/>
</div>
<div class="lyrico-lyrics-wrapper">Adangidum Uyiridam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adangidum Uyiridam "/>
</div>
<div class="lyrico-lyrics-wrapper">Alarattum Osai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alarattum Osai "/>
</div>
<div class="lyrico-lyrics-wrapper">Mudivile Alarattum Aasai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudivile Alarattum Aasai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Laava Nenjukkul Theedhan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laava Nenjukkul Theedhan "/>
</div>
<div class="lyrico-lyrics-wrapper">Kannaukkul Minnal Pol 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaukkul Minnal Pol "/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum Vandhaayadaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Vandhaayadaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Saayam Poanaalum Kaayam Aanaalum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saayam Poanaalum Kaayam Aanaalum "/>
</div>
<div class="lyrico-lyrics-wrapper">Dhayam Ondragum Kan Koodada 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhayam Ondragum Kan Koodada "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yennangal Muzhudhum Vanmangal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yennangal Muzhudhum Vanmangal "/>
</div>
<div class="lyrico-lyrics-wrapper">Niraiya Jenmangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niraiya Jenmangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudikkum Unn Theeyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudikkum Unn Theeyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Saththangal Koduththu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saththangal Koduththu "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yudhangal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yudhangal "/>
</div>
<div class="lyrico-lyrics-wrapper">Nadaththu Raththathai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadaththu Raththathai "/>
</div>
<div class="lyrico-lyrics-wrapper">Kalaththil Sindhamal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalaththil Sindhamal "/>
</div>
<div class="lyrico-lyrics-wrapper">Ovvoandrai Mudikka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ovvoandrai Mudikka "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unn Jeevan Thudikka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unn Jeevan Thudikka "/>
</div>
<div class="lyrico-lyrics-wrapper">Ul Nenjam Veriyil Suththaadho 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ul Nenjam Veriyil Suththaadho "/>
</div>
<div class="lyrico-lyrics-wrapper">Kanmunnae Nadakkum Ellamae 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanmunnae Nadakkum Ellamae "/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkum Ennaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkum Ennaalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Marakka Koodaadho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marakka Koodaadho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhayam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Needhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Needhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhayam"/>
</div>
</pre>
