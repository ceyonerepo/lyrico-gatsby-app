---
title: "kanna kati kaathule song lyrics"
album: "Margandeyan"
artist: "Sundar C Babu"
lyricist: "Gnanakaravel"
director: "FEFSI Vijayan"
path: "/albums/markandeyan-lyrics"
song: "Kanna Kati Kaathule"
image: ../../images/albumart/markandeyan.jpg
date: 2011-07-29
lang: tamil
youtubeLink: "https://www.youtube.com/embed/moT9lOue1bA"
type: "happy"
singers:
  - Velmurugan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ae Kanna Katti Kaathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Kanna Katti Kaathula"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarakkadichaa Mappula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarakkadichaa Mappula"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarikanji Ketkuthada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarikanji Ketkuthada"/>
</div>
<div class="lyrico-lyrics-wrapper">Konduvaa Maappulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konduvaa Maappulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Uppukkari Aduppula 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uppukkari Aduppula "/>
</div>
<div class="lyrico-lyrics-wrapper">Eppavarum Maappilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppavarum Maappilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Perattukkari Thingathaandaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perattukkari Thingathaandaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Porappu Maappilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porappu Maappilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Saathi Sanam Kaathirukka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saathi Sanam Kaathirukka "/>
</div>
<div class="lyrico-lyrics-wrapper">Aattukkadaa Veguthada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aattukkadaa Veguthada"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaattupaiyan Koottathukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattupaiyan Koottathukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Karikkozhambey Kadavulada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karikkozhambey Kadavulada"/>
</div>
<div class="lyrico-lyrics-wrapper">Pachakkari Vaasathukkey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pachakkari Vaasathukkey "/>
</div>
<div class="lyrico-lyrics-wrapper">Echividum Payalgalukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Echividum Payalgalukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Sothukari Eruthuvacha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sothukari Eruthuvacha "/>
</div>
<div class="lyrico-lyrics-wrapper">Sothukooda Uthiriyadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sothukooda Uthiriyadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dei Dei Hei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dei Dei Hei"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ae Kanna Katti Kaathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Kanna Katti Kaathula"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarakkadichaa Mappula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarakkadichaa Mappula"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarikanji Ketkuthada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarikanji Ketkuthada"/>
</div>
<div class="lyrico-lyrics-wrapper">Konduvaa Maappulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konduvaa Maappulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Uppukkari Aduppula 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uppukkari Aduppula "/>
</div>
<div class="lyrico-lyrics-wrapper">Eppavarum Maappilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppavarum Maappilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Perattukkari Thingathaandaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perattukkari Thingathaandaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Porappu Maappilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porappu Maappilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Soru Ilai Poattu Virunthu Vechaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soru Ilai Poattu Virunthu Vechaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiruttu Karithaandaa Rusi Athigam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiruttu Karithaandaa Rusi Athigam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varutha Kari Thinnu Varusham Moonaachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varutha Kari Thinnu Varusham Moonaachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vayirae Vedichaa Thaan Veri Adangum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayirae Vedichaa Thaan Veri Adangum"/>
</div>
<div class="lyrico-lyrics-wrapper">Suvarettiya Motha Suttuthinnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suvarettiya Motha Suttuthinnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Evan Thaduthaalum Ragalappannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evan Thaduthaalum Ragalappannu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjikkari Vachi Varuvalpannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjikkari Vachi Varuvalpannu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenji Irumbaakkum Nørukkithinnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenji Irumbaakkum Nørukkithinnu"/>
</div>
È<div class="lyrico-lyrics-wrapper">eral Thundu Ila Maaripøanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eral Thundu Ila Maaripøanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pathu Køødaaram Pøarkalanthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathu Køødaaram Pøarkalanthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Aattukkaalu Chaaru Kudichaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aattukkaalu Chaaru Kudichaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adangaathu Alapparathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adangaathu Alapparathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ae Kanna Katti Kaathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Kanna Katti Kaathula"/>
</div>
Š<div class="lyrico-lyrics-wrapper">arakkadichaa Mappula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arakkadichaa Mappula"/>
</div>
Š<div class="lyrico-lyrics-wrapper">arikanji Ketkuthada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arikanji Ketkuthada"/>
</div>
<div class="lyrico-lyrics-wrapper">Kønduvaa Maappulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kønduvaa Maappulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Uppukkari Aduppula 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uppukkari Aduppula "/>
</div>
È<div class="lyrico-lyrics-wrapper">ppavarum Maappilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ppavarum Maappilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Perattukkari Thingathaandaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perattukkari Thingathaandaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Pørappu Maappilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pørappu Maappilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaanja Melagaayum Pøøndum Thengaayum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanja Melagaayum Pøøndum Thengaayum"/>
</div>
Š<div class="lyrico-lyrics-wrapper">aernthaa Rusi Køødum Thøvanjathukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aernthaa Rusi Køødum Thøvanjathukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Patta Šaaraayam Vayitha Punnaakum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Patta Šaaraayam Vayitha Punnaakum"/>
</div>
<div class="lyrico-lyrics-wrapper">Marunthaa Athøada Kudalirukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marunthaa Athøada Kudalirukku"/>
</div>
Š<div class="lyrico-lyrics-wrapper">anda Vanthaa Mutti Thøøkkippødu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anda Vanthaa Mutti Thøøkkippødu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Valuvaakkum Thala Karithaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Valuvaakkum Thala Karithaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Veettukkulla Thenam Kattil Aada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veettukkulla Thenam Kattil Aada"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Usuppethum Thødakkarithaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Usuppethum Thødakkarithaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ratham Kønjam Patta Šarakkum Kønjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ratham Kønjam Patta Šarakkum Kønjam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pøtaa Unakkulla Urumum Šingam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pøtaa Unakkulla Urumum Šingam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ae Aattu Møølaiya Varattithinnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Aattu Møølaiya Varattithinnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Puli Kaala Vegam Varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puli Kaala Vegam Varum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ae Kanna Katti Kaathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Kanna Katti Kaathula"/>
</div>
Š<div class="lyrico-lyrics-wrapper">arakkadichaa Mappula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arakkadichaa Mappula"/>
</div>
Š<div class="lyrico-lyrics-wrapper">arikanji Ketkuthada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arikanji Ketkuthada"/>
</div>
<div class="lyrico-lyrics-wrapper">Kønduvaa Maappulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kønduvaa Maappulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Uppukkari Aduppula 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uppukkari Aduppula "/>
</div>
È<div class="lyrico-lyrics-wrapper">ppavarum Maappilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ppavarum Maappilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Perattukkari Thingathaandaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perattukkari Thingathaandaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Pørappu Maappilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pørappu Maappilla"/>
</div>
Š<div class="lyrico-lyrics-wrapper">aathi Šanam Kaathirukka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aathi Šanam Kaathirukka "/>
</div>
<div class="lyrico-lyrics-wrapper">Aattukkadaa Veguthada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aattukkadaa Veguthada"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaattupaiyan Køøttathukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattupaiyan Køøttathukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Karikkøzhambey Kadavulada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karikkøzhambey Kadavulada"/>
</div>
<div class="lyrico-lyrics-wrapper">Pachakkari Vaasathukkey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pachakkari Vaasathukkey "/>
</div>
È<div class="lyrico-lyrics-wrapper">chividum Payalgalukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chividum Payalgalukku"/>
</div>
Šø<div class="lyrico-lyrics-wrapper">thukari Èruthuvacha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thukari Èruthuvacha "/>
</div>
Šø<div class="lyrico-lyrics-wrapper">thukøøda Uthiriyadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thukøøda Uthiriyadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dei Dei Hei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dei Dei Hei"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ae Kanna Katti Kaathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Kanna Katti Kaathula"/>
</div>
Š<div class="lyrico-lyrics-wrapper">arakkadichaa Mappula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arakkadichaa Mappula"/>
</div>
Š<div class="lyrico-lyrics-wrapper">arikanji Ketkuthada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arikanji Ketkuthada"/>
</div>
<div class="lyrico-lyrics-wrapper">Kønduvaa Maappulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kønduvaa Maappulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Uppukkari Aduppula 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uppukkari Aduppula "/>
</div>
È<div class="lyrico-lyrics-wrapper">ppavarum Maappilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ppavarum Maappilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Perattukkari Thingathaandaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perattukkari Thingathaandaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Pørappu Maappilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pørappu Maappilla"/>
</div>
</pre>
