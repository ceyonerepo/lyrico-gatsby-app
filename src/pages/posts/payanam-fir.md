---
title: "payanam song lyrics"
album: "FIR"
artist: "Ashwath"
lyricist: "Bagavathy PK"
director: "Manu Anand"
path: "/albums/fir-lyrics"
song: "Payanam"
image: ../../images/albumart/fir.jpg
date: 2022-02-11
lang: tamil
youtubeLink: "https://www.youtube.com/embed/2lMwjtfw92M"
type: "love"
singers:
  - Abhay Jodhpurkar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yen intha kirakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen intha kirakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thazhatha mayakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thazhatha mayakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sil endra iravum athil thodangum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sil endra iravum athil thodangum"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha payanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha payanam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pesatha mozhiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesatha mozhiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedatha nilavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedatha nilavum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennodu avalum idhu thodarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennodu avalum idhu thodarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha payanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha payanam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mudhal murai uyir varai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhal murai uyir varai "/>
</div>
<div class="lyrico-lyrics-wrapper">silirtu paarkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="silirtu paarkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Aval tharum unarvu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aval tharum unarvu "/>
</div>
<div class="lyrico-lyrics-wrapper">thaan enai ketkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaan enai ketkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sikkikondadum uyirum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sikkikondadum uyirum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thikkithindadum manamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thikkithindadum manamum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaththi koothada thavikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaththi koothada thavikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha nodiyil intha payanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha nodiyil intha payanam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En vidiyal athu pudhiya thotramo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En vidiyal athu pudhiya thotramo"/>
</div>
<div class="lyrico-lyrics-wrapper">Un arugil idhu enna matramo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un arugil idhu enna matramo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iravin madiyil marugum manathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravin madiyil marugum manathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Salikkum kanava salana ninaiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Salikkum kanava salana ninaiva"/>
</div>
<div class="lyrico-lyrics-wrapper">Irukkum nilavum karukkul pozhudhil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irukkum nilavum karukkul pozhudhil"/>
</div>
<div class="lyrico-lyrics-wrapper">Nerungi varava vizhagi vidava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nerungi varava vizhagi vidava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sikkikondadum uyirum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sikkikondadum uyirum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thikkithindadum manamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thikkithindadum manamum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaththi koothada thavikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaththi koothada thavikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha nodiyil intha payanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha nodiyil intha payanam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sil endra iravum athil thodangum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sil endra iravum athil thodangum"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha payanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha payanam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un idhayam adhu ennai sehrumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un idhayam adhu ennai sehrumo"/>
</div>
<div class="lyrico-lyrics-wrapper">Naam uravu adhu ondru podhumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam uravu adhu ondru podhumo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mazhaiyin thuliyil mudhalil swarisam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhaiyin thuliyil mudhalil swarisam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sitharum ninaivil iniya kuzhappum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sitharum ninaivil iniya kuzhappum"/>
</div>
<div class="lyrico-lyrics-wrapper">Malarin malarum pudhiya sugangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malarin malarum pudhiya sugangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Unathu endrathum mugara thayakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unathu endrathum mugara thayakkam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sikkikondadum uyirum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sikkikondadum uyirum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thikkithindadum manamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thikkithindadum manamum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaththi koothada thavikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaththi koothada thavikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha nodiyil intha payanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha nodiyil intha payanam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen intha kirakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen intha kirakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thazhatha mayakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thazhatha mayakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sil endra iravum athil thodangum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sil endra iravum athil thodangum"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha payanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha payanam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pesatha mozhiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesatha mozhiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedatha nilavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedatha nilavum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennodu avalum idhu thodarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennodu avalum idhu thodarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha payanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha payanam"/>
</div>
</pre>
