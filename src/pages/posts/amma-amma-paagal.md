---
title: "amma amma song lyrics"
album: "Paagal"
artist: "Radhan"
lyricist: "Ramajoggaya Sastry"
director: "Naressh Kuppili"
path: "/albums/paagal-lyrics"
song: "Amma Amma"
image: ../../images/albumart/paagal.jpg
date: 2021-08-14
lang: telugu
youtubeLink: "https://www.youtube.com/embed/wgySWgIO4bg"
type: "sentiment"
singers:
  - Sid Sriram
  - Veda
  - Vagdevi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kanupaapa Nuvvai Velugisthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanupaapa Nuvvai Velugisthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Kalaku Rangula Merupisthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Kalaku Rangula Merupisthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adugadugu Needai Nadipisthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adugadugu Needai Nadipisthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Prathi Malupulo Nanu Gelipisthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prathi Malupulo Nanu Gelipisthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Andaga Undave Eppudoo Nuvvilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andaga Undave Eppudoo Nuvvilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pandugai Nindave Lopalaa Velupalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pandugai Nindave Lopalaa Velupalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu Naathodai Lenidhe Nenelaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu Naathodai Lenidhe Nenelaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Amma Amma Nee Vennela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma Amma Nee Vennela"/>
</div>
<div class="lyrico-lyrics-wrapper">Nithyam Naapai Undaalilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nithyam Naapai Undaalilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Amma Amma Nee Vennela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma Amma Nee Vennela"/>
</div>
<div class="lyrico-lyrics-wrapper">Nithyam Naapai Undaalilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nithyam Naapai Undaalilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Amma Amma Nee Vennela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma Amma Nee Vennela"/>
</div>
<div class="lyrico-lyrics-wrapper">Nithyam Naapai Undaalilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nithyam Naapai Undaalilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Amma Amma Nee Vennela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma Amma Nee Vennela"/>
</div>
<div class="lyrico-lyrics-wrapper">Nithyam Naapai Undaalilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nithyam Naapai Undaalilaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thalanimuru Cheyi Chaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalanimuru Cheyi Chaalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanuvantha Haayi Swaraalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanuvantha Haayi Swaraalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Laalana Swaanthana Anni Neeve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laalana Swaanthana Anni Neeve"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasaraa Panchina Aanaati Nee Konavelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasaraa Panchina Aanaati Nee Konavelu"/>
</div>
<div class="lyrico-lyrics-wrapper">Deevenai Nadapagaa Nindu Noorellu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Deevenai Nadapagaa Nindu Noorellu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Modati Nesthamaa Nee 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Modati Nesthamaa Nee "/>
</div>
<div class="lyrico-lyrics-wrapper">Theepi Guruthulu Velu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theepi Guruthulu Velu"/>
</div>
<div class="lyrico-lyrics-wrapper">Repane Rojuku Daari Deepaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Repane Rojuku Daari Deepaalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Amma Amma Nee Vennela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma Amma Nee Vennela"/>
</div>
<div class="lyrico-lyrics-wrapper">Nithyam Naapai Undaalilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nithyam Naapai Undaalilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Amma Amma Nee Vennela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma Amma Nee Vennela"/>
</div>
<div class="lyrico-lyrics-wrapper">Nithyam Naapai Undaalilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nithyam Naapai Undaalilaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lokaana Ammalantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lokaana Ammalantha"/>
</div>
<div class="lyrico-lyrics-wrapper">ndhinchu Premananthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ndhinchu Premananthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Okka Nuvve Varamugaa Panchinaavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okka Nuvve Varamugaa Panchinaavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinnadhe Aakaasham 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinnadhe Aakaasham "/>
</div>
<div class="lyrico-lyrics-wrapper">Anipinchu Mamatavu Neevu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anipinchu Mamatavu Neevu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannilaa Penchagaa Enchukunnaavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannilaa Penchagaa Enchukunnaavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enni Marujanmalu Naakedhurupadinagaani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enni Marujanmalu Naakedhurupadinagaani"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Odi Paapagaa Nannundaneemmaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Odi Paapagaa Nannundaneemmaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Amma Amma Nee Vennela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma Amma Nee Vennela"/>
</div>
<div class="lyrico-lyrics-wrapper">Nithyam Naapai Undaalilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nithyam Naapai Undaalilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Amma Amma Nee Vennela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma Amma Nee Vennela"/>
</div>
<div class="lyrico-lyrics-wrapper">Nithyam Naapai Undaalilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nithyam Naapai Undaalilaa"/>
</div>
</pre>
