---
title: "goindhammavaala song lyrics"
album: "Vada Chennai"
artist: "Santhosh Narayanan"
lyricist: "Rokesh"
director: "Vetrimaaran"
path: "/albums/vada-chennai-lyrics"
song: "Goindhammavaala"
image: ../../images/albumart/vada-chennai.jpg
date: 2018-10-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/I-Hc0Xr7WUM"
type: "happy"
singers:
  - Dhanush
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Mmmkkum mmmkkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mmmkkum mmmkkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mmmkkum mmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mmmkkum mmm"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Goindhammavaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Goindhammavaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Goindhan manguraan lovvaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Goindhan manguraan lovvaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yei yei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei yei"/>
</div>
<div class="lyrico-lyrics-wrapper">Yei yei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei yei"/>
</div>
<div class="lyrico-lyrics-wrapper">Yei yei yei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei yei yei"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kolaaru pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolaaru pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Bejaaraa suthuraan ponnaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bejaaraa suthuraan ponnaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Yei yei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei yei"/>
</div>
<div class="lyrico-lyrics-wrapper">Yei yei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei yei"/>
</div>
<div class="lyrico-lyrics-wrapper">Yei yei yei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei yei yei"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mmmkkum mmmkkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mmmkkum mmmkkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mmmkkum yei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mmmkkum yei"/>
</div>
<div class="lyrico-lyrics-wrapper">Mmmkkum mmmkkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mmmkkum mmmkkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mmmkkum mmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mmmkkum mmm"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tholuvaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholuvaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava jilovaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava jilovaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharuvaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharuvaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Baluvaa vaa vaa vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baluvaa vaa vaa vaa vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Daavadikkura saavadikkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daavadikkura saavadikkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayyae noyyinnu kaitharukkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyae noyyinnu kaitharukkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Kammaayila kallaa pecha kudukkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kammaayila kallaa pecha kudukkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Thottaakkaa pattunu sadhaaikkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottaakkaa pattunu sadhaaikkura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paathidikkura naakkadikkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paathidikkura naakkadikkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayyayyo innammaa sirikkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyayyo innammaa sirikkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulla fullaa yamma nee irukkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulla fullaa yamma nee irukkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Gujaalaa mayakkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gujaalaa mayakkura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanaa thaa nanaa naanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanaa thaa nanaa naanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanaa naanaa naanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanaa naanaa naanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanaa naanaa naanaa naanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanaa naanaa naanaa naanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">hei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hei"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanaa thaa nanaa naanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanaa thaa nanaa naanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanaa naanaa naanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanaa naanaa naanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanaa naanaa naanaa naanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanaa naanaa naanaa naanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ilichinu eenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilichinu eenu"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayatha isthunuputtaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayatha isthunuputtaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mmmkkum mmmkkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mmmkkum mmmkkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mmmkkum mmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mmmkkum mmm"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Palichunu maanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palichunu maanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannaalayae kaathaadi uttaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaalayae kaathaadi uttaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mmmkkum mmmkkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mmmkkum mmmkkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mmmkkum mmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mmmkkum mmm"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mmmkkum mmmkkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mmmkkum mmmkkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mmmkkum mmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mmmkkum mmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Mmmkkum mmmkkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mmmkkum mmmkkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mmmkkum mmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mmmkkum mmm"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaadi yamma vellaattaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadi yamma vellaattaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Velagaadha unnai naan thottaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velagaadha unnai naan thottaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Saanjaa nillu support-ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saanjaa nillu support-ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjundu konju silent-ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjundu konju silent-ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hmkum hmkum hmkum hmkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmkum hmkum hmkum hmkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmkum hmkum hmkum hmkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmkum hmkum hmkum hmkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Yei goindhammavaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei goindhammavaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Goindhan manguraan lovvaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Goindhan manguraan lovvaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolaaru pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolaaru pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Bejaaraa suthuraan ponnaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bejaaraa suthuraan ponnaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei"/>
</div>
<div class="lyrico-lyrics-wrapper">Daavadikkura saavadikkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daavadikkura saavadikkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayyae noyyinnu kaitharukkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyae noyyinnu kaitharukkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Kammaayila kallaa pecha kudukkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kammaayila kallaa pecha kudukkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Thottaakkaa pattunu sadhaaikkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottaakkaa pattunu sadhaaikkura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paathidikkura naakkadikkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paathidikkura naakkadikkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayyayyo innammaa sirikkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyayyo innammaa sirikkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulla fullaa yamma nee irukkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulla fullaa yamma nee irukkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Gujaalaa mayakkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gujaalaa mayakkura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanaa thaa nanaa naanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanaa thaa nanaa naanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanaa naanaa naanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanaa naanaa naanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanaa naanaa naanaa naanaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanaa naanaa naanaa naanaa "/>
</div>
<div class="lyrico-lyrics-wrapper">hei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hei"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanaa thaa nanaa naanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanaa thaa nanaa naanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanaa naanaa naanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanaa naanaa naanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanaa naanaa naanaa naanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanaa naanaa naanaa naanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaiya pudicha kathaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiya pudicha kathaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">En mood-ah yemaathaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En mood-ah yemaathaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Mutham mattum pathaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mutham mattum pathaadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Flat-aakkum yamma un bodha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Flat-aakkum yamma un bodha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mmmkkum mmmkkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mmmkkum mmmkkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mmmkkum mmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mmmkkum mmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Mmmkkum mmmkkum mmmkkum mmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mmmkkum mmmkkum mmmkkum mmm"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yei goindhammavaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei goindhammavaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Goindhan manguraan lovvaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Goindhan manguraan lovvaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolaaru pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolaaru pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Bejaaraa suthuraan ponnaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bejaaraa suthuraan ponnaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa kaalaiyila poo thalaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa kaalaiyila poo thalaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Naasthi illaadha nadaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naasthi illaadha nadaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Yammaa neram ninnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yammaa neram ninnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Road-tu kadaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Road-tu kadaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakkaavaa theettura pisurilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkaavaa theettura pisurilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bore-adikkala un madiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bore-adikkala un madiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Paduthaa poiyidhum theriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paduthaa poiyidhum theriyala"/>
</div>
<div class="lyrico-lyrics-wrapper">Kosukkadikooda saluppedukkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kosukkadikooda saluppedukkala"/>
</div>
<div class="lyrico-lyrics-wrapper">Figure-u nenappula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Figure-u nenappula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanaa thaa nanaa naanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanaa thaa nanaa naanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanaa naanaa naanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanaa naanaa naanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanaa naanaa naanaa naanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanaa naanaa naanaa naanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">hei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hei"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanaa thaa nanaa naanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanaa thaa nanaa naanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanaa naanaa naanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanaa naanaa naanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanaa naanaa naanaa naanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanaa naanaa naanaa naanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Goindhammavaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Goindhammavaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Mmmkkum eei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mmmkkum eei"/>
</div>
<div class="lyrico-lyrics-wrapper">Mmmkkum eei mmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mmmkkum eei mmm"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Goindhammavaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Goindhammavaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Mmmkkum mmmkkum mmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mmmkkum mmmkkum mmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Goindhammavaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Goindhammavaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Eh yeei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh yeei"/>
</div>
<div class="lyrico-lyrics-wrapper">Goindhammavaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Goindhammavaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Mmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Goindhammavaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Goindhammavaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayyo goindhammavaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyo goindhammavaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Yei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei"/>
</div>
</pre>
