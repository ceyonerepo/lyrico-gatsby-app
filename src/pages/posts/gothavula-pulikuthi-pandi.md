---
title: "gothavula song lyrics"
album: "Pulikuthi Pandi"
artist: "N R Raghunanthan"
lyricist: "Mohan Raj"
director: "M. Muthaiah"
path: "/albums/pulikuthi-pandi-song-lyrics"
song: "Gothavula"
image: ../../images/albumart/pulikuthi-pandi.jpg
date: 2021-01-15
lang: tamil
youtubeLink: "https://www.youtube.com/embed/MvMCAVR_8Yg"
type: "Intorduction"
singers:
  - Mahalingam
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Adicha Pori Kalangum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adicha Pori Kalangum"/>
</div>
<div class="lyrico-lyrics-wrapper">Moracha Veri Kelambum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moracha Veri Kelambum"/>
</div>
<div class="lyrico-lyrics-wrapper">Salam Poda Vaikkum Sandiyaru Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Salam Poda Vaikkum Sandiyaru Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kai Kal Veda Vedangum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai Kal Veda Vedangum"/>
</div>
<div class="lyrico-lyrics-wrapper">Udambe Nadu Nadungum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udambe Nadu Nadungum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Paartha Neeyum Etti Iru Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Paartha Neeyum Etti Iru Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gothavula Kuthikka Soratta Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gothavula Kuthikka Soratta Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Kaala Vecha Sooraveli Edukkum Puzhuthi Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Kaala Vecha Sooraveli Edukkum Puzhuthi Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Gothavula Kuthikka Soratta Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Gothavula Kuthikka Soratta Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Kaala Vecha Sooraveli Edukkum Puzhuthi Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Kaala Vecha Sooraveli Edukkum Puzhuthi Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aarambathil Pesididuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aarambathil Pesididuven"/>
</div>
<div class="lyrico-lyrics-wrapper">Aproma Agadiduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aproma Agadiduven"/>
</div>
<div class="lyrico-lyrics-wrapper">Athai Avan Kekkalana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athai Avan Kekkalana"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarunu Naan Kaattiduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarunu Naan Kaattiduven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kettatha Naan Maranthuduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kettatha Naan Maranthuduven"/>
</div>
<div class="lyrico-lyrics-wrapper">Nallatha Naan Manichiduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nallatha Naan Manichiduven"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooru Vamba Othikkiduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooru Vamba Othikkiduven"/>
</div>
<div class="lyrico-lyrics-wrapper">Vantha Vamba Pozhanthuduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vantha Vamba Pozhanthuduven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gothavula…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gothavula…"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Gothavula…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Gothavula…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Gothavula Kuthikka Soratta Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Gothavula Kuthikka Soratta Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Kaala Vecha Sooraveli Edukkum Puzhuthi Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Kaala Vecha Sooraveli Edukkum Puzhuthi Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Dey Gothavula Kuthikka Soratta Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dey Gothavula Kuthikka Soratta Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Kaala Vecha Sooraveli Edukkum Puzhuthi Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Kaala Vecha Sooraveli Edukkum Puzhuthi Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Yethi Kattu Yerakki Kattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Yethi Kattu Yerakki Kattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aattamthula Pozhanthu Kattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aattamthula Pozhanthu Kattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thozhu Renda Nimithikittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozhu Renda Nimithikittu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Maathi Kattu Madichi Kattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Maathi Kattu Madichi Kattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Rappula Nee Round-U Kattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rappula Nee Round-U Kattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannu Renda Urutti Kittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu Renda Urutti Kittu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Pambarama Suththuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Pambarama Suththuvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattaiya Thaan Kelappuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattaiya Thaan Kelappuvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattasa Thaan Pottu Kittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattasa Thaan Pottu Kittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhill-Ah Nippom Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhill-Ah Nippom Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanga Vanthu Ninna Arunthu Pogum Jallikattu Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga Vanthu Ninna Arunthu Pogum Jallikattu Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga Paartha Odum Tsunami-Um Surundukittu Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga Paartha Odum Tsunami-Um Surundukittu Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga Vanthu Ninna Arunthu Pogum Jallikattu Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga Vanthu Ninna Arunthu Pogum Jallikattu Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga Paartha Odum Tsunami-Um Surundukittu Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga Paartha Odum Tsunami-Um Surundukittu Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pulivesham Katti Aaduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pulivesham Katti Aaduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga Puli Paalil Tea-Eh Poduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga Puli Paalil Tea-Eh Poduvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Pulivesham Katti Aaduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Pulivesham Katti Aaduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga Puli Paalil Tea-Eh Poduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga Puli Paalil Tea-Eh Poduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Pangali, Pulivesham Katti Aaduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pangali, Pulivesham Katti Aaduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga Puli Paalil Tea-Eh Poduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga Puli Paalil Tea-Eh Poduvom"/>
</div>
</pre>
