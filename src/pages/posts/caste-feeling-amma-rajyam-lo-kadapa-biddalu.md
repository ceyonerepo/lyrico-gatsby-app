---
title: "caste feeling song lyrics"
album: "Amma Rajyam Lo Kadapa Biddalu"
artist: "Ravi Shankar"
lyricist: "Sirasri"
director: "Siddhartha Thatholu"
path: "/albums/amma-rajyam-lo-kadapa-biddalu-lyrics"
song: "Caste Feeling"
image: ../../images/albumart/amma-rajyam-lo-kadapa-biddalu.jpg
date: 2019-12-12
lang: telugu
youtubeLink: "https://www.youtube.com/embed/g9nODSJiyLQ"
type: "mass"
singers:
  - Ram Gopal Varma
  - RGV
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Naaku Chaala Caste Feeling Undi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naaku Chaala Caste Feeling Undi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ala Undatam Nenu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ala Undatam Nenu "/>
</div>
<div class="lyrico-lyrics-wrapper">Chaala Garwamga Feel Avthanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chaala Garwamga Feel Avthanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Caste Feeling Undatam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Caste Feeling Undatam "/>
</div>
<div class="lyrico-lyrics-wrapper">Chaala Vere Feelingla Kanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chaala Vere Feelingla Kanna"/>
</div>
<div class="lyrico-lyrics-wrapper">Chaala Chaala Better 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chaala Chaala Better "/>
</div>
<div class="lyrico-lyrics-wrapper">Feeling Ani Naa Feeling
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Feeling Ani Naa Feeling"/>
</div>
<div class="lyrico-lyrics-wrapper">Yevari Kulam Meeda 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yevari Kulam Meeda "/>
</div>
<div class="lyrico-lyrics-wrapper">Vallakunna Premani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vallakunna Premani"/>
</div>
<div class="lyrico-lyrics-wrapper">Kula Gajji Ane Oka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kula Gajji Ane Oka "/>
</div>
<div class="lyrico-lyrics-wrapper">Parama Neecha Asahyamaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parama Neecha Asahyamaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Perutho Varninche Vallaki 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perutho Varninche Vallaki "/>
</div>
<div class="lyrico-lyrics-wrapper">Idhe Naa Samadhanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhe Naa Samadhanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhalaadi Kulalu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhalaadi Kulalu "/>
</div>
<div class="lyrico-lyrics-wrapper">Rajyanga Baddamga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rajyanga Baddamga "/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Deshamlo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Deshamlo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kulam Adiginavaadu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulam Adiginavaadu "/>
</div>
<div class="lyrico-lyrics-wrapper">Gaadida Laanti Hypocrisy 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaadida Laanti Hypocrisy "/>
</div>
<div class="lyrico-lyrics-wrapper">Nindina Quotationlani 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nindina Quotationlani "/>
</div>
<div class="lyrico-lyrics-wrapper">Janam Meeda Rudde 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Janam Meeda Rudde "/>
</div>
<div class="lyrico-lyrics-wrapper">Medialu Unnappudu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Medialu Unnappudu "/>
</div>
<div class="lyrico-lyrics-wrapper">Kulanidemundandi Antune 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulanidemundandi Antune "/>
</div>
<div class="lyrico-lyrics-wrapper">Phalana Pranthamlo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phalana Pranthamlo"/>
</div>
<div class="lyrico-lyrics-wrapper">Phalana Kulam Vaari 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phalana Kulam Vaari "/>
</div>
<div class="lyrico-lyrics-wrapper">Votle Yekkuva Kabatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Votle Yekkuva Kabatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Phalana Kulam Nayakudike 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phalana Kulam Nayakudike "/>
</div>
<div class="lyrico-lyrics-wrapper">Seat Ivvalane Raajakeeya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seat Ivvalane Raajakeeya"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhagulbajilu Unnappudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhagulbajilu Unnappudu"/>
</div>
<div class="lyrico-lyrics-wrapper">Prathibake Pattam Kattali Antune
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prathibake Pattam Kattali Antune"/>
</div>
<div class="lyrico-lyrics-wrapper">Phalana Kulamlo Puttadu Kabatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phalana Kulamlo Puttadu Kabatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Athaniki Prathiba 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athaniki Prathiba "/>
</div>
<div class="lyrico-lyrics-wrapper">Lekapoyina Paravaledu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lekapoyina Paravaledu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ane Sadalimpulu Chesthunnappudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ane Sadalimpulu Chesthunnappudu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilaanti Okadanikokati 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilaanti Okadanikokati "/>
</div>
<div class="lyrico-lyrics-wrapper">Sambhandam Leni Vyathireka Matalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sambhandam Leni Vyathireka Matalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vini Vini Vini Visugetthi Chirakudobbi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vini Vini Vini Visugetthi Chirakudobbi"/>
</div>
<div class="lyrico-lyrics-wrapper">Caste Meede Sarwam Nadusthundani 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Caste Meede Sarwam Nadusthundani "/>
</div>
<div class="lyrico-lyrics-wrapper">Namme Ee Rastramlo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namme Ee Rastramlo"/>
</div>
<div class="lyrico-lyrics-wrapper">Musugulo Guddulatalaapi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Musugulo Guddulatalaapi "/>
</div>
<div class="lyrico-lyrics-wrapper">Caste Feelingni Bahirangamga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Caste Feelingni Bahirangamga "/>
</div>
<div class="lyrico-lyrics-wrapper">Sagarwamga Gonthetthi Arichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sagarwamga Gonthetthi Arichi"/>
</div>
<div class="lyrico-lyrics-wrapper">Chatukovalani Cheppe Uddeshamthone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chatukovalani Cheppe Uddeshamthone"/>
</div>
<div class="lyrico-lyrics-wrapper">Kamma Rajyam Lo Kadapa Reddlu Cinemalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kamma Rajyam Lo Kadapa Reddlu Cinemalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandarbhanusaranga Ee 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandarbhanusaranga Ee "/>
</div>
<div class="lyrico-lyrics-wrapper">Caste Feeling Paata Petta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Caste Feeling Paata Petta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kammalu Kaapulu Reddlu Rajulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kammalu Kaapulu Reddlu Rajulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vyshyulu Brahmanulu Extralu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vyshyulu Brahmanulu Extralu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kammalu Kaapulu Reddlu Rajulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kammalu Kaapulu Reddlu Rajulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vyshyulu Brahmanulu Extralu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vyshyulu Brahmanulu Extralu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenu Naa Desham 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenu Naa Desham "/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Ooru Naa Kutumbam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Ooru Naa Kutumbam"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Matham Naa Snehithulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Matham Naa Snehithulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Bandhuvulu Naa Pillalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Bandhuvulu Naa Pillalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Feelinglanni Correctainapudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Feelinglanni Correctainapudu"/>
</div>
<div class="lyrico-lyrics-wrapper">Caste Feeling Yenduku Thappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Caste Feeling Yenduku Thappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Caste Feeling Yenduku Thappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Caste Feeling Yenduku Thappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhenikiraa Ee Hypocrisy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhenikiraa Ee Hypocrisy"/>
</div>
<div class="lyrico-lyrics-wrapper">Yendukuraa Ee Hypocrisy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yendukuraa Ee Hypocrisy"/>
</div>
<div class="lyrico-lyrics-wrapper">Samuhamanedi Oka Aikamathyamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samuhamanedi Oka Aikamathyamu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kulamu Perutho Okatiga Unte 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulamu Perutho Okatiga Unte "/>
</div>
<div class="lyrico-lyrics-wrapper">Yenduku Thappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenduku Thappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kulamu Perutho Goppalu Chebithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulamu Perutho Goppalu Chebithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Yevariki Muppu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yevariki Muppu"/>
</div>
<div class="lyrico-lyrics-wrapper">Caste Feeling Yenduku Thappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Caste Feeling Yenduku Thappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Caste Feeling Yenduku Thappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Caste Feeling Yenduku Thappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yemitiraa Ee Hypocrisy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yemitiraa Ee Hypocrisy"/>
</div>
<div class="lyrico-lyrics-wrapper">Yendukuraa Ee Hypocracy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yendukuraa Ee Hypocracy"/>
</div>
<div class="lyrico-lyrics-wrapper">Kammalu Kaapulu Reddlu Rajulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kammalu Kaapulu Reddlu Rajulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vyshyulu Brahmanulu Extralu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vyshyulu Brahmanulu Extralu"/>
</div>
<div class="lyrico-lyrics-wrapper">Deshanni Keerthisthe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Deshanni Keerthisthe "/>
</div>
<div class="lyrico-lyrics-wrapper">Desha Bhakthi Ainapudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Desha Bhakthi Ainapudu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kulanni Keerthinche Kula Bhakthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulanni Keerthinche Kula Bhakthi"/>
</div>
<div class="lyrico-lyrics-wrapper">Yenduku Thappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenduku Thappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mathanni Nammithe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mathanni Nammithe "/>
</div>
<div class="lyrico-lyrics-wrapper">Daiva Bhakthi Annapudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daiva Bhakthi Annapudu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kulanni Nammukune Kula Bhakthi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulanni Nammukune Kula Bhakthi "/>
</div>
<div class="lyrico-lyrics-wrapper">Yenduku Thappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenduku Thappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Feelinglanni Correctainapudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Feelinglanni Correctainapudu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oka Caste Feeling Yenduku Thappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oka Caste Feeling Yenduku Thappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oka Caste Feeling Yenduku Thappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oka Caste Feeling Yenduku Thappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kammalu Kaapulu Reddlu Rajulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kammalu Kaapulu Reddlu Rajulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vyshyulu Brahmanulu Extralu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vyshyulu Brahmanulu Extralu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kammalu Kaapulu Reddlu Rajulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kammalu Kaapulu Reddlu Rajulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vyshyulu Brahmanulu Extralu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vyshyulu Brahmanulu Extralu"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhinnatwamlo Yekathwam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhinnatwamlo Yekathwam "/>
</div>
<div class="lyrico-lyrics-wrapper">Ani Chaate Deshamlo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ani Chaate Deshamlo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kulam Peru Yetthithe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulam Peru Yetthithe "/>
</div>
<div class="lyrico-lyrics-wrapper">Yenduku Thappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenduku Thappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Reservationlatho Nadiche Ee Deshamlo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Reservationlatho Nadiche Ee Deshamlo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kulam Peru Adigithe Yenduku Muppu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulam Peru Adigithe Yenduku Muppu"/>
</div>
<div class="lyrico-lyrics-wrapper">Denikee Ee Hypocrisy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Denikee Ee Hypocrisy"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Rastram Naa Baasha Naa Istam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Rastram Naa Baasha Naa Istam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ani Goppalu Cheppagaliginapudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ani Goppalu Cheppagaliginapudu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Kulam Peru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Kulam Peru "/>
</div>
<div class="lyrico-lyrics-wrapper">Goppaga Cheppenduku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Goppaga Cheppenduku"/>
</div>
<div class="lyrico-lyrics-wrapper">Naakenduku Siggu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naakenduku Siggu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mera Bharat Mahaan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mera Bharat Mahaan "/>
</div>
<div class="lyrico-lyrics-wrapper">Ani Yelugetthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ani Yelugetthi"/>
</div>
<div class="lyrico-lyrics-wrapper">Chaatagaliginappudu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chaatagaliginappudu "/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Kulame Vardhillali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Kulame Vardhillali"/>
</div>
<div class="lyrico-lyrics-wrapper">Ani Aravadam Yedhuku Thappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ani Aravadam Yedhuku Thappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kammalu Kaapulu Reddlu Rajulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kammalu Kaapulu Reddlu Rajulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vyshyulu Brahmanulu Extralu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vyshyulu Brahmanulu Extralu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kammalu Kaapulu Reddlu Rajulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kammalu Kaapulu Reddlu Rajulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vyshyulu Brahmanulu Extralu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vyshyulu Brahmanulu Extralu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Intha Logic Tho Cheppina 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Logic Tho Cheppina "/>
</div>
<div class="lyrico-lyrics-wrapper">Meekardham Avvakapothe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meekardham Avvakapothe"/>
</div>
<div class="lyrico-lyrics-wrapper">Meeku Ardham Chesukovatam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meeku Ardham Chesukovatam "/>
</div>
<div class="lyrico-lyrics-wrapper">Istam Ledani Naaku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Istam Ledani Naaku "/>
</div>
<div class="lyrico-lyrics-wrapper">Ardham Avthundi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ardham Avthundi"/>
</div>
</pre>
