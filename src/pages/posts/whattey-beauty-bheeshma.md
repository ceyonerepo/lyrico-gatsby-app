---
title: "whattey beauty song lyrics"
album: "Bheeshma"
artist: "Mahati Swara Sagar"
lyricist: "Kasarla Shyam"
director: "Venky Kudumula"
path: "/albums/bheeshma-lyrics"
song: "Whattey Beauty"
image: ../../images/albumart/bheeshma.jpg
date: 2020-02-21
lang: telugu
youtubeLink: "https://www.youtube.com/embed/uyiuYfSAc-A"
type: "love"
singers:
  - Dhanunjay
  - Amala Chebolu
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Whattey whattey whattey beauty
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Whattey whattey whattey beauty"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu yaada unte aane ooty
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu yaada unte aane ooty"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Whattey whattey whattey beauty
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Whattey whattey whattey beauty"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu yaada unte aane ooty
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu yaada unte aane ooty"/>
</div>
<div class="lyrico-lyrics-wrapper">Tipputhunte nadume naughty
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tipputhunte nadume naughty"/>
</div>
<div class="lyrico-lyrics-wrapper">Na kandle chese kantri duty
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na kandle chese kantri duty"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nuvvu dhaggari kostunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu dhaggari kostunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Sallaga sali pedathandhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sallaga sali pedathandhey"/>
</div>
<div class="lyrico-lyrics-wrapper">Dooramelli pothante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dooramelli pothante"/>
</div>
<div class="lyrico-lyrics-wrapper">Masth udaka posthandhe dhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Masth udaka posthandhe dhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Tight hug ichchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tight hug ichchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Tattoo la antukoraadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tattoo la antukoraadhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Twinkle twinkle twinkle little star
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Twinkle twinkle twinkle little star"/>
</div>
<div class="lyrico-lyrics-wrapper">Amma ayya intlo yavaruu leru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma ayya intlo yavaruu leru"/>
</div>
<div class="lyrico-lyrics-wrapper">Desi twinkle twinkle twinkle little star
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Desi twinkle twinkle twinkle little star"/>
</div>
<div class="lyrico-lyrics-wrapper">Terichunchave pori frontudoru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Terichunchave pori frontudoru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Soodake sitti mantalu putti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soodake sitti mantalu putti"/>
</div>
<div class="lyrico-lyrics-wrapper">Fire engine tiruguthandhe gantalu kotti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fire engine tiruguthandhe gantalu kotti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rail engine la koodhal petti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rail engine la koodhal petti"/>
</div>
<div class="lyrico-lyrics-wrapper">Timantha gadipeyyaku maatalu thoti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Timantha gadipeyyaku maatalu thoti"/>
</div>
<div class="lyrico-lyrics-wrapper">Endalo nuv thiragodde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endalo nuv thiragodde"/>
</div>
<div class="lyrico-lyrics-wrapper">Suryunike chamatattdhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suryunike chamatattdhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Inthandane daachodhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inthandane daachodhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Income tax raid ayipoddhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Income tax raid ayipoddhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Twinkle twinkle twinkle little star
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Twinkle twinkle twinkle little star"/>
</div>
<div class="lyrico-lyrics-wrapper">Amma ayya intlo yavaruu leru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma ayya intlo yavaruu leru"/>
</div>
<div class="lyrico-lyrics-wrapper">Desi twinkle twinkle twinkle little star
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Desi twinkle twinkle twinkle little star"/>
</div>
<div class="lyrico-lyrics-wrapper">Terichunchave pori frontudoru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Terichunchave pori frontudoru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">NUv koosunna eh seataina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="NUv koosunna eh seataina"/>
</div>
<div class="lyrico-lyrics-wrapper">Swarganiki direct ga adi flightenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Swarganiki direct ga adi flightenna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Innalugaa singlegunnaa nee photo ke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innalugaa singlegunnaa nee photo ke"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenu frame ayi ponaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenu frame ayi ponaa"/>
</div>
<div class="lyrico-lyrics-wrapper">NUvukaalu mpina chote
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="NUvukaalu mpina chote"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee bhumiki beauty spoteyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee bhumiki beauty spoteyy"/>
</div>
<div class="lyrico-lyrics-wrapper">Foreign lo nuv puttuntey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Foreign lo nuv puttuntey"/>
</div>
<div class="lyrico-lyrics-wrapper">Thellolant duckout eyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thellolant duckout eyy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Twinkle twinkle twinkle little star
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Twinkle twinkle twinkle little star"/>
</div>
<div class="lyrico-lyrics-wrapper">Amma ayya intlo yavaruu leru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma ayya intlo yavaruu leru"/>
</div>
<div class="lyrico-lyrics-wrapper">Desi twinkle twinkle twinkle little star
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Desi twinkle twinkle twinkle little star"/>
</div>
<div class="lyrico-lyrics-wrapper">Terichunchave pori frontudoru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Terichunchave pori frontudoru"/>
</div>
</pre>
