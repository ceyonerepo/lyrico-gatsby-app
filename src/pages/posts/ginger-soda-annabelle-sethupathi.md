---
title: "ginger soda song lyrics"
album: "Annabelle Sethupathi"
artist: "Krishna Kishor"
lyricist: "Ku. Karthik"
director: "Deepak Sundarrajan"
path: "/albums/annabelle-sethupathi-lyrics"
song: "Ginger Soda"
image: ../../images/albumart/annabelle-sethupathi.jpg
date: 2021-09-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/8d4e7dce94Y"
type: "happy"
singers:
  - Anirudh Ravichander
  - Yashita Sharma
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ginger-u Soda Munnale Water-ah Oothatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ginger-u Soda Munnale Water-ah Oothatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Under-u Gang Munnale Finger-ah Neettatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Under-u Gang Munnale Finger-ah Neettatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Anthamaari Indhamaari Yengeyum Nee Pesatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anthamaari Indhamaari Yengeyum Nee Pesatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Opponent-u Summanu Thaan Neeyum Nenaikatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Opponent-u Summanu Thaan Neeyum Nenaikatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Singer-u Kitta Vanthu Nee Sangathi Vekkatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Singer-u Kitta Vanthu Nee Sangathi Vekkatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunder-u Gate-u Munnale Speaker-ah Kaattatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunder-u Gate-u Munnale Speaker-ah Kaattatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Munna Vacha Kaala Neeyum Pinna Yedukkatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munna Vacha Kaala Neeyum Pinna Yedukkatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Route Eduthu Odurappo Pecha Kudukkatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Route Eduthu Odurappo Pecha Kudukkatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Wrong-ah Right-ah Seirathu Vitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wrong-ah Right-ah Seirathu Vitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Atha Weight-ah Quite-ah Senjiduvom Yaaru Kitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atha Weight-ah Quite-ah Senjiduvom Yaaru Kitta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeppavum Billa Billa Thaan Athari Pacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeppavum Billa Billa Thaan Athari Pacha"/>
</div>
<div class="lyrico-lyrics-wrapper">Area Namma Jilla Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Area Namma Jilla Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattanam Kalla Kalla Thaan Kannula Patta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattanam Kalla Kalla Thaan Kannula Patta"/>
</div>
<div class="lyrico-lyrics-wrapper">Maattunom Full-ah Gulla Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maattunom Full-ah Gulla Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aasai Illatha Aala Paarthale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai Illatha Aala Paarthale"/>
</div>
<div class="lyrico-lyrics-wrapper">Waste-u Fellow-nu Sollum Bhoomi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Waste-u Fellow-nu Sollum Bhoomi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vesham Podathe Aalu Ingilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vesham Podathe Aalu Ingilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Yennu Kelu Neeyum Money Thaan Saamy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yennu Kelu Neeyum Money Thaan Saamy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaaru Best-u Yaaru First-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaru Best-u Yaaru First-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan Thaan Inge King-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan Thaan Inge King-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum Aagalam Naanum Aagalam Raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Aagalam Naanum Aagalam Raja"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhill Oda Nee Munna Vandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhill Oda Nee Munna Vandha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeppavum Billa Billa Thaan Athari Pacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeppavum Billa Billa Thaan Athari Pacha"/>
</div>
<div class="lyrico-lyrics-wrapper">Area Namma Jilla Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Area Namma Jilla Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattanam Kalla Kalla Thaan Kannula Patta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattanam Kalla Kalla Thaan Kannula Patta"/>
</div>
<div class="lyrico-lyrics-wrapper">Maattunom Full-ah Gulla Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maattunom Full-ah Gulla Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Singer-u Kitta Vanthu Nee Sangathi Vekkatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Singer-u Kitta Vanthu Nee Sangathi Vekkatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunder-u Gate-u Munnale Speaker-ah Kaattatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunder-u Gate-u Munnale Speaker-ah Kaattatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Munna Vacha Kaala Neeyum Pinna Yedukkatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munna Vacha Kaala Neeyum Pinna Yedukkatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Route Eduthu Odurappo Pecha Kudukkatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Route Eduthu Odurappo Pecha Kudukkatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Wrong-ah Right-ah Seirathu Vitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wrong-ah Right-ah Seirathu Vitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Atha Weight-ah Quite-ah Senjiduvom Yaaru Kitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atha Weight-ah Quite-ah Senjiduvom Yaaru Kitta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeppavum Billa Billa Thaan Athari Pacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeppavum Billa Billa Thaan Athari Pacha"/>
</div>
<div class="lyrico-lyrics-wrapper">Area Namma Jilla Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Area Namma Jilla Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattanam Kalla Kalla Thaan Kannula Patta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattanam Kalla Kalla Thaan Kannula Patta"/>
</div>
<div class="lyrico-lyrics-wrapper">Maattunom Full-ah Gulla Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maattunom Full-ah Gulla Thaan"/>
</div>
</pre>
