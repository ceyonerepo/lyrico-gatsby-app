---
title: "jigelu rani song lyrics"
album: "Rangasthalam"
artist: "Devi Sri Prasad"
lyricist: "Chandrabose"
director: "Sukumar"
path: "/albums/rangasthalam-lyrics"
song: "Jigelu Rani"
image: ../../images/albumart/rangasthalam.jpg
date: 2022-06-30
lang: telugu
youtubeLink: "https://www.youtube.com/embed/Fhfpa-6utR8"
type: "happy"
singers:
  -	Rela Kumar
  - Ganta Venkata Lakshmi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Oo muddu petave jigelu rani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo muddu petave jigelu rani"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannaina kotava jigelu rani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaina kotava jigelu rani"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo muddu petave jigelu rani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo muddu petave jigelu rani"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannaina kotave jigelu rani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaina kotave jigelu rani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muddemo mana sabaki petesaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muddemo mana sabaki petesaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannemo tarananiki kottesane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannemo tarananiki kottesane"/>
</div>
<div class="lyrico-lyrics-wrapper">Muddemo mana sabaki petesaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muddemo mana sabaki petesaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannemo tarananiki kottesane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannemo tarananiki kottesane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Okasari vattestava jigelu rani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okasari vattestava jigelu rani"/>
</div>
<div class="lyrico-lyrics-wrapper">Kotta Presidentki adhi dachiunchaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kotta Presidentki adhi dachiunchaane"/>
</div>
<div class="lyrico-lyrics-wrapper">maapitela intikostava jigelu raani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maapitela intikostava jigelu raani"/>
</div>
<div class="lyrico-lyrics-wrapper">nee ayya thoti poti neku vaddantane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee ayya thoti poti neku vaddantane"/>
</div>
<div class="lyrico-lyrics-wrapper">mari nakem istave jigelu raani Oyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mari nakem istave jigelu raani Oyy"/>
</div>
<div class="lyrico-lyrics-wrapper">nuvu korindi edhanina ichestaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvu korindi edhanina ichestaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">jil jil jil jigelu raaja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jil jil jil jigelu raaja"/>
</div>
<div class="lyrico-lyrics-wrapper">nuvu adigindi edaina kaadhantaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvu adigindi edaina kaadhantaana"/>
</div>
<div class="lyrico-lyrics-wrapper">jil jil jil jigelu raaja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jil jil jil jigelu raaja"/>
</div>
<div class="lyrico-lyrics-wrapper">unnadhadigithe nenu ledhantaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnadhadigithe nenu ledhantaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nee vayasu sepave jigelu raani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee vayasu sepave jigelu raani"/>
</div>
<div class="lyrico-lyrics-wrapper">nee Aaro classulo aapesaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee Aaro classulo aapesaane"/>
</div>
<div class="lyrico-lyrics-wrapper">nuvu sadhivindenthe jigelu raani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvu sadhivindenthe jigelu raani"/>
</div>
<div class="lyrico-lyrics-wrapper">maa magalla weakness sadhivesaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maa magalla weakness sadhivesaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oo navvu navvave jigelu raani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo navvu navvave jigelu raani"/>
</div>
<div class="lyrico-lyrics-wrapper">guddi cheddi panchi oodithe navvesaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="guddi cheddi panchi oodithe navvesaane"/>
</div>
<div class="lyrico-lyrics-wrapper">nannu baava anave jigelu raani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nannu baava anave jigelu raani"/>
</div>
<div class="lyrico-lyrics-wrapper">adhi Police ollake resevation ye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adhi Police ollake resevation ye"/>
</div>
<div class="lyrico-lyrics-wrapper">Premistavaa nannu jigelu raanii Hoyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Premistavaa nannu jigelu raanii Hoyi"/>
</div>
<div class="lyrico-lyrics-wrapper">raassithava mari nee aasthi paasthini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="raassithava mari nee aasthi paasthini"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">jil jil jil jigelu raaja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jil jil jil jigelu raaja"/>
</div>
<div class="lyrico-lyrics-wrapper">nuvu adigindi edaina kaadhantaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvu adigindi edaina kaadhantaana"/>
</div>
<div class="lyrico-lyrics-wrapper">jil jil jil jigelu raaja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jil jil jil jigelu raaja"/>
</div>
<div class="lyrico-lyrics-wrapper">unnadadigithe nenu ledantaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnadadigithe nenu ledantaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oyy Nuv pettina poolu immantamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oyy Nuv pettina poolu immantamu"/>
</div>
<div class="lyrico-lyrics-wrapper">poolathoti vatini poojistamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poolathoti vatini poojistamu"/>
</div>
<div class="lyrico-lyrics-wrapper">nuvu kattina koka immantamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvu kattina koka immantamu"/>
</div>
<div class="lyrico-lyrics-wrapper">dhanni chuttuku memu padukuntamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhanni chuttuku memu padukuntamu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvu esina Gajulu immantamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvu esina Gajulu immantamu"/>
</div>
<div class="lyrico-lyrics-wrapper">vaati sappudu vintu sahcipotamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaati sappudu vintu sahcipotamu"/>
</div>
<div class="lyrico-lyrics-wrapper">arey nuvu poosina scentu immantamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arey nuvu poosina scentu immantamu"/>
</div>
<div class="lyrico-lyrics-wrapper">ara bara vasana chustu bathikesthamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ara bara vasana chustu bathikesthamu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">jil jil jil jigelu raaja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jil jil jil jigelu raaja"/>
</div>
<div class="lyrico-lyrics-wrapper">vatini vela kathalo pettanu raaja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vatini vela kathalo pettanu raaja"/>
</div>
<div class="lyrico-lyrics-wrapper">jil jil jil jigelu raaja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jil jil jil jigelu raaja"/>
</div>
<div class="lyrico-lyrics-wrapper">evadi paata aadu padandoyi raaja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="evadi paata aadu padandoyi raaja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">naa paata velukunna ungaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa paata velukunna ungaram"/>
</div>
<div class="lyrico-lyrics-wrapper">naa paata thulam bangaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa paata thulam bangaram"/>
</div>
<div class="lyrico-lyrics-wrapper">naa paata santhalo konna kodedhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa paata santhalo konna kodedhu"/>
</div>
<div class="lyrico-lyrics-wrapper">naa paata puli goru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa paata puli goru"/>
</div>
<div class="lyrico-lyrics-wrapper">vendi pallem ekaram mamidi thota
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vendi pallem ekaram mamidi thota"/>
</div>
<div class="lyrico-lyrics-wrapper">maa avida techina katnam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maa avida techina katnam"/>
</div>
<div class="lyrico-lyrics-wrapper">kattinchukunna illu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kattinchukunna illu"/>
</div>
<div class="lyrico-lyrics-wrapper">naa paata rice uu milluu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa paata rice uu milluu"/>
</div>
<div class="lyrico-lyrics-wrapper">ivanni kadhu gaani naa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ivanni kadhu gaani naa "/>
</div>
<div class="lyrico-lyrics-wrapper">pata cashuu lakshaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pata cashuu lakshaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ayibaaboiii lashee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayibaaboiii lashee"/>
</div>
</pre>
