---
title: "run for your life song lyrics"
album: "6 Athiyayam"
artist: "Sam C.S."
lyricist: "Kavitha Thomas - Karki Bhava"
director: "	Cable Sankar - Shankar Thiyagarajan - Ajayan Bala - EAV Suresh - Lokesh Rajendran- Sridhar Venkatesan"
path: "/albums/6-athiyayam-song-lyrics"
song: "Run For Your Life"
image: ../../images/albumart/6-athiyayam.jpg
date: 2018-02-23
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Fel2Ou9bCNs"
type: "happy"
singers:
  - Kavitha Thomas
  - Ma Ka Pa Anand
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">hai better run
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hai better run"/>
</div>
<div class="lyrico-lyrics-wrapper">iam wanna get you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iam wanna get you"/>
</div>
<div class="lyrico-lyrics-wrapper">watch out the night
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="watch out the night"/>
</div>
<div class="lyrico-lyrics-wrapper">is calling you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="is calling you"/>
</div>
<div class="lyrico-lyrics-wrapper">deep in the dark
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="deep in the dark"/>
</div>
<div class="lyrico-lyrics-wrapper">its drawing you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="its drawing you"/>
</div>
<div class="lyrico-lyrics-wrapper">cant run cant hide
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cant run cant hide"/>
</div>
<div class="lyrico-lyrics-wrapper">the light wont do
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="the light wont do"/>
</div>
<div class="lyrico-lyrics-wrapper">you think you escape
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="you think you escape"/>
</div>
<div class="lyrico-lyrics-wrapper">thats when you loos
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thats when you loos"/>
</div>
<div class="lyrico-lyrics-wrapper">run for your life
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="run for your life"/>
</div>
<div class="lyrico-lyrics-wrapper">you better run
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="you better run"/>
</div>
<div class="lyrico-lyrics-wrapper">you better run
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="you better run"/>
</div>
<div class="lyrico-lyrics-wrapper">its coming to get you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="its coming to get you"/>
</div>
<div class="lyrico-lyrics-wrapper">your better run
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="your better run"/>
</div>
<div class="lyrico-lyrics-wrapper">run for your life
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="run for your life"/>
</div>
<div class="lyrico-lyrics-wrapper">run for your life
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="run for your life"/>
</div>
<div class="lyrico-lyrics-wrapper">you better go go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="you better go go"/>
</div>
<div class="lyrico-lyrics-wrapper">run for your life
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="run for your life"/>
</div>
<div class="lyrico-lyrics-wrapper">you better run
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="you better run"/>
</div>
<div class="lyrico-lyrics-wrapper">you better run
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="you better run"/>
</div>
<div class="lyrico-lyrics-wrapper">its coming to get you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="its coming to get you"/>
</div>
<div class="lyrico-lyrics-wrapper">you better run
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="you better run"/>
</div>
<div class="lyrico-lyrics-wrapper">run for your life
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="run for your life"/>
</div>
<div class="lyrico-lyrics-wrapper">you better run
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="you better run"/>
</div>
<div class="lyrico-lyrics-wrapper">its coming to get you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="its coming to get you"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vellai selai katti vidava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vellai selai katti vidava"/>
</div>
<div class="lyrico-lyrics-wrapper">peya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="peya"/>
</div>
<div class="lyrico-lyrics-wrapper">gundu malli onnu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gundu malli onnu "/>
</div>
<div class="lyrico-lyrics-wrapper">vachi vidava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vachi vidava"/>
</div>
<div class="lyrico-lyrics-wrapper">velli golusai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="velli golusai"/>
</div>
<div class="lyrico-lyrics-wrapper">matti vidava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="matti vidava"/>
</div>
<div class="lyrico-lyrics-wrapper">naaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naaye"/>
</div>
<div class="lyrico-lyrics-wrapper">kooti vanthu konjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kooti vanthu konjam"/>
</div>
<div class="lyrico-lyrics-wrapper">katha vidava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="katha vidava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vellai selai katti vidava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vellai selai katti vidava"/>
</div>
<div class="lyrico-lyrics-wrapper">peya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="peya"/>
</div>
<div class="lyrico-lyrics-wrapper">gundu malli onnu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gundu malli onnu "/>
</div>
<div class="lyrico-lyrics-wrapper">vachi vidava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vachi vidava"/>
</div>
<div class="lyrico-lyrics-wrapper">velli golusai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="velli golusai"/>
</div>
<div class="lyrico-lyrics-wrapper">matti vidava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="matti vidava"/>
</div>
<div class="lyrico-lyrics-wrapper">naaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naaye"/>
</div>
<div class="lyrico-lyrics-wrapper">kooti vanthu konjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kooti vanthu konjam"/>
</div>
<div class="lyrico-lyrics-wrapper">katha vidava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="katha vidava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">rathiri yudham theri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rathiri yudham theri"/>
</div>
<div class="lyrico-lyrics-wrapper">therikum peigalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="therikum peigalum"/>
</div>
<div class="lyrico-lyrics-wrapper">kolla adam pidikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kolla adam pidikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">naai nari satham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naai nari satham"/>
</div>
<div class="lyrico-lyrics-wrapper">kizhi kizhikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kizhi kizhikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">ratham ratham 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ratham ratham "/>
</div>
<div class="lyrico-lyrics-wrapper">varatha sudukatu pakam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varatha sudukatu pakam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">run for your life
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="run for your life"/>
</div>
<div class="lyrico-lyrics-wrapper">you better run
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="you better run"/>
</div>
<div class="lyrico-lyrics-wrapper">you better run
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="you better run"/>
</div>
<div class="lyrico-lyrics-wrapper">its coming to get you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="its coming to get you"/>
</div>
<div class="lyrico-lyrics-wrapper">your better run
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="your better run"/>
</div>
<div class="lyrico-lyrics-wrapper">run for your life
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="run for your life"/>
</div>
<div class="lyrico-lyrics-wrapper">run for your life
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="run for your life"/>
</div>
<div class="lyrico-lyrics-wrapper">you better go go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="you better go go"/>
</div>
<div class="lyrico-lyrics-wrapper">run for your life
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="run for your life"/>
</div>
<div class="lyrico-lyrics-wrapper">you better run
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="you better run"/>
</div>
<div class="lyrico-lyrics-wrapper">you better run
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="you better run"/>
</div>
<div class="lyrico-lyrics-wrapper">its coming to get you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="its coming to get you"/>
</div>
<div class="lyrico-lyrics-wrapper">you better run
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="you better run"/>
</div>
<div class="lyrico-lyrics-wrapper">run for your life
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="run for your life"/>
</div>
<div class="lyrico-lyrics-wrapper">you better run
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="you better run"/>
</div>
<div class="lyrico-lyrics-wrapper">its coming to get you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="its coming to get you"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ennamma en 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennamma en "/>
</div>
<div class="lyrico-lyrics-wrapper">mohini neyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mohini neyum"/>
</div>
<div class="lyrico-lyrics-wrapper">endha sopu pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="endha sopu pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">konjam kulikira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konjam kulikira"/>
</div>
<div class="lyrico-lyrics-wrapper">vellaiya  mogam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vellaiya  mogam"/>
</div>
<div class="lyrico-lyrics-wrapper">vechu irukiraye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vechu irukiraye"/>
</div>
<div class="lyrico-lyrics-wrapper">neyum kutty kura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neyum kutty kura"/>
</div>
<div class="lyrico-lyrics-wrapper">powder pottuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="powder pottuda"/>
</div>
<div class="lyrico-lyrics-wrapper">minukkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="minukkura"/>
</div>
<div class="lyrico-lyrics-wrapper">nooru varusam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nooru varusam"/>
</div>
<div class="lyrico-lyrics-wrapper">aana kuda young
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aana kuda young"/>
</div>
<div class="lyrico-lyrics-wrapper">noodles pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="noodles pola"/>
</div>
<div class="lyrico-lyrics-wrapper">nilamaana tongue
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nilamaana tongue"/>
</div>
<div class="lyrico-lyrics-wrapper">peye unaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="peye unaku"/>
</div>
<div class="lyrico-lyrics-wrapper">odha poren sangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="odha poren sangu"/>
</div>
<div class="lyrico-lyrics-wrapper">unna konna naathandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna konna naathandi"/>
</div>
<div class="lyrico-lyrics-wrapper">sudukatu king
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sudukatu king"/>
</div>
<div class="lyrico-lyrics-wrapper">adiye peye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adiye peye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">run for your life
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="run for your life"/>
</div>
<div class="lyrico-lyrics-wrapper">you better run
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="you better run"/>
</div>
<div class="lyrico-lyrics-wrapper">you better run
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="you better run"/>
</div>
<div class="lyrico-lyrics-wrapper">its coming to get you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="its coming to get you"/>
</div>
<div class="lyrico-lyrics-wrapper">your better run
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="your better run"/>
</div>
<div class="lyrico-lyrics-wrapper">run for your life
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="run for your life"/>
</div>
<div class="lyrico-lyrics-wrapper">run for your life
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="run for your life"/>
</div>
<div class="lyrico-lyrics-wrapper">you better go go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="you better go go"/>
</div>
<div class="lyrico-lyrics-wrapper">run for your life
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="run for your life"/>
</div>
<div class="lyrico-lyrics-wrapper">you better run
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="you better run"/>
</div>
<div class="lyrico-lyrics-wrapper">you better run
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="you better run"/>
</div>
<div class="lyrico-lyrics-wrapper">its coming to get you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="its coming to get you"/>
</div>
<div class="lyrico-lyrics-wrapper">you better run
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="you better run"/>
</div>
<div class="lyrico-lyrics-wrapper">run for your life
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="run for your life"/>
</div>
<div class="lyrico-lyrics-wrapper">you better run
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="you better run"/>
</div>
<div class="lyrico-lyrics-wrapper">its coming to get you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="its coming to get you"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vaada vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaada vaada"/>
</div>
<div class="lyrico-lyrics-wrapper">panni munji vaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="panni munji vaya"/>
</div>
</pre>
