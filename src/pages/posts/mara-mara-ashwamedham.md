---
title: "mara mara song lyrics"
album: "Ashwamedham"
artist: "Charan Arjun"
lyricist: "Charan Arjun"
director: "Nitin Gawde"
path: "/albums/ashwamedham-lyrics"
song: "Mara Mara"
image: ../../images/albumart/ashwamedham.jpg
date: 2019-12-06
lang: telugu
youtubeLink: "https://www.youtube.com/embed/NT48NFvgerE"
type: "happy"
singers:
  - Mounika Reddy
  - Charan Arjun
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">maara maara maara 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maara maara maara "/>
</div>
<div class="lyrico-lyrics-wrapper">maaraama raakumaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maaraama raakumaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">jaraa nammaraa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jaraa nammaraa "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">maara maara maara 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maara maara maara "/>
</div>
<div class="lyrico-lyrics-wrapper">maaraama raakumaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maaraama raakumaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">jaraa nammaraa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jaraa nammaraa "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">neeku kondi manasaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeku kondi manasaara"/>
</div>
<div class="lyrico-lyrics-wrapper">neemundundi dhekhoraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neemundundi dhekhoraa"/>
</div>
<div class="lyrico-lyrics-wrapper">genuine bommaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="genuine bommaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">lilli puvvuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lilli puvvuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">chilli navvuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chilli navvuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">belli danceu cheyagara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="belli danceu cheyagara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">lalli kaadhura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lalli kaadhura"/>
</div>
<div class="lyrico-lyrics-wrapper">gilli chudaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gilli chudaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">kallo lenu kallamundhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kallo lenu kallamundhe"/>
</div>
<div class="lyrico-lyrics-wrapper">vunnaraa rai rai rai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vunnaraa rai rai rai"/>
</div>
<div class="lyrico-lyrics-wrapper">nenu railunuroy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenu railunuroy"/>
</div>
<div class="lyrico-lyrics-wrapper">railunuroy railunuroy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="railunuroy railunuroy"/>
</div>
<div class="lyrico-lyrics-wrapper">nuvve rai rai rai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvve rai rai rai"/>
</div>
<div class="lyrico-lyrics-wrapper">neeperu naapai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeperu naapai"/>
</div>
<div class="lyrico-lyrics-wrapper">whole and sole ku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="whole and sole ku"/>
</div>
<div class="lyrico-lyrics-wrapper">ownaruvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ownaruvai"/>
</div>
<div class="lyrico-lyrics-wrapper">non stop ga nanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="non stop ga nanne"/>
</div>
<div class="lyrico-lyrics-wrapper">nadapaaloy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nadapaaloy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">maara maara maara 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maara maara maara "/>
</div>
<div class="lyrico-lyrics-wrapper">maaraama raakumaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maaraama raakumaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">jaraa nammaraa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jaraa nammaraa "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">neeku kondi manasaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeku kondi manasaara"/>
</div>
<div class="lyrico-lyrics-wrapper">neemundundi dhekhoraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neemundundi dhekhoraa"/>
</div>
<div class="lyrico-lyrics-wrapper">genuine bommaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="genuine bommaraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sundhara sikkindhara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sundhara sikkindhara"/>
</div>
<div class="lyrico-lyrics-wrapper">andham neeke sikkindiraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="andham neeke sikkindiraa"/>
</div>
<div class="lyrico-lyrics-wrapper">aadaraa nuv padaraa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aadaraa nuv padaraa "/>
</div>
<div class="lyrico-lyrics-wrapper">nee okkadike hakkundraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee okkadike hakkundraa"/>
</div>
<div class="lyrico-lyrics-wrapper">mundaraa munumundaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mundaraa munumundaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">emavuno anthaa mayaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="emavuno anthaa mayaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">unnadi oka zindagi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnadi oka zindagi "/>
</div>
<div class="lyrico-lyrics-wrapper">teevrangaa nuv jeevinchara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="teevrangaa nuv jeevinchara"/>
</div>
<div class="lyrico-lyrics-wrapper">jodhaa akbaru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jodhaa akbaru "/>
</div>
<div class="lyrico-lyrics-wrapper">nenu nuvvuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenu nuvvuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">inko history
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inko history"/>
</div>
<div class="lyrico-lyrics-wrapper">printavvagaa jatavvara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="printavvagaa jatavvara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">rai rai rai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rai rai rai"/>
</div>
<div class="lyrico-lyrics-wrapper">nenu railunuroy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenu railunuroy"/>
</div>
<div class="lyrico-lyrics-wrapper">railunuroy railunuroy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="railunuroy railunuroy"/>
</div>
<div class="lyrico-lyrics-wrapper">nuvve rai rai rai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvve rai rai rai"/>
</div>
<div class="lyrico-lyrics-wrapper">neeperu naapai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeperu naapai"/>
</div>
<div class="lyrico-lyrics-wrapper">whole and sole ku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="whole and sole ku"/>
</div>
<div class="lyrico-lyrics-wrapper">ownaruvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ownaruvai"/>
</div>
<div class="lyrico-lyrics-wrapper">non stop ga nanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="non stop ga nanne"/>
</div>
<div class="lyrico-lyrics-wrapper">nadapaaloy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nadapaaloy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">baavaraa dil brovaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="baavaraa dil brovaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">heavenante enno ledhura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="heavenante enno ledhura"/>
</div>
<div class="lyrico-lyrics-wrapper">vundiraa nee mundhara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vundiraa nee mundhara"/>
</div>
<div class="lyrico-lyrics-wrapper">jendaa neeke upindiraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jendaa neeke upindiraa"/>
</div>
<div class="lyrico-lyrics-wrapper">antharaa chumantharaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antharaa chumantharaa"/>
</div>
<div class="lyrico-lyrics-wrapper">endhey pillaa nee gaabharaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="endhey pillaa nee gaabharaa"/>
</div>
<div class="lyrico-lyrics-wrapper">chelladhy naamundaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chelladhy naamundaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">mana dhantha vere jathara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mana dhantha vere jathara"/>
</div>
<div class="lyrico-lyrics-wrapper">lusty nenuraa listey chudara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lusty nenuraa listey chudara"/>
</div>
<div class="lyrico-lyrics-wrapper">testu drivuke nuvu raadora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="testu drivuke nuvu raadora"/>
</div>
<div class="lyrico-lyrics-wrapper">padara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="padara"/>
</div>
<div class="lyrico-lyrics-wrapper">entaa thondaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="entaa thondaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">pove mandaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pove mandaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">justu ippude modalinadey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="justu ippude modalinadey"/>
</div>
<div class="lyrico-lyrics-wrapper">o new eraa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="o new eraa "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">rai rai rai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rai rai rai"/>
</div>
<div class="lyrico-lyrics-wrapper">nenu railunuroy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenu railunuroy"/>
</div>
<div class="lyrico-lyrics-wrapper">railunuroy railunuroy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="railunuroy railunuroy"/>
</div>
<div class="lyrico-lyrics-wrapper">nuvve rai rai rai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvve rai rai rai"/>
</div>
<div class="lyrico-lyrics-wrapper">neeperu naapai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeperu naapai"/>
</div>
<div class="lyrico-lyrics-wrapper">whole and sole ku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="whole and sole ku"/>
</div>
<div class="lyrico-lyrics-wrapper">ownaruvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ownaruvai"/>
</div>
<div class="lyrico-lyrics-wrapper">non stop ga nanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="non stop ga nanne"/>
</div>
<div class="lyrico-lyrics-wrapper">nadapaaloy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nadapaaloy"/>
</div>
</pre>
