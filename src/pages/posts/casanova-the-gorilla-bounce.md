---
title: "casanova song lyrics"
album: "The Gorilla Bounce"
artist: "King"
lyricist: "King"
director: "Yash Chhabra"
path: "/albums/the-gorilla-bounce-lyrics"
song: "Casanova"
image: ../../images/albumart/the-gorilla-bounce.jpg
date: 2021-05-21
lang: hindi
youtubeLink: "https://www.youtube.com/embed/wUNtsTrlHaA"
type: "Love"
singers:
  - King
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">You already know what it is
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You already know what it is"/>
</div>
<div class="lyrico-lyrics-wrapper">King!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="King!"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kabhi phir na milein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabhi phir na milein"/>
</div>
<div class="lyrico-lyrics-wrapper">Na aap shikwa karein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na aap shikwa karein"/>
</div>
<div class="lyrico-lyrics-wrapper">Zamana bole casanova
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zamana bole casanova"/>
</div>
<div class="lyrico-lyrics-wrapper">Ab jaana hum kya karein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ab jaana hum kya karein"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kabhi phir na milein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabhi phir na milein"/>
</div>
<div class="lyrico-lyrics-wrapper">Na aap shikwa karein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na aap shikwa karein"/>
</div>
<div class="lyrico-lyrics-wrapper">Zamana bole casanova
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zamana bole casanova"/>
</div>
<div class="lyrico-lyrics-wrapper">Ab jaana hum kya karein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ab jaana hum kya karein"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">I never break a heart
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I never break a heart"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeh cheezein mujhpe nahi jachti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh cheezein mujhpe nahi jachti"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh baby I never play that part
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh baby I never play that part"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu kya hi nikalegi galti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu kya hi nikalegi galti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Main tere saamne phir tere peeche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main tere saamne phir tere peeche"/>
</div>
<div class="lyrico-lyrics-wrapper">Kyun ilzamon ki kashti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kyun ilzamon ki kashti"/>
</div>
<div class="lyrico-lyrics-wrapper">Main kyun hi churaunga jeetne mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main kyun hi churaunga jeetne mein"/>
</div>
<div class="lyrico-lyrics-wrapper">Jab der nahi lagti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jab der nahi lagti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kehti dil yeh mera tha seene mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kehti dil yeh mera tha seene mein"/>
</div>
<div class="lyrico-lyrics-wrapper">Jab raat ko soyi thi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jab raat ko soyi thi"/>
</div>
<div class="lyrico-lyrics-wrapper">Shayad tu hi hai woh jiski wajah se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shayad tu hi hai woh jiski wajah se"/>
</div>
<div class="lyrico-lyrics-wrapper">Baad mein royi thi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baad mein royi thi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pata hi na chala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pata hi na chala"/>
</div>
<div class="lyrico-lyrics-wrapper">Tujhse pyaar ho gaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tujhse pyaar ho gaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Darwaza dekhti rahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Darwaza dekhti rahi"/>
</div>
<div class="lyrico-lyrics-wrapper">Intezar ho gaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intezar ho gaya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kabhi phir na milein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabhi phir na milein"/>
</div>
<div class="lyrico-lyrics-wrapper">Na aap shikwa karein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na aap shikwa karein"/>
</div>
<div class="lyrico-lyrics-wrapper">Zamana bole casanova
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zamana bole casanova"/>
</div>
<div class="lyrico-lyrics-wrapper">Ab jana hum kya karein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ab jana hum kya karein"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tu hi kehti thi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu hi kehti thi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pyaar na karenge hum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pyaar na karenge hum"/>
</div>
<div class="lyrico-lyrics-wrapper">Maine bola tha kho dunga tujhko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maine bola tha kho dunga tujhko"/>
</div>
<div class="lyrico-lyrics-wrapper">Toh dhoondhega tera sanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Toh dhoondhega tera sanam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Teri galti hui aise bichhde hum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teri galti hui aise bichhde hum"/>
</div>
<div class="lyrico-lyrics-wrapper">Tune khel ke tode usool
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tune khel ke tode usool"/>
</div>
<div class="lyrico-lyrics-wrapper">Ab na rakhna koyi bharam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ab na rakhna koyi bharam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">In honthon mein band batein teri meri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="In honthon mein band batein teri meri"/>
</div>
<div class="lyrico-lyrics-wrapper">Raz na kahin khul jaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raz na kahin khul jaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Kisi raste milo tum kabhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kisi raste milo tum kabhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ik dooje ko dekhke hass jaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ik dooje ko dekhke hass jaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aur guzro tum samne aage ko badh jayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aur guzro tum samne aage ko badh jayo"/>
</div>
<div class="lyrico-lyrics-wrapper">Peeche phir mudna nahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peeche phir mudna nahi"/>
</div>
<div class="lyrico-lyrics-wrapper">Zamana yeh padh lega nazron ki batein humari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zamana yeh padh lega nazron ki batein humari"/>
</div>
<div class="lyrico-lyrics-wrapper">Main tujhse bas kehna yahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main tujhse bas kehna yahi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jo kabhi hum na milein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jo kabhi hum na milein"/>
</div>
<div class="lyrico-lyrics-wrapper">Na aap shikwa karein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na aap shikwa karein"/>
</div>
<div class="lyrico-lyrics-wrapper">Zamana bole casanova
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zamana bole casanova"/>
</div>
<div class="lyrico-lyrics-wrapper">Ab jana hum kya karein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ab jana hum kya karein"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kabhi phir na milein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabhi phir na milein"/>
</div>
<div class="lyrico-lyrics-wrapper">Na aap shikwa karein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na aap shikwa karein"/>
</div>
<div class="lyrico-lyrics-wrapper">Zamana bole casanova
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zamana bole casanova"/>
</div>
<div class="lyrico-lyrics-wrapper">Ab jana hum kya karein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ab jana hum kya karein"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Casanova, Casanova
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Casanova, Casanova"/>
</div>
</pre>
