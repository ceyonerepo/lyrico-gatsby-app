---
title: "itho intha nenjodu song lyrics"
album: "Good Luck"
artist: "Manoj Bhatnaghar"
lyricist: "Vairamuthu"
director: "Manoj Bhatnaghar"
path: "/albums/good-luck-song-lyrics"
song: "Itho Intha Nenjodu"
image: ../../images/albumart/good-luck.jpg
date: 2000-02-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/LRj7Xb_cQ1U"
type: "melody"
singers:
  - S.P. Balasubrahmanyam
  - Chitra
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Dhum thaannana dhum thaannana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhum thaannana dhum thaannana"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhum thaannana dhum thaannana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhum thaannana dhum thaannana"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhum thaannana dhum thaannana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhum thaannana dhum thaannana"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhum thaannana dhum thaannana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhum thaannana dhum thaannana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idho intha nenjodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idho intha nenjodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Orey veeduthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orey veeduthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Orey veedu ullathellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orey veedu ullathellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee vaazhaththaan……
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee vaazhaththaan……"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir theeyil deepam yaetrinen…..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir theeyil deepam yaetrinen….."/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai kondru ennai oottrinen….
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai kondru ennai oottrinen…."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idho intha nenjodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idho intha nenjodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Orey veeduthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orey veeduthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Orey veedu ullathellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orey veedu ullathellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee vaazhaththaan……
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee vaazhaththaan……"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir theeyil deepam yaetrinen…..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir theeyil deepam yaetrinen….."/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai kondru ennai oottrinen….
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai kondru ennai oottrinen…."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyirai pirithu iru paadhi seithen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirai pirithu iru paadhi seithen"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakku adhile sari paadhi thanthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakku adhile sari paadhi thanthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Unadhu uyiril sari paadhi konden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unadhu uyiril sari paadhi konden"/>
</div>
<div class="lyrico-lyrics-wrapper">Enadhu uyiril muzhumaiyum thandhen…..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enadhu uyiril muzhumaiyum thandhen….."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ovvondrum mudiyum endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ovvondrum mudiyum endru"/>
</div>
<div class="lyrico-lyrics-wrapper">Vingyaanam sollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vingyaanam sollum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nam kadhal mudiyaadhendru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam kadhal mudiyaadhendru"/>
</div>
<div class="lyrico-lyrics-wrapper">En niyaanam sollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En niyaanam sollum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalangal vaazhum kaalame….ae…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalangal vaazhum kaalame….ae…"/>
</div>
<div class="lyrico-lyrics-wrapper">Nam kadhal ellai vaazhume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam kadhal ellai vaazhume"/>
</div>
<div class="lyrico-lyrics-wrapper">En kaigal unnil neelumae…..ae….
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kaigal unnil neelumae…..ae…."/>
</div>
<div class="lyrico-lyrics-wrapper">Imaikkaamal unnai aalume……ae…..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imaikkaamal unnai aalume……ae….."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ka ma pa ka ma ni sa ri riririr
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ka ma pa ka ma ni sa ri riririr"/>
</div>
<div class="lyrico-lyrics-wrapper">Ka ma pa ka ma ni sa ni sa sa sa sa sa saa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ka ma pa ka ma ni sa ni sa sa sa sa sa saa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ka ma pa ka ma ni sa ri riririr
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ka ma pa ka ma ni sa ri riririr"/>
</div>
<div class="lyrico-lyrics-wrapper">Ka ma pa ka ma ni sa ni sa sa sa sa sa saa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ka ma pa ka ma ni sa ni sa sa sa sa sa saa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pa sa ni ririri….
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pa sa ni ririri…."/>
</div>
<div class="lyrico-lyrics-wrapper">Sa ri sa kakaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sa ri sa kakaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Sa ri sa saa ni tha pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sa ri sa saa ni tha pa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ma pa ka ma ri ga sa ri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ma pa ka ma ri ga sa ri"/>
</div>
<div class="lyrico-lyrics-wrapper">Ma pa ka ma ri ga sa ri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ma pa ka ma ri ga sa ri"/>
</div>
<div class="lyrico-lyrics-wrapper">Ka ma ni tha pa ni ka ri saa….
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ka ma ni tha pa ni ka ri saa…."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhayam thirandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam thirandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Poondhottam seithai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poondhottam seithai"/>
</div>
<div class="lyrico-lyrics-wrapper">Inimel enakku ennenna seivaai….
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inimel enakku ennenna seivaai…."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nilavai pariththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilavai pariththu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un koonthal mudippen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un koonthal mudippen"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinmeen pariththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinmeen pariththu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maniyaaram thoduppaen….
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maniyaaram thoduppaen…."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaeredhum vendaam vendaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaeredhum vendaam vendaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Un maarbu podhum…..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un maarbu podhum….."/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir pogum kaalam kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir pogum kaalam kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru paarvai podhum.
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru paarvai podhum."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un sollil vedham ketkkiren….
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un sollil vedham ketkkiren…."/>
</div>
<div class="lyrico-lyrics-wrapper">Un kannil ennai paarkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kannil ennai paarkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnodu ennai serkkiren.
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnodu ennai serkkiren."/>
</div>
<div class="lyrico-lyrics-wrapper">Un maarbil kannam theikkiren.
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un maarbil kannam theikkiren."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idho intha nenjodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idho intha nenjodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Orey veeduthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orey veeduthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Orey veedu ullathellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orey veedu ullathellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee vaazhaththaan……
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee vaazhaththaan……"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir theeyil deepam yaetrinen…..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir theeyil deepam yaetrinen….."/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai kondru ennai oottrinen….
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai kondru ennai oottrinen…."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhum thaannana dhum thaannana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhum thaannana dhum thaannana"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhum thaannana dhum thaannana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhum thaannana dhum thaannana"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhum thaannana dhum thaannana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhum thaannana dhum thaannana"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhum thaannana dhum thaannana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhum thaannana dhum thaannana"/>
</div>
</pre>
