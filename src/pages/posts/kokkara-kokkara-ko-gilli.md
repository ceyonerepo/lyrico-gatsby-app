---
title: "kokkara kokkara ko song lyrics"
album: "Gilli"
artist: "Vidyasagar"
lyricist: "Yugabharathi"
director: "Dharani"
path: "/albums/gilli-lyrics"
song: "Kokkara Kokkara Ko"
image: ../../images/albumart/gilli.jpg
date: 2004-04-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/rXorBw_EP88"
type: "happy"
singers:
  - Udit Narayan
  - Sujatha Mohan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Thum Shak Thum Thum Thum Shak
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thum Shak Thum Thum Thum Shak"/>
</div>
<div class="lyrico-lyrics-wrapper">Thum Shak Thum Thum Thum Shak
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thum Shak Thum Thum Thum Shak"/>
</div>
<div class="lyrico-lyrics-wrapper">Hei Hei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei Hei"/>
</div>
<div class="lyrico-lyrics-wrapper">Thum Shak Thum Thum Thum Shak
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thum Shak Thum Thum Thum Shak"/>
</div>
<div class="lyrico-lyrics-wrapper">Thum Shak Thum Thum Thum Shak
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thum Shak Thum Thum Thum Shak"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thum Shak Thum Thum Thum Shak
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thum Shak Thum Thum Thum Shak"/>
</div>
<div class="lyrico-lyrics-wrapper">Thum Shak Thum Thum Thum Shak Heiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thum Shak Thum Thum Thum Shak Heiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kokkara Kokkara Koo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kokkara Kokkara Koo"/>
</div>
<div class="lyrico-lyrics-wrapper">Aye Vidiya Kokkara Koo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aye Vidiya Kokkara Koo"/>
</div>
<div class="lyrico-lyrics-wrapper">Irundha Iruttellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irundha Iruttellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini Mele Kokkara Koo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Mele Kokkara Koo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kokkara Kokkara Koo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kokkara Kokkara Koo"/>
</div>
<div class="lyrico-lyrics-wrapper">Seval Kokkara Koo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seval Kokkara Koo"/>
</div>
<div class="lyrico-lyrics-wrapper">Seval Koova Kulle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seval Koova Kulle"/>
</div>
<div class="lyrico-lyrics-wrapper">Pettai Kozhi Kokkara Koo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pettai Kozhi Kokkara Koo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sangu Sakkaram Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sangu Sakkaram Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasu Suththura Velai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu Suththura Velai"/>
</div>
<div class="lyrico-lyrics-wrapper">Suranganika Maalu Genaavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suranganika Maalu Genaavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Atho Paaru Vaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atho Paaru Vaanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuni Thuvaikkuthu Megam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuni Thuvaikkuthu Megam"/>
</div>
<div class="lyrico-lyrics-wrapper">Velagi Poguthu Sogam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velagi Poguthu Sogam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kokkara Kokkara Koo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kokkara Kokkara Koo"/>
</div>
<div class="lyrico-lyrics-wrapper">Aye Vidiya Kokkara Koo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aye Vidiya Kokkara Koo"/>
</div>
<div class="lyrico-lyrics-wrapper">Irundha Iruttellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irundha Iruttellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini Mele Kokkara Koo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Mele Kokkara Koo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Le Le Ulele Ule Ule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Le Le Ulele Ule Ule"/>
</div>
<div class="lyrico-lyrics-wrapper">Le Le Kulle Kulle Ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Le Le Kulle Kulle Ma"/>
</div>
<div class="lyrico-lyrics-wrapper">Le Le Ulele Ule Ule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Le Le Ulele Ule Ule"/>
</div>
<div class="lyrico-lyrics-wrapper">Le Le Kulle Kulle Ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Le Le Kulle Kulle Ma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vellimani Kolusukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vellimani Kolusukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Thullugira Manasukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thullugira Manasukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Santhosam Nelachirukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Santhosam Nelachirukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Saamikitta Kettirukken
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saamikitta Kettirukken"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thum Shak
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thum Shak"/>
</div>
<div class="lyrico-lyrics-wrapper">Thum Thum Shak Hey Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thum Thum Shak Hey Hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ellorum Arugirukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellorum Arugirukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Pollaapu Vilagirukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pollaapu Vilagirukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbaana Ungakitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbaana Ungakitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Aandavanai Paathirukken
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aandavanai Paathirukken"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennam Irundha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennam Irundha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethuvum Nadakkum Thannaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethuvum Nadakkum Thannaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Aye Nee Thuninja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aye Nee Thuninja"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagam Unakku Pinnaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagam Unakku Pinnaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuththuvilakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuththuvilakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Siricha Siricha Thappedhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siricha Siricha Thappedhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kollayadichan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kollayadichan"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasa Manasa Ippodhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasa Manasa Ippodhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Namma Pakkam Kaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma Pakkam Kaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Veesuratha Paathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veesuratha Paathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nallavangalai Serthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nallavangalai Serthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Podu Dhinam Koothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Podu Dhinam Koothu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thumshak Thumshak Thumshak
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thumshak Thumshak Thumshak"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kokkara Kokkara Koo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kokkara Kokkara Koo"/>
</div>
<div class="lyrico-lyrics-wrapper">Aye Vidiya Kokkara Koo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aye Vidiya Kokkara Koo"/>
</div>
<div class="lyrico-lyrics-wrapper">Irundha Iruttellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irundha Iruttellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini Mele Kokkara Koo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Mele Kokkara Koo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hei Hei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei Hei"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Hei Hei Heiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei Hei Heiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kandhanukku Valliya Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandhanukku Valliya Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannanukku Raadhaya Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannanukku Raadhaya Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasaikonda Uyirukellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasaikonda Uyirukellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunaiyirukku Bhoomiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunaiyirukku Bhoomiyila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thum Shak
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thum Shak"/>
</div>
<div class="lyrico-lyrics-wrapper">Thum Thum Thum Shak Hey Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thum Thum Thum Shak Hey Hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannukulla Kannan Irukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannukulla Kannan Irukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjukulla Nenappirukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjukulla Nenappirukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarukulla Yaaru Irukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarukulla Yaaru Irukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Therinjavanga Yaarumillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Therinjavanga Yaarumillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rekkai Katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rekkai Katti"/>
</div>
<div class="lyrico-lyrics-wrapper">Parakkum Parakkum Vellaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parakkum Parakkum Vellaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vekka Pattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vekka Pattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maraikum Maraikum Nenjodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maraikum Maraikum Nenjodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Sittukuruvi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Sittukuruvi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirikkum Sirikkum Kannodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirikkum Sirikkum Kannodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottum Aruvi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottum Aruvi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudhikkum Kudhikkum Ennodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudhikkum Kudhikkum Ennodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chittan Chittaan Sidukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chittan Chittaan Sidukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippa Ullathellaam Namakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippa Ullathellaam Namakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Kettathathaan Odhukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kettathathaan Odhukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini Namma Kitta Kezhakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Namma Kitta Kezhakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kokkara Kokkara Koo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kokkara Kokkara Koo"/>
</div>
<div class="lyrico-lyrics-wrapper">Aye Vidiya Kokkara Koo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aye Vidiya Kokkara Koo"/>
</div>
<div class="lyrico-lyrics-wrapper">Irundha Iruttellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irundha Iruttellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini Mele Kokkara Koo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Mele Kokkara Koo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hei Hei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei Hei"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kokkara Kokkara Koo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kokkara Kokkara Koo"/>
</div>
<div class="lyrico-lyrics-wrapper">Seval Kokkara Koo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seval Kokkara Koo"/>
</div>
<div class="lyrico-lyrics-wrapper">Seval Koova Kulle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seval Koova Kulle"/>
</div>
<div class="lyrico-lyrics-wrapper">Pettai Kozhi Kokkara Koo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pettai Kozhi Kokkara Koo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hei Hei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei Hei"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sangu Sakkaram Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sangu Sakkaram Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasu Suththura Velai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu Suththura Velai"/>
</div>
<div class="lyrico-lyrics-wrapper">Suranganika Maalu Genaavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suranganika Maalu Genaavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Atho Paaru Vaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atho Paaru Vaanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuni Thuvaikkuthu Megam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuni Thuvaikkuthu Megam"/>
</div>
<div class="lyrico-lyrics-wrapper">Velagi Poguthu Sogam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velagi Poguthu Sogam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kokkara Kokkara Koo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kokkara Kokkara Koo"/>
</div>
<div class="lyrico-lyrics-wrapper">Aye Vidiya Kokkara Koo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aye Vidiya Kokkara Koo"/>
</div>
<div class="lyrico-lyrics-wrapper">Irundha Iruttellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irundha Iruttellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini Mele Kokkara Koo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Mele Kokkara Koo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kokkara Kokkara Koo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kokkara Kokkara Koo"/>
</div>
<div class="lyrico-lyrics-wrapper">Seval Kokkara Koo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seval Kokkara Koo"/>
</div>
<div class="lyrico-lyrics-wrapper">Seval Koova Kulle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seval Koova Kulle"/>
</div>
<div class="lyrico-lyrics-wrapper">Pettai Kozhi Kokkara Koo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pettai Kozhi Kokkara Koo"/>
</div>
</pre>
