---
title: "naanthaan kathiru song lyrics"
album: "Sarkarai Thookalai Oru Punnagai"
artist: "Rajesh Appukuttan"
lyricist: "Kattalai Jeya"
director: "Mahesh Padmanabhan "
path: "/albums/sarkarai-thookalai-oru-punnagai-lyrics"
song: "Naanthaan Kathiru"
image: ../../images/albumart/sarkarai-thookalai-oru-punnagai.jpg
date: 2021-12-31
lang: tamil
youtubeLink: "https://www.youtube.com/embed/3_eN3BYa5AM"
type: "mass"
singers:
  - unknown
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">kathiru kathiru kathiru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathiru kathiru kathiru"/>
</div>
<div class="lyrico-lyrics-wrapper">kathiru kathiru kathiru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathiru kathiru kathiru"/>
</div>
<div class="lyrico-lyrics-wrapper">kathiru kathiru kathiru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathiru kathiru kathiru"/>
</div>
<div class="lyrico-lyrics-wrapper">kathiru kathiru kathiru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathiru kathiru kathiru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">naan thaan kathiru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan thaan kathiru"/>
</div>
<div class="lyrico-lyrics-wrapper">naan thaan kathiru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan thaan kathiru"/>
</div>
<div class="lyrico-lyrics-wrapper">naan thaan kathiru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan thaan kathiru"/>
</div>
<div class="lyrico-lyrics-wrapper">naan thaan kathiru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan thaan kathiru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">naan thaan kathiru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan thaan kathiru"/>
</div>
<div class="lyrico-lyrics-wrapper">naan thaan kathiru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan thaan kathiru"/>
</div>
<div class="lyrico-lyrics-wrapper">naan thaan kathiru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan thaan kathiru"/>
</div>
<div class="lyrico-lyrics-wrapper">naan thaan kathiru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan thaan kathiru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aatam pottu paada vanthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aatam pottu paada vanthen"/>
</div>
<div class="lyrico-lyrics-wrapper">naan thaan kathiru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan thaan kathiru"/>
</div>
<div class="lyrico-lyrics-wrapper">valathu kaalai eduthu vachen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valathu kaalai eduthu vachen"/>
</div>
<div class="lyrico-lyrics-wrapper">naan thaan kathiru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan thaan kathiru"/>
</div>
<div class="lyrico-lyrics-wrapper">othu nee othu ada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="othu nee othu ada"/>
</div>
<div class="lyrico-lyrics-wrapper">naan thaan kathiru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan thaan kathiru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">naan thaan kathiru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan thaan kathiru"/>
</div>
<div class="lyrico-lyrics-wrapper">naan thaan kathiru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan thaan kathiru"/>
</div>
<div class="lyrico-lyrics-wrapper">naan thaan kathiru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan thaan kathiru"/>
</div>
<div class="lyrico-lyrics-wrapper">naan thaan kathiru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan thaan kathiru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">azhaga sirippu arivo sirappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="azhaga sirippu arivo sirappu"/>
</div>
<div class="lyrico-lyrics-wrapper">ethilum poruppu kettikaaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethilum poruppu kettikaaran"/>
</div>
<div class="lyrico-lyrics-wrapper">naan than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan than"/>
</div>
<div class="lyrico-lyrics-wrapper">ilamai thudikum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ilamai thudikum "/>
</div>
<div class="lyrico-lyrics-wrapper">ithayam vedikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithayam vedikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">ilasai pidikkum machakaaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ilasai pidikkum machakaaran"/>
</div>
<div class="lyrico-lyrics-wrapper">naan than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan than"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">anbaa vanthavanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anbaa vanthavanai"/>
</div>
<div class="lyrico-lyrics-wrapper">panbaa kumbiduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="panbaa kumbiduven"/>
</div>
<div class="lyrico-lyrics-wrapper">vambu senchavanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vambu senchavanai"/>
</div>
<div class="lyrico-lyrics-wrapper">kummu kummiduven oy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kummu kummiduven oy"/>
</div>
<div class="lyrico-lyrics-wrapper">thappa kandathume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thappa kandathume"/>
</div>
<div class="lyrico-lyrics-wrapper">pongu pongiduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pongu pongiduven"/>
</div>
<div class="lyrico-lyrics-wrapper">uyaraana katrorai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyaraana katrorai"/>
</div>
<div class="lyrico-lyrics-wrapper">panivodu ullathail
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="panivodu ullathail"/>
</div>
<div class="lyrico-lyrics-wrapper">uyiraaga naan mathipen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyiraaga naan mathipen"/>
</div>
<div class="lyrico-lyrics-wrapper">vayathaana petrorai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vayathaana petrorai"/>
</div>
<div class="lyrico-lyrics-wrapper">muthiyorin illathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muthiyorin illathil"/>
</div>
<div class="lyrico-lyrics-wrapper">vittorai naan mithipen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vittorai naan mithipen"/>
</div>
<div class="lyrico-lyrics-wrapper">hey hey oh ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey hey oh ooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">naan thaan kathiru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan thaan kathiru"/>
</div>
<div class="lyrico-lyrics-wrapper">naan thaan kathiru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan thaan kathiru"/>
</div>
<div class="lyrico-lyrics-wrapper">naan thaan kathiru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan thaan kathiru"/>
</div>
<div class="lyrico-lyrics-wrapper">naan thaan kathiru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan thaan kathiru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">emma sunthariye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="emma sunthariye"/>
</div>
<div class="lyrico-lyrics-wrapper">summa suthuriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="summa suthuriye"/>
</div>
<div class="lyrico-lyrics-wrapper">kannal sokkuriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannal sokkuriye"/>
</div>
<div class="lyrico-lyrics-wrapper">kanjaa vakkiriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanjaa vakkiriye"/>
</div>
<div class="lyrico-lyrics-wrapper">allo alluriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="allo alluriye"/>
</div>
<div class="lyrico-lyrics-wrapper">thullo thulluriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thullo thulluriye"/>
</div>
<div class="lyrico-lyrics-wrapper">azhagana malarellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="azhagana malarellam"/>
</div>
<div class="lyrico-lyrics-wrapper">sediyodu irunthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sediyodu irunthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">parikaama naan rasipen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="parikaama naan rasipen"/>
</div>
<div class="lyrico-lyrics-wrapper">enakaana ethirkaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enakaana ethirkaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">iravodu irunthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iravodu irunthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">parakaama naan jeyipen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="parakaama naan jeyipen"/>
</div>
<div class="lyrico-lyrics-wrapper">hey hey oh ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey hey oh ooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nee thaan kathiru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee thaan kathiru"/>
</div>
<div class="lyrico-lyrics-wrapper">nee thaan kathiru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee thaan kathiru"/>
</div>
<div class="lyrico-lyrics-wrapper">nee thaan kathiru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee thaan kathiru"/>
</div>
<div class="lyrico-lyrics-wrapper">nee thaan kathiru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee thaan kathiru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aatam pottu paada vanthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aatam pottu paada vanthen"/>
</div>
<div class="lyrico-lyrics-wrapper">naan thaan kathiru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan thaan kathiru"/>
</div>
<div class="lyrico-lyrics-wrapper">valathu kaalai eduthu vachen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valathu kaalai eduthu vachen"/>
</div>
<div class="lyrico-lyrics-wrapper">naan thaan kathiru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan thaan kathiru"/>
</div>
<div class="lyrico-lyrics-wrapper">othu nee othu ada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="othu nee othu ada"/>
</div>
<div class="lyrico-lyrics-wrapper">naan thaan kathiru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan thaan kathiru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">naan thaan kathiru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan thaan kathiru"/>
</div>
<div class="lyrico-lyrics-wrapper">naan thaan kathiru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan thaan kathiru"/>
</div>
<div class="lyrico-lyrics-wrapper">naan thaan kathiru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan thaan kathiru"/>
</div>
<div class="lyrico-lyrics-wrapper">naan thaan kathiru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan thaan kathiru"/>
</div>
</pre>
