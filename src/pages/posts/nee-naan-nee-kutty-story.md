---
title: "nee naan nee song lyrics"
album: "Kutty Story"
artist: "Karthik"
lyricist: "Madhan Karky"
director: "Gautham Vasudev Menon - Vijay - Venkat Prabhu - Nalan Kumarasamy"
path: "/albums/kutty-story-song-lyrics"
song: "Nee Naan Nee"
image: ../../images/albumart/kutty-story.jpg
date: 2021-02-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/vmDT4KJtWlM"
type: "Love"
singers:
  - Rajaganapathy
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Thee thooral moongil megam maalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thee thooral moongil megam maalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee naan nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee naan nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Panithuli paal paarvai thendral thenir thaagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panithuli paal paarvai thendral thenir thaagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee naan nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee naan nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oonjal oola udal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oonjal oola udal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavu kobam kanneer neethan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavu kobam kanneer neethan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poo poonai punnagai puzh poochi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poo poonai punnagai puzh poochi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee naan nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee naan nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thiraipadam thozh thookkam kaalam katru kaatchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiraipadam thozh thookkam kaalam katru kaatchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee naan nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee naan nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaalam thamizh thedal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaalam thamizh thedal"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadhai paadham paadal nee naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadhai paadham paadal nee naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neelam pulli kanasathuram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelam pulli kanasathuram"/>
</div>
<div class="lyrico-lyrics-wrapper">Varivadivam vaanurthi koombu minkaantham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varivadivam vaanurthi koombu minkaantham"/>
</div>
<div class="lyrico-lyrics-wrapper">Idar mel anmai nee naan nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idar mel anmai nee naan nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kozh saivu thazh thervu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kozh saivu thazh thervu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nizh sorvu nee thirvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nizh sorvu nee thirvu"/>
</div>
<div class="lyrico-lyrics-wrapper">Korvai thirntha varthai oorvalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Korvai thirntha varthai oorvalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nuzh naanal vaanavil vaan vaasal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuzh naanal vaanavil vaan vaasal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee naan nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee naan nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadal kavithai kaaram nermai nesam neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadal kavithai kaaram nermai nesam neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee naan nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee naan nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aadal aasai anbu netru naalai natpu nee naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadal aasai anbu netru naalai natpu nee naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadal aasai anbu netru naalai natpu nee naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadal aasai anbu netru naalai natpu nee naan"/>
</div>
</pre>
