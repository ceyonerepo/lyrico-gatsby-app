---
title: "thealu thealu song lyrics"
album: "Theal"
artist: "C. Sathya"
lyricist: "Kiruthiyaa"
director: "Harikumar"
path: "/albums/theal-lyrics"
song: "Thealu Thealu"
image: ../../images/albumart/theal.jpg
date: 2022-01-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/2kxhpCw4kOI"
type: "mass"
singers:
  - Harikumar Atul
  - Hyde Karty
  - Raghav Jock
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">nenjan kootu mancha sooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjan kootu mancha sooru"/>
</div>
<div class="lyrico-lyrics-wrapper">setharum paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="setharum paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">pacha lattang kai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pacha lattang kai "/>
</div>
<div class="lyrico-lyrics-wrapper">sullan manta egirum paru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sullan manta egirum paru"/>
</div>
<div class="lyrico-lyrics-wrapper">tapaikira tubakoora verukura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tapaikira tubakoora verukura"/>
</div>
<div class="lyrico-lyrics-wrapper">kabalatha thabal nu odikira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kabalatha thabal nu odikira"/>
</div>
<div class="lyrico-lyrics-wrapper">vasthathu usaru karun thealu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vasthathu usaru karun thealu"/>
</div>
<div class="lyrico-lyrics-wrapper">thanthure aaven da karuvadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanthure aaven da karuvadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thealu thealu thealu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thealu thealu thealu "/>
</div>
<div class="lyrico-lyrics-wrapper">therichu vaaya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="therichu vaaya "/>
</div>
<div class="lyrico-lyrics-wrapper">therichu oodu ooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="therichu oodu ooru"/>
</div>
<div class="lyrico-lyrics-wrapper">onna oodu poi seru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="onna oodu poi seru"/>
</div>
<div class="lyrico-lyrics-wrapper">polapa kilichu kilichu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="polapa kilichu kilichu "/>
</div>
<div class="lyrico-lyrics-wrapper">kilichu pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kilichu pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">moochu mele paru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moochu mele paru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thealu thealu thealu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thealu thealu thealu "/>
</div>
<div class="lyrico-lyrics-wrapper">therichu vaaya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="therichu vaaya "/>
</div>
<div class="lyrico-lyrics-wrapper">therichu oodu ooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="therichu oodu ooru"/>
</div>
<div class="lyrico-lyrics-wrapper">onna oodu poi seru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="onna oodu poi seru"/>
</div>
<div class="lyrico-lyrics-wrapper">polapa kilichu kilichu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="polapa kilichu kilichu "/>
</div>
<div class="lyrico-lyrics-wrapper">kilichu pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kilichu pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">moochu mele paru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moochu mele paru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">machan eti mithi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="machan eti mithi"/>
</div>
<div class="lyrico-lyrics-wrapper">side ah pirichi thonga vidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="side ah pirichi thonga vidu"/>
</div>
<div class="lyrico-lyrics-wrapper">macha thatti vidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="macha thatti vidu"/>
</div>
<div class="lyrico-lyrics-wrapper">sanga pirichi thonga vidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sanga pirichi thonga vidu"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu ivanoda kotta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu ivanoda kotta"/>
</div>
<div class="lyrico-lyrics-wrapper">muducha uyiroda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muducha uyiroda"/>
</div>
<div class="lyrico-lyrics-wrapper">neeyum oodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeyum oodu"/>
</div>
<div class="lyrico-lyrics-wrapper">varuma ivana pola thillu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varuma ivana pola thillu"/>
</div>
<div class="lyrico-lyrics-wrapper">ivan vanthaka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ivan vanthaka "/>
</div>
<div class="lyrico-lyrics-wrapper">prachananu sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="prachananu sollu"/>
</div>
<div class="lyrico-lyrics-wrapper">nee yaara vena yevana 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee yaara vena yevana "/>
</div>
<div class="lyrico-lyrics-wrapper">vena yethuva vena irunthuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vena yethuva vena irunthuko"/>
</div>
<div class="lyrico-lyrics-wrapper">avan kanna pathu therinjuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avan kanna pathu therinjuko"/>
</div>
<div class="lyrico-lyrics-wrapper">en ratham mella purunjuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en ratham mella purunjuko"/>
</div>
<div class="lyrico-lyrics-wrapper">velanu vantha serum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="velanu vantha serum "/>
</div>
<div class="lyrico-lyrics-wrapper">thagararu nee kelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thagararu nee kelu"/>
</div>
<div class="lyrico-lyrics-wrapper">vambu thumbu vachukatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vambu thumbu vachukatha"/>
</div>
<div class="lyrico-lyrics-wrapper">savadi nee vangikatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="savadi nee vangikatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ivan thealu nee kelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ivan thealu nee kelu"/>
</div>
<div class="lyrico-lyrics-wrapper">adi sema thoolu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi sema thoolu"/>
</div>
<div class="lyrico-lyrics-wrapper">ivan thealu nee kelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ivan thealu nee kelu"/>
</div>
<div class="lyrico-lyrics-wrapper">adi sema thoolu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi sema thoolu"/>
</div>
<div class="lyrico-lyrics-wrapper">intha petaiku nan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha petaiku nan"/>
</div>
<div class="lyrico-lyrics-wrapper">than da durai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="than da durai"/>
</div>
<div class="lyrico-lyrics-wrapper">ivan thealu nee kelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ivan thealu nee kelu"/>
</div>
<div class="lyrico-lyrics-wrapper">adi sema thoolu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi sema thoolu"/>
</div>
<div class="lyrico-lyrics-wrapper">ivan thealu nee kelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ivan thealu nee kelu"/>
</div>
<div class="lyrico-lyrics-wrapper">adi sema thoolu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi sema thoolu"/>
</div>
<div class="lyrico-lyrics-wrapper">ivan thealu nee kelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ivan thealu nee kelu"/>
</div>
<div class="lyrico-lyrics-wrapper">adi sema thoolu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi sema thoolu"/>
</div>
<div class="lyrico-lyrics-wrapper">ivan thealu nee kelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ivan thealu nee kelu"/>
</div>
<div class="lyrico-lyrics-wrapper">adi sema thoolu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi sema thoolu"/>
</div>
</pre>
