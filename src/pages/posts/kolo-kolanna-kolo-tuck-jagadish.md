---
title: "kolo kolanna kolo song lyrics"
album: "Tuck Jagadish"
artist: "S. Thaman"
lyricist: "Sirivennela Seetharama Sastry"
director: "Shiva Nirvana"
path: "/albums/tuck-jagadish-lyrics"
song: "Kolo Kolanna Kolo"
image: ../../images/albumart/tuck-jagadish.jpg
date: 2021-09-10
lang: telugu
youtubeLink: "https://www.youtube.com/embed/2Tq4oQIAEUU"
type: "happy"
singers:
  - Armaan Malik
  - Sri Krishna
  - Harini Ivvaturi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kolo kolanna kolo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolo kolanna kolo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kommalu kila kila navvali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kommalu kila kila navvali"/>
</div>
<div class="lyrico-lyrics-wrapper">Kovello velige jyothulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kovello velige jyothulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallalo koluvundali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallalo koluvundali"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaraaru ruthuvulloni 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraaru ruthuvulloni "/>
</div>
<div class="lyrico-lyrics-wrapper">aakarlenidhi emvundhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aakarlenidhi emvundhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Chudale gaani manne 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chudale gaani manne "/>
</div>
<div class="lyrico-lyrics-wrapper">rangula pudhotavuthundhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rangula pudhotavuthundhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thodai nee venta 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodai nee venta "/>
</div>
<div class="lyrico-lyrics-wrapper">kada dhaaka nenunta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kada dhaaka nenunta"/>
</div>
<div class="lyrico-lyrics-wrapper">Raallaina mullaina 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raallaina mullaina "/>
</div>
<div class="lyrico-lyrics-wrapper">mana adugulu padithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mana adugulu padithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Poolai pongaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poolai pongaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvu dheenanga ye mulo kurchunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvu dheenanga ye mulo kurchunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu ventade dhigule velipothundhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu ventade dhigule velipothundhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yama dhairyanga edhurelli nilchunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yama dhairyanga edhurelli nilchunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnedhirinchi bedhurinka untundha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnedhirinchi bedhurinka untundha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolo kolanna kolo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolo kolanna kolo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kommalu kila kila navvali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kommalu kila kila navvali"/>
</div>
<div class="lyrico-lyrics-wrapper">Kovello velige jyothulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kovello velige jyothulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallalo koluvundali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallalo koluvundali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chinna chinna anandhalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna chinna anandhalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinnaboni anubandhalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinnaboni anubandhalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Appudappudu chekkiliginthalu peduthundanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appudappudu chekkiliginthalu peduthundanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalatha kannillu leni 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalatha kannillu leni "/>
</div>
<div class="lyrico-lyrics-wrapper">chinna naati kerinthalni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chinna naati kerinthalni"/>
</div>
<div class="lyrico-lyrics-wrapper">Chitikesi itu rammantu pilipinchaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chitikesi itu rammantu pilipinchaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhilosthundhi chudu kannula vindhugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhilosthundhi chudu kannula vindhugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Urandharini kalipe ummadi pandugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urandharini kalipe ummadi pandugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa naluguritho chelimi panchuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa naluguritho chelimi panchuko"/>
</div>
<div class="lyrico-lyrics-wrapper">Chirunagavu sirulu penchuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chirunagavu sirulu penchuko"/>
</div>
<div class="lyrico-lyrics-wrapper">Jadivaane paduthunna jadisena thadisena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jadivaane paduthunna jadisena thadisena"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee pedhavula pai chirunavvulu eppudaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee pedhavula pai chirunavvulu eppudaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvu dheenanga ye mulo kurchunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvu dheenanga ye mulo kurchunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu ventade dhigule velipothundhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu ventade dhigule velipothundhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yama dhairyanga edhurelli nilchunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yama dhairyanga edhurelli nilchunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnedhirinchi bedhurinka untundha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnedhirinchi bedhurinka untundha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neelonu naalonu ee neleygaa ammai undi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelonu naalonu ee neleygaa ammai undi"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha ayinolle gaani parulevvaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha ayinolle gaani parulevvaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Manaloni chutturikanni muripinche ee dhurannni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manaloni chutturikanni muripinche ee dhurannni"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheripe veelundhante kaadhanarevvaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheripe veelundhante kaadhanarevvaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Oka puvu vichina gandham urike podhugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oka puvu vichina gandham urike podhugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Padhi mandhiki aanandham panchaka podhugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padhi mandhiki aanandham panchaka podhugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa thagina varasaina tharaka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa thagina varasaina tharaka "/>
</div>
<div class="lyrico-lyrics-wrapper">theralu vidi dhariki cheragaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theralu vidi dhariki cheragaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Prathi nithyam punnamigaa anukodha nelavanka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prathi nithyam punnamigaa anukodha nelavanka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalalanni viriyaga virisina vennelagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalalanni viriyaga virisina vennelagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvu dheenanga ye mulo kurchunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvu dheenanga ye mulo kurchunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu ventade dhigule velipothundhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu ventade dhigule velipothundhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yama dhairyanga edhurelli nilchunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yama dhairyanga edhurelli nilchunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnedhirinchi bedhurinka untundha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnedhirinchi bedhurinka untundha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolo kolanna kolo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolo kolanna kolo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kommalu kila kila navvali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kommalu kila kila navvali"/>
</div>
<div class="lyrico-lyrics-wrapper">Kovello velige jyothulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kovello velige jyothulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallalo koluvundali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallalo koluvundali"/>
</div>
</pre>
