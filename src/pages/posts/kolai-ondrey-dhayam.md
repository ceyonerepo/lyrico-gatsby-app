---
title: "kolai ondrey song lyrics"
album: "Dhayam"
artist: "Sathish Selvam"
lyricist: "Muthamil"
director: "Kannan Rangaswamy"
path: "/albums/dhayam-lyrics"
song: "Kolai Ondrey"
image: ../../images/albumart/dhayam.jpg
date: 2017-03-24
lang: tamil
youtubeLink: "https://www.youtube.com/embed/i5bmQgJzxoE"
type: "melody"
singers:
  - Shakthisree Gopalan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kolai Ondre Vilai Inge 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolai Ondre Vilai Inge "/>
</div>
<div class="lyrico-lyrics-wrapper">Vinai Ondre Vidhai Inge 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinai Ondre Vidhai Inge "/>
</div>
<div class="lyrico-lyrics-wrapper">Nilai Vendren Pizhai Seidhe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilai Vendren Pizhai Seidhe "/>
</div>
<div class="lyrico-lyrics-wrapper">Niram Engum Nijam Enge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niram Engum Nijam Enge"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vedikkindren Vidai Enge 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedikkindren Vidai Enge "/>
</div>
<div class="lyrico-lyrics-wrapper">Thudikkindren Thunai Enge 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thudikkindren Thunai Enge "/>
</div>
<div class="lyrico-lyrics-wrapper">Verum Kai Dhaan Ini Inge 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verum Kai Dhaan Ini Inge "/>
</div>
<div class="lyrico-lyrics-wrapper">Vaerukindren Enai Naan Verukiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaerukindren Enai Naan Verukiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Therindhe Naan Azhigindren 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Therindhe Naan Azhigindren "/>
</div>
<div class="lyrico-lyrics-wrapper">Pirindhe Naan Azhugindren 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirindhe Naan Azhugindren "/>
</div>
<div class="lyrico-lyrics-wrapper">Edhai Naanum Madhiththeno 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhai Naanum Madhiththeno "/>
</div>
<div class="lyrico-lyrics-wrapper">Midhiththoda Idindhene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Midhiththoda Idindhene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manam Solla Vazhi Sella 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam Solla Vazhi Sella "/>
</div>
<div class="lyrico-lyrics-wrapper">Irundhenna Ini Vella 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irundhenna Ini Vella "/>
</div>
<div class="lyrico-lyrics-wrapper">Panam Kaana Pagai Konden 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panam Kaana Pagai Konden "/>
</div>
<div class="lyrico-lyrics-wrapper">Verukindren Enai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verukindren Enai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Verukkiren 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Verukkiren "/>
</div>
<div class="lyrico-lyrics-wrapper">Kalaindhen 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalaindhen "/>
</div>
<div class="lyrico-lyrics-wrapper">Tholaindhen 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholaindhen "/>
</div>
<div class="lyrico-lyrics-wrapper">Vadhaindhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadhaindhen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Valli Yeno Arundhen 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valli Yeno Arundhen "/>
</div>
<div class="lyrico-lyrics-wrapper">Anal Ariyudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anal Ariyudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Edharkkaga Yemattram 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edharkkaga Yemattram "/>
</div>
<div class="lyrico-lyrics-wrapper">Thavikkindren Thadumattram 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thavikkindren Thadumattram "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Udhaithinge Olindhodum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udhaithinge Olindhodum "/>
</div>
<div class="lyrico-lyrics-wrapper">Uyarvukka Urumaatram 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyarvukka Urumaatram "/>
</div>
<div class="lyrico-lyrics-wrapper">Madhi Kondu Pizhaiththaenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhi Kondu Pizhaiththaenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir Kolla Uzhaiththenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Kolla Uzhaiththenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sadhiyodu Vasiththaene 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sadhiyodu Vasiththaene "/>
</div>
<div class="lyrico-lyrics-wrapper">Verukkindran Enai Naan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verukkindran Enai Naan "/>
</div>
<div class="lyrico-lyrics-wrapper">Verukkindren 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verukkindren "/>
</div>
<div class="lyrico-lyrics-wrapper">Vidaadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidaadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thodaadha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodaadha "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vinaave Vidai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinaave Vidai "/>
</div>
<div class="lyrico-lyrics-wrapper">Pazhi Yen Pagai Yen 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pazhi Yen Pagai Yen "/>
</div>
<div class="lyrico-lyrics-wrapper">Marandhaen Pirai Kaana 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marandhaen Pirai Kaana "/>
</div>
<div class="lyrico-lyrics-wrapper">Thirindhaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirindhaen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhisai Thinarudhe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhisai Thinarudhe "/>
</div>
<div class="lyrico-lyrics-wrapper">Marandhen Pirai Kaana 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marandhen Pirai Kaana "/>
</div>
<div class="lyrico-lyrics-wrapper">Thirindhen 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirindhen "/>
</div>
<div class="lyrico-lyrics-wrapper">Dhisai Thinarudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhisai Thinarudhe"/>
</div>
</pre>
