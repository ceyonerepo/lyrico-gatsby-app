---
title: "manasuki hanikaram song lyrics"
album: "Thellavarithe Guruvaram"
artist: "Kaala Bhairava"
lyricist: "Krishna Vallepu"
director: "Manikanth Gelli"
path: "/albums/thellavarithe-guruvaram-lyrics"
song: "Manasuki Hanikaram Ammaye"
image: ../../images/albumart/thellavarithe-guruvaram.jpg
date: 2021-03-27
lang: telugu
youtubeLink: "https://www.youtube.com/embed/047h2i8F-Sg"
type: "love"
singers:
  - Achu Rajamani
  - Prudhvi Chandra
  - Kaala Bhairava
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Manasuku Hanikaram Ammaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasuku Hanikaram Ammaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Thelisina Thappukodu Abbaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thelisina Thappukodu Abbaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Vadhalalem Undalem
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadhalalem Undalem"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhalalem Aagalem
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhalalem Aagalem"/>
</div>
<div class="lyrico-lyrics-wrapper">Vadhalalem Undalem
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadhalalem Undalem"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhalalem aagalem
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhalalem aagalem"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sare Sare Annaamo Raadhaare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sare Sare Annaamo Raadhaare"/>
</div>
<div class="lyrico-lyrics-wrapper">Nahi Nahi Annaamo Godhaare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nahi Nahi Annaamo Godhaare"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aadallantha Anthera Mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadallantha Anthera Mama"/>
</div>
<div class="lyrico-lyrics-wrapper">Ardham Kaani Puzzle Ye Ra Mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ardham Kaani Puzzle Ye Ra Mama"/>
</div>
<div class="lyrico-lyrics-wrapper">Chesedhantha Chesthune Mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chesedhantha Chesthune Mama"/>
</div>
<div class="lyrico-lyrics-wrapper">Choopisthaaru Cinemaanu Mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choopisthaaru Cinemaanu Mama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vadhalalem Undalem
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadhalalem Undalem"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhalalem Aagalem
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhalalem Aagalem"/>
</div>
<div class="lyrico-lyrics-wrapper">Vadhalalem Undalem
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadhalalem Undalem"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhalalem aagalem
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhalalem aagalem"/>
</div>
<div class="lyrico-lyrics-wrapper">Vadhalalem Undalem
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadhalalem Undalem"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhalalem Aagalem
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhalalem Aagalem"/>
</div>
<div class="lyrico-lyrics-wrapper">Vadhalalem Undalem
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadhalalem Undalem"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhalalem aagalem
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhalalem aagalem"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theliviga Pulihore Kalipesthaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theliviga Pulihore Kalipesthaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Chivaraki Karepaakulaipothaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chivaraki Karepaakulaipothaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kakkalem Mingalem
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kakkalem Mingalem"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheppalem Dhaachalem
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheppalem Dhaachalem"/>
</div>
<div class="lyrico-lyrics-wrapper">Kakkalem Mingalem
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kakkalem Mingalem"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheppalem Dhaachalem
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheppalem Dhaachalem"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidudala Undani Khaidheelam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidudala Undani Khaidheelam"/>
</div>
<div class="lyrico-lyrics-wrapper">Maguvula Chethilo Bandheelam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maguvula Chethilo Bandheelam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meere Meere Praanam Antaare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meere Meere Praanam Antaare"/>
</div>
<div class="lyrico-lyrics-wrapper">Roju Roju Praanam Thintaare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Roju Roju Praanam Thintaare"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha Nuvve Antu Untaare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Nuvve Antu Untaare"/>
</div>
<div class="lyrico-lyrics-wrapper">Anthe Choosi Jump Ayipothaare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anthe Choosi Jump Ayipothaare"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kakkalem Mingalem
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kakkalem Mingalem"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheppalem Dhaachalem
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheppalem Dhaachalem"/>
</div>
<div class="lyrico-lyrics-wrapper">Kakkalem Mingalem
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kakkalem Mingalem"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheppalem Dhaachalem
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheppalem Dhaachalem"/>
</div>
<div class="lyrico-lyrics-wrapper">Kakkalem Mingalem
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kakkalem Mingalem"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheppalem Dhaachalem
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheppalem Dhaachalem"/>
</div>
<div class="lyrico-lyrics-wrapper">Kakkalem Mingalem
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kakkalem Mingalem"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheppalem Dhaachalem
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheppalem Dhaachalem"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaguvuki Katthule Dhoostaare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaguvuki Katthule Dhoostaare"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappulanni Maapaine Thosthaare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappulanni Maapaine Thosthaare"/>
</div>
<div class="lyrico-lyrics-wrapper">Neggalem Thaggalem
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neggalem Thaggalem"/>
</div>
<div class="lyrico-lyrics-wrapper">Bathakalem Chaavalem
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bathakalem Chaavalem"/>
</div>
<div class="lyrico-lyrics-wrapper">Neggalem Thaggalem
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neggalem Thaggalem"/>
</div>
<div class="lyrico-lyrics-wrapper">Bathakalem Chaavalem
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bathakalem Chaavalem"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Leniponi Doubt Ye Raajesthaare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Leniponi Doubt Ye Raajesthaare"/>
</div>
<div class="lyrico-lyrics-wrapper">Nijamani Fight Ye Chesesthaare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijamani Fight Ye Chesesthaare"/>
</div>
<div class="lyrico-lyrics-wrapper">Etta Unna Thantaale Kaaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Etta Unna Thantaale Kaaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadesthaaru Manathoti Peka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadesthaaru Manathoti Peka"/>
</div>
<div class="lyrico-lyrics-wrapper">Argue Veelantu Leka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Argue Veelantu Leka"/>
</div>
<div class="lyrico-lyrics-wrapper">Moogai Pothe Bathukantha Keka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moogai Pothe Bathukantha Keka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neggalem Thaggalem
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neggalem Thaggalem"/>
</div>
<div class="lyrico-lyrics-wrapper">Bathakalem chaavalem
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bathakalem chaavalem"/>
</div>
</pre>
