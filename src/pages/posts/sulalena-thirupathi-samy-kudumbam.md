---
title: "sulalena song lyrics"
album: "Thirupathi Samy Kudumbam"
artist: "Sam D Raj"
lyricist: "Meenachisundaram"
director: "Suresh Shanmugam"
path: "/albums/thirupathi-samy-kudumbam-lyrics"
song: "Sulalena"
image: ../../images/albumart/thirupathi-samy-kudumbam.jpg
date: 2019-12-13
lang: tamil
youtubeLink: "https://www.youtube.com/embed/vScXlHAUeL0"
type: "happy"
singers:
  - Harini Ramesh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">pularudhe naazhigai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pularudhe naazhigai"/>
</div>
<div class="lyrico-lyrics-wrapper">avizhuthe poovagai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avizhuthe poovagai"/>
</div>
<div class="lyrico-lyrics-wrapper">suzhalena suzhalum nerame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suzhalena suzhalum nerame"/>
</div>
<div class="lyrico-lyrics-wrapper">mazhaiyena thoorum kaalame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mazhaiyena thoorum kaalame"/>
</div>
<div class="lyrico-lyrics-wrapper">midhamana soozhnilai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="midhamana soozhnilai"/>
</div>
<div class="lyrico-lyrics-wrapper">oru thoovalai thoovuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru thoovalai thoovuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">mruthuvana naaligai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mruthuvana naaligai"/>
</div>
<div class="lyrico-lyrics-wrapper">perum aavalai koota
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="perum aavalai koota"/>
</div>
<div class="lyrico-lyrics-wrapper">vaanilai maaruthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaanilai maaruthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">suzhalena suzhalum nerame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suzhalena suzhalum nerame"/>
</div>
<div class="lyrico-lyrics-wrapper">mazhaiyena thoorum kaalame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mazhaiyena thoorum kaalame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">meendumena vendume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meendumena vendume"/>
</div>
<div class="lyrico-lyrics-wrapper">padi thandume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="padi thandume"/>
</div>
<div class="lyrico-lyrics-wrapper">athai kandu nee kalithidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athai kandu nee kalithidu"/>
</div>
<div class="lyrico-lyrics-wrapper">podhum ena modhume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="podhum ena modhume"/>
</div>
<div class="lyrico-lyrics-wrapper">thalai kothume siru sindu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalai kothume siru sindu"/>
</div>
<div class="lyrico-lyrics-wrapper">nee mudinthudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee mudinthudu"/>
</div>
<div class="lyrico-lyrics-wrapper">sol ondrile vadhai kondrida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sol ondrile vadhai kondrida"/>
</div>
<div class="lyrico-lyrics-wrapper">aram vendume pagai vendrida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aram vendume pagai vendrida"/>
</div>
<div class="lyrico-lyrics-wrapper">araigalin sirai neengiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="araigalin sirai neengiye"/>
</div>
<div class="lyrico-lyrics-wrapper">paravaiyin vaan alandhithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paravaiyin vaan alandhithu"/>
</div>
<div class="lyrico-lyrics-wrapper">puthu valiyodu thani vizhiyodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthu valiyodu thani vizhiyodu"/>
</div>
<div class="lyrico-lyrics-wrapper">kalam kandidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalam kandidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">suzhalena suzhalum nerame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suzhalena suzhalum nerame"/>
</div>
<div class="lyrico-lyrics-wrapper">mazhaiyena thoorum kaalame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mazhaiyena thoorum kaalame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">maaya oli soolume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maaya oli soolume"/>
</div>
<div class="lyrico-lyrics-wrapper">agam mevume athai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="agam mevume athai "/>
</div>
<div class="lyrico-lyrics-wrapper">kandu nee unarnthidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kandu nee unarnthidu"/>
</div>
<div class="lyrico-lyrics-wrapper">thaaya mena aadume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaaya mena aadume"/>
</div>
<div class="lyrico-lyrics-wrapper">vilai yaadume unai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vilai yaadume unai "/>
</div>
<div class="lyrico-lyrics-wrapper">vendru nee nindridu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vendru nee nindridu"/>
</div>
<div class="lyrico-lyrics-wrapper">viral asaivile oli illaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viral asaivile oli illaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">viral sodukida thisai adhirume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viral sodukida thisai adhirume"/>
</div>
<div class="lyrico-lyrics-wrapper">thugalin thulai meeriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thugalin thulai meeriye"/>
</div>
<div class="lyrico-lyrics-wrapper">ranangalai veli aachidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ranangalai veli aachidu"/>
</div>
<div class="lyrico-lyrics-wrapper">thani nadaiyodu vega 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thani nadaiyodu vega "/>
</div>
<div class="lyrico-lyrics-wrapper">thunaiyodu nadai potidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thunaiyodu nadai potidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">suzhalena suzhalum nerame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suzhalena suzhalum nerame"/>
</div>
<div class="lyrico-lyrics-wrapper">mazhaiyena thoorum kaalame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mazhaiyena thoorum kaalame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">mruthuvana naaligai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mruthuvana naaligai"/>
</div>
<div class="lyrico-lyrics-wrapper">perum aavalai koota
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="perum aavalai koota"/>
</div>
<div class="lyrico-lyrics-wrapper">vaanilai maaruthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaanilai maaruthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">suzhalena suzhalum nerame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suzhalena suzhalum nerame"/>
</div>
<div class="lyrico-lyrics-wrapper">mazhaiyena thoorum kaalame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mazhaiyena thoorum kaalame"/>
</div>
</pre>
