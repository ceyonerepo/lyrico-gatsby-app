---
title: "light house kannu song lyrics"
album: "Maggy"
artist: "UM Steven Sathish"
lyricist: "Maima sudhagar - UM stevan sathish"
director: "Kartikeyen"
path: "/albums/maggy-lyrics"
song: "Light House Kannu"
image: ../../images/albumart/maggy.jpg
date: 2019-11-22
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ldOuv9LJxBc"
type: "gaana"
singers:
  - Velmurugan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">hey light house kannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey light house kannu"/>
</div>
<div class="lyrico-lyrics-wrapper">nee siricha heroinnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee siricha heroinnu"/>
</div>
<div class="lyrico-lyrics-wrapper">kalarulla nee thandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalarulla nee thandi"/>
</div>
<div class="lyrico-lyrics-wrapper">foreigner ponnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="foreigner ponnu"/>
</div>
<div class="lyrico-lyrics-wrapper">nee nadantha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee nadantha "/>
</div>
<div class="lyrico-lyrics-wrapper">jolikkum theru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jolikkum theru"/>
</div>
<div class="lyrico-lyrics-wrapper">un pechu karumbu charu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un pechu karumbu charu"/>
</div>
<div class="lyrico-lyrics-wrapper">asathura azhagiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="asathura azhagiye"/>
</div>
<div class="lyrico-lyrics-wrapper">nee konjam thirumbi paru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee konjam thirumbi paru"/>
</div>
<div class="lyrico-lyrics-wrapper">en azhagu thevathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en azhagu thevathai"/>
</div>
<div class="lyrico-lyrics-wrapper">konjam sirichikko 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konjam sirichikko "/>
</div>
<div class="lyrico-lyrics-wrapper">nee siricha than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee siricha than"/>
</div>
<div class="lyrico-lyrics-wrapper">nilavukkuda odipoyi olinjikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nilavukkuda odipoyi olinjikum"/>
</div>
<div class="lyrico-lyrics-wrapper">en azhagu thevathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en azhagu thevathai"/>
</div>
<div class="lyrico-lyrics-wrapper">konjam sirichikko 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konjam sirichikko "/>
</div>
<div class="lyrico-lyrics-wrapper">nee siricha than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee siricha than"/>
</div>
<div class="lyrico-lyrics-wrapper">nilavukkuda odipoyi olinjikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nilavukkuda odipoyi olinjikum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">hey light house kannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey light house kannu"/>
</div>
<div class="lyrico-lyrics-wrapper">nee siricha heroinnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee siricha heroinnu"/>
</div>
<div class="lyrico-lyrics-wrapper">kalarulla nee thandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalarulla nee thandi"/>
</div>
<div class="lyrico-lyrics-wrapper">foreigner ponnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="foreigner ponnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vaadi rochakari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaadi rochakari"/>
</div>
<div class="lyrico-lyrics-wrapper">unnala unnala yengureney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnala unnala yengureney"/>
</div>
<div class="lyrico-lyrics-wrapper">adi vaadi pasakari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi vaadi pasakari"/>
</div>
<div class="lyrico-lyrics-wrapper">unna nan unna nan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna nan unna nan"/>
</div>
<div class="lyrico-lyrics-wrapper">thanguvaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanguvaney"/>
</div>
<div class="lyrico-lyrics-wrapper">adi penney ne senju vacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi penney ne senju vacha"/>
</div>
<div class="lyrico-lyrics-wrapper">silaiye muraiche enna kollathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="silaiye muraiche enna kollathe"/>
</div>
<div class="lyrico-lyrics-wrapper">un kanne enna sikka vacha valaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kanne enna sikka vacha valaye"/>
</div>
<div class="lyrico-lyrics-wrapper">nadiche vachi seyatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nadiche vachi seyatha"/>
</div>
<div class="lyrico-lyrics-wrapper">adi kalli chedi kannala 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi kalli chedi kannala "/>
</div>
<div class="lyrico-lyrics-wrapper">enna kuthi porale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna kuthi porale"/>
</div>
<div class="lyrico-lyrics-wrapper">kattumaram pola enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kattumaram pola enna"/>
</div>
<div class="lyrico-lyrics-wrapper">methaka vachu porale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="methaka vachu porale"/>
</div>
<div class="lyrico-lyrics-wrapper">kutti kutti kaiyale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kutti kutti kaiyale"/>
</div>
<div class="lyrico-lyrics-wrapper">kuslukeduthu porale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuslukeduthu porale"/>
</div>
<div class="lyrico-lyrics-wrapper">yaru innu pakama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaru innu pakama"/>
</div>
<div class="lyrico-lyrics-wrapper">sozhatti adichu porale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sozhatti adichu porale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">hey light house kannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey light house kannu"/>
</div>
<div class="lyrico-lyrics-wrapper">nee siricha heroinnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee siricha heroinnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">adi vaadi trendi lady
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi vaadi trendi lady"/>
</div>
<div class="lyrico-lyrics-wrapper">natchathiram pola iva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="natchathiram pola iva"/>
</div>
<div class="lyrico-lyrics-wrapper">jolikirale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jolikirale"/>
</div>
<div class="lyrico-lyrics-wrapper">adi vaadi diamond body
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi vaadi diamond body"/>
</div>
<div class="lyrico-lyrics-wrapper">pola bright aaga bright aaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pola bright aaga bright aaga"/>
</div>
<div class="lyrico-lyrics-wrapper">minugurale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="minugurale"/>
</div>
<div class="lyrico-lyrics-wrapper">adi penne ne kolantha pola sirikira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi penne ne kolantha pola sirikira"/>
</div>
<div class="lyrico-lyrics-wrapper">atha parthu sethu ponene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="atha parthu sethu ponene"/>
</div>
<div class="lyrico-lyrics-wrapper">adi kanne ne pattam poochi thanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi kanne ne pattam poochi thanadi"/>
</div>
<div class="lyrico-lyrics-wrapper">unna nenache nan parakuran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna nenache nan parakuran"/>
</div>
<div class="lyrico-lyrics-wrapper">nee palkova sweetudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee palkova sweetudi"/>
</div>
<div class="lyrico-lyrics-wrapper">un kannu rendum cutedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kannu rendum cutedi"/>
</div>
<div class="lyrico-lyrics-wrapper">un azhaga pathu nanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un azhaga pathu nanga"/>
</div>
<div class="lyrico-lyrics-wrapper">bomma pola ayitom di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bomma pola ayitom di"/>
</div>
<div class="lyrico-lyrics-wrapper">ne pesunale waitu di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ne pesunale waitu di"/>
</div>
<div class="lyrico-lyrics-wrapper">athula vizhunthom nanga di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athula vizhunthom nanga di"/>
</div>
<div class="lyrico-lyrics-wrapper">unna pola yarum illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna pola yarum illa"/>
</div>
<div class="lyrico-lyrics-wrapper">google ah kettu para di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="google ah kettu para di"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">hey light house kannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey light house kannu"/>
</div>
<div class="lyrico-lyrics-wrapper">nee siricha heroinnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee siricha heroinnu"/>
</div>
</pre>
