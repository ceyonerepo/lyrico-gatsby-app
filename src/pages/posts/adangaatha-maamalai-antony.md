---
title: "adangaatha maamalai song lyrics"
album: "Antony"
artist: "Sivatmikha"
lyricist: "Sivam"
director: "Kutti Kumar"
path: "/albums/antony-lyrics"
song: "Adangaatha Maamalai"
image: ../../images/albumart/antony.jpg
date: 2018-06-01
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Wz9Sqi1DHDs"
type: "mass"
singers:
  - Sivam
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">parakuthu parakuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="parakuthu parakuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">parakuthu pole
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="parakuthu pole"/>
</div>
<div class="lyrico-lyrics-wrapper">adangaatha maamalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adangaatha maamalai"/>
</div>
<div class="lyrico-lyrics-wrapper">veipugal paar malai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veipugal paar malai"/>
</div>
<div class="lyrico-lyrics-wrapper">maraiyatha sooriyal pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maraiyatha sooriyal pol"/>
</div>
<div class="lyrico-lyrics-wrapper">parai kelurai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="parai kelurai"/>
</div>
<div class="lyrico-lyrics-wrapper">unathaaga aarambam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unathaaga aarambam"/>
</div>
<div class="lyrico-lyrics-wrapper">viraiyatha theevanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viraiyatha theevanam"/>
</div>
<div class="lyrico-lyrics-wrapper">inimel than boogambam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inimel than boogambam"/>
</div>
<div class="lyrico-lyrics-wrapper">adithida udaithida 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adithida udaithida "/>
</div>
<div class="lyrico-lyrics-wrapper">malaigalum sitharum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malaigalum sitharum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kodupaarvai konda nenjai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kodupaarvai konda nenjai"/>
</div>
<div class="lyrico-lyrics-wrapper">nari pole aadum kannai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nari pole aadum kannai"/>
</div>
<div class="lyrico-lyrics-wrapper">asaraamal thetruvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="asaraamal thetruvaan"/>
</div>
<div class="lyrico-lyrics-wrapper">adiyoodu veelthuvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adiyoodu veelthuvaan"/>
</div>
<div class="lyrico-lyrics-wrapper">valarathe valarathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valarathe valarathe"/>
</div>
<div class="lyrico-lyrics-wrapper">aniyaayam nigalaathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aniyaayam nigalaathe"/>
</div>
<div class="lyrico-lyrics-wrapper">adangathe adangathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adangathe adangathe"/>
</div>
<div class="lyrico-lyrics-wrapper">ivan veeram miralaathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ivan veeram miralaathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ithuve than boogambam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithuve than boogambam"/>
</div>
<div class="lyrico-lyrics-wrapper">ithuve than boogambam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithuve than boogambam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">adangaatha maamalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adangaatha maamalai"/>
</div>
<div class="lyrico-lyrics-wrapper">veipugal paar malai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veipugal paar malai"/>
</div>
<div class="lyrico-lyrics-wrapper">maraiyatha sooriyal pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maraiyatha sooriyal pol"/>
</div>
<div class="lyrico-lyrics-wrapper">parai kelurai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="parai kelurai"/>
</div>
<div class="lyrico-lyrics-wrapper">unathaaga aarambam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unathaaga aarambam"/>
</div>
<div class="lyrico-lyrics-wrapper">viraiyatha theevanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viraiyatha theevanam"/>
</div>
<div class="lyrico-lyrics-wrapper">inimel than boogambam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inimel than boogambam"/>
</div>
<div class="lyrico-lyrics-wrapper">adithida udaithida 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adithida udaithida "/>
</div>
<div class="lyrico-lyrics-wrapper">malaigalum sitharum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malaigalum sitharum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">urainthidum nimidamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="urainthidum nimidamum"/>
</div>
<div class="lyrico-lyrics-wrapper">elunthidume ini eluvathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="elunthidume ini eluvathu"/>
</div>
<div class="lyrico-lyrics-wrapper">pilai ena uraithidume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pilai ena uraithidume"/>
</div>
<div class="lyrico-lyrics-wrapper">vali thanil irupavan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vali thanil irupavan "/>
</div>
<div class="lyrico-lyrics-wrapper">ariginile avan karam thoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ariginile avan karam thoda"/>
</div>
<div class="lyrico-lyrics-wrapper">ivan uyir elunthidume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ivan uyir elunthidume"/>
</div>
<div class="lyrico-lyrics-wrapper">padaithavan irupathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="padaithavan irupathu"/>
</div>
<div class="lyrico-lyrics-wrapper">pagaiyinile thinam manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pagaiyinile thinam manam"/>
</div>
<div class="lyrico-lyrics-wrapper">mattum kuruthiyil erinthidave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mattum kuruthiyil erinthidave"/>
</div>
<div class="lyrico-lyrics-wrapper">nilavena mudinthidum erinthidave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nilavena mudinthidum erinthidave"/>
</div>
<div class="lyrico-lyrics-wrapper">puthu silavettum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthu silavettum "/>
</div>
<div class="lyrico-lyrics-wrapper">othungi nimirnthidave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="othungi nimirnthidave"/>
</div>
</pre>
