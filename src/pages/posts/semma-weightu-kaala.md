---
title: "semma weightu song lyrics"
album: "Kaala"
artist: "Santhosh Narayanan"
lyricist: "Dopeadelicz - Arunraja Kamaraj - Logan"
director: "Pa. Ranjith"
path: "/albums/kaala-lyrics"
song: "Semma Weightu"
image: ../../images/albumart/kaala.jpg
date: 2018-06-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/9d1GHxWIBds"
type: "mass"
singers:
  - Hariharasudhan
  - Santhosh Narayanan
  - Arunraja Kamaraj
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Semma weighttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Semma weighttu"/>
</div>
<div class="lyrico-lyrics-wrapper">Semma weighttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Semma weighttu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adanga maruppavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adanga maruppavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Velicham koduppavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velicham koduppavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavala kalaikkiravan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavala kalaikkiravan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarunnudhan kaattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarunnudhan kaattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manasa thodavilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasa thodavilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Manushan vidavilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manushan vidavilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Karuppa poosikittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuppa poosikittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthavaru greattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthavaru greattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engal karuppar nagarathin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engal karuppar nagarathin"/>
</div>
<div class="lyrico-lyrics-wrapper">Karuppu vairam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuppu vairam"/>
</div>
<div class="lyrico-lyrics-wrapper">Karun chiruthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karun chiruthai"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha ooru kaaval veeran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha ooru kaaval veeran"/>
</div>
<div class="lyrico-lyrics-wrapper">Machadunna veedu thirumba maata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machadunna veedu thirumba maata"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga chawlula pottu thaakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga chawlula pottu thaakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaru vandhalum namma vazhiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaru vandhalum namma vazhiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Be careful
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Be careful"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ithudhan dhaaravi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithudhan dhaaravi"/>
</div>
<div class="lyrico-lyrics-wrapper">Paaru paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaru paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaru yaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaru yaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhutta un munnadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhutta un munnadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Yehei mavanae nee gaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yehei mavanae nee gaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaala saet inimae namma pinnadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaala saet inimae namma pinnadi"/>
</div>
<div class="lyrico-lyrics-wrapper">So sidhara vidalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="So sidhara vidalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhara vidalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhara vidalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Siraga virithu parakka vidalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siraga virithu parakka vidalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadukka vandhalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadukka vandhalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadaiyillaamal azhithu vidalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadaiyillaamal azhithu vidalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Semma weightu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Semma weightu"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma kaala saettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma kaala saettu"/>
</div>
<div class="lyrico-lyrics-wrapper">Heyyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyyy"/>
</div>
<div class="lyrico-lyrics-wrapper">Semma weightu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Semma weightu"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma kaala saettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma kaala saettu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Cross road T junction
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cross road T junction"/>
</div>
60 <div class="lyrico-lyrics-wrapper">feettu 90 Feetu koliwada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="feettu 90 Feetu koliwada"/>
</div>
<div class="lyrico-lyrics-wrapper">Kumbharwada romba romba billa da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kumbharwada romba romba billa da"/>
</div>
<div class="lyrico-lyrics-wrapper">Ottumotha areavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ottumotha areavum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalavoda qila da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalavoda qila da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Semma weightu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Semma weightu"/>
</div>
<div class="lyrico-lyrics-wrapper">Semma weightu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Semma weightu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kovil mani athanaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kovil mani athanaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Othumaiya olikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Othumaiya olikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sondha pandham polathan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sondha pandham polathan"/>
</div>
<div class="lyrico-lyrics-wrapper">Onna naanga iruppom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna naanga iruppom"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppadiyum enga kodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppadiyum enga kodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Uchathula parakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uchathula parakkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vanakkam namaashkaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanakkam namaashkaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Salam alaikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Salam alaikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppavum namma kootam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppavum namma kootam"/>
</div>
<div class="lyrico-lyrics-wrapper">United ah irukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="United ah irukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Slum ah pathi un ennatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Slum ah pathi un ennatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam maathikko ulla vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam maathikko ulla vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga life style-ah nee paathukko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga life style-ah nee paathukko"/>
</div>
<div class="lyrico-lyrics-wrapper">Jopada veedanalum shokka naanga iruppom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jopada veedanalum shokka naanga iruppom"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaaluku keezha keechadnaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaluku keezha keechadnaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenja nimithi nadappom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenja nimithi nadappom"/>
</div>
<div class="lyrico-lyrics-wrapper">Thol koduppom thukkatthilum sirippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thol koduppom thukkatthilum sirippom"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeri pesi paaru thonga vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeri pesi paaru thonga vittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thola urippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thola urippom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaiya katti vaaya pothiNinna kaalam pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiya katti vaaya pothiNinna kaalam pochu"/>
</div>
<div class="lyrico-lyrics-wrapper">Etti vandhu ninnathellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Etti vandhu ninnathellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanathila yethiyachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanathila yethiyachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dharaavi enga area
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dharaavi enga area"/>
</div>
<div class="lyrico-lyrics-wrapper">Inga kaala saet thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga kaala saet thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Avaru munna vera yaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avaru munna vera yaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inga kaala saet thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga kaala saet thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Avaru munna vera yaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avaru munna vera yaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Semma weightu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Semma weightu"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma kaala saettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma kaala saettu"/>
</div>
<div class="lyrico-lyrics-wrapper">Heyyy hey hey hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyyy hey hey hey hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Semma weightu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Semma weightu"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma kaala saettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma kaala saettu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaa haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaa haa"/>
</div>
<div class="lyrico-lyrics-wrapper">Romba kadharnaak maatha kaala saet
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba kadharnaak maatha kaala saet"/>
</div>
<div class="lyrico-lyrics-wrapper">Ise bajke jab there
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ise bajke jab there"/>
</div>
<div class="lyrico-lyrics-wrapper">Irathe naa ho naek
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irathe naa ho naek"/>
</div>
<div class="lyrico-lyrics-wrapper">Maathatek thyaache
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maathatek thyaache"/>
</div>
<div class="lyrico-lyrics-wrapper">Sange maaje maappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sange maaje maappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Street ulla sathur
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Street ulla sathur"/>
</div>
<div class="lyrico-lyrics-wrapper">Thethil thulaa aappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thethil thulaa aappu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nagaru nerisal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nagaru nerisal"/>
</div>
<div class="lyrico-lyrics-wrapper">Penanju kedappom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Penanju kedappom"/>
</div>
<div class="lyrico-lyrics-wrapper">Thagara ottil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thagara ottil"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaakku pidippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaakku pidippom"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyaram therinju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyaram therinju"/>
</div>
<div class="lyrico-lyrics-wrapper">Usura koduppom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usura koduppom"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulukka nenacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulukka nenacha"/>
</div>
<div class="lyrico-lyrics-wrapper">Veratti adippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veratti adippom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Semma weightu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Semma weightu"/>
</div>
<div class="lyrico-lyrics-wrapper">Semma weightu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Semma weightu"/>
</div>
<div class="lyrico-lyrics-wrapper">Semma weightu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Semma weightu"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma kaala saettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma kaala saettu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Onnaavae vaazhurathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnaavae vaazhurathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppavumae mukkiyamdhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppavumae mukkiyamdhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nammoda makkalukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nammoda makkalukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Othumaiya kaththirundha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Othumaiya kaththirundha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalaikka nenacha kalaiya maatom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalaikka nenacha kalaiya maatom"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhikka nenacha nenappa azhippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhikka nenacha nenappa azhippom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Semma weightu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Semma weightu"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma kaala saettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma kaala saettu"/>
</div>
<div class="lyrico-lyrics-wrapper">Heyy hey hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy hey hey hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Inga kaala saet dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga kaala saet dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Avaru munna vera yaar?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avaru munna vera yaar?"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey hey hey hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hey hey hey hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Semma weightu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Semma weightu"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma kaala saettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma kaala saettu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaala kaala kaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaala kaala kaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaala namma kaala saet
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaala namma kaala saet"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaala kaala kaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaala kaala kaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaala namma kaala saet
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaala namma kaala saet"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaala kaala kaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaala kaala kaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaala namma kaala saet
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaala namma kaala saet"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaala kaala kaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaala kaala kaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaala namma kaala saet
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaala namma kaala saet"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaala kaala kaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaala kaala kaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaala namma kaala saet
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaala namma kaala saet"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaala saet
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaala saet"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma kaala saet
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma kaala saet"/>
</div>
</pre>
