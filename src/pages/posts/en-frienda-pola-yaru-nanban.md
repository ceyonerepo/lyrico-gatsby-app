---
title: "en frienda pola song lyrics"
album: "Nanban"
artist: "Harris Jayaraj"
lyricist: "Viveka"
director: "S. Shankar"
path: "/albums/nanban-lyrics"
song: "En Frienda Pola Yaru"
image: ../../images/albumart/nanban.jpg
date: 2012-01-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/v1XvuO0pEOU"
type: "Friendhsip"
singers:
  - Krish
  - Suchith Suresan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">En Frienda Pola Yaru Machchan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Frienda Pola Yaru Machchan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avan Trenda Ella Maaththi Vechchan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan Trenda Ella Maaththi Vechchan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Enga Pona Enga Machchan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Enga Pona Enga Machchan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ena Enni Enni Yenga Vechchan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ena Enni Enni Yenga Vechchan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Natpaala Namma Nenja Thechchan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natpaala Namma Nenja Thechchan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nam Kannil Neera Ponga Vechchan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam Kannil Neera Ponga Vechchan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Frienda Pola Yaru Machchan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Frienda Pola Yaru Machchan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avan Trenda Ella Maaththi Vechchan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan Trenda Ella Maaththi Vechchan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Enga Pona Enga Machchan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Enga Pona Enga Machchan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ena Enni Enni Yenga Vechchan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ena Enni Enni Yenga Vechchan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Natpaala Namma Nenja Thechchan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natpaala Namma Nenja Thechchan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nam Kannil Neera Ponga Vechchan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam Kannil Neera Ponga Vechchan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thozhanin Thozhgalum Annai Madi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozhanin Thozhgalum Annai Madi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avan Thooraththil Poothitta Thoppul Kodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan Thooraththil Poothitta Thoppul Kodi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhalai Thaandiyum Ullabadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhalai Thaandiyum Ullabadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endrum Natputhaan Uyranthathu Paththu Padi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endrum Natputhaan Uyranthathu Paththu Padi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Natpai Naangal Petrom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Natpai Naangal Petrom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athaanale Yaavum Katrom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athaanale Yaavum Katrom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mele Mele Sendrom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mele Mele Sendrom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaan Megam Pole Nindrom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaan Megam Pole Nindrom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pudhu Paathai Neeye Pottu Thanthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu Paathai Neeye Pottu Thanthaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen Paadhi Valiyil Vittu Sendraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Paadhi Valiyil Vittu Sendraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Thaayai Thedum Pillaiyanom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Thaayai Thedum Pillaiyanom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee illai Endraal Enge Povom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee illai Endraal Enge Povom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Frienda Pola Yaru Machchan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Frienda Pola Yaru Machchan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avan Trenda Ella Maaththi Vechchan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan Trenda Ella Maaththi Vechchan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Enga Pona Enga Machchan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Enga Pona Enga Machchan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ena Enni Enni Yenga Vechchan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ena Enni Enni Yenga Vechchan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Natpaala Namma Nenja Thechchan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natpaala Namma Nenja Thechchan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nam Kannil Neera Ponga Vechchan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam Kannil Neera Ponga Vechchan"/>
</div>
</pre>
