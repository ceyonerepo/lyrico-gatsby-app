---
title: "paravaigal song lyrics"
album: "Vantha Rajava Than Varuven"
artist: "Hiphop Tamizha"
lyricist: "Hiphop Tamizha"
director: "Sundar C"
path: "/albums/vantha-rajava-than-varuven-lyrics"
song: "Paravaigal"
image: ../../images/albumart/vantha-rajava-than-varuven.jpg
date: 2019-02-01
lang: tamil
youtubeLink: "https://www.youtube.com/embed/YIczSRkvWtY"
type: "mass"
singers:
  - Sanjith Hegde
  - Hiphop Tamizha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Paravaigal Paranthu Pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paravaigal Paranthu Pona"/>
</div>
<div class="lyrico-lyrics-wrapper">Pinbum Ondru Koodum Neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pinbum Ondru Koodum Neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Athai Thedum Aagaayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athai Thedum Aagaayam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uravugal Kalainthu Pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uravugal Kalainthu Pona"/>
</div>
<div class="lyrico-lyrics-wrapper">Pinbum Nenjil Ninaivugal Vaazhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pinbum Nenjil Ninaivugal Vaazhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Athai Thedi Nee Pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athai Thedi Nee Pogum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeah"/>
</div>
<div class="lyrico-lyrics-wrapper">Chillin Like A Boss
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chillin Like A Boss"/>
</div>
<div class="lyrico-lyrics-wrapper">Machan Ready-ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machan Ready-ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Still I Got Dem Flaws
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Still I Got Dem Flaws"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini Sema Athiradi Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Sema Athiradi Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">I Am Reaching For The Stars
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Am Reaching For The Stars"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini Parakkum En Kodi Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Parakkum En Kodi Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Summa Therikka Viduvom Vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summa Therikka Viduvom Vaada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">You Know Who We Are
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Know Who We Are"/>
</div>
<div class="lyrico-lyrics-wrapper">We Are Banging On This Track With STR
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="We Are Banging On This Track With STR"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo Hoo Hooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo Hoo Hooo"/>
</div>
<div class="lyrico-lyrics-wrapper">You Know Who We Are
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Know Who We Are"/>
</div>
<div class="lyrico-lyrics-wrapper">We Are Banging On This Track With STR
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="We Are Banging On This Track With STR"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Nallavan Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Nallavan Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aana Kettavan Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aana Kettavan Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Uthaman Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Uthaman Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Budhanum Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Budhanum Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Buththhiya Solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Buththhiya Solla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avasarama Aduthavan Vaazhkaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avasarama Aduthavan Vaazhkaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Mooka Nuzhaikkum Da Ooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mooka Nuzhaikkum Da Ooru"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu Kedakkatum Pathukkalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu Kedakkatum Pathukkalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma Pasangala Onna Seru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma Pasangala Onna Seru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vesham Illatha Paasam Venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vesham Illatha Paasam Venum"/>
</div>
<div class="lyrico-lyrics-wrapper">Desam Ellame Ini Onna Seranum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Desam Ellame Ini Onna Seranum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vesham Illatha Paasam Venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vesham Illatha Paasam Venum"/>
</div>
<div class="lyrico-lyrics-wrapper">Desam Ellame Ini Onna Seranum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Desam Ellame Ini Onna Seranum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">You Know Who We Are
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Know Who We Are"/>
</div>
<div class="lyrico-lyrics-wrapper">We Are Banging On This Track With STR
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="We Are Banging On This Track With STR"/>
</div>
<div class="lyrico-lyrics-wrapper">Haan You Know Who We Are
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haan You Know Who We Are"/>
</div>
<div class="lyrico-lyrics-wrapper">We Are Banging On This Track With STR
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="We Are Banging On This Track With STR"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unakkulla Irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkulla Irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethukku Da Veruppu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethukku Da Veruppu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Mura Porappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Mura Porappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Magizhchiya Parappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Magizhchiya Parappu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sondha Bandham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sondha Bandham"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaavum Sernthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaavum Sernthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhvathe Vaazhkkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvathe Vaazhkkai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyarthum Da Uzhaippu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyarthum Da Uzhaippu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalaikanum Kalaippu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalaikanum Kalaippu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhi Ellaam Maranthuttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi Ellaam Maranthuttu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirippathe Sirappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirippathe Sirappu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inbam Thunbam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inbam Thunbam"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaavum Sernthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaavum Sernthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sethikidum Vaazhkkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sethikidum Vaazhkkai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konjam Poruppu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Poruppu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sattai Karuppu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sattai Karuppu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thotta Theriyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thotta Theriyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sudum Neruppu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sudum Neruppu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethukku Unakku Ethukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethukku Unakku Ethukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Pathi Unakku Enna Kavalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Pathi Unakku Enna Kavalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam Vilakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Vilakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Vithi Vilakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vithi Vilakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Vilakkama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Vilakkama"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilakkava Ennai Adichavanaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilakkava Ennai Adichavanaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Anaipen Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anaipen Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhisaya Piravi Sarithiram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhisaya Piravi Sarithiram"/>
</div>
<div class="lyrico-lyrics-wrapper">Padaippan Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padaippan Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">You Know Who We Are
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Know Who We Are"/>
</div>
<div class="lyrico-lyrics-wrapper">We Are Banging On This Track With STR
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="We Are Banging On This Track With STR"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vesham Illatha Paasam Venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vesham Illatha Paasam Venum"/>
</div>
<div class="lyrico-lyrics-wrapper">Desam Ellame Ini Onna Seranum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Desam Ellame Ini Onna Seranum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vesham Illatha Paasam Venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vesham Illatha Paasam Venum"/>
</div>
<div class="lyrico-lyrics-wrapper">Desam Ellame Ini Onna Seranum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Desam Ellame Ini Onna Seranum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paravaigal Paranthu Pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paravaigal Paranthu Pona"/>
</div>
<div class="lyrico-lyrics-wrapper">Pinbum Ondru Koodum Neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pinbum Ondru Koodum Neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Athai Thedum Aagaayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athai Thedum Aagaayam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Nallavan Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Nallavan Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aana Kettavan Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aana Kettavan Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Uthaman Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Uthaman Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Budhanum Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Budhanum Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Buththiya Solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Buththiya Solla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">You Know Who We Are
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Know Who We Are"/>
</div>
<div class="lyrico-lyrics-wrapper">We Are Banging On This Track With STR
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="We Are Banging On This Track With STR"/>
</div>
<div class="lyrico-lyrics-wrapper">You Know Who We Are
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Know Who We Are"/>
</div>
<div class="lyrico-lyrics-wrapper">We Are Banging On This Track With STR
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="We Are Banging On This Track With STR"/>
</div>
</pre>
