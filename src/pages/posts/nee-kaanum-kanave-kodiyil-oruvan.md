---
title: "nee kaanum kanave song lyrics"
album: "Kodiyil Oruvan"
artist: "	Nivas K. Prasanna - Harish arjun"
lyricist: "Mohan Rajan"
director: "Ananda Krishnan"
path: "/albums/kodiyil-oruvan-lyrics"
song: "Nee Kaanum Kanave"
image: ../../images/albumart/kodiyil-oruvan.jpg
date: 2021-09-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/NANmoMCBNLw"
type: "motivation"
singers:
  - Sathyaprakash Dharmar
  - Abhishek Ravishankar
  - Vanjula Oppili
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nee Kaanum Kanave Unnai Uruvakum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kaanum Kanave Unnai Uruvakum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakku Vazhukattum Vazhiyai Siridhakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakku Vazhukattum Vazhiyai Siridhakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhavai Perithakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhavai Perithakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Ingu Yaarendru Oorukku Sollumada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Ingu Yaarendru Oorukku Sollumada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ooru Kazhugin Sirage Kaatrai Padiyakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooru Kazhugin Sirage Kaatrai Padiyakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadalai Podiyakkum Vaanai Vazhiyakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadalai Podiyakkum Vaanai Vazhiyakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavai Nijamakkum Tholviyin Kelviye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavai Nijamakkum Tholviyin Kelviye"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhvinai Maatrumada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvinai Maatrumada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ooru Alaiyai Pole Vizhundhu Vizhundhu Ezhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooru Alaiyai Pole Vizhundhu Vizhundhu Ezhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhundhu Ezhundhu Vidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhundhu Ezhundhu Vidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadaiyai Thagarthu Vidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadaiyai Thagarthu Vidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Karaiyai Adaindhu Vidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karaiyai Adaindhu Vidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nambikkai Kondu Nee Vaazhvinai Adainthu Vidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nambikkai Kondu Nee Vaazhvinai Adainthu Vidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ooru Alaiyai Pole Edhilum Vizhundhuvidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooru Alaiyai Pole Edhilum Vizhundhuvidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhaiyum Karainthuvidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhaiyum Karainthuvidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Anaiyai Udaithuvidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anaiyai Udaithuvidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Anaithum Koduthuvidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anaithum Koduthuvidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhvadhe Varamena Aanandham Konduvidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvadhe Varamena Aanandham Konduvidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ooru Theeyai Pole Vegam Kondu Vidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooru Theeyai Pole Vegam Kondu Vidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vettai Aadividu Theemai Edharkkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vettai Aadividu Theemai Edharkkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Theervu Ezhudhi Vidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Theervu Ezhudhi Vidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sodhanai Enbadhai Saadhanai Aakkividu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sodhanai Enbadhai Saadhanai Aakkividu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vervai Ellam Inge Undhan Perai Solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vervai Ellam Inge Undhan Perai Solla"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhegam Pookkum Poovagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhegam Pookkum Poovagum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nadhi Azhagu Nadhi Karai Azhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadhi Azhagu Nadhi Karai Azhagu"/>
</div>
<div class="lyrico-lyrics-wrapper">Karai Odhungidum Nuraigalum Or Azhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karai Odhungidum Nuraigalum Or Azhagu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadai Azhagu Adhil Thadam Pazhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadai Azhagu Adhil Thadam Pazhagu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Valimaikku Vaazhkkayil Vazhi Pazhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Valimaikku Vaazhkkayil Vazhi Pazhagu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bayam Unnai Vittu Ponal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bayam Unnai Vittu Ponal"/>
</div>
<div class="lyrico-lyrics-wrapper">Balam Unnil Vanthu Serum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Balam Unnil Vanthu Serum"/>
</div>
<div class="lyrico-lyrics-wrapper">Suyam Nenjai Thottu Mattrum Vaanil Yettrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suyam Nenjai Thottu Mattrum Vaanil Yettrum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Innalil Unnai Pattrum Neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Innalil Unnai Pattrum Neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanal Kangal Rendil Thondrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanal Kangal Rendil Thondrum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kangal Endral Kanneer Undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangal Endral Kanneer Undu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjam Endral Baaram Undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjam Endral Baaram Undu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paathai Endral Pallam Undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paathai Endral Pallam Undu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhooram Endral Pakkam Undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhooram Endral Pakkam Undu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thozhvi Endral Vetri Undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozhvi Endral Vetri Undu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sogam Endral Sorgam Undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sogam Endral Sorgam Undu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhkkai Endral Ellam Undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkkai Endral Ellam Undu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhndhu Paru Vegam Kondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhndhu Paru Vegam Kondu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kangal Endral Kanneer Undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangal Endral Kanneer Undu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjam Endral Baaram Undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjam Endral Baaram Undu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paathai Endral Pallam Undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paathai Endral Pallam Undu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhooram Endral Pakkam Undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhooram Endral Pakkam Undu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thozhvi Endral Vettri Undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozhvi Endral Vettri Undu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sogam Endral Sorgam Undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sogam Endral Sorgam Undu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhkkai Endral Ellam Undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkkai Endral Ellam Undu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhndhu Paru Vegam Kondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhndhu Paru Vegam Kondu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unakkul Unnai Thedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkul Unnai Thedu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnil Ulagin Veedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnil Ulagin Veedu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethaiyum Thaandi Munneru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethaiyum Thaandi Munneru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nadhi Azhagu Nadhi Karai Azhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadhi Azhagu Nadhi Karai Azhagu"/>
</div>
<div class="lyrico-lyrics-wrapper">Karai Odhungidum Nuraigalum Or Azhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karai Odhungidum Nuraigalum Or Azhagu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadai Azhagu Adhil Thadam Pazhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadai Azhagu Adhil Thadam Pazhagu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Valimaikku Vaazhkkayil Vazhi Pazhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Valimaikku Vaazhkkayil Vazhi Pazhagu"/>
</div>
</pre>
