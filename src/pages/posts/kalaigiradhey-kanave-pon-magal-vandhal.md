---
title: 'kalaigiradhey kanave song lyrics'
album: 'Pon Magal Vandhal'
artist: 'Govind Vasantha'
lyricist: 'Uma Devi'
director: 'J J Fredrick'
path: '/albums/pon-magal-vandhal-song-lyrics'
song: 'Kalaigiradhey Kanave'
image: ../../images/albumart/ponmagal-vandhal.jpg
date: 2020-05-29
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Oabst6IuSGc"
type: 'sad'
singers: 
- Govind Vasantha
---

<pre class="lyrics-native">
    Coming Soon
</pre>

<pre class="lyrics-english">

  <div class="lyrico-lyrics-wrapper">Kalaigiradhae kanavae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kalaigiradhae kanavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidiyaadho iravae
<input type="checkbox" class="lyrico-select-lyric-line" value="Vidiyaadho iravae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhvae inimel
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaazhvae inimel"/>
</div>
<div class="lyrico-lyrics-wrapper">Vali aagaadha
<input type="checkbox" class="lyrico-select-lyric-line" value="Vali aagaadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Maraithidalam saatchi
<input type="checkbox" class="lyrico-select-lyric-line" value="Maraithidalam saatchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Marainthidumo kaatchi
<input type="checkbox" class="lyrico-select-lyric-line" value="Marainthidumo kaatchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Unmai oru naal arangeraadha
<input type="checkbox" class="lyrico-select-lyric-line" value="Unmai oru naal arangeraadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ooka thunaiye un sol valiyae
<input type="checkbox" class="lyrico-select-lyric-line" value="Ooka thunaiye un sol valiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeakkam thandhaal ennaaven
<input type="checkbox" class="lyrico-select-lyric-line" value="Yeakkam thandhaal ennaaven"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaakkum karamae vizhigal mooda
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaakkum karamae vizhigal mooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Engae selven ini naanae
<input type="checkbox" class="lyrico-select-lyric-line" value="Engae selven ini naanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vazhi illa kaattil tholainthena
<input type="checkbox" class="lyrico-select-lyric-line" value="Vazhi illa kaattil tholainthena"/>
</div>
<div class="lyrico-lyrics-wrapper">Illai…vazhi thaedi vaanam thoduvena
<input type="checkbox" class="lyrico-select-lyric-line" value="Illai…vazhi thaedi vaanam thoduvena"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ilaiyellaam vizhunthaalum
<input type="checkbox" class="lyrico-select-lyric-line" value="Ilaiyellaam vizhunthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Maram ingu veezhaadhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Maram ingu veezhaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilavenil adhu neethaanae
<input type="checkbox" class="lyrico-select-lyric-line" value="Ilavenil adhu neethaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Irul unnai soozhnthaalum
<input type="checkbox" class="lyrico-select-lyric-line" value="Irul unnai soozhnthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Irulaagi pogaadhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Irulaagi pogaadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oli yaenthu
<input type="checkbox" class="lyrico-select-lyric-line" value="Oli yaenthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi pagalae nee
<input type="checkbox" class="lyrico-select-lyric-line" value="Adi pagalae nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kaalangal unnodu
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaalangal unnodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaayangal kaatrodu
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaayangal kaatrodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa vaa vaazhnthu paarkalam
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaa vaa vaazhnthu paarkalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogaatha aazhangal
<input type="checkbox" class="lyrico-select-lyric-line" value="Pogaatha aazhangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Aagaaya thoorangal povom
<input type="checkbox" class="lyrico-select-lyric-line" value="Aagaaya thoorangal povom"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa vendru vaanneri
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaa vendru vaanneri"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vaazh kondu saithaalum
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaazh kondu saithaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Poradum veraagi
<input type="checkbox" class="lyrico-select-lyric-line" value="Poradum veraagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa vaa nimirndhu nirkkava
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaa vaa nimirndhu nirkkava"/>
</div>
<div class="lyrico-lyrics-wrapper">Poo pola veezhaamal
<input type="checkbox" class="lyrico-select-lyric-line" value="Poo pola veezhaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ver pola nee maari
<input type="checkbox" class="lyrico-select-lyric-line" value="Ver pola nee maari"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa vaa vaagai nee soodava
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaa vaa vaagai nee soodava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Pudhirgal yaavum vidaigal aagi
<input type="checkbox" class="lyrico-select-lyric-line" value="Pudhirgal yaavum vidaigal aagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Udalil siragai tharugiradhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Udalil siragai tharugiradhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidaigal thandha thisaiyil indru
<input type="checkbox" class="lyrico-select-lyric-line" value="Vidaigal thandha thisaiyil indru"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhidhaai paravai parakkiradhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Pudhidhaai paravai parakkiradhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Pirakkaadha vidiyal kidaiyaadhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Pirakkaadha vidiyal kidaiyaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pennae
<input type="checkbox" class="lyrico-select-lyric-line" value="Pennae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirakkaadha thisaigal kidaiyaadhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Thirakkaadha thisaigal kidaiyaadhu"/>
</div>
</pre>