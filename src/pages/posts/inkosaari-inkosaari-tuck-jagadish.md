---
title: "inkosaari inkosaari song lyrics"
album: "Tuck Jagadish"
artist: "S. Thaman"
lyricist: "Chaitanya Prasad"
director: "Shiva Nirvana"
path: "/albums/tuck-jagadish-lyrics"
song: "Inkosaari Inkosaari"
image: ../../images/albumart/tuck-jagadish.jpg
date: 2021-09-10
lang: telugu
youtubeLink: "https://www.youtube.com/embed/eVYhs9x1XMI"
type: "love"
singers:
  - Shreya Ghoshal
  - Kaala Bhairava
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Inkkosaari Inkosaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inkkosaari Inkosaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Yedhalo Cheri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Yedhalo Cheri"/>
</div>
<div class="lyrico-lyrics-wrapper">Mallosaari Mallosaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mallosaari Mallosaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Pilavaalandhi Nuvu Prathisaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pilavaalandhi Nuvu Prathisaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasuke Modhalaindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasuke Modhalaindhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Modhati Maatallo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Modhati Maatallo"/>
</div>
<div class="lyrico-lyrics-wrapper">Vayasuke Varadhidhe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayasuke Varadhidhe "/>
</div>
<div class="lyrico-lyrics-wrapper">Valupu Vanallo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valupu Vanallo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudhuruga Nilavadhe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudhuruga Nilavadhe "/>
</div>
<div class="lyrico-lyrics-wrapper">Chilipi Oohallo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chilipi Oohallo"/>
</div>
<div class="lyrico-lyrics-wrapper">Thgadhanee Telisina 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thgadhanee Telisina "/>
</div>
<div class="lyrico-lyrics-wrapper">Chivari Haddhullo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chivari Haddhullo"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Raadharilo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Raadharilo "/>
</div>
<div class="lyrico-lyrics-wrapper">Godharilaa Vachavemo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Godharilaa Vachavemo"/>
</div>
<div class="lyrico-lyrics-wrapper">Neerendallo Naa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neerendallo Naa "/>
</div>
<div class="lyrico-lyrics-wrapper">Gundello Punnagala Puchavemo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundello Punnagala Puchavemo"/>
</div>
<div class="lyrico-lyrics-wrapper">Yegaresey Oohalne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yegaresey Oohalne"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheripesey Haddhulne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheripesey Haddhulne"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaateddham Dhikkulne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaateddham Dhikkulne"/>
</div>
<div class="lyrico-lyrics-wrapper">Chuseddham Chukkalne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chuseddham Chukkalne"/>
</div>
<div class="lyrico-lyrics-wrapper">Yegaresey Oohalne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yegaresey Oohalne"/>
</div>
<div class="lyrico-lyrics-wrapper">Yegaresey Oohalne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yegaresey Oohalne"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheripesey Haddhulne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheripesey Haddhulne"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheripesey Haddhulne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheripesey Haddhulne"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaateddham Dhikkulne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaateddham Dhikkulne"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaateddham Dhikkulne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaateddham Dhikkulne"/>
</div>
<div class="lyrico-lyrics-wrapper">Chuseddham Chukkalne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chuseddham Chukkalne"/>
</div>
<div class="lyrico-lyrics-wrapper">Chuseddham Chukkalne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chuseddham Chukkalne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kavvisthavu Neevu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavvisthavu Neevu "/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Kanti Baanalatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kanti Baanalatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Gunde Alladela 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gunde Alladela "/>
</div>
<div class="lyrico-lyrics-wrapper">Navvisthavu Neevu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Navvisthavu Neevu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Konte Konaalatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Konte Konaalatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Chanti Pilladila Kanne Eedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chanti Pilladila Kanne Eedu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolaatamadindhi Kantipaapalo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolaatamadindhi Kantipaapalo "/>
</div>
<div class="lyrico-lyrics-wrapper">Ninne Dhaachindi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninne Dhaachindi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnaleni Ibbandhi Baavundhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnaleni Ibbandhi Baavundhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu Kori Rammantundhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu Kori Rammantundhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Raadharilo Godharila Vachavemo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Raadharilo Godharila Vachavemo"/>
</div>
<div class="lyrico-lyrics-wrapper">Neerendallo Naa Gundello 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neerendallo Naa Gundello "/>
</div>
<div class="lyrico-lyrics-wrapper">Punnagalaa Poochavemo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Punnagalaa Poochavemo"/>
</div>
<div class="lyrico-lyrics-wrapper">Yegaresey Oohalne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yegaresey Oohalne"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheripesey Haddhulne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheripesey Haddhulne"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaateddham Dhikkulne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaateddham Dhikkulne"/>
</div>
<div class="lyrico-lyrics-wrapper">Chuseddham Chukkalne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chuseddham Chukkalne"/>
</div>
<div class="lyrico-lyrics-wrapper">Yegaresey Oohalne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yegaresey Oohalne"/>
</div>
<div class="lyrico-lyrics-wrapper">Yegaresey Oohalne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yegaresey Oohalne"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheripesey Haddhulne 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheripesey Haddhulne "/>
</div>
<div class="lyrico-lyrics-wrapper">Cheripesey Haddhulne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheripesey Haddhulne"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaateddham Dhikkulne 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaateddham Dhikkulne "/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaateddham Dhikkulne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaateddham Dhikkulne"/>
</div>
<div class="lyrico-lyrics-wrapper">Chuseddham Chukkalne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chuseddham Chukkalne"/>
</div>
<div class="lyrico-lyrics-wrapper">Chuseddham Chukkalne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chuseddham Chukkalne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inkkosaari Inkosaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inkkosaari Inkosaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Yedhalo Cheri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Yedhalo Cheri"/>
</div>
<div class="lyrico-lyrics-wrapper">Mallosaari Mallosaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mallosaari Mallosaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Pilavaalandhi Nuvu Prathisaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pilavaalandhi Nuvu Prathisaari"/>
</div>
</pre>
