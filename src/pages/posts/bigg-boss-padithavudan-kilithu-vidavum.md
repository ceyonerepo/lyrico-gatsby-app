---
title: "big boss song lyrics"
album: "PAdithavudan Kilithu Vidavum"
artist: "Niro Prabakaran"
lyricist: "Maa - Sha"
director: "Hari Uthraa"
path: "/albums/padithavudan-kilithu-vidavum-lyrics"
song: "Bigg Boss"
image: ../../images/albumart/padithavudan-kilithu-vidavum.jpg
date: 2018-09-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/R03VCPJm364"
type: "mass"
singers:
  - Shazz Nsr
  - Niro Prabhakaran
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">inky pinky pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inky pinky pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">therinchudhan odiko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="therinchudhan odiko"/>
</div>
<div class="lyrico-lyrics-wrapper">rallu sillu bagulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rallu sillu bagulu"/>
</div>
<div class="lyrico-lyrics-wrapper">kiliyadhan odikko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kiliyadhan odikko"/>
</div>
<div class="lyrico-lyrics-wrapper">kaatu theeyai koluthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatu theeyai koluthi"/>
</div>
<div class="lyrico-lyrics-wrapper">parandhu dhan odikko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="parandhu dhan odikko"/>
</div>
<div class="lyrico-lyrics-wrapper">maanja noola vechi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maanja noola vechi"/>
</div>
<div class="lyrico-lyrics-wrapper">deala dhan mudichiko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="deala dhan mudichiko"/>
</div>
<div class="lyrico-lyrics-wrapper">odu odu therika than odu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="odu odu therika than odu"/>
</div>
<div class="lyrico-lyrics-wrapper">thala therika therichu dhan odu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thala therika therichu dhan odu"/>
</div>
<div class="lyrico-lyrics-wrapper">kattan katura sathuranga aatam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kattan katura sathuranga aatam"/>
</div>
<div class="lyrico-lyrics-wrapper">routa podra kalavara kootam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="routa podra kalavara kootam"/>
</div>
<div class="lyrico-lyrics-wrapper">therika therika nee odu odu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="therika therika nee odu odu"/>
</div>
<div class="lyrico-lyrics-wrapper">odavum muiyama ahhnn
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="odavum muiyama ahhnn"/>
</div>
<div class="lyrico-lyrics-wrapper">iruttukula maatikita 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iruttukula maatikita "/>
</div>
<div class="lyrico-lyrics-wrapper">indu idukula oduvom getha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indu idukula oduvom getha"/>
</div>
<div class="lyrico-lyrics-wrapper">kaatukula sikadha maanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatukula sikadha maanu"/>
</div>
<div class="lyrico-lyrics-wrapper">thorathi odum singam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thorathi odum singam"/>
</div>
<div class="lyrico-lyrics-wrapper">puli kootam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puli kootam"/>
</div>
<div class="lyrico-lyrics-wrapper">maamam yaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maamam yaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">machan yaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="machan yaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">maatuna soli mudinjidhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maatuna soli mudinjidhu"/>
</div>
<div class="lyrico-lyrics-wrapper">odavum mudiyadhu ahhnn
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="odavum mudiyadhu ahhnn"/>
</div>
<div class="lyrico-lyrics-wrapper">oliyavum mudiyadhu ahhnn
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oliyavum mudiyadhu ahhnn"/>
</div>
<div class="lyrico-lyrics-wrapper">iruttu thopula sikadha maapu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iruttu thopula sikadha maapu"/>
</div>
<div class="lyrico-lyrics-wrapper">odraa bigg bossu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="odraa bigg bossu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">inky pinky pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inky pinky pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">therinchudhan odiko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="therinchudhan odiko"/>
</div>
<div class="lyrico-lyrics-wrapper">rallu sillu bagulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rallu sillu bagulu"/>
</div>
<div class="lyrico-lyrics-wrapper">kiliyadhan odikko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kiliyadhan odikko"/>
</div>
<div class="lyrico-lyrics-wrapper">kaatu theeyai koluthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatu theeyai koluthi"/>
</div>
<div class="lyrico-lyrics-wrapper">parandhu dhan odikko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="parandhu dhan odikko"/>
</div>
<div class="lyrico-lyrics-wrapper">maanja noola vechi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maanja noola vechi"/>
</div>
<div class="lyrico-lyrics-wrapper">deala dhan mudichiko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="deala dhan mudichiko"/>
</div>
<div class="lyrico-lyrics-wrapper">odra odra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="odra odra"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">virru virru nu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="virru virru nu"/>
</div>
<div class="lyrico-lyrics-wrapper">kilichu dhan odu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kilichu dhan odu"/>
</div>
<div class="lyrico-lyrics-wrapper">mersal aaki merati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mersal aaki merati"/>
</div>
<div class="lyrico-lyrics-wrapper">dhan odu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhan odu"/>
</div>
<div class="lyrico-lyrics-wrapper">sodakku podura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sodakku podura"/>
</div>
<div class="lyrico-lyrics-wrapper">koku maaku aatam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koku maaku aatam"/>
</div>
<div class="lyrico-lyrics-wrapper">uruti maratura 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uruti maratura "/>
</div>
<div class="lyrico-lyrics-wrapper">silu vandu kootam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="silu vandu kootam"/>
</div>
<div class="lyrico-lyrics-wrapper">vettai aadum vetai dhan da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vettai aadum vetai dhan da"/>
</div>
<div class="lyrico-lyrics-wrapper">bongu illa aatam dhan da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bongu illa aatam dhan da"/>
</div>
<div class="lyrico-lyrics-wrapper">sarru burru nu odu odu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sarru burru nu odu odu"/>
</div>
<div class="lyrico-lyrics-wrapper">sillaki dummaiki 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sillaki dummaiki "/>
</div>
<div class="lyrico-lyrics-wrapper">sillaki dummaiki 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sillaki dummaiki "/>
</div>
<div class="lyrico-lyrics-wrapper">kaatu kulla sikki kitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatu kulla sikki kitta"/>
</div>
<div class="lyrico-lyrics-wrapper">sangi mangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sangi mangi"/>
</div>
<div class="lyrico-lyrics-wrapper">nee sikkuna kuska
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee sikkuna kuska"/>
</div>
<div class="lyrico-lyrics-wrapper">erra thokku soru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="erra thokku soru"/>
</div>
<div class="lyrico-lyrics-wrapper">left to the right
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="left to the right"/>
</div>
<div class="lyrico-lyrics-wrapper">right to the left
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="right to the left"/>
</div>
<div class="lyrico-lyrics-wrapper">kakki kitta matadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kakki kitta matadha"/>
</div>
<div class="lyrico-lyrics-wrapper">kakki kitta matadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kakki kitta matadha"/>
</div>
<div class="lyrico-lyrics-wrapper">kola kuthu podra mappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kola kuthu podra mappu"/>
</div>
<div class="lyrico-lyrics-wrapper">gummanguthu kuthu da beepu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gummanguthu kuthu da beepu"/>
</div>
<div class="lyrico-lyrics-wrapper">vaikadha aappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaikadha aappu"/>
</div>
<div class="lyrico-lyrics-wrapper">sikadha gappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sikadha gappu"/>
</div>
<div class="lyrico-lyrics-wrapper">thunnadha soupu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thunnadha soupu"/>
</div>
<div class="lyrico-lyrics-wrapper">podadha soapu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="podadha soapu"/>
</div>
<div class="lyrico-lyrics-wrapper">dhudu vaangi dhudu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhudu vaangi dhudu "/>
</div>
<div class="lyrico-lyrics-wrapper">vaangi voteu podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaangi voteu podu"/>
</div>
<div class="lyrico-lyrics-wrapper">voteu pottu voteu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="voteu pottu voteu"/>
</div>
<div class="lyrico-lyrics-wrapper">pottu routea podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pottu routea podu"/>
</div>
<div class="lyrico-lyrics-wrapper">routea pottu routea
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="routea pottu routea"/>
</div>
<div class="lyrico-lyrics-wrapper">pottu sketcha podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pottu sketcha podu"/>
</div>
<div class="lyrico-lyrics-wrapper">sketchu pottu sketchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sketchu pottu sketchu"/>
</div>
<div class="lyrico-lyrics-wrapper">pottu aataiya podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pottu aataiya podu"/>
</div>
<div class="lyrico-lyrics-wrapper">noola pottu noola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="noola pottu noola"/>
</div>
<div class="lyrico-lyrics-wrapper">pottu deala podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pottu deala podu"/>
</div>
<div class="lyrico-lyrics-wrapper">deala pottu deala 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="deala pottu deala "/>
</div>
<div class="lyrico-lyrics-wrapper">pottu noola vidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pottu noola vidu"/>
</div>
<div class="lyrico-lyrics-wrapper">moota kattu moota
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moota kattu moota"/>
</div>
<div class="lyrico-lyrics-wrapper">kattu seatu kattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kattu seatu kattu"/>
</div>
<div class="lyrico-lyrics-wrapper">seatu kattu voota
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seatu kattu voota"/>
</div>
<div class="lyrico-lyrics-wrapper">kattu nadaiya kattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kattu nadaiya kattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">inky pinky pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inky pinky pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">therinchudhan odiko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="therinchudhan odiko"/>
</div>
<div class="lyrico-lyrics-wrapper">rallu sillu bagulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rallu sillu bagulu"/>
</div>
<div class="lyrico-lyrics-wrapper">kiliyadhan odikko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kiliyadhan odikko"/>
</div>
<div class="lyrico-lyrics-wrapper">kaatu theeyai koluthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatu theeyai koluthi"/>
</div>
<div class="lyrico-lyrics-wrapper">parandhu dhan odikko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="parandhu dhan odikko"/>
</div>
<div class="lyrico-lyrics-wrapper">maanja noola vechi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maanja noola vechi"/>
</div>
<div class="lyrico-lyrics-wrapper">deala dhan mudichiko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="deala dhan mudichiko"/>
</div>
<div class="lyrico-lyrics-wrapper">odu odu therika than odu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="odu odu therika than odu"/>
</div>
<div class="lyrico-lyrics-wrapper">thala therika therichu dhan odu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thala therika therichu dhan odu"/>
</div>
<div class="lyrico-lyrics-wrapper">odu odu therika than odu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="odu odu therika than odu"/>
</div>
<div class="lyrico-lyrics-wrapper">thala therika therichu dhan odu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thala therika therichu dhan odu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">odavum muiyama ahhnn
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="odavum muiyama ahhnn"/>
</div>
<div class="lyrico-lyrics-wrapper">iruttukula maatikita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iruttukula maatikita"/>
</div>
<div class="lyrico-lyrics-wrapper">odavum muiyama ahhnn
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="odavum muiyama ahhnn"/>
</div>
<div class="lyrico-lyrics-wrapper">iruttukula maatikita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iruttukula maatikita"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vote podu therikathan podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vote podu therikathan podu"/>
</div>
<div class="lyrico-lyrics-wrapper">podu thala therika voteu podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="podu thala therika voteu podu"/>
</div>
<div class="lyrico-lyrics-wrapper">therika than podu podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="therika than podu podu"/>
</div>
<div class="lyrico-lyrics-wrapper">thalai therikka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalai therikka "/>
</div>
</pre>
