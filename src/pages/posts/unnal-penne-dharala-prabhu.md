---
title: 'unnal penne song lyrics'
album: 'Dharala Prabhu'
artist: 'Inno Genga'
lyricist: 'Vignesh Shivan'
director: 'Krishna Marimuthu'
path: '/albums/dharala-prabhu-song-lyrics'
song: 'Unnal Penne'
image: ../../images/albumart/dharala-prabhu.jpg
date: 2020-03-13
lang: tamil
singers: 
- Inno Genga
youtubeLink: 'https://www.youtube.com/embed/YK0RfHjCjLk'
type: 'love'
---

<pre class="lyrics-native">
    Coming Soon
</pre>

<pre class="lyrics-english">

  <div class="lyrico-lyrics-wrapper">Vaa vandhu saindhu kondu
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaa vandhu saindhu kondu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanavillai paarpomaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaanavillai paarpomaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Namai paarthu sirikkum nilavil
<input type="checkbox" class="lyrico-select-lyric-line" value="Namai paarthu sirikkum nilavil"/>
</div>
<div class="lyrico-lyrics-wrapper">Theanai konjam serpoma
<input type="checkbox" class="lyrico-select-lyric-line" value="Theanai konjam serpoma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thaniyaga thavitha kaalam
<input type="checkbox" class="lyrico-select-lyric-line" value="Thaniyaga thavitha kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalli ponadhu unnaalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Thalli ponadhu unnaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee vandhathum unai thandhathum
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee vandhathum unai thandhathum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadanthathae ini podhumae
<input type="checkbox" class="lyrico-select-lyric-line" value="Nadanthathae ini podhumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kaatrin vaasanai maaruthae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaatrin vaasanai maaruthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaindhu boomiyil thoorudhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaindhu boomiyil thoorudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanda kanavellam kooduthae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kanda kanavellam kooduthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhkai maaruthae
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaazhkai maaruthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kaatrin vaasanai maaruthae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaatrin vaasanai maaruthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaindhu boomiyil thoorudhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaindhu boomiyil thoorudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanda kanavellam kooduthae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kanda kanavellam kooduthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ondru seruthae
<input type="checkbox" class="lyrico-select-lyric-line" value="Ondru seruthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Unnaal unnaal pennae
<input type="checkbox" class="lyrico-select-lyric-line" value="Unnaal unnaal pennae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaamae ellaamae
<input type="checkbox" class="lyrico-select-lyric-line" value="Ellaamae ellaamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagaai theriyuthae
<input type="checkbox" class="lyrico-select-lyric-line" value="Azhagaai theriyuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaam theriyuthae
<input type="checkbox" class="lyrico-select-lyric-line" value="Ellaam theriyuthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">En mel un maeni urasum
<input type="checkbox" class="lyrico-select-lyric-line" value="En mel un maeni urasum"/>
</div>
<div class="lyrico-lyrics-wrapper">Annodi anal parakkum
<input type="checkbox" class="lyrico-select-lyric-line" value="Annodi anal parakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Endru theriyuthae
<input type="checkbox" class="lyrico-select-lyric-line" value="Endru theriyuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaam puriyuthae
<input type="checkbox" class="lyrico-select-lyric-line" value="Ellaam puriyuthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kodhaiyin bodhaiyil
<input type="checkbox" class="lyrico-select-lyric-line" value="Kodhaiyin bodhaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan midhandhu pogiren
<input type="checkbox" class="lyrico-select-lyric-line" value="Naan midhandhu pogiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Ovvondrum rendaaga
<input type="checkbox" class="lyrico-select-lyric-line" value="Ovvondrum rendaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kann munnae nindraada paarkiren
<input type="checkbox" class="lyrico-select-lyric-line" value="Kann munnae nindraada paarkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Pakkathil varumbodhu pennae
<input type="checkbox" class="lyrico-select-lyric-line" value="Pakkathil varumbodhu pennae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vekkathai vidukka thevai illaiyae
<input type="checkbox" class="lyrico-select-lyric-line" value="Vekkathai vidukka thevai illaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Neram illaiyae
<input type="checkbox" class="lyrico-select-lyric-line" value="Neram illaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un vaasam varumbodhu penane
<input type="checkbox" class="lyrico-select-lyric-line" value="Un vaasam varumbodhu penane"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu pola enakku endru aanadhilaiyae
<input type="checkbox" class="lyrico-select-lyric-line" value="Idhu pola enakku endru aanadhilaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanadhilaiyae…
<input type="checkbox" class="lyrico-select-lyric-line" value="Aanadhilaiyae…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kaatrin vaasanai maaruthae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaatrin vaasanai maaruthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaindhu boomiyil thoorudhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaindhu boomiyil thoorudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanda kanavellam kooduthae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kanda kanavellam kooduthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhkai maaruthae
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaazhkai maaruthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kaatrin vaasanai maaruthae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaatrin vaasanai maaruthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaindhu boomiyil thoorudhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaindhu boomiyil thoorudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanda kanavellam kooduthae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kanda kanavellam kooduthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ondru seruthae
<input type="checkbox" class="lyrico-select-lyric-line" value="Ondru seruthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Unnaal unnaal pennae
<input type="checkbox" class="lyrico-select-lyric-line" value="Unnaal unnaal pennae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaamae ellaamae
<input type="checkbox" class="lyrico-select-lyric-line" value="Ellaamae ellaamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagaai theriyuthae
<input type="checkbox" class="lyrico-select-lyric-line" value="Azhagaai theriyuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaam theriyuthae
<input type="checkbox" class="lyrico-select-lyric-line" value="Ellaam theriyuthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">En mel un maeni urasum
<input type="checkbox" class="lyrico-select-lyric-line" value="En mel un maeni urasum"/>
</div>
<div class="lyrico-lyrics-wrapper">Annodi anal parakkum
<input type="checkbox" class="lyrico-select-lyric-line" value="Annodi anal parakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Endru theriyuthae
<input type="checkbox" class="lyrico-select-lyric-line" value="Endru theriyuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaam puriyuthae
<input type="checkbox" class="lyrico-select-lyric-line" value="Ellaam puriyuthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vaa vandhu saindhu kondu
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaa vandhu saindhu kondu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanavillai paarpoma
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaanavillai paarpoma"/>
</div>
<div class="lyrico-lyrics-wrapper">Namai paarthu sirikkum nilavil
<input type="checkbox" class="lyrico-select-lyric-line" value="Namai paarthu sirikkum nilavil"/>
</div>
<div class="lyrico-lyrics-wrapper">Theanai konjam serpoma
<input type="checkbox" class="lyrico-select-lyric-line" value="Theanai konjam serpoma"/>
</div>
</pre>