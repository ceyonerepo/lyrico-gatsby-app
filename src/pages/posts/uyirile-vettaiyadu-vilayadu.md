---
title: 'uyirile enadhu song lyrics'
album: 'Vettaiyadu Vilayadu'
artist: 'Harris Jayaraj'
lyricist: 'Thamarai'
director: 'Gowtham Vasudev Menon'
path: '/albums/vettaiyadu-vilayadu-song-lyrics'
song: 'Uyirile Enadhu Uyirile'
image: ../../images/albumart/vettaiyadu-vilayadu.jpg
date: 2006-08-25
lang: tamil
singers: 
- Mahalakshmi
- Srinivas
youtubeLink: "https://www.youtube.com/embed/hqwKIBdJfTc"
type: 'sad'
---

<pre class="lyrics-native">
    Coming Soon
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Uyirilae enathu uyirilae
<input type="checkbox" class="lyrico-select-lyric-line" value="Uyirilae enathu uyirilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru thuli theeyai utharinaai
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru thuli theeyai utharinaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Unarvilae enathu unarvilae
<input type="checkbox" class="lyrico-select-lyric-line" value="Unarvilae enathu unarvilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Anuvena udainthu sitharinai
<input type="checkbox" class="lyrico-select-lyric-line" value="Anuvena udainthu sitharinai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Yen ennai maruthu pogiraai
<input type="checkbox" class="lyrico-select-lyric-line" value="Yen ennai maruthu pogiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanal neerodu sergiraai
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaanal neerodu sergiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodutha thaai sonna idhayathai
<input type="checkbox" class="lyrico-select-lyric-line" value="Kodutha thaai sonna idhayathai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiruppi naan vaanga maatenae
<input type="checkbox" class="lyrico-select-lyric-line" value="Thiruppi naan vaanga maatenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Uyirilae enathu uyirilae
<input type="checkbox" class="lyrico-select-lyric-line" value="Uyirilae enathu uyirilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru thuli theeyai utharinaai
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru thuli theeyai utharinaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Unarvilae enathu unarvilae
<input type="checkbox" class="lyrico-select-lyric-line" value="Unarvilae enathu unarvilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Anuvena udainthu sitharinai
<input type="checkbox" class="lyrico-select-lyric-line" value="Anuvena udainthu sitharinai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Aruginil ulla thooramae
<input type="checkbox" class="lyrico-select-lyric-line" value="Aruginil ulla thooramae"/>
</div>
<div class="lyrico-lyrics-wrapper">Alai kadal theendum vaanamae
<input type="checkbox" class="lyrico-select-lyric-line" value="Alai kadal theendum vaanamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nesikka nenjam rendu
<input type="checkbox" class="lyrico-select-lyric-line" value="Nesikka nenjam rendu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pothatha pothatha nee sollu
<input type="checkbox" class="lyrico-select-lyric-line" value="Pothatha pothatha nee sollu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nesamum rendam murai
<input type="checkbox" class="lyrico-select-lyric-line" value="Nesamum rendam murai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaratha koodaatha nee sollu
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaaratha koodaatha nee sollu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ithu nadanthida kuudumo
<input type="checkbox" class="lyrico-select-lyric-line" value="Ithu nadanthida kuudumo"/>
</div>
<div class="lyrico-lyrics-wrapper">Iru thurvangal seruma
<input type="checkbox" class="lyrico-select-lyric-line" value="Iru thurvangal seruma"/>
</div>
<div class="lyrico-lyrics-wrapper">Ucharithu neeyum vilaga
<input type="checkbox" class="lyrico-select-lyric-line" value="Ucharithu neeyum vilaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Thathalithu naanum maruga
<input type="checkbox" class="lyrico-select-lyric-line" value="Thathalithu naanum maruga"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna seiveno…
<input type="checkbox" class="lyrico-select-lyric-line" value="Enna seiveno…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Uyirilae enathu uyirilae
<input type="checkbox" class="lyrico-select-lyric-line" value="Uyirilae enathu uyirilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru thuli theeyai utharinaai
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru thuli theeyai utharinaai"/>
</div>
  <div class="lyrico-lyrics-wrapper">Unarvilae enathu unarvilae
<input type="checkbox" class="lyrico-select-lyric-line" value="Unarvilae enathu unarvilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Anuvena udainthu sitharinai
<input type="checkbox" class="lyrico-select-lyric-line" value="Anuvena udainthu sitharinai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Yetho ondru ennai thadukuthae
<input type="checkbox" class="lyrico-select-lyric-line" value="Yetho ondru ennai thadukuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pen thaanae nee endru muraikuthae
<input type="checkbox" class="lyrico-select-lyric-line" value="Pen thaanae nee endru muraikuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennullae kaayangal
<input type="checkbox" class="lyrico-select-lyric-line" value="Ennullae kaayangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaraamal theeraamal nindrenae
<input type="checkbox" class="lyrico-select-lyric-line" value="Aaraamal theeraamal nindrenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Visiriyaai un kaigal
<input type="checkbox" class="lyrico-select-lyric-line" value="Visiriyaai un kaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthaalum vangamal sendrenae
<input type="checkbox" class="lyrico-select-lyric-line" value="Vanthaalum vangamal sendrenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vaa vandhu ennai sernthidu
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaa vandhu ennai sernthidu"/>
</div>
<div class="lyrico-lyrics-wrapper">En tholgalil theinthidu
<input type="checkbox" class="lyrico-select-lyric-line" value="En tholgalil theinthidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Solla vanthen solli mudithen
<input type="checkbox" class="lyrico-select-lyric-line" value="Solla vanthen solli mudithen"/>
</div>
<div class="lyrico-lyrics-wrapper">Varum dhisai paarthu iruppen
<input type="checkbox" class="lyrico-select-lyric-line" value="Varum dhisai paarthu iruppen"/>
</div>
<div class="lyrico-lyrics-wrapper">Naatkal ponaalum…
<input type="checkbox" class="lyrico-select-lyric-line" value="Naatkal ponaalum…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Hmm..mmm..mmmm..
<input type="checkbox" class="lyrico-select-lyric-line" value="Hmm..mmm..mmmm.."/>
</div>
<div class="lyrico-lyrics-wrapper">Hmm..mmm..mmmm…mmm…
<input type="checkbox" class="lyrico-select-lyric-line" value="Hmm..mmm..mmmm…mmm…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Yen ennai maruthu pogiraai
<input type="checkbox" class="lyrico-select-lyric-line" value="Yen ennai maruthu pogiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanal neerodu sergiraai
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaanal neerodu sergiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodutha thaai sonna idhayathai
<input type="checkbox" class="lyrico-select-lyric-line" value="Kodutha thaai sonna idhayathai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiruppi naan vaanga maatenae
<input type="checkbox" class="lyrico-select-lyric-line" value="Thiruppi naan vaanga maatenae"/>
</div>
</pre>