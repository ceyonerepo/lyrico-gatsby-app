---
title: "maya maya song lyrics"
album: "Neeya 2"
artist: "Shabir"
lyricist: "Mohanrajan"
director: "L. Suresh"
path: "/albums/neeya-2-lyrics"
song: "Maya Maya"
image: ../../images/albumart/neeya-2.jpg
date: 2019-05-24
lang: tamil
youtubeLink: "https://www.youtube.com/embed/WcoVK7VJ4wc"
type: "love"
singers:
  - Javed Ali
  - Shashaa Tirupati
  - Janani Venkat
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">maya maya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maya maya"/>
</div>
<div class="lyrico-lyrics-wrapper">mayak kanalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mayak kanalae"/>
</div>
<div class="lyrico-lyrics-wrapper">maya maya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maya maya"/>
</div>
<div class="lyrico-lyrics-wrapper">mayam seidhaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mayam seidhaalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">idhazh padaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhazh padaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">vidathe vaa vaa enai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidathe vaa vaa enai"/>
</div>
<div class="lyrico-lyrics-wrapper">alli chellu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alli chellu"/>
</div>
<div class="lyrico-lyrics-wrapper">idam tharaathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idam tharaathae"/>
</div>
<div class="lyrico-lyrics-wrapper">kedaathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kedaathae"/>
</div>
<div class="lyrico-lyrics-wrapper">vaa vaa yennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaa vaa yennai"/>
</div>
<div class="lyrico-lyrics-wrapper">killi kollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="killi kollu"/>
</div>
<div class="lyrico-lyrics-wrapper">en ullam thiranthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en ullam thiranthu"/>
</div>
<div class="lyrico-lyrics-wrapper">naan unai vanguven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan unai vanguven"/>
</div>
<div class="lyrico-lyrics-wrapper">imaigal moodiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="imaigal moodiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">uyirena kathiduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyirena kathiduven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">alae alae alae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alae alae alae"/>
</div>
<div class="lyrico-lyrics-wrapper">alaepolae paayen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alaepolae paayen"/>
</div>
<div class="lyrico-lyrics-wrapper">malae malae malae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malae malae malae"/>
</div>
<div class="lyrico-lyrics-wrapper">maarmeethu meyyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maarmeethu meyyae"/>
</div>
<div class="lyrico-lyrics-wrapper">valae valae valae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valae valae valae"/>
</div>
<div class="lyrico-lyrics-wrapper">valae veesa vaayen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valae veesa vaayen"/>
</div>
<div class="lyrico-lyrics-wrapper">yelae yelae yelae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yelae yelae yelae"/>
</div>
<div class="lyrico-lyrics-wrapper">yegaantha dheeran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yegaantha dheeran"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ninaivai thirudugindraaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninaivai thirudugindraaye"/>
</div>
<div class="lyrico-lyrics-wrapper">unarvai varudugindraayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unarvai varudugindraayae"/>
</div>
<div class="lyrico-lyrics-wrapper">edhilum nee dhane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edhilum nee dhane"/>
</div>
<div class="lyrico-lyrics-wrapper">edhuvum nee dhane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edhuvum nee dhane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">maya maya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maya maya"/>
</div>
<div class="lyrico-lyrics-wrapper">mayak kanalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mayak kanalae"/>
</div>
<div class="lyrico-lyrics-wrapper">maya maya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maya maya"/>
</div>
<div class="lyrico-lyrics-wrapper">mayam seidhaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mayam seidhaalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">maya maya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maya maya"/>
</div>
<div class="lyrico-lyrics-wrapper">mayak kanalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mayak kanalae"/>
</div>
<div class="lyrico-lyrics-wrapper">maya maya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maya maya"/>
</div>
<div class="lyrico-lyrics-wrapper">mayam seidhaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mayam seidhaalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">unnai polavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai polavum"/>
</div>
<div class="lyrico-lyrics-wrapper">yenai polavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yenai polavum"/>
</div>
<div class="lyrico-lyrics-wrapper">manil sernthavar yaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manil sernthavar yaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">janman kodi dhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="janman kodi dhan"/>
</div>
<div class="lyrico-lyrics-wrapper">annil vaazhume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="annil vaazhume"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjai vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjai vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">nee kaelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee kaelu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">idhayam thirandhu viduvomae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhayam thirandhu viduvomae"/>
</div>
<div class="lyrico-lyrics-wrapper">ulagai marandhu viduvomae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulagai marandhu viduvomae"/>
</div>
<div class="lyrico-lyrics-wrapper">iravum pagalumena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iravum pagalumena"/>
</div>
<div class="lyrico-lyrics-wrapper">pagalum iravumena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pagalum iravumena"/>
</div>
<div class="lyrico-lyrics-wrapper">uravil kalandu viduvome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uravil kalandu viduvome"/>
</div>
<div class="lyrico-lyrics-wrapper">piriyaamal enakul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="piriyaamal enakul"/>
</div>
<div class="lyrico-lyrics-wrapper">ini neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ini neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">unakul ini naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unakul ini naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">oru uyiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru uyiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">vaazhndhirupom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaazhndhirupom"/>
</div>
<div class="lyrico-lyrics-wrapper">pririyaamal enakul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pririyaamal enakul"/>
</div>
<div class="lyrico-lyrics-wrapper">ini neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ini neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">unakul ini naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unakul ini naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">oru uyiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru uyiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">vaazhndhirupom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaazhndhirupom"/>
</div>
<div class="lyrico-lyrics-wrapper">ennai thalli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennai thalli"/>
</div>
<div class="lyrico-lyrics-wrapper">pogirai mogathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pogirai mogathil"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjai koigirai vegathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjai koigirai vegathil"/>
</div>
<div class="lyrico-lyrics-wrapper">ipothume epothume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ipothume epothume"/>
</div>
<div class="lyrico-lyrics-wrapper">en kaadhal unoda 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kaadhal unoda "/>
</div>
<div class="lyrico-lyrics-wrapper">neengamal vaazhumadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neengamal vaazhumadi"/>
</div>
<div class="lyrico-lyrics-wrapper">neengamal vaazhumadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neengamal vaazhumadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">maya maya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maya maya"/>
</div>
<div class="lyrico-lyrics-wrapper">mayak kanalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mayak kanalae"/>
</div>
<div class="lyrico-lyrics-wrapper">maya maya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maya maya"/>
</div>
<div class="lyrico-lyrics-wrapper">mayam seidhaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mayam seidhaalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">maya maya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maya maya"/>
</div>
<div class="lyrico-lyrics-wrapper">mayak kanalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mayak kanalae"/>
</div>
<div class="lyrico-lyrics-wrapper">maya maya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maya maya"/>
</div>
<div class="lyrico-lyrics-wrapper">mayam seidhaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mayam seidhaalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">unai ootri nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unai ootri nee"/>
</div>
<div class="lyrico-lyrics-wrapper">yennai maatrinaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yennai maatrinaai"/>
</div>
<div class="lyrico-lyrics-wrapper">inbam kolgiren naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inbam kolgiren naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">yennai vangi nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yennai vangi nee"/>
</div>
<div class="lyrico-lyrics-wrapper">kaadhal neetinaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadhal neetinaai"/>
</div>
<div class="lyrico-lyrics-wrapper">anbe ver ena venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anbe ver ena venum"/>
</div>
<div class="lyrico-lyrics-wrapper">naanam kadandhu viduvomaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naanam kadandhu viduvomaa"/>
</div>
<div class="lyrico-lyrics-wrapper">naanum kalandhu viduvomaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naanum kalandhu viduvomaa"/>
</div>
<div class="lyrico-lyrics-wrapper">thodakam mudindhu vidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thodakam mudindhu vidum"/>
</div>
<div class="lyrico-lyrics-wrapper">mudivu thondangi vidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudivu thondangi vidum"/>
</div>
<div class="lyrico-lyrics-wrapper">kaamam payindru varuvomaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaamam payindru varuvomaa"/>
</div>
<div class="lyrico-lyrics-wrapper">vilagaamal udalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vilagaamal udalum"/>
</div>
<div class="lyrico-lyrics-wrapper">puthir podum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthir podum"/>
</div>
<div class="lyrico-lyrics-wrapper">viralum vidai thedum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viralum vidai thedum"/>
</div>
<div class="lyrico-lyrics-wrapper">urakam adhai marandhiduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="urakam adhai marandhiduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">vilagaamal udalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vilagaamal udalum"/>
</div>
<div class="lyrico-lyrics-wrapper">puthir podum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthir podum"/>
</div>
<div class="lyrico-lyrics-wrapper">viralum vidai thedum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viralum vidai thedum"/>
</div>
<div class="lyrico-lyrics-wrapper">urakam adhai marandhiduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="urakam adhai marandhiduvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ada panjum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada panjum"/>
</div>
<div class="lyrico-lyrics-wrapper">theeyudhan pathikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theeyudhan pathikum"/>
</div>
<div class="lyrico-lyrics-wrapper">andha inbam than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="andha inbam than"/>
</div>
<div class="lyrico-lyrics-wrapper">thithikom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thithikom"/>
</div>
<div class="lyrico-lyrics-wrapper">ennaagumo yedhagumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennaagumo yedhagumo"/>
</div>
<div class="lyrico-lyrics-wrapper">unodu vazhaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unodu vazhaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">yenjeevan pogadhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yenjeevan pogadhadi"/>
</div>
<div class="lyrico-lyrics-wrapper">yenjeevan pogadhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yenjeevan pogadhadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">maya maya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maya maya"/>
</div>
<div class="lyrico-lyrics-wrapper">mayak kanalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mayak kanalae"/>
</div>
<div class="lyrico-lyrics-wrapper">maya maya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maya maya"/>
</div>
<div class="lyrico-lyrics-wrapper">mayam seidhaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mayam seidhaalae"/>
</div>
</pre>
