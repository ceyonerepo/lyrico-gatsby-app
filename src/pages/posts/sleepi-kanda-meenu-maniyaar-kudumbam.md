---
title: "sleepi kanda meenu song lyrics"
album: "Maniyaar Kudumbam"
artist: "Thambi Ramaiah"
lyricist: "Thambi Ramaiah"
director: "Thambi Ramaiah"
path: "/albums/maniyaar-kudumbam-lyrics"
song: "Sleepi Kanda Meenu"
image: ../../images/albumart/maniyaar-kudumbam.jpg
date: 2018-08-03
lang: tamil
youtubeLink: "https://www.youtube.com/embed/1NcSMaEcIeg"
type: "love"
singers:
  - Karthik
  - Chinmayi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Thaana thaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaana thaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana thandhaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana thandhaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaana thaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaana thaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana thandhaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana thandhaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaana thaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaana thaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana thandhaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana thandhaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thandha nana thana naaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandha nana thana naaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sleep sleep sleep sleep sleep
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sleep sleep sleep sleep sleep"/>
</div>
<div class="lyrico-lyrics-wrapper">Seema seema seema
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seema seema seema"/>
</div>
<div class="lyrico-lyrics-wrapper">Seema seema
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seema seema"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sleepi kanda meena pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sleepi kanda meena pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Sema karuva mulla pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sema karuva mulla pola"/>
</div>
<div class="lyrico-lyrics-wrapper">En manasil un nenappu valaruthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En manasil un nenappu valaruthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi enna meeri en naaku olaruthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi enna meeri en naaku olaruthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sleepi kanda meena pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sleepi kanda meena pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Sema karuva mulla pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sema karuva mulla pola"/>
</div>
<div class="lyrico-lyrics-wrapper">En manasil un nenappu valaruthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En manasil un nenappu valaruthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada enna meeri en naaku olaruthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada enna meeri en naaku olaruthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thenga poo manasum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thenga poo manasum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thennam paalai chirippum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thennam paalai chirippum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unkuda sernthu vaazha solluthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unkuda sernthu vaazha solluthae"/>
</div>
<div class="lyrico-lyrics-wrapper">En usuru kulla pallu muluchu kolluthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En usuru kulla pallu muluchu kolluthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sleep sleep sleep sleep sleep
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sleep sleep sleep sleep sleep"/>
</div>
<div class="lyrico-lyrics-wrapper">Seema seema seema
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seema seema seema"/>
</div>
<div class="lyrico-lyrics-wrapper">Seema seema
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seema seema"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sleepi kanda meena pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sleepi kanda meena pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Sema karuva mulla pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sema karuva mulla pola"/>
</div>
<div class="lyrico-lyrics-wrapper">En manasil un nenappu valaruthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En manasil un nenappu valaruthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi enna meeri en naaku olaruthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi enna meeri en naaku olaruthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un paal piditha paruthi pinju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un paal piditha paruthi pinju"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarvaiyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarvaiyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">En neththi mattum vervaiyaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En neththi mattum vervaiyaachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nermaiyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nermaiyilae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee rotula thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee rotula thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nada nadantha pogaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nada nadantha pogaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan vaai thiranthu nikkurenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan vaai thiranthu nikkurenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetkathila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetkathila"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee vaazha pazhathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee vaazha pazhathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Oosi yethum maruthuvachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oosi yethum maruthuvachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan vazhum varai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan vazhum varai"/>
</div>
<div class="lyrico-lyrics-wrapper">Sathiyama un katchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sathiyama un katchi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sleep sleep sleep sleep sleep
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sleep sleep sleep sleep sleep"/>
</div>
<div class="lyrico-lyrics-wrapper">Seema seema seema
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seema seema seema"/>
</div>
<div class="lyrico-lyrics-wrapper">Seema seema
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seema seema"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sleepi kanda meena pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sleepi kanda meena pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Sema karuva mulla pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sema karuva mulla pola"/>
</div>
<div class="lyrico-lyrics-wrapper">En manasil un nenappu valaruthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En manasil un nenappu valaruthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi enna meeri en naaku olaruthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi enna meeri en naaku olaruthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unna pakkum munnae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna pakkum munnae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku peru ooru suththi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku peru ooru suththi"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha kali manna poosi kitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha kali manna poosi kitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Usura suththi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usura suththi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaasu konjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasu konjam"/>
</div>
<div class="lyrico-lyrics-wrapper">Serthu vainga kallam illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Serthu vainga kallam illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna minja yaarum illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna minja yaarum illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jillavula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jillavula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee irundu kidantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee irundu kidantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanathil oli thanthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanathil oli thanthaai"/>
</div>
<div class="lyrico-lyrics-wrapper">En idhayam solluthu enakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En idhayam solluthu enakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee thaan irandaam thaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thaan irandaam thaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sleep sleep sleep sleep sleep
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sleep sleep sleep sleep sleep"/>
</div>
<div class="lyrico-lyrics-wrapper">Seema seema seema
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seema seema seema"/>
</div>
<div class="lyrico-lyrics-wrapper">Seema seema
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seema seema"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sleepi kanda meena pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sleepi kanda meena pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Sema karuva mulla pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sema karuva mulla pola"/>
</div>
<div class="lyrico-lyrics-wrapper">En manasil un nenappu valaruthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En manasil un nenappu valaruthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi enna meeri en naaku olaruthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi enna meeri en naaku olaruthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaana thaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaana thaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana thandhaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana thandhaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaana thaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaana thaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana thandhaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana thandhaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaana thaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaana thaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana thandhaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana thandhaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thandha nana thana naaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandha nana thana naaa"/>
</div>
</pre>
