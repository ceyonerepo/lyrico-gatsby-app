---
title: 'morattu thamizhan da song lyrics'
album: 'Pattas'
artist: 'Vivek Mervin'
lyricist: 'Vivek'
director: 'R.S.Durai Senthilkumar'
path: '/albums/pattas-song-lyrics'
song: 'Morattu Thamizhan Da'
image: ../../images/albumart/pattas.jpg
date: 2020-01-15
lang: tamil
singers:
- Vivek - Mervin
youtubeLink: 'https://www.youtube.com/embed/e1PWEsg1Ezg'
type: 'mass'
---



<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Hai hai hai hai
<input type="checkbox" class="lyrico-select-lyric-line" value="Hai hai hai hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Hai hai hai hai
<input type="checkbox" class="lyrico-select-lyric-line" value="Hai hai hai hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Heyyyyyyyyyyyyyyyyyyy
<input type="checkbox" class="lyrico-select-lyric-line" value="Heyyyyyyyyyyyyyyyyyyy"/>
</div>
<div class="lyrico-lyrics-wrapper">Hai hai hai hai
<input type="checkbox" class="lyrico-select-lyric-line" value="Hai hai hai hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thimu thimu thimiradikutha
<input type="checkbox" class="lyrico-select-lyric-line" value="Thimu thimu thimiradikutha"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey…pada pada pada vedikkutha
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey…pada pada pada vedikkutha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa…nara narambunga pudaikutha
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaa…nara narambunga pudaikutha"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey hey…ethiriya vachu polakkutha
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey hey…ethiriya vachu polakkutha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vaada…
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaada…"/>
</div>
<div class="lyrico-lyrics-wrapper">Veeram enga varama
<input type="checkbox" class="lyrico-select-lyric-line" value="Veeram enga varama"/>
</div>
<div class="lyrico-lyrics-wrapper">Puliya thorathum morama
<input type="checkbox" class="lyrico-select-lyric-line" value="Puliya thorathum morama"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey karuna garvam rendum kalandha
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey karuna garvam rendum kalandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Morattu thamizhan da
<input type="checkbox" class="lyrico-select-lyric-line" value="Morattu thamizhan da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Hai hai hai hai
<input type="checkbox" class="lyrico-select-lyric-line" value="Hai hai hai hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Hai hai hai hai
<input type="checkbox" class="lyrico-select-lyric-line" value="Hai hai hai hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Hai hai hai hai
<input type="checkbox" class="lyrico-select-lyric-line" value="Hai hai hai hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Hai hai hai hai
<input type="checkbox" class="lyrico-select-lyric-line" value="Hai hai hai hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Indha aarambam pudhusu
<input type="checkbox" class="lyrico-select-lyric-line" value="Indha aarambam pudhusu"/>
</div>
<div class="lyrico-lyrics-wrapper">Varalaaru perusu
<input type="checkbox" class="lyrico-select-lyric-line" value="Varalaaru perusu"/>
</div>
<div class="lyrico-lyrics-wrapper">Endha maanuda kulamum
<input type="checkbox" class="lyrico-select-lyric-line" value="Endha maanuda kulamum"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga pugazha thottathu kedaiyathu
<input type="checkbox" class="lyrico-select-lyric-line" value="Enga pugazha thottathu kedaiyathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Mann saayura pozhuthum
<input type="checkbox" class="lyrico-select-lyric-line" value="Mann saayura pozhuthum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vel maarbula irukkum
<input type="checkbox" class="lyrico-select-lyric-line" value="Vel maarbula irukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Endha edhiriyin vaalum
<input type="checkbox" class="lyrico-select-lyric-line" value="Endha edhiriyin vaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga mudhuga paathathu kedaiyathu
<input type="checkbox" class="lyrico-select-lyric-line" value="Enga mudhuga paathathu kedaiyathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Oru aayiram kai unnai sarikkum
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru aayiram kai unnai sarikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un manasa thookki niruthu
<input type="checkbox" class="lyrico-select-lyric-line" value="Un manasa thookki niruthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai veesida paakura yevanum
<input type="checkbox" class="lyrico-select-lyric-line" value="Unnai veesida paakura yevanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Etti vizhanum vazhiya koduthu
<input type="checkbox" class="lyrico-select-lyric-line" value="Etti vizhanum vazhiya koduthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ellaamae koduthu osathura boomi
<input type="checkbox" class="lyrico-select-lyric-line" value="Ellaamae koduthu osathura boomi"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbaana kaiya aravanaikkum
<input type="checkbox" class="lyrico-select-lyric-line" value="Anbaana kaiya aravanaikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnoda kaaval irukkura varaikkum
<input type="checkbox" class="lyrico-select-lyric-line" value="Unnoda kaaval irukkura varaikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nammoda man thaan jeychirukkum
<input type="checkbox" class="lyrico-select-lyric-line" value="Nammoda man thaan jeychirukkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vaettai ini daa
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaettai ini daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey…
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey…"/>
</div>
<div class="lyrico-lyrics-wrapper">Aattam veri daa
<input type="checkbox" class="lyrico-select-lyric-line" value="Aattam veri daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa…
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaa…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Saattai edu da
<input type="checkbox" class="lyrico-select-lyric-line" value="Saattai edu da"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey hey…
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey hey…"/>
</div>
<div class="lyrico-lyrics-wrapper">Kootta idi daa
<input type="checkbox" class="lyrico-select-lyric-line" value="Kootta idi daa"/>
</div>
  <div class="lyrico-lyrics-wrapper">Heyyyyyyyyyyyyyyyyy
<input type="checkbox" class="lyrico-select-lyric-line" value="Heyyyyyyyyyyyyyyyyy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vaada…
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaada…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Hai hai hai hai
<input type="checkbox" class="lyrico-select-lyric-line" value="Hai hai hai hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Hai hai hai hai
<input type="checkbox" class="lyrico-select-lyric-line" value="Hai hai hai hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Hai hai hai hai
<input type="checkbox" class="lyrico-select-lyric-line" value="Hai hai hai hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Hai hai hai hai
<input type="checkbox" class="lyrico-select-lyric-line" value="Hai hai hai hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kalai vaazhum boomi
<input type="checkbox" class="lyrico-select-lyric-line" value="Kalai vaazhum boomi"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu thaanae saamy
<input type="checkbox" class="lyrico-select-lyric-line" value="Adhu thaanae saamy"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalai solla kettu
<input type="checkbox" class="lyrico-select-lyric-line" value="Kalai solla kettu"/>
</div>
<div class="lyrico-lyrics-wrapper">Panpaada kaathom
<input type="checkbox" class="lyrico-select-lyric-line" value="Panpaada kaathom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ayal naattu mogam
<input type="checkbox" class="lyrico-select-lyric-line" value="Ayal naattu mogam"/>
</div>
<div class="lyrico-lyrics-wrapper">Athukulla maatti
<input type="checkbox" class="lyrico-select-lyric-line" value="Athukulla maatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Innaikku yeno
<input type="checkbox" class="lyrico-select-lyric-line" value="Innaikku yeno"/>
</div>
<div class="lyrico-lyrics-wrapper">Inathoda thavichom
<input type="checkbox" class="lyrico-select-lyric-line" value="Inathoda thavichom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Hey…vera tholacha
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey…vera tholacha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vera oruthan
<input type="checkbox" class="lyrico-select-lyric-line" value="Vera oruthan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pathayila nee mannaava
<input type="checkbox" class="lyrico-select-lyric-line" value="Pathayila nee mannaava"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai madhicha
<input type="checkbox" class="lyrico-select-lyric-line" value="Unnai madhicha"/>
</div>
<div class="lyrico-lyrics-wrapper">Munna nee vazhiya kaati pova
<input type="checkbox" class="lyrico-select-lyric-line" value="Munna nee vazhiya kaati pova"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Rosham irukka
<input type="checkbox" class="lyrico-select-lyric-line" value="Rosham irukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Meesai murukkum
<input type="checkbox" class="lyrico-select-lyric-line" value="Meesai murukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattabomman kaiya aava
<input type="checkbox" class="lyrico-select-lyric-line" value="Kattabomman kaiya aava"/>
</div>
<div class="lyrico-lyrics-wrapper">Koovam irukka
<input type="checkbox" class="lyrico-select-lyric-line" value="Koovam irukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Bharathiyin mundasa aava
<input type="checkbox" class="lyrico-select-lyric-line" value="Bharathiyin mundasa aava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ellaamae koduthu osathura boomi
<input type="checkbox" class="lyrico-select-lyric-line" value="Ellaamae koduthu osathura boomi"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbaana kaiya aravanaikkum
<input type="checkbox" class="lyrico-select-lyric-line" value="Anbaana kaiya aravanaikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnoda kaaval irukkura varaikkum
<input type="checkbox" class="lyrico-select-lyric-line" value="Unnoda kaaval irukkura varaikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nammoda man thaan jeychirukkum
<input type="checkbox" class="lyrico-select-lyric-line" value="Nammoda man thaan jeychirukkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vaettai ini daa
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaettai ini daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey…
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey…"/>
</div>
<div class="lyrico-lyrics-wrapper">Aattam veri daa
<input type="checkbox" class="lyrico-select-lyric-line" value="Aattam veri daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa…
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaa…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Saattai edu da
<input type="checkbox" class="lyrico-select-lyric-line" value="Saattai edu da"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey hey…
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey hey…"/>
</div>
<div class="lyrico-lyrics-wrapper">Kootta idi daa
<input type="checkbox" class="lyrico-select-lyric-line" value="Kootta idi daa"/>
</div>
  <div class="lyrico-lyrics-wrapper">Vaada…
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaada…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Indha aarambam pudhusu
<input type="checkbox" class="lyrico-select-lyric-line" value="Indha aarambam pudhusu"/>
</div>
<div class="lyrico-lyrics-wrapper">Varalaaru perusu
<input type="checkbox" class="lyrico-select-lyric-line" value="Varalaaru perusu"/>
</div>
<div class="lyrico-lyrics-wrapper">Endha maanuda kulamum
<input type="checkbox" class="lyrico-select-lyric-line" value="Endha maanuda kulamum"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga pugazha thottathu kedaiyathu
<input type="checkbox" class="lyrico-select-lyric-line" value="Enga pugazha thottathu kedaiyathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Mann saayura pozhuthum
<input type="checkbox" class="lyrico-select-lyric-line" value="Mann saayura pozhuthum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vel maarbula irukkum
<input type="checkbox" class="lyrico-select-lyric-line" value="Vel maarbula irukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Endha edhiriyin vaalum
<input type="checkbox" class="lyrico-select-lyric-line" value="Endha edhiriyin vaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga mudhuga paathathu kedaiyathu
<input type="checkbox" class="lyrico-select-lyric-line" value="Enga mudhuga paathathu kedaiyathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Oru aayiram kai unnai sarikkum
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru aayiram kai unnai sarikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un manasa thookki niruthu
<input type="checkbox" class="lyrico-select-lyric-line" value="Un manasa thookki niruthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai veesida paakura yevanum
<input type="checkbox" class="lyrico-select-lyric-line" value="Unnai veesida paakura yevanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Etti vizhanum vazhiya koduthu
<input type="checkbox" class="lyrico-select-lyric-line" value="Etti vizhanum vazhiya koduthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vaettai ini daa
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaettai ini daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey…
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey…"/>
</div>
<div class="lyrico-lyrics-wrapper">Aattam veri daa
<input type="checkbox" class="lyrico-select-lyric-line" value="Aattam veri daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa…
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaa…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Saattai edu da
<input type="checkbox" class="lyrico-select-lyric-line" value="Saattai edu da"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey hey…
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey hey…"/>
</div>
<div class="lyrico-lyrics-wrapper">Kootta idi daa
<input type="checkbox" class="lyrico-select-lyric-line" value="Kootta idi daa"/>
</div>
  <div class="lyrico-lyrics-wrapper">Vaada…
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaada…"/>
</div>
</pre>