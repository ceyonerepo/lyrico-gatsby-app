---
title: "mashallah song lyrics"
album: "Krishna Rao Super Market"
artist: "Bhole Shavali"
lyricist: "Sai Siri"
director: "Sreenath Pulakuram"
path: "/albums/krishna-rao-super-market-lyrics"
song: "Mashallah"
image: ../../images/albumart/krishna-rao-super-market.jpg
date: 2019-10-18
lang: telugu
youtubeLink: "https://www.youtube.com/embed/sujMMehqqFE"
type: "love"
singers:
  - Spoorthi Jitender
  - Bhole Shavali
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">oh saaya re re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oh saaya re re"/>
</div>
<div class="lyrico-lyrics-wrapper">saaya re re saayoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saaya re re saayoo"/>
</div>
<div class="lyrico-lyrics-wrapper">saaya re re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saaya re re"/>
</div>
<div class="lyrico-lyrics-wrapper">saaya re re saayoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saaya re re saayoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">maashallah 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maashallah "/>
</div>
<div class="lyrico-lyrics-wrapper">ye pyari ladiki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ye pyari ladiki"/>
</div>
<div class="lyrico-lyrics-wrapper">dilki khidiki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dilki khidiki"/>
</div>
<div class="lyrico-lyrics-wrapper">kholiki aayire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kholiki aayire"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">maashallah 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maashallah "/>
</div>
<div class="lyrico-lyrics-wrapper">ye jhanuka thukada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ye jhanuka thukada"/>
</div>
<div class="lyrico-lyrics-wrapper">premka  mukada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="premka  mukada"/>
</div>
<div class="lyrico-lyrics-wrapper">dilse usne ghayire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dilse usne ghayire"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">naa manasunu thaakina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa manasunu thaakina"/>
</div>
<div class="lyrico-lyrics-wrapper">thaaravayo saathiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaaravayo saathiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">o saathiyo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="o saathiyo "/>
</div>
<div class="lyrico-lyrics-wrapper">nee sarasunu cherina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee sarasunu cherina"/>
</div>
<div class="lyrico-lyrics-wrapper">chepanayo saathiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chepanayo saathiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">o sathiyo oh pillo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="o sathiyo oh pillo"/>
</div>
<div class="lyrico-lyrics-wrapper">nee premanu preminche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee premanu preminche"/>
</div>
<div class="lyrico-lyrics-wrapper">vaannayyaane nee valla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaannayyaane nee valla"/>
</div>
<div class="lyrico-lyrics-wrapper">aa premanu preminche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aa premanu preminche"/>
</div>
<div class="lyrico-lyrics-wrapper">premayyaane 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="premayyaane "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">maashallah 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maashallah "/>
</div>
<div class="lyrico-lyrics-wrapper">ye pyari ladiki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ye pyari ladiki"/>
</div>
<div class="lyrico-lyrics-wrapper">dilki khidiki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dilki khidiki"/>
</div>
<div class="lyrico-lyrics-wrapper">kholiki aayire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kholiki aayire"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">maashallah 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maashallah "/>
</div>
<div class="lyrico-lyrics-wrapper">ye jhanuka thukada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ye jhanuka thukada"/>
</div>
<div class="lyrico-lyrics-wrapper">premka  mukada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="premka  mukada"/>
</div>
<div class="lyrico-lyrics-wrapper">dilse usne ghayire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dilse usne ghayire"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">naa manasunu thaakina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa manasunu thaakina"/>
</div>
<div class="lyrico-lyrics-wrapper">thaaravayo saathiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaaravayo saathiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">o saathiyo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="o saathiyo "/>
</div>
<div class="lyrico-lyrics-wrapper">nee sarasunu cherina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee sarasunu cherina"/>
</div>
<div class="lyrico-lyrics-wrapper">chepanayo saathiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chepanayo saathiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">o sathiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="o sathiyo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oh saaya re re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oh saaya re re"/>
</div>
<div class="lyrico-lyrics-wrapper">saaya re re saayoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saaya re re saayoo"/>
</div>
<div class="lyrico-lyrics-wrapper">saaya re re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saaya re re"/>
</div>
<div class="lyrico-lyrics-wrapper">saaya re re saayoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saaya re re saayoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">prathi yugamuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="prathi yugamuna"/>
</div>
<div class="lyrico-lyrics-wrapper">nee sagamunai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee sagamunai"/>
</div>
<div class="lyrico-lyrics-wrapper">nee kosam pudathane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee kosam pudathane"/>
</div>
<div class="lyrico-lyrics-wrapper">yedha padhe padhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yedha padhe padhe"/>
</div>
<div class="lyrico-lyrics-wrapper">nanu padhamantu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanu padhamantu"/>
</div>
<div class="lyrico-lyrics-wrapper">neetho sadhaa sadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neetho sadhaa sadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">saagaalandhi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saagaalandhi "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oh pillaa naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oh pillaa naa"/>
</div>
<div class="lyrico-lyrics-wrapper">lokamlokochave swargamla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lokamlokochave swargamla"/>
</div>
<div class="lyrico-lyrics-wrapper">nee vallaa ney 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee vallaa ney "/>
</div>
<div class="lyrico-lyrics-wrapper">majnulaa maaraane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="majnulaa maaraane"/>
</div>
<div class="lyrico-lyrics-wrapper">oh lailaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oh lailaa "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">maashallah 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maashallah "/>
</div>
<div class="lyrico-lyrics-wrapper">ye pyari ladiki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ye pyari ladiki"/>
</div>
<div class="lyrico-lyrics-wrapper">dilki khidiki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dilki khidiki"/>
</div>
<div class="lyrico-lyrics-wrapper">kholiki aayire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kholiki aayire"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">maashallah 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maashallah "/>
</div>
<div class="lyrico-lyrics-wrapper">ye jhanuka thukada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ye jhanuka thukada"/>
</div>
<div class="lyrico-lyrics-wrapper">premka  mukada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="premka  mukada"/>
</div>
<div class="lyrico-lyrics-wrapper">dilse usne ghayire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dilse usne ghayire"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">maashallah 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maashallah "/>
</div>
<div class="lyrico-lyrics-wrapper">ye pyari ladiki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ye pyari ladiki"/>
</div>
<div class="lyrico-lyrics-wrapper">dilki khidiki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dilki khidiki"/>
</div>
<div class="lyrico-lyrics-wrapper">kholiki aayire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kholiki aayire"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">maashallah 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maashallah "/>
</div>
<div class="lyrico-lyrics-wrapper">ye jhanuka thukada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ye jhanuka thukada"/>
</div>
<div class="lyrico-lyrics-wrapper">premka  mukada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="premka  mukada"/>
</div>
<div class="lyrico-lyrics-wrapper">dilse usne ghayire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dilse usne ghayire"/>
</div>
</pre>
