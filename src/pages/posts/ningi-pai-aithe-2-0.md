---
title: "ningi pai song lyrics"
album: "Aithe 2.0"
artist: "Arun Chiluveru"
lyricist: "Kittu Vissapragada"
director: "Raj Madiraju"
path: "/albums/aithe-2-0-lyrics"
song: "Ningi Pai"
image: ../../images/albumart/aithe-2-0.jpg
date: 2018-03-16
lang: telugu
youtubeLink: "https://www.youtube.com/embed/_vobz5-d9eY"
type: "friendship"
singers:
  -	Naresh Iyer
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ningi pai needalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ningi pai needalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Edagaraa annadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edagaraa annadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Chitula batuke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chitula batuke"/>
</div>
<div class="lyrico-lyrics-wrapper">Chiguru todige
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chiguru todige"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasu gadilo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu gadilo"/>
</div>
<div class="lyrico-lyrics-wrapper">Alalu egase
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alalu egase"/>
</div>
<div class="lyrico-lyrics-wrapper">Gelupu pilupe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gelupu pilupe"/>
</div>
<div class="lyrico-lyrics-wrapper">Telisi aduge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Telisi aduge"/>
</div>
<div class="lyrico-lyrics-wrapper">Parugu pedutoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parugu pedutoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Alupu mariche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alupu mariche"/>
</div>
<div class="lyrico-lyrics-wrapper">Andanee andalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andanee andalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Andigaa aashayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andigaa aashayam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Velugula valale kannula Paina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velugula valale kannula Paina"/>
</div>
<div class="lyrico-lyrics-wrapper">Vestoo undi ee udayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vestoo undi ee udayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalale patti niddura lepi choodantoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalale patti niddura lepi choodantoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Padapadamantoo gamyam vaipu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padapadamantoo gamyam vaipu"/>
</div>
<div class="lyrico-lyrics-wrapper">Tarumutu undi utsaaham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tarumutu undi utsaaham"/>
</div>
<div class="lyrico-lyrics-wrapper">Oohallona repati gelupu munde istoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oohallona repati gelupu munde istoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Prapanchame patangamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prapanchame patangamai"/>
</div>
<div class="lyrico-lyrics-wrapper">Maapai ilaa saagagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maapai ilaa saagagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vishwasame aa daaramai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vishwasame aa daaramai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadhaaramai maaradaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadhaaramai maaradaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aakale Aayudham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aakale Aayudham"/>
</div>
<div class="lyrico-lyrics-wrapper">Snehame Indhanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Snehame Indhanam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Urumu sadilo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urumu sadilo"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinuku Tadilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinuku Tadilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudami Odilo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudami Odilo"/>
</div>
<div class="lyrico-lyrics-wrapper">Segalu karige
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Segalu karige"/>
</div>
<div class="lyrico-lyrics-wrapper">Badulu telipe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Badulu telipe"/>
</div>
<div class="lyrico-lyrics-wrapper">Parimalamulaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parimalamulaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pilichi valache
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pilichi valache"/>
</div>
<div class="lyrico-lyrics-wrapper">Shramani Gelupe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shramani Gelupe"/>
</div>
<div class="lyrico-lyrics-wrapper">Aashaga Aashake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aashaga Aashake"/>
</div>
<div class="lyrico-lyrics-wrapper">Aayuve penchagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayuve penchagaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aa gudi gantaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa gudi gantaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa badi ganta ki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa badi ganta ki"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulikipadina manasu telipe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulikipadina manasu telipe"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa prati adugu atu vaipe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa prati adugu atu vaipe"/>
</div>
<div class="lyrico-lyrics-wrapper">Eenati aashane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eenati aashane"/>
</div>
<div class="lyrico-lyrics-wrapper">Maa pallekivvagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa pallekivvagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Repati gelupai untaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Repati gelupai untaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa oori gundepai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa oori gundepai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Peru raayanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Peru raayanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa oori gundepai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa oori gundepai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Peru raayanaa..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Peru raayanaa.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Urumu sadilo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urumu sadilo"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinuku Tadilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinuku Tadilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudami Odilo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudami Odilo"/>
</div>
<div class="lyrico-lyrics-wrapper">Segalu karige
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Segalu karige"/>
</div>
<div class="lyrico-lyrics-wrapper">Badulu telipe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Badulu telipe"/>
</div>
<div class="lyrico-lyrics-wrapper">Parimalamulaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parimalamulaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pilichi valache
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pilichi valache"/>
</div>
<div class="lyrico-lyrics-wrapper">Shramani Gelupe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shramani Gelupe"/>
</div>
<div class="lyrico-lyrics-wrapper">Aashaga Aashake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aashaga Aashake"/>
</div>
<div class="lyrico-lyrics-wrapper">Aayuve penchagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayuve penchagaa"/>
</div>
</pre>
