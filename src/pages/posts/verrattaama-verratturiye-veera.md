---
title: "verrattaama verratturiye song lyrics"
album: "Veera"
artist: "Leon James"
lyricist: "Ko Sesha"
director: "Rajaraman"
path: "/albums/veera-lyrics"
song: "Verrattaama Verratturiye"
image: ../../images/albumart/veera.jpg
date: 2018-02-16
lang: tamil
youtubeLink: "https://www.youtube.com/embed/P5vclbQH3CU"
type: "love"
singers:
  - Sid Sriram
  - Neeti Mohan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Verattama veratturiyae Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verattama veratturiyae Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoraththama thorathuriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoraththama thorathuriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Theruvellam thirinjenae naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theruvellam thirinjenae naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna thedi tholanjenadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna thedi tholanjenadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Merattaama meratturiyae nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Merattaama meratturiyae nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadaththama kadathuriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadaththama kadathuriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Verrappaga alanjalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verrappaga alanjalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un nenapathan alanjenadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un nenapathan alanjenadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee venum naan vazha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee venum naan vazha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee venum kan mooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee venum kan mooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Varuvenae uyirai tharuvenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varuvenae uyirai tharuvenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku ellam adiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku ellam adiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini neethaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini neethaanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee venum naan vazha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee venum naan vazha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee venum kan mooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee venum kan mooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Varuvenae uyirai tharuvenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varuvenae uyirai tharuvenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku ellam adiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku ellam adiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini neethaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini neethaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethaanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adanga yetkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adanga yetkam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai thakkumbodhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai thakkumbodhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi nenjil idi minnal aachae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi nenjil idi minnal aachae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thodanga kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodanga kaadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thodanga mugavariya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodanga mugavariya"/>
</div>
<div class="lyrico-lyrics-wrapper">Thandhenae en thookkam pochae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandhenae en thookkam pochae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endhan viral neengi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan viral neengi"/>
</div>
<div class="lyrico-lyrics-wrapper">Engeyum pogadhae nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engeyum pogadhae nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Un paarvaaiyaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un paarvaaiyaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mella erikkirayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mella erikkirayae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee venum naan vazha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee venum naan vazha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee venum kan mooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee venum kan mooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Varuvenae uyirai tharuvenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varuvenae uyirai tharuvenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku ellam adiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku ellam adiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini neethaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini neethaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Verattama veratturiyae Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verattama veratturiyae Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoraththama thorathuriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoraththama thorathuriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Theruvellam thirinjenae naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theruvellam thirinjenae naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna thedi tholanjenadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna thedi tholanjenadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Merattaama meratturiyae nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Merattaama meratturiyae nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadaththama kadathuriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadaththama kadathuriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Verrappaga alanjalum um
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verrappaga alanjalum um"/>
</div>
<div class="lyrico-lyrics-wrapper">Un nenapathan alanjenadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un nenapathan alanjenadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee venum naan vazha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee venum naan vazha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee venum kan mooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee venum kan mooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Varuvenae uyirai tharuvenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varuvenae uyirai tharuvenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku ellam adiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku ellam adiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini neethaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini neethaanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee venum naan vazha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee venum naan vazha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee venum kan mooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee venum kan mooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Varuvenae uyirai tharuvenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varuvenae uyirai tharuvenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku ellam adiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku ellam adiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini neethaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini neethaanae"/>
</div>
</pre>
