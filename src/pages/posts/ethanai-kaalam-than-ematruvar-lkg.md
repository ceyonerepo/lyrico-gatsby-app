---
title: "ethanani kalam than ematruvar song lyrics"
album: "LKG"
artist: "Leon James"
lyricist: "Thanjai N. Ramaiah Dass"
director: "K. R. Prabhu"
path: "/albums/lkg-lyrics"
song: "Ethanani Kalam Than Ematruvar"
image: ../../images/albumart/lkg.jpg
date: 2019-02-22
lang: tamil
youtubeLink: "https://www.youtube.com/embed/rz8h9ZjqjMw"
type: "motivational"
singers:
  - Sean Roldan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ethanai Kaalam Thaan Yemaatruvaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethanai Kaalam Thaan Yemaatruvaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha Naatile Naam Naatile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Naatile Naam Naatile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Innum Ethanai Kaalam Thaan Yemaatruvar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum Ethanai Kaalam Thaan Yemaatruvar"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha Naatile Sondha Naatile Naam Naatile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Naatile Sondha Naatile Naam Naatile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Innum Ethanai Kaalam Thaan Yemaatruvar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum Ethanai Kaalam Thaan Yemaatruvar"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha Naatile Sondha Naatile Naam Naatile Naatile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Naatile Sondha Naatile Naam Naatile Naatile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sathiyam Thavaradha Utthaman Polave Nadikkiraar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sathiyam Thavaradha Utthaman Polave Nadikkiraar"/>
</div>
<div class="lyrico-lyrics-wrapper">Sathiyam Thavaradha Utthaman Polave Nadikkiraar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sathiyam Thavaradha Utthaman Polave Nadikkiraar"/>
</div>
<div class="lyrico-lyrics-wrapper">Samayam Paarthu Pala Vagaiyilum Kollai Adikkiraar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samayam Paarthu Pala Vagaiyilum Kollai Adikkiraar"/>
</div>
<div class="lyrico-lyrics-wrapper">Samayam Paarthu Pala Vagaiyilum Kollai Adikkiraar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samayam Paarthu Pala Vagaiyilum Kollai Adikkiraar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bakthanai Polave Pagal Vesham Kaatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bakthanai Polave Pagal Vesham Kaatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Pamara Makkalai Valaiyinil Matti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pamara Makkalai Valaiyinil Matti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ethanai Kaalam Thaan…Ooohoohoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethanai Kaalam Thaan…Ooohoohoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Innum Ethanai Kaalam Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum Ethanai Kaalam Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Innum Ethanai Kaalam Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum Ethanai Kaalam Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yemaatruvaar Intha Naatile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yemaatruvaar Intha Naatile"/>
</div>
<div class="lyrico-lyrics-wrapper">Sondha Naatile Naam Naatile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sondha Naatile Naam Naatile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pechinil Matum Veeram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pechinil Matum Veeram"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivar Seivathellam Verum Beram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivar Seivathellam Verum Beram"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaakkugal Koduppadhu Vazhakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaakkugal Koduppadhu Vazhakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhai Marappadhum Ivaradhu Pazhakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai Marappadhum Ivaradhu Pazhakkam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yevaradhu Kaalaiyum Pidippar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yevaradhu Kaalaiyum Pidippar"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidukkena Vaariyum Viduvaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidukkena Vaariyum Viduvaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Yevaradhu Kaalaiyum Pidippar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yevaradhu Kaalaiyum Pidippar"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidukkena Vaariyum Viduvaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidukkena Vaariyum Viduvaar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Innum Ethanai Kaalam Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum Ethanai Kaalam Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Innum Ethanai Kaalam Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum Ethanai Kaalam Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Innum Ethanai Kaalam Thaan Yemaatruvar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum Ethanai Kaalam Thaan Yemaatruvar"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha Naatile Sondha Naatile Naam Naatile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Naatile Sondha Naatile Naam Naatile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naatile Thaiye Naatile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naatile Thaiye Naatile"/>
</div>
<div class="lyrico-lyrics-wrapper">Naatile Thaiye Naatile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naatile Thaiye Naatile"/>
</div>
<div class="lyrico-lyrics-wrapper">Naatile Thaiye Naatile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naatile Thaiye Naatile"/>
</div>
<div class="lyrico-lyrics-wrapper">Naatile Thaiye Naatile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naatile Thaiye Naatile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Panaththinai Vaariye Kodupaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panaththinai Vaariye Kodupaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Kizhaviyai Kattiyum Pidippaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kizhaviyai Kattiyum Pidippaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Yezhaiyin Kudisaikkul Pugunthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yezhaiyin Kudisaikkul Pugunthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Avar Veetinil Koozhaiyum Kudippaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avar Veetinil Koozhaiyum Kudippaar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Ungalil Oruvan Enbaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Ungalil Oruvan Enbaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Vendradhum Yaar Nee Enbaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vendradhum Yaar Nee Enbaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Ungalil Oruvan Enbaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Ungalil Oruvan Enbaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Vendradhum Yaar Nee Enbaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vendradhum Yaar Nee Enbaar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ini Ethanai Kaalam Thaan Yemaatruvar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Ethanai Kaalam Thaan Yemaatruvar"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha Naatile Sondha Naatile Naam Naatile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Naatile Sondha Naatile Naam Naatile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sathiyam Thavaradha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sathiyam Thavaradha"/>
</div>
<div class="lyrico-lyrics-wrapper">Utthaman Polave Nadikkiraar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Utthaman Polave Nadikkiraar"/>
</div>
<div class="lyrico-lyrics-wrapper">Samayam Paarthu Pala Vagaiyilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samayam Paarthu Pala Vagaiyilum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kollai Adikkiraar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kollai Adikkiraar"/>
</div>
<div class="lyrico-lyrics-wrapper">Samayam Paarthu Pala Vagaiyilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samayam Paarthu Pala Vagaiyilum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kollai Adikkiraar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kollai Adikkiraar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bakthanai Polave Pagal Vesham Kaatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bakthanai Polave Pagal Vesham Kaatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Pamara Makkalai Valaiyinil Matti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pamara Makkalai Valaiyinil Matti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Innum Ethanai Kaalam Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum Ethanai Kaalam Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Innum Ethanai Kaalam Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum Ethanai Kaalam Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Innum Ethanai Kaalam Thaan Yemaatruvar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum Ethanai Kaalam Thaan Yemaatruvar"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha Naatile Sondha Naatile Naam Naatile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Naatile Sondha Naatile Naam Naatile"/>
</div>
</pre>
