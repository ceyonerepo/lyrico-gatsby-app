---
title: "veeram theme song lyrics"
album: "Veeram"
artist: "Devi Sri Prasad"
lyricist: "Viveka"
director: "Siva"
path: "/albums/veeram-lyrics"
song: "Veeram Theme Song - Ratha Gaja Thuraga"
image: ../../images/albumart/veeram.jpg
date: 2014-01-10
lang: tamil
youtubeLink: "https://www.youtube.com/embed/lTBR9etyh3o"
type: "Mass"
singers:
  - Anand
  - Koushik
  - Deepak
  - Jagadish
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ratha Gaja Thuraga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ratha Gaja Thuraga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paathathigal Ethrippinum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paathathigal Ethrippinum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhakkalam Purinthidum Veeram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhakkalam Purinthidum Veeram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivan Mathabujam Irandum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Mathabujam Irandum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Malaiyena Ezhunthida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malaiyena Ezhunthida"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Serukkalam Sitharidum Veeram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Serukkalam Sitharidum Veeram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ratha Gaja Thuraga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ratha Gaja Thuraga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paathathigal Ethrippinum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paathathigal Ethrippinum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhakkalam Purinthidum Veeram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhakkalam Purinthidum Veeram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivan Mathabujam Irandum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Mathabujam Irandum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Malaiyena Ezhunthida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malaiyena Ezhunthida"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Serukkalam Sitharidum Veeram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Serukkalam Sitharidum Veeram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saga Manithan Oru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saga Manithan Oru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thuyar Ena Kasinthathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuyar Ena Kasinthathum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Agam Pathari Ezhum Veeram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Agam Pathari Ezhum Veeram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thugal Alavum Pagai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thugal Alavum Pagai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pugal Ingu Thavarena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pugal Ingu Thavarena"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaapparanaai Nirkkum Veeram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaapparanaai Nirkkum Veeram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thani Arimaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thani Arimaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pola Enna Tharunam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pola Enna Tharunam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaakkidum Perum Veeram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaakkidum Perum Veeram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ratha Gaja Thuraga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ratha Gaja Thuraga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paathathigal Ethrippinum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paathathigal Ethrippinum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhakkalam Purinthidum Veeram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhakkalam Purinthidum Veeram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivan Mathabujam Irandum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Mathabujam Irandum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Malaiyena Ezhunthida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malaiyena Ezhunthida"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Serukkalam Sitharidum Veeram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Serukkalam Sitharidum Veeram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sigai Thoda Ninaithavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sigai Thoda Ninaithavan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Siram Vizhum Tharaiyinil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siram Vizhum Tharaiyinil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eedinai Illa Veeram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eedinai Illa Veeram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Dhisaigalum Thigaithidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Dhisaigalum Thigaithidum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paarthavai Pathaithidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarthavai Pathaithidum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sarithiram Viyanthidum Veeram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarithiram Viyanthidum Veeram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Erithalalaai Nindru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Erithalalaai Nindru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ethirigal Alarida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethirigal Alarida"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Samarinil Thimiridum Veeram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samarinil Thimiridum Veeram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bayam Enum Sol Ingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bayam Enum Sol Ingu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Parichavam Illayada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parichavam Illayada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paramaporul Varam Thantha Veeram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paramaporul Varam Thantha Veeram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadum Punale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadum Punale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Motha Varum Velaiyilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Motha Varum Velaiyilum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kali Purum Thani Veeram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kali Purum Thani Veeram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ratha Gaja Thuraga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ratha Gaja Thuraga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paathathigal Ethrippinum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paathathigal Ethrippinum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhakkalam Purinthidum Veeram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhakkalam Purinthidum Veeram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivan Mathabujam Irandum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Mathabujam Irandum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Malaiyena Ezhunthida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malaiyena Ezhunthida"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Serukkalam Sitharidum Veeram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Serukkalam Sitharidum Veeram"/>
</div>
</pre>
