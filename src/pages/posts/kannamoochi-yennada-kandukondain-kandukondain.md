---
title: "kannamoochi yennada song lyrics"
album: "Kandukondain Kandukondain"
artist: "A. R. Rahman"
lyricist: "Vairamuthu"
director: "Rajiv Menon"
path: "/albums/kandukondain-kandukondain-lyrics"
song: "Kannamoochi Yennada"
image: ../../images/albumart/kandukondain-kandukondain.jpg
date: 2000-05-05
lang: tamil
youtubeLink: "https://www.youtube.com/embed/b0yba2XCahI"
type: "melody"
singers:
  - K. S. Chithra
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kannamoochi Yenada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannamoochi Yenada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannamoochi Yenada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannamoochi Yenada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Kanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kanna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannamoochi Yaenada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannamoochi Yaenada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Kanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kanna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Kannadi Porul Polada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Kannadi Porul Polada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannamoochi Yaenadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannamoochi Yaenadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Kanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kanna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Kannadi Porul Polada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Kannadi Porul Polada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andha Nadhiyin Karaiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Nadhiyin Karaiyai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Kaetten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Kaetten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andha Kaatrai Niruthiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Kaatrai Niruthiyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ketten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andha Nadhiyin Karaiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Nadhiyin Karaiyai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Ketten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Ketten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andha Kaatrai Niruthiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Kaatrai Niruthiyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ketten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaan Vizhiyai Keten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaan Vizhiyai Keten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidaiyae Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidaiyae Illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaan Vizhiyai Keten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaan Vizhiyai Keten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidaiye Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidaiye Illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irudhiyil Unnai Kanden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irudhiyil Unnai Kanden"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irudhaiya Poovil Kanden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irudhaiya Poovil Kanden"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irudhiyil Unnai Kanden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irudhiyil Unnai Kanden"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irudhaiya Poovil Kandaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irudhaiya Poovil Kandaen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannamoochi Yenada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannamoochi Yenada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Kanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kanna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Kannadi Porul Polada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Kannadi Porul Polada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Manam Unakkoru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Manam Unakkoru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vilaiyaattu Bommaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilaiyaattu Bommaiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Manam Unakkoru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Manam Unakkoru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vilaiyaattu Bommaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilaiyaattu Bommaiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakkena Unarchigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkena Unarchigal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaniyaaga Illaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaniyaaga Illaiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjin Azhai Urangaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjin Azhai Urangaadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Idhazh Kondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Idhazh Kondu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaai Mooda Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaai Mooda Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Kannaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kannaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Idhazh Kondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Idhazh Kondu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaai Mooda Vaa En Kannaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaai Mooda Vaa En Kannaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Imai Kondu Vizhi Mooda Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Imai Kondu Vizhi Mooda Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Udal Dhaan En Udai Allavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Udal Dhaan En Udai Allavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paar Kadalil Aadiya Pinnum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paar Kadalil Aadiya Pinnum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Vannam Maaravillai Innum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Vannam Maaravillai Innum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paar Kadalil Aadiya Pinnum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paar Kadalil Aadiya Pinnum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Vannam Maaravillai Innum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Vannam Maaravillai Innum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Nenjil Koodiye Niram Maaravaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nenjil Koodiye Niram Maaravaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennuyiril Nee Vandhu Serga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennuyiril Nee Vandhu Serga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Udhadugal Eeramaai Vaazhga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udhadugal Eeramaai Vaazhga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalandhidava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalandhidava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannamoochi Yenadaa En Kannaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannamoochi Yenadaa En Kannaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Kannaadi Porul Polada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Kannaadi Porul Polada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaan Mazhai Vizhumbodhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaan Mazhai Vizhumbodhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Malai Kondu Kaaththaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malai Kondu Kaaththaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaan Mazhai Vizhumbodhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaan Mazhai Vizhumbodhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Malai Kondu Kaaththaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malai Kondu Kaaththaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kan Mazhai Vilumbodhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan Mazhai Vilumbodhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edhil Ennai Kaappaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhil Ennai Kaappaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poovin Kanneerai Rasippaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovin Kanneerai Rasippaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Enna Penn Illaiyaa En Kanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Enna Penn Illaiyaa En Kanna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhai Nee Kaana Kannillaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai Nee Kaana Kannillaiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Kanavugalil Naan Illaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kanavugalil Naan Illaiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhinam Oosalaaduthen Manasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam Oosalaaduthen Manasu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Oomai Alla En Kolusu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Oomai Alla En Kolusu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Ullmoochiley Uyir Neengudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Ullmoochiley Uyir Neengudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennuyir Thudikkaamaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennuyir Thudikkaamaley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaappadhu Un Theendaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaappadhu Un Theendaley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
 <div class="lyrico-lyrics-wrapper">Uyir Thara Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Thara Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannamoochi Yenadaa En Kanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannamoochi Yenadaa En Kanna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Kannadi Porul Polada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Kannadi Porul Polada"/>
</div>
</pre>
