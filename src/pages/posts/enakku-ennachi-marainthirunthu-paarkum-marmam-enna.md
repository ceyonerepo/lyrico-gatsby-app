---
title: "enakku ennachi song lyrics"
album: "Marainthirunthu Paarkum Marmam Enna"
artist: "Achu Rajamani"
lyricist: "Pa Vijay"
director: "R. Rahesh"
path: "/albums/marainthirunthu-paarkum-marmam-enna-lyrics"
song: "Enakku Ennachi"
image: ../../images/albumart/marainthirunthu-paarkum-marmam-enna.jpg
date: 2018-08-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Aip-LCEL5kY"
type: "love"
singers:
  - Ala B Bala
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">enaaku ennachi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enaaku ennachi "/>
</div>
<div class="lyrico-lyrics-wrapper">dantanaukku danaku dannachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dantanaukku danaku dannachi"/>
</div>
<div class="lyrico-lyrics-wrapper">santhadi saakula naakula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="santhadi saakula naakula"/>
</div>
<div class="lyrico-lyrics-wrapper">un peru ottikichchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un peru ottikichchi"/>
</div>
<div class="lyrico-lyrics-wrapper">manasukullara ennamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasukullara ennamo"/>
</div>
<div class="lyrico-lyrics-wrapper">kirukku undaachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kirukku undaachi"/>
</div>
<div class="lyrico-lyrics-wrapper">ipadi aanathu illaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ipadi aanathu illaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">ellam maridichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellam maridichi"/>
</div>
<div class="lyrico-lyrics-wrapper">ethukunu than theriyalaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethukunu than theriyalaye"/>
</div>
<div class="lyrico-lyrics-wrapper">enna panatum puriyalaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna panatum puriyalaye"/>
</div>
<div class="lyrico-lyrics-wrapper">sonna pecha ketkama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sonna pecha ketkama"/>
</div>
<div class="lyrico-lyrics-wrapper">en manasu oduthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en manasu oduthey"/>
</div>
<div class="lyrico-lyrics-wrapper">ada poochandi poochandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada poochandi poochandi"/>
</div>
<div class="lyrico-lyrics-wrapper">una paarthale paarthale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="una paarthale paarthale"/>
</div>
<div class="lyrico-lyrics-wrapper">en kaluthu  narambu iluthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kaluthu  narambu iluthu"/>
</div>
<div class="lyrico-lyrics-wrapper">kaththuthu enna senja nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaththuthu enna senja nee"/>
</div>
<div class="lyrico-lyrics-wrapper">ada kannadi kannadi athu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada kannadi kannadi athu"/>
</div>
<div class="lyrico-lyrics-wrapper">munnadi munnadi nan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munnadi munnadi nan"/>
</div>
<div class="lyrico-lyrics-wrapper">sirikum pozhudhu enaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sirikum pozhudhu enaku"/>
</div>
<div class="lyrico-lyrics-wrapper">pathila eppadi nikura nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathila eppadi nikura nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">naatu nadu rathiriyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naatu nadu rathiriyil"/>
</div>
<div class="lyrico-lyrics-wrapper">nan matume muluchukiten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan matume muluchukiten"/>
</div>
<div class="lyrico-lyrics-wrapper">thoongama thoongama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoongama thoongama"/>
</div>
<div class="lyrico-lyrics-wrapper">porandu kitteney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="porandu kitteney"/>
</div>
<div class="lyrico-lyrics-wrapper">motta madi mel irupen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="motta madi mel irupen"/>
</div>
<div class="lyrico-lyrics-wrapper">roatula nee pogaiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="roatula nee pogaiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">un kooda en nenja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kooda en nenja"/>
</div>
<div class="lyrico-lyrics-wrapper">kuthika vittene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuthika vittene"/>
</div>
<div class="lyrico-lyrics-wrapper">en intha kirukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en intha kirukku"/>
</div>
<div class="lyrico-lyrics-wrapper">intha kiruku japane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha kiruku japane"/>
</div>
<div class="lyrico-lyrics-wrapper">en japane en nenjukulla than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en japane en nenjukulla than"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjukulla than nippene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjukulla than nippene"/>
</div>
<div class="lyrico-lyrics-wrapper">vanthu nippene iyoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanthu nippene iyoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">enaaku ennachi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enaaku ennachi "/>
</div>
<div class="lyrico-lyrics-wrapper">dantanaukku danaku dannachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dantanaukku danaku dannachi"/>
</div>
<div class="lyrico-lyrics-wrapper">santhadi saakula naakula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="santhadi saakula naakula"/>
</div>
<div class="lyrico-lyrics-wrapper">un peru ottikichchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un peru ottikichchi"/>
</div>
<div class="lyrico-lyrics-wrapper">manasukullara ennamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasukullara ennamo"/>
</div>
<div class="lyrico-lyrics-wrapper">kirukku undaachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kirukku undaachi"/>
</div>
<div class="lyrico-lyrics-wrapper">ipadi aanathu illaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ipadi aanathu illaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">ellam maridichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellam maridichi"/>
</div>
<div class="lyrico-lyrics-wrapper">ethukunu than theriyalaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethukunu than theriyalaye"/>
</div>
<div class="lyrico-lyrics-wrapper">enna panatum puriyalaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna panatum puriyalaye"/>
</div>
<div class="lyrico-lyrics-wrapper">sonna pecha ketkama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sonna pecha ketkama"/>
</div>
<div class="lyrico-lyrics-wrapper">en manasu oduthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en manasu oduthey"/>
</div>
<div class="lyrico-lyrics-wrapper">ada poochandi poochandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada poochandi poochandi"/>
</div>
<div class="lyrico-lyrics-wrapper">una paarthale paarthale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="una paarthale paarthale"/>
</div>
<div class="lyrico-lyrics-wrapper">en kaluthu  narambu iluthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kaluthu  narambu iluthu"/>
</div>
<div class="lyrico-lyrics-wrapper">kaththuthu enna senja nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaththuthu enna senja nee"/>
</div>
<div class="lyrico-lyrics-wrapper">ada kannadi kannadi athu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada kannadi kannadi athu"/>
</div>
<div class="lyrico-lyrics-wrapper">munnadi munnadi nan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munnadi munnadi nan"/>
</div>
<div class="lyrico-lyrics-wrapper">sirikum pozhudhu enaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sirikum pozhudhu enaku"/>
</div>
<div class="lyrico-lyrics-wrapper">pathila eppadi nikura nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathila eppadi nikura nee"/>
</div>
</pre>
