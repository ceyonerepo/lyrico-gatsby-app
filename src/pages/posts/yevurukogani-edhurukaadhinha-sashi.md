---
title: "yevurukogani song lyrics"
album: "Sashi"
artist: "Arun Chiluveru"
lyricist: "Anantha Sriram"
director: "Srinivas Naidu Nadikatla"
path: "/albums/sashi-lyrics"
song: "Yevurukogani edhurukaadhinha"
image: ../../images/albumart/sashi.jpg
date: 2021-03-19
lang: telugu
youtubeLink: "https://www.youtube.com/embed/w5AqtQ6r6Wk"
type: "sad"
singers:
  - Nayayan Iyer
  - Naresh Iyer
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">evarikogani edhurukaadhinha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="evarikogani edhurukaadhinha"/>
</div>
<div class="lyrico-lyrics-wrapper">Baruvai epudilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baruvai epudilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Okaritho shwasa okaritho cheyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okaritho shwasa okaritho cheyi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalapalante adhi ela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalapalante adhi ela"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanipinche aa runamukame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanipinche aa runamukame"/>
</div>
<div class="lyrico-lyrics-wrapper">Janma talavanchindi kshanamuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Janma talavanchindi kshanamuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Pranam panche manasune
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pranam panche manasune"/>
</div>
<div class="lyrico-lyrics-wrapper">Nedu vadhilesindha chivarana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nedu vadhilesindha chivarana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">evarikogani edhurukaadhinha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="evarikogani edhurukaadhinha"/>
</div>
<div class="lyrico-lyrics-wrapper">Baruvai epudilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baruvai epudilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Okaritho shwasa okaritho cheyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okaritho shwasa okaritho cheyi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalapalante adhi ela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalapalante adhi ela"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Niluvuna ninnuu nilupukunnaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niluvuna ninnuu nilupukunnaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Vadhalan vadhalana kudhuruna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadhalan vadhalana kudhuruna"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanulalo ninnu kalupukunnaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanulalo ninnu kalupukunnaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Marvana maruvana jaruguna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marvana maruvana jaruguna"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayina gaani vadhilesthunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayina gaani vadhilesthunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasunu anuchukoni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasunu anuchukoni"/>
</div>
<div class="lyrico-lyrics-wrapper">Mari neekosam marichesthunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mari neekosam marichesthunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Maroka bathukuthoni malupuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maroka bathukuthoni malupuna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Evarikogani edhurukaadhinha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evarikogani edhurukaadhinha"/>
</div>
<div class="lyrico-lyrics-wrapper">Baruvai epudilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baruvai epudilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Okaritho shwasa okaritho cheyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okaritho shwasa okaritho cheyi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalapalante adhi ela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalapalante adhi ela"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanipinche aa runamukame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanipinche aa runamukame"/>
</div>
<div class="lyrico-lyrics-wrapper">Janma talavanchindi kshanamuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Janma talavanchindi kshanamuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Pranam panche manasune
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pranam panche manasune"/>
</div>
<div class="lyrico-lyrics-wrapper">Nedu vadhilesindha chivarana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nedu vadhilesindha chivarana"/>
</div>
</pre>
