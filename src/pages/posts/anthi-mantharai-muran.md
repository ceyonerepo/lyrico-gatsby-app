---
title: "anthi mantharai song lyrics"
album: "Muran"
artist: "Sajan Madhav"
lyricist: "Priyan"
director: "Rajan Madhav"
path: "/albums/muran-lyrics"
song: "Anthi Mantharai"
image: ../../images/albumart/muran.jpg
date: 2011-09-30
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Vl-V9KT9hGM"
type: "happy"
singers:
  - Rita Thyagarajan
  - Daya
  - Naveen Madhav
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Andhimantharai poo poo pookum neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhimantharai poo poo pookum neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada innum enna thooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada innum enna thooram"/>
</div>
<div class="lyrico-lyrics-wrapper">Kollum manadhu koo koo koochal podum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kollum manadhu koo koo koochal podum"/>
</div>
<div class="lyrico-lyrics-wrapper">ellaikalai meerum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellaikalai meerum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andhimantharai poo poo pookum neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhimantharai poo poo pookum neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Innum innum enna thooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum innum enna thooram"/>
</div>
<div class="lyrico-lyrics-wrapper">Kollum manadhu koo koo koochal podum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kollum manadhu koo koo koochal podum"/>
</div>
<div class="lyrico-lyrics-wrapper">ellai ellaikalai meerum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellai ellaikalai meerum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ingum angum palakkangal meyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingum angum palakkangal meyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaniye thoorathil nilavum kaayum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaniye thoorathil nilavum kaayum"/>
</div>
<div class="lyrico-lyrics-wrapper">Etti nikkaadhe vendum vegam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Etti nikkaadhe vendum vegam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasai theeraamal iravo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai theeraamal iravo "/>
</div>
<div class="lyrico-lyrics-wrapper">alaindhu thaeyum thaeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alaindhu thaeyum thaeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">unmel paayum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unmel paayum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andhimantharai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhimantharai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iraviltaan vaanavil naan -nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iraviltaan vaanavil naan -nee"/>
</div>
<div class="lyrico-lyrics-wrapper">thottal podhum sindhum enthan vannam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thottal podhum sindhum enthan vannam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagal ingae thevayilai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagal ingae thevayilai"/>
</div>
<div class="lyrico-lyrics-wrapper">neelattum indha pozhu innum innum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neelattum indha pozhu innum innum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ila nenjil aasai oonjal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ila nenjil aasai oonjal"/>
</div>
<div class="lyrico-lyrics-wrapper">Idai velai indri aadum yogam konjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idai velai indri aadum yogam konjam"/>
</div>
<div class="lyrico-lyrics-wrapper">Idi minnal puyalai pole
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idi minnal puyalai pole"/>
</div>
<div class="lyrico-lyrics-wrapper">oru Nodiyil maarum nenjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru Nodiyil maarum nenjam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iruttukendrum koochamillai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruttukendrum koochamillai "/>
</div>
<div class="lyrico-lyrics-wrapper">naam sollvadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naam sollvadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Acham vittu micham thottal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Acham vittu micham thottal "/>
</div>
<div class="lyrico-lyrics-wrapper">uchcham nee selvadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uchcham nee selvadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Andhimantharai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhimantharai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Megam megamum urasikollum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megam megamum urasikollum "/>
</div>
<div class="lyrico-lyrics-wrapper">karrupu iravile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karrupu iravile"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhegamum dhegamum urasi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhegamum dhegamum urasi "/>
</div>
<div class="lyrico-lyrics-wrapper">kondaal nerupu manadhile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kondaal nerupu manadhile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vattathukulle vaazhum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vattathukulle vaazhum "/>
</div>
<div class="lyrico-lyrics-wrapper">vazhkai inbam illaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vazhkai inbam illaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottai thaandum podhe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottai thaandum podhe "/>
</div>
<div class="lyrico-lyrics-wrapper">sparisam eerkum unnaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sparisam eerkum unnaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanjathanam un idayilum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanjathanam un idayilum "/>
</div>
<div class="lyrico-lyrics-wrapper">udaiyilum naan kaanpadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="udaiyilum naan kaanpadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Panjam mattum illai en azhagendru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panjam mattum illai en azhagendru"/>
</div>
<div class="lyrico-lyrics-wrapper">Theenda theenda thilaikirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theenda theenda thilaikirathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andhimantharai poo poo pookum neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhimantharai poo poo pookum neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada innum enna thooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada innum enna thooram"/>
</div>
<div class="lyrico-lyrics-wrapper">Kollum manadhu koo koo koochal podum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kollum manadhu koo koo koochal podum"/>
</div>
<div class="lyrico-lyrics-wrapper">ellaikalai meerum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellaikalai meerum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ingum angum palakkangal meyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingum angum palakkangal meyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaniye thoorathil nilavum kaayum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaniye thoorathil nilavum kaayum"/>
</div>
<div class="lyrico-lyrics-wrapper">Etti nikkaadhe vendum vegam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Etti nikkaadhe vendum vegam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasai theeraamal iravo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai theeraamal iravo "/>
</div>
<div class="lyrico-lyrics-wrapper">alaindhu thaeyum thaeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alaindhu thaeyum thaeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">unmel paayum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unmel paayum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andhimantharai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhimantharai"/>
</div>
</pre>
