---
title: "sundaranaayavane song lyrics"
album: "Halal Love Story"
artist: "Bijibal - Shahabaz Aman"
lyricist: "Muhsin Parari"
director: "Zakariya Mohammed"
path: "/albums/halal-love-story-lyrics"
song: "Sundaranaayavane"
image: ../../images/albumart/halal-love-story.jpg
date: 2020-10-15
lang: malayalam
youtubeLink: "https://www.youtube.com/embed/eDOODIDoXH4"
type: "melody"
singers:
  - Shahabaz Aman
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">sundaranaayavane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sundaranaayavane"/>
</div>
<div class="lyrico-lyrics-wrapper">subhaanalla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="subhaanalla"/>
</div>
<div class="lyrico-lyrics-wrapper">alhamdulillah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alhamdulillah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sundaranaayavane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sundaranaayavane"/>
</div>
<div class="lyrico-lyrics-wrapper">subhaanalla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="subhaanalla"/>
</div>
<div class="lyrico-lyrics-wrapper">alhamdulillah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alhamdulillah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ullin hilaalaaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullin hilaalaaya"/>
</div>
<div class="lyrico-lyrics-wrapper">kannin jamaalaaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannin jamaalaaya"/>
</div>
<div class="lyrico-lyrics-wrapper">kaathin kalaamaaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaathin kalaamaaya"/>
</div>
<div class="lyrico-lyrics-wrapper">paarin kamaalaaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paarin kamaalaaya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaattathoru poovinte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaattathoru poovinte"/>
</div>
<div class="lyrico-lyrics-wrapper">madhurikkum manamaaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="madhurikkum manamaaya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sundaranaayavane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sundaranaayavane"/>
</div>
<div class="lyrico-lyrics-wrapper">subhaanallaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="subhaanallaa"/>
</div>
<div class="lyrico-lyrics-wrapper">alhamdulillah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alhamdulillah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">chelilaalathin nenjathurappinte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chelilaalathin nenjathurappinte"/>
</div>
<div class="lyrico-lyrics-wrapper">jabalukal therthoru kone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jabalukal therthoru kone"/>
</div>
<div class="lyrico-lyrics-wrapper">nerinaazhathil niyyathurappichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nerinaazhathil niyyathurappichen"/>
</div>
<div class="lyrico-lyrics-wrapper">amalukal seenathaakkene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="amalukal seenathaakkene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">lavil ajabinte  thoppil inakkathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lavil ajabinte  thoppil inakkathil"/>
</div>
<div class="lyrico-lyrics-wrapper">hrudayangal viriyicha hubbe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hrudayangal viriyicha hubbe"/>
</div>
<div class="lyrico-lyrics-wrapper">naavil adabinte  noorin thilakkathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naavil adabinte  noorin thilakkathil"/>
</div>
<div class="lyrico-lyrics-wrapper">vaakkennil muthaakk rabbe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaakkennil muthaakk rabbe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sundaranaayavane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sundaranaayavane"/>
</div>
<div class="lyrico-lyrics-wrapper">subhaanallaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="subhaanallaa"/>
</div>
<div class="lyrico-lyrics-wrapper">alhamdulillah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alhamdulillah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ullin hilaalaaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ullin hilaalaaya"/>
</div>
<div class="lyrico-lyrics-wrapper">kannin jamaalaaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannin jamaalaaya"/>
</div>
<div class="lyrico-lyrics-wrapper">kaathin kalaamaaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaathin kalaamaaya"/>
</div>
<div class="lyrico-lyrics-wrapper">paarin kamaalaaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paarin kamaalaaya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaattathoru poovinte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaattathoru poovinte"/>
</div>
<div class="lyrico-lyrics-wrapper">madhurikkum manamaaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="madhurikkum manamaaya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sundaranaayavane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sundaranaayavane"/>
</div>
<div class="lyrico-lyrics-wrapper">subhaanallaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="subhaanallaa"/>
</div>
<div class="lyrico-lyrics-wrapper">alhamdulillah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alhamdulillah"/>
</div>
</pre>
