---
title: "thudithezhu thozha song lyrics"
album: "Taanakkaran"
artist: "Ghibran"
lyricist: "Chandru"
director: "Tamizh"
path: "/albums/taanakkaran-lyrics"
song: "Thudithezhu Thozha"
image: ../../images/albumart/taanakkaran.jpg
date: 2022-04-08
lang: tamil
youtubeLink: "https://www.youtube.com/embed/BAxKJ9cHkMc"
type: "mass"
singers:
  - Shenbagaraj
  - Aravind Srinivas
  - Narayanan
  - Sarath Santosh	
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">thudithezhu thozha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thudithezhu thozha"/>
</div>
<div class="lyrico-lyrics-wrapper">idi saththam neeya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idi saththam neeya"/>
</div>
<div class="lyrico-lyrics-wrapper">vizhithidu thozha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vizhithidu thozha"/>
</div>
<div class="lyrico-lyrics-wrapper">vidivelli neeya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidivelli neeya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thudithezhu thozha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thudithezhu thozha"/>
</div>
<div class="lyrico-lyrics-wrapper">idi saththam neeya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idi saththam neeya"/>
</div>
<div class="lyrico-lyrics-wrapper">vizhithidu thozha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vizhithidu thozha"/>
</div>
<div class="lyrico-lyrics-wrapper">vidivelli neeya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidivelli neeya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oththa karuththai nillada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oththa karuththai nillada"/>
</div>
<div class="lyrico-lyrics-wrapper">moththa ulagai vellada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moththa ulagai vellada"/>
</div>
<div class="lyrico-lyrics-wrapper">sigaram uyaram illada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sigaram uyaram illada"/>
</div>
<div class="lyrico-lyrics-wrapper">siragai viriththal ellada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="siragai viriththal ellada"/>
</div>
<div class="lyrico-lyrics-wrapper">viralai neetum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viralai neetum"/>
</div>
<div class="lyrico-lyrics-wrapper">dhisaiyil kaatru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhisaiyil kaatru"/>
</div>
<div class="lyrico-lyrics-wrapper">vetri namadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetri namadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">vidiyum vidiyum vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidiyum vidiyum vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pirappaal perumai illada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pirappaal perumai illada"/>
</div>
<div class="lyrico-lyrics-wrapper">neruppai thindru seridaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neruppai thindru seridaa"/>
</div>
<div class="lyrico-lyrics-wrapper">kidaiththa vaaippu ithuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kidaiththa vaaippu ithuda"/>
</div>
<div class="lyrico-lyrics-wrapper">thadaiyai thagarthu erida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thadaiyai thagarthu erida"/>
</div>
<div class="lyrico-lyrics-wrapper">viralai neetum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viralai neetum"/>
</div>
<div class="lyrico-lyrics-wrapper">dhisaiyil kaatru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhisaiyil kaatru"/>
</div>
<div class="lyrico-lyrics-wrapper">vetri namadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetri namadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">vidiyum vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidiyum vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vetrikkum tholvikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetrikkum tholvikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">idaipatta dhoorandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idaipatta dhoorandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">irukindra idamendru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irukindra idamendru"/>
</div>
<div class="lyrico-lyrics-wrapper">asaipottu parenda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="asaipottu parenda"/>
</div>
<div class="lyrico-lyrics-wrapper">unnai nee unnai nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai nee unnai nee"/>
</div>
<div class="lyrico-lyrics-wrapper">usuppudaa uyirena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="usuppudaa uyirena"/>
</div>
<div class="lyrico-lyrics-wrapper">manmuttum vaanmuttum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manmuttum vaanmuttum"/>
</div>
<div class="lyrico-lyrics-wrapper">vaan muttum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaan muttum"/>
</div>
<div class="lyrico-lyrics-wrapper">maramaagi nillada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maramaagi nillada"/>
</div>
<div class="lyrico-lyrics-wrapper">thodu thodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thodu thodu"/>
</div>
<div class="lyrico-lyrics-wrapper">thodu vaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thodu vaanam"/>
</div>
<div class="lyrico-lyrics-wrapper">dhooram illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhooram illai"/>
</div>
<div class="lyrico-lyrics-wrapper">edu edu edu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edu edu edu"/>
</div>
<div class="lyrico-lyrics-wrapper">thuyar baaram illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thuyar baaram illai"/>
</div>
<div class="lyrico-lyrics-wrapper">tharigida thaka tharigida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tharigida thaka tharigida"/>
</div>
<div class="lyrico-lyrics-wrapper">thaka tharigida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaka tharigida"/>
</div>
<div class="lyrico-lyrics-wrapper">thaka thaka thaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaka thaka thaka"/>
</div>
<div class="lyrico-lyrics-wrapper">thaka tharigida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaka tharigida"/>
</div>
<div class="lyrico-lyrics-wrapper">thaka thaka thaka tha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaka thaka thaka tha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">suttrum indha boomi than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suttrum indha boomi than"/>
</div>
<div class="lyrico-lyrics-wrapper">katru thantha paadam than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="katru thantha paadam than"/>
</div>
<div class="lyrico-lyrics-wrapper">otrai nimidam oyvendral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="otrai nimidam oyvendral"/>
</div>
<div class="lyrico-lyrics-wrapper">uyirgal mannil edhada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyirgal mannil edhada"/>
</div>
<div class="lyrico-lyrics-wrapper">kizhakku sivakkum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kizhakku sivakkum "/>
</div>
<div class="lyrico-lyrics-wrapper">pothu than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pothu than"/>
</div>
<div class="lyrico-lyrics-wrapper">irulum ingu idamarudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irulum ingu idamarudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">valigal thaangum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valigal thaangum"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjam dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjam dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">vaazhvai vendru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaazhvai vendru"/>
</div>
<div class="lyrico-lyrics-wrapper">vaazhgiradhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaazhgiradhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vetrikkum tholvikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetrikkum tholvikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">idaipatta dhoorandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idaipatta dhoorandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">irukindra idamendru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irukindra idamendru"/>
</div>
<div class="lyrico-lyrics-wrapper">asaipottu parenda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="asaipottu parenda"/>
</div>
<div class="lyrico-lyrics-wrapper">unnai nee unnai nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai nee unnai nee"/>
</div>
<div class="lyrico-lyrics-wrapper">usuppudaa uyirena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="usuppudaa uyirena"/>
</div>
<div class="lyrico-lyrics-wrapper">manmuttum vaanmuttum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manmuttum vaanmuttum"/>
</div>
<div class="lyrico-lyrics-wrapper">vaan muttum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaan muttum"/>
</div>
<div class="lyrico-lyrics-wrapper">maramaagi nillada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maramaagi nillada"/>
</div>
<div class="lyrico-lyrics-wrapper">thodu thodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thodu thodu"/>
</div>
<div class="lyrico-lyrics-wrapper">thodu vaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thodu vaanam"/>
</div>
<div class="lyrico-lyrics-wrapper">dhooram illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhooram illai"/>
</div>
<div class="lyrico-lyrics-wrapper">edu edu edu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edu edu edu"/>
</div>
<div class="lyrico-lyrics-wrapper">thuyar baaram illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thuyar baaram illai"/>
</div>
<div class="lyrico-lyrics-wrapper">tharigida thaka tharigida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tharigida thaka tharigida"/>
</div>
<div class="lyrico-lyrics-wrapper">thaka tharigida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaka tharigida"/>
</div>
<div class="lyrico-lyrics-wrapper">thaka thaka thaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaka thaka thaka"/>
</div>
<div class="lyrico-lyrics-wrapper">thaka tharigida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaka tharigida"/>
</div>
<div class="lyrico-lyrics-wrapper">thaka thaka thaka tha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaka thaka thaka tha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thudithezhu thozha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thudithezhu thozha"/>
</div>
<div class="lyrico-lyrics-wrapper">idi saththam neeya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idi saththam neeya"/>
</div>
<div class="lyrico-lyrics-wrapper">vizhithidu thozha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vizhithidu thozha"/>
</div>
<div class="lyrico-lyrics-wrapper">vidivelli neeya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidivelli neeya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thudithezhu thozha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thudithezhu thozha"/>
</div>
<div class="lyrico-lyrics-wrapper">idi saththam neeya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idi saththam neeya"/>
</div>
<div class="lyrico-lyrics-wrapper">vizhithidu thozha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vizhithidu thozha"/>
</div>
<div class="lyrico-lyrics-wrapper">vidivelli neeya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidivelli neeya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">puzhudhi parakka nadada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puzhudhi parakka nadada"/>
</div>
<div class="lyrico-lyrics-wrapper">puyalai kilappi vidada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puyalai kilappi vidada"/>
</div>
<div class="lyrico-lyrics-wrapper">unnai jeyikka evanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai jeyikka evanum"/>
</div>
<div class="lyrico-lyrics-wrapper">illai enbadhu nijamdaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="illai enbadhu nijamdaa"/>
</div>
<div class="lyrico-lyrics-wrapper">viralai neetum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viralai neetum"/>
</div>
<div class="lyrico-lyrics-wrapper">dhisaiyil kaatru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhisaiyil kaatru"/>
</div>
<div class="lyrico-lyrics-wrapper">vetri namadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetri namadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">vidiyum vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidiyum vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kottum kurudhi unakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kottum kurudhi unakku"/>
</div>
<div class="lyrico-lyrics-wrapper">sottum viyarvai thaandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sottum viyarvai thaandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">kazhugu paarvai kollada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kazhugu paarvai kollada"/>
</div>
<div class="lyrico-lyrics-wrapper">ilakkai nokki selladaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ilakkai nokki selladaa"/>
</div>
<div class="lyrico-lyrics-wrapper">viralai neetum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viralai neetum"/>
</div>
<div class="lyrico-lyrics-wrapper">dhisaiyil kaatru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhisaiyil kaatru"/>
</div>
<div class="lyrico-lyrics-wrapper">vetri namadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetri namadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">vidiyum vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidiyum vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vetrikkum tholvikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetrikkum tholvikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">idaipatta dhoorandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idaipatta dhoorandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">irukindra idamendru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irukindra idamendru"/>
</div>
<div class="lyrico-lyrics-wrapper">asaipottu parenda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="asaipottu parenda"/>
</div>
<div class="lyrico-lyrics-wrapper">unnai nee unnai nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai nee unnai nee"/>
</div>
<div class="lyrico-lyrics-wrapper">usuppudaa uyirena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="usuppudaa uyirena"/>
</div>
<div class="lyrico-lyrics-wrapper">manmuttum vaanmuttum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manmuttum vaanmuttum"/>
</div>
<div class="lyrico-lyrics-wrapper">vaan muttum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaan muttum"/>
</div>
<div class="lyrico-lyrics-wrapper">maramaagi nillada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maramaagi nillada"/>
</div>
<div class="lyrico-lyrics-wrapper">thodu thodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thodu thodu"/>
</div>
<div class="lyrico-lyrics-wrapper">thodu vaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thodu vaanam"/>
</div>
<div class="lyrico-lyrics-wrapper">dhooram illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhooram illai"/>
</div>
<div class="lyrico-lyrics-wrapper">edu edu edu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edu edu edu"/>
</div>
<div class="lyrico-lyrics-wrapper">thuyar baaram illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thuyar baaram illai"/>
</div>
<div class="lyrico-lyrics-wrapper">tharigida thaka tharigida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tharigida thaka tharigida"/>
</div>
<div class="lyrico-lyrics-wrapper">thaka tharigida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaka tharigida"/>
</div>
<div class="lyrico-lyrics-wrapper">thaka thaka thaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaka thaka thaka"/>
</div>
<div class="lyrico-lyrics-wrapper">thaka tharigida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaka tharigida"/>
</div>
<div class="lyrico-lyrics-wrapper">thaka thaka thaka tha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaka thaka thaka tha"/>
</div>
</pre>
