---
title: "surprise me song lyrics"
album: "Pyaar Prema Kaadhal"
artist: "Yuvan Shankar Raja"
lyricist: "Madhan Karky"
director: "Elan"
path: "/albums/pyaar-prema-kaadhal-lyrics"
song: "Surprise Me"
image: ../../images/albumart/pyaar-prema-kaadhal.jpg
date: 2018-08-10
lang: tamil
youtubeLink: "https://www.youtube.com/embed/VzppuKWR-5U"
type: "love"
singers:
  - Yuvan Shankar Raja
  - Priya Mali
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kanavu kanavu pookkalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavu kanavu pookkalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninaivil nugarnthu paarkkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaivil nugarnthu paarkkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">En naavil naanae en thaenaagiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En naavil naanae en thaenaagiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Siragu siragu megathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siragu siragu megathai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aninthu paranthu pogiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aninthu paranthu pogiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanattra naanaaga naanaagiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanattra naanaaga naanaagiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaaaahaa ohoo oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaaahaa ohoo oo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ohoo veesum vellai kaatril
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohoo veesum vellai kaatril"/>
</div>
<div class="lyrico-lyrics-wrapper">En swasam neenga paarka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En swasam neenga paarka"/>
</div>
<div class="lyrico-lyrics-wrapper">En dhega theeyil aadai neithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En dhega theeyil aadai neithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pootti vaithenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pootti vaithenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Antha saayam pona vaanil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha saayam pona vaanil"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru vaanavillai polae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru vaanavillai polae"/>
</div>
<div class="lyrico-lyrics-wrapper">En vaazhvil vannam sertha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En vaazhvil vannam sertha"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai kaadhal kondenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai kaadhal kondenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En maarbilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En maarbilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un moochu paayum antha velaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un moochu paayum antha velaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannin oram neer thuli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannin oram neer thuli"/>
</div>
<div class="lyrico-lyrics-wrapper">Veezha kandenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veezha kandenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeh yeh yeh yehiyeh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh yeh yeh yehiyeh"/>
</div>
<div class="lyrico-lyrics-wrapper">Veesum vellai kaatril
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veesum vellai kaatril"/>
</div>
<div class="lyrico-lyrics-wrapper">En swasam neenga paarka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En swasam neenga paarka"/>
</div>
<div class="lyrico-lyrics-wrapper">En dhega theeyil aadai neithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En dhega theeyil aadai neithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pootti vaithenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pootti vaithenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Antha saayam pona vaanil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha saayam pona vaanil"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru vaanavillai pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru vaanavillai pola"/>
</div>
<div class="lyrico-lyrics-wrapper">En vaazhvil vannam sertha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En vaazhvil vannam sertha"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai kaadhal kondenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai kaadhal kondenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pani malaigalil amaidhiyai pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pani malaigalil amaidhiyai pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee vaazhnthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee vaazhnthaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan varum varaiyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan varum varaiyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Puyalena naan thaakkinen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puyalena naan thaakkinen"/>
</div>
<div class="lyrico-lyrics-wrapper">Yethetho minnalgal veesinen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethetho minnalgal veesinen"/>
</div>
<div class="lyrico-lyrics-wrapper">Oyamal jovendru pesinen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oyamal jovendru pesinen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Marakilaigalil vazhinthidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marakilaigalil vazhinthidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhai thuli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai thuli"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharai vizhum munnae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharai vizhum munnae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uraivathai pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uraivathai pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee uthirukkum vaarthai ovvondrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee uthirukkum vaarthai ovvondrum"/>
</div>
<div class="lyrico-lyrics-wrapper">Urainthu poga kandenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urainthu poga kandenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhai kaadhal endra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhai kaadhal endra"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinna sollil pooti vaikathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna sollil pooti vaikathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaaaahaa ohoo oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaaahaa ohoo oo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veesum vellai kaatril
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veesum vellai kaatril"/>
</div>
<div class="lyrico-lyrics-wrapper">En swasam neenga paarka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En swasam neenga paarka"/>
</div>
<div class="lyrico-lyrics-wrapper">En dhega theeyil aadai neithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En dhega theeyil aadai neithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pootti vaithenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pootti vaithenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Antha saayam pona vaanil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha saayam pona vaanil"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru vaanavillai polae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru vaanavillai polae"/>
</div>
<div class="lyrico-lyrics-wrapper">En vaazhvil vannam sertha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En vaazhvil vannam sertha"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai kaadhal kondenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai kaadhal kondenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En maarbilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En maarbilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un moochu paayum antha velaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un moochu paayum antha velaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannin oram neer thuli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannin oram neer thuli"/>
</div>
<div class="lyrico-lyrics-wrapper">Veezha kandenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veezha kandenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pyaar prema kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pyaar prema kaadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Pyaar prema kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pyaar prema kaadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Pyaar prema kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pyaar prema kaadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Pyaar prema kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pyaar prema kaadhal"/>
</div>
</pre>
