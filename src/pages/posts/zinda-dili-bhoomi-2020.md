---
title: "zinda dili song lyrics"
album: "Bhoomi 2020"
artist: "Salim Sulaiman"
lyricist: "Niranjan Iyengar"
director: "Shakti Hasija"
path: "/albums/bhoomi-2020-lyrics"
song: "Zinda Dili"
image: ../../images/albumart/bhoomi-2020.jpg
date: 2020-12-30
lang: hindi
youtubeLink: "https://www.youtube.com/embed/846eLq5QKFk"
type: "happy"
singers:
  - Arijit Singh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Pal do pal ki hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pal do pal ki hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeh apni zindgaani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh apni zindgaani"/>
</div>
<div class="lyrico-lyrics-wrapper">Jee le toh suhaani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jee le toh suhaani"/>
</div>
<div class="lyrico-lyrics-wrapper">Ya phir hai beimaani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ya phir hai beimaani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Khwabon ke dum pe hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khwabon ke dum pe hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Iski har rawani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iski har rawani"/>
</div>
<div class="lyrico-lyrics-wrapper">Iski dhun pe naache
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iski dhun pe naache"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoke hum roohaani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoke hum roohaani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aankhon ne aankhon se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aankhon ne aankhon se"/>
</div>
<div class="lyrico-lyrics-wrapper">Nazron ki zabaani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nazron ki zabaani"/>
</div>
<div class="lyrico-lyrics-wrapper">Lafzon se chhupayi hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lafzon se chhupayi hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeh woh kahaani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh woh kahaani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Din ke saaye mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Din ke saaye mein"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho raatein bhi begaani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho raatein bhi begaani"/>
</div>
<div class="lyrico-lyrics-wrapper">Kar dein hum fanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kar dein hum fanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Apni bezubaani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Apni bezubaani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Zinda dili.. Yahoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zinda dili.. Yahoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Zinda dili.. Yahoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zinda dili.. Yahoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Zinda dili.. O..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zinda dili.. O.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Roothi raaton ki hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Roothi raaton ki hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Jhoothi yeh siyaahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jhoothi yeh siyaahi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sooni saanson se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sooni saanson se"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu paa lega judayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu paa lega judayi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jeena hai tujhe toh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jeena hai tujhe toh"/>
</div>
<div class="lyrico-lyrics-wrapper">De de yeh gawaahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="De de yeh gawaahi"/>
</div>
<div class="lyrico-lyrics-wrapper">Likh de aasmaan pe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Likh de aasmaan pe"/>
</div>
<div class="lyrico-lyrics-wrapper">Apni hi rihaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Apni hi rihaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sun le har ghadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sun le har ghadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Jo deti hai duhayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jo deti hai duhayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Apne haath mein hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Apne haath mein hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Apni hi rubayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Apni hi rubayi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Girti boondon si hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Girti boondon si hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Duniya yeh banayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Duniya yeh banayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Udte lamhon ne hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udte lamhon ne hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Humko yeh sikhayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Humko yeh sikhayi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Zinda dili.. Yahoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zinda dili.. Yahoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Zinda dili..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zinda dili.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Soyi hai jo khushi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soyi hai jo khushi"/>
</div>
<div class="lyrico-lyrics-wrapper">Saanson mein jo basi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saanson mein jo basi"/>
</div>
<div class="lyrico-lyrics-wrapper">Rahein nayi mili mili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rahein nayi mili mili"/>
</div>
<div class="lyrico-lyrics-wrapper">Zinda dili.. Yahoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zinda dili.. Yahoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Zinda dili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zinda dili"/>
</div>
</pre>
