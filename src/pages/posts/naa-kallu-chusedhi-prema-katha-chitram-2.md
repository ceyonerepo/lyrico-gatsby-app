---
title: "naa kallu chusedhi song lyrics"
album: "Prema Katha Chitram 2"
artist: "S.B. Uddhav"
lyricist: "Kasarla Shyam"
director: "Hari Kishan"
path: "/albums/prema-katha-chitram-2-lyrics"
song: "Naa Kallu Chusedhi"
image: ../../images/albumart/prema-katha-chitram-2.jpg
date: 2019-04-06
lang: telugu
youtubeLink: "https://www.youtube.com/embed/SN95YAx4J4o"
type: "love"
singers:
  - Satya Yamini
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Naa kallu choosedi nee kalane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa kallu choosedi nee kalane"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa manasu mosedi neepai oohalane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa manasu mosedi neepai oohalane"/>
</div>
<div class="lyrico-lyrics-wrapper">Chirunavvantu telisindi nee valane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chirunavvantu telisindi nee valane"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa adugu nadichindi ninu cheraalane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa adugu nadichindi ninu cheraalane"/>
</div>
<div class="lyrico-lyrics-wrapper">Prati roju nee raakato modalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prati roju nee raakato modalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalo ika raayani kathalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalo ika raayani kathalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeto ee maate telipedi elaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeto ee maate telipedi elaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Telusaakallu choosedi nee kalane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Telusaakallu choosedi nee kalane"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa manasu mosedi neepai oohalane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa manasu mosedi neepai oohalane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enno enno kopaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enno enno kopaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Anni anni marichaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anni anni marichaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanne nanne maarchetantagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanne nanne maarchetantagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninna monna alakalu ennunnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninna monna alakalu ennunnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinni chinni chinukulu anukunnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinni chinni chinukulu anukunnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Tulli tulli tadisaa vintagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tulli tulli tadisaa vintagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pratisaari vedukutunnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pratisaari vedukutunnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Eduraite ne tappukunnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eduraite ne tappukunnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Eda chaatu maatu aata emito
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eda chaatu maatu aata emito"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Prati roju nee raakato modalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prati roju nee raakato modalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalo ika raayani kathalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalo ika raayani kathalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeto ee maate telipedi elaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeto ee maate telipedi elaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Telusaakallu choosedi nee kalane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Telusaakallu choosedi nee kalane"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa manasu mosedi neepai oohalane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa manasu mosedi neepai oohalane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Allestunna gaalalle 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allestunna gaalalle "/>
</div>
<div class="lyrico-lyrics-wrapper">challestunnaa poovvulne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="challestunnaa poovvulne"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvve velle daarilo mundugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvve velle daarilo mundugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nimpestunnaa rangulu ennenno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nimpestunnaa rangulu ennenno"/>
</div>
<div class="lyrico-lyrics-wrapper">Pampistunnaa sandadulu inkenno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pampistunnaa sandadulu inkenno"/>
</div>
<div class="lyrico-lyrics-wrapper">Vente vuntu neeke needalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vente vuntu neeke needalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Baduledi teliyakunnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baduledi teliyakunnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidalene e velanainaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidalene e velanainaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Toliprema naaku praanamavvagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Toliprema naaku praanamavvagaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Prati roju nee raakato modalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prati roju nee raakato modalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalo ika raayani kathalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalo ika raayani kathalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeto ee maate telipedi elaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeto ee maate telipedi elaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Telusaakallu choosedi nee kalane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Telusaakallu choosedi nee kalane"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa manasu mosedi neepai oohalane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa manasu mosedi neepai oohalane"/>
</div>
</pre>
