---
title: "cinema ante mama song lyrics"
album: "Dubsmash"
artist: "Vamssih B"
lyricist: "Balavardhan"
director: "Keshav Depur"
path: "/albums/dubsmash-lyrics"
song: "Cinema Ante Mama"
image: ../../images/albumart/dubsmash.jpg
date: 2020-01-30
lang: telugu
youtubeLink: "https://www.youtube.com/embed/hbQFQ9aWUyQ"
type: "happy"
singers:
  - Mukesh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Illu Vadhili Vakilodhili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illu Vadhili Vakilodhili"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattukunna Aali Thaali Nammi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattukunna Aali Thaali Nammi"/>
</div>
<div class="lyrico-lyrics-wrapper">Erra Busulekki Studiolu Thirigi Thirigi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Erra Busulekki Studiolu Thirigi Thirigi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ekki Ekki Edichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ekki Ekki Edichi"/>
</div>
<div class="lyrico-lyrics-wrapper">Cinemane Sarvasawam anukoni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cinemane Sarvasawam anukoni"/>
</div>
<div class="lyrico-lyrics-wrapper">Sankanaki Poyaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sankanaki Poyaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yay Eppudu chudu cinima Vallani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yay Eppudu chudu cinima Vallani"/>
</div>
<div class="lyrico-lyrics-wrapper">thidatha VUntaru Goppa Goppavallu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thidatha VUntaru Goppa Goppavallu "/>
</div>
<div class="lyrico-lyrics-wrapper">Vunnaruga Vallagurinchi Cheppocchugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vunnaruga Vallagurinchi Cheppocchugaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Cinema Ante Mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cinema Ante Mama"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinthakaya Pulupu Karam Mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinthakaya Pulupu Karam Mama"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhi Kotra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhi Kotra"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinthakaya Pulupu Karam Mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinthakaya Pulupu Karam Mama"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhi Kotra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhi Kotra"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiyyanguntadhi Anukunte Mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiyyanguntadhi Anukunte Mama"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhi chana Kashtam Mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhi chana Kashtam Mama"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhi Kotra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhi Kotra"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhi chana Kashtam Mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhi chana Kashtam Mama"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhi Kotra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhi Kotra"/>
</div>
<div class="lyrico-lyrics-wrapper">Katthi Meeda Saamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katthi Meeda Saamu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chestharu Maha Mahulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chestharu Maha Mahulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Niluvetthu Bomma Thisi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niluvetthu Bomma Thisi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kasarathu ladinaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kasarathu ladinaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Katthi Meeda Saamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katthi Meeda Saamu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chestharu Maha Mahulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chestharu Maha Mahulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Niluvetthu Bomma Thisi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niluvetthu Bomma Thisi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kasarathu ladinaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kasarathu ladinaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bioscopu Ragupathi Venkayya naidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bioscopu Ragupathi Venkayya naidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nunchi Rushuval Cinema Thecchina HM reddy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nunchi Rushuval Cinema Thecchina HM reddy"/>
</div>
<div class="lyrico-lyrics-wrapper">Black and Wight Bomma Nunchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Black and Wight Bomma Nunchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Color cinemaa Thisina Pullayya CS Rao Goppavaruraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Color cinemaa Thisina Pullayya CS Rao Goppavaruraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalluru Subbhayya Daggara nunchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalluru Subbhayya Daggara nunchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nedu Ramcharan’u Ntr’u Prabhas Varaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nedu Ramcharan’u Ntr’u Prabhas Varaku"/>
</div>
<div class="lyrico-lyrics-wrapper">Surabhi Kamalabai Daggaranunchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Surabhi Kamalabai Daggaranunchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nedu Rakulu Prithi Samantha Keerthi Varaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nedu Rakulu Prithi Samantha Keerthi Varaku"/>
</div>
<div class="lyrico-lyrics-wrapper">Ista Paddaru Mama Kashta Paddaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ista Paddaru Mama Kashta Paddaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ista Paddaru Mama Kashta Paddaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ista Paddaru Mama Kashta Paddaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Telugu cinema Lokanike chupincharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Telugu cinema Lokanike chupincharu"/>
</div>
<div class="lyrico-lyrics-wrapper">Telugu cinema Lokanike chupincharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Telugu cinema Lokanike chupincharu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pixel Cemara Nunchi Digital Varaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pixel Cemara Nunchi Digital Varaku"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennenno Vacchayi Upgretionlu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennenno Vacchayi Upgretionlu"/>
</div>
<div class="lyrico-lyrics-wrapper">Graphicse lenappudu Vitalachaarya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Graphicse lenappudu Vitalachaarya"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottha cinemalanu Lokaniki Chupincharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottha cinemalanu Lokaniki Chupincharu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adigirani Cemara Daggaranunchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adigirani Cemara Daggaranunchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nedu Rathnavelu Chota K simbi Varaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nedu Rathnavelu Chota K simbi Varaku"/>
</div>
<div class="lyrico-lyrics-wrapper">HR padbhanabhaShasri Nunchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="HR padbhanabhaShasri Nunchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nedu Ilayaraja Kiravani Dsp Varaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nedu Ilayaraja Kiravani Dsp Varaku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ista Paddaru Mama Kashta Paddaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ista Paddaru Mama Kashta Paddaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ista Paddaru Mama Kashta Paddaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ista Paddaru Mama Kashta Paddaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Telugu cinema Lokanike chupincharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Telugu cinema Lokanike chupincharu"/>
</div>
<div class="lyrico-lyrics-wrapper">Telugu cinema Lokanike chupincharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Telugu cinema Lokanike chupincharu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ramakrishna Maacharyula Daggaranunchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ramakrishna Maacharyula Daggaranunchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nedu Trivikram Paruchuri Madhav Varaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nedu Trivikram Paruchuri Madhav Varaku"/>
</div>
<div class="lyrico-lyrics-wrapper">Dailogula Varshamtho Vupesaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dailogula Varshamtho Vupesaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Telugu Prekshakulanu Cinematho Kattesharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Telugu Prekshakulanu Cinematho Kattesharu"/>
</div>
<div class="lyrico-lyrics-wrapper">Jandala Kesavadaasu Modalantu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jandala Kesavadaasu Modalantu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nedu Sirivennela Chandrabosu Patalavaraku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nedu Sirivennela Chandrabosu Patalavaraku"/>
</div>
<div class="lyrico-lyrics-wrapper">Ghantasa Nepayya Ganamunchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ghantasa Nepayya Ganamunchi"/>
</div>
<div class="lyrico-lyrics-wrapper">nedu SPV Jeesudaasu chitra Varaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nedu SPV Jeesudaasu chitra Varaku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ista Paddaru Mama Kashta Paddaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ista Paddaru Mama Kashta Paddaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ista Paddaru Mama Kashta Paddaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ista Paddaru Mama Kashta Paddaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Telugu cinema Lokanike chupincharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Telugu cinema Lokanike chupincharu"/>
</div>
<div class="lyrico-lyrics-wrapper">Telugu cinema Lokanike chupincharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Telugu cinema Lokanike chupincharu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Baaburao Badokar daggaranunchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baaburao Badokar daggaranunchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nedu Botagiri Marthand K Venkat Varaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nedu Botagiri Marthand K Venkat Varaku"/>
</div>
<div class="lyrico-lyrics-wrapper">Srinivasa Kulakarni Daggaranunchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Srinivasa Kulakarni Daggaranunchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nedu Prabhudeva Larence Prem Rakshith Varaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nedu Prabhudeva Larence Prem Rakshith Varaku"/>
</div>
<div class="lyrico-lyrics-wrapper">HM Reddy Direction Modalantu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="HM Reddy Direction Modalantu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nedu Rajamouli Sukumar Krish Varaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nedu Rajamouli Sukumar Krish Varaku"/>
</div>
<div class="lyrico-lyrics-wrapper">Nirmanam R desi Iran Nunchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nirmanam R desi Iran Nunchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nedu Dil Raju Suresh Allu Varaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nedu Dil Raju Suresh Allu Varaku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ista Paddaru Mama Kashta Paddaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ista Paddaru Mama Kashta Paddaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ista Paddaru Mama Kashta Paddaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ista Paddaru Mama Kashta Paddaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Telugu cinema Lokanike chupincharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Telugu cinema Lokanike chupincharu"/>
</div>
<div class="lyrico-lyrics-wrapper">Telugu cinema Lokanike chupincharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Telugu cinema Lokanike chupincharu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Twinty eight craft cinemane nilabedatharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Twinty eight craft cinemane nilabedatharu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ista Paddaru Vacchi chusaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ista Paddaru Vacchi chusaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Telugu Prekshakule Cinemane Gelipincharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Telugu Prekshakule Cinemane Gelipincharu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maa Telugu Cinemane Lokaniki Chupincharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa Telugu Cinemane Lokaniki Chupincharu"/>
</div>
</pre>
