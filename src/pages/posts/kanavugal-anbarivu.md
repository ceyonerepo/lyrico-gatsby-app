---
title: "kanavugal song lyrics"
album: "Anbarivu"
artist: "Hiphop Tamizha"
lyricist: "Yaazhi Dragon"
director: "Aswin Raam"
path: "/albums/anbarivu-song-lyrics"
song: "Kanavugal"
image: ../../images/albumart/anbarivu.jpg
date: 2022-01-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/cBu4kg9mB3k"
type: "happy"
singers:
  - Benny Dayal
  - Bamba Bakya
  - Sam Vishal
  - Srinisha Jayaseelan
  - Sridhar Sena
  - Maanasi K
  - Shilvi Sharon
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Get up kaalaiyila kanna muzhikkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Get up kaalaiyila kanna muzhikkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Buddy ready ezhundhiri 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Buddy ready ezhundhiri "/>
</div>
<div class="lyrico-lyrics-wrapper">snooze la phone eh podama nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="snooze la phone eh podama nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Ovvuru naalum oru gift u thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ovvuru naalum oru gift u thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhu pudikumo atha mattum pannu machan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhu pudikumo atha mattum pannu machan"/>
</div>
<div class="lyrico-lyrics-wrapper">Summa jaikanum jollya kaala vecha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summa jaikanum jollya kaala vecha"/>
</div>
<div class="lyrico-lyrics-wrapper">Yellam saathiyam dhan dhinam nee ozhaicha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yellam saathiyam dhan dhinam nee ozhaicha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pagutharivu adhu miga miga nandru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagutharivu adhu miga miga nandru"/>
</div>
<div class="lyrico-lyrics-wrapper">Andrellam angey vidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andrellam angey vidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini only indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini only indru"/>
</div>
<div class="lyrico-lyrics-wrapper">Inaiku setha naalaiku paal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inaiku setha naalaiku paal"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalaiku naalaiku paar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalaiku naalaiku paar"/>
</div>
<div class="lyrico-lyrics-wrapper">Dropping bars fancy cars
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dropping bars fancy cars"/>
</div>
<div class="lyrico-lyrics-wrapper">Dripping swags super flex
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dripping swags super flex"/>
</div>
<div class="lyrico-lyrics-wrapper">Romba casual inaiku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba casual inaiku "/>
</div>
<div class="lyrico-lyrics-wrapper">setha naalaiku paal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="setha naalaiku paal"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalaiku naalaiku paar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalaiku naalaiku paar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yenna paada readya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenna paada readya"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhavugal thirapom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhavugal thirapom"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavugal parippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavugal parippom"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru murai thaan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru murai thaan "/>
</div>
<div class="lyrico-lyrics-wrapper">vaazhkai vaazhnthu paarpom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaazhkai vaazhnthu paarpom"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadal mazhai kadapom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadal mazhai kadapom"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadai adhai udaipom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadai adhai udaipom"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru murai than vaazhkai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru murai than vaazhkai "/>
</div>
<div class="lyrico-lyrics-wrapper">uyara parapom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyara parapom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Super herona yenga appa mattum dhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Super herona yenga appa mattum dhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thallinillu marvel dc yellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thallinillu marvel dc yellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Yenna suthi yen gang 24 7
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenna suthi yen gang 24 7"/>
</div>
<div class="lyrico-lyrics-wrapper">Yenakume thanimaiya unarala naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenakume thanimaiya unarala naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Venaa yedhavum venna polaam vaa polaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venaa yedhavum venna polaam vaa polaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithuvareyum athuvareykum senthu polam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithuvareyum athuvareykum senthu polam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Get up kalaiyila kanna muzhikanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Get up kalaiyila kanna muzhikanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Buddy ready ezhundhiri 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Buddy ready ezhundhiri "/>
</div>
<div class="lyrico-lyrics-wrapper">snooze le phone eh podama nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="snooze le phone eh podama nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Ovvuru naalum oru gift u thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ovvuru naalum oru gift u thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhu pudikumo atha mattum pannu machan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhu pudikumo atha mattum pannu machan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhavugal thirapom kanavugal parippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhavugal thirapom kanavugal parippom"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru murai than vaazhkai vaazhnthu paarpom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru murai than vaazhkai vaazhnthu paarpom"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadal mazhai kadapom thadai adhai udaipom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadal mazhai kadapom thadai adhai udaipom"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru murai than vaazhkai uyara parapom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru murai than vaazhkai uyara parapom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nyaayire ennai vittu pogathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nyaayire ennai vittu pogathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhuven unnai kaanum varaiyil naane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhuven unnai kaanum varaiyil naane"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadanthathu kadakatum kadanthidum varai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadanthathu kadakatum kadanthidum varai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadandhidu nanba nanadandhidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadandhidu nanba nanadandhidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aruginal irupathai arugainal aravanaithu po
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aruginal irupathai arugainal aravanaithu po"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbal uzhagai vendridu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbal uzhagai vendridu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Get up kalaiyila kanna muzhikanum Buddy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Get up kalaiyila kanna muzhikanum Buddy"/>
</div>
<div class="lyrico-lyrics-wrapper">Ready ezhundhiri snooze le phone eh podama nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ready ezhundhiri snooze le phone eh podama nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Ovvuru naalum oru gift u thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ovvuru naalum oru gift u thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yellam koode sethu paada ready ya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yellam koode sethu paada ready ya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhavugal thirapom kanavugal parippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhavugal thirapom kanavugal parippom"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru murai than vaazhkai vaazhnthu paarpom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru murai than vaazhkai vaazhnthu paarpom"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadal mazhai kadapom thadai adhai udaipom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadal mazhai kadapom thadai adhai udaipom"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru murai than vaazhkai uyara parapom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru murai than vaazhkai uyara parapom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhavugal thirapom kanavugal parippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhavugal thirapom kanavugal parippom"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru murai than vaazhkai vaazhnthu paarpom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru murai than vaazhkai vaazhnthu paarpom"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadal mazhai kadapom thadai adhai udaipom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadal mazhai kadapom thadai adhai udaipom"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru murai than vaazhkai uyara parapom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru murai than vaazhkai uyara parapom"/>
</div>
</pre>
