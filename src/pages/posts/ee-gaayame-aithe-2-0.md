---
title: "ee gaayame song lyrics"
album: "Aithe 2.0"
artist: "Arun Chiluveru"
lyricist: "Kittu Vissapragada"
director: "Raj Madiraju"
path: "/albums/aithe-2-0-lyrics"
song: "Ee Gaayame"
image: ../../images/albumart/aithe-2-0.jpg
date: 2018-03-16
lang: telugu
youtubeLink: "https://www.youtube.com/embed/c0A901Nxlhc"
type: "love"
singers:
  -	Ruturaj Mohanthy
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">ee gaayame 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ee gaayame "/>
</div>
<div class="lyrico-lyrics-wrapper">mee gamyamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mee gamyamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">ee vanchane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ee vanchane"/>
</div>
<div class="lyrico-lyrics-wrapper">migile teeramaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="migile teeramaa"/>
</div>
<div class="lyrico-lyrics-wrapper">kshanam kshanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kshanam kshanam"/>
</div>
<div class="lyrico-lyrics-wrapper">penchina aashale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="penchina aashale"/>
</div>
<div class="lyrico-lyrics-wrapper">kadam kadam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadam kadam"/>
</div>
<div class="lyrico-lyrics-wrapper">kalipina oohale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalipina oohale"/>
</div>
<div class="lyrico-lyrics-wrapper">kanam kanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanam kanam"/>
</div>
<div class="lyrico-lyrics-wrapper">nippula varshamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nippula varshamai"/>
</div>
<div class="lyrico-lyrics-wrapper">manoranam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manoranam"/>
</div>
<div class="lyrico-lyrics-wrapper">maarche gaayamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maarche gaayamai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kalaa kalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalaa kalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">nisheedhi lona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nisheedhi lona"/>
</div>
<div class="lyrico-lyrics-wrapper">alaa alaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alaa alaa"/>
</div>
<div class="lyrico-lyrics-wrapper">samaadhi ayina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="samaadhi ayina"/>
</div>
<div class="lyrico-lyrics-wrapper">kshanaalalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kshanaalalo"/>
</div>
<div class="lyrico-lyrics-wrapper">kale chejaaripoye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kale chejaaripoye"/>
</div>
<div class="lyrico-lyrics-wrapper">niraashale 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="niraashale "/>
</div>
<div class="lyrico-lyrics-wrapper">nishwaasamaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nishwaasamaina"/>
</div>
<div class="lyrico-lyrics-wrapper">vinodame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vinodame"/>
</div>
<div class="lyrico-lyrics-wrapper">vishadhamaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vishadhamaina"/>
</div>
<div class="lyrico-lyrics-wrapper">parabhavam ila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="parabhavam ila"/>
</div>
<div class="lyrico-lyrics-wrapper">aakroshamayye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aakroshamayye"/>
</div>
<div class="lyrico-lyrics-wrapper">adugadugoo alochinchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adugadugoo alochinchi"/>
</div>
<div class="lyrico-lyrics-wrapper">veyaalantoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veyaalantoo"/>
</div>
<div class="lyrico-lyrics-wrapper">odidudukulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="odidudukulu"/>
</div>
<div class="lyrico-lyrics-wrapper">chese gaayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chese gaayam"/>
</div>
<div class="lyrico-lyrics-wrapper">nerpe patham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nerpe patham"/>
</div>
<div class="lyrico-lyrics-wrapper">ato ito eto
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ato ito eto"/>
</div>
<div class="lyrico-lyrics-wrapper">prayaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="prayaanam"/>
</div>
<div class="lyrico-lyrics-wrapper">anukshanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anukshanam"/>
</div>
<div class="lyrico-lyrics-wrapper">edo pramaadam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edo pramaadam"/>
</div>
<div class="lyrico-lyrics-wrapper">dahinchinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dahinchinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">sahinchadaa pranam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sahinchadaa pranam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kshanam kshanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kshanam kshanam"/>
</div>
<div class="lyrico-lyrics-wrapper">penchina aashale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="penchina aashale"/>
</div>
<div class="lyrico-lyrics-wrapper">kadam kadam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadam kadam"/>
</div>
<div class="lyrico-lyrics-wrapper">kalipina oohale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalipina oohale"/>
</div>
<div class="lyrico-lyrics-wrapper">kanam kanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanam kanam"/>
</div>
<div class="lyrico-lyrics-wrapper">nippula varshamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nippula varshamai"/>
</div>
<div class="lyrico-lyrics-wrapper">manoranam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manoranam"/>
</div>
<div class="lyrico-lyrics-wrapper">maarche gaayamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maarche gaayamai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">gathinchina pratee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gathinchina pratee"/>
</div>
<div class="lyrico-lyrics-wrapper">kshanaanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kshanaanni"/>
</div>
<div class="lyrico-lyrics-wrapper">guninchina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="guninchina"/>
</div>
<div class="lyrico-lyrics-wrapper">nirveda bhavam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nirveda bhavam"/>
</div>
<div class="lyrico-lyrics-wrapper">gadinchina patham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gadinchina patham"/>
</div>
<div class="lyrico-lyrics-wrapper">nirliptame autunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nirliptame autunte"/>
</div>
<div class="lyrico-lyrics-wrapper">shapinchina 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="shapinchina "/>
</div>
<div class="lyrico-lyrics-wrapper">mano gataanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mano gataanni"/>
</div>
<div class="lyrico-lyrics-wrapper">chalinchani 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chalinchani "/>
</div>
<div class="lyrico-lyrics-wrapper">ee jeevitaanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ee jeevitaanni"/>
</div>
<div class="lyrico-lyrics-wrapper">jayinchina gunam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jayinchina gunam"/>
</div>
<div class="lyrico-lyrics-wrapper">kanipinchani teerunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanipinchani teerunte"/>
</div>
<div class="lyrico-lyrics-wrapper">kalakalame choose
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalakalame choose"/>
</div>
<div class="lyrico-lyrics-wrapper">nayanam emantundo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nayanam emantundo"/>
</div>
<div class="lyrico-lyrics-wrapper">bhayapadutoo jarige
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bhayapadutoo jarige"/>
</div>
<div class="lyrico-lyrics-wrapper">payanam etu potundo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="payanam etu potundo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ato ito eto
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ato ito eto"/>
</div>
<div class="lyrico-lyrics-wrapper">prayaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="prayaanam"/>
</div>
<div class="lyrico-lyrics-wrapper">anukshanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anukshanam"/>
</div>
<div class="lyrico-lyrics-wrapper">edo pramaadam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edo pramaadam"/>
</div>
<div class="lyrico-lyrics-wrapper">dahinchinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dahinchinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">sahinchadaa pranam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sahinchadaa pranam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kshanam kshanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kshanam kshanam"/>
</div>
<div class="lyrico-lyrics-wrapper">penchina aashale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="penchina aashale"/>
</div>
<div class="lyrico-lyrics-wrapper">kadam kadam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadam kadam"/>
</div>
<div class="lyrico-lyrics-wrapper">kalipina oohale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalipina oohale"/>
</div>
<div class="lyrico-lyrics-wrapper">kanam kanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanam kanam"/>
</div>
<div class="lyrico-lyrics-wrapper">nippula varshamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nippula varshamai"/>
</div>
<div class="lyrico-lyrics-wrapper">manoranam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manoranam"/>
</div>
<div class="lyrico-lyrics-wrapper">maarche gaayamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maarche gaayamai"/>
</div>
</pre>
