---
title: "jallikattu song lyrics"
album: "Saayam"
artist: "Nagha Udhayan"
lyricist: "Pon Seeman"
director: "Antony Samy"
path: "/albums/saayam-lyrics"
song: "Jallikattu"
image: ../../images/albumart/saayam.jpg
date: 2022-02-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/TIhSs9QDtTY"
type: "happy"
singers:
  - Anthakudi Ilayaraja
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">eeramannu veeramannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eeramannu veeramannu"/>
</div>
<div class="lyrico-lyrics-wrapper">kondadum naalaiennu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kondadum naalaiennu"/>
</div>
<div class="lyrico-lyrics-wrapper">yei ettupatti padi alakum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yei ettupatti padi alakum"/>
</div>
<div class="lyrico-lyrics-wrapper">engaloda thaai mannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engaloda thaai mannu"/>
</div>
<div class="lyrico-lyrics-wrapper">yei thennatu seemaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yei thennatu seemaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">ada therodum veethiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada therodum veethiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">kaapu kati kathu irukum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaapu kati kathu irukum"/>
</div>
<div class="lyrico-lyrics-wrapper">enga kulam kakkum iyanare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga kulam kakkum iyanare"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">jalli kattu kaala pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jalli kattu kaala pola"/>
</div>
<div class="lyrico-lyrics-wrapper">thulli varuthu saamy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thulli varuthu saamy"/>
</div>
<div class="lyrico-lyrics-wrapper">ooru sanam oodi vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooru sanam oodi vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">soodam ethi kaami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="soodam ethi kaami"/>
</div>
<div class="lyrico-lyrics-wrapper">devakota veethiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="devakota veethiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">thiruvilavum joru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thiruvilavum joru"/>
</div>
<div class="lyrico-lyrics-wrapper">vaanavedi potu vedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaanavedi potu vedi"/>
</div>
<div class="lyrico-lyrics-wrapper">thool parakum ooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thool parakum ooru"/>
</div>
<div class="lyrico-lyrics-wrapper">pattampoochi pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pattampoochi pola"/>
</div>
<div class="lyrico-lyrics-wrapper">parakuthu enga manasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="parakuthu enga manasu"/>
</div>
<div class="lyrico-lyrics-wrapper">kuchi ice a pola ada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuchi ice a pola ada"/>
</div>
<div class="lyrico-lyrics-wrapper">karaiyuthunga vayasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karaiyuthunga vayasu"/>
</div>
<div class="lyrico-lyrics-wrapper">pattampoochi pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pattampoochi pola"/>
</div>
<div class="lyrico-lyrics-wrapper">parakuthu enga manasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="parakuthu enga manasu"/>
</div>
<div class="lyrico-lyrics-wrapper">kuchi ice a pola ada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuchi ice a pola ada"/>
</div>
<div class="lyrico-lyrics-wrapper">karaiyuthunga vayasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karaiyuthunga vayasu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">jalli kattu kaala pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jalli kattu kaala pola"/>
</div>
<div class="lyrico-lyrics-wrapper">thulli varuthu saamy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thulli varuthu saamy"/>
</div>
<div class="lyrico-lyrics-wrapper">ooru sanam oodi vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooru sanam oodi vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">soodam ethi kaami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="soodam ethi kaami"/>
</div>
<div class="lyrico-lyrics-wrapper">devakota veethiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="devakota veethiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">thiruvilavum joru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thiruvilavum joru"/>
</div>
<div class="lyrico-lyrics-wrapper">vaanavedi potu vedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaanavedi potu vedi"/>
</div>
<div class="lyrico-lyrics-wrapper">thool parakum ooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thool parakum ooru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thoranamum maavilaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoranamum maavilaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">seerialyum pulb um thane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seerialyum pulb um thane"/>
</div>
<div class="lyrico-lyrics-wrapper">kanna parikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanna parikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">vaanathula megam vara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaanathula megam vara"/>
</div>
<div class="lyrico-lyrics-wrapper">vaana vedi potu intha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaana vedi potu intha"/>
</div>
<div class="lyrico-lyrics-wrapper">ooru sirikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooru sirikum"/>
</div>
<div class="lyrico-lyrics-wrapper">yei kuri paathu uri adipom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yei kuri paathu uri adipom"/>
</div>
<div class="lyrico-lyrics-wrapper">ethiri vanthale uri adipom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethiri vanthale uri adipom"/>
</div>
<div class="lyrico-lyrics-wrapper">ponnungala pathu rasipom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponnungala pathu rasipom"/>
</div>
<div class="lyrico-lyrics-wrapper">mela manja thanee oothi adipom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mela manja thanee oothi adipom"/>
</div>
<div class="lyrico-lyrics-wrapper">yei patti thotti engum ada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yei patti thotti engum ada"/>
</div>
<div class="lyrico-lyrics-wrapper">pattaya kelappum paatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pattaya kelappum paatu"/>
</div>
<div class="lyrico-lyrics-wrapper">ettu thisa kekum ithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ettu thisa kekum ithu"/>
</div>
<div class="lyrico-lyrics-wrapper">enga ooru vetu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga ooru vetu"/>
</div>
<div class="lyrico-lyrics-wrapper">yei patti thotti engum ada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yei patti thotti engum ada"/>
</div>
<div class="lyrico-lyrics-wrapper">pattaya kelappum paatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pattaya kelappum paatu"/>
</div>
<div class="lyrico-lyrics-wrapper">ettu thisa kekum ithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ettu thisa kekum ithu"/>
</div>
<div class="lyrico-lyrics-wrapper">enga ooru vetu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga ooru vetu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">koodi varum kootathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koodi varum kootathula"/>
</div>
<div class="lyrico-lyrics-wrapper">yarum vanthu thappu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yarum vanthu thappu "/>
</div>
<div class="lyrico-lyrics-wrapper">senja kaiya odappom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="senja kaiya odappom"/>
</div>
<div class="lyrico-lyrics-wrapper">thedi varum oorukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thedi varum oorukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">devathanga ponanganna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="devathanga ponanganna"/>
</div>
<div class="lyrico-lyrics-wrapper">poova parippom 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poova parippom "/>
</div>
<div class="lyrico-lyrics-wrapper">naanga onnaga vattam adipom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naanga onnaga vattam adipom"/>
</div>
<div class="lyrico-lyrics-wrapper">ethu vanthalum senju mudipom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethu vanthalum senju mudipom"/>
</div>
<div class="lyrico-lyrics-wrapper">ada kuthatam potu rasipom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada kuthatam potu rasipom"/>
</div>
<div class="lyrico-lyrics-wrapper">kutti puliyaatam thulli kuthipom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kutti puliyaatam thulli kuthipom"/>
</div>
<div class="lyrico-lyrics-wrapper">aatam iyya ithu pataya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aatam iyya ithu pataya"/>
</div>
<div class="lyrico-lyrics-wrapper">kelappum aatam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kelappum aatam"/>
</div>
<div class="lyrico-lyrics-wrapper">kootam iyya kootam ithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kootam iyya kootam ithu"/>
</div>
<div class="lyrico-lyrics-wrapper">koothu katura kootam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koothu katura kootam"/>
</div>
<div class="lyrico-lyrics-wrapper">aatam iyya ithu pataya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aatam iyya ithu pataya"/>
</div>
<div class="lyrico-lyrics-wrapper">kelappum aatam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kelappum aatam"/>
</div>
<div class="lyrico-lyrics-wrapper">kootam iyya kootam ithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kootam iyya kootam ithu"/>
</div>
<div class="lyrico-lyrics-wrapper">koothu katura kootam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koothu katura kootam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">jalli kattu kaala pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jalli kattu kaala pola"/>
</div>
<div class="lyrico-lyrics-wrapper">thulli varuthu saamy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thulli varuthu saamy"/>
</div>
<div class="lyrico-lyrics-wrapper">ooru sanam oodi vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooru sanam oodi vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">soodam ethi kaami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="soodam ethi kaami"/>
</div>
<div class="lyrico-lyrics-wrapper">thanana thane nane nane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanana thane nane nane"/>
</div>
<div class="lyrico-lyrics-wrapper">thanana thane nane nane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanana thane nane nane"/>
</div>
<div class="lyrico-lyrics-wrapper">thanana thane nane nane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanana thane nane nane"/>
</div>
</pre>
