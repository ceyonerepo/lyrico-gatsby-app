---
title: "theera kadha song lyrics"
album: "Gauthamante Radham"
artist: "Ankit Menon - Anuraj O. B"
lyricist: "Vinayak Sasikumar"
director: "Anand Menon"
path: "/albums/gauthamante-radham-lyrics"
song: "Theera Kadha"
image: ../../images/albumart/gauthamante-radham.jpg
date: 2020-01-31
lang: malayalam
youtubeLink: "https://www.youtube.com/embed/fJHpI_vF4kY"
type: "happy"
singers:
  - Ankit Menon
  - Preetha Madhu Menon
  - Ashwin Aryan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vinveyile Vinmazhaye 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinveyile Vinmazhaye "/>
</div>
<div class="lyrico-lyrics-wrapper">Manvazhiye Manvazhiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manvazhiye Manvazhiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Innithile Varunnu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innithile Varunnu "/>
</div>
<div class="lyrico-lyrics-wrapper">Naam Varunnithaa Naam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam Varunnithaa Naam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kinaavo Anekam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kinaavo Anekam "/>
</div>
<div class="lyrico-lyrics-wrapper">Vidhooram Vimookam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidhooram Vimookam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ananthamaam Ee 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ananthamaam Ee "/>
</div>
<div class="lyrico-lyrics-wrapper">Paatha Pole
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paatha Pole"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanmunayil Poothu Thaarakam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanmunayil Poothu Thaarakam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithal Pol Nerthe Maanasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithal Pol Nerthe Maanasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Naam Thedunnu Ee 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam Thedunnu Ee "/>
</div>
<div class="lyrico-lyrics-wrapper">Yaathra Than Theeraakkadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaathra Than Theeraakkadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeraakkadha Aa Aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeraakkadha Aa Aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeraakkadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeraakkadhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nirangal Swarangal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nirangal Swarangal "/>
</div>
<div class="lyrico-lyrics-wrapper">Vanangal Dalangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanangal Dalangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadannu Poy Kadannu Poy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadannu Poy Kadannu Poy"/>
</div>
<div class="lyrico-lyrics-wrapper">Athokke Pathukke Innaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athokke Pathukke Innaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Novo Marannu Poy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Novo Marannu Poy"/>
</div>
<div class="lyrico-lyrics-wrapper">Marannu Poy Naam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marannu Poy Naam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Nilavilere Ormmakal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Nilavilere Ormmakal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kedaathe Bhaakki Aakave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kedaathe Bhaakki Aakave"/>
</div>
<div class="lyrico-lyrics-wrapper">Naam Thedunnu Ee Yaathra Than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam Thedunnu Ee Yaathra Than"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeraakkadha Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeraakkadha Oh"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeraakkadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeraakkadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeraakkadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeraakkadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Aaa"/>
</div>
</pre>
