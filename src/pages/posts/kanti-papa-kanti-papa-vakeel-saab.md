---
title: "kanti papa song lyrics"
album: "Vakeel Saab"
artist: "S. Thaman"
lyricist: "Ramajogayya Sastry"
director: "Venu Sriram"
path: "/albums/vakeel-saab-lyrics"
song: "Kanti Papa Kanti Papa"
image: ../../images/albumart/vakeel-saab.jpg
date: 2021-04-09
lang: telugu
youtubeLink: "https://www.youtube.com/embed/_GqYza1yQ9c"
type: "love"
singers:
  - Armaan Malik
  - Deepu
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kanti papa kanti papa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanti papa kanti papa"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheppanaina ledhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheppanaina ledhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvanthala aalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvanthala aalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Enni kalalu kanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enni kalalu kanna"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaali muvva kaali muvva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaali muvva kaali muvva"/>
</div>
<div class="lyrico-lyrics-wrapper">Savvadaina ledhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Savvadaina ledhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvenninalluga venta thiruguthunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvenninalluga venta thiruguthunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee raaka yeruvaaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee raaka yeruvaaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee chupe prema lekha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee chupe prema lekha"/>
</div>
<div class="lyrico-lyrics-wrapper">Neelo nuvvagipoka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelo nuvvagipoka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalisave kaanthi rekha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalisave kaanthi rekha"/>
</div>
<div class="lyrico-lyrics-wrapper">Anthuleni prema nuvvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anthuleni prema nuvvai"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha dhuram vachhinaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha dhuram vachhinaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Andhamaina baramantha naaku panchinaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhamaina baramantha naaku panchinaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Modhalega kottha kottha kathalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Modhalega kottha kottha kathalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Modhalega kottha kothha kalalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Modhalega kottha kothha kalalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ikapaina nuvvu nenu badhulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ikapaina nuvvu nenu badhulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Manamanna kottha maata modhalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manamanna kottha maata modhalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanti papa kanti papa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanti papa kanti papa"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheppanaina ledhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheppanaina ledhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvanthala aalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvanthala aalaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saapa maapa maapa maaga saamagarisa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saapa maapa maapa maaga saamagarisa"/>
</div>
<div class="lyrico-lyrics-wrapper">Saapa maapa maapa maaga saamagarisa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saapa maapa maapa maaga saamagarisa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sudhathi sumalochani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sudhathi sumalochani"/>
</div>
<div class="lyrico-lyrics-wrapper">Sumanohara hasini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sumanohara hasini"/>
</div>
<div class="lyrico-lyrics-wrapper">Ramani priya bashini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ramani priya bashini"/>
</div>
<div class="lyrico-lyrics-wrapper">Karuna guna baasini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuna guna baasini"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasaina vaadini manuvaadina aamani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasaina vaadini manuvaadina aamani"/>
</div>
<div class="lyrico-lyrics-wrapper">badhuliyave cheli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="badhuliyave cheli"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvu pondhina premani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvu pondhina premani"/>
</div>
<div class="lyrico-lyrics-wrapper">Pandanti prananni kanave kaanukaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pandanti prananni kanave kaanukaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Saapa maapa maapa maaga saamagarisa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saapa maapa maapa maaga saamagarisa"/>
</div>
<div class="lyrico-lyrics-wrapper">Saapa maapa maapa maaga saamagarisa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saapa maapa maapa maaga saamagarisa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yedhalo yekanthamu emaindho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhalo yekanthamu emaindho"/>
</div>
<div class="lyrico-lyrics-wrapper">Emaindho emito
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emaindho emito"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhigo nee raakatho velipoyindheto
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhigo nee raakatho velipoyindheto"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalo maro nannu chusa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalo maro nannu chusa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeko snehithudni chesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeko snehithudni chesa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam kaagithalatho janta perluga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam kaagithalatho janta perluga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu nannu raasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu nannu raasa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aakasam godugu needa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aakasam godugu needa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudamega poola meda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudamega poola meda"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye chupulu vaalakunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye chupulu vaalakunda"/>
</div>
<div class="lyrico-lyrics-wrapper">Preme mana kota goda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Preme mana kota goda"/>
</div>
<div class="lyrico-lyrics-wrapper">Naaku nuvvai neeku nenai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naaku nuvvai neeku nenai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye kshananni vadhalakunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye kshananni vadhalakunda"/>
</div>
<div class="lyrico-lyrics-wrapper">Guruthulenno penchukundham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Guruthulenno penchukundham"/>
</div>
<div class="lyrico-lyrics-wrapper">Gunde chotu ninda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gunde chotu ninda"/>
</div>
<div class="lyrico-lyrics-wrapper">Modhalega kottha kottha kathalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Modhalega kottha kottha kathalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Modhalega kottha kothha kalalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Modhalega kottha kothha kalalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ikapaina nuvvu nenu badhulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ikapaina nuvvu nenu badhulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Manamanna kottha maata modhalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manamanna kottha maata modhalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Modhalega kottha kottha kathalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Modhalega kottha kottha kathalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Modhalega kottha kothha kalalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Modhalega kottha kothha kalalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ikapaina nuvvu nenu badhulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ikapaina nuvvu nenu badhulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Manamanna kottha maata modhalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manamanna kottha maata modhalu"/>
</div>
</pre>
