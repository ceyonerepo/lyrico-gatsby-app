---
title: "pinjula pinjula song lyrics"
album: "Simba"
artist: "Vishal Chandrasekhar"
lyricist: "Viveka"
director: "Arvind Sridhar"
path: "/albums/simba-lyrics"
song: "Pinjula Pinjula"
image: ../../images/albumart/simba.jpg
date: 2019-01-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/8y_DyMy2_h0"
type: "sad"
singers:
  - Silambarasan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Pinjula Pinjula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pinjula Pinjula"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam Vedhumbi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam Vedhumbi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thavikuthada Life Fu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thavikuthada Life Fu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjula Ulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjula Ulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Manjaa Soru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manjaa Soru"/>
</div>
<div class="lyrico-lyrics-wrapper">Edukkuthada Love Vu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edukkuthada Love Vu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pinjula Pinjula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pinjula Pinjula"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam Vedhumbi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam Vedhumbi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thavikuthada Life Fu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thavikuthada Life Fu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjula Ulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjula Ulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Manjaa Soru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manjaa Soru"/>
</div>
<div class="lyrico-lyrics-wrapper">Edukkuthada Love Vu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edukkuthada Love Vu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Daavadicha Ponnu Ippo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daavadicha Ponnu Ippo"/>
</div>
<div class="lyrico-lyrics-wrapper">Dive Vu Adichitaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dive Vu Adichitaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhale Venaamunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhale Venaamunu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiyya Virichitaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyya Virichitaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaaya Katta Pol Irundhaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaya Katta Pol Irundhaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Ondi Katta Aakkiputaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondi Katta Aakkiputaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Ondi Katta Aakkiputaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondi Katta Aakkiputaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sondham Bandham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sondham Bandham"/>
</div>
<div class="lyrico-lyrics-wrapper">Odhungi Pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odhungi Pochu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanna Muzhi Pidhungi Pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna Muzhi Pidhungi Pochu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadhavaraa Anaichikathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadhavaraa Anaichikathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarum Illa Maamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarum Illa Maamu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nyaabaga Azhukai Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nyaabaga Azhukai Ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuvachida Manasukkoru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuvachida Manasukkoru"/>
</div>
<div class="lyrico-lyrics-wrapper">Soapa Yaarum Idhuvaraikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soapa Yaarum Idhuvaraikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kandupudikkala Maama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandupudikkala Maama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhamaa Thundu Nenju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhamaa Thundu Nenju"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu Nenje Illa Panju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu Nenje Illa Panju"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeya Vachi Poyitaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeya Vachi Poyitaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirumu Pudicha Vanji
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirumu Pudicha Vanji"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanimayil Manasukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanimayil Manasukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Karaiyiraan Ivane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karaiyiraan Ivane"/>
</div>
<div class="lyrico-lyrics-wrapper">Ondi Katta Aakkiputaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondi Katta Aakkiputaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Ondi Katta Aakkiputaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondi Katta Aakkiputaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andhuruchu Dangu Vaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhuruchu Dangu Vaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava Manam Thaarumaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Manam Thaarumaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandhu Pondhu Indhu Idukkil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhu Pondhu Indhu Idukkil"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandhosame Ledhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhosame Ledhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rendu Kattaan Vayasulathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendu Kattaan Vayasulathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thundu Thundaa Aakkiputta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thundu Thundaa Aakkiputta"/>
</div>
<div class="lyrico-lyrics-wrapper">Gundu Katta Thookki Pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundu Katta Thookki Pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kumuruthaiyaa Soodhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kumuruthaiyaa Soodhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kattam Katti Vachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattam Katti Vachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivana Velukkathaiyaa Machi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivana Velukkathaiyaa Machi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam Pannum Kolam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam Pannum Kolam"/>
</div>
<div class="lyrico-lyrics-wrapper">Romba Kodumaiyada Machi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba Kodumaiyada Machi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivanukku Kadavulum Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivanukku Kadavulum Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhuvum Tharalaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhuvum Tharalaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannil Innum Kanneer
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannil Innum Kanneer"/>
</div>
<div class="lyrico-lyrics-wrapper">Varalaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varalaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Varalaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varalaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Varalaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varalaye"/>
</div>
</pre>
