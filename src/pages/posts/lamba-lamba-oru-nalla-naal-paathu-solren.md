---
title: "lamba lamba song lyrics"
album: "Oru Nalla Naal Paathu Solren"
artist: "Justin Prabhakaran"
lyricist: "Karthik Netha"
director: "Arumuga Kumar"
path: "/albums/oru-nalla-naal-paathu-solren-lyrics"
song: "Lamba Lamba"
image: ../../images/albumart/oru-nalla-naal-paathu-solren.jpg
date: 2018-02-02
lang: tamil
youtubeLink: "https://www.youtube.com/embed/um0ndnAyc8c"
type: "happy"
singers:
  - Christopher Stanley
  - Femcee Nicki Ziee G
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Lamba lamba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lamba lamba"/>
</div>
<div class="lyrico-lyrics-wrapper">Lamba lamba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lamba lamba"/>
</div>
<div class="lyrico-lyrics-wrapper">Lamba lamba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lamba lamba"/>
</div>
<div class="lyrico-lyrics-wrapper">Lamba lamba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lamba lamba"/>
</div>
<div class="lyrico-lyrics-wrapper">Lam lam lam lam lamba lamba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lam lam lam lam lamba lamba"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lamba lamba lambadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lamba lamba lambadi"/>
</div>
<div class="lyrico-lyrics-wrapper">We are naatu gym body
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="We are naatu gym body"/>
</div>
<div class="lyrico-lyrics-wrapper">Bothai yerum munnadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bothai yerum munnadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pottukkoda kannadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pottukkoda kannadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lamba lamba lambadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lamba lamba lambadi"/>
</div>
<div class="lyrico-lyrics-wrapper">We are naatu gym body
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="We are naatu gym body"/>
</div>
<div class="lyrico-lyrics-wrapper">Koopudranyan munnadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koopudranyan munnadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Inthaa adi pinnadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inthaa adi pinnadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rasht rasht rasht rasht
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rasht rasht rasht rasht"/>
</div>
<div class="lyrico-lyrics-wrapper">Rashtrapathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rashtrapathi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vazhkai oru gongango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhkai oru gongango"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazha enna ep ko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazha enna ep ko"/>
</div>
<div class="lyrico-lyrics-wrapper">Maala thantha maattiko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maala thantha maattiko"/>
</div>
<div class="lyrico-lyrics-wrapper">Illa neeye pottukkoppa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illa neeye pottukkoppa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pa pa pa pa pa pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pa pa pa pa pa pa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pa pa pa pa pa pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pa pa pa pa pa pa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Champestha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Champestha"/>
</div>
<div class="lyrico-lyrics-wrapper">Jakrathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jakrathaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nallavangalukku nallathuthaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nallavangalukku nallathuthaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadakkum belive me da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadakkum belive me da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pai mara kappalu pannuvom nakkalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pai mara kappalu pannuvom nakkalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthichu vikkalu vikkalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthichu vikkalu vikkalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey tea kadai benchula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey tea kadai benchula"/>
</div>
<div class="lyrico-lyrics-wrapper">Bunnukku kenjala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bunnukku kenjala"/>
</div>
<div class="lyrico-lyrics-wrapper">Evanukkum anjala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evanukkum anjala"/>
</div>
<div class="lyrico-lyrics-wrapper">Poda poda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poda poda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ek gavumae ek kissan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ek gavumae ek kissan"/>
</div>
<div class="lyrico-lyrics-wrapper">Raghu thathatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raghu thathatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaman kitta lifta kettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaman kitta lifta kettu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paropom getha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paropom getha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye atlas mapula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye atlas mapula"/>
</div>
<div class="lyrico-lyrics-wrapper">Cycle gapula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cycle gapula"/>
</div>
<div class="lyrico-lyrics-wrapper">Irupomda topula topla thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irupomda topula topla thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye pogura pokkula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye pogura pokkula"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakkuvom jokeula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakkuvom jokeula"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanguvom heartula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanguvom heartula"/>
</div>
<div class="lyrico-lyrics-wrapper">Heartula thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heartula thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye people selvanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye people selvanae"/>
</div>
<div class="lyrico-lyrics-wrapper">You are simple aanthaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You are simple aanthaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee of the people
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee of the people"/>
</div>
<div class="lyrico-lyrics-wrapper">By the people
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="By the people"/>
</div>
<div class="lyrico-lyrics-wrapper">For the people
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="For the people"/>
</div>
<div class="lyrico-lyrics-wrapper">Prananathanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prananathanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Na bangaru buchi silaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na bangaru buchi silaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Na bangaru buchi silaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na bangaru buchi silaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Na bangaru buchi silakamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na bangaru buchi silakamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Naathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naathaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lamba lamba lambadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lamba lamba lambadi"/>
</div>
<div class="lyrico-lyrics-wrapper">We are naatu gym body
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="We are naatu gym body"/>
</div>
<div class="lyrico-lyrics-wrapper">Bothai yerum munnadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bothai yerum munnadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pottukkoda kannadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pottukkoda kannadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lamba lamba lambadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lamba lamba lambadi"/>
</div>
<div class="lyrico-lyrics-wrapper">We are naatu gym body
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="We are naatu gym body"/>
</div>
<div class="lyrico-lyrics-wrapper">Bothai yerum munnadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bothai yerum munnadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pottukkoda kannadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pottukkoda kannadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Okay walk the world
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okay walk the world"/>
</div>
<div class="lyrico-lyrics-wrapper">Life ain’t a gamble
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life ain’t a gamble"/>
</div>
<div class="lyrico-lyrics-wrapper">If you can’t handle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="If you can’t handle"/>
</div>
<div class="lyrico-lyrics-wrapper">That ain’t ma trouble
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="That ain’t ma trouble"/>
</div>
<div class="lyrico-lyrics-wrapper">Pop in ya bubble
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pop in ya bubble"/>
</div>
<div class="lyrico-lyrics-wrapper">Welcome to the real world
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Welcome to the real world"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaw Mr Yamen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaw Mr Yamen"/>
</div>
<div class="lyrico-lyrics-wrapper">Tell em what chu cooking
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tell em what chu cooking"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lamba He is yamen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lamba He is yamen"/>
</div>
<div class="lyrico-lyrics-wrapper">Lamba He is yamen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lamba He is yamen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Okay hold up!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okay hold up!"/>
</div>
<div class="lyrico-lyrics-wrapper">So we taking a trip?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="So we taking a trip?"/>
</div>
<div class="lyrico-lyrics-wrapper">His crown so cool
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="His crown so cool"/>
</div>
<div class="lyrico-lyrics-wrapper">And we losing grip!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="And we losing grip!"/>
</div>
<div class="lyrico-lyrics-wrapper">Hail the mighty lord
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hail the mighty lord"/>
</div>
<div class="lyrico-lyrics-wrapper">He made me laugh a lot
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="He made me laugh a lot"/>
</div>
<div class="lyrico-lyrics-wrapper">Damn he so cute!!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Damn he so cute!!"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lamba lamba lambadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lamba lamba lambadi"/>
</div>
<div class="lyrico-lyrics-wrapper">We are naatu gym body
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="We are naatu gym body"/>
</div>
<div class="lyrico-lyrics-wrapper">Bothai yerum munnadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bothai yerum munnadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pottukkoda kannadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pottukkoda kannadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lamba lamba lambadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lamba lamba lambadi"/>
</div>
<div class="lyrico-lyrics-wrapper">We are naatu gym body
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="We are naatu gym body"/>
</div>
<div class="lyrico-lyrics-wrapper">Bothai yerum munnadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bothai yerum munnadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pottukkoda kannadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pottukkoda kannadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lambadi!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lambadi!"/>
</div>
</pre>
