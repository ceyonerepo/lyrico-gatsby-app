---
title: "thayin munney song lyrics"
album: "Khyla"
artist: "Shravan"
lyricist: "Vadivarasu "
director: "Baskar Sinouvassane"
path: "/albums/khyla-lyrics"
song: "Thayin Munney"
image: ../../images/albumart/khyla.jpg
date: 2019-12-20
lang: tamil
youtubeLink: "https://www.youtube.com/embed/FxI1lQyTGdQ"
type: "affection"
singers:
  - Vineeth Madhavan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">thayin munne entha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thayin munne entha"/>
</div>
<div class="lyrico-lyrics-wrapper">theivamum pinne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theivamum pinne"/>
</div>
<div class="lyrico-lyrics-wrapper">aarathanai aarathanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aarathanai aarathanai"/>
</div>
<div class="lyrico-lyrics-wrapper">vaanum kooda chinnan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaanum kooda chinnan"/>
</div>
<div class="lyrico-lyrics-wrapper">siru kudai aagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="siru kudai aagum"/>
</div>
<div class="lyrico-lyrics-wrapper">aarathanai aarathanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aarathanai aarathanai"/>
</div>
<div class="lyrico-lyrics-wrapper">elu suram pathaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="elu suram pathaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">ettai olippal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ettai olippal"/>
</div>
<div class="lyrico-lyrics-wrapper">ettatha uyarathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ettatha uyarathai"/>
</div>
<div class="lyrico-lyrics-wrapper">etti alippal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="etti alippal"/>
</div>
<div class="lyrico-lyrics-wrapper">eduthalum kuraiyatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eduthalum kuraiyatha"/>
</div>
<div class="lyrico-lyrics-wrapper">selvamai irupal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="selvamai irupal"/>
</div>
<div class="lyrico-lyrics-wrapper">koduthalum vaadatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koduthalum vaadatha"/>
</div>
<div class="lyrico-lyrics-wrapper">malarai siripal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malarai siripal"/>
</div>
<div class="lyrico-lyrics-wrapper">aarathanai aarathanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aarathanai aarathanai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">suttalum urugatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suttalum urugatha"/>
</div>
<div class="lyrico-lyrics-wrapper">thangamai jolithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thangamai jolithu"/>
</div>
<div class="lyrico-lyrics-wrapper">allamal kollamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="allamal kollamal"/>
</div>
<div class="lyrico-lyrics-wrapper">alagu tharuval 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alagu tharuval "/>
</div>
<div class="lyrico-lyrics-wrapper">puthaithalum kundratha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthaithalum kundratha"/>
</div>
<div class="lyrico-lyrics-wrapper">vairamai olirthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vairamai olirthu"/>
</div>
<div class="lyrico-lyrics-wrapper">sollamal thullamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sollamal thullamal"/>
</div>
<div class="lyrico-lyrics-wrapper">vaiyam kaapaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaiyam kaapaal"/>
</div>
<div class="lyrico-lyrics-wrapper">iyilum i aval
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iyilum i aval"/>
</div>
<div class="lyrico-lyrics-wrapper">aim poothathilum osanthaval
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aim poothathilum osanthaval"/>
</div>
<div class="lyrico-lyrics-wrapper">uyirum meiyum sernthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyirum meiyum sernthu"/>
</div>
<div class="lyrico-lyrics-wrapper">uyirmei than aval amma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyirmei than aval amma"/>
</div>
<div class="lyrico-lyrics-wrapper">aarathanai aarathanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aarathanai aarathanai"/>
</div>
<div class="lyrico-lyrics-wrapper">aarathanai aarathanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aarathanai aarathanai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aaaaaa aaaaa aaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaaaaa aaaaa aaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">aaaaaa aaaaa aaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaaaaa aaaaa aaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">alithalum aliyatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alithalum aliyatha"/>
</div>
<div class="lyrico-lyrics-wrapper">anbai theeti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anbai theeti"/>
</div>
<div class="lyrico-lyrics-wrapper">vaalvil mudiyatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalvil mudiyatha"/>
</div>
<div class="lyrico-lyrics-wrapper">vannam serpal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vannam serpal"/>
</div>
<div class="lyrico-lyrics-wrapper">kalithalum kaliyatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalithalum kaliyatha"/>
</div>
<div class="lyrico-lyrics-wrapper">panbai ooti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="panbai ooti"/>
</div>
<div class="lyrico-lyrics-wrapper">valiyil nodiyatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valiyil nodiyatha"/>
</div>
<div class="lyrico-lyrics-wrapper">nesam korpal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nesam korpal"/>
</div>
<div class="lyrico-lyrics-wrapper">iyilum i aval
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iyilum i aval"/>
</div>
<div class="lyrico-lyrics-wrapper">aim nilathilum irupaval
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aim nilathilum irupaval"/>
</div>
<div class="lyrico-lyrics-wrapper">uyirum meiyum serntha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyirum meiyum serntha"/>
</div>
<div class="lyrico-lyrics-wrapper">uyirmei aval than amma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyirmei aval than amma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thayin munne entha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thayin munne entha"/>
</div>
<div class="lyrico-lyrics-wrapper">theivamum pinne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theivamum pinne"/>
</div>
<div class="lyrico-lyrics-wrapper">aarathanai aarathanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aarathanai aarathanai"/>
</div>
<div class="lyrico-lyrics-wrapper">vaanum kooda chinnan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaanum kooda chinnan"/>
</div>
<div class="lyrico-lyrics-wrapper">siru kudai aagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="siru kudai aagum"/>
</div>
<div class="lyrico-lyrics-wrapper">aarathanai aarathanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aarathanai aarathanai"/>
</div>
<div class="lyrico-lyrics-wrapper">elu suram pathaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="elu suram pathaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">ettai olippal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ettai olippal"/>
</div>
<div class="lyrico-lyrics-wrapper">ettatha uyarathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ettatha uyarathai"/>
</div>
<div class="lyrico-lyrics-wrapper">etti alippal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="etti alippal"/>
</div>
<div class="lyrico-lyrics-wrapper">eduthalum kuraiyatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eduthalum kuraiyatha"/>
</div>
<div class="lyrico-lyrics-wrapper">selvamai irupal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="selvamai irupal"/>
</div>
<div class="lyrico-lyrics-wrapper">koduthalum vaadatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koduthalum vaadatha"/>
</div>
<div class="lyrico-lyrics-wrapper">malarai siripal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malarai siripal"/>
</div>
<div class="lyrico-lyrics-wrapper">aarathanai aarathanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aarathanai aarathanai"/>
</div>
<div class="lyrico-lyrics-wrapper">aarathanai aarathanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aarathanai aarathanai"/>
</div>
</pre>
