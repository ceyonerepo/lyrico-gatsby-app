---
title: 'pona pogattum song lyrics'
album: 'Master'
artist: 'Anirudh Ravichander'
lyricist: 'Vishnu Edavan'
director: 'Lokesh Kanagaraj'
path: '/albums/master-song-lyrics'
song: 'Pona Pogattum'
image: ../../images/albumart/master.jpg
date: 2021-01-13
lang: tamil
youtubeLink: ''
type: 'sad'
singers: 
- Anirudh Ravichander
---



<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">

  <div class="lyrico-lyrics-wrapper">Tharuthala kadharuna
<input type="checkbox" class="lyrico-select-lyric-line" value="Tharuthala kadharuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Ketkuma ketkuma
<input type="checkbox" class="lyrico-select-lyric-line" value="Ketkuma ketkuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharuthala kadharuna
<input type="checkbox" class="lyrico-select-lyric-line" value="Tharuthala kadharuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Ketkuma ketkuma
<input type="checkbox" class="lyrico-select-lyric-line" value="Ketkuma ketkuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Iruttu araiyula
<input type="checkbox" class="lyrico-select-lyric-line" value="Iruttu araiyula"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada onnum maravila
<input type="checkbox" class="lyrico-select-lyric-line" value="Ada onnum maravila"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan siraga virikka thaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Naan siraga virikka thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam aasa valarthutten
<input type="checkbox" class="lyrico-select-lyric-line" value="Konjam aasa valarthutten"/>
</div>
<div class="lyrico-lyrics-wrapper">Velicham therinjathu
<input type="checkbox" class="lyrico-select-lyric-line" value="Velicham therinjathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mela vaanam vidinjathu
<input type="checkbox" class="lyrico-select-lyric-line" value="Mela vaanam vidinjathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aana parakka mudiyala
<input type="checkbox" class="lyrico-select-lyric-line" value="Aana parakka mudiyala"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi nagara mudiyala
<input type="checkbox" class="lyrico-select-lyric-line" value="Adi nagara mudiyala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Pacha mannu pulla rendu
<input type="checkbox" class="lyrico-select-lyric-line" value="Pacha mannu pulla rendu"/>
</div>
<div class="lyrico-lyrics-wrapper">Moochu ninnu poyaachu inga
<input type="checkbox" class="lyrico-select-lyric-line" value="Moochu ninnu poyaachu inga"/>
</div>
<div class="lyrico-lyrics-wrapper">Thangurathum thongurathum
<input type="checkbox" class="lyrico-select-lyric-line" value="Thangurathum thongurathum"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendum onnu aayaachi
<input type="checkbox" class="lyrico-select-lyric-line" value="Rendum onnu aayaachi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kannu rendum kalanguthaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Kannu rendum kalanguthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyae thodachukko ingae
<input type="checkbox" class="lyrico-select-lyric-line" value="Neeyae thodachukko ingae"/>
</div>
<div class="lyrico-lyrics-wrapper">Appan aathaa yaarum illae
<input type="checkbox" class="lyrico-select-lyric-line" value="Appan aathaa yaarum illae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaniyae thavichukko
<input type="checkbox" class="lyrico-select-lyric-line" value="Thaniyae thavichukko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ponaa pogattum
<input type="checkbox" class="lyrico-select-lyric-line" value="Ponaa pogattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pichaa usuru thaanae
<input type="checkbox" class="lyrico-select-lyric-line" value="Pichaa usuru thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiruntha mudiyaatha
<input type="checkbox" class="lyrico-select-lyric-line" value="Thiruntha mudiyaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharuthala thaan naamae
<input type="checkbox" class="lyrico-select-lyric-line" value="Tharuthala thaan naamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponaa pogattum
<input type="checkbox" class="lyrico-select-lyric-line" value="Ponaa pogattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pichaa usuru thaanae
<input type="checkbox" class="lyrico-select-lyric-line" value="Pichaa usuru thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiruntha aasai ulla
<input type="checkbox" class="lyrico-select-lyric-line" value="Thiruntha aasai ulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharuthala thaan naamae
<input type="checkbox" class="lyrico-select-lyric-line" value="Tharuthala thaan naamae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Tharuthala kadharuna
<input type="checkbox" class="lyrico-select-lyric-line" value="Tharuthala kadharuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Ketkuma ketkuma
<input type="checkbox" class="lyrico-select-lyric-line" value="Ketkuma ketkuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharuthala kadharuna
<input type="checkbox" class="lyrico-select-lyric-line" value="Tharuthala kadharuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Ketkuma ketkuma
<input type="checkbox" class="lyrico-select-lyric-line" value="Ketkuma ketkuma"/>
</div>
</pre>