---
title: "edhemaina song lyrics"
album: "Evaru"
artist: "Sricharan Pakala"
lyricist: "V N V Ramesh Kumar"
director: "Venkat Ramji"
path: "/albums/evaru-lyrics"
song: "Edhemaina"
image: ../../images/albumart/evaru.jpg
date: 2019-08-15
lang: telugu
youtubeLink: 
type: "happy"
singers:
  - Poojan Kohli
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">avaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avaru"/>
</div>
<div class="lyrico-lyrics-wrapper">avaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avaru"/>
</div>
<div class="lyrico-lyrics-wrapper">avaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ranme roju prathi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ranme roju prathi "/>
</div>
<div class="lyrico-lyrics-wrapper">wadiki gelichedavvaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="wadiki gelichedavvaru"/>
</div>
<div class="lyrico-lyrics-wrapper">kshaname chaalu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kshaname chaalu "/>
</div>
<div class="lyrico-lyrics-wrapper">papaniki baliga avevaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="papaniki baliga avevaru"/>
</div>
<div class="lyrico-lyrics-wrapper">dorike varaku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dorike varaku "/>
</div>
<div class="lyrico-lyrics-wrapper">rajalula tirigedavvaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rajalula tirigedavvaru"/>
</div>
<div class="lyrico-lyrics-wrapper">musuge theesi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="musuge theesi "/>
</div>
<div class="lyrico-lyrics-wrapper">lokaniki telipedavvaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lokaniki telipedavvaru"/>
</div>
<div class="lyrico-lyrics-wrapper">ra asura asura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ra asura asura"/>
</div>
<div class="lyrico-lyrics-wrapper">ere veseyra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ere veseyra"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">edemaina edemaina 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edemaina edemaina "/>
</div>
<div class="lyrico-lyrics-wrapper">edemaina agaduga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edemaina agaduga"/>
</div>
<div class="lyrico-lyrics-wrapper">adurevarunna adurevarunna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adurevarunna adurevarunna "/>
</div>
<div class="lyrico-lyrics-wrapper">adurevarunna vadalduga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adurevarunna vadalduga"/>
</div>
<div class="lyrico-lyrics-wrapper">edemaina edemaina 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edemaina edemaina "/>
</div>
<div class="lyrico-lyrics-wrapper">edemaina agaduga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edemaina agaduga"/>
</div>
<div class="lyrico-lyrics-wrapper">adurevarunna adurevarunna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adurevarunna adurevarunna "/>
</div>
<div class="lyrico-lyrics-wrapper">adurevarunna vadalduga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adurevarunna vadalduga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">avaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avaru"/>
</div>
<div class="lyrico-lyrics-wrapper">avaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avaru"/>
</div>
<div class="lyrico-lyrics-wrapper">avaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nuvu chikati ayite mari sury vidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvu chikati ayite mari sury vidu"/>
</div>
<div class="lyrico-lyrics-wrapper">nitelle ninne ventadestadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nitelle ninne ventadestadu"/>
</div>
<div class="lyrico-lyrics-wrapper">nee gatmedaina teg tavvestadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee gatmedaina teg tavvestadu"/>
</div>
<div class="lyrico-lyrics-wrapper">nuvu tadini tante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvu tadini tante"/>
</div>
<div class="lyrico-lyrics-wrapper">taldanne vidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="taldanne vidu"/>
</div>
<div class="lyrico-lyrics-wrapper">ra asura asura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ra asura asura"/>
</div>
<div class="lyrico-lyrics-wrapper">ere veseyra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ere veseyra"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">edemaina edemaina 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edemaina edemaina "/>
</div>
<div class="lyrico-lyrics-wrapper">edemaina agaduga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edemaina agaduga"/>
</div>
<div class="lyrico-lyrics-wrapper">adurevarunna adurevarunna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adurevarunna adurevarunna "/>
</div>
<div class="lyrico-lyrics-wrapper">adurevarunna vadalduga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adurevarunna vadalduga"/>
</div>
<div class="lyrico-lyrics-wrapper">edemaina edemaina 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edemaina edemaina "/>
</div>
<div class="lyrico-lyrics-wrapper">edemaina agaduga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edemaina agaduga"/>
</div>
<div class="lyrico-lyrics-wrapper">adurevarunna adurevarunna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adurevarunna adurevarunna "/>
</div>
<div class="lyrico-lyrics-wrapper">adurevarunna vadalduga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adurevarunna vadalduga"/>
</div>
<div class="lyrico-lyrics-wrapper">avaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avaru"/>
</div>
</pre>
