---
title: "payya payya song lyrics"
album: "Enkitta Mothathe"
artist: "Natarajan Sankaran"
lyricist: "Yugabharathi"
director: "Ramu Chellappa"
path: "/albums/enkitta-mothathe-lyrics"
song: "Payya Payya"
image: ../../images/albumart/enkitta-mothathe.jpg
date: 2017-03-24
lang: tamil
youtubeLink: "https://www.youtube.com/embed/L9orOa980Us"
type: "love"
singers:
  - V V Prasanna
  - Vandana Srinivasan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Onna Enni Kaathirukken Vaayendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna Enni Kaathirukken Vaayendi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennathukku Kaatture Nee Poochaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennathukku Kaatture Nee Poochaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasula Yendi Velakayum Pota
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasula Yendi Velakayum Pota"/>
</div>
<div class="lyrico-lyrics-wrapper">Velangala Edhum Verikkira Rote'a
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velangala Edhum Verikkira Rote'a"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Payya Payya Nerungi Vaaren Pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Payya Payya Nerungi Vaaren Pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Vayya Vayya Tharuven Naanum Tholla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayya Vayya Tharuven Naanum Tholla"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasula Yendi Velakayum Pota
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasula Yendi Velakayum Pota"/>
</div>
<div class="lyrico-lyrics-wrapper">Velangala Edhum Therikkira Rote'a
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velangala Edhum Therikkira Rote'a"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theriyum Theriyum Idhu Unn Settai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theriyum Theriyum Idhu Unn Settai"/>
</div>
<div class="lyrico-lyrics-wrapper">Payya Payya Nerungi Vaaren Pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Payya Payya Nerungi Vaaren Pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Vayya Vayya Tharuven Naanum Tholla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayya Vayya Tharuven Naanum Tholla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hmm Pacha Thanniya Irundhen Munnala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm Pacha Thanniya Irundhen Munnala"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadarasam Aagivitten Unnaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadarasam Aagivitten Unnaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Kandu Nee Siricha Pinnaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Kandu Nee Siricha Pinnaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Ekka Chakkama Sevandhen Thannale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ekka Chakkama Sevandhen Thannale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidinja Pinnalum Orangara Aalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidinja Pinnalum Orangara Aalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Nenachedhaan Keranguren Kelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Nenachedhaan Keranguren Kelu"/>
</div>
<div class="lyrico-lyrics-wrapper">Edha Nenachalum Ularura Vaayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edha Nenachalum Ularura Vaayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kedakudhu Unaal Oonji Poyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kedakudhu Unaal Oonji Poyi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theriyum Theriyum Idhu Unn Settai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theriyum Theriyum Idhu Unn Settai"/>
</div>
<div class="lyrico-lyrics-wrapper">Payya Payya Nerungi Vaaren Pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Payya Payya Nerungi Vaaren Pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Vayya Vayya Tharuven Naanum Tholla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayya Vayya Tharuven Naanum Tholla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Petha Thaayum Kooda Kaatadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Petha Thaayum Kooda Kaatadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbula Nee Aani Adichi Maatadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbula Nee Aani Adichi Maatadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kilikira Oleya Nee Aanji
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kilikira Oleya Nee Aanji"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjukulla Paakuriye Aaranji
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjukulla Paakuriye Aaranji"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pazhakamilaadha Kanavugalaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pazhakamilaadha Kanavugalaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Paduthira Needhaan Padharudhu Moola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paduthira Needhaan Padharudhu Moola"/>
</div>
<div class="lyrico-lyrics-wrapper">Kozhandhaiya Pola Kudhikira Aasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kozhandhaiya Pola Kudhikira Aasa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodukudhu Imsa Enna Pesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodukudhu Imsa Enna Pesa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theriyum Theriyum Idhu Unn Settai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theriyum Theriyum Idhu Unn Settai"/>
</div>
<div class="lyrico-lyrics-wrapper">Payya Payya Nerungi Vaaren Pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Payya Payya Nerungi Vaaren Pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Vayya Vayya Tharuven Naanum Tholla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayya Vayya Tharuven Naanum Tholla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thandha Naane Naane Thandha Naane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandha Naane Naane Thandha Naane"/>
</div>
<div class="lyrico-lyrics-wrapper">Thandha Naane Naane Thandha Naane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandha Naane Naane Thandha Naane"/>
</div>
</pre>
