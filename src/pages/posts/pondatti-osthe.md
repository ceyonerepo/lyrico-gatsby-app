---
title: "pondatti song lyrics"
album: "Osthe"
artist: "Sai Thaman"
lyricist: "Silambarasan"
director: "S. Dharani"
path: "/albums/osthe-lyrics"
song: "Pondatti"
image: ../../images/albumart/osthe.jpg
date: 2011-12-08
lang: tamil
youtubeLink: "https://www.youtube.com/embed/pfHJ_BIQ7P8"
type: "love"
singers:
  - Silambarasan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hey vaadi vaadi vaadi vaadi cute pondati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey vaadi vaadi vaadi vaadi cute pondati"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa thaanga maten thoonga maten nee illaati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa thaanga maten thoonga maten nee illaati"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi vaadi vaadi vaadi vaadi hot pondati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi vaadi vaadi vaadi vaadi hot pondati"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa thaanga maten thoonga maten nee illaati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa thaanga maten thoonga maten nee illaati"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pondati adi nee thaanae en sweety
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pondati adi nee thaanae en sweety"/>
</div>
<div class="lyrico-lyrics-wrapper">I love u till you are a paati thevai illa vapaati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I love u till you are a paati thevai illa vapaati"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pondati adi nee thaanae en sweety
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pondati adi nee thaanae en sweety"/>
</div>
<div class="lyrico-lyrics-wrapper">I love u till you are a paati thevai illa vapaati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I love u till you are a paati thevai illa vapaati"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nalla kanavana naan irupen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla kanavana naan irupen"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru uthamana naan nadapen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru uthamana naan nadapen"/>
</div>
<div class="lyrico-lyrics-wrapper">Un thollai ellam porupen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un thollai ellam porupen"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kashtatha naan koraipen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kashtatha naan koraipen"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kankalanga vidamaten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kankalanga vidamaten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey vaadi vaadi vaadi vaadi cute pondati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey vaadi vaadi vaadi vaadi cute pondati"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa thaanga maten thoonga maten nee illaati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa thaanga maten thoonga maten nee illaati"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi vaadi vaadi vaadi vaadi hot pondati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi vaadi vaadi vaadi vaadi hot pondati"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa thaanga maten thoonga maten nee illaati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa thaanga maten thoonga maten nee illaati"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pondati Pondati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pondati Pondati"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Coffee koduthu kalaiyila naanae unna ezhupi viduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Coffee koduthu kalaiyila naanae unna ezhupi viduven"/>
</div>
<div class="lyrico-lyrics-wrapper">Samaika theriyalana naanae samayal senju unnaku ooti viduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samaika theriyalana naanae samayal senju unnaku ooti viduven"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna naan ennaikumae santhega pada maten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna naan ennaikumae santhega pada maten"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna neeyum santhega padum maathiri nadaka maten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna neeyum santhega padum maathiri nadaka maten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un uyira naan irupen en uyira unna nenapen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un uyira naan irupen en uyira unna nenapen"/>
</div>
<div class="lyrico-lyrics-wrapper">En nenjula unna somapen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En nenjula unna somapen"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna daily naanum rasipen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna daily naanum rasipen"/>
</div>
<div class="lyrico-lyrics-wrapper">Un nizhala polae naan irupen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un nizhala polae naan irupen"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey vaadi vaadi vaadi vaadi cute pondati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey vaadi vaadi vaadi vaadi cute pondati"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa thaanga maten thoonga maten nee illaati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa thaanga maten thoonga maten nee illaati"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi vaadi adi vaadi en hot pondati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi vaadi adi vaadi en hot pondati"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa thaanga maten thoonga maten nee illaati ti ti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa thaanga maten thoonga maten nee illaati ti ti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En pondati pondati pondati pondati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En pondati pondati pondati pondati"/>
</div>
<div class="lyrico-lyrics-wrapper">Venam vapaati vapaati vapaati vapaati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venam vapaati vapaati vapaati vapaati"/>
</div>
<div class="lyrico-lyrics-wrapper">Pondati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pondati"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey hey hey unaku munnadi sathiyama en usuru enna vidaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hey hey unaku munnadi sathiyama en usuru enna vidaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yenna naan poita unna yaarum vithavaya paaka koodathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenna naan poita unna yaarum vithavaya paaka koodathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna vita unna evandi paathupaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna vita unna evandi paathupaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalla pathupenu solli poiya nadipaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla pathupenu solli poiya nadipaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru thagapan polae irupen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru thagapan polae irupen"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru thaaya polavum irupen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru thaaya polavum irupen"/>
</div>
<div class="lyrico-lyrics-wrapper">Un nanban polae nadapen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un nanban polae nadapen"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha kadavul polae kaapen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha kadavul polae kaapen"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kozhanthaiyaavum naan porapen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kozhanthaiyaavum naan porapen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey vaadi vaadi vaadi vaadi cute pondati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey vaadi vaadi vaadi vaadi cute pondati"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa thaanga maten thoonga maten nee illaati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa thaanga maten thoonga maten nee illaati"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi vaadi adi vaadi en hot pondati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi vaadi adi vaadi en hot pondati"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa thaanga maten thoonga maten nee illaati ti ti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa thaanga maten thoonga maten nee illaati ti ti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En pondati pondati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En pondati pondati"/>
</div>
<div class="lyrico-lyrics-wrapper">I love you till you are a paati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I love you till you are a paati"/>
</div>
<div class="lyrico-lyrics-wrapper">En pondati pondati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En pondati pondati"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaku theva illa vapaati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaku theva illa vapaati"/>
</div>
<div class="lyrico-lyrics-wrapper">I love you de my pondati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I love you de my pondati"/>
</div>
</pre>
