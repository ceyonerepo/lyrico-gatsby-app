---
title: "bum ha rum song lyrics"
album: "Odu Raja Odu"
artist: "Tosh Nanda"
lyricist: "Kavithran"
director: "Nishanth Ravindaran - Jathin Sanker Raj"
path: "/albums/odu-raja-odu-lyrics"
song: "Bum Ha Rum"
image: ../../images/albumart/odu-raja-odu.jpg
date: 2018-08-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/nFC-UpBwqho"
type: "happy"
singers:
  - Karthik
  - Shwetha Mohan
  - Abishek
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Re Pa Paaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Re Pa Paaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Life Semma Happy Happy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life Semma Happy Happy"/>
</div>
<div class="lyrico-lyrics-wrapper">Come On Pulla Take A Selfie
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Come On Pulla Take A Selfie"/>
</div>
<div class="lyrico-lyrics-wrapper">Teen Age Oda Kanavu Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teen Age Oda Kanavu Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhkai Pattam Vaanavil Mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkai Pattam Vaanavil Mela"/>
</div>
<div class="lyrico-lyrics-wrapper">Boomiku Oru Putai Potu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boomiku Oru Putai Potu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinnaithaadi Podu Route
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinnaithaadi Podu Route"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilavin Mela Paadal Ketu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilavin Mela Paadal Ketu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinmeen Pidipai Thoondil Potu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinmeen Pidipai Thoondil Potu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thattu Thadaikal Yeathum Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thattu Thadaikal Yeathum Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattu Kaaval Onnum Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattu Kaaval Onnum Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellai Illai Illai Thollai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellai Illai Illai Thollai"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha Thodakkam Andha Mudivu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Thodakkam Andha Mudivu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeathum Inge Marmam Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeathum Inge Marmam Illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Odu Odu Odu Raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odu Odu Odu Raja"/>
</div>
<div class="lyrico-lyrics-wrapper">Odu Odu Odu Raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odu Odu Odu Raja"/>
</div>
<div class="lyrico-lyrics-wrapper">Odu Odu Odu Raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odu Odu Odu Raja"/>
</div>
<div class="lyrico-lyrics-wrapper">Odu Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odu Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Odu Odu Odu Raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odu Odu Odu Raja"/>
</div>
<div class="lyrico-lyrics-wrapper">Odu Odu Odu Raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odu Odu Odu Raja"/>
</div>
<div class="lyrico-lyrics-wrapper">Odu Odu Odu Raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odu Odu Odu Raja"/>
</div>
<div class="lyrico-lyrics-wrapper">Odu Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odu Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Odu Odu Odu Raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odu Odu Odu Raja"/>
</div>
<div class="lyrico-lyrics-wrapper">Odu Odu Odu Raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odu Odu Odu Raja"/>
</div>
<div class="lyrico-lyrics-wrapper">Odu Odu Odu Raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odu Odu Odu Raja"/>
</div>
<div class="lyrico-lyrics-wrapper">Odu Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odu Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Life-u Happy Happy Happy Happy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life-u Happy Happy Happy Happy"/>
</div>
<div class="lyrico-lyrics-wrapper">Life-u Happy Happy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life-u Happy Happy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yai Senthu Sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yai Senthu Sollu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Life-u Happy Happy Happy Happy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life-u Happy Happy Happy Happy"/>
</div>
<div class="lyrico-lyrics-wrapper">Life-u Happy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life-u Happy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dai Sorimeenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dai Sorimeenu"/>
</div>
<div class="lyrico-lyrics-wrapper">Don't Worry
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Don't Worry"/>
</div>
<div class="lyrico-lyrics-wrapper">Why Worry Worry Worry Worry
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Why Worry Worry Worry Worry"/>
</div>
<div class="lyrico-lyrics-wrapper">Be Happy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Be Happy"/>
</div>
<div class="lyrico-lyrics-wrapper">Be Happy Happy Happy Happy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Be Happy Happy Happy Happy"/>
</div>
<div class="lyrico-lyrics-wrapper">Semma Semma Semma Semma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Semma Semma Semma Semma"/>
</div>
<div class="lyrico-lyrics-wrapper">Happy Alway Happy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Happy Alway Happy"/>
</div>
<div class="lyrico-lyrics-wrapper">Happy Happy Happy Happy Selfie
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Happy Happy Happy Happy Selfie"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannukulle Konjam Kanavugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannukulle Konjam Kanavugal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalai Vaarum Haunting Ninaivugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalai Vaarum Haunting Ninaivugal"/>
</div>
<div class="lyrico-lyrics-wrapper">Saalai Engum Puththam Puthumaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saalai Engum Puththam Puthumaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Brain Ah Arikkum Panja Palamaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Brain Ah Arikkum Panja Palamaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhumpi Paraka Vendum Siragugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhumpi Paraka Vendum Siragugal"/>
</div>
<div class="lyrico-lyrics-wrapper">Inimel Naamthan Paayum Paravaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inimel Naamthan Paayum Paravaigal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iravu Muzhukka Looty Looty
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravu Muzhukka Looty Looty"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagalu Muzhukka Sleeping Duty
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagalu Muzhukka Sleeping Duty"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadigaaram Vedichu Pochu Machi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadigaaram Vedichu Pochu Machi"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha Kaalam Intha Kolam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Kaalam Intha Kolam"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Agum Engu Pogummo?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Agum Engu Pogummo?"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Odu Odu Odu Raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odu Odu Odu Raja"/>
</div>
<div class="lyrico-lyrics-wrapper">Odu Odu Odu Raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odu Odu Odu Raja"/>
</div>
<div class="lyrico-lyrics-wrapper">Odu Odu Odu Raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odu Odu Odu Raja"/>
</div>
<div class="lyrico-lyrics-wrapper">Odu Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odu Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Odu Odu Odu Raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odu Odu Odu Raja"/>
</div>
<div class="lyrico-lyrics-wrapper">Odu Odu Odu Raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odu Odu Odu Raja"/>
</div>
<div class="lyrico-lyrics-wrapper">Odu Odu Odu Raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odu Odu Odu Raja"/>
</div>
<div class="lyrico-lyrics-wrapper">Odu Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odu Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Odu Odu Odu Raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odu Odu Odu Raja"/>
</div>
<div class="lyrico-lyrics-wrapper">Odu Odu Odu Raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odu Odu Odu Raja"/>
</div>
<div class="lyrico-lyrics-wrapper">Odu Odu Odu Raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odu Odu Odu Raja"/>
</div>
<div class="lyrico-lyrics-wrapper">Odu Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odu Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Life-u Happy Happy Happy Happy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life-u Happy Happy Happy Happy"/>
</div>
<div class="lyrico-lyrics-wrapper">Life-u Happy Happy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life-u Happy Happy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Re Pa Paaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Re Pa Paaaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Life-u Happy Happy Happy Happy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life-u Happy Happy Happy Happy"/>
</div>
<div class="lyrico-lyrics-wrapper">Life-u Happy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life-u Happy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Okay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okay"/>
</div>
</pre>
