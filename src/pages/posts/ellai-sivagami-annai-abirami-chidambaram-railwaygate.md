---
title: "ellai sivagami annai abirami song lyrics"
album: "Chidambaram Railwaygate"
artist: "Karthik Raja"
lyricist: "Priyan"
director: "Sivabalan"
path: "/albums/chidambaram-railwaygate-lyrics"
song: "Ellai Sivagami Annai Abirami"
image: ../../images/albumart/chidambaram-railwaygate.jpg
date: 2021-02-05
lang: tamil
youtubeLink: "https://www.youtube.com/embed/y0roHYwgtmk"
type: "Love"
singers:
  - Ilaiyaraaja
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">ellai sivagami annai abirami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellai sivagami annai abirami"/>
</div>
<div class="lyrico-lyrics-wrapper">undhan thiru paadham 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="undhan thiru paadham "/>
</div>
<div class="lyrico-lyrics-wrapper">saran adanthom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saran adanthom"/>
</div>
<div class="lyrico-lyrics-wrapper">eesan udanana sakthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eesan udanana sakthi"/>
</div>
<div class="lyrico-lyrics-wrapper">unai paadi sitham thiruveri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unai paadi sitham thiruveri"/>
</div>
<div class="lyrico-lyrics-wrapper">adi paninthom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi paninthom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thillai nagar thevathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thillai nagar thevathai"/>
</div>
<div class="lyrico-lyrics-wrapper">en thevathai kanden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en thevathai kanden"/>
</div>
<div class="lyrico-lyrics-wrapper">thanga thugil tharagai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanga thugil tharagai"/>
</div>
<div class="lyrico-lyrics-wrapper">aval tharagai enben
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aval tharagai enben"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">minnal mazhai polave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="minnal mazhai polave"/>
</div>
<div class="lyrico-lyrics-wrapper">ennai theendinal inge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennai theendinal inge"/>
</div>
<div class="lyrico-lyrics-wrapper">minnum pani kaatile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="minnum pani kaatile"/>
</div>
<div class="lyrico-lyrics-wrapper">theeyagiye nindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theeyagiye nindren"/>
</div>
<div class="lyrico-lyrics-wrapper">pala kodi aandugal anathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pala kodi aandugal anathu"/>
</div>
<div class="lyrico-lyrics-wrapper">boomi un pol alagiyai kaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="boomi un pol alagiyai kaana"/>
</div>
<div class="lyrico-lyrics-wrapper">ini enna aven nanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ini enna aven nanum"/>
</div>
<div class="lyrico-lyrics-wrapper">ethiril nindru ne enai kaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethiril nindru ne enai kaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thillai nagar thevathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thillai nagar thevathai"/>
</div>
<div class="lyrico-lyrics-wrapper">en thevathai kanden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en thevathai kanden"/>
</div>
<div class="lyrico-lyrics-wrapper">thanga thugil tharagai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanga thugil tharagai"/>
</div>
<div class="lyrico-lyrics-wrapper">aval tharagai enben
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aval tharagai enben"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaatril aadum un koondhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatril aadum un koondhal"/>
</div>
<div class="lyrico-lyrics-wrapper">paarthal enna paguthariva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paarthal enna paguthariva"/>
</div>
<div class="lyrico-lyrics-wrapper">netri piriyil sudiya kungumam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="netri piriyil sudiya kungumam"/>
</div>
<div class="lyrico-lyrics-wrapper">adada athu than aanmigama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adada athu than aanmigama"/>
</div>
<div class="lyrico-lyrics-wrapper">valaigindra puruvam irandum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valaigindra puruvam irandum"/>
</div>
<div class="lyrico-lyrics-wrapper">en mel paya thudikindra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en mel paya thudikindra"/>
</div>
<div class="lyrico-lyrics-wrapper">vil thana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vil thana"/>
</div>
<div class="lyrico-lyrics-wrapper">ilukindra kangal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ilukindra kangal "/>
</div>
<div class="lyrico-lyrics-wrapper">ragasiyam kaatum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ragasiyam kaatum"/>
</div>
<div class="lyrico-lyrics-wrapper">karuppu vellai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karuppu vellai"/>
</div>
<div class="lyrico-lyrics-wrapper">thirai padama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thirai padama"/>
</div>
<div class="lyrico-lyrics-wrapper">mookin nuniyo muluthai ennai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mookin nuniyo muluthai ennai "/>
</div>
<div class="lyrico-lyrics-wrapper">eerkum manthira malai mugada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eerkum manthira malai mugada"/>
</div>
<div class="lyrico-lyrics-wrapper">idhalgal rendum inaithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhalgal rendum inaithu"/>
</div>
<div class="lyrico-lyrics-wrapper">naalum mupaal pesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naalum mupaal pesum"/>
</div>
<div class="lyrico-lyrics-wrapper">thirukuralaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thirukuralaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thillai nagar thevathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thillai nagar thevathai"/>
</div>
<div class="lyrico-lyrics-wrapper">en thevathai kanden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en thevathai kanden"/>
</div>
<div class="lyrico-lyrics-wrapper">thanga thugil tharagai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanga thugil tharagai"/>
</div>
<div class="lyrico-lyrics-wrapper">aval tharagai enben
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aval tharagai enben"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">mudi kodhum un sevigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudi kodhum un sevigal"/>
</div>
<div class="lyrico-lyrics-wrapper">kelvi kuriyai irukiratha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kelvi kuriyai irukiratha"/>
</div>
<div class="lyrico-lyrics-wrapper">mugam ennadi suriya chandira 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mugam ennadi suriya chandira "/>
</div>
<div class="lyrico-lyrics-wrapper">oliyai ingu jolikiratha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oliyai ingu jolikiratha"/>
</div>
<div class="lyrico-lyrics-wrapper">mellidai antha kondai oosi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mellidai antha kondai oosi"/>
</div>
<div class="lyrico-lyrics-wrapper">valaivai ingu neligiratha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valaivai ingu neligiratha"/>
</div>
<div class="lyrico-lyrics-wrapper">boomiyai thodugindra paadam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="boomiyai thodugindra paadam"/>
</div>
<div class="lyrico-lyrics-wrapper">pirantha kulanthaiyin mugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pirantha kulanthaiyin mugam"/>
</div>
<div class="lyrico-lyrics-wrapper">pol sivakiratha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pol sivakiratha"/>
</div>
<div class="lyrico-lyrics-wrapper">alagai aadum motha udalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alagai aadum motha udalum"/>
</div>
<div class="lyrico-lyrics-wrapper">theiva vadivathin nagal thana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theiva vadivathin nagal thana"/>
</div>
<div class="lyrico-lyrics-wrapper">arugil serum nanum inge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arugil serum nanum inge"/>
</div>
<div class="lyrico-lyrics-wrapper">aanin kadavul thugal thana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aanin kadavul thugal thana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thillai nagar thevathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thillai nagar thevathai"/>
</div>
<div class="lyrico-lyrics-wrapper">en thevathai kanden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en thevathai kanden"/>
</div>
<div class="lyrico-lyrics-wrapper">thanga thugil tharagai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanga thugil tharagai"/>
</div>
<div class="lyrico-lyrics-wrapper">aval tharagai enben
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aval tharagai enben"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">minnal mazhai polave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="minnal mazhai polave"/>
</div>
<div class="lyrico-lyrics-wrapper">ennai theendinal inge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennai theendinal inge"/>
</div>
<div class="lyrico-lyrics-wrapper">minnum pani kaatile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="minnum pani kaatile"/>
</div>
<div class="lyrico-lyrics-wrapper">theeyagiye nindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theeyagiye nindren"/>
</div>
<div class="lyrico-lyrics-wrapper">pala kodi aandugal anathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pala kodi aandugal anathu"/>
</div>
<div class="lyrico-lyrics-wrapper">boomi un pol alagiyai kaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="boomi un pol alagiyai kaana"/>
</div>
<div class="lyrico-lyrics-wrapper">ini enna aven nanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ini enna aven nanum"/>
</div>
<div class="lyrico-lyrics-wrapper">ethiril nindru ne enai kaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethiril nindru ne enai kaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thillai nagar thevathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thillai nagar thevathai"/>
</div>
<div class="lyrico-lyrics-wrapper">en thevathai kanden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en thevathai kanden"/>
</div>
<div class="lyrico-lyrics-wrapper">thanga thugil tharagai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanga thugil tharagai"/>
</div>
<div class="lyrico-lyrics-wrapper">aval tharagai enben
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aval tharagai enben"/>
</div>
</pre>
