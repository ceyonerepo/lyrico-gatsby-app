---
title: "maanguyelum kizhiyum song lyrics"
album: "Akka Kuruvi"
artist: "Ilaiyaraaja"
lyricist: "Ilaiyaraaja"
director: "Samy"
path: "/albums/akka-kuruvi-lyrics"
song: "Maanguyelum Kizhiyum"
image: ../../images/albumart/akka-kuruvi.jpg
date: 2022-05-06
lang: tamil
youtubeLink: "https://www.youtube.com/embed/25iU4iltJSc"
type: "happy"
singers:
  - Ilaiyaraaja
  - Vibhavari
  - Aishwarya
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">maanguyelum kizhiyum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maanguyelum kizhiyum "/>
</div>
<div class="lyrico-lyrics-wrapper">koodi pesum engal veedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koodi pesum engal veedu"/>
</div>
<div class="lyrico-lyrics-wrapper">mayilai pola manathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mayilai pola manathai"/>
</div>
<div class="lyrico-lyrics-wrapper">thiranthu aadi paadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thiranthu aadi paadu"/>
</div>
<div class="lyrico-lyrics-wrapper">poonthendral kaatru vaasal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poonthendral kaatru vaasal"/>
</div>
<div class="lyrico-lyrics-wrapper">thirakum alagu paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thirakum alagu paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">anbin raagam indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anbin raagam indru"/>
</div>
<div class="lyrico-lyrics-wrapper">ketkum paaru paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ketkum paaru paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">aha paasam nesam ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aha paasam nesam ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">vetru vaarthai alla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetru vaarthai alla"/>
</div>
<div class="lyrico-lyrics-wrapper">aasaiyodu koodi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aasaiyodu koodi "/>
</div>
<div class="lyrico-lyrics-wrapper">vaalvu vaala solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalvu vaala solla"/>
</div>
<div class="lyrico-lyrics-wrapper">engal nenjil anbuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engal nenjil anbuku"/>
</div>
<div class="lyrico-lyrics-wrapper">endrum panjam illai illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="endrum panjam illai illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">maanguyelum kizhiyum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maanguyelum kizhiyum "/>
</div>
<div class="lyrico-lyrics-wrapper">koodi pesum engal veedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koodi pesum engal veedu"/>
</div>
<div class="lyrico-lyrics-wrapper">mayilai pola manathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mayilai pola manathai"/>
</div>
<div class="lyrico-lyrics-wrapper">thiranthu aadi paadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thiranthu aadi paadu"/>
</div>
<div class="lyrico-lyrics-wrapper">kaatrai pola veesu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatrai pola veesu "/>
</div>
<div class="lyrico-lyrics-wrapper">veesu kaigal korthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veesu kaigal korthu"/>
</div>
<div class="lyrico-lyrics-wrapper">megam pola oodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="megam pola oodu"/>
</div>
<div class="lyrico-lyrics-wrapper">oodu vaanam paarthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oodu vaanam paarthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaatrai pola veesu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatrai pola veesu "/>
</div>
<div class="lyrico-lyrics-wrapper">veesu kaigal korthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veesu kaigal korthu"/>
</div>
<div class="lyrico-lyrics-wrapper">megam pola oodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="megam pola oodu"/>
</div>
<div class="lyrico-lyrics-wrapper">oodu vaanam paarthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oodu vaanam paarthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ooril ulla ther ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooril ulla ther ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">engal veetai paarthu than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engal veetai paarthu than"/>
</div>
<div class="lyrico-lyrics-wrapper">anbu kondu vaalthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anbu kondu vaalthu"/>
</div>
<div class="lyrico-lyrics-wrapper">paarka palaguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paarka palaguthe"/>
</div>
<div class="lyrico-lyrics-wrapper">oorin kangal muluvathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oorin kangal muluvathum"/>
</div>
<div class="lyrico-lyrics-wrapper">endrum engal meethu thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="endrum engal meethu thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">paarpavar poraamai thristi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paarpavar poraamai thristi"/>
</div>
<div class="lyrico-lyrics-wrapper">keelu than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="keelu than"/>
</div>
<div class="lyrico-lyrics-wrapper">paarthu paarthu padipatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paarthu paarthu padipatha"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjil baasai pesuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjil baasai pesuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">solli koduthu varuvatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="solli koduthu varuvatha"/>
</div>
<div class="lyrico-lyrics-wrapper">anbu ennum paadame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anbu ennum paadame"/>
</div>
<div class="lyrico-lyrics-wrapper">engal veedu entha uyirkum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engal veedu entha uyirkum "/>
</div>
<div class="lyrico-lyrics-wrapper">sontha veedu than 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sontha veedu than "/>
</div>
<div class="lyrico-lyrics-wrapper">vanthu irunthu paarthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanthu irunthu paarthu"/>
</div>
<div class="lyrico-lyrics-wrapper">paarthu therinthu kollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paarthu therinthu kollu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">maanguyelum kizhiyum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maanguyelum kizhiyum "/>
</div>
<div class="lyrico-lyrics-wrapper">koodi pesum engal veedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koodi pesum engal veedu"/>
</div>
<div class="lyrico-lyrics-wrapper">mayilai pola manathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mayilai pola manathai"/>
</div>
<div class="lyrico-lyrics-wrapper">thiranthu aadi paadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thiranthu aadi paadu"/>
</div>
<div class="lyrico-lyrics-wrapper">poonthendral kaatru vaasal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poonthendral kaatru vaasal"/>
</div>
<div class="lyrico-lyrics-wrapper">thirakum alagu paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thirakum alagu paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">anbin raagam indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anbin raagam indru"/>
</div>
<div class="lyrico-lyrics-wrapper">ketkum paaru paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ketkum paaru paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thangai konda thunbathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thangai konda thunbathai"/>
</div>
<div class="lyrico-lyrics-wrapper">annan pagirnthu kolluvan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="annan pagirnthu kolluvan"/>
</div>
<div class="lyrico-lyrics-wrapper">thangai thadutha pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thangai thadutha pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">kepathu illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kepathu illai"/>
</div>
<div class="lyrico-lyrics-wrapper">annan seitha thavaru ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="annan seitha thavaru ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">thangaiyodu poei vidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thangaiyodu poei vidum"/>
</div>
<div class="lyrico-lyrics-wrapper">thanthai kaathil pootu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanthai kaathil pootu"/>
</div>
<div class="lyrico-lyrics-wrapper">kodupathu illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kodupathu illai"/>
</div>
<div class="lyrico-lyrics-wrapper">annai thantha uravu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="annai thantha uravu"/>
</div>
<div class="lyrico-lyrics-wrapper">ellam ooril engum irukuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellam ooril engum irukuma"/>
</div>
<div class="lyrico-lyrics-wrapper">annan thangai otrumai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="annan thangai otrumai"/>
</div>
<div class="lyrico-lyrics-wrapper">endrum kaana kidaikuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="endrum kaana kidaikuma"/>
</div>
<div class="lyrico-lyrics-wrapper">engal veedu engal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engal veedu engal "/>
</div>
<div class="lyrico-lyrics-wrapper">kudumbam engum illaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kudumbam engum illaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">vanthu irunthu paarthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanthu irunthu paarthu"/>
</div>
<div class="lyrico-lyrics-wrapper">paarthu therinthu kollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paarthu therinthu kollu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">maanguyelum kizhiyum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maanguyelum kizhiyum "/>
</div>
<div class="lyrico-lyrics-wrapper">koodi pesum engal veedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koodi pesum engal veedu"/>
</div>
<div class="lyrico-lyrics-wrapper">mayilai pola manathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mayilai pola manathai"/>
</div>
<div class="lyrico-lyrics-wrapper">thiranthu aadi paadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thiranthu aadi paadu"/>
</div>
<div class="lyrico-lyrics-wrapper">aha paasam nesam ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aha paasam nesam ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">vetru vaarthai alla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetru vaarthai alla"/>
</div>
<div class="lyrico-lyrics-wrapper">engal nenjil anbuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engal nenjil anbuku"/>
</div>
<div class="lyrico-lyrics-wrapper">endrum panjam illai illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="endrum panjam illai illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">maanguyelum kizhiyum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maanguyelum kizhiyum "/>
</div>
<div class="lyrico-lyrics-wrapper">koodi pesum engal veedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koodi pesum engal veedu"/>
</div>
<div class="lyrico-lyrics-wrapper">mayilai pola manathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mayilai pola manathai"/>
</div>
<div class="lyrico-lyrics-wrapper">thiranthu aadi paadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thiranthu aadi paadu"/>
</div>
</pre>
