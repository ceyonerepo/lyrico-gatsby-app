---
title: "yen pulla song lyrics"
album: "Dhoni Kabadi Kuzhu"
artist: "Roshan Joseph"
lyricist: "N Rasa"
director: "P Iyyappan"
path: "/albums/dhoni-kabadi-kuzhu-lyrics"
song: "Yen Pulla"
image: ../../images/albumart/dhoni-kabadi-kuzhu.jpg
date: 2018-12-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/bRZsqdU28Ww"
type: "love"
singers:
  - Roshan Joseph
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">ye pulla ye pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ye pulla ye pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">epa varuva love solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="epa varuva love solla"/>
</div>
<div class="lyrico-lyrics-wrapper">yen pulla enakulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yen pulla enakulla"/>
</div>
<div class="lyrico-lyrics-wrapper">kathal thantha nee mella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathal thantha nee mella"/>
</div>
<div class="lyrico-lyrics-wrapper">karumba than inikiraye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karumba than inikiraye"/>
</div>
<div class="lyrico-lyrics-wrapper">irumbu enna karaikuraye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irumbu enna karaikuraye"/>
</div>
<div class="lyrico-lyrics-wrapper">vairam pola minnuraye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vairam pola minnuraye"/>
</div>
<div class="lyrico-lyrics-wrapper">vairakiyam pannuraye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vairakiyam pannuraye"/>
</div>
<div class="lyrico-lyrics-wrapper">epo pulla neyum vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="epo pulla neyum vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">enna konjuva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna konjuva"/>
</div>
<div class="lyrico-lyrics-wrapper">epo pulla neyum enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="epo pulla neyum enna"/>
</div>
<div class="lyrico-lyrics-wrapper">kaiyil alluva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaiyil alluva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ye pulla ye pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ye pulla ye pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">epa varuva love solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="epa varuva love solla"/>
</div>
<div class="lyrico-lyrics-wrapper">yen pulla enakulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yen pulla enakulla"/>
</div>
<div class="lyrico-lyrics-wrapper">kathal thantha nee mella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathal thantha nee mella"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">soru thannee irangala di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="soru thannee irangala di"/>
</div>
<div class="lyrico-lyrics-wrapper">thookam enaku pidikala di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thookam enaku pidikala di"/>
</div>
<div class="lyrico-lyrics-wrapper">un kurala ketida thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kurala ketida thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">thinamum poluthu vidiyuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thinamum poluthu vidiyuthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">enakul etho pannuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enakul etho pannuthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">un nenappu thinnuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un nenappu thinnuthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">un arugil irunthidave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un arugil irunthidave"/>
</div>
<div class="lyrico-lyrics-wrapper">en usurum virumbuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en usurum virumbuthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">aari pona kaayam pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aari pona kaayam pona"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjukkule un kaayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjukkule un kaayam"/>
</div>
<div class="lyrico-lyrics-wrapper">kathal solla nanum vanthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathal solla nanum vanthen"/>
</div>
<div class="lyrico-lyrics-wrapper">ponathenna ne maayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponathenna ne maayam"/>
</div>
<div class="lyrico-lyrics-wrapper">epo pulla neyum vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="epo pulla neyum vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">enna konjuva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna konjuva"/>
</div>
<div class="lyrico-lyrics-wrapper">epo pulla neyum enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="epo pulla neyum enna"/>
</div>
<div class="lyrico-lyrics-wrapper">kaiyil alluva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaiyil alluva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ye pulla ye pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ye pulla ye pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">epa varuva love solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="epa varuva love solla"/>
</div>
<div class="lyrico-lyrics-wrapper">yen pulla enakulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yen pulla enakulla"/>
</div>
<div class="lyrico-lyrics-wrapper">kathal thantha nee mella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathal thantha nee mella"/>
</div>
</pre>