---
title: "chella kutty song lyrics"
album: "Vettai Naai"
artist: "Ganesh Chandrasekaran"
lyricist: "Ganesh Chandrasekaran"
director: "S. Jai Shankar"
path: "/albums/vettai-naai-lyrics"
song: "Chella Kutty"
image: ../../images/albumart/vettai-naai.jpg
date: 2021-02-26
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ZYriWFGa07Y"
type: "Love"
singers:
  - Anand Aravindakshan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Haiyoooo Enna paathuputala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haiyoooo Enna paathuputala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aalathan saachupuatala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalathan saachupuatala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjoram Kaadhal Koorudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjoram Kaadhal Koorudhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ade manasa alasiputala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ade manasa alasiputala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna solla theriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna solla theriyala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enaku onnum poriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enaku onnum poriyala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kirukan pola thiriyuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kirukan pola thiriyuren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kolhapathiladhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolhapathiladhan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna solla theriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna solla theriyala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enaku onnum poriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enaku onnum poriyala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kirukan pola thiriyuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kirukan pola thiriyuren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kolhapathiladhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolhapathiladhan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haiyoooo Enna paathuputala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haiyoooo Enna paathuputala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aalathan saachupuatala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalathan saachupuatala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjoram Kaadhal Koorudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjoram Kaadhal Koorudhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ade manasa alasiputala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ade manasa alasiputala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chella kutty needhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chella kutty needhan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En chella kutty needhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En chella kutty needhan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannu muthiyala purati podura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu muthiyala purati podura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chella kutty needhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chella kutty needhan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En chella kutty needhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En chella kutty needhan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konnom kuthi oram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konnom kuthi oram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna kaaya podura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna kaaya podura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Elale Elale Elala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Elale Elale Elala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Elale Elale Elala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Elale Elale Elala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Elale Elale Elala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Elale Elale Elala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Elale Elale Elala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Elale Elale Elala"/>
</div>
<div class="lyrico-lyrics-wrapper">Adeyathi nenjukul yenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adeyathi nenjukul yenu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kodiyethi kolluriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodiyethi kolluriye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Una paatha nodiyil dhinam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Una paatha nodiyil dhinam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaaasaaa…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaasaaa…"/>
</div>
<div class="lyrico-lyrics-wrapper">Usupethi enakul yeno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usupethi enakul yeno"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Usuragi nikiriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usuragi nikiriye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Udalaga enakul dhinam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udalaga enakul dhinam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koosa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koosa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyire en uyire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyire en uyire"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna katti podura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna katti podura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adiye yen adiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiye yen adiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhinam enna thakura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam enna thakura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alage en alage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alage en alage"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa unna theduren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa unna theduren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbae en anbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbae en anbae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unna aasapaduren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna aasapaduren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chella kutty needhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chella kutty needhan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En chella kutty needhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En chella kutty needhan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannu muthiyala purati podura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu muthiyala purati podura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chella kutty needhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chella kutty needhan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En chella kutty needhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En chella kutty needhan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konnom kuthi oram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konnom kuthi oram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna kaaya podura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna kaaya podura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhomdhom thana Dhomdhom thana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhomdhom thana Dhomdhom thana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thandhana dhumthana dhom 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandhana dhumthana dhom "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhomdhom thana Dhomdhom thana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhomdhom thana Dhomdhom thana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thandhana dhumthana dhom 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandhana dhumthana dhom "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhomdhom thana Dhomdhom thana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhomdhom thana Dhomdhom thana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thandhana dhumthana dhom 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandhana dhumthana dhom "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhomdhom thana Dhomdhom thana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhomdhom thana Dhomdhom thana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thandhana dhumthana dhom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandhana dhumthana dhom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chella kutty needhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chella kutty needhan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En chella kutty needhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En chella kutty needhan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannu muthiyala purati podura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu muthiyala purati podura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chella kutty needhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chella kutty needhan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En chella kutty needhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En chella kutty needhan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konnom kuthi oram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konnom kuthi oram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna kaaya podura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna kaaya podura"/>
</div>
</pre>
