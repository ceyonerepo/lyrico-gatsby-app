---
title: "kola kalle ilaa song lyrics"
album: "Varudu Kaavalenu"
artist: "Vishal Chandrashekhar"
lyricist: "Sirivennela Sitaramasastri"
director: "Lakshmi Sowjanya"
path: "/albums/varudu-kaavalenu-lyrics"
song: "Kola Kalle Ilaa"
image: ../../images/albumart/varudu-kaavalenu.jpg
date: 2021-10-15
lang: telugu
youtubeLink: "https://www.youtube.com/embed/VCtVrJLNR1c"
type: "love"
singers:
  - Sid Sriram
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Choopuley Na Gunde Anchullo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choopuley Na Gunde Anchullo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kunchelaa Needhe Bomma Geesthunnaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kunchelaa Needhe Bomma Geesthunnaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Puvvuula Na Oohal Gummamlo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puvvuula Na Oohal Gummamlo"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoranam Avuthu Nuvve Niluchunnaavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoranam Avuthu Nuvve Niluchunnaavey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konchemaina Ishtamenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konchemaina Ishtamenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aduguthundhey Mounangaa Naa Oopire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aduguthundhey Mounangaa Naa Oopire"/>
</div>
<div class="lyrico-lyrics-wrapper">Dooramunnaa Cheruvavuthuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dooramunnaa Cheruvavuthuu"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheppukundhey Naaloni Tondharey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheppukundhey Naaloni Tondharey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kola Kalle Ilaa Gunde Gille Yelaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kola Kalle Ilaa Gunde Gille Yelaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeli Mabbullo Neney Thelethalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeli Mabbullo Neney Thelethalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Konte Navvey Ilaa Champuthuntey Yelaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konte Navvey Ilaa Champuthuntey Yelaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kotha Rangullo Praanamey Thadisenthalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kotha Rangullo Praanamey Thadisenthalaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Malli Malli Raavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malli Malli Raavey"/>
</div>
<div class="lyrico-lyrics-wrapper">Poola Jallu Thevey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poola Jallu Thevey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nuvvelley Dharulalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvelley Dharulalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Chirugaaliki Parimalamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chirugaaliki Parimalamey"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhi Nanney Kammesthuu Vundhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhi Nanney Kammesthuu Vundhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa Kanti Reppalalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Kanti Reppalalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kunukulakika Kalavaramey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kunukulakika Kalavaramey"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhi Nanney Vedhisthu Vundhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhi Nanney Vedhisthu Vundhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nishinala Visuruthuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nishinala Visuruthuu"/>
</div>
<div class="lyrico-lyrics-wrapper">Shashi Nuvvai Meravagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shashi Nuvvai Meravagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mansulo Padhanisey Musugey Theesenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mansulo Padhanisey Musugey Theesenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Iruvuram Okarigaa Jathapadey Theerugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruvuram Okarigaa Jathapadey Theerugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana Kathe Malupune Korena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana Kathe Malupune Korena"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kola Kalle Ilaa Gunde Gille Yelaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kola Kalle Ilaa Gunde Gille Yelaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeli Mabbullo Neney Thelethalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeli Mabbullo Neney Thelethalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Konte Navvey Ilaa Champuthuntey Yelaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konte Navvey Ilaa Champuthuntey Yelaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kotha Rangullo Praanamey Thadisenthalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kotha Rangullo Praanamey Thadisenthalaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Malli Malli Raavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malli Malli Raavey"/>
</div>
<div class="lyrico-lyrics-wrapper">Poola Jallu Thevey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poola Jallu Thevey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Choopuley Na Gunde Anchullo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choopuley Na Gunde Anchullo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kunchelaa Needhe Bomma Geesthunnaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kunchelaa Needhe Bomma Geesthunnaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Na Na Na Hmm Hmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na Na Na Hmm Hmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Na Na Na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na Na Na"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Malli Malli Raavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malli Malli Raavey"/>
</div>
</pre>
