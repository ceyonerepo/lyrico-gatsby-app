---
title: "the voice without words song lyrics"
album: "99 Songs"
artist: "A.R. Rahman"
lyricist: "Navneet Virk"
director: "Vishwesh Krishnamoorthy"
path: "/albums/99-songs-lyrics"
song: "The Voice Without Words"
image: ../../images/albumart/99-songs.jpg
date: 2021-04-16
lang: tamil
youtubeLink: "https://www.youtube.com/embed/nwvYUhKK8q4"
type: "Melody"
singers:
  - A.R. Rahman
  - Poorvi Koutish
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">listen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="listen"/>
</div>
<div class="lyrico-lyrics-wrapper">do you hear it
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="do you hear it"/>
</div>
<div class="lyrico-lyrics-wrapper">do it, it was pressed to the trees
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="do it, it was pressed to the trees"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">do you hear it
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="do you hear it"/>
</div>
<div class="lyrico-lyrics-wrapper">the earth calls to the sky
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="the earth calls to the sky"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">do you hear it
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="do you hear it"/>
</div>
<div class="lyrico-lyrics-wrapper">the ocean sinks to the mud
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="the ocean sinks to the mud"/>
</div>
<div class="lyrico-lyrics-wrapper">do you hear it
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="do you hear it"/>
</div>
<div class="lyrico-lyrics-wrapper">there are voices that speak without words
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="there are voices that speak without words"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">listen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="listen"/>
</div>
<div class="lyrico-lyrics-wrapper">listen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="listen"/>
</div>
<div class="lyrico-lyrics-wrapper">listen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="listen"/>
</div>
<div class="lyrico-lyrics-wrapper">listen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="listen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">there are fliers that flew without flame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="there are fliers that flew without flame"/>
</div>
<div class="lyrico-lyrics-wrapper">such a burning
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="such a burning"/>
</div>
<div class="lyrico-lyrics-wrapper">such a  burning
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="such a  burning"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">listen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="listen"/>
</div>
<div class="lyrico-lyrics-wrapper">listen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="listen"/>
</div>
<div class="lyrico-lyrics-wrapper">sucha a burning is love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sucha a burning is love"/>
</div>
</pre>
