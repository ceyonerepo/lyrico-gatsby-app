---
title: 'uttradheenga yeppov song lyrics'
album: 'Karnan'
artist: 'Santhosh Narayanan'
lyricist: 'Mari Selvaraj'
director: 'Mari Selvaraj'
path: '/albums/karnan-song-lyrics'
song: 'Uttradheenga Yeppov'
image: ../../images/albumart/karnan.jpg
date: 2021-04-09
lang: tamil
youtubeLink: "https://www.youtube.com/embed/uH3bKP8og-o"
type: 'Sad'
singers: 
- Dhee
- Santhosh Narayanan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">

<div class="lyrico-lyrics-wrapper">Uttradheenga Yeppov
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uttradheenga Yeppov"/>
</div>
<div class="lyrico-lyrics-wrapper">Uttradheenga Yemmov
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uttradheenga Yemmov"/>
</div>
<div class="lyrico-lyrics-wrapper">Uttradheenga Yeppov
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uttradheenga Yeppov"/>
</div>
<div class="lyrico-lyrics-wrapper">Uttradheenga Yemmov
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uttradheenga Yemmov"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uttradheenga Yeppov
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uttradheenga Yeppov"/>
</div>
<div class="lyrico-lyrics-wrapper">Uttradheenga Yemmov
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uttradheenga Yemmov"/>
</div>
<div class="lyrico-lyrics-wrapper">Uttradheenga Yeppov Yemmov
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uttradheenga Yeppov Yemmov"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uttradheenga Uttradheenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uttradheenga Uttradheenga"/>
</div>
<div class="lyrico-lyrics-wrapper">Uttradheenga Uttradheenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uttradheenga Uttradheenga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thathaikka Puthaikka Thavalaisoru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thathaikka Puthaikka Thavalaisoru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ettu Erumai Eruma Paalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettu Erumai Eruma Paalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thookku Marathula Thunaiyakatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thookku Marathula Thunaiyakatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Thopi Potta Peyi Vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thopi Potta Peyi Vanthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kooppiduthu Kulaviduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooppiduthu Kulaviduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanga Mavane Bayapadathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanga Mavane Bayapadathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Chella Mavale Bayapadathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chella Mavale Bayapadathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadhi Kodiye Bayapadathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadhi Kodiye Bayapadathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uttradheenga Yeppov
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uttradheenga Yeppov"/>
</div>
<div class="lyrico-lyrics-wrapper">Uttradheenga Yemmov
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uttradheenga Yemmov"/>
</div>
<div class="lyrico-lyrics-wrapper">Uttradheenga Thaththaov
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uttradheenga Thaththaov"/>
</div>
<div class="lyrico-lyrics-wrapper">Uttradheenga Yachchov
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uttradheenga Yachchov"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Kattupechi Paaduren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Kattupechi Paaduren"/>
</div>
<div class="lyrico-lyrics-wrapper">Unga Kathukulla Kekkuthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unga Kathukulla Kekkuthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha Uchchimalaiyil Vaaduren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Uchchimalaiyil Vaaduren"/>
</div>
<div class="lyrico-lyrics-wrapper">Unga Ullungaiyil Theriyuthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unga Ullungaiyil Theriyuthaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Uttradheenga Yeppov
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Uttradheenga Yeppov"/>
</div>
<div class="lyrico-lyrics-wrapper">Uttradheenga Yemmov
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uttradheenga Yemmov"/>
</div>
<div class="lyrico-lyrics-wrapper">Uttradheenga Yennov
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uttradheenga Yennov"/>
</div>
<div class="lyrico-lyrics-wrapper">Uttradheenga Yakkov
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uttradheenga Yakkov"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uttradheenga Uttradheenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uttradheenga Uttradheenga"/>
</div>
<div class="lyrico-lyrics-wrapper">Uttradheenga Uttradheenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uttradheenga Uttradheenga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho Ho..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho Ho.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uttradheenga Uttradheenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uttradheenga Uttradheenga"/>
</div>
<div class="lyrico-lyrics-wrapper">Uttradheenga Uttradheenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uttradheenga Uttradheenga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Annaa Varaan Innaa Varaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annaa Varaan Innaa Varaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaakkisattai Kaandaan Varaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaakkisattai Kaandaan Varaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Katteruthu Mela Varaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katteruthu Mela Varaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kandavana Adika Varaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandavana Adika Varaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanavaiyellam Posuka Varaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavaiyellam Posuka Varaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanga Mavane Bayappadathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanga Mavane Bayappadathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Chella Mavale Bayappadathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chella Mavale Bayappadathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadhi Kodiye Bayappadathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadhi Kodiye Bayappadathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanga Thera Marichchi Aaduvvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanga Thera Marichchi Aaduvvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Yerum Paithaavellam Niruththunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yerum Paithaavellam Niruththunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Oor Ulagam Suththa Povvrom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oor Ulagam Suththa Povvrom"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga Rekkaiyenga Kelunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Rekkaiyenga Kelunga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uttradheenga Yeppov
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uttradheenga Yeppov"/>
</div>
<div class="lyrico-lyrics-wrapper">Uttradheenga Yemmov
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uttradheenga Yemmov"/>
</div>
<div class="lyrico-lyrics-wrapper">Uttradheenga Thaththaov
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uttradheenga Thaththaov"/>
</div>
<div class="lyrico-lyrics-wrapper">Uttradheenga Yachchov
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uttradheenga Yachchov"/>
</div>
<div class="lyrico-lyrics-wrapper">Uttradheenga Yennov
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uttradheenga Yennov"/>
</div>
<div class="lyrico-lyrics-wrapper">Uttradheenga Yakkov
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uttradheenga Yakkov"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uttradheenga Uttradheenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uttradheenga Uttradheenga"/>
</div>
<div class="lyrico-lyrics-wrapper">Uttradheenga Uttradheenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uttradheenga Uttradheenga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uttradheenga Yeppov
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uttradheenga Yeppov"/>
</div>
<div class="lyrico-lyrics-wrapper">Uttradheenga Yemmov
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uttradheenga Yemmov"/>
</div>
<div class="lyrico-lyrics-wrapper">Uttradheenga Thaththaov
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uttradheenga Thaththaov"/>
</div>
<div class="lyrico-lyrics-wrapper">Uttradheenga Yachchov
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uttradheenga Yachchov"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Ho Ho…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Ho Ho…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uttradheenga Uttradheenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uttradheenga Uttradheenga"/>
</div>
<div class="lyrico-lyrics-wrapper">Uttradheenga Uttradheenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uttradheenga Uttradheenga"/>
</div>
</pre>