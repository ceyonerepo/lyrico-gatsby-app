---
title: "so baby song lyrics"
album: "Doctor"
artist: "Anirudh Ravichander"
lyricist: "Sivakarthikeyan"
director: "Nelson Dilipkumar"
path: "/albums/doctor-lyrics"
song: "So Baby"
image: ../../images/albumart/doctor.jpg
date: 2021-10-09
lang: tamil
youtubeLink: "https://www.youtube.com/embed/fOyGnZPy0vo"
type: "love"
singers:
  - Anirudh Ravichander
  - Ananthakkrishnan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Awal mugam parthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Awal mugam parthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan intru thadumarine
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan intru thadumarine"/>
</div>
<div class="lyrico-lyrics-wrapper">Nizhal thoda verthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nizhal thoda verthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir konda silai aaguren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir konda silai aaguren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Awal mugam parthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Awal mugam parthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan intru thadumarine
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan intru thadumarine"/>
</div>
<div class="lyrico-lyrics-wrapper">Nizhal thoda verthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nizhal thoda verthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir konda silai aaguren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir konda silai aaguren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">So baby don’t you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="So baby don’t you"/>
</div>
<div class="lyrico-lyrics-wrapper">Break break my
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Break break my"/>
</div>
<div class="lyrico-lyrics-wrapper">Break beak my heart
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Break beak my heart"/>
</div>
<div class="lyrico-lyrics-wrapper">So baby don’t you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="So baby don’t you"/>
</div>
<div class="lyrico-lyrics-wrapper">Break break my
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Break break my"/>
</div>
<div class="lyrico-lyrics-wrapper">Break beak my heart
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Break beak my heart"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Don’t you dare break my heart
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Don’t you dare break my heart"/>
</div>
<div class="lyrico-lyrics-wrapper">Break my heart break my heart
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Break my heart break my heart"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">So baby don’t you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="So baby don’t you"/>
</div>
<div class="lyrico-lyrics-wrapper">Break break my
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Break break my"/>
</div>
<div class="lyrico-lyrics-wrapper">Break beak my heart
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Break beak my heart"/>
</div>
<div class="lyrico-lyrics-wrapper">So baby don’t you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="So baby don’t you"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithuwarai partha pennil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithuwarai partha pennil"/>
</div>
<div class="lyrico-lyrics-wrapper">Unayi pola enayi yaarume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unayi pola enayi yaarume"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagale kondru thindru uyir meetu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagale kondru thindru uyir meetu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thara villaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thara villaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ithuwarai partha pennil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithuwarai partha pennil"/>
</div>
<div class="lyrico-lyrics-wrapper">Unayi pola enayi yaarume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unayi pola enayi yaarume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhagale kondru thindru uyir meetu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagale kondru thindru uyir meetu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thara villaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thara villaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnoda nadakkintra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnoda nadakkintra"/>
</div>
<div class="lyrico-lyrics-wrapper">Siru dhooram kidaikkintra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru dhooram kidaikkintra"/>
</div>
<div class="lyrico-lyrics-wrapper">Sila neram en vazhvin varamagave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sila neram en vazhvin varamagave"/>
</div>
<div class="lyrico-lyrics-wrapper">Iravellam unai thedi alaikintren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravellam unai thedi alaikintren"/>
</div>
<div class="lyrico-lyrics-wrapper">Kidaiththum naan tholaigintren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kidaiththum naan tholaigintren"/>
</div>
<div class="lyrico-lyrics-wrapper">Puyal kaatril vilakaagave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puyal kaatril vilakaagave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">So baby don’t you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="So baby don’t you"/>
</div>
<div class="lyrico-lyrics-wrapper">Break break my
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Break break my"/>
</div>
<div class="lyrico-lyrics-wrapper">Break beak my heart
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Break beak my heart"/>
</div>
<div class="lyrico-lyrics-wrapper">So baby don’t you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="So baby don’t you"/>
</div>
<div class="lyrico-lyrics-wrapper">Break break my
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Break break my"/>
</div>
<div class="lyrico-lyrics-wrapper">Break beak my heart
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Break beak my heart"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Don’t you dare break my heart
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Don’t you dare break my heart"/>
</div>
<div class="lyrico-lyrics-wrapper">Break my heart break my heart
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Break my heart break my heart"/>
</div>
<div class="lyrico-lyrics-wrapper">So baby don’t you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="So baby don’t you"/>
</div>
<div class="lyrico-lyrics-wrapper">Break break my
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Break break my"/>
</div>
<div class="lyrico-lyrics-wrapper">Break beak my heart
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Break beak my heart"/>
</div>
<div class="lyrico-lyrics-wrapper">So baby don’t you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="So baby don’t you"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">So baby don’t you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="So baby don’t you"/>
</div>
<div class="lyrico-lyrics-wrapper">Break break my
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Break break my"/>
</div>
<div class="lyrico-lyrics-wrapper">Break beak my heart
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Break beak my heart"/>
</div>
<div class="lyrico-lyrics-wrapper">So baby don’t you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="So baby don’t you"/>
</div>
<div class="lyrico-lyrics-wrapper">Break break my
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Break break my"/>
</div>
<div class="lyrico-lyrics-wrapper">Break beak my heart
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Break beak my heart"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Don’t you dare break my heart
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Don’t you dare break my heart"/>
</div>
<div class="lyrico-lyrics-wrapper">Break my heart break my heart
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Break my heart break my heart"/>
</div>
<div class="lyrico-lyrics-wrapper">So baby don’t you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="So baby don’t you"/>
</div>
<div class="lyrico-lyrics-wrapper">Break break my
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Break break my"/>
</div>
<div class="lyrico-lyrics-wrapper">Break beak my heart
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Break beak my heart"/>
</div>
<div class="lyrico-lyrics-wrapper">So baby don’t you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="So baby don’t you"/>
</div>
</pre>
