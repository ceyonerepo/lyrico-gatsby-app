---
title: "en vennilave song lyrics"
album: "Aadukalam"
artist: "G. V. Prakash Kumar"
lyricist: "Yugabharathi"
director: "V. I. S. Jayapalan"
path: "/albums/aadukalam-lyrics"
song: "En Vennilave"
image: ../../images/albumart/aadukalam.jpg
date: 2011-01-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ZFi56Y9YaFI"
type: "sad"
singers:
  - KK
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">En Vennilave Erikkadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Vennilave Erikkadhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Kanavugalai Sithaikkadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kanavugalai Sithaikkadhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Vennilave Erikkadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Vennilave Erikkadhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Kanavugalai Sithaikkadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kanavugalai Sithaikkadhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnil Padarndha En Uyirai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnil Padarndha En Uyirai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mannil Yeno Veesivittaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannil Yeno Veesivittaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mannil Yeno Veesivittaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannil Yeno Veesivittaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Vennilave Erikkadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Vennilave Erikkadhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Kanavugalai Sithaikkadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kanavugalai Sithaikkadhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paniyil Ilaiyatra Thani Maram Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paniyil Ilaiyatra Thani Maram Naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paalaiyil Thudithidum Siru Pulu Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paalaiyil Thudithidum Siru Pulu Naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhal Devadhai Pol Vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Devadhai Pol Vandhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thalaipali Ketpathu Mohiniyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaipali Ketpathu Mohiniyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Yen Erithaai Meenaachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Yen Erithaai Meenaachi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Nizhalil Vaazhum Madhuraiyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Nizhalil Vaazhum Madhuraiyadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mazhaiyaai Thanalaanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhaiyaai Thanalaanen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mannil Eno Veesivittaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannil Eno Veesivittaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kallipaalai Ootrivittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallipaalai Ootrivittu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Velli Nilavaai Ponavaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velli Nilavaai Ponavaley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennil Valarththu Porchiragu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennil Valarththu Porchiragu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Odinthida Nadandhidum Kodum Puyaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odinthida Nadandhidum Kodum Puyaley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhagarai Thottadhaal Vaigai Nadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagarai Thottadhaal Vaigai Nadhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alaigadal Seraa Madhuraiyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alaigadal Seraa Madhuraiyadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Vizhivaal Muzhivaal Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Vizhivaal Muzhivaal Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mannil Yeno Veesivittai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannil Yeno Veesivittai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Vennilave Erikkadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Vennilave Erikkadhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Kanavugalai Sithaikkadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kanavugalai Sithaikkadhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnil Padarndha En Uyirai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnil Padarndha En Uyirai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mannil Yeno Veesivittaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannil Yeno Veesivittaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mannil Yeno Veesivittaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannil Yeno Veesivittaai"/>
</div>
</pre>
