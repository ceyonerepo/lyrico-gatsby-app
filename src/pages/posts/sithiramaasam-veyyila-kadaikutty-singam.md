---
title: "sithiramaasam veyyila song lyrics"
album: "Kadaikutty Singam"
artist: "D. Imman"
lyricist: "Mahalingam"
director: "Pandiraj"
path: "/albums/kadaikutty-singam-lyrics"
song: "Sithiramaasam Veyyila"
image: ../../images/albumart/kadaikutty-singam.jpg
date: 2018-07-13
lang: tamil
youtubeLink: "https://www.youtube.com/embed/gaglZCWkA7U"
type: "love"
singers:
  - Mahalingam
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Sithiramaasam veiyila un paarvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sithiramaasam veiyila un paarvai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai suttu erichi kolluthadi aala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai suttu erichi kolluthadi aala"/>
</div>
<div class="lyrico-lyrics-wrapper">Margazhi maatham kulira ippo aava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Margazhi maatham kulira ippo aava"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai marbodu serthu anaichi serva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai marbodu serthu anaichi serva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aththa maga rathiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aththa maga rathiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un azhagula naan alaiyiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un azhagula naan alaiyiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Oththa solla ketka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oththa solla ketka"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan niththam suththi kolaiyiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan niththam suththi kolaiyiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aththa maga rathiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aththa maga rathiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un azhagula naan alaiyiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un azhagula naan alaiyiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Oththa solla ketka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oththa solla ketka"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan niththam suththi kolaiyiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan niththam suththi kolaiyiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yelelo yelo yele yelelo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yelelo yelo yele yelelo"/>
</div>
<div class="lyrico-lyrics-wrapper">En meethu unthan paarvai padatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En meethu unthan paarvai padatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Yelelo yelo yele yelelo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yelelo yelo yele yelelo"/>
</div>
<div class="lyrico-lyrics-wrapper">En meethu unthan paarvai padatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En meethu unthan paarvai padatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Sithiramaasam adi sithirimaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sithiramaasam adi sithirimaasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sithiramaasam veiyila un paarvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sithiramaasam veiyila un paarvai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai suttu erichi kolluthadi aala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai suttu erichi kolluthadi aala"/>
</div>
<div class="lyrico-lyrics-wrapper">Margazhi maatham kulira ippo aava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Margazhi maatham kulira ippo aava"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai marbodu serthu anaichi serva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai marbodu serthu anaichi serva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aththa maga rathiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aththa maga rathiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un azhagula naan alaiyiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un azhagula naan alaiyiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Oththa solla ketka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oththa solla ketka"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan niththam suththi kolaiyiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan niththam suththi kolaiyiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aththa maga rathiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aththa maga rathiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un azhagula naan alaiyiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un azhagula naan alaiyiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Oththa solla ketka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oththa solla ketka"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan niththam suththi kolaiyiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan niththam suththi kolaiyiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yelelo yelo yele yelelo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yelelo yelo yele yelelo"/>
</div>
<div class="lyrico-lyrics-wrapper">En meethu unthan paarvai padatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En meethu unthan paarvai padatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Yelelo yelo yele yelelo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yelelo yelo yele yelelo"/>
</div>
<div class="lyrico-lyrics-wrapper">En meethu unthan paarvai padathooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En meethu unthan paarvai padathooo"/>
</div>
</pre>
