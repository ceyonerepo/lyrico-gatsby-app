---
title: "aazhi soozhndha song lyrics"
album: "Sivappu Manjal Pachai"
artist: "Siddhu Kumar"
lyricist: "Mohan Rajan"
director: "Sasi"
path: "/albums/sivappu-manjal-pachai-lyrics"
song: "Aazhi Soozhndha"
image: ../../images/albumart/sivappu-manjal-pachai.jpg
date: 2019-09-06
lang: tamil
youtubeLink: "https://www.youtube.com/embed/UVYApsOcdtc"
type: "happy"
singers:
  - Sreekanth Hariharan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aazhi Soozhndha Ulagilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aazhi Soozhndha Ulagilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaavum Alagaachae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaavum Alagaachae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vayadhai Meeriya Vaazhvilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayadhai Meeriya Vaazhvilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Siru Kavidhai Uruvaachae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru Kavidhai Uruvaachae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Viralinai Thaandidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viralinai Thaandidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nagamena Ivan Paasamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nagamena Ivan Paasamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kireedamaa Baaramaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kireedamaa Baaramaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Puriyumaa Sila Neramae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puriyumaa Sila Neramae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivan Annan Paadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Annan Paadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thandhai Meedhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandhai Meedhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanaanae Aanaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanaanae Aanaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thambhi Endra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thambhi Endra"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilaiyai Kadanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilaiyai Kadanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponaanae Ponaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponaanae Ponaanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aazhi Soozhndha Ulagilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aazhi Soozhndha Ulagilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaavum Alagaachae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaavum Alagaachae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vayadhai Meeriya Vaazhvilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayadhai Meeriya Vaazhvilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Siru Kavidhai Uruvaachae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru Kavidhai Uruvaachae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Urangum Bodhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urangum Bodhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivanin Gavanam Urangi Pogaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivanin Gavanam Urangi Pogaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavil Kooda Kaaval Seiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavil Kooda Kaaval Seiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadamai Maravaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadamai Maravaadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ulagamae Ivalena Ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagamae Ivalena Ivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhum Azhagai Paaradaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhum Azhagai Paaradaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Magal Yena Valarkkiraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Magal Yena Valarkkiraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan Uyaram Kuraindha Thaaiyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Uyaram Kuraindha Thaaiyada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivanin Anbai Alanthida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivanin Anbai Alanthida"/>
</div>
<div class="lyrico-lyrics-wrapper">Endha Mozhiyum Pothaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endha Mozhiyum Pothaathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivan Annan Paadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Annan Paadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thandhai Meedhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandhai Meedhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanaanae Aanaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanaanae Aanaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thambhi Endra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thambhi Endra"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilaiyai Kadanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilaiyai Kadanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponaanae Ponaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponaanae Ponaanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aazhi Soozhndha Ulagilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aazhi Soozhndha Ulagilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaavum Alagaachae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaavum Alagaachae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vayadhai Meeriya Vaazhvilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayadhai Meeriya Vaazhvilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Siru Kavidhai Uruvaachae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru Kavidhai Uruvaachae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Viralinai Thaandidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viralinai Thaandidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nagamena Ivan Paasamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nagamena Ivan Paasamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kireedamaa Baaramaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kireedamaa Baaramaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Puriyumaa Sila Neramae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puriyumaa Sila Neramae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivan Annan Paadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Annan Paadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thandhai Meedhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandhai Meedhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanaanae Aanaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanaanae Aanaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thambhi Endra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thambhi Endra"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilaiyai Kadanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilaiyai Kadanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponaanae Ponaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponaanae Ponaanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aazhi Soozhndha Ulagilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aazhi Soozhndha Ulagilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaavum Alagaachae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaavum Alagaachae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vayadhai Meeriya Vaazhvilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayadhai Meeriya Vaazhvilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Siru Kavidhai Uruvaachae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru Kavidhai Uruvaachae"/>
</div>
</pre>
