---
title: 'paisa note song lyrics'
album: 'Comali'
artist: 'Hiphop Tamizha'
lyricist: 'Hiphop Tamizha'
director: 'Pradeep Ranganathan'
path: '/albums/comali-song-lyrics'
song: 'Paisa Note'
image: ../../images/albumart/comali.jpg
date: 2019-08-15
lang: tamil
youtubeLink: "https://www.youtube.com/embed/lPg2iuMAdLc"
type: 'love'
singers: 
- Hiphop Tamizha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Paisa note ah uththu paathen
<input type="checkbox" class="lyrico-select-lyric-line" value="Paisa note ah uththu paathen"/>
</div>
<div class="lyrico-lyrics-wrapper">Gandhi-ya dhaan kaanom
<input type="checkbox" class="lyrico-select-lyric-line" value="Gandhi-ya dhaan kaanom"/>
</div>
<div class="lyrico-lyrics-wrapper">Un mugam dhaan theriyuthu
<input type="checkbox" class="lyrico-select-lyric-line" value="Un mugam dhaan theriyuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna panna naanum
<input type="checkbox" class="lyrico-select-lyric-line" value="Enna panna naanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kovilukkul poyi paarthen
<input type="checkbox" class="lyrico-select-lyric-line" value="Kovilukkul poyi paarthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Samiya dhaan kaanum
<input type="checkbox" class="lyrico-select-lyric-line" value="Samiya dhaan kaanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Saami selai pol irukkum
<input type="checkbox" class="lyrico-select-lyric-line" value="Saami selai pol irukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee mattum dhaan venum
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee mattum dhaan venum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Enakku nee mattum dhaan venum
<input type="checkbox" class="lyrico-select-lyric-line" value="Enakku nee mattum dhaan venum"/>
</div>
<div class="lyrico-lyrics-wrapper">Indhaadi paalu pazham theanum
<input type="checkbox" class="lyrico-select-lyric-line" value="Indhaadi paalu pazham theanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Karpooram yethuven dee naanum
<input type="checkbox" class="lyrico-select-lyric-line" value="Karpooram yethuven dee naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee venum enakku ovvoru naalum
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee venum enakku ovvoru naalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Are u ready
<input type="checkbox" class="lyrico-select-lyric-line" value="Are u ready"/>
</div>
<div class="lyrico-lyrics-wrapper">Are u ready
<input type="checkbox" class="lyrico-select-lyric-line" value="Are u ready"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Theanu mittai lipu-kku
<input type="checkbox" class="lyrico-select-lyric-line" value="Theanu mittai lipu-kku"/>
</div>
<div class="lyrico-lyrics-wrapper">Thevai ella lip stick-u
<input type="checkbox" class="lyrico-select-lyric-line" value="Thevai ella lip stick-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Vennillavu eye-kku
<input type="checkbox" class="lyrico-select-lyric-line" value="Vennillavu eye-kku"/>
</div>
<div class="lyrico-lyrics-wrapper">Venamadi eyetex-uu….
<input type="checkbox" class="lyrico-select-lyric-line" value="Venamadi eyetex-uu…."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thaaru maaru range-la
<input type="checkbox" class="lyrico-select-lyric-line" value="Thaaru maaru range-la"/>
</div>
<div class="lyrico-lyrics-wrapper">Vechiruken nenjula
<input type="checkbox" class="lyrico-select-lyric-line" value="Vechiruken nenjula"/>
</div>
<div class="lyrico-lyrics-wrapper">Vera level azhagula
<input type="checkbox" class="lyrico-select-lyric-line" value="Vera level azhagula"/>
</div>
<div class="lyrico-lyrics-wrapper">Paakuren dee kannula
<input type="checkbox" class="lyrico-select-lyric-line" value="Paakuren dee kannula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kaadhal tholviya
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaadhal tholviya"/>
</div>
<div class="lyrico-lyrics-wrapper">Parthavan dee naanu
<input type="checkbox" class="lyrico-select-lyric-line" value="Parthavan dee naanu"/>
</div>
<div class="lyrico-lyrics-wrapper">First love-ula
<input type="checkbox" class="lyrico-select-lyric-line" value="First love-ula"/>
</div>
<div class="lyrico-lyrics-wrapper">Thotthavan dee naanu
<input type="checkbox" class="lyrico-select-lyric-line" value="Thotthavan dee naanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Love-kaaga enguren dee naanu
<input type="checkbox" class="lyrico-select-lyric-line" value="Love-kaaga enguren dee naanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullangkaiyil thaangiduvenae
<input type="checkbox" class="lyrico-select-lyric-line" value="Ullangkaiyil thaangiduvenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ada sathiyama solluren
<input type="checkbox" class="lyrico-select-lyric-line" value="Ada sathiyama solluren"/>
</div>
<div class="lyrico-lyrics-wrapper">En mela sathiyama solluren
<input type="checkbox" class="lyrico-select-lyric-line" value="En mela sathiyama solluren"/>
</div>
<div class="lyrico-lyrics-wrapper">Un mela sathiyama solluren
<input type="checkbox" class="lyrico-select-lyric-line" value="Un mela sathiyama solluren"/>
</div>
  <div class="lyrico-lyrics-wrapper">Seekiram thaan solli thola
<input type="checkbox" class="lyrico-select-lyric-line" value="Seekiram thaan solli thola"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaya yen da mellura
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaaya yen da mellura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Andha maari indha maari
<input type="checkbox" class="lyrico-select-lyric-line" value="Andha maari indha maari"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna maari yaarum illa
<input type="checkbox" class="lyrico-select-lyric-line" value="Unna maari yaarum illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna maari enna maari
<input type="checkbox" class="lyrico-select-lyric-line" value="Unna maari enna maari"/>
</div>
<div class="lyrico-lyrics-wrapper">Jodi illa oorukkula
<input type="checkbox" class="lyrico-select-lyric-line" value="Jodi illa oorukkula"/>
</div>
<div class="lyrico-lyrics-wrapper">Vera maari aachu pulla
<input type="checkbox" class="lyrico-select-lyric-line" value="Vera maari aachu pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaam maari pochu ulla
<input type="checkbox" class="lyrico-select-lyric-line" value="Ellaam maari pochu ulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee mattum thaan venum
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee mattum thaan venum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vera yaarum thevai illa
<input type="checkbox" class="lyrico-select-lyric-line" value="Vera yaarum thevai illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Paisa note ah uththu paathen
<input type="checkbox" class="lyrico-select-lyric-line" value="Paisa note ah uththu paathen"/>
</div>
<div class="lyrico-lyrics-wrapper">Gandhi-ya dhaan kaanom
<input type="checkbox" class="lyrico-select-lyric-line" value="Gandhi-ya dhaan kaanom"/>
</div>
<div class="lyrico-lyrics-wrapper">Un mugam dhaan theriyuthu
<input type="checkbox" class="lyrico-select-lyric-line" value="Un mugam dhaan theriyuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna panna naanum
<input type="checkbox" class="lyrico-select-lyric-line" value="Enna panna naanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kovilukkul poyi paarthen
<input type="checkbox" class="lyrico-select-lyric-line" value="Kovilukkul poyi paarthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Samiya dhaan kaanum
<input type="checkbox" class="lyrico-select-lyric-line" value="Samiya dhaan kaanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Saami selai pol irukkum
<input type="checkbox" class="lyrico-select-lyric-line" value="Saami selai pol irukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee mattum dhaan venum
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee mattum dhaan venum"/>
</div>
  <div class="lyrico-lyrics-wrapper">Nee mattum dhaan venum
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee mattum dhaan venum"/>
</div>
  <div class="lyrico-lyrics-wrapper">Enakku nee mattum dhaan venum
<input type="checkbox" class="lyrico-select-lyric-line" value="Enakku nee mattum dhaan venum"/>
</div>
  <div class="lyrico-lyrics-wrapper">Enakku nee mattum dhaan venum
<input type="checkbox" class="lyrico-select-lyric-line" value="Enakku nee mattum dhaan venum"/>
</div>
  <div class="lyrico-lyrics-wrapper">Indhaadi paalu pazham theanum
<input type="checkbox" class="lyrico-select-lyric-line" value="Indhaadi paalu pazham theanum"/>
</div>
  <div class="lyrico-lyrics-wrapper">Indhaadi paalu pazham theanum
<input type="checkbox" class="lyrico-select-lyric-line" value="Indhaadi paalu pazham theanum"/>
</div>
  <div class="lyrico-lyrics-wrapper">Karpooram yethuven dee naanum
<input type="checkbox" class="lyrico-select-lyric-line" value="Karpooram yethuven dee naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee venum enakku ovvoru naalum
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee venum enakku ovvoru naalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Andha maari indha maari
<input type="checkbox" class="lyrico-select-lyric-line" value="Andha maari indha maari"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna maari yaarum illa
<input type="checkbox" class="lyrico-select-lyric-line" value="Unna maari yaarum illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna maari enna maari
<input type="checkbox" class="lyrico-select-lyric-line" value="Unna maari enna maari"/>
</div>
<div class="lyrico-lyrics-wrapper">Jodi illa oorukkula
<input type="checkbox" class="lyrico-select-lyric-line" value="Jodi illa oorukkula"/>
</div>
<div class="lyrico-lyrics-wrapper">Vera maari aachu pulla
<input type="checkbox" class="lyrico-select-lyric-line" value="Vera maari aachu pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaam maari pochu ulla
<input type="checkbox" class="lyrico-select-lyric-line" value="Ellaam maari pochu ulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee mattum thaan venum
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee mattum thaan venum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vera yaarum thevai illa
<input type="checkbox" class="lyrico-select-lyric-line" value="Vera yaarum thevai illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Nee venum enakku
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee venum enakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Ovvoru naalum
<input type="checkbox" class="lyrico-select-lyric-line" value="Ovvoru naalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee venum enakku
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee venum enakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Ovvoru naalum
<input type="checkbox" class="lyrico-select-lyric-line" value="Ovvoru naalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee venum enakku
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee venum enakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Ovvoru naalum
<input type="checkbox" class="lyrico-select-lyric-line" value="Ovvoru naalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee venum enakku
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee venum enakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Ovvoru naalum
<input type="checkbox" class="lyrico-select-lyric-line" value="Ovvoru naalum"/>
</div>
</pre>