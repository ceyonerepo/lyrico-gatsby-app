---
title: "karuva paiya song lyrics"
album: "Nari Vettai"
artist: "Charles Dhana"
lyricist: "Akash Sudhakar"
director: "Akash Sudhakar"
path: "/albums/nari-vettai-lyrics"
song: "Karuva Paiya"
image: ../../images/albumart/nari-vettai.jpg
date: 2018-02-09
lang: tamil
youtubeLink: "https://www.youtube.com/embed/3O-gHu-TkVM"
type: "love"
singers:
  - Charlesdhana
  - Janaki Iyer
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">ada karuva paiya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada karuva paiya "/>
</div>
<div class="lyrico-lyrics-wrapper">enna ethuka da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna ethuka da"/>
</div>
<div class="lyrico-lyrics-wrapper">un aruva meesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un aruva meesa"/>
</div>
<div class="lyrico-lyrics-wrapper">enna sollama kolluthada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna sollama kolluthada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ada karuva paiya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada karuva paiya "/>
</div>
<div class="lyrico-lyrics-wrapper">enna ethuka da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna ethuka da"/>
</div>
<div class="lyrico-lyrics-wrapper">un aruva meesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un aruva meesa"/>
</div>
<div class="lyrico-lyrics-wrapper">enna sollama kolluthada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna sollama kolluthada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ithu kadhal alla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu kadhal alla"/>
</div>
<div class="lyrico-lyrics-wrapper">konjam kamam alla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konjam kamam alla"/>
</div>
<div class="lyrico-lyrics-wrapper">un ulaga thandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un ulaga thandi"/>
</div>
<div class="lyrico-lyrics-wrapper">etho onnu kolluthu mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="etho onnu kolluthu mama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ithu kadhal alla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu kadhal alla"/>
</div>
<div class="lyrico-lyrics-wrapper">konjam kamam alla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konjam kamam alla"/>
</div>
<div class="lyrico-lyrics-wrapper">un ulaga thandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un ulaga thandi"/>
</div>
<div class="lyrico-lyrics-wrapper">etho onnu kolluthu mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="etho onnu kolluthu mama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ada karuva paiya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada karuva paiya "/>
</div>
<div class="lyrico-lyrics-wrapper">enna ethuka da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna ethuka da"/>
</div>
<div class="lyrico-lyrics-wrapper">un aruva meesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un aruva meesa"/>
</div>
<div class="lyrico-lyrics-wrapper">enna sollama kolluthada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna sollama kolluthada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">entha mozhiyila solluven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="entha mozhiyila solluven"/>
</div>
<div class="lyrico-lyrics-wrapper">en idhaya kadhala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en idhaya kadhala"/>
</div>
<div class="lyrico-lyrics-wrapper">ada ovoru nodiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada ovoru nodiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">ovoru yugama maruthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ovoru yugama maruthe"/>
</div>
<div class="lyrico-lyrics-wrapper">iravum pagalum maranthu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iravum pagalum maranthu "/>
</div>
<div class="lyrico-lyrics-wrapper">pochu nenapula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pochu nenapula"/>
</div>
<div class="lyrico-lyrics-wrapper">un nenapula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un nenapula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">unna partha mudhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna partha mudhal"/>
</div>
<div class="lyrico-lyrics-wrapper">idhayame tholanju pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhayame tholanju pochu"/>
</div>
<div class="lyrico-lyrics-wrapper">una rasika thodangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="una rasika thodangi"/>
</div>
<div class="lyrico-lyrics-wrapper">ennaye maranthu pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennaye maranthu pochu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ada karuva paiya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada karuva paiya "/>
</div>
<div class="lyrico-lyrics-wrapper">enna ethuka da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna ethuka da"/>
</div>
<div class="lyrico-lyrics-wrapper">un aruva meesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un aruva meesa"/>
</div>
<div class="lyrico-lyrics-wrapper">enna sollama kolluthada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna sollama kolluthada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thedi thedi na tholainthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thedi thedi na tholainthen"/>
</div>
<div class="lyrico-lyrics-wrapper">unna parthathum kuranjen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna parthathum kuranjen"/>
</div>
<div class="lyrico-lyrics-wrapper">ne nallavana kettavana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ne nallavana kettavana"/>
</div>
<div class="lyrico-lyrics-wrapper">theriyala unna thavira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theriyala unna thavira"/>
</div>
<div class="lyrico-lyrics-wrapper">nilal kooda illada 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nilal kooda illada "/>
</div>
<div class="lyrico-lyrics-wrapper">ada illada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada illada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">un verva thuliyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un verva thuliyil"/>
</div>
<div class="lyrico-lyrics-wrapper">nanaiya aasa patten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanaiya aasa patten"/>
</div>
<div class="lyrico-lyrics-wrapper">un madiyil thavala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un madiyil thavala"/>
</div>
<div class="lyrico-lyrics-wrapper">kulanthaya maruvene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulanthaya maruvene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ada karuva paiya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada karuva paiya "/>
</div>
<div class="lyrico-lyrics-wrapper">enna ethuka da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna ethuka da"/>
</div>
<div class="lyrico-lyrics-wrapper">un aruva meesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un aruva meesa"/>
</div>
<div class="lyrico-lyrics-wrapper">enna sollama kolluthada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna sollama kolluthada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ada karuva paiya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada karuva paiya "/>
</div>
<div class="lyrico-lyrics-wrapper">enna ethuka da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna ethuka da"/>
</div>
<div class="lyrico-lyrics-wrapper">un aruva meesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un aruva meesa"/>
</div>
<div class="lyrico-lyrics-wrapper">enna sollama kolluthada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna sollama kolluthada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ithu kadhal alla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu kadhal alla"/>
</div>
<div class="lyrico-lyrics-wrapper">konjam kamam alla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konjam kamam alla"/>
</div>
<div class="lyrico-lyrics-wrapper">un ulaga thandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un ulaga thandi"/>
</div>
<div class="lyrico-lyrics-wrapper">etho onnu kolluthu mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="etho onnu kolluthu mama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ithu kadhal alla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu kadhal alla"/>
</div>
<div class="lyrico-lyrics-wrapper">konjam kamam alla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konjam kamam alla"/>
</div>
<div class="lyrico-lyrics-wrapper">un ulaga thandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un ulaga thandi"/>
</div>
<div class="lyrico-lyrics-wrapper">etho onnu kolluthu mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="etho onnu kolluthu mama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ada karuva paiya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada karuva paiya "/>
</div>
<div class="lyrico-lyrics-wrapper">enna ethuka da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna ethuka da"/>
</div>
<div class="lyrico-lyrics-wrapper">un aruva meesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un aruva meesa"/>
</div>
<div class="lyrico-lyrics-wrapper">enna sollama kolluthada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna sollama kolluthada"/>
</div>
</pre>
