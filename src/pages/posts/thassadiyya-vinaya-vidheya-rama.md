---
title: "thassadiyya song lyrics"
album: "Vinaya Vidheya Rama"
artist: "Devi Sri Prasad"
lyricist: "Sri Mani"
director: "Boyapati Srinu"
path: "/albums/vinaya-vidheya-rama-lyrics"
song: "Thassadiyya"
image: ../../images/albumart/vinaya-vidheya-rama.jpg
date: 2019-01-11
lang: telugu
youtubeLink: "https://www.youtube.com/embed/gbJipp2XASc"
type: "love"
singers:
  - Jaspreet Jasz
  - M.M. Manasi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Sunn mere maahiya ve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sunn mere maahiya ve"/>
</div>
<div class="lyrico-lyrics-wrapper">mahiya ve Ho mahiya ve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mahiya ve Ho mahiya ve"/>
</div>
<div class="lyrico-lyrics-wrapper">Tere sang rehna ve jeena ve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere sang rehna ve jeena ve"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho janiya ve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho janiya ve"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sunn mere maahiya ve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sunn mere maahiya ve"/>
</div>
<div class="lyrico-lyrics-wrapper">mahiya ve Ho mahiya ve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mahiya ve Ho mahiya ve"/>
</div>
<div class="lyrico-lyrics-wrapper">Tere sang rehna ve jeena ve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere sang rehna ve jeena ve"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho janiya ve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho janiya ve"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh yeah Oh yeah Oh yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh yeah Oh yeah Oh yeah"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh yeah Oh yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh yeah Oh yeah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Romeo Juilet malle puttinattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romeo Juilet malle puttinattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Untadhanta mana jattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Untadhanta mana jattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vala kathalo climax positive gaa rastinatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vala kathalo climax positive gaa rastinatu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maana love story hittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maana love story hittu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Shah Jahan Mumtaaz reborn ayinattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shah Jahan Mumtaaz reborn ayinattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Untamanta manam outtu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Untamanta manam outtu"/>
</div>
<div class="lyrico-lyrics-wrapper">Re-plan chessi nuvu ee sarainna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Re-plan chessi nuvu ee sarainna"/>
</div>
<div class="lyrico-lyrics-wrapper">Taj Mahal mundhe kattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Taj Mahal mundhe kattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">You are my girl
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You are my girl"/>
</div>
<div class="lyrico-lyrics-wrapper">You are my girl
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You are my girl"/>
</div>
<div class="lyrico-lyrics-wrapper">Monalisa navvu sanajaaji puvvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Monalisa navvu sanajaaji puvvu"/>
</div>
<div class="lyrico-lyrics-wrapper">Okataithe nuvvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okataithe nuvvu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">You are my world
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You are my world"/>
</div>
<div class="lyrico-lyrics-wrapper">You are my world
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You are my world"/>
</div>
<div class="lyrico-lyrics-wrapper">Vedi vedi laava sweet pala koova
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedi vedi laava sweet pala koova"/>
</div>
<div class="lyrico-lyrics-wrapper">Okkataithe nuvvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okkataithe nuvvu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thassadiyya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thassadiyya"/>
</div>
<div class="lyrico-lyrics-wrapper">Lets’s do the mama miya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lets’s do the mama miya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thassadiyya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thassadiyya"/>
</div>
<div class="lyrico-lyrics-wrapper">Lets’s do the mama miya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lets’s do the mama miya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thassadiyya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thassadiyya"/>
</div>
<div class="lyrico-lyrics-wrapper">Lets’s do the mama miya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lets’s do the mama miya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thassadiyya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thassadiyya"/>
</div>
<div class="lyrico-lyrics-wrapper">Lets’s do the mama miya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lets’s do the mama miya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sunn mere maahiya ve mahiya ve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sunn mere maahiya ve mahiya ve"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho mahiya ve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho mahiya ve"/>
</div>
<div class="lyrico-lyrics-wrapper">Tere sang rehna ve jeena ve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere sang rehna ve jeena ve"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho janiya ve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho janiya ve"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Donut laanti kallu thippi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Donut laanti kallu thippi"/>
</div>
<div class="lyrico-lyrics-wrapper">Chocolate lips rendu vippi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chocolate lips rendu vippi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ice fruit mantalevo cheppi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ice fruit mantalevo cheppi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanu pichhe kinchina ve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanu pichhe kinchina ve"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Red bull laanti navvu thotti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Red bull laanti navvu thotti"/>
</div>
<div class="lyrico-lyrics-wrapper">Dumbell laanti kanda choopi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dumbell laanti kanda choopi"/>
</div>
<div class="lyrico-lyrics-wrapper">Love symbol la gunde loki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love symbol la gunde loki"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuv enter ichchinaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuv enter ichchinaave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Creamev nuvvsu stoney nenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Creamev nuvvsu stoney nenu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oktai podhaam cream stone la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oktai podhaam cream stone la"/>
</div>
<div class="lyrico-lyrics-wrapper">Bredde nivvu jamme nenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bredde nivvu jamme nenu"/>
</div>
<div class="lyrico-lyrics-wrapper">Miksai podhaam bread jam la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Miksai podhaam bread jam la"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thassadiyya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thassadiyya"/>
</div>
<div class="lyrico-lyrics-wrapper">Lets’s do the mama miya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lets’s do the mama miya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thassadiyya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thassadiyya"/>
</div>
<div class="lyrico-lyrics-wrapper">Lets’s do the mama miya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lets’s do the mama miya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chanda mama meedha kalu peeti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chanda mama meedha kalu peeti"/>
</div>
<div class="lyrico-lyrics-wrapper">Arm strong ponipoyi nattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arm strong ponipoyi nattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee bugga meedha okka mudhu petti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee bugga meedha okka mudhu petti"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenu kooda pongipauna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenu kooda pongipauna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Newton mind ne laagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Newton mind ne laagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa apple murisipoyinattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa apple murisipoyinattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Na hagguloki ninne laagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na hagguloki ninne laagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Neenu kooda murisiponna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neenu kooda murisiponna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Viral aayi na video la veligipodha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viral aayi na video la veligipodha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvuunte na chitti zindagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvuunte na chitti zindagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Trend set chesina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Trend set chesina"/>
</div>
<div class="lyrico-lyrics-wrapper">Teadsenantoo peru raadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teadsenantoo peru raadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana vandhella ishqu bomma ki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana vandhella ishqu bomma ki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thassadiyya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thassadiyya"/>
</div>
<div class="lyrico-lyrics-wrapper">Lets’s do the mama miya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lets’s do the mama miya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thassadiyya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thassadiyya"/>
</div>
<div class="lyrico-lyrics-wrapper">Lets’s do the mama miya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lets’s do the mama miya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sunn mere maahiya ve mahiya ve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sunn mere maahiya ve mahiya ve"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho mahiya ve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho mahiya ve"/>
</div>
<div class="lyrico-lyrics-wrapper">Tere sang rehna ve jeena ve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere sang rehna ve jeena ve"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho janiya ve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho janiya ve"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sunn mere maahiya ve mahiya ve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sunn mere maahiya ve mahiya ve"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho mahiya ve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho mahiya ve"/>
</div>
<div class="lyrico-lyrics-wrapper">Tere sang rehna ve jeena ve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere sang rehna ve jeena ve"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho janiya ve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho janiya ve"/>
</div>
</pre>
