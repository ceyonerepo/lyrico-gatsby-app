---
title: "rang barse song lyrics"
album: "Touch Chesi Chudu"
artist: "JAM 8 - Mani Sharma"
lyricist: "Rehman"
director: "Vikram Sirikonda"
path: "/albums/touch-chesi-chudu-lyrics"
song: "Rang Barse"
image: ../../images/albumart/touch-chesi-chudu.jpg
date: 2018-02-02
lang: telugu
youtubeLink: "https://www.youtube.com/embed/jtORJZSelk8"
type: "happy"
singers:
  - Akashdeep Sengupta
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Rang Barse Chal Rang Barse 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rang Barse Chal Rang Barse "/>
</div>
<div class="lyrico-lyrics-wrapper">Are Adugu Aduguna Rang Barise
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Are Adugu Aduguna Rang Barise"/>
</div>
<div class="lyrico-lyrics-wrapper">Rang Barse Chal Rang Barse 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rang Barse Chal Rang Barse "/>
</div>
<div class="lyrico-lyrics-wrapper">Sari Kotha Rangulo Dil Thadise
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sari Kotha Rangulo Dil Thadise"/>
</div>
<div class="lyrico-lyrics-wrapper">Yenda Vaana Edhainaa Godugulendhukanta 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenda Vaana Edhainaa Godugulendhukanta "/>
</div>
<div class="lyrico-lyrics-wrapper">Gunde Ninda Uppongey Prema Undhi Venta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gunde Ninda Uppongey Prema Undhi Venta"/>
</div>
<div class="lyrico-lyrics-wrapper">Rang Barse Chal Rang Barse 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rang Barse Chal Rang Barse "/>
</div>
<div class="lyrico-lyrics-wrapper">Are Adugu Aduguna Rang Barise
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Are Adugu Aduguna Rang Barise"/>
</div>
<div class="lyrico-lyrics-wrapper">Rang Barse Chal Rang Barse 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rang Barse Chal Rang Barse "/>
</div>
<div class="lyrico-lyrics-wrapper">Sari Kotha Rangulo Dil Thadise
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sari Kotha Rangulo Dil Thadise"/>
</div>
<div class="lyrico-lyrics-wrapper">Yenda Vaana Edhainaa Godugulendhukanta 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenda Vaana Edhainaa Godugulendhukanta "/>
</div>
<div class="lyrico-lyrics-wrapper">Gunde Ninda Uppongey Prema Undhi Venta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gunde Ninda Uppongey Prema Undhi Venta"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarle Anuko Yedhurayye Ye Dhaaraina Okay 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarle Anuko Yedhurayye Ye Dhaaraina Okay "/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Oo Parledhanuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Oo Parledhanuko"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhi Kooda Dosthe Avadha Neekey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhi Kooda Dosthe Avadha Neekey"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Oo Ee Kshanam Andhuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Oo Ee Kshanam Andhuko"/>
</div>
<div class="lyrico-lyrics-wrapper">Oooo Kallalo Nimpuko Oooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oooo Kallalo Nimpuko Oooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Rang Barse Chal Rang Barse 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rang Barse Chal Rang Barse "/>
</div>
<div class="lyrico-lyrics-wrapper">Are Adugu Aduguna Rang Barise
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Are Adugu Aduguna Rang Barise"/>
</div>
<div class="lyrico-lyrics-wrapper">Rang Barse Chal Rang Barse 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rang Barse Chal Rang Barse "/>
</div>
<div class="lyrico-lyrics-wrapper">Sari Kotha Rangulo Dil Thadise
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sari Kotha Rangulo Dil Thadise"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manasannadhi Kadhile Nadhi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasannadhi Kadhile Nadhi "/>
</div>
<div class="lyrico-lyrics-wrapper">Adhi Gala Gala Paruguna Ponee 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhi Gala Gala Paruguna Ponee "/>
</div>
<div class="lyrico-lyrics-wrapper">Nimishaaniki Nimishaaniki 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nimishaaniki Nimishaaniki "/>
</div>
<div class="lyrico-lyrics-wrapper">Nadumuna Kala Nijamai Raanee 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadumuna Kala Nijamai Raanee "/>
</div>
<div class="lyrico-lyrics-wrapper">Life Annadhi Athi Chinnadhi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life Annadhi Athi Chinnadhi "/>
</div>
<div class="lyrico-lyrics-wrapper">Ani Theliyanidhevariki Kaanee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ani Theliyanidhevariki Kaanee"/>
</div>
<div class="lyrico-lyrics-wrapper">Saradhalake Saripodhanee 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saradhalake Saripodhanee "/>
</div>
<div class="lyrico-lyrics-wrapper">Ika Vadhalaku Ye Nimishaanee 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ika Vadhalaku Ye Nimishaanee "/>
</div>
<div class="lyrico-lyrics-wrapper">Challo Rey Kaalamtho Savvaarey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Challo Rey Kaalamtho Savvaarey "/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Holla Rey Manasaara Jeelerey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Holla Rey Manasaara Jeelerey "/>
</div>
<div class="lyrico-lyrics-wrapper">Oooo Okatantey Osaarey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oooo Okatantey Osaarey "/>
</div>
<div class="lyrico-lyrics-wrapper">Lelerey Ledhantaa One Morey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lelerey Ledhantaa One Morey "/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Hey Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Hey Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Kshanam Andhuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Kshanam Andhuko"/>
</div>
<div class="lyrico-lyrics-wrapper">Oooo Kallalo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oooo Kallalo "/>
</div>
<div class="lyrico-lyrics-wrapper">Nimpuko Oooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nimpuko Oooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Rang Barse Chal Rang Barse 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rang Barse Chal Rang Barse "/>
</div>
<div class="lyrico-lyrics-wrapper">Are Adugu Aduguna Rang Barise
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Are Adugu Aduguna Rang Barise"/>
</div>
<div class="lyrico-lyrics-wrapper">Rang Barse Chal Rang Barse 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rang Barse Chal Rang Barse "/>
</div>
<div class="lyrico-lyrics-wrapper">Sari Kotha Rangulo Dil Thadise
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sari Kotha Rangulo Dil Thadise"/>
</div>
<div class="lyrico-lyrics-wrapper">Yenda Vaana Edhainaa Godugulendhukanta 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenda Vaana Edhainaa Godugulendhukanta "/>
</div>
<div class="lyrico-lyrics-wrapper">Gunde Ninda Uppongey Prema Undhi Venta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gunde Ninda Uppongey Prema Undhi Venta"/>
</div>
<div class="lyrico-lyrics-wrapper">Rang Barse Chal Rang Barse 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rang Barse Chal Rang Barse "/>
</div>
<div class="lyrico-lyrics-wrapper">Are Adugu Aduguna Rang Barise
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Are Adugu Aduguna Rang Barise"/>
</div>
<div class="lyrico-lyrics-wrapper">Rang Barse Chal Rang Barse 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rang Barse Chal Rang Barse "/>
</div>
<div class="lyrico-lyrics-wrapper">Sari Kotha Rangulo Dil Thadise
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sari Kotha Rangulo Dil Thadise"/>
</div>
<div class="lyrico-lyrics-wrapper">Yenda Vaana Edhainaa Godugulendhukanta 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenda Vaana Edhainaa Godugulendhukanta "/>
</div>
<div class="lyrico-lyrics-wrapper">Gunde Ninda Uppongey Prema Undhi Venta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gunde Ninda Uppongey Prema Undhi Venta"/>
</div>
</pre>
