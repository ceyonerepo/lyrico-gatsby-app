---
title: "varungaalam engalathu odum song lyrics"
album: "Nadodigal 2"
artist: "Justin Prabhakaran"
lyricist: "G. Logan - Arivu"
director: "Samuthrakani"
path: "/albums/nadodigal-2-lyrics"
song: "Varungaalam Engalathu"
image: ../../images/albumart/nadodigal-2.jpg
date: 2020-01-31
lang: tamil
youtubeLink: "https://www.youtube.com/embed/3naPKbnGy1c"
type: "Motivational"
singers:
  - G. Logan
  - Arivu
  - Gana Ulagam Dharini
  - Isaivani
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Varungaalam Engaldhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varungaalam Engaldhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Varalaarum Engaldhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varalaarum Engaldhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhigaram Engaldhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhigaram Engaldhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhula Edhudaa Ungaldhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhula Edhudaa Ungaldhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Veli Pottaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Veli Pottaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaattaaru Sikkaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattaaru Sikkaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Vaacha Pechellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Vaacha Pechellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini Velaikkavaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Velaikkavaadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Vote Thaane Venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Vote Thaane Venum"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam Kasirundha Podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Kasirundha Podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Note Irundhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Note Irundhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Naatai Aazhum Kai Naatum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naatai Aazhum Kai Naatum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Kolgai Yaaru Ketta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kolgai Yaaru Ketta"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kalvi Yaaru Paatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kalvi Yaaru Paatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Nallavano Kettavano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Nallavano Kettavano"/>
</div>
<div class="lyrico-lyrics-wrapper">Edraa Note
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edraa Note"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Padukka Naathiyilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padukka Naathiyilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhaigalin Kudisaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhaigalin Kudisaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhukku Maadi Mela Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhukku Maadi Mela Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Namadhu Thalaivargal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namadhu Thalaivargal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhukku Aadai Yodu Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhukku Aadai Yodu Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Namadhu Kuzhanthaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namadhu Kuzhanthaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Mithakum Bothaiyodu Yen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mithakum Bothaiyodu Yen"/>
</div>
<div class="lyrico-lyrics-wrapper">Namadhu Illainjargal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namadhu Illainjargal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pasikku Thirudiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pasikku Thirudiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Manidhanai Udhaikkirom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manidhanai Udhaikkirom"/>
</div>
<div class="lyrico-lyrics-wrapper">Pathukkum Thirudanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathukkum Thirudanai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalaivanaai Mathikkirom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaivanaai Mathikkirom"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadikkum Manithar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadikkum Manithar"/>
</div>
<div class="lyrico-lyrics-wrapper">Pinnale Koottangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pinnale Koottangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Uzhaikkum Manithar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uzhaikkum Manithar"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhvil Porattangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvil Porattangal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varungaalam Engaldhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varungaalam Engaldhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Varalaarum Engaldhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varalaarum Engaldhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhigaram Engaldhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhigaram Engaldhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhula Edhudaa Ungaldhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhula Edhudaa Ungaldhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Veli Pottaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Veli Pottaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaattaaru Sikkaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattaaru Sikkaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Vaacha Pechellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Vaacha Pechellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini Velaikkavaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Velaikkavaadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gindhakkadha Jidhakka Jidhakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gindhakkadha Jidhakka Jidhakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Gindhakkadha Jidhakka Jidhakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gindhakkadha Jidhakka Jidhakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Gindhakkadha Jidhakka Jidhakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gindhakkadha Jidhakka Jidhakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Gindhakkadha Jidhakka Jidhakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gindhakkadha Jidhakka Jidhakka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennada Vazhkkai Idhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennada Vazhkkai Idhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Panam Thaan Perusakkidhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panam Thaan Perusakkidhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Panathoda Compare Panna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panathoda Compare Panna"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagam Sirisaakkidhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagam Sirisaakkidhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaarukitta Solluvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarukitta Solluvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga Koraiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Koraiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha Medaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Medaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesuvonda Neraya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesuvonda Neraya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Vaazhkaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Vaazhkaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Egapatta Kashtam Varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Egapatta Kashtam Varum"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha Kashtatha Nee Thaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Kashtatha Nee Thaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandha Vettri Varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandha Vettri Varum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inga Adiyum Othaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga Adiyum Othaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vangi Thongudhu Katchi Kodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vangi Thongudhu Katchi Kodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Namma Kaalam Thairiyama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Namma Kaalam Thairiyama"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeri Adi Adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeri Adi Adi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kola Kolla Karpazhippu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kola Kolla Karpazhippu"/>
</div>
<div class="lyrico-lyrics-wrapper">Perigiduchi Naatula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perigiduchi Naatula"/>
</div>
<div class="lyrico-lyrics-wrapper">Panathaasa Kaatti Janatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panathaasa Kaatti Janatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thorathittaanga Roatila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thorathittaanga Roatila"/>
</div>
<div class="lyrico-lyrics-wrapper">Namakkinnu Urima Irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namakkinnu Urima Irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhukku Othungi Poganum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhukku Othungi Poganum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadukkura Aazha Udhachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadukkura Aazha Udhachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendula Onnu Paakkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendula Onnu Paakkanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mel Padippu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mel Padippu"/>
</div>
<div class="lyrico-lyrics-wrapper">Padikkanumnu Aasai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padikkanumnu Aasai"/>
</div>
<div class="lyrico-lyrics-wrapper">Athukku Aappu Veikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athukku Aappu Veikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Thittam Pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thittam Pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kondu Varaan Neet-aah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondu Varaan Neet-aah"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaarukkum Ore
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaarukkum Ore"/>
</div>
<div class="lyrico-lyrics-wrapper">Needhi Irukudha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Needhi Irukudha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Naanga Ketkum Kelvikellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Naanga Ketkum Kelvikellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ongitta Badhilu Irukudhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ongitta Badhilu Irukudhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arasiyal Vilambaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arasiyal Vilambaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Aana Athu Illada Nerandharam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aana Athu Illada Nerandharam"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaathi Pera Solli Inge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaathi Pera Solli Inge"/>
</div>
<div class="lyrico-lyrics-wrapper">Yegapatta Kalavaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yegapatta Kalavaram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pathaviya Pudikkiraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathaviya Pudikkiraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma Maanatha Thaan Parikkiraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma Maanatha Thaan Parikkiraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vottu Potta Janatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vottu Potta Janatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Engada Oruthan Ippa Mathikiraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engada Oruthan Ippa Mathikiraan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vettu Kutha Undu Pannum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vettu Kutha Undu Pannum"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaathi Venaanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaathi Venaanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Ottu Thuniyum Mela Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Ottu Thuniyum Mela Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Seththu Ponaa Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seththu Ponaa Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennanamo Nadakuthinga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennanamo Nadakuthinga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannu Munnaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu Munnaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma Onna Irundha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma Onna Irundha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaam Varum Namma Pinnaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaam Varum Namma Pinnaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Namma Pinnaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Namma Pinnaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Namma Pinnala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Namma Pinnala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Padi Padi Padi Padi Padi Padinnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padi Padi Padi Padi Padi Padinnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Parentuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parentuu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Kudi Kudi Kudi Kudi Kudinnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kudi Kudi Kudi Kudi Kudinnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Governmentu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Governmentu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Padikkava Kudikkava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padikkava Kudikkava"/>
</div>
<div class="lyrico-lyrics-wrapper">Padikkava Kudikkava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padikkava Kudikkava"/>
</div>
<div class="lyrico-lyrics-wrapper">Kozhambi Poyi Nikkiraanda Studentu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kozhambi Poyi Nikkiraanda Studentu"/>
</div>
<div class="lyrico-lyrics-wrapper">Studentu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Studentu"/>
</div>
<div class="lyrico-lyrics-wrapper">Studentu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Studentu"/>
</div>
<div class="lyrico-lyrics-wrapper">Studentu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Studentu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varungaalam Engaldhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varungaalam Engaldhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Varalaarum Engaldhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varalaarum Engaldhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhigaram Engaldhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhigaram Engaldhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhula Yedhuda Ungaldhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhula Yedhuda Ungaldhu"/>
</div>
</pre>
