---
title: "dandanakka dandanakka thavuladi song lyrics"
album: "Velan"
artist: "Gopi Sundar"
lyricist: "Velmurugan - Kalaimagan Mubarak"
director: "Kavin"
path: "/albums/velan-lyrics"
song: "Dandanakka Dandanakka Thavuladi"
image: ../../images/albumart/velan.jpg
date: 2021-12-31
lang: tamil
youtubeLink: "https://www.youtube.com/embed/gVZNXpEsYRk"
type: "happy"
singers:
  - Velmurugan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Oothungada kombu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oothungada kombu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottungada kottukaaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottungada kottukaaran"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada nama ellarum vaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada nama ellarum vaanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Velan vanthuruchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velan vanthuruchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thillayaru parambarayila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thillayaru parambarayila"/>
</div>
<div class="lyrico-lyrics-wrapper">Pazhanisamy peththedutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pazhanisamy peththedutha"/>
</div>
<div class="lyrico-lyrics-wrapper">Velan thaan pass aitaan paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velan thaan pass aitaan paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippo oorukkae pandhi pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo oorukkae pandhi pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Parimaaruvom kari soru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parimaaruvom kari soru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yei dandanakka dandanakka thavuladi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei dandanakka dandanakka thavuladi"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru vetri thaanae 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru vetri thaanae "/>
</div>
<div class="lyrico-lyrics-wrapper">thozhvikkellaam badhiladi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thozhvikkellaam badhiladi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalavaazha ela mela thalagari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalavaazha ela mela thalagari"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada thadabudala nadakkuthu oru pudipudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada thadabudala nadakkuthu oru pudipudi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pass-aagi vanthutaan pangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pass-aagi vanthutaan pangu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippa oorukkul aagitaan trend-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippa oorukkul aagitaan trend-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pass-aagi vanthutaan pangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pass-aagi vanthutaan pangu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippa oorukkul aagitaan trend-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippa oorukkul aagitaan trend-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varungalam namakkachu vaazhnthu kaatuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varungalam namakkachu vaazhnthu kaatuvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Varalaru padaikka thaan paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varalaru padaikka thaan paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanbanukku mela oru sondham illada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanbanukku mela oru sondham illada"/>
</div>
<div class="lyrico-lyrics-wrapper">Avana pola yaarum inga bandham illada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avana pola yaarum inga bandham illada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yei dandanakka yei dandanakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei dandanakka yei dandanakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Yei dandanakka dandanakka thavuladi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei dandanakka dandanakka thavuladi"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru vetri thaanae thozhvikkellam badhiladi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru vetri thaanae thozhvikkellam badhiladi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalavaazha ela mela thalagari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalavaazha ela mela thalagari"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada thadabudala nadakkuthu oru pudipudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada thadabudala nadakkuthu oru pudipudi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nambikkai mattum irundha ellaam nadakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nambikkai mattum irundha ellaam nadakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma kaalukkum keezha andha vaanam kedakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma kaalukkum keezha andha vaanam kedakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thottadhellaam inimel dhool parakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottadhellaam inimel dhool parakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Inga thokkuravan jeyikka oru neram irukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga thokkuravan jeyikka oru neram irukkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yei kuthu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei kuthu da"/>
</div>
<div class="lyrico-lyrics-wrapper">Yei kuthu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei kuthu da"/>
</div>
<div class="lyrico-lyrics-wrapper">Yei kuthu da kuthu da aattam varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei kuthu da kuthu da aattam varum"/>
</div>
<div class="lyrico-lyrics-wrapper">Summa thattuda thattuda paatu varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summa thattuda thattuda paatu varum"/>
</div>
<div class="lyrico-lyrics-wrapper">Yei kuthu da kuthu da aattam varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei kuthu da kuthu da aattam varum"/>
</div>
<div class="lyrico-lyrics-wrapper">Summa thattuda thattuda paatu varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summa thattuda thattuda paatu varum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pass-aagi vanthutaan pangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pass-aagi vanthutaan pangu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippa oorukkul aagitaan trend-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippa oorukkul aagitaan trend-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pass-aagi vanthutaan pangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pass-aagi vanthutaan pangu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippa oorukkul aagitaan trend-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippa oorukkul aagitaan trend-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yei dandanakka yei dandanakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei dandanakka yei dandanakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Yei dandanakka dandanakka thavuladi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei dandanakka dandanakka thavuladi"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru vetri thaanae thozhvikkellam badhiladi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru vetri thaanae thozhvikkellam badhiladi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aa kaasu mattum irundhakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa kaasu mattum irundhakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Oodhariyaakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oodhariyaakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kooda kalviyum sendhirundha oorae viyakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooda kalviyum sendhirundha oorae viyakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarukkul natpae thaan uyarvanadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarukkul natpae thaan uyarvanadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu paasangu illadha uravanadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu paasangu illadha uravanadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yei kuthu da kuthu da aattam varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei kuthu da kuthu da aattam varum"/>
</div>
<div class="lyrico-lyrics-wrapper">Summa thattuda thattuda paatu varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summa thattuda thattuda paatu varum"/>
</div>
<div class="lyrico-lyrics-wrapper">Yei kuthu da kuthu da aattam varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei kuthu da kuthu da aattam varum"/>
</div>
<div class="lyrico-lyrics-wrapper">Summa thattuda thattuda paatu varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summa thattuda thattuda paatu varum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pass-aagi vanthutaan pangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pass-aagi vanthutaan pangu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippa oorukkul aagitaan trend-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippa oorukkul aagitaan trend-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pass-aagi vanthutaan pangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pass-aagi vanthutaan pangu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippa oorukkul aagitaan trend-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippa oorukkul aagitaan trend-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yei dandanakka yei dandanakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei dandanakka yei dandanakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Yei dandanakka dandanakka thavuladi thavuladi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei dandanakka dandanakka thavuladi thavuladi"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru vetri thaanae thozhvikkellam badhiladi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru vetri thaanae thozhvikkellam badhiladi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalavaazha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalavaazha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalavaazha ela mela thalagarithalagari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalavaazha ela mela thalagarithalagari"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada thadabudala nadakkuthu oru pudipudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada thadabudala nadakkuthu oru pudipudi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pass-aagi vanthutaan pangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pass-aagi vanthutaan pangu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippa oorukkul aagitaan trend-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippa oorukkul aagitaan trend-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pass-aagi vanthutaan pangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pass-aagi vanthutaan pangu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippa oorukkul aagitaan trend-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippa oorukkul aagitaan trend-u"/>
</div>
</pre>
