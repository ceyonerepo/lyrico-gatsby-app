---
title: "annanapathi kavalaiilla song lyrics"
album: "Mannar Vagaiyara"
artist: "Jakes Bejoy"
lyricist: "Boopathy Pandian"
director: "G. Boopathy Pandian"
path: "/albums/mannar-vagaiyara-lyrics"
song: "Annanapathi Kavalaiilla"
image: ../../images/albumart/mannar-vagaiyara.jpg
date: 2018-01-26
lang: tamil
youtubeLink: "https://www.youtube.com/embed/rmfgIP8fv3A"
type: "love"
singers:
  - Remya Nambeesan
  - Renjith
  - Inzamam-Ul-Huq	
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Annanapathi kavalaiillaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annanapathi kavalaiillaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku appanapathiyum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku appanapathiyum "/>
</div>
<div class="lyrico-lyrics-wrapper">kavalaiillaaaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kavalaiillaaaaaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ippo freeya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo freeya"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru song polamaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru song polamaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ready floor
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ready floor"/>
</div>
<div class="lyrico-lyrics-wrapper">Come to the floor
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Come to the floor"/>
</div>
<div class="lyrico-lyrics-wrapper">Take it back to the 90’s
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Take it back to the 90’s"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En annanapathi kavalaiilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En annanapathi kavalaiilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku appanapathiyum kavaiilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku appanapathiyum kavaiilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooru pathi kavalaiilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooru pathi kavalaiilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku yaaru pathiyum kavalaiilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku yaaru pathiyum kavalaiilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan unakkaga pootha rosa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan unakkaga pootha rosa"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna alekka thooku raasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna alekka thooku raasa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorukkul poovum maasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorukkul poovum maasa"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku needhanae kaadhal baasha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku needhanae kaadhal baasha"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarukum teriyama bombay-il manickama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarukum teriyama bombay-il manickama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhvoma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvoma"/>
</div>
<div class="lyrico-lyrics-wrapper">Hei hei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei hei"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa mama"/>
</div>
<div class="lyrico-lyrics-wrapper">Hei hei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei hei"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhvoma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvoma"/>
</div>
<div class="lyrico-lyrics-wrapper">Hei hei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei hei"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa mama"/>
</div>
<div class="lyrico-lyrics-wrapper">Hei hei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei hei"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Come on
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Come on"/>
</div>
<div class="lyrico-lyrics-wrapper">Brother father
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Brother father"/>
</div>
<div class="lyrico-lyrics-wrapper">I don’t worry
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I don’t worry"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaali katta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaali katta"/>
</div>
<div class="lyrico-lyrics-wrapper">I am ready ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I am ready ma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vella vedakozhi neethan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vella vedakozhi neethan"/>
</div>
<div class="lyrico-lyrics-wrapper">Naatu nallennaiyum naanthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naatu nallennaiyum naanthan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vachi samachiko neethan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vachi samachiko neethan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nachinnu iruppenae naanthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nachinnu iruppenae naanthan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennathaan vittu poiyittennu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennathaan vittu poiyittennu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalangithaan ninaen naanum pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalangithaan ninaen naanum pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">En athaan neeyum venumunnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En athaan neeyum venumunnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthutten naan un kaiyukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthutten naan un kaiyukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Aiyuttom remix-u than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyuttom remix-u than"/>
</div>
<div class="lyrico-lyrics-wrapper">Naama aiyuttom remix-u than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naama aiyuttom remix-u than"/>
</div>
<div class="lyrico-lyrics-wrapper">Sernthu adipomae ini six-u than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sernthu adipomae ini six-u than"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En annanapathi kavalaiilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En annanapathi kavalaiilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakkali en appanapathiyum kavaiilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakkali en appanapathiyum kavaiilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hoi hoi hoi hoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoi hoi hoi hoi"/>
</div>
1234
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nalla super thethi paarthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla super thethi paarthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Panthal paavunalae pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panthal paavunalae pottu"/>
</div>
  <div class="lyrico-lyrics-wrapper">Vantha sanathikku soru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vantha sanathikku soru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada velli elai mela pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada velli elai mela pottu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaada dei maalai maathikalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada dei maalai maathikalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Evanachum ketta paathukulam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evanachum ketta paathukulam"/>
</div>
<div class="lyrico-lyrics-wrapper">Dum dum dum pi pi eppo eppo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dum dum dum pi pi eppo eppo"/>
</div>
<div class="lyrico-lyrics-wrapper">Dam dam dam happy ippo ippo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dam dam dam happy ippo ippo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rajaavin paata pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rajaavin paata pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilayarajavin paata pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilayarajavin paata pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">En azhaga thaaliya neeyum kattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En azhaga thaaliya neeyum kattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un annanapathi kavalaiilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un annanapathi kavalaiilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakkali un appanapathiyum kavalaiilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakkali un appanapathiyum kavalaiilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooru pathi kavalaiilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooru pathi kavalaiilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku yaara pathiyum kavalaiilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku yaara pathiyum kavalaiilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adichi norukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adichi norukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthuthan en lover
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthuthan en lover"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna patha than unakku ennathan agathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna patha than unakku ennathan agathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Podu thagida podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podu thagida podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru 90’s paatu podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru 90’s paatu podu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey adichi terikka vanthuthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey adichi terikka vanthuthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna patha than unakku ennathan agathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna patha than unakku ennathan agathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Podu thagida podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podu thagida podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru 90’s paatu podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru 90’s paatu podu"/>
</div>
</pre>
