---
title: "sivalinga song lyrics"
album: "Shivalinga"
artist: "S S Thaman"
lyricist: "Viveka"
director: "P Vasu"
path: "/albums/shivalinga-lyrics"
song: "Sivalinga"
image: ../../images/albumart/shivalinga.jpg
date: 2017-04-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/r4aay2rjDIg"
type: "mass"
singers:
  -	Usha Uthup
  - Kalpana
  - Sri Krishna
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Jaya Jaya Jaya Jaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaya Jaya Jaya Jaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Vedhigal Muzhangida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedhigal Muzhangida"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadaigalai Udaithidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadaigalai Udaithidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Maha Shiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maha Shiva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thegu Thegu Theguvena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thegu Thegu Theguvena"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeriyum Sudaraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeriyum Sudaraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedamudam Nadanthidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedamudam Nadanthidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Shiva Shiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shiva Shiva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veru Veru Veruvana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veru Veru Veruvana"/>
</div>
<div class="lyrico-lyrics-wrapper">Sara Sara Sara Vena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sara Sara Sara Vena"/>
</div>
<div class="lyrico-lyrics-wrapper">Vegam Kattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vegam Kattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Maha Shiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maha Shiva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hara Hara Hara Hara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hara Hara Hara Hara"/>
</div>
<div class="lyrico-lyrics-wrapper">Hara Hara Hara Hara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hara Hara Hara Hara"/>
</div>
<div class="lyrico-lyrics-wrapper">Shiva Shiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shiva Shiva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sivalingaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sivalingaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veru Veru Veruvana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veru Veru Veruvana"/>
</div>
<div class="lyrico-lyrics-wrapper">Sara Sara Sara Vena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sara Sara Sara Vena"/>
</div>
<div class="lyrico-lyrics-wrapper">Vegam Kattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vegam Kattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Maha Shiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maha Shiva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hara Hara Hara Hara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hara Hara Hara Hara"/>
</div>
<div class="lyrico-lyrics-wrapper">Hara Hara Hara Hara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hara Hara Hara Hara"/>
</div>
<div class="lyrico-lyrics-wrapper">Shiva Shiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shiva Shiva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jagamathai Kaathidum Yuga Linga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jagamathai Kaathidum Yuga Linga"/>
</div>
<div class="lyrico-lyrics-wrapper">Saranadaithorukku Pani Linga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saranadaithorukku Pani Linga"/>
</div>
<div class="lyrico-lyrics-wrapper">Sinam Kondaal Agni Linga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sinam Kondaal Agni Linga"/>
</div>
<div class="lyrico-lyrics-wrapper">Sivalinga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sivalinga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mathamkondu Varugayil Gaja Linga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mathamkondu Varugayil Gaja Linga"/>
</div>
<div class="lyrico-lyrics-wrapper">Malaiyena Thozhkonda Buja Linga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malaiyena Thozhkonda Buja Linga"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharuvathile Ivan Dhana Linga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharuvathile Ivan Dhana Linga"/>
</div>
<div class="lyrico-lyrics-wrapper">Sivalinga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sivalinga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kan Sivandhal Perunkattaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan Sivandhal Perunkattaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalam Pugundhal Angu Bali Nooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalam Pugundhal Angu Bali Nooru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ovvoru Idathilum Mugam Veru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ovvoru Idathilum Mugam Veru"/>
</div>
<div class="lyrico-lyrics-wrapper">Sivalinga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sivalinga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yethaiyum Maraithida Ninaikaathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethaiyum Maraithida Ninaikaathey"/>
</div>
<div class="lyrico-lyrics-wrapper">Athellam Ivanidam Palikaathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athellam Ivanidam Palikaathey"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhai Mudipaane Oru Noduikkulley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhai Mudipaane Oru Noduikkulley"/>
</div>
<div class="lyrico-lyrics-wrapper">Athuthaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athuthaney"/>
</div>
<div class="lyrico-lyrics-wrapper">Sivalinga Aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sivalinga Aaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sivalinga Sivalinga Sivalinga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sivalinga Sivalinga Sivalinga"/>
</div>
<div class="lyrico-lyrics-wrapper">Sivalinga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sivalinga"/>
</div>
<div class="lyrico-lyrics-wrapper">Sivalinga Sivalinga Sivalinga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sivalinga Sivalinga Sivalinga"/>
</div>
<div class="lyrico-lyrics-wrapper">Sivalinga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sivalinga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adai Mazhai Naduvilum Erivaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adai Mazhai Naduvilum Erivaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Agamathil Neruppudan Therivaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Agamathil Neruppudan Therivaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Thisaiyettum Theemaigal Azhippane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thisaiyettum Theemaigal Azhippane"/>
</div>
<div class="lyrico-lyrics-wrapper">Sivalinga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sivalinga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idam Valam Merkkil Yellame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idam Valam Merkkil Yellame"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivanathu Paarvai Vaipaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivanathu Paarvai Vaipaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadam Purandaal Athai Saipaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadam Purandaal Athai Saipaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Sivalinga Ga Ga Ga Ga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sivalinga Ga Ga Ga Ga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sivalinga Sivalinga Sivalinga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sivalinga Sivalinga Sivalinga"/>
</div>
<div class="lyrico-lyrics-wrapper">Sivalinga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sivalinga"/>
</div>
<div class="lyrico-lyrics-wrapper">Sivalinga Sivalinga Sivalinga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sivalinga Sivalinga Sivalinga"/>
</div>
<div class="lyrico-lyrics-wrapper">Sivalinga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sivalinga"/>
</div>
</pre>
