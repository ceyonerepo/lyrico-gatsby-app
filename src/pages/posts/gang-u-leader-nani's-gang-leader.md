---
title: "gang-u leader song lyrics"
album: "Nani's Gang Leader"
artist: "Anirudh Ravichander"
lyricist: "Anantha Sriram"
director: "Vikram Kumar"
path: "/albums/nani's-gang-leader-lyrics"
song: "Gang-u Leader"
image: ../../images/albumart/nanis-gang-leader.jpg
date: 2019-09-13
lang: telugu
youtubeLink: "https://www.youtube.com/embed/6sDSZwH2EpM"
type: "mass"
singers:
  - Anirudh Ravichander
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ye Scenenu Sirigi Seattulirigi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Scenenu Sirigi Seattulirigi"/>
</div>
<div class="lyrico-lyrics-wrapper">Seetti Kottaloi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seetti Kottaloi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye Cededu Nizam Andhra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Cededu Nizam Andhra"/>
</div>
<div class="lyrico-lyrics-wrapper">Sindhu Thokkaloi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sindhu Thokkaloi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sitike Vesi Welcome Cheppandoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sitike Vesi Welcome Cheppandoi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirunavvultho Harathi Pattandoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirunavvultho Harathi Pattandoi"/>
</div>
<div class="lyrico-lyrics-wrapper">Gangu Gangu Leader Vacchadu Legandoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gangu Gangu Leader Vacchadu Legandoi"/>
</div>
<div class="lyrico-lyrics-wrapper">Hangu Hang Over Lo Oogaali Padhandoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hangu Hang Over Lo Oogaali Padhandoi"/>
</div>
<div class="lyrico-lyrics-wrapper">Gangu Gangu Leader Vacchadu Legandoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gangu Gangu Leader Vacchadu Legandoi"/>
</div>
<div class="lyrico-lyrics-wrapper">Hangu Hang Over Lo Oogaali Padhandoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hangu Hang Over Lo Oogaali Padhandoi"/>
</div>
<div class="lyrico-lyrics-wrapper">Scenenu Sirigi Seattulirigi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Scenenu Sirigi Seattulirigi"/>
</div>
<div class="lyrico-lyrics-wrapper">Seetti Kottaloi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seetti Kottaloi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye Cededu Nizam Andhra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Cededu Nizam Andhra"/>
</div>
<div class="lyrico-lyrics-wrapper">Sindhu Thokkaloi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sindhu Thokkaloi"/>
</div>
<div class="lyrico-lyrics-wrapper">P P
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="P P"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Saraswati Perolone Kontha Softu Ro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Saraswati Perolone Kontha Softu Ro"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Bamma Maro Bhadrakali Kadharo..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Bamma Maro Bhadrakali Kadharo.."/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Varalakshmi Matalone Antha Hardu Ro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Varalakshmi Matalone Antha Hardu Ro"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Amma Inko Annapurna Kadharo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Amma Inko Annapurna Kadharo"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Kantlo Kopanni Ee Kantlo Istanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Kantlo Kopanni Ee Kantlo Istanni"/>
</div>
<div class="lyrico-lyrics-wrapper">Chupisthu Untadhoi  Ma Priya Darling..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chupisthu Untadhoi  Ma Priya Darling.."/>
</div>
<div class="lyrico-lyrics-wrapper">Swathi Laa O Chelli Andhariki Undunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Swathi Laa O Chelli Andhariki Undunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Lokam O Swargam Avunani Na Feeling
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Lokam O Swargam Avunani Na Feeling"/>
</div>
<div class="lyrico-lyrics-wrapper">Addede Chinnu (Chinnu)
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Addede Chinnu (Chinnu)"/>
</div>
<div class="lyrico-lyrics-wrapper">Pencilu Idhi Pennu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pencilu Idhi Pennu"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha Kalisi (Kalisi)
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Kalisi (Kalisi)"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinchestharu Ninnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinchestharu Ninnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gangu Gangu Leader Vacchadu Legandoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gangu Gangu Leader Vacchadu Legandoi"/>
</div>
<div class="lyrico-lyrics-wrapper">Hangu Hang Over Lo Oogaali Padhandoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hangu Hang Over Lo Oogaali Padhandoi"/>
</div>
<div class="lyrico-lyrics-wrapper">Gangu Gangu Leader Vacchadu Legandoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gangu Gangu Leader Vacchadu Legandoi"/>
</div>
<div class="lyrico-lyrics-wrapper">Hangu Hang Over Lo Oogaali Padhandoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hangu Hang Over Lo Oogaali Padhandoi"/>
</div>
<div class="lyrico-lyrics-wrapper">Scenenu Sirigi Seattulirigi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Scenenu Sirigi Seattulirigi"/>
</div>
<div class="lyrico-lyrics-wrapper">Seetti Kottaloi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seetti Kottaloi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye Cededu Nizam Andhra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Cededu Nizam Andhra"/>
</div>
<div class="lyrico-lyrics-wrapper">Sindhu Thokkaloi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sindhu Thokkaloi"/>
</div>
<div class="lyrico-lyrics-wrapper">P P
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="P P"/>
</div>
<div class="lyrico-lyrics-wrapper">Gangu Gangu Leader Vacchadu Legandoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gangu Gangu Leader Vacchadu Legandoi"/>
</div>
<div class="lyrico-lyrics-wrapper">Hangu Hang Over Lo Oogaali Padhandoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hangu Hang Over Lo Oogaali Padhandoi"/>
</div>
<div class="lyrico-lyrics-wrapper">Gangu Gangu Leader Vacchadu Legandoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gangu Gangu Leader Vacchadu Legandoi"/>
</div>
<div class="lyrico-lyrics-wrapper">Hangu Hang Over Lo Oogaali Padhandoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hangu Hang Over Lo Oogaali Padhandoi"/>
</div>
<div class="lyrico-lyrics-wrapper">Tipiri Tipiri Tippu Tipiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tipiri Tipiri Tippu Tipiri"/>
</div>
<div class="lyrico-lyrics-wrapper">Tipiri Tipiri Tippu Tipiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tipiri Tipiri Tippu Tipiri"/>
</div>
<div class="lyrico-lyrics-wrapper">Tipiri Tipiri Tippu Tipiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tipiri Tipiri Tippu Tipiri"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhagdhigdhi Dhagdhigdhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhagdhigdhi Dhagdhigdhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Tipiri Tipiri Tippu Tipiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tipiri Tipiri Tippu Tipiri"/>
</div>
<div class="lyrico-lyrics-wrapper">Tipiri Tipiri Tippu Tipiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tipiri Tipiri Tippu Tipiri"/>
</div>
<div class="lyrico-lyrics-wrapper">Tipiri Tipiri Tippu Tipiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tipiri Tipiri Tippu Tipiri"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhagdhigdhi Dhagdhigdhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhagdhigdhi Dhagdhigdhi"/>
</div>
</pre>
