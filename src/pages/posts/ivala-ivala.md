---
title: "ivala ivala song lyrics"
album: "charlie chaplin 2"
artist: "amrish"
lyricist: "prabhu deva"
director: "Sakthi chithambaram"
path: "/albums/charlie-chaplin-2-song-lyrics"
song: "ivala ivala"
image: ../../images/albumart/charlie-chaplin-2.jpg
date: 2019-01-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/oqlLzL779hE"
type: "love"
singers:
  - amrish
---

<pre class="lyrics-native">
</pre>
<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ivala Ivala Ivala Ivala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivala Ivala Ivala Ivala"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivala Ivala Ivala Ivala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivala Ivala Ivala Ivala"/>
</div>
<div class="lyrico-lyrics-wrapper">Romba Pudichiruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba Pudichiruku"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku Romba Pudichiruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku Romba Pudichiruku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avala Avala Avala Avala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avala Avala Avala Avala"/>
</div>
<div class="lyrico-lyrics-wrapper">Avala Avala Avala Avala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avala Avala Avala Avala"/>
</div>
<div class="lyrico-lyrics-wrapper">Avalo Pudichiruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avalo Pudichiruku"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku Avalo Pudichiruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku Avalo Pudichiruku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivala Ivala Ivala Ivala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivala Ivala Ivala Ivala"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivala Ivala Ivala Ivala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivala Ivala Ivala Ivala"/>
</div>
<div class="lyrico-lyrics-wrapper">Romba Pudichiruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba Pudichiruku"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku Romba Pudichiruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku Romba Pudichiruku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avala Avala Avala Avala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avala Avala Avala Avala"/>
</div>
<div class="lyrico-lyrics-wrapper">Avala Avala Avala Avala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avala Avala Avala Avala"/>
</div>
<div class="lyrico-lyrics-wrapper">Avalo Pudichiruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avalo Pudichiruku"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku Avalo Pudichiruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku Avalo Pudichiruku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhura Dhura Dhura Dhura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhura Dhura Dhura Dhura"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhura Dhura Dhura Dhura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhura Dhura Dhura Dhura"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhooram Poralae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhooram Poralae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Thooki Poralae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Thooki Poralae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivala Ivala Ivala Ivala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivala Ivala Ivala Ivala"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivala Ivala Ivala Ivala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivala Ivala Ivala Ivala"/>
</div>
<div class="lyrico-lyrics-wrapper">Romba Pudichiruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba Pudichiruku"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku Romba Pudichiruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku Romba Pudichiruku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Make-up Podala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Make-up Podala"/>
</div>
<div class="lyrico-lyrics-wrapper">Thala Sevala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thala Sevala"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanalum Ava Mugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanalum Ava Mugam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudichirukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudichirukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Akthar Poosala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Akthar Poosala"/>
</div>
<div class="lyrico-lyrics-wrapper">Maali Vaikala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maali Vaikala"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanalum Ava Vaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanalum Ava Vaasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudichirukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudichirukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kolu Kolu Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolu Kolu Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Olliyavum Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Olliyavum Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanalum Ava Shape-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanalum Ava Shape-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudichirukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudichirukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">English-u Pesala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="English-u Pesala"/>
</div>
<div class="lyrico-lyrics-wrapper">Hindi Pesala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hindi Pesala"/>
</div>
<div class="lyrico-lyrics-wrapper">Tamizhatchi Avala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tamizhatchi Avala"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudichirukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudichirukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaniyaga Pesi Sirikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaniyaga Pesi Sirikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paithiyam Aagiviten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paithiyam Aagiviten"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalyanam Aagamal Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalyanam Aagamal Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavan Aagivitean
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavan Aagivitean"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivala Ivala Ivala Ivala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivala Ivala Ivala Ivala"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivala Ivala Ivala Ivala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivala Ivala Ivala Ivala"/>
</div>
<div class="lyrico-lyrics-wrapper">Romba Pudichiruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba Pudichiruku"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku Romba Pudichiruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku Romba Pudichiruku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mutta Mutta Kannula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mutta Mutta Kannula"/>
</div>
<div class="lyrico-lyrics-wrapper">Kotta Kotta Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kotta Kotta Enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Mulungura Azhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mulungura Azhagu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudichirukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudichirukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sattu-nu Naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sattu-nu Naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattu-nu Paatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattu-nu Paatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Look-a Mathuradhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Look-a Mathuradhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudichirukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudichirukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennoda Pirandha Naal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennoda Pirandha Naal"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudikala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudikala"/>
</div>
<div class="lyrico-lyrics-wrapper">Aana Avaloda Priandha Naal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aana Avaloda Priandha Naal"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudichirukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudichirukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennoda Amma Appa Vidavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennoda Amma Appa Vidavum"/>
</div>
<div class="lyrico-lyrics-wrapper">Avaloda Amma Appa Pudichirukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avaloda Amma Appa Pudichirukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ava Seiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Seiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kutty Settai’ku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kutty Settai’ku"/>
</div>
<div class="lyrico-lyrics-wrapper">Adimai Naan Dhanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adimai Naan Dhanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avaloda Chutti Kuzhandhaiku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avaloda Chutti Kuzhandhaiku"/>
</div>
<div class="lyrico-lyrics-wrapper">Appa Aava-nae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appa Aava-nae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivala Ivala Ivala Ivala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivala Ivala Ivala Ivala"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivala Ivala Ivala Ivala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivala Ivala Ivala Ivala"/>
</div>
<div class="lyrico-lyrics-wrapper">Romba Pudichiruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba Pudichiruku"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku Romba Pudichiruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku Romba Pudichiruku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avala Avala Avala Avala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avala Avala Avala Avala"/>
</div>
<div class="lyrico-lyrics-wrapper">Avala Avala Avala Avala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avala Avala Avala Avala"/>
</div>
<div class="lyrico-lyrics-wrapper">Avalo Pudichiruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avalo Pudichiruku"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku Avalo Pudichiruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku Avalo Pudichiruku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhura Dhura Dhura Dhura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhura Dhura Dhura Dhura"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhura Dhura Dhura Dhura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhura Dhura Dhura Dhura"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhooram Poralae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhooram Poralae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Thooki Poralae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Thooki Poralae"/>
</div>
</pre>
