---
title: "kurrodu perfect song lyrics"
album: "Ego"
artist: "Sai Karthik "
lyricist: "Bhaskarabhatla"
director: "RV Subramanyam"
path: "/albums/ego-lyrics"
song: "Kurrodu Perfect"
image: ../../images/albumart/ego.jpg
date: 2018-01-05
lang: telugu
youtubeLink: "https://www.youtube.com/embed/BJOuruYaeHg"
type: "mass"
singers:
  - Anurag Kulkarni
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">tam tam tam tam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tam tam tam tam"/>
</div>
<div class="lyrico-lyrics-wrapper">tada tada tatta dada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tada tada tatta dada"/>
</div>
<div class="lyrico-lyrics-wrapper">tam tam tam tam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tam tam tam tam"/>
</div>
<div class="lyrico-lyrics-wrapper">tada tada ta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tada tada ta"/>
</div>
<div class="lyrico-lyrics-wrapper">tam tam tam tam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tam tam tam tam"/>
</div>
<div class="lyrico-lyrics-wrapper">tada tada tatta dada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tada tada tatta dada"/>
</div>
<div class="lyrico-lyrics-wrapper">tam tam tam tam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tam tam tam tam"/>
</div>
<div class="lyrico-lyrics-wrapper">tada tada ta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tada tada ta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">tam tam tam tam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tam tam tam tam"/>
</div>
<div class="lyrico-lyrics-wrapper">tada tada tatta dada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tada tada tatta dada"/>
</div>
<div class="lyrico-lyrics-wrapper">tam tam tam tam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tam tam tam tam"/>
</div>
<div class="lyrico-lyrics-wrapper">tada tada ta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tada tada ta"/>
</div>
<div class="lyrico-lyrics-wrapper">tam tam tam tam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tam tam tam tam"/>
</div>
<div class="lyrico-lyrics-wrapper">tada tada tatta dada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tada tada tatta dada"/>
</div>
<div class="lyrico-lyrics-wrapper">tam tam tam tam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tam tam tam tam"/>
</div>
<div class="lyrico-lyrics-wrapper">tada tada ta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tada tada ta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kurradu perfect ro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kurradu perfect ro"/>
</div>
<div class="lyrico-lyrics-wrapper">jet speedani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jet speedani"/>
</div>
<div class="lyrico-lyrics-wrapper">rocketu gaadani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rocketu gaadani"/>
</div>
<div class="lyrico-lyrics-wrapper">talkundile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="talkundile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">tam tam tam tam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tam tam tam tam"/>
</div>
<div class="lyrico-lyrics-wrapper">tada tada tatta dada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tada tada tatta dada"/>
</div>
<div class="lyrico-lyrics-wrapper">tam tam tam tam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tam tam tam tam"/>
</div>
<div class="lyrico-lyrics-wrapper">tada tada ta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tada tada ta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">hey bullodu bullettu ro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey bullodu bullettu ro"/>
</div>
<div class="lyrico-lyrics-wrapper">naatu gaadani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naatu gaadani"/>
</div>
<div class="lyrico-lyrics-wrapper">yama sweetu gaadani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yama sweetu gaadani"/>
</div>
<div class="lyrico-lyrics-wrapper">traze undile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="traze undile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">tam tam tam tam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tam tam tam tam"/>
</div>
<div class="lyrico-lyrics-wrapper">tada tada tatta dada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tada tada tatta dada"/>
</div>
<div class="lyrico-lyrics-wrapper">tam tam tam tam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tam tam tam tam"/>
</div>
<div class="lyrico-lyrics-wrapper">tada tada ta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tada tada ta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oora maasu plus
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oora maasu plus"/>
</div>
<div class="lyrico-lyrics-wrapper">class unna theatre la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="class unna theatre la"/>
</div>
<div class="lyrico-lyrics-wrapper">horu dappu plus
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="horu dappu plus"/>
</div>
<div class="lyrico-lyrics-wrapper">dancelunna thiranallala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dancelunna thiranallala"/>
</div>
<div class="lyrico-lyrics-wrapper">nenedunte aadantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenedunte aadantha"/>
</div>
<div class="lyrico-lyrics-wrapper">sandallu saradhale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sandallu saradhale"/>
</div>
<div class="lyrico-lyrics-wrapper">maare hey maare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maare hey maare"/>
</div>
<div class="lyrico-lyrics-wrapper">naa loni yoru teenmaare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa loni yoru teenmaare"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">tam tam tam tam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tam tam tam tam"/>
</div>
<div class="lyrico-lyrics-wrapper">tada tada tatta dada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tada tada tatta dada"/>
</div>
<div class="lyrico-lyrics-wrapper">tam tam tam tam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tam tam tam tam"/>
</div>
<div class="lyrico-lyrics-wrapper">tada tada ta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tada tada ta"/>
</div>
<div class="lyrico-lyrics-wrapper">tam tam tam tam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tam tam tam tam"/>
</div>
<div class="lyrico-lyrics-wrapper">tada tada tatta dada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tada tada tatta dada"/>
</div>
<div class="lyrico-lyrics-wrapper">tam tam tam tam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tam tam tam tam"/>
</div>
<div class="lyrico-lyrics-wrapper">tada tada ta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tada tada ta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">hey saraa saraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey saraa saraa"/>
</div>
<div class="lyrico-lyrics-wrapper">kathe doose
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathe doose"/>
</div>
<div class="lyrico-lyrics-wrapper">pandem kodi nenele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pandem kodi nenele"/>
</div>
<div class="lyrico-lyrics-wrapper">hey naram naram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey naram naram"/>
</div>
<div class="lyrico-lyrics-wrapper">saayam chese
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saayam chese"/>
</div>
<div class="lyrico-lyrics-wrapper">husharinka naadele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="husharinka naadele"/>
</div>
<div class="lyrico-lyrics-wrapper">aa merupu plus urumu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aa merupu plus urumu"/>
</div>
<div class="lyrico-lyrics-wrapper">unna mirapakayala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna mirapakayala"/>
</div>
<div class="lyrico-lyrics-wrapper">kanne pilla siggu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanne pilla siggu"/>
</div>
<div class="lyrico-lyrics-wrapper">diddukunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="diddukunna"/>
</div>
<div class="lyrico-lyrics-wrapper">rangu muggula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rangu muggula"/>
</div>
<div class="lyrico-lyrics-wrapper">nenedunte aadantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenedunte aadantha"/>
</div>
<div class="lyrico-lyrics-wrapper">sankaranthi pandagale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sankaranthi pandagale"/>
</div>
<div class="lyrico-lyrics-wrapper">maare hey maare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maare hey maare"/>
</div>
<div class="lyrico-lyrics-wrapper">naa maata theeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa maata theeru"/>
</div>
<div class="lyrico-lyrics-wrapper">teen maare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="teen maare"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">tam tam tam tam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tam tam tam tam"/>
</div>
<div class="lyrico-lyrics-wrapper">tada tada tatta dada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tada tada tatta dada"/>
</div>
<div class="lyrico-lyrics-wrapper">tam tam tam tam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tam tam tam tam"/>
</div>
<div class="lyrico-lyrics-wrapper">tada tada ta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tada tada ta"/>
</div>
<div class="lyrico-lyrics-wrapper">tam tam tam tam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tam tam tam tam"/>
</div>
<div class="lyrico-lyrics-wrapper">tada tada tatta dada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tada tada tatta dada"/>
</div>
<div class="lyrico-lyrics-wrapper">tam tam tam tam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tam tam tam tam"/>
</div>
<div class="lyrico-lyrics-wrapper">tada tada ta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tada tada ta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nanda gopaalude 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanda gopaalude "/>
</div>
<div class="lyrico-lyrics-wrapper">brunda govindhude
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="brunda govindhude"/>
</div>
<div class="lyrico-lyrics-wrapper">vaadu vasthadani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaadu vasthadani"/>
</div>
<div class="lyrico-lyrics-wrapper">kanne muddulu vaade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanne muddulu vaade"/>
</div>
<div class="lyrico-lyrics-wrapper">kaanukisthadani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaanukisthadani"/>
</div>
<div class="lyrico-lyrics-wrapper">vadili vasthunnane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vadili vasthunnane"/>
</div>
<div class="lyrico-lyrics-wrapper">kalala deepalane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalala deepalane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ooru vaada vaadu veedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooru vaada vaadu veedu"/>
</div>
<div class="lyrico-lyrics-wrapper">chuttu antha chuttale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chuttu antha chuttale"/>
</div>
<div class="lyrico-lyrics-wrapper">era maava era baava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="era maava era baava"/>
</div>
<div class="lyrico-lyrics-wrapper">antu chuttukuntare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antu chuttukuntare"/>
</div>
<div class="lyrico-lyrics-wrapper">etikedureede pulasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="etikedureede pulasa"/>
</div>
<div class="lyrico-lyrics-wrapper">ruchiki saatedile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ruchiki saatedile"/>
</div>
<div class="lyrico-lyrics-wrapper">kotikokkadanti naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kotikokkadanti naa"/>
</div>
<div class="lyrico-lyrics-wrapper">premaku poti lede
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="premaku poti lede"/>
</div>
<div class="lyrico-lyrics-wrapper">naa unikunna nelantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa unikunna nelantha"/>
</div>
<div class="lyrico-lyrics-wrapper">paduguriki pulakintha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paduguriki pulakintha"/>
</div>
<div class="lyrico-lyrics-wrapper">maare hey maare naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maare hey maare naa"/>
</div>
<div class="lyrico-lyrics-wrapper">pogaru theeru teenmaare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pogaru theeru teenmaare"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">tam tam tam tam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tam tam tam tam"/>
</div>
<div class="lyrico-lyrics-wrapper">tada tada tatta dada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tada tada tatta dada"/>
</div>
<div class="lyrico-lyrics-wrapper">tam tam tam tam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tam tam tam tam"/>
</div>
<div class="lyrico-lyrics-wrapper">tada tada ta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tada tada ta"/>
</div>
<div class="lyrico-lyrics-wrapper">tam tam tam tam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tam tam tam tam"/>
</div>
<div class="lyrico-lyrics-wrapper">tada tada tatta dada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tada tada tatta dada"/>
</div>
<div class="lyrico-lyrics-wrapper">tam tam tam tam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tam tam tam tam"/>
</div>
<div class="lyrico-lyrics-wrapper">tada tada ta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tada tada ta"/>
</div>
</pre>
