---
title: "boss song song lyrics"
album: "Vidhi Madhi Ultaa"
artist: "Ashwin Vinayagamoorthy"
lyricist: "Kabilan"
director: "Vijai Balaji"
path: "/albums/vidhi-madhi-ultaa-lyrics"
song: "Boss Song"
image: ../../images/albumart/vidhi-madhi-ultaa.jpg
date: 2018-01-05
lang: tamil
youtubeLink: "https://www.youtube.com/embed/nVgrSQYjN1o"
type: "gana"
singers:
  - Gana Bala
  - Anthony Daasan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Thaaa na na na na naaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaa na na na na naaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaaa na na na na naaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaa na na na na naaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaaa na na na na naaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaa na na na na naaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaaa na na na na naaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaa na na na na naaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Annan dhaan da don-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annan dhaan da don-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatadha nee scene-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatadha nee scene-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Annan kayyil vachikuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annan kayyil vachikuna"/>
</div>
108 <div class="lyrico-lyrics-wrapper">van-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="van-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hoi annan dhaan da don-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoi annan dhaan da don-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatadha nee scene-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatadha nee scene-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Annan kayyil vachikuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annan kayyil vachikuna"/>
</div>
108 <div class="lyrico-lyrics-wrapper">van-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="van-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nethu varaikkum dummy piece-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nethu varaikkum dummy piece-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Innikku naa semma mass-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innikku naa semma mass-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Kilichu pota rendae piece-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kilichu pota rendae piece-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Boss-u boss-u mass-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boss-u boss-u mass-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nethu varaikkum dummy piece-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nethu varaikkum dummy piece-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Innikku naa semma mass-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innikku naa semma mass-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Kilichu pota rendae piece-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kilichu pota rendae piece-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Boss-u boss-u mass-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boss-u boss-u mass-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaaa na na na na naaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaa na na na na naaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaaa na na na na naaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaa na na na na naaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaaa na na na na naaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaa na na na na naaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaaa na na na na naaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaa na na na na naaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un nenjukkulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un nenjukkulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Bayam irukkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bayam irukkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">En selaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En selaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan therakkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan therakkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pera solli oorae koodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pera solli oorae koodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Koothaadanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koothaadanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeh kaththi mona kaiyaala naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh kaththi mona kaiyaala naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannathula pooran uttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannathula pooran uttu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuppakkiyal nethimelae heh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuppakkiyal nethimelae heh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Annanoda seetu kattil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annanoda seetu kattil"/>
</div>
<div class="lyrico-lyrics-wrapper">Raani mattum dhaan da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raani mattum dhaan da"/>
</div>
<div class="lyrico-lyrics-wrapper">Andhapura kaavalargal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhapura kaavalargal"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga mattum dhaan da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga mattum dhaan da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yengalukkul eppodhumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yengalukkul eppodhumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sanda illa poda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sanda illa poda"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathi kathi karuthu sonna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathi kathi karuthu sonna"/>
</div>
<div class="lyrico-lyrics-wrapper">Kabaliku soda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabaliku soda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaaki satta veratti meratti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaki satta veratti meratti"/>
</div>
<div class="lyrico-lyrics-wrapper">Urutti poratti adichu poda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urutti poratti adichu poda"/>
</div>
<div class="lyrico-lyrics-wrapper">Engakitta edukka pudikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engakitta edukka pudikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Siriya thuruppam sikkaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siriya thuruppam sikkaadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaaa na na na na naaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaa na na na na naaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaaa na na na na naaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaa na na na na naaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaaki satta veratti meratti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaki satta veratti meratti"/>
</div>
<div class="lyrico-lyrics-wrapper">Urutti poratti adichu poda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urutti poratti adichu poda"/>
</div>
<div class="lyrico-lyrics-wrapper">Engakitta edukka pudikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engakitta edukka pudikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Siriya thuruppam sikkaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siriya thuruppam sikkaadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aadu oodu katti aadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadu oodu katti aadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Podu kuthattam podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podu kuthattam podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Odu oora vittu odu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odu oora vittu odu"/>
</div>
<div class="lyrico-lyrics-wrapper">En kitta nee nikkadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kitta nee nikkadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan jallikattu maadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan jallikattu maadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaaa na na na na naaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaa na na na na naaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaaa na na na na naaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaa na na na na naaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaaa na na na na naaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaa na na na na naaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaaa na na na na naaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaa na na na na naaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Annan dhaan da don-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annan dhaan da don-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatadha nee scene-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatadha nee scene-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Annan kayyil vachikuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annan kayyil vachikuna"/>
</div>
108 <div class="lyrico-lyrics-wrapper">van-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="van-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeh annan dhaan da don-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh annan dhaan da don-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Don-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Don-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatadha nee scene-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatadha nee scene-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Scene-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Scene-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Annan kayyil vachikuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annan kayyil vachikuna"/>
</div>
108 <div class="lyrico-lyrics-wrapper">van-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="van-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Van-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Van-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nethu varaikkum dummy piece-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nethu varaikkum dummy piece-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Innikku naa semma mass-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innikku naa semma mass-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Kilichu pota rendae piece-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kilichu pota rendae piece-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Boss-u boss-u mass-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boss-u boss-u mass-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nethu varaikkum dummy piece-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nethu varaikkum dummy piece-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Innikku naa semma mass-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innikku naa semma mass-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Kilichu pota rendae piece-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kilichu pota rendae piece-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Boss-u boss-u mass-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boss-u boss-u mass-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aadu. oodu katti aadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadu. oodu katti aadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Podu kuthattam podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podu kuthattam podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Odu oora vittu odu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odu oora vittu odu"/>
</div>
<div class="lyrico-lyrics-wrapper">En kitta nee nikkadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kitta nee nikkadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan jallikattu maadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan jallikattu maadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaaa na na na na naaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaa na na na na naaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaaa na na na na naaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaa na na na na naaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaaa na na na na naaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaa na na na na naaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaaa na na na na naaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaa na na na na naaaaa"/>
</div>
</pre>
