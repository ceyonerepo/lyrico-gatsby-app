---
title: "thee thee theeyena song lyrics"
album: "Kaathiruppor Pattiyal"
artist: "Sean Roldan"
lyricist: "Sean Roldan"
director: "Balaiya D. Rajasekhar"
path: "/albums/kaathiruppor-pattiyal-song-lyrics"
song: "Thee Thee Theeyena"
image: ../../images/albumart/kaathiruppor-pattiyal.jpg
date: 2018-05-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/5qKndvchrUQ"
type: "love"
singers:
  - Adhithya Rao
  - Shakthisree Gopalan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">thee thee thee yena molaithal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thee thee thee yena molaithal"/>
</div>
<div class="lyrico-lyrics-wrapper">poo poo poovena sirithal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poo poo poovena sirithal"/>
</div>
<div class="lyrico-lyrics-wrapper">boomiyil piranthaval
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="boomiyil piranthaval"/>
</div>
<div class="lyrico-lyrics-wrapper">enakena ival thane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enakena ival thane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kalai kanavugal koduthal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalai kanavugal koduthal"/>
</div>
<div class="lyrico-lyrics-wrapper">maalai mugavari maraithal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maalai mugavari maraithal"/>
</div>
<div class="lyrico-lyrics-wrapper">kathalil vizhuthathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathalil vizhuthathum"/>
</div>
<div class="lyrico-lyrics-wrapper">katrena aanene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="katrena aanene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thee thee thee yena molaithal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thee thee thee yena molaithal"/>
</div>
<div class="lyrico-lyrics-wrapper">poo poo poovena sirithal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poo poo poovena sirithal"/>
</div>
<div class="lyrico-lyrics-wrapper">boomiyil piranthaval
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="boomiyil piranthaval"/>
</div>
<div class="lyrico-lyrics-wrapper">enakena ival thane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enakena ival thane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nan nan thaniyena thirinthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan nan thaniyena thirinthen"/>
</div>
<div class="lyrico-lyrics-wrapper">nee nee manathukul nulaithai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee nee manathukul nulaithai"/>
</div>
<div class="lyrico-lyrics-wrapper">thee yena erithaval 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thee yena erithaval "/>
</div>
<div class="lyrico-lyrics-wrapper">panithuli aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="panithuli aanen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">unnai parthathum aasai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai parthathum aasai "/>
</div>
<div class="lyrico-lyrics-wrapper">kooduthe ennai adithalum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kooduthe ennai adithalum "/>
</div>
<div class="lyrico-lyrics-wrapper">anaithalum unathaven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anaithalum unathaven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">solli theerkave illai varthaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="solli theerkave illai varthaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">unthan idhal parthu manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unthan idhal parthu manam"/>
</div>
<div class="lyrico-lyrics-wrapper">thedum mounam mounam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thedum mounam mounam "/>
</div>
<div class="lyrico-lyrics-wrapper">sadhithen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sadhithen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thee thee thee yena molaithal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thee thee thee yena molaithal"/>
</div>
<div class="lyrico-lyrics-wrapper">poo poo poovena sirithal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poo poo poovena sirithal"/>
</div>
<div class="lyrico-lyrics-wrapper">boomiyil piranthaval
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="boomiyil piranthaval"/>
</div>
<div class="lyrico-lyrics-wrapper">enakena ival thane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enakena ival thane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kalai kanavugal koduthal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalai kanavugal koduthal"/>
</div>
<div class="lyrico-lyrics-wrapper">maalai mugavari maraithal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maalai mugavari maraithal"/>
</div>
<div class="lyrico-lyrics-wrapper">kathalil vizhuthathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathalil vizhuthathum"/>
</div>
<div class="lyrico-lyrics-wrapper">katrena aanene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="katrena aanene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">unnal nanum norungida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnal nanum norungida"/>
</div>
<div class="lyrico-lyrics-wrapper">bodhai pesum mozhigail
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bodhai pesum mozhigail"/>
</div>
<div class="lyrico-lyrics-wrapper">solven solven enakene unnai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="solven solven enakene unnai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ennai neeyum nerungida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennai neeyum nerungida"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhal pesum vizhigail
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhal pesum vizhigail"/>
</div>
<div class="lyrico-lyrics-wrapper">kanden kanden unakena ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanden kanden unakena ennai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ennil neyum unnil nanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennil neyum unnil nanum"/>
</div>
<div class="lyrico-lyrics-wrapper">yedhai thedinom yedhai serave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yedhai thedinom yedhai serave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thee thee thee yena molaithal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thee thee thee yena molaithal"/>
</div>
<div class="lyrico-lyrics-wrapper">poo poo poovena sirithal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poo poo poovena sirithal"/>
</div>
<div class="lyrico-lyrics-wrapper">boomiyil piranthaval
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="boomiyil piranthaval"/>
</div>
<div class="lyrico-lyrics-wrapper">enakena ival thane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enakena ival thane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kalai kanavugal koduthal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalai kanavugal koduthal"/>
</div>
<div class="lyrico-lyrics-wrapper">maalai mugavari maraithal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maalai mugavari maraithal"/>
</div>
<div class="lyrico-lyrics-wrapper">kathalil vizhuthathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathalil vizhuthathum"/>
</div>
<div class="lyrico-lyrics-wrapper">katrena aanene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="katrena aanene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">unnai parthathum aasai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai parthathum aasai "/>
</div>
<div class="lyrico-lyrics-wrapper">kooduthe ennai adithalum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kooduthe ennai adithalum "/>
</div>
<div class="lyrico-lyrics-wrapper">anaithalum unathaven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anaithalum unathaven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">solli theerkave illai varthaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="solli theerkave illai varthaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">unthan idhal parthu manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unthan idhal parthu manam"/>
</div>
<div class="lyrico-lyrics-wrapper">thedum mounam mounam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thedum mounam mounam "/>
</div>
<div class="lyrico-lyrics-wrapper">sadhithen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sadhithen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thee thee thee yena molaithal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thee thee thee yena molaithal"/>
</div>
<div class="lyrico-lyrics-wrapper">poo poo poovena sirithal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poo poo poovena sirithal"/>
</div>
<div class="lyrico-lyrics-wrapper">boomiyil piranthaval
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="boomiyil piranthaval"/>
</div>
<div class="lyrico-lyrics-wrapper">enakena ival thane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enakena ival thane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kalai kanavugal koduthal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalai kanavugal koduthal"/>
</div>
<div class="lyrico-lyrics-wrapper">maalai mugavari maraithal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maalai mugavari maraithal"/>
</div>
<div class="lyrico-lyrics-wrapper">kathalil vizhuthathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathalil vizhuthathum"/>
</div>
<div class="lyrico-lyrics-wrapper">katrena aanene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="katrena aanene"/>
</div>
</pre>
