---
title: "maya maya song lyrics"
album: "Sarvam Thaala Mayam"
artist: "A. R. Rahman"
lyricist: "Na. Muthukumar"
director: "Rajiv Menon"
path: "/albums/sarvam-thaala-mayam-lyrics"
song: "Maya Maya"
image: ../../images/albumart/sarvam-thaala-mayam.jpg
date: 2019-02-01
lang: tamil
youtubeLink: "https://www.youtube.com/embed/4BueIUDPriY"
type: "love"
singers:
  - Chinmayi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Maaya Maaya Manamohana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaya Maaya Manamohana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unakaaga Priandhenadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakaaga Priandhenadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maaya Maaya Manamohana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaya Maaya Manamohana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unakkaaga Pirandhenadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkaaga Pirandhenadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Madiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Madiyil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Pon Nodiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Pon Nodiyil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Saainthaal Podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Saainthaal Podhum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Viraivil Varuvaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viraivil Varuvaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Nenaipin Irulil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Nenaipin Irulil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Per Anbin Ozhiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Per Anbin Ozhiyil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Vaazhnthaal Podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Vaazhnthaal Podhum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Viraivil Varuvaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viraivil Varuvaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Kaalangal Theernthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Kaalangal Theernthaalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endrum Theeratha Kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endrum Theeratha Kaadhal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kai Yenthi Ketteney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai Yenthi Ketteney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maya Maaya Manamohanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maya Maaya Manamohanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unakkaaga Piranthenadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkaaga Piranthenadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sedi Kodi Ilai Uranga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sedi Kodi Ilai Uranga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sinungidum Nadhi Uranga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sinungidum Nadhi Uranga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mazhaigalil Mugil Uranga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhaigalil Mugil Uranga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mai Vizhi Mattum Keranga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mai Vizhi Mattum Keranga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yegaantha Iravum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yegaantha Iravum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Erigindra Nilavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Erigindra Nilavum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanimayil Vaattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanimayil Vaattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaatum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaatum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Kaadhal Vidhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kaadhal Vidhai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Kaadhal Mazhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kaadhal Mazhai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Thooral Thanthaal Ingey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Thooral Thanthaal Ingey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Anbe Naan Vaazhveney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Anbe Naan Vaazhveney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maaya Maaya Manamohana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaya Maaya Manamohana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unakkaaha Priandhenda Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkaaha Priandhenda Oh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhayathil Oru Paravai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayathil Oru Paravai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Virikidhu Athan Siragai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Virikidhu Athan Siragai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Siragalla Athu Siluvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siragalla Athu Siluvai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyir Peru Kodu Uravai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Peru Kodu Uravai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanaththai Alanthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanaththai Alanthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Megaththai Pilanthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megaththai Pilanthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhalai Kaatril
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalai Kaatril"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidhaithen Oh Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidhaithen Oh Oh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Koottin Arai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Koottin Arai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Maarbil Amai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Maarbil Amai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Swaasam Sernthaal Ingey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Swaasam Sernthaal Ingey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Anbe Naan Vaazhven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Anbe Naan Vaazhven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maaya Maaya Manamohana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaya Maaya Manamohana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unakkaaga Pirandhenadaa Ohh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkaaga Pirandhenadaa Ohh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maaya Maaya Manamohana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaya Maaya Manamohana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unakkaaga Pirandhenadaa Ohh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkaaga Pirandhenadaa Ohh"/>
</div>
</pre>
