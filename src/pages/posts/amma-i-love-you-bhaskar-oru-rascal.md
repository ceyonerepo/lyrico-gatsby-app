---
title: "amma i love you song lyrics"
album: "Bhaskar Oru Rascal"
artist: "Amresh Ganesh"
lyricist: "Pa. Vijay"
director: "Siddique"
path: "/albums/bhaskar-oru-rascal-lyrics"
song: "Amma I Love You"
image: ../../images/albumart/bhaskar-oru-rascal.jpg
date: 2018-05-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/3K9geFQpVqs"
type: "happy"
singers:
  - Shreya Ghoshal
  - Sreya Jayadeep
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ahaaa ahaaa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahaaa ahaaa aaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ohh amma amma amma neeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohh amma amma amma neeyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbin uruvam neeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbin uruvam neeyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagil ulagil un pol sondham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagil ulagil un pol sondham"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarum illaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarum illaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Amma i love you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma i love you"/>
</div>
<div class="lyrico-lyrics-wrapper">Mummy i love you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mummy i love you"/>
</div>
<div class="lyrico-lyrics-wrapper">Amma i love you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma i love you"/>
</div>
<div class="lyrico-lyrics-wrapper">Mummy i love you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mummy i love you"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ohhh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohhh"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai ennai konji parkum azhagae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai ennai konji parkum azhagae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai unnai konji theerkava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai unnai konji theerkava"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhan kaiyil indru neeyum kuzhandhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan kaiyil indru neeyum kuzhandhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pola marida vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pola marida vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee pirandhathil naanum ingae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee pirandhathil naanum ingae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaaiyai pirandhaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaiyai pirandhaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee aadidum oonjalaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee aadidum oonjalaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhan nenjam yengudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan nenjam yengudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Amma i love you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma i love you"/>
</div>
<div class="lyrico-lyrics-wrapper">Mummy i love you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mummy i love you"/>
</div>
<div class="lyrico-lyrics-wrapper">Amma i love you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma i love you"/>
</div>
<div class="lyrico-lyrics-wrapper">Mummy i love you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mummy i love you"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaahaaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaahaaaaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan seiyyum kurumbugal rasikkiriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan seiyyum kurumbugal rasikkiriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan tharum valiyilum sirikkiriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan tharum valiyilum sirikkiriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Poi kobathai kottum podhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poi kobathai kottum podhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam konjam nadikkiriyae aeaeae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam konjam nadikkiriyae aeaeae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Isai nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Isai nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennil kanneerthuli nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennil kanneerthuli nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Mun pagal oli neee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mun pagal oli neee"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir nee endhanPunmuruvalum nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir nee endhanPunmuruvalum nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar nee endhan thol neeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar nee endhan thol neeyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Amma i love you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma i love you"/>
</div>
<div class="lyrico-lyrics-wrapper">Mummy i love you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mummy i love you"/>
</div>
<div class="lyrico-lyrics-wrapper">Amma i love you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma i love you"/>
</div>
<div class="lyrico-lyrics-wrapper">Mummy i love you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mummy i love you"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ahaaaaaa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahaaaaaa aaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ahaaaaaaaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahaaaaaaaaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ahaaaaaa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahaaaaaa aaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ahaaaaaaaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahaaaaaaaaaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa mummy fairy tale story solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa mummy fairy tale story solla"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanavil koodavae walking sella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanavil koodavae walking sella"/>
</div>
<div class="lyrico-lyrics-wrapper">Naam sernthingu podura loottikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam sernthingu podura loottikku"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellai illa aaaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellai illa aaaaaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Honeyma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Honeyma"/>
</div>
<div class="lyrico-lyrics-wrapper">Innum nee solladiyamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum nee solladiyamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee innum suguma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee innum suguma"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhatti konji nee sirikkaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhatti konji nee sirikkaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Seiyaa nee thaaya soll
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seiyaa nee thaaya soll"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai ennai konji parkum azhagae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai ennai konji parkum azhagae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai unnai konji theerkava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai unnai konji theerkava"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhan kaiyil indru neeyum kuzhandhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan kaiyil indru neeyum kuzhandhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pola marida vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pola marida vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee pirandhathil naanum ingae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee pirandhathil naanum ingae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaayai pirandhaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaayai pirandhaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee aadidum oonjalaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee aadidum oonjalaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhan nenjam yengudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan nenjam yengudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Amma i love you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma i love you"/>
</div>
<div class="lyrico-lyrics-wrapper">Mummy i love you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mummy i love you"/>
</div>
<div class="lyrico-lyrics-wrapper">Amma i love you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma i love you"/>
</div>
<div class="lyrico-lyrics-wrapper">Mummy i love you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mummy i love you"/>
</div>
</pre>
