---
title: "woo aa aha aha song lyrics"
album: "F3"
artist: "Devi Sri Prasad"
lyricist: "Kasarla Shyam"
director: "Anil Ravipudi"
path: "/albums/f3-lyrics"
song: "Woo Aa Aha Aha"
image: ../../images/albumart/f3.jpg
date: 2022-05-27
lang: telugu
youtubeLink: "https://www.youtube.com/embed/ogerst0UQsk"
type: "happy"
singers:
  -	Sagar
  - Sunidhi Chauhan
  - Lavita Lobo
  - SP Abhishek
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Woo aa aha aha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Woo aa aha aha"/>
</div>
<div class="lyrico-lyrics-wrapper">Woo aa aha aha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Woo aa aha aha"/>
</div>
<div class="lyrico-lyrics-wrapper">Woo aa aha aha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Woo aa aha aha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee korameesam chusthuntey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee korameesam chusthuntey"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvatta thippesthuntey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvatta thippesthuntey"/>
</div>
<div class="lyrico-lyrics-wrapper">Woo aa aha aha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Woo aa aha aha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee maanly lukke chusthuntey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee maanly lukke chusthuntey"/>
</div>
<div class="lyrico-lyrics-wrapper">Moon walk chese naa heart ey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moon walk chese naa heart ey"/>
</div>
<div class="lyrico-lyrics-wrapper">Woo aa aha aha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Woo aa aha aha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">F1 race caaralle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="F1 race caaralle"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakkaa strong body
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkaa strong body"/>
</div>
<div class="lyrico-lyrics-wrapper">Rai rai mantu raathri kalallo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rai rai mantu raathri kalallo"/>
</div>
<div class="lyrico-lyrics-wrapper">Chesthunna dhaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chesthunna dhaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vuf vuf oodhesthunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vuf vuf oodhesthunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaggatledhe vedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaggatledhe vedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhuke ledi singam la nenoo ready
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhuke ledi singam la nenoo ready"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Everybody put your body
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Everybody put your body"/>
</div>
<div class="lyrico-lyrics-wrapper">On the floor and say
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="On the floor and say"/>
</div>
<div class="lyrico-lyrics-wrapper">Woo aa aha aha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Woo aa aha aha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Everybody put your body
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Everybody put your body"/>
</div>
<div class="lyrico-lyrics-wrapper">On the floor and say
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="On the floor and say"/>
</div>
<div class="lyrico-lyrics-wrapper">Woo aa aha aha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Woo aa aha aha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">French winu nee skinnu tonu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="French winu nee skinnu tonu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvu twinnu brothero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvu twinnu brothero"/>
</div>
<div class="lyrico-lyrics-wrapper">Emo manmadhunike aha aha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emo manmadhunike aha aha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En chilled gunna naa tight kopu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En chilled gunna naa tight kopu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh dinglone soku daajamake aha aha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh dinglone soku daajamake aha aha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethil lada nattam mathikatirgeji
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethil lada nattam mathikatirgeji"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethi crushu ni tho chaka chaka adiskaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethi crushu ni tho chaka chaka adiskaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Junnoo mukka ninnu jinnulo munchesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Junnoo mukka ninnu jinnulo munchesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Tastu chusi jaldi kasa kasa karugestho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tastu chusi jaldi kasa kasa karugestho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Everybody put your body
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Everybody put your body"/>
</div>
<div class="lyrico-lyrics-wrapper">On the floor and say
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="On the floor and say"/>
</div>
<div class="lyrico-lyrics-wrapper">Woo aa aha aha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Woo aa aha aha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Everybody put your body yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Everybody put your body yeah"/>
</div>
<div class="lyrico-lyrics-wrapper">On the floor and say
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="On the floor and say"/>
</div>
<div class="lyrico-lyrics-wrapper">Woo aa aha aha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Woo aa aha aha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee touch chaalu oh tannu poolu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee touch chaalu oh tannu poolu"/>
</div>
<div class="lyrico-lyrics-wrapper">Stennu gunnu thoti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Stennu gunnu thoti"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannu pelchinatte aha aha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannu pelchinatte aha aha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa kannavesi oh spin ball
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa kannavesi oh spin ball"/>
</div>
<div class="lyrico-lyrics-wrapper">Ni sanna nadme
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ni sanna nadme"/>
</div>
<div class="lyrico-lyrics-wrapper">Batting chesthnande aha aha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Batting chesthnande aha aha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aa aa ee ee antu chakkadamadlitti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa aa ee ee antu chakkadamadlitti"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedu zeddu ninne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedu zeddu ninne"/>
</div>
<div class="lyrico-lyrics-wrapper">Chaka chaka chadyestha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chaka chaka chadyestha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Zero size chusaavante raathiriki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zero size chusaavante raathiriki"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhaa markul vesthav
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhaa markul vesthav"/>
</div>
<div class="lyrico-lyrics-wrapper">Padha padha gadhiki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padha padha gadhiki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Everybody put your body
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Everybody put your body"/>
</div>
<div class="lyrico-lyrics-wrapper">On the floor and say
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="On the floor and say"/>
</div>
<div class="lyrico-lyrics-wrapper">Woo aa aha aha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Woo aa aha aha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Everybody put your body yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Everybody put your body yeah"/>
</div>
<div class="lyrico-lyrics-wrapper">On the floor and say
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="On the floor and say"/>
</div>
<div class="lyrico-lyrics-wrapper">Woo aa aha aha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Woo aa aha aha"/>
</div>
<div class="lyrico-lyrics-wrapper">Woo aa aha aha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Woo aa aha aha "/>
</div>
</pre>
