---
title: "kol kol song lyrics"
album: "Taish"
artist: "Raghav Sachar"
lyricist: "Rohit Sharma"
director: "Bejoy Nambiar"
path: "/albums/taish-lyrics"
song: "Kol Kol"
image: ../../images/albumart/taish.jpg
date: 2020-10-29
lang: hindi
youtubeLink: "https://www.youtube.com/embed/fewLx2KtlyU"
type: "love"
singers:
  - Jyotica Tangri
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Mainu Kol Kol Rehn De
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mainu Kol Kol Rehn De"/>
</div>
<div class="lyrico-lyrics-wrapper">Mainu Kol Kol Rehn De
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mainu Kol Kol Rehn De"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuch Dard Ne Ishq De Kehan De
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuch Dard Ne Ishq De Kehan De"/>
</div>
<div class="lyrico-lyrics-wrapper">Mainu Kol Kol Rehan De
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mainu Kol Kol Rehan De"/>
</div>
<div class="lyrico-lyrics-wrapper">Mainu Kol Kol Rehan De
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mainu Kol Kol Rehan De"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuch Dard Ne Ishq De Kehn De
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuch Dard Ne Ishq De Kehn De"/>
</div>
<div class="lyrico-lyrics-wrapper">Channa Rajj Ke Main Tainu Vekhlaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Channa Rajj Ke Main Tainu Vekhlaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Tainu Saahan Ch Bhar Laan Seklaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tainu Saahan Ch Bhar Laan Seklaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Channa Rajj Ke Main Tainu Vekhlaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Channa Rajj Ke Main Tainu Vekhlaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Tainu Saahan Ch Bhar Laan Seklaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tainu Saahan Ch Bhar Laan Seklaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tainu Jee Laan Main Roohan Talak
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tainu Jee Laan Main Roohan Talak"/>
</div>
<div class="lyrico-lyrics-wrapper">Jhapkan Na Ik Pal Palak
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jhapkan Na Ik Pal Palak"/>
</div>
<div class="lyrico-lyrics-wrapper">Seene Rakh Ke Sir Ro Lain De
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seene Rakh Ke Sir Ro Lain De"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mainu Kol Kol Rehn De
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mainu Kol Kol Rehn De"/>
</div>
<div class="lyrico-lyrics-wrapper">Mainu Kol Kol Rehn De
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mainu Kol Kol Rehn De"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuch Dard Ne Ishq De Kehan De
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuch Dard Ne Ishq De Kehan De"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mainu Kol Kol Rehn De
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mainu Kol Kol Rehn De"/>
</div>
<div class="lyrico-lyrics-wrapper">Mainu Kol Kol Rehn De
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mainu Kol Kol Rehn De"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuch Dard Ne Ishq De Kehan De
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuch Dard Ne Ishq De Kehan De"/>
</div>
</pre>
