---
title: 'adada mazhai da song lyrics'
album: 'Paiya'
artist: 'Yuvan Shankar Raja'
lyricist: 'Na Muthukumar'
director: 'N.Lingusamy'
path: '/albums/paiya-song-lyrics'
song: 'Adada Mazhai da'
image: ../../images/albumart/paiya.jpg
date: 2010-04-02
singers: 
- Rahul Nambiar
- Saindhavi
lang: tamil
youtubeLink: "https://www.youtube.com/embed/IngvLGgX-YU"
type: 'love'
---

<pre class="lyrics-native">
    Coming Soon
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Thantha nae thantha nanae
<input type="checkbox" class="lyrico-select-lyric-line" value="Thantha nae thantha nanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thantha naanae thana nae
<input type="checkbox" class="lyrico-select-lyric-line" value="Thantha naanae thana nae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thantha nae thantha nanae
<input type="checkbox" class="lyrico-select-lyric-line" value="Thantha nae thantha nanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thantha nanae naaaaa……
<input type="checkbox" class="lyrico-select-lyric-line" value="Thantha nanae naaaaa……"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Adadaa mazhaidaa adai mazhaidaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Adadaa mazhaidaa adai mazhaidaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagaa sirichaa puyal mazhaidaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Azhagaa sirichaa puyal mazhaidaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adadaa mazhaidaa adai mazhaidaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Adadaa mazhaidaa adai mazhaidaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagaa sirichaa puyal mazhaidaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Azhagaa sirichaa puyal mazhaidaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Maari maari mazhaiyadikka
<input type="checkbox" class="lyrico-select-lyric-line" value="Maari maari mazhaiyadikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasukkulla kodaipidikka
<input type="checkbox" class="lyrico-select-lyric-line" value="Manasukkulla kodaipidikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalgal naalaachu kaigal ettaachu
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaalgal naalaachu kaigal ettaachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna aachu edhaachu edhedho aayaachu
<input type="checkbox" class="lyrico-select-lyric-line" value="Enna aachu edhaachu edhedho aayaachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Mayil thogai pola
<input type="checkbox" class="lyrico-select-lyric-line" value="Mayil thogai pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Iva mazhaiyil aadumbodhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Iva mazhaiyil aadumbodhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Rayil paalam pola
<input type="checkbox" class="lyrico-select-lyric-line" value="Rayil paalam pola"/>
</div>
<div class="lyrico-lyrics-wrapper">En manasum aadum paaru
<input type="checkbox" class="lyrico-select-lyric-line" value="En manasum aadum paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna aachu edhaachu edhedho aayaachu
<input type="checkbox" class="lyrico-select-lyric-line" value="Enna aachu edhaachu edhedho aayaachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Adadaa mazhaidaa adai mazhaidaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Adadaa mazhaidaa adai mazhaidaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagaa sirichaa puyal mazhaidaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Azhagaa sirichaa puyal mazhaidaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thana nan nan nana
<input type="checkbox" class="lyrico-select-lyric-line" value="Thana nan nan nana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana nan nan nana
<input type="checkbox" class="lyrico-select-lyric-line" value="Thana nan nan nana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana nan nan nana …na na na na….
<input type="checkbox" class="lyrico-select-lyric-line" value="Thana nan nan nana …na na na na…."/>
</div>
<div class="lyrico-lyrics-wrapper">Thana nan nan nana
<input type="checkbox" class="lyrico-select-lyric-line" value="Thana nan nan nana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana nan nan nana
<input type="checkbox" class="lyrico-select-lyric-line" value="Thana nan nan nana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana nan nan nana …na na na na….aaaa…
<input type="checkbox" class="lyrico-select-lyric-line" value="Thana nan nan nana …na na na na….aaaa…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Paattu paattu paadaadha paattu
<input type="checkbox" class="lyrico-select-lyric-line" value="Paattu paattu paadaadha paattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhaithaan paadudhu ketkaadhappaattu
<input type="checkbox" class="lyrico-select-lyric-line" value="Mazhaithaan paadudhu ketkaadhappaattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai ennai serthu vacha
<input type="checkbox" class="lyrico-select-lyric-line" value="Unnai ennai serthu vacha"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhaikkoru salaam podu
<input type="checkbox" class="lyrico-select-lyric-line" value="Mazhaikkoru salaam podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai konjam kaanalayae
<input type="checkbox" class="lyrico-select-lyric-line" value="Ennai konjam kaanalayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkulla thedippaaru
<input type="checkbox" class="lyrico-select-lyric-line" value="Unakkulla thedippaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Mandhiram pola irukku
<input type="checkbox" class="lyrico-select-lyric-line" value="Mandhiram pola irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu thandhiram pola irukku
<input type="checkbox" class="lyrico-select-lyric-line" value="Pudhu thandhiram pola irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Bambaram pola enakku
<input type="checkbox" class="lyrico-select-lyric-line" value="Bambaram pola enakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalaimathiyil suthudhu kirukku
<input type="checkbox" class="lyrico-select-lyric-line" value="Thalaimathiyil suthudhu kirukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhevadhai engae en dhevadhai engae
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhevadhai engae en dhevadhai engae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu santhoshamaai aadudhu ingae
<input type="checkbox" class="lyrico-select-lyric-line" value="Adhu santhoshamaai aadudhu ingae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Onnappola veraarum illa
<input type="checkbox" class="lyrico-select-lyric-line" value="Onnappola veraarum illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaivittaa veraaru solla
<input type="checkbox" class="lyrico-select-lyric-line" value="Ennaivittaa veraaru solla"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinna chinna kannu rendu
<input type="checkbox" class="lyrico-select-lyric-line" value="Chinna chinna kannu rendu"/>
</div>
<div class="lyrico-lyrics-wrapper">Koduththenna anuppivachaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Koduththenna anuppivachaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha kannum podhalaiyae
<input type="checkbox" class="lyrico-select-lyric-line" value="Indha kannum podhalaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhukkivalai padaichu vachaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Edhukkivalai padaichu vachaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Pattaamboochi ponnu
<input type="checkbox" class="lyrico-select-lyric-line" value="Pattaamboochi ponnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil padapadakkum ninnu
<input type="checkbox" class="lyrico-select-lyric-line" value="Nenjil padapadakkum ninnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Poovum ivalum onnu
<input type="checkbox" class="lyrico-select-lyric-line" value="Poovum ivalum onnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai konnupputtaa konnu
<input type="checkbox" class="lyrico-select-lyric-line" value="Ennai konnupputtaa konnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Povadhu engae naan povadhu engae
<input type="checkbox" class="lyrico-select-lyric-line" value="Povadhu engae naan povadhu engae"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam thallaadhudhae boadhaiyil ingae
<input type="checkbox" class="lyrico-select-lyric-line" value="Manam thallaadhudhae boadhaiyil ingae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Adadaa mazhaidaa adai mazhaidaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Adadaa mazhaidaa adai mazhaidaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagaa sirichaa anal mazhaidaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Azhagaa sirichaa anal mazhaidaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adadaa mazhaidaa adai mazhaidaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Adadaa mazhaidaa adai mazhaidaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagaa sirichaa anal mazhaidaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Azhagaa sirichaa anal mazhaidaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Pinni pinni mazhai adikka
<input type="checkbox" class="lyrico-select-lyric-line" value="Pinni pinni mazhai adikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Minnal vanthu kodai pidikka
<input type="checkbox" class="lyrico-select-lyric-line" value="Minnal vanthu kodai pidikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanam rendu aachu bhoomi thundaachu
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaanam rendu aachu bhoomi thundaachu"/>
</div>
<div class="lyrico-lyrics-wrapper">En moochu kaathaala mazhai kooda sudaachu
<input type="checkbox" class="lyrico-select-lyric-line" value="En moochu kaathaala mazhai kooda sudaachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kudaiyai neeti yaarum
<input type="checkbox" class="lyrico-select-lyric-line" value="Kudaiyai neeti yaarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha mazhaiyai thadukka venam
<input type="checkbox" class="lyrico-select-lyric-line" value="Intha mazhaiyai thadukka venam"/>
</div>
<div class="lyrico-lyrics-wrapper">Anaiya pottu yaarum
<input type="checkbox" class="lyrico-select-lyric-line" value="Anaiya pottu yaarum"/>
</div>
<div class="lyrico-lyrics-wrapper">En manasa adakka venam
<input type="checkbox" class="lyrico-select-lyric-line" value="En manasa adakka venam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kondaaduu kondaadu koothaadi kondaadu..
<input type="checkbox" class="lyrico-select-lyric-line" value="Kondaaduu kondaadu koothaadi kondaadu.."/>
</div>
</pre>