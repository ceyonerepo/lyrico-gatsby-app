---
title: "pori pathi vizhum song lyrics"
album: "Rum"
artist: "Anirudh Ravichander"
lyricist: "	Vivek - Divine"
director: "Sai Bharath"
path: "/albums/rum-lyrics"
song: "Pori Pathi Vizhum"
image: ../../images/albumart/rum.jpg
date: 2017-02-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/419h1YJT2ew"
type: "mass"
singers:
  - Diwakar
  - Divine
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yethu Da Kaiyoda Eduthu Vanthathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethu Da Kaiyoda Eduthu Vanthathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellam Thirudi Nee Saethathu Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellam Thirudi Nee Saethathu Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nirvaanamaa Than Vizhudha Boomiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nirvaanamaa Than Vizhudha Boomiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellam Valachu Nee Potathu Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellam Valachu Nee Potathu Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yethellam Innaiku Sondhamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethellam Innaiku Sondhamo"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu Ulagam Unkitta Thothathu Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu Ulagam Unkitta Thothathu Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yethu Da Kaiyoda Eduthu Vanthathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethu Da Kaiyoda Eduthu Vanthathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellam Thirudi Nee Saethathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellam Thirudi Nee Saethathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Attam Ini Pori
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Attam Ini Pori"/>
</div>
<div class="lyrico-lyrics-wrapper">Paththi Vizhum Pori
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paththi Vizhum Pori"/>
</div>
<div class="lyrico-lyrics-wrapper">Paththi Vizhum Pori
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paththi Vizhum Pori"/>
</div>
<div class="lyrico-lyrics-wrapper">Paththi Vizhum Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paththi Vizhum Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vettai Idhu Pasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vettai Idhu Pasi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulla Vara Pasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulla Vara Pasi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulla Vara Pasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulla Vara Pasi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulla Vara Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulla Vara Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Attam Ini Pori
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Attam Ini Pori"/>
</div>
<div class="lyrico-lyrics-wrapper">Paththi Vizhum Pori
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paththi Vizhum Pori"/>
</div>
<div class="lyrico-lyrics-wrapper">Paththi Vizhum Pori
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paththi Vizhum Pori"/>
</div>
<div class="lyrico-lyrics-wrapper">Paththi Vizhum Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paththi Vizhum Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vettai Idhu Pasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vettai Idhu Pasi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulla Vara Pasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulla Vara Pasi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulla Vara Pasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulla Vara Pasi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulla Vara Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulla Vara Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sher Iss Sheher Ka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sher Iss Sheher Ka"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaane Machate Tehelka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaane Machate Tehelka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kwab Hai Mehel Ka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kwab Hai Mehel Ka"/>
</div>
<div class="lyrico-lyrics-wrapper">Galiyon Mein Phir Bhi Telhaltha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Galiyon Mein Phir Bhi Telhaltha"/>
</div>
<div class="lyrico-lyrics-wrapper">Flow Ye Keher Ka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Flow Ye Keher Ka"/>
</div>
<div class="lyrico-lyrics-wrapper">Dimaag Mein Tere Feheltha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dimaag Mein Tere Feheltha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ami Chilla Rahi Beta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ami Chilla Rahi Beta"/>
</div>
<div class="lyrico-lyrics-wrapper">Beat Pe Reham Kha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Beat Pe Reham Kha"/>
</div>
<div class="lyrico-lyrics-wrapper">Rap Ka Shahensha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rap Ka Shahensha"/>
</div>
<div class="lyrico-lyrics-wrapper">Badshah Nap Le Raastha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Badshah Nap Le Raastha"/>
</div>
<div class="lyrico-lyrics-wrapper">Dard Ki Daastan Sunn
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dard Ki Daastan Sunn"/>
</div>
<div class="lyrico-lyrics-wrapper">Mard Ke Raasthe Ghum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mard Ke Raasthe Ghum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paise Ke Nashe Ko Chun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paise Ke Nashe Ko Chun"/>
</div>
<div class="lyrico-lyrics-wrapper">Paise Se Nasha Math Chun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paise Se Nasha Math Chun"/>
</div>
<div class="lyrico-lyrics-wrapper">Paise Ke Nashe Ki Wajah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paise Ke Nashe Ki Wajah"/>
</div>
<div class="lyrico-lyrics-wrapper">Hogi Taklifein Ghum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hogi Taklifein Ghum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Silk Rhymes Flow Sharp
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silk Rhymes Flow Sharp"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaise Shark Tooth
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaise Shark Tooth"/>
</div>
<div class="lyrico-lyrics-wrapper">Thu Kapoos
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thu Kapoos"/>
</div>
<div class="lyrico-lyrics-wrapper">Apun Hard Beta Jaa Pooch
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Apun Hard Beta Jaa Pooch"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Agar Asli Hai Tho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Agar Asli Hai Tho"/>
</div>
<div class="lyrico-lyrics-wrapper">Saath Dhu Nakli Tho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saath Dhu Nakli Tho"/>
</div>
<div class="lyrico-lyrics-wrapper">Chamat Dhu Bolo Konsa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chamat Dhu Bolo Konsa"/>
</div>
<div class="lyrico-lyrics-wrapper">Paat Dhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paat Dhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yethu Da Kaiyoda Eduthu Vanthathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethu Da Kaiyoda Eduthu Vanthathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellam Thirudi Nee Saethathu Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellam Thirudi Nee Saethathu Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nirvaanamaa Than Vizhudha Boomiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nirvaanamaa Than Vizhudha Boomiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellam Valachu Nee Potathu Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellam Valachu Nee Potathu Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yethellam Innaiku Sondhamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethellam Innaiku Sondhamo"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu Ulagam Unkitta Thothathu Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu Ulagam Unkitta Thothathu Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yethu Da Kaiyoda Eduthu Vanthathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethu Da Kaiyoda Eduthu Vanthathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellam Thirudi Nee Saethathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellam Thirudi Nee Saethathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Attam Ini Pori
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Attam Ini Pori"/>
</div>
<div class="lyrico-lyrics-wrapper">Paththi Vizhum Pori
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paththi Vizhum Pori"/>
</div>
<div class="lyrico-lyrics-wrapper">Paththi Vizhum Pori
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paththi Vizhum Pori"/>
</div>
<div class="lyrico-lyrics-wrapper">Paththi Vizhum Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paththi Vizhum Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vettai Idhu Pasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vettai Idhu Pasi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulla Vara Pasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulla Vara Pasi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulla Vara Pasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulla Vara Pasi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulla Vara Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulla Vara Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Attam Ini Pori
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Attam Ini Pori"/>
</div>
<div class="lyrico-lyrics-wrapper">Paththi Vizhum Pori
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paththi Vizhum Pori"/>
</div>
<div class="lyrico-lyrics-wrapper">Paththi Vizhum Pori
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paththi Vizhum Pori"/>
</div>
<div class="lyrico-lyrics-wrapper">Paththi Vizhum Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paththi Vizhum Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vettai Idhu Pasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vettai Idhu Pasi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulla Vara Pasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulla Vara Pasi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulla Vara Pasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulla Vara Pasi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulla Vara Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulla Vara Da"/>
</div>
</pre>
