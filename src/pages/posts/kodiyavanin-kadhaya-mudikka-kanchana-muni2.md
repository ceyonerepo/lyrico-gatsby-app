---
title: "kodiyavanin kadhaya mudikka song lyrics"
album: "Kanchana-Muni2"
artist: "S. Thaman"
lyricist: "Viveka"
director: "Raghava Lawrence"
path: "/albums/kanchana-muni2-lyrics"
song: "Kodiyavanin Kadhaya Mudikka"
image: ../../images/albumart/kanchana-muni2.jpg
date: 2011-07-22
lang: tamil
youtubeLink: "https://www.youtube.com/embed/NnrQfaq8bYU"
type: "Mass"
singers:
  - MLR Karthikeyan
  - Sriram
  - Malathy Lakshman
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kodiyavanin kadhaiyai mudikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodiyavanin kadhaiyai mudikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Koravalaiya thedi kadikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koravalaiya thedi kadikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Naaru naaraa odamba kizhikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naaru naaraa odamba kizhikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadutheruvil sedhara adikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadutheruvil sedhara adikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Puzhuvapoala nasukki eriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puzhuvapoala nasukki eriya"/>
</div>
<div class="lyrico-lyrics-wrapper">Puzhicha raththam thelichi nadakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puzhicha raththam thelichi nadakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Thundu thundaa narukki edukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thundu thundaa narukki edukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Thulla vechu usura edukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thulla vechu usura edukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandhikkae adhirchi kodukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhikkae adhirchi kodukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Sagala vidha vadhaigal puriyavae……
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sagala vidha vadhaigal puriyavae……"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vanjinam vanjinam ponga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanjinam vanjinam ponga"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilaiyaada varaa kanchana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilaiyaada varaa kanchana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kova pazhaththa pola kannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kova pazhaththa pola kannu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kovaththula sevakkum sevakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kovaththula sevakkum sevakkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey vettiya maram pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey vettiya maram pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna saaikkavarraa kanchana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna saaikkavarraa kanchana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Othadu thudikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Othadu thudikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Odambu thudikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odambu thudikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kedachcha nodiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kedachcha nodiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Sambhavam nadakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sambhavam nadakkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kenjida kenjida kenjida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kenjida kenjida kenjida"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna kizhichcheriya poraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna kizhichcheriya poraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhara kadhara kadhara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhara kadhara kadhara"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan kadha mudikka poraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan kadha mudikka poraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey vandhuttaa vandhuttaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey vandhuttaa vandhuttaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhuttaa vandhuttaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhuttaa vandhuttaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhuttaa vandhuttaadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhuttaa vandhuttaadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Soorakkaaththa pola varaadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soorakkaaththa pola varaadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey sodukku pottu azhikka varaadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey sodukku pottu azhikka varaadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanum pennum kalandhu varaadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanum pennum kalandhu varaadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Onna pirichchi meya ezhundhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna pirichchi meya ezhundhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Varaadaa dei dei dei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varaadaa dei dei dei"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vanjinam vanjinam ponga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanjinam vanjinam ponga"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilaiyaada varaa kanchana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilaiyaada varaa kanchana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kova pazhaththa pola kannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kova pazhaththa pola kannu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kovaththula sevakkum sevakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kovaththula sevakkum sevakkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey vettiya maram pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey vettiya maram pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna saaikkavarraa kanchana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna saaikkavarraa kanchana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Othadu thudikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Othadu thudikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Odambu thudikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odambu thudikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kedachcha nodiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kedachcha nodiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Sambhavam nadakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sambhavam nadakkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kabaala maalaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabaala maalaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kazhuththil urula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kazhuththil urula"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangalai paarththaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangalai paarththaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Evanum merala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evanum merala"/>
</div>
<div class="lyrico-lyrics-wrapper">Agaala velaiyil vettaikku varaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Agaala velaiyil vettaikku varaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhiri pudhiri aachchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhiri pudhiri aachchi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Appalam polavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appalam polavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhiri norunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhiri norunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Angavum ingavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Angavum ingavum"/>
</div>
<div class="lyrico-lyrics-wrapper">Udalgal sidhara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udalgal sidhara"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppavum engavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppavum engavum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanaadha raatchchasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanaadha raatchchasi"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhirinil vandhaachchae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhirinil vandhaachchae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kombaeri mookkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kombaeri mookkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodhuma naaganum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodhuma naaganum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannaadi viriya puttiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaadi viriya puttiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Saarappaambum suruttappaambum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saarappaambum suruttappaambum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vellikkol varaiyanaaganum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vellikkol varaiyanaaganum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pavala paambum mannuli paambum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pavala paambum mannuli paambum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pasum saambhal thanni paambum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pasum saambhal thanni paambum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudi viriyanum kattu viriyanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudi viriyanum kattu viriyanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Koodi seeravae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koodi seeravae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Soorakkaaththa pola varaadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soorakkaaththa pola varaadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey sodukku pottu azhikka varaadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey sodukku pottu azhikka varaadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanum pennum kalandhu varaadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanum pennum kalandhu varaadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Onna pirichchi meya ezhundhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna pirichchi meya ezhundhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Varaadaa dei dei dei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varaadaa dei dei dei"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyir eduppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir eduppen"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadha mudippen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadha mudippen"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir eduppen kadha mudippen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir eduppen kadha mudippen"/>
</div>
<div class="lyrico-lyrics-wrapper">Karuvaruppen naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuvaruppen naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho ho ho ho ho ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho ho ho ho ho ho"/>
</div>
<div class="lyrico-lyrics-wrapper">Umbalaa umbalaa hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Umbalaa umbalaa hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho ho ho ho ho ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho ho ho ho ho ho"/>
</div>
<div class="lyrico-lyrics-wrapper">Umbalaa umbalaa hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Umbalaa umbalaa hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannula neruppu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannula neruppu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pori parakkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pori parakkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaigalum kaalgalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaigalum kaalgalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thudi thudikkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thudi thudikkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Parkkalum pasiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parkkalum pasiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Nara narangudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nara narangudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kollura neram vandhaachchae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kollura neram vandhaachchae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanamum bhoomiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanamum bhoomiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadu nadungudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadu nadungudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vangakkadal pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vangakkadal pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaaththu urumudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaththu urumudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Singa nadaiyudan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Singa nadaiyudan"/>
</div>
<div class="lyrico-lyrics-wrapper">Singaari roobhaththil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Singaari roobhaththil"/>
</div>
<div class="lyrico-lyrics-wrapper">Sedhachchida vandhaachchae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sedhachchida vandhaachchae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Siththirai veiyilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siththirai veiyilum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalangi pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalangi pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sevakkum iva kanna paarthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sevakkum iva kanna paarthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aththanai dhisaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aththanai dhisaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhirndhu pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhirndhu pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Adadaa iva vgaam paarthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adadaa iva vgaam paarthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudhirai nadungi oda oda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudhirai nadungi oda oda"/>
</div>
<div class="lyrico-lyrics-wrapper">Udalai iva kizhikkaporaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udalai iva kizhikkaporaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Udhava venaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udhava venaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhayangharaththa kaatta poraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhayangharaththa kaatta poraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Soorakkaaththa pola varaadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soorakkaaththa pola varaadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey sodukku pottu azhikka varaadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey sodukku pottu azhikka varaadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanum pennum kalandhu varaadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanum pennum kalandhu varaadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Onna pirichchi meya ezhundhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna pirichchi meya ezhundhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Varaadaa dei dei dei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varaadaa dei dei dei"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vanjinam vanjinam ponga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanjinam vanjinam ponga"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilaiyaada varaa kanchana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilaiyaada varaa kanchana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kova pazhaththa pola kannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kova pazhaththa pola kannu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kovaththula sevakkum sevakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kovaththula sevakkum sevakkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey vettiya maram pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey vettiya maram pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna saaikkavarraa kanchana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna saaikkavarraa kanchana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Othadu thudikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Othadu thudikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Odambu thudikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odambu thudikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kedachcha nodiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kedachcha nodiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Sambhavam nadakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sambhavam nadakkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eduththa sabadham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eduththa sabadham"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudikkum varaikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudikkum varaikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Imaigal yedhu urakkam urakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imaigal yedhu urakkam urakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">Adikkum adiyil malaiyum parakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adikkum adiyil malaiyum parakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuridhi nadhiyil bhoomi sivakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuridhi nadhiyil bhoomi sivakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aththanai elumbum norungum norungum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aththanai elumbum norungum norungum"/>
</div>
<div class="lyrico-lyrics-wrapper">Padharum oosai agilam thirumbum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padharum oosai agilam thirumbum"/>
</div>
<div class="lyrico-lyrics-wrapper">Modhiya kodaari munaigal odaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Modhiya kodaari munaigal odaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ottiya udalgal muzhudhum sedharum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ottiya udalgal muzhudhum sedharum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattiya kayiru arundhu udhirum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattiya kayiru arundhu udhirum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavu kaanum vadhaigalil nadakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavu kaanum vadhaigalil nadakkum"/>
</div>
</pre>
