---
title: "thalam poyi song lyrics"
album: "Ayyappanum Koshiyum"
artist: "Jakes Bejoy"
lyricist: "Rafeeq Ahmed"
director: "Sachy"
path: "/albums/ayyappanum-koshiyum-lyrics"
song: "Thalam Poyi"
image: ../../images/albumart/ayyappanum-koshiyum.jpg
date: 2020-02-07
lang: malayalam
youtubeLink: "https://www.youtube.com/embed/8kQN_vzQST4"
type: "sad"
singers:
  - Jakes Bejoy
  - Sangeethaa
  - Sangeetha Sajith
  - Nanjamma
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Thalam Poyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalam Poyi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappum Poyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappum Poyi"/>
</div>
<div class="lyrico-lyrics-wrapper">Inn Raavin Chaambaladinje
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inn Raavin Chaambaladinje"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aa Chaambal Koonel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Chaambal Koonel"/>
</div>
<div class="lyrico-lyrics-wrapper">Thee Naalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thee Naalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeri Erinje
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeri Erinje"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aadi Peyy Kaatte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadi Peyy Kaatte"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalam Nee Kando
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalam Nee Kando"/>
</div>
<div class="lyrico-lyrics-wrapper">Varum Varaayka Pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varum Varaayka Pol"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veeshunnu Veeshillenno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veeshunnu Veeshillenno"/>
</div>
<div class="lyrico-lyrics-wrapper">Porunno Porillenno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porunno Porillenno"/>
</div>
<div class="lyrico-lyrics-wrapper">Veeshunnu Veeshillenno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veeshunnu Veeshillenno"/>
</div>
<div class="lyrico-lyrics-wrapper">Porunno Porillenno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porunno Porillenno"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thalam Poyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalam Poyi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappum Poyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappum Poyi"/>
</div>
<div class="lyrico-lyrics-wrapper">Inn Raavin Chaambaladinje
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inn Raavin Chaambaladinje"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aa Chaambal Koonel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Chaambal Koonel"/>
</div>
<div class="lyrico-lyrics-wrapper">Thee Naalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thee Naalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeri Erinje
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeri Erinje"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pakalo Raathriyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakalo Raathriyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Velichamo Irulo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velichamo Irulo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nizhalo Naalamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nizhalo Naalamo"/>
</div>
<div class="lyrico-lyrics-wrapper">Sharikkumee Poruletho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sharikkumee Poruletho"/>
</div>
<div class="lyrico-lyrics-wrapper">Nin Jeevithamithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nin Jeevithamithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mattaaro Kaanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mattaaro Kaanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Swapnamo Sathyamo Maayamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Swapnamo Sathyamo Maayamo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalathin Jaalathin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalathin Jaalathin"/>
</div>
<div class="lyrico-lyrics-wrapper">Nooleniyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nooleniyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolangal Paavakkoothadunnuvo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolangal Paavakkoothadunnuvo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thalam Poyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalam Poyi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappum Poyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappum Poyi"/>
</div>
<div class="lyrico-lyrics-wrapper">Inn Raavin Chaambaladinje
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inn Raavin Chaambaladinje"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aa Chaambal Koonel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Chaambal Koonel"/>
</div>
<div class="lyrico-lyrics-wrapper">Thee Naalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thee Naalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeri Erinje
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeri Erinje"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aadi Peyy Kaatte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadi Peyy Kaatte"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalam Nee Kando
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalam Nee Kando"/>
</div>
<div class="lyrico-lyrics-wrapper">Varum Varaayka Pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varum Varaayka Pol"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veeshunnu Veeshillenno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veeshunnu Veeshillenno"/>
</div>
<div class="lyrico-lyrics-wrapper">Porunno Porillenno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porunno Porillenno"/>
</div>
<div class="lyrico-lyrics-wrapper">Veeshunnu Veeshillenno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veeshunnu Veeshillenno"/>
</div>
<div class="lyrico-lyrics-wrapper">Porunno Porillenno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porunno Porillenno"/>
</div>
</pre>
