---
title: "yarin vaalkai song lyrics"
album: "Naduvan"
artist: "Dharan Kumar"
lyricist: "Madhan Karky"
director: "Sharran Kumar"
path: "/albums/naduvan-lyrics"
song: "Yarin Vaalkai"
image: ../../images/albumart/naduvan.jpg
date: 2021-09-24
lang: tamil
youtubeLink: "https://www.youtube.com/embed/z3HFaukqbVE"
type: "happy"
singers:
  - Santhosh Jayakaran
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yaarin vaazhkai idhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarin vaazhkai idhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarin moochu idhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarin moochu idhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarin udalukkul naano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarin udalukkul naano"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarai ketkiren sol bimbamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarai ketkiren sol bimbamae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanum naanum oor enthiram enavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum naanum oor enthiram enavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Maari vittathaai thondridum kanavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maari vittathaai thondridum kanavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Endrum endrumae kalaindhae pogadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endrum endrumae kalaindhae pogadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veru or idhazh punnagai ondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veru or idhazh punnagai ondru"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhan idhazhilae pookkudhu indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan idhazhilae pookkudhu indru"/>
</div>
<div class="lyrico-lyrics-wrapper">Endrum endrumae udhira kudadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endrum endrumae udhira kudadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaarin vaazhkai idhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarin vaazhkai idhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarin moochu idhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarin moochu idhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarin udalukkul naano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarin udalukkul naano"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarai ketkiren sol bimbamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarai ketkiren sol bimbamae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanum naanum oor enthiram enavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum naanum oor enthiram enavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Maari vittathaai thondridum kanavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maari vittathaai thondridum kanavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Endrum endrumae kalaindhae pogadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endrum endrumae kalaindhae pogadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veru or idhazh punnagai ondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veru or idhazh punnagai ondru"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhan idhazhilae pookkudhu indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan idhazhilae pookkudhu indru"/>
</div>
<div class="lyrico-lyrics-wrapper">Endrum endrumae udhira kudadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endrum endrumae udhira kudadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan unai nee enaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan unai nee enaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanavae nikazhnthakavenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanavae nikazhnthakavenna"/>
</div>
<div class="lyrico-lyrics-wrapper">Thozhiyaai kaadhalaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozhiyaai kaadhalaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Maravae nikazhnthakavenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maravae nikazhnthakavenna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee endhan mugavari endro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee endhan mugavari endro"/>
</div>
<div class="lyrico-lyrics-wrapper">Nan undhan nigazhpadam endro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nan undhan nigazhpadam endro"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalathin pokkil marakkoodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalathin pokkil marakkoodum"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhan nikazhnthakavenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhan nikazhnthakavenna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En vaazhvin thonridam veru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En vaazhvin thonridam veru"/>
</div>
<div class="lyrico-lyrics-wrapper">Un vaazhvin seridam veru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un vaazhvin seridam veru"/>
</div>
<div class="lyrico-lyrics-wrapper">Nigazhnthakavinalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nigazhnthakavinalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingae thikazhkiroma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingae thikazhkiroma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaarin perunkadhaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarin perunkadhaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum nanum siru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum nanum siru"/>
</div>
<div class="lyrico-lyrics-wrapper">Paththirangal ena aanom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paththirangal ena aanom"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarin kavithaiyil sol aagirom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarin kavithaiyil sol aagirom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanum naanum oor enthiram enavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum naanum oor enthiram enavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Maari vittathaai thondridum kanavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maari vittathaai thondridum kanavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Endrum endrumae kalaindhae pogadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endrum endrumae kalaindhae pogadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veru or idhazh punnagai ondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veru or idhazh punnagai ondru"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhan idhazhilae pookkudhu indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan idhazhilae pookkudhu indru"/>
</div>
<div class="lyrico-lyrics-wrapper">Endrum endrumae udhira kudadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endrum endrumae udhira kudadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanum naanum oor enthiram enavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum naanum oor enthiram enavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Maari vittathaai thondridum kanavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maari vittathaai thondridum kanavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Endrum endrumae kalaindhae pogadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endrum endrumae kalaindhae pogadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veru or idhazh punnagai ondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veru or idhazh punnagai ondru"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhan idhazhilae pookkudhu indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan idhazhilae pookkudhu indru"/>
</div>
<div class="lyrico-lyrics-wrapper">Endrum endrumae udhira kudadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endrum endrumae udhira kudadhae"/>
</div>
</pre>
