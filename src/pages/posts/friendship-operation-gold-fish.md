---
title: "friendship song lyrics"
album: "Operation Gold Fish"
artist: "Sricharan Pakala"
lyricist: "Ramajogayya Sastry"
director: "Sai Kiran Adivi"
path: "/albums/operation-gold-fish-lyrics"
song: "Friendship"
image: ../../images/albumart/operation-gold-fish.jpg
date: 2019-10-18
lang: telugu
youtubeLink: "https://www.youtube.com/embed/2zqMGSAp2zY"
type: "friendship"
singers:
  - Sricharan Pakala
  - Nithya Naresh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ki Ki Ki Kiraku Friendship
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ki Ki Ki Kiraku Friendship"/>
</div>
<div class="lyrico-lyrics-wrapper">Mandhe Mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandhe Mama"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheers To Friendship
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheers To Friendship"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheppukundamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheppukundamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">We Are The Friendship
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="We Are The Friendship"/>
</div>
<div class="lyrico-lyrics-wrapper">Chirunaamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chirunaamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhelu Long Live
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhelu Long Live"/>
</div>
<div class="lyrico-lyrics-wrapper">Friendship Hangaamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friendship Hangaamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Kushi Ecstasy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Kushi Ecstasy"/>
</div>
<div class="lyrico-lyrics-wrapper">Oxygen Naadiraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oxygen Naadiraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye Rushi Saadhinchani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Rushi Saadhinchani"/>
</div>
<div class="lyrico-lyrics-wrapper">Modern Moksham Idiraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Modern Moksham Idiraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Forever Ee Friendship Anthem
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Forever Ee Friendship Anthem"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadukundhamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadukundhamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Free Style Lo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Free Style Lo"/>
</div>
<div class="lyrico-lyrics-wrapper">Forever Ee Friendship Album
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Forever Ee Friendship Album"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaachukundamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaachukundamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gundelooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundelooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aksharaalaloo Telipe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aksharaalaloo Telipe"/>
</div>
<div class="lyrico-lyrics-wrapper">Maata Kaadhuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maata Kaadhuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Life Time Manasu Maruvani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life Time Manasu Maruvani"/>
</div>
<div class="lyrico-lyrics-wrapper">Beetoven Swarame Idiraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Beetoven Swarame Idiraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Okka Naatitho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okka Naatitho"/>
</div>
<div class="lyrico-lyrics-wrapper">Mugise Baatakaadhuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mugise Baatakaadhuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nonstop Carnival Ga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nonstop Carnival Ga"/>
</div>
<div class="lyrico-lyrics-wrapper">Konasaage Journey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konasaage Journey"/>
</div>
<div class="lyrico-lyrics-wrapper">Friendship Raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friendship Raa"/>
</div>
<div class="lyrico-lyrics-wrapper">Forever Ee Friendship Anthem
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Forever Ee Friendship Anthem"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadukundhamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadukundhamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Free Style Lo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Free Style Lo"/>
</div>
<div class="lyrico-lyrics-wrapper">Forever Ee Friendship Album
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Forever Ee Friendship Album"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaachukundamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaachukundamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gundelooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundelooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Forever Ee Friendship Anthem
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Forever Ee Friendship Anthem"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadukundhamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadukundhamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Free Style Lo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Free Style Lo"/>
</div>
<div class="lyrico-lyrics-wrapper">Forever Ee Friendship Album
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Forever Ee Friendship Album"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaachukundamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaachukundamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gundelooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundelooo"/>
</div>
</pre>
