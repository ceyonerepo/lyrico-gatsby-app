---
title: "pattaya kelappu song lyrics"
album: "Bairavaa"
artist: "Santhosh Narayanan"
lyricist: "Vairamuthu"
director: "Bharathan"
path: "/albums/bairavaa-lyrics"
song: "Pattaya Kelappu"
image: ../../images/albumart/bairavaa.jpg
date: 2017-01-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/uNou48Y-nD4"
type: "mass"
singers:
  - Ananthu
  - Benny Dayal
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Pattaya kelappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattaya kelappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuttaya kulappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuttaya kulappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattaya kelappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattaya kelappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Patti thotti yellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Patti thotti yellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattaya kelappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattaya kelappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kurava meena pudikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurava meena pudikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuttaya kulappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuttaya kulappu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaa Kattu katta saetha nottu kattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaa Kattu katta saetha nottu kattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Perumpoottu pottu kedaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perumpoottu pottu kedaku"/>
</div>
<div class="lyrico-lyrics-wrapper">Paravaikellam oru vangi illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paravaikellam oru vangi illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu patiniya kedakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu patiniya kedakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kattu katta saetha nottu kattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattu katta saetha nottu kattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Perumpoottu pottu kedaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perumpoottu pottu kedaku"/>
</div>
<div class="lyrico-lyrics-wrapper">Paravaikellam oru vangi illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paravaikellam oru vangi illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu patiniya kedakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu patiniya kedakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey saetha sakarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey saetha sakarai"/>
</div>
<div class="lyrico-lyrics-wrapper">Udamba kedukum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udamba kedukum"/>
</div>
<div class="lyrico-lyrics-wrapper">Saetha panamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saetha panamum"/>
</div>
<div class="lyrico-lyrics-wrapper">Usura kuraikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usura kuraikum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paathu paathu selavalikanumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paathu paathu selavalikanumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaahaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaahaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vellai nilavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vellai nilavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharaiku varuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharaiku varuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Velli kiragam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velli kiragam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilaiku varuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilaiku varuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaasai veesi kanaku pananumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasai veesi kanaku pananumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaasai edu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasai edu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathum thisaiyai maathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathum thisaiyai maathum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaasai edu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasai edu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo kadalil railum pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo kadalil railum pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaasai edu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasai edu"/>
</div>
<div class="lyrico-lyrics-wrapper">Imayam konjam kuniyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imayam konjam kuniyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaasai edu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasai edu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pootti vechu ennu panna poraaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pootti vechu ennu panna poraaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alli kodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alli kodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Illa aatam podhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illa aatam podhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Singam ellam saemikaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Singam ellam saemikaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Jill endra kondaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jill endra kondaadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kattu katta saetha nottu kattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattu katta saetha nottu kattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Perumpoottu pottu kedaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perumpoottu pottu kedaku"/>
</div>
<div class="lyrico-lyrics-wrapper">Paravaikellam oru vangi illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paravaikellam oru vangi illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu patiniya kedakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu patiniya kedakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vazhanumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhanumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yezhai pazhai nammai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yezhai pazhai nammai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhuthanumeae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhuthanumeae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh ethiri vanthal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh ethiri vanthal"/>
</div>
<div class="lyrico-lyrics-wrapper">Modhi paakumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Modhi paakumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaikum koottam endral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaikum koottam endral"/>
</div>
<div class="lyrico-lyrics-wrapper">Saikanumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saikanumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Needhi ketu thambi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Needhi ketu thambi"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyae nillu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyae nillu"/>
</div>
<div class="lyrico-lyrics-wrapper">Puthi sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthi sollu"/>
</div>
<div class="lyrico-lyrics-wrapper">Illa potu thallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illa potu thallu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada thapathapa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada thapathapa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vijayun villu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vijayun villu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dharmathil nilachu nillu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dharmathil nilachu nillu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rendaayiram aanda vazha pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendaayiram aanda vazha pora"/>
</div>
<div class="lyrico-lyrics-wrapper">Chumma pooti vechu edhukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chumma pooti vechu edhukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Paravaikellam oru vangi illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paravaikellam oru vangi illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu pattiniya kedakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu pattiniya kedakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pattaya kelappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattaya kelappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuttaya kulappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuttaya kulappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattaya kelappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattaya kelappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Patti thotti yellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Patti thotti yellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattaya kelappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattaya kelappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kurava meena pudikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurava meena pudikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuttaya kulappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuttaya kulappu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaa Kattu katta saetha nottu kattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaa Kattu katta saetha nottu kattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Perumpoottu pottu kedaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perumpoottu pottu kedaku"/>
</div>
<div class="lyrico-lyrics-wrapper">Paravaikellam oru vangi illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paravaikellam oru vangi illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu patiniya kedakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu patiniya kedakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kattu katta saetha nottu kattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattu katta saetha nottu kattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Perumpoottu pottu kedaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perumpoottu pottu kedaku"/>
</div>
<div class="lyrico-lyrics-wrapper">Paravaikellam oru vangi illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paravaikellam oru vangi illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu patiniya kedakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu patiniya kedakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pattaya kelappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattaya kelappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuttaya kulappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuttaya kulappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattaya kelappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattaya kelappu"/>
</div>
</pre>
