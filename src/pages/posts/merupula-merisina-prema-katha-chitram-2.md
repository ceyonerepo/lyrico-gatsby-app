---
title: "merupula merisina song lyrics"
album: "Prema Katha Chitram 2"
artist: "S.B. Uddhav"
lyricist: "Ananth Sriram"
director: "Hari Kishan"
path: "/albums/prema-katha-chitram-2-lyrics"
song: "Merupula Merisina"
image: ../../images/albumart/prema-katha-chitram-2.jpg
date: 2019-04-06
lang: telugu
youtubeLink: "https://www.youtube.com/embed/dcB5xRy7TXQ"
type: "love"
singers:
  - Rahul Sipligunj
  - Ramya Behara
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Merupulaa merisina chirunavva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Merupulaa merisina chirunavva"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinukula manasuni tadipeyvaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinukula manasuni tadipeyvaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Urumulaa urimina toli aasaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urumulaa urimina toli aasaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Varadalaa unnadi varasaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varadalaa unnadi varasaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Chilipi kougilai cherukonaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chilipi kougilai cherukonaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Valapu oopirai undiponaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valapu oopirai undiponaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pedavi kommapai vaaliponaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pedavi kommapai vaaliponaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Modati premanai malli pooyanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Modati premanai malli pooyanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Merupulaa merisina chirunavva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Merupulaa merisina chirunavva"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinukula manasuni tadipeyvaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinukula manasuni tadipeyvaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Reyilaa tolireyilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Reyilaa tolireyilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gadupudaam jeevitam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gadupudaam jeevitam"/>
</div>
<div class="lyrico-lyrics-wrapper">Marapuki maimarapuki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marapuki maimarapuki"/>
</div>
<div class="lyrico-lyrics-wrapper">Palukudaam swaagatam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palukudaam swaagatam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenu neelo alaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenu neelo alaa"/>
</div>
<div class="lyrico-lyrics-wrapper">nuvvu naalo ilaa idelaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvvu naalo ilaa idelaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Merupulaa merisina chirunavva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Merupulaa merisina chirunavva"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinukula manasuni tadipeyvaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinukula manasuni tadipeyvaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nelaki chirugaaliki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nelaki chirugaaliki"/>
</div>
<div class="lyrico-lyrics-wrapper">nadumanunde kshanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nadumanunde kshanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Veluguki mari masakaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veluguki mari masakaki"/>
</div>
<div class="lyrico-lyrics-wrapper">mudulu veddaam manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudulu veddaam manam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenu nuvvavvagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenu nuvvavvagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">nuvvu nenavvagaa ilaagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvvu nenavvagaa ilaagaa"/>
</div>
</pre>
