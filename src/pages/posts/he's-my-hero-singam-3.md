---
title: "he's my hero song lyrics"
album: "Singam III"
artist: "Harris Jayaraj"
lyricist: "Hari - Maalavika Manoj"
director: "Hari Gopalakrishnan"
path: "/albums/singam-3-lyrics"
song: "He's my Hero"
image: ../../images/albumart/singam-3.jpg
date: 2017-02-09
lang: tamil
youtubeLink: "https://www.youtube.com/embed/yfjw6jTqTug"
type: "Sad"
singers:
  - Maalavika Manoj
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">He is my hero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="He is my hero"/>
</div>
<div class="lyrico-lyrics-wrapper">Now i am feeling like a fool
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Now i am feeling like a fool"/>
</div>
<div class="lyrico-lyrics-wrapper">All i ever wanted
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="All i ever wanted"/>
</div>
<div class="lyrico-lyrics-wrapper">All i ever needed
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="All i ever needed"/>
</div>
<div class="lyrico-lyrics-wrapper">He drives me bloody crazy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="He drives me bloody crazy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">He is my hero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="He is my hero"/>
</div>
<div class="lyrico-lyrics-wrapper">Tears i cry will let him know
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tears i cry will let him know"/>
</div>
<div class="lyrico-lyrics-wrapper">I wish that i can show him
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I wish that i can show him"/>
</div>
<div class="lyrico-lyrics-wrapper">Feeling wanna know him
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Feeling wanna know him"/>
</div>
<div class="lyrico-lyrics-wrapper">Who is this bloody buddy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Who is this bloody buddy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avan thaan avanae thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan thaan avanae thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan thaan avan thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan thaan avan thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan mattum mattum thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan mattum mattum thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">He is my sorrow
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="He is my sorrow"/>
</div>
<div class="lyrico-lyrics-wrapper">All the way he makes me feel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="All the way he makes me feel"/>
</div>
<div class="lyrico-lyrics-wrapper">Well i guess its now or never
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Well i guess its now or never"/>
</div>
<div class="lyrico-lyrics-wrapper">And i will wait for ever
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="And i will wait for ever"/>
</div>
<div class="lyrico-lyrics-wrapper">Tell him that i am ready
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tell him that i am ready"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hurt and hollow
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hurt and hollow"/>
</div>
<div class="lyrico-lyrics-wrapper">Thats the way he makes me feel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thats the way he makes me feel"/>
</div>
<div class="lyrico-lyrics-wrapper">Well i guess its now or never
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Well i guess its now or never"/>
</div>
<div class="lyrico-lyrics-wrapper">And i will wait for ever
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="And i will wait for ever"/>
</div>
<div class="lyrico-lyrics-wrapper">I know my love is steady
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I know my love is steady"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avan thaan avanae thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan thaan avanae thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan thaan avan thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan thaan avan thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan mattum mattum thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan mattum mattum thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avan thaan avanae thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan thaan avanae thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan thaan avan thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan thaan avan thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan mattum mattum thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan mattum mattum thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">I wanna wanna love you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I wanna wanna love you"/>
</div>
<div class="lyrico-lyrics-wrapper">I am never gonna miss you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I am never gonna miss you"/>
</div>
<div class="lyrico-lyrics-wrapper">I wanna wanna need you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I wanna wanna need you"/>
</div>
<div class="lyrico-lyrics-wrapper">I am never gonna miss you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I am never gonna miss you"/>
</div>
<div class="lyrico-lyrics-wrapper">I wanna wanna love you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I wanna wanna love you"/>
</div>
<div class="lyrico-lyrics-wrapper">I am never gonna miss you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I am never gonna miss you"/>
</div>
<div class="lyrico-lyrics-wrapper">I wanna wanna need you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I wanna wanna need you"/>
</div>
<div class="lyrico-lyrics-wrapper">I am never gonna miss you ((2) times)
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I am never gonna miss you ((2) times)"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">I need you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I need you"/>
</div>
</pre>
