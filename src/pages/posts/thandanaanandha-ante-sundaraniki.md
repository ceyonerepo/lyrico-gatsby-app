---
title: "thandanaanandha song lyrics"
album: "Ante Sundaraniki"
artist: "Vivek Sagar"
lyricist: "Ramajogayya Sastry"
director: "Vivek Athreya"
path: "/albums/ante-sundaraniki-lyrics"
song: "Thandanaanandha"
image: ../../images/albumart/ante-sundaraniki.jpg
date: 2022-06-10
lang: telugu
youtubeLink: "https://www.youtube.com/embed/S17-kYaDZX8"
type: "happy"
singers:
  -	Shankar Mahadevan
  - Swetha Mohan
  - Gomathi Iyer
  - Sree Poornima
  - Snigdha Sharma
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Chenguchatu cheguevera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chenguchatu cheguevera"/>
</div>
<div class="lyrico-lyrics-wrapper">Viplavala vipra sitara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viplavala vipra sitara"/>
</div>
<div class="lyrico-lyrics-wrapper">Janta cherukoga leelaabaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Janta cherukoga leelaabaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Uthine uurukuntaaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uthine uurukuntaaraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aa deshavali pulihora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa deshavali pulihora"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalipinaarugaa cheyyaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalipinaarugaa cheyyaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanchi daaka kadha saagistaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanchi daaka kadha saagistaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhyalone munakestaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhyalone munakestaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Atuvaaru aavakaya fans-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atuvaaru aavakaya fans-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Maremo ituveeru cake wine friends-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maremo ituveeru cake wine friends-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bhalega kudhirindhile ee alliance-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhalega kudhirindhile ee alliance-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Ante sundaranikinka pellenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ante sundaranikinka pellenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Leelapaapa buggachukka thrillenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Leelapaapa buggachukka thrillenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">All the sides akshinthala jallenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="All the sides akshinthala jallenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Church wedding bellsu ghallu ghallena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Church wedding bellsu ghallu ghallena"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tandanaananda chayya chayya changure
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tandanaananda chayya chayya changure"/>
</div>
<div class="lyrico-lyrics-wrapper">Tandanaananda tayyare talangure
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tandanaananda tayyare talangure"/>
</div>
<div class="lyrico-lyrics-wrapper">Tandanaananda chayya chayya changure
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tandanaananda chayya chayya changure"/>
</div>
<div class="lyrico-lyrics-wrapper">Tandanaananda chayya chayya changure
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tandanaananda chayya chayya changure"/>
</div>
<div class="lyrico-lyrics-wrapper">Tandanaananda tayyare talangure
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tandanaananda tayyare talangure"/>
</div>
<div class="lyrico-lyrics-wrapper">Tandanaananda chayya chayya changure
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tandanaananda chayya chayya changure"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tattatattay laggam time-u raane vachesindi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tattatattay laggam time-u raane vachesindi"/>
</div>
<div class="lyrico-lyrics-wrapper">Andarlo anandam tannuku vachesindi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andarlo anandam tannuku vachesindi"/>
</div>
<div class="lyrico-lyrics-wrapper">Anthalo o darunam arey jarigipoyenandi ayyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anthalo o darunam arey jarigipoyenandi ayyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Pelli ungaralu tali bottu maayamaayeenandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pelli ungaralu tali bottu maayamaayeenandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayyayo adentandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyayo adentandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ante ante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ante ante"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ante sundaranikinka anthena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ante sundaranikinka anthena"/>
</div>
<div class="lyrico-lyrics-wrapper">Moodumulla muchatinka doubtena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moodumulla muchatinka doubtena"/>
</div>
<div class="lyrico-lyrics-wrapper">Lifelongu brahmachari vanteenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lifelongu brahmachari vanteenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Paapam pelli signalandukoda antena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paapam pelli signalandukoda antena"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tandanaananda chayya chayya changure
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tandanaananda chayya chayya changure"/>
</div>
<div class="lyrico-lyrics-wrapper">Tandanaananda chayya chayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tandanaananda chayya chayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Tandanaananda chayya chayya changure
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tandanaananda chayya chayya changure"/>
</div>
<div class="lyrico-lyrics-wrapper">Tandanaananda tayyare talangure
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tandanaananda tayyare talangure"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tandanaananda chayya chayya changure
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tandanaananda chayya chayya changure"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaha oho abbabbo o what a beauty
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaha oho abbabbo o what a beauty"/>
</div>
<div class="lyrico-lyrics-wrapper">Tandanaananda chayya chayya changure
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tandanaananda chayya chayya changure"/>
</div>
<div class="lyrico-lyrics-wrapper">Tandanaananda tayyare talangure
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tandanaananda tayyare talangure"/>
</div>
<div class="lyrico-lyrics-wrapper">Tandanaananda chayya chayya changure
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tandanaananda chayya chayya changure"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tandanaananda pi pi pi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tandanaananda pi pi pi"/>
</div>
<div class="lyrico-lyrics-wrapper">Tandanaananda pi pi pi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tandanaananda pi pi pi"/>
</div>
<div class="lyrico-lyrics-wrapper">Tandanaananda pi pi pi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tandanaananda pi pi pi"/>
</div>
<div class="lyrico-lyrics-wrapper">Tandanaananda pi pi pi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tandanaananda pi pi pi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaha ohoho abbabbo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaha ohoho abbabbo"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh what a beauty
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh what a beauty"/>
</div>
<div class="lyrico-lyrics-wrapper">Ante sundaraniki thadasthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ante sundaraniki thadasthu"/>
</div>
</pre>
