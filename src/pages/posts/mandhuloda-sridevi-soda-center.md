---
title: "mandhuloda song lyrics"
album: "Sridevi Soda Center"
artist: "Mani Sharma"
lyricist: "Kasarla Shyam"
director: "Karuna Kumar"
path: "/albums/sridevi-soda-center-lyrics"
song: "Mandhuloda"
image: ../../images/albumart/sridevi-soda-center.jpg
date: 2021-08-27
lang: telugu
youtubeLink: "https://www.youtube.com/embed/tNrTtiCM0yY"
type: "happy"
singers:
  - Sahithi Chaganti
  - Dhanunjay Seepana
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aa Addhala medallo undeti dhaananuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Addhala medallo undeti dhaananuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Addhala medallo undeti dhaananuraa Ayithey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Addhala medallo undeti dhaananuraa Ayithey"/>
</div>
<div class="lyrico-lyrics-wrapper">Singapoor rangababu flight ekkamannadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Singapoor rangababu flight ekkamannadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ungarala gangireddy gold offer ichhadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ungarala gangireddy gold offer ichhadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thikkaregi yamababu muhurthalu pettesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thikkaregi yamababu muhurthalu pettesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Peddhuri nayudutho pelli chesinaruro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peddhuri nayudutho pelli chesinaruro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mandhuloda ori mayaloda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandhuloda ori mayaloda"/>
</div>
<div class="lyrico-lyrics-wrapper">Mama raara mandhula chinnoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mama raara mandhula chinnoda"/>
</div>
<div class="lyrico-lyrics-wrapper">Mandhuloda ori mayaloda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandhuloda ori mayaloda"/>
</div>
<div class="lyrico-lyrics-wrapper">Mama raara mandhula chinnoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mama raara mandhula chinnoda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Peddhuri nayuduki ninnichhi pelli chesthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peddhuri nayuduki ninnichhi pelli chesthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Maddhuri peddi reddy maddhela vaayinchinaade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maddhuri peddi reddy maddhela vaayinchinaade"/>
</div>
<div class="lyrico-lyrics-wrapper">Sinnoori sittibabu chidathalu kottadey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sinnoori sittibabu chidathalu kottadey"/>
</div>
<div class="lyrico-lyrics-wrapper">Mandhulode aadu maayalodey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandhulode aadu maayalodey"/>
</div>
<div class="lyrico-lyrics-wrapper">Malli raade mandhula chinnode
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malli raade mandhula chinnode"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mandhulode aadu maayalodey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandhulode aadu maayalodey"/>
</div>
<div class="lyrico-lyrics-wrapper">Malli raade mandhula chinnode
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malli raade mandhula chinnode"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa mogudu nayudu ye pani paata seyyakunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa mogudu nayudu ye pani paata seyyakunda"/>
</div>
<div class="lyrico-lyrics-wrapper">Moolikalu verlu thetthanani aaduvulu pattukupoyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moolikalu verlu thetthanani aaduvulu pattukupoyi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannu marisey ponadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannu marisey ponadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Avuna ye oorlu ellaadu em techhadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avuna ye oorlu ellaadu em techhadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thoorpu ellaadu thummeru techhadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoorpu ellaadu thummeru techhadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Padamara ellaadu palleru techhadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padamara ellaadu palleru techhadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dachhanamelladu dabberu techhadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dachhanamelladu dabberu techhadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Uttharamelladu ulleru techhadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uttharamelladu ulleru techhadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mandhulu mandhulu ani mayamai poyinadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandhulu mandhulu ani mayamai poyinadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mandhuloda ori mayaloda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandhuloda ori mayaloda"/>
</div>
<div class="lyrico-lyrics-wrapper">Mama raara mandhula chinnoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mama raara mandhula chinnoda"/>
</div>
<div class="lyrico-lyrics-wrapper">Mandhuloda ori mayaloda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandhuloda ori mayaloda"/>
</div>
<div class="lyrico-lyrics-wrapper">Mama raara mandhula chinnoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mama raara mandhula chinnoda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mandhulode aadu maayalodey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandhulode aadu maayalodey"/>
</div>
<div class="lyrico-lyrics-wrapper">Malli raade mandhula chinnode
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malli raade mandhula chinnode"/>
</div>
<div class="lyrico-lyrics-wrapper">Mandhulode aadu maayalodey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandhulode aadu maayalodey"/>
</div>
<div class="lyrico-lyrics-wrapper">Malli raade mandhula chinnode
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malli raade mandhula chinnode"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paitey pattamante palleru techhada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paitey pattamante palleru techhada"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadume gillamante nalleru allada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadume gillamante nalleru allada"/>
</div>
<div class="lyrico-lyrics-wrapper">Muddulu pettamante moolikalu ichhada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muddulu pettamante moolikalu ichhada"/>
</div>
<div class="lyrico-lyrics-wrapper">Muchhata teerchamante moodu oorlu tirigaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muchhata teerchamante moodu oorlu tirigaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Memunname pilla vaddu neeku mandhu maaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Memunname pilla vaddu neeku mandhu maaku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mandhuloda ori mayaloda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandhuloda ori mayaloda"/>
</div>
<div class="lyrico-lyrics-wrapper">Mama raku mandhula chinnoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mama raku mandhula chinnoda"/>
</div>
<div class="lyrico-lyrics-wrapper">Mandhuloda ori mayaloda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandhuloda ori mayaloda"/>
</div>
<div class="lyrico-lyrics-wrapper">Malli raku mandhula chinnoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malli raku mandhula chinnoda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mandhuloda ori mayaloda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandhuloda ori mayaloda"/>
</div>
<div class="lyrico-lyrics-wrapper">Mama rara mandhula chinnoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mama rara mandhula chinnoda"/>
</div>
<div class="lyrico-lyrics-wrapper">Mandhuloda ori mayaloda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandhuloda ori mayaloda"/>
</div>
<div class="lyrico-lyrics-wrapper">Mama rara mandhula chinnoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mama rara mandhula chinnoda"/>
</div>
</pre>
