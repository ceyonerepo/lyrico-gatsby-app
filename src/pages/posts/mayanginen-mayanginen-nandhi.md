---
title: "mayanginen mayanginen song lyrics"
album: "Nandhi"
artist: "Bharathwaj"
lyricist: "Muthu Vijayan"
director: "Tamilvannan"
path: "/albums/thoonga-nagaram-lyrics"
song: "Mayanginen Mayanginen"
image: ../../images/albumart/thoonga-nagaram.jpg
date: 2011-02-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/el4qVfirQj8"
type: "love"
singers:
  - Mukesh
  - Priyadarshini
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Mayanginen mayanginen un madiyil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayanginen mayanginen un madiyil "/>
</div>
<div class="lyrico-lyrics-wrapper">vizhundhu norunginen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vizhundhu norunginen"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayanginen mayanginen un madiyil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayanginen mayanginen un madiyil "/>
</div>
<div class="lyrico-lyrics-wrapper">vizhundhu norunginen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vizhundhu norunginen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuzhambinen polambinen indru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuzhambinen polambinen indru "/>
</div>
<div class="lyrico-lyrics-wrapper">kuzhumbiya mugathai alambinen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuzhumbiya mugathai alambinen"/>
</div>
<div class="lyrico-lyrics-wrapper">En ulagam un kaaladiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En ulagam un kaaladiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">en pozhudhu vidiyidhu un punnagaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en pozhudhu vidiyidhu un punnagaiyil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En kanavu perugudhu un kan asaivil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kanavu perugudhu un kan asaivil"/>
</div>
<div class="lyrico-lyrics-wrapper">en kaalam nagarudhu un kaippidiyil                          
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kaalam nagarudhu un kaippidiyil                          "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Man kudisai thinnaiyila adiye 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Man kudisai thinnaiyila adiye "/>
</div>
<div class="lyrico-lyrics-wrapper">aasaiyil pesura naeram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aasaiyil pesura naeram"/>
</div>
<div class="lyrico-lyrics-wrapper">thaajjimahal poala maarippoagum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaajjimahal poala maarippoagum "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uchchi veiyil kuththaiyila unna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uchchi veiyil kuththaiyila unna "/>
</div>
<div class="lyrico-lyrics-wrapper">usurula nenaichaa poadhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="usurula nenaichaa poadhum"/>
</div>
<div class="lyrico-lyrics-wrapper">siththiraiyum maargazhiyaai panithoovum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="siththiraiyum maargazhiyaai panithoovum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kallichediyil malligai pookkum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallichediyil malligai pookkum "/>
</div>
<div class="lyrico-lyrics-wrapper">un viral pattaaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un viral pattaaley"/>
</div>
<div class="lyrico-lyrics-wrapper">adi malattu megamum mazhaiyai tharum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi malattu megamum mazhaiyai tharum "/>
</div>
<div class="lyrico-lyrics-wrapper">un mugam paarthaaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un mugam paarthaaley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mittaai kidaitha pallichirumi aaneney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mittaai kidaitha pallichirumi aaneney"/>
</div>
<div class="lyrico-lyrics-wrapper">adutha veettil maradhiyil koalam poatteney         
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adutha veettil maradhiyil koalam poatteney         "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theradiyil nee irundhaal nenjo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theradiyil nee irundhaal nenjo "/>
</div>
<div class="lyrico-lyrics-wrapper">thandichuvaikka ninaikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thandichuvaikka ninaikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">ennaikkandu nee ozhinjaa adhu pudikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennaikkandu nee ozhinjaa adhu pudikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">o oor anaiyil neekkulichaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="o oor anaiyil neekkulichaa"/>
</div>
<div class="lyrico-lyrics-wrapper">aambala meengalum perani nadathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aambala meengalum perani nadathum"/>
</div>
<div class="lyrico-lyrics-wrapper">veeduvarai thaedivandhu onnnakkadikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veeduvarai thaedivandhu onnnakkadikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Evalum kandu poaivida 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evalum kandu poaivida "/>
</div>
<div class="lyrico-lyrics-wrapper">maattaal unnai kalavaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maattaal unnai kalavaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">unnai paththiramaaga pootti 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai paththiramaaga pootti "/>
</div>
<div class="lyrico-lyrics-wrapper">vaithadhu en mana alamaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaithadhu en mana alamaari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uzhuthakkaattu puzhudhiyaaga parandheney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uzhuthakkaattu puzhudhiyaaga parandheney"/>
</div>
<div class="lyrico-lyrics-wrapper">un kazhuthu Ora machchathoadu kalandheney 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kazhuthu Ora machchathoadu kalandheney "/>
</div>
</pre>