---
title: "un aruginile song lyrics"
album: "Mangai Maanvizhi Ambugal"
artist: "Thameem Ansari"
lyricist: "Thameem Ansari"
director: "Vino"
path: "/albums/mangai-maanvizhi-ambugal-lyrics"
song: "Un Aruginile"
image: ../../images/albumart/mangai-maanvizhi-ambugal.jpg
date: 2018-07-13
lang: tamil
youtubeLink: "https://www.youtube.com/embed/clzMeBc5EY8"
type: "love"
singers:
  - Thameem Ansari
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Un Aruginilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Aruginilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai Tholaithen Nilavoliyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai Tholaithen Nilavoliyil"/>
</div>
<div class="lyrico-lyrics-wrapper">En Idhayathidam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Idhayathidam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kettadhu Yaar Un Idhayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kettadhu Yaar Un Idhayam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thoolainthidumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoolainthidumo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nagarnthidumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nagarnthidumo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadanthu Sendra Natkkal Neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadanthu Sendra Natkkal Neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Maranthidumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maranthidumo"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilagidumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilagidumo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadanthu Sendra Azhagiya Thottram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadanthu Sendra Azhagiya Thottram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Ketta Pinbu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Ketta Pinbu"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharamaruthai Vali Unarthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharamaruthai Vali Unarthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Aruginilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Aruginilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai Tholaithen Nilavoliyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai Tholaithen Nilavoliyil"/>
</div>
<div class="lyrico-lyrics-wrapper">En Idhayathidam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Idhayathidam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kettadhu Yaar Un Idhayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kettadhu Yaar Un Idhayam"/>
</div>
</pre>
