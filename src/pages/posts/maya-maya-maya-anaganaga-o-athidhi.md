---
title: "maya maya maya song lyrics"
album: "Anaganaga O Athidhi"
artist: "Arrol Corelli"
lyricist: "Kalyan Chakravarthy"
director: "Dayal Padmanabhan"
path: "/albums/anaganaga-o-athidhi-lyrics"
song: "Maya Maya Maya"
image: ../../images/albumart/anaganaga-o-athidhi.jpg
date: 2020-11-20
lang: telugu
youtubeLink: "https://www.youtube.com/embed/D_uFZMMf5E0"
type: "melody"
singers:
  - Satya Prakash
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Maya maya maya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maya maya maya"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanupapanu reppe podiche maya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanupapanu reppe podiche maya"/>
</div>
<div class="lyrico-lyrics-wrapper">Usire posi visre pasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usire posi visre pasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Teliselopune tellarenu raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teliselopune tellarenu raa"/>
</div>
<div class="lyrico-lyrics-wrapper">Maya maya maya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maya maya maya"/>
</div>
<div class="lyrico-lyrics-wrapper">Manishe vishamai poye naya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manishe vishamai poye naya"/>
</div>
<div class="lyrico-lyrics-wrapper">Maya maya maya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maya maya maya"/>
</div>
<div class="lyrico-lyrics-wrapper">Manishe mrugamai poya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manishe mrugamai poya"/>
</div>
<div class="lyrico-lyrics-wrapper">Athidhi tho aameku bandhamerugakunnadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athidhi tho aameku bandhamerugakunnadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaasula aashalo kannu nanakunnadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasula aashalo kannu nanakunnadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Paame pillanu minge theerai saage yatralo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paame pillanu minge theerai saage yatralo"/>
</div>
<div class="lyrico-lyrics-wrapper">Pege kadhaldha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pege kadhaldha"/>
</div>
<div class="lyrico-lyrics-wrapper">Maya maya maya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maya maya maya"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasuku chuttam maya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasuku chuttam maya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Punnama narakame dhaatinchu vaadu putrude
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Punnama narakame dhaatinchu vaadu putrude"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadaku ni yedhutane vigatha jeevudaayene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadaku ni yedhutane vigatha jeevudaayene"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanche chene mese varusai mugise ee katha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanche chene mese varusai mugise ee katha"/>
</div>
<div class="lyrico-lyrics-wrapper">Mugise lopuno… Maya maya maya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mugise lopuno… Maya maya maya"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasuna aashe maya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasuna aashe maya"/>
</div>
<div class="lyrico-lyrics-wrapper">Maya maya maya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maya maya maya"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha mugise poya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha mugise poya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chivariki aashe chidhamesindhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chivariki aashe chidhamesindhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharagani baadhe migilinchindha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharagani baadhe migilinchindha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadapati payanam kalakalipindha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadapati payanam kalakalipindha"/>
</div>
<div class="lyrico-lyrics-wrapper">Aakari majili modhalaindha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aakari majili modhalaindha"/>
</div>
<div class="lyrico-lyrics-wrapper">Manishi theeru marichi poye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manishi theeru marichi poye"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadichinavu neevu raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadichinavu neevu raa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu kachu aatagadu thudaku theerpu nichera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu kachu aatagadu thudaku theerpu nichera"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maya maya maya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maya maya maya"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninne munche maya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninne munche maya"/>
</div>
<div class="lyrico-lyrics-wrapper">Maya maya maya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maya maya maya"/>
</div>
<div class="lyrico-lyrics-wrapper">Marmam telsina maya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marmam telsina maya"/>
</div>
<div class="lyrico-lyrics-wrapper">Maya maya maya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maya maya maya"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaliyugam anthata vilayam maya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaliyugam anthata vilayam maya"/>
</div>
<div class="lyrico-lyrics-wrapper">Maya maya maya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maya maya maya"/>
</div>
<div class="lyrico-lyrics-wrapper">Manishini patte maya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manishini patte maya"/>
</div>
</pre>
