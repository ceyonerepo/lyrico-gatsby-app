---
title: "idhu varai song lyrics"
album: "Muran"
artist: "Sajan Madhav"
lyricist: "Priyan"
director: "Rajan Madhav"
path: "/albums/muran-lyrics"
song: "Idhu Varai"
image: ../../images/albumart/muran.jpg
date: 2011-09-30
lang: tamil
youtubeLink: "https://www.youtube.com/embed/rSac4Box99s"
type: "love"
singers:
  - Naresh Iyer
  - Saindhavi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Idhuvarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhuvarai"/>
</div>
<div class="lyrico-lyrics-wrapper">en nenjai theendiyadhillai poomazhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en nenjai theendiyadhillai poomazhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudhal murai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhal murai"/>
</div>
<div class="lyrico-lyrics-wrapper">nanaithu chendraai uyirin vervarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanaithu chendraai uyirin vervarai"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhuvarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhuvarai"/>
</div>
<div class="lyrico-lyrics-wrapper">nirangal maariyadhillai vaanilai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nirangal maariyadhillai vaanilai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudhalmurai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhalmurai"/>
</div>
<div class="lyrico-lyrics-wrapper">nee vandhaai vaanavil aanadhu soolnilai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee vandhaai vaanavil aanadhu soolnilai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaanal idhuvarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaanal idhuvarai"/>
</div>
<div class="lyrico-lyrics-wrapper">En naatkal thanimayil veenaai ponadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En naatkal thanimayil veenaai ponadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaal mudhalmurai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaal mudhalmurai"/>
</div>
<div class="lyrico-lyrics-wrapper">En ovvoru nodiyum azhagaai aanadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En ovvoru nodiyum azhagaai aanadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yenno idhuvarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenno idhuvarai"/>
</div>
<div class="lyrico-lyrics-wrapper">pala mozhigal irundhum vaarthai thozhaindhadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pala mozhigal irundhum vaarthai thozhaindhadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaal mudhalmurai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaal mudhalmurai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mounam kooda kadhaigal pesudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mounam kooda kadhaigal pesudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhuvarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhuvarai"/>
</div>
<div class="lyrico-lyrics-wrapper">en nenjai theendiyadhillai poomazhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en nenjai theendiyadhillai poomazhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudhal murai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhal murai"/>
</div>
<div class="lyrico-lyrics-wrapper">nanaithu chendraai uyirin vervarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanaithu chendraai uyirin vervarai"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhuvarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhuvarai"/>
</div>
<div class="lyrico-lyrics-wrapper">nirangal maariyadhillai vaanilai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nirangal maariyadhillai vaanilai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudhalmurai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhalmurai"/>
</div>
<div class="lyrico-lyrics-wrapper">nee vandhaai vaanavil aanadhu soolnilai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee vandhaai vaanavil aanadhu soolnilai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaattrile mirandidum illai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattrile mirandidum illai "/>
</div>
<div class="lyrico-lyrics-wrapper">rasithathillai idhuvarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rasithathillai idhuvarai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaagidham asaivadhum azhagengiren 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaagidham asaivadhum azhagengiren "/>
</div>
<div class="lyrico-lyrics-wrapper">naan mudhalmurai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan mudhalmurai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadal alai virumbiyum kaal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadal alai virumbiyum kaal "/>
</div>
<div class="lyrico-lyrics-wrapper">nanaithathillai idhuvarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanaithathillai idhuvarai"/>
</div>
<div class="lyrico-lyrics-wrapper">Panithuli thottadhum sugam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panithuli thottadhum sugam "/>
</div>
<div class="lyrico-lyrics-wrapper">engiren naan mudhalmurai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engiren naan mudhalmurai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yenoo idhuvarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenoo idhuvarai"/>
</div>
<div class="lyrico-lyrics-wrapper">puyal vandhu modhiyum saaindhadhillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puyal vandhu modhiyum saaindhadhillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaanaal mudhalmurai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaanaal mudhalmurai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayil iragondru urasi aadi ponen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayil iragondru urasi aadi ponen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeeno idhuvarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeeno idhuvarai"/>
</div>
<div class="lyrico-lyrics-wrapper">manam solvadhai kettkum endru irundhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manam solvadhai kettkum endru irundhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanaal mudhalmurai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanaal mudhalmurai"/>
</div>
<div class="lyrico-lyrics-wrapper">therindhe muludhaai naan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="therindhe muludhaai naan "/>
</div>
<div class="lyrico-lyrics-wrapper">envasam elandhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="envasam elandhen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhuvarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhuvarai"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhuvarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhuvarai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saalayin orangal naan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saalayin orangal naan "/>
</div>
<div class="lyrico-lyrics-wrapper">paarthathillai idhuvarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paarthathillai idhuvarai"/>
</div>
<div class="lyrico-lyrics-wrapper">Indruthaan kaankiren poondhottangal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indruthaan kaankiren poondhottangal "/>
</div>
<div class="lyrico-lyrics-wrapper">angu mudhalmurai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="angu mudhalmurai"/>
</div>
<div class="lyrico-lyrics-wrapper">Sooriyan velichamum ennai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sooriyan velichamum ennai "/>
</div>
<div class="lyrico-lyrics-wrapper">suttathilali idhuvarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suttathilali idhuvarai"/>
</div>
<div class="lyrico-lyrics-wrapper">Minmini oliyile karaikiren 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minmini oliyile karaikiren "/>
</div>
<div class="lyrico-lyrics-wrapper">naan mudhalmurai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan mudhalmurai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yenno idhuvarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenno idhuvarai"/>
</div>
<div class="lyrico-lyrics-wrapper">vetkangal enmel soolnthathillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetkangal enmel soolnthathillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanaal mudhalmurai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanaal mudhalmurai"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha maatram yethanaal puriyavillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha maatram yethanaal puriyavillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yenno idhuvarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenno idhuvarai"/>
</div>
<div class="lyrico-lyrics-wrapper">vennilavum ennai theendavillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vennilavum ennai theendavillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanaal mudhalmurai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanaal mudhalmurai"/>
</div>
<div class="lyrico-lyrics-wrapper">un vatta pottil otti kondaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un vatta pottil otti kondaen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhuvarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhuvarai"/>
</div>
<div class="lyrico-lyrics-wrapper">en nenjai theendiyadhillai poomazhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en nenjai theendiyadhillai poomazhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudhal murai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhal murai"/>
</div>
<div class="lyrico-lyrics-wrapper">nanaithu chendraai uyirin vervarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanaithu chendraai uyirin vervarai"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhuvarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhuvarai"/>
</div>
<div class="lyrico-lyrics-wrapper">nirangal maariyadhillai vaanilai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nirangal maariyadhillai vaanilai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudhalmurai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhalmurai"/>
</div>
<div class="lyrico-lyrics-wrapper">nee vandhaai vaanavil aanadhu soolnilai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee vandhaai vaanavil aanadhu soolnilai"/>
</div>
</pre>
