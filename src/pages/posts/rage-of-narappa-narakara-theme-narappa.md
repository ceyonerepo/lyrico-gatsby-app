---
title: "rage of narappa narakara theme song lyrics"
album: "Narappa"
artist: "Mani Sharma"
lyricist: "Ananta Sriram"
director: "Srikanth Addala"
path: "/albums/narappa-lyrics"
song: "Rage of Narappa - Narakara Theme"
image: ../../images/albumart/narappa.jpg
date: 2021-07-20
lang: telugu
youtubeLink: "https://www.youtube.com/embed/qrF_Er_OqDI"
type: "mass"
singers:
  - L.V. Revanth
  - Sai Charan
  - Sri Krishna
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Raa narakaraa narakaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raa narakaraa narakaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhuru thirigi kasiga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhuru thirigi kasiga"/>
</div>
<div class="lyrico-lyrics-wrapper">Raa narakaraa narakaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raa narakaraa narakaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalalu yegiri padaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalalu yegiri padaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Raa chera chera cheragara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raa chera chera cheragara"/>
</div>
<div class="lyrico-lyrics-wrapper">Medani medani vidiga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Medani medani vidiga"/>
</div>
<div class="lyrico-lyrics-wrapper">Raa tharamara thuramara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raa tharamara thuramara"/>
</div>
<div class="lyrico-lyrics-wrapper">Naramu naramu viraga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naramu naramu viraga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kathi gonthulo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathi gonthulo"/>
</div>
<div class="lyrico-lyrics-wrapper">Netthureyyaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netthureyyaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Matti notilo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Matti notilo"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhanda muddha kalipi veyyara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhanda muddha kalipi veyyara"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeli ninginee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeli ninginee"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaali ranguni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaali ranguni"/>
</div>
<div class="lyrico-lyrics-wrapper">Yerra yerragaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yerra yerragaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Maarchi veyyaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maarchi veyyaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aayudhaanike aayudhanivai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayudhaanike aayudhanivai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aayuvulni teesi chesukora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayuvulni teesi chesukora"/>
</div>
<div class="lyrico-lyrics-wrapper">Chaavu jaatharaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chaavu jaatharaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raa narakaraa narakaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raa narakaraa narakaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhuru thirigi kasiga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhuru thirigi kasiga"/>
</div>
<div class="lyrico-lyrics-wrapper">Raa narakaraa narakaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raa narakaraa narakaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalalu yegiri padaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalalu yegiri padaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Raa chera chera cheragara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raa chera chera cheragara"/>
</div>
<div class="lyrico-lyrics-wrapper">Medani medani vidiga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Medani medani vidiga"/>
</div>
<div class="lyrico-lyrics-wrapper">Raa tharamara thuramara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raa tharamara thuramara"/>
</div>
<div class="lyrico-lyrics-wrapper">Naramu naramu viraga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naramu naramu viraga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gundeloki gunapamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundeloki gunapamai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadupuloki kodavalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadupuloki kodavalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhoosukelli kosukelli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhoosukelli kosukelli"/>
</div>
<div class="lyrico-lyrics-wrapper">Pegulanni tholachivai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pegulanni tholachivai"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaayapadda manasuvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaayapadda manasuvai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mosapadda manishivai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mosapadda manishivai"/>
</div>
<div class="lyrico-lyrics-wrapper">Lopalunna rakshasunni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lopalunna rakshasunni"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaachakinka paikithiy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaachakinka paikithiy"/>
</div>
<div class="lyrico-lyrics-wrapper">Kroora mrugamuvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kroora mrugamuvai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kroora kroora mrugamuvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kroora kroora mrugamuvai"/>
</div>
<div class="lyrico-lyrics-wrapper">Gorla koralatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gorla koralatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaalla rommulanni volichivey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaalla rommulanni volichivey"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaala yamudivai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaala yamudivai"/>
</div>
<div class="lyrico-lyrics-wrapper">Poonakala yamudivai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poonakala yamudivai"/>
</div>
<div class="lyrico-lyrics-wrapper">Paasham isrivey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paasham isrivey"/>
</div>
<div class="lyrico-lyrics-wrapper">Vennu poosalanni virichivey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vennu poosalanni virichivey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raa narakaraa narakaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raa narakaraa narakaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhuru thirigi kasiga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhuru thirigi kasiga"/>
</div>
<div class="lyrico-lyrics-wrapper">Raa narakaraa narakaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raa narakaraa narakaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalalu yegiri padaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalalu yegiri padaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Raa chera chera cheragara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raa chera chera cheragara"/>
</div>
<div class="lyrico-lyrics-wrapper">Medani medani vidiga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Medani medani vidiga"/>
</div>
<div class="lyrico-lyrics-wrapper">Raa tharamara thuramara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raa tharamara thuramara"/>
</div>
<div class="lyrico-lyrics-wrapper">Naramu naramu viraga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naramu naramu viraga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vetu vesthe naluguru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetu vesthe naluguru"/>
</div>
<div class="lyrico-lyrics-wrapper">Potu vesthe padhuguru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Potu vesthe padhuguru"/>
</div>
<div class="lyrico-lyrics-wrapper">Veta neeku kottha kaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veta neeku kottha kaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vellu evaru migalaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vellu evaru migalaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Raguluthunna kshanamulo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raguluthunna kshanamulo"/>
</div>
<div class="lyrico-lyrics-wrapper">Segalakalla veluthuru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Segalakalla veluthuru"/>
</div>
<div class="lyrico-lyrics-wrapper">Thagalabeduthu vunte vaallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thagalabeduthu vunte vaallu"/>
</div>
<div class="lyrico-lyrics-wrapper">Venaka mundhu migalaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venaka mundhu migalaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kori samaramo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kori samaramo"/>
</div>
<div class="lyrico-lyrics-wrapper">Korakunda samaramo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Korakunda samaramo"/>
</div>
<div class="lyrico-lyrics-wrapper">Yentha samaramo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yentha samaramo"/>
</div>
<div class="lyrico-lyrics-wrapper">Mugiyadaniki yentha samayamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mugiyadaniki yentha samayamo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kori samaramo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kori samaramo"/>
</div>
<div class="lyrico-lyrics-wrapper">Korakunda samaramo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Korakunda samaramo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kori maranamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kori maranamo"/>
</div>
<div class="lyrico-lyrics-wrapper">Korakunda maranamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Korakunda maranamo"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhi maranamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhi maranamo"/>
</div>
<div class="lyrico-lyrics-wrapper">Thelchadaniki edhi tharunamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thelchadaniki edhi tharunamo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raa narakaraa narakaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raa narakaraa narakaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhuru thirigi kasiga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhuru thirigi kasiga"/>
</div>
<div class="lyrico-lyrics-wrapper">Raa narakaraa narakaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raa narakaraa narakaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalalu yegiri padaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalalu yegiri padaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Raa chera chera cheragara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raa chera chera cheragara"/>
</div>
<div class="lyrico-lyrics-wrapper">Medani medani vidiga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Medani medani vidiga"/>
</div>
<div class="lyrico-lyrics-wrapper">Raa tharamara thuramara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raa tharamara thuramara"/>
</div>
<div class="lyrico-lyrics-wrapper">Naramu naramu viraga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naramu naramu viraga"/>
</div>
</pre>
