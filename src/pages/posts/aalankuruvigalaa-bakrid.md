---
title: "aalankuruvigalaa song lyrics"
album: "Bakrid"
artist: "D. Imman"
lyricist: "Mani Amuthavan"
director: "Jagadeesan Subu"
path: "/albums/bakrid-lyrics"
song: "Aalankuruvigalaa"
image: ../../images/albumart/bakrid.jpg
date: 2019-08-23
lang: tamil
youtubeLink: "https://www.youtube.com/embed/F1Aowcix4rc"
type: "happy"
singers:
  - Sid Sriram
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aalanguruvigalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalanguruvigalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga Vaasal Varuvigalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Vaasal Varuvigalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aalanguruvigalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalanguruvigalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazha Solli Tharuvigalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazha Solli Tharuvigalaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anba Theda Eduthome Piravi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anba Theda Eduthome Piravi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thangam Thedi Parakkathe Kuruvi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangam Thedi Parakkathe Kuruvi"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Purinja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Purinja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaiyil Ettadha Ettadha Santhosam Ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyil Ettadha Ettadha Santhosam Ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Veettil Ukkarume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Veettil Ukkarume"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannu Kottame Kottame Kottarum Pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu Kottame Kottame Kottarum Pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thikkadi Mukkadume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thikkadi Mukkadume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aalanguruvigalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalanguruvigalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga Vaasal Varuvigalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Vaasal Varuvigalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aalanguruvigalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalanguruvigalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazha Solli Tharuvigalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazha Solli Tharuvigalaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nammoda Mugathu Saayalula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nammoda Mugathu Saayalula"/>
</div>
<div class="lyrico-lyrics-wrapper">Munnavanga Vaazhntha Thadam Irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munnavanga Vaazhntha Thadam Irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Yethanai Peru Vanthalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethanai Peru Vanthalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhnthukalam Vaanga Idam Irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhnthukalam Vaanga Idam Irukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ellame Venungura Unakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellame Venungura Unakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Athil Kaakakum Kuruvikum Pangu Irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athil Kaakakum Kuruvikum Pangu Irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Yethetho Kottainga Idinju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethetho Kottainga Idinju"/>
</div>
<div class="lyrico-lyrics-wrapper">Athan Mel Ippa Maramthane Mulachu Kidakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athan Mel Ippa Maramthane Mulachu Kidakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Arumbum Erumbum Naam Sondham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arumbum Erumbum Naam Sondham"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirumbum Thisaiyellam Anandham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirumbum Thisaiyellam Anandham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aalanguruvigalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalanguruvigalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga Vaasal Varuvigalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Vaasal Varuvigalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aalanguruvigalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalanguruvigalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazha Solli Tharuvigalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazha Solli Tharuvigalaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Naalum Marakkama Namakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Naalum Marakkama Namakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Ozhi Vaari Yerikaatha Kizhakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ozhi Vaari Yerikaatha Kizhakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Inga Poranthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga Poranthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaaru Vanthalum Illenu Sollame Boomi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaru Vanthalum Illenu Sollame Boomi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellarkkum Ellamtharum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellarkkum Ellamtharum"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha Chinnoondu Chinnoondu Poochikun Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Chinnoondu Chinnoondu Poochikun Kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Poo Poothu Theana Tharum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poo Poothu Theana Tharum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aalanguruvigalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalanguruvigalaa"/>
</div>
</pre>
