---
title: "hayaki baby song lyrics"
album: "Kapatadhaari"
artist: "Simon K King"
lyricist: "Vanamali"
director: "Pradeep Krishnamoorthy"
path: "/albums/kapatadhaari-lyrics"
song: "Hayaki Baby - Chelarege Cheekati"
image: ../../images/albumart/kapatadhaari.jpg
date: 2021-02-19
lang: telugu
youtubeLink: "https://www.youtube.com/embed/XkRRk5UeFt0"
type: "happy"
singers:
  - Sanah Moidutty
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Chelarege cheekati lo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chelarege cheekati lo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanu kalche vyadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanu kalche vyadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Theerche jatha neeve kadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theerche jatha neeve kadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Marala marala thanuvu kore saradha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marala marala thanuvu kore saradha"/>
</div>
<div class="lyrico-lyrics-wrapper">Hop up in this club with some models models
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hop up in this club with some models models"/>
</div>
<div class="lyrico-lyrics-wrapper">Sit in vip poppin bottles bottles
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sit in vip poppin bottles bottles"/>
</div>
<div class="lyrico-lyrics-wrapper">We aint slow in down
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="We aint slow in down"/>
</div>
<div class="lyrico-lyrics-wrapper">We full throttle we just do it
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="We full throttle we just do it"/>
</div>
<div class="lyrico-lyrics-wrapper">Big homie we just so colossal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Big homie we just so colossal"/>
</div>
<div class="lyrico-lyrics-wrapper">You just break it now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You just break it now"/>
</div>
<div class="lyrico-lyrics-wrapper">Baby girl you know to work it
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby girl you know to work it"/>
</div>
<div class="lyrico-lyrics-wrapper">Bounce it to the sides and i love the way you twerk it
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bounce it to the sides and i love the way you twerk it"/>
</div>
<div class="lyrico-lyrics-wrapper">Shake it now break it exoyic like teriyaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shake it now break it exoyic like teriyaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Spin it disc jockey I’m dance in with this hayakki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Spin it disc jockey I’m dance in with this hayakki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey hayakki haykki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hayakki haykki"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattu pattu pattu naa paiki paaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattu pattu pattu naa paiki paaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannu chuttu muttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannu chuttu muttu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey hayakki haykki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hayakki haykki"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattu pattu pattu naa paiki paaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattu pattu pattu naa paiki paaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannu chuttu muttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannu chuttu muttu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa pogarentho nuv pasi gattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa pogarentho nuv pasi gattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee dhayyam vadhilisthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee dhayyam vadhilisthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee paruvanne o pani pattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee paruvanne o pani pattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa deham aruvisthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa deham aruvisthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">I love it the way you shake it around
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I love it the way you shake it around"/>
</div>
<div class="lyrico-lyrics-wrapper">Girl you make me psycho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Girl you make me psycho"/>
</div>
<div class="lyrico-lyrics-wrapper">Keep what you do in dont ever stop it babe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Keep what you do in dont ever stop it babe"/>
</div>
<div class="lyrico-lyrics-wrapper">Make my mind blow cant make my mind blow
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Make my mind blow cant make my mind blow"/>
</div>
<div class="lyrico-lyrics-wrapper">Cant get you out cant cant cant
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cant get you out cant cant cant"/>
</div>
<div class="lyrico-lyrics-wrapper">Get you out of my mind when the night is done
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Get you out of my mind when the night is done"/>
</div>
<div class="lyrico-lyrics-wrapper">Let me take control
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Let me take control"/>
</div>
<div class="lyrico-lyrics-wrapper">Inni inni hayakki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inni inni hayakki"/>
</div>
<div class="lyrico-lyrics-wrapper">Inni inni hayakki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inni inni hayakki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey hayakki haykki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hayakki haykki"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattu pattu pattu naa paiki paaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattu pattu pattu naa paiki paaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannu chuttu muttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannu chuttu muttu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey hayakki haykki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hayakki haykki"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattu pattu pattu naa paiki paaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattu pattu pattu naa paiki paaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannu chuttu muttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannu chuttu muttu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hayaki on the floor
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hayaki on the floor"/>
</div>
<div class="lyrico-lyrics-wrapper">Yalla yalla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yalla yalla"/>
</div>
<div class="lyrico-lyrics-wrapper">Inni inni hayakki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inni inni hayakki"/>
</div>
<div class="lyrico-lyrics-wrapper">Valla haabee bee Valla haabee bee Valla haabee bee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valla haabee bee Valla haabee bee Valla haabee bee"/>
</div>
<div class="lyrico-lyrics-wrapper">Inni inni hayakki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inni inni hayakki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">O naa vollokocchi manmathude ponantunnade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O naa vollokocchi manmathude ponantunnade"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa indhrudemo swargamante cheekottesaade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa indhrudemo swargamante cheekottesaade"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa andhalanni kollagotti vellalani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa andhalanni kollagotti vellalani"/>
</div>
<div class="lyrico-lyrics-wrapper">Gumikudi gumikudi podhaa lokame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gumikudi gumikudi podhaa lokame"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa shilpam chekki chudu saradhaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa shilpam chekki chudu saradhaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa brhame neeku sallam chesthade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa brhame neeku sallam chesthade"/>
</div>
<div class="lyrico-lyrics-wrapper">Na bomme geesi chudu alavoga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na bomme geesi chudu alavoga"/>
</div>
<div class="lyrico-lyrics-wrapper">Picaso puttada nee kalle pattada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Picaso puttada nee kalle pattada"/>
</div>
<div class="lyrico-lyrics-wrapper">Hayaki baby hayaki baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hayaki baby hayaki baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa sogasuku dasoham endharendharo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa sogasuku dasoham endharendharo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hayaki baby hayaki baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hayaki baby hayaki baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee orakanta chusaana chitthu chitthuro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee orakanta chusaana chitthu chitthuro"/>
</div>
<div class="lyrico-lyrics-wrapper">Yetetta sokindhe nee galitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yetetta sokindhe nee galitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Bette yedhanta vollo vaalitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bette yedhanta vollo vaalitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Koose o pitta ichheyitta maa vaata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koose o pitta ichheyitta maa vaata"/>
</div>
<div class="lyrico-lyrics-wrapper">Pariche nee paita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pariche nee paita"/>
</div>
<div class="lyrico-lyrics-wrapper">Inni inni hayki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inni inni hayki"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh yeah"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey hayakki haykki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hayakki haykki"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattu pattu pattu naa paiki paaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattu pattu pattu naa paiki paaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannu chuttu muttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannu chuttu muttu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey hayakki haykki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hayakki haykki"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattu pattu pattu naa paiki paaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattu pattu pattu naa paiki paaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannu chuttu muttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannu chuttu muttu"/>
</div>
</pre>
