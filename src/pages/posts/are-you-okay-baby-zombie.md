---
title: "are you okay baby song lyrics"
album: "Zombie"
artist: "Premgi Amaren"
lyricist: "Ku Karthik"
director: "Bhuvan Nullan"
path: "/albums/zombie-lyrics"
song: "Are You Okay Baby"
image: ../../images/albumart/zombie.jpg
date: 2019-09-06
lang: tamil
youtubeLink: "https://www.youtube.com/embed/NXO2w_6iAR4"
type: "happy"
singers:
  - Anthony Daasan
  - Pravin Saivi
  - Premji Amaren
  - Kharesma Ravichandran
  - Swagatha S. Krishnan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Dance With Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dance With Me"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dama Dammaa Sola Si
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dama Dammaa Sola Si"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Semma Scene Party
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Semma Scene Party"/>
</div>
<div class="lyrico-lyrics-wrapper">Fine Tune Up Pannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fine Tune Up Pannu"/>
</div>
<div class="lyrico-lyrics-wrapper">Do Re Mi Pa So La Si
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Do Re Mi Pa So La Si"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vilayaadu Jumanji
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilayaadu Jumanji"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadaiyilla Come On Ji
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadaiyilla Come On Ji"/>
</div>
<div class="lyrico-lyrics-wrapper">Adipoma Ippo Bang Bang
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adipoma Ippo Bang Bang"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Don’t You Worry
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Don’t You Worry"/>
</div>
<div class="lyrico-lyrics-wrapper">Worry Worry
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Worry Worry"/>
</div>
<div class="lyrico-lyrics-wrapper">Sokki Poga Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sokki Poga Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Don’t You Worry
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Don’t You Worry"/>
</div>
<div class="lyrico-lyrics-wrapper">Worry Worry
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Worry Worry"/>
</div>
<div class="lyrico-lyrics-wrapper">Sorgam Inga Fall-ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sorgam Inga Fall-ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kutty Butterflies-um
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kutty Butterflies-um"/>
</div>
<div class="lyrico-lyrics-wrapper">Cat Walk Poda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cat Walk Poda"/>
</div>
<div class="lyrico-lyrics-wrapper">Rainbow Show Vaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rainbow Show Vaachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Blood Cell-u Kulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Blood Cell-u Kulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bell Ringaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bell Ringaachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa Kaadhula Kekkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Kaadhula Kekkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maestro Music
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maestro Music"/>
</div>
<div class="lyrico-lyrics-wrapper">Fun-uh Tone Aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fun-uh Tone Aachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Are You Okay Baby?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Are You Okay Baby?"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Semma Weight-u Scene-uh Dhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Semma Weight-u Scene-uh Dhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolamaasu Tune-uh Dhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolamaasu Tune-uh Dhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Velavaasi Pola Yaethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velavaasi Pola Yaethu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ka Ma Pa Dha Pa Ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ka Ma Pa Dha Pa Ni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poippamaa Pushpamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poippamaa Pushpamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellarum Vaangamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellarum Vaangamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hifi Nee Podu Bang Bang
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hifi Nee Podu Bang Bang"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Don’t You Worry
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Don’t You Worry"/>
</div>
<div class="lyrico-lyrics-wrapper">Worry Worry
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Worry Worry"/>
</div>
<div class="lyrico-lyrics-wrapper">Ekkachakkamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ekkachakkamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Don’t You Worry
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Don’t You Worry"/>
</div>
<div class="lyrico-lyrics-wrapper">Worry Worry
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Worry Worry"/>
</div>
<div class="lyrico-lyrics-wrapper">Inbam Kotti Poomaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inbam Kotti Poomaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Pablo Picasso Painting Dhna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Pablo Picasso Painting Dhna"/>
</div>
<div class="lyrico-lyrics-wrapper">Moochu Vittaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moochu Vittaachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Leonardo Da Vinci Sculpture Dhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Leonardo Da Vinci Sculpture Dhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nikkaamalae Aadudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikkaamalae Aadudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">No 8-u Wonder Aayachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="No 8-u Wonder Aayachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Are You Okay Baby?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Are You Okay Baby?"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ippudu Chudu Naina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippudu Chudu Naina"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey What
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey What"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey What Nonsense
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey What Nonsense"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Step Idhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Step Idhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhukku Epdi Aadurathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhukku Epdi Aadurathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanga Ippa Epdi Aaduronu Paakurigala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga Ippa Epdi Aaduronu Paakurigala"/>
</div>
<div class="lyrico-lyrics-wrapper">Indecent-ah Behave Pandranga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indecent-ah Behave Pandranga"/>
</div>
<div class="lyrico-lyrics-wrapper">Dj Petta Philips Okay Bro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dj Petta Philips Okay Bro"/>
</div>
<div class="lyrico-lyrics-wrapper">Please Change The Mood
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Please Change The Mood"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi Podu Mangaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Podu Mangaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vedi Podu Majadhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedi Podu Majadhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga Route-ah Pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga Route-ah Pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pedala Pinni Kaatataa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pedala Pinni Kaatataa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Namma Screen Kulla Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma Screen Kulla Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Endha Scene-uh Odaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endha Scene-uh Odaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga Step-ah Vecha House Full
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga Step-ah Vecha House Full"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unakku Solli Tharatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakku Solli Tharatta"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharatta Tharatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharatta Tharatta"/>
</div>
<div class="lyrico-lyrics-wrapper">Kettukka Nee Alert-ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kettukka Nee Alert-ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Alert-ah Alert-ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alert-ah Alert-ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanakku Panna Varatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanakku Panna Varatta"/>
</div>
<div class="lyrico-lyrics-wrapper">Varatta Varatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varatta Varatta"/>
</div>
<div class="lyrico-lyrics-wrapper">Sethukka Nee Correct-ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sethukka Nee Correct-ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Correct-ah Correct-ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Correct-ah Correct-ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Chinna Life-uh Kulla Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Chinna Life-uh Kulla Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Right Wrongu Sollatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Right Wrongu Sollatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha Vayasu Ponaalae Varadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Vayasu Ponaalae Varadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Semma Speed Kondattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Semma Speed Kondattam"/>
</div>
<div class="lyrico-lyrics-wrapper">Summa Bore-u Adikkadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summa Bore-u Adikkadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nikkaadha Nikkaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikkaadha Nikkaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu Gold-u Time-ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu Gold-u Time-ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Waste Pannadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Waste Pannadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Are You Okay Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Are You Okay Baby"/>
</div>
</pre>
