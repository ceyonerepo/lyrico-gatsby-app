---
title: 'machan inga vandhira song lyrics'
album: 'Kaappaan'
artist: 'Harris Jayaraj'
lyricist: 'Kabilan Vairamuthu'
director: 'K V Anand'
path: '/albums/kaappaan-song-lyrics'
song: 'Machan Inga Vandhira'
image: ../../images/albumart/kaappaan.jpg
date: 2019-09-20
singers: 
- Kharesma Ravichandran
- Nikhita Gandhi
- Shabnam
lang: tamil
youtubeLink: "https://www.youtube.com/embed/hywviZuO12g"
type: 'pub'
---

<pre class="lyrics-native">
    Coming Soon
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Alright lets do this….
<input type="checkbox" class="lyrico-select-lyric-line" value="Alright lets do this…."/>
</div>
<div class="lyrico-lyrics-wrapper">Ohh ooo ooo
<input type="checkbox" class="lyrico-select-lyric-line" value="Ohh ooo ooo"/>
</div>
<div class="lyrico-lyrics-wrapper">We in the club now…come on
<input type="checkbox" class="lyrico-select-lyric-line" value="We in the club now…come on"/>
</div>
<div class="lyrico-lyrics-wrapper">Show girls on the floor
<input type="checkbox" class="lyrico-select-lyric-line" value="Show girls on the floor"/>
</div>
<div class="lyrico-lyrics-wrapper">Dirty eyes hot and spice
<input type="checkbox" class="lyrico-select-lyric-line" value="Dirty eyes hot and spice"/>
</div>
<div class="lyrico-lyrics-wrapper">Hit them-a get them-a
<input type="checkbox" class="lyrico-select-lyric-line" value="Hit them-a get them-a"/>
</div>
<div class="lyrico-lyrics-wrapper">Rid them-a dilemma
<input type="checkbox" class="lyrico-select-lyric-line" value="Rid them-a dilemma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Machaan inga vanthira?…
<input type="checkbox" class="lyrico-select-lyric-line" value="Machaan inga vanthira?…"/>
</div>
<div class="lyrico-lyrics-wrapper">Malappa enna paathira?..
<input type="checkbox" class="lyrico-select-lyric-line" value="Malappa enna paathira?.."/>
</div>
<div class="lyrico-lyrics-wrapper">Maraikaamal marukaamal uraipeera?…
<input type="checkbox" class="lyrico-select-lyric-line" value="Maraikaamal marukaamal uraipeera?…"/>
</div>
<div class="lyrico-lyrics-wrapper">Poovai kaana vanthira?..
<input type="checkbox" class="lyrico-select-lyric-line" value="Poovai kaana vanthira?.."/>
</div>
<div class="lyrico-lyrics-wrapper">Thenai thaedi vanthira?…
<input type="checkbox" class="lyrico-select-lyric-line" value="Thenai thaedi vanthira?…"/>
</div>
<div class="lyrico-lyrics-wrapper">Pothaamal pooncholai ketppira?…
<input type="checkbox" class="lyrico-select-lyric-line" value="Pothaamal pooncholai ketppira?…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Indru nee punithan illai
<input type="checkbox" class="lyrico-select-lyric-line" value="Indru nee punithan illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaluvida thadaiyae illai
<input type="checkbox" class="lyrico-select-lyric-line" value="Thaluvida thadaiyae illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Madiyilae ennai thavira..
<input type="checkbox" class="lyrico-select-lyric-line" value="Madiyilae ennai thavira.."/>
</div>
<div class="lyrico-lyrics-wrapper">Puviyilae asaivaae illai..
<input type="checkbox" class="lyrico-select-lyric-line" value="Puviyilae asaivaae illai.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Itchaiyin puyalaai vanthen
<input type="checkbox" class="lyrico-select-lyric-line" value="Itchaiyin puyalaai vanthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Meesaiyai poosikonden
<input type="checkbox" class="lyrico-select-lyric-line" value="Meesaiyai poosikonden"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjilae nenjai korthen
<input type="checkbox" class="lyrico-select-lyric-line" value="Nenjilae nenjai korthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnilae mirugam serthen
<input type="checkbox" class="lyrico-select-lyric-line" value="Unnilae mirugam serthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Machaan inga vanthira?…
<input type="checkbox" class="lyrico-select-lyric-line" value="Machaan inga vanthira?…"/>
</div>
<div class="lyrico-lyrics-wrapper">Malappa enna paathira?..
<input type="checkbox" class="lyrico-select-lyric-line" value="Malappa enna paathira?.."/>
</div>
<div class="lyrico-lyrics-wrapper">Maraikaamal marukaamal uraipeera?…
<input type="checkbox" class="lyrico-select-lyric-line" value="Maraikaamal marukaamal uraipeera?…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Itty bitty itty bitty waist
<input type="checkbox" class="lyrico-select-lyric-line" value="Itty bitty itty bitty waist"/>
</div>
<div class="lyrico-lyrics-wrapper">Moves and her grooves
<input type="checkbox" class="lyrico-select-lyric-line" value="Moves and her grooves"/>
</div>
<div class="lyrico-lyrics-wrapper">On the pole in the dance floor
<input type="checkbox" class="lyrico-select-lyric-line" value="On the pole in the dance floor"/>
</div>
<div class="lyrico-lyrics-wrapper">Witty witty witty witty babes
<input type="checkbox" class="lyrico-select-lyric-line" value="Witty witty witty witty babes"/>
</div>
<div class="lyrico-lyrics-wrapper">Wine with a shine
<input type="checkbox" class="lyrico-select-lyric-line" value="Wine with a shine"/>
</div>
<div class="lyrico-lyrics-wrapper">And a take at her zamboo
<input type="checkbox" class="lyrico-select-lyric-line" value="And a take at her zamboo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hip hop and the beat box
<input type="checkbox" class="lyrico-select-lyric-line" value="Hip hop and the beat box"/>
</div>
<div class="lyrico-lyrics-wrapper">Who gonna rub my body off
<input type="checkbox" class="lyrico-select-lyric-line" value="Who gonna rub my body off"/>
</div>
<div class="lyrico-lyrics-wrapper">Big bob never gonna stop
<input type="checkbox" class="lyrico-select-lyric-line" value="Big bob never gonna stop"/>
</div>
<div class="lyrico-lyrics-wrapper">3<div class="lyrico-lyrics-wrapper">am come and drop me off</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="am come and drop me off"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Inbathil iravil
<input type="checkbox" class="lyrico-select-lyric-line" value="Inbathil iravil"/>
</div>
<div class="lyrico-lyrics-wrapper">Niraveri illai
<input type="checkbox" class="lyrico-select-lyric-line" value="Niraveri illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakkathil varugaiyil
<input type="checkbox" class="lyrico-select-lyric-line" value="Pakkathil varugaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Avarum ivarum verillai
<input type="checkbox" class="lyrico-select-lyric-line" value="Avarum ivarum verillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Muththathai pozhiya
<input type="checkbox" class="lyrico-select-lyric-line" value="Muththathai pozhiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Suththa paththam illai
<input type="checkbox" class="lyrico-select-lyric-line" value="Suththa paththam illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Uchathai thodugaiyil
<input type="checkbox" class="lyrico-select-lyric-line" value="Uchathai thodugaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum naanum irandillai
<input type="checkbox" class="lyrico-select-lyric-line" value="Neeyum naanum irandillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Raththa kothippa kelapi vittu
<input type="checkbox" class="lyrico-select-lyric-line" value="Raththa kothippa kelapi vittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ketta kanavellam ezhupivittu
<input type="checkbox" class="lyrico-select-lyric-line" value="Ketta kanavellam ezhupivittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Patta pagalilae seyal paduthu
<input type="checkbox" class="lyrico-select-lyric-line" value="Patta pagalilae seyal paduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">24/7 <div class="lyrico-lyrics-wrapper">yen kanakku machaan..machaan..</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="yen kanakku machaan..machaan.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Machaan inga vanthira?…
<input type="checkbox" class="lyrico-select-lyric-line" value="Machaan inga vanthira?…"/>
</div>
<div class="lyrico-lyrics-wrapper">Malappa enna paathira?..
<input type="checkbox" class="lyrico-select-lyric-line" value="Malappa enna paathira?.."/>
</div>
<div class="lyrico-lyrics-wrapper">Maraikaamal marukaamal uraipeera?…
<input type="checkbox" class="lyrico-select-lyric-line" value="Maraikaamal marukaamal uraipeera?…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Indru nee punithan illai
<input type="checkbox" class="lyrico-select-lyric-line" value="Indru nee punithan illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaluvida thadaiyae illai
<input type="checkbox" class="lyrico-select-lyric-line" value="Thaluvida thadaiyae illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Madiyilae ennai thavira..
<input type="checkbox" class="lyrico-select-lyric-line" value="Madiyilae ennai thavira.."/>
</div>
<div class="lyrico-lyrics-wrapper">Puviyilae asaivaae illai..
<input type="checkbox" class="lyrico-select-lyric-line" value="Puviyilae asaivaae illai.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Itchaiyin puyalaai vanthen
<input type="checkbox" class="lyrico-select-lyric-line" value="Itchaiyin puyalaai vanthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Meesaiyai poosikonden
<input type="checkbox" class="lyrico-select-lyric-line" value="Meesaiyai poosikonden"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjilae nenjai korthen
<input type="checkbox" class="lyrico-select-lyric-line" value="Nenjilae nenjai korthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnilae mirugam serthen
<input type="checkbox" class="lyrico-select-lyric-line" value="Unnilae mirugam serthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Yoo.. tha tha tha tha tha tha tha…
<input type="checkbox" class="lyrico-select-lyric-line" value="Yoo.. tha tha tha tha tha tha tha…"/>
</div>
<div class="lyrico-lyrics-wrapper">Lala lalala la la laaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Lala lalala la la laaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Na na na na na na naa naa naa naa
<input type="checkbox" class="lyrico-select-lyric-line" value="Na na na na na na naa naa naa naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Lalala lalala lalalae ah ahh……
<input type="checkbox" class="lyrico-select-lyric-line" value="Lalala lalala lalalae ah ahh……"/>
</div>
</pre>