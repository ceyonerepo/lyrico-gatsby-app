---
title: "va kalli song lyrics"
album: "Kalavani Mappillai"
artist: "NR Raghunanthan"
lyricist: "Mohan Rajan"
director: "Gandhi Manivasagam"
path: "/albums/kalavani-mappillai-lyrics"
song: "Va Kalli"
image: ../../images/albumart/kalavani-mappillai.jpg
date: 2018-11-06
lang: tamil
youtubeLink: "https://www.youtube.com/embed/NnYmHu_KoXc"
type: "love"
singers:
  - Anthony Daasan
  - Anitha Karthikeyan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Missu miss aana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Missu miss aana"/>
</div>
<div class="lyrico-lyrics-wrapper">Mrs aagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mrs aagum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kissu kiss aana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kissu kiss aana"/>
</div>
<div class="lyrico-lyrics-wrapper">Guiness aagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Guiness aagum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thassu bussulam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thassu bussulam"/>
</div>
<div class="lyrico-lyrics-wrapper">English aagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="English aagum"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha paattu youtube
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha paattu youtube"/>
</div>
<div class="lyrico-lyrics-wrapper">Facebook ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Facebook ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Trending aagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Trending aagum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa kalli munnala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa kalli munnala"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaasam malli pinnala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaasam malli pinnala"/>
</div>
<div class="lyrico-lyrics-wrapper">Seema nelli kannala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seema nelli kannala"/>
</div>
<div class="lyrico-lyrics-wrapper">Seendi vitta thannala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seendi vitta thannala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ah ah isottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ah ah isottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ah ah ah alekku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ah ah ah alekku"/>
</div>
<div class="lyrico-lyrics-wrapper">Ah ah isottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ah ah isottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ah ah ah alekku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ah ah ah alekku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ah vittu sollalla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ah vittu sollalla"/>
</div>
<div class="lyrico-lyrics-wrapper">Aala vettum mappillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aala vettum mappillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Poo mottu anbaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poo mottu anbaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Pooti vecha nenjala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pooti vecha nenjala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa nee alekka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa nee alekka"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaada nee alekka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada nee alekka"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee ennai melakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee ennai melakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Thookkikadaa alekka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thookkikadaa alekka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kandapadi aasai vechan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandapadi aasai vechan"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum un mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum un mela"/>
</div>
<div class="lyrico-lyrics-wrapper">Sonna padi aadurenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sonna padi aadurenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aiyyo unnala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyyo unnala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaththi kaththi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaththi kaththi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandai podum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandai podum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandai illa paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandai illa paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaththi vechu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaththi vechu"/>
</div>
<div class="lyrico-lyrics-wrapper">Konnu podum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konnu podum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandaiya nee paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandaiya nee paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ah ah isokku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ah ah isokku"/>
</div>
<div class="lyrico-lyrics-wrapper">Ah ah ah alekku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ah ah ah alekku"/>
</div>
<div class="lyrico-lyrics-wrapper">Ah ah isokku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ah ah isokku"/>
</div>
<div class="lyrico-lyrics-wrapper">Ah ah ah alekku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ah ah ah alekku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unakkunnu samanjirukken
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkunnu samanjirukken"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhaga amanjirukken
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhaga amanjirukken"/>
</div>
<div class="lyrico-lyrics-wrapper">Kitta vanthu ottinakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kitta vanthu ottinakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Etti neeyum oduriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Etti neeyum oduriyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey osarama valarnthirukken
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey osarama valarnthirukken"/>
</div>
<div class="lyrico-lyrics-wrapper">Ozhukkama naan irukken
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ozhukkama naan irukken"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannu renda moodi kittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu renda moodi kittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanavilla theduriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanavilla theduriye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En nenja thottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En nenja thottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee ennai muttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee ennai muttu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini meththaiyila jallikattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini meththaiyila jallikattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En kaiya thotttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kaiya thotttu"/>
</div>
<div class="lyrico-lyrics-wrapper">En kaala thottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kaala thottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey meththaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey meththaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini mallukattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini mallukattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey onnae onnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey onnae onnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannae kannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannae kannu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippo kodu pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo kodu pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey intha pudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey intha pudi"/>
</div>
<div class="lyrico-lyrics-wrapper">Mothathaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mothathaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanthuttenae naanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanthuttenae naanu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa en thangatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa en thangatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethaan en pondatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethaan en pondatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee adi illatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee adi illatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan kerangiduven kannatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan kerangiduven kannatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa en aithala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa en aithala"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaada en aithala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada en aithala"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan un manasula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan un manasula"/>
</div>
<div class="lyrico-lyrics-wrapper">Vechurikken aithala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vechurikken aithala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoi"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoi hoi hoi hoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoi hoi hoi hoi"/>
</div>
<div class="lyrico-lyrics-wrapper">Alekka lekka lekka alekka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alekka lekka lekka alekka"/>
</div>
<div class="lyrico-lyrics-wrapper">Alekka lekka lekka alekka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alekka lekka lekka alekka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kazhugu kanazhagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kazhugu kanazhagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasa kothuriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasa kothuriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadugu machazhagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadugu machazhagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Usura pichu poguriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usura pichu poguriyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aasai pechu azhaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai pechu azhaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Aala saikuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aala saikuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ice-eh vechu ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ice-eh vechu ennai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nice-u panna paakuriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nice-u panna paakuriyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan thekki vecha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan thekki vecha"/>
</div>
<div class="lyrico-lyrics-wrapper">En aasai ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En aasai ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi theana 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi theana "/>
</div>
<div class="lyrico-lyrics-wrapper">pola thanthidava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pola thanthidava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En thevai ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En thevai ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan sollidava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan sollidava"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthu thaeni 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthu thaeni "/>
</div>
<div class="lyrico-lyrics-wrapper">pola kottidava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pola kottidava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un pakkathila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un pakkathila"/>
</div>
<div class="lyrico-lyrics-wrapper">Bommai pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bommai pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaguren di naanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaguren di naanu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un moochi kaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un moochi kaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vangi naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vangi naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Moochi vida poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moochi vida poren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa en lolakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa en lolakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaadi en lolakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadi en lolakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan un melakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan un melakku"/>
</div>
<div class="lyrico-lyrics-wrapper">En manasa konjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En manasa konjam"/>
</div>
<div class="lyrico-lyrics-wrapper">Cool aakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cool aakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa en aithala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa en aithala"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaada en aithala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada en aithala"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan un manasula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan un manasula"/>
</div>
<div class="lyrico-lyrics-wrapper">Vechurikken aithala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vechurikken aithala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey vaa kalli munnala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey vaa kalli munnala"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaasam malli pinnala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaasam malli pinnala"/>
</div>
<div class="lyrico-lyrics-wrapper">Seema nelli kannala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seema nelli kannala"/>
</div>
<div class="lyrico-lyrics-wrapper">Seendi vitta thannala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seendi vitta thannala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ah vittu sollalla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ah vittu sollalla"/>
</div>
<div class="lyrico-lyrics-wrapper">Aala vettum mappillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aala vettum mappillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Poo moottu anbaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poo moottu anbaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Pooti vecha nenjala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pooti vecha nenjala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kandapadi aasai vechan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandapadi aasai vechan"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum un mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum un mela"/>
</div>
<div class="lyrico-lyrics-wrapper">Sonna padi aadurenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sonna padi aadurenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aiyyo unnala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyyo unnala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaththi kaththi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaththi kaththi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandai podum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandai podum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandai illa paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandai illa paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaththi vechu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaththi vechu"/>
</div>
<div class="lyrico-lyrics-wrapper">Konnu podum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konnu podum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandaiya nee paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandaiya nee paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa en thangatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa en thangatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethaan en pondatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethaan en pondatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee adi illatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee adi illatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan kerangiduven kannatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan kerangiduven kannatti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa nee alekka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa nee alekka"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaada nee alekka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada nee alekka"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee ennai melakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee ennai melakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Thookkikadaa alekka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thookkikadaa alekka"/>
</div>
</pre>
