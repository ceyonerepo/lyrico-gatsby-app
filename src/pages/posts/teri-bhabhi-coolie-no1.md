---
title: "teri bhabhi song lyrics"
album: "Coolie No1"
artist: "Javed - Mohsin"
lyricist: "Danish Sabri"
director: "David Dhawan"
path: "/albums/coolie-no1-lyrics"
song: "Teri Bhabhi"
image: ../../images/albumart/coolie-no1.jpg
date: 2020-12-25
lang: hindi
youtubeLink: "https://www.youtube.com/embed/oq6av2OmQ1w"
type: "mass"
singers:
  - Javed - Mohsin
  - Dev Negi
  - Neha Kakkar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Sab jidhar wo hai udhar dekh rahe hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sab jidhar wo hai udhar dekh rahe hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Hum to public ki nazar dekh rahe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hum to public ki nazar dekh rahe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Main samajh hi gaya tha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main samajh hi gaya tha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ke baat kuch badi hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ke baat kuch badi hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Agar baat kuch badi nahi to
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Agar baat kuch badi nahi to"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuch to gadbadi hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuch to gadbadi hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hatt jaa saamne se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hatt jaa saamne se"/>
</div>
<div class="lyrico-lyrics-wrapper">Teri bhabhi khadi hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teri bhabhi khadi hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Hatt jaa saamne se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hatt jaa saamne se"/>
</div>
<div class="lyrico-lyrics-wrapper">Meri ankhiyan ladi hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meri ankhiyan ladi hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arre hatt jaa saamne se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arre hatt jaa saamne se"/>
</div>
<div class="lyrico-lyrics-wrapper">Teri bhabhi khadi hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teri bhabhi khadi hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Hatt jaa saamne se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hatt jaa saamne se"/>
</div>
<div class="lyrico-lyrics-wrapper">Meri ankhiyan ladi hain
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meri ankhiyan ladi hain"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Apni baahon mein tujhko main le lunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Apni baahon mein tujhko main le lunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Teri zulfon se day night khelunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teri zulfon se day night khelunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Jo tu roothegi tujhko manaunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jo tu roothegi tujhko manaunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Tere daddy ke nakhre uthaunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere daddy ke nakhre uthaunga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jab bhi tu dekhti hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jab bhi tu dekhti hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Meri jaan mujhko hans ke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meri jaan mujhko hans ke"/>
</div>
<div class="lyrico-lyrics-wrapper">Aisa lage hai mujhko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aisa lage hai mujhko"/>
</div>
<div class="lyrico-lyrics-wrapper">Nagin gayi hai dass ke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nagin gayi hai dass ke"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hatt jaa saamne se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hatt jaa saamne se"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho bhabhi ho bhabhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho bhabhi ho bhabhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho bhabhi bhabhi bhabhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho bhabhi bhabhi bhabhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hatt jaa saamne se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hatt jaa saamne se"/>
</div>
<div class="lyrico-lyrics-wrapper">Tere bhaiya khade hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere bhaiya khade hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Arre hatt jaa saamne se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arre hatt jaa saamne se"/>
</div>
<div class="lyrico-lyrics-wrapper">Humre naina lade hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Humre naina lade hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sab oh idhar main hoon jidhar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sab oh idhar main hoon jidhar"/>
</div>
<div class="lyrico-lyrics-wrapper">Dekh rahe hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dekh rahe hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Hum to public ki nazar dekh rahe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hum to public ki nazar dekh rahe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Main samajh hi gayi thi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main samajh hi gayi thi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ke baat kuch badi hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ke baat kuch badi hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Agar baat kuch badi nahi to
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Agar baat kuch badi nahi to"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuch to gadbadi hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuch to gadbadi hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Humri bhabhi khadi hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Humri bhabhi khadi hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Inki ankhiyan ladi hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inki ankhiyan ladi hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Teri bhabhi..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teri bhabhi.."/>
</div>
<div class="lyrico-lyrics-wrapper">Tere bhaiya..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere bhaiya.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho bhabhi ho bhabhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho bhabhi ho bhabhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho bhabhi bhabhi bhabhi haan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho bhabhi bhabhi bhabhi haan"/>
</div>
</pre>
