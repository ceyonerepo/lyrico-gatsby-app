---
title: "thozha vaanam dhooramillai song lyrics"
album: "Ilaignan"
artist: "Vidyasagar"
lyricist: "Pa. Vijay"
director: "Suresh Krishna"
path: "/albums/ilaignan-lyrics"
song: "Thozha Vaanam Dhooramillai"
image: ../../images/albumart/ilaignan.jpg
date: 2011-01-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/rWWPA5ESZFc"
type: "motivational"
singers:
  - Hariharan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Thoazha vaanam thooram illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoazha vaanam thooram illai"/>
</div>
<div class="lyrico-lyrics-wrapper">thoatram maraivu kaatrumillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoatram maraivu kaatrumillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoazha vaanam thooram illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoazha vaanam thooram illai"/>
</div>
<div class="lyrico-lyrics-wrapper">thoatram maraivu kaatrumillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoatram maraivu kaatrumillai"/>
</div>
<div class="lyrico-lyrics-wrapper">veettil thongum viralukkellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veettil thongum viralukkellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">vin meen kidaippadhillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vin meen kidaippadhillai"/>
</div>
<div class="lyrico-lyrics-wrapper">thoalvi endra sollil thaedu vetri endra sollai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoalvi endra sollil thaedu vetri endra sollai"/>
</div>
<div class="lyrico-lyrics-wrapper">vegam adhai allikkattu soagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vegam adhai allikkattu soagam"/>
</div>
<div class="lyrico-lyrics-wrapper">adhai thooram vittu vetri yetrikkattu yetrikkattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adhai thooram vittu vetri yetrikkattu yetrikkattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thoazha vaanam thooram illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoazha vaanam thooram illai"/>
</div>
<div class="lyrico-lyrics-wrapper">thoatram maraivu kaatrumillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoatram maraivu kaatrumillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoazha vaanam thooram illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoazha vaanam thooram illai"/>
</div>
<div class="lyrico-lyrics-wrapper">thoatram maraivu kaatrumillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoatram maraivu kaatrumillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uligalai thaangu silai varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uligalai thaangu silai varum"/>
</div>
<div class="lyrico-lyrics-wrapper">valigalai thaangu oli varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valigalai thaangu oli varum"/>
</div>
<div class="lyrico-lyrics-wrapper">nambikkai ondre thunai varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nambikkai ondre thunai varum"/>
</div>
<div class="lyrico-lyrics-wrapper">nichayam velvoam anaivarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nichayam velvoam anaivarum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uligalai thaangu silai varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uligalai thaangu silai varum"/>
</div>
<div class="lyrico-lyrics-wrapper">valigalai thaangu oli varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valigalai thaangu oli varum"/>
</div>
<div class="lyrico-lyrics-wrapper">nambikkai ondre thunai varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nambikkai ondre thunai varum"/>
</div>
<div class="lyrico-lyrics-wrapper">nichayam velvoam anaivarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nichayam velvoam anaivarum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Netri vervai poandra vedhathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netri vervai poandra vedhathai"/>
</div>
<div class="lyrico-lyrics-wrapper">ingey yaarum ezhudhavillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ingey yaarum ezhudhavillai"/>
</div>
<div class="lyrico-lyrics-wrapper">avamaanathai poandra anubava paadam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avamaanathai poandra anubava paadam"/>
</div>
<div class="lyrico-lyrics-wrapper">machchil iruppadhillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="machchil iruppadhillai"/>
</div>
<div class="lyrico-lyrics-wrapper">mudiyum endrey sindhippoam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudiyum endrey sindhippoam"/>
</div>
<div class="lyrico-lyrics-wrapper">thadaigalai edhaiyum sandhippoam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thadaigalai edhaiyum sandhippoam"/>
</div>
<div class="lyrico-lyrics-wrapper">iravai pagalaai neettippoam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iravai pagalaai neettippoam"/>
</div>
<div class="lyrico-lyrics-wrapper">innum innum saadhippoam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="innum innum saadhippoam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidiyidu vidiyidhu vidiyalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidiyidu vidiyidhu vidiyalum"/>
</div>
<div class="lyrico-lyrics-wrapper">vidiyidhu vidiyidhu sandhippoam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidiyidhu vidiyidhu sandhippoam"/>
</div>
<div class="lyrico-lyrics-wrapper">mudiyidhu mudiyidhu iravugal mudiyidhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudiyidhu mudiyidhu iravugal mudiyidhu"/>
</div>
<div class="lyrico-lyrics-wrapper">ini ezhu ini ezhu saadhippoam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ini ezhu ini ezhu saadhippoam"/>
</div>
<div class="lyrico-lyrics-wrapper">pudhiyadhu pudhiyadhu sindhippoam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pudhiyadhu pudhiyadhu sindhippoam"/>
</div>
<div class="lyrico-lyrics-wrapper">pazhaiyadhu pazhaiyadhu nindhippoam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pazhaiyadhu pazhaiyadhu nindhippoam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhuvadhu ondrum thavaralla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhuvadhu ondrum thavaralla"/>
</div>
<div class="lyrico-lyrics-wrapper">vizha vizha ezhuvoam pagaivella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vizha vizha ezhuvoam pagaivella"/>
</div>
<div class="lyrico-lyrics-wrapper">vizhigalil kanneer vidhiyalla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vizhigalil kanneer vidhiyalla"/>
</div>
<div class="lyrico-lyrics-wrapper">vidhiyai kirukka mella mella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidhiyai kirukka mella mella"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhuvadhu ondrum thavaralla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhuvadhu ondrum thavaralla"/>
</div>
<div class="lyrico-lyrics-wrapper">vizha vizha ezhuvoam pagaivella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vizha vizha ezhuvoam pagaivella"/>
</div>
<div class="lyrico-lyrics-wrapper">vizhigalil kanneer vidhiyalla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vizhigalil kanneer vidhiyalla"/>
</div>
<div class="lyrico-lyrics-wrapper">vidhiyai kirukka mella mella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidhiyai kirukka mella mella"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadal alaigalai alaigalai alli vaithaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadal alaigalai alaigalai alli vaithaal"/>
</div>
<div class="lyrico-lyrics-wrapper">sooriyan alaindhidumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sooriyan alaindhidumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">ada naatkalum poagum nadappugal poagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada naatkalum poagum nadappugal poagum"/>
</div>
<div class="lyrico-lyrics-wrapper">nambikkai poaividumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nambikkai poaividumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edhu thaan nammaal mudiyaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhu thaan nammaal mudiyaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">edhuvum ingey kidaiyaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edhuvum ingey kidaiyaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">edhuvum ingey kidaiyaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edhuvum ingey kidaiyaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">maatram endra vaarthaikku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maatram endra vaarthaikku "/>
</div>
<div class="lyrico-lyrics-wrapper">arththam enna theriyaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arththam enna theriyaadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thoazha vaanam thooram illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoazha vaanam thooram illai"/>
</div>
<div class="lyrico-lyrics-wrapper">thoatram maraivu kaatrumillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoatram maraivu kaatrumillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoazha vaanam thooram illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoazha vaanam thooram illai"/>
</div>
<div class="lyrico-lyrics-wrapper">thoatram maraivu kaatrumillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoatram maraivu kaatrumillai"/>
</div>
</pre>
