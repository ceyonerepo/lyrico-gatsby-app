---
title: "kirukkan song lyrics"
album: "Kanavu Variyam"
artist: "Shyam Benjamin"
lyricist: "Arun Chidambaram"
director: "Arun Chidambaram"
path: "/albums/kanavu-variyam-lyrics"
song: "Kirukkan"
image: ../../images/albumart/kanavu-variyam.jpg
date: 2017-02-24
lang: tamil
youtubeLink: "https://www.youtube.com/embed/-jm9BxXy6do"
type: "mass"
singers:
  - Sirkazhi G Sivachidambaram
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ivan Kirukkan Dhaan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Kirukkan Dhaan "/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan Kirukkan Dhaan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Kirukkan Dhaan "/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan Kirukkan Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Kirukkan Dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan Kirukkan Dhaan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Kirukkan Dhaan "/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan Kirukkan Dhaan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Kirukkan Dhaan "/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan Kirukkan Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Kirukkan Dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan Kirukkan Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Kirukkan Dhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thatta Thatta Dhaan Neeyum Aagalaam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thatta Thatta Dhaan Neeyum Aagalaam "/>
</div>
<div class="lyrico-lyrics-wrapper">Thatta Thatta Dhaan Naanum Aagalaam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thatta Thatta Dhaan Naanum Aagalaam "/>
</div>
<div class="lyrico-lyrics-wrapper">Thatta Thatta Dhaan Yaarum Aagalaam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thatta Thatta Dhaan Yaarum Aagalaam "/>
</div>
<div class="lyrico-lyrics-wrapper">Edison'aa Oru Edison'aa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edison'aa Oru Edison'aa "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thatta Thatta Dhaan Neeyum Aagalaam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thatta Thatta Dhaan Neeyum Aagalaam "/>
</div>
<div class="lyrico-lyrics-wrapper">Thatta Thatta Dhaan Naanum Aagalaam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thatta Thatta Dhaan Naanum Aagalaam "/>
</div>
<div class="lyrico-lyrics-wrapper">Thatta Thatta Dhaan Yaarum Aagalaam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thatta Thatta Dhaan Yaarum Aagalaam "/>
</div>
<div class="lyrico-lyrics-wrapper">Edison'aa Oru Edison'aa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edison'aa Oru Edison'aa "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uzhaippai Kottu Tin Tin'a 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uzhaippai Kottu Tin Tin'a "/>
</div>
<div class="lyrico-lyrics-wrapper">Panam Thaanaa Kottum Tone Tone'a 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panam Thaanaa Kottum Tone Tone'a "/>
</div>
<div class="lyrico-lyrics-wrapper">Uzhaippai Kottu Tin Tin'a 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uzhaippai Kottu Tin Tin'a "/>
</div>
<div class="lyrico-lyrics-wrapper">Panam Thaanaa Kottum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panam Thaanaa Kottum "/>
</div>
<div class="lyrico-lyrics-wrapper">Tone Tone Tone'a 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tone Tone Tone'a "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thatta Thatta Dhaan Neeyum Aagalaam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thatta Thatta Dhaan Neeyum Aagalaam "/>
</div>
<div class="lyrico-lyrics-wrapper">Thatta Thatta Dhaan Naanum Aagalaam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thatta Thatta Dhaan Naanum Aagalaam "/>
</div>
<div class="lyrico-lyrics-wrapper">Thatta Thatta Dhaan Yaarum Aagalaam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thatta Thatta Dhaan Yaarum Aagalaam "/>
</div>
<div class="lyrico-lyrics-wrapper">Edison'aa Oru Edison'aa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edison'aa Oru Edison'aa "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">X-ray Thandha Conradaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="X-ray Thandha Conradaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Manaivi Sonnaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manaivi Sonnaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Kirukkan Kirukkan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kirukkan Kirukkan "/>
</div>
<div class="lyrico-lyrics-wrapper">Kirukkan Kirukkan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kirukkan Kirukkan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Car'ai Kanda H Ford'ai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Car'ai Kanda H Ford'ai "/>
</div>
<div class="lyrico-lyrics-wrapper">Uravu Sonnadhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uravu Sonnadhu "/>
</div>
<div class="lyrico-lyrics-wrapper">Kirukkan Kirukkan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kirukkan Kirukkan "/>
</div>
<div class="lyrico-lyrics-wrapper">Kirukkan Kirukkan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kirukkan Kirukkan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mudhalil Kudaiyudan Nadandhavana 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhalil Kudaiyudan Nadandhavana "/>
</div>
<div class="lyrico-lyrics-wrapper">Theruvay Sonnadhu Kirukkan Dhaan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theruvay Sonnadhu Kirukkan Dhaan "/>
</div>
<div class="lyrico-lyrics-wrapper">Bhoomi Urundai Endru Sonnavana 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhoomi Urundai Endru Sonnavana "/>
</div>
<div class="lyrico-lyrics-wrapper">Oorey Sonnadhu Kirukkan Dhaan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorey Sonnadhu Kirukkan Dhaan "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aattu Mandhai Kootaththiley 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aattu Mandhai Kootaththiley "/>
</div>
<div class="lyrico-lyrics-wrapper">Arivai Tholaiththavan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arivai Tholaiththavan "/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaam Arignan Dhaan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaam Arignan Dhaan "/>
</div>
<div class="lyrico-lyrics-wrapper">Sindhikka Marandha Ulagaththila 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sindhikka Marandha Ulagaththila "/>
</div>
<div class="lyrico-lyrics-wrapper">Arivai Vidhaippavan Kirukkan Dhaan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arivai Vidhaippavan Kirukkan Dhaan "/>
</div>
<div class="lyrico-lyrics-wrapper">Arivai Vidhaippavan Kirukkan Dhaan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arivai Vidhaippavan Kirukkan Dhaan "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thatta Thatta Dhaan Neeyum Aagalaam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thatta Thatta Dhaan Neeyum Aagalaam "/>
</div>
<div class="lyrico-lyrics-wrapper">Thatta Thatta Dhaan Naanum Aagalaam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thatta Thatta Dhaan Naanum Aagalaam "/>
</div>
<div class="lyrico-lyrics-wrapper">Thatta Thatta Dhaan Yaarum Aagalaam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thatta Thatta Dhaan Yaarum Aagalaam "/>
</div>
<div class="lyrico-lyrics-wrapper">Edison'aa Oru Edison'aa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edison'aa Oru Edison'aa "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kirukkan Chinnanchiru Moolaikkey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kirukkan Chinnanchiru Moolaikkey "/>
</div>
<div class="lyrico-lyrics-wrapper">Agilam Aalum Balamundu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Agilam Aalum Balamundu "/>
</div>
<div class="lyrico-lyrics-wrapper">Kirukkan Maatri Yosi Vallavanay 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kirukkan Maatri Yosi Vallavanay "/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkum Mannil Idamundu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkum Mannil Idamundu "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naalai Vendaam Indraikkey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalai Vendaam Indraikkey "/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhudaa Neeyum Very Kondu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhudaa Neeyum Very Kondu "/>
</div>
<div class="lyrico-lyrics-wrapper">Nithtam Vanangum Saamikku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nithtam Vanangum Saamikku "/>
</div>
<div class="lyrico-lyrics-wrapper">Adhigam Ingay Vasavundu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhigam Ingay Vasavundu "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Panaththai Thedum Kootaththirku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panaththai Thedum Kootaththirku "/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavai Thuraththa Solbhavan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavai Thuraththa Solbhavan "/>
</div>
<div class="lyrico-lyrics-wrapper">Kirukkan Dhaan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kirukkan Dhaan "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neengal Vaazhum Ulagaththaiyae 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neengal Vaazhum Ulagaththaiyae "/>
</div>
<div class="lyrico-lyrics-wrapper">Sedhukkiyadhu Indha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sedhukkiyadhu Indha "/>
</div>
<div class="lyrico-lyrics-wrapper">Kirukkargal Dhaan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kirukkargal Dhaan "/>
</div>
<div class="lyrico-lyrics-wrapper">Thambi Sedhukkiyadhu Indha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thambi Sedhukkiyadhu Indha "/>
</div>
<div class="lyrico-lyrics-wrapper">Kirukkargal Dhaan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kirukkargal Dhaan "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivan Kirukkan Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Kirukkan Dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan Kirukkan Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Kirukkan Dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan Kirukkan Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Kirukkan Dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan Kirukkan Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Kirukkan Dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan Kirukkan Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Kirukkan Dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan Kirukkan Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Kirukkan Dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan Kirukkan Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Kirukkan Dhaan"/>
</div>
</pre>
