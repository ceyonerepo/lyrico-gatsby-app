---
title: "uchi malai pillaiyare song lyrics"
album: "Kannakkol"
artist: "Bobby"
lyricist: "Muthuvijayan"
director: "V.A.Kumaresan"
path: "/albums/kannakkol-lyrics"
song: "Uchi Malai Pillaiyare"
image: ../../images/albumart/kannakkol.jpg
date: 2018-06-15
lang: tamil
youtubeLink: "https://www.youtube.com/embed/dpPSBxir_OY"
type: "love"
singers:
  - Haricharan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">uchi malai pillaiyare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uchi malai pillaiyare"/>
</div>
<div class="lyrico-lyrics-wrapper">enna nadakuthada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna nadakuthada"/>
</div>
<div class="lyrico-lyrics-wrapper">panju panja nenju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="panju panja nenju"/>
</div>
<div class="lyrico-lyrics-wrapper">ipo pinju parakuthada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ipo pinju parakuthada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">uchi malai pillaiyare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uchi malai pillaiyare"/>
</div>
<div class="lyrico-lyrics-wrapper">enna nadakuthada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna nadakuthada"/>
</div>
<div class="lyrico-lyrics-wrapper">panju panja nenju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="panju panja nenju"/>
</div>
<div class="lyrico-lyrics-wrapper">ipo pinju parakuthada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ipo pinju parakuthada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thala mela nilavu mella 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thala mela nilavu mella "/>
</div>
<div class="lyrico-lyrics-wrapper">thadavi kuduthu pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thadavi kuduthu pochu"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu pola enaku munne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu pola enaku munne"/>
</div>
<div class="lyrico-lyrics-wrapper">adachathu illa moochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adachathu illa moochu"/>
</div>
<div class="lyrico-lyrics-wrapper">kalli chediyila malli kodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalli chediyila malli kodi"/>
</div>
<div class="lyrico-lyrics-wrapper">onnu suthikichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="onnu suthikichu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">uchi malai pillaiyare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uchi malai pillaiyare"/>
</div>
<div class="lyrico-lyrics-wrapper">enna nadakuthada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna nadakuthada"/>
</div>
<div class="lyrico-lyrics-wrapper">panju panja nenju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="panju panja nenju"/>
</div>
<div class="lyrico-lyrics-wrapper">ipo pinju parakuthada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ipo pinju parakuthada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">boomikitta anumaithiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="boomikitta anumaithiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">vaangikittu mala varuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaangikittu mala varuma"/>
</div>
<div class="lyrico-lyrics-wrapper">mummukitta ketukittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mummukitta ketukittu"/>
</div>
<div class="lyrico-lyrics-wrapper">poo varuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poo varuma"/>
</div>
<div class="lyrico-lyrics-wrapper">veru kitta ragasiyama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veru kitta ragasiyama"/>
</div>
<div class="lyrico-lyrics-wrapper">pesi vittu kila varuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pesi vittu kila varuma"/>
</div>
<div class="lyrico-lyrics-wrapper">namma kitta solli vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="namma kitta solli vittu"/>
</div>
<div class="lyrico-lyrics-wrapper">sugam varuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sugam varuma"/>
</div>
<div class="lyrico-lyrics-wrapper">nadikum manasu thani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nadikum manasu thani"/>
</div>
<div class="lyrico-lyrics-wrapper">marama thoppa matha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="marama thoppa matha"/>
</div>
<div class="lyrico-lyrics-wrapper">thuna varuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thuna varuma"/>
</div>
<div class="lyrico-lyrics-wrapper">ipo eduka therinja manasu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ipo eduka therinja manasu "/>
</div>
<div class="lyrico-lyrics-wrapper">koduka thunajathu enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koduka thunajathu enna"/>
</div>
<div class="lyrico-lyrics-wrapper">ada elutha padika therinja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada elutha padika therinja"/>
</div>
<div class="lyrico-lyrics-wrapper">kavitha varanum unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kavitha varanum unna"/>
</div>
<div class="lyrico-lyrics-wrapper">oru sembu thaniyila thala 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru sembu thaniyila thala "/>
</div>
<div class="lyrico-lyrics-wrapper">kulikiren epadi theriyala 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulikiren epadi theriyala "/>
</div>
<div class="lyrico-lyrics-wrapper">ada kaagitha kapalil kadal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada kaagitha kapalil kadal"/>
</div>
<div class="lyrico-lyrics-wrapper">kadakuren onnume puriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadakuren onnume puriyala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">uchi malai pillaiyare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uchi malai pillaiyare"/>
</div>
<div class="lyrico-lyrics-wrapper">enna nadakuthada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna nadakuthada"/>
</div>
<div class="lyrico-lyrics-wrapper">panju panja nenju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="panju panja nenju"/>
</div>
<div class="lyrico-lyrics-wrapper">ipo pinju parakuthada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ipo pinju parakuthada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thala mela nilavu mella 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thala mela nilavu mella "/>
</div>
<div class="lyrico-lyrics-wrapper">thadavi kuduthu pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thadavi kuduthu pochu"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu pola enaku munne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu pola enaku munne"/>
</div>
<div class="lyrico-lyrics-wrapper">adachathu illa moochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adachathu illa moochu"/>
</div>
<div class="lyrico-lyrics-wrapper">kalli chediyila malli kodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalli chediyila malli kodi"/>
</div>
<div class="lyrico-lyrics-wrapper">onnu suthikichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="onnu suthikichu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">uchi malai pillaiyare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uchi malai pillaiyare"/>
</div>
<div class="lyrico-lyrics-wrapper">enna nadakuthada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna nadakuthada"/>
</div>
<div class="lyrico-lyrics-wrapper">panju panja nenju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="panju panja nenju"/>
</div>
<div class="lyrico-lyrics-wrapper">ipo pinju parakuthada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ipo pinju parakuthada"/>
</div>
</pre>
