---
title: "manigal kulunguthey song lyrics"
album: "Madurai Manikuravar"
artist: "Isaignani Ilaiyaraaja"
lyricist: "Kavignar Muthulingam"
director: "K Raajarishi"
path: "/albums/madurai-manikuravar-lyrics"
song: "Manigal Kulunguthey"
image: ../../images/albumart/madurai-manikuravar.jpg
date: 2021-12-31
lang: tamil
youtubeLink: 
type: "love"
singers:
  - Karthik
  - Vibhavari Joshi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">manigal kulunguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manigal kulunguthe"/>
</div>
<div class="lyrico-lyrics-wrapper">manasu kedukuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasu kedukuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">kolusu kolusu kolusu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kolusu kolusu kolusu"/>
</div>
<div class="lyrico-lyrics-wrapper">kathavu thirakuthe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathavu thirakuthe "/>
</div>
<div class="lyrico-lyrics-wrapper">kanavu theriyuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavu theriyuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">puthusu puthusu puthusu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthusu puthusu puthusu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">manigal kulunguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manigal kulunguthe"/>
</div>
<div class="lyrico-lyrics-wrapper">manasu kedukuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasu kedukuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">kolusu kolusu kolusu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kolusu kolusu kolusu"/>
</div>
<div class="lyrico-lyrics-wrapper">kathavu thirakuthe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathavu thirakuthe "/>
</div>
<div class="lyrico-lyrics-wrapper">kanavu theriyuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavu theriyuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">puthusu puthusu puthusu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthusu puthusu puthusu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">maduramil kathalar yar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maduramil kathalar yar"/>
</div>
<div class="lyrico-lyrics-wrapper">yaro ithu varai iruntharo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaro ithu varai iruntharo"/>
</div>
<div class="lyrico-lyrics-wrapper">adi thadiyil kathalai marantharo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi thadiyil kathalai marantharo"/>
</div>
<div class="lyrico-lyrics-wrapper">kedupidiyil kidantharo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kedupidiyil kidantharo"/>
</div>
<div class="lyrico-lyrics-wrapper">pottu matuthadi kathale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pottu matuthadi kathale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">manigal kulunguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manigal kulunguthe"/>
</div>
<div class="lyrico-lyrics-wrapper">manasu kedukuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasu kedukuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">kolusu kolusu kolusu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kolusu kolusu kolusu"/>
</div>
<div class="lyrico-lyrics-wrapper">kathavu thirakuthe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathavu thirakuthe "/>
</div>
<div class="lyrico-lyrics-wrapper">kanavu theriyuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavu theriyuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">puthusu puthusu puthusu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthusu puthusu puthusu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nee nadantha vagai palam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee nadantha vagai palam"/>
</div>
<div class="lyrico-lyrics-wrapper">nan nadanthu ponale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan nadanthu ponale"/>
</div>
<div class="lyrico-lyrics-wrapper">un manasil ullathu ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un manasil ullathu ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">enaku eduthu sollatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enaku eduthu sollatho"/>
</div>
<div class="lyrico-lyrics-wrapper">poo kodukum ponna paalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poo kodukum ponna paalam"/>
</div>
<div class="lyrico-lyrics-wrapper">puriyum vartha sonnale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puriyum vartha sonnale"/>
</div>
<div class="lyrico-lyrics-wrapper">poova pola manatha pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poova pola manatha pola"/>
</div>
<div class="lyrico-lyrics-wrapper">sernthu iruka sonnale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sernthu iruka sonnale"/>
</div>
<div class="lyrico-lyrics-wrapper">karupu mayil thogai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karupu mayil thogai"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu ellam kanda padi aadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu ellam kanda padi aadum"/>
</div>
<div class="lyrico-lyrics-wrapper">otha vali pogum thoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="otha vali pogum thoda"/>
</div>
<div class="lyrico-lyrics-wrapper">muthu vali thirumbum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muthu vali thirumbum"/>
</div>
<div class="lyrico-lyrics-wrapper">en nenapil vanthathu ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en nenapil vanthathu ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">un nenapil vanthiduma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un nenapil vanthiduma"/>
</div>
<div class="lyrico-lyrics-wrapper">en nenapil vanthathu ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en nenapil vanthathu ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">un nenapil vanthiduma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un nenapil vanthiduma"/>
</div>
<div class="lyrico-lyrics-wrapper">un idathil yaru ketpathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un idathil yaru ketpathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">manigal kulunguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manigal kulunguthe"/>
</div>
<div class="lyrico-lyrics-wrapper">manasu kedukuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasu kedukuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">kolusu kolusu kolusu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kolusu kolusu kolusu"/>
</div>
<div class="lyrico-lyrics-wrapper">kathavu thirakuthe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathavu thirakuthe "/>
</div>
<div class="lyrico-lyrics-wrapper">kanavu theriyuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavu theriyuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">puthusu puthusu puthusu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthusu puthusu puthusu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">malaiyile poovai parka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malaiyile poovai parka"/>
</div>
<div class="lyrico-lyrics-wrapper">mela vaasal nanum vanthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mela vaasal nanum vanthen"/>
</div>
<div class="lyrico-lyrics-wrapper">angu iruntha maalai ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="angu iruntha maalai ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">avalai thedum alagai kanden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avalai thedum alagai kanden"/>
</div>
<div class="lyrico-lyrics-wrapper">poojaiku endru maalai vangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poojaiku endru maalai vangi"/>
</div>
<div class="lyrico-lyrics-wrapper">kovil sella ennam konden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kovil sella ennam konden"/>
</div>
<div class="lyrico-lyrics-wrapper">ennai ethir parthu theivam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennai ethir parthu theivam"/>
</div>
<div class="lyrico-lyrics-wrapper">enthan ethiril thondra kanden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enthan ethiril thondra kanden"/>
</div>
<div class="lyrico-lyrics-wrapper">gopurathu deepam sudar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gopurathu deepam sudar"/>
</div>
<div class="lyrico-lyrics-wrapper">veesi solvathu theriyuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veesi solvathu theriyuma"/>
</div>
<div class="lyrico-lyrics-wrapper">mandapathu maniyil nal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mandapathu maniyil nal"/>
</div>
<div class="lyrico-lyrics-wrapper">vaarthai katpathu puriyuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaarthai katpathu puriyuma"/>
</div>
<div class="lyrico-lyrics-wrapper">pon polusa vanthu vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pon polusa vanthu vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">poon siraga thanthathu inga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poon siraga thanthathu inga"/>
</div>
<div class="lyrico-lyrics-wrapper">pon polusa vanthu vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pon polusa vanthu vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">poon siraga thanthathu inga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poon siraga thanthathu inga"/>
</div>
<div class="lyrico-lyrics-wrapper">nan paranthu engu povathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan paranthu engu povathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">manigal kulunguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manigal kulunguthe"/>
</div>
<div class="lyrico-lyrics-wrapper">manasu kedukuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasu kedukuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">kolusu kolusu kolusu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kolusu kolusu kolusu"/>
</div>
<div class="lyrico-lyrics-wrapper">kathavu thirakuthe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathavu thirakuthe "/>
</div>
<div class="lyrico-lyrics-wrapper">kanavu theriyuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavu theriyuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">puthusu puthusu puthusu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthusu puthusu puthusu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">manigal kulunguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manigal kulunguthe"/>
</div>
<div class="lyrico-lyrics-wrapper">manasu kedukuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasu kedukuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">kolusu kolusu kolusu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kolusu kolusu kolusu"/>
</div>
<div class="lyrico-lyrics-wrapper">kathavu thirakuthe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathavu thirakuthe "/>
</div>
<div class="lyrico-lyrics-wrapper">kanavu theriyuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavu theriyuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">puthusu puthusu puthusu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthusu puthusu puthusu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">maduramil kathalar yar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maduramil kathalar yar"/>
</div>
<div class="lyrico-lyrics-wrapper">yaro ithu varai iruntharo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaro ithu varai iruntharo"/>
</div>
<div class="lyrico-lyrics-wrapper">adi thadiyil kathalai marantharo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi thadiyil kathalai marantharo"/>
</div>
<div class="lyrico-lyrics-wrapper">kedupidiyil kidantharo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kedupidiyil kidantharo"/>
</div>
<div class="lyrico-lyrics-wrapper">pottu matuthadi kathale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pottu matuthadi kathale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">manigal kulunguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manigal kulunguthe"/>
</div>
<div class="lyrico-lyrics-wrapper">manasu kedukuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasu kedukuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">kolusu kolusu kolusu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kolusu kolusu kolusu"/>
</div>
<div class="lyrico-lyrics-wrapper">kathavu thirakuthe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathavu thirakuthe "/>
</div>
<div class="lyrico-lyrics-wrapper">kanavu theriyuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavu theriyuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">puthusu puthusu puthusu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthusu puthusu puthusu"/>
</div>
</pre>
