---
title: "isaiyil thodanguthamma song lyrics"
album: "Hey Ram"
artist: "Ilaiyaraaja"
lyricist: "Ilaiyaraaja"
director: "Kamal Haasan"
path: "/albums/hey-ram-song-lyrics"
song: "Isaiyil Thodanguthamma"
image: ../../images/albumart/hey-ram.jpg
date: 2000-02-18
lang: tamil
youtubeLink: "https://www.youtube.com/embed/qYdV01j8Z5o"
type: "melody"
singers:
  - Ajoy Chakrabarty
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Isaiyil Thodanguthamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Isaiyil Thodanguthamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Viraga Naadagama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viraga Naadagama"/>
</div>
<div class="lyrico-lyrics-wrapper">Vasantham Kandathamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vasantham Kandathamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaadum Vaalibame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadum Vaalibame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vasantha Kolangalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vasantha Kolangalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanin Devathaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanin Devathaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kandu Rasikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandu Rasikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthu Koodivittaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthu Koodivittaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingu Namakku Ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingu Namakku Ho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Isaiyil Thodanguthamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Isaiyil Thodanguthamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Viraga Naadagame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viraga Naadagame"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Thin Thin Thin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Thin Thin Thin"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vasantham Kandathamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vasantham Kandathamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaadum Vaalibame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadum Vaalibame"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Thin Thin Thin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Thin Thin Thin"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jai Ram Chandru Ki Jai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jai Ram Chandru Ki Jai"/>
</div>
<div class="lyrico-lyrics-wrapper">Jai Ram Chandru Ki Jai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jai Ram Chandru Ki Jai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theynthu Valarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theynthu Valarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Then Nilaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Then Nilaave"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannil Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannil Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Theynthidaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theynthidaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thee Kuzhambaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thee Kuzhambaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Olira Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Olira Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanaththil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanaththil"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanaththil Minnidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanaththil Minnidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vairaththin Thaaragai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vairaththin Thaaragai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoranangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoranangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhoomikku Konduva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhoomikku Konduva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Isaiyil Thodanguthamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Isaiyil Thodanguthamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Viraga Naadagame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viraga Naadagame"/>
</div>
<div class="lyrico-lyrics-wrapper">Vasantham Kandathamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vasantham Kandathamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaadum Vaalibame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadum Vaalibame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naalil Paadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalil Paadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Irulil Pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irulil Pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Iyarkkaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iyarkkaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhvil Paathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvil Paathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanmai Theemai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanmai Theemai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedalil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyirgale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirgale"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirgale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirgale"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirgale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirgale"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagile Inbaththai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagile Inbaththai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedi Thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedi Thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Grahathukku Vanthathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Grahathukku Vanthathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Isaiyil Thodanguthamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Isaiyil Thodanguthamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Viraga Naadagame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viraga Naadagame"/>
</div>
<div class="lyrico-lyrics-wrapper">Vasantham Kandathamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vasantham Kandathamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaadum Vaalibame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadum Vaalibame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vasantha Kolangalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vasantha Kolangalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanin Devathaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanin Devathaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kandu Rasikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandu Rasikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthu Koodivittaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthu Koodivittaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingu Namakku Ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingu Namakku Ho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Isaiyil Thodanguthamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Isaiyil Thodanguthamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Viraga Naadagame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viraga Naadagame"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Thin Thin Thin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Thin Thin Thin"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vasantham Kandathamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vasantham Kandathamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaadum Vaalibame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadum Vaalibame"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Thin Thin Thin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Thin Thin Thin"/>
</div>
</pre>
