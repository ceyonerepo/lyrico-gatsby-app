---
title: "get well soon song lyrics"
album: "Raja Vaaru Rani Gaaru"
artist: "Jay Krish"
lyricist: "Sanapati Bharadwaj Patrudu"
director: "Ravi Kiran Kola"
path: "/albums/raja-vaaru-rani-gaaru-lyrics"
song: "Get Well Soon"
image: ../../images/albumart/raja-vaaru-rani-gaaru.jpg
date: 2019-11-29
lang: telugu
youtubeLink: "https://www.youtube.com/embed/9x0ncJV5Nbc"
type: "happy"
singers:
  - Hariharasudhan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ok Evaruuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ok Evaruuu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadagani Mutyamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadagani Mutyamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Bongulo Daagina Raagamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bongulo Daagina Raagamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Tinnagaa Tirigina Gaelamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tinnagaa Tirigina Gaelamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Villu Laa Bendaipoyina Baanamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Villu Laa Bendaipoyina Baanamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vadduraa Velloddu Dorakani Daariki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadduraa Velloddu Dorakani Daariki"/>
</div>
<div class="lyrico-lyrics-wrapper">Buddhigaa Ookottu Oollo Figarukee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Buddhigaa Ookottu Oollo Figarukee"/>
</div>
<div class="lyrico-lyrics-wrapper">Elakalae Korikina Kaavyamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Elakalae Korikina Kaavyamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Theragaa Dorikina Costly Sitramaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theragaa Dorikina Costly Sitramaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhyalone Madhya Yelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhyalone Madhya Yelu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mari Mari Soopae Valapadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mari Mari Soopae Valapadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Shaanaa Telividi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shaanaa Telividi "/>
</div>
<div class="lyrico-lyrics-wrapper">Yeeeeeeeeeeee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeeeeeeeeeeee"/>
</div>
<div class="lyrico-lyrics-wrapper">Kiri Kiri Eragani Ninu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kiri Kiri Eragani Ninu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooooooooooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooooooooooooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Aataa Dadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aataa Dadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Saaaaaaaaaaalika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saaaaaaaaaaalika"/>
</div>
<div class="lyrico-lyrics-wrapper">Kita Kita Seshtalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kita Kita Seshtalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeeeeeeeeeeem
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeeeeeeeeeeem"/>
</div>
<div class="lyrico-lyrics-wrapper">Pani Ledaa Sinnavaadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pani Ledaa Sinnavaadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sympathy Korani Gaayamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sympathy Korani Gaayamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Entakee Aagani Solo Gaeyamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entakee Aagani Solo Gaeyamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">I Really Wanna Know
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Really Wanna Know"/>
</div>
<div class="lyrico-lyrics-wrapper">What Your Intentions Are
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="What Your Intentions Are"/>
</div>
<div class="lyrico-lyrics-wrapper">Your Playing With My
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Your Playing With My"/>
</div>
<div class="lyrico-lyrics-wrapper">Friend Like He's A
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friend Like He's A"/>
</div>
<div class="lyrico-lyrics-wrapper">Damn Guitar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Damn Guitar"/>
</div>
<div class="lyrico-lyrics-wrapper">And The Sad Part
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="And The Sad Part"/>
</div>
<div class="lyrico-lyrics-wrapper">Is He Don't Even Notice
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Is He Don't Even Notice"/>
</div>
<div class="lyrico-lyrics-wrapper">But Usually By Now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="But Usually By Now"/>
</div>
<div class="lyrico-lyrics-wrapper">He Would've Gone And
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="He Would've Gone And"/>
</div>
<div class="lyrico-lyrics-wrapper">Blown It
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Blown It"/>
</div>
<div class="lyrico-lyrics-wrapper">Its Just Likqe Him
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Its Just Likqe Him"/>
</div>
<div class="lyrico-lyrics-wrapper">Him To All 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Him To All "/>
</div>
<div class="lyrico-lyrics-wrapper">In Love Way Too Soon
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="In Love Way Too Soon"/>
</div>
<div class="lyrico-lyrics-wrapper">And Your Like
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="And Your Like"/>
</div>
<div class="lyrico-lyrics-wrapper">A Wolf You Got Him
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="A Wolf You Got Him"/>
</div>
<div class="lyrico-lyrics-wrapper">Howling At The Moon
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Howling At The Moon"/>
</div>
<div class="lyrico-lyrics-wrapper">Or More Like A Witch
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Or More Like A Witch"/>
</div>
<div class="lyrico-lyrics-wrapper">Trynahitch Up On A
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Trynahitch Up On A"/>
</div>
<div class="lyrico-lyrics-wrapper">Broom My Friends
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Broom My Friends"/>
</div>
<div class="lyrico-lyrics-wrapper">Sick Call A Doctor
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sick Call A Doctor"/>
</div>
<div class="lyrico-lyrics-wrapper">Help Him Get Well Soon
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Help Him Get Well Soon"/>
</div>
<div class="lyrico-lyrics-wrapper">And I Hate To See Like This 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="And I Hate To See Like This "/>
</div>
<div class="lyrico-lyrics-wrapper">I Hate To Write This
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Hate To Write This"/>
</div>
<div class="lyrico-lyrics-wrapper">But Clouds They
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="But Clouds They"/>
</div>
<div class="lyrico-lyrics-wrapper">Don't Thunder
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Don't Thunder"/>
</div>
<div class="lyrico-lyrics-wrapper">Without Then Lightning
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Without Then Lightning"/>
</div>
<div class="lyrico-lyrics-wrapper">Left-u Side-u Nunna Labbu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Left-u Side-u Nunna Labbu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dabbu Gunde Gaari 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dabbu Gunde Gaari "/>
</div>
<div class="lyrico-lyrics-wrapper">Love-u Story-ee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love-u Story-ee"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanchikelle Bus-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanchikelle Bus-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Lona Koosundae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lona Koosundae"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasae Low Gaa Undae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasae Low Gaa Undae"/>
</div>
<div class="lyrico-lyrics-wrapper">Digulae Lo Lo Undae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Digulae Lo Lo Undae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gaalilo Ninchundi Maeda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaalilo Ninchundi Maeda"/>
</div>
<div class="lyrico-lyrics-wrapper">Dunna Kae Puttindi Dooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dunna Kae Puttindi Dooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Rajuu Neekuu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rajuu Neekuu "/>
</div>
<div class="lyrico-lyrics-wrapper">Asalu Pagodu Pai Vaadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asalu Pagodu Pai Vaadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pai Nunde Planets-u Taedaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pai Nunde Planets-u Taedaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayyo Nee Darling-u Aedaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyo Nee Darling-u Aedaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Raadaa Luck-u Arerae Nee Dikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raadaa Luck-u Arerae Nee Dikku"/>
</div>
<div class="lyrico-lyrics-wrapper">Konchem Koodaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konchem Koodaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilaa Vilaa Vilaa Vilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilaa Vilaa Vilaa Vilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Wings Ae Laeni Tooneegalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wings Ae Laeni Tooneegalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilaa Ilaa Ennaallilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilaa Ilaa Ennaallilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Untaavo Taelaedelaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Untaavo Taelaedelaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Galaa Galaa Elaagolaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Galaa Galaa Elaagolaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Smile-iyyaraa Santodilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Smile-iyyaraa Santodilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">B.C Naati Pathos-u Hero Laa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="B.C Naati Pathos-u Hero Laa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ati Nae Seyyadamika 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ati Nae Seyyadamika "/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvu Aapayyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvu Aapayyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Enduku Godavayyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enduku Godavayyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Love Tagaleyyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Love Tagaleyyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Get Well Soonayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Get Well Soonayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Oohalo Oopiri Aadadae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oohalo Oopiri Aadadae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uramadae Megham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uramadae Megham"/>
</div>
<div class="lyrico-lyrics-wrapper">Merupey Raanidey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Merupey Raanidey"/>
</div>
</pre>
