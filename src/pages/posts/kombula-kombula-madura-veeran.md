---
title: "kombula kombula song lyrics"
album: "Madura Veeran"
artist: "Santhosh Dhayanidhi"
lyricist: "Yugabharathi"
director: "P.G. Muthiah"
path: "/albums/madura-veeran-lyrics"
song: "Kombula Kombula"
image: ../../images/albumart/madura-veeran.jpg
date: 2018-02-02
lang: tamil
youtubeLink: "https://www.youtube.com/embed/v5qtn2GDK14"
type: "happy"
singers:
  - Mahalingam
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kombula Kombula Salliya Kattida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kombula Kombula Salliya Kattida"/>
</div>
<div class="lyrico-lyrics-wrapper">Sondhamum Bandhamum Suthi Irundhida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sondhamum Bandhamum Suthi Irundhida"/>
</div>
<div class="lyrico-lyrics-wrapper">Annanum Thambiyum Thembula Ninnuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annanum Thambiyum Thembula Ninnuda"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandhanam Kungumam Nenjila Ottida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhanam Kungumam Nenjila Ottida"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kombula Kombula Salliya Kattida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kombula Kombula Salliya Kattida"/>
</div>
<div class="lyrico-lyrics-wrapper">Sondhamum Bandhamum Suthi Irundhida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sondhamum Bandhamum Suthi Irundhida"/>
</div>
<div class="lyrico-lyrics-wrapper">Annanum Thambiyum Thembula Ninnuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annanum Thambiyum Thembula Ninnuda"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandhanam Kungumam Nenjila Ottida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhanam Kungumam Nenjila Ottida"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaadivaasal Therandhudichu Paaru Paaru Paaru Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadivaasal Therandhudichu Paaru Paaru Paaru Paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Veerathaala Paeredutha Ooru Ooru Ooru Ooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veerathaala Paeredutha Ooru Ooru Ooru Ooru"/>
</div>
<div class="lyrico-lyrics-wrapper">Maanamulla Saadhi Sanam Yaaru Yaaru Yaaru Yaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maanamulla Saadhi Sanam Yaaru Yaaru Yaaru Yaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Naangadhaanu Paanji Vandhu Kooru Kooru Kooru Kooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naangadhaanu Paanji Vandhu Kooru Kooru Kooru Kooru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kombula Kombula Salliya Kattida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kombula Kombula Salliya Kattida"/>
</div>
<div class="lyrico-lyrics-wrapper">Sondhamum Bandhamum Suthi Irundhida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sondhamum Bandhamum Suthi Irundhida"/>
</div>
<div class="lyrico-lyrics-wrapper">Annanum Thambiyum Thembula Ninnuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annanum Thambiyum Thembula Ninnuda"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandhanam Kungumam Nenjila Ottida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhanam Kungumam Nenjila Ottida"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mannula Vedha Vedhaika Maataiye Nambi Nipom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannula Vedha Vedhaika Maataiye Nambi Nipom"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheivamaa Adha Thozhudhu Nethiyila Potu Vepom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheivamaa Adha Thozhudhu Nethiyila Potu Vepom"/>
</div>
<div class="lyrico-lyrics-wrapper">Yerota Maadu Irundhaa Yezhaiye Illaiyadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yerota Maadu Irundhaa Yezhaiye Illaiyadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naadaanda Raasanukum Maadudhaan Selvamadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naadaanda Raasanukum Maadudhaan Selvamadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maamadhura Seemayila Marikozhundhu Vaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maamadhura Seemayila Marikozhundhu Vaasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannaadhi Mannar Kulam Maraiyaadha Paasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannaadhi Mannar Kulam Maraiyaadha Paasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagachaa Vaal Edukum Pasichaa Soru Tharum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagachaa Vaal Edukum Pasichaa Soru Tharum"/>
</div>
<div class="lyrico-lyrics-wrapper">Anachaa Aasayile Anbu Seiya Odivarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anachaa Aasayile Anbu Seiya Odivarum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kombula Kombula Salliya Kattida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kombula Kombula Salliya Kattida"/>
</div>
<div class="lyrico-lyrics-wrapper">Sondhamum Bandhamum Suthi Irundhida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sondhamum Bandhamum Suthi Irundhida"/>
</div>
<div class="lyrico-lyrics-wrapper">Annanum Thambiyum Thembula Ninnuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annanum Thambiyum Thembula Ninnuda"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandhanam Kungumam Nenjila Ottida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhanam Kungumam Nenjila Ottida"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pethavala Manasukulla Koyilaa Kattivepom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pethavala Manasukulla Koyilaa Kattivepom"/>
</div>
<div class="lyrico-lyrics-wrapper">Kondava Gunam Arinji Pullakutty Pethedupom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondava Gunam Arinji Pullakutty Pethedupom"/>
</div>
<div class="lyrico-lyrics-wrapper">Yemaathum Pervazhigal Aalaanadhillaiyadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yemaathum Pervazhigal Aalaanadhillaiyadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Poraatam Ennavendru Kandadhenga Vamsamadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraatam Ennavendru Kandadhenga Vamsamadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Needhiyadha Nelanirutha Vandha Karpukarasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Needhiyadha Nelanirutha Vandha Karpukarasi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sodhanaya Kandapinnum Ninnaa Vinna Urasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sodhanaya Kandapinnum Ninnaa Vinna Urasi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenaikum Athanaiyum Palikum Bhoomi Idhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenaikum Athanaiyum Palikum Bhoomi Idhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodhikum Veyililum Uzhaikum Kootam Idhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodhikum Veyililum Uzhaikum Kootam Idhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kombula Kombula Salliya Kattida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kombula Kombula Salliya Kattida"/>
</div>
<div class="lyrico-lyrics-wrapper">Sondhamum Bandhamum Suthi Irundhida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sondhamum Bandhamum Suthi Irundhida"/>
</div>
<div class="lyrico-lyrics-wrapper">Annanum Thambiyum Thembula Ninnuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annanum Thambiyum Thembula Ninnuda"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandhanam Kungumam Nenjila Ottida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhanam Kungumam Nenjila Ottida"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaadivaasal Therandhudichu Paaru Paaru Paaru Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadivaasal Therandhudichu Paaru Paaru Paaru Paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Veerathaala Paeredutha Ooru Ooru Ooru Ooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veerathaala Paeredutha Ooru Ooru Ooru Ooru"/>
</div>
<div class="lyrico-lyrics-wrapper">Maanamulla Saadhi Sanam Yaaru Yaaru Yaaru Yaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maanamulla Saadhi Sanam Yaaru Yaaru Yaaru Yaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Naangadhaanu Paanji Vandhu Kooru Kooru Kooru Kooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naangadhaanu Paanji Vandhu Kooru Kooru Kooru Kooru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kombula Kombula Salliya Kattida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kombula Kombula Salliya Kattida"/>
</div>
<div class="lyrico-lyrics-wrapper">Sondhamum Bandhamum Suthi Irundhida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sondhamum Bandhamum Suthi Irundhida"/>
</div>
<div class="lyrico-lyrics-wrapper">Annanum Thambiyum Thembula Ninnuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annanum Thambiyum Thembula Ninnuda"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandhanam Kungumam Nenjila Ottida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhanam Kungumam Nenjila Ottida"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kombula Kombula Salliya Kattida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kombula Kombula Salliya Kattida"/>
</div>
<div class="lyrico-lyrics-wrapper">Sondhamum Bandhamum Suthi Irundhida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sondhamum Bandhamum Suthi Irundhida"/>
</div>
<div class="lyrico-lyrics-wrapper">Annanum Thambiyum Thembula Ninnuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annanum Thambiyum Thembula Ninnuda"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandhanam Kungumam Nenjila Ottida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhanam Kungumam Nenjila Ottida"/>
</div>
</pre>
