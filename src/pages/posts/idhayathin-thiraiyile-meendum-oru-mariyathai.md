---
title: "idhayathin thiraiyile song lyrics"
album: "Meendum Oru Mariyathai"
artist: "N. R. Raghunanthan - Sharran Surya - Yuvan Shankar Raja"
lyricist: "Madhan Karky"
director: "Bharathiraja"
path: "/albums/meendum-oru-mariyathai-lyrics"
song: "Idhayathin Thiraiyile"
image: ../../images/albumart/meendum-oru-mariyathai.jpg
date: 2020-02-21
lang: tamil
youtubeLink: "https://www.youtube.com/embed/9YYAq9MPJxE"
type: "melody"
singers:
  - Abhay Jodhpurkar
  - Alisha Thomas
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Idhayathin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayathin"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiraiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiraiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Muga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Muga"/>
</div>
<div class="lyrico-lyrics-wrapper">Madiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">En Uyir
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Uyir"/>
</div>
<div class="lyrico-lyrics-wrapper">Kondu Kanavai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondu Kanavai"/>
</div>
<div class="lyrico-lyrics-wrapper">Varainthirunthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varainthirunthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Varaiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varaiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Enai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Enai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhali"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa En
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa En"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhala"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathirunthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathirunthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Imaithidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imaithidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhalgalin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhalgalin"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhirvile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhirvile"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhithidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhithidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Manavizhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manavizhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naduvile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naduvile"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Ezhuppi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Ezhuppi"/>
</div>
<div class="lyrico-lyrics-wrapper">Varugiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varugiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Viral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Viral"/>
</div>
<div class="lyrico-lyrics-wrapper">Alaigalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alaigalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivalain
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivalain"/>
</div>
<div class="lyrico-lyrics-wrapper">Padiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhala"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippadikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippadikku"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuni Nanaikiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuni Nanaikiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanave"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeri Varum Oviyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeri Varum Oviyam"/>
</div>
</pre>
