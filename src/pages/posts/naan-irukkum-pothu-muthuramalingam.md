---
title: "naan irukkum pothu song lyrics"
album: "Muthuramalingam"
artist: "Ilaiyaraaja"
lyricist: "Panchu Arunachalam"
director: "Rajadurai"
path: "/albums/muthuramalingam-lyrics"
song: "Naan Irukkum Pothu"
image: ../../images/albumart/muthuramalingam.jpg
date: 2017-02-24
lang: tamil
youtubeLink: "https://www.youtube.com/embed/5o3CHmQc6HU"
type: "mass"
singers:
  - S P Balasubrahmanyam
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">man saavaye katiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="man saavaye katiye"/>
</div>
<div class="lyrico-lyrics-wrapper">pathai endru endrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathai endru endrum"/>
</div>
<div class="lyrico-lyrics-wrapper">moodathu iyya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moodathu iyya"/>
</div>
<div class="lyrico-lyrics-wrapper">munneri munne nada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munneri munne nada"/>
</div>
<div class="lyrico-lyrics-wrapper">inge vaanam thalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inge vaanam thalai"/>
</div>
<div class="lyrico-lyrics-wrapper">thaalthu vaarathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaalthu vaarathe"/>
</div>
<div class="lyrico-lyrics-wrapper">mannuku sorvenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mannuku sorvenna"/>
</div>
<div class="lyrico-lyrics-wrapper">vendame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vendame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nan irukkum pothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan irukkum pothu"/>
</div>
<div class="lyrico-lyrics-wrapper">unakku enna kavalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unakku enna kavalai"/>
</div>
<div class="lyrico-lyrics-wrapper">thanambikkai vitudathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanambikkai vitudathe"/>
</div>
<div class="lyrico-lyrics-wrapper">yen etharku varutham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yen etharku varutham"/>
</div>
<div class="lyrico-lyrics-wrapper">vitu vidu mayakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vitu vidu mayakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjuruthi vitudathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjuruthi vitudathe"/>
</div>
<div class="lyrico-lyrics-wrapper">munnorgal vaalthathu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munnorgal vaalthathu "/>
</div>
<div class="lyrico-lyrics-wrapper">ellam kettu kondalum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellam kettu kondalum "/>
</div>
<div class="lyrico-lyrics-wrapper">thelivu illaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thelivu illaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">ullorin palakkam ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ullorin palakkam ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">ponaal munera vali illaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponaal munera vali illaiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nan irukkum pothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan irukkum pothu"/>
</div>
<div class="lyrico-lyrics-wrapper">unakku enna kavalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unakku enna kavalai"/>
</div>
<div class="lyrico-lyrics-wrapper">thanambikkai vitudathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanambikkai vitudathe"/>
</div>
<div class="lyrico-lyrics-wrapper">yen etharku varutham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yen etharku varutham"/>
</div>
<div class="lyrico-lyrics-wrapper">vitu vidu mayakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vitu vidu mayakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjuruthi vitudathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjuruthi vitudathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aetram indri 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aetram indri "/>
</div>
<div class="lyrico-lyrics-wrapper">maatram indri 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maatram indri "/>
</div>
<div class="lyrico-lyrics-wrapper">yaarum illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaarum illai"/>
</div>
<div class="lyrico-lyrics-wrapper">kutram indri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kutram indri"/>
</div>
<div class="lyrico-lyrics-wrapper">thunbam indri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thunbam indri"/>
</div>
<div class="lyrico-lyrics-wrapper">veedum illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veedum illai"/>
</div>
<div class="lyrico-lyrics-wrapper">aeri sendraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aeri sendraal"/>
</div>
<div class="lyrico-lyrics-wrapper">aeniyaiye thalli vidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aeniyaiye thalli vidum"/>
</div>
<div class="lyrico-lyrics-wrapper">nandri ketta pergalai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nandri ketta pergalai "/>
</div>
<div class="lyrico-lyrics-wrapper">naam kandathu undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naam kandathu undu"/>
</div>
<div class="lyrico-lyrics-wrapper">yaar enna sonnal enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaar enna sonnal enna"/>
</div>
<div class="lyrico-lyrics-wrapper">nee than raajathi raajan iyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee than raajathi raajan iyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">ellorum kondadidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellorum kondadidum"/>
</div>
<div class="lyrico-lyrics-wrapper">engal veerathi veeran iyya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engal veerathi veeran iyya"/>
</div>
<div class="lyrico-lyrics-wrapper">nermai irukuthu nenjum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nermai irukuthu nenjum"/>
</div>
<div class="lyrico-lyrics-wrapper">nimirnthathu kai kaatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nimirnthathu kai kaatu"/>
</div>
<div class="lyrico-lyrics-wrapper">anubavam thanthathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anubavam thanthathu"/>
</div>
<div class="lyrico-lyrics-wrapper">perumaigal nirainthathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="perumaigal nirainthathu"/>
</div>
<div class="lyrico-lyrics-wrapper">vazhi kaatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vazhi kaatu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nan irukkum pothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan irukkum pothu"/>
</div>
<div class="lyrico-lyrics-wrapper">unakku enna kavalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unakku enna kavalai"/>
</div>
<div class="lyrico-lyrics-wrapper">thanambikkai vitudathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanambikkai vitudathe"/>
</div>
<div class="lyrico-lyrics-wrapper">yen etharku varutham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yen etharku varutham"/>
</div>
<div class="lyrico-lyrics-wrapper">vitu vidu mayakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vitu vidu mayakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjuruthi vitudathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjuruthi vitudathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">netru endrum naalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="netru endrum naalai"/>
</div>
<div class="lyrico-lyrics-wrapper">endrum nitham nitham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="endrum nitham nitham"/>
</div>
<div class="lyrico-lyrics-wrapper">nammai nambi naam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nammai nambi naam"/>
</div>
<div class="lyrico-lyrics-wrapper">ulaithu aetram kandom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulaithu aetram kandom"/>
</div>
<div class="lyrico-lyrics-wrapper">vaalbavarai kandu kandu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalbavarai kandu kandu"/>
</div>
<div class="lyrico-lyrics-wrapper">vayir erivaar vanjagarai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vayir erivaar vanjagarai "/>
</div>
<div class="lyrico-lyrics-wrapper">oorgal engum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oorgal engum"/>
</div>
<div class="lyrico-lyrics-wrapper">naamum kandom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naamum kandom"/>
</div>
<div class="lyrico-lyrics-wrapper">enna enna vanthal enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna enna vanthal enna"/>
</div>
<div class="lyrico-lyrics-wrapper">nee than mannathi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee than mannathi "/>
</div>
<div class="lyrico-lyrics-wrapper">mannan ayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mannan ayya"/>
</div>
<div class="lyrico-lyrics-wrapper">munneru munneru endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munneru munneru endru"/>
</div>
<div class="lyrico-lyrics-wrapper">sonna soorathi sooran ayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sonna soorathi sooran ayya"/>
</div>
<div class="lyrico-lyrics-wrapper">kaalam irukuthu kadamaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaalam irukuthu kadamaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">alaikuthu kai kaatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alaikuthu kai kaatu"/>
</div>
<div class="lyrico-lyrics-wrapper">thaalvil padithathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaalvil padithathu"/>
</div>
<div class="lyrico-lyrics-wrapper">vaalkaiku uthavuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalkaiku uthavuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">vazhi kaatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vazhi kaatu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nan irukkum pothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan irukkum pothu"/>
</div>
<div class="lyrico-lyrics-wrapper">unakku enna kavalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unakku enna kavalai"/>
</div>
<div class="lyrico-lyrics-wrapper">thanambikkai vitudathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanambikkai vitudathe"/>
</div>
<div class="lyrico-lyrics-wrapper">yen etharku varutham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yen etharku varutham"/>
</div>
<div class="lyrico-lyrics-wrapper">vitu vidu mayakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vitu vidu mayakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjuruthi vitudathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjuruthi vitudathe"/>
</div>
</pre>
