---
title: "madhurai madhurai song lyrics"
album: "Aayiram Vilakku"
artist: "Srikanth Deva"
lyricist: "Vairamuthu"
director: "S.P. Hosimin"
path: "/albums/aayiram-vilakku-lyrics"
song: "Madhurai Madhurai"
image: ../../images/albumart/aayiram-vilakku.jpg
date: 2011-09-23
lang: tamil
youtubeLink: "https://www.youtube.com/embed/jdPtAdKzatg"
type: "happy"
singers:
  - Naveen
  - Velmurugan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Odungal odi ullam urugi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odungal odi ullam urugi"/>
</div>
<div class="lyrico-lyrics-wrapper">Isai paadungal paada vanthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Isai paadungal paada vanthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Parum porulae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parum porulae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Odungal odi ullam urugi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odungal odi ullam urugi"/>
</div>
<div class="lyrico-lyrics-wrapper">Isai paadungal paada vanthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Isai paadungal paada vanthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Parum porulaeae.ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parum porulaeae.ae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadungal eduthu nadamiduvaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadungal eduthu nadamiduvaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Iraivaa un tamil amudhai padikkava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iraivaa un tamil amudhai padikkava"/>
</div>
<div class="lyrico-lyrics-wrapper">Un adiyen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un adiyen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Madhura madhura madhura madhura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhura madhura madhura madhura"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhura madhura madhura madhura thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhura madhura madhura madhura thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhura thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhura thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhura madhura madhura madhura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhura madhura madhura madhura"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhura madhura madhura madhura thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhura madhura madhura madhura thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhura thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhura thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhura madhura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhura madhura"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye madhura madhura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye madhura madhura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan nee madhuraiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan nee madhuraiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannu muzhichom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu muzhichom"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaigaiyila thannikkudichom aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaigaiyila thannikkudichom aa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye madhura madhura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye madhura madhura"/>
</div>
<div class="lyrico-lyrics-wrapper">Pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pol"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye madhura madhura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye madhura madhura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meen pol karagaattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meen pol karagaattam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kandu kazhichom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandu kazhichom"/>
</div>
<div class="lyrico-lyrics-wrapper">Oyilaattam aadi mudichom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oyilaattam aadi mudichom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paandiya mannan aanda ooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paandiya mannan aanda ooru"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma sokkanaadhar konda koyil paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma sokkanaadhar konda koyil paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Naagareegam vandhaalumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naagareegam vandhaalumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu nagaram illa verum graamam paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu nagaram illa verum graamam paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhura madhura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhura madhura"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye madhura madhura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye madhura madhura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan nee madhuraiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan nee madhuraiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannu muzhichom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu muzhichom"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaigaiyila thannikkudichom aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaigaiyila thannikkudichom aa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanga malli ye malli ye malli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga malli ye malli ye malli"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaadi kalli yei kalli yei kalli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadi kalli yei kalli yei kalli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanga malli malli malli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga malli malli malli"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaadi kalli kalli en kalli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadi kalli kalli en kalli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Madhuraiyil arasaalum meenatchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhuraiyil arasaalum meenatchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhuraiyil arasaalum meenatchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhuraiyil arasaalum meenatchi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye mela maasi veedhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye mela maasi veedhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Angu manakkum manakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Angu manakkum manakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Malliya poovae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malliya poovae"/>
</div>
<div class="lyrico-lyrics-wrapper">Keezha maasi veedhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Keezha maasi veedhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Angu kodhikkum kodhikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Angu kodhikkum kodhikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodalu kolambhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodalu kolambhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhakthan yeri vaarom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhakthan yeri vaarom"/>
</div>
<div class="lyrico-lyrics-wrapper">Angu vidiya vidiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Angu vidiya vidiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Koothum kudiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koothum kudiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhura toongaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhura toongaadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey vettukkooraiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey vettukkooraiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaakkaa kaththunaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaakkaa kaththunaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Virundhu varudhunnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Virundhu varudhunnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kozhi adippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kozhi adippom"/>
</div>
<div class="lyrico-lyrics-wrapper">Pazhaiya pagaiyellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pazhaiya pagaiyellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Moottakkatti mudichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moottakkatti mudichi"/>
</div>
<div class="lyrico-lyrics-wrapper">Bandha paasathil gummiyadippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bandha paasathil gummiyadippom"/>
</div>
<div class="lyrico-lyrics-wrapper">Engooru vaazhkkaiyila tharammirukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engooru vaazhkkaiyila tharammirukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Kooda thamizhirukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooda thamizhirukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Aiyaaththa pannaikkoru rusi irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyaaththa pannaikkoru rusi irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudichaa pasi irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudichaa pasi irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Engooru kaadhalukku madhippirukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engooru kaadhalukku madhippirukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannin manammirukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannin manammirukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Hei aruvaala thotta oru thirammirukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei aruvaala thotta oru thirammirukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal adaivadharku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal adaivadharku"/>
</div>
<div class="lyrico-lyrics-wrapper">Heyeyyy eyyyeyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyeyyy eyyyeyy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhura madhura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhura madhura"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye madhura madhura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye madhura madhura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yei naan ye nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei naan ye nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhuraiyila kannu muzhichom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhuraiyila kannu muzhichom"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaigaiyila thannikkudichom aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaigaiyila thannikkudichom aa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Madhura madhura madhura madhura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhura madhura madhura madhura"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhura madhura madhura madhura thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhura madhura madhura madhura thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhura thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhura thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhura madhura madhura madhura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhura madhura madhura madhura"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhura madhura madhura madhura thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhura madhura madhura madhura thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhura thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhura thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhagar malaikku melae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagar malaikku melae"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagu ponnuga koottam irukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagu ponnuga koottam irukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaanai malaikku keezha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaanai malaikku keezha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaavani ponnunga neechaladikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaavani ponnunga neechaladikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinnakkallukku pakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinnakkallukku pakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sinunga kulunga aattam nadakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sinunga kulunga aattam nadakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhura maaraadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhura maaraadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hei hei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei hei"/>
</div>
<div class="lyrico-lyrics-wrapper">Valarum kaadhalar vasadhikku odhunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valarum kaadhalar vasadhikku odhunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaigai aatrukkul edammirukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaigai aatrukkul edammirukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Sagala saadhiyum onnum munnaa irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sagala saadhiyum onnum munnaa irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhura mannukku manasirukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhura mannukku manasirukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Panjaalae thachchaalum palan irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panjaalae thachchaalum palan irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Makkal balammirukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Makkal balammirukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Panjumittaai viththaalum thozhil irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panjumittaai viththaalum thozhil irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Ozhaikku thunivirukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ozhaikku thunivirukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Kondaattam eppodhum nerainjirukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondaattam eppodhum nerainjirukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorekkulirndhirukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorekkulirndhirukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthaarai vaazhavaikka vazhi irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthaarai vaazhavaikka vazhi irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaasal therandhirukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaasal therandhirukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Heyeyyy eyyyeyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyeyyy eyyyeyy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhura madhura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhura madhura"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye madhura madhura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye madhura madhura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan nee madhuraiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan nee madhuraiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannu muzhichom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu muzhichom"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaigaiyila thannikkudichom aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaigaiyila thannikkudichom aa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye madhura madhura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye madhura madhura"/>
</div>
<div class="lyrico-lyrics-wrapper">Pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pol"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye madhura madhura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye madhura madhura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meen pol karagaattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meen pol karagaattam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kandu kazhichom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandu kazhichom"/>
</div>
<div class="lyrico-lyrics-wrapper">Oyilaattam aadi mudichom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oyilaattam aadi mudichom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Madhura madhura madhura madhura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhura madhura madhura madhura"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhura madhura madhura madhura thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhura madhura madhura madhura thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhura thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhura thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhura madhura madhura madhura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhura madhura madhura madhura"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhura madhura madhura madhura thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhura madhura madhura madhura thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhura thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhura thaan"/>
</div>
</pre>
