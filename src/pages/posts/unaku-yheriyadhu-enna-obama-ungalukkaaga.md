---
title: "unaku theriyadhu enna song lyrics"
album: "Obama Ungalukkaaga"
artist: "Srikanth Deva"
lyricist: "Kavi Perarasu Vaira Muthu"
director: "Nani Bala"
path: "/albums/obama-ungalukkaaga-lyrics"
song: "Unaku Theriyadhu Enna"
image: ../../images/albumart/obama-ungalukkaaga.jpg
date: 2021-12-31
lang: tamil
youtubeLink: "https://www.youtube.com/embed/93wrz1T_XL0"
type: "love"
singers:
  - Keshavan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">unaku theriyathu enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unaku theriyathu enna"/>
</div>
<div class="lyrico-lyrics-wrapper">enaku theriyum unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enaku theriyum unna"/>
</div>
<div class="lyrico-lyrics-wrapper">penne unnai pin thodarven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="penne unnai pin thodarven"/>
</div>
<div class="lyrico-lyrics-wrapper">penne unnai pin thodarven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="penne unnai pin thodarven"/>
</div>
<div class="lyrico-lyrics-wrapper">pasuvai thodarum kandrai pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pasuvai thodarum kandrai pola"/>
</div>
<div class="lyrico-lyrics-wrapper">netrai thodarum indrai pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="netrai thodarum indrai pola"/>
</div>
<div class="lyrico-lyrics-wrapper">vaalvai thodarum vidhiyai pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalvai thodarum vidhiyai pola"/>
</div>
<div class="lyrico-lyrics-wrapper">penne unnai pin thodarven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="penne unnai pin thodarven"/>
</div>
<div class="lyrico-lyrics-wrapper">penne unnai pin thodarven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="penne unnai pin thodarven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vinil unnai paarkaiyale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vinil unnai paarkaiyale"/>
</div>
<div class="lyrico-lyrics-wrapper">unthan kannai alavedupen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unthan kannai alavedupen"/>
</div>
<div class="lyrico-lyrics-wrapper">nilavai paarkaiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nilavai paarkaiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">un netriyai alaveduthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un netriyai alaveduthen"/>
</div>
<div class="lyrico-lyrics-wrapper">kanigal paarkaiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanigal paarkaiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">kanigal paarkaiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanigal paarkaiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">un kannam alaveduthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kannam alaveduthen"/>
</div>
<div class="lyrico-lyrics-wrapper">kavalam paarkaiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kavalam paarkaiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">un thadangal alaveduthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un thadangal alaveduthen"/>
</div>
<div class="lyrico-lyrics-wrapper">unnai paarkaiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai paarkaiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">un vannam alaveduthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un vannam alaveduthen"/>
</div>
<div class="lyrico-lyrics-wrapper">nee ennai kadakaiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee ennai kadakaiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">naan ellam maranthu viten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan ellam maranthu viten"/>
</div>
<div class="lyrico-lyrics-wrapper">unnai eppadi alavedupen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai eppadi alavedupen"/>
</div>
<div class="lyrico-lyrics-wrapper">adi vinnai epadi nagal edupen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi vinnai epadi nagal edupen"/>
</div>
<div class="lyrico-lyrics-wrapper">unnai eppadi alavedupen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai eppadi alavedupen"/>
</div>
<div class="lyrico-lyrics-wrapper">adi vinnai epadi nagal edupen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi vinnai epadi nagal edupen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">yaro thodaruvathai ne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaro thodaruvathai ne"/>
</div>
<div class="lyrico-lyrics-wrapper">angam patharugirai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="angam patharugirai"/>
</div>
<div class="lyrico-lyrics-wrapper">ennai kaana than nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennai kaana than nee"/>
</div>
<div class="lyrico-lyrics-wrapper">engo thedugirai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engo thedugirai"/>
</div>
<div class="lyrico-lyrics-wrapper">uyire kathaliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyire kathaliye"/>
</div>
<div class="lyrico-lyrics-wrapper">uyire kathaliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyire kathaliye"/>
</div>
<div class="lyrico-lyrics-wrapper">enai ulle thedi paar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enai ulle thedi paar"/>
</div>
<div class="lyrico-lyrics-wrapper">un unmai uruvathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un unmai uruvathai"/>
</div>
<div class="lyrico-lyrics-wrapper">un uyiril thedi paar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un uyiril thedi paar"/>
</div>
<div class="lyrico-lyrics-wrapper">enthan peyar enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enthan peyar enna"/>
</div>
<div class="lyrico-lyrics-wrapper">un irandu kolusai kel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un irandu kolusai kel"/>
</div>
<div class="lyrico-lyrics-wrapper">en kathal alavu enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kathal alavu enna"/>
</div>
<div class="lyrico-lyrics-wrapper">un kaladi nilalai kel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kaladi nilalai kel"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">unnai eppadi alavedupen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai eppadi alavedupen"/>
</div>
<div class="lyrico-lyrics-wrapper">adi vinnai epadi nagal edupen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi vinnai epadi nagal edupen"/>
</div>
<div class="lyrico-lyrics-wrapper">unnai eppadi alavedupen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai eppadi alavedupen"/>
</div>
<div class="lyrico-lyrics-wrapper">adi vinnai epadi nagal edupen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi vinnai epadi nagal edupen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">unaku theriyathu enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unaku theriyathu enna"/>
</div>
<div class="lyrico-lyrics-wrapper">enaku theriyum unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enaku theriyum unna"/>
</div>
<div class="lyrico-lyrics-wrapper">penne unnai pin thodarven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="penne unnai pin thodarven"/>
</div>
<div class="lyrico-lyrics-wrapper">penne unnai pin thodarven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="penne unnai pin thodarven"/>
</div>
<div class="lyrico-lyrics-wrapper">penne unnai pin thodarven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="penne unnai pin thodarven"/>
</div>
</pre>
