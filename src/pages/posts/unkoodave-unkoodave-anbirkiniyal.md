---
title: "unkoodave unkoodave song lyrics"
album: "Anbirkiniyal"
artist: "Javed Riaz"
lyricist: "Lalithanand"
director: "Gokul"
path: "/albums/anbirkiniyal-lyrics"
song: "Unkoodave Unkoodave"
image: ../../images/albumart/anbirkiniyal.jpg
date: 2021-03-05
lang: tamil
youtubeLink: "https://www.youtube.com/embed/UhlaAVA8FMo"
type: "Sad"
singers:
  - Prarthana Indrajith
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">un koodave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un koodave"/>
</div>
<div class="lyrico-lyrics-wrapper">un koodave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un koodave"/>
</div>
<div class="lyrico-lyrics-wrapper">thannaal varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thannaal varum"/>
</div>
<div class="lyrico-lyrics-wrapper">en kaalgale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kaalgale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">en paalamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en paalamum"/>
</div>
<div class="lyrico-lyrics-wrapper">en paasamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en paasamum"/>
</div>
<div class="lyrico-lyrics-wrapper">kanmodinen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanmodinen"/>
</div>
<div class="lyrico-lyrics-wrapper">un tholile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un tholile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kan moodalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kan moodalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">man moodalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="man moodalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">appothum nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="appothum nee"/>
</div>
<div class="lyrico-lyrics-wrapper">en thedale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en thedale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kai korthathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kai korthathum"/>
</div>
<div class="lyrico-lyrics-wrapper">kan paarthathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kan paarthathum"/>
</div>
<div class="lyrico-lyrics-wrapper">muthadume 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muthadume "/>
</div>
<div class="lyrico-lyrics-wrapper">en moochile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en moochile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">un koodave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un koodave"/>
</div>
<div class="lyrico-lyrics-wrapper">un koodave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un koodave"/>
</div>
<div class="lyrico-lyrics-wrapper">un koodave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un koodave"/>
</div>
<div class="lyrico-lyrics-wrapper">un koodave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un koodave"/>
</div>
</pre>
