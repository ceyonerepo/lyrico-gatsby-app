---
title: "enjoy enjaami"
album: "Majja"
artist: "Dhee ft. Arivu"
lyricist: "Arivu"
director: "Amith Krishnan"
path: "/albums/enjoy-enjaami-song-lyrics"
song: "Enjoy Enjaami"
image: ../../images/albumart/enjoy-enjaami.jpg
date: 2021-03-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/eYq7WapuDLU"
type: "album"
singers:
  - Dhee
  - Arivu
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Cuckoo cuckoo...
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cuckoo cuckoo..."/>
</div>
<div class="lyrico-lyrics-wrapper">Thaatha thaatha kala vetti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaatha thaatha kala vetti"/>
</div>
<div class="lyrico-lyrics-wrapper">Cuckoo cuckoo...
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cuckoo cuckoo..."/>
</div>
<div class="lyrico-lyrics-wrapper">Pondhula yaaru meen koththi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pondhula yaaru meen koththi"/>
</div>
<div class="lyrico-lyrics-wrapper">Cuckoo cuckoo...
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cuckoo cuckoo..."/>
</div>
<div class="lyrico-lyrics-wrapper">Thanniyil odum thavalaikki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanniyil odum thavalaikki"/>
</div>
<div class="lyrico-lyrics-wrapper">Cuckoo cuckoo...
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cuckoo cuckoo..."/>
</div>
<div class="lyrico-lyrics-wrapper">Kambali poochi thangachi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kambali poochi thangachi "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Allimalar kodi angadhame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allimalar kodi angadhame"/>
</div>
<div class="lyrico-lyrics-wrapper">Ottara ottara sandhaname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ottara ottara sandhaname"/>
</div>
<div class="lyrico-lyrics-wrapper">Mullai malar kodi muththaarame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mullai malar kodi muththaarame"/>
</div>
<div class="lyrico-lyrics-wrapper">Engooru engooru kuththaalame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engooru engooru kuththaalame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Surukku paiyamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Surukku paiyamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Veththala mattaiyamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veththala mattaiyamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Somandha kaiyamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Somandha kaiyamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Maththalam kottuyamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maththalam kottuyamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaaiyamma thaaiyamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaiyamma thaaiyamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna panna maayamma 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna panna maayamma "/>
</div>
<div class="lyrico-lyrics-wrapper">Valliamma peraandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valliamma peraandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sangadhiya kellendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sangadhiya kellendi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannaadiya kaanaamdi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaadiya kaanaamdi "/>
</div>
<div class="lyrico-lyrics-wrapper">Indhaarraa peraandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indhaarraa peraandi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Annakkili annakkili 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annakkili annakkili "/>
</div>
<div class="lyrico-lyrics-wrapper">Adi aalamarakkela vannakkili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi aalamarakkela vannakkili"/>
</div>
<div class="lyrico-lyrics-wrapper">Nallapadi vaazhacholli 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nallapadi vaazhacholli "/>
</div>
<div class="lyrico-lyrics-wrapper">Indha manna koduthaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha manna koduthaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Poorvakudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poorvakudi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kammaankara kaaniyellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kammaankara kaaniyellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadith thirinjaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadith thirinjaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadhikkudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadhikkudi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naayi nari 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naayi nari "/>
</div>
<div class="lyrico-lyrics-wrapper">Poonaikundhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poonaikundhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha erikkolam kooda sondhammadi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha erikkolam kooda sondhammadi "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enjoy, enjaami 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enjoy, enjaami "/>
</div>
<div class="lyrico-lyrics-wrapper">vaango vaango onnaagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaango vaango onnaagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Amma yi ambaari 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma yi ambaari "/>
</div>
<div class="lyrico-lyrics-wrapper">Indha indha mummaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha indha mummaari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Cuckoo cuckoo...
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cuckoo cuckoo..."/>
</div>
<div class="lyrico-lyrics-wrapper">Muttaiya podum kozhikku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muttaiya podum kozhikku "/>
</div>
<div class="lyrico-lyrics-wrapper">Cuckoo cuckoo...
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cuckoo cuckoo..."/>
</div>
<div class="lyrico-lyrics-wrapper">Oppanai yaaru maiyilukku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oppanai yaaru maiyilukku "/>
</div>
<div class="lyrico-lyrics-wrapper">Cuckoo cuckoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cuckoo cuckoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Pachchaiya poosum paasikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pachchaiya poosum paasikku"/>
</div>
<div class="lyrico-lyrics-wrapper">Cuckoo cuckoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cuckoo cuckoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuchchiya adukkuna kootukku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuchchiya adukkuna kootukku "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paadu patta makka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadu patta makka "/>
</div>
<div class="lyrico-lyrics-wrapper">Varappu mettukkaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varappu mettukkaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Vervathanni sokkaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vervathanni sokkaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Minukkum naattukkaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minukkum naattukkaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Aakaatti karuppatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aakaatti karuppatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Oodhaangolu mannuchatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oodhaangolu mannuchatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Aathoram koodukatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aathoram koodukatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Arambichcha naagareegam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arambichcha naagareegam"/>
</div>
<div class="lyrico-lyrics-wrapper">Jhan jhana jhanakku jhana makkale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jhan jhana jhanakku jhana makkale"/>
</div>
<div class="lyrico-lyrics-wrapper">Uppuku chappu kottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uppuku chappu kottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Muttaikulla saththukottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muttaikulla saththukottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Attaikku raththangkottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Attaikku raththangkottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kittipullu vettu vettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kittipullu vettu vettu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan anju maram valarthen...
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan anju maram valarthen..."/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagana thottam vachchen...
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagana thottam vachchen..."/>
</div>
<div class="lyrico-lyrics-wrapper">Thottam sezhithaalum en thonda 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottam sezhithaalum en thonda "/>
</div>
<div class="lyrico-lyrics-wrapper">nanaiyalaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanaiyalaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En kadale karaye...
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kadale karaye..."/>
</div>
<div class="lyrico-lyrics-wrapper">Vaname saname...
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaname saname..."/>
</div>
<div class="lyrico-lyrics-wrapper">Nelame kolame...
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nelame kolame..."/>
</div>
<div class="lyrico-lyrics-wrapper">Edame thadame...
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edame thadame..."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enjoy, enjaami 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enjoy, enjaami "/>
</div>
<div class="lyrico-lyrics-wrapper">vaango vaango onnaagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaango vaango onnaagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Amma yi ambaari 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma yi ambaari "/>
</div>
<div class="lyrico-lyrics-wrapper">Indha indha mummaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha indha mummaari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paattan poottan kaaththa boomi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paattan poottan kaaththa boomi"/>
</div>
<div class="lyrico-lyrics-wrapper">aatam pottu kaattum saami 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aatam pottu kaattum saami "/>
</div>
<div class="lyrico-lyrics-wrapper">Raatinandha suththi vandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raatinandha suththi vandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Seva koovuchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seva koovuchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu pottu vachcha echamdhaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu pottu vachcha echamdhaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaada maarichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaada maarichu"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma naada maarichu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma naada maarichu "/>
</div>
<div class="lyrico-lyrics-wrapper">Indha veeda maarichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha veeda maarichu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna kora enna kora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna kora enna kora"/>
</div>
<div class="lyrico-lyrics-wrapper">En seeni karumbukku enna kora 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En seeni karumbukku enna kora "/>
</div>
<div class="lyrico-lyrics-wrapper">Enna kora enna kora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna kora enna kora"/>
</div>
<div class="lyrico-lyrics-wrapper">En chella peraandikku enna kora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En chella peraandikku enna kora"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pandhalulla paavarka...
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pandhalulla paavarka..."/>
</div>
<div class="lyrico-lyrics-wrapper">Pandhalulla paavarka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pandhalulla paavarka"/>
</div>
<div class="lyrico-lyrics-wrapper">Vedhakallu vitturukku...
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedhakallu vitturukku..."/>
</div>
<div class="lyrico-lyrics-wrapper">Vedhakallu vitturukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedhakallu vitturukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Appan aathaa vittadhungo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appan aathaa vittadhungo"/>
</div>
<div class="lyrico-lyrics-wrapper">Appan aathaa vittandhungo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appan aathaa vittandhungo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ah.....
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ah....."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enjoy, enjaami 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enjoy, enjaami "/>
</div>
<div class="lyrico-lyrics-wrapper">vaango vaango onnaagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaango vaango onnaagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Amma yi ambaari 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma yi ambaari "/>
</div>
<div class="lyrico-lyrics-wrapper">Indha indha mummaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha indha mummaari"/>
</div>
</pre>
