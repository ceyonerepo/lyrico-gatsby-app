---
title: "aara thedum song lyrics"
album: "Vellai Yaanai"
artist: "Santhosh Narayanan"
lyricist: "Rajumurugan"
director: "Subramaniam Siva"
path: "/albums/vellai-yaanai-lyrics"
song: "Aara Thedum"
image: ../../images/albumart/vellai-yaanai.jpg
date: 2021-07-11
lang: tamil
youtubeLink: "https://www.youtube.com/embed/0cp7aOiKrbM"
type: "sad"
singers:
  - Santhosh Narayanan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aalam Pondhu Thavittu Kunju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalam Pondhu Thavittu Kunju"/>
</div>
<div class="lyrico-lyrics-wrapper">Aara Thaedum Aara Thaedum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aara Thaedum Aara Thaedum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aara Thaedum Aara Thaedum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aara Thaedum Aara Thaedum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mottapana Munisaami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mottapana Munisaami"/>
</div>
<div class="lyrico-lyrics-wrapper">Aara Thaedum Aara Thaedum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aara Thaedum Aara Thaedum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aara Thaedum Aara Thaedum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aara Thaedum Aara Thaedum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lorry Yethum Aathu Mannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lorry Yethum Aathu Mannu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aara Thaedum Aara Thaedum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aara Thaedum Aara Thaedum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aara Thaedum Aara Thaedum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aara Thaedum Aara Thaedum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Soru Pootha Yeru Kalappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soru Pootha Yeru Kalappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aara Thaedum Aara Thaedum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aara Thaedum Aara Thaedum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Ohoh Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Ohoh Oh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sanamae En Sanamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sanamae En Sanamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Onna Thaedum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna Thaedum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathu Ninna Saami Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathu Ninna Saami Ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanna Moodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna Moodum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanna Moodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna Moodum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veppa Mara Kaathellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veppa Mara Kaathellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Veppa Mara Kaathellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veppa Mara Kaathellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vedha Potta Kaiya Thaedum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedha Potta Kaiya Thaedum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudi Vandha Kuruvi Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudi Vandha Kuruvi Ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Uzhundha Mannu Puzhu Thaedum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uzhundha Mannu Puzhu Thaedum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poosaram Pootha Paththayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poosaram Pootha Paththayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu Nellu Vaasam Thaedum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu Nellu Vaasam Thaedum"/>
</div>
<div class="lyrico-lyrics-wrapper">Muppoga Boomi Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muppoga Boomi Ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Veedaaga Vela Thaedum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veedaaga Vela Thaedum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aara Thaedum Aara Thaedum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aara Thaedum Aara Thaedum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aara Thaedum Aara Thaedum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aara Thaedum Aara Thaedum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sanamae En Sanamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sanamae En Sanamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Onna Thaedum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna Thaedum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathu Ninna Saami Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathu Ninna Saami Ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanna Moodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna Moodum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanna Moodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna Moodum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aara Thaedum Aara Thaedum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aara Thaedum Aara Thaedum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aara Thaedum Aara Thaedum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aara Thaedum Aara Thaedum"/>
</div>
</pre>
