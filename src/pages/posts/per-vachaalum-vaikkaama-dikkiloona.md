---
title: "per vachaalum vaikkaama lyrics"
album: "Dikkiloona"
artist: "Yuvan shankar raja"
lyricist: "Vaali"
director: "Karthik Yogi"
path: "/albums/dikkiloona-song-lyrics"
song: "Per Vachaalum Vaikkaama"
image: ../../images/albumart/dikkiloona.jpg
date: 2021-09-10
lang: tamil
youtubeLink: "https://www.youtube.com/embed/-BUDo3mow00"
type: "happy"
singers:
  - Malaysia Vasudevan
  - S. Janaki
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Per vachaalum vaikkaama ponalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Per vachaalum vaikkaama ponalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Malli vaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malli vaasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu kuthaala suga vaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu kuthaala suga vaasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada ippodhum eppodhum muppodhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada ippodhum eppodhum muppodhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thottu pesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottu pesum"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha pennoda sagavaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha pennoda sagavaasam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mottu thaan vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mottu thaan vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sottu thaen thandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sottu thaen thandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kitta thaan otta thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kitta thaan otta thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Katta thaan appappappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katta thaan appappappa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Per vachaalum vaikkaama ponalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Per vachaalum vaikkaama ponalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Malli vaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malli vaasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu kuthaala suga vaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu kuthaala suga vaasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada ippodhum eppodhum muppodhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada ippodhum eppodhum muppodhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thottu pesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottu pesum"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha pennoda sagavaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha pennoda sagavaasam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kodai veppathil koyil theppathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodai veppathil koyil theppathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeralaam yeralaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeralaam yeralaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaaman kundrathil kaadhal mandrathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaman kundrathil kaadhal mandrathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Seralaam seralaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seralaam seralaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mandharai chediyoram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandharai chediyoram"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam mallandhu nedu nerum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam mallandhu nedu nerum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandhosham peralamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhosham peralamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhil sandhegam varalama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhil sandhegam varalama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pandhakaal nattu pattuppaai ittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pandhakaal nattu pattuppaai ittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mella thaan alla thaan killa thaan appapappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mella thaan alla thaan killa thaan appapappa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vachaalum vaikkaama ponalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vachaalum vaikkaama ponalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Malli vaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malli vaasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Hei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu kuthaala suga vaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu kuthaala suga vaasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada ippodhum eppodhum muppodhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada ippodhum eppodhum muppodhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thottu pesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottu pesum"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha pennoda sagavaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha pennoda sagavaasam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mottu thaan vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mottu thaan vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sottu thaen thandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sottu thaen thandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kitta thaan otta thaan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kitta thaan otta thaan "/>
</div>
<div class="lyrico-lyrics-wrapper">katta thaan appappappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="katta thaan appappappa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vachaalum vaikkaama ponalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vachaalum vaikkaama ponalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Malli vaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malli vaasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu kuthaala suga vaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu kuthaala suga vaasam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal mannana neeyum kannana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal mannana neeyum kannana"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalum orr alangaramaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalum orr alangaramaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thozhi mella thaan thedhi sollathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozhi mella thaan thedhi sollathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thondrinen avadharamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thondrinen avadharamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalyanam mudikaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalyanam mudikaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma kacheri thodangaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma kacheri thodangaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallaalae anai pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallaalae anai pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey indha kaveri adangaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey indha kaveri adangaadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Appappaa appu thappappaa thappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appappaa appu thappappaa thappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Setuppa setuppa yetti poo appapapa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Setuppa setuppa yetti poo appapapa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Per vachaalum vaikkaama ponalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Per vachaalum vaikkaama ponalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Malli vaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malli vaasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu kuthaala suga vaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu kuthaala suga vaasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada ippodhum eppodhum muppodhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada ippodhum eppodhum muppodhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thottu pesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottu pesum"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha pennoda sagavaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha pennoda sagavaasam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mottu thaan vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mottu thaan vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sottu thaen thandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sottu thaen thandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kitta thaan otta thaan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kitta thaan otta thaan "/>
</div>
<div class="lyrico-lyrics-wrapper">katta thaan appappappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="katta thaan appappappa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Per vachaalum vaikkaama ponalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Per vachaalum vaikkaama ponalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Malli vaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malli vaasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu kuthaala suga vaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu kuthaala suga vaasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada ippodhum eppodhum muppodhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada ippodhum eppodhum muppodhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thottu pesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottu pesum"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha pennoda sagavaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha pennoda sagavaasam"/>
</div>
</pre>
