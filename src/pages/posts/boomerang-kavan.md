---
title: "boomerang song lyrics"
album: "Kavan"
artist: "Hiphop Tamizha"
lyricist: "Kabilan Vairamuthu"
director: "K V Anand"
path: "/albums/kavan-lyrics"
song: "Boomerang"
image: ../../images/albumart/kavan.jpg
date: 2017-03-31
lang: tamil
youtubeLink: "https://www.youtube.com/embed/8N6rdxsAN4M"
type: "happy"
singers:
  - Hiphop Tamizha
  - Nikhita Gandhi
  - Velmurugan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yaaro ada aappu vaikka povathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaro ada aappu vaikka povathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaro oru soappu poda povathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaro oru soappu poda povathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaroun kanavai thiruda povathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaroun kanavai thiruda povathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Munnottam illaatha vaazhkkai ithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munnottam illaatha vaazhkkai ithu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enjoymentum thaniyaa illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enjoymentum thaniyaa illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Panjaayathum thaniyaa illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panjaayathum thaniyaa illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannaadiyaam manasukkulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaadiyaam manasukkulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Mugam paakka vaaravanga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mugam paakka vaaravanga "/>
</div>
<div class="lyrico-lyrics-wrapper">kanakae illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanakae illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Innaalil innaaroda kaadhalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innaalil innaaroda kaadhalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Innaalil innaaroda modhalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innaalil innaaroda modhalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivarukku evaru melae mogamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivarukku evaru melae mogamo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ariyaatha nilamai thaano gyanaamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ariyaatha nilamai thaano gyanaamo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enjoymentum thaniyaa illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enjoymentum thaniyaa illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Panjaayathum thaniyaa illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panjaayathum thaniyaa illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannaadiyaam manasukkulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaadiyaam manasukkulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Mugam paakka vaaravanga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mugam paakka vaaravanga "/>
</div>
<div class="lyrico-lyrics-wrapper">kanakae illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanakae illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thana nanae naa aaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana nanae naa aaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana nanae naa aaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana nanae naa aaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaaro ada aappu vaikka povathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaro ada aappu vaikka povathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaro oru soappu poda povathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaro oru soappu poda povathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaroun kanavai thiruda povathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaroun kanavai thiruda povathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Munnottam illaatha vaazhkkai ithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munnottam illaatha vaazhkkai ithu"/>
</div>
</pre>
