---
title: "yaar intha devathai song lyrics"
album: "Unnai Ninaithu"
artist: "Sirpy"
lyricist: "P. Vijay"
director: "Vikraman"
path: "/albums/unnai-ninaithu-lyrics"
song: "Yaar Intha Devathai"
image: ../../images/albumart/unnai-ninaithu.jpg
date: 2002-05-10
lang: tamil
youtubeLink: "https://www.youtube.com/embed/2xhTrCbPJTg"
type: "love"
singers:
  - Hariharan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yaar intha devathai yaar intha devathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar intha devathai yaar intha devathai"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar intha devathai yaar intha devathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar intha devathai yaar intha devathai"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru kodi pookal uzhagengum undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru kodi pookal uzhagengum undu"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha pen polae azhagana poo ondru ullatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha pen polae azhagana poo ondru ullatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaar intha devathai yaar intha devathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar intha devathai yaar intha devathai"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar intha devathai yaar intha devathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar intha devathai yaar intha devathai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pani kooda unmel padum velaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pani kooda unmel padum velaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kulir thaangidamal dhegam nadungumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulir thaangidamal dhegam nadungumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Malar kooda unnai thodum velaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malar kooda unnai thodum velaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Poo endru thaanae sooda ninaikumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poo endru thaanae sooda ninaikumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Amutham undu vaazhnthaal aayul mudivathillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amutham undu vaazhnthaal aayul mudivathillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un azhagai paarthu vaazhnthaal amutham thevai illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un azhagai paarthu vaazhnthaal amutham thevai illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai thedum pothu idhayam ingu sugamaaga tholainthadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai thedum pothu idhayam ingu sugamaaga tholainthadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaar intha devathai yaar intha devathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar intha devathai yaar intha devathai"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar intha devathai yaar intha devathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar intha devathai yaar intha devathai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbae un kangal suzhal engiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbae un kangal suzhal engiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Athanaalae angae muzhgi pogiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athanaalae angae muzhgi pogiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbae un peyarai padagengiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbae un peyarai padagengiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Athai solli thaanae karaiyai sergiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athai solli thaanae karaiyai sergiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un kolusin osai ketka thanga manigal korpen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kolusin osai ketka thanga manigal korpen"/>
</div>
<div class="lyrico-lyrics-wrapper">Athil irandu kurainthu ponaal kannin manigal serpen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athil irandu kurainthu ponaal kannin manigal serpen"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai theevu polae kaathu nirka kadalaaga maaruven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai theevu polae kaathu nirka kadalaaga maaruven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaar intha devathai yaar intha devathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar intha devathai yaar intha devathai"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar intha devathai yaar intha devathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar intha devathai yaar intha devathai"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru kodi pookal uzhagengum undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru kodi pookal uzhagengum undu"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha pen polae azhagana poo ondru ullatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha pen polae azhagana poo ondru ullatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaar intha devathai yaar intha devathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar intha devathai yaar intha devathai"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar intha devathai yaar intha devathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar intha devathai yaar intha devathai"/>
</div>
</pre>
