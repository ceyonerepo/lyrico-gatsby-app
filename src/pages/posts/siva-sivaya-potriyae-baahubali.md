---
title: 'siva sivaya potriyae song lyrics'
album: 'Baahubali'
artist: "M.M. Keeravani"
lyricist: 'Madhan Karky'
director: 'S.S. Rajamouli'
path: '/albums/baahubali-song-lyrics'
song: 'Siva Sivaya Potriyae'
image: ../../images/albumart/baahubali.jpg
date: 2015-06-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/tB1clq0VNWE"
type: 'Devotional'
singers: 
- M.M. Keeravani
- Vaikom Vijayalakshmi
---

<pre class="lyrics-native">
  </pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Sivaa sivaaya potriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sivaa sivaaya potriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Namachchivaaya potriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namachchivaaya potriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pirapparukkum aeganae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirapparukkum aeganae"/>
</div>
<div class="lyrico-lyrics-wrapper">Porutharul aneganae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porutharul aneganae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paramporul un naamathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paramporul un naamathai"/>
</div>
<div class="lyrico-lyrics-wrapper">Karangguvithu paadinom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karangguvithu paadinom"/>
</div>
<div class="lyrico-lyrics-wrapper">Irappili un kaalkalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irappili un kaalkalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirangguvithu thedinom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirangguvithu thedinom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaaru ivan…yaaru ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaru ivan…yaaru ivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallathookki poraanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallathookki poraanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pulla pola tholu mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pulla pola tholu mela"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna thookki poraanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna thookki poraanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannu rendu podhala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu rendu podhala"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiyu kaalu odala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyu kaalu odala"/>
</div>
<div class="lyrico-lyrics-wrapper">Gangaiyathaan thedikittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gangaiyathaan thedikittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanna thaanae sumandhukittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanna thaanae sumandhukittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Lingam nadandhu pogudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lingam nadandhu pogudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yellaiyillaadha aadhiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yellaiyillaadha aadhiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaamunarndha jodhiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaamunarndha jodhiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Malaimagal un paadhiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malaimagal un paadhiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalaimagal un kaidhiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalaimagal un kaidhiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arulvallaan em arpudhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arulvallaan em arpudhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Arumporul em archchidhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arumporul em archchidhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Umai virumbum uthaman
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Umai virumbum uthaman"/>
</div>
<div class="lyrico-lyrics-wrapper">Uruvilaa em uruthiran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uruvilaa em uruthiran"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Olirvidum em thesanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Olirvidum em thesanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kulirmalai than vaasanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulirmalai than vaasanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhilmigu em nesanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhilmigu em nesanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhithozhikkum eesanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhithozhikkum eesanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nillaamal aadum pandhamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nillaamal aadum pandhamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallaagi nirkum undhamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallaagi nirkum undhamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallaa engatku sondhamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallaa engatku sondhamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaa uyirkkum andhamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaa uyirkkum andhamae"/>
</div>
</pre>