---
title: 'kutty story song lyrics'
album: 'Master'
artist: 'Anirudh Ravichander'
lyricist: 'Arunraja Kamaraj'
director: 'Lokesh Kanagaraj'
path: '/albums/master-song-lyrics'
song: 'Kutty Story'
image: ../../images/albumart/master.jpg
date: 2021-01-13
lang: tamil
youtubeLink: 'https://www.youtube.com/embed/gjnrtCKZqYg'
type: 'philosophy'
singers: 
- Vijay
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">

  <div class="lyrico-lyrics-wrapper">Let me sing a kutti story
<input type="checkbox" class="lyrico-select-lyric-line" value="Let me sing a kutti story"/>
</div>
<div class="lyrico-lyrics-wrapper">Pay attention listen to me
<input type="checkbox" class="lyrico-select-lyric-line" value="Pay attention listen to me"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Enna-na englishuu…
<input type="checkbox" class="lyrico-select-lyric-line" value="Enna-na englishuu…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Just listen bro..
<input type="checkbox" class="lyrico-select-lyric-line" value="Just listen bro.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Let me sing a kutti story
<input type="checkbox" class="lyrico-select-lyric-line" value="Let me sing a kutti story"/>
</div>
<div class="lyrico-lyrics-wrapper">Pay attention listen to me
<input type="checkbox" class="lyrico-select-lyric-line" value="Pay attention listen to me"/>
</div>
<div class="lyrico-lyrics-wrapper">If you want take it
<input type="checkbox" class="lyrico-select-lyric-line" value="If you want take it"/>
</div>
<div class="lyrico-lyrics-wrapper">Or else venam tension
<input type="checkbox" class="lyrico-select-lyric-line" value="Or else venam tension"/>
</div>
<div class="lyrico-lyrics-wrapper">leave it baby..
<input type="checkbox" class="lyrico-select-lyric-line" value="leave it baby.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Life is very short nanbaa…
<input type="checkbox" class="lyrico-select-lyric-line" value="Life is very short nanbaa…"/>
</div>
<div class="lyrico-lyrics-wrapper">Always be happy..
<input type="checkbox" class="lyrico-select-lyric-line" value="Always be happy.."/>
</div>
<div class="lyrico-lyrics-wrapper">Palavidha problems will
<input type="checkbox" class="lyrico-select-lyric-line" value="Palavidha problems will"/>
</div>
<div class="lyrico-lyrics-wrapper">Come and go
<input type="checkbox" class="lyrico-select-lyric-line" value="Come and go"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam chill pannu maapi..
<input type="checkbox" class="lyrico-select-lyric-line" value="Konjam chill pannu maapi.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Together man….
<input type="checkbox" class="lyrico-select-lyric-line" value="Together man…."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Let me sing a kutti story
<input type="checkbox" class="lyrico-select-lyric-line" value="Let me sing a kutti story"/>
</div>
<div class="lyrico-lyrics-wrapper">Pay attention listen to me
<input type="checkbox" class="lyrico-select-lyric-line" value="Pay attention listen to me"/>
</div>
<div class="lyrico-lyrics-wrapper">If you want take it
<input type="checkbox" class="lyrico-select-lyric-line" value="If you want take it"/>
</div>
<div class="lyrico-lyrics-wrapper">Or else venam tension
<input type="checkbox" class="lyrico-select-lyric-line" value="Or else venam tension"/>
</div>
<div class="lyrico-lyrics-wrapper">Leave it baby..
<input type="checkbox" class="lyrico-select-lyric-line" value="Leave it baby.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Life is very short nanbaa…
<input type="checkbox" class="lyrico-select-lyric-line" value="Life is very short nanbaa…"/>
</div>
<div class="lyrico-lyrics-wrapper">Always be happy..
<input type="checkbox" class="lyrico-select-lyric-line" value="Always be happy.."/>
</div>
<div class="lyrico-lyrics-wrapper">Design design ah..
<input type="checkbox" class="lyrico-select-lyric-line" value="Design design ah.."/>
</div>
<div class="lyrico-lyrics-wrapper">Problems will come and go
<input type="checkbox" class="lyrico-select-lyric-line" value="Problems will come and go"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam chill pannu maapi..
<input type="checkbox" class="lyrico-select-lyric-line" value="Konjam chill pannu maapi.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">No tension baby….
<input type="checkbox" class="lyrico-select-lyric-line" value="No tension baby…."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Speedaa ponaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Speedaa ponaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gavanam mustu…
<input type="checkbox" class="lyrico-select-lyric-line" value="Gavanam mustu…"/>
</div>
<div class="lyrico-lyrics-wrapper">Slowaah ponaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Slowaah ponaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Steadyum mustu…
<input type="checkbox" class="lyrico-select-lyric-line" value="Steadyum mustu…"/>
</div>
<div class="lyrico-lyrics-wrapper">Heyyy hahahaha
<input type="checkbox" class="lyrico-select-lyric-line" value="Heyyy hahahaha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Anger always misery baby
<input type="checkbox" class="lyrico-select-lyric-line" value="Anger always misery baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Friendaah ninna powerful maapi
<input type="checkbox" class="lyrico-select-lyric-line" value="Friendaah ninna powerful maapi"/>
</div>
<div class="lyrico-lyrics-wrapper">Haters are gonna hate
<input type="checkbox" class="lyrico-select-lyric-line" value="Haters are gonna hate"/>
</div>
<div class="lyrico-lyrics-wrapper">But ignore calmly
<input type="checkbox" class="lyrico-select-lyric-line" value="But ignore calmly"/>
</div>
<div class="lyrico-lyrics-wrapper">Negativity ellam thalli vai baby
<input type="checkbox" class="lyrico-select-lyric-line" value="Negativity ellam thalli vai baby"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Focus on what you dream
<input type="checkbox" class="lyrico-select-lyric-line" value="Focus on what you dream"/>
</div>
<div class="lyrico-lyrics-wrapper">And dont worry maapi
<input type="checkbox" class="lyrico-select-lyric-line" value="And dont worry maapi"/>
</div>
<div class="lyrico-lyrics-wrapper">Postivity unna lift pannum baby
<input type="checkbox" class="lyrico-select-lyric-line" value="Postivity unna lift pannum baby"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Life is very short nanbaa…
<input type="checkbox" class="lyrico-select-lyric-line" value="Life is very short nanbaa…"/>
</div>
<div class="lyrico-lyrics-wrapper">Always be happy..
<input type="checkbox" class="lyrico-select-lyric-line" value="Always be happy.."/>
</div>
<div class="lyrico-lyrics-wrapper">Very many problems will
<input type="checkbox" class="lyrico-select-lyric-line" value="Very many problems will"/>
</div>
<div class="lyrico-lyrics-wrapper">Come and go
<input type="checkbox" class="lyrico-select-lyric-line" value="Come and go"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam chill pannu maapi..
<input type="checkbox" class="lyrico-select-lyric-line" value="Konjam chill pannu maapi.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Students….
<input type="checkbox" class="lyrico-select-lyric-line" value="Students…."/>
</div>
<div class="lyrico-lyrics-wrapper">Let me sing a kutti story
<input type="checkbox" class="lyrico-select-lyric-line" value="Let me sing a kutti story"/>
</div>
<div class="lyrico-lyrics-wrapper">Pay attention… listen to me
<input type="checkbox" class="lyrico-select-lyric-line" value="Pay attention… listen to me"/>
</div>
<div class="lyrico-lyrics-wrapper">If you want take it
<input type="checkbox" class="lyrico-select-lyric-line" value="If you want take it"/>
</div>
<div class="lyrico-lyrics-wrapper">Or else venam tension
<input type="checkbox" class="lyrico-select-lyric-line" value="Or else venam tension"/>
</div>
<div class="lyrico-lyrics-wrapper">Leave it baby..
<input type="checkbox" class="lyrico-select-lyric-line" value="Leave it baby.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Life is very short nanbaa…
<input type="checkbox" class="lyrico-select-lyric-line" value="Life is very short nanbaa…"/>
</div>
<div class="lyrico-lyrics-wrapper">Always be happy..
<input type="checkbox" class="lyrico-select-lyric-line" value="Always be happy.."/>
</div>
<div class="lyrico-lyrics-wrapper">Design design ah..
<input type="checkbox" class="lyrico-select-lyric-line" value="Design design ah.."/>
</div>
<div class="lyrico-lyrics-wrapper">Problems will come and go
<input type="checkbox" class="lyrico-select-lyric-line" value="Problems will come and go"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam chill pannu maapi..
<input type="checkbox" class="lyrico-select-lyric-line" value="Konjam chill pannu maapi.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">No tension baby….
<input type="checkbox" class="lyrico-select-lyric-line" value="No tension baby…."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Hardworkum venum
<input type="checkbox" class="lyrico-select-lyric-line" value="Hardworkum venum"/>
</div>
<div class="lyrico-lyrics-wrapper">Smart workum venum
<input type="checkbox" class="lyrico-select-lyric-line" value="Smart workum venum"/>
</div>
<div class="lyrico-lyrics-wrapper">Self motivation adhu needhanae
<input type="checkbox" class="lyrico-select-lyric-line" value="Self motivation adhu needhanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Education venum
<input type="checkbox" class="lyrico-select-lyric-line" value="Education venum"/>
</div>
<div class="lyrico-lyrics-wrapper">Dedication venum
<input type="checkbox" class="lyrico-select-lyric-line" value="Dedication venum"/>
</div>
<div class="lyrico-lyrics-wrapper">Self valuation adha panni paaren
<input type="checkbox" class="lyrico-select-lyric-line" value="Self valuation adha panni paaren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Dont be the person spreading
<input type="checkbox" class="lyrico-select-lyric-line" value="Dont be the person spreading"/>
</div>
<div class="lyrico-lyrics-wrapper">Haterd maapi
<input type="checkbox" class="lyrico-select-lyric-line" value="Haterd maapi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pinnadi pesuradhu rombha crappy
<input type="checkbox" class="lyrico-select-lyric-line" value="Pinnadi pesuradhu rombha crappy"/>
</div>
<div class="lyrico-lyrics-wrapper">Always be polite and just dont be nasty
<input type="checkbox" class="lyrico-select-lyric-line" value="Always be polite and just dont be nasty"/>
</div>
<div class="lyrico-lyrics-wrapper">You will be the reason to make
<input type="checkbox" class="lyrico-select-lyric-line" value="You will be the reason to make"/>
</div>
<div class="lyrico-lyrics-wrapper">Someone happy
<input type="checkbox" class="lyrico-select-lyric-line" value="Someone happy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Life is very short nanbaa…
<input type="checkbox" class="lyrico-select-lyric-line" value="Life is very short nanbaa…"/>
</div>
<div class="lyrico-lyrics-wrapper">Always be happy..
<input type="checkbox" class="lyrico-select-lyric-line" value="Always be happy.."/>
</div>
<div class="lyrico-lyrics-wrapper">Palavidha problems will
<input type="checkbox" class="lyrico-select-lyric-line" value="Palavidha problems will"/>
</div>
<div class="lyrico-lyrics-wrapper">Come and go
<input type="checkbox" class="lyrico-select-lyric-line" value="Come and go"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam chill pannu maapi..
<input type="checkbox" class="lyrico-select-lyric-line" value="Konjam chill pannu maapi.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">One last time
<input type="checkbox" class="lyrico-select-lyric-line" value="One last time"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Let me sing a kutti story
<input type="checkbox" class="lyrico-select-lyric-line" value="Let me sing a kutti story"/>
</div>
<div class="lyrico-lyrics-wrapper">Pay attention…. listen to me
<input type="checkbox" class="lyrico-select-lyric-line" value="Pay attention…. listen to me"/>
</div>
<div class="lyrico-lyrics-wrapper">If you want take it
<input type="checkbox" class="lyrico-select-lyric-line" value="If you want take it"/>
</div>
<div class="lyrico-lyrics-wrapper">Or else venam tension
<input type="checkbox" class="lyrico-select-lyric-line" value="Or else venam tension"/>
</div>
<div class="lyrico-lyrics-wrapper">Leave it baby..
<input type="checkbox" class="lyrico-select-lyric-line" value="Leave it baby.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Life is very short nanbaa…
<input type="checkbox" class="lyrico-select-lyric-line" value="Life is very short nanbaa…"/>
</div>
<div class="lyrico-lyrics-wrapper">Always be happy..
<input type="checkbox" class="lyrico-select-lyric-line" value="Always be happy.."/>
</div>
<div class="lyrico-lyrics-wrapper">Design design ah..
<input type="checkbox" class="lyrico-select-lyric-line" value="Design design ah.."/>
</div>
<div class="lyrico-lyrics-wrapper">Problems will come and go
<input type="checkbox" class="lyrico-select-lyric-line" value="Problems will come and go"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam chill pannu maapi..
<input type="checkbox" class="lyrico-select-lyric-line" value="Konjam chill pannu maapi.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">That was my kutty story
<input type="checkbox" class="lyrico-select-lyric-line" value="That was my kutty story"/>
</div>
<div class="lyrico-lyrics-wrapper">How was my kutty story
<input type="checkbox" class="lyrico-select-lyric-line" value="How was my kutty story"/>
</div>
<div class="lyrico-lyrics-wrapper">That was my kutty story
<input type="checkbox" class="lyrico-select-lyric-line" value="That was my kutty story"/>
</div>
<div class="lyrico-lyrics-wrapper">How was my kutty story…
<input type="checkbox" class="lyrico-select-lyric-line" value="How was my kutty story…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Just awesome-na
<input type="checkbox" class="lyrico-select-lyric-line" value="Just awesome-na"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">No tension babyy…
<input type="checkbox" class="lyrico-select-lyric-line" value="No tension babyy…"/>
</div>
</pre>