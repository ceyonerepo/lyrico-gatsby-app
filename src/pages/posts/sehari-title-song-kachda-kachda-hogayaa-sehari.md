---
title: "sehari title song song lyrics"
album: "Sehari"
artist: "Prashanth R Vihari"
lyricist: "Bhaskarabhatla "
director: "Gnanasagar Dwaraka"
path: "/albums/sehari-lyrics"
song: "Sehari Title Song - Kachda Kachda Hogayaa"
image: ../../images/albumart/sehari.jpg
date: 2022-02-11
lang: telugu
youtubeLink: "https://www.youtube.com/embed/py5UHMKj8a8"
type: "title song"
singers:
  - Ram Miryala 
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Haa kachda kachda hogayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa kachda kachda hogayaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ardhamaitha ledhayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ardhamaitha ledhayaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yehe achata muchata ledhayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yehe achata muchata ledhayaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vashapadathaledhu endhayyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vashapadathaledhu endhayyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Esina peggu esthaavunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Esina peggu esthaavunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Kickke vasthaledhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kickke vasthaledhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Andhari naseebu raasinodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhari naseebu raasinodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanne dhekaledhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanne dhekaledhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nanne nannere arey nanne nannere
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanne nannere arey nanne nannere"/>
</div>
<div class="lyrico-lyrics-wrapper">Lite lelore ye ye massu steppey raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lite lelore ye ye massu steppey raa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenaadaalanna paadaalannaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenaadaalanna paadaalannaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Zindagilo lehde sehari sehari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zindagilo lehde sehari sehari"/>
</div>
<div class="lyrico-lyrics-wrapper">Chillavvaalanna navvaalanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chillavvaalanna navvaalanna"/>
</div>
<div class="lyrico-lyrics-wrapper">Zindagilo ledhe sehari sehari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zindagilo ledhe sehari sehari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aa hey atu itu atu itu ani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa hey atu itu atu itu ani"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye dhaari thochadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye dhaari thochadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasuku nilakada lene ledhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasuku nilakada lene ledhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Theliyani vayasidhi kadhaa edhedho cheshaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theliyani vayasidhi kadhaa edhedho cheshaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappanthaa naadhe naadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappanthaa naadhe naadhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh my god-u, oho it’s so hard-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh my god-u, oho it’s so hard-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Arere arere life ye riskaipoye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arere arere life ye riskaipoye"/>
</div>
<div class="lyrico-lyrics-wrapper">Egiraane innaallu ningi anchullona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Egiraane innaallu ningi anchullona"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhooraane korelli panjaraanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhooraane korelli panjaraanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ontariga irukkupoyi shoonyamlona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ontariga irukkupoyi shoonyamlona"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa swetcha koraku choosthu unnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa swetcha koraku choosthu unnaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenaadaalanna paadaalannaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenaadaalanna paadaalannaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Zindagilo lehde sehari sehari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zindagilo lehde sehari sehari"/>
</div>
<div class="lyrico-lyrics-wrapper">Chillavvaalanna navvaalanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chillavvaalanna navvaalanna"/>
</div>
<div class="lyrico-lyrics-wrapper">Zindagilo ledhe sehari sehari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zindagilo ledhe sehari sehari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aa sehari sehari choode nannosaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa sehari sehari choode nannosaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Sehari sehari ledhe vere dhaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sehari sehari ledhe vere dhaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Sehari sehari vachhey saraasari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sehari sehari vachhey saraasari"/>
</div>
<div class="lyrico-lyrics-wrapper">Ipude ipude ipude sehari sehari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ipude ipude ipude sehari sehari"/>
</div>
</pre>
