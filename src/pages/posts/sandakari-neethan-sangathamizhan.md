---
title: "sandakari neethan song lyrics"
album: "Sangathamizhan"
artist: "	Vivek - Mervin"
lyricist: "Prakash Francis"
director: "Vijay Chandar"
path: "/albums/sangathamizhan-lyrics"
song: "Sandakari Neethan"
image: ../../images/albumart/sangathamizhan.jpg
date: 2019-11-15
lang: tamil
youtubeLink: "https://www.youtube.com/embed/jH0s36fgP54"
type: "love"
singers:
  - Anirudh Ravichander
  - Jonita Gandhi
  - Mervin Solomon
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">En Sandakaari Needhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Sandakaari Needhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">En Sandakozhi Needhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Sandakozhi Needhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Sathiyama Inimel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sathiyama Inimel"/>
</div>
<div class="lyrico-lyrics-wrapper">En Sondhamellam Nee Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Sondhamellam Nee Dhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ehh Ennai Thaandi Poravalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ehh Ennai Thaandi Poravalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Orakannal Oru Paarvai Paarthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orakannal Oru Paarvai Paarthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Konna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Konna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sariyaa Nadanthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sariyaa Nadanthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaanavae Sarukkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanavae Sarukkuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennadi Enna Panna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennadi Enna Panna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaedho Maaruthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaedho Maaruthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Bodhai Yeruthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bodhai Yeruthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Paarkaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Paarkaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaedho Aaguthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaedho Aaguthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaam Seruthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaam Seruthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam Sirikaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Sirikaiyila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Thaandi Ponaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Thaandi Ponaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanna Kaati Ponaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna Kaati Ponaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Thaandi Ponaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Thaandi Ponaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanna Kaatti Pogum Pothae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna Kaatti Pogum Pothae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Ava Kondu Ponaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Ava Kondu Ponaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sandakaari Needhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandakaari Needhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">En Sandakozhi Needhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Sandakozhi Needhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Sathiyama Inimel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sathiyama Inimel"/>
</div>
<div class="lyrico-lyrics-wrapper">En Sondhamellam Nee Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Sondhamellam Nee Dhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sandakaari Needhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandakaari Needhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">En Sandakozhi Needhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Sandakozhi Needhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Sathiyama Inimel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Sathiyama Inimel"/>
</div>
<div class="lyrico-lyrics-wrapper">En Sondhamellam Nee Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Sondhamellam Nee Dhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mazhai Thuli Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai Thuli Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Malalaiyum Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malalaiyum Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Ennai Sera Kaathirupenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Ennai Sera Kaathirupenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irai Madhi Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irai Madhi Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Nila Oli Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nila Oli Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Nee Varum Neram Paathirupenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Nee Varum Neram Paathirupenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu Yenoo Pudhu Mayakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Yenoo Pudhu Mayakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thelinthidum Ennam Yenoo Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thelinthidum Ennam Yenoo Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini Venaam Oru Thayakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Venaam Oru Thayakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">Iruthi Varaim Nam Pirivae Illai Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruthi Varaim Nam Pirivae Illai Illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eh Ennai Thaandi Poravalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh Ennai Thaandi Poravalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Orakannal Oru Paarvai Paarthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orakannal Oru Paarvai Paarthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Konna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Konna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sariyaa Nadanthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sariyaa Nadanthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaanavae Sarukkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanavae Sarukkuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennadi Enna Panna Enna Panna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennadi Enna Panna Enna Panna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaedho Maarutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaedho Maarutha"/>
</div>
<div class="lyrico-lyrics-wrapper">Bodhai Yerutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bodhai Yerutha"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Paakkaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Paakkaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaedhoo Aagutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaedhoo Aagutha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaam Maarutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaam Maarutha"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam Sirikaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Sirikaiyila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Thaandi Ponaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Thaandi Ponaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanna Kaatti Pogum Pothae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna Kaatti Pogum Pothae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Ava Kondu Ponaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Ava Kondu Ponaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sandakaari Needhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandakaari Needhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">En Sandakozhi Needhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Sandakozhi Needhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Sathiyama Inimel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sathiyama Inimel"/>
</div>
<div class="lyrico-lyrics-wrapper">En Sondhamellam Nee Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Sondhamellam Nee Dhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sandakaari Needhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandakaari Needhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">En Sandakozhi Needhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Sandakozhi Needhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Sathiyama Inimel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Sathiyama Inimel"/>
</div>
<div class="lyrico-lyrics-wrapper">En Sondhamellam Nee Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Sondhamellam Nee Dhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eh Enna Thaandi Ponaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh Enna Thaandi Ponaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Thaandi Ponaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Thaandi Ponaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandakaari Needhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandakaari Needhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">En Sandakozhi Needhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Sandakozhi Needhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Sathiyama Inimel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Sathiyama Inimel"/>
</div>
<div class="lyrico-lyrics-wrapper">En Sondhamellam Nee Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Sondhamellam Nee Dhaan"/>
</div>
</pre>
