---
title: "dhimu dhimu song lyrics"
album: "Engeyum Kadhal"
artist: "Harris Jayaraj"
lyricist: "Na. Muthukumar"
director: "Prabhudeva"
path: "/albums/engeyum-kadhal-lyrics"
song: "Dhimu Dhimu"
image: ../../images/albumart/engeyum-kadhal.jpg
date: 2011-05-06
lang: tamil
youtubeLink: "https://www.youtube.com/embed/QoK8x_2C4go"
type: "love"
singers:
  - Karthik
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Konjam Kanavugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Kanavugal"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam Ninaivugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Ninaivugal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjai Nanjaakki Sellum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjai Nanjaakki Sellum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konjum Uravugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjum Uravugal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kenjum Thinaivugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kenjum Thinaivugal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannai Thundaakki Thullum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannai Thundaakki Thullum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konjam Kanavugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Kanavugal"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam Ninaivugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Ninaivugal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjai Nanjaakki Sellum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjai Nanjaakki Sellum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konjum Uravugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjum Uravugal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kenjum Thinaivugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kenjum Thinaivugal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannai Thundaakki Thullum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannai Thundaakki Thullum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhimu Dhimu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhimu Dhimu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhim Dhim Dhinam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhim Dhim Dhinam"/>
</div>
<div class="lyrico-lyrics-wrapper">Allaadum Manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allaadum Manam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannil Kadhal Varam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannil Kadhal Varam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhama Dhama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhama Dhama"/>
</div>
<div class="lyrico-lyrics-wrapper">Dham Dham Sugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dham Dham Sugam"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaale Nitham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaale Nitham"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil Koodum Manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil Koodum Manam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Anbe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Anbe"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Sendraal Kooda Vaasam Veesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Sendraal Kooda Vaasam Veesum"/>
</div>
<div class="lyrico-lyrics-wrapper">Veesum Veesum Veesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veesum Veesum Veesum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Anbe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Anbe"/>
</div>
<div class="lyrico-lyrics-wrapper">En Naatkal Endrum Pola Pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Naatkal Endrum Pola Pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogum Pogum Pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogum Pogum Pogum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Ulle En Ulle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Ulle En Ulle"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannaale Kadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannaale Kadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ganam Konden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ganam Konden"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhimu Dhimu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhimu Dhimu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhim Dhim Dhinam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhim Dhim Dhinam"/>
</div>
<div class="lyrico-lyrics-wrapper">Allaadum Manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allaadum Manam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannil Kadhal Varam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannil Kadhal Varam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhama Dhama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhama Dhama"/>
</div>
<div class="lyrico-lyrics-wrapper">Dham Dham Sugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dham Dham Sugam"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaale Nitham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaale Nitham"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil Koodum Manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil Koodum Manam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ullame Ullame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullame Ullame"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulle Unnai Kaana Vandhene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulle Unnai Kaana Vandhene"/>
</div>
<div class="lyrico-lyrics-wrapper">Undaagiraai Thundaagiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undaagiraai Thundaagiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaal Kaayam Kondene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaal Kaayam Kondene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaayaththai Nesiththene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaayaththai Nesiththene"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Solla Naanum Ini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Solla Naanum Ini"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Kanavilum Vasithene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Kanavilum Vasithene"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennudaiya Ulagam Thani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennudaiya Ulagam Thani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konjam Kanavugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Kanavugal"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam Ninaivugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Ninaivugal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjai Nanjaakki Sellum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjai Nanjaakki Sellum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konjum Uravugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjum Uravugal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kenjum Thinaivugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kenjum Thinaivugal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannai Thundaakki Thullum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannai Thundaakki Thullum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konjam Kanavugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Kanavugal"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam Ninaivugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Ninaivugal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjai Nanjaakki Sellum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjai Nanjaakki Sellum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konjum Uravugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjum Uravugal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kenjum Thinaivugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kenjum Thinaivugal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannai Thundaakki Thullum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannai Thundaakki Thullum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Santhosamum Sogamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Santhosamum Sogamum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sernthu Vanthu Thakka Kandene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sernthu Vanthu Thakka Kandene"/>
</div>
<div class="lyrico-lyrics-wrapper">Santhegamaai Ennaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Santhegamaai Ennaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum Paarthu Kondene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum Paarthu Kondene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jaamaththil Vidukkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaamaththil Vidukkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Jannal Vali Thoongum Nizha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jannal Vali Thoongum Nizha"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kaaichalil Kothikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kaaichalil Kothikkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannukkulle Kadhal Vizha Vizha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannukkulle Kadhal Vizha Vizha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhimu Dhimu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhimu Dhimu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhim Dhim Dhinam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhim Dhim Dhinam"/>
</div>
<div class="lyrico-lyrics-wrapper">Allaadum Manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allaadum Manam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannil Kadhal Varam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannil Kadhal Varam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhama Dhama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhama Dhama"/>
</div>
<div class="lyrico-lyrics-wrapper">Dham Dham Sugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dham Dham Sugam"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaale Nitham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaale Nitham"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil Koodum Manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil Koodum Manam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Anbe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Anbe"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Sendraal Kooda Vaasam Veesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Sendraal Kooda Vaasam Veesum"/>
</div>
<div class="lyrico-lyrics-wrapper">Veesum Veesum Veesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veesum Veesum Veesum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Anbe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Anbe"/>
</div>
<div class="lyrico-lyrics-wrapper">En Naatkal Endrum Pola Pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Naatkal Endrum Pola Pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogum Pogum Pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogum Pogum Pogum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Ulle En Ulle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Ulle En Ulle"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannaale Kadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannaale Kadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ganam Konden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ganam Konden"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konjam Kanavugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Kanavugal"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam Ninaivugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Ninaivugal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjai Nanjaakki Sellum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjai Nanjaakki Sellum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konjum Uravugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjum Uravugal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kenjum Thinaivugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kenjum Thinaivugal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannai Thundaakki Thullum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannai Thundaakki Thullum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konjam Kanavugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Kanavugal"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam Ninaivugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Ninaivugal"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjum Uravugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjum Uravugal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kenjum Thinaivugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kenjum Thinaivugal"/>
</div>
</pre>
