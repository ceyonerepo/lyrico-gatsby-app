---
title: "antha ishtam song lyrics"
album: "Bheemla Nayak"
artist: "S. Thaman"
lyricist: "Ramajogayya Sastry"
director: "Saagar K Chandra"
path: "/albums/bheemla-nayak-lyrics"
song: "Antha Ishtam"
image: ../../images/albumart/bheemla-nayak.jpg
date: 2022-02-25
lang: telugu
youtubeLink: "https://www.youtube.com/embed/WjDTP0AtblE"
type: "love"
singers:
  - K.S. Chithra
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Eesintha Nannatta Na Na Na Na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eesintha Nannatta Na Na Na Na"/>
</div>
<div class="lyrico-lyrics-wrapper">Koosintha Panjeyyaniyyavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koosintha Panjeyyaniyyavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enthodive Gaani Mm Mm Na Na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enthodive Gaani Mm Mm Na Na"/>
</div>
<div class="lyrico-lyrics-wrapper">Muddisthe Maaraamu Seyyavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muddisthe Maaraamu Seyyavu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Peretti Nenetta 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peretti Nenetta "/>
</div>
<div class="lyrico-lyrics-wrapper">Pilichedi Thalichedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pilichedi Thalichedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Inti Penivitive
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inti Penivitive"/>
</div>
<div class="lyrico-lyrics-wrapper">Bottetti Muddhetti 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bottetti Muddhetti "/>
</div>
<div class="lyrico-lyrics-wrapper">Nanu Cheradeesina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanu Cheradeesina"/>
</div>
<div class="lyrico-lyrics-wrapper">Devulla Sarisaative
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Devulla Sarisaative"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa Bangari Maava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Bangari Maava"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Balashaali Maava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Balashaali Maava"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Melloni 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Melloni "/>
</div>
<div class="lyrico-lyrics-wrapper">Nallapoosallo Manipoosave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nallapoosallo Manipoosave"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Sudigaali Maava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Sudigaali Maava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eesintha Nannatta Pone Poniyyavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eesintha Nannatta Pone Poniyyavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Koosintha Panjeyyaniyyavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koosintha Panjeyyaniyyavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enthodive Gaani Sonthodive Nuvvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enthodive Gaani Sonthodive Nuvvu"/>
</div>
<div class="lyrico-lyrics-wrapper">Muddisthe Maaraamu Seyyavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muddisthe Maaraamu Seyyavu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gaali Kougillugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaali Kougillugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Chuttu Muttesi Untaavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chuttu Muttesi Untaavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oopiraadaneevurayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oopiraadaneevurayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Puttumachhalaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Puttumachhalaku"/>
</div>
<div class="lyrico-lyrics-wrapper">Thodabuttinaavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodabuttinaavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeku Naaku Dishti Thiyya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeku Naaku Dishti Thiyya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Antha Istame Endayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Istame Endayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha Istame Endayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Istame Endayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha Ishtamendayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Ishtamendayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeku, Naa Meena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeku, Naa Meena"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha Ishtamendayya Neeku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Ishtamendayya Neeku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eesintha Nannatta Pone Poniyyavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eesintha Nannatta Pone Poniyyavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Koosintha Panjeyyaniyyavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koosintha Panjeyyaniyyavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enthodive Gaani Sonthodive Nuvvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enthodive Gaani Sonthodive Nuvvu"/>
</div>
<div class="lyrico-lyrics-wrapper">Muddisthe Maaraamu Seyyavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muddisthe Maaraamu Seyyavu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gaali Kougillugaa Chuttu Muttesi Untaavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaali Kougillugaa Chuttu Muttesi Untaavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oopiraadaneevurayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oopiraadaneevurayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Puttumachhalaku Thodabuttinaavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Puttumachhalaku Thodabuttinaavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeku Naaku Dishti Thiyya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeku Naaku Dishti Thiyya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Antha Istame Endayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Istame Endayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha Istame Endayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Istame Endayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha Ishtamendayya Neeku Naa Meena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Ishtamendayya Neeku Naa Meena"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha Ishtamendayya Neeku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Ishtamendayya Neeku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Thalli Kannadho Ninnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Thalli Kannadho Ninnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Koti Kalalaku Raaraajai Velisinaavanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koti Kalalaku Raaraajai Velisinaavanta"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye Poota Puttinaavo Nuvvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Poota Puttinaavo Nuvvu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Achhangaa Punnami Ayyuntaadhanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Achhangaa Punnami Ayyuntaadhanta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Velakattalenanni Velugulni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velakattalenanni Velugulni"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Kanta Pooyinchinaavanta Nuvvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Kanta Pooyinchinaavanta Nuvvu"/>
</div>
<div class="lyrico-lyrics-wrapper">Etthu Kondameedi Kohinoore Gaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Etthu Kondameedi Kohinoore Gaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gunde Lothu Praanamaina Isthaavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gunde Lothu Praanamaina Isthaavu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Antha Istame Endayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Istame Endayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha Istame Endayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Istame Endayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha Ishtamendayya Neeku Naa Meena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Ishtamendayya Neeku Naa Meena"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha Ishtamendayya Neeku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Ishtamendayya Neeku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Antha Istame Endhayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Istame Endhayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha Istame Endhayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Istame Endhayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha Ishtamendayya Neeku Naa Meena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Ishtamendayya Neeku Naa Meena"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha Ishtamendayya Neeku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Ishtamendayya Neeku"/>
</div>
</pre>
