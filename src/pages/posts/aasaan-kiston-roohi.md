---
title: "aasaan kiston song lyrics"
album: "Roohi"
artist: "Sachin-Jigar"
lyricist: "Amitabh Bhattacharya"
director: "Hardik Mehta"
path: "/albums/roohi-lyrics"
song: "Aasaan Kiston"
image: ../../images/albumart/roohi.jpg
date: 2021-03-11
lang: hindi
youtubeLink: "https://www.youtube.com/embed/UaWe1REc81w"
type: "love"
singers:
  - Jubin Nautiyal
  - Sachin-Jigar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Chhup chhup ke dilbar ka deedar kar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chhup chhup ke dilbar ka deedar kar"/>
</div>
<div class="lyrico-lyrics-wrapper">Aye dil tu aahista izhaar kar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aye dil tu aahista izhaar kar"/>
</div>
<div class="lyrico-lyrics-wrapper">Saara ka saara na karna abhi se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saara ka saara na karna abhi se"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasaan kiston mein tu pyaar kar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasaan kiston mein tu pyaar kar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chhup chhup ke dilbar ka deedar kar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chhup chhup ke dilbar ka deedar kar"/>
</div>
<div class="lyrico-lyrics-wrapper">Aye dil tu aahista izhaar kar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aye dil tu aahista izhaar kar"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagle saara ka saara na karna abhi se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagle saara ka saara na karna abhi se"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasaan kiston mein tu pyaar kar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasaan kiston mein tu pyaar kar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pehle nibha ke dekhi hai tune
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pehle nibha ke dekhi hai tune"/>
</div>
<div class="lyrico-lyrics-wrapper">Mehangi mohabbat vilayati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mehangi mohabbat vilayati"/>
</div>
<div class="lyrico-lyrics-wrapper">Pad jaaye jisme lene ka dena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pad jaaye jisme lene ka dena"/>
</div>
<div class="lyrico-lyrics-wrapper">Ghaate ka sauda nihayati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ghaate ka sauda nihayati"/>
</div>
<div class="lyrico-lyrics-wrapper">Karna agar hi hai to pyaar kar le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karna agar hi hai to pyaar kar le"/>
</div>
<div class="lyrico-lyrics-wrapper">Sasta sa desi kifayati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sasta sa desi kifayati"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pehle tu jitna laparwah tha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pehle tu jitna laparwah tha"/>
</div>
<div class="lyrico-lyrics-wrapper">Utna sambhal ke hi iss baar kar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Utna sambhal ke hi iss baar kar"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagle saara ka saara na karna abhi se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagle saara ka saara na karna abhi se"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasaan kiston mein tu pyaar kar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasaan kiston mein tu pyaar kar"/>
</div>
</pre>
