---
title: "kadhale song lyrics"
album: "Kadhal Kan Kattudhe"
artist: "Pavan"
lyricist: "Mohanraja"
director: "Shivaraj"
path: "/albums/kadhal-kan-kattudhe-lyrics"
song: "Kadhale"
image: ../../images/albumart/kadhal-kan-kattudhe.jpg
date: 2017-02-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/O-z_aNJNs2s"
type: "love"
singers:
  - Karthik
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kaadhalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkenna pavam seitheno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkenna pavam seitheno"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu varai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu varai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vittuvittu engae sendraayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vittuvittu engae sendraayo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey kaadhalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey kaadhalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkenna pavam seitheno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkenna pavam seitheno"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu varai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu varai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vittuvittu engae sendraayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vittuvittu engae sendraayo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ethanai naatkalaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethanai naatkalaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Iththanai kaadhalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iththanai kaadhalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakena serthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakena serthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaithen nenjae naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaithen nenjae naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eththanai azhamaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eththanai azhamaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Iththnai aasaiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iththnai aasaiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakullae naan maraithen anbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakullae naan maraithen anbae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkenna pavam seitheno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkenna pavam seitheno"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu varai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu varai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vittuvittu engae sendraayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vittuvittu engae sendraayo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Netriravu vizhithirundhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netriravu vizhithirundhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Karanam nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karanam nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Karanam nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karanam nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Karanam nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karanam nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhigalaiyilae 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhigalaiyilae "/>
</div>
<div class="lyrico-lyrics-wrapper">vizhithu konden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vizhithu konden"/>
</div>
<div class="lyrico-lyrics-wrapper">Karanam nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karanam nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Karanam nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karanam nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Karanam nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karanam nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unai pirindhu engae ponalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai pirindhu engae ponalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninaivudanae neeyum varuvaayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaivudanae neeyum varuvaayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee illaa idamum enakkedhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee illaa idamum enakkedhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnarugae serndhae irundhalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnarugae serndhae irundhalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kan muzhuthum neeyae nirpaayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan muzhuthum neeyae nirpaayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Imaigaladhu paavam imaikaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imaigaladhu paavam imaikaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai yedho seigindrai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai yedho seigindrai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkenna pavam seitheno seitheno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkenna pavam seitheno seitheno"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu varai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu varai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vittuvittu engae sendraayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vittuvittu engae sendraayo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veetu suvaril kirukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veetu suvaril kirukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Siru pillai pola naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru pillai pola naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan perai kaatril
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan perai kaatril"/>
</div>
<div class="lyrico-lyrics-wrapper">Varaikindrenae avasaramaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varaikindrenae avasaramaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai paarkum nodiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai paarkum nodiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannaadi paarka thondrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaadi paarka thondrum"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan kannil ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan kannil ennai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan paarkindrenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan paarkindrenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru kanavaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru kanavaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kalaindhu ponalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kalaindhu ponalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Iravellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholaindha en 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholaindha en "/>
</div>
<div class="lyrico-lyrics-wrapper">kanavukku kaathiruppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavukku kaathiruppen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ulariyadhu vaarthai alla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulariyadhu vaarthai alla"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee vasikkum idhayam en kannae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee vasikkum idhayam en kannae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyrugiyadhu naanum alla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyrugiyadhu naanum alla"/>
</div>
<div class="lyrico-lyrics-wrapper">Ne kodutha kaadhal pennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne kodutha kaadhal pennae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkenna pavam seitheno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkenna pavam seitheno"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu varai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu varai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vittuvittu engae sendraayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vittuvittu engae sendraayo"/>
</div>
</pre>
