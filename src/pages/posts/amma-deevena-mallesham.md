---
title: "amma deevena song lyrics"
album: "Mallesham"
artist: "Mark K Robin"
lyricist: "Chandrabose"
director: "Raj R"
path: "/albums/mallesham-lyrics"
song: "Amma Deevena"
image: ../../images/albumart/mallesham.jpg
date: 2019-06-21
lang: telugu
youtubeLink: "https://www.youtube.com/embed/c7VeOUnEU8k"
type: "melody"
singers:
  - Sri Krishna
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Amma Deevena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma Deevena"/>
</div>
<div class="lyrico-lyrics-wrapper">Ika Nanna Rakshana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ika Nanna Rakshana"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Rendu Kalasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Rendu Kalasi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nene Ayyelaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nene Ayyelaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chentha Cheranaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chentha Cheranaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ika Seva Cheyanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ika Seva Cheyanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninu Chanti Paapalaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninu Chanti Paapalaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Saakalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saakalaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Buvva Pettanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Buvva Pettanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilaa Ilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilaa Ilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Bujjaginchanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bujjaginchanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathalu Cheppanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathalu Cheppanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Alaa Alaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alaa Alaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalatha Theerchanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalatha Theerchanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Bojja Lopala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Bojja Lopala"/>
</div>
<div class="lyrico-lyrics-wrapper">Bajjunna Paapani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bajjunna Paapani"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Nannakinka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Nannakinka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanukiyyavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanukiyyavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Buddhi Lopala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Buddhi Lopala"/>
</div>
<div class="lyrico-lyrics-wrapper">Daagunna Biddani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daagunna Biddani"/>
</div>
<div class="lyrico-lyrics-wrapper">Lokaanikantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lokaanikantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanukiyyanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanukiyyanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Amma Deevena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma Deevena"/>
</div>
<div class="lyrico-lyrics-wrapper">Ika Nanna Rakshana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ika Nanna Rakshana"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Rendu Kalasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Rendu Kalasi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nene Ayyelaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nene Ayyelaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Chentha Cheranaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chentha Cheranaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ika Seva Cheyanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ika Seva Cheyanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninu Chanti Paapalaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninu Chanti Paapalaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Saakalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saakalaa"/>
</div>
</pre>
