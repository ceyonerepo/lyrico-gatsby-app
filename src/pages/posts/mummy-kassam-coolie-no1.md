---
title: "mummy kassam song lyrics"
album: "Coolie No1"
artist: "Tanishk Bagchi"
lyricist: "Ikka - Shabbir Ahmed"
director: "David Dhawan"
path: "/albums/coolie-no1-lyrics"
song: "Mummy Kassam"
image: ../../images/albumart/coolie-no1.jpg
date: 2020-12-25
lang: hindi
youtubeLink: "https://www.youtube.com/embed/lG1y7x_W-kQ"
type: "happy"
singers:
  - Udit Narayan
  - Ikka
  - Monali Thakur
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ari oh monalisa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ari oh monalisa"/>
</div>
<div class="lyrico-lyrics-wrapper">Chalega aisa kaisa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chalega aisa kaisa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ari oh monalisa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ari oh monalisa"/>
</div>
<div class="lyrico-lyrics-wrapper">Chhodun na tera peechha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chhodun na tera peechha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho badi mind blowing ladki fasaayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho badi mind blowing ladki fasaayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Badi mind blowing ladki fasaayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Badi mind blowing ladki fasaayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Mummy kasam main to gaya die
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mummy kasam main to gaya die"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arre winter ka mausam hai bhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arre winter ka mausam hai bhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aye winter ka mausam hai bhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aye winter ka mausam hai bhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Upar se chhoti rajayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Upar se chhoti rajayi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho tera dhoondh ke laya hun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho tera dhoondh ke laya hun"/>
</div>
<div class="lyrico-lyrics-wrapper">Jhumka bazaar se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jhumka bazaar se"/>
</div>
<div class="lyrico-lyrics-wrapper">Kab se khada hun bann ke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kab se khada hun bann ke"/>
</div>
<div class="lyrico-lyrics-wrapper">Majhnu kataar mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Majhnu kataar mein"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaha aihe oho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaha aihe oho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhoondh ke laya hun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhoondh ke laya hun"/>
</div>
<div class="lyrico-lyrics-wrapper">Jhumka bazaar se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jhumka bazaar se"/>
</div>
<div class="lyrico-lyrics-wrapper">Kab se khada hun bann ke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kab se khada hun bann ke"/>
</div>
<div class="lyrico-lyrics-wrapper">Majhnu kataar mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Majhnu kataar mein"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haye re teri beauty ne neendein udayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haye re teri beauty ne neendein udayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Mummy kassam main to gaya die
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mummy kassam main to gaya die"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Baaton mein teri main na aaun balma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baaton mein teri main na aaun balma"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaahe tu beech mein le aaye teri maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaahe tu beech mein le aaye teri maa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haan"/>
</div>
<div class="lyrico-lyrics-wrapper">Baaton mein teri main na aaun balma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baaton mein teri main na aaun balma"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaahe tu beech mein le aaye teri maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaahe tu beech mein le aaye teri maa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaahe peechhe hai pada tu to ziddi hai bada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaahe peechhe hai pada tu to ziddi hai bada"/>
</div>
<div class="lyrico-lyrics-wrapper">Teri jhoothi baaton mein na main
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teri jhoothi baaton mein na main"/>
</div>
<div class="lyrico-lyrics-wrapper">Aayi aayi aayi aayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayi aayi aayi aayi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho dafa 302 mujhe lagwayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho dafa 302 mujhe lagwayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Mummy kasam main to gaya die
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mummy kasam main to gaya die"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bade bhagyon se bani tu lugayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bade bhagyon se bani tu lugayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Mummy kasam main to gaya die
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mummy kasam main to gaya die"/>
</div>
</pre>
