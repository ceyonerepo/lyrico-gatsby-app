---
title: "mathurangalam song lyrics"
album: "Kavan"
artist: "Hiphop Tamizha"
lyricist: "Kabilan Vairamuthu"
director: "K V Anand"
path: "/albums/kavan-lyrics"
song: "Mathurangalam"
image: ../../images/albumart/kavan.jpg
date: 2017-03-31
lang: tamil
youtubeLink: "https://www.youtube.com/embed/K6vtt3DXEnw"
type: "happy"
singers:
  - Kaber Vasuki 
  - Kids Chorus 
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Mathuraaingalam edhayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mathuraaingalam edhayo"/>
</div>
<div class="lyrico-lyrics-wrapper">Mathuraaingalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mathuraaingalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aalae illadhaa kadaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalae illadhaa kadaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Tea-ya athuraaingalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tea-ya athuraaingalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mathuraaingalam edhayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mathuraaingalam edhayo"/>
</div>
<div class="lyrico-lyrics-wrapper">Mathuraaingalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mathuraaingalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aalae illadhaa kadaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalae illadhaa kadaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Tea-ya athuraaingalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tea-ya athuraaingalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aiyaai yaeee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyaai yaeee"/>
</div>
<div class="lyrico-lyrics-wrapper">Aii aiyaa aiyaai yaeee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aii aiyaa aiyaai yaeee"/>
</div>
<div class="lyrico-lyrics-wrapper">Aiyaai yaeee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyaai yaeee"/>
</div>
<div class="lyrico-lyrics-wrapper">Aii aiyaa aiyaai yaeee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aii aiyaa aiyaai yaeee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iththu pona office kulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iththu pona office kulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithana kootha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithana kootha"/>
</div>
<div class="lyrico-lyrics-wrapper">Oththa patil osara neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oththa patil osara neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Rajinikantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rajinikantha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ootukkaga unna petha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ootukkaga unna petha"/>
</div>
<div class="lyrico-lyrics-wrapper">Unga aatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unga aatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kannitheevil matikitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kannitheevil matikitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Sindhu bhatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sindhu bhatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sikku vizhundha serial than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sikku vizhundha serial than"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirappu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un dokku vizhundha documentary
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un dokku vizhundha documentary"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaduppu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaduppu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vikka vandha sandha kulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vikka vandha sandha kulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mavanae makki pona 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mavanae makki pona "/>
</div>
<div class="lyrico-lyrics-wrapper">kolgai ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kolgai ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Aiyaai yaeee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyaai yaeee"/>
</div>
<div class="lyrico-lyrics-wrapper">Aii aiyaa aiyaai yaeee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aii aiyaa aiyaai yaeee"/>
</div>
<div class="lyrico-lyrics-wrapper">Aiyaai yaeee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyaai yaeee"/>
</div>
<div class="lyrico-lyrics-wrapper">Aii aiyaa aiyaai yaeee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aii aiyaa aiyaai yaeee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mathuraaingalam edhayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mathuraaingalam edhayo"/>
</div>
<div class="lyrico-lyrics-wrapper">Mathuraaingalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mathuraaingalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aalae illadhaa kadaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalae illadhaa kadaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Tea-ya athuraaingalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tea-ya athuraaingalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mathuraaingalam edhayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mathuraaingalam edhayo"/>
</div>
<div class="lyrico-lyrics-wrapper">Mathuraaingalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mathuraaingalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aalae illadhaa kadaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalae illadhaa kadaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Tea-ya athuraaingalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tea-ya athuraaingalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Natpai ellam kootiyandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natpai ellam kootiyandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kummi adikkira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kummi adikkira"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee kuppai ellam paththa vechu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kuppai ellam paththa vechu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponga veikkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponga veikkura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irumbai ellam karumbu aaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irumbai ellam karumbu aaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavu kanura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavu kanura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee oththa vela sothukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee oththa vela sothukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vettu veikkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vettu veikkura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey motta madi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey motta madi"/>
</div>
<div class="lyrico-lyrics-wrapper">Antenaa-la kakka pee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antenaa-la kakka pee"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee mutti mothinalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee mutti mothinalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeraathu TRP
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeraathu TRP"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rest eduthu oothikudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rest eduthu oothikudi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sukku kappi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sukku kappi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhkaiyoda narai vayil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhkaiyoda narai vayil"/>
</div>
<div class="lyrico-lyrics-wrapper">Naama pee pee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naama pee pee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pee pee pee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pee pee pee"/>
</div>
<div class="lyrico-lyrics-wrapper">Pee pee pee pee pee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pee pee pee pee pee"/>
</div>
</pre>
