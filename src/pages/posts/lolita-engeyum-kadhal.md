---
title: "lolita song lyrics"
album: "Engeyum Kadhal"
artist: "Harris Jayaraj"
lyricist: "Thamarai"
director: "Prabhudeva"
path: "/albums/engeyum-kadhal-lyrics"
song: "Lolita"
image: ../../images/albumart/engeyum-kadhal.jpg
date: 2011-05-06
lang: tamil
youtubeLink: "https://www.youtube.com/embed/NM600z8McII"
type: "love"
singers:
  - Karthik
  - Prashanthini
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Lolita Hello Lolita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lolita Hello Lolita"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Dhooram Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Dhooram Kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakkam Aaga Maaruthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkam Aaga Maaruthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pon Manjal Manjal Penne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pon Manjal Manjal Penne"/>
</div>
<div class="lyrico-lyrics-wrapper">Enge Selgiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enge Selgiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Minnanjal Pole Vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minnanjal Pole Vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sendru Kolgiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sendru Kolgiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Vegam Kaatti Pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vegam Kaatti Pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pothu Noguthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pothu Noguthey"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Dhooram Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Dhooram Kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakkamaaga Maaruthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkamaaga Maaruthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Loita Hello Lolita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Loita Hello Lolita"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Karai Illaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Karai Illaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangal Neetti Thalluthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangal Neetti Thalluthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unmaiyai Naan Sollattaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmaiyai Naan Sollattaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Mulaam Poosaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Mulaam Poosaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Pechchil Ellaam Ullathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pechchil Ellaam Ullathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pon Manjal Manjal Penne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pon Manjal Manjal Penne"/>
</div>
<div class="lyrico-lyrics-wrapper">Enge Selgiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enge Selgiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Minnanjal Pole Vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minnanjal Pole Vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sendru Kolgiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sendru Kolgiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Vegam Kaatti Pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vegam Kaatti Pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pothu Noguthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pothu Noguthey"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Dhooram Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Dhooram Kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakkamaaga Maaruthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkamaaga Maaruthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Loita Hello Lolita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Loita Hello Lolita"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Karai Illaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Karai Illaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangal Neetti Thalluthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangal Neetti Thalluthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unmaiyai Sollattaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmaiyai Sollattaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Mulaam Poosaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Mulaam Poosaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Pechchil Ellaam Ullathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pechchil Ellaam Ullathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kottum Bodhey Mazhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottum Bodhey Mazhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kotta Vittaal Pizhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kotta Vittaal Pizhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaisai Vaanam Maathi Paarkirai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaisai Vaanam Maathi Paarkirai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pengal Ellaam Chedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pengal Ellaam Chedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Patri Kollum Kodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Patri Kollum Kodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Endre Thappu Thappaai Solgiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endre Thappu Thappaai Solgiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Naal Pakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Naal Pakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">Neer Soozhntha Theevallavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neer Soozhntha Theevallavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar Vanthaalum Saaigindra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar Vanthaalum Saaigindra"/>
</div>
<div class="lyrico-lyrics-wrapper">Ther Allavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ther Allavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Alai Noorai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Alai Noorai"/>
</div>
<div class="lyrico-lyrics-wrapper">Adai Kaakkum Kadal Allavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adai Kaakkum Kadal Allavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">En Aagaayam Athil Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Aagaayam Athil Kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Pala Vennilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala Vennilaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pon Manjal Manjal Penne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pon Manjal Manjal Penne"/>
</div>
<div class="lyrico-lyrics-wrapper">Enge Selgiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enge Selgiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Minnanjal Pole Vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minnanjal Pole Vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sendru Kolgiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sendru Kolgiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Vegam Kaatti Pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vegam Kaatti Pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pothu Noguthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pothu Noguthey"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Dhooram Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Dhooram Kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakkamaaga Maaruthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkamaaga Maaruthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Loita Hello Lolita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Loita Hello Lolita"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Karai Illaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Karai Illaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangal Neetti Thalluthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangal Neetti Thalluthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unmaiyai Naan Sollattaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmaiyai Naan Sollattaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Mulaam Poosaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Mulaam Poosaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Pechchil Ellaam Ullathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pechchil Ellaam Ullathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaanaai Vanthaal Rusi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanaai Vanthaal Rusi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalli Sendraal Rasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalli Sendraal Rasi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennum Vaazhkai Inbam Allavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennum Vaazhkai Inbam Allavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muththam Endraal Siri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muththam Endraal Siri"/>
</div>
<div class="lyrico-lyrics-wrapper">Katti Kondaal Neri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katti Kondaal Neri"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanni Moodi Kondu Killavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanni Moodi Kondu Killavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Sollum Pala Nooril
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Sollum Pala Nooril"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Illaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Illaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Azhagaana Pala Poovil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Azhagaana Pala Poovil"/>
</div>
<div class="lyrico-lyrics-wrapper">Then Illaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Then Illaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Vellaththil Naan Ondrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Vellaththil Naan Ondrum"/>
</div>
<div class="lyrico-lyrics-wrapper">Purambillaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Purambillaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Rusi Paarkka Thalai Thaalthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Rusi Paarkka Thalai Thaalthum"/>
</div>
<div class="lyrico-lyrics-wrapper">Varambillaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varambillaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Loita Hello Lolita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Loita Hello Lolita"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Karai Illaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Karai Illaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangal Neetti Thalluthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangal Neetti Thalluthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unmaiyai Naan Sollattaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmaiyai Naan Sollattaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Mulaam Poosaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Mulaam Poosaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Pechchil Ellaam Ullathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pechchil Ellaam Ullathey"/>
</div>
</pre>
