---
title: "nee naa prathikshanam song lyrics"
album: "Pranavum"
artist: "Padmanav Bharadwaj"
lyricist: "Chirravuri Vijay Kumar"
director: "Kumar.G"
path: "/albums/pranavum-lyrics"
song: "Nee Naa Prathikshanam"
image: ../../images/albumart/pranavum.jpg
date: 2021-02-05
lang: telugu
youtubeLink: "https://www.youtube.com/embed/-EqU9dgDNSg"
type: "love"
singers:
  - Anurag Kulkarni
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nee Naa Prathikshanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Naa Prathikshanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Preme Prapanchamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Preme Prapanchamai"/>
</div>
<div class="lyrico-lyrics-wrapper">Sagina Jeevitham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sagina Jeevitham"/>
</div>
<div class="lyrico-lyrics-wrapper">Aagina Pranamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagina Pranamai"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam Kane Prathi Kala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam Kane Prathi Kala"/>
</div>
<div class="lyrico-lyrics-wrapper">Nijam Ayye Kshanalalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijam Ayye Kshanalalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninge Ralentha Laaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninge Ralentha Laaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nele Koolenthalaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nele Koolenthalaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Naa Prathikshanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Naa Prathikshanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Preme Prapanchamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Preme Prapanchamai"/>
</div>
<div class="lyrico-lyrics-wrapper">Sagina Jeevitham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sagina Jeevitham"/>
</div>
<div class="lyrico-lyrics-wrapper">Aagina Pranamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagina Pranamai"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeve Sarvamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeve Sarvamai"/>
</div>
<div class="lyrico-lyrics-wrapper">Unde Nammakam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unde Nammakam"/>
</div>
<div class="lyrico-lyrics-wrapper">Mande Ivalaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mande Ivalaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gundelo Bhookampamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundelo Bhookampamai"/>
</div>
<div class="lyrico-lyrics-wrapper">Undi Leka Ooge Oopirai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undi Leka Ooge Oopirai"/>
</div>
<div class="lyrico-lyrics-wrapper">Sneham Drohamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sneham Drohamai"/>
</div>
<div class="lyrico-lyrics-wrapper">Sokham Panchenaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sokham Panchenaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sneham Drohamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sneham Drohamai"/>
</div>
<div class="lyrico-lyrics-wrapper">Sokham Panchenaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sokham Panchenaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Varinchina Vasanthame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varinchina Vasanthame"/>
</div>
<div class="lyrico-lyrics-wrapper">Dahimchene Smashanamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dahimchene Smashanamai"/>
</div>
<div class="lyrico-lyrics-wrapper">Athuku Padani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athuku Padani"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana Brathuku Lo Soonyamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana Brathuku Lo Soonyamai"/>
</div>
<div class="lyrico-lyrics-wrapper">Maate Pedavilo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maate Pedavilo"/>
</div>
<div class="lyrico-lyrics-wrapper">Maate Mounamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maate Mounamai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aase Nishidhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aase Nishidhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Baate Oo Yedarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baate Oo Yedarai"/>
</div>
<div class="lyrico-lyrics-wrapper">Needai Modai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Needai Modai"/>
</div>
<div class="lyrico-lyrics-wrapper">Keedai Maareney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Keedai Maareney"/>
</div>
<div class="lyrico-lyrics-wrapper">Lopam Evaridho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lopam Evaridho"/>
</div>
<div class="lyrico-lyrics-wrapper">Theledhelaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theledhelaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Lopam Evaridho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lopam Evaridho"/>
</div>
<div class="lyrico-lyrics-wrapper">Theledhelaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theledhelaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Usurane Neevedhane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usurane Neevedhane"/>
</div>
<div class="lyrico-lyrics-wrapper">Visham Chimme Avedhaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Visham Chimme Avedhaney"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharumuthunte Ninu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharumuthunte Ninu"/>
</div>
<div class="lyrico-lyrics-wrapper">Marachipodam Elaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marachipodam Elaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Naa Prathikshanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Naa Prathikshanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Preme Prapanchamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Preme Prapanchamai"/>
</div>
<div class="lyrico-lyrics-wrapper">Sagina Jeevitham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sagina Jeevitham"/>
</div>
<div class="lyrico-lyrics-wrapper">Aagina Pranamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagina Pranamai"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam Kane Prathi Kala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam Kane Prathi Kala"/>
</div>
<div class="lyrico-lyrics-wrapper">Nijam Ayye Kshanalalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijam Ayye Kshanalalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninge Ralentha Laaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninge Ralentha Laaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nele Koolenthalaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nele Koolenthalaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Vedhana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Vedhana"/>
</div>
<div class="lyrico-lyrics-wrapper">Neekela Choopadam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neekela Choopadam"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Badhelaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Badhelaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Lolo Dacheyyadam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lolo Dacheyyadam"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam Kane Prathi Kala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam Kane Prathi Kala"/>
</div>
<div class="lyrico-lyrics-wrapper">Nijam Ayye Kshanalalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijam Ayye Kshanalalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninge Ralentha Laaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninge Ralentha Laaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nele Koolenthalaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nele Koolenthalaaa"/>
</div>
</pre>
