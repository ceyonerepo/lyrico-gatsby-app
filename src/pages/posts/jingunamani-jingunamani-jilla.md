---
title: "jingunamani jingunamani song lyrics"
album: "Jilla"
artist: "D. Imman"
lyricist: "Viveka"
director: "Nesan"
path: "/albums/jilla-song-lyrics"
song: "Jingunamani Jingunamani"
image: ../../images/albumart/jilla.jpg
date: 2014-01-09
lang: tamil
youtubeLink: "https://www.youtube.com/embed/E6n52bwh_ho"
type: "Celebration"
singers:
  - Sunidhi Chauhan
  - Ranjith
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Jingunamani Jingunamani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jingunamani Jingunamani"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirichuputta Nenjula Aanee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirichuputta Nenjula Aanee"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Vengala Kinni Vengala Kinni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Vengala Kinni Vengala Kinni"/>
</div>
<div class="lyrico-lyrics-wrapper">Pola Minnum Mandhira Meni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pola Minnum Mandhira Meni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Na Vekkathukku Pakkathula Pogathava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na Vekkathukku Pakkathula Pogathava"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Noole Illaa Selai Katta Aalaanava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Noole Illaa Selai Katta Aalaanava"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Nee Vandha En Bandha Gaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Nee Vandha En Bandha Gaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Aamaango Aamaango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aamaango Aamaango"/>
</div>
<div class="lyrico-lyrics-wrapper">Aamaango Aamaango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aamaango Aamaango"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Jingunamani Jingunamani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Jingunamani Jingunamani"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirichuputta Nenjula Aanee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirichuputta Nenjula Aanee"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Vengala Kinni Vengala Kinni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Vengala Kinni Vengala Kinni"/>
</div>
<div class="lyrico-lyrics-wrapper">Pola Minnum Mandhira Meni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pola Minnum Mandhira Meni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu Enna Udamba Veda Kozhi Kozhamba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Enna Udamba Veda Kozhi Kozhamba"/>
</div>
<div class="lyrico-lyrics-wrapper">Puriyama Naa Paakuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puriyama Naa Paakuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Samachathu Illa Samanjathu Thaanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samachathu Illa Samanjathu Thaanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakaaga Ela Poduren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakaaga Ela Poduren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ritcher Vachu Paartha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ritcher Vachu Paartha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Pathu Earthquake ku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Pathu Earthquake ku"/>
</div>
<div class="lyrico-lyrics-wrapper">Ottu Motham Udal Aaduthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ottu Motham Udal Aaduthey"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetti Vachu Pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetti Vachu Pona"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Water Melon Cake ku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Water Melon Cake ku"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakkam Vantha Kulir Aagume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkam Vantha Kulir Aagume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Get To Together Venum Naama Kettu Poga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Get To Together Venum Naama Kettu Poga"/>
</div>
<div class="lyrico-lyrics-wrapper">Aamaango Aamaango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aamaango Aamaango"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Jingunamani Jingunamani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Jingunamani Jingunamani"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirichuputta Nenjula Aane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirichuputta Nenjula Aane"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Vengala Kinni Vengala Kinni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Vengala Kinni Vengala Kinni"/>
</div>
<div class="lyrico-lyrics-wrapper">Pola Minnum Mandhira Meni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pola Minnum Mandhira Meni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jingunamani Jingunamani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jingunamani Jingunamani"/>
</div>
<div class="lyrico-lyrics-wrapper">Kitta Vaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kitta Vaadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jingunamani Jingunamani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jingunamani Jingunamani"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirichuputta Nenjula Aane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirichuputta Nenjula Aane"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Vengala Kinni Vengala Kinni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Vengala Kinni Vengala Kinni"/>
</div>
<div class="lyrico-lyrics-wrapper">Pola Minnum Mandhira Meni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pola Minnum Mandhira Meni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Na Vekkathukku Pakkathula Pogathava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na Vekkathukku Pakkathula Pogathava"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Noole Illaa Selai Katta Aalaanava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Noole Illaa Selai Katta Aalaanava"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Nee Vandha En Bandha Gaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Nee Vandha En Bandha Gaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Aamaango Aamaango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aamaango Aamaango"/>
</div>
<div class="lyrico-lyrics-wrapper">Aamaango Aamaango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aamaango Aamaango"/>
</div>
</pre>
