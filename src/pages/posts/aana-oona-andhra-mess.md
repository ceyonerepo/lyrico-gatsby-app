---
title: "aana oona song lyrics"
album: "Andhra Mess"
artist: "Prashant Pillai"
lyricist: "Kutty Revathi - Mohanraj - Turaz"
director: "Jai"
path: "/albums/andhra-mess-lyrics"
song: "Aana Oona"
image: ../../images/albumart/andhra-mess.jpg
date: 2018-06-22
lang: tamil
youtubeLink: "https://www.youtube.com/embed/_XE8ttpZYuE"
type: "mass"
singers:
  - unknown
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">ye aanaa oonaa thaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ye aanaa oonaa thaanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">thanthanaa thinthanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanthanaa thinthanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">ye vaanaa ponaa varumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ye vaanaa ponaa varumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">vaazhkkai saddenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaazhkkai saddenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">ettenaa paththanna sikkunaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ettenaa paththanna sikkunaa"/>
</div>
<div class="lyrico-lyrics-wrapper">aandiyum aavaan jiththanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aandiyum aavaan jiththanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">onnunnaa ovvonna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="onnunnaa ovvonna"/>
</div>
<div class="lyrico-lyrics-wrapper">ondunnaa thappennaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ondunnaa thappennaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thikkithikki paththikichche thaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thikkithikki paththikichche thaanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">yerikkulla maattikittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yerikkulla maattikittu"/>
</div>
<div class="lyrico-lyrics-wrapper">yegaantham thedikkittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yegaantham thedikkittu"/>
</div>
<div class="lyrico-lyrics-wrapper">seththukkulla vaazhuriye meenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seththukkulla vaazhuriye meenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">kadalalai karaikalai thottu - alai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadalalai karaikalai thottu - alai"/>
</div>
<div class="lyrico-lyrics-wrapper">thirumbuthu nuraigalai vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thirumbuthu nuraigalai vittu"/>
</div>
<div class="lyrico-lyrics-wrapper">kaatrin kaigal pattu - nurai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatrin kaigal pattu - nurai"/>
</div>
<div class="lyrico-lyrics-wrapper">pokuthu ulagaththai vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pokuthu ulagaththai vittu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ye thirudi ye thirudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ye thirudi ye thirudi"/>
</div>
<div class="lyrico-lyrics-wrapper">ye thirudi ye thirudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ye thirudi ye thirudi"/>
</div>
<div class="lyrico-lyrics-wrapper">nee manasa mayakkum magudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee manasa mayakkum magudi"/>
</div>
<div class="lyrico-lyrics-wrapper">idhu kaama maadum thegidi vidudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhu kaama maadum thegidi vidudi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ye thirudi ye thirudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ye thirudi ye thirudi"/>
</div>
<div class="lyrico-lyrics-wrapper">ye thirudi ye thirudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ye thirudi ye thirudi"/>
</div>
<div class="lyrico-lyrics-wrapper">nee manasa mayakkum magudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee manasa mayakkum magudi"/>
</div>
<div class="lyrico-lyrics-wrapper">idhu kaama maadum thegidi vidudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhu kaama maadum thegidi vidudi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ye kazhugaa parakudhu nenjam - ada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ye kazhugaa parakudhu nenjam - ada"/>
</div>
<div class="lyrico-lyrics-wrapper">sethari kedakkudhu vanjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sethari kedakkudhu vanjam"/>
</div>
<div class="lyrico-lyrics-wrapper">koraiyudhu kodiyil konjam - idai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koraiyudhu kodiyil konjam - idai"/>
</div>
<div class="lyrico-lyrics-wrapper">karaiyudhu idhupudhu panjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karaiyudhu idhupudhu panjam"/>
</div>
</pre>
