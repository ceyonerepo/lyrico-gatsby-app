---
title: "noolupoya song lyrics"
album: "Trance"
artist: "Jackson Vijayan - Vinayakan"
lyricist: "Vinayak Sasikumar"
director: "Anwar Rasheed"
path: "/albums/trance-lyrics"
song: "Noolupoya"
image: ../../images/albumart/trance.jpg
date: 2020-02-20
lang: malayalam
youtubeLink: "https://www.youtube.com/embed/xUwqjU283pY"
type: "happy"
singers:
  - Pradeep Kumar
  - Mohammed Maqbool Mansoor
  - Varun Sunil
  - Jackson Vijayan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Noolu poya noolu pattangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Noolu poya noolu pattangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Paariyodum vaanamaay maari njaanithaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paariyodum vaanamaay maari njaanithaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thengunnunde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thengunnunde"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaithaangillaa vaanil ekanaayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaithaangillaa vaanil ekanaayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaane theertha komarakkola kettiyaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaane theertha komarakkola kettiyaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennil ninnu thenju maanju poy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennil ninnu thenju maanju poy"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanam polum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanam polum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalavara paathi pakuthi doorathu theernne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalavara paathi pakuthi doorathu theernne"/>
</div>
<div class="lyrico-lyrics-wrapper">Njanen vaanam vidhikalinnonnay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Njanen vaanam vidhikalinnonnay"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru dinam vanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru dinam vanne"/>
</div>
</pre>
