---
title: "chitti story song lyrics"
album: "Master"
artist: "Anirudh Ravichander"
lyricist: "Anantha Sriram"
director: "Lokesh Kanakaraj"
path: "/albums/master-lyrics"
song: "Let Me Sing a Chitti Story"
image: ../../images/albumart/master.jpg
date: 2021-01-13
lang: telugu
youtubeLink: "https://www.youtube.com/embed/5WDQgBGfx40"
type: "mass"
singers:
  - Anirudh Ravichander
  - Sam Vishal
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Let me sing a chitty story
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Let me sing a chitty story"/>
</div>
<div class="lyrico-lyrics-wrapper">Pay attention listen to me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pay attention listen to me"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhana englishu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhana englishu"/>
</div>
<div class="lyrico-lyrics-wrapper">Just listen bro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Just listen bro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Let me sing a chitty story
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Let me sing a chitty story"/>
</div>
<div class="lyrico-lyrics-wrapper">Pay attention listen to me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pay attention listen to me"/>
</div>
<div class="lyrico-lyrics-wrapper">If you want take it or else
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="If you want take it or else"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaddhoy tension leave it baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaddhoy tension leave it baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Life is very shortabbaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life is very shortabbaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Always be happy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Always be happy"/>
</div>
<div class="lyrico-lyrics-wrapper">Pari pari problems will come and go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pari pari problems will come and go"/>
</div>
<div class="lyrico-lyrics-wrapper">Kochem chill maaro baapi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kochem chill maaro baapi"/>
</div>
<div class="lyrico-lyrics-wrapper">Together man
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Together man"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Let me sing a chitti story
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Let me sing a chitti story"/>
</div>
<div class="lyrico-lyrics-wrapper">Pay attention listen to me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pay attention listen to me"/>
</div>
<div class="lyrico-lyrics-wrapper">If you want take it or else
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="If you want take it or else"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaddhoy tension leave it baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaddhoy tension leave it baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Life is very shortabbaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life is very shortabbaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Always be happy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Always be happy"/>
</div>
<div class="lyrico-lyrics-wrapper">Design designo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Design designo"/>
</div>
<div class="lyrico-lyrics-wrapper">Problems will come and go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Problems will come and go"/>
</div>
<div class="lyrico-lyrics-wrapper">Konchem chill maro baapi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konchem chill maro baapi"/>
</div>
<div class="lyrico-lyrics-wrapper">No tension baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="No tension baby"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Speedga pothe gamanika must-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Speedga pothe gamanika must-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Slowga pothe steady ye best-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Slowga pothe steady ye best-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Anger always misery baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anger always misery baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Friends ye chala powerful maapi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friends ye chala powerful maapi"/>
</div>
<div class="lyrico-lyrics-wrapper">Haters are gonna hate but ignore calmly
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haters are gonna hate but ignore calmly"/>
</div>
<div class="lyrico-lyrics-wrapper">Negativity antha thanney ve baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Negativity antha thanney ve baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Focus on what you dream
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Focus on what you dream"/>
</div>
<div class="lyrico-lyrics-wrapper">You don’t worry maapi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You don’t worry maapi"/>
</div>
<div class="lyrico-lyrics-wrapper">Postivity unte lift mari baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Postivity unte lift mari baby"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Life is very shortabbaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life is very shortabbaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Always be happy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Always be happy"/>
</div>
<div class="lyrico-lyrics-wrapper">Design designo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Design designo"/>
</div>
<div class="lyrico-lyrics-wrapper">Problems will come and go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Problems will come and go"/>
</div>
<div class="lyrico-lyrics-wrapper">Konchem chill maro baapi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konchem chill maro baapi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Students, Let me sing a chitti story
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Students, Let me sing a chitti story"/>
</div>
<div class="lyrico-lyrics-wrapper">Pay attention listen to me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pay attention listen to me"/>
</div>
<div class="lyrico-lyrics-wrapper">If you want take it or else
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="If you want take it or else"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaddhoy tension leave it baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaddhoy tension leave it baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Life is very shortabbaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life is very shortabbaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Always be happy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Always be happy"/>
</div>
<div class="lyrico-lyrics-wrapper">Design design oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Design design oh"/>
</div>
<div class="lyrico-lyrics-wrapper">Problems will come and go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Problems will come and go"/>
</div>
<div class="lyrico-lyrics-wrapper">Konchem chill maro baapi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konchem chill maro baapi"/>
</div>
<div class="lyrico-lyrics-wrapper">No tension baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="No tension baby"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hardwork mukhyam smart work mukhyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hardwork mukhyam smart work mukhyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Self motivation adhi neethoney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Self motivation adhi neethoney"/>
</div>
<div class="lyrico-lyrics-wrapper">Education mukhyam dedication mukhyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Education mukhyam dedication mukhyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Self valuation adhi pakka porey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Self valuation adhi pakka porey"/>
</div>
<div class="lyrico-lyrics-wrapper">Don’t be the person spreading haterd maapi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Don’t be the person spreading haterd maapi"/>
</div>
<div class="lyrico-lyrics-wrapper">Venkaala maatalodhoerabba crappy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venkaala maatalodhoerabba crappy"/>
</div>
<div class="lyrico-lyrics-wrapper">Always be polite and just don’t be nasty
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Always be polite and just don’t be nasty"/>
</div>
<div class="lyrico-lyrics-wrapper">You will be the reason to make someone happy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You will be the reason to make someone happy"/>
</div>
<div class="lyrico-lyrics-wrapper">Life is very shortabbaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life is very shortabbaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Always be happy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Always be happy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pari pari problems will come and go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pari pari problems will come and go"/>
</div>
<div class="lyrico-lyrics-wrapper">Kochem chill maaro baapi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kochem chill maaro baapi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">One last time
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="One last time"/>
</div>
<div class="lyrico-lyrics-wrapper">Let me sing a chitti story
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Let me sing a chitti story"/>
</div>
</pre>
