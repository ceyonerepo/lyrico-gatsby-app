---
title: "peyyum nilaavu song lyrics"
album: "Maniyarayile Ashokan"
artist: "Sreehari K. Nair"
lyricist: "B. K. Hari Narayanan"
director: "Shamzu Zayba"
path: "/albums/maniyarayile-ashokan-lyrics"
song: "Peyyum Nilaavu"
image: ../../images/albumart/maniyarayile-ashokan.jpg
date: 2020-08-31
lang: malayalam
youtubeLink: "https://www.youtube.com/embed/6IqH7gTJsok"
type: "love"
singers:
  - K. S. Harisankar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Peyyum Nilavulla Raavil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peyyum Nilavulla Raavil"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaro Aaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaro Aaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Aambal Mani Poovinullil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aambal Mani Poovinullil"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanne Aaroo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanne Aaroo"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaar Meghavum Ven Thaaravum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaar Meghavum Ven Thaaravum"/>
</div>
<div class="lyrico-lyrics-wrapper">Manjum Kaatum Kaanathe Thaazhe Vanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manjum Kaatum Kaanathe Thaazhe Vanne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iru Mizhikalil Aniviralodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iru Mizhikalil Aniviralodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoovunnu Poovil Aaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoovunnu Poovil Aaro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Venal Kinavin Thooval Pozhinje
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venal Kinavin Thooval Pozhinje"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanathe Ninnil Cherunnatharo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanathe Ninnil Cherunnatharo"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoo Maarivillin Chayanagalaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoo Maarivillin Chayanagalaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullam Thalodan Kaineetti Aaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullam Thalodan Kaineetti Aaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathoram Vannoro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathoram Vannoro"/>
</div>
<div class="lyrico-lyrics-wrapper">Nimishanthin Eenangal Moolum Aaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nimishanthin Eenangal Moolum Aaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Mounam Polum Thenayae Maattum Aaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mounam Polum Thenayae Maattum Aaro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Megham Pole
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megham Pole"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhaneer Kudam Anurgam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhaneer Kudam Anurgam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thorathe Thanne Aaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thorathe Thanne Aaro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raa Theerathin Aambal Poovo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raa Theerathin Aambal Poovo"/>
</div>
<div class="lyrico-lyrics-wrapper">Maanathe Moha Thinkalodu Cherum Neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maanathe Moha Thinkalodu Cherum Neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Premathin Aadhya Sugandham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Premathin Aadhya Sugandham"/>
</div>
<div class="lyrico-lyrics-wrapper">Iravathin Mizhikalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravathin Mizhikalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivare Nokki Nilkkume Izha Muriyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivare Nokki Nilkkume Izha Muriyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaaval Pole
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaval Pole"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaro Dhoore
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaro Dhoore"/>
</div>
<div class="lyrico-lyrics-wrapper">Aathmavin Geetham Paadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aathmavin Geetham Paadum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Etho Vegam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Etho Vegam"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhaneer Kudam Anurgam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhaneer Kudam Anurgam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thorathe Peyyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thorathe Peyyum"/>
</div>
</pre>
