---
title: "o buri buggala song lyrics"
album: "Ego"
artist: "Sai Karthik "
lyricist: "RV Subramanyam"
director: "RV Subramanyam"
path: "/albums/ego-lyrics"
song: "O Buri Buggala"
image: ../../images/albumart/ego.jpg
date: 2018-01-05
lang: telugu
youtubeLink: "https://www.youtube.com/embed/kWWb7VMoxTA"
type: "mass"
singers:
  - Sai Karthik
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">o boorebuggala booremma 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="o boorebuggala booremma "/>
</div>
<div class="lyrico-lyrics-wrapper">balupekki undamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="balupekki undamma"/>
</div>
<div class="lyrico-lyrics-wrapper">adi adugesthe anthamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi adugesthe anthamu"/>
</div>
<div class="lyrico-lyrics-wrapper">nadichosthe narakamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nadichosthe narakamu"/>
</div>
<div class="lyrico-lyrics-wrapper">babu orey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="babu orey"/>
</div>
<div class="lyrico-lyrics-wrapper">daani ollantaa pogarura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="daani ollantaa pogarura"/>
</div>
<div class="lyrico-lyrics-wrapper">nene dinchestaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nene dinchestaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">rama daani jhada 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rama daani jhada "/>
</div>
<div class="lyrico-lyrics-wrapper">choodu baaredu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="choodu baaredu"/>
</div>
<div class="lyrico-lyrics-wrapper">ghede thoka oopudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ghede thoka oopudu"/>
</div>
<div class="lyrico-lyrics-wrapper">paapa meda chooste rothara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paapa meda chooste rothara"/>
</div>
<div class="lyrico-lyrics-wrapper">saara seesa moothara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saara seesa moothara"/>
</div>
<div class="lyrico-lyrics-wrapper">pilla nadumundi pikakala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pilla nadumundi pikakala"/>
</div>
<div class="lyrico-lyrics-wrapper">cheraguni o peedakala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cheraguni o peedakala"/>
</div>
<div class="lyrico-lyrics-wrapper">hey soothha daanoyyaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey soothha daanoyyaram"/>
</div>
<div class="lyrico-lyrics-wrapper">soopiththa na yavvaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="soopiththa na yavvaram"/>
</div>
<div class="lyrico-lyrics-wrapper">soothha daanoyyaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="soothha daanoyyaram"/>
</div>
<div class="lyrico-lyrics-wrapper">soopittha naa yavvaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="soopittha naa yavvaram"/>
</div>
<div class="lyrico-lyrics-wrapper">soothha daanoyyaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="soothha daanoyyaram"/>
</div>
<div class="lyrico-lyrics-wrapper">soopittha naa yavvaram 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="soopittha naa yavvaram "/>
</div>
<div class="lyrico-lyrics-wrapper">soothha daanoyyaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="soothha daanoyyaram"/>
</div>
<div class="lyrico-lyrics-wrapper">soopittha naa yavvaram 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="soopittha naa yavvaram "/>
</div>
<div class="lyrico-lyrics-wrapper">soothha daanoyyaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="soothha daanoyyaram"/>
</div>
<div class="lyrico-lyrics-wrapper">soopittha naa yavvaram 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="soopittha naa yavvaram "/>
</div>
<div class="lyrico-lyrics-wrapper">babugaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="babugaru"/>
</div>
<div class="lyrico-lyrics-wrapper">soothha daanoyyaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="soothha daanoyyaram"/>
</div>
<div class="lyrico-lyrics-wrapper">soopittha naa yavvaram 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="soopittha naa yavvaram "/>
</div>
<div class="lyrico-lyrics-wrapper">soothha daanoyyaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="soothha daanoyyaram"/>
</div>
<div class="lyrico-lyrics-wrapper">soopittha naa yavvaram 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="soopittha naa yavvaram "/>
</div>
<div class="lyrico-lyrics-wrapper">babugaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="babugaru"/>
</div>
<div class="lyrico-lyrics-wrapper">soothha daanoyyaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="soothha daanoyyaram"/>
</div>
<div class="lyrico-lyrics-wrapper">soopittha naa yavvaram 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="soopittha naa yavvaram "/>
</div>
</pre>
