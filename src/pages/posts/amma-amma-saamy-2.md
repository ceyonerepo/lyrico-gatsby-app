---
title: "amma amma song lyrics"
album: "Saamy 2"
artist: "Devi Sri Prasad"
lyricist: "Viveka"
director: "Hari"
path: "/albums/saamy-2-song-lyrics"
song: "Amma Amma"
image: ../../images/albumart/saamy-2.jpg
date: 2018-09-21
lang: tamil
youtubeLink: "https://www.youtube.com/embed/88MxX6rqfOk"
type: "Sentiment"
singers:
  - Karthik
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Amma Amma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma Amma"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Vida Deivamillai Manmeley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Vida Deivamillai Manmeley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaaye Thaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaye Thaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulla Sondham Ellam Unakku Pinnaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulla Sondham Ellam Unakku Pinnaley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Undhan Anbu Mattum Pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan Anbu Mattum Pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaana Romba Chinnadhaakum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaana Romba Chinnadhaakum "/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhkai Engum Vannamaakum Amma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhkai Engum Vannamaakum Amma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kọdikalai Kuvithaalum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kọdikalai Kuvithaalum "/>
</div>
<div class="lyrico-lyrics-wrapper">Kọttaikalai Pidithaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kọttaikalai Pidithaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaaye Undhan Madi Pọle Varumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaye Undhan Madi Pọle Varumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mazhai Neerin Thuli Pọle 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai Neerin Thuli Pọle "/>
</div>
<div class="lyrico-lyrics-wrapper">Mega Thuymai Thaaymai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mega Thuymai Thaaymai "/>
</div>
<div class="lyrico-lyrics-wrapper">Amma Amma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma Amma"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Vida Deivamillai Manmeley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Vida Deivamillai Manmeley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaaye Thaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaye Thaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulla Sọndham Ellam Unakku Pinnaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulla Sọndham Ellam Unakku Pinnaley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Amma Amma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma Amma"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Endhan Annaiyaga Kandene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Endhan Annaiyaga Kandene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaaye Thaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaye Thaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeyidathil Unnai Thinna Thandhene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeyidathil Unnai Thinna Thandhene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paththi Erinja Un Dhegam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paththi Erinja Un Dhegam"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinna Usurukkum Nọvum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna Usurukkum Nọvum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaththi Azhamudiyaama Thavisen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaththi Azhamudiyaama Thavisen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unathuyir Pirinjalum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unathuyir Pirinjalum "/>
</div>
<div class="lyrico-lyrics-wrapper">Enathuyir Kọduthaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enathuyir Kọduthaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenaikayil Kọlamachi Maname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenaikayil Kọlamachi Maname"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unai Meendum Thulaidhene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai Meendum Thulaidhene"/>
</div>
<div class="lyrico-lyrics-wrapper">Perum Sọgam Thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perum Sọgam Thaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Amma Amma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma Amma"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Vida Deivamillai Manmeley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Vida Deivamillai Manmeley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaaye Thaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaye Thaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulla Sọndham Ellam Unakku Pinnaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulla Sọndham Ellam Unakku Pinnaley"/>
</div>
</pre>
