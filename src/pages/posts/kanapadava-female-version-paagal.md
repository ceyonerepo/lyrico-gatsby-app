---
title: "kanapadava female version song lyrics"
album: "Paagal"
artist: "Leon James"
lyricist: "Prasanna Kumar Bezawada"
director: "Naressh Kuppili"
path: "/albums/paagal-lyrics"
song: "Kanapadava Female Version"
image: ../../images/albumart/paagal.jpg
date: 2021-08-14
lang: telugu
youtubeLink: "https://www.youtube.com/embed/OOtuA5V6Py8"
type: "sad"
singers:
  - Sameera Bharadwaj
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Velipothondhi Praaname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velipothondhi Praaname"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanabaduthondhi Shoonyame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanabaduthondhi Shoonyame"/>
</div>
<div class="lyrico-lyrics-wrapper">Vadhileluthondhi Gaayame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadhileluthondhi Gaayame"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanneeti Gnaapakame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneeti Gnaapakame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Velivesindhi Kaalame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velivesindhi Kaalame"/>
</div>
<div class="lyrico-lyrics-wrapper">Uri Teesindhi Premane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uri Teesindhi Premane"/>
</div>
<div class="lyrico-lyrics-wrapper">Usiresindhi Mouname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usiresindhi Mouname"/>
</div>
<div class="lyrico-lyrics-wrapper">Ontarinai Migilaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ontarinai Migilaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanapadavaa Kanabadavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanapadavaa Kanabadavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanneerai Migiledathaavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneerai Migiledathaavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Chirunnavai edhurochi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chirunnavai edhurochi"/>
</div>
<div class="lyrico-lyrics-wrapper">Chithiloke Nedathavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chithiloke Nedathavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanapadavaa Kanabadavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanapadavaa Kanabadavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Shidhilam Chesi Pothaavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shidhilam Chesi Pothaavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gundenu Kose Katha Nuvva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundenu Kose Katha Nuvva"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadadhaaka Vasthaavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadadhaaka Vasthaavaa"/>
</div>
</pre>
