---
title: "dhooram song lyrics"
album: "Adithya Varma"
artist: "Radhan"
lyricist: "Viveka"
director: "Gireesaaya"
path: "/albums/adithya-varma-lyrics"
song: "Dhooram"
image: ../../images/albumart/adithya-varma.jpg
date: 2019-11-22
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Nm1daqC9B0A"
type: "love"
singers:
  - Dhvani Bhanushali
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Dhooram Andraadam Solluthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhooram Andraadam Solluthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Eeram Kannooram Minnudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eeram Kannooram Minnudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum Vaazhum Bhoomi Meedhilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Vaazhum Bhoomi Meedhilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum Vaazhndhaal Podhum Kaadhalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum Vaazhndhaal Podhum Kaadhalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oyaamal Unnai Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oyaamal Unnai Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullam Thaedi Porada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullam Thaedi Porada"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarthaena Maattena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarthaena Maattena"/>
</div>
<div class="lyrico-lyrics-wrapper">Paavai Nenjam Thindaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paavai Nenjam Thindaada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa Vaa Vaa En Uyirae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Vaa Vaa En Uyirae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa Vaa Vaa En Uravae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Vaa Vaa En Uravae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa Vaa Vaa En Ulagae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Vaa Vaa En Ulagae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Naan Naan Unadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Naan Naan Unadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veesum Kaattrellam Un Manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veesum Kaattrellam Un Manam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanum Vazhi Engum Un Thadam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanum Vazhi Engum Un Thadam"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhooram Dhooram Vaazhum Nodigalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhooram Dhooram Vaazhum Nodigalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarai Pola Baaram Aagudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarai Pola Baaram Aagudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Illai Endraalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Illai Endraalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum Kooda Ingillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum Kooda Ingillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaan Megam Theendatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaan Megam Theendatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilavin Naatkal En Thollai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilavin Naatkal En Thollai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa Vaa Vaa En Uyirae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Vaa Vaa En Uyirae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa Vaa Vaa En Uravae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Vaa Vaa En Uravae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa Vaa Vaa En Ulagae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Vaa Vaa En Ulagae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Naan Naan Unadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Naan Naan Unadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa Vaa Vaaen Uyirae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Vaa Vaaen Uyirae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa Vaa Vaa En Uravae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Vaa Vaa En Uravae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa Vaa Vaa En Ulagae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Vaa Vaa En Ulagae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Naan Naan Unadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Naan Naan Unadhae"/>
</div>
</pre>
