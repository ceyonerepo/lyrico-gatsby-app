---
title: 'kanda vara sollunga song lyrics'
album: 'Karnan'
artist: 'Santhosh Narayanan'
lyricist: 'Mari Selvaraj'
director: 'Mari Selvaraj'
path: '/albums/karnan-song-lyrics'
song: 'Kanda Vara Sollunga'
image: ../../images/albumart/karnan.jpg
date: 2021-04-09
lang: tamil
youtubeLink: "https://www.youtube.com/embed/EVfw6pXVN5c"
type: 'sad'
singers: 
- Kidakkuzhi Mariyammal
- Santhosh Narayanan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">

<div class="lyrico-lyrics-wrapper">Suriyanum Pekkavilla Santhiranum Satchiyilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suriyanum Pekkavilla Santhiranum Satchiyilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Suriyanum Pekkavilla Santhiranum Satchiyilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suriyanum Pekkavilla Santhiranum Satchiyilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Pathakathi Petha Pulla Panjam Thinnu Valatha Pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathakathi Petha Pulla Panjam Thinnu Valatha Pulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanda Vara Sollunga Karnana Kaiyoda Koottivarunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanda Vara Sollunga Karnana Kaiyoda Koottivarunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Avana Kanda Vara Sollunga, Karnana Kaiyoda Koottivarunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avana Kanda Vara Sollunga, Karnana Kaiyoda Koottivarunga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanda Vara Sollunga Kaiyoda Koottivarunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanda Vara Sollunga Kaiyoda Koottivarunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ammadi Alamaran Marathumela Uchikila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammadi Alamaran Marathumela Uchikila"/>
</div>
<div class="lyrico-lyrics-wrapper">Ammadi Alamaran Marathumela Uchikila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammadi Alamaran Marathumela Uchikila"/>
</div>
<div class="lyrico-lyrics-wrapper">Othakili Ninna Kooda Kathum Paru Avan Pera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Othakili Ninna Kooda Kathum Paru Avan Pera"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanda Vara Sollunga Karnana Kaiyoda Koottivarunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanda Vara Sollunga Karnana Kaiyoda Koottivarunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Avana Kanda Vara Sollunga, Karnana Kaiyoda Koottivarunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avana Kanda Vara Sollunga, Karnana Kaiyoda Koottivarunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanda Vara Sollunga Kaiyoda Koottivarunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanda Vara Sollunga Kaiyoda Koottivarunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanda Vara Sollunga Kaiyoda Koottivarunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanda Vara Sollunga Kaiyoda Koottivarunga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oorella Koyillappa Koyilellam Samiyappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorella Koyillappa Koyilellam Samiyappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorella Koyillappa Koyilellam Samiyappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorella Koyillappa Koyilellam Samiyappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Otha Poodakooda Illauyappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Otha Poodakooda Illauyappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yenga Kudumbathula Oruthunappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenga Kudumbathula Oruthunappa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanda Vara Sollunga Karnana Kaiyoda Koottivarunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanda Vara Sollunga Karnana Kaiyoda Koottivarunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Avana Kanda Vara Sollunga Karnana Kaiyoda Koottivarunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avana Kanda Vara Sollunga Karnana Kaiyoda Koottivarunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanda Vara Sollunga Kaiyoda Koottivarunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanda Vara Sollunga Kaiyoda Koottivarunga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kavasathaiyum Kandathilla Yentha Kundalumum Koodayilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavasathaiyum Kandathilla Yentha Kundalumum Koodayilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Val Thookki Ninnan Paru Vanthu Sandappotta Yevanumillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Val Thookki Ninnan Paru Vanthu Sandappotta Yevanumillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Val Thookki Ninnan Paru Vanthu Sandappotta Yevanumillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Val Thookki Ninnan Paru Vanthu Sandappotta Yevanumillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanda Vara Sollunga Karnana Kaiyoda Koottivarunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanda Vara Sollunga Karnana Kaiyoda Koottivarunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Avana Kanda Vara Sollunga Karnana Kaiyoda Koottivarunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avana Kanda Vara Sollunga Karnana Kaiyoda Koottivarunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanda Vara Sollunga Kaiyoda Koottivarunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanda Vara Sollunga Kaiyoda Koottivarunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanda Vara Sollunga Kaiyoda Koottivarunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanda Vara Sollunga Kaiyoda Koottivarunga"/>
</div>
</pre>