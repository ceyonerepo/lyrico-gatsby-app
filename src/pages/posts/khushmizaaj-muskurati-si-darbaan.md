---
title: "khushmizaaj - muskurati si song lyrics"
album: "Darbaan"
artist: "Amartya Bobo Rahut"
lyricist: "Manoj Yadav"
director: "Bipin Nadkarni"
path: "/albums/darbaan-lyrics"
song: "Khushmizaaj - Muskurati Si"
image: ../../images/albumart/darbaan.jpg
date: 2020-12-04
lang: hindi
youtubeLink: "https://www.youtube.com/embed/aiPBhlRX5lc"
type: "affection"
singers:
  - Arijit Singh
  - Amartya Bobo Rahut
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Muskurati si hansi mili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muskurati si hansi mili"/>
</div>
<div class="lyrico-lyrics-wrapper">Khwabon ke ek mod pe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khwabon ke ek mod pe"/>
</div>
<div class="lyrico-lyrics-wrapper">Dil to hai athanni pyaar ki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil to hai athanni pyaar ki"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhak le hatheli moond ke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhak le hatheli moond ke"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aapas mein rakh lenge baant ke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aapas mein rakh lenge baant ke"/>
</div>
<div class="lyrico-lyrics-wrapper">Zindagi ke bulbule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zindagi ke bulbule"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jo mila hai tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jo mila hai tu"/>
</div>
<div class="lyrico-lyrics-wrapper">Zindagi khush mizaaj hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zindagi khush mizaaj hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pyaar hai jo mila hai tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pyaar hai jo mila hai tu"/>
</div>
<div class="lyrico-lyrics-wrapper">Har khushi khush mizaaj hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Har khushi khush mizaaj hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Baadlon ki bastiyon mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baadlon ki bastiyon mein"/>
</div>
<div class="lyrico-lyrics-wrapper">Ek gharaunda dhoondhein chalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ek gharaunda dhoondhein chalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasra ho umra bhar ka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasra ho umra bhar ka"/>
</div>
<div class="lyrico-lyrics-wrapper">Khwabdaani bhar lein chalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khwabdaani bhar lein chalo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kagazon pe aao mann ke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kagazon pe aao mann ke"/>
</div>
<div class="lyrico-lyrics-wrapper">Lafz meethe gholein koyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lafz meethe gholein koyi"/>
</div>
<div class="lyrico-lyrics-wrapper">Do dilon ki ek kahaani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Do dilon ki ek kahaani"/>
</div>
<div class="lyrico-lyrics-wrapper">Lamha lamha jodein koyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lamha lamha jodein koyi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aapas mein rakh lenge baant ke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aapas mein rakh lenge baant ke"/>
</div>
<div class="lyrico-lyrics-wrapper">Zindagi ke bulbule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zindagi ke bulbule"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jo mila hai tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jo mila hai tu"/>
</div>
<div class="lyrico-lyrics-wrapper">Zindagi khush mizaaj hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zindagi khush mizaaj hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pyar hai jo mila hai tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pyar hai jo mila hai tu"/>
</div>
<div class="lyrico-lyrics-wrapper">Har khushi khush mizaaj hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Har khushi khush mizaaj hai"/>
</div>
</pre>
