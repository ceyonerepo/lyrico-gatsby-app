---
title: "kannungala chellangala song lyrics"
album: "Nenjam Marappathilai"
artist: "Yuvan Shankar Raja"
lyricist: "Selvaraghavan"
director: "Selvaraghavan"
path: "/albums/nenjam-marappathilai-song-lyrics"
song: "Kannungala Chellangala"
image: ../../images/albumart/nenjam-marappathilai.jpg
date: 2021-03-05
lang: tamil
youtubeLink: "https://www.youtube.com/embed/BHgEYNc-418"
type: "Entertaining"
singers:
  - Yuvan Shankar Raja
---

<pre class="lyrics-native">
</pre>
<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kannungala En Kannungala,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannungala En Kannungala,"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennamma Police-ta,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennamma Police-ta,"/>
</div>
<div class="lyrico-lyrics-wrapper">Poi Sonneenga Da (x2),
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poi Sonneenga Da (x2),"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakkuthaan Juram,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkuthaan Juram,"/>
</div>
<div class="lyrico-lyrics-wrapper">Ha Ha Ha Ha Juram,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ha Ha Ha Ha Juram,"/>
</div>
<div class="lyrico-lyrics-wrapper">Seththa Naai Mela Thaan,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seththa Naai Mela Thaan,"/>
</div>
<div class="lyrico-lyrics-wrapper">Eththana Lorry Thaan.
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eththana Lorry Thaan."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iyya Vandhutaarunga,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iyya Vandhutaarunga,"/>
</div>
<div class="lyrico-lyrics-wrapper">Coffee Podanumunga,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Coffee Podanumunga,"/>
</div>
<div class="lyrico-lyrics-wrapper">Iyya Vandhutaarunga,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iyya Vandhutaarunga,"/>
</div>
<div class="lyrico-lyrics-wrapper">Gate-ah Thorakkanumunga,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gate-ah Thorakkanumunga,"/>
</div>
<div class="lyrico-lyrics-wrapper">Sathiyam.
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sathiyam."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thandhai Oruvan,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandhai Oruvan,"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha Iraivan,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Iraivan,"/>
</div>
<div class="lyrico-lyrics-wrapper">Avanum Annai,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avanum Annai,"/>
</div>
<div class="lyrico-lyrics-wrapper">Illadhavan.
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illadhavan."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thannai Thedi,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannai Thedi,"/>
</div>
<div class="lyrico-lyrics-wrapper">Eangum Pillai,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eangum Pillai,"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannil Urakkam,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannil Urakkam,"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolvaanavan.
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolvaanavan."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poovum Ponnum,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovum Ponnum,"/>
</div>
<div class="lyrico-lyrics-wrapper">Porundhi Vazhum,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porundhi Vazhum,"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhalai Keten,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhalai Keten,"/>
</div>
<div class="lyrico-lyrics-wrapper">Thandhaanavan.
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandhaanavan."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naalai Ulagil,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalai Ulagil,"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum Naanum,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Naanum,"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhum Vazhigal,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhum Vazhigal,"/>
</div>
<div class="lyrico-lyrics-wrapper">Seivaanavan.
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seivaanavan."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Ponmanigal,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Ponmanigal,"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Thoongavillai (x2).
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Thoongavillai (x2)."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannungala En Kannungala,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannungala En Kannungala,"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennamma Police-ta,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennamma Police-ta,"/>
</div>
<div class="lyrico-lyrics-wrapper">Poi Sonneenga Da (x2).
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poi Sonneenga Da (x2)."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakkuthaan Juram,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkuthaan Juram,"/>
</div>
<div class="lyrico-lyrics-wrapper">Ha Ha Ha Ha Juram,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ha Ha Ha Ha Juram,"/>
</div>
<div class="lyrico-lyrics-wrapper">Seththa Naai Mela Thaan,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seththa Naai Mela Thaan,"/>
</div>
<div class="lyrico-lyrics-wrapper">Eththana Lorry Thaan.
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eththana Lorry Thaan."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iyya Vandhutaarunga,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iyya Vandhutaarunga,"/>
</div>
<div class="lyrico-lyrics-wrapper">Coffee Podanumunga,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Coffee Podanumunga,"/>
</div>
<div class="lyrico-lyrics-wrapper">Iyya Vandhutaarunga,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iyya Vandhutaarunga,"/>
</div>
<div class="lyrico-lyrics-wrapper">Gate-ah Thorakkanumunga,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gate-ah Thorakkanumunga,"/>
</div>
<div class="lyrico-lyrics-wrapper">Sathiyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sathiyam"/>
</div>
</pre>
