---
title: "badhule leni song lyrics"
album: "3 Monkeys"
artist: "Anil kumar G"
lyricist: "ShreeMani"
director: "Anil Kumar G"
path: "/albums/3-monkeys-lyrics"
song: "Badhule Leni"
image: ../../images/albumart/3-monkeys.jpg
date: 2020-02-07
lang: telugu
youtubeLink: "https://www.youtube.com/embed/NGlll9z59YU"
type: "sad"
singers:
  - Hari Charan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Badhule Leeni Ee Prashnemito
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Badhule Leeni Ee Prashnemito"/>
</div>
<div class="lyrico-lyrics-wrapper">Dorike Varaku Aa Badhulemito
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dorike Varaku Aa Badhulemito"/>
</div>
<div class="lyrico-lyrics-wrapper">Navvuthu Egiree Guvvale Moodanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Navvuthu Egiree Guvvale Moodanta"/>
</div>
<div class="lyrico-lyrics-wrapper">Rekkale Virigee Raalukunnayanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rekkale Virigee Raalukunnayanta"/>
</div>
<div class="lyrico-lyrics-wrapper">VIdhi Visirindhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="VIdhi Visirindhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Valanee Pannindhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valanee Pannindhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalachedhirindha Ee kalalakee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalachedhirindha Ee kalalakee"/>
</div>
<div class="lyrico-lyrics-wrapper">Idi Kaade kadha Brathukantha Vyadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idi Kaade kadha Brathukantha Vyadhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ammala Maaravu Sayame Chesavoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammala Maaravu Sayame Chesavoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nyaayame Annaru Thodugaa Vunnaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nyaayame Annaru Thodugaa Vunnaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Kashtame Needhaina Ishtamai Vaccharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kashtame Needhaina Ishtamai Vaccharu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chelimikee Chavosthee Chivarikem Chesthavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chelimikee Chavosthee Chivarikem Chesthavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ika selavantoo Kashtam veluthundaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ika selavantoo Kashtam veluthundaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mari Edirinchi Prayaninchavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mari Edirinchi Prayaninchavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Brathukivvalani Brathukodilesthavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Brathukivvalani Brathukodilesthavaa"/>
</div>
</pre>
