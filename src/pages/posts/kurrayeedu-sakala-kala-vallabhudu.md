---
title: "kurrayeedu song lyrics"
album: "Sakala Kala Vallabhudu"
artist: "Ajay Patnaik"
lyricist: "Giridhar Naidu"
director: "Shiva Ganesh"
path: "/albums/sakala-kala-vallabhudu-lyrics"
song: "Kurrayeedu"
image: ../../images/albumart/sakala-kala-vallabhudu.jpg
date: 2019-02-01
lang: telugu
youtubeLink: "https://www.youtube.com/embed/_DnOstK6sS4"
type: "happy"
singers:
  - Sricharan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kuttayedu Colouru Etti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuttayedu Colouru Etti"/>
</div>
<div class="lyrico-lyrics-wrapper">Seeti Kotti Aaderoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seeti Kotti Aaderoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Janalantha Gole Petti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Janalantha Gole Petti"/>
</div>
<div class="lyrico-lyrics-wrapper">Jajja Nakaa Dummeroo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jajja Nakaa Dummeroo"/>
</div>
<div class="lyrico-lyrics-wrapper">Lungi Etti Nikkaru Katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lungi Etti Nikkaru Katti"/>
</div>
<div class="lyrico-lyrics-wrapper">Maassu Beat Haearoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maassu Beat Haearoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Zeedi Ginjal Patti Ginjal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zeedi Ginjal Patti Ginjal"/>
</div>
<div class="lyrico-lyrics-wrapper">Dj Mixulo Oogeyroo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dj Mixulo Oogeyroo"/>
</div>
<div class="lyrico-lyrics-wrapper">Korra mesaam Melevesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Korra mesaam Melevesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Neelo Dookudu Chooperoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelo Dookudu Chooperoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhanavesam Runaavesam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhanavesam Runaavesam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaatam Needero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaatam Needero"/>
</div>
<div class="lyrico-lyrics-wrapper">Neelo Plassu Naalo Minuss
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelo Plassu Naalo Minuss"/>
</div>
<div class="lyrico-lyrics-wrapper">Gundel Dhadel Beteroo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundel Dhadel Beteroo"/>
</div>
<div class="lyrico-lyrics-wrapper">Touch Eh Chesthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Touch Eh Chesthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Shocke Putti Sahpel Sahakeroo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shocke Putti Sahpel Sahakeroo"/>
</div>
<div class="lyrico-lyrics-wrapper">Janaa Jajjanaka Jajjanaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Janaa Jajjanaka Jajjanaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Janaku Jana Janaku Jana Janaku Jana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Janaku Jana Janaku Jana Janaku Jana"/>
</div>
<div class="lyrico-lyrics-wrapper">Chicchu Buddi Satta Choopaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chicchu Buddi Satta Choopaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Janaa Jajjanaka Jajjanaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Janaa Jajjanaka Jajjanaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Janaku Jana Janaku Jana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Janaku Jana Janaku Jana"/>
</div>
<div class="lyrico-lyrics-wrapper">Local Kaakul Koothal Pettaalee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Local Kaakul Koothal Pettaalee"/>
</div>
<div class="lyrico-lyrics-wrapper">Sakala Kalaa Vallabhudu Neevenayyoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sakala Kalaa Vallabhudu Neevenayyoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Adunu Chesi Naamametti Govindayyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adunu Chesi Naamametti Govindayyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Sakala Kalaa Vallabhudu Neevenayyoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sakala Kalaa Vallabhudu Neevenayyoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Eekanesi Kondalaagu Maa Krishnayyoo…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eekanesi Kondalaagu Maa Krishnayyoo…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sakala Kalaa Sakala Kalaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sakala Kalaa Sakala Kalaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Sakala Kalaa Vallabhudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sakala Kalaa Vallabhudu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sakala Kalaa Sakala Kalaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sakala Kalaa Sakala Kalaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Sakala Kalaa Vallabhudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sakala Kalaa Vallabhudu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sakala Kalaa Sakala Kalaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sakala Kalaa Sakala Kalaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Sakala Kalaa Vallabhudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sakala Kalaa Vallabhudu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sakala Kalaa Sakala Kalaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sakala Kalaa Sakala Kalaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Sakala Kalaa Vallabhudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sakala Kalaa Vallabhudu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Erra Chokka Erra Kalla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Erra Chokka Erra Kalla"/>
</div>
<div class="lyrico-lyrics-wrapper">Jodu Pette C Class Ollam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jodu Pette C Class Ollam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nammithe Pranam Pettey Yadam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nammithe Pranam Pettey Yadam"/>
</div>
<div class="lyrico-lyrics-wrapper">Maalo Gratitude
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maalo Gratitude"/>
</div>
<div class="lyrico-lyrics-wrapper">Maasina Gaddam Chimpiri Juttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maasina Gaddam Chimpiri Juttu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinigina Paintuu Pettina Perlam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinigina Paintuu Pettina Perlam"/>
</div>
<div class="lyrico-lyrics-wrapper">Local Branduku Model Gaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Local Branduku Model Gaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Maalo Atitude
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maalo Atitude"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarraa Sarraa ryzing Ichhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarraa Sarraa ryzing Ichhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Reyyi Reyyina Bikela picchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Reyyi Reyyina Bikela picchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naraa Naram Netturu Ponge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naraa Naram Netturu Ponge"/>
</div>
<div class="lyrico-lyrics-wrapper">Enjoy Energyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enjoy Energyy"/>
</div>
<div class="lyrico-lyrics-wrapper">Rangula Melam Ravikalu Katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangula Melam Ravikalu Katti"/>
</div>
<div class="lyrico-lyrics-wrapper">Naagini Danceulu Palteelu Kotti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naagini Danceulu Palteelu Kotti"/>
</div>
<div class="lyrico-lyrics-wrapper">Picchi Pichchigaa Chindulu Veseyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Picchi Pichchigaa Chindulu Veseyy"/>
</div>
<div class="lyrico-lyrics-wrapper">Yavvaname Micrechee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yavvaname Micrechee"/>
</div>
<div class="lyrico-lyrics-wrapper">Andani Aasaku Mem Vellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andani Aasaku Mem Vellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnadi Chaalani Padi Untam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnadi Chaalani Padi Untam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ningiki Medalu Mem Kattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ningiki Medalu Mem Kattam"/>
</div>
<div class="lyrico-lyrics-wrapper">Bindass Untam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bindass Untam"/>
</div>
<div class="lyrico-lyrics-wrapper">Takkulu Tieulu Mem Veyyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Takkulu Tieulu Mem Veyyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thokkala Jobulu Mem Cheyyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thokkala Jobulu Mem Cheyyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nacchina Teeruga Teeriggaa Pani Chesthuntam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nacchina Teeruga Teeriggaa Pani Chesthuntam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sakala Kalaa Vallabhudu Neevenayyoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sakala Kalaa Vallabhudu Neevenayyoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Agarubetti Doopamesi Diste Toyyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Agarubetti Doopamesi Diste Toyyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Sakala Kalaa Vallabhudu Neevenayyoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sakala Kalaa Vallabhudu Neevenayyoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Moratugunna Manasu Vena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moratugunna Manasu Vena"/>
</div>
<div class="lyrico-lyrics-wrapper">Maa Krishnayyoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa Krishnayyoo"/>
</div>
</pre>
