---
title: "manathin saalayil song lyrics"
album: "Champion"
artist: "Arrol Corelli"
lyricist: "Viveka"
director: "Suseenthiran"
path: "/albums/champion-lyrics"
song: "Manathin Saalayil"
image: ../../images/albumart/champion.jpg
date: 2019-12-13
lang: tamil
youtubeLink: "https://www.youtube.com/embed/IeSsLHu-POA"
type: "love"
singers:
  - Arrol Corelli
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Manathin Saalaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manathin Saalaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Maavilai Thoranam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maavilai Thoranam"/>
</div>
<div class="lyrico-lyrics-wrapper">Arugil Devathai Vaasanai Kaaranam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arugil Devathai Vaasanai Kaaranam"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhoomiyae Oru Poovai Maaruthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhoomiyae Oru Poovai Maaruthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhkai Kulir Theevaai Thonridudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkai Kulir Theevaai Thonridudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanaa Kanaa Kaatchiyellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanaa Kanaa Kaatchiyellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanaai Thanaai Therigirathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanaai Thanaai Therigirathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Theera Theera Aasai Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theera Theera Aasai Ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeyaai Theeyaai Erigirathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeyaai Theeyaai Erigirathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thindaadi Thindaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thindaadi Thindaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhisai Theriyaatha Yekkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhisai Theriyaatha Yekkam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kondaadi Kondaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondaadi Kondaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolaveriyoda Thaakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolaveriyoda Thaakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudhal Murai Idhu Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhal Murai Idhu Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Vidha Aasa Ada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Vidha Aasa Ada"/>
</div>
<div class="lyrico-lyrics-wrapper">Pulithinna Pallaga Koosuthu Pesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pulithinna Pallaga Koosuthu Pesa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ulla Yedho Oorudhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulla Yedho Oorudhadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Usurae Poguthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usurae Poguthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagae Thalai Keezhachae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagae Thalai Keezhachae"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhu Enna Thonaladi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhu Enna Thonaladi"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayam Kaanaladi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam Kaanaladi"/>
</div>
<div class="lyrico-lyrics-wrapper">Neruppu Ela Pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neruppu Ela Pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Panichoru Parimaarathaeae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panichoru Parimaarathaeae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanaa Kanaa Kaatchiyellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanaa Kanaa Kaatchiyellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanaai Thanaai Therigirathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanaai Thanaai Therigirathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Theera Theera Aasai Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theera Theera Aasai Ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeyaai Theeyaai Erigirathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeyaai Theeyaai Erigirathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ohunnodu Unnodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohunnodu Unnodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Kai Korthu Ponaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Kai Korthu Ponaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinnodu Vinnodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinnodu Vinnodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam Vilaiyaadum Thaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam Vilaiyaadum Thaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Arikuri Kaattaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arikuri Kaattaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Adai Mazhai Peiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adai Mazhai Peiya"/>
</div>
<div class="lyrico-lyrics-wrapper">En Ida Valam Puriyaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Ida Valam Puriyaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Enna Seiyya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Enna Seiyya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaanja Maram Pookkuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanja Maram Pookkuthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavaa Thaakkuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavaa Thaakkuthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thelivaa Tholanjae Ponaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thelivaa Tholanjae Ponaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sattakkulla Verkkuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sattakkulla Verkkuthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sangeetham Kekkuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sangeetham Kekkuthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhuvum Puriyaama Ila Nenjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhuvum Puriyaama Ila Nenjam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kulamaa Maara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulamaa Maara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanaa Kanaa Kaatchiyellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanaa Kanaa Kaatchiyellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanaai Thanaai Therigirathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanaai Thanaai Therigirathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Theera Theera Aasai Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theera Theera Aasai Ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeyaai Theeyaai Erigirathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeyaai Theeyaai Erigirathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manathin Saalaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manathin Saalaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Maavilai Thoranam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maavilai Thoranam"/>
</div>
<div class="lyrico-lyrics-wrapper">Arugil Devathai Vaasanai Kaaranam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arugil Devathai Vaasanai Kaaranam"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhoomiyae Oru Poovai Maaruthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhoomiyae Oru Poovai Maaruthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhkai Kulir Theevaai Thonridudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkai Kulir Theevaai Thonridudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanaa Kanaa Kaatchiyellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanaa Kanaa Kaatchiyellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanaai Thanaai Therigirathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanaai Thanaai Therigirathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Theera Theera Aasai Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theera Theera Aasai Ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeyaai Theeyaai Erigirathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeyaai Theeyaai Erigirathae"/>
</div>
</pre>
