---
title: "sakiye song lyrics"
album: "Oh Manapenne"
artist: "Vishal Chandrashekhar"
lyricist: "Mohan Rajan"
director: "Kaarthikk Sundar"
path: "/albums/oh-manapenne-lyrics"
song: "Sakiye"
image: ../../images/albumart/oh-manapenne.jpg
date: 2021-10-22
lang: tamil
youtubeLink: "https://www.youtube.com/embed/1jT8PkwzAWk"
type: "sad"
singers:
  - Yazin Nizar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">ethaiyo tholaithenadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethaiyo tholaithenadi"/>
</div>
<div class="lyrico-lyrics-wrapper">tholaithenadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tholaithenadi"/>
</div>
<div class="lyrico-lyrics-wrapper">thadumariye valive 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thadumariye valive "/>
</div>
<div class="lyrico-lyrics-wrapper">vali yenadi vali yenadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vali yenadi vali yenadi"/>
</div>
<div class="lyrico-lyrics-wrapper">enai meeriye pogiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enai meeriye pogiren"/>
</div>
<div class="lyrico-lyrics-wrapper">thooramaga thooramaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thooramaga thooramaga"/>
</div>
<div class="lyrico-lyrics-wrapper">adiye valgiren mounamaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adiye valgiren mounamaga"/>
</div>
<div class="lyrico-lyrics-wrapper">thaniye ini enna aagumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaniye ini enna aagumo"/>
</div>
<div class="lyrico-lyrics-wrapper">theriyathadi theriyathadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theriyathadi theriyathadi"/>
</div>
<div class="lyrico-lyrics-wrapper">pathil thedi nanum valgiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathil thedi nanum valgiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pesatha varthaiyodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pesatha varthaiyodu"/>
</div>
<div class="lyrico-lyrics-wrapper">nanum valthu kayam aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanum valthu kayam aanen"/>
</div>
<div class="lyrico-lyrics-wrapper">yethotho aasaiyodu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yethotho aasaiyodu "/>
</div>
<div class="lyrico-lyrics-wrapper">thotru pogiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thotru pogiren"/>
</div>
<div class="lyrico-lyrics-wrapper">aadatha vaalkai ondrai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aadatha vaalkai ondrai"/>
</div>
<div class="lyrico-lyrics-wrapper">nanum vaala engi nindrene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanum vaala engi nindrene"/>
</div>
<div class="lyrico-lyrics-wrapper">ellame kaanal aanathe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellame kaanal aanathe "/>
</div>
<div class="lyrico-lyrics-wrapper">yaavume maayame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaavume maayame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kilai mel poo polave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kilai mel poo polave"/>
</div>
<div class="lyrico-lyrics-wrapper">malarnthen unakagave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malarnthen unakagave"/>
</div>
<div class="lyrico-lyrics-wrapper">poo indru mannodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poo indru mannodu"/>
</div>
<div class="lyrico-lyrics-wrapper">kilai indru vinnodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kilai indru vinnodu"/>
</div>
<div class="lyrico-lyrics-wrapper">en nenjam valiyodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en nenjam valiyodu"/>
</div>
<div class="lyrico-lyrics-wrapper">ellame pilaiyodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellame pilaiyodu"/>
</div>
<div class="lyrico-lyrics-wrapper">kanavai kalanthomadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavai kalanthomadi"/>
</div>
<div class="lyrico-lyrics-wrapper">sakiye sakiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sakiye sakiye"/>
</div>
<div class="lyrico-lyrics-wrapper">kathaiyai mudinthomadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathaiyai mudinthomadi"/>
</div>
<div class="lyrico-lyrics-wrapper">sakiye sakiye sakiye 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sakiye sakiye sakiye "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pesatha varthaiyodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pesatha varthaiyodu"/>
</div>
<div class="lyrico-lyrics-wrapper">nanum valthu kayam aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanum valthu kayam aanen"/>
</div>
<div class="lyrico-lyrics-wrapper">yethotho aasaiyodu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yethotho aasaiyodu "/>
</div>
<div class="lyrico-lyrics-wrapper">thotru pogiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thotru pogiren"/>
</div>
<div class="lyrico-lyrics-wrapper">aadatha vaalkai ondrai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aadatha vaalkai ondrai"/>
</div>
<div class="lyrico-lyrics-wrapper">nanum vaala engi nindrene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanum vaala engi nindrene"/>
</div>
<div class="lyrico-lyrics-wrapper">ellame kaanal aanathe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellame kaanal aanathe "/>
</div>
<div class="lyrico-lyrics-wrapper">yaavume maayame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaavume maayame"/>
</div>
</pre>
