---
title: "miss india theme song lyrics"
album: "Miss India"
artist: "S. Thaman"
lyricist: "Kalyan Chakravarthi"
director: "Narendra Nath"
path: "/albums/miss-india-lyrics"
song: "Miss India Theme"
image: ../../images/albumart/miss-india.jpg
date: 2020-11-04
lang: telugu
youtubeLink: "https://www.youtube.com/embed/FfALBOXac0E"
type: "theme song"
singers:
  - Harika Narayan
  - Sruthi Ranjani
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Tharikita Tharikita Thatthatthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharikita Tharikita Thatthatthai"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharikita Tharikita Thatthatthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharikita Tharikita Thatthatthai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thathatthaayi Thathaayika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thathatthaayi Thathaayika"/>
</div>
<div class="lyrico-lyrics-wrapper">Thathatthaayi Thathaayika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thathatthaayi Thathaayika"/>
</div>
<div class="lyrico-lyrics-wrapper">Thatthaay Thatthaay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thatthaay Thatthaay"/>
</div>
<div class="lyrico-lyrics-wrapper">Thatthaay Thatthaay Thaay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thatthaay Thatthaay Thaay"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Prathi Udhayam Siddhame Kadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prathi Udhayam Siddhame Kadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vijayaaniki Vedhikai Sadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vijayaaniki Vedhikai Sadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhi Gamanam Cheralevugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhi Gamanam Cheralevugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gamyaaniki Gaalivaatugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gamyaaniki Gaalivaatugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Anunimisham Cheppaledhugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anunimisham Cheppaledhugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Marunimisham Midhya Kaadhugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marunimisham Midhya Kaadhugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethoduga Dhairyamundagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethoduga Dhairyamundagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Otamane Maataledhugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Otamane Maataledhugaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nighantuvedhi Cheppaledhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nighantuvedhi Cheppaledhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Niruttharaala Aashayaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niruttharaala Aashayaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Likinchanunna Repu Raase
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Likinchanunna Repu Raase"/>
</div>
<div class="lyrico-lyrics-wrapper">Lipantukundhi Swedhamele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lipantukundhi Swedhamele"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tharam Tharam Nirantharam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharam Tharam Nirantharam"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeraajanam Nee Saahaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeraajanam Nee Saahaasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nisheedhilo Niraamayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nisheedhilo Niraamayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Jayinchadhaa Ushodhayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jayinchadhaa Ushodhayam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tharam Tharam Nirantharam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharam Tharam Nirantharam"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeraajanam Nee Saahaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeraajanam Nee Saahaasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nisheedhilo Niraamayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nisheedhilo Niraamayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Jayinchadhaa Ushodhayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jayinchadhaa Ushodhayam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tharikita Tharikita Thatthatthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharikita Tharikita Thatthatthai"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharikita Tharikita Thatthatthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharikita Tharikita Thatthatthai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thathatthaayi Thathaayika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thathatthaayi Thathaayika"/>
</div>
<div class="lyrico-lyrics-wrapper">Thathatthaayi Thathaayika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thathatthaayi Thathaayika"/>
</div>
<div class="lyrico-lyrics-wrapper">Thatthaay Thatthaay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thatthaay Thatthaay"/>
</div>
<div class="lyrico-lyrics-wrapper">Thatthaay Thatthaay Thaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thatthaay Thatthaay Thaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Prathi Udhayam Siddhame Kadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prathi Udhayam Siddhame Kadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vijayaaniki Vedhikai Sadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vijayaaniki Vedhikai Sadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhi Gamanam Cheralevugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhi Gamanam Cheralevugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gamyaaniki Gaalivaatugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gamyaaniki Gaalivaatugaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nisshabdame Virigipodhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nisshabdame Virigipodhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Saadhane Shabdhamaithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Saadhane Shabdhamaithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Viruddhame Veegipodhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viruddhame Veegipodhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Mounamo Yuddhamaithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Mounamo Yuddhamaithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharam Tharam Nirantharam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharam Tharam Nirantharam"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeraajanam Nee Saahaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeraajanam Nee Saahaasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nisheedhilo Niraamayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nisheedhilo Niraamayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Jayinchadhaa Ushodhayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jayinchadhaa Ushodhayam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nisshabdame Virigipodhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nisshabdame Virigipodhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Saadhane Shabdhamaithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Saadhane Shabdhamaithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Viruddhame Veegipodhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viruddhame Veegipodhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Mounamo Yuddhamaithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Mounamo Yuddhamaithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharam Tharam Nirantharam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharam Tharam Nirantharam"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeraajanam Nee Saahaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeraajanam Nee Saahaasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nisheedhilo Niraamayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nisheedhilo Niraamayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Jayinchadhaa Ushodhayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jayinchadhaa Ushodhayam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tharikita Tharikita Thatthatthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharikita Tharikita Thatthatthai"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharikita Tharikita Thatthatthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharikita Tharikita Thatthatthai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thathatthaayi Thathaayika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thathatthaayi Thathaayika"/>
</div>
<div class="lyrico-lyrics-wrapper">Thathatthaayi Thathaayika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thathatthaayi Thathaayika"/>
</div>
<div class="lyrico-lyrics-wrapper">Thatthaay Thatthaay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thatthaay Thatthaay"/>
</div>
<div class="lyrico-lyrics-wrapper">Thatthaay Thatthaay Thaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thatthaay Thatthaay Thaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tharikita Tharikita Thatthatthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharikita Tharikita Thatthatthai"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharikita Tharikita Thatthatthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharikita Tharikita Thatthatthai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thathatthaayi Thathaayika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thathatthaayi Thathaayika"/>
</div>
<div class="lyrico-lyrics-wrapper">Thathatthaayi Thathaayika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thathatthaayi Thathaayika"/>
</div>
<div class="lyrico-lyrics-wrapper">Thatthaay Thatthaay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thatthaay Thatthaay"/>
</div>
<div class="lyrico-lyrics-wrapper">Thatthaay Thatthaay Thaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thatthaay Thatthaay Thaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Prathi Udhayam Siddhame Kadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prathi Udhayam Siddhame Kadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vijayaaniki Vedhikai Sadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vijayaaniki Vedhikai Sadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhi Gamanam Cheralevugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhi Gamanam Cheralevugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gamyaaniki Gaalivaatugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gamyaaniki Gaalivaatugaa"/>
</div>
</pre>
