---
title: 'manusan than manusan thansong lyrics'
album: 'Dharala Prabhu'
artist: 'Kaber Vasuki'
lyricist: 'Kaber Vasuki'
director: 'Krishna Marimuthu'
path: '/albums/dharala-prabhu-song-lyrics'
song: 'Manusan Than Manusan Than'
image: ../../images/albumart/dharala-prabhu.jpg
date: 2020-03-13
lang: tamil
singers: 
- Kaber Vasuki
youtubeLink: "https://www.youtube.com/embed/KeN59Y6j4cs"
type: 'Advice'
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">

  <div class="lyrico-lyrics-wrapper">un perachana list-ah podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un perachana list-ah podu"/>
</div>
<div class="lyrico-lyrics-wrapper">kaaranam paaru ore aalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaaranam paaru ore aalu"/>
</div>
<div class="lyrico-lyrics-wrapper">theervellam solli paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theervellam solli paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">kaaranam paatha athe aalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaaranam paatha athe aalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">manusan than manusan than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manusan than manusan than"/>
</div>
<div class="lyrico-lyrics-wrapper">vaazhavepathum manusan than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaazhavepathum manusan than"/>
</div>
<div class="lyrico-lyrics-wrapper">manusan than manusan than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manusan than manusan than"/>
</div>
<div class="lyrico-lyrics-wrapper">saavadipathum manusan than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saavadipathum manusan than"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">karpana than manusa sakthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karpana than manusa sakthi"/>
</div>
<div class="lyrico-lyrics-wrapper">ada virpana thane manusa buthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada virpana thane manusa buthi"/>
</div>
<div class="lyrico-lyrics-wrapper">ulaga vetti attapetti kulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulaga vetti attapetti kulla"/>
</div>
<div class="lyrico-lyrics-wrapper">sticker otti koovi vitha pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sticker otti koovi vitha pulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">manusan than manusan than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manusan than manusan than"/>
</div>
<div class="lyrico-lyrics-wrapper">nilava thotathum manusan than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nilava thotathum manusan than"/>
</div>
<div class="lyrico-lyrics-wrapper">manusan than manusan than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manusan than manusan than"/>
</div>
<div class="lyrico-lyrics-wrapper">neethi odachathum manusan than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neethi odachathum manusan than"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">manusa idhayam odachu paaru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manusa idhayam odachu paaru "/>
</div>
<div class="lyrico-lyrics-wrapper">oru aalu kulla ethana uuru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru aalu kulla ethana uuru"/>
</div>
<div class="lyrico-lyrics-wrapper">oru ooram unmai pesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru ooram unmai pesa"/>
</div>
<div class="lyrico-lyrics-wrapper">aanalum manota meesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aanalum manota meesa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">manasatchi aatchi ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasatchi aatchi ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">en kannala pathathilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kannala pathathilla"/>
</div>
<div class="lyrico-lyrics-wrapper">mansaathi irukum eathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mansaathi irukum eathil"/>
</div>
<div class="lyrico-lyrics-wrapper">adhigaram yenda servavilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adhigaram yenda servavilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">manusan than manusan than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manusan than manusan than"/>
</div>
<div class="lyrico-lyrics-wrapper">pethu vithathum manusan than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pethu vithathum manusan than"/>
</div>
<div class="lyrico-lyrics-wrapper">manusan than manusan than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manusan than manusan than"/>
</div>
<div class="lyrico-lyrics-wrapper">sothu sethathum manusan than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sothu sethathum manusan than"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kili pura vandu kuriv nethili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kili pura vandu kuriv nethili"/>
</div>
<div class="lyrico-lyrics-wrapper">sura erumbu kolavi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sura erumbu kolavi"/>
</div>
<div class="lyrico-lyrics-wrapper">nari puli arima sirutha yaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nari puli arima sirutha yaana"/>
</div>
<div class="lyrico-lyrics-wrapper">giraffe eruma vaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="giraffe eruma vaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">kozhi aadu maadu thulasi korangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kozhi aadu maadu thulasi korangu"/>
</div>
<div class="lyrico-lyrics-wrapper">moongil maram chedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moongil maram chedi"/>
</div>
<div class="lyrico-lyrics-wrapper">kodi yethuvume vambu seiyama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kodi yethuvume vambu seiyama"/>
</div>
<div class="lyrico-lyrics-wrapper">paathiya aduga gathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paathiya aduga gathi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">manusan than manusan than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manusan than manusan than"/>
</div>
<div class="lyrico-lyrics-wrapper">aativepathum manusan than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aativepathum manusan than"/>
</div>
<div class="lyrico-lyrics-wrapper">manusan than manusan than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manusan than manusan than"/>
</div>
<div class="lyrico-lyrics-wrapper">aatampoduran manusan than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aatampoduran manusan than"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">karpana than manusa sakthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karpana than manusa sakthi"/>
</div>
<div class="lyrico-lyrics-wrapper">ada virpana thane manusa buthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada virpana thane manusa buthi"/>
</div>
<div class="lyrico-lyrics-wrapper">ulaga vetti attapeti kulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulaga vetti attapeti kulla"/>
</div>
<div class="lyrico-lyrics-wrapper">sticker otti koovi vitha pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sticker otti koovi vitha pulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">manusan than manusan than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manusan than manusan than"/>
</div>
<div class="lyrico-lyrics-wrapper">vaazhavepathum manusan than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaazhavepathum manusan than"/>
</div>
<div class="lyrico-lyrics-wrapper">manusan than manusan than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manusan than manusan than"/>
</div>
<div class="lyrico-lyrics-wrapper">saavadipathum manusan than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saavadipathum manusan than"/>
</div>
</pre>