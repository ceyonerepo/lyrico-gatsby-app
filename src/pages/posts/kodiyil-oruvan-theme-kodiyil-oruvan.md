---
title: "kodiyil oruvan theme song lyrics"
album: "Kodiyil Oruvan"
artist: "	Nivas K. Prasanna - Harish arjun"
lyricist: "Arun Bharathi"
director: "Ananda Krishnan"
path: "/albums/kodiyil-oruvan-lyrics"
song: "Kodiyil Oruvan Theme"
image: ../../images/albumart/kodiyil-oruvan.jpg
date: 2021-09-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/4XVeJjXMngw"
type: "theme"
singers:
  - Vijay Antony
  - Dinesh Kanagaratnam (Rap)
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kodiyil Oruvan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodiyil Oruvan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodiyil Oruvan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodiyil Oruvan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodiyil Oruvan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodiyil Oruvan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodiyil Oruvan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodiyil Oruvan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kodiyil Oruvan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodiyil Oruvan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodiyil Oruvan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodiyil Oruvan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodiyil Oruvan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodiyil Oruvan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodiyil Oruvan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodiyil Oruvan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kodiyil Oruvan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodiyil Oruvan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodiyil Oruvan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodiyil Oruvan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodiyil Oruvan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodiyil Oruvan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodiyil Oruvan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodiyil Oruvan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kodiyil Oruvan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodiyil Oruvan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodiyil Oruvan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodiyil Oruvan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodiyil Oruvan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodiyil Oruvan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodiyil Oruvan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodiyil Oruvan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kodiyil Oruvan Yezhayin Pudhalvan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodiyil Oruvan Yezhayin Pudhalvan"/>
</div>
<div class="lyrico-lyrics-wrapper">Naatai Vitruthan Kooru Pottidum Kootathirku Pagaivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naatai Vitruthan Kooru Pottidum Kootathirku Pagaivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodiyil Oruvan Kobathil Samaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodiyil Oruvan Kobathil Samaran"/>
</div>
<div class="lyrico-lyrics-wrapper">Naatai Vitruthan Kooru Pottidum Kootathirku Pagaivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naatai Vitruthan Kooru Pottidum Kootathirku Pagaivan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pasikkum Koottam Theruvil Nirkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pasikkum Koottam Theruvil Nirkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilaiyai Maatra Mudivugal Eduppan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilaiyai Maatra Mudivugal Eduppan"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayam Muzhukka Unnai Sumandhu Vazhigal Thaanguvan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam Muzhukka Unnai Sumandhu Vazhigal Thaanguvan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Padhukkum Koottam Thalaigal Therikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padhukkum Koottam Thalaigal Therikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Theruvil Oda Thalaivazhi Koduppan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theruvil Oda Thalaivazhi Koduppan"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkai Pirandhu Unnakkai Irundhu Unakkai Vaazhuvan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkai Pirandhu Unnakkai Irundhu Unakkai Vaazhuvan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kodiyil Oruvan Yezhayin Pudhalvan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodiyil Oruvan Yezhayin Pudhalvan"/>
</div>
<div class="lyrico-lyrics-wrapper">Naatai Vitruthan Kooru Pottidum Kootathirku Pagaivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naatai Vitruthan Kooru Pottidum Kootathirku Pagaivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodiyil Oruvan Kobathil Samaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodiyil Oruvan Kobathil Samaran"/>
</div>
<div class="lyrico-lyrics-wrapper">Naatai Vitruthan Kooru Pottidum Kootathirku Pagaivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naatai Vitruthan Kooru Pottidum Kootathirku Pagaivan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">The Drop Of The Hat
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="The Drop Of The Hat"/>
</div>
<div class="lyrico-lyrics-wrapper">The Drop Of The Rap
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="The Drop Of The Rap"/>
</div>
<div class="lyrico-lyrics-wrapper">The Drop Of The Beat
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="The Drop Of The Beat"/>
</div>
<div class="lyrico-lyrics-wrapper">You Might It Listen To What Thoughts Was Fact
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Might It Listen To What Thoughts Was Fact"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">The Drop Of The Hat
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="The Drop Of The Hat"/>
</div>
<div class="lyrico-lyrics-wrapper">The Drop Of The Rap
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="The Drop Of The Rap"/>
</div>
<div class="lyrico-lyrics-wrapper">The Drop Of The Beat
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="The Drop Of The Beat"/>
</div>
<div class="lyrico-lyrics-wrapper">You Might It Listen To What Thoughts Was Fact
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Might It Listen To What Thoughts Was Fact"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pinnadi Pesatha Munnadi Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pinnadi Pesatha Munnadi Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kan Katti Vithaiya No No
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan Katti Vithaiya No No"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannadi Munnadi Viruntha Kattiputta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannadi Munnadi Viruntha Kattiputta"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhe Gethu Than Aagathu No No
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhe Gethu Than Aagathu No No"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivan Than Udambil Kadavul Kondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Than Udambil Kadavul Kondu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mirugam Azhithu Irukkuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mirugam Azhithu Irukkuda"/>
</div>
<div class="lyrico-lyrics-wrapper">Yezhayin Manathil Kodi Katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yezhayin Manathil Kodi Katti"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazha Piranthavan Naanada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazha Piranthavan Naanada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yenga Sananga Ellam Nalla Sanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenga Sananga Ellam Nalla Sanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuzha Saami Kuooa Onna Nikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuzha Saami Kuooa Onna Nikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodi Kodiya Thaan Koottam Serum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodi Kodiya Thaan Koottam Serum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ondru Sernthu Ippo Seiyya Porom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondru Sernthu Ippo Seiyya Porom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Janaga Managa Thookki Adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Janaga Managa Thookki Adi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana Thana Thana Thookki Adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana Thana Thana Thookki Adi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanamana Kana Kana Jana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanamana Kana Kana Jana"/>
</div>
<div class="lyrico-lyrics-wrapper">Kana Kana Kana Kana Kana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kana Kana Kana Kana Kana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanaga Nanaga Nanaga Nanaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanaga Nanaga Nanaga Nanaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanaga Nanaga Nanaga Nanaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanaga Nanaga Nanaga Nanaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanaga Nanaga Nanaga Nanaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanaga Nanaga Nanaga Nanaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanaga Nanaga Nanaga Nanaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanaga Nanaga Nanaga Nanaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbil Thaayavan Pagai Kootathikku Peyavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbil Thaayavan Pagai Kootathikku Peyavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Avanai Nambividu Unnai Kaapatruvan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avanai Nambividu Unnai Kaapatruvan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nodikkul Mudippan Vidiyal Koduppan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nodikkul Mudippan Vidiyal Koduppan"/>
</div>
<div class="lyrico-lyrics-wrapper">Idikkum Idiyin Madiyil Paduppan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idikkum Idiyin Madiyil Paduppan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thadukkum Thadaikkal Ivanin Padikkal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadukkum Thadaikkal Ivanin Padikkal"/>
</div>
<div class="lyrico-lyrics-wrapper">Sigarathai Thoduvan Nanban
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sigarathai Thoduvan Nanban"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kodiyil Oruvan Yezhayin Pudhalvan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodiyil Oruvan Yezhayin Pudhalvan"/>
</div>
<div class="lyrico-lyrics-wrapper">Naatai Vitruthan Kooru Pottidum Kootathirku Pagaivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naatai Vitruthan Kooru Pottidum Kootathirku Pagaivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodiyil Oruvan Kobathil Samaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodiyil Oruvan Kobathil Samaran"/>
</div>
<div class="lyrico-lyrics-wrapper">Naatai Vitruthan Kooru Pottidum Kootathirku Pagaivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naatai Vitruthan Kooru Pottidum Kootathirku Pagaivan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pasikkum Koottam Theruvil Nirkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pasikkum Koottam Theruvil Nirkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilaiyai Maatra Mudivugal Eduppan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilaiyai Maatra Mudivugal Eduppan"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayam Muzhukka Unnai Sumandhu Vazhigal Thaanguvan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam Muzhukka Unnai Sumandhu Vazhigal Thaanguvan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Padhukkum Koottam Thalaigal Therikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padhukkum Koottam Thalaigal Therikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Theruvil Oda Thalaivazhi Koduppan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theruvil Oda Thalaivazhi Koduppan"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkai Pirandhu Unnakkai Irundhu Unakkai Vaazhuvan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkai Pirandhu Unnakkai Irundhu Unakkai Vaazhuvan"/>
</div>
</pre>
