---
title: "puriyavillai idhu puriyavillai song lyrics"
album: "Singam II"
artist: "Devi Sri Prasad"
lyricist: "Viveka"
director: "Hari Gopalakrishnan"
path: "/albums/singam-2-lyrics"
song: "Puriyavillai Idhu Puriyavillai"
image: ../../images/albumart/singam-2.jpg
date: 2013-07-05
lang: tamil
youtubeLink: "https://www.youtube.com/embed/EeI_sprZFxQ"
type: "Love"
singers:
  - Swetha Mohan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Puriyavillai idhu puriyavillai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puriyavillai idhu puriyavillai "/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu puriyavillai.. 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu puriyavillai.. "/>
</div>
<div class="lyrico-lyrics-wrapper">Mudhalmudhalaai manam karaivadhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhalmudhalaai manam karaivadhu "/>
</div>
<div class="lyrico-lyrics-wrapper">Yen endru puriyavillai.. 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen endru puriyavillai.. "/>
</div>
<div class="lyrico-lyrics-wrapper">Unadhu nyaabagam maraiyavillai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unadhu nyaabagam maraiyavillai "/>
</div>
<div class="lyrico-lyrics-wrapper">adhai Maraikka ennidam thiramai illai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adhai Maraikka ennidam thiramai illai "/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhiyil paarkkiren vaanavillai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhiyil paarkkiren vaanavillai "/>
</div>
<div class="lyrico-lyrics-wrapper">adhil Vizhuntha kaaranam thondravillai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adhil Vizhuntha kaaranam thondravillai "/>
</div>
<div class="lyrico-lyrics-wrapper">Idhupol idhuvarai aanadhillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhupol idhuvarai aanadhillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Puriyavillai idhu puriyavillai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puriyavillai idhu puriyavillai "/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu puriyavillai.. 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu puriyavillai.. "/>
</div>
<div class="lyrico-lyrics-wrapper">Mudhalmudhalaai manam karaivadhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhalmudhalaai manam karaivadhu "/>
</div>
<div class="lyrico-lyrics-wrapper">Yen endru puriyavillai..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen endru puriyavillai.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalai ezhundhavudan En kanavugal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalai ezhundhavudan En kanavugal "/>
</div>
<div class="lyrico-lyrics-wrapper">mudivathillai Maalai marainthaalum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudivathillai Maalai marainthaalum "/>
</div>
<div class="lyrico-lyrics-wrapper">Pallikoodam marapadhillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pallikoodam marapadhillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thozhi thunaiyai virumbavillai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozhi thunaiyai virumbavillai "/>
</div>
<div class="lyrico-lyrics-wrapper">Thozhan neeyum maaravillai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozhan neeyum maaravillai "/>
</div>
<div class="lyrico-lyrics-wrapper">Pechil pazhaya vegam illai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pechil pazhaya vegam illai "/>
</div>
<div class="lyrico-lyrics-wrapper">Pesa yedhum vaarthaigal illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesa yedhum vaarthaigal illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Puriyavillai idhu puriyavillai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puriyavillai idhu puriyavillai "/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu puriyavillai.. 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu puriyavillai.. "/>
</div>
<div class="lyrico-lyrics-wrapper">Mudhalmudhalaai manam karaivadhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhalmudhalaai manam karaivadhu "/>
</div>
<div class="lyrico-lyrics-wrapper">Yen endru puriyavillai..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen endru puriyavillai.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haa… saaral mazhaiyinilae 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa… saaral mazhaiyinilae "/>
</div>
<div class="lyrico-lyrics-wrapper">Udal eeram unaravillai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udal eeram unaravillai "/>
</div>
<div class="lyrico-lyrics-wrapper">Saalai marangalilae 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saalai marangalilae "/>
</div>
<div class="lyrico-lyrics-wrapper">Indru yeno nizhalgal illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indru yeno nizhalgal illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalgal erandum tharaiyil illai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalgal erandum tharaiyil illai "/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam neram maaravillai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam neram maaravillai "/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatril edhuvum asaiyavillai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatril edhuvum asaiyavillai "/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal pola kodumai illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal pola kodumai illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Puriyavillai idhu puriyavillai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puriyavillai idhu puriyavillai "/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu puriyavillai.. 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu puriyavillai.. "/>
</div>
<div class="lyrico-lyrics-wrapper">Mudhalmudhalaai manam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhalmudhalaai manam "/>
</div>
<div class="lyrico-lyrics-wrapper">karaivadhu Yen endru puriyavillai………….
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karaivadhu Yen endru puriyavillai…………."/>
</div>
</pre>
