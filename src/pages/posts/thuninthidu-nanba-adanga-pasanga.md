---
title: "thuninthidu nanba song lyrics"
album: "Adanga Pasanga"
artist: "AK Alldern"
lyricist: "S Kumaresan"
director: "R. Selvanathan"
path: "/albums/adanga-pasanga-lyrics"
song: "Thuninthidu Nanba"
image: ../../images/albumart/adanga-pasanga.jpg
date: 2018-10-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/vlDXAe4X0ZA"
type: "happy"
singers:
  - sulpi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">thuninthidu nanba thuninthidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thuninthidu nanba thuninthidu"/>
</div>
<div class="lyrico-lyrics-wrapper">thuninthidu thuninthidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thuninthidu thuninthidu"/>
</div>
<div class="lyrico-lyrics-wrapper">un thuyarathai neekida porupadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un thuyarathai neekida porupadu"/>
</div>
<div class="lyrico-lyrics-wrapper">porupadu porupadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="porupadu porupadu"/>
</div>
<div class="lyrico-lyrics-wrapper">nee kalathai velvathum elithada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee kalathai velvathum elithada"/>
</div>
<div class="lyrico-lyrics-wrapper">elithada elithada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="elithada elithada"/>
</div>
<div class="lyrico-lyrics-wrapper">un ulaipinil uruthiyum kollada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un ulaipinil uruthiyum kollada"/>
</div>
<div class="lyrico-lyrics-wrapper">kollada kollada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kollada kollada"/>
</div>
<div class="lyrico-lyrics-wrapper">yei tholvaiyai kandu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yei tholvaiyai kandu "/>
</div>
<div class="lyrico-lyrics-wrapper">thuvandu vidaathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thuvandu vidaathe"/>
</div>
<div class="lyrico-lyrics-wrapper">thuyarathil neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thuyarathil neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">moolgi vidaathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moolgi vidaathe"/>
</div>
<div class="lyrico-lyrics-wrapper">kudumbam unathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kudumbam unathu"/>
</div>
<div class="lyrico-lyrics-wrapper">kovilai pola unarnthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kovilai pola unarnthe"/>
</div>
<div class="lyrico-lyrics-wrapper">neyum unmaiyaai vaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neyum unmaiyaai vaalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thuninthidu nanba thuninthidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thuninthidu nanba thuninthidu"/>
</div>
<div class="lyrico-lyrics-wrapper">thuninthidu thuninthidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thuninthidu thuninthidu"/>
</div>
<div class="lyrico-lyrics-wrapper">un thuyarathai neekida porupadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un thuyarathai neekida porupadu"/>
</div>
<div class="lyrico-lyrics-wrapper">porupadu porupadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="porupadu porupadu"/>
</div>
<div class="lyrico-lyrics-wrapper">nee kalathai velvathum elithada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee kalathai velvathum elithada"/>
</div>
<div class="lyrico-lyrics-wrapper">elithada elithada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="elithada elithada"/>
</div>
<div class="lyrico-lyrics-wrapper">un ulaipinil uruthiyum kollada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un ulaipinil uruthiyum kollada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ye adanga pasanga naanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ye adanga pasanga naanga"/>
</div>
<div class="lyrico-lyrics-wrapper">ipo adangi ponom thaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ipo adangi ponom thaanga"/>
</div>
<div class="lyrico-lyrics-wrapper">antha sigarathin mele aeri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antha sigarathin mele aeri"/>
</div>
<div class="lyrico-lyrics-wrapper">enga kodiya naduvom naanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga kodiya naduvom naanga"/>
</div>
<div class="lyrico-lyrics-wrapper">ada adanga pasanga naanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada adanga pasanga naanga"/>
</div>
<div class="lyrico-lyrics-wrapper">ipo adangi ponom thaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ipo adangi ponom thaanga"/>
</div>
<div class="lyrico-lyrics-wrapper">antha sigarathin mele aeri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antha sigarathin mele aeri"/>
</div>
<div class="lyrico-lyrics-wrapper">enga kodiya naduvom naanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga kodiya naduvom naanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">love la jeyicha ada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="love la jeyicha ada"/>
</div>
<div class="lyrico-lyrics-wrapper">kaalam oodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaalam oodum"/>
</div>
<div class="lyrico-lyrics-wrapper">ada life la jeyicha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada life la jeyicha"/>
</div>
<div class="lyrico-lyrics-wrapper">endrum inbam koodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="endrum inbam koodum"/>
</div>
<div class="lyrico-lyrics-wrapper">ada love la jeyicha ada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada love la jeyicha ada"/>
</div>
<div class="lyrico-lyrics-wrapper">kaalam oodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaalam oodum"/>
</div>
<div class="lyrico-lyrics-wrapper">hey life la jeyicha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey life la jeyicha"/>
</div>
<div class="lyrico-lyrics-wrapper">endrum inbam koodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="endrum inbam koodum"/>
</div>
</pre>
