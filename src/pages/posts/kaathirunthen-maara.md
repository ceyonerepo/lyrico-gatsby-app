---
title: "kathirunthen song lyrics"
album: "Maara"
artist: "Ghibran"
lyricist: "Thamarai"
director: "Dhilip Kumar"
path: "/albums/maara-song-lyrics"
song: "Kaathirunthen"
image: ../../images/albumart/maara.jpg
date: 2021-01-08
lang: tamil
youtubeLink: "https://www.youtube.com/embed/COfMScmya-M"
type: "love"
singers:
  - Ananthu
  - Srisha Vijayasekar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Kaaththirunthaen Kaaththirunthaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaththirunthaen Kaaththirunthaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaaladi Oosaigal Ketkum Varai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaladi Oosaigal Ketkum Varai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paarththirunthaen Paarththirunthaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarththirunthaen Paarththirunthaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarvaigal Poi Varum Thooram Varai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarvaigal Poi Varum Thooram Varai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neengaamal Unnai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neengaamal Unnai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Enni Vaazhnthaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Enni Vaazhnthaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninaivil Paadhi Kanavil Paadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaivil Paadhi Kanavil Paadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalthorum Idhae Nilai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalthorum Idhae Nilai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veliyil Solla Mudiyathendrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veliyil Solla Mudiyathendrum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Kooda Adhae Nilai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Kooda Adhae Nilai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paarththirunthaen Paarththirunthaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarththirunthaen Paarththirunthaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarvaigal Poi Varum Thooram Varai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarvaigal Poi Varum Thooram Varai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mugavarigal Illaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mugavarigal Illaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Mudhal Kadithamaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Mudhal Kadithamaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pala Kadhavu Mothum Kaagitham Aanaennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala Kadhavu Mothum Kaagitham Aanaennae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arimugangal Illaa Pala Kadhavugalilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arimugangal Illaa Pala Kadhavugalilum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Mugaththai Thedum Kaarmugil Naanaenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Mugaththai Thedum Kaarmugil Naanaenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pesaatha Kadhai Nooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesaatha Kadhai Nooru"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesum Nilai Varumpothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesum Nilai Varumpothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaarththai Ena Edhvum Varaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaarththai Ena Edhvum Varaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Varaathu Varaathu Mounam Aanaenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varaathu Varaathu Mounam Aanaenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalam Urainthae…Pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam Urainthae…Pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaattru Azhuthae…Theerum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattru Azhuthae…Theerum"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha Nodi Iranthaalum Sammatham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Nodi Iranthaalum Sammatham"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanneerin Mazhaiyil Kadalgalum Neeraadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneerin Mazhaiyil Kadalgalum Neeraadum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaaththirunthaen Kaaththirunthaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaththirunthaen Kaaththirunthaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaaladi Oosaigal Ketkum Varai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaladi Oosaigal Ketkum Varai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neengaamal Unnai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neengaamal Unnai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Enni Vaazhnthaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Enni Vaazhnthaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninaivil Paadhi Kanavil Paadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaivil Paadhi Kanavil Paadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalthorum Idhae Nilai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalthorum Idhae Nilai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veliyil Solla Mudiyathendrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veliyil Solla Mudiyathendrum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Kooda Adhae Nilai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Kooda Adhae Nilai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paarththirunthaen Paarththirunthaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarththirunthaen Paarththirunthaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarvaigal Poi Varum Thooram Varai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarvaigal Poi Varum Thooram Varai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aa…Aa…Aa….Aa……
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa…Aa…Aa….Aa……"/>
</div>
</pre>
