---
title: "heer - ranje ishq ka song lyrics"
album: "Durgamati"
artist: "Naman Adhikari - Abhinav Sharma - Malini Awasthi"
lyricist: "Dipti Misra"
director: "G. Ashok"
path: "/albums/durgamati-lyrics"
song: "Heer - Ranje Ishq Ka"
image: ../../images/albumart/durgamati.jpg
date: 2020-12-11
lang: hindi
youtubeLink: "https://www.youtube.com/embed/qSOe0SB1ILk"
type: "mass"
singers:
  - Malini Awasthi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ranj-e-ishq ka kyaa jaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ranj-e-ishq ka kyaa jaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Peer fakeer
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peer fakeer"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeh to radha jaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh to radha jaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Ya phir jaane heer
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ya phir jaane heer"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bin ranjhe ki heer huyi main
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bin ranjhe ki heer huyi main"/>
</div>
<div class="lyrico-lyrics-wrapper">Bin ranjhe ki heer huyi main
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bin ranjhe ki heer huyi main"/>
</div>
<div class="lyrico-lyrics-wrapper">Aap hi apni peer huyi main
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aap hi apni peer huyi main"/>
</div>
<div class="lyrico-lyrics-wrapper">Aap hi apni peer huyi main
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aap hi apni peer huyi main"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bin ranjhe ki heer huyi main
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bin ranjhe ki heer huyi main"/>
</div>
<div class="lyrico-lyrics-wrapper">Bin ranjhe ki heer huyi main
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bin ranjhe ki heer huyi main"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Khud ko khud mein qaid kiya hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khud ko khud mein qaid kiya hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Khud ko khud mein qaid kiya hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khud ko khud mein qaid kiya hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Khud apni zanjeer huyi main
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khud apni zanjeer huyi main"/>
</div>
<div class="lyrico-lyrics-wrapper">Khud apni zanjeer huyi main
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khud apni zanjeer huyi main"/>
</div>
<div class="lyrico-lyrics-wrapper">Bin ranjhe ki heer huyi main
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bin ranjhe ki heer huyi main"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Heer huyi main!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heer huyi main!"/>
</div>
</pre>
