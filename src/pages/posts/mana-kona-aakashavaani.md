---
title: "mana kona song lyrics"
album: "Aakashavaani"
artist: "Kaala Bhairava"
lyricist: "Ananth Sriram"
director: "Ashwin Gangaraju"
path: "/albums/aakashavaani-lyrics"
song: "Mana Kona"
image: ../../images/albumart/aakashavaani.jpg
date: 2021-09-24
lang: telugu
youtubeLink: "https://www.youtube.com/embed/JNRqObkSaCw"
type: "love"
singers:
  - Mangli
  - Kaala Bhairava
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Koyyaahe Koyyaake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koyyaahe Koyyaake"/>
</div>
<div class="lyrico-lyrics-wrapper">Kommalane Ammalane 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kommalane Ammalane "/>
</div>
<div class="lyrico-lyrics-wrapper">Koyanai Puttaa Kolusukonaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koyanai Puttaa Kolusukonaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thayyaahe Thayyaahe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thayyaahe Thayyaahe"/>
</div>
<div class="lyrico-lyrics-wrapper">Thummalane Thammulane 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thummalane Thammulane "/>
</div>
<div class="lyrico-lyrics-wrapper">Thovalo Ittaa Nadusukonaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thovalo Ittaa Nadusukonaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mannuke Mokkuthu Unde Manakonaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannuke Mokkuthu Unde Manakonaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mabbuke Dhandamuvette Manakonaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mabbuke Dhandamuvette Manakonaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Bandake Dandeyyamandhe Manakonaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bandake Dandeyyamandhe Manakonaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Bathkune Pandaga Jese Manakonaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bathkune Pandaga Jese Manakonaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koyyaahe Koyyaake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koyyaahe Koyyaake"/>
</div>
<div class="lyrico-lyrics-wrapper">Kommalane Ammalane 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kommalane Ammalane "/>
</div>
<div class="lyrico-lyrics-wrapper">Koyanai Puttaa Kolusukonaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koyanai Puttaa Kolusukonaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thayyaahe Thayyaahe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thayyaahe Thayyaahe"/>
</div>
<div class="lyrico-lyrics-wrapper">Thummalane Thammulane 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thummalane Thammulane "/>
</div>
<div class="lyrico-lyrics-wrapper">Thovalo Ittaa Nadusukonaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thovalo Ittaa Nadusukonaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sayyaahe Sayyaahe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sayyaahe Sayyaahe"/>
</div>
<div class="lyrico-lyrics-wrapper">Sethulani Sethulake Seyyave 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sethulani Sethulake Seyyave "/>
</div>
<div class="lyrico-lyrics-wrapper">Sekka Panisey Koona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sekka Panisey Koona"/>
</div>
<div class="lyrico-lyrics-wrapper">Sayyaahe Sayyaahe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sayyaahe Sayyaahe"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandhelunni Sandhadike Saathi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhelunni Sandhadike Saathi "/>
</div>
<div class="lyrico-lyrics-wrapper">Ikka Idisey Koona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ikka Idisey Koona"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pittale Suttamulandhe Manakonaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pittale Suttamulandhe Manakonaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Puttake Pattamugatte Manakonaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puttake Pattamugatte Manakonaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Janthuve Jangamudandhe Manakonaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Janthuve Jangamudandhe Manakonaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Munthalo Ganganu Patte Manakonaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munthalo Ganganu Patte Manakonaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Koyyaahe Koyyaake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koyyaahe Koyyaake"/>
</div>
<div class="lyrico-lyrics-wrapper">Kommalane Ammalane Koyanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kommalane Ammalane Koyanai"/>
</div>
</pre>
