---
title: "appan policeda song lyrics"
album: "Kadhalar Kudiyiruppu"
artist: "James Vasanthan"
lyricist: "Na. Muthukumar"
director: "AMR Ramesh"
path: "/albums/kadhalar-kudiyiruppu-lyrics"
song: "Appan Policeda"
image: ../../images/albumart/kadhalar-kudiyiruppu.jpg
date: 2011-02-18
lang: tamil
youtubeLink: "https://www.youtube.com/embed/BFBIdAINNCE"
type: "happy"
singers:
  - Sunandan
  - Bharani
  - Prasanna
  - Srinivasan
  - Padmanabhan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Sonnadha kettidamaattom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sonnadha kettidamaattom"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaettadha sollidamaattom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaettadha sollidamaattom"/>
</div>
<div class="lyrico-lyrics-wrapper">Veettula thangida maatom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veettula thangida maatom"/>
</div>
<div class="lyrico-lyrics-wrapper">Poattadhellaam thinnuda maattom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poattadhellaam thinnuda maattom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sonnadha kettidamaattom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sonnadha kettidamaattom"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaettadha sollidamaattom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaettadha sollidamaattom"/>
</div>
<div class="lyrico-lyrics-wrapper">Veettula thangida maatom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veettula thangida maatom"/>
</div>
<div class="lyrico-lyrics-wrapper">Poattadhellaam thinnuda maattom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poattadhellaam thinnuda maattom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sonnadha kettidamaattom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sonnadha kettidamaattom"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaettadha sollidamaattom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaettadha sollidamaattom"/>
</div>
<div class="lyrico-lyrics-wrapper">Veettula thangida maatom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veettula thangida maatom"/>
</div>
<div class="lyrico-lyrics-wrapper">Poattadhellaam thinnuda maattom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poattadhellaam thinnuda maattom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naama rettai vaalu thaanadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naama rettai vaalu thaanadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Namakku kettapperu vaangadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namakku kettapperu vaangadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaamara koazhi thaanadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaamara koazhi thaanadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorula pudicha edam paarudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorula pudicha edam paarudaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naama rettai vaalu thaanadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naama rettai vaalu thaanadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Namakku kettapperu vaangadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namakku kettapperu vaangadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaamara koazhi thaanadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaamara koazhi thaanadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorula pudicha edam paarudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorula pudicha edam paarudaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Appan Police - u ulla paalisudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appan Police - u ulla paalisudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagaa thiruduvadhil mannanadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaa thiruduvadhil mannanadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adadaa aandavano vennai eduththavandhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adadaa aandavano vennai eduththavandhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhanaal emperum kannanadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhanaal emperum kannanadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalaiyil pettikkada thammu adippoam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalaiyil pettikkada thammu adippoam"/>
</div>
<div class="lyrico-lyrics-wrapper">Maalaiyil buttiyila rammu adippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maalaiyil buttiyila rammu adippom"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaela vettiyillla roundu adippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaela vettiyillla roundu adippom"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaalap bullattula speedaa naam poavom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaalap bullattula speedaa naam poavom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Appan Police - u ulla paalisudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appan Police - u ulla paalisudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagaa thiruduvadhil mannanadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaa thiruduvadhil mannanadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adadaa aandavano vennai eduththavandhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adadaa aandavano vennai eduththavandhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhanaal emperum kannanadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhanaal emperum kannanadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nighttu shovukkoru ticket eduppom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nighttu shovukkoru ticket eduppom"/>
</div>
<div class="lyrico-lyrics-wrapper">Fight-tu seenu vandhaa bigilum adippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fight-tu seenu vandhaa bigilum adippom"/>
</div>
<div class="lyrico-lyrics-wrapper">Saittu adikkathaaney sangam amaippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saittu adikkathaaney sangam amaippom"/>
</div>
<div class="lyrico-lyrics-wrapper">Light-ta lukku vittaa Herovaa aavom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Light-ta lukku vittaa Herovaa aavom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nighttu shovukkoru ticket eduppom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nighttu shovukkoru ticket eduppom"/>
</div>
<div class="lyrico-lyrics-wrapper">Fight-tu seenu vandhaa bigilum adippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fight-tu seenu vandhaa bigilum adippom"/>
</div>
<div class="lyrico-lyrics-wrapper">Saittu adikkathaaney sangam amaippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saittu adikkathaaney sangam amaippom"/>
</div>
<div class="lyrico-lyrics-wrapper">Light-ta lukku vittaa Herovaa aavom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Light-ta lukku vittaa Herovaa aavom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Appan Police-u ulla paalisu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appan Police-u ulla paalisu"/>
</div>
<div class="lyrico-lyrics-wrapper">Rekkai illaama kaalparakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rekkai illaama kaalparakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappum thappu illa right-tum right-tu illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappum thappu illa right-tum right-tu illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nammala oru naalu oormadhikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nammala oru naalu oormadhikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sonnadha kettidamaattom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sonnadha kettidamaattom"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaettadha sollidamaattom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaettadha sollidamaattom"/>
</div>
<div class="lyrico-lyrics-wrapper">Veettula thangida maatom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veettula thangida maatom"/>
</div>
<div class="lyrico-lyrics-wrapper">Poattadhellaam thinnuda maattom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poattadhellaam thinnuda maattom"/>
</div>
</pre>
