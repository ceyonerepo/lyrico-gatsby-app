---
title: "kanulanu adiga song lyrics"
album: "Amaram Akhilam Prema"
artist: "Radhan"
lyricist: "Krishna Kanth"
director: "Jonathan Edwards"
path: "/albums/amaram-akhilam-prema-lyrics"
song: "Kanulanu Adiga"
image: ../../images/albumart/amaram-akhilam-prema.jpg
date: 2020-09-18
lang: telugu
youtubeLink: "https://www.youtube.com/embed/IdBHHkU7Gfo"
type: "love"
singers:
  - Ranjith
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">kanulanu adiga vadhalamani nee rupe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanulanu adiga vadhalamani nee rupe"/>
</div>
<div class="lyrico-lyrics-wrapper">edhuru thirige prathi dhanni neela chupe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edhuru thirige prathi dhanni neela chupe"/>
</div>
<div class="lyrico-lyrics-wrapper">manasunadiga viduvamani nee usae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasunadiga viduvamani nee usae"/>
</div>
<div class="lyrico-lyrics-wrapper">apudu thelisindhe na manase natho ledhani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="apudu thelisindhe na manase natho ledhani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ee kshanam kaalame aagiponi ila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ee kshanam kaalame aagiponi ila"/>
</div>
<div class="lyrico-lyrics-wrapper">jeevitham chaladhe chudaga ninnila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jeevitham chaladhe chudaga ninnila"/>
</div>
<div class="lyrico-lyrics-wrapper">ne premincheyanaa ninne inthala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ne premincheyanaa ninne inthala"/>
</div>
<div class="lyrico-lyrics-wrapper">naa akari upiri varaku na pranam poina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa akari upiri varaku na pranam poina"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nee swaasavvana nee ventunta ne kada varaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee swaasavvana nee ventunta ne kada varaku"/>
</div>
<div class="lyrico-lyrics-wrapper">na santhosam nuvve na badha nuvve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="na santhosam nuvve na badha nuvve"/>
</div>
<div class="lyrico-lyrics-wrapper">nuvve naku cheekati velugu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvve naku cheekati velugu"/>
</div>
<div class="lyrico-lyrics-wrapper">nuvvu na ninnale na repu nuvve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvvu na ninnale na repu nuvve"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nuvve naku votami gelupu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvve naku votami gelupu"/>
</div>
<div class="lyrico-lyrics-wrapper">alasatavaela kudhuravuthaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alasatavaela kudhuravuthaa "/>
</div>
<div class="lyrico-lyrics-wrapper">pranam nimire cheyavutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pranam nimire cheyavutha"/>
</div>
<div class="lyrico-lyrics-wrapper">bujame thaduthu ne nilabadatha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bujame thaduthu ne nilabadatha "/>
</div>
<div class="lyrico-lyrics-wrapper">gelupi venake padathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gelupi venake padathaa"/>
</div>
<div class="lyrico-lyrics-wrapper">jadale dhuvvuthu chanuvutho 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jadale dhuvvuthu chanuvutho "/>
</div>
<div class="lyrico-lyrics-wrapper">thidathaa amme avuthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thidathaa amme avuthaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">adagani bandham nenavuthaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adagani bandham nenavuthaa "/>
</div>
<div class="lyrico-lyrics-wrapper">nee bandhalu nenuntaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee bandhalu nenuntaa"/>
</div>
<div class="lyrico-lyrics-wrapper">ne premincheyana ninne inthala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ne premincheyana ninne inthala"/>
</div>
<div class="lyrico-lyrics-wrapper">naa akari upiri varaku na pranam poina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa akari upiri varaku na pranam poina"/>
</div>
<div class="lyrico-lyrics-wrapper">nee swasavvana nee ventunta ne kada varaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee swasavvana nee ventunta ne kada varaku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">na santhosam nuvve na badha nuvve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="na santhosam nuvve na badha nuvve"/>
</div>
<div class="lyrico-lyrics-wrapper">nuvve naku cheekati velugu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvve naku cheekati velugu"/>
</div>
<div class="lyrico-lyrics-wrapper">nuvvu na ninnale na repu nuvve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvvu na ninnale na repu nuvve"/>
</div>
<div class="lyrico-lyrics-wrapper">nuvve naku otami gelupu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvve naku otami gelupu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ha avunani ante adugavuthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ha avunani ante adugavuthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhani thosthe needavuthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhani thosthe needavuthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">kanule cheri kalavadabadathaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanule cheri kalavadabadathaa "/>
</div>
<div class="lyrico-lyrics-wrapper">chedune tarimi nedathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chedune tarimi nedathaa"/>
</div>
<div class="lyrico-lyrics-wrapper">kastam vasthe edhuruga vuntaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kastam vasthe edhuruga vuntaa "/>
</div>
<div class="lyrico-lyrics-wrapper">nisilo velugavuthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nisilo velugavuthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">adakamundhe anni avthaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adakamundhe anni avthaa "/>
</div>
<div class="lyrico-lyrics-wrapper">adige lopala nene ivvanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adige lopala nene ivvanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">anulanu adiga vadhalamani nee rupe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anulanu adiga vadhalamani nee rupe"/>
</div>
<div class="lyrico-lyrics-wrapper">edhuru thirige prathi dhanni neela chupe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edhuru thirige prathi dhanni neela chupe"/>
</div>
<div class="lyrico-lyrics-wrapper">manasunadiga viduvamani nee use
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasunadiga viduvamani nee use"/>
</div>
<div class="lyrico-lyrics-wrapper">apudu thelisindhe na manase natho ledhani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="apudu thelisindhe na manase natho ledhani"/>
</div>
</pre>