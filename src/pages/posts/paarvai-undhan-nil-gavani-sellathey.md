---
title: "paarvai undhan song lyrics"
album: "Nil Gavani Sellathey"
artist: "Selvaganesh"
lyricist: "Na. Muthukumar"
director: "Anand Chakravarthy"
path: "/albums/nil-gavani-sellathey-lyrics"
song: "Paarvai Undhan"
image: ../../images/albumart/nil-gavani-sellathey.jpg
date: 2011-03-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/CU7swnK3WjQ"
type: "happy"
singers:
  - Vasundhara Das
  - Sricharan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Thoalil Un Thoalil Naan Saayum Anneram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoalil Un Thoalil Naan Saayum Anneram"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil En Nenjil Perinbam Undaagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil En Nenjil Perinbam Undaagum"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbey Un Anbil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbey Un Anbil"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Vaazhum Inneram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Vaazhum Inneram"/>
</div>
<div class="lyrico-lyrics-wrapper">Aiyo Ippoadhey Seththaalum Santhoasham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyo Ippoadhey Seththaalum Santhoasham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paarvai Undhan Paarvai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarvai Undhan Paarvai "/>
</div>
<div class="lyrico-lyrics-wrapper">Kulir Neram Adhu Poarvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulir Neram Adhu Poarvai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kangal En Kannil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kangal En Kannil"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhu Kaadhal Solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhu Kaadhal Solla"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaarthai Undhan Vaarthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaarthai Undhan Vaarthai"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu Vaarthai Illai Vaazhkkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu Vaarthai Illai Vaazhkkai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kaigal En Kaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kaigal En Kaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhu Veppam Solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhu Veppam Solla"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Perai Sollumboadhey Yedho Mayakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Perai Sollumboadhey Yedho Mayakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanmoodi Sollum Vaarthai Dhinandhoarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanmoodi Sollum Vaarthai Dhinandhoarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Munkoabam Konjam Undu Adadaa Enakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munkoabam Konjam Undu Adadaa Enakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Munbu Koabam Kooda Parandhodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Munbu Koabam Kooda Parandhodum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thoalil Un Thoalil Naan Saayum Anneram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoalil Un Thoalil Naan Saayum Anneram"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil En Nenjil Perinbam Undaagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil En Nenjil Perinbam Undaagum"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbey Un Anbil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbey Un Anbil"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Vaazhum Inneram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Vaazhum Inneram"/>
</div>
<div class="lyrico-lyrics-wrapper">Aiyo Ippoadhey Seththaalum Santhoasham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyo Ippoadhey Seththaalum Santhoasham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnoadu Pesappesa Neram Inikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnoadu Pesappesa Neram Inikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sila Neram Mounam Kooda Azhagaagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sila Neram Mounam Kooda Azhagaagum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Meedhu Aasaippattu Idhayam Thudikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Meedhu Aasaippattu Idhayam Thudikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Munbu Achcham Naanam Thadam Maarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Munbu Achcham Naanam Thadam Maarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaal Naan Unnaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaal Naan Unnaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaaththaadi Poal Aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaththaadi Poal Aanen"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaal Un Munnaal Engengo Naan Poanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaal Un Munnaal Engengo Naan Poanen"/>
</div>
<div class="lyrico-lyrics-wrapper">Illai Nee Illai Illai Naan Thaan Ennaaven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illai Nee Illai Illai Naan Thaan Ennaaven"/>
</div>
<div class="lyrico-lyrics-wrapper">Aiyo Appoadhey Angeye Naan Saaven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyo Appoadhey Angeye Naan Saaven"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kannin Imaippoaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kannin Imaippoaley"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhven Unnoadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhven Unnoadu"/>
</div>
</pre>
