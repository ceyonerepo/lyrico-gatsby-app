---
title: "thuppuna thodachukkuven song lyrics"
album: "Nenjamundu Nermaiyundu Odu Raja"
artist: "Shabir"
lyricist: "Kumaran Kumanan"
director: "Karthik Venugopalan"
path: "/albums/nenjamundu-nermaiyundu-odu-raja-lyrics"
song: "Thuppuna Thodachukkuven"
image: ../../images/albumart/nenjamundu-nermaiyundu-odu-raja.jpg
date: 2019-06-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/hmkdTlBqrN8"
type: "happy"
singers:
  - Anthony Dassan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Inkem Inkem Inkem Kaavalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inkem Inkem Inkem Kaavalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sirrr Paattu Paadiyae Aaganuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirrr Paattu Paadiyae Aaganuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen Naa Paada Koodaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Naa Paada Koodaatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Illa Sir Neenga Paaduna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illa Sir Neenga Paaduna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Paadrathu Thaan Prachanaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Paadrathu Thaan Prachanaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Paaduven Gethaa Style-Ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaduven Gethaa Style-Ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhu Illa Sir Makkalukku Edhavudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu Illa Sir Makkalukku Edhavudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Makkal Na Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Makkal Na Enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Minji Minji Pona Kaari Thuppuvanghla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minji Minji Pona Kaari Thuppuvanghla"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuppuna Thudachiduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuppuna Thudachiduven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Seatu-Ku Kenjiduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seatu-Ku Kenjiduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Voteu-Kku Kumbiduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Voteu-Kku Kumbiduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Assault-Ah Tweet-Ah Pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Assault-Ah Tweet-Ah Pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Admin Pera Solliduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Admin Pera Solliduvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veetukku 20 Rubaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veetukku 20 Rubaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuduthalae Jeyichuduvom Pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuduthalae Jeyichuduvom Pa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thermacol-Ah Aathula Vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thermacol-Ah Aathula Vittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeraviya Thaduthuduvom Pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeraviya Thaduthuduvom Pa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thuppuna Thuppuna Thuppuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuppuna Thuppuna Thuppuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Thodachikkuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodachikkuven"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuppuna Thuppuna Thuppuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuppuna Thuppuna Thuppuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Thodachikkuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodachikkuven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Thuppuna Thuppuna Thuppuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Thuppuna Thuppuna Thuppuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Thodachikkuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodachikkuven"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Thuppuna Thuppuna Thuppuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Thuppuna Thuppuna Thuppuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Thodachikkuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodachikkuven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thandhaa Nae Thaanae Naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandhaa Nae Thaanae Naanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaanae Naanae Naa Naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanae Naanae Naa Naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thandhaa Nae Thaanae Naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandhaa Nae Thaanae Naanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaanae Naanae Naa Naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanae Naanae Naa Naa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saadhi Madhatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saadhi Madhatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kizhichu Thovachu Eduppom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kizhichu Thovachu Eduppom"/>
</div>
<div class="lyrico-lyrics-wrapper">Election Vote-Ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Election Vote-Ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Saadhi Vachi Piripom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saadhi Vachi Piripom"/>
</div>
<div class="lyrico-lyrics-wrapper">Thimira Pesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thimira Pesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaniya Ninnu Thotha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaniya Ninnu Thotha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kooptu Vachu Panamtha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooptu Vachu Panamtha"/>
</div>
<div class="lyrico-lyrics-wrapper">Pangu Pirippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pangu Pirippom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Singam-Naa Karjana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Singam-Naa Karjana"/>
</div>
<div class="lyrico-lyrics-wrapper">Cm-Na Archana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cm-Na Archana"/>
</div>
<div class="lyrico-lyrics-wrapper">Vote-U-Kku Dhakshana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vote-U-Kku Dhakshana"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhuvumae Puriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhuvumae Puriyala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ponnaagum Bhoomi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponnaagum Bhoomi"/>
</div>
<div class="lyrico-lyrics-wrapper">Makkal Thaan Saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Makkal Thaan Saami"/>
</div>
<div class="lyrico-lyrics-wrapper">Sonna Poikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sonna Poikku"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannakku Irukka Kaami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannakku Irukka Kaami"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thuppuna Thuppuna Thuppuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuppuna Thuppuna Thuppuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Thodachikkuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodachikkuven"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuppuna Thuppuna Thuppuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuppuna Thuppuna Thuppuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Thodachikkuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodachikkuven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Thuppuna Thuppuna Thuppuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Thuppuna Thuppuna Thuppuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Thodachikkuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodachikkuven"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Thuppuna Thuppuna Thuppuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Thuppuna Thuppuna Thuppuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Thodachikkuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodachikkuven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thuppuna Thuppuna Thuppuna Thuppuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuppuna Thuppuna Thuppuna Thuppuna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thuu Thuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuu Thuu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thuppuna Thuppuna Thuppuna Thuppuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuppuna Thuppuna Thuppuna Thuppuna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thuppuna Thuppuna Thuppuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuppuna Thuppuna Thuppuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Thodachikkuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodachikkuven"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuppuna Thuppuna Thuppuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuppuna Thuppuna Thuppuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Thodachikkuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodachikkuven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Thuppuna Thuppuna Thuppuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Thuppuna Thuppuna Thuppuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Thodachikkuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodachikkuven"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Thuppuna Thuppuna Thuppuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Thuppuna Thuppuna Thuppuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Thodachikkuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodachikkuven"/>
</div>
<div class="lyrico-lyrics-wrapper">Thodachikkuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodachikkuven"/>
</div>
</pre>
