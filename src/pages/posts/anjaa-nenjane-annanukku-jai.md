---
title: "anjaa nenjane song lyrics"
album: "Annanukku Jai"
artist: "Arrol Corelli"
lyricist: "Sammie Kootaliz"
director: "Rajkumar"
path: "/albums/annanukku-jai-lyrics"
song: "Anjaa Nenjane"
image: ../../images/albumart/annanukku-jai.jpg
date: 2018-08-31
lang: tamil
youtubeLink: "https://www.youtube.com/embed/xtT7kJB_XUk"
type: "happy"
singers:
  - Anthony Daasan
  - Sammie Kootaliz
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Sathikka pirantha socrates
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sathikka pirantha socrates"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam potrum karlmarx
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam potrum karlmarx"/>
</div>
<div class="lyrico-lyrics-wrapper">Varumai ozhikkum karnanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varumai ozhikkum karnanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulaga ariyum mannanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulaga ariyum mannanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un kural thirukural
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kural thirukural"/>
</div>
<div class="lyrico-lyrics-wrapper">Un chinnam thodum viral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un chinnam thodum viral"/>
</div>
<div class="lyrico-lyrics-wrapper">Manitha kula manikkamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manitha kula manikkamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagam potrum kaavalanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagam potrum kaavalanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaaikulathin thava pudhalva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaikulathin thava pudhalva"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharani potrum engal iraiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharani potrum engal iraiva"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa vaa vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa vaa vaa vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anjaa nenjanae karunai vallalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anjaa nenjanae karunai vallalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Latchiya thalaiva vazhgavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Latchiya thalaiva vazhgavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Arivin arivae anbin sigaramae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arivin arivae anbin sigaramae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalaya mudhalva vazhgavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalaya mudhalva vazhgavae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sorva vittu verva kottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sorva vittu verva kottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan paarvai patta athu pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan paarvai patta athu pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">Manna vittu ponalumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manna vittu ponalumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma peru iruntha thaan adaiyaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma peru iruntha thaan adaiyaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sathikka pirantha socrates-eh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sathikka pirantha socrates-eh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ezhai makkal pangaalane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhai makkal pangaalane"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaviya thaayin thalai maganae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaviya thaayin thalai maganae"/>
</div>
<div class="lyrico-lyrics-wrapper">Udan pirappae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udan pirappae"/>
</div>
<div class="lyrico-lyrics-wrapper">Raththathin raththam neeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raththathin raththam neeyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadavul padaitha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadavul padaitha "/>
</div>
<div class="lyrico-lyrics-wrapper">kadaisi nallavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadaisi nallavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethanae thalaiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethanae thalaiva"/>
</div>
<div class="lyrico-lyrics-wrapper">Sinthanai rocket-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sinthanai rocket-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Seyalil nee jet-tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seyalil nee jet-tu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanangirom thalaiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanangirom thalaiva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sorva vittu nee verva kottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sorva vittu nee verva kottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan paarvai patta pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan paarvai patta pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sorva vittu nee verva kottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sorva vittu nee verva kottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan paarvai patta pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan paarvai patta pothum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Sathikka pirantha socrates-eh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sathikka pirantha socrates-eh"/>
</div>
</pre>
