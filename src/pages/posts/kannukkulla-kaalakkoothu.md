---
title: "kannukkulla song lyrics"
album: "Kaalakkoothu"
artist: "Justin Prabhakaran"
lyricist: "Kattalai Jaya"
director: "M. Nagarajan"
path: "/albums/kaalakkoothu-lyrics"
song: "Kannukkulla"
image: ../../images/albumart/kaalakkoothu.jpg
date: 2018-05-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/TaKTHY8QYZo"
type: "love"
singers:
  - Sathyaprakash
  - Sharanya Srinivas
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hmmmmmmmmmmmmmmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmmmmmmmmmmmmmmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmmmmmmmmmmmm mmmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmmmmmmmmmmmm mmmm"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannukkulla vachuruken
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannukkulla vachuruken"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjukkulla thachuruken
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjukkulla thachuruken"/>
</div>
<div class="lyrico-lyrics-wrapper">Moochu kaathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moochu kaathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan kalakka vaarenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan kalakka vaarenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna kollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna kollum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un nenappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un nenappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullukkulla baththirama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullukkulla baththirama"/>
</div>
<div class="lyrico-lyrics-wrapper">Seththu vachu soththezhuthi thaarenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seththu vachu soththezhuthi thaarenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adiyoda en manasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiyoda en manasa"/>
</div>
<div class="lyrico-lyrics-wrapper">Madiyoda saachavalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madiyoda saachavalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Inga enkitta irukkum usurayumthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga enkitta irukkum usurayumthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi ippothae unakku tharen vaariya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi ippothae unakku tharen vaariya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannukkulla vachuruken
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannukkulla vachuruken"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjukkulla thachuruken
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjukkulla thachuruken"/>
</div>
<div class="lyrico-lyrics-wrapper">Moochu kaathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moochu kaathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan kalakka vaarenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan kalakka vaarenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna kollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna kollum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un nenappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un nenappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullukkulla baththirama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullukkulla baththirama"/>
</div>
<div class="lyrico-lyrics-wrapper">Seththu vachu soththezhuthi thaarenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seththu vachu soththezhuthi thaarenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Onna naan suththi paakanumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna naan suththi paakanumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sokkithaan neththi verkkanumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sokkithaan neththi verkkanumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enga ompuththi poguradho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga ompuththi poguradho"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna nee konjam aala vidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna nee konjam aala vidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aasaiya nerungayula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasaiya nerungayula"/>
</div>
<div class="lyrico-lyrics-wrapper">Lesa nee odhungiriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lesa nee odhungiriyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan unna senthidum nerathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan unna senthidum nerathula"/>
</div>
<div class="lyrico-lyrics-wrapper">Nam thooramum odumae thoorathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam thooramum odumae thoorathula"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuli ver vidura nenju eerathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuli ver vidura nenju eerathula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeyum kanneera sindhuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum kanneera sindhuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum vaaduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum vaaduven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannukkulla vachuruken
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannukkulla vachuruken"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjukkulla thachuruken
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjukkulla thachuruken"/>
</div>
<div class="lyrico-lyrics-wrapper">Moochu kaathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moochu kaathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan kalakka vaarenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan kalakka vaarenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ahaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna kollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna kollum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un nenappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un nenappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullukkulla baththirama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullukkulla baththirama"/>
</div>
<div class="lyrico-lyrics-wrapper">Seththu vachu soththezhuthi thaarenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seththu vachu soththezhuthi thaarenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uchchi monthu naan paakaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uchchi monthu naan paakaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil aasaiyum ooruthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil aasaiyum ooruthadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poththi naan vecha aasaiyellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poththi naan vecha aasaiyellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Muththam nee thantha theerumada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muththam nee thantha theerumada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paasama moraikiriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paasama moraikiriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kovama Anaikiriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kovama Anaikiriyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En kannu neeyinu aana pinnae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kannu neeyinu aana pinnae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan paakkura verentha aanum pennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan paakkura verentha aanum pennae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga pogayilum neeyum venum munnae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga pogayilum neeyum venum munnae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Intha jenmaththil unmaththam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha jenmaththil unmaththam"/>
</div>
<div class="lyrico-lyrics-wrapper">Un mel aaguren…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un mel aaguren…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannukkulla mmmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannukkulla mmmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmm hmmm mmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm hmmm mmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolraale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolraale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna kollum un nenappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna kollum un nenappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullukkulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullukkulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppudi eppudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppudi eppudi"/>
</div>
<div class="lyrico-lyrics-wrapper">Seththu vechu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seththu vechu "/>
</div>
<div class="lyrico-lyrics-wrapper">soththezhudhi tharuviyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="soththezhudhi tharuviyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adiyoda en manasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiyoda en manasa"/>
</div>
<div class="lyrico-lyrics-wrapper">Madiyoda saachavalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madiyoda saachavalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Inga enkitta irukkum usurayumthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga enkitta irukkum usurayumthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi ippothae unakku tharen vaarenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi ippothae unakku tharen vaarenae"/>
</div>
</pre>
