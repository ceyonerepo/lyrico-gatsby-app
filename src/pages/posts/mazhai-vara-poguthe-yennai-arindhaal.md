---
title: "mazhi vara poguthe song lyrics"
album: "Yennai Arindhaal"
artist: "Harris Jayaraj"
lyricist: "Thamarai"
director: "Gautham Vasudev Menon"
path: "/albums/yennai-arindhaal-lyrics"
song: "Mazhi Vara Poguthe"
image: ../../images/albumart/yennai-arindhaal.jpg
date: 2015-02-06
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Nz1iHi0fhzI"
type: "Love"
singers:
  - Karthik
  - Emcee Jesz
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Mazhai Vara Pogudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai Vara Pogudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuligalum Thooruthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuligalum Thooruthey"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanaiyaamal Enna Seiven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanaiyaamal Enna Seiven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Malarvanam Mooduthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malarvanam Mooduthey"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhuramum Oorudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhuramum Oorudhey"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholaiyaamal Enge Poven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholaiyaamal Enge Poven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pugai Pola Ven Panjaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pugai Pola Ven Panjaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Midhakkindra En Nenjai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Midhakkindra En Nenjai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethai Seithu Meetpen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethai Seithu Meetpen"/>
</div>
<div class="lyrico-lyrics-wrapper">Evar Solli Ketpen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evar Solli Ketpen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadal Pondra Kannaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadal Pondra Kannaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Vaari Sendraale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Vaari Sendraale"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilanthene indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilanthene indru"/>
</div>
<div class="lyrico-lyrics-wrapper">Irunthaalum Nandru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irunthaalum Nandru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Annal Mele Konjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annal Mele Konjam"/>
</div>
<div class="lyrico-lyrics-wrapper">Punal Mele Konjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Punal Mele Konjam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadumaari Nirkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadumaari Nirkum"/>
</div>
<div class="lyrico-lyrics-wrapper">En Nenjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nenjam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mazhai Vara Pogudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai Vara Pogudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuligalum Thooruthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuligalum Thooruthey"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanaiyaamal Enna Seiven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanaiyaamal Enna Seiven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Malarvanam Mooduthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malarvanam Mooduthey"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhuramum Oorudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhuramum Oorudhey"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholaiyaamal Enge Poven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholaiyaamal Enge Poven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karu Karu Kangalaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karu Karu Kangalaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kayalvizhi Kolgiraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kayalvizhi Kolgiraal"/>
</div>
<div class="lyrico-lyrics-wrapper">Valithaalum Yetho Sugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valithaalum Yetho Sugam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kulivilum Kannaththil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulivilum Kannaththil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudiyiru Engiraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudiyiru Engiraal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilaiyillaa Aayul Varam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilaiyillaa Aayul Varam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nila Thoongum Megaththil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nila Thoongum Megaththil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanaa Kaanum Neraththil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanaa Kaanum Neraththil"/>
</div>
<div class="lyrico-lyrics-wrapper">Avalthaane Vandhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avalthaane Vandhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Anaikkaamal Sendraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anaikkaamal Sendraal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">imai Rendum Moodaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="imai Rendum Moodaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Urakkangal Vaaraathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urakkangal Vaaraathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Athai Kadhal Endraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athai Kadhal Endraal"/>
</div>
<div class="lyrico-lyrics-wrapper">Avalthaane Thanthaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avalthaane Thanthaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maranthaalum Unnai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maranthaalum Unnai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadanthaalum Pinne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadanthaalum Pinne"/>
</div>
<div class="lyrico-lyrics-wrapper">Manamengum Aval Niyabagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manamengum Aval Niyabagam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannai Katti Vittaal Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannai Katti Vittaal Kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattaamboochchi Poovai Thedum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattaamboochchi Poovai Thedum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhai Endraal Mannai Thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai Endraal Mannai Thaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhu Serum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhu Serum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannai Katti Vittaal Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannai Katti Vittaal Kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattaamboochchi Poovai Thedum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattaamboochchi Poovai Thedum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhai Endraal Mannai Thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai Endraal Mannai Thaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhu Serum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhu Serum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ei Entha Pakkam Nirkindraayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ei Entha Pakkam Nirkindraayo"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha Pakkam Kangal Pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Pakkam Kangal Pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Munnum Pinnum Nee Nadanthaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munnum Pinnum Nee Nadanthaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Oonjalaadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oonjalaadum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Suzhalum Mayil Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suzhalum Mayil Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Thogai En Thozhil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Thogai En Thozhil"/>
</div>
<div class="lyrico-lyrics-wrapper">Sugamaai Puralum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sugamaai Puralum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarpen En Vaazhnaalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarpen En Vaazhnaalil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mazhai Vara Pogudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai Vara Pogudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuligalum Thooruthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuligalum Thooruthey"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanaiyaamal Enna Seiven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanaiyaamal Enna Seiven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Malarvanam Mooduthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malarvanam Mooduthey"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhuramum Oorudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhuramum Oorudhey"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholaiyaamal Enge Poven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholaiyaamal Enge Poven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pugai Pola Ven Panjaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pugai Pola Ven Panjaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Midhakkindra En Nenjai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Midhakkindra En Nenjai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethai Seithu Meetpen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethai Seithu Meetpen"/>
</div>
<div class="lyrico-lyrics-wrapper">Evar Solli Ketpen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evar Solli Ketpen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadal Pondra Kannaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadal Pondra Kannaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Vaari Sendraale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Vaari Sendraale"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilanthene indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilanthene indru"/>
</div>
<div class="lyrico-lyrics-wrapper">Irunthaalum Nandru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irunthaalum Nandru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Annal Mele Konjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annal Mele Konjam"/>
</div>
<div class="lyrico-lyrics-wrapper">Punal Mele Konjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Punal Mele Konjam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadumaari Nirkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadumaari Nirkum"/>
</div>
<div class="lyrico-lyrics-wrapper">En Nenjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nenjam"/>
</div>
</pre>
