---
title: "kalakkalu mr.localu song lyrics"
album: "Mr. Local"
artist: "Hiphop Tamizha"
lyricist: "K.R. Dharan"
director: "M. Rajesh"
path: "/albums/mr-local-lyrics"
song: "Kalakkalu Mr.Localu"
image: ../../images/albumart/mr-local.jpg
date: 2019-05-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/v9iYAsoX5t8"
type: "mass"
singers:
  - Sivakarthikeyan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hey Pakka Middle Class su da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Pakka Middle Class su da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Status Ellaam Dhoosu Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Status Ellaam Dhoosu Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pesi Paaru Aduththa Nimisham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesi Paaru Aduththa Nimisham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aayiruva Dhosthu Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayiruva Dhosthu Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalakkalu Mr.Localu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalakkalu Mr.Localu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Kalakkalu Mr.Localu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Kalakkalu Mr.Localu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enga Ottu Moththa Areavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Ottu Moththa Areavum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engaloda Guestu Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engaloda Guestu Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sontha Bantham Rendavuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sontha Bantham Rendavuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Natuputhaana First tu Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natuputhaana First tu Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalakkalu Mr.Localu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalakkalu Mr.Localu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Kalakkalu Mr.Localu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Kalakkalu Mr.Localu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Vellam Vantha Varuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Vellam Vantha Varuvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ullathellaam Tharuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullathellaam Tharuvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enga Makkaloda Thevai Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Makkaloda Thevai Ellaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poraadiye Peruvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraadiye Peruvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ishta Pattu Ulaippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishta Pattu Ulaippom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kashtam Vantha Sirippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kashtam Vantha Sirippom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mutti Mothi Mela Yeri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mutti Mothi Mela Yeri"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uchchathukke Parappom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uchchathukke Parappom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Appan Kaasa Namba Maattom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appan Kaasa Namba Maattom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sontha Uzhaippula Varuvom Mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sontha Uzhaippula Varuvom Mela"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Benz Su Car ru Perusu illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Benz Su Car ru Perusu illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Friendship Irukku Athukku Mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friendship Irukku Athukku Mela"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Kalakkalu Mr.Localu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Kalakkalu Mr.Localu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Velagu Velagu Velagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Velagu Velagu Velagu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Velagu Velagu Velagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Velagu Velagu Velagu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivan Kashtathula Kooda Nippaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Kashtathula Kooda Nippaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nambikkaiyaa Pazhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nambikkaiyaa Pazhagu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Velagu Velagu Velagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Velagu Velagu Velagu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Velagu Velagu Velagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Velagu Velagu Velagu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivan Ellaarukkum Chella Pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Ellaarukkum Chella Pulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ullam Romba Azhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullam Romba Azhagu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oththa Bike Kum Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oththa Bike Kum Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ottaiyaa Ponaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ottaiyaa Ponaalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanga Therra Pola Paathukkuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanga Therra Pola Paathukkuvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Costly Phone nu Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Costly Phone nu Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaiyila Irunthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyila Irunthaalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oc Wifi Ku Yengiduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oc Wifi Ku Yengiduvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Emi Kattiye Kattiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emi Kattiye Kattiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karanju Pochu En Sambalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karanju Pochu En Sambalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Naalu Intha City Ye City Ye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Naalu Intha City Ye City Ye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Virikkum Sigappu Kambalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Virikkum Sigappu Kambalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Aanaalum Santhosam Karanju Pogala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Aanaalum Santhosam Karanju Pogala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kavalai illa En Lifu Kulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavalai illa En Lifu Kulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Entha Thadainga Vanthaalum Parava illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entha Thadainga Vanthaalum Parava illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thunivu Kuraiyaathu Manasukkulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunivu Kuraiyaathu Manasukkulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kudumbam Sirikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudumbam Sirikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhinamum Uzhaippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinamum Uzhaippom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Appa Ammaakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appa Ammaakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Usuraiye Koduppom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usuraiye Koduppom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Localu Mr.Localu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Localu Mr.Localu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Pakka Middle Class su da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Pakka Middle Class su da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Status Ellaam Dhoosu Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Status Ellaam Dhoosu Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pesi Paaru Aduththa Nimisham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesi Paaru Aduththa Nimisham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aayiruva Dhosthu Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayiruva Dhosthu Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalakkalu Mr.Localu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalakkalu Mr.Localu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Kalakkalu Mr.Localu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Kalakkalu Mr.Localu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enga Ottu Moththa Areavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Ottu Moththa Areavum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engaloda Guestu Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engaloda Guestu Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sontha Bantham Rendavuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sontha Bantham Rendavuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Natuputhaana First tu Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natuputhaana First tu Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalakkalu Mr.Localu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalakkalu Mr.Localu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Kalakkalu Mr.Localu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Kalakkalu Mr.Localu"/>
</div>
</pre>
