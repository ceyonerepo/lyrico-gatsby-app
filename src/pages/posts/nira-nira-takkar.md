---
title: 'nira song lyrics'
album: 'Takkar'
artist: 'Nivas K. Prasanna'
lyricist: 'Ku Karthik'
director: 'Karthik G Krish'
path: '/albums/takkar-song-lyrics'
song: 'Nira Nira'
image: ../../images/albumart/takkar.jpg
date: 2020-01-01
singers: 
- Sid Sriram
- Gautham Vasudev Menon
- Malvi Sundaresan
lang: tamil
youtubeLink: 'https://www.youtube.com/embed/4jTy5jnMkYc'
type: 'sad'
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Niraa niraa nee en niraa
<input type="checkbox" class="lyrico-select-lyric-line" value="Niraa niraa nee en niraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiraa thiraa ninaithiraa
<input type="checkbox" class="lyrico-select-lyric-line" value="Thiraa thiraa ninaithiraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nodi sugam tharaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Nodi sugam tharaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhi yugam vidaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Vazhi yugam vidaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vizhiyilae oru keeralae
<input type="checkbox" class="lyrico-select-lyric-line" value="Vizhiyilae oru keeralae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhundhaathae theriyaamalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Vizhundhaathae theriyaamalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharaiyilae nizhal veguthae
<input type="checkbox" class="lyrico-select-lyric-line" value="Tharaiyilae nizhal veguthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanimayai ariyaamalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Thanimayai ariyaamalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ninaivugal vilaiyaaduthae
<input type="checkbox" class="lyrico-select-lyric-line" value="Ninaivugal vilaiyaaduthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nijam adhu puriyaamalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Nijam adhu puriyaamalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhazhgalum thirakaamalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Idhazhgalum thirakaamalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayangal inainthida uyir pizhidhidum
<input type="checkbox" class="lyrico-select-lyric-line" value="Idhayangal inainthida uyir pizhidhidum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Pogaathae azhagae
<input type="checkbox" class="lyrico-select-lyric-line" value="Pogaathae azhagae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini thaangaathae uyirae
<input type="checkbox" class="lyrico-select-lyric-line" value="Ini thaangaathae uyirae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai thondaathae thimirae
<input type="checkbox" class="lyrico-select-lyric-line" value="Enai thondaathae thimirae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagal vesham podaathae
<input type="checkbox" class="lyrico-select-lyric-line" value="Pagal vesham podaathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Unai theeramal pidithen
<input type="checkbox" class="lyrico-select-lyric-line" value="Unai theeramal pidithen"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirin ullae maraithen
<input type="checkbox" class="lyrico-select-lyric-line" value="Uyirin ullae maraithen"/>
</div>
<div class="lyrico-lyrics-wrapper">Veliyil konjam nadithenae
<input type="checkbox" class="lyrico-select-lyric-line" value="Veliyil konjam nadithenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Niraa niraa nee en niraa
<input type="checkbox" class="lyrico-select-lyric-line" value="Niraa niraa nee en niraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiraa thiraa ninaithiraa
<input type="checkbox" class="lyrico-select-lyric-line" value="Thiraa thiraa ninaithiraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nodi sugam tharaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Nodi sugam tharaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhi yugam vidaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Vazhi yugam vidaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Niraa…
<input type="checkbox" class="lyrico-select-lyric-line" value="Niraa…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thiraa….
<input type="checkbox" class="lyrico-select-lyric-line" value="Thiraa…."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Niraa…thiraa
<input type="checkbox" class="lyrico-select-lyric-line" value="Niraa…thiraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Niraa…aaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Niraa…aaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Nodigal thaavi odum
<input type="checkbox" class="lyrico-select-lyric-line" value="Nodigal thaavi odum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mutkalodu sandaiyittu
<input type="checkbox" class="lyrico-select-lyric-line" value="Mutkalodu sandaiyittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandha padhai poga solli
<input type="checkbox" class="lyrico-select-lyric-line" value="Vandha padhai poga solli"/>
</div>
<div class="lyrico-lyrics-wrapper">Netrai meendum ketten
<input type="checkbox" class="lyrico-select-lyric-line" value="Netrai meendum ketten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Urugi urugi neeyum
<input type="checkbox" class="lyrico-select-lyric-line" value="Urugi urugi neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ularipona vaarthai yaavum
<input type="checkbox" class="lyrico-select-lyric-line" value="Ularipona vaarthai yaavum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nyabagathil thedi thedi
<input type="checkbox" class="lyrico-select-lyric-line" value="Nyabagathil thedi thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhil kettu paarthen
<input type="checkbox" class="lyrico-select-lyric-line" value="Kadhil kettu paarthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Undhan madiyil naanum
<input type="checkbox" class="lyrico-select-lyric-line" value="Undhan madiyil naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Urangi pona tharunam thannai
<input type="checkbox" class="lyrico-select-lyric-line" value="Urangi pona tharunam thannai"/>
</div>
<div class="lyrico-lyrics-wrapper">Padam piditha minnalodu
<input type="checkbox" class="lyrico-select-lyric-line" value="Padam piditha minnalodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pugai padangal ketten
<input type="checkbox" class="lyrico-select-lyric-line" value="Pugai padangal ketten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Udhadum udhadum
<input type="checkbox" class="lyrico-select-lyric-line" value="Udhadum udhadum"/>
</div>
<div class="lyrico-lyrics-wrapper">Urasi uyir paritha
<input type="checkbox" class="lyrico-select-lyric-line" value="Urasi uyir paritha"/>
</div>
<div class="lyrico-lyrics-wrapper">Saththam yaavum padhivu seidhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Saththam yaavum padhivu seidhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Serthu vaitha ilaigal thulaiyil
<input type="checkbox" class="lyrico-select-lyric-line" value="Serthu vaitha ilaigal thulaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Etti paarthen
<input type="checkbox" class="lyrico-select-lyric-line" value="Etti paarthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Mezhugin thiriyil eriyum
<input type="checkbox" class="lyrico-select-lyric-line" value="Mezhugin thiriyil eriyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeyai vandhaai
<input type="checkbox" class="lyrico-select-lyric-line" value="Theeyai vandhaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mezhugin udalai mella
<input type="checkbox" class="lyrico-select-lyric-line" value="Mezhugin udalai mella"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeno thindraai
<input type="checkbox" class="lyrico-select-lyric-line" value="Yeno thindraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Undhan moochu kaatrai
<input type="checkbox" class="lyrico-select-lyric-line" value="Undhan moochu kaatrai"/>
</div>
<div class="lyrico-lyrics-wrapper">Oodhi ponaai pizhaithiduven adi
<input type="checkbox" class="lyrico-select-lyric-line" value="Oodhi ponaai pizhaithiduven adi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Tharaiyil thavazhum kaadhal
<input type="checkbox" class="lyrico-select-lyric-line" value="Tharaiyil thavazhum kaadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarthaal enna
<input type="checkbox" class="lyrico-select-lyric-line" value="Paarthaal enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam pesi pesi theerthaal enna
<input type="checkbox" class="lyrico-select-lyric-line" value="Konjam pesi pesi theerthaal enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha kaalam neram ellaam
<input type="checkbox" class="lyrico-select-lyric-line" value="Indha kaalam neram ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru murai kanavaai kalainthiduma
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru murai kanavaai kalainthiduma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Unai theeramal pidithen
<input type="checkbox" class="lyrico-select-lyric-line" value="Unai theeramal pidithen"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirin ullae maraithen
<input type="checkbox" class="lyrico-select-lyric-line" value="Uyirin ullae maraithen"/>
</div>
<div class="lyrico-lyrics-wrapper">Veliyil konjam nadithenae
<input type="checkbox" class="lyrico-select-lyric-line" value="Veliyil konjam nadithenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vizhiyilae oru keeralae
<input type="checkbox" class="lyrico-select-lyric-line" value="Vizhiyilae oru keeralae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhundhaathae theriyaamalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Vizhundhaathae theriyaamalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharaiyilae nizhal veguthae
<input type="checkbox" class="lyrico-select-lyric-line" value="Tharaiyilae nizhal veguthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanimayai ariyaamalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Thanimayai ariyaamalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ninaivugal vilaiyaaduthae
<input type="checkbox" class="lyrico-select-lyric-line" value="Ninaivugal vilaiyaaduthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nijam adhu puriyaamalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Nijam adhu puriyaamalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhazhgalum thirakaamalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Idhazhgalum thirakaamalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayangal inainthida uyir pizhidhidum
<input type="checkbox" class="lyrico-select-lyric-line" value="Idhayangal inainthida uyir pizhidhidum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Pogaathae azhagae
<input type="checkbox" class="lyrico-select-lyric-line" value="Pogaathae azhagae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini thaangaathae uyirae
<input type="checkbox" class="lyrico-select-lyric-line" value="Ini thaangaathae uyirae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai thondaathae thimirae
<input type="checkbox" class="lyrico-select-lyric-line" value="Enai thondaathae thimirae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagal vesham podaathae
<input type="checkbox" class="lyrico-select-lyric-line" value="Pagal vesham podaathae"/>
</div>
  <div class="lyrico-lyrics-wrapper">Vesham podaathae
<input type="checkbox" class="lyrico-select-lyric-line" value="Vesham podaathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Unai theeramal pidithen
<input type="checkbox" class="lyrico-select-lyric-line" value="Unai theeramal pidithen"/>
</div>
  <div class="lyrico-lyrics-wrapper">Theeramal pidithen
<input type="checkbox" class="lyrico-select-lyric-line" value="Theeramal pidithen"/>
</div>
  <div class="lyrico-lyrics-wrapper">Uyirin ullae maraithen
<input type="checkbox" class="lyrico-select-lyric-line" value="Uyirin ullae maraithen"/>
</div>
  <div class="lyrico-lyrics-wrapper">Maraithen
<input type="checkbox" class="lyrico-select-lyric-line" value="Maraithen"/>
</div>
  <div class="lyrico-lyrics-wrapper">Veliyil konjam nadithenae
<input type="checkbox" class="lyrico-select-lyric-line" value="Veliyil konjam nadithenae"/>
</div>
  <div class="lyrico-lyrics-wrapper">Nadithenae
<input type="checkbox" class="lyrico-select-lyric-line" value="Nadithenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Niraa
<input type="checkbox" class="lyrico-select-lyric-line" value="Niraa"/>
</div>
  <div class="lyrico-lyrics-wrapper">Niraa
<input type="checkbox" class="lyrico-select-lyric-line" value="Niraa"/>
</div>
  <div class="lyrico-lyrics-wrapper">Niraa
<input type="checkbox" class="lyrico-select-lyric-line" value="Niraa"/>
</div>
  <div class="lyrico-lyrics-wrapper">Niraa
<input type="checkbox" class="lyrico-select-lyric-line" value="Niraa"/>
</div>
  <div class="lyrico-lyrics-wrapper">Nee en niraa
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee en niraa"/>
</div>
  <div class="lyrico-lyrics-wrapper">Nee en niraa
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee en niraa"/>
</div>
  <div class="lyrico-lyrics-wrapper">Thiraa   Thiraa
<input type="checkbox" class="lyrico-select-lyric-line" value="Thiraa   Thiraa"/>
</div>
  <div class="lyrico-lyrics-wrapper">Thiraa   Thiraa
<input type="checkbox" class="lyrico-select-lyric-line" value="Thiraa   Thiraa"/>
</div>
  <div class="lyrico-lyrics-wrapper">Ninaithiraa   Ninaithiraa
<input type="checkbox" class="lyrico-select-lyric-line" value="Ninaithiraa   Ninaithiraa"/>
</div>
  <div class="lyrico-lyrics-wrapper">Nodi sugam tharaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Nodi sugam tharaa"/>
</div>
  <div class="lyrico-lyrics-wrapper">Tharaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Tharaa"/>
</div>
  <div class="lyrico-lyrics-wrapper">Vazhi yugam vidaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Vazhi yugam vidaa"/>
</div>
  <div class="lyrico-lyrics-wrapper">Vidaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Vidaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">I’ts not her face
<input type="checkbox" class="lyrico-select-lyric-line" value="I’ts not her face"/>
</div>
<div class="lyrico-lyrics-wrapper">It’s something about her
<input type="checkbox" class="lyrico-select-lyric-line" value="It’s something about her"/>
</div>
<div class="lyrico-lyrics-wrapper">It’s just being in the right place with her
<input type="checkbox" class="lyrico-select-lyric-line" value="It’s just being in the right place with her"/>
</div>
<div class="lyrico-lyrics-wrapper">It’s just the moment
<input type="checkbox" class="lyrico-select-lyric-line" value="It’s just the moment"/>
</div>
<div class="lyrico-lyrics-wrapper">It’s everything about her man
<input type="checkbox" class="lyrico-select-lyric-line" value="It’s everything about her man"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">I don’t know what to say
<input type="checkbox" class="lyrico-select-lyric-line" value="I don’t know what to say"/>
</div>
<div class="lyrico-lyrics-wrapper">It’s everything about her
<input type="checkbox" class="lyrico-select-lyric-line" value="It’s everything about her"/>
</div>
<div class="lyrico-lyrics-wrapper">No it’s not her face
<input type="checkbox" class="lyrico-select-lyric-line" value="No it’s not her face"/>
</div>
<div class="lyrico-lyrics-wrapper">It’s beyond that
<input type="checkbox" class="lyrico-select-lyric-line" value="It’s beyond that"/>
</div>
</pre>