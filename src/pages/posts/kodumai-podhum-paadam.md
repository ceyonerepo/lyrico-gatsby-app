---
title: "kodumai podhum song lyrics"
album: "Paadam"
artist: "Ganesh Raghavendra"
lyricist: "Piraisudan"
director: "Rajasekhar"
path: "/albums/paadam-song-lyrics"
song: "Kodumai Podhum"
image: ../../images/albumart/paadam.jpg
date: 2018-04-27
lang: tamil
youtubeLink: "https://www.youtube.com/embed/OWRDxwyjIkA"
type: "sad"
singers:
  - Ganesh Ragavendhra
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">kodumai pothum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kodumai pothum "/>
</div>
<div class="lyrico-lyrics-wrapper">thinamum vedhanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thinamum vedhanai"/>
</div>
<div class="lyrico-lyrics-wrapper">siluvai baaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="siluvai baaram"/>
</div>
<div class="lyrico-lyrics-wrapper">kangalil eeram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kangalil eeram"/>
</div>
<div class="lyrico-lyrics-wrapper">pala pala baashai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pala pala baashai"/>
</div>
<div class="lyrico-lyrics-wrapper">mozhigalin oosai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mozhigalin oosai"/>
</div>
<div class="lyrico-lyrics-wrapper">puriyave illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puriyave illai"/>
</div>
<div class="lyrico-lyrics-wrapper">thinam thinam thollai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thinam thinam thollai"/>
</div>
<div class="lyrico-lyrics-wrapper">paadam paadam manapaadam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paadam paadam manapaadam"/>
</div>
<div class="lyrico-lyrics-wrapper">seithen onnum aerala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seithen onnum aerala"/>
</div>
<div class="lyrico-lyrics-wrapper">palli paadam mathipeeda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="palli paadam mathipeeda"/>
</div>
<div class="lyrico-lyrics-wrapper">pola ennum maarala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pola ennum maarala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kalimanna thalaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalimanna thalaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">kai kaalu nadukathil kedaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kai kaalu nadukathil kedaku"/>
</div>
<div class="lyrico-lyrics-wrapper">aiaiyo payama iruku enaku than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aiaiyo payama iruku enaku than"/>
</div>
<div class="lyrico-lyrics-wrapper">thookathil polamburen nane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thookathil polamburen nane"/>
</div>
<div class="lyrico-lyrics-wrapper">thugathil alaiyuren veene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thugathil alaiyuren veene"/>
</div>
<div class="lyrico-lyrics-wrapper">naragathil kooda 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naragathil kooda "/>
</div>
<div class="lyrico-lyrics-wrapper">enna vidunga pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna vidunga pa"/>
</div>
<div class="lyrico-lyrics-wrapper">thalaiyil aerathe english 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalaiyil aerathe english "/>
</div>
<div class="lyrico-lyrics-wrapper">eh padikirene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eh padikirene"/>
</div>
<div class="lyrico-lyrics-wrapper">pambu puthukul kaiya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pambu puthukul kaiya "/>
</div>
<div class="lyrico-lyrics-wrapper">vitu thudikirene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vitu thudikirene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kodumai pothum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kodumai pothum "/>
</div>
<div class="lyrico-lyrics-wrapper">thinamum vedhanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thinamum vedhanai"/>
</div>
<div class="lyrico-lyrics-wrapper">siluvai baaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="siluvai baaram"/>
</div>
<div class="lyrico-lyrics-wrapper">kangalil eeram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kangalil eeram"/>
</div>
<div class="lyrico-lyrics-wrapper">pala pala baashai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pala pala baashai"/>
</div>
<div class="lyrico-lyrics-wrapper">mozhigalin oosai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mozhigalin oosai"/>
</div>
<div class="lyrico-lyrics-wrapper">puriyave illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puriyave illai"/>
</div>
<div class="lyrico-lyrics-wrapper">thinam thinam thollai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thinam thinam thollai"/>
</div>
<div class="lyrico-lyrics-wrapper">paadam paadam manapaadam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paadam paadam manapaadam"/>
</div>
<div class="lyrico-lyrics-wrapper">seithen onnum aerala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seithen onnum aerala"/>
</div>
<div class="lyrico-lyrics-wrapper">palli paadam mathipeeda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="palli paadam mathipeeda"/>
</div>
<div class="lyrico-lyrics-wrapper">pola ennum maarala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pola ennum maarala"/>
</div>
</pre>
