---
title: "paadhakathee penne song lyrics"
album: "Margazhi 16"
artist: "E.K. Bobby"
lyricist: "Priyan"
director: "K. Stephen"
path: "/albums/margazhi-16-lyrics"
song: "Paadhakathee Penne"
image: ../../images/albumart/margazhi-16.jpg
date: 2011-02-18
lang: tamil
youtubeLink: "https://www.youtube.com/embed/nj3BoGFn00k"
type: "sad"
singers:
  - Karthik
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Thaananna thannanney paadhakkaththi ponney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaananna thannanney paadhakkaththi ponney"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaananna thannanney kaadhalikka vandhavaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaananna thannanney kaadhalikka vandhavaley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paadhakkaththi ponney enna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadhakkaththi ponney enna "/>
</div>
<div class="lyrico-lyrics-wrapper">kaadhalikka vandhavaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadhalikka vandhavaley"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjikkulla nanja vachchi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjikkulla nanja vachchi "/>
</div>
<div class="lyrico-lyrics-wrapper">enna vittuppoanavaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna vittuppoanavaley"/>
</div>
<div class="lyrico-lyrics-wrapper">usurukkul nerappivachen unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="usurukkul nerappivachen unna"/>
</div>
<div class="lyrico-lyrics-wrapper">nee kannakkatti kaattula vittadhu enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee kannakkatti kaattula vittadhu enna"/>
</div>
<div class="lyrico-lyrics-wrapper">ennamittu engey neeyum poana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennamittu engey neeyum poana"/>
</div>
<div class="lyrico-lyrics-wrapper">idha kaaththkkooda theeyaa sududhu enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idha kaaththkkooda theeyaa sududhu enna"/>
</div>
<div class="lyrico-lyrics-wrapper">uyir moochu odainjuppoachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyir moochu odainjuppoachu"/>
</div>
<div class="lyrico-lyrics-wrapper">udal sallada salladaiyaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="udal sallada salladaiyaachu"/>
</div>
<div class="lyrico-lyrics-wrapper">nee illai enna aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee illai enna aachu"/>
</div>
<div class="lyrico-lyrics-wrapper">indha ulagam peruththu poachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indha ulagam peruththu poachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paadhakkaththi ponney enna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadhakkaththi ponney enna "/>
</div>
<div class="lyrico-lyrics-wrapper">kaadhalikka vandhavaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadhalikka vandhavaley"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjikkulla nanja vachchi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjikkulla nanja vachchi "/>
</div>
<div class="lyrico-lyrics-wrapper">enna vittuppoanavaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna vittuppoanavaley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannukkettum thooram adha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannukkettum thooram adha "/>
</div>
<div class="lyrico-lyrics-wrapper">paarththupputten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paarththupputten"/>
</div>
<div class="lyrico-lyrics-wrapper">en nenjikkulla koattaikkatti 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en nenjikkulla koattaikkatti "/>
</div>
<div class="lyrico-lyrics-wrapper">vachchadhellaam veenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vachchadhellaam veenu"/>
</div>
<div class="lyrico-lyrics-wrapper">moagam paarkka kaaththu kedandhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moagam paarkka kaaththu kedandhen"/>
</div>
<div class="lyrico-lyrics-wrapper">mezhugaa karainjen onna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mezhugaa karainjen onna "/>
</div>
<div class="lyrico-lyrics-wrapper">kaanala engo tholainjen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaanala engo tholainjen"/>
</div>
<div class="lyrico-lyrics-wrapper">nenaicheththavichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenaicheththavichen"/>
</div>
<div class="lyrico-lyrics-wrapper">nee vidhaicha paadhaiyellaam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee vidhaicha paadhaiyellaam "/>
</div>
<div class="lyrico-lyrics-wrapper">vaasamulla poo mulaikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaasamulla poo mulaikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">andha poovaai naanirukka naadhiyilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="andha poovaai naanirukka naadhiyilla"/>
</div>
<div class="lyrico-lyrics-wrapper">nee pakkathil irundha kaalam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee pakkathil irundha kaalam "/>
</div>
<div class="lyrico-lyrics-wrapper">edhiril dheriyudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edhiril dheriyudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">nee marainjuppoan maaya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee marainjuppoan maaya "/>
</div>
<div class="lyrico-lyrics-wrapper">manasu valikkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasu valikkudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paadhakkaththi ponney enna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadhakkaththi ponney enna "/>
</div>
<div class="lyrico-lyrics-wrapper">kaadhalikka vandhavaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadhalikka vandhavaley"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjikkulla nanja vachchi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjikkulla nanja vachchi "/>
</div>
<div class="lyrico-lyrics-wrapper">enna vittuppoanavaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna vittuppoanavaley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nallaa idrundha kaadhalukku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nallaa idrundha kaadhalukku "/>
</div>
<div class="lyrico-lyrics-wrapper">endi ippadi aachchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="endi ippadi aachchu"/>
</div>
<div class="lyrico-lyrics-wrapper">nam joadipporuththam paarththu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nam joadipporuththam paarththu "/>
</div>
<div class="lyrico-lyrics-wrapper">ooril kannuppattu poachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooril kannuppattu poachu"/>
</div>
<div class="lyrico-lyrics-wrapper">vidhippoadura kanakku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidhippoadura kanakku "/>
</div>
<div class="lyrico-lyrics-wrapper">vazhakku puriyala enakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vazhakku puriyala enakku"/>
</div>
<div class="lyrico-lyrics-wrapper">en manasu thavikkum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en manasu thavikkum "/>
</div>
<div class="lyrico-lyrics-wrapper">thavippu theriyala onakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thavippu theriyala onakku"/>
</div>
<div class="lyrico-lyrics-wrapper">vaazhavaangu vaazha vandha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaazhavaangu vaazha vandha "/>
</div>
<div class="lyrico-lyrics-wrapper">aalamaram saanjiruchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aalamaram saanjiruchu"/>
</div>
<div class="lyrico-lyrics-wrapper">aththanaiyum sillu silla 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aththanaiyum sillu silla "/>
</div>
<div class="lyrico-lyrics-wrapper">norungiduchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="norungiduchu"/>
</div>
<div class="lyrico-lyrics-wrapper">pala virisal vizhudha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pala virisal vizhudha "/>
</div>
<div class="lyrico-lyrics-wrapper">manasa serkka vazhiyilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasa serkka vazhiyilla"/>
</div>
<div class="lyrico-lyrics-wrapper">indha thanimaiyoada 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indha thanimaiyoada "/>
</div>
<div class="lyrico-lyrics-wrapper">valiya thaanga mudiyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valiya thaanga mudiyala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paadhakkaththi ponney enna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadhakkaththi ponney enna "/>
</div>
<div class="lyrico-lyrics-wrapper">kaadhalikka vandhavaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadhalikka vandhavaley"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjikkulla nanja vachchi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjikkulla nanja vachchi "/>
</div>
<div class="lyrico-lyrics-wrapper">enna vittuppoanavaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna vittuppoanavaley"/>
</div>
</pre>
