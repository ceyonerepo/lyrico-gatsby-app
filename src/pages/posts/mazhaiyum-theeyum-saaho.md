---
title: "mazhaiyum theeyum song lyrics"
album: "Saaho"
artist: "Guru Randhawa"
lyricist: "Madhan Karky"
director: "Sujeeth"
path: "/albums/saaho-lyrics"
song: "Mazhaiyum Theeyum"
image: ../../images/albumart/saaho.jpg
date: 2019-08-30
lang: tamil
youtubeLink: "https://www.youtube.com/embed/_UANyfgXO8k"
type: "love"
singers:
  - Haricharan
  - Shakthisree Gopalan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yethondrum Sollamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethondrum Sollamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhunthaayae En Melae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhunthaayae En Melae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yarendru Ketenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yarendru Ketenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaan Megam Endrayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaan Megam Endrayae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Cho Vendru Nilaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cho Vendru Nilaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thooralgal En Melae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thooralgal En Melae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Endru Ketenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Endru Ketenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Men Muththam Endrayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Men Muththam Endrayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaai Ennai Nanaithaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaai Ennai Nanaithaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Nenjin Theeyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nenjin Theeyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Vizhumpotho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vizhumpotho"/>
</div>
<div class="lyrico-lyrics-wrapper">Mella Sugankaanutho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mella Sugankaanutho"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnal Maarutho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnal Maarutho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Nenjin Theeyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nenjin Theeyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Vizhumpotho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vizhumpotho"/>
</div>
<div class="lyrico-lyrics-wrapper">Mella Sugankaanutho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mella Sugankaanutho"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnal Maarutho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnal Maarutho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aadamal Kollamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadamal Kollamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirpam Pol Unthan Thee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirpam Pol Unthan Thee"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunindru Un Thegam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunindru Un Thegam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thugal Maarathu Naan Sinthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thugal Maarathu Naan Sinthi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theeramal Neengamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeramal Neengamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaan Engum Ponnandhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaan Engum Ponnandhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Nee Yendha Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Nee Yendha Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Iyam Yen Endru Nee Sinthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iyam Yen Endru Nee Sinthi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaai Ennai Adainthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaai Ennai Adainthaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Nenjin Theeyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Nenjin Theeyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Vizhumpotho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Vizhumpotho"/>
</div>
<div class="lyrico-lyrics-wrapper">Ondrum Anaiyaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondrum Anaiyaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Yellai Megam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Yellai Megam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Nenjin Theeyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nenjin Theeyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Vizhumpotho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vizhumpotho"/>
</div>
<div class="lyrico-lyrics-wrapper">Mella Sugankaanutho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mella Sugankaanutho"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnal Maarutho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnal Maarutho"/>
</div>
</pre>
