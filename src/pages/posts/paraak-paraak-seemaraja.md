---
title: "paraak paraak song lyrics"
album: "Seemaraja"
artist: "D Imman"
lyricist: "Yugabharathi"
director: "Ponram"
path: "/albums/seemaraja-lyrics"
song: "Paraak Paraak"
image: ../../images/albumart/seemaraja.jpg
date: 2018-09-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/SFEH60yHbSM"
type: "happy"
singers:
  - Senthil Ganesh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Rajadhi raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rajadhi raja"/>
</div>
<div class="lyrico-lyrics-wrapper">Raja gambeera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja gambeera"/>
</div>
<div class="lyrico-lyrics-wrapper">Pandiya kula manna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pandiya kula manna"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadambavel raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadambavel raja"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhi vazhiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi vazhiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veppam poo malai soodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veppam poo malai soodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Venghai padaiyodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venghai padaiyodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Singam patti seema raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Singam patti seema raja"/>
</div>
<div class="lyrico-lyrics-wrapper">Paraak paraak paraak
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paraak paraak paraak"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paan paare paare paare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paan paare paare paare"/>
</div>
<div class="lyrico-lyrics-wrapper">Paan paare paare paare paan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paan paare paare paare paan"/>
</div>
<div class="lyrico-lyrics-wrapper">Paraak paraak
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paraak paraak"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paan paare paare paare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paan paare paare paare"/>
</div>
<div class="lyrico-lyrics-wrapper">Paan paare paare paare paan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paan paare paare paare paan"/>
</div>
<div class="lyrico-lyrics-wrapper">Paraak paraak
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paraak paraak"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paan paare paare paare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paan paare paare paare"/>
</div>
<div class="lyrico-lyrics-wrapper">Paan paare paare paare paan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paan paare paare paare paan"/>
</div>
<div class="lyrico-lyrics-wrapper">Paraak paraak 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paraak paraak "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paan paare paare paare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paan paare paare paare"/>
</div>
<div class="lyrico-lyrics-wrapper">Paan paare paare paare paan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paan paare paare paare paan"/>
</div>
<div class="lyrico-lyrics-wrapper">Paraak paraak 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paraak paraak "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ettu ooru ettum padi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettu ooru ettum padi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kondadu vettu vedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondadu vettu vedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thikku therikkum padi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thikku therikkum padi"/>
</div>
<div class="lyrico-lyrics-wrapper">Makka nee naatta pudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Makka nee naatta pudi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varuthu varuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varuthu varuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Varuthu varathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varuthu varathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Valari padai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valari padai"/>
</div>
<div class="lyrico-lyrics-wrapper">Alari olari pathari ethiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alari olari pathari ethiri"/>
</div>
<div class="lyrico-lyrics-wrapper">Egiri vida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Egiri vida"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vettaruva velu kamba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vettaruva velu kamba"/>
</div>
<div class="lyrico-lyrics-wrapper">Mela thooki pudi da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mela thooki pudi da"/>
</div>
<div class="lyrico-lyrics-wrapper">Muttiyaiyum pethudavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muttiyaiyum pethudavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oda oda adi da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oda oda adi da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paan paare paare paare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paan paare paare paare"/>
</div>
<div class="lyrico-lyrics-wrapper">Paan paare paare paare paan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paan paare paare paare paan"/>
</div>
<div class="lyrico-lyrics-wrapper">Paraak paraak
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paraak paraak"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paan paare paare paare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paan paare paare paare"/>
</div>
<div class="lyrico-lyrics-wrapper">Paan paare paare paare paan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paan paare paare paare paan"/>
</div>
<div class="lyrico-lyrics-wrapper">Paraak paraak 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paraak paraak "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ettu ooru ettum padi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettu ooru ettum padi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kondadu vettu vedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondadu vettu vedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thikku therikkum padi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thikku therikkum padi"/>
</div>
<div class="lyrico-lyrics-wrapper">Makka nee naatta pudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Makka nee naatta pudi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varuthu varuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varuthu varuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Varuthu varathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varuthu varathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Asura padai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asura padai"/>
</div>
<div class="lyrico-lyrics-wrapper">Alari olari pathari ethiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alari olari pathari ethiri"/>
</div>
<div class="lyrico-lyrics-wrapper">Egiri vida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Egiri vida"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nethiyila thee eduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nethiyila thee eduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thamizha kaatha inam thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thamizha kaatha inam thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Sathiyamae maanamena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sathiyamae maanamena"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhum veera kuzham thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhum veera kuzham thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paan paare paare paare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paan paare paare paare"/>
</div>
<div class="lyrico-lyrics-wrapper">Paan paare paare paare paan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paan paare paare paare paan"/>
</div>
<div class="lyrico-lyrics-wrapper">Paraak paraak
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paraak paraak"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paan paare paare paare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paan paare paare paare"/>
</div>
<div class="lyrico-lyrics-wrapper">Paan paare paare paare paan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paan paare paare paare paan"/>
</div>
<div class="lyrico-lyrics-wrapper">Paraak paraak 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paraak paraak "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sorimuthu aiyyanar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sorimuthu aiyyanar"/>
</div>
<div class="lyrico-lyrics-wrapper">Kovililae kavadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kovililae kavadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Orumithu thookayilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orumithu thookayilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Osandhidumae un kodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Osandhidumae un kodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sorimuthu aiyyanar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sorimuthu aiyyanar"/>
</div>
<div class="lyrico-lyrics-wrapper">Kovililae kavadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kovililae kavadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Orumithu thookayilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orumithu thookayilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Osandhidumae un kodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Osandhidumae un kodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sorimuthu aiyyanar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sorimuthu aiyyanar"/>
</div>
<div class="lyrico-lyrics-wrapper">Kovililae kavadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kovililae kavadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Orumithu thookayilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orumithu thookayilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Osandhidumae un kodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Osandhidumae un kodi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pootta kootta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pootta kootta"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaandamalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaandamalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazha nenaikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazha nenaikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaai kuzhamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaai kuzhamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Koottai saamy polae ninnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koottai saamy polae ninnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Veetta vagaiya kaathidumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veetta vagaiya kaathidumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aanai ambu senai ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanai ambu senai ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Avasiyamae illaiyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avasiyamae illaiyada"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanai ettum veerathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanai ettum veerathula"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthu thaan ellaiyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthu thaan ellaiyada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vethu nella vervaiyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vethu nella vervaiyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Velaiya vecha koottam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velaiya vecha koottam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ver aalu ullae vantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ver aalu ullae vantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Edukka venum oottam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edukka venum oottam"/>
</div>
<div class="lyrico-lyrics-wrapper">Makka makka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Makka makka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paan paare paare paare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paan paare paare paare"/>
</div>
<div class="lyrico-lyrics-wrapper">Paan paare paare paare paan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paan paare paare paare paan"/>
</div>
<div class="lyrico-lyrics-wrapper">Paraak paraak
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paraak paraak"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paan paare paare paare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paan paare paare paare"/>
</div>
<div class="lyrico-lyrics-wrapper">Paan paare paare paare paan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paan paare paare paare paan"/>
</div>
<div class="lyrico-lyrics-wrapper">Paraak paraak 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paraak paraak "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ettu ooru ettum padi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettu ooru ettum padi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kondadu vettu vedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondadu vettu vedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thikku therikkum padi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thikku therikkum padi"/>
</div>
<div class="lyrico-lyrics-wrapper">Makka nee naatta pudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Makka nee naatta pudi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varuthu varuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varuthu varuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Varuthu varathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varuthu varathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavari padai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavari padai"/>
</div>
<div class="lyrico-lyrics-wrapper">Alari olari pathari ethiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alari olari pathari ethiri"/>
</div>
<div class="lyrico-lyrics-wrapper">Egiri vida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Egiri vida"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andam athu aadidavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andam athu aadidavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aandu partha janam thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aandu partha janam thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thottatthellaam vetri aaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottatthellaam vetri aaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodi kodi jeyam thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodi kodi jeyam thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paan paare paare paare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paan paare paare paare"/>
</div>
<div class="lyrico-lyrics-wrapper">Paan paare paare paare paan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paan paare paare paare paan"/>
</div>
<div class="lyrico-lyrics-wrapper">Paraak paraak
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paraak paraak"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paan paare paare paare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paan paare paare paare"/>
</div>
<div class="lyrico-lyrics-wrapper">Paan paare paare paare paan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paan paare paare paare paan"/>
</div>
<div class="lyrico-lyrics-wrapper">Paraak paraak 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paraak paraak "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paan paare paare paare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paan paare paare paare"/>
</div>
<div class="lyrico-lyrics-wrapper">Paan paare paare paare paan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paan paare paare paare paan"/>
</div>
<div class="lyrico-lyrics-wrapper">Paraak paraak 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paraak paraak "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paan paare paare paare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paan paare paare paare"/>
</div>
<div class="lyrico-lyrics-wrapper">Paan paare paare paare paan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paan paare paare paare paan"/>
</div>
<div class="lyrico-lyrics-wrapper">Paraak paraak 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paraak paraak "/>
</div>
</pre>
