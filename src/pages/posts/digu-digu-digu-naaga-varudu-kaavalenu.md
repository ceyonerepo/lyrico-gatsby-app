---
title: "digu digu digu naaga song lyrics"
album: "Varudu Kaavalenu"
artist: "Vishal Chandrashekhar"
lyricist: "Anantha Sriram"
director: "Lakshmi Sowjanya"
path: "/albums/varudu-kaavalenu-lyrics"
song: "Digu Digu Digu Naaga"
image: ../../images/albumart/varudu-kaavalenu.jpg
date: 2021-10-15
lang: telugu
youtubeLink: "https://www.youtube.com/embed/OHiLeVjHKyI"
type: "happy"
singers:
  - Shreya Ghoshal
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Digu digu digu nag
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Digu digu digu nag"/>
</div>
<div class="lyrico-lyrics-wrapper">Digu digu digu nag
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Digu digu digu nag"/>
</div>
<div class="lyrico-lyrics-wrapper">Digu digu digu naga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Digu digu digu naga"/>
</div>
<div class="lyrico-lyrics-wrapper">Nagona divya sundara naago naaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nagona divya sundara naago naaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Digu digu digu naga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Digu digu digu naga"/>
</div>
<div class="lyrico-lyrics-wrapper">Nagona divya sundara naago naaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nagona divya sundara naago naaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Nageti salakaada naketti paniro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nageti salakaada naketti paniro"/>
</div>
<div class="lyrico-lyrics-wrapper">Naapagaddi sellakada naketti paniro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naapagaddi sellakada naketti paniro"/>
</div>
<div class="lyrico-lyrics-wrapper">sandhala sasnthakaada naketti paniro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sandhala sasnthakaada naketti paniro"/>
</div>
<div class="lyrico-lyrics-wrapper">Saakirevu taguvu kaada naketti paniro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saakirevu taguvu kaada naketti paniro"/>
</div>
<div class="lyrico-lyrics-wrapper">Iraga petti maraga petti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iraga petti maraga petti"/>
</div>
<div class="lyrico-lyrics-wrapper">Migala petti tagalapetti elakapettina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Migala petti tagalapetti elakapettina"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee yavvaram chaluro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee yavvaram chaluro"/>
</div>
<div class="lyrico-lyrics-wrapper">Kompakochhi poroy kodanaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kompakochhi poroy kodanaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kompa munchuthondhoy eedu baaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kompa munchuthondhoy eedu baaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kompakochhi poroy kodanaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kompakochhi poroy kodanaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kompa munchuthondhoy eedu baaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kompa munchuthondhoy eedu baaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Sempa gilli poroy setti naaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sempa gilli poroy setti naaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Samputhondi paite
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samputhondi paite"/>
</div>
<div class="lyrico-lyrics-wrapper">Digu digu digu naga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Digu digu digu naga"/>
</div>
<div class="lyrico-lyrics-wrapper">Nagona divya sundara naago naaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nagona divya sundara naago naaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nananaa naagi naagi naaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nananaa naagi naagi naaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Nananaa naagi naagi naaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nananaa naagi naagi naaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Nananaa naagi naagi naaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nananaa naagi naagi naaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Nananaa naagi naagi naaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nananaa naagi naagi naaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Godugu thoti poye dhanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Godugu thoti poye dhanni"/>
</div>
<div class="lyrico-lyrics-wrapper">Gudise dhaaka techhukuntav
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gudise dhaaka techhukuntav"/>
</div>
<div class="lyrico-lyrics-wrapper">Oori meedhi godavalanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oori meedhi godavalanni"/>
</div>
<div class="lyrico-lyrics-wrapper">Netthi meedhi ketthukuntav
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netthi meedhi ketthukuntav"/>
</div>
<div class="lyrico-lyrics-wrapper">Godugu thoti poye dhanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Godugu thoti poye dhanni"/>
</div>
<div class="lyrico-lyrics-wrapper">Gudise dhaaka techhukuntav
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gudise dhaaka techhukuntav"/>
</div>
<div class="lyrico-lyrics-wrapper">Alakathone illu alikithene 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alakathone illu alikithene "/>
</div>
<div class="lyrico-lyrics-wrapper">gaani ee dhikku sudav
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gaani ee dhikku sudav"/>
</div>
<div class="lyrico-lyrics-wrapper">Paisaki paniki raani 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paisaki paniki raani "/>
</div>
<div class="lyrico-lyrics-wrapper">kaaniki paniki raani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaaniki paniki raani"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanne moju teerchaleni 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanne moju teerchaleni "/>
</div>
<div class="lyrico-lyrics-wrapper">sunnalu saaluro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sunnalu saaluro"/>
</div>
<div class="lyrico-lyrics-wrapper">Kompakochhi poroy kodanaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kompakochhi poroy kodanaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kompa munchuthondoy eedu baaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kompa munchuthondoy eedu baaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Gampa dinchi raroy gaddunaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gampa dinchi raroy gaddunaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Gampedu aasha naalo rampamega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gampedu aasha naalo rampamega"/>
</div>
<div class="lyrico-lyrics-wrapper">Digu digu digu naaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Digu digu digu naaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Naagona divya sundara naago naaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naagona divya sundara naago naaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Digu digu digu naaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Digu digu digu naaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Naagona divya sundara naago naaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naagona divya sundara naago naaga"/>
</div>
<div class="lyrico-lyrics-wrapper">O naaga o naga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O naaga o naga"/>
</div>
</pre>
