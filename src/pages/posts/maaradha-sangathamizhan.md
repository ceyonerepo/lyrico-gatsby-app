---
title: "maaradha song lyrics"
album: "Sangathamizhan"
artist: "	Vivek - Mervin"
lyricist: "Madhan Karky"
director: "Vijay Chandar"
path: "/albums/sangathamizhan-lyrics"
song: "Maaradha"
image: ../../images/albumart/sangathamizhan.jpg
date: 2019-11-15
lang: tamil
youtubeLink: "https://www.youtube.com/embed/YFuBSy6VSn8"
type: "mass"
singers:
  - Shankar Mahadevan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hey Aayiram Kodigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Aayiram Kodigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Adithavanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adithavanai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathida Vazhi Irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathida Vazhi Irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Pasiyinil Yengidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pasiyinil Yengidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhaikku Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhaikku Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Siraiyinil Edam Irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siraiyinil Edam Irukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vinkalam Padaithida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinkalam Padaithida"/>
</div>
<div class="lyrico-lyrics-wrapper">Nidhi Irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nidhi Irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Kazhivarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kazhivarai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nammakketharkku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nammakketharkku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aalaigal Amaithida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalaigal Amaithida"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilam Irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilam Irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Vayal Veli Namakkedharkku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayal Veli Namakkedharkku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Keezh Vaanam Vaanam Vidiyaathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Keezh Vaanam Vaanam Vidiyaathaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaaal Unnaal Mudiyaathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaaal Unnaal Mudiyaathaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unmai Undhan Thunai Endraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmai Undhan Thunai Endraal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetri Unnai Adaiyaathaa Ezhadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetri Unnai Adaiyaathaa Ezhadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Kaaalm Maaratha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Kaaalm Maaratha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatchi Maarathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatchi Maarathaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ondraai Naam Nindraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondraai Naam Nindraal"/>
</div>
<div class="lyrico-lyrics-wrapper">Aatchi Maarathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aatchi Maarathaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhkai Maarathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkai Maarathaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sogam Maarathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sogam Maarathaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Maatram Naam Endraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maatram Naam Endraal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaam Maarathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaam Maarathaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pala Pala Aayiram Kodigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala Pala Aayiram Kodigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Adithavanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adithavanai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathida Vazhi Irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathida Vazhi Irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Pasiyinil Yengidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pasiyinil Yengidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhaikku Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhaikku Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Siraiyinil Edam Irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siraiyinil Edam Irukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vinkalam Padaithida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinkalam Padaithida"/>
</div>
<div class="lyrico-lyrics-wrapper">Nidhi Irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nidhi Irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Kazhivarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kazhivarai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nammakketharkku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nammakketharkku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aalaigal Amaithida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalaigal Amaithida"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilam Irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilam Irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Vayal Veli Namakkedharkku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayal Veli Namakkedharkku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanneerin Artham Maara Kandom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneerin Artham Maara Kandom"/>
</div>
<div class="lyrico-lyrics-wrapper">Inbangal Nenjil Yera Kandom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inbangal Nenjil Yera Kandom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Siru Siru Siru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru Siru Siru"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhiyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhiyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Perum Perum Perum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perum Perum Perum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavugal"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhai Thaduthidum Thadaigalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai Thaduthidum Thadaigalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Udaipomaaa Udanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udaipomaaa Udanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pala Pala Pala Arasiyal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala Pala Pala Arasiyal"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhai Ethirthida Purapadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai Ethirthida Purapadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhithena Oru Sarithiram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhithena Oru Sarithiram"/>
</div>
<div class="lyrico-lyrics-wrapper">Padaithida Ezhadaaa Udanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padaithida Ezhadaaa Udanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Endru Sollum Bothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Endru Sollum Bothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ottadhu Udhadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ottadhu Udhadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naam Endru Kaththi Solli Poraadadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam Endru Kaththi Solli Poraadadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Poraali Inamada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraali Inamada"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Ho Naalai Namadhadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Ho Naalai Namadhadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Kaaalm Maaratha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Kaaalm Maaratha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatchi Maarathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatchi Maarathaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ondraai Naam Nindraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondraai Naam Nindraal"/>
</div>
<div class="lyrico-lyrics-wrapper">Aatchi Maarathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aatchi Maarathaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhkai Maarathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkai Maarathaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sogam Maarathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sogam Maarathaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Maatram Naam Endraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maatram Naam Endraal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaam Maarathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaam Maarathaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poraali Inamada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraali Inamada"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalai Namadhadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalai Namadhadaa"/>
</div>
</pre>
