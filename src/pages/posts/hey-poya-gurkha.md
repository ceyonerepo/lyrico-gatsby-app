---
title: "hey poya song lyrics"
album: "Gurkha"
artist: "Raj Aryan"
lyricist: "Sivakarthikeyan"
director: "Sam Anton"
path: "/albums/gurkha-lyrics"
song: "Hey Poya"
image: ../../images/albumart/gurkha.jpg
date: 2019-07-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/AOuZno4pbWU"
type: "happy"
singers:
  - Rita
  - Anthony Daasan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Theeya Therikkum Thanos Ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeya Therikkum Thanos Ivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Paasathula Pachapulla Pola Thaan Ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paasathula Pachapulla Pola Thaan Ivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kakki Satta Gethu Ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kakki Satta Gethu Ivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Saamiyum Singamum Serntha Ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saamiyum Singamum Serntha Ivan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaana Serndha Kootam Ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaana Serndha Kootam Ivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nyaayathukku Onnu Vitta Machaan Ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nyaayathukku Onnu Vitta Machaan Ivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Speedaa Payum Bullet Ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Speedaa Payum Bullet Ivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nightukkaana Rightu Ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nightukkaana Rightu Ivan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poyaa Sathama Paadu Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poyaa Sathama Paadu Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Poyaa Nikkaama Odu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poyaa Nikkaama Odu"/>
</div>
<div class="lyrico-lyrics-wrapper">Poyaa Route Ah Thaan Maathi Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poyaa Route Ah Thaan Maathi Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Poyaa Fastaa Odu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poyaa Fastaa Odu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poyaa Sathama Paadu Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poyaa Sathama Paadu Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Poyaa Nikkaama Odu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poyaa Nikkaama Odu"/>
</div>
<div class="lyrico-lyrics-wrapper">Poyaa Route Ah Thaan Maathi Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poyaa Route Ah Thaan Maathi Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Poyaa Fastaa Odu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poyaa Fastaa Odu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rathiriyila Ratchasa Veeran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rathiriyila Ratchasa Veeran"/>
</div>
<div class="lyrico-lyrics-wrapper">Robberugala Raavura Sooran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Robberugala Raavura Sooran"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan Peruthaan Underutakeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan Peruthaan Underutakeru"/>
</div>
<div class="lyrico-lyrics-wrapper">Aalu Rounderu Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalu Rounderu Paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanmaanatha Vittadhum Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanmaanatha Vittadhum Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Avamaanatha Pattadhum Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avamaanatha Pattadhum Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vegumaanatha Vettiya Kattura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vegumaanatha Vettiya Kattura"/>
</div>
<div class="lyrico-lyrics-wrapper">Singha Kutti Baabu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Singha Kutti Baabu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Late-ah Kedaikkira Latest Model
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Late-ah Kedaikkira Latest Model"/>
</div>
<div class="lyrico-lyrics-wrapper">Ketta Kodukkura Karnan Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketta Kodukkura Karnan Dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thotta Therikkira Cutest Smile Ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thotta Therikkira Cutest Smile Ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Alli Veesum Kannan Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alli Veesum Kannan Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theeya Therikkum Thanos Ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeya Therikkum Thanos Ivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Paasathula Pachapulla Pola Thaan Ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paasathula Pachapulla Pola Thaan Ivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kakki Satta Gethu Ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kakki Satta Gethu Ivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Saamiyum Singamum Serntha Ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saamiyum Singamum Serntha Ivan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaana Serndha Kootam Ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaana Serndha Kootam Ivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nyaayathukku Onnu Vitta Machaan Ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nyaayathukku Onnu Vitta Machaan Ivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Speedaa Payum Bullet Ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Speedaa Payum Bullet Ivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nightukkaana Rightu Ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nightukkaana Rightu Ivan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poyaa Sathama Paadu Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poyaa Sathama Paadu Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Poyaa Nikkaama Odu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poyaa Nikkaama Odu"/>
</div>
<div class="lyrico-lyrics-wrapper">Poyaa Route Ah Thaan Maathi Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poyaa Route Ah Thaan Maathi Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Poyaa Fastaa Odu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poyaa Fastaa Odu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poyaa Sathama Paadu Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poyaa Sathama Paadu Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Poyaa Nikkaama Odu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poyaa Nikkaama Odu"/>
</div>
<div class="lyrico-lyrics-wrapper">Poyaa Route Ah Thaan Maathi Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poyaa Route Ah Thaan Maathi Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Poyaa Fastaa Odu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poyaa Fastaa Odu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poyaa Hei Pooyaah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poyaa Hei Pooyaah"/>
</div>
<div class="lyrico-lyrics-wrapper">Hei Poyaah Heyy Poyaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei Poyaah Heyy Poyaaa"/>
</div>
</pre>
