---
title: "aatam paaru song lyrics"
album: "Nari Vettai"
artist: "Charles Dhana"
lyricist: "Akash Sudhakar"
director: "Akash Sudhakar"
path: "/albums/nari-vettai-lyrics"
song: "Aatam Paaru"
image: ../../images/albumart/nari-vettai.jpg
date: 2018-02-09
lang: tamil
youtubeLink: "https://www.youtube.com/embed/_os9VW46kDk"
type: "happy"
singers:
  - Kavitha Gopi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">aatam paaru aatam paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aatam paaru aatam paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">engaloda kootam paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engaloda kootam paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">paravai pota oora pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paravai pota oora pola"/>
</div>
<div class="lyrico-lyrics-wrapper">serunga nalla paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="serunga nalla paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kadavul pola vaalum enga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadavul pola vaalum enga"/>
</div>
<div class="lyrico-lyrics-wrapper">vempuliyin kathaya kelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vempuliyin kathaya kelu"/>
</div>
<div class="lyrico-lyrics-wrapper">kadavul pola vaalum enga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadavul pola vaalum enga"/>
</div>
<div class="lyrico-lyrics-wrapper">vempuliyin kathaya kelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vempuliyin kathaya kelu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aatam paaru aatam paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aatam paaru aatam paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">engaloda kootam paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engaloda kootam paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">paravai pota oora pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paravai pota oora pola"/>
</div>
<div class="lyrico-lyrics-wrapper">serunga nalla paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="serunga nalla paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">karakudi sirpi nanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karakudi sirpi nanu"/>
</div>
<div class="lyrico-lyrics-wrapper">veedu pola vaala venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veedu pola vaala venum"/>
</div>
<div class="lyrico-lyrics-wrapper">coimbatore kongu tamil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="coimbatore kongu tamil"/>
</div>
<div class="lyrico-lyrics-wrapper">sutham venum da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sutham venum da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">namma namakal mutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="namma namakal mutta"/>
</div>
<div class="lyrico-lyrics-wrapper">pola panbu venum da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pola panbu venum da"/>
</div>
<div class="lyrico-lyrics-wrapper">madura malli madura malli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="madura malli madura malli"/>
</div>
<div class="lyrico-lyrics-wrapper">vaasam pola ooru poora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaasam pola ooru poora"/>
</div>
<div class="lyrico-lyrics-wrapper">manaka venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manaka venum"/>
</div>
<div class="lyrico-lyrics-wrapper">maruga malli thoothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maruga malli thoothu"/>
</div>
<div class="lyrico-lyrics-wrapper">pola milira venum da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pola milira venum da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">namma ariyalur jallikattu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="namma ariyalur jallikattu "/>
</div>
<div class="lyrico-lyrics-wrapper">vegam venum da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vegam venum da"/>
</div>
<div class="lyrico-lyrics-wrapper">erode makkal pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="erode makkal pola"/>
</div>
<div class="lyrico-lyrics-wrapper">eeram venum da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eeram venum da"/>
</div>
<div class="lyrico-lyrics-wrapper">kanchipuram seela pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanchipuram seela pola"/>
</div>
<div class="lyrico-lyrics-wrapper">nerma venum da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nerma venum da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pochamali santha pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pochamali santha pola"/>
</div>
<div class="lyrico-lyrics-wrapper">pesa venum da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pesa venum da"/>
</div>
<div class="lyrico-lyrics-wrapper">dindugal poota pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dindugal poota pola"/>
</div>
<div class="lyrico-lyrics-wrapper">manasu venum da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasu venum da"/>
</div>
<div class="lyrico-lyrics-wrapper">ada manasu venum da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada manasu venum da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">neelagiri vaasam thailam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neelagiri vaasam thailam"/>
</div>
<div class="lyrico-lyrics-wrapper">periyakulam marudha maram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="periyakulam marudha maram"/>
</div>
<div class="lyrico-lyrics-wrapper">madhavaram paala pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="madhavaram paala pola"/>
</div>
<div class="lyrico-lyrics-wrapper">thayiru venum da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thayiru venum da"/>
</div>
<div class="lyrico-lyrics-wrapper">kumari amman mookuthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kumari amman mookuthi"/>
</div>
<div class="lyrico-lyrics-wrapper">pol jolika venum da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pol jolika venum da"/>
</div>
<div class="lyrico-lyrics-wrapper">irutula jolika venum da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irutula jolika venum da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aatam paaru aatam paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aatam paaru aatam paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">engaloda kootam paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engaloda kootam paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">paravai pota oora pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paravai pota oora pola"/>
</div>
<div class="lyrico-lyrics-wrapper">serunga nalla paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="serunga nalla paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">senji kota alaga pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="senji kota alaga pola"/>
</div>
<div class="lyrico-lyrics-wrapper">eelagiri kulira pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eelagiri kulira pola"/>
</div>
<div class="lyrico-lyrics-wrapper">thanjavure kovila pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanjavure kovila pola"/>
</div>
<div class="lyrico-lyrics-wrapper">nilaika venum da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nilaika venum da"/>
</div>
<div class="lyrico-lyrics-wrapper">namma virudhunagar viyabari 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="namma virudhunagar viyabari "/>
</div>
<div class="lyrico-lyrics-wrapper">pol peruga venum da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pol peruga venum da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thirupur thuniya pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thirupur thuniya pola"/>
</div>
<div class="lyrico-lyrics-wrapper">trichy mala kota pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="trichy mala kota pola"/>
</div>
<div class="lyrico-lyrics-wrapper">thoothukudi santham pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoothukudi santham pola"/>
</div>
<div class="lyrico-lyrics-wrapper">iruka venum da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iruka venum da"/>
</div>
<div class="lyrico-lyrics-wrapper">namma perambalur mundiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="namma perambalur mundiri"/>
</div>
<div class="lyrico-lyrics-wrapper">pol inika venum da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pol inika venum da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">karuvooru thai kulam pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karuvooru thai kulam pol"/>
</div>
<div class="lyrico-lyrics-wrapper">irakam venum da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irakam venum da"/>
</div>
<div class="lyrico-lyrics-wrapper">kadalur uppu pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadalur uppu pola"/>
</div>
<div class="lyrico-lyrics-wrapper">rosam venum da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rosam venum da"/>
</div>
<div class="lyrico-lyrics-wrapper">kadadur athiyamana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadadur athiyamana"/>
</div>
<div class="lyrico-lyrics-wrapper">pola vaalu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pola vaalu da"/>
</div>
<div class="lyrico-lyrics-wrapper">selathu kallankuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="selathu kallankuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">patta kelu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="patta kelu da"/>
</div>
<div class="lyrico-lyrics-wrapper">kettu aadi paru da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kettu aadi paru da"/>
</div>
<div class="lyrico-lyrics-wrapper">kaasi metu kupathukum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaasi metu kupathukum"/>
</div>
<div class="lyrico-lyrics-wrapper">thirunelveli seemaikume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thirunelveli seemaikume"/>
</div>
<div class="lyrico-lyrics-wrapper">rameswaram paalam pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rameswaram paalam pola"/>
</div>
<div class="lyrico-lyrics-wrapper">nattu neelam da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nattu neelam da"/>
</div>
<div class="lyrico-lyrics-wrapper">enga naalu patinam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga naalu patinam"/>
</div>
<div class="lyrico-lyrics-wrapper">puthukotai vaalka veru da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthukotai vaalka veru da"/>
</div>
<div class="lyrico-lyrics-wrapper">vaaltha puriyum paru da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaaltha puriyum paru da"/>
</div>
<div class="lyrico-lyrics-wrapper">neyum vaalthu paru da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neyum vaalthu paru da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aatam paaru aatam paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aatam paaru aatam paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">engaloda kootam paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engaloda kootam paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">paravai pota oora pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paravai pota oora pola"/>
</div>
<div class="lyrico-lyrics-wrapper">serunga nalla paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="serunga nalla paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kadavul pola vaalum enga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadavul pola vaalum enga"/>
</div>
<div class="lyrico-lyrics-wrapper">vempuliyin kathaya kelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vempuliyin kathaya kelu"/>
</div>
<div class="lyrico-lyrics-wrapper">kadavul pola vaalum enga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadavul pola vaalum enga"/>
</div>
<div class="lyrico-lyrics-wrapper">vempuliyin kathaya kelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vempuliyin kathaya kelu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aatam paaru aatam paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aatam paaru aatam paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">engaloda kootam paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engaloda kootam paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">paravai pota oora pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paravai pota oora pola"/>
</div>
<div class="lyrico-lyrics-wrapper">serunga nalla paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="serunga nalla paaru"/>
</div>
</pre>
