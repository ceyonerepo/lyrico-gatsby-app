---
title: "manathil song lyrics"
album: "Yenda Thalaiyila Yenna Vekkala"
artist: "A.R. Reihana"
lyricist: "Thyagarajan"
director: "Vignesh Karthick"
path: "/albums/yenda-thalaiyila-yenna-vekkala-song-lyrics"
song: "Manathil"
image: ../../images/albumart/yenda-thalaiyila-yenna-vekkala.jpg
date: 2018-02-23
lang: tamil
youtubeLink: "https://www.youtube.com/embed/eZc08uNxoh0"
type: "melody"
singers:
  - Deepika Thyagarajan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">manathil thaayin ninaivu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manathil thaayin ninaivu"/>
</div>
<div class="lyrico-lyrics-wrapper">meendum piranthidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meendum piranthidum"/>
</div>
<div class="lyrico-lyrics-wrapper">pudhu uravu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pudhu uravu"/>
</div>
<div class="lyrico-lyrics-wrapper">ninaivaa illai kanavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninaivaa illai kanavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">oru annuyir theduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru annuyir theduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">en manathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en manathaa"/>
</div>
<div class="lyrico-lyrics-wrapper">puthirum puth urava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthirum puth urava"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu varugira varava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu varugira varava"/>
</div>
<div class="lyrico-lyrics-wrapper">thaayin thani paasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaayin thani paasam"/>
</div>
<div class="lyrico-lyrics-wrapper">ivan thanil veesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ivan thanil veesum"/>
</div>
<div class="lyrico-lyrics-wrapper">ivanaale meendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ivanaale meendum"/>
</div>
<div class="lyrico-lyrics-wrapper">piranthen indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="piranthen indru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">manathil thaayin ninaivu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manathil thaayin ninaivu"/>
</div>
<div class="lyrico-lyrics-wrapper">meendum piranthidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meendum piranthidum"/>
</div>
<div class="lyrico-lyrics-wrapper">pudhu uravu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pudhu uravu"/>
</div>
</pre>
