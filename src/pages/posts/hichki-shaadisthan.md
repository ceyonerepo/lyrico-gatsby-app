---
title: "hichki song lyrics"
album: "Shaadisthan"
artist: "Nakul Sharma - Sahil Bhatia"
lyricist: "Regional Folk -reinterpreted by Swaroop Khan - Surbhi Dashputra"
director: "Raj Singh Chaudhary"
path: "/albums/shaadisthan-lyrics"
song: "Hichki"
image: ../../images/albumart/shaadisthan.jpg
date: 2021-06-11
lang: hindi
youtubeLink: "https://www.youtube.com/embed/GX_uBahSjos"
type: "happy"
singers:
  - Isheeta Chakrvarty 
  - Swaroop Khan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Sajna Mora Balma Sajna Balma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sajna Mora Balma Sajna Balma"/>
</div>
<div class="lyrico-lyrics-wrapper">Sajan Preet Lagab Ke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sajan Preet Lagab Ke"/>
</div>
<div class="lyrico-lyrics-wrapper">Sajan Preet, Sajan Preet Lagab Ke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sajan Preet, Sajan Preet Lagab Ke"/>
</div>
<div class="lyrico-lyrics-wrapper">Ya Door Desh Mat Jaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ya Door Desh Mat Jaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ya Door Desh Mat Jaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ya Door Desh Mat Jaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Baso Hamari Nagri Mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baso Hamari Nagri Mein"/>
</div>
<div class="lyrico-lyrics-wrapper">Baso Hamari Nagri Mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baso Hamari Nagri Mein"/>
</div>
<div class="lyrico-lyrics-wrapper">Baso Hamari Nagri Mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baso Hamari Nagri Mein"/>
</div>
<div class="lyrico-lyrics-wrapper">Piya Hum Mange Tum Thar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Piya Hum Mange Tum Thar"/>
</div>
<div class="lyrico-lyrics-wrapper">Piya Hum Mange Tum Thar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Piya Hum Mange Tum Thar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Advertisement
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Advertisement"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Badilo Badilo Badilo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Badilo Badilo Badilo"/>
</div>
<div class="lyrico-lyrics-wrapper">Alijo Alijo Alijo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alijo Alijo Alijo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Badilo Chitare Mahro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Badilo Chitare Mahro"/>
</div>
<div class="lyrico-lyrics-wrapper">Alijo Chitare Mahro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alijo Chitare Mahro"/>
</div>
<div class="lyrico-lyrics-wrapper">Sahilo Chitare Mahro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sahilo Chitare Mahro"/>
</div>
<div class="lyrico-lyrics-wrapper">Parnu Nu Chitare Sa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parnu Nu Chitare Sa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alijo Chitare Mahro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alijo Chitare Mahro"/>
</div>
<div class="lyrico-lyrics-wrapper">Badilo Chitare Mahro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Badilo Chitare Mahro"/>
</div>
<div class="lyrico-lyrics-wrapper">Sahilo Chitare Mahro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sahilo Chitare Mahro"/>
</div>
<div class="lyrico-lyrics-wrapper">Pardesi Chitare Sa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pardesi Chitare Sa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mahro Badilo Chitare Mahro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mahro Badilo Chitare Mahro"/>
</div>
<div class="lyrico-lyrics-wrapper">Alijo Chitare Bairan Awe Hichki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alijo Chitare Bairan Awe Hichki"/>
</div>
<div class="lyrico-lyrics-wrapper">O Mahro Badilo Chitare Bairan Awe Hichki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Mahro Badilo Chitare Bairan Awe Hichki"/>
</div>
<div class="lyrico-lyrics-wrapper">O Mahro Paran Nu Chiyare Mahro Awe Hichki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Mahro Paran Nu Chiyare Mahro Awe Hichki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho Naina Kan No Bajiro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho Naina Kan No Bajiro"/>
</div>
<div class="lyrico-lyrics-wrapper">Naina Kan No Bajiro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naina Kan No Bajiro"/>
</div>
<div class="lyrico-lyrics-wrapper">Mahro Chidiya Chug Chug Jawe Bhavarsa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mahro Chidiya Chug Chug Jawe Bhavarsa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho Bhavarsa Jiyo Bhavrsa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho Bhavarsa Jiyo Bhavrsa"/>
</div>
<div class="lyrico-lyrics-wrapper">Chidiya Chug Chug Jawe Re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chidiya Chug Chug Jawe Re"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho Mai Tan Dhola Mana Kiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho Mai Tan Dhola Mana Kiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Mai Tan Dhola Mana Kiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mai Tan Dhola Mana Kiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">The Pardesha Mat Jave Bhavrsa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="The Pardesha Mat Jave Bhavrsa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho Bhavarsa Jiyo Bhavrsa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho Bhavarsa Jiyo Bhavrsa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pardesha Mat Jave Re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pardesha Mat Jave Re"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">He Mora Parnu Na Nari Pardeshi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="He Mora Parnu Na Nari Pardeshi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ab To Awe Re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ab To Awe Re"/>
</div>
<div class="lyrico-lyrics-wrapper">Marho Baadilo Bairan Awe Hichki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marho Baadilo Bairan Awe Hichki"/>
</div>
</pre>
