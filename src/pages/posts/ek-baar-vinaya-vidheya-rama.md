---
title: "ek baar song lyrics"
album: "Vinaya Vidheya Rama"
artist: "Devi Sri Prasad"
lyricist: "Sri Mani"
director: "Boyapati Srinu"
path: "/albums/vinaya-vidheya-rama-lyrics"
song: "Ek Baar"
image: ../../images/albumart/vinaya-vidheya-rama.jpg
date: 2019-01-11
lang: telugu
youtubeLink: "https://www.youtube.com/embed/mmyjBIo5ncE"
type: "happy"
singers:
  - Devi Sri Prasad
  - Ranina Reddy
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">O baby you are so beautiful ful
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O baby you are so beautiful ful"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuv okay ante lets just chill chill
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuv okay ante lets just chill chill"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kallallone undhi munthakallu kallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kallallone undhi munthakallu kallu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee volle veyyi ompulunna villu villu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee volle veyyi ompulunna villu villu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">O baby you are so beautiful ful
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O baby you are so beautiful ful"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuv okay ante lets just chill chill
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuv okay ante lets just chill chill"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kallallone undhi munthakallu kallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kallallone undhi munthakallu kallu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee volle veyyi ompulunna villu villu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee volle veyyi ompulunna villu villu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey raave naa aleesha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey raave naa aleesha"/>
</div>
<div class="lyrico-lyrics-wrapper">Choopistha tamasha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choopistha tamasha"/>
</div>
<div class="lyrico-lyrics-wrapper">Untadhi naalo nisha hamesha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Untadhi naalo nisha hamesha"/>
</div>
<div class="lyrico-lyrics-wrapper">Naaloni kalakar neeloni alankar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naaloni kalakar neeloni alankar"/>
</div>
<div class="lyrico-lyrics-wrapper">Mix aithe disco bar full hushaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mix aithe disco bar full hushaar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ek baar ek baar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ek baar ek baar"/>
</div>
<div class="lyrico-lyrics-wrapper">Duppesi step maar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Duppesi step maar"/>
</div>
<div class="lyrico-lyrics-wrapper">Ek baar ek baar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ek baar ek baar"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhancheyro dance floor
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhancheyro dance floor"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ek baar ek baar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ek baar ek baar"/>
</div>
<div class="lyrico-lyrics-wrapper">Duppesi step maar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Duppesi step maar"/>
</div>
<div class="lyrico-lyrics-wrapper">Ek baar ek baar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ek baar ek baar"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhancheyro dance floor
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhancheyro dance floor"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">O baby you are so beautiful ful
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O baby you are so beautiful ful"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuv okay ante lets just chill chill
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuv okay ante lets just chill chill"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kallallone undhi munthakallu kallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kallallone undhi munthakallu kallu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee volle veyyi ompulunna villu villu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee volle veyyi ompulunna villu villu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ek baar ek baar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ek baar ek baar"/>
</div>
<div class="lyrico-lyrics-wrapper">Duppesi step maar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Duppesi step maar"/>
</div>
<div class="lyrico-lyrics-wrapper">Ek baar ek baar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ek baar ek baar"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhancheyro dance floor
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhancheyro dance floor"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">O flight laaga take off
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O flight laaga take off"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye rayyumandhile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye rayyumandhile"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee nadaka choosi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee nadaka choosi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa bulli heart-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa bulli heart-u"/>
</div>
<div class="lyrico-lyrics-wrapper">High torch kite alle yeguruthundile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="High torch kite alle yeguruthundile"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee raakathoti naa heart beat-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee raakathoti naa heart beat-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">O mini o mini naa sokula sogamini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O mini o mini naa sokula sogamini"/>
</div>
<div class="lyrico-lyrics-wrapper">Thinipistha biriyaani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thinipistha biriyaani"/>
</div>
<div class="lyrico-lyrics-wrapper">Choopistha naa duniyaani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choopistha naa duniyaani"/>
</div>
<div class="lyrico-lyrics-wrapper">O honey o honey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O honey o honey"/>
</div>
<div class="lyrico-lyrics-wrapper">Ika nuvve naa kahani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ika nuvve naa kahani"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa vayyaraala gani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa vayyaraala gani"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuv yem chesthavo gani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuv yem chesthavo gani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ek baar ek baar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ek baar ek baar"/>
</div>
<div class="lyrico-lyrics-wrapper">Duppesi step maar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Duppesi step maar"/>
</div>
<div class="lyrico-lyrics-wrapper">Ek baar ek baar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ek baar ek baar"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhancheyro dance floor
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhancheyro dance floor"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">O baby you are so beautiful ful
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O baby you are so beautiful ful"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuv okay ante lets just chill chill
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuv okay ante lets just chill chill"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kallallone undhi munthakallu kallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kallallone undhi munthakallu kallu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee volle veyyi ompulunna villu villu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee volle veyyi ompulunna villu villu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey sunna kunna viluvento
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey sunna kunna viluvento"/>
</div>
<div class="lyrico-lyrics-wrapper">Thelisinaadhile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thelisinaadhile"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee sannaayanti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee sannaayanti"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadumu choosinaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadumu choosinaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey rent leni current ante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey rent leni current ante"/>
</div>
<div class="lyrico-lyrics-wrapper">Thelisinaadhile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thelisinaadhile"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee konte choopu shock kottinaaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee konte choopu shock kottinaaka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rani o rani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rani o rani"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannu neetho paate rani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannu neetho paate rani"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee solo sogasulanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee solo sogasulanni"/>
</div>
<div class="lyrico-lyrics-wrapper">Flow lo yetthukuponi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Flow lo yetthukuponi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaja raja jani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaja raja jani"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee jaanu nene poni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee jaanu nene poni"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee super sound ki bani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee super sound ki bani"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey Super hit aiponi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey Super hit aiponi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ek baar ek baar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ek baar ek baar"/>
</div>
<div class="lyrico-lyrics-wrapper">Duppesi step maar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Duppesi step maar"/>
</div>
<div class="lyrico-lyrics-wrapper">Ek baar ek baar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ek baar ek baar"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhancheyro dance floor
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhancheyro dance floor"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ek baar ek baar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ek baar ek baar"/>
</div>
<div class="lyrico-lyrics-wrapper">Duppesi step maar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Duppesi step maar"/>
</div>
<div class="lyrico-lyrics-wrapper">Ek baar ek baar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ek baar ek baar"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhancheyro dance floor
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhancheyro dance floor"/>
</div>
</pre>
