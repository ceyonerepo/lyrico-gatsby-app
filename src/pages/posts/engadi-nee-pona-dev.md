---
title: "engadi nee pona song lyrics"
album: "Dev"
artist: "Harris Jayaraj"
lyricist: "Kabilan"
director: "Rajath Ravishankar"
path: "/albums/dev-lyrics"
song: "Engadi Nee Pona"
image: ../../images/albumart/dev.jpg
date: 2019-02-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/0Z7t4VYC98o"
type: "sad"
singers:
  - S. P. Balasubrahmanyam
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ennai Vittu Engadi Nee Pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Vittu Engadi Nee Pona"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal Illaa Kadhalana Aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Illaa Kadhalana Aanen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Vittu Engadi Nee Pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Vittu Engadi Nee Pona"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal Illaa Kadhalana Aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Illaa Kadhalana Aanen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanneer Illamal Naanal Illaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanneer Illamal Naanal Illaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanneer Illamal Kadhal Illaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneer Illamal Kadhal Illaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Vittu Unnai Vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Vittu Unnai Vittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Vittu Engadi Nee Pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Vittu Engadi Nee Pona"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal Illaa Kadhalana Aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Illaa Kadhalana Aanen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andha Vaanam Parthathu Unnaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Vaanam Parthathu Unnaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Partha Vaanavil Enge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Partha Vaanavil Enge"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Neril Vandhu Nee Ninnaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Neril Vandhu Nee Ninnaale"/>
</div>
<div class="lyrico-lyrics-wrapper">En Ennam Vannamaagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Ennam Vannamaagum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Peru Paavam Yenbathu Yaar Endral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peru Paavam Yenbathu Yaar Endral"/>
</div>
<div class="lyrico-lyrics-wrapper">Pala Kaalam Kaalamai Kadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala Kaalam Kaalamai Kadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Aanukum Pennukum Endrendrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Aanukum Pennukum Endrendrum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Moondram Ulagam Mothal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Moondram Ulagam Mothal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Enna Tappu Senjen Pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Enna Tappu Senjen Pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kadhal Vittu Ennai Kolla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kadhal Vittu Ennai Kolla"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Unnai Thaandi Vaadi Mella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Unnai Thaandi Vaadi Mella"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku Endrum Un Mel Kovam Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku Endrum Un Mel Kovam Illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Vittu Engadi Nee Pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Vittu Engadi Nee Pona"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal Illaa Kadhalana Aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Illaa Kadhalana Aanen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Vittu Engadi Nee Pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Vittu Engadi Nee Pona"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal Illaa Kadhalana Aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Illaa Kadhalana Aanen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Vaththi kuchiyin Thala Melai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Vaththi kuchiyin Thala Melai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Kadhal Theeye Vaiththaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kadhal Theeye Vaiththaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Athil Venththu Ponathu Naanthaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athil Venththu Ponathu Naanthaney"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kadhal Illai Kanney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kadhal Illai Kanney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Thethi Pola Nee Kilithaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Thethi Pola Nee Kilithaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinam Vazhthum Vaasagam Naaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam Vazhthum Vaasagam Naaney"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Vaaram Yezhu Naal Unnaaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Vaaram Yezhu Naal Unnaaley"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Varusham Aachu Darling
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Varusham Aachu Darling"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Kadhal Paththi Enna Solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kadhal Paththi Enna Solla"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu Kaathil Onnum Keppathilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu Kaathil Onnum Keppathilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Mannukulla Pogum Munney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Mannukulla Pogum Munney"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kadhal Neethan Sollu Penney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kadhal Neethan Sollu Penney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Vittu Engadi Nee Pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Vittu Engadi Nee Pona"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal Illaa Kadhalana Aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Illaa Kadhalana Aanen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanneer Illamal Naanal Illaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanneer Illamal Naanal Illaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanneer Illamal Kadhal Illaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneer Illamal Kadhal Illaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Vittu Ennai Vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Vittu Ennai Vittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Vittu Engadi Nee Pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Vittu Engadi Nee Pona"/>
</div>
<div class="lyrico-lyrics-wrapper">Engadi Nee Pona Ennai Vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engadi Nee Pona Ennai Vittu"/>
</div>
</pre>
