---
title: "kattukkul vettai song lyrics"
album: "Thulam"
artist: "Alex Premnath"
lyricist: "Nadhi Vijaykumar"
director: "Rajanagajothi"
path: "/albums/thulam-lyrics"
song: "Kattukkul Vettai"
image: ../../images/albumart/thulam.jpg
date: 2018-12-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Pe2e1gX0CZA"
type: "happy"
singers:
  - Anitha Shaiq
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kattukkulla vettaiyaadum koottam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattukkulla vettaiyaadum koottam"/>
</div>
<div class="lyrico-lyrics-wrapper">Maanai kandaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maanai kandaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaanaga thullal podum aattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanaga thullal podum aattam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mullai poo pola endhan vaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mullai poo pola endhan vaasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai kandaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai kandaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Meenaaga thudikkum endhan swaasam haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meenaaga thudikkum endhan swaasam haa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thunaiyaaga varuvaaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunaiyaaga varuvaaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Puyalaaga kalaivaayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puyalaaga kalaivaayaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhai pola muththam ittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai pola muththam ittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thottu thottu thottu vittu poo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottu thottu thottu vittu poo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kattukkulla vettaiyaadum koottam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattukkulla vettaiyaadum koottam"/>
</div>
<div class="lyrico-lyrics-wrapper">Maanai kandaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maanai kandaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaanaga thullal podum aattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanaga thullal podum aattam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jumbaa jumbaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jumbaa jumbaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jumbala jumbala jumbaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jumbala jumbala jumbaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jumbaa jumbaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jumbaa jumbaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jumbala jumbala jumbaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jumbala jumbala jumbaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hmmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Iravukkul saththam illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravukkul saththam illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilavukkoo vetkkam illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilavukkoo vetkkam illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ahaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalavikkul kaamamindri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalavikkul kaamamindri"/>
</div>
<div class="lyrico-lyrics-wrapper">Yethum illaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethum illaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaa aaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vettaikkul veeram illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vettaikkul veeram illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ichchaikku pengal illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ichchaikku pengal illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ahaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Penmaikku kaadhal indri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Penmaikku kaadhal indri"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhum illaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhum illaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada thunai irundhaal vettri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada thunai irundhaal vettri"/>
</div>
<div class="lyrico-lyrics-wrapper">Siru kaadhal pesum pattri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru kaadhal pesum pattri"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu yudhtham seidhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu yudhtham seidhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaamam kolvomae aeaeae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaamam kolvomae aeaeae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattukkulla vettaiyaadum koottam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattukkulla vettaiyaadum koottam"/>
</div>
<div class="lyrico-lyrics-wrapper">Maanai kandaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maanai kandaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaanaga thullal podum aattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanaga thullal podum aattam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hae lae ullae ullae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hae lae ullae ullae"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo ullae purrrrrr
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo ullae purrrrrr"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hmmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannukkul kaadhal illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannukkul kaadhal illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjukkul eeram illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjukkul eeram illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ahaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Achchathil utcham thoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achchathil utcham thoda"/>
</div>
<div class="lyrico-lyrics-wrapper">Povadhillaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Povadhillaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mazhai naalil verpadhillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai naalil verpadhillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pani naalil poopadhillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pani naalil poopadhillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ahaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannichai nindraal pengal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannichai nindraal pengal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thorpadhillaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thorpadhillaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu kadalaal inaiyum dhegam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu kadalaal inaiyum dhegam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu puyalaai pogum mogam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu puyalaai pogum mogam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thee patri kondaal neerum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thee patri kondaal neerum "/>
</div>
<div class="lyrico-lyrics-wrapper">illaiyae ae aeae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="illaiyae ae aeae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kattukkulla vettaiyaadum koottam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattukkulla vettaiyaadum koottam"/>
</div>
<div class="lyrico-lyrics-wrapper">Maanai kandaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maanai kandaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaanaga thullal podum aattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanaga thullal podum aattam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mullai poo pola endhan vaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mullai poo pola endhan vaasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai kandaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai kandaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Meenaaga thudikkum endhan swaasam haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meenaaga thudikkum endhan swaasam haa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thunaiyaaga varuvaaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunaiyaaga varuvaaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Puyalaaga kalaivaayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puyalaaga kalaivaayaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhai pola muththam ittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai pola muththam ittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thottu thottu thottu vittu poo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottu thottu thottu vittu poo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kattukkulla vettaiyaadum koottam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattukkulla vettaiyaadum koottam"/>
</div>
<div class="lyrico-lyrics-wrapper">Maanai kandaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maanai kandaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaanaga thullal podum aattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanaga thullal podum aattam"/>
</div>
</pre>
