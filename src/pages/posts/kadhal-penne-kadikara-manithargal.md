---
title: "kadhal penne song lyrics"
album: "Kadikara Manithargal"
artist: "Sam CS"
lyricist: "Na Muthukumar"
director: "Vaigarai Balan"
path: "/albums/kadikara-manithargal-lyrics"
song: "Kadhal Penne"
image: ../../images/albumart/kadikara-manithargal.jpg
date: 2018-08-03
lang: tamil
youtubeLink: "https://www.youtube.com/embed/z4-vleCc_YE"
type: "love"
singers:
  - Karthik
  - Haritha Balakrishnan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Naananaa naananaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naananaa naananaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanananaanaa naanananaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanananaanaa naanananaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naananaa naananaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naananaa naananaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanananaanaa naanananaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanananaanaa naanananaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">O kaadhal pennae kaadhal pennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O kaadhal pennae kaadhal pennae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan paithiyamaanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan paithiyamaanen"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kannukkulla ennavachi nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kannukkulla ennavachi nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaithiyam paaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaithiyam paaren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En kaadhal pennae kaadhal pennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kaadhal pennae kaadhal pennae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan paithiyamaanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan paithiyamaanen"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kannukkulla ennavachi nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kannukkulla ennavachi nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaithiyam paaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaithiyam paaren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un nenjikkulla veedu kattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un nenjikkulla veedu kattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaadagaikku vaaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadagaikku vaaren"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaadagaiyaa enna venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadagaiyaa enna venum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam pooraa kaadhal thaaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam pooraa kaadhal thaaren"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavula neethaan adikkadi vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavula neethaan adikkadi vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adidhadi senja naan una kenja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adidhadi senja naan una kenja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal pennae kaadhal pennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal pennae kaadhal pennae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan paithiyamaanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan paithiyamaanen"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kannukkulla ennavachi nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kannukkulla ennavachi nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaithiyam paaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaithiyam paaren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hei pennae neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei pennae neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhevadhai thaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhevadhai thaanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasukkul enna ketkkudhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasukkul enna ketkkudhadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aiyo kaadhal raatchasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyo kaadhal raatchasi"/>
</div>
<div class="lyrico-lyrics-wrapper">Endrae kadharudhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endrae kadharudhadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poovil poothu vandhaval thaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovil poothu vandhaval thaanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Punnagai paarthaal thonudhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Punnagai paarthaal thonudhadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Roja poovil mullirundhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Roja poovil mullirundhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai kolludhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai kolludhadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naendhu vitta aatta pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naendhu vitta aatta pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Verthukotta vachiputta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verthukotta vachiputta"/>
</div>
<div class="lyrico-lyrics-wrapper">En kaalam neram maathipputta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kaalam neram maathipputta"/>
</div>
<div class="lyrico-lyrics-wrapper">Podi podi potta pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podi podi potta pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Loosaa enna vittutta pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Loosaa enna vittutta pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaala naanum thoongavilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaala naanum thoongavilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Moochukkaaththa vaangala pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moochukkaaththa vaangala pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Pulla pulla pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pulla pulla pulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En kaadhal pennae kaadhal pennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kaadhal pennae kaadhal pennae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan paithiyamaanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan paithiyamaanen"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kannukkulla ennavachi nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kannukkulla ennavachi nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaithiyam paaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaithiyam paaren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey…kaadhal ingae sathiyamaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey…kaadhal ingae sathiyamaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kirukkunnu ippo puriyudhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kirukkunnu ippo puriyudhadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanna moodum podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna moodum podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un mugam theriyudhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un mugam theriyudhadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalai maalai raathiri velai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalai maalai raathiri velai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kandabadi nenappu alaiyudhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandabadi nenappu alaiyudhadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu sinnapulla pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu sinnapulla pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkulla ozhiyudhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkulla ozhiyudhadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ekkuthappaa maattikkiten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ekkuthappaa maattikkiten"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil vandhu poottikkitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil vandhu poottikkitta"/>
</div>
<div class="lyrico-lyrics-wrapper">En kaiyila kaiya korthukkitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kaiyila kaiya korthukkitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Podi podi potta pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podi podi potta pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Podi podi potta pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podi podi potta pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Loosaa enna vittutta pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Loosaa enna vittutta pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaala naanum thoongavilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaala naanum thoongavilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Moochukkaatha vaangala pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moochukkaatha vaangala pulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En kaadhal pennae kaadhal pennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kaadhal pennae kaadhal pennae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan paithiyamaanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan paithiyamaanen"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kannukkulla ennavachi nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kannukkulla ennavachi nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaithiyam paaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaithiyam paaren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un nenjikkulla veedu kattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un nenjikkulla veedu kattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaadagaikku vaaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadagaikku vaaren"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaadagaiyaa enna venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadagaiyaa enna venum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam pooraa kaadhal thaaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam pooraa kaadhal thaaren"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavula neethaan adikkadi vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavula neethaan adikkadi vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adidhadi senja naan una kenja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adidhadi senja naan una kenja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal pennae kaadhal pennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal pennae kaadhal pennae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan paithiyamaanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan paithiyamaanen"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kannukkulla ennavachi nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kannukkulla ennavachi nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaithiyam paaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaithiyam paaren"/>
</div>
</pre>
