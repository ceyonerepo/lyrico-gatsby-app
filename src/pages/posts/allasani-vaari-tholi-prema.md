---
title: "allasani vaari song lyrics"
album: "Tholi Prema"
artist: "S Thaman"
lyricist: "Sri Mani"
director: "Venky Atluri"
path: "/albums/tholi-prema-lyrics"
song: "Allasani Vaari"
image: ../../images/albumart/tholi-prema.jpg
date: 2018-02-10
lang: telugu
youtubeLink: "https://www.youtube.com/embed/8qhpfB7qWtM"
type: "love"
singers:
  -	Shreya Ghoshal
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Allasaani Vaari Padyamaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allasaani Vaari Padyamaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Viswanaatha Vaari Muthyamaa Muthyamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viswanaatha Vaari Muthyamaa Muthyamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalidaasu Prema Kaavyamaa Kaavyamaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalidaasu Prema Kaavyamaa Kaavyamaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Tyaagaraaja Sangeethamaa Geethamaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tyaagaraaja Sangeethamaa Geethamaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Allasaani Vaari Padyamaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allasaani Vaari Padyamaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Viswanaatha Vaari Muthyamaa Muthyamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viswanaatha Vaari Muthyamaa Muthyamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalidaasu Prema Kaavyamaa Kaavyamaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalidaasu Prema Kaavyamaa Kaavyamaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Tyaagaraaja Sangeethamaa Geethamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tyaagaraaja Sangeethamaa Geethamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Polike Leni Paatalaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Polike Leni Paatalaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu Pilichaavu Nannilaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu Pilichaavu Nannilaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Chinni Chirunavvu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinni Chirunavvu "/>
</div>
<div class="lyrico-lyrics-wrapper">Letha Chiguraasha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Letha Chiguraasha "/>
</div>
<div class="lyrico-lyrics-wrapper">Malli Poosaayile Ilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malli Poosaayile Ilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">D D D Destiny 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="D D D Destiny "/>
</div>
<div class="lyrico-lyrics-wrapper">Life E Maarindani 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life E Maarindani "/>
</div>
<div class="lyrico-lyrics-wrapper">Edo Jarigindani 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edo Jarigindani "/>
</div>
<div class="lyrico-lyrics-wrapper">It's Got Me Feeling So Heavenly 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="It's Got Me Feeling So Heavenly "/>
</div>
<div class="lyrico-lyrics-wrapper">D D D Destiny 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="D D D Destiny "/>
</div>
<div class="lyrico-lyrics-wrapper">Life E Maarindani 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life E Maarindani "/>
</div>
<div class="lyrico-lyrics-wrapper">Edo Jarigindani 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edo Jarigindani "/>
</div>
<div class="lyrico-lyrics-wrapper">It's Got Me Feeling So Heavenly
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="It's Got Me Feeling So Heavenly"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Allasaani Vaari Padyamaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allasaani Vaari Padyamaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Viswanaatha Vaari Muthyamaa Muthyamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viswanaatha Vaari Muthyamaa Muthyamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalidaasu Prema Kaavyamaa Kaavyamaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalidaasu Prema Kaavyamaa Kaavyamaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Tyaagaraaja Sangeethamaa Geethamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tyaagaraaja Sangeethamaa Geethamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Needalaa Nuvvochi Venta Vaalagaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Needalaa Nuvvochi Venta Vaalagaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Gundelo Uyyaalalooginatlugaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundelo Uyyaalalooginatlugaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Gonthulo Swaraala Mooga Pilupule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gonthulo Swaraala Mooga Pilupule"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandadi Chesenaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandadi Chesenaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Thodulaa Nuvvochi Daggaravvagaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodulaa Nuvvochi Daggaravvagaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Inthalo Ennenni Vinthalo Ilaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inthalo Ennenni Vinthalo Ilaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Kanthulaa Kalalni Jallinattuga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanthulaa Kalalni Jallinattuga "/>
</div>
<div class="lyrico-lyrics-wrapper">Praanam Murisenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Praanam Murisenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thenelo Unna Theeyana 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thenelo Unna Theeyana "/>
</div>
<div class="lyrico-lyrics-wrapper">Bhaashalo Unna Laalana 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhaashalo Unna Laalana "/>
</div>
<div class="lyrico-lyrics-wrapper">Kummaristhunnaa Pongi Pothunnaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kummaristhunnaa Pongi Pothunnaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu Kaliseti Velanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu Kaliseti Velanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalame Doboochulaaduthunnado 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalame Doboochulaaduthunnado "/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanuke Kshanaallo Panchuthunnado 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanuke Kshanaallo Panchuthunnado "/>
</div>
<div class="lyrico-lyrics-wrapper">Kaaranam Oohinchaniv
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaranam Oohinchaniv"/>
</div>
<div class="lyrico-lyrics-wrapper">vanannado Emavuthunnado 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanannado Emavuthunnado "/>
</div>
<div class="lyrico-lyrics-wrapper">Swapname Nijamgaa Maaruthunnado 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Swapname Nijamgaa Maaruthunnado "/>
</div>
<div class="lyrico-lyrics-wrapper">Saagaram Nadalle Paaruthunnado 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saagaram Nadalle Paaruthunnado "/>
</div>
<div class="lyrico-lyrics-wrapper">Sathyame Idanthaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sathyame Idanthaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Nammanannado Emantunnado
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nammanannado Emantunnado"/>
</div>
<div class="lyrico-lyrics-wrapper">Marichipoyaanu Nannilaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marichipoyaanu Nannilaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Marachipoleka Ninnilaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marachipoleka Ninnilaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Letha Praayaala 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Letha Praayaala "/>
</div>
<div class="lyrico-lyrics-wrapper">Paatha Pranayaale 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paatha Pranayaale "/>
</div>
<div class="lyrico-lyrics-wrapper">Kotthagaa Poothaleselaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kotthagaa Poothaleselaa"/>
</div>
<div class="lyrico-lyrics-wrapper">D D D Destiny 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="D D D Destiny "/>
</div>
<div class="lyrico-lyrics-wrapper">Life E Maarindani 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life E Maarindani "/>
</div>
<div class="lyrico-lyrics-wrapper">Edo Jarigindani 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edo Jarigindani "/>
</div>
<div class="lyrico-lyrics-wrapper">It's Got Me Feeling So Heavenly
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="It's Got Me Feeling So Heavenly"/>
</div>
<div class="lyrico-lyrics-wrapper">D D D Destiny 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="D D D Destiny "/>
</div>
<div class="lyrico-lyrics-wrapper">Life E Maarindani 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life E Maarindani "/>
</div>
<div class="lyrico-lyrics-wrapper">Edo Jarigindani 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edo Jarigindani "/>
</div>
<div class="lyrico-lyrics-wrapper">It's Got Me Feeling So Heavenly
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="It's Got Me Feeling So Heavenly"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Destiny Destiny Destiny
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Destiny Destiny Destiny"/>
</div>
</pre>
