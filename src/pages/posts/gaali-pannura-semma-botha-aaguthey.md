---
title: "gaali pannura song lyrics"
album: "Semma Botha Aaguthey"
artist: "Yuvan Shankar Raja"
lyricist: "Badri Venkatesh"
director: "Badri Venkatesh"
path: "/albums/semma-botha-aaguthey-lyrics"
song: "Gaali Pannura"
image: ../../images/albumart/semma-botha-aaguthey.jpg
date: 2018-06-29
lang: tamil
youtubeLink: "https://www.youtube.com/embed/olCquynILoM"
type: "mass"
singers:
  - Remya Nambeesan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Gali Pannura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gali Pannura"/>
</div>
<div class="lyrico-lyrics-wrapper">Gali Pannura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gali Pannura"/>
</div>
<div class="lyrico-lyrics-wrapper">Sketchu Udambaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sketchu Udambaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Gali Pannura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gali Pannura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gali Pannura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gali Pannura"/>
</div>
<div class="lyrico-lyrics-wrapper">Gali Pannura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gali Pannura"/>
</div>
<div class="lyrico-lyrics-wrapper">Hipu Nelival
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hipu Nelival"/>
</div>
<div class="lyrico-lyrics-wrapper">Gali Pannura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gali Pannura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Soota Kelapi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soota Kelapi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sorgam Theriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sorgam Theriya"/>
</div>
<div class="lyrico-lyrics-wrapper">Botha Yethi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Botha Yethi"/>
</div>
<div class="lyrico-lyrics-wrapper">Gali Pannura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gali Pannura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Marana Kuliyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marana Kuliyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaya Vachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaya Vachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayaki Mayaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayaki Mayaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Gali Pannura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gali Pannura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gali Pannura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gali Pannura"/>
</div>
<div class="lyrico-lyrics-wrapper">Gali Pannura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gali Pannura"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadai Maranthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadai Maranthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gali Pannura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gali Pannura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gali Pannura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gali Pannura"/>
</div>
<div class="lyrico-lyrics-wrapper">Gali Pannura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gali Pannura"/>
</div>
<div class="lyrico-lyrics-wrapper">Apple Nirathaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Apple Nirathaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Gali Pannura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gali Pannura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pathara Pathara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathara Pathara"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathara Kathara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathara Kathara"/>
</div>
<div class="lyrico-lyrics-wrapper">Kamatheeyal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kamatheeyal"/>
</div>
<div class="lyrico-lyrics-wrapper">Gali Pannura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gali Pannura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aanmai Thathumba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanmai Thathumba"/>
</div>
<div class="lyrico-lyrics-wrapper">Penmai Inanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Penmai Inanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Katti Thaluvi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katti Thaluvi"/>
</div>
<div class="lyrico-lyrics-wrapper">Gali Pannura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gali Pannura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gali Pannura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gali Pannura"/>
</div>
<div class="lyrico-lyrics-wrapper">Gali Pannura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gali Pannura"/>
</div>
<div class="lyrico-lyrics-wrapper">Aya Yayayoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aya Yayayoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Gali Pannura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gali Pannura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gali Pannura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gali Pannura"/>
</div>
<div class="lyrico-lyrics-wrapper">Gali Pannura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gali Pannura"/>
</div>
<div class="lyrico-lyrics-wrapper">Aya Yayaiyoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aya Yayaiyoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Gali Pannura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gali Pannura"/>
</div>
</pre>
