---
title: "nee yen usuru pulla song lyrics"
album: "Anandham Vilayadum Veedu"
artist: "Siddhu Kumar"
lyricist: "Snehan"
director: "Nandha Periyasamy"
path: "/albums/anandham-vilayadum-veedu-song-lyrics"
song: "Nee Yen Usuru Pulla"
image: ../../images/albumart/anandham-vilayadum-veedu.jpg
date: 2021-12-24
lang: tamil
youtubeLink: "https://www.youtube.com/embed/asNBbG6pz74"
type: "love"
singers:
  - GV Prakash Kumar
  - Sivaangi Krishnakumar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nee yen usurupulla ithu poyyilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee yen usurupulla ithu poyyilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhura manasukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhura manasukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee yen moothapulla ithu poyyilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee yen moothapulla ithu poyyilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan unna summapen ulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan unna summapen ulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unakkunnuthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkunnuthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Usuraa irupen boomiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usuraa irupen boomiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai vitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai vitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Uravunu sollikka yarumilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uravunu sollikka yarumilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aamm maa maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aamm maa maa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee yen usurupulla ithu poyyilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee yen usurupulla ithu poyyilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhura manasukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhura manasukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee yen moothapulla ithu poyyilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee yen moothapulla ithu poyyilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan unna sumapen ulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan unna sumapen ulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theruvellam un ninnapu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theruvellam un ninnapu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thinnaiyila yem pozhappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thinnaiyila yem pozhappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeppodi nee vanthu paai poduva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeppodi nee vanthu paai poduva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eerathuni theepidikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eerathuni theepidikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Eththana naal thudikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eththana naal thudikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppoda nee vanthu poochuduva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppoda nee vanthu poochuduva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeeng kulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeeng kulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan illa oor solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan illa oor solla"/>
</div>
<div class="lyrico-lyrics-wrapper">Koda raatinam pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koda raatinam pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Alladi thalldi onna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alladi thalldi onna "/>
</div>
<div class="lyrico-lyrics-wrapper">naan suththurendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan suththurendi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un kuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kuda"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan kuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan kuda"/>
</div>
<div class="lyrico-lyrics-wrapper">Naal theda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naal theda"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru kodangi thedi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru kodangi thedi "/>
</div>
<div class="lyrico-lyrics-wrapper">kadellam medellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadellam medellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Roadllam suththurenda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Roadllam suththurenda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazha meena un
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazha meena un"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanappula saanjen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanappula saanjen"/>
</div>
<div class="lyrico-lyrics-wrapper">Thinam saamam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thinam saamam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aana un ninappula menjen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aana un ninappula menjen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koolam pottale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koolam pottale"/>
</div>
<div class="lyrico-lyrics-wrapper">Meesaiya varanjen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meesaiya varanjen"/>
</div>
<div class="lyrico-lyrics-wrapper">Un vetti noola naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un vetti noola naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ottikka alanjen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ottikka alanjen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee yen usurupulla ithu poyyilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee yen usurupulla ithu poyyilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhura manasukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhura manasukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee yen moothapulla ithu poyyilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee yen moothapulla ithu poyyilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan unna sumapen ulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan unna sumapen ulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unakkunnuthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkunnuthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Usuraa irupen boomiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usuraa irupen boomiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai vitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai vitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Uravunnu sollikka yarumilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uravunnu sollikka yarumilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Amm maa maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amm maa maa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee yen usurupulla ithu poyyilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee yen usurupulla ithu poyyilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhura manasukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhura manasukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee yen moothapulla ithu poyyilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee yen moothapulla ithu poyyilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan unna sumapen ulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan unna sumapen ulla"/>
</div>
</pre>
