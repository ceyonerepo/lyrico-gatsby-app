---
title: "kanna thorada song lyrics"
album: "Kasu Mela Kasu"
artist: "M.S. Pandian"
lyricist: "Karuppaiah"
director: "K.S. Pazhani"
path: "/albums/kasu-mela-kasu-lyrics"
song: "Kanna Thorada"
image: ../../images/albumart/kasu-mela-kasu.jpg
date: 2018-07-06
lang: tamil
youtubeLink: "https://www.youtube.com/embed/dI-mJ5A4hi4"
type: "mass"
singers:
  - Gaana Bala
  - MS Pandian
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">kanna thorada yeppa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanna thorada yeppa"/>
</div>
<div class="lyrico-lyrics-wrapper">kanna thorada kanna thorada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanna thorada kanna thorada"/>
</div>
<div class="lyrico-lyrics-wrapper">kanna thorada kadavule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanna thorada kadavule"/>
</div>
<div class="lyrico-lyrics-wrapper">kasu iruntha kadhal varuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kasu iruntha kadhal varuma"/>
</div>
<div class="lyrico-lyrics-wrapper">unmai sollu da kadavule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unmai sollu da kadavule"/>
</div>
<div class="lyrico-lyrics-wrapper">aanum pennum ondraagi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aanum pennum ondraagi "/>
</div>
<div class="lyrico-lyrics-wrapper">than thondriyathu ulagame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="than thondriyathu ulagame"/>
</div>
<div class="lyrico-lyrics-wrapper">kathal matum koodathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathal matum koodathu"/>
</div>
<div class="lyrico-lyrics-wrapper">endral boomi engum naragame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="endral boomi engum naragame"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu raajavukum koojavukum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu raajavukum koojavukum"/>
</div>
<div class="lyrico-lyrics-wrapper">common aana visayame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="common aana visayame"/>
</div>
<div class="lyrico-lyrics-wrapper">kadavule thodarume 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadavule thodarume "/>
</div>
<div class="lyrico-lyrics-wrapper">intha mayakame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha mayakame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kanna thorada kanna thorada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanna thorada kanna thorada"/>
</div>
<div class="lyrico-lyrics-wrapper">kanna thorada kadavule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanna thorada kadavule"/>
</div>
<div class="lyrico-lyrics-wrapper">kasu iruntha kadhal varuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kasu iruntha kadhal varuma"/>
</div>
<div class="lyrico-lyrics-wrapper">unmai sollu da kadavule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unmai sollu da kadavule"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aanasaiyum ponnasaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aanasaiyum ponnasaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">illaiyina yarukume inbam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="illaiyina yarukume inbam "/>
</div>
<div class="lyrico-lyrics-wrapper">illa thunbam illa pothuva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="illa thunbam illa pothuva"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu pattan pootan kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu pattan pootan kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">thotte vaalkaiyile vantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thotte vaalkaiyile vantha"/>
</div>
<div class="lyrico-lyrics-wrapper">thappa methuva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thappa methuva"/>
</div>
<div class="lyrico-lyrics-wrapper">oru anbu mattum nu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru anbu mattum nu"/>
</div>
<div class="lyrico-lyrics-wrapper">alatikama solliputu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alatikama solliputu"/>
</div>
<div class="lyrico-lyrics-wrapper">panathu mela 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="panathu mela "/>
</div>
<div class="lyrico-lyrics-wrapper">thunguvanga veetula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thunguvanga veetula"/>
</div>
<div class="lyrico-lyrics-wrapper">ada jaathi ethum illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada jaathi ethum illai"/>
</div>
<div class="lyrico-lyrics-wrapper">endru paadi vacha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="endru paadi vacha "/>
</div>
<div class="lyrico-lyrics-wrapper">bharathikum jaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bharathikum jaathi"/>
</div>
<div class="lyrico-lyrics-wrapper">poster oturanga naatula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poster oturanga naatula"/>
</div>
<div class="lyrico-lyrics-wrapper">kadavule ada kadavule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadavule ada kadavule"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kanna thorada kanna thorada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanna thorada kanna thorada"/>
</div>
<div class="lyrico-lyrics-wrapper">kanna thorada kadavule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanna thorada kadavule"/>
</div>
<div class="lyrico-lyrics-wrapper">kasu iruntha kadhal varuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kasu iruntha kadhal varuma"/>
</div>
<div class="lyrico-lyrics-wrapper">unmai sollu da kadavule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unmai sollu da kadavule"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ippo ulla appan ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ippo ulla appan ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">pethu edutha pullaiyaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pethu edutha pullaiyaale"/>
</div>
<div class="lyrico-lyrics-wrapper">sothu pathu venumunu nenapa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sothu pathu venumunu nenapa"/>
</div>
<div class="lyrico-lyrics-wrapper">athan thevatha pol ponna pathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athan thevatha pol ponna pathu"/>
</div>
<div class="lyrico-lyrics-wrapper">kathalikka venum nu thudipa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathalikka venum nu thudipa"/>
</div>
<div class="lyrico-lyrics-wrapper">ingu currency mogam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ingu currency mogam"/>
</div>
<div class="lyrico-lyrics-wrapper">appan nenjil kadhal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="appan nenjil kadhal "/>
</div>
<div class="lyrico-lyrics-wrapper">mogam pulla nenjil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mogam pulla nenjil"/>
</div>
<div class="lyrico-lyrics-wrapper">unmaiyaana kathal ennavagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unmaiyaana kathal ennavagum"/>
</div>
<div class="lyrico-lyrics-wrapper">ipo munna pola ooru illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ipo munna pola ooru illa"/>
</div>
<div class="lyrico-lyrics-wrapper">nalavanga yaarum illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nalavanga yaarum illa"/>
</div>
<div class="lyrico-lyrics-wrapper">kathal mothal kalavaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathal mothal kalavaram"/>
</div>
<div class="lyrico-lyrics-wrapper">thaan naalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaan naalum"/>
</div>
<div class="lyrico-lyrics-wrapper">kadavule ada kadavule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadavule ada kadavule"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kanna thorada kanna thorada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanna thorada kanna thorada"/>
</div>
<div class="lyrico-lyrics-wrapper">kanna thorada kadavule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanna thorada kadavule"/>
</div>
<div class="lyrico-lyrics-wrapper">kasu iruntha kadhal varuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kasu iruntha kadhal varuma"/>
</div>
<div class="lyrico-lyrics-wrapper">unmai sollu da kadavule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unmai sollu da kadavule"/>
</div>
<div class="lyrico-lyrics-wrapper">aanum pennum ondraagi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aanum pennum ondraagi "/>
</div>
<div class="lyrico-lyrics-wrapper">than thondriyathu ulagame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="than thondriyathu ulagame"/>
</div>
<div class="lyrico-lyrics-wrapper">kathal matum koodathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathal matum koodathu"/>
</div>
<div class="lyrico-lyrics-wrapper">endral boomi engum naragame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="endral boomi engum naragame"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu raajavukum koojavukum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu raajavukum koojavukum"/>
</div>
<div class="lyrico-lyrics-wrapper">common aana visayame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="common aana visayame"/>
</div>
<div class="lyrico-lyrics-wrapper">kadavule thodarume 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadavule thodarume "/>
</div>
<div class="lyrico-lyrics-wrapper">intha mayakame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha mayakame"/>
</div>
</pre>
