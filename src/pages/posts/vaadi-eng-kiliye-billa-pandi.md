---
title: "vaadi eng kiliye song lyrics"
album: "Billa Pandi"
artist: "Ilayavan"
lyricist: "Pa Meenatchi Sundaram"
director: "Raj Sethupathy"
path: "/albums/billa-pandi-lyrics"
song: "Vaadi Eng Kiliye"
image: ../../images/albumart/billa-pandi.jpg
date: 2018-11-06
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ZTqIDwj-PIA"
type: "happy"
singers:
  - Velmurugan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">adi vatta vatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi vatta vatta"/>
</div>
<div class="lyrico-lyrics-wrapper">pottazhagi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pottazhagi "/>
</div>
<div class="lyrico-lyrics-wrapper">vagidedutha muthazhagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vagidedutha muthazhagi"/>
</div>
<div class="lyrico-lyrics-wrapper">onna varambu meeri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="onna varambu meeri"/>
</div>
<div class="lyrico-lyrics-wrapper">paakaa poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paakaa poren"/>
</div>
<div class="lyrico-lyrics-wrapper">elavam panji iduppazhagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="elavam panji iduppazhagi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vaadi eng kiliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaadi eng kiliye"/>
</div>
<div class="lyrico-lyrics-wrapper">neetha eaa radhiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neetha eaa radhiye"/>
</div>
<div class="lyrico-lyrics-wrapper">atha maga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="atha maga "/>
</div>
<div class="lyrico-lyrics-wrapper">rathiname vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rathiname vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">enna mayakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna mayakka"/>
</div>
<div class="lyrico-lyrics-wrapper">konja neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konja neram"/>
</div>
<div class="lyrico-lyrics-wrapper">konji pesava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konji pesava"/>
</div>
<div class="lyrico-lyrics-wrapper">panju mittai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="panju mittai"/>
</div>
<div class="lyrico-lyrics-wrapper">bambarame vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bambarame vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">pesi mudinju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pesi mudinju"/>
</div>
<div class="lyrico-lyrics-wrapper">sethu vacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sethu vacha"/>
</div>
<div class="lyrico-lyrics-wrapper">sellang konjava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sellang konjava"/>
</div>
<div class="lyrico-lyrics-wrapper">porayetthi oru aasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="porayetthi oru aasa"/>
</div>
<div class="lyrico-lyrics-wrapper">usuppetthi ulukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="usuppetthi ulukka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kandang kathiri veroda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kandang kathiri veroda"/>
</div>
<div class="lyrico-lyrics-wrapper">konda munthiri naaroda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konda munthiri naaroda"/>
</div>
<div class="lyrico-lyrics-wrapper">appidi sollunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="appidi sollunga"/>
</div>
<div class="lyrico-lyrics-wrapper">mothavare 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mothavare "/>
</div>
<div class="lyrico-lyrics-wrapper">pechu vakkula 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pechu vakkula "/>
</div>
<div class="lyrico-lyrics-wrapper">pathavare pathavare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathavare pathavare"/>
</div>
<div class="lyrico-lyrics-wrapper">pathavare pathavare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathavare pathavare"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">onna pathathum olanjea
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="onna pathathum olanjea"/>
</div>
<div class="lyrico-lyrics-wrapper">nela kokzhanje velanjae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nela kokzhanje velanjae"/>
</div>
<div class="lyrico-lyrics-wrapper">moththa azhagula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moththa azhagula"/>
</div>
<div class="lyrico-lyrics-wrapper">murukkethura azhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="murukkethura azhagu"/>
</div>
<div class="lyrico-lyrics-wrapper">vanthu pazhagu solavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanthu pazhagu solavu"/>
</div>
<div class="lyrico-lyrics-wrapper">nenja podaikurea
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenja podaikurea"/>
</div>
<div class="lyrico-lyrics-wrapper">ammadi asanjaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ammadi asanjaa"/>
</div>
<div class="lyrico-lyrics-wrapper">porakkum kalagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="porakkum kalagam"/>
</div>
<div class="lyrico-lyrics-wrapper">summa nee irunthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="summa nee irunthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">patharum olagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="patharum olagam"/>
</div>
<div class="lyrico-lyrics-wrapper">vellama velaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vellama velaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">vetha moraiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetha moraiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">vethappomaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vethappomaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">color coloraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="color coloraa"/>
</div>
<div class="lyrico-lyrics-wrapper">color coloraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="color coloraa"/>
</div>
<div class="lyrico-lyrics-wrapper">pacha kannadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pacha kannadi"/>
</div>
<div class="lyrico-lyrics-wrapper">pallu illa kilavanukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pallu illa kilavanukku"/>
</div>
<div class="lyrico-lyrics-wrapper">double pondatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="double pondatti"/>
</div>
<div class="lyrico-lyrics-wrapper">double pondatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="double pondatti"/>
</div>
<div class="lyrico-lyrics-wrapper">double pondatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="double pondatti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">velakethura pozhuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="velakethura pozhuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">konji kolavu thazhuvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konji kolavu thazhuvu"/>
</div>
<div class="lyrico-lyrics-wrapper">pacha narambula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pacha narambula"/>
</div>
<div class="lyrico-lyrics-wrapper">sulukethura valaivu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sulukethura valaivu"/>
</div>
<div class="lyrico-lyrics-wrapper">oru nelivu sulivu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru nelivu sulivu"/>
</div>
<div class="lyrico-lyrics-wrapper">pasi adangala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pasi adangala"/>
</div>
<div class="lyrico-lyrics-wrapper">adiyathi asanthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adiyathi asanthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">asaththum azhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="asaththum azhagu"/>
</div>
<div class="lyrico-lyrics-wrapper">kodiyeththi oravaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kodiyeththi oravaa"/>
</div>
<div class="lyrico-lyrics-wrapper">azhaikkum nelavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="azhaikkum nelavu"/>
</div>
<div class="lyrico-lyrics-wrapper">koraiyaama koduppea
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koraiyaama koduppea"/>
</div>
<div class="lyrico-lyrics-wrapper">panja radhame varuvayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="panja radhame varuvayaa"/>
</div>
<div class="lyrico-lyrics-wrapper">anna nadaiyazhagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anna nadaiyazhagi"/>
</div>
<div class="lyrico-lyrics-wrapper">adi thanga nerathazhagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi thanga nerathazhagi"/>
</div>
<div class="lyrico-lyrics-wrapper">sekka sevappazhagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sekka sevappazhagi"/>
</div>
<div class="lyrico-lyrics-wrapper">ean seeni siripazhagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ean seeni siripazhagi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vaadi eng kiliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaadi eng kiliye"/>
</div>
<div class="lyrico-lyrics-wrapper">neetha eaa radhiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neetha eaa radhiye"/>
</div>
<div class="lyrico-lyrics-wrapper">atha maga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="atha maga "/>
</div>
<div class="lyrico-lyrics-wrapper">rathiname vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rathiname vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">enna mayakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna mayakka"/>
</div>
<div class="lyrico-lyrics-wrapper">konja neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konja neram"/>
</div>
<div class="lyrico-lyrics-wrapper">konji pesava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konji pesava"/>
</div>
<div class="lyrico-lyrics-wrapper">panju mittai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="panju mittai"/>
</div>
<div class="lyrico-lyrics-wrapper">bambarame vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bambarame vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">pesi mudinju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pesi mudinju"/>
</div>
<div class="lyrico-lyrics-wrapper">sethu vacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sethu vacha"/>
</div>
<div class="lyrico-lyrics-wrapper">sellang konjava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sellang konjava"/>
</div>
<div class="lyrico-lyrics-wrapper">porayetthi oru aasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="porayetthi oru aasa"/>
</div>
<div class="lyrico-lyrics-wrapper">usuppetthi ulukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="usuppetthi ulukka"/>
</div>
</pre>
