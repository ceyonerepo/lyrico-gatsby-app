---
title: "vennila song lyrics"
album: "Vidiyadha Iravondru Vendum"
artist: "GKV"
lyricist: "Karuppaiyaa Murugan"
director: "Karuppaiyaa Murugan"
path: "/albums/vidiyadha-iravondru-vendum-lyrics"
song: "Vennila"
image: ../../images/albumart/vidiyadha-iravondru-vendum.jpg
date: 2022-02-11
lang: tamil
youtubeLink: "https://www.youtube.com/embed/PH0ZlZLnvW0"
type: "love"
singers:
  - Uday Prakash
  - RK Vilasini
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">vennila nee en vennila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vennila nee en vennila"/>
</div>
<div class="lyrico-lyrics-wrapper">pennila nee en pennila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pennila nee en pennila"/>
</div>
<div class="lyrico-lyrics-wrapper">vennila nee en vennila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vennila nee en vennila"/>
</div>
<div class="lyrico-lyrics-wrapper">pennila nee en pennila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pennila nee en pennila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">valluvan ezhuthiya kuralu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valluvan ezhuthiya kuralu"/>
</div>
<div class="lyrico-lyrics-wrapper">un udhadai paduthirukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un udhadai paduthirukku"/>
</div>
<div class="lyrico-lyrics-wrapper">minnala pol un kannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="minnala pol un kannu"/>
</div>
<div class="lyrico-lyrics-wrapper">bulb ah palichunu iruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bulb ah palichunu iruku"/>
</div>
<div class="lyrico-lyrics-wrapper">hey vennila 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey vennila "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">uchanthala mayir uyir nenaika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uchanthala mayir uyir nenaika"/>
</div>
<div class="lyrico-lyrics-wrapper">ushnam vanthu kathakathaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ushnam vanthu kathakathaka"/>
</div>
<div class="lyrico-lyrics-wrapper">thegam mella thee pidikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thegam mella thee pidikka"/>
</div>
<div class="lyrico-lyrics-wrapper">poovum malarnthu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poovum malarnthu "/>
</div>
<div class="lyrico-lyrics-wrapper">vaasam manakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaasam manakka"/>
</div>
<div class="lyrico-lyrics-wrapper">ohoh otrainayiru rettaiyanathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ohoh otrainayiru rettaiyanathu"/>
</div>
<div class="lyrico-lyrics-wrapper">kalavikkanangal kooduthalagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalavikkanangal kooduthalagi"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu pol ithuvarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu pol ithuvarai"/>
</div>
<div class="lyrico-lyrics-wrapper">unarthathum illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unarthathum illai"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu mudiva illai nedikuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu mudiva illai nedikuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">micha penmai madai thiranthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="micha penmai madai thiranthu"/>
</div>
<div class="lyrico-lyrics-wrapper">kankal vezhiyil kadhal nuzhainthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kankal vezhiyil kadhal nuzhainthu"/>
</div>
<div class="lyrico-lyrics-wrapper">vayitrukulle oru thiravam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vayitrukulle oru thiravam"/>
</div>
<div class="lyrico-lyrics-wrapper">nee thottu thulankiya piragum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee thottu thulankiya piragum"/>
</div>
<div class="lyrico-lyrics-wrapper">hmm otrai nodiyil patri kolluthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hmm otrai nodiyil patri kolluthe"/>
</div>
<div class="lyrico-lyrics-wrapper">valikal yavum ipo valuvaimari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valikal yavum ipo valuvaimari"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu pol ithuvarai inithathu illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu pol ithuvarai inithathu illai"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu mudiva illa needikuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu mudiva illa needikuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">valluvan ezhuthiya kuralu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valluvan ezhuthiya kuralu"/>
</div>
<div class="lyrico-lyrics-wrapper">un udhadai paduthirukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un udhadai paduthirukku"/>
</div>
<div class="lyrico-lyrics-wrapper">minnala pol un kannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="minnala pol un kannu"/>
</div>
<div class="lyrico-lyrics-wrapper">bulb ah palichunu iruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bulb ah palichunu iruku"/>
</div>
<div class="lyrico-lyrics-wrapper">hey vennila 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey vennila "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vennila nee en vennila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vennila nee en vennila"/>
</div>
<div class="lyrico-lyrics-wrapper">pennila nee en pennila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pennila nee en pennila"/>
</div>
<div class="lyrico-lyrics-wrapper">vennila nee en vennila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vennila nee en vennila"/>
</div>
<div class="lyrico-lyrics-wrapper">pennila nee en pennila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pennila nee en pennila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">valluvan ezhuthiya kuralu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valluvan ezhuthiya kuralu"/>
</div>
<div class="lyrico-lyrics-wrapper">un udhadai paduthirukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un udhadai paduthirukku"/>
</div>
<div class="lyrico-lyrics-wrapper">minnala pol un kannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="minnala pol un kannu"/>
</div>
<div class="lyrico-lyrics-wrapper">bulb ah palichunu iruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bulb ah palichunu iruku"/>
</div>
<div class="lyrico-lyrics-wrapper">hey vennila 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey vennila "/>
</div>
</pre>
