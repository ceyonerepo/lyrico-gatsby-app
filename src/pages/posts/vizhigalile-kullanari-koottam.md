---
title: "vizhigalile song lyrics"
album: "Kullanari Koottam"
artist: "V. Selvaganesh"
lyricist: "Na. Muthukumar"
director: "Sribalaji"
path: "/albums/kullanari-koottam-lyrics"
song: "Vizhigalile"
image: ../../images/albumart/kullanari-koottam.jpg
date: 2011-03-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/93cGwdghkvM"
type: "love"
singers:
  - Karthik
  - Chinmayi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vizhigalile Vizhigalile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhigalile Vizhigalile"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu Pudhu Mayakkam Yaar Thanthaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu Pudhu Mayakkam Yaar Thanthaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Aruginile Varugaiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aruginile Varugaiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu Pudhu Thayakkam Yaar Thanthaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu Pudhu Thayakkam Yaar Thanthaar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnodu Irukkum Ponnaana Nimidam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnodu Irukkum Ponnaana Nimidam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaalum Thodarnthida Nenjam Yengum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaalum Thodarnthida Nenjam Yengum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhigalile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhigalile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhigalile Vizhigalile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhigalile Vizhigalile"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu Pudhu Mayakkam Yaar Thanthaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu Pudhu Mayakkam Yaar Thanthaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Aruginile Varugaiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aruginile Varugaiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu Pudhu Thayakkam Yaar Thanthaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu Pudhu Thayakkam Yaar Thanthaar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnodu Irukkum Ponnaana Nimidam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnodu Irukkum Ponnaana Nimidam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaalum Thodarnthida Nenjam Yengum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaalum Thodarnthida Nenjam Yengum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhigalile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhigalile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inbathil Ithu Enna Vagai Inbamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inbathil Ithu Enna Vagai Inbamo"/>
</div>
<div class="lyrico-lyrics-wrapper">Inbathil Ithu Enna Vagai Inbamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inbathil Ithu Enna Vagai Inbamo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadanthu Pogaiyil Parakkuthu Manadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadanthu Pogaiyil Parakkuthu Manadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thunbathil Ithu Enna Vagai Thunbamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunbathil Ithu Enna Vagai Thunbamo"/>
</div>
<div class="lyrico-lyrics-wrapper">Neruppill Erivathai Unaruthu Vayadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neruppill Erivathai Unaruthu Vayadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ithu Varai Enakku Idhu Pol Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu Varai Enakku Idhu Pol Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Iruthaya Araiyil Nadukkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruthaya Araiyil Nadukkam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanavugal Anaithum Un Pol Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavugal Anaithum Un Pol Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhidhaai Irukkuthu Enakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhidhaai Irukkuthu Enakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnodu Irukkum Ponnaana Nimidam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnodu Irukkum Ponnaana Nimidam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaalum Thodarnthida Nenjam Yengum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaalum Thodarnthida Nenjam Yengum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhigalile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhigalile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sonthathil Idhu Enna Vagai Sonthamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sonthathil Idhu Enna Vagai Sonthamo"/>
</div>
<div class="lyrico-lyrics-wrapper">Iraivan Thantha Varam Inainthathu Nenjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iraivan Thantha Varam Inainthathu Nenjam"/>
</div>
<div class="lyrico-lyrics-wrapper">Mothathil Idhu Enna Vagai Bandhamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mothathil Idhu Enna Vagai Bandhamo"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhazgal Sollavillai Purinthathu Konjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhazgal Sollavillai Purinthathu Konjam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu Enna Kanava Nijama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Enna Kanava Nijama"/>
</div>
<div class="lyrico-lyrics-wrapper">Idharkku Yaaridam Ketpen Vilakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idharkku Yaaridam Ketpen Vilakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Enna Pagala Irava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Enna Pagala Irava"/>
</div>
<div class="lyrico-lyrics-wrapper">Iravin Aruginil Sooriyan Velicham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravin Aruginil Sooriyan Velicham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnodu Irukkum Ponnaana Nimidam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnodu Irukkum Ponnaana Nimidam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaalum Thodarnthida Nenjam Yengum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaalum Thodarnthida Nenjam Yengum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhigalile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhigalile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhigalile Vizhigalile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhigalile Vizhigalile"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu Pudhu Mayakkam Yaar Thanthaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu Pudhu Mayakkam Yaar Thanthaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Aruginile Varugaiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aruginile Varugaiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu Pudhu Thayakkam Yaar Thanthaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu Pudhu Thayakkam Yaar Thanthaar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnodu Irukkum Ponnaana Nimidam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnodu Irukkum Ponnaana Nimidam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaalum Thodarnthida Nenjam Yengum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaalum Thodarnthida Nenjam Yengum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhigalile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhigalile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhigalile Vizhigalile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhigalile Vizhigalile"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu Pudhu Mayakkam Yaar Thanthaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu Pudhu Mayakkam Yaar Thanthaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhigalile Vizhigalile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhigalile Vizhigalile"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu Pudhu Mayakkam Yaar Thanthaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu Pudhu Mayakkam Yaar Thanthaar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aruginile Varugaiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aruginile Varugaiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu Pudhu Thayakkam Yaar Thanthaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu Pudhu Thayakkam Yaar Thanthaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Aruginile Varugaiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aruginile Varugaiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu Pudhu Thayakkam Yaar Thanthaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu Pudhu Thayakkam Yaar Thanthaar"/>
</div>
</pre>
