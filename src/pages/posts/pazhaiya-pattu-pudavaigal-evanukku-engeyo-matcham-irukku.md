---
title: "pazhaiya pattu pudavaigal song lyrics"
album: "Evanukku Engeyo Matcham Irukku"
artist: "Natarajan Sankaran"
lyricist: "Viveka"
director: "A R Mukesh"
path: "/albums/evanukku-engeyo-matcham-irukku-lyrics"
song: "Pazhaiya Pattu Pudavaigal"
image: ../../images/albumart/evanukku-engeyo-matcham-irukku.jpg
date: 2018-12-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/8I8z26r0SQI"
type: "happy"
singers:
  - Santhosh Hariharan
  - Natarajan Sankaran
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Palaya pattu veshtigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palaya pattu veshtigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattu paavadaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattu paavadaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kilinchirunthaalum paravalla maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kilinchirunthaalum paravalla maa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ungha veedu thedi vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ungha veedu thedi vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaancheepuram mill velaikkae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaancheepuram mill velaikkae"/>
</div>
<div class="lyrico-lyrics-wrapper">Eduthukkirom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eduthukkirom"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaangamma vaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaangamma vaanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha vandi ponaa varaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha vandi ponaa varaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaasu pozhuthu ponaa kedaikkadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasu pozhuthu ponaa kedaikkadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Palaya pattu pudavaigal (Dialogue)
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palaya pattu pudavaigal (Dialogue)"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
420
<div class="lyrico-lyrics-wrapper">Palaya pattu pudavaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palaya pattu pudavaigal"/>
</div>
420
<div class="lyrico-lyrics-wrapper">Palaya pattu pudavaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palaya pattu pudavaigal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hei oru veetta thedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei oru veetta thedu"/>
</div>
<div class="lyrico-lyrics-wrapper">Heii adhil routeh podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heii adhil routeh podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hei semma kaattu kaattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei semma kaattu kaattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hei namma ketkka yaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei namma ketkka yaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaru yaaru yaaru yaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaru yaaru yaaru yaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thodappakatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodappakatta"/>
</div>
<div class="lyrico-lyrics-wrapper">Isthiri petti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Isthiri petti"/>
</div>
<div class="lyrico-lyrics-wrapper">Paathiram pandam radio
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paathiram pandam radio"/>
</div>
<div class="lyrico-lyrics-wrapper">Plastic kooda pinja aada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Plastic kooda pinja aada"/>
</div>
<div class="lyrico-lyrics-wrapper">Kedaichaa lavatikkoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kedaichaa lavatikkoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Side ettum paarthu kondae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Side ettum paarthu kondae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirudum kalaiya kathukko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirudum kalaiya kathukko"/>
</div>
<div class="lyrico-lyrics-wrapper">Siren saththam tholaivil kettaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siren saththam tholaivil kettaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Escape aagippo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Escape aagippo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
420
<div class="lyrico-lyrics-wrapper">Palaya pattu pudavaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palaya pattu pudavaigal"/>
</div>
420
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aattaiya podalaam vaadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aattaiya podalaam vaadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vettaiyum aadalaam soodaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vettaiyum aadalaam soodaa"/>
</div>
420
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hei oru veetta thedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei oru veetta thedu"/>
</div>
<div class="lyrico-lyrics-wrapper">Heii adhil routeh podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heii adhil routeh podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Podu podu podu podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podu podu podu podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Eh endha oorum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh endha oorum"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma sondha ooru oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma sondha ooru oo"/>
</div>
<div class="lyrico-lyrics-wrapper">Eh ellaa veedum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh ellaa veedum"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma sondha veedu oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma sondha veedu oo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Katraa mootaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katraa mootaiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eh endha oorum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh endha oorum"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma sondha ooru oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma sondha ooru oo"/>
</div>
<div class="lyrico-lyrics-wrapper">Eh ellaa veedum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh ellaa veedum"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma sondha veedu oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma sondha veedu oo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eh bill gates aaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh bill gates aaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Venaam venaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venaam venaam"/>
</div>
<div class="lyrico-lyrics-wrapper">EB billu kattuna pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="EB billu kattuna pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">Fashion unavu venaa venaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fashion unavu venaa venaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ration arisi pothumadaa.aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ration arisi pothumadaa.aaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
420
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aattaiya podalaam vaadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aattaiya podalaam vaadaa"/>
</div>
420
<div class="lyrico-lyrics-wrapper">Vettaiyum aadalaam soodaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vettaiyum aadalaam soodaa"/>
</div>
420
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hei oru veetta thedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei oru veetta thedu"/>
</div>
<div class="lyrico-lyrics-wrapper">Heii adhil routeh podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heii adhil routeh podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hei semma kaattu kaattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei semma kaattu kaattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hei namma ketkka yaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei namma ketkka yaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaru yaaru yaaru yaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaru yaaru yaaru yaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thodappakatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodappakatta"/>
</div>
<div class="lyrico-lyrics-wrapper">Isthiri petti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Isthiri petti"/>
</div>
<div class="lyrico-lyrics-wrapper">Paathiram pandam radio
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paathiram pandam radio"/>
</div>
<div class="lyrico-lyrics-wrapper">Plastic kooda pinja aada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Plastic kooda pinja aada"/>
</div>
<div class="lyrico-lyrics-wrapper">Kedaichaa lavatikkoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kedaichaa lavatikkoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Side ettum paarthu kondae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Side ettum paarthu kondae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirudum kalaiya kathukko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirudum kalaiya kathukko"/>
</div>
<div class="lyrico-lyrics-wrapper">Siren saththam tholaivil kettaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siren saththam tholaivil kettaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Escape aagippo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Escape aagippo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Palaya pattu pudavaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palaya pattu pudavaigal"/>
</div>
420
</pre>