---
title: "sil sil silala song lyrics"
album: "Unnai Ninaithu"
artist: "Sirpy"
lyricist: "P. Vijay"
director: "Vikraman"
path: "/albums/unnai-ninaithu-lyrics"
song: "Sil Sil Silala"
image: ../../images/albumart/unnai-ninaithu.jpg
date: 2002-05-10
lang: tamil
youtubeLink: "https://www.youtube.com/embed/bcVfu_217v0"
type: "love"
singers:
  - P. Unnikrishnan
  - Sujatha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Sil sil sil silala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sil sil sil silala"/>
</div>
<div class="lyrico-lyrics-wrapper">Sil sil sil silala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sil sil sil silala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sil sil sil silala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sil sil sil silala"/>
</div>
<div class="lyrico-lyrics-wrapper">Sol sol nee minnalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sol sol nee minnalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sil sil sil silala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sil sil sil silala"/>
</div>
<div class="lyrico-lyrics-wrapper">Sol sol nee minnalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sol sol nee minnalaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee kaadhal aevaalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kaadhal aevaalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kangal koorvaalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kangal koorvaalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee saaralaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee saaralaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Isai thooralaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Isai thooralaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Poonjolai aanavalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poonjolai aanavalaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sil sil sil silala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sil sil sil silala"/>
</div>
<div class="lyrico-lyrics-wrapper">Sol sol naan minnalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sol sol naan minnalaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee irukkum naalil ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee irukkum naalil ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Imayathin melae iruppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imayathin melae iruppen"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum ingu illaa naalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum ingu illaa naalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Enmeethu imayam irukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enmeethu imayam irukkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ahimsaiyaai arugil vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahimsaiyaai arugil vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanmuraiyil irangugiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanmuraiyil irangugiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirppamae ennadi maayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirppamae ennadi maayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirppiyai sethukkigiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirppiyai sethukkigiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru swaasam podhumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru swaasam podhumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naamum vaalalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naamum vaalalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sil sil sil silala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sil sil sil silala"/>
</div>
<div class="lyrico-lyrics-wrapper">Sol sol nee minnalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sol sol nee minnalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sil sil sil silala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sil sil sil silala"/>
</div>
<div class="lyrico-lyrics-wrapper">Sol sol naan minnalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sol sol naan minnalaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal oru nyaabaga marathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal oru nyaabaga marathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaiyae naanum maranthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaiyae naanum maranthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaiyae neeyum maranthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaiyae neeyum maranthaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Maranthathaal ondraai inainthom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maranthathaal ondraai inainthom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai pol kavithai sonnaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai pol kavithai sonnaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagamae thalai aattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagamae thalai aattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nammai pol kaadhalar paarthaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nammai pol kaadhalar paarthaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Tajmahal kai thattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tajmahal kai thattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhalennum pulliyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalennum pulliyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhoomi ullathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhoomi ullathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sil sil sil silala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sil sil sil silala"/>
</div>
<div class="lyrico-lyrics-wrapper">Sol sol nee minnalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sol sol nee minnalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sil sil sil silala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sil sil sil silala"/>
</div>
<div class="lyrico-lyrics-wrapper">Sol sol naan minnalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sol sol naan minnalaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee kaadhal aevaalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kaadhal aevaalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kangal koorvaalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kangal koorvaalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee saaralaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee saaralaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Isai thooralaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Isai thooralaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Poonjolai aanavalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poonjolai aanavalaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sil sil sil silala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sil sil sil silala"/>
</div>
<div class="lyrico-lyrics-wrapper">Sol sol naan minnalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sol sol naan minnalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sil sil sil silala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sil sil sil silala"/>
</div>
<div class="lyrico-lyrics-wrapper">Sol sol nee minnalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sol sol nee minnalaa"/>
</div>
</pre>
