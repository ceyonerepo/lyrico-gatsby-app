---
title: "tasaku tasaku song lyrics"
album: "EPCO 302"
artist: "Alex Paul"
lyricist: "Rajagunasekar"
director: "Salangai Durai"
path: "/albums/epco-302-lyrics"
song: "Tasaku Tasaku"
image: ../../images/albumart/epco-302.jpg
date: 2021-12-31
lang: tamil
youtubeLink: "https://www.youtube.com/embed/OZ0dib_R4-g"
type: "happy"
singers:
  - Hemambika
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">tasaku tasaku dum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tasaku tasaku dum"/>
</div>
<div class="lyrico-lyrics-wrapper">tasaku tasaku dum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tasaku tasaku dum"/>
</div>
<div class="lyrico-lyrics-wrapper">tasaku tasaku dum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tasaku tasaku dum"/>
</div>
<div class="lyrico-lyrics-wrapper">tasaku tasaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tasaku tasaku"/>
</div>
<div class="lyrico-lyrics-wrapper">tasaku tasaku dum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tasaku tasaku dum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaathirunthe pazhagi pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaathirunthe pazhagi pochu"/>
</div>
<div class="lyrico-lyrics-wrapper">vaalkai pathai mariyachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalkai pathai mariyachu"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhal mattum vanthu vital
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhal mattum vanthu vital"/>
</div>
<div class="lyrico-lyrics-wrapper">unaku ellam sontham aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unaku ellam sontham aachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaathirunthe pazhagi pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaathirunthe pazhagi pochu"/>
</div>
<div class="lyrico-lyrics-wrapper">vaalkai pathai mariyachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalkai pathai mariyachu"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhal mattum vanthu vital
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhal mattum vanthu vital"/>
</div>
<div class="lyrico-lyrics-wrapper">unaku ellam sontham aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unaku ellam sontham aachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oodi poga ethuvum pakathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oodi poga ethuvum pakathe"/>
</div>
<div class="lyrico-lyrics-wrapper">vega thadai velaiku aagathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vega thadai velaiku aagathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kathaluku jaathi illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathaluku jaathi illai"/>
</div>
<div class="lyrico-lyrics-wrapper">kattupadu ethum illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kattupadu ethum illai"/>
</div>
<div class="lyrico-lyrics-wrapper">petham ellam verum paadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="petham ellam verum paadu"/>
</div>
<div class="lyrico-lyrics-wrapper">pengal inam munthi vara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pengal inam munthi vara"/>
</div>
<div class="lyrico-lyrics-wrapper">peatham inge marainthidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="peatham inge marainthidum"/>
</div>
<div class="lyrico-lyrics-wrapper">perumaiku kurai ethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="perumaiku kurai ethu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaathirunthe pazhagi pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaathirunthe pazhagi pochu"/>
</div>
<div class="lyrico-lyrics-wrapper">vaalkai pathai mariyachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalkai pathai mariyachu"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhal mattum vanthu vital
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhal mattum vanthu vital"/>
</div>
<div class="lyrico-lyrics-wrapper">unaku ellam sontham aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unaku ellam sontham aachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nilai illatha ulagam puriyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nilai illatha ulagam puriyum"/>
</div>
<div class="lyrico-lyrics-wrapper">thalaipe illa valakum kuraiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalaipe illa valakum kuraiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">neeril kooda vilakku eriyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeril kooda vilakku eriyum"/>
</div>
<div class="lyrico-lyrics-wrapper">kadalil muthu nagaiyil inaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadalil muthu nagaiyil inaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">athu pol kadhal unnai inaikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu pol kadhal unnai inaikum"/>
</div>
<div class="lyrico-lyrics-wrapper">nandhavanam naalum poo pookum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nandhavanam naalum poo pookum"/>
</div>
<div class="lyrico-lyrics-wrapper">sirika sirika valve sirakum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sirika sirika valve sirakum"/>
</div>
<div class="lyrico-lyrics-wrapper">sikkal valaigal vilagi kidakum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sikkal valaigal vilagi kidakum"/>
</div>
<div class="lyrico-lyrics-wrapper">sirika sirika valve sirakum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sirika sirika valve sirakum"/>
</div>
<div class="lyrico-lyrics-wrapper">sikkal valaigal vilagi kidakum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sikkal valaigal vilagi kidakum"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhal pole inimai kidaiyathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhal pole inimai kidaiyathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kathaluku jaathi illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathaluku jaathi illai"/>
</div>
<div class="lyrico-lyrics-wrapper">kattupadu ethum illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kattupadu ethum illai"/>
</div>
<div class="lyrico-lyrics-wrapper">petham ellam verum paadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="petham ellam verum paadu"/>
</div>
<div class="lyrico-lyrics-wrapper">pengal inam munthi vara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pengal inam munthi vara"/>
</div>
<div class="lyrico-lyrics-wrapper">peatham inge marainthidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="peatham inge marainthidum"/>
</div>
<div class="lyrico-lyrics-wrapper">perumaiku kurai ethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="perumaiku kurai ethu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaathirunthe pazhagi pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaathirunthe pazhagi pochu"/>
</div>
<div class="lyrico-lyrics-wrapper">vaalkai pathai mariyachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalkai pathai mariyachu"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhal mattum vanthu vital
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhal mattum vanthu vital"/>
</div>
<div class="lyrico-lyrics-wrapper">unaku ellam sontham aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unaku ellam sontham aachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nirupa anika mudiyum aana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nirupa anika mudiyum aana"/>
</div>
<div class="lyrico-lyrics-wrapper">nenappa anaika mudiyathu aiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenappa anaika mudiyathu aiya"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhal seitha kaalam inikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhal seitha kaalam inikum"/>
</div>
<div class="lyrico-lyrics-wrapper">veliya maraikum veliya mayangum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veliya maraikum veliya mayangum"/>
</div>
<div class="lyrico-lyrics-wrapper">nilamai thane ellor valakam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nilamai thane ellor valakam"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhal mogam undu evarkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhal mogam undu evarkum"/>
</div>
<div class="lyrico-lyrics-wrapper">kaigal rendil unnai maati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaigal rendil unnai maati"/>
</div>
<div class="lyrico-lyrics-wrapper">mutham sinthum latcham thandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mutham sinthum latcham thandi"/>
</div>
<div class="lyrico-lyrics-wrapper">kaigal rendil unnai maati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaigal rendil unnai maati"/>
</div>
<div class="lyrico-lyrics-wrapper">mutham sinthum latcham thandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mutham sinthum latcham thandi"/>
</div>
<div class="lyrico-lyrics-wrapper">unthan vaalvu sorgam aagumada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unthan vaalvu sorgam aagumada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kathaluku jaathi illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathaluku jaathi illai"/>
</div>
<div class="lyrico-lyrics-wrapper">kattupadu ethum illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kattupadu ethum illai"/>
</div>
<div class="lyrico-lyrics-wrapper">petham ellam verum paadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="petham ellam verum paadu"/>
</div>
<div class="lyrico-lyrics-wrapper">pengal inam munthi vara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pengal inam munthi vara"/>
</div>
<div class="lyrico-lyrics-wrapper">peatham inge marainthidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="peatham inge marainthidum"/>
</div>
<div class="lyrico-lyrics-wrapper">perumaiku kurai ethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="perumaiku kurai ethu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaathirunthe pazhagi pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaathirunthe pazhagi pochu"/>
</div>
<div class="lyrico-lyrics-wrapper">vaalkai pathai mariyachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalkai pathai mariyachu"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhal mattum vanthu vital
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhal mattum vanthu vital"/>
</div>
<div class="lyrico-lyrics-wrapper">unaku ellam sontham aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unaku ellam sontham aachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oodi poga ethuvum pakathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oodi poga ethuvum pakathe"/>
</div>
<div class="lyrico-lyrics-wrapper">vega thadai velaiku aagathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vega thadai velaiku aagathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kathaluku jaathi illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathaluku jaathi illai"/>
</div>
<div class="lyrico-lyrics-wrapper">kattupadu ethum illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kattupadu ethum illai"/>
</div>
<div class="lyrico-lyrics-wrapper">petham ellam verum paadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="petham ellam verum paadu"/>
</div>
<div class="lyrico-lyrics-wrapper">pengal inam munthi vara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pengal inam munthi vara"/>
</div>
<div class="lyrico-lyrics-wrapper">peatham inge marainthidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="peatham inge marainthidum"/>
</div>
<div class="lyrico-lyrics-wrapper">perumaiku kurai ethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="perumaiku kurai ethu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kathaluku jaathi illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathaluku jaathi illai"/>
</div>
<div class="lyrico-lyrics-wrapper">kattupadu ethum illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kattupadu ethum illai"/>
</div>
<div class="lyrico-lyrics-wrapper">petham ellam verum paadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="petham ellam verum paadu"/>
</div>
<div class="lyrico-lyrics-wrapper">pengal inam munthi vara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pengal inam munthi vara"/>
</div>
<div class="lyrico-lyrics-wrapper">peatham inge marainthidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="peatham inge marainthidum"/>
</div>
<div class="lyrico-lyrics-wrapper">perumaiku kurai ethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="perumaiku kurai ethu"/>
</div>
</pre>
