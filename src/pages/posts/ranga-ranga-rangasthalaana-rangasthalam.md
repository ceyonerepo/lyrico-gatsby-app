---
title: "ranga ranga rangasthalaana song lyrics"
album: "Rangasthalam"
artist: "Devi Sri Prasad"
lyricist: "Chandrabose"
director: "Sukumar"
path: "/albums/rangasthalam-lyrics"
song: "Ranga Ranga Rangasthalaana"
image: ../../images/albumart/rangasthalam.jpg
date: 2022-06-30
lang: telugu
youtubeLink: "https://www.youtube.com/embed/8Ah68MjWODE"
type: "happy"
singers:
  -	Rahul Sipligunj
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ranga Ranga Rangasthalana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ranga Ranga Rangasthalana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ranga Ranga Rangasthalaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ranga Ranga Rangasthalaanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Rangu Poosukokunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangu Poosukokunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Yesamesukokunnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yesamesukokunnaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aata Bommalam Anta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aata Bommalam Anta"/>
</div>
<div class="lyrico-lyrics-wrapper">Manamanthaa Tholubommalam Anta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manamanthaa Tholubommalam Anta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aata Bommalam Anta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aata Bommalam Anta"/>
</div>
<div class="lyrico-lyrics-wrapper">Manamanthaa Tholubommalam Anta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manamanthaa Tholubommalam Anta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ranga Ranga Rangasthalaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ranga Ranga Rangasthalaanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aata Modalettaaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aata Modalettaaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhyaloni Aapaleni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhyaloni Aapaleni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aata Bommalam Anta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aata Bommalam Anta"/>
</div>
<div class="lyrico-lyrics-wrapper">Manamanthaa Tholubommalam Anta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manamanthaa Tholubommalam Anta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aata Bommalam Anta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aata Bommalam Anta"/>
</div>
<div class="lyrico-lyrics-wrapper">Manamanthaa Tholubommalam Anta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manamanthaa Tholubommalam Anta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanapadani Cheyyedo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanapadani Cheyyedo"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadisthunnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadisthunnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aata Bommalam Antaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aata Bommalam Antaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Inapadani Paataki Sindaadestunnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inapadani Paataki Sindaadestunnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholu Bommalam Anta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholu Bommalam Anta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dunguru Dunguru Dunguru Dumuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dunguru Dunguru Dunguru Dumuko"/>
</div>
<div class="lyrico-lyrics-wrapper">Dunguru Dunguru Dunguru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dunguru Dunguru Dunguru "/>
</div>
<div class="lyrico-lyrics-wrapper">Dunguru Dunguru Dunguru Dumuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dunguru Dunguru Dunguru Dumuko"/>
</div>
<div class="lyrico-lyrics-wrapper">Dunguru Dunguru Dunguru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dunguru Dunguru Dunguru "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ranga Ranga Rangasthalaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ranga Ranga Rangasthalaanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Rangu Poosukokunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangu Poosukokunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Yesamesukokunnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yesamesukokunnaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aata Bommalam Anta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aata Bommalam Anta"/>
</div>
<div class="lyrico-lyrics-wrapper">Manamanthaa Tholubommalam Anta 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manamanthaa Tholubommalam Anta "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aata Bommalam Anta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aata Bommalam Anta"/>
</div>
<div class="lyrico-lyrics-wrapper">Manamanthaa Tholubommalam Anta 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manamanthaa Tholubommalam Anta "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gangante Shivudi Gaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gangante Shivudi Gaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Pellaam Anta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pellaam Anta"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaalante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaalante"/>
</div>
<div class="lyrico-lyrics-wrapper">Hanumanthudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hanumanthudi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanna Gaaranta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanna Gaaranta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gaali Peelchadaanikaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaali Peelchadaanikaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Gonthu Thadavadaanikaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gonthu Thadavadaanikaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaallu Kanukarinchaalanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaallu Kanukarinchaalanta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Venuvante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venuvante"/>
</div>
<div class="lyrico-lyrics-wrapper">Kittamurthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kittamurthi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaadhyam Anta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadhyam Anta"/>
</div>
<div class="lyrico-lyrics-wrapper">Shoolamante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shoolamante"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalikamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalikamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayudhamanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayudhamanta"/>
</div>
<div class="lyrico-lyrics-wrapper">Paata Paadatanikaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paata Paadatanikaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Potu Podavataanikaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Potu Podavataanikaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaallu Aanathisthene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaallu Aanathisthene"/>
</div>
<div class="lyrico-lyrics-wrapper">Anni Jarigevanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anni Jarigevanta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ranga Ranga Rangasthalaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ranga Ranga Rangasthalaanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Rangu Poosukokunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangu Poosukokunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Yesamesukokunnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yesamesukokunnaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aata Bommalam Anta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aata Bommalam Anta"/>
</div>
<div class="lyrico-lyrics-wrapper">Manamanthaa Tholubommalam Anta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manamanthaa Tholubommalam Anta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aata Bommalam Anta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aata Bommalam Anta"/>
</div>
<div class="lyrico-lyrics-wrapper">Manamanthaa Tholubommalam Anta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manamanthaa Tholubommalam Anta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dunguru Dunguru Dunguru Dumuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dunguru Dunguru Dunguru Dumuko"/>
</div>
<div class="lyrico-lyrics-wrapper">Dunguru Dunguru Dunguru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dunguru Dunguru Dunguru"/>
</div>
<div class="lyrico-lyrics-wrapper">Dunguru Dunguru Dunguru Dumuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dunguru Dunguru Dunguru Dumuko"/>
</div>
<div class="lyrico-lyrics-wrapper">Dunguru Dunguru Dunguru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dunguru Dunguru Dunguru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Padi Thalalu Unnodu Raavanudantaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padi Thalalu Unnodu Raavanudantaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Okka Thalapu Kooda 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okka Thalapu Kooda "/>
</div>
<div class="lyrico-lyrics-wrapper">Chedu Leka Ramudi Kantaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chedu Leka Ramudi Kantaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Raama Raavanula Betti 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raama Raavanula Betti "/>
</div>
<div class="lyrico-lyrics-wrapper">Raamaayanam Aata Gatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raamaayanam Aata Gatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Manchi Chedula Madhya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manchi Chedula Madhya "/>
</div>
<div class="lyrico-lyrics-wrapper">Manani Pettaarantaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manani Pettaarantaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dharmaanni Thappanodu Dharmarajata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dharmaanni Thappanodu Dharmarajata"/>
</div>
<div class="lyrico-lyrics-wrapper">Daya Leni Vadu Yamadharmarajata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daya Leni Vadu Yamadharmarajata"/>
</div>
<div class="lyrico-lyrics-wrapper">Veedi Baata Nadavakunte 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veedi Baata Nadavakunte "/>
</div>
<div class="lyrico-lyrics-wrapper">Vaadi Vetu Thappadantu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadi Vetu Thappadantu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Brathukuni Naatakanga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Brathukuni Naatakanga "/>
</div>
<div class="lyrico-lyrics-wrapper">Aadistunnaaranta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadistunnaaranta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ranga Ranga Rangasthalaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ranga Ranga Rangasthalaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadadaanikante Mundu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadadaanikante Mundu "/>
</div>
<div class="lyrico-lyrics-wrapper">Saadhanantu Cheyyaleni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saadhanantu Cheyyaleni"/>
</div>
<div class="lyrico-lyrics-wrapper">Aata Bommalam Anta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aata Bommalam Anta"/>
</div>
<div class="lyrico-lyrics-wrapper">Manamanthaa Tholu Bommalam Anta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manamanthaa Tholu Bommalam Anta"/>
</div>
<div class="lyrico-lyrics-wrapper">Aata Bommalam Anta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aata Bommalam Anta"/>
</div>
<div class="lyrico-lyrics-wrapper">Manamanthaa Tholu Bommalam Anta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manamanthaa Tholu Bommalam Anta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dunguru Dunguru Dunguru Dumuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dunguru Dunguru Dunguru Dumuko"/>
</div>
<div class="lyrico-lyrics-wrapper">Dunguru Dunguru Dunguru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dunguru Dunguru Dunguru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dunguru Dunguru Dunguru Dumuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dunguru Dunguru Dunguru Dumuko"/>
</div>
<div class="lyrico-lyrics-wrapper">Dunguru Dunguru Dunguru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dunguru Dunguru Dunguru"/>
</div>
</pre>
