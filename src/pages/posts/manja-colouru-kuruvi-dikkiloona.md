---
title: "manja colouru kuruvi lyrics"
album: "Dikkiloona"
artist: "Yuvan shankar raja"
lyricist: "Saravedi Saran"
director: "Karthik Yogi"
path: "/albums/dikkiloona-song-lyrics"
song: "Manja Colouru Kuruvi"
image: ../../images/albumart/dikkiloona.jpg
date: 2021-09-10
lang: tamil
youtubeLink: "https://www.youtube.com/embed/06pbgwi8ITQ"
type: "happy"
singers:
  - Shyam Vishwanathan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ava manja colouru kuruvi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava manja colouru kuruvi"/>
</div>
<div class="lyrico-lyrics-wrapper">En manasa potta uruvi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En manasa potta uruvi"/>
</div>
<div class="lyrico-lyrics-wrapper">Maanja potta noolu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maanja potta noolu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippo arundhu pochu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo arundhu pochu da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ava kutralathu aruvi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava kutralathu aruvi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan dhamma thoondu karuvi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan dhamma thoondu karuvi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattampoochi paadhiyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattampoochi paadhiyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Parandhu pochu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parandhu pochu da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha ponnungallamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha ponnungallamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Romba mosam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba mosam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ha haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ha haa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava pesum pechellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava pesum pechellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Veli vesam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veli vesam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ha haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ha haa"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha kaadhal ellamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha kaadhal ellamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sila maasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sila maasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ha haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ha haa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai kola naasam senji putta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai kola naasam senji putta"/>
</div>
<div class="lyrico-lyrics-wrapper">Da da da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Da da da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aasai pattu ketta ava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai pattu ketta ava"/>
</div>
<div class="lyrico-lyrics-wrapper">Aavinu milk-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aavinu milk-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava veettu pakkam poyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava veettu pakkam poyi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan vangunen bulb-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan vangunen bulb-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhaga paathu vizhunthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhaga paathu vizhunthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava halwa pechula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava halwa pechula"/>
</div>
<div class="lyrico-lyrics-wrapper">Aandi aagi nikka vacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aandi aagi nikka vacha"/>
</div>
<div class="lyrico-lyrics-wrapper">Marina beach-la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marina beach-la"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai suththa vutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai suththa vutta"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru gadigarama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru gadigarama"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan kanneer vitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan kanneer vitten"/>
</div>
<div class="lyrico-lyrics-wrapper">Kann vizhiyorama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kann vizhiyorama"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada idhukku mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada idhukku mela"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini evalum vena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini evalum vena"/>
</div>
<div class="lyrico-lyrics-wrapper">Vena vena en kavala theera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vena vena en kavala theera"/>
</div>
<div class="lyrico-lyrics-wrapper">Paduren gaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paduren gaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha ponnungallamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha ponnungallamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Romba mosam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba mosam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ha haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ha haa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava pesum pechellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava pesum pechellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Veli vesam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veli vesam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ha haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ha haa"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha kaadhal ellamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha kaadhal ellamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sila maasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sila maasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ha haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ha haa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai kola naasam senji putta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai kola naasam senji putta"/>
</div>
<div class="lyrico-lyrics-wrapper">Da da da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Da da da"/>
</div>
</pre>
