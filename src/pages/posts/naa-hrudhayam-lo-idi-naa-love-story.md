---
title: "naa hrudhayam lo song lyrics"
album: "Idi Naa Love Story"
artist: "Srinath Vijay"
lyricist: "Gopi - Ramesh"
director: "Ramesh Gopi"
path: "/albums/idi-naa-love-story-lyrics"
song: "Naa Hrudhayam Lo"
image: ../../images/albumart/idi-naa-love-story.jpg
date: 2018-02-14
lang: telugu
youtubeLink: "https://www.youtube.com/embed/ati2ld_JrUI"
type: "sad"
singers:
  -	Abhay Jodhpurkar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Naa Hrudhayamlo Nuvunnaavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Hrudhayamlo Nuvunnaavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Praanam Nuvvega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Praanam Nuvvega"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannerai Karigi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannerai Karigi "/>
</div>
<div class="lyrico-lyrics-wrapper">Vilapisthundhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilapisthundhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Manase Choodavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manase Choodavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa Hrudhayamlo Nuvunnaavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Hrudhayamlo Nuvunnaavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Praanam Nuvvega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Praanam Nuvvega"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannerai Karigi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannerai Karigi "/>
</div>
<div class="lyrico-lyrics-wrapper">Vilapisthundhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilapisthundhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Manase Choodavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manase Choodavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa Manase Naatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Manase Naatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Mounange Vundhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mounange Vundhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Premake llaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Premake llaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvve Saakshi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvve Saakshi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Prema Kathaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Prema Kathaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Yevare Dhikku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yevare Dhikku "/>
</div>
<div class="lyrico-lyrics-wrapper">Marichaavu Nanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marichaavu Nanne"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvve Naaku Janmantha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvve Naaku Janmantha "/>
</div>
<div class="lyrico-lyrics-wrapper">Thodu Anukunaanu Le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodu Anukunaanu Le"/>
</div>
<div class="lyrico-lyrics-wrapper">Kshanamaina Neeku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kshanamaina Neeku "/>
</div>
<div class="lyrico-lyrics-wrapper">Gurthaina Raaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gurthaina Raaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Antham Ayindhi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antham Ayindhi "/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Prema Kathaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Prema Kathaki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa Hrudhayamlo Nuvunnaavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Hrudhayamlo Nuvunnaavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Praanam Nuvvega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Praanam Nuvvega"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannerai Karigi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannerai Karigi "/>
</div>
<div class="lyrico-lyrics-wrapper">Vilapisthundhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilapisthundhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Manase Choodavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manase Choodavaa"/>
</div>
</pre>
