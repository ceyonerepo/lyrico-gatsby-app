---
title: "paatu solli paada solli song lyrics"
album: "Azhagi"
artist: "Ilaiyaraaja"
lyricist: "Ilaiyaraaja"
director: "Thangar Bachchan"
path: "/albums/azhagi-lyrics"
song: "Paatu Solli Paada Solli"
image: ../../images/albumart/azhagi.jpg
date: 2002-01-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/bUIqDNk48xg"
type: "melody"
singers:
  - Sadhana Sargam
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Paattu Cholli Paada Cholli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paattu Cholli Paada Cholli"/>
</div>
<div class="lyrico-lyrics-wrapper">Kungumam Vandhadhamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kungumam Vandhadhamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Kettu Kolla Kitta Vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kettu Kolla Kitta Vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mangalam Thandhadhamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mangalam Thandhadhamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Kungumamum Mangalamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kungumamum Mangalamum"/>
</div>
<div class="lyrico-lyrics-wrapper">Otti Vandha Rettai Kuzhanthaiyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Otti Vandha Rettai Kuzhanthaiyadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandhanathu Sindhu Ondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhanathu Sindhu Ondru"/>
</div>
<div class="lyrico-lyrics-wrapper">Katti Kondu Mettondru Thandhadhadieee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katti Kondu Mettondru Thandhadhadieee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paattu Cholli Paada Cholli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paattu Cholli Paada Cholli"/>
</div>
<div class="lyrico-lyrics-wrapper">Kungumam Vandhadhamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kungumam Vandhadhamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Kettu Kolla Kitta Vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kettu Kolla Kitta Vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mangalam Thandhadhamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mangalam Thandhadhamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ilamaiyile Kanavugalil Midhandhu Sendren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilamaiyile Kanavugalil Midhandhu Sendren"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanimaiyile Alaiyadithu Odhungi Vandhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanimaiyile Alaiyadithu Odhungi Vandhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanavillin Varavudhanai Yaar Arivaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanavillin Varavudhanai Yaar Arivaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhakai Sellum Paadhaithanai Yaar Uraippaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhakai Sellum Paadhaithanai Yaar Uraippaar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irul Thodangidum Merku Angu Innum Iruppathu Edharku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irul Thodangidum Merku Angu Innum Iruppathu Edharku"/>
</div>
<div class="lyrico-lyrics-wrapper">Oli Thodangidum Kizhakku Undu Podhuvinil Oru Vilakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oli Thodangidum Kizhakku Undu Podhuvinil Oru Vilakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Oli Irukkumidam Kizhakkumillai Merkumillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oli Irukkumidam Kizhakkumillai Merkumillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paattu Cholli Paada Cholli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paattu Cholli Paada Cholli"/>
</div>
<div class="lyrico-lyrics-wrapper">Kungumam Vandhadhamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kungumam Vandhadhamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Kettu Kolla Kitta Vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kettu Kolla Kitta Vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mangalam Thandhadhamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mangalam Thandhadhamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pudhiya Isai Kadhavu Indru Thirandhadhamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhiya Isai Kadhavu Indru Thirandhadhamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Sevi Unara Isaiyai Manam Unarndhadhamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sevi Unara Isaiyai Manam Unarndhadhamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Idam Kodutha Deivam Adhai Arindhu Konden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idam Kodutha Deivam Adhai Arindhu Konden"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhthi Adhai Vanangi Nindre Vaazhndhiduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhthi Adhai Vanangi Nindre Vaazhndhiduven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andru Sendra Ilam Paruvam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andru Sendra Ilam Paruvam"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhai Enna Enna Manam Niraiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai Enna Enna Manam Niraiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Andru Izhandhadhu Meendum Endhan Kaiyil Kidaithadhu Varame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andru Izhandhadhu Meendum Endhan Kaiyil Kidaithadhu Varame"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhai Kai Pidithe Thodarndhu Selven Kalakamillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai Kai Pidithe Thodarndhu Selven Kalakamillai"/>
</div>
</pre>
