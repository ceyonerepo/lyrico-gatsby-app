---
title: "edho edho kotha hai song lyrics"
album: "Life Anubhavinchu Raja"
artist: "Ram"
lyricist: "Srinivasa Mouli"
director: "Suresh Thirumur"
path: "/albums/life-anubhavinchu-raja-lyrics"
song: "Edho Edho Kotha Hai"
image: ../../images/albumart/life-anubhavinchu-raja.jpg
date: 2020-02-14
lang: telugu
youtubeLink: "https://www.youtube.com/embed/aazC85iUyf4"
type: "love"
singers:
  - Karthik
  - Swetha Mohan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">sa sa nisa sasa nisa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sa sa nisa sasa nisa"/>
</div>
<div class="lyrico-lyrics-wrapper">sapa pamagarisari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sapa pamagarisari"/>
</div>
<div class="lyrico-lyrics-wrapper">sa sa nisa sasa nisa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sa sa nisa sasa nisa"/>
</div>
<div class="lyrico-lyrics-wrapper">sasa sarigapamagari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sasa sarigapamagari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aedo Aedo Kotta Haayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aedo Aedo Kotta Haayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannae Allinadae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannae Allinadae"/>
</div>
<div class="lyrico-lyrics-wrapper">Emto Teepini Pondi Praanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emto Teepini Pondi Praanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Komcham Tullinadae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Komcham Tullinadae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aedo Aedo Kotta Haayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aedo Aedo Kotta Haayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannae Allinadae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannae Allinadae"/>
</div>
<div class="lyrico-lyrics-wrapper">Emto Teepini Pondi Praanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emto Teepini Pondi Praanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Komcham Tullinadae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Komcham Tullinadae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naalo Nannu Naalo Nannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalo Nannu Naalo Nannu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dochaesaavulae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dochaesaavulae"/>
</div>
<div class="lyrico-lyrics-wrapper">Choostoo Choostoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choostoo Choostoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Praemai Nannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Praemai Nannu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chuttae Maayavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chuttae Maayavae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">riga riga riga riga paga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="riga riga riga riga paga"/>
</div>
<div class="lyrico-lyrics-wrapper">sari sari sari sari mari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sari sari sari sari mari"/>
</div>
<div class="lyrico-lyrics-wrapper">sagapagarisa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sagapagarisa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">riga riga riga riga paga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="riga riga riga riga paga"/>
</div>
<div class="lyrico-lyrics-wrapper">sari sari sari sari mari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sari sari sari sari mari"/>
</div>
<div class="lyrico-lyrics-wrapper">sagapagarisa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sagapagarisa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninigapa gani Sanigapa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninigapa gani Sanigapa"/>
</div>
<div class="lyrico-lyrics-wrapper">ninigapagari gapagapagapa gapari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninigapagari gapagapagapa gapari"/>
</div>
<div class="lyrico-lyrics-wrapper">sarigapari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sarigapari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nuvvoo Naenoo Oka Jagamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvoo Naenoo Oka Jagamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalaa Neelonu Mrudu Svaramaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalaa Neelonu Mrudu Svaramaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Lokam Maarchae Parichayamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lokam Maarchae Parichayamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalo Chaeraavae Priya Varamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalo Chaeraavae Priya Varamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aunaa Aunaa Idi Nijamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aunaa Aunaa Idi Nijamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Taelae Daaraedi Paravasamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Taelae Daaraedi Paravasamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">riga riga riga riga paga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="riga riga riga riga paga"/>
</div>
<div class="lyrico-lyrics-wrapper">sari sari sari sari mari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sari sari sari sari mari"/>
</div>
<div class="lyrico-lyrics-wrapper">sagapagarisa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sagapagarisa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">riga riga riga riga paga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="riga riga riga riga paga"/>
</div>
<div class="lyrico-lyrics-wrapper">sari sari sari sari mari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sari sari sari sari mari"/>
</div>
<div class="lyrico-lyrics-wrapper">sagapagarisa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sagapagarisa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aemo Aemo Kala Nijamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aemo Aemo Kala Nijamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Choopae Neevaina Sambaramaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choopae Neevaina Sambaramaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Praemae Lolo Poovanamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Praemae Lolo Poovanamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Daachae Vellaeni Parimalamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daachae Vellaeni Parimalamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Auno Kaado Anagalamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Auno Kaado Anagalamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Praanam Choostonte Priya Mahima
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Praanam Choostonte Priya Mahima"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aedo Aedo Kotta Haayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aedo Aedo Kotta Haayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannae Allinadae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannae Allinadae"/>
</div>
<div class="lyrico-lyrics-wrapper">Emto Teepini Pondi Praanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emto Teepini Pondi Praanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Komcham Tullinadae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Komcham Tullinadae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naalo Nannu Naalo Nannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalo Nannu Naalo Nannu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dochaesaavulae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dochaesaavulae"/>
</div>
<div class="lyrico-lyrics-wrapper">Choostoo Choostoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choostoo Choostoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Praemai Nannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Praemai Nannu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chuttae Maayavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chuttae Maayavae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edalayae Kudupunae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edalayae Kudupunae"/>
</div>
<div class="lyrico-lyrics-wrapper">Marachenae Naedu Rutuvulae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marachenae Naedu Rutuvulae"/>
</div>
<div class="lyrico-lyrics-wrapper">Maarutoo Virisenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maarutoo Virisenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Eedu Saakshyam Nee Manasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eedu Saakshyam Nee Manasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadalikae Erugadae Samayamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadalikae Erugadae Samayamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Choodu Manasuna Odigenae 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choodu Manasuna Odigenae "/>
</div>
<div class="lyrico-lyrics-wrapper">Svargamae Naedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Svargamae Naedu"/>
</div>
<div class="lyrico-lyrics-wrapper">Neekoo Idi Telusu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neekoo Idi Telusu"/>
</div>
</pre>
