---
title: "darling song lyrics"
album: "Chanakya"
artist: "Vishal Chandrasekhar"
lyricist: Ramajogayya Sastry"
director: "Thiru"
path: "/albums/chanakya-lyrics"
song: "Darling"
image: ../../images/albumart/chanakya.jpg
date: 2019-10-05
lang: telugu
youtubeLink: "https://www.youtube.com/embed/AmRfbzUYXek"
type: "love"
singers:
  - Harini
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Darling My Dear Darling
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Darling My Dear Darling"/>
</div>
<div class="lyrico-lyrics-wrapper">Yendhukantha Firing
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yendhukantha Firing"/>
</div>
<div class="lyrico-lyrics-wrapper">Chudamaku Chura Chura Chura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chudamaku Chura Chura Chura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Feeling Gundeloni Feeling
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Feeling Gundeloni Feeling"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallalona Waiting
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallalona Waiting"/>
</div>
<div class="lyrico-lyrics-wrapper">Gurthupatti Telusuko Jara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gurthupatti Telusuko Jara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nimmalanga Vunna Dhanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nimmalanga Vunna Dhanni"/>
</div>
<div class="lyrico-lyrics-wrapper">Ningi Dhaka Yegaresi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ningi Dhaka Yegaresi"/>
</div>
<div class="lyrico-lyrics-wrapper">Prema Geema Ledhu Antu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prema Geema Ledhu Antu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maata Thappaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maata Thappaku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaaa Kammanaina Kalalona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaa Kammanaina Kalalona"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu Nannu Kalipesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu Nannu Kalipesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Wallposter Vesinaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wallposter Vesinaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Plate Thippaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Plate Thippaku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Antha Scene Ledura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Scene Ledura"/>
</div>
<div class="lyrico-lyrics-wrapper">Aataladu Kokura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aataladu Kokura"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadapilla Aduguthondani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadapilla Aduguthondani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naatakaalu Maanara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naatakaalu Maanara"/>
</div>
<div class="lyrico-lyrics-wrapper">Daachipetta Levura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daachipetta Levura"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasulona Vunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasulona Vunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Premani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Premani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naddharalo Nadichi Vacchinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naddharalo Nadichi Vacchinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naakallalo  Thiruguthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naakallalo  Thiruguthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Em Theliyanatlu Entalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Em Theliyanatlu Entalaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poddhuponi Oosulaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poddhuponi Oosulaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naatho Paatey Gaduputhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naatho Paatey Gaduputhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gurthundanatlu Aatalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gurthundanatlu Aatalaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naaaaa Manasidhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naaaaa Manasidhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Prema Dhaadiki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Prema Dhaadiki"/>
</div>
<div class="lyrico-lyrics-wrapper">Alladuthunnadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alladuthunnadhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eeee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eeee"/>
</div>
<div class="lyrico-lyrics-wrapper">Sogasidhi Ninnu Cheradaaniki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sogasidhi Ninnu Cheradaaniki"/>
</div>
<div class="lyrico-lyrics-wrapper">Vechi Unnadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vechi Unnadhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jagamnunu Gelichinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jagamnunu Gelichinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Magasiri Madhanudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Magasiri Madhanudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aada Manusu Chadhivi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aada Manusu Chadhivi"/>
</div>
<div class="lyrico-lyrics-wrapper">Choodaraa Sarigaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choodaraa Sarigaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Darling
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Darling"/>
</div>
<div class="lyrico-lyrics-wrapper">My Dear Darling
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="My Dear Darling"/>
</div>
<div class="lyrico-lyrics-wrapper">Yendhukantha Firing
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yendhukantha Firing"/>
</div>
<div class="lyrico-lyrics-wrapper">Chudamaku Chura Chura Chura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chudamaku Chura Chura Chura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Feeling
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Feeling"/>
</div>
<div class="lyrico-lyrics-wrapper">Gundeloni Feeling
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundeloni Feeling"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallalona Waiting
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallalona Waiting"/>
</div>
<div class="lyrico-lyrics-wrapper">Gurthupatti Telusuko Jara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gurthupatti Telusuko Jara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nimmalanga Vunna Dhanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nimmalanga Vunna Dhanni"/>
</div>
<div class="lyrico-lyrics-wrapper">Ningi Dhaka Yegaresi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ningi Dhaka Yegaresi"/>
</div>
<div class="lyrico-lyrics-wrapper">Prema Geema Ledhu Antu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prema Geema Ledhu Antu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maata Thappaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maata Thappaku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kammanaina Kalalona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kammanaina Kalalona"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu Nannu Kalipesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu Nannu Kalipesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Wallposter Vesinaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wallposter Vesinaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Plate Thippaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Plate Thippaku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Antha Scene Ledura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Scene Ledura"/>
</div>
<div class="lyrico-lyrics-wrapper">Aataladu Kokura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aataladu Kokura"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadapilla Aduguthondani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadapilla Aduguthondani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naatakaalu Maanara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naatakaalu Maanara"/>
</div>
<div class="lyrico-lyrics-wrapper">Daachipetta Levura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daachipetta Levura"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasulona Vunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasulona Vunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Premani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Premani"/>
</div>
</pre>
