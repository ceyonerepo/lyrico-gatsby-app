---
title: "dil diyan gallan song lyrics"
album: "Myrah"
artist: "Sayam Qureshi - Mohit Manuja"
lyricist: "Rooppreet Dhamija"
director: "Anil Dhoble"
path: "/albums/myrah-lyrics"
song: "Dil Diyan Gallan"
image: ../../images/albumart/myrah.jpg
date: 2018-11-23
lang: hindi
youtubeLink: "https://www.youtube.com/embed/40apNVuMPXs"
type: "love"
singers:
  - Shivika Rajesh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Dil diyan gallan, tu ki jaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil diyan gallan, tu ki jaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu mera ki hai, ae rabb vi jaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu mera ki hai, ae rabb vi jaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu jadd vekhhe, ae dil mushkaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu jadd vekhhe, ae dil mushkaave"/>
</div>
<div class="lyrico-lyrics-wrapper">Dil nu milan di, dhoonda main bahaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil nu milan di, dhoonda main bahaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ranjhiyan tere kadaman te chala ae dil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ranjhiyan tere kadaman te chala ae dil"/>
</div>
<div class="lyrico-lyrics-wrapper">Jadd tu aave tu haasil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jadd tu aave tu haasil"/>
</div>
<div class="lyrico-lyrics-wrapper">Mera hi naa rahe ae dil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mera hi naa rahe ae dil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ranjhiyan jind vich meri tu ho shamil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ranjhiyan jind vich meri tu ho shamil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kade tu mainu aake mil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kade tu mainu aake mil"/>
</div>
<div class="lyrico-lyrics-wrapper">Tere bin nahiyon lagda dil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere bin nahiyon lagda dil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haan.. Haan..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haan.. Haan.."/>
</div>
<div class="lyrico-lyrics-wrapper">Haan.. Haan..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haan.. Haan.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mahiya ve haaniya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mahiya ve haaniya"/>
</div>
<div class="lyrico-lyrics-wrapper">Rehke door tu mainu na sata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rehke door tu mainu na sata"/>
</div>
<div class="lyrico-lyrics-wrapper">Ki karaan tainu ki main samjhavaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ki karaan tainu ki main samjhavaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Door door dil mann da nahiyon lagda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Door door dil mann da nahiyon lagda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Teri hassi meri khushi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teri hassi meri khushi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ae vi khushi tere te vaar di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae vi khushi tere te vaar di"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dil diyan gallan, tu ki jaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil diyan gallan, tu ki jaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu mera ki hai, ae rabb vi jaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu mera ki hai, ae rabb vi jaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu jadd vekhhe, ae dil mushkaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu jadd vekhhe, ae dil mushkaave"/>
</div>
<div class="lyrico-lyrics-wrapper">Dil nu milan di, dhoonda main bahaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil nu milan di, dhoonda main bahaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ranjhiyan tere kadaman te chala ae dil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ranjhiyan tere kadaman te chala ae dil"/>
</div>
<div class="lyrico-lyrics-wrapper">Jadd tu aave tu haasil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jadd tu aave tu haasil"/>
</div>
<div class="lyrico-lyrics-wrapper">Mera hi naa rahe ae dil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mera hi naa rahe ae dil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ranjhiyan jind vich meri tu ho shamil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ranjhiyan jind vich meri tu ho shamil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kade tu mainu aake mil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kade tu mainu aake mil"/>
</div>
<div class="lyrico-lyrics-wrapper">Tere bin nahiyon lagda dil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere bin nahiyon lagda dil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haan.. Haan..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haan.. Haan.."/>
</div>
<div class="lyrico-lyrics-wrapper">Haan.. Haan..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haan.. Haan.."/>
</div>
</pre>
