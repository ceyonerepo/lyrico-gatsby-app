---
title: "kan jaadayil ooram song lyrics"
album: "Kalavani Sirukki"
artist: "Dharun Antony"
lyricist: "unknown"
director: "Ravi Rahul"
path: "/albums/kalavani-sirukki-lyrics"
song: "Kan Jaadayil Ooram"
image: ../../images/albumart/kalavani-sirukki.jpg
date: 2018-10-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/tSOFIS1GGD0"
type: "love"
singers:
  - Unknown
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">kan jaadaiyin ooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kan jaadaiyin ooram"/>
</div>
<div class="lyrico-lyrics-wrapper">sila vannathi poochi parakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sila vannathi poochi parakka"/>
</div>
<div class="lyrico-lyrics-wrapper">man paanaiyin eeram en
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="man paanaiyin eeram en"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjodu kasinthu irukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjodu kasinthu irukka"/>
</div>
<div class="lyrico-lyrics-wrapper">un veetu suvaril varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un veetu suvaril varum"/>
</div>
<div class="lyrico-lyrics-wrapper">poongaathu en pera eluthiyathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poongaathu en pera eluthiyathe"/>
</div>
<div class="lyrico-lyrics-wrapper">nee thandum padiyil adi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee thandum padiyil adi "/>
</div>
<div class="lyrico-lyrics-wrapper">ippothu en nenju kidakurathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ippothu en nenju kidakurathe"/>
</div>
<div class="lyrico-lyrics-wrapper">paathu po un iruthayam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paathu po un iruthayam "/>
</div>
<div class="lyrico-lyrics-wrapper">kidakuthu keele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kidakuthu keele"/>
</div>
<div class="lyrico-lyrics-wrapper">en pulla enna killa pota
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en pulla enna killa pota"/>
</div>
<div class="lyrico-lyrics-wrapper">kambu illa vethalaya pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kambu illa vethalaya pola"/>
</div>
<div class="lyrico-lyrics-wrapper">karunkalla kedakene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karunkalla kedakene"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu sariyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu sariyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">thalai mela eluthuna eluththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalai mela eluthuna eluththu"/>
</div>
<div class="lyrico-lyrics-wrapper">aliyaama maranje kedakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aliyaama maranje kedakku"/>
</div>
<div class="lyrico-lyrics-wrapper">enakunnu eluthunu pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enakunnu eluthunu pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">nee thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kan jaadaiyin ooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kan jaadaiyin ooram"/>
</div>
<div class="lyrico-lyrics-wrapper">sila vannathi poochi parakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sila vannathi poochi parakka"/>
</div>
<div class="lyrico-lyrics-wrapper">man paanaiyin eeram en
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="man paanaiyin eeram en"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjodu kasinthu irukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjodu kasinthu irukka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">man vasanai varume adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="man vasanai varume adi"/>
</div>
<div class="lyrico-lyrics-wrapper">pottunu thooral vantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pottunu thooral vantha"/>
</div>
<div class="lyrico-lyrics-wrapper">pen vasanai varume ada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pen vasanai varume ada"/>
</div>
<div class="lyrico-lyrics-wrapper">sattunu kathal vanthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sattunu kathal vanthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">man keela kedakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="man keela kedakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">kadala kaayee en aasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadala kaayee en aasa"/>
</div>
<div class="lyrico-lyrics-wrapper">athu pol kedakuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu pol kedakuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">athoda pidungi muthangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athoda pidungi muthangal"/>
</div>
<div class="lyrico-lyrics-wrapper">kudupa athuvum pulachudume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kudupa athuvum pulachudume"/>
</div>
<div class="lyrico-lyrics-wrapper">en pulla enna killa pota
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en pulla enna killa pota"/>
</div>
<div class="lyrico-lyrics-wrapper">kambu illa vethalaya pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kambu illa vethalaya pola"/>
</div>
<div class="lyrico-lyrics-wrapper">karunkalla kedakene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karunkalla kedakene"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu sariyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu sariyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">thalai mela eluthuna eluththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalai mela eluthuna eluththu"/>
</div>
<div class="lyrico-lyrics-wrapper">aliyaama maranje kedakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aliyaama maranje kedakku"/>
</div>
<div class="lyrico-lyrics-wrapper">enakunnu eluthunu pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enakunnu eluthunu pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">nee thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vesatha oru naanal antha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vesatha oru naanal antha"/>
</div>
<div class="lyrico-lyrics-wrapper">eesaana moolaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eesaana moolaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">pekaaga athu maarum adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pekaaga athu maarum adi"/>
</div>
<div class="lyrico-lyrics-wrapper">nee thotta velaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee thotta velaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">poosari munne aadu pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poosari munne aadu pola"/>
</div>
<div class="lyrico-lyrics-wrapper">en kathal kedanthu mulikuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kathal kedanthu mulikuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">anboda eduthu nenjoda anachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anboda eduthu nenjoda anachu"/>
</div>
<div class="lyrico-lyrics-wrapper">konjamal ithayam adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konjamal ithayam adi"/>
</div>
<div class="lyrico-lyrics-wrapper">en pulla enna killa pota
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en pulla enna killa pota"/>
</div>
<div class="lyrico-lyrics-wrapper">kambu illa vethalaya pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kambu illa vethalaya pola"/>
</div>
<div class="lyrico-lyrics-wrapper">karunkalla kedakene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karunkalla kedakene"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu sariyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu sariyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">thalai mela eluthuna eluththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalai mela eluthuna eluththu"/>
</div>
<div class="lyrico-lyrics-wrapper">aliyaama maranje kedakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aliyaama maranje kedakku"/>
</div>
<div class="lyrico-lyrics-wrapper">enakunnu eluthunu pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enakunnu eluthunu pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">nee thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kan jaadaiyin ooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kan jaadaiyin ooram"/>
</div>
<div class="lyrico-lyrics-wrapper">sila vannathi poochi parakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sila vannathi poochi parakka"/>
</div>
<div class="lyrico-lyrics-wrapper">man paanaiyin eeram en
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="man paanaiyin eeram en"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjodu kasinthu irukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjodu kasinthu irukka"/>
</div>
<div class="lyrico-lyrics-wrapper">un veetu suvaril varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un veetu suvaril varum"/>
</div>
<div class="lyrico-lyrics-wrapper">poongaathu en pera eluthiyathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poongaathu en pera eluthiyathe"/>
</div>
<div class="lyrico-lyrics-wrapper">nee thandum padiyil adi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee thandum padiyil adi "/>
</div>
<div class="lyrico-lyrics-wrapper">ippothu en nenju kidakurathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ippothu en nenju kidakurathe"/>
</div>
<div class="lyrico-lyrics-wrapper">paathu po un iruthayam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paathu po un iruthayam "/>
</div>
<div class="lyrico-lyrics-wrapper">kidakuthu keele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kidakuthu keele"/>
</div>
<div class="lyrico-lyrics-wrapper">en pulla enna killa pota
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en pulla enna killa pota"/>
</div>
<div class="lyrico-lyrics-wrapper">kambu illa vethalaya pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kambu illa vethalaya pola"/>
</div>
<div class="lyrico-lyrics-wrapper">karunkalla kedakene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karunkalla kedakene"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu sariyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu sariyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">thalai mela eluthuna eluththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalai mela eluthuna eluththu"/>
</div>
<div class="lyrico-lyrics-wrapper">aliyaama maranje kedakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aliyaama maranje kedakku"/>
</div>
<div class="lyrico-lyrics-wrapper">enakunnu eluthunu pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enakunnu eluthunu pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">nee thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee thaan"/>
</div>
</pre>
