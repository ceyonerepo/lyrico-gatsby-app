---
title: "ga gha megha song lyrics"
album: "Chal Mohan Ranga"
artist: "S S Thaman"
lyricist: "Krishna Kanth"
director: "Krishna Chaitanya"
path: "/albums/chal-mohan-ranga-lyrics"
song: "Ga Gha Megha"
image: ../../images/albumart/chal-mohan-ranga.jpg
date: 2018-04-06
lang: telugu
youtubeLink: "https://www.youtube.com/embed/y76BJN0_0QQ"
type: "happy"
singers:
  -	Rahul Nambiar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ga Gha Ga Gha Megha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ga Gha Ga Gha Megha "/>
</div>
<div class="lyrico-lyrics-wrapper">Kanule Cheppe Kotha Saga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanule Cheppe Kotha Saga "/>
</div>
<div class="lyrico-lyrics-wrapper">Ga Gha Ga Gha Megha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ga Gha Ga Gha Megha "/>
</div>
<div class="lyrico-lyrics-wrapper">Ninge Manaku Nedu Paga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninge Manaku Nedu Paga "/>
</div>
<div class="lyrico-lyrics-wrapper">Ga Gha Ga Gha Megha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ga Gha Ga Gha Megha "/>
</div>
<div class="lyrico-lyrics-wrapper">Allesave Hayi Teega 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allesave Hayi Teega "/>
</div>
<div class="lyrico-lyrics-wrapper">Ga Gha Ga Gha Megha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ga Gha Ga Gha Megha "/>
</div>
<div class="lyrico-lyrics-wrapper">Payanam Inka Mundukega 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Payanam Inka Mundukega "/>
</div>
<div class="lyrico-lyrics-wrapper">Ilage Ilage Ilage 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilage Ilage Ilage "/>
</div>
<div class="lyrico-lyrics-wrapper">Yetepo Vellaali Antu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yetepo Vellaali Antu "/>
</div>
<div class="lyrico-lyrics-wrapper">Manasu Laage 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu Laage "/>
</div>
<div class="lyrico-lyrics-wrapper">Alaage Alaage Alaage 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alaage Alaage Alaage "/>
</div>
<div class="lyrico-lyrics-wrapper">Antune Ledento 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antune Ledento "/>
</div>
<div class="lyrico-lyrics-wrapper">Yedi Mundulaage 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedi Mundulaage "/>
</div>
<div class="lyrico-lyrics-wrapper">Ivvale Ivvale Ivvale 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivvale Ivvale Ivvale "/>
</div>
<div class="lyrico-lyrics-wrapper">Kannullo Kalalni 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannullo Kalalni "/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu Paiki Lage 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu Paiki Lage "/>
</div>
<div class="lyrico-lyrics-wrapper">Sarele Sarele 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarele Sarele "/>
</div>
<div class="lyrico-lyrics-wrapper">Gha Annanu Le Meghaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gha Annanu Le Meghaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ga Gha Ga Gha Megha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ga Gha Ga Gha Megha "/>
</div>
<div class="lyrico-lyrics-wrapper">Kanule Cheppe Kotha Saga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanule Cheppe Kotha Saga "/>
</div>
<div class="lyrico-lyrics-wrapper">Ga Gha Ga Gha Megha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ga Gha Ga Gha Megha "/>
</div>
<div class="lyrico-lyrics-wrapper">Ninge Manaku Nedu Paga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninge Manaku Nedu Paga "/>
</div>
<div class="lyrico-lyrics-wrapper">Ga Gha Ga Gha Megha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ga Gha Ga Gha Megha "/>
</div>
<div class="lyrico-lyrics-wrapper">Allesave Hayi Teega 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allesave Hayi Teega "/>
</div>
<div class="lyrico-lyrics-wrapper">Ga Gha Ga Gha Megha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ga Gha Ga Gha Megha "/>
</div>
<div class="lyrico-lyrics-wrapper">Payanam Inka Mundukega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Payanam Inka Mundukega"/>
</div>
<div class="lyrico-lyrics-wrapper">Baby Everyday 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby Everyday "/>
</div>
<div class="lyrico-lyrics-wrapper">I Write You Love Letter 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Write You Love Letter "/>
</div>
<div class="lyrico-lyrics-wrapper">I Promise 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Promise "/>
</div>
<div class="lyrico-lyrics-wrapper">I’ll Make Your Day Much Better 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I’ll Make Your Day Much Better "/>
</div>
<div class="lyrico-lyrics-wrapper">I Promise 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Promise "/>
</div>
<div class="lyrico-lyrics-wrapper">I Won’t Treat You 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Won’t Treat You "/>
</div>
<div class="lyrico-lyrics-wrapper">Like Them Others 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Like Them Others "/>
</div>
<div class="lyrico-lyrics-wrapper">I Promise I Won’t Make You 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Promise I Won’t Make You "/>
</div>
<div class="lyrico-lyrics-wrapper">Think Of The Rather 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Think Of The Rather "/>
</div>
<div class="lyrico-lyrics-wrapper">If You Looking At The Sky 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="If You Looking At The Sky "/>
</div>
<div class="lyrico-lyrics-wrapper">That’s Up Above 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="That’s Up Above "/>
</div>
<div class="lyrico-lyrics-wrapper">The Moon And The Stars 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="The Moon And The Stars "/>
</div>
<div class="lyrico-lyrics-wrapper">Are The Symbols Of My Love 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Are The Symbols Of My Love "/>
</div>
<div class="lyrico-lyrics-wrapper">Just Call Me By My Name 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Just Call Me By My Name "/>
</div>
<div class="lyrico-lyrics-wrapper">When You Need Me My Dear 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="When You Need Me My Dear "/>
</div>
<div class="lyrico-lyrics-wrapper">And I’ll Be Right There 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="And I’ll Be Right There "/>
</div>
<div class="lyrico-lyrics-wrapper">To Make Ur Problems Disappear 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="To Make Ur Problems Disappear "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gammathulo Oogamaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gammathulo Oogamaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Tullintalo Telamaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tullintalo Telamaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Inthinthalai Santhosham Maatho 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inthinthalai Santhosham Maatho "/>
</div>
<div class="lyrico-lyrics-wrapper">Sandadi Chesenaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandadi Chesenaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Hatathuga Yedalona 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hatathuga Yedalona "/>
</div>
<div class="lyrico-lyrics-wrapper">Hadavide Perigena 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hadavide Perigena "/>
</div>
<div class="lyrico-lyrics-wrapper">Amantamu Ee Chirunavvulake 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amantamu Ee Chirunavvulake "/>
</div>
<div class="lyrico-lyrics-wrapper">Artham Dorikenaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Artham Dorikenaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Oo Oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Oo Oo"/>
</div>
<div class="lyrico-lyrics-wrapper">Munde Malupu Vundoo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munde Malupu Vundoo "/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Oo Oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Oo Oo"/>
</div>
<div class="lyrico-lyrics-wrapper">Gha Annanu Le Meghaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gha Annanu Le Meghaa"/>
</div>
</pre>
