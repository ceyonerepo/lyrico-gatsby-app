---
title: "vathikuchi pathikadhuda song lyrics"
album: "Dheena"
artist: "Yuvan Shankar Raja"
lyricist: "Vaali"
director: "A.R. Murugadoss"
path: "/albums/dheena-lyrics"
song: "Vathikuchi Pathikadhuda"
image: ../../images/albumart/dheena.jpg
date: 2001-01-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/LTKldRHh4LI"
type: "mass intro"
singers:
  - S. P. Balasubramaniam
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">VathiKuchi Pathikaadhuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="VathiKuchi Pathikaadhuda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yarum Vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yarum Vandhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Urasura Varaiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urasura Varaiyile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vambu Thumbu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vambu Thumbu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vachukaadhada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vachukaadhada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaarum Unnai Usupura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarum Unnai Usupura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varaiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varaiyile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eekuchiya Illama Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eekuchiya Illama Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theekuchiya Iruda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theekuchiya Iruda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ulle Oru Ushnam Vandhaa Un
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulle Oru Ushnam Vandhaa Un"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhvil Velicham Varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvil Velicham Varum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">VathiKuchi Pathikaadhuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="VathiKuchi Pathikaadhuda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yarum Vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yarum Vandhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Urasura Varaiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urasura Varaiyile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vambu Thumbu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vambu Thumbu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vachukaadhada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vachukaadhada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaarum Unnai Usupura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarum Unnai Usupura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varaiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varaiyile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manasu Uduthina Kavalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu Uduthina Kavalai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thuni Eduthu Avizhtheri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuni Eduthu Avizhtheri"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edharku Ini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edharku Ini"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irukum Kanneeraiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irukum Kanneeraiyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yetham Nee Pottedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yetham Nee Pottedu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhavaa Inge Vandhom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhavaa Inge Vandhom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aadu Paadu Aanandhamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadu Paadu Aanandhamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">VathiKuchi Pathikaadhuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="VathiKuchi Pathikaadhuda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yarum Vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yarum Vandhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Urasura Varaiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urasura Varaiyile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vambu Thumbu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vambu Thumbu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vachukaadhada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vachukaadhada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaarum Unnai Usupura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarum Unnai Usupura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varaiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varaiyile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muyarchi Seithaal Samayathule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muyarchi Seithaal Samayathule"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mudhuku Thaangum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhuku Thaangum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Imayathaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imayathaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manasa Irumbaakanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasa Irumbaakanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Malaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malaiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thurumbaakanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thurumbaakanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aazhkadal Koodathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aazhkadal Koodathaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaru Pole Maarumadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaru Pole Maarumadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vathi Kuchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vathi Kuchi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pathikaadhuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathikaadhuda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yarum Vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yarum Vandhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Urasura Varaiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urasura Varaiyile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vambu Thumbu Vachukaadhafa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vambu Thumbu Vachukaadhafa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaarum Unnai Usupura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarum Unnai Usupura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varaiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varaiyile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eekuchiya Illama Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eekuchiya Illama Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theekuchiya Iruda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theekuchiya Iruda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ulle Oru Ushnam Vandhaa Un
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulle Oru Ushnam Vandhaa Un"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhvil Velicham Varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvil Velicham Varum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">VathiKuchi Pathikaadhuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="VathiKuchi Pathikaadhuda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yarum Vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yarum Vandhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Urasura Varaiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urasura Varaiyile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vambu Thumbu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vambu Thumbu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vachukaadhada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vachukaadhada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaarum Unnai Usupura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarum Unnai Usupura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varaiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varaiyile"/>
</div>
</pre>
