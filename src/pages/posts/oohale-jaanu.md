---
title: "oohale song lyrics"
album: "Jaanu"
artist: "Govind Vasantha"
lyricist: "Sri Mani"
director: "C. Prem Kumar"
path: "/albums/jaanu-lyrics"
song: "Oohale"
image: ../../images/albumart/jaanu.jpg
date: 2020-02-07
lang: telugu
youtubeLink: "https://www.youtube.com/embed/XkDuzNrtd4U"
type: "melody"
singers:
  - Chinmayi
  - Govind Vasantha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aa Aa Chinni Mounamulonaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Aa Chinni Mounamulonaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Enni Ee Ee Oogisalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enni Ee Ee Oogisalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanta Neeru Lenee Roju Kalisene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanta Neeru Lenee Roju Kalisene"/>
</div>
<div class="lyrico-lyrics-wrapper">Praanamulo Praana Sade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Praanamulo Praana Sade"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oohale Oohale Ninu Vidavavule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oohale Oohale Ninu Vidavavule"/>
</div>
<div class="lyrico-lyrics-wrapper">Gundeke Praanamai Poose Poose
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundeke Praanamai Poose Poose"/>
</div>
<div class="lyrico-lyrics-wrapper">Oohale Oohale Ninu Marichina Vela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oohale Oohale Ninu Marichina Vela"/>
</div>
<div class="lyrico-lyrics-wrapper">Oopire Leni Vela Aa Aa Aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oopire Leni Vela Aa Aa Aa"/>
</div>
</pre>
