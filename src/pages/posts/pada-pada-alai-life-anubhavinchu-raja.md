---
title: "pada pada alai song lyrics"
album: "Life Anubhavinchu Raja"
artist: "Ram"
lyricist: "Srinivasa Mouli"
director: "Suresh Thirumur"
path: "/albums/life-anubhavinchu-raja-lyrics"
song: "Pada Pada Alai"
image: ../../images/albumart/life-anubhavinchu-raja.jpg
date: 2020-02-14
lang: telugu
youtubeLink: "https://www.youtube.com/embed/sC-Ic3i6gxc"
type: "happy"
singers:
  - Revanth
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Pada Pada Alai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pada Pada Alai"/>
</div>
<div class="lyrico-lyrics-wrapper">Anagani Kalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anagani Kalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Adugoka Kathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adugoka Kathai"/>
</div>
<div class="lyrico-lyrics-wrapper">Padara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padara"/>
</div>
<div class="lyrico-lyrics-wrapper">Chedarani Madhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chedarani Madhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhayamunu Kudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhayamunu Kudhey"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheritavu Nuve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheritavu Nuve"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pada Pada Alai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pada Pada Alai"/>
</div>
<div class="lyrico-lyrics-wrapper">Anagani Kalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anagani Kalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Adugoka Kathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adugoka Kathai"/>
</div>
<div class="lyrico-lyrics-wrapper">Padara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padara"/>
</div>
<div class="lyrico-lyrics-wrapper">Chedarani Madhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chedarani Madhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhayamunu Kudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhayamunu Kudhey"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheritavu Nuve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheritavu Nuve"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ee Gayam Chaesae Kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Gayam Chaesae Kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo kapatam Leni Addam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo kapatam Leni Addam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Satta Nike chupi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Satta Nike chupi"/>
</div>
<div class="lyrico-lyrics-wrapper">Chesenu Ento Sayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chesenu Ento Sayam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ee Gayam Chaesae Kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Gayam Chaesae Kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo kapatam Leni Addam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo kapatam Leni Addam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Satta Nike chupi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Satta Nike chupi"/>
</div>
<div class="lyrico-lyrics-wrapper">Chesenu Ento Sayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chesenu Ento Sayam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pada Pada Alai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pada Pada Alai"/>
</div>
<div class="lyrico-lyrics-wrapper">Anagani Kalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anagani Kalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Adugoka Kathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adugoka Kathai"/>
</div>
<div class="lyrico-lyrics-wrapper">Padara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padara"/>
</div>
<div class="lyrico-lyrics-wrapper">Chedarani Madhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chedarani Madhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhayamunu Kudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhayamunu Kudhey"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheritavu Nuve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheritavu Nuve"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yea Yea Yea Yea
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yea Yea Yea Yea"/>
</div>
<div class="lyrico-lyrics-wrapper">yaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaha"/>
</div>
<div class="lyrico-lyrics-wrapper">hey hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey hey hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aatu Potu Anni Daati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aatu Potu Anni Daati"/>
</div>
<div class="lyrico-lyrics-wrapper">Roju Neepai Nuve Gelichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Roju Neepai Nuve Gelichi"/>
</div>
<div class="lyrico-lyrics-wrapper">Raajee Vaddu Vodileyoddu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raajee Vaddu Vodileyoddu"/>
</div>
<div class="lyrico-lyrics-wrapper">Teera Aakarlo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teera Aakarlo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aatu Potu Anni Daati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aatu Potu Anni Daati"/>
</div>
<div class="lyrico-lyrics-wrapper">Roju Neepai Nuve Gelichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Roju Neepai Nuve Gelichi"/>
</div>
<div class="lyrico-lyrics-wrapper">Raajee Vaddu Vodileyoddu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raajee Vaddu Vodileyoddu"/>
</div>
<div class="lyrico-lyrics-wrapper">Teera Aakarlo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teera Aakarlo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Pogaru Nee Teguva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Pogaru Nee Teguva"/>
</div>
<div class="lyrico-lyrics-wrapper">Chupistayi Ninne Sariga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chupistayi Ninne Sariga"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Kalalu Nee Viluva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kalalu Nee Viluva"/>
</div>
<div class="lyrico-lyrics-wrapper">Vere Evaro Gurtinchala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vere Evaro Gurtinchala"/>
</div>
<div class="lyrico-lyrics-wrapper">Pantamga nu Saiyyannavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pantamga nu Saiyyannavo"/>
</div>
<div class="lyrico-lyrics-wrapper">Anni Addi Poradavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anni Addi Poradavo"/>
</div>
<div class="lyrico-lyrics-wrapper">Oopiri Laga Asha Unte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oopiri Laga Asha Unte"/>
</div>
<div class="lyrico-lyrics-wrapper">Pranam Gelupavada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pranam Gelupavada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yea Yea Yea Yea
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yea Yea Yea Yea"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hay you dont reallu know
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hay you dont reallu know"/>
</div>
<div class="lyrico-lyrics-wrapper">What I’m Capable Of Yea
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="What I’m Capable Of Yea"/>
</div>
<div class="lyrico-lyrics-wrapper">Set The Mood and Hit the Button
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Set The Mood and Hit the Button"/>
</div>
<div class="lyrico-lyrics-wrapper">Set It High Yea
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Set It High Yea"/>
</div>
<div class="lyrico-lyrics-wrapper">Come On Let Me Show You
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Come On Let Me Show You"/>
</div>
<div class="lyrico-lyrics-wrapper">How I do It Hi Fi yea
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="How I do It Hi Fi yea"/>
</div>
<div class="lyrico-lyrics-wrapper">Really Cool, So Me Hustting
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Really Cool, So Me Hustting"/>
</div>
<div class="lyrico-lyrics-wrapper">On My way up So High Yea
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="On My way up So High Yea"/>
</div>
<div class="lyrico-lyrics-wrapper">Dolla Bills Don’t Mind
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dolla Bills Don’t Mind"/>
</div>
<div class="lyrico-lyrics-wrapper">Spending It Real
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Spending It Real"/>
</div>
<div class="lyrico-lyrics-wrapper">Quick Like I Own In Ma crib
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Quick Like I Own In Ma crib"/>
</div>
<div class="lyrico-lyrics-wrapper">Don’t Worry
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Don’t Worry"/>
</div>
<div class="lyrico-lyrics-wrapper">Lemme Lemme Show You Ma Hustle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lemme Lemme Show You Ma Hustle"/>
</div>
<div class="lyrico-lyrics-wrapper">Don’t worry
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Don’t worry"/>
</div>
<div class="lyrico-lyrics-wrapper">Let Me Set It High
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Let Me Set It High"/>
</div>
<div class="lyrico-lyrics-wrapper">For The Game
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="For The Game"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dooram Anna Maate Vaddu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dooram Anna Maate Vaddu"/>
</div>
<div class="lyrico-lyrics-wrapper">Daaree Tennu Alochinchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daaree Tennu Alochinchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mullo Rallo Ennunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mullo Rallo Ennunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Daatellu Shasinchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daatellu Shasinchu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dooram Anna Maate Vaddu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dooram Anna Maate Vaddu"/>
</div>
<div class="lyrico-lyrics-wrapper">Daaree Tennu Alochinchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daaree Tennu Alochinchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mullo Rallo Ennunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mullo Rallo Ennunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Daatellu Shasinchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daatellu Shasinchu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee digulu Nee Kalata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee digulu Nee Kalata"/>
</div>
<div class="lyrico-lyrics-wrapper">Swaragam Odilo Cherustaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Swaragam Odilo Cherustaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee chemata Nee telvi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee chemata Nee telvi"/>
</div>
<div class="lyrico-lyrics-wrapper">Neelo Unna Sattuva Anava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelo Unna Sattuva Anava"/>
</div>
<div class="lyrico-lyrics-wrapper">Nerrurulona Souryam Undi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nerrurulona Souryam Undi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattai Doosa Prayam Undi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattai Doosa Prayam Undi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pondalanna Kankshe Unte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pondalanna Kankshe Unte"/>
</div>
<div class="lyrico-lyrics-wrapper">Lakshyam Needera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lakshyam Needera"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pada Pada Alai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pada Pada Alai"/>
</div>
<div class="lyrico-lyrics-wrapper">Anagani Kalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anagani Kalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Adugoka Kathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adugoka Kathai"/>
</div>
<div class="lyrico-lyrics-wrapper">Padara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padara"/>
</div>
<div class="lyrico-lyrics-wrapper">Chedarani Madhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chedarani Madhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhayamunu Kudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhayamunu Kudhey"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheritavu Nuve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheritavu Nuve"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pada Pada Alai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pada Pada Alai"/>
</div>
<div class="lyrico-lyrics-wrapper">Anagani Kalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anagani Kalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Adugoka Kathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adugoka Kathai"/>
</div>
<div class="lyrico-lyrics-wrapper">Padara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padara"/>
</div>
<div class="lyrico-lyrics-wrapper">Chedarani Madhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chedarani Madhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhayamunu Kudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhayamunu Kudhey"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheritavu Nuve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheritavu Nuve"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ee Gayam Chaesae Kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Gayam Chaesae Kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo kapatam Leni Addam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo kapatam Leni Addam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Satta Nike chupi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Satta Nike chupi"/>
</div>
<div class="lyrico-lyrics-wrapper">Chesenu Ento Sayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chesenu Ento Sayam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ee Gayam Chaesae Kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Gayam Chaesae Kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo kapatam Leni Addam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo kapatam Leni Addam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Satta Nike chupi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Satta Nike chupi"/>
</div>
<div class="lyrico-lyrics-wrapper">Chesenu Ento Sayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chesenu Ento Sayam"/>
</div>
</pre>
