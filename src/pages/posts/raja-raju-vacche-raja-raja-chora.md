---
title: "raja raju vacche song lyrics"
album: "Raja Raja Chora"
artist: "Vivek Sagar"
lyricist: "Hasith Goli"
director: "Hasith Goli"
path: "/albums/raja-raja-chora-lyrics"
song: "Raja Raju Vacche"
image: ../../images/albumart/raja-raja-chora.jpg
date: 2021-08-19
lang: telugu
youtubeLink: "https://www.youtube.com/embed/4YrhUbfnb9w"
type: "mass"
singers:
  - Mohana Bhogaraju
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Allavari kukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allavari kukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Bow bow mannadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bow bow mannadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa kaalla gajja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa kaalla gajja"/>
</div>
<div class="lyrico-lyrics-wrapper">Ghal ghal mannadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ghal ghal mannadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sankalo paapa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sankalo paapa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaev kaev mannadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaev kaev mannadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Bow hey bow hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bow hey bow hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sunakamama baagu sardi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sunakamama baagu sardi"/>
</div>
<div class="lyrico-lyrics-wrapper">Baagu sardi mukkinakkinaaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baagu sardi mukkinakkinaaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Godanekki podham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Godanekki podham"/>
</div>
<div class="lyrico-lyrics-wrapper">Sunakamama thoka suttei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sunakamama thoka suttei"/>
</div>
<div class="lyrico-lyrics-wrapper">Suttina thokalle vanakaragaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suttina thokalle vanakaragaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirigedi manushuloi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirigedi manushuloi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Doralani meeku meeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Doralani meeku meeru"/>
</div>
<div class="lyrico-lyrics-wrapper">Doruluthu thirigaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Doruluthu thirigaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Chorabadi chedipote
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chorabadi chedipote"/>
</div>
<div class="lyrico-lyrics-wrapper">Chathikala padataru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chathikala padataru"/>
</div>
<div class="lyrico-lyrics-wrapper">Donga gaaru o raju garu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Donga gaaru o raju garu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanakam sunna sunna koode
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanakam sunna sunna koode"/>
</div>
<div class="lyrico-lyrics-wrapper">Sunakapu simhasanam veede
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sunakapu simhasanam veede"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey janakaa vacchadanta fradu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey janakaa vacchadanta fradu"/>
</div>
<div class="lyrico-lyrics-wrapper">Palake addhekicchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palake addhekicchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana badinodilina balapamula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana badinodilina balapamula"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadiga podiga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadiga podiga"/>
</div>
<div class="lyrico-lyrics-wrapper">Seragani marakalalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seragani marakalalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Surake padithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Surake padithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Seragavu vaathalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seragavu vaathalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pulilaa manake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pulilaa manake"/>
</div>
<div class="lyrico-lyrics-wrapper">Padavata padi saaraluu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padavata padi saaraluu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanuke thoka mudisi nadisey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanuke thoka mudisi nadisey"/>
</div>
<div class="lyrico-lyrics-wrapper">Padusu kodakaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padusu kodakaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sunakamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sunakamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thinnadantha nindu pottaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thinnadantha nindu pottaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Dochukunnadantha guttaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dochukunnadantha guttaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Sitti potti guttalekkake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sitti potti guttalekkake"/>
</div>
<div class="lyrico-lyrics-wrapper">Sitharala kota settaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sitharala kota settaaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raaja raaju vacche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raaja raaju vacche"/>
</div>
<div class="lyrico-lyrics-wrapper">Lokaalu mecche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lokaalu mecche"/>
</div>
<div class="lyrico-lyrics-wrapper">Raaja raaju vacche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raaja raaju vacche"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaadhalenno tecche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaadhalenno tecche"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irukula intlona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irukula intlona"/>
</div>
<div class="lyrico-lyrics-wrapper">Suddenuga sound aina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suddenuga sound aina"/>
</div>
<div class="lyrico-lyrics-wrapper">Doubt lekunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Doubt lekunda"/>
</div>
<div class="lyrico-lyrics-wrapper">Okko settpu meedenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okko settpu meedenaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Angaranga bhogamlo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Angaranga bhogamlo"/>
</div>
<div class="lyrico-lyrics-wrapper">Sambadamga saagaava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sambadamga saagaava"/>
</div>
<div class="lyrico-lyrics-wrapper">Namminolla beraale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namminolla beraale"/>
</div>
<div class="lyrico-lyrics-wrapper">Sannagilluthunnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sannagilluthunnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sannagilluthunnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sannagilluthunnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sannagilluthunnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sannagilluthunnaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tinguranga veshamlo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tinguranga veshamlo"/>
</div>
<div class="lyrico-lyrics-wrapper">Lingulingu mantunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lingulingu mantunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Hangupongu ledandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hangupongu ledandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kireetamittaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kireetamittaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Devalokamantha thathasthu geyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Devalokamantha thathasthu geyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Raju donge meeru naamadeyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raju donge meeru naamadeyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Rajukunde wildu mante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rajukunde wildu mante"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarigga maarakunte aaradanthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarigga maarakunte aaradanthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanakam sunna sunna koode
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanakam sunna sunna koode"/>
</div>
<div class="lyrico-lyrics-wrapper">Sunakapu simhasanam veede
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sunakapu simhasanam veede"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanakam sunna sunna koode
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanakam sunna sunna koode"/>
</div>
<div class="lyrico-lyrics-wrapper">Sunakapu simhasanam veede
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sunakapu simhasanam veede"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey janakaa vacchadanta fradu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey janakaa vacchadanta fradu"/>
</div>
<div class="lyrico-lyrics-wrapper">Palake addhekicchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palake addhekicchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana badinodilina balapamula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana badinodilina balapamula"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanakam sunna sunna koode
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanakam sunna sunna koode"/>
</div>
<div class="lyrico-lyrics-wrapper">Sunakapu simhasanam veede
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sunakapu simhasanam veede"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanakam sunna sunna koode
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanakam sunna sunna koode"/>
</div>
<div class="lyrico-lyrics-wrapper">Sunakapu simhasanam veede
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sunakapu simhasanam veede"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sunakamama baagu sardi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sunakamama baagu sardi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sunakamaama baagu sardi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sunakamaama baagu sardi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raaja raaju vacche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raaja raaju vacche"/>
</div>
<div class="lyrico-lyrics-wrapper">Lokaalu mecche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lokaalu mecche"/>
</div>
<div class="lyrico-lyrics-wrapper">Raaja raaju vacche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raaja raaju vacche"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaadhalenno tecche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaadhalenno tecche"/>
</div>
</pre>
