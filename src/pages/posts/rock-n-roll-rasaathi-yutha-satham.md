---
title: "rock n roll rasaathi song lyrics"
album: "Yutha Satham"
artist: "	D. Imman"
lyricist: "Yugabharathi"
director: "Ezhil"
path: "/albums/yutha-satham-lyrics"
song: "Rock N Roll Rasaathi"
image: ../../images/albumart/yutha-satham.jpg
date: 2022-03-18
lang: tamil
youtubeLink: "https://www.youtube.com/embed/1I_jdbo8d2o"
type: "love"
singers:
  - Benny Dayal
  - Joewin Shamalina
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">rock n roll 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rock n roll "/>
</div>
<div class="lyrico-lyrics-wrapper">raasathi raasathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="raasathi raasathi"/>
</div>
<div class="lyrico-lyrics-wrapper">unna paathea poven 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna paathea poven "/>
</div>
<div class="lyrico-lyrics-wrapper">soodethi soodethi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="soodethi soodethi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">rock n roll 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rock n roll "/>
</div>
<div class="lyrico-lyrics-wrapper">raasaathi raasaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="raasaathi raasaathi"/>
</div>
<div class="lyrico-lyrics-wrapper">ena nekka eanthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ena nekka eanthen"/>
</div>
<div class="lyrico-lyrics-wrapper">kaimaathi kaimaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaimaathi kaimaathi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sangojapattu vaazha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sangojapattu vaazha"/>
</div>
<div class="lyrico-lyrics-wrapper">onnum theva illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="onnum theva illa"/>
</div>
<div class="lyrico-lyrics-wrapper">sallabam endrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sallabam endrum"/>
</div>
<div class="lyrico-lyrics-wrapper">paavam illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paavam illa"/>
</div>
<div class="lyrico-lyrics-wrapper">lovvalea kettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lovvalea kettu"/>
</div>
<div class="lyrico-lyrics-wrapper">ponavanga yarumilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponavanga yarumilla"/>
</div>
<div class="lyrico-lyrics-wrapper">ullasankolla neramilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ullasankolla neramilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">singaari sarakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="singaari sarakku"/>
</div>
<div class="lyrico-lyrics-wrapper">sarakku sarakku sarakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sarakku sarakku sarakku"/>
</div>
<div class="lyrico-lyrics-wrapper">kikerri erakku erakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kikerri erakku erakku"/>
</div>
<div class="lyrico-lyrics-wrapper">erakku erakku erakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="erakku erakku erakku"/>
</div>
<div class="lyrico-lyrics-wrapper">singaari vaipena kacheri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="singaari vaipena kacheri"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">rock n roll 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rock n roll "/>
</div>
<div class="lyrico-lyrics-wrapper">raasathi raasathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="raasathi raasathi"/>
</div>
<div class="lyrico-lyrics-wrapper">unna paathea poven 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna paathea poven "/>
</div>
<div class="lyrico-lyrics-wrapper">soodethi soodethi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="soodethi soodethi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">poote potidatha aalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poote potidatha aalu"/>
</div>
<div class="lyrico-lyrics-wrapper">yaarumame yaarumame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaarumame yaarumame"/>
</div>
<div class="lyrico-lyrics-wrapper">thothe poiedama vanthu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thothe poiedama vanthu "/>
</div>
<div class="lyrico-lyrics-wrapper">aadu gamea aadu gamea
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aadu gamea aadu gamea"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">savaale savaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="savaale savaale"/>
</div>
<div class="lyrico-lyrics-wrapper">samaali vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="samaali vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">tharane ippa naa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tharane ippa naa "/>
</div>
<div class="lyrico-lyrics-wrapper">virunthu nee arunthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="virunthu nee arunthu"/>
</div>
<div class="lyrico-lyrics-wrapper">dhivaane dhivaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhivaane dhivaane"/>
</div>
<div class="lyrico-lyrics-wrapper">alaadhi inbam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alaadhi inbam"/>
</div>
<div class="lyrico-lyrics-wrapper">kedaama kettuppo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kedaama kettuppo"/>
</div>
<div class="lyrico-lyrics-wrapper">thunindhu meimaranthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thunindhu meimaranthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">paapaa nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paapaa nee"/>
</div>
<div class="lyrico-lyrics-wrapper">kaiyaa kotha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaiyaa kotha"/>
</div>
<div class="lyrico-lyrics-wrapper">aave iruttaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aave iruttaa"/>
</div>
<div class="lyrico-lyrics-wrapper">ethaatha deebam eaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethaatha deebam eaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">naanum varattaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naanum varattaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vidaama thodaathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidaama thodaathe"/>
</div>
<div class="lyrico-lyrics-wrapper">porama padaathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="porama padaathe"/>
</div>
<div class="lyrico-lyrics-wrapper">unnodu engum poidathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnodu engum poidathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">rock n roll 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rock n roll "/>
</div>
<div class="lyrico-lyrics-wrapper">raasathi raasathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="raasathi raasathi"/>
</div>
<div class="lyrico-lyrics-wrapper">unna paathea poven 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna paathea poven "/>
</div>
<div class="lyrico-lyrics-wrapper">soodethi soodethi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="soodethi soodethi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">rock n roll 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rock n roll "/>
</div>
<div class="lyrico-lyrics-wrapper">raasaathi raasaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="raasaathi raasaathi"/>
</div>
<div class="lyrico-lyrics-wrapper">ena nekka eanthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ena nekka eanthen"/>
</div>
<div class="lyrico-lyrics-wrapper">kaimaathi kaimaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaimaathi kaimaathi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sangojapattu vaazha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sangojapattu vaazha"/>
</div>
<div class="lyrico-lyrics-wrapper">onnum theva illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="onnum theva illa"/>
</div>
<div class="lyrico-lyrics-wrapper">sallabam endrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sallabam endrum"/>
</div>
<div class="lyrico-lyrics-wrapper">paavam illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paavam illa"/>
</div>
<div class="lyrico-lyrics-wrapper">lovvalea kettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lovvalea kettu"/>
</div>
<div class="lyrico-lyrics-wrapper">ponavanga yarumilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponavanga yarumilla"/>
</div>
<div class="lyrico-lyrics-wrapper">ullasankolla neramilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ullasankolla neramilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">singaari sarakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="singaari sarakku"/>
</div>
<div class="lyrico-lyrics-wrapper">sarakku sarakku sarakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sarakku sarakku sarakku"/>
</div>
<div class="lyrico-lyrics-wrapper">kikerri erakku erakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kikerri erakku erakku"/>
</div>
<div class="lyrico-lyrics-wrapper">erakku erakku erakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="erakku erakku erakku"/>
</div>
<div class="lyrico-lyrics-wrapper">singaari vaipena kacheri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="singaari vaipena kacheri"/>
</div>
</pre>
