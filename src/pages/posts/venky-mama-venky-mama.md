---
title: "venky mama song lyrics"
album: "Venky Mama"
artist: "S. Thaman"
lyricist: "Ramajogayya Sastry"
director: "KS Ravindra"
path: "/albums/venky-mama-lyrics"
song: "Venky Mama"
image: ../../images/albumart/venky-mama.jpg
date: 2019-12-13
lang: telugu
youtubeLink: "https://www.youtube.com/embed/R6UfuPsYI1w"
type: "happy"
singers:
  - Sri Krishna
  - Mohana Bhogaraju
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Dhraaksharam Jangamayya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhraaksharam Jangamayya "/>
</div>
<div class="lyrico-lyrics-wrapper">Bheemalingaya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bheemalingaya "/>
</div>
<div class="lyrico-lyrics-wrapper">Biddala Kaachukovayaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Biddala Kaachukovayaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Biddala Kaachukovayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Biddala Kaachukovayaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasunna Mahimunna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasunna Mahimunna "/>
</div>
<div class="lyrico-lyrics-wrapper">Maanikaambika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maanikaambika"/>
</div>
<div class="lyrico-lyrics-wrapper">Challani Thalli Thoduga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Challani Thalli Thoduga"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu Panchave Dhaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu Panchave Dhaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Macherugani Menamaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Macherugani Menamaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Melujaathi Rathnam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Melujaathi Rathnam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Maamaku Alludante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Maamaku Alludante"/>
</div>
<div class="lyrico-lyrics-wrapper">Anthuleni Praanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anthuleni Praanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Irugu Dhisti Porugi Dhisti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irugu Dhisti Porugi Dhisti"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeyavamma Siri Godaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeyavamma Siri Godaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Ey Paadukallu Padakunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ey Paadukallu Padakunda"/>
</div>
<div class="lyrico-lyrics-wrapper">Veelliddharu Kalisundaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veelliddharu Kalisundaali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mama Mama Mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mama Mama Mama"/>
</div>
<div class="lyrico-lyrics-wrapper">Ne Palikina Tholi Padhama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne Palikina Tholi Padhama"/>
</div>
<div class="lyrico-lyrics-wrapper">Naake Dhorikina Varamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naake Dhorikina Varamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naakai Nilichina Balamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naakai Nilichina Balamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Kaali Adugullo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kaali Adugullo"/>
</div>
<div class="lyrico-lyrics-wrapper">Vundhi Naa Gudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vundhi Naa Gudi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Noti Palukullo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Noti Palukullo"/>
</div>
<div class="lyrico-lyrics-wrapper">Vundhi Naa Badi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vundhi Naa Badi"/>
</div>
<div class="lyrico-lyrics-wrapper">Puduthoone Nee Vodilo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puduthoone Nee Vodilo"/>
</div>
<div class="lyrico-lyrics-wrapper">Papanai Padi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Papanai Padi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Pere Mogindhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Pere Mogindhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Gunde Savvadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gunde Savvadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ammaina Naannaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammaina Naannaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvele Venky Mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvele Venky Mama"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Dhairyam Naa Sainyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Dhairyam Naa Sainyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvele Venky Mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvele Venky Mama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Bhujamekki 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Bhujamekki "/>
</div>
<div class="lyrico-lyrics-wrapper">Chusuna Lokam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chusuna Lokam"/>
</div>
<div class="lyrico-lyrics-wrapper">Naakentho Andhamainadhi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naakentho Andhamainadhi "/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Jatha Nadichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Jatha Nadichi"/>
</div>
<div class="lyrico-lyrics-wrapper">Gadipina Kaalam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gadipina Kaalam "/>
</div>
<div class="lyrico-lyrics-wrapper">Gelipinche Paatamainadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gelipinche Paatamainadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Paadham Ey Punyam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Paadham Ey Punyam "/>
</div>
<div class="lyrico-lyrics-wrapper">Chesukunnadho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chesukunnadho"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Vechhani 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vechhani "/>
</div>
<div class="lyrico-lyrics-wrapper">Gundelapai Aadukunnadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundelapai Aadukunnadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Ratham Panchukunna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Ratham Panchukunna "/>
</div>
<div class="lyrico-lyrics-wrapper">Janma Hakkutho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Janma Hakkutho"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalo Prathi Gunamu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalo Prathi Gunamu "/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Polikainadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Polikainadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ammaina Naannaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammaina Naannaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvele Venky Mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvele Venky Mama"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Dhairyam Naa Sainyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Dhairyam Naa Sainyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvele Venky Mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvele Venky Mama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Seethakka Geethakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Seethakka Geethakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Maalachhakka Mangakka Chudande
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maalachhakka Mangakka Chudande"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Pakka Evarocharo Enchakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Pakka Evarocharo Enchakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Vullasam Vutshaaham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Vullasam Vutshaaham"/>
</div>
<div class="lyrico-lyrics-wrapper">Jodi Katti Bandekka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jodi Katti Bandekka"/>
</div>
<div class="lyrico-lyrics-wrapper">Maamaa Allullu Vachhaare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maamaa Allullu Vachhaare"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaathara Gaale Vedekka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaathara Gaale Vedekka"/>
</div>
<div class="lyrico-lyrics-wrapper">Itta Kalisi Vasthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Itta Kalisi Vasthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakka Pakkaa Nadichi Vasthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakka Pakkaa Nadichi Vasthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Reppaleyadamika 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Reppaleyadamika "/>
</div>
<div class="lyrico-lyrics-wrapper">Rendu Kallu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendu Kallu "/>
</div>
<div class="lyrico-lyrics-wrapper">Marichipovaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marichipovaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Veellechotunna Inthe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veellechotunna Inthe "/>
</div>
<div class="lyrico-lyrics-wrapper">Racha Rachho Rangula Santha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Racha Rachho Rangula Santha"/>
</div>
<div class="lyrico-lyrics-wrapper">Manchi Eggottesi Dhigaarante 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manchi Eggottesi Dhigaarante "/>
</div>
<div class="lyrico-lyrics-wrapper">Pamba Regaalee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pamba Regaalee"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Seethakka Geethakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Seethakka Geethakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Addhirabanna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Addhirabanna "/>
</div>
<div class="lyrico-lyrics-wrapper">Iddharikiddharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iddharikiddharu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hema Hemi Bullolle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hema Hemi Bullolle"/>
</div>
<div class="lyrico-lyrics-wrapper">Vooruvaada Horetthinche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vooruvaada Horetthinche"/>
</div>
<div class="lyrico-lyrics-wrapper">Saradha Gaalla Chinnolle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saradha Gaalla Chinnolle"/>
</div>
<div class="lyrico-lyrics-wrapper">Varasakkemo Orayyo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varasakkemo Orayyo "/>
</div>
<div class="lyrico-lyrics-wrapper">Veellu Maamaa Allulle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veellu Maamaa Allulle"/>
</div>
<div class="lyrico-lyrics-wrapper">Vayasu Theda Theesesthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayasu Theda Theesesthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakka Allari Pillolle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakka Allari Pillolle"/>
</div>
<div class="lyrico-lyrics-wrapper">Venky Mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venky Mama"/>
</div>
<div class="lyrico-lyrics-wrapper">Venky Mamaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venky Mamaaa"/>
</div>
</pre>
