---
title: "adi yendi pulla song lyrics"
album: "Kazhugu 2"
artist: "Yuvan Shankar Raja"
lyricist: "Mohan Rajan"
director: "Sathyasiva"
path: "/albums/kazhugu-2-lyrics"
song: "Adi Yendi Pulla"
image: ../../images/albumart/kazhugu-2.jpg
date: 2019-08-01
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Jy5fnFyuexY"
type: "love"
singers:
  - Yuvan Shankar Raja
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Adi Yendipulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Yendipulla"/>
</div>
<div class="lyrico-lyrics-wrapper">En Manasukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Manasukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Veesi Pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Veesi Pora"/>
</div>
<div class="lyrico-lyrics-wrapper">Or Vaanavilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Or Vaanavilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Moochu Kaaththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Moochu Kaaththa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Izhuthukitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Izhuthukitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Thedi Parthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Thedi Parthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Kaanavilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Kaanavilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Onna Kannukulla Otti Vachi Rasippene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna Kannukulla Otti Vachi Rasippene"/>
</div>
<div class="lyrico-lyrics-wrapper">Entha Anbal Unthan Ayulathan Valappene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entha Anbal Unthan Ayulathan Valappene"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum Kanduvacha Kanavellam Ketppene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Kanduvacha Kanavellam Ketppene"/>
</div>
<div class="lyrico-lyrics-wrapper">Atha Ovvonnaga Un Munnala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atha Ovvonnaga Un Munnala"/>
</div>
<div class="lyrico-lyrics-wrapper">Kondanthu Vappen Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondanthu Vappen Naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi Yendipulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Yendipulla"/>
</div>
<div class="lyrico-lyrics-wrapper">En Manasukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Manasukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Veesi Pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Veesi Pora"/>
</div>
<div class="lyrico-lyrics-wrapper">Or Vaanavilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Or Vaanavilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Moochu Kaaththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Moochu Kaaththa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Izhuthukitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Izhuthukitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Thedi Parthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Thedi Parthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Kaanavilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Kaanavilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mazhai Vittu Ponalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai Vittu Ponalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilai Sottum Neeraga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilai Sottum Neeraga"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakkulle Vizhunthaai Neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkulle Vizhunthaai Neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu Yenadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu Yenadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Imai Meeri Theduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imai Meeri Theduthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhigalum Unnaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhigalum Unnaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Idaiveli Maaruthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idaiveli Maaruthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha Nerame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Nerame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnodu Naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnodu Naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennodu Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennodu Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppothum Vazha Ketkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppothum Vazha Ketkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">En Pearile Un Pearinai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Pearile Un Pearinai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ondrai Korkkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondrai Korkkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Santhoshamum Kanneer Tharum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Santhoshamum Kanneer Tharum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnale Indru Parkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnale Indru Parkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Thookkam Parthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Thookkam Parthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum Thoonguven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum Thoonguven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi Yendipulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Yendipulla"/>
</div>
<div class="lyrico-lyrics-wrapper">En Manasukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Manasukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Veesi Pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Veesi Pora"/>
</div>
<div class="lyrico-lyrics-wrapper">Or Vaanavilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Or Vaanavilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Moochu Kaaththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Moochu Kaaththa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Izhuthukitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Izhuthukitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Thedi Parthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Thedi Parthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Kaanavilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Kaanavilla"/>
</div>
</pre>
