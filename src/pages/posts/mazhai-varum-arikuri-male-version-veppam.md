---
title: "mazhai varum arikuri male version song lyrics"
album: "Veppam"
artist: "Joshua Sridhar"
lyricist: "Na. Muthukumar"
director: "Anjana"
path: "/albums/veppam-lyrics"
song: "Mazhai Varum Arikuri Male Version"
image: ../../images/albumart/veppam.jpg
date: 2011-07-29
lang: tamil
youtubeLink: "https://www.youtube.com/embed/aVhhSr4ZH9Q"
type: "love"
singers:
  - Naresh Iyer
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Mazhai Varum Arikuri En Vizhigalil Theriyuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai Varum Arikuri En Vizhigalil Theriyuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam Indru Ninaiyuthae Ithu Enna Kaadhala Saadhala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam Indru Ninaiyuthae Ithu Enna Kaadhala Saadhala"/>
</div>
<div class="lyrico-lyrics-wrapper">Pazhagiya Kaalangal En Paarvayil Viriyuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pazhagiya Kaalangal En Paarvayil Viriyuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadhaigal Nazhuvuthae Ithu Yeno Yeno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadhaigal Nazhuvuthae Ithu Yeno Yeno"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Thoolil Saayum Pothu Urchagam Kollum Kangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Thoolil Saayum Pothu Urchagam Kollum Kangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Enge Enge Endru Unaithedi Thedi Paarkirathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Enge Enge Endru Unaithedi Thedi Paarkirathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnodu Poogumpothu Poo Pookum Saalai Yaavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnodu Poogumpothu Poo Pookum Saalai Yaavum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Enge Endru Ennai Ketta Pinbu Vaadiduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Enge Endru Ennai Ketta Pinbu Vaadiduthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mazhai Varum Arikuri En Vizhigalil Theriyuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai Varum Arikuri En Vizhigalil Theriyuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam Indru Ninaiyuthae Ithu Enna Kaadhala Saadhala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam Indru Ninaiyuthae Ithu Enna Kaadhala Saadhala"/>
</div>
<div class="lyrico-lyrics-wrapper">Pazhagiya Kaalangal En Paarvayil Viriyuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pazhagiya Kaalangal En Paarvayil Viriyuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadhaigal Nazhuvuthae Ithu Yeno Yeno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadhaigal Nazhuvuthae Ithu Yeno Yeno"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ariyaadha Vayadhil Vidhaithathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ariyaadha Vayadhil Vidhaithathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Ho Ho Ho Oo Oo Oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Ho Ho Ho Oo Oo Oo"/>
</div>
<div class="lyrico-lyrics-wrapper">Athuvaagavae Thaanai Valarnthathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athuvaagavae Thaanai Valarnthathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Ho Ho Ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Ho Ho Ho"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhidhaai Oru Poovum Pookaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhidhaai Oru Poovum Pookaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Ho Ho Ho Oo Oo Oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Ho Ho Ho Oo Oo Oo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Yaaradhai Yaaradhai Parithathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Yaaradhai Yaaradhai Parithathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Ho Ho Ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Ho Ho Ho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Kaal Thadam Sendra Vazhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kaal Thadam Sendra Vazhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarthu Naanum Vandhaenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarthu Naanum Vandhaenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu Padhiyil Tholainthadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu Padhiyil Tholainthadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Kaettadhu Azhagiya Nerangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Kaettadhu Azhagiya Nerangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Ho Ho Ho Oo Oo Oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Ho Ho Ho Oo Oo Oo"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar Thanthadhu Vizhigalil Eerangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar Thanthadhu Vizhigalil Eerangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Ho Ho Ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Ho Ho Ho"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Kaettadhu Vaanavil Maayangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Kaettadhu Vaanavil Maayangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Ho Ho Ho Oo Oo Oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Ho Ho Ho Oo Oo Oo"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar Thanthadhu Vazhigalil Kaayangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar Thanthadhu Vazhigalil Kaayangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Ho Ho Ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Ho Ho Ho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Intha Kaadhalum Oruvagai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Kaadhalum Oruvagai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sithravathai Thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sithravathai Thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu Uyirudan Erikuthada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu Uyirudan Erikuthada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ohooooohohohoooooooooooh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohooooohohohoooooooooooh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mazhai Varum Arikuri En Vizhigalil Theriyuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai Varum Arikuri En Vizhigalil Theriyuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam Indru Ninaiyuthae Ithu Enna Kaadhala Saadhala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam Indru Ninaiyuthae Ithu Enna Kaadhala Saadhala"/>
</div>
<div class="lyrico-lyrics-wrapper">Pazhagiya Kaalangal En Paarvayil Viriyuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pazhagiya Kaalangal En Paarvayil Viriyuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadhaigal Nazhuvuthae Ithu Yeno Yeno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadhaigal Nazhuvuthae Ithu Yeno Yeno"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Thoolil Saayum Pothu Urchagam Kollum Kangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Thoolil Saayum Pothu Urchagam Kollum Kangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Enge Enge Endru Unaithedi Thedi Paarkirathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Enge Enge Endru Unaithedi Thedi Paarkirathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnodu Poogumpothu Poo Pookum Saalai Yaavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnodu Poogumpothu Poo Pookum Saalai Yaavum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Enge Endru Ennai Ketta Pinbu Vaadiduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Enge Endru Ennai Ketta Pinbu Vaadiduthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mazhai Varum Arikuri En Vizhigalil Theriyuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai Varum Arikuri En Vizhigalil Theriyuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam Indru Ninaiyuthae Ithu Enna Kaadhala Saadhala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam Indru Ninaiyuthae Ithu Enna Kaadhala Saadhala"/>
</div>
<div class="lyrico-lyrics-wrapper">Pazhagiya Kaalangal En Paarvayil Viriyuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pazhagiya Kaalangal En Paarvayil Viriyuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadhaigal Nazhuvuthae Ithu Yeno Yeno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadhaigal Nazhuvuthae Ithu Yeno Yeno"/>
</div>
</pre>
