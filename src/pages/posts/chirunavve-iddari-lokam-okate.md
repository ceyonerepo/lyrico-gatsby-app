---
title: "chirunavve song lyrics"
album: "Iddari Lokam Okate"
artist: "Mickey J Meyer"
lyricist: "Balaji"
director: "	GR Krishna"
path: "/albums/prati-roju-pandage-lyrics"
song: "Chirunavve"
image: ../../images/albumart/prati-roju-pandage.jpg
date: 2019-12-25
lang: telugu
youtubeLink: "https://www.youtube.com/embed/wV2ujaASgwU"
type: "love"
singers:
  - Ishaq Vali
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Chirunavve dhorikindhe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chirunavve dhorikindhe "/>
</div>
<div class="lyrico-lyrics-wrapper">ninu thaliche pedhaalake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninu thaliche pedhaalake"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanu nene marichaane 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanu nene marichaane "/>
</div>
<div class="lyrico-lyrics-wrapper">ninu choosthu nijaanikee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninu choosthu nijaanikee"/>
</div>
<div class="lyrico-lyrics-wrapper">Kshanam kshanam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kshanam kshanam "/>
</div>
<div class="lyrico-lyrics-wrapper">nee needaga nanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee needaga nanne"/>
</div>
<div class="lyrico-lyrics-wrapper">Padhe padhe choosaanee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padhe padhe choosaanee"/>
</div>
<div class="lyrico-lyrics-wrapper">Ila ilaa ee nelanu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ila ilaa ee nelanu "/>
</div>
<div class="lyrico-lyrics-wrapper">vidichi oohai nadichaanee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidichi oohai nadichaanee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhiddina kanu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhiddina kanu "/>
</div>
<div class="lyrico-lyrics-wrapper">bomme nelavankayyindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bomme nelavankayyindhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee reyi sandhellona kalakee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee reyi sandhellona kalakee"/>
</div>
<div class="lyrico-lyrics-wrapper">Muddhula adugullo jaadalu puulella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muddhula adugullo jaadalu puulella"/>
</div>
<div class="lyrico-lyrics-wrapper">Oodhaare maaraayi naa kathakee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oodhaare maaraayi naa kathakee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gundeloo paatha jnaapakam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundeloo paatha jnaapakam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kotha rangulee poosenee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kotha rangulee poosenee"/>
</div>
<div class="lyrico-lyrics-wrapper">Roopane manasu nuuvane 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Roopane manasu nuuvane "/>
</div>
<div class="lyrico-lyrics-wrapper">thelisi guvvalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thelisi guvvalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Maari ningiki yegaranaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maari ningiki yegaranaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neetho oo oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neetho oo oo"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethoo oo oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethoo oo oo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chirunavve dhorikindhe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chirunavve dhorikindhe "/>
</div>
<div class="lyrico-lyrics-wrapper">ninu thaliche pedhaalake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninu thaliche pedhaalake"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanu nene marichaane 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanu nene marichaane "/>
</div>
<div class="lyrico-lyrics-wrapper">ninu choosthu nijaanikee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninu choosthu nijaanikee"/>
</div>
<div class="lyrico-lyrics-wrapper">Kshanam kshanam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kshanam kshanam "/>
</div>
<div class="lyrico-lyrics-wrapper">nee needaga nanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee needaga nanne"/>
</div>
<div class="lyrico-lyrics-wrapper">Padhe padhe choosaanee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padhe padhe choosaanee"/>
</div>
<div class="lyrico-lyrics-wrapper">Ila ilaa ee nelanu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ila ilaa ee nelanu "/>
</div>
<div class="lyrico-lyrics-wrapper">vidichi oohai nadichaanee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidichi oohai nadichaanee"/>
</div>
</pre>
