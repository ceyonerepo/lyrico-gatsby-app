---
title: "oru kuchi oru kulfi song lyrics"
album: "Kalakalappu 2"
artist: "Hiphop Tamizha"
lyricist: "Rokesh - Saravedi Saran - Hiphop Tamizha"
director: "Sundar C"
path: "/albums/kalakalappu-2-lyrics"
song: "Oru Kuchi Oru Kulfi"
image: ../../images/albumart/kalakalappu-2.jpg
date: 2018-02-09
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ggoeWxrJTY4"
type: "love"
singers:
  - Gana Vinoth
  - Saravedi Saran
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Oru kuchi oru kulfi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru kuchi oru kulfi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthu ninnu edu selfie
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthu ninnu edu selfie"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru kuchi oru kulfi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru kuchi oru kulfi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthu ninnu edu selfie
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthu ninnu edu selfie"/>
</div>
<div class="lyrico-lyrics-wrapper">Idiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unkuda than photo pudchen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unkuda than photo pudchen"/>
</div>
<div class="lyrico-lyrics-wrapper">Touch-u phone-u-la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Touch-u phone-u-la"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Touch-uh panni ichu kuduppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Touch-uh panni ichu kuduppen"/>
</div>
<div class="lyrico-lyrics-wrapper">Honeymoon-u-la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Honeymoon-u-la"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unkuda than photo pudchen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unkuda than photo pudchen"/>
</div>
<div class="lyrico-lyrics-wrapper">Touch-u phone-u-la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Touch-u phone-u-la"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Touch-uh panni ichu kuduppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Touch-uh panni ichu kuduppen"/>
</div>
<div class="lyrico-lyrics-wrapper">Honeymoon-u-la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Honeymoon-u-la"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeppa sonna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeppa sonna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thalli thalli povatha ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalli thalli povatha ma"/>
</div>
<div class="lyrico-lyrics-wrapper">Nillu nillu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nillu nillu"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma kalyanatha enga vechikalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma kalyanatha enga vechikalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollu sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollu sollu"/>
</div>
<div class="lyrico-lyrics-wrapper">Niruthi sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niruthi sollu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thalli thalli povatha ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalli thalli povatha ma"/>
</div>
<div class="lyrico-lyrics-wrapper">Nillu nillu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nillu nillu"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma kalyanatha enga vechikalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma kalyanatha enga vechikalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollu sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollu sollu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vanthaa mahalakshmi-eh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthaa mahalakshmi-eh"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee vanthathaala over night-la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee vanthathaala over night-la"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum rajini-eh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum rajini-eh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vanthaa mahalakshmi-eh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthaa mahalakshmi-eh"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee vanthathaala over night-la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee vanthathaala over night-la"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum rajini-eh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum rajini-eh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaaru yaaru annatha yaaru yaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaru yaaru annatha yaaru yaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Annan thanda akkavukku superstar-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annan thanda akkavukku superstar-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaru yaaru annatha yaaru yaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaru yaaru annatha yaaru yaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Annan thanda akkavukku superstar-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annan thanda akkavukku superstar-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Straight-ah unnai partha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Straight-ah unnai partha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee irukkira fight-ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee irukkira fight-ah"/>
</div>
<div class="lyrico-lyrics-wrapper">White-ah tube light-ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="White-ah tube light-ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee jollikira bright-ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee jollikira bright-ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Size-ah chinna vayasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Size-ah chinna vayasa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai parthen nice-ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai parthen nice-ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey pala palanu minukkuriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey pala palanu minukkuriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Plastic-u rice-ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Plastic-u rice-ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karanja kuchi ice-ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karanja kuchi ice-ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Kulukkal bumper price-ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulukkal bumper price-ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Biriyani nee irukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Biriyani nee irukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethukadi pizza-ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethukadi pizza-ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakkathil neeyum irunthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkathil neeyum irunthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Parthukadi maaman dhil-eh thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parthukadi maaman dhil-eh thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Tajmahal-u katti vechan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tajmahal-u katti vechan"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasu kulla than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu kulla than"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jingiliya jinukka devathaiya kanakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jingiliya jinukka devathaiya kanakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Nambala pol couple-u ooril irukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nambala pol couple-u ooril irukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Jingiliya jinukka devathaiya kanakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jingiliya jinukka devathaiya kanakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Nambala pol couple-u ooril irukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nambala pol couple-u ooril irukka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha ooril irukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha ooril irukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma ooril irukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma ooril irukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha ooril irukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha ooril irukka"/>
</div>
<div class="lyrico-lyrics-wrapper">So whatsup-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="So whatsup-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru kuchi oru kulfi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru kuchi oru kulfi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthu ninnu edu selfie
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthu ninnu edu selfie"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuchi oru kulfi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuchi oru kulfi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthu ninnu edu selfie
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthu ninnu edu selfie"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unkuda than photo pudchen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unkuda than photo pudchen"/>
</div>
<div class="lyrico-lyrics-wrapper">Touch-u phone-u-la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Touch-u phone-u-la"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Touch-uh panni ichu kuduppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Touch-uh panni ichu kuduppen"/>
</div>
<div class="lyrico-lyrics-wrapper">Honeymoon-u-la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Honeymoon-u-la"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unkuda than photo pudchen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unkuda than photo pudchen"/>
</div>
<div class="lyrico-lyrics-wrapper">Touch-u phone-u-la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Touch-u phone-u-la"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Touch-uh panni ichu kuduppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Touch-uh panni ichu kuduppen"/>
</div>
<div class="lyrico-lyrics-wrapper">Honeymoon-u-la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Honeymoon-u-la"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thalli thalli povatha ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalli thalli povatha ma"/>
</div>
<div class="lyrico-lyrics-wrapper">Nillu nillu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nillu nillu"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma kalyanatha enga vechikalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma kalyanatha enga vechikalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollu sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollu sollu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thalli thalli povatha ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalli thalli povatha ma"/>
</div>
<div class="lyrico-lyrics-wrapper">Nillu nillu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nillu nillu"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma kalyanatha enga vechikalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma kalyanatha enga vechikalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollu sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollu sollu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vanthaa mahalakshmi-eh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthaa mahalakshmi-eh"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee vanthathaala over night-la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee vanthathaala over night-la"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum rajini-eh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum rajini-eh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vanthaa mahalakshmi-eh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthaa mahalakshmi-eh"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee vanthathaala over night-la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee vanthathaala over night-la"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum rajini-eh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum rajini-eh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru kuchi oru kulfi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru kuchi oru kulfi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthu ninnu edu selfie
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthu ninnu edu selfie"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru kuchi oru kulfi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru kuchi oru kulfi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthu ninnu edu selfie
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthu ninnu edu selfie"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollu sollu sollu maama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollu sollu sollu maama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaaru yaaru annatha yaaru yaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaru yaaru annatha yaaru yaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Annan thanda akkavukku superstar-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annan thanda akkavukku superstar-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Sokka sonna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sokka sonna"/>
</div>
</pre>
