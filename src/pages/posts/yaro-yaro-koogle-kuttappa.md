---
title: "yaro yaro song lyrics"
album: "Koogle Kuttappa"
artist: "Ghibran"
lyricist: "Madhan Karky"
director: "Sabari – Saravanan"
path: "/albums/koogle-kuttappa-lyrics"
song: "Yaro Yaro"
image: ../../images/albumart/koogle-kuttappa.jpg
date: 2022-05-06
lang: tamil
youtubeLink: "https://www.youtube.com/embed/eZPqpTXe0hE"
type: "melody"
singers:
  - Aravind Srinivas
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yaro Yaro Idhuvo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaro Yaro Idhuvo"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayam Thanil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam Thanil"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhithaai Thiravum Kadhavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhithaai Thiravum Kadhavo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ularum Ularum Manadhil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ularum Ularum Manadhil"/>
</div>
<div class="lyrico-lyrics-wrapper">Puriyaavidham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puriyaavidham"/>
</div>
<div class="lyrico-lyrics-wrapper">Paravum Eeram Edhuvo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paravum Eeram Edhuvo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alavum Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alavum Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Vayadhum Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Vayadhum Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Anbe Unthan Uyaram Sollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Anbe Unthan Uyaram Sollum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sondham Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sondham Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Raththam Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Raththam Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanalum Ullam Paasam Kollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanalum Ullam Paasam Kollum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadum Karum Paaraikul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadum Karum Paaraikul"/>
</div>
<div class="lyrico-lyrics-wrapper">Pugunthidum Verondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pugunthidum Verondru"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhir Edhir Paarkkaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhir Edhir Paarkkaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thulir Vidum Aanandha Kanneeraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thulir Vidum Aanandha Kanneeraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaraa Thooraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaraa Thooraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyirin Porul Ariyaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirin Porul Ariyaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Porulin Uyir Arindhaaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porulin Uyir Arindhaaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhanai Inimel Ivan Enbaayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhanai Inimel Ivan Enbaayo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanimai Enum Sol Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanimai Enum Sol Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Inimel Adhil Bayan Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inimel Adhil Bayan Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivanum Ini Un
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivanum Ini Un"/>
</div>
<div class="lyrico-lyrics-wrapper">Magan Enbaaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Magan Enbaaya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Marabanu Tharavillaiyenil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marabanu Tharavillaiyenil"/>
</div>
<div class="lyrico-lyrics-wrapper">Magan Illaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Magan Illaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Avanadhu Min Unarvugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avanadhu Min Unarvugal"/>
</div>
<div class="lyrico-lyrics-wrapper">Unarvillaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unarvillaiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mannodu Undaagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannodu Undaagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Man Servathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Man Servathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar Unmai Yaar Bommai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar Unmai Yaar Bommai"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar Solvathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar Solvathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thalai Kodhidum Thaayaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalai Kodhidum Thaayaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilaiyaadidum Seyaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilaiyaadidum Seyaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuyaril Thunaiyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuyaril Thunaiyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan Aanaano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Aanaano"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arivoottum Theeyaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arivoottum Theeyaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaal Aattidum Naayaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaal Aattidum Naayaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Thozhan Tholaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozhan Tholaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan Aanaano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Aanaano"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadavulin Uruvathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadavulin Uruvathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Sevaganaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Sevaganaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkena Uyir Tharum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkena Uyir Tharum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Kaavalanaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Kaavalanaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhikaalai Keetraaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhikaalai Keetraaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuyil Neekkuvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuyil Neekkuvaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhi Moodi Thalai Saaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhi Moodi Thalai Saaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaalaattuvan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaalaattuvan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaro Yaro Idhuvo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaro Yaro Idhuvo"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayam Thanil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam Thanil"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhithaai Thiravum Kadhavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhithaai Thiravum Kadhavo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ularum Ularum Manadhil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ularum Ularum Manadhil"/>
</div>
<div class="lyrico-lyrics-wrapper">Puriyaavidham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puriyaavidham"/>
</div>
<div class="lyrico-lyrics-wrapper">Paravum Eeram Edhuvo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paravum Eeram Edhuvo"/>
</div>
</pre>
