---
title: "edhedho song lyrics"
album: "Sattapadi Kutram"
artist: "Vijay Antony"
lyricist: "S.A. Chandrasekhar"
director: "S.A. Chandrasekhar"
path: "/albums/sattapadi-kutram-lyrics"
song: "Edhedho"
image: ../../images/albumart/sattapadi-kutram.jpg
date: 2011-03-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/D11Pm6zyLy8"
type: "love"
singers:
  - Vijay Antony
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yedhedho yedhedho aagippoachey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhedho yedhedho aagippoachey"/>
</div>
<div class="lyrico-lyrics-wrapper">poovendru moadhi en 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poovendru moadhi en "/>
</div>
<div class="lyrico-lyrics-wrapper">yaanaiye saanjippoachey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaanaiye saanjippoachey"/>
</div>
<div class="lyrico-lyrics-wrapper">idhu enna pudhu noayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhu enna pudhu noayaa"/>
</div>
<div class="lyrico-lyrics-wrapper">vayadhukkul varum theeyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vayadhukkul varum theeyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">enakkulley kaadhal mazhaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enakkulley kaadhal mazhaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">enakkulley kaadhal mazhaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enakkulley kaadhal mazhaiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yedhedho yedhedho aagippoachey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhedho yedhedho aagippoachey"/>
</div>
<div class="lyrico-lyrics-wrapper">poovendru moadhi en 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poovendru moadhi en "/>
</div>
<div class="lyrico-lyrics-wrapper">yaanaiye saanjippoachey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaanaiye saanjippoachey"/>
</div>
<div class="lyrico-lyrics-wrapper">idhu enna pudhu noayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhu enna pudhu noayaa"/>
</div>
<div class="lyrico-lyrics-wrapper">vayadhukkul varum theeyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vayadhukkul varum theeyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">enakkulley kaadhal mazhaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enakkulley kaadhal mazhaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">enakkulley kaadhal mazhaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enakkulley kaadhal mazhaiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saayangaala velaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saayangaala velaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">mutham ketka pidikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mutham ketka pidikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">koodi pesum vaarthaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koodi pesum vaarthaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">vetkappattu sirikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetkappattu sirikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">nee irukkum veettiley 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee irukkum veettiley "/>
</div>
<div class="lyrico-lyrics-wrapper">en ninaivu vasikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en ninaivu vasikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjikkulley nee irukkiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjikkulley nee irukkiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">saalaiyoara pookkal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saalaiyoara pookkal "/>
</div>
<div class="lyrico-lyrics-wrapper">un koondhal thedithudikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un koondhal thedithudikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">nee nadandha veedhi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee nadandha veedhi "/>
</div>
<div class="lyrico-lyrics-wrapper">mun paadham thedi thavikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mun paadham thedi thavikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">neekkuditha Coffee-yil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neekkuditha Coffee-yil "/>
</div>
<div class="lyrico-lyrics-wrapper">eekkal moikka marukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eekkal moikka marukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjikkulley neeyirukkiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjikkulley neeyirukkiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">nijamaai nijamaai en 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nijamaai nijamaai en "/>
</div>
<div class="lyrico-lyrics-wrapper">kaiyirandum rekkaiyaanadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaiyirandum rekkaiyaanadhey"/>
</div>
<div class="lyrico-lyrics-wrapper">nijamaai nijamaai en aa hoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nijamaai nijamaai en aa hoi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yedhedho yedhedho aagippoachey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhedho yedhedho aagippoachey"/>
</div>
<div class="lyrico-lyrics-wrapper">poovendru moadhi en 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poovendru moadhi en "/>
</div>
<div class="lyrico-lyrics-wrapper">yaanaiye saanjippoachey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaanaiye saanjippoachey"/>
</div>
<div class="lyrico-lyrics-wrapper">idhu enna pudhu noayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhu enna pudhu noayaa"/>
</div>
<div class="lyrico-lyrics-wrapper">vayadhukkul varum theeyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vayadhukkul varum theeyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">enakkulley kaadhal mazhaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enakkulley kaadhal mazhaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">enakkulley kaadhal mazhaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enakkulley kaadhal mazhaiyaa"/>
</div>
</pre>
