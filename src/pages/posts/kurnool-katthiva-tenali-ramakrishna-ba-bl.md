---
title: "kurnool katthiva song lyrics"
album: "Tenali Ramakrishna BA BL"
artist: "Sai Karthik"
lyricist: "Chilaka Rekka Ganesh"
director: "G. Nageswara Reddy"
path: "/albums/tenali-ramakrishna-ba-bl-lyrics"
song: "Kurnool Katthiva"
image: ../../images/albumart/tenali-ramakrishna-ba-bl.jpg
date: 2019-11-15
lang: telugu
youtubeLink: 
type: "happy"
singers:
  - Dhanunjay
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Gulabee Kannulenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gulabee Kannulenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gundello Gucchuthunna Mullulenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundello Gucchuthunna Mullulenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gummasoku Konaseema
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gummasoku Konaseema"/>
</div>
<div class="lyrico-lyrics-wrapper">Nammipothe Ayyo Rayalaseema
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nammipothe Ayyo Rayalaseema"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kurnoolu Katthivaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurnoolu Katthivaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Guntoor Mirchivaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Guntoor Mirchivaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kurnoolu Katthivaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurnoolu Katthivaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Guntoor Mirchivaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Guntoor Mirchivaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanu Kastha Kanikarcha Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanu Kastha Kanikarcha Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Konda Reddy Buruzu Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konda Reddy Buruzu Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pulla Reddy Sweet Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pulla Reddy Sweet Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Norooristhoone Vuntavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Norooristhoone Vuntavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Tamota Rangulaagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tamota Rangulaagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thala Thala Laaduthunnaaveh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thala Thala Laaduthunnaaveh"/>
</div>
<div class="lyrico-lyrics-wrapper">Touch Chesthe Notu Bomb Laaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Touch Chesthe Notu Bomb Laaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Peluthunnaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peluthunnaave"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Gunde Kaaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Gunde Kaaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannu Mottikayalu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannu Mottikayalu "/>
</div>
<div class="lyrico-lyrics-wrapper">Vesthundhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vesthundhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninu Lavvu Cheyyakapothe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninu Lavvu Cheyyakapothe"/>
</div>
<div class="lyrico-lyrics-wrapper">Aagipothaanantundheh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagipothaanantundheh"/>
</div>
<div class="lyrico-lyrics-wrapper">Ammoo Censor Boarduvaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammoo Censor Boarduvaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Lavvu Scenante Katthera Vesthaavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lavvu Scenante Katthera Vesthaavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Lammo Pisinaari Pillavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Lammo Pisinaari Pillavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinna Muddhaina Appuga Ivvavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna Muddhaina Appuga Ivvavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rathraithe Kallo Kocchi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rathraithe Kallo Kocchi "/>
</div>
<div class="lyrico-lyrics-wrapper">Disturb Chesthaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Disturb Chesthaave"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagalemo Pattinchukoveh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagalemo Pattinchukoveh"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Seemamattilona Vajraalutaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Seemamattilona Vajraalutaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Vajramaa Dhorakave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Vajramaa Dhorakave"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Korra Biyyamlaa Birrugunnave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Korra Biyyamlaa Birrugunnave"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Choopu Kodavaltho Koyyaniyyave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Choopu Kodavaltho Koyyaniyyave"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Aali Laali Ayiporaadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Aali Laali Ayiporaadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadathaane Neekeh Thaalhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadathaane Neekeh Thaalhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Pilla Nuvveh Kavaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Pilla Nuvveh Kavaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Lekapothe Ee Oopiri Aagaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lekapothe Ee Oopiri Aagaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Pilla Nuvveh Kavaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Pilla Nuvveh Kavaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Lekapothe Ee Oopiri Aagaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lekapothe Ee Oopiri Aagaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Kurnoolu Arey Kurnoolu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurnoolu Arey Kurnoolu"/>
</div>
</pre>
