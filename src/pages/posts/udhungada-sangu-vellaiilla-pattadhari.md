---
title: "udhungada sangu song lyrics"
album: "Vellaiilla Pattadhari"
artist: "Anirudh Ravichander"
lyricist: "Dhanush"
director: "Velraj"
path: "/albums/vellaiilla-pattadhari-lyrics"
song: "Udhungada Sangu"
image: ../../images/albumart/vellaiilla-pattadhari.jpg
date: 2014-02-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/RCXzH27eOIA"
type: "Sad"
singers:
  - Anirudh Ravichander
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vaazhkaiya Thedi Naanum Poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkaiya Thedi Naanum Poren"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaandula Paadum Paattukkaaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaandula Paadum Paattukkaaran"/>
</div>
<div class="lyrico-lyrics-wrapper">Bodhaiyil Paadum Soga Paatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bodhaiyil Paadum Soga Paatta"/>
</div>
<div class="lyrico-lyrics-wrapper">Sodava Kalandhu Paada Poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sodava Kalandhu Paada Poren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maamen Otaandi Periya Loosaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maamen Otaandi Periya Loosaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Vaangiye Naan Strong Aana Maayaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Vaangiye Naan Strong Aana Maayaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanen Naan Bondi Athaiyumthaan Thaandi Poraaduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanen Naan Bondi Athaiyumthaan Thaandi Poraaduven"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Veriyaana Virumaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Veriyaana Virumaandi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Udhungada Sangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Udhungada Sangu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Thanda Soru Kingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Thanda Soru Kingu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thamizh is My Mother Tongue
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thamizh is My Mother Tongue"/>
</div>
<div class="lyrico-lyrics-wrapper">I’m Single and I’m Young
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I’m Single and I’m Young"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Oothungada Sangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Oothungada Sangu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Thanda Soru Kingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Thanda Soru Kingu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thamizh is My Mother Tongue
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thamizh is My Mother Tongue"/>
</div>
<div class="lyrico-lyrics-wrapper">I’m Single and I’m Youngu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I’m Single and I’m Youngu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oothu Sungu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oothu Sungu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanthaan Kingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanthaan Kingu"/>
</div>
<div class="lyrico-lyrics-wrapper">I’m Single and I’m Youngu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I’m Single and I’m Youngu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sangu Naanthaan Kingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sangu Naanthaan Kingu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mother Toungue
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mother Toungue"/>
</div>
<div class="lyrico-lyrics-wrapper">I’m Single and I’m Youngu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I’m Single and I’m Youngu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yerumaikku Kooda Blue Cross Irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yerumaikku Kooda Blue Cross Irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakkaaga Yosikka Uyira Irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkaaga Yosikka Uyira Irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Maratha Suththi Duet Paadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maratha Suththi Duet Paadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Love Panna Enakkum Thaan Aasai Irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love Panna Enakkum Thaan Aasai Irukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maanam Rosamla Keezha Vittaachuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maanam Rosamla Keezha Vittaachuda"/>
</div>
<div class="lyrico-lyrics-wrapper">Plastic Poo Kooda Vaadi Poyaachuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Plastic Poo Kooda Vaadi Poyaachuda"/>
</div>
<div class="lyrico-lyrics-wrapper">Vella Sollaama Ulla Alugurenda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vella Sollaama Ulla Alugurenda"/>
</div>
<div class="lyrico-lyrics-wrapper">Vella Manasellam Inga Kanakillada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vella Manasellam Inga Kanakillada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dandanakka Danakanakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dandanakka Danakanakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Dandanakka Danakanakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dandanakka Danakanakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Dandanakka Danakanakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dandanakka Danakanakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Dandanakka Danakanakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dandanakka Danakanakka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dandanakka Danakanakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dandanakka Danakanakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Dandanakka Danakanakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dandanakka Danakanakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Dandanakka Danakanakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dandanakka Danakanakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Dandanakka Danakanakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dandanakka Danakanakka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Udhungada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Udhungada"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Thanda Soru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Thanda Soru"/>
</div>
<div class="lyrico-lyrics-wrapper">Thamizh is My Mother
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thamizh is My Mother"/>
</div>
<div class="lyrico-lyrics-wrapper">I’m Single and I’m
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I’m Single and I’m"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Udhungada Sangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Udhungada Sangu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Thanda Soru Kingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Thanda Soru Kingu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thamizh is My Mother Tongue
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thamizh is My Mother Tongue"/>
</div>
<div class="lyrico-lyrics-wrapper">I’m Single and I’m Youngu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I’m Single and I’m Youngu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">I’m Single and I’m Youngu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I’m Single and I’m Youngu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oothu Sungu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oothu Sungu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanthaan Kingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanthaan Kingu"/>
</div>
<div class="lyrico-lyrics-wrapper">I’m Single and I’m Youngu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I’m Single and I’m Youngu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sangu Naanthaan Kingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sangu Naanthaan Kingu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mother Toungue
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mother Toungue"/>
</div>
<div class="lyrico-lyrics-wrapper">I’m Single and I’m Youngu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I’m Single and I’m Youngu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Oothungada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Oothungada"/>
</div>
</pre>
