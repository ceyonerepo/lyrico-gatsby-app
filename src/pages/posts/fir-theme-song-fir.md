---
title: "fir theme song song lyrics"
album: "FIR"
artist: "Ashwath"
lyricist: "L Karunakaran - Christopher Stanley - Mobin"
director: "Manu Anand"
path: "/albums/fir-lyrics"
song: "FIR - Theme Song"
image: ../../images/albumart/fir.jpg
date: 2022-02-11
lang: tamil
youtubeLink: "https://www.youtube.com/embed/n8SG1I4lpLo"
type: "theme"
singers:
  - Silambarasan TR
  - Christopher Stanley
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">yar yena yar yena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yar yena yar yena"/>
</div>
<div class="lyrico-lyrics-wrapper">ulagam ketka ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulagam ketka ivan"/>
</div>
<div class="lyrico-lyrics-wrapper">yar yena arinthida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yar yena arinthida"/>
</div>
<div class="lyrico-lyrics-wrapper">pagaivar verka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pagaivar verka"/>
</div>
<div class="lyrico-lyrics-wrapper">porkalam ivan kalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="porkalam ivan kalam"/>
</div>
<div class="lyrico-lyrics-wrapper">puluthi terika nari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puluthi terika nari"/>
</div>
<div class="lyrico-lyrics-wrapper">vettai thuvanguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vettai thuvanguthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pori ena thee yena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pori ena thee yena"/>
</div>
<div class="lyrico-lyrics-wrapper">kangal nokka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kangal nokka"/>
</div>
<div class="lyrico-lyrics-wrapper">viral thevinai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viral thevinai "/>
</div>
<div class="lyrico-lyrics-wrapper">kondru serka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kondru serka"/>
</div>
<div class="lyrico-lyrics-wrapper">thesame vegamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thesame vegamai"/>
</div>
<div class="lyrico-lyrics-wrapper">yaathi veelka ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaathi veelka ivan"/>
</div>
<div class="lyrico-lyrics-wrapper">thavam than kalaiyuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thavam than kalaiyuthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sara sara sara saravena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sara sara sara saravena"/>
</div>
<div class="lyrico-lyrics-wrapper">veyum tharano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veyum tharano"/>
</div>
<div class="lyrico-lyrics-wrapper">thada thada thada vena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thada thada thada vena"/>
</div>
<div class="lyrico-lyrics-wrapper">paayum tharano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paayum tharano"/>
</div>
<div class="lyrico-lyrics-wrapper">viru viru viru ena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viru viru viru ena"/>
</div>
<div class="lyrico-lyrics-wrapper">meyum nagano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meyum nagano"/>
</div>
<div class="lyrico-lyrics-wrapper">thuru thuru thuru vena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thuru thuru thuru vena"/>
</div>
<div class="lyrico-lyrics-wrapper">nogum marano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nogum marano"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">do you see me 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="do you see me "/>
</div>
<div class="lyrico-lyrics-wrapper">look like a ganster
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="look like a ganster"/>
</div>
<div class="lyrico-lyrics-wrapper">do you hear me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="do you hear me"/>
</div>
<div class="lyrico-lyrics-wrapper">roke like a master
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="roke like a master"/>
</div>
<div class="lyrico-lyrics-wrapper">do you see me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="do you see me"/>
</div>
<div class="lyrico-lyrics-wrapper">do you hear me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="do you hear me"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">arasanin padaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arasanin padaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">uyir irakkum porali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyir irakkum porali"/>
</div>
<div class="lyrico-lyrics-wrapper">ariviyal valaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ariviyal valaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">vinnai thandum vinnani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vinnai thandum vinnani"/>
</div>
<div class="lyrico-lyrics-wrapper">aruvadai seiya kathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aruvadai seiya kathu"/>
</div>
<div class="lyrico-lyrics-wrapper">irukum antha ulavan nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irukum antha ulavan nee"/>
</div>
<div class="lyrico-lyrics-wrapper">matha pirivinai athuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="matha pirivinai athuku"/>
</div>
<div class="lyrico-lyrics-wrapper">ethu aadum manithan nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethu aadum manithan nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nan thununjavan da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan thununjavan da"/>
</div>
<div class="lyrico-lyrics-wrapper">enna purunjavan yar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna purunjavan yar"/>
</div>
<div class="lyrico-lyrics-wrapper">nan mirugamada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan mirugamada"/>
</div>
<div class="lyrico-lyrics-wrapper">enna theriyutha paru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna theriyutha paru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">igalavan mugathirai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="igalavan mugathirai"/>
</div>
<div class="lyrico-lyrics-wrapper">kilipavan ivano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kilipavan ivano"/>
</div>
<div class="lyrico-lyrics-wrapper">irutinil ulavidum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irutinil ulavidum "/>
</div>
<div class="lyrico-lyrics-wrapper">pagalavan magano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pagalavan magano"/>
</div>
<div class="lyrico-lyrics-wrapper">iru uyir porthiya oru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iru uyir porthiya oru"/>
</div>
<div class="lyrico-lyrics-wrapper">udal nadano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="udal nadano"/>
</div>
<div class="lyrico-lyrics-wrapper">ivan ivan ivan endraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ivan ivan ivan endraal"/>
</div>
<div class="lyrico-lyrics-wrapper">avan thaan ivano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avan thaan ivano"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">im a beast im a treat im fir
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="im a beast im a treat im fir"/>
</div>
<div class="lyrico-lyrics-wrapper">imma fight imma strike im fir
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="imma fight imma strike im fir"/>
</div>
<div class="lyrico-lyrics-wrapper">im just another you aim fir
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="im just another you aim fir"/>
</div>
<div class="lyrico-lyrics-wrapper">im the you get me a aim fir
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="im the you get me a aim fir"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">yar yena yar yena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yar yena yar yena"/>
</div>
<div class="lyrico-lyrics-wrapper">ulagam ketka ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulagam ketka ivan"/>
</div>
<div class="lyrico-lyrics-wrapper">yar yena arinthida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yar yena arinthida"/>
</div>
<div class="lyrico-lyrics-wrapper">pagaivar verka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pagaivar verka"/>
</div>
<div class="lyrico-lyrics-wrapper">porkalam ivan kalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="porkalam ivan kalam"/>
</div>
<div class="lyrico-lyrics-wrapper">puluthi terika nari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puluthi terika nari"/>
</div>
<div class="lyrico-lyrics-wrapper">vettai thuvanguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vettai thuvanguthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pori ena thee yena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pori ena thee yena"/>
</div>
<div class="lyrico-lyrics-wrapper">kangal nokka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kangal nokka"/>
</div>
<div class="lyrico-lyrics-wrapper">viral thevinai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viral thevinai "/>
</div>
<div class="lyrico-lyrics-wrapper">kondru serka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kondru serka"/>
</div>
<div class="lyrico-lyrics-wrapper">thesame vegamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thesame vegamai"/>
</div>
<div class="lyrico-lyrics-wrapper">yaathi veelka ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaathi veelka ivan"/>
</div>
<div class="lyrico-lyrics-wrapper">thavam than kalaiyuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thavam than kalaiyuthe"/>
</div>
</pre>
