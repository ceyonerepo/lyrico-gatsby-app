---
title: "vaanam engilum song lyrics"
album: "Nil Gavani Sellathey"
artist: "Selvaganesh"
lyricist: "Na. Muthukumar"
director: "Anand Chakravarthy"
path: "/albums/nil-gavani-sellathey-lyrics"
song: "Vaanam Engilum"
image: ../../images/albumart/nil-gavani-sellathey.jpg
date: 2011-03-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/KKRlcAUNxBM"
type: "happy"
singers:
  - Ranjith
  - Vijaynarain
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vaanam Engilum Pudhuppudhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam Engilum Pudhuppudhu "/>
</div>
<div class="lyrico-lyrics-wrapper">Megam Poagudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megam Poagudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhigal Engilum Pudhuppudhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhigal Engilum Pudhuppudhu "/>
</div>
<div class="lyrico-lyrics-wrapper">Pookkal Pookkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pookkal Pookkudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thada Thada Thada Thaalathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thada Thada Thada Thaalathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidu Vidu Vidu Thegathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidu Vidu Vidu Thegathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoda Thoda Thoda Thoorathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoda Thoda Thoda Thoorathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Chuda Chuda Chuda Marmathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chuda Chuda Chuda Marmathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhuvo Edhuvo Azhaikkum Vaa Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhuvo Edhuvo Azhaikkum Vaa Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhkkai Enbadhu Neeroattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkkai Enbadhu Neeroattam"/>
</div>
<div class="lyrico-lyrics-wrapper">Mirandaal Pallangal Bayamootum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mirandaal Pallangal Bayamootum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunindhaal Thoorangal Kai Ettum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunindhaal Thoorangal Kai Ettum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidindhaal Vilagividum Pani Moottam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidindhaal Vilagividum Pani Moottam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanam Engilum Pudhuppudhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam Engilum Pudhuppudhu "/>
</div>
<div class="lyrico-lyrics-wrapper">Megam Poagudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megam Poagudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhigal Engilum Pudhuppudhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhigal Engilum Pudhuppudhu "/>
</div>
<div class="lyrico-lyrics-wrapper">Pookkal Pookkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pookkal Pookkudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varuvadhu Adhai Yaarum Yaarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varuvadhu Adhai Yaarum Yaarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Adadaa Arindhida Mudiyaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adadaa Arindhida Mudiyaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Poagindra Paadhai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poagindra Paadhai "/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhidhaai Vandhadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhidhaai Vandhadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhai Yaarum Yaarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai Yaarum Yaarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhapin Thaduthida Mudiyaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhapin Thaduthida Mudiyaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhkkai En Geedhai Idhuthaano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkkai En Geedhai Idhuthaano"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thada Thada Thada Thaalathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thada Thada Thada Thaalathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidu Vidu Vidu Thegathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidu Vidu Vidu Thegathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoda Thoda Thoda Thoorathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoda Thoda Thoda Thoorathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Chuda Chuda Chuda Marmathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chuda Chuda Chuda Marmathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhuvo Edhuvo Azhaikkum Vaa Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhuvo Edhuvo Azhaikkum Vaa Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhkkai Enbadhu Neeroattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkkai Enbadhu Neeroattam"/>
</div>
<div class="lyrico-lyrics-wrapper">Mirandaal Pallangal Bayamootum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mirandaal Pallangal Bayamootum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunindhaal Thoorangal Kai Ettum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunindhaal Thoorangal Kai Ettum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidindhaal Vilagividum Pani Moottam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidindhaal Vilagividum Pani Moottam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanam Engilum Pudhuppudhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam Engilum Pudhuppudhu "/>
</div>
<div class="lyrico-lyrics-wrapper">Megam Poagudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megam Poagudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhigal Engilum Pudhuppudhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhigal Engilum Pudhuppudhu "/>
</div>
<div class="lyrico-lyrics-wrapper">Pookkal Pookkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pookkal Pookkudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manadhil Anai Yaarum Yaarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manadhil Anai Yaarum Yaarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Adadaa Poattadhu Kidaiyaadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adadaa Poattadhu Kidaiyaadhey"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasaikku Alavey Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasaikku Alavey Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilamaiyil Ada Yaarum Parandhida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilamaiyil Ada Yaarum Parandhida"/>
</div>
<div class="lyrico-lyrics-wrapper">Payandhadhu Kidaiyaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Payandhadhu Kidaiyaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanamum Ellai Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanamum Ellai Illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thada Thada Thada Thaalathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thada Thada Thada Thaalathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidu Vidu Vidu Thegathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidu Vidu Vidu Thegathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoda Thoda Thoda Thoorathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoda Thoda Thoda Thoorathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Chuda Chuda Chuda Marmathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chuda Chuda Chuda Marmathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhuvo Edhuvo Azhaikkum Vaa Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhuvo Edhuvo Azhaikkum Vaa Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhkkai Enbadhu Neeroattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkkai Enbadhu Neeroattam"/>
</div>
<div class="lyrico-lyrics-wrapper">Mirandaal Pallangal Bayamootum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mirandaal Pallangal Bayamootum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunindhaal Thoorangal Kai Ettum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunindhaal Thoorangal Kai Ettum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidindhaal Vilagividum Pani Moottam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidindhaal Vilagividum Pani Moottam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanam Engilum Pudhuppudhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam Engilum Pudhuppudhu "/>
</div>
<div class="lyrico-lyrics-wrapper">Megam Poagudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megam Poagudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhigal Engilum Pudhuppudhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhigal Engilum Pudhuppudhu "/>
</div>
<div class="lyrico-lyrics-wrapper">Pookkal Pookkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pookkal Pookkudhu"/>
</div>
</pre>
