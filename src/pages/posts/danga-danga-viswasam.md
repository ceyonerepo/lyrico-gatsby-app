---
title: 'danga danga song lyrics'
album: 'Viswasam'
artist: 'D Imman'
lyricist: 'Arunbharathi'
director: 'Siva'
path: '/albums/viswasam-song-lyrics'
song: 'Danga Danga'
image: ../../images/albumart/viswasam.jpg
date: 2019-01-10
lang: tamil
singers:
- Senthil Ganesh
- Rajalakshmi
youtubeLink: "https://www.youtube.com/embed/Ad9xNStkeGI"
type: 'folk'
---



<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Oththa nodi unna kandu
<input type="checkbox" class="lyrico-select-lyric-line" value="Oththa nodi unna kandu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil soora kaathu
<input type="checkbox" class="lyrico-select-lyric-line" value="Nenjil soora kaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Suththi suththi veesuthadi
<input type="checkbox" class="lyrico-select-lyric-line" value="Suththi suththi veesuthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanna kaatti thaethu
<input type="checkbox" class="lyrico-select-lyric-line" value="Kanna kaatti thaethu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Danga danga danga danga
<input type="checkbox" class="lyrico-select-lyric-line" value="Danga danga danga danga"/>
</div>
<div class="lyrico-lyrics-wrapper">Danga danga danae
<input type="checkbox" class="lyrico-select-lyric-line" value="Danga danga danae"/>
</div>
<div class="lyrico-lyrics-wrapper">Digiru danga digiru danga
<input type="checkbox" class="lyrico-select-lyric-line" value="Digiru danga digiru danga"/>
</div>
<div class="lyrico-lyrics-wrapper">Digiru danga danae
<input type="checkbox" class="lyrico-select-lyric-line" value="Digiru danga danae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Uchcham thala suthuthaiyaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Uchcham thala suthuthaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai vandhu paarthu
<input type="checkbox" class="lyrico-select-lyric-line" value="Unnai vandhu paarthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Uththu paarthu uththu paarthu
<input type="checkbox" class="lyrico-select-lyric-line" value="Uththu paarthu uththu paarthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kulikka vecha neththu
<input type="checkbox" class="lyrico-select-lyric-line" value="Kulikka vecha neththu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Danga danga danga danga
<input type="checkbox" class="lyrico-select-lyric-line" value="Danga danga danga danga"/>
</div>
<div class="lyrico-lyrics-wrapper">Danga danga danae
<input type="checkbox" class="lyrico-select-lyric-line" value="Danga danga danae"/>
</div>
<div class="lyrico-lyrics-wrapper">Digiru danga digiru danga
<input type="checkbox" class="lyrico-select-lyric-line" value="Digiru danga digiru danga"/>
</div>
<div class="lyrico-lyrics-wrapper">Digiru danga danae
<input type="checkbox" class="lyrico-select-lyric-line" value="Digiru danga danae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Goiyaa pazham
<input type="checkbox" class="lyrico-select-lyric-line" value="Goiyaa pazham"/>
</div>
<div class="lyrico-lyrics-wrapper">Heyy goiyaa pazham
<input type="checkbox" class="lyrico-select-lyric-line" value="Heyy goiyaa pazham"/>
</div>
<div class="lyrico-lyrics-wrapper">Goiyaa pazham nee thaanu
<input type="checkbox" class="lyrico-select-lyric-line" value="Goiyaa pazham nee thaanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Koththi thinga paarthen
<input type="checkbox" class="lyrico-select-lyric-line" value="Koththi thinga paarthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Gundu maanga venaannu
<input type="checkbox" class="lyrico-select-lyric-line" value="Gundu maanga venaannu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna thaandi ketten
<input type="checkbox" class="lyrico-select-lyric-line" value="Unna thaandi ketten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Danga danga danga danga
<input type="checkbox" class="lyrico-select-lyric-line" value="Danga danga danga danga"/>
</div>
<div class="lyrico-lyrics-wrapper">Danga danga danae
<input type="checkbox" class="lyrico-select-lyric-line" value="Danga danga danae"/>
</div>
<div class="lyrico-lyrics-wrapper">Digiru danga digiru danga
<input type="checkbox" class="lyrico-select-lyric-line" value="Digiru danga digiru danga"/>
</div>
<div class="lyrico-lyrics-wrapper">Digiru danga danae
<input type="checkbox" class="lyrico-select-lyric-line" value="Digiru danga danae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thanni chembu neethaanu
<input type="checkbox" class="lyrico-select-lyric-line" value="Thanni chembu neethaanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaagam pola vandhen
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaagam pola vandhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholuva kambu neeyaana
<input type="checkbox" class="lyrico-select-lyric-line" value="Tholuva kambu neeyaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Kookki pottu ninnen
<input type="checkbox" class="lyrico-select-lyric-line" value="Kookki pottu ninnen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Danga danga danga danga
<input type="checkbox" class="lyrico-select-lyric-line" value="Danga danga danga danga"/>
</div>
<div class="lyrico-lyrics-wrapper">Danga danga danae
<input type="checkbox" class="lyrico-select-lyric-line" value="Danga danga danae"/>
</div>
<div class="lyrico-lyrics-wrapper">Digiru danga digiru danga
<input type="checkbox" class="lyrico-select-lyric-line" value="Digiru danga digiru danga"/>
</div>
<div class="lyrico-lyrics-wrapper">Digiru danga danae
<input type="checkbox" class="lyrico-select-lyric-line" value="Digiru danga danae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Aalamaram nee thaanu
<input type="checkbox" class="lyrico-select-lyric-line" value="Aalamaram nee thaanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pullaiyaaraa vandhen
<input type="checkbox" class="lyrico-select-lyric-line" value="Pullaiyaaraa vandhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Soori kaththi kanna kaatta
<input type="checkbox" class="lyrico-select-lyric-line" value="Soori kaththi kanna kaatta"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaayam pattu ninnen
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaayam pattu ninnen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ammi kallu nee thaanu
<input type="checkbox" class="lyrico-select-lyric-line" value="Ammi kallu nee thaanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaenga sillaa vandhen
<input type="checkbox" class="lyrico-select-lyric-line" value="Thaenga sillaa vandhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Aattukallu neeaaga
<input type="checkbox" class="lyrico-select-lyric-line" value="Aattukallu neeaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Maavu arachi thandhen
<input type="checkbox" class="lyrico-select-lyric-line" value="Maavu arachi thandhen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Danga danga danga danga
<input type="checkbox" class="lyrico-select-lyric-line" value="Danga danga danga danga"/>
</div>
<div class="lyrico-lyrics-wrapper">Danga danga danae
<input type="checkbox" class="lyrico-select-lyric-line" value="Danga danga danae"/>
</div>
<div class="lyrico-lyrics-wrapper">Digiru danga digiru danga
<input type="checkbox" class="lyrico-select-lyric-line" value="Digiru danga digiru danga"/>
</div>
<div class="lyrico-lyrics-wrapper">Digiru danga danae
<input type="checkbox" class="lyrico-select-lyric-line" value="Digiru danga danae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kallaa petti
<input type="checkbox" class="lyrico-select-lyric-line" value="Kallaa petti"/>
</div>
<div class="lyrico-lyrics-wrapper">Heyy kallaa petti
<input type="checkbox" class="lyrico-select-lyric-line" value="Heyy kallaa petti"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallaa petti nee thaanu
<input type="checkbox" class="lyrico-select-lyric-line" value="Kallaa petti nee thaanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaasu poda vandhen
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaasu poda vandhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanga petti unna kandu
<input type="checkbox" class="lyrico-select-lyric-line" value="Thanga petti unna kandu"/>
</div>
<div class="lyrico-lyrics-wrapper">Malachu poiyu ninnen
<input type="checkbox" class="lyrico-select-lyric-line" value="Malachu poiyu ninnen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Danga danga danga danga
<input type="checkbox" class="lyrico-select-lyric-line" value="Danga danga danga danga"/>
</div>
<div class="lyrico-lyrics-wrapper">Danga danga danae
<input type="checkbox" class="lyrico-select-lyric-line" value="Danga danga danae"/>
</div>
<div class="lyrico-lyrics-wrapper">Digiru danga digiru danga
<input type="checkbox" class="lyrico-select-lyric-line" value="Digiru danga digiru danga"/>
</div>
<div class="lyrico-lyrics-wrapper">Digiru danga danae
<input type="checkbox" class="lyrico-select-lyric-line" value="Digiru danga danae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Heyy kettimelam
<input type="checkbox" class="lyrico-select-lyric-line" value="Heyy kettimelam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottida thaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Kottida thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathurikken maama
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaathurikken maama"/>
</div>
<div class="lyrico-lyrics-wrapper">Manja thaali kondu vandhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Manja thaali kondu vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattidunga aama
<input type="checkbox" class="lyrico-select-lyric-line" value="Kattidunga aama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Danga danga danga danga
<input type="checkbox" class="lyrico-select-lyric-line" value="Danga danga danga danga"/>
</div>
<div class="lyrico-lyrics-wrapper">Danga danga danae
<input type="checkbox" class="lyrico-select-lyric-line" value="Danga danga danae"/>
</div>
<div class="lyrico-lyrics-wrapper">Digiru danga digiru danga
<input type="checkbox" class="lyrico-select-lyric-line" value="Digiru danga digiru danga"/>
</div>
<div class="lyrico-lyrics-wrapper">Digiru danga danae (2 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Digiru danga danae"/></div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Danga danga danga danga
<input type="checkbox" class="lyrico-select-lyric-line" value="Danga danga danga danga"/>
</div>
<div class="lyrico-lyrics-wrapper">Danga danga danae
<input type="checkbox" class="lyrico-select-lyric-line" value="Danga danga danae"/>
</div>
<div class="lyrico-lyrics-wrapper">Digiru danga digiru danga
<input type="checkbox" class="lyrico-select-lyric-line" value="Digiru danga digiru danga"/>
</div>
<div class="lyrico-lyrics-wrapper">Digiru danga danae (2 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Digiru danga danae"/></div>
</div>
</pre>