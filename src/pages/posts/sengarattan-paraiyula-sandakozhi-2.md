---
title: "sengarattan paraiyula song lyrics"
album: "Sandakozhi 2"
artist: "Yuvan Sankar Raja"
lyricist: "Arivumathi"
director: "N. Linguswamy"
path: "/albums/sandakozhi-2-lyrics"
song: "Sengarattan Paraiyula"
image: ../../images/albumart/sandakozhi-2.jpg
date: 2018-10-18
lang: tamil
youtubeLink: "https://www.youtube.com/embed/1grgjUPjEIA"
type: "Love"
singers:
  - Ramani Ammal
  - Senthil Dass
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Sengarattan Paaraiyula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sengarattan Paaraiyula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sittothungum Velaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sittothungum Velaiyila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Akkurama Paakuriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Akkurama Paakuriye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ekka Vittu Thookuriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ekka Vittu Thookuriye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Akkurama Paakuriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Akkurama Paakuriye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanne Nannaanane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanne Nannaanane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Ekka Vittu Thookuriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Ekka Vittu Thookuriye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanne Nannaanane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanne Nannaanane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konjamum Nalla Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjamum Nalla Illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanne Nannaanane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanne Nannaanane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanne Nannaanane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanne Nannaanane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maanag Karukayila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maanag Karukayila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maarappu Moraikkayila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maarappu Moraikkayila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaru Nuraikayila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaru Nuraikayila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aadu Rendu Verikkayila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadu Rendu Verikkayila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjukkulla Jinjunukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjukkulla Jinjunukka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanne Nannaanane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanne Nannaanane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manjanathi Marainjirikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manjanathi Marainjirikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanne Nannaanane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanne Nannaanane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manjanathi Marainjirikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manjanathi Marainjirikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanne Nannaanane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanne Nannaanane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanne Nannaanane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanne Nannaanane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kondayila Kozhi Kuththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondayila Kozhi Kuththa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paakku Muzhi Pacha Kuththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paakku Muzhi Pacha Kuththa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Suththu Muththum Yaarum Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suththu Muththum Yaarum Illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oththukkita Kitta Varen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oththukkita Kitta Varen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Suththu Muththum Yaarum Illaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suththu Muththum Yaarum Illaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanne Nannaanane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanne Nannaanane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Oththukkita Kitta Varen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Oththukkita Kitta Varen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanne Nannaanane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanne Nannaanane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muththam Tharen Muththam Tharen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muththam Tharen Muththam Tharen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanne Nannaanane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanne Nannaanane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oda Ozhukayile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oda Ozhukayile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaada Othugayilaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaada Othugayilaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Whistle U Adichu Koopputhakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Whistle U Adichu Koopputhakka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vegu Peru Paapaangannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vegu Peru Paapaangannu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Usura Vittu  Koopittenaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usura Vittu  Koopittenaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ullukulla Kekalaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullukulla Kekalaya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Usura Vittu Koopittenaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usura Vittu Koopittenaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanne Nannaanane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanne Nannaanane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Ullukulla Kekalaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Ullukulla Kekalaya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanne Nannaanane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanne Nannaanane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ullukulla Kekkalaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullukulla Kekkalaya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanne Nannaanane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanne Nannaanane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ullukulla Kekkalaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullukulla Kekkalaya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanne Nannaanane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanne Nannaanane"/>
</div>
</pre>
