---
title: "ismart title song song lyrics"
album: "Ismart Shankar"
artist: "Mani Sharma"
lyricist: "Bhaskarabhatla Ravi Kumar"
director: "Puri Jagannadh"
path: "/albums/ismart-shankar-lyrics"
song: "Ismart Title Song"
image: ../../images/albumart/ismart-shankar.jpg
date: 2019-07-18
lang: telugu
youtubeLink: "https://www.youtube.com/embed/Ox4ih-vJu7E"
type: "title track"
singers:
  - Anurag Kulkarni
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Patha hai main kaun hoon
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Patha hai main kaun hoon"/>
</div>
<div class="lyrico-lyrics-wrapper">Shankar ustaad iSmart Shankar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shankar ustaad iSmart Shankar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gada bida laku befikar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gada bida laku befikar"/>
</div>
<div class="lyrico-lyrics-wrapper">Sadak sadak kadak pogar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sadak sadak kadak pogar"/>
</div>
<div class="lyrico-lyrics-wrapper">iStyle dekho niche upar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iStyle dekho niche upar"/>
</div>
<div class="lyrico-lyrics-wrapper">iSh iSh iSmartu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iSh iSh iSmartu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naam bole tho galli hadal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam bole tho galli hadal"/>
</div>
<div class="lyrico-lyrics-wrapper">Double dhimak undhi idhar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Double dhimak undhi idhar"/>
</div>
<div class="lyrico-lyrics-wrapper">Karle apni neeche nazar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karle apni neeche nazar"/>
</div>
<div class="lyrico-lyrics-wrapper">iSh iSh iSmartu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iSh iSh iSmartu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey hyderabad shahar mei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hyderabad shahar mei"/>
</div>
<div class="lyrico-lyrics-wrapper">Pucho bey saale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pucho bey saale"/>
</div>
<div class="lyrico-lyrics-wrapper">Charminar chadharghat antha naadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Charminar chadharghat antha naadhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kiri kiri kiri kiri kiri kiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kiri kiri kiri kiri kiri kiri"/>
</div>
<div class="lyrico-lyrics-wrapper">Jesthe maki kirikire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jesthe maki kirikire"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hahaha iSmart Shankar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hahaha iSmart Shankar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eh beer esukunta bindasugunta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh beer esukunta bindasugunta"/>
</div>
<div class="lyrico-lyrics-wrapper">Bam bhole shambo shiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bam bhole shambo shiva"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanu viketodu duniyala ledu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanu viketodu duniyala ledu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yadunna naadhe hawaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yadunna naadhe hawaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edaina gani matter
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edaina gani matter"/>
</div>
<div class="lyrico-lyrics-wrapper">Chai batthi pe settle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chai batthi pe settle"/>
</div>
<div class="lyrico-lyrics-wrapper">Tegaledhante agar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tegaledhante agar"/>
</div>
<div class="lyrico-lyrics-wrapper">Sar pe phod dhun bottle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sar pe phod dhun bottle"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">iSmile emo kirrak brother
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iSmile emo kirrak brother"/>
</div>
<div class="lyrico-lyrics-wrapper">Cut out emo garam figure
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cut out emo garam figure"/>
</div>
<div class="lyrico-lyrics-wrapper">Akkad bakkad ek hi takkar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Akkad bakkad ek hi takkar"/>
</div>
<div class="lyrico-lyrics-wrapper">iSmart Shankar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iSmart Shankar"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhigindante khatham matter
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhigindante khatham matter"/>
</div>
<div class="lyrico-lyrics-wrapper">Makkeliragadhise meter
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Makkeliragadhise meter"/>
</div>
<div class="lyrico-lyrics-wrapper">Katak matak chattar battar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katak matak chattar battar"/>
</div>
<div class="lyrico-lyrics-wrapper">iSmart Shankar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iSmart Shankar"/>
</div>
<div class="lyrico-lyrics-wrapper">Oye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hahaha iSmart Shankar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hahaha iSmart Shankar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eyy bomma Nuv hu ante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eyy bomma Nuv hu ante"/>
</div>
<div class="lyrico-lyrics-wrapper">Golkonda repair chesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Golkonda repair chesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee chethila pedtha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee chethila pedtha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu begum ni chesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu begum ni chesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Qila meedha kuso bedtha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Qila meedha kuso bedtha"/>
</div>
<div class="lyrico-lyrics-wrapper">Hahaha kya bolthi.. Ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hahaha kya bolthi.. Ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chal bey saale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chal bey saale"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilantollani mastu chushna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilantollani mastu chushna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dil ne pathangila egareska poye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil ne pathangila egareska poye"/>
</div>
<div class="lyrico-lyrics-wrapper">Khaddu kha kheer ladki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khaddu kha kheer ladki"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa kantla vadithe idiSedhe ledhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa kantla vadithe idiSedhe ledhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattestha uriki uriki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattestha uriki uriki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bas hei ek nazar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bas hei ek nazar"/>
</div>
<div class="lyrico-lyrics-wrapper">Bajega dil ki bazar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bajega dil ki bazar"/>
</div>
<div class="lyrico-lyrics-wrapper">De dunga banthi flower
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="De dunga banthi flower"/>
</div>
<div class="lyrico-lyrics-wrapper">Gunguru gunguru gal gal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gunguru gunguru gal gal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Fidaa hua dekh ke shakal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fidaa hua dekh ke shakal"/>
</div>
<div class="lyrico-lyrics-wrapper">Love jestha rathri pagal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love jestha rathri pagal"/>
</div>
<div class="lyrico-lyrics-wrapper">Koni vedatha kilo nagal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koni vedatha kilo nagal"/>
</div>
<div class="lyrico-lyrics-wrapper">iSmart Shankar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iSmart Shankar"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadum chusthe centimeter
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadum chusthe centimeter"/>
</div>
<div class="lyrico-lyrics-wrapper">Venakostha kilometer
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venakostha kilometer"/>
</div>
<div class="lyrico-lyrics-wrapper">Gift iStha 7 seater
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gift iStha 7 seater"/>
</div>
<div class="lyrico-lyrics-wrapper">iSmart Shankar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iSmart Shankar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey iSmart nuvvu thurum raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey iSmart nuvvu thurum raa"/>
</div>
</pre>
