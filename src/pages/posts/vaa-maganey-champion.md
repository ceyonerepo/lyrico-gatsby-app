---
title: "vaa maganey song lyrics"
album: "Champion"
artist: "Arrol Corelli"
lyricist: "Mohan Rajan"
director: "Suseenthiran"
path: "/albums/champion-lyrics"
song: "Vaa Maganey"
image: ../../images/albumart/champion.jpg
date: 2019-12-13
lang: tamil
youtubeLink: 
type: "melody"
singers:
  - Haricharan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vaa Maganae Vaa Maganae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Maganae Vaa Maganae"/>
</div>
<div class="lyrico-lyrics-wrapper">Munnadi Nee Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munnadi Nee Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Kanda En Kanavin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Kanda En Kanavin"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannadi Nee Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannadi Nee Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Toun Toun Toun Toun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Toun Toun Toun Toun"/>
</div>
<div class="lyrico-lyrics-wrapper">Tatataan Tatataan Toun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tatataan Tatataan Toun"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Po Maganae Po Maganae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Po Maganae Po Maganae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pandhaadi Nee Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pandhaadi Nee Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Dhesaiya Nee Therinja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Dhesaiya Nee Therinja"/>
</div>
<div class="lyrico-lyrics-wrapper">Killadi Nee Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Killadi Nee Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha Ulagil Apponoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Ulagil Apponoda"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavu Ellaamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavu Ellaamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Onnaaga Thaan Irukkum Keladaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnaaga Thaan Irukkum Keladaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaan Nenacha Vaazhkaiya Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaan Nenacha Vaazhkaiya Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pilla Vaazhanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pilla Vaazhanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhukku Mela Vera Illavae Illa Daaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhukku Mela Vera Illavae Illa Daaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Toun Toun Toun Toun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Toun Toun Toun Toun"/>
</div>
<div class="lyrico-lyrics-wrapper">Tatataan Tatataan Toun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tatataan Tatataan Toun"/>
</div>
<div class="lyrico-lyrics-wrapper">Toun Toun Toun Toun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Toun Toun Toun Toun"/>
</div>
<div class="lyrico-lyrics-wrapper">Tatataan Tatataan Toun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tatataan Tatataan Toun"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vilaiyaattu Adhu Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilaiyaattu Adhu Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhigaalaiyila Eppodhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhigaalaiyila Eppodhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagaa Unna Ezha Veikkumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaa Unna Ezha Veikkumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Udambodu Manasum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udambodu Manasum"/>
</div>
<div class="lyrico-lyrics-wrapper">Balam Aagum Vara Neeyum Aadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Balam Aagum Vara Neeyum Aadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vervai Ellaam Unna Sedhukkumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vervai Ellaam Unna Sedhukkumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maidhaanam Kaththu Tharum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maidhaanam Kaththu Tharum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadam Pala Nooru Daa Keluu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadam Pala Nooru Daa Keluu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudivaatham Athu Onnu Thaan Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudivaatham Athu Onnu Thaan Daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Mela Yeththumae Paaruuuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Mela Yeththumae Paaruuuu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Vaazhka Vilaiyaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Vaazhka Vilaiyaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Aarambikkum Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aarambikkum Daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhukkulla Nee Jeyikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhukkulla Nee Jeyikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathukanum Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathukanum Daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Toun Toun Toun Toun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Toun Toun Toun Toun"/>
</div>
<div class="lyrico-lyrics-wrapper">Tatataan Tatataan Toun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tatataan Tatataan Toun"/>
</div>
<div class="lyrico-lyrics-wrapper">Toun Toun Toun Toun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Toun Toun Toun Toun"/>
</div>
<div class="lyrico-lyrics-wrapper">Tatataan Tatataan Toun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tatataan Tatataan Toun"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa Maganae Vaa Maganae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Maganae Vaa Maganae"/>
</div>
<div class="lyrico-lyrics-wrapper">Munnadi Nee Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munnadi Nee Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Kanda En Kanavin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Kanda En Kanavin"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannadi Nee Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannadi Nee Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Po Maganae Po Maganae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Po Maganae Po Maganae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pandhaadi Nee Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pandhaadi Nee Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Dhesaiya Nee Therinja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Dhesaiya Nee Therinja"/>
</div>
<div class="lyrico-lyrics-wrapper">Killadi Nee Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Killadi Nee Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha Ulagil Apponoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Ulagil Apponoda"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavu Ellaamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavu Ellaamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Onnaaga Thaan Irukkum Keladaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnaaga Thaan Irukkum Keladaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaan Nenacha Vaazhkaiya Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaan Nenacha Vaazhkaiya Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pilla Vaazhanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pilla Vaazhanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhukku Mela Vera Illavae Illa Daaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhukku Mela Vera Illavae Illa Daaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Toun Toun Toun Toun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Toun Toun Toun Toun"/>
</div>
<div class="lyrico-lyrics-wrapper">Tatataan Tatataan Toun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tatataan Tatataan Toun"/>
</div>
<div class="lyrico-lyrics-wrapper">Toun Toun Toun Toun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Toun Toun Toun Toun"/>
</div>
<div class="lyrico-lyrics-wrapper">Tatataan Tatataan Toun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tatataan Tatataan Toun"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Toun Toun Toun Toun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Toun Toun Toun Toun"/>
</div>
<div class="lyrico-lyrics-wrapper">Tatataan Tatataan Toun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tatataan Tatataan Toun"/>
</div>
<div class="lyrico-lyrics-wrapper">Toun Toun Toun Toun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Toun Toun Toun Toun"/>
</div>
<div class="lyrico-lyrics-wrapper">Tatataan Tatataan Toun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tatataan Tatataan Toun"/>
</div>
</pre>
