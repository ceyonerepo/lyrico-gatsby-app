---
title: "behti si song lyrics"
album: "Darbaan"
artist: "Raajeev V Bhalla"
lyricist: "Akshay K. Saxena"
director: "Bipin Nadkarni"
path: "/albums/darbaan-lyrics"
song: "Behti Si"
image: ../../images/albumart/darbaan.jpg
date: 2020-12-04
lang: hindi
youtubeLink: "https://www.youtube.com/embed/ZysqY0l9a8Q"
type: "happy"
singers:
  - Raajeev V. Bhalla
  - Rashi Harmalkar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">o re o re ..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="o re o re .."/>
</div>
<div class="lyrico-lyrics-wrapper">bahatee see rukee hogee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bahatee see rukee hogee"/>
</div>
<div class="lyrico-lyrics-wrapper">ja raha hoon hee kahaan hona chaahie
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ja raha hoon hee kahaan hona chaahie"/>
</div>
<div class="lyrico-lyrics-wrapper">od tera nazar na aaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="od tera nazar na aaya"/>
</div>
<div class="lyrico-lyrics-wrapper">kee sapane se takarae too
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kee sapane se takarae too"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">bahatee see ye zindagee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bahatee see ye zindagee"/>
</div>
<div class="lyrico-lyrics-wrapper">zindagee suniye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="zindagee suniye"/>
</div>
<div class="lyrico-lyrics-wrapper">bahatee see ye zindagee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bahatee see ye zindagee"/>
</div>
<div class="lyrico-lyrics-wrapper">zindagee suniye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="zindagee suniye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">o re o re ..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="o re o re .."/>
</div>
<div class="lyrico-lyrics-wrapper">o re o re ..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="o re o re .."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">koee kahe too ek pahelee hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koee kahe too ek pahelee hai"/>
</div>
<div class="lyrico-lyrics-wrapper">bujh na paaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bujh na paaya"/>
</div>
<div class="lyrico-lyrics-wrapper">lukka-chhupee kesee ye khelee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lukka-chhupee kesee ye khelee"/>
</div>
<div class="lyrico-lyrics-wrapper">khoj na paaya main abhee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="khoj na paaya main abhee"/>
</div>
<div class="lyrico-lyrics-wrapper">unnanan ..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnanan .."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">bahatee see ye zindagee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bahatee see ye zindagee"/>
</div>
<div class="lyrico-lyrics-wrapper">zindagee suniye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="zindagee suniye"/>
</div>
<div class="lyrico-lyrics-wrapper">bahatee see ye zindagee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bahatee see ye zindagee"/>
</div>
<div class="lyrico-lyrics-wrapper">zindagee suniye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="zindagee suniye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">o re o re ..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="o re o re .."/>
</div>
<div class="lyrico-lyrics-wrapper">o re o re ..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="o re o re .."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">bahatee see
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bahatee see"/>
</div>
<div class="lyrico-lyrics-wrapper">ja raha hoon hee kahaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ja raha hoon hee kahaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nazar na aaee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nazar na aaee"/>
</div>
<div class="lyrico-lyrics-wrapper">kyoon sapane se takarae too
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kyoon sapane se takarae too"/>
</div>
<div class="lyrico-lyrics-wrapper">bahatee see ye zindagee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bahatee see ye zindagee"/>
</div>
<div class="lyrico-lyrics-wrapper">zindagee suniye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="zindagee suniye"/>
</div>
<div class="lyrico-lyrics-wrapper">bahatee see ye zindagee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bahatee see ye zindagee"/>
</div>
<div class="lyrico-lyrics-wrapper">zindagee suniye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="zindagee suniye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">o re o re ..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="o re o re .."/>
</div>
<div class="lyrico-lyrics-wrapper">o re o re ..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="o re o re .."/>
</div>
<div class="lyrico-lyrics-wrapper">o re o re ..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="o re o re .."/>
</div>
</pre>
