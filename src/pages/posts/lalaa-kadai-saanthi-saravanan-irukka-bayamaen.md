---
title: "lalaa kadai saanthi song lyrics"
album: "Saravanan Irukka Bayamaen"
artist: "D Imman"
lyricist: "Yugabharathi"
director: "Ezhil"
path: "/albums/saravanan-irukka-bayamaen-lyrics"
song: "Lalaa Kadai Saanthi"
image: ../../images/albumart/saravanan-irukka-bayamaen.jpg
date: 2017-05-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/D3O6nOrI5Ug"
type: "happy"
singers:
  -	Benny Dayal
  - Sunidhi Chauhan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vethu vethuppaa inichirukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vethu vethuppaa inichirukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vedala ponnu naanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedala ponnu naanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Viruppa pattu nerungi vanthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viruppa pattu nerungi vanthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Velakku anaikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velakku anaikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaammaa nee sonnaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaammaa nee sonnaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharuvenae thaenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharuvenae thaenu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vattiyoda asala vaangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vattiyoda asala vaangu"/>
</div>
<div class="lyrico-lyrics-wrapper">Athuthaanae scene-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athuthaanae scene-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lala kada shanthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lala kada shanthi"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaal aanenae naan boonthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaal aanenae naan boonthi"/>
</div>
<div class="lyrico-lyrics-wrapper">Lala kada shanthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lala kada shanthi"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaal aanenae naan boonthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaal aanenae naan boonthi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paathaa pala palakkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paathaa pala palakkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Paalaa vazhiya veikkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paalaa vazhiya veikkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Keethaa kizhiya veikkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Keethaa kizhiya veikkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Kirukkethi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kirukkethi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kettaa kathai alakkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kettaa kathai alakkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Kepaa enna vedikkira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kepaa enna vedikkira"/>
</div>
<div class="lyrico-lyrics-wrapper">Theettaa othungi nikkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theettaa othungi nikkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Usuppethi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usuppethi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lala kada shanthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lala kada shanthi"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna povenae naan yenthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna povenae naan yenthi"/>
</div>
<div class="lyrico-lyrics-wrapper">Lala kada shanthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lala kada shanthi"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna povenae naan yenthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna povenae naan yenthi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yendaa eli pudikkira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yendaa eli pudikkira"/>
</div>
<div class="lyrico-lyrics-wrapper">Eeyaayela virikkira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eeyaayela virikkira"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoondi thuruva nikkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoondi thuruva nikkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Suthi yethi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suthi yethi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaandaasuzhala veikkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaandaasuzhala veikkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaagaavazhi marikkira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaagaavazhi marikkira"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaandi thavaru pannura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaandi thavaru pannura"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi yaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi yaathi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lala kada shanthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lala kada shanthi"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaal aanenae naan boonthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaal aanenae naan boonthi"/>
</div>
<div class="lyrico-lyrics-wrapper">Lala kada shanthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lala kada shanthi"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna povenae naan yenthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna povenae naan yenthi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aasai verakaduppula vega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai verakaduppula vega"/>
</div>
<div class="lyrico-lyrics-wrapper">Velanju nikkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velanju nikkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Rosa unna nenaikkaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rosa unna nenaikkaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Nedi yerum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nedi yerum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paasi payiru kannula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paasi payiru kannula"/>
</div>
<div class="lyrico-lyrics-wrapper">Badam paruppu seiyura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Badam paruppu seiyura"/>
</div>
<div class="lyrico-lyrics-wrapper">Rasa unna nerungaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rasa unna nerungaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Rusi maarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rusi maarum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mathaaga manasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mathaaga manasa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kadaiyaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kadaiyaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna maaraappil patham paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna maaraappil patham paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sathaaga sakalamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sathaaga sakalamum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thara poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thara poren"/>
</div>
<div class="lyrico-lyrics-wrapper">Metha saappaattil pasi aaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Metha saappaattil pasi aaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Usurae kekka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usurae kekka"/>
</div>
<div class="lyrico-lyrics-wrapper">Una naan thookka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Una naan thookka"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaren maaman koothadikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaren maaman koothadikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lala kada shanthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lala kada shanthi"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna povenae naan yenthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna povenae naan yenthi"/>
</div>
<div class="lyrico-lyrics-wrapper">Lala kada shanthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lala kada shanthi"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaal aanenae naan boonthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaal aanenae naan boonthi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paalum kothichirukkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paalum kothichirukkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paayum virichirukkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paayum virichirukkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanaa athu ethukkunnu theriyaathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanaa athu ethukkunnu theriyaathaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaayum kaninjirukkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaayum kaninjirukkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaayum nananjirukkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaayum nananjirukkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanaa athu unakkunnu puriyaathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanaa athu unakkunnu puriyaathaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un kannaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kannaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Valaviyum odaiyaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valaviyum odaiyaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna kattoda madippenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna kattoda madippenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En kandaangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kandaangi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudavaiyum kasangaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudavaiyum kasangaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoda mallaanthu kedappenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoda mallaanthu kedappenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Udanae vaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udanae vaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ozhungaa thaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ozhungaa thaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Moodi poda ennaathadia
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moodi poda ennaathadia"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lala kada shanthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lala kada shanthi"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna povenae naan yenthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna povenae naan yenthi"/>
</div>
<div class="lyrico-lyrics-wrapper">Lala kada shanthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lala kada shanthi"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaal aanenae naan boonthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaal aanenae naan boonthi"/>
</div>
</pre>
