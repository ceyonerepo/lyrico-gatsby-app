---
title: "poraney poraney song lyrics"
album: "Vaagai Sooda Vaa"
artist: "Ghibran"
lyricist: "Karthik Netha"
director: "A. Sarkunam"
path: "/albums/vaagai-sooda-vaa-lyrics"
song: "Poraney Poraney"
image: ../../images/albumart/vaagai-sooda-vaa.jpg
date: 2011-09-30
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Mk_-E8PksYI"
type: "happy"
singers:
  - Neha Bhasin
  - Naveen Madhav
  - Ranjith
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Poranae poranae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poranae poranae"/>
</div>
<div class="lyrico-lyrics-wrapper">Poranae poranae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poranae poranae"/>
</div>
<div class="lyrico-lyrics-wrapper">kathoda thoothalapola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathoda thoothalapola"/>
</div>
<div class="lyrico-lyrics-wrapper">Poranae poranae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poranae poranae"/>
</div>
<div class="lyrico-lyrics-wrapper">povaamathaan poranae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="povaamathaan poranae"/>
</div>
<div class="lyrico-lyrics-wrapper">Poranae poranae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poranae poranae"/>
</div>
<div class="lyrico-lyrics-wrapper">kathoda thoothalapola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathoda thoothalapola"/>
</div>
<div class="lyrico-lyrics-wrapper">Poranae poranae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poranae poranae"/>
</div>
<div class="lyrico-lyrics-wrapper">povaamathaan poranae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="povaamathaan poranae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhagaai nee neranja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaai nee neranja"/>
</div>
<div class="lyrico-lyrics-wrapper">Adadaa ponthukkul puviyal pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adadaa ponthukkul puviyal pola"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poralae poralae 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poralae poralae "/>
</div>
<div class="lyrico-lyrics-wrapper">kathoda thoothalapola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathoda thoothalapola"/>
</div>
<div class="lyrico-lyrics-wrapper">Poralae poralae 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poralae poralae "/>
</div>
<div class="lyrico-lyrics-wrapper">povaamathaan poralae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="povaamathaan poralae"/>
</div>
<div class="lyrico-lyrics-wrapper">Poralae poralae 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poralae poralae "/>
</div>
<div class="lyrico-lyrics-wrapper">kathoda thoothalapola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathoda thoothalapola"/>
</div>
<div class="lyrico-lyrics-wrapper">Poralae poralae 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poralae poralae "/>
</div>
<div class="lyrico-lyrics-wrapper">povaamathaan poralae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="povaamathaan poralae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paruvam thodangi aasavechen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paruvam thodangi aasavechen"/>
</div>
<div class="lyrico-lyrics-wrapper">Illaatha saamikkum poosavechen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illaatha saamikkum poosavechen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mazhayil nenanja kaatha pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhayil nenanja kaatha pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasa neeyum nenachuputta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasa neeyum nenachuputta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eerakulaaya konjam eraval thaayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eerakulaaya konjam eraval thaayaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna manasa konjam punnaya vaayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna manasa konjam punnaya vaayaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Era eranga paarkum rosakaaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Era eranga paarkum rosakaaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Tea thoolu vasam konda mosakkaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tea thoolu vasam konda mosakkaraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada nellanguruvi onnu manasa manasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada nellanguruvi onnu manasa manasa"/>
</div>
<div class="lyrico-lyrics-wrapper">Siru kannanguliyilae pathukiruchae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru kannanguliyilae pathukiruchae"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinna chinna korathi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna chinna korathi "/>
</div>
<div class="lyrico-lyrics-wrapper">ponnu kannu muliyathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponnu kannu muliyathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Echangaaya aanjiruchae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Echangaaya aanjiruchae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poranae poranae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poranae poranae"/>
</div>
<div class="lyrico-lyrics-wrapper">kathoda thoothalapola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathoda thoothalapola"/>
</div>
<div class="lyrico-lyrics-wrapper">Poranae poranae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poranae poranae"/>
</div>
<div class="lyrico-lyrics-wrapper">povaamathaan poranae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="povaamathaan poranae"/>
</div>
<div class="lyrico-lyrics-wrapper">Poranae poranae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poranae poranae"/>
</div>
<div class="lyrico-lyrics-wrapper">kathoda thoothalapola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathoda thoothalapola"/>
</div>
<div class="lyrico-lyrics-wrapper">Poranae poranae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poranae poranae"/>
</div>
<div class="lyrico-lyrics-wrapper">povaamathaan poranae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="povaamathaan poranae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kennathu nelavaa naa irunden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kennathu nelavaa naa irunden"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalla erinju kolapiputta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalla erinju kolapiputta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unna paarthu pesayila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna paarthu pesayila"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendaam muraiyaa kuththa vechen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendaam muraiyaa kuththa vechen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mookana kowna pola un nenappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mookana kowna pola un nenappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheembalu vaasam pola un siripu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheembalu vaasam pola un siripu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adakaakum kozhi pola en thavippu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adakaakum kozhi pola en thavippu"/>
</div>
<div class="lyrico-lyrics-wrapper">Posukinnu poothirukae en pozhappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Posukinnu poothirukae en pozhappu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi manja kezhangae 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi manja kezhangae "/>
</div>
<div class="lyrico-lyrics-wrapper">unna nenachi nenachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna nenachi nenachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thenam manasukulla 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thenam manasukulla "/>
</div>
<div class="lyrico-lyrics-wrapper">vechi pootikitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vechi pootikitten"/>
</div>
<div class="lyrico-lyrics-wrapper">Un pinju viral pathicha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un pinju viral pathicha "/>
</div>
<div class="lyrico-lyrics-wrapper">manna eduthu naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manna eduthu naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaayathukku poosikiten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaayathukku poosikiten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poralae poralae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poralae poralae"/>
</div>
<div class="lyrico-lyrics-wrapper">Poralae Poralae 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poralae Poralae "/>
</div>
<div class="lyrico-lyrics-wrapper">povaamathaan poralae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="povaamathaan poralae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhagaai nee neranja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaai nee neranja"/>
</div>
<div class="lyrico-lyrics-wrapper">Adadaa ponthukkul puviyal pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adadaa ponthukkul puviyal pola"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poranae poranae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poranae poranae"/>
</div>
<div class="lyrico-lyrics-wrapper">kathoda thoothalapola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathoda thoothalapola"/>
</div>
<div class="lyrico-lyrics-wrapper">Poranae poranae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poranae poranae"/>
</div>
<div class="lyrico-lyrics-wrapper">povaamathaan poranae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="povaamathaan poranae"/>
</div>
<div class="lyrico-lyrics-wrapper">Poranae poranae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poranae poranae"/>
</div>
<div class="lyrico-lyrics-wrapper">kathoda thoothalapola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathoda thoothalapola"/>
</div>
<div class="lyrico-lyrics-wrapper">Poranae poranae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poranae poranae"/>
</div>
<div class="lyrico-lyrics-wrapper">povaamathaan poranae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="povaamathaan poranae"/>
</div>
</pre>
