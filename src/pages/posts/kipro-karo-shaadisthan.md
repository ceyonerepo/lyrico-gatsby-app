---
title: "kirpa karo song lyrics"
album: "Shaadisthan"
artist: "Nakul Sharma - Sahil Bhatia"
lyricist: "Amir Khusrau"
director: "Raj Singh Chaudhary"
path: "/albums/shaadisthan-lyrics"
song: "Kirpa Karo"
image: ../../images/albumart/shaadisthan.jpg
date: 2021-06-11
lang: hindi
youtubeLink: "https://www.youtube.com/embed/1kfo9JsK8TM"
type: "happy"
singers:
  - Swaroop Khan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Khwaja Ji Maharaja Ji
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khwaja Ji Maharaja Ji"/>
</div>
<div class="lyrico-lyrics-wrapper">Khwaja Ji Maharaja Ji
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khwaja Ji Maharaja Ji"/>
</div>
<div class="lyrico-lyrics-wrapper">Tum Bado Garib Nawaz
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tum Bado Garib Nawaz"/>
</div>
<div class="lyrico-lyrics-wrapper">Apna Kar Ke Rakhiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Apna Kar Ke Rakhiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Tohe Baahein Pakde Ki Raaz
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tohe Baahein Pakde Ki Raaz"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Khwaja E Khwajiga Moumuddi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khwaja E Khwajiga Moumuddi"/>
</div>
<div class="lyrico-lyrics-wrapper">Fakre Ko No Makam Muimuddi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fakre Ko No Makam Muimuddi"/>
</div>
<div class="lyrico-lyrics-wrapper">Murshde Rehnuma E Ehle Safa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Murshde Rehnuma E Ehle Safa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hadiyeh In Sujam Muimuddi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hadiyeh In Sujam Muimuddi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kripa Karo Maharaj Muimuddi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kripa Karo Maharaj Muimuddi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kripa Karo Maharaj
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kripa Karo Maharaj"/>
</div>
<div class="lyrico-lyrics-wrapper">Kripa Karo Maharaj Muimuddi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kripa Karo Maharaj Muimuddi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kripa Karo Maharaj
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kripa Karo Maharaj"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho Garib Nawaz Garib Nawaz
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho Garib Nawaz Garib Nawaz"/>
</div>
<div class="lyrico-lyrics-wrapper">Garib Nawaz Garib Nawaz
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Garib Nawaz Garib Nawaz"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho Kripa Karo Maharaj Muimuddi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho Kripa Karo Maharaj Muimuddi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kripa Karo Maharaj Muimuddi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kripa Karo Maharaj Muimuddi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kripa Karo Maharaj
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kripa Karo Maharaj"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho Garib Nawaz
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho Garib Nawaz"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tumhari Daya Ki Sun Ke Sabriyaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tumhari Daya Ki Sun Ke Sabriyaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Aan Padi Ajj Meri Nagriyaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aan Padi Ajj Meri Nagriyaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Sish Jukayu Me Chumu Dehlayiaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sish Jukayu Me Chumu Dehlayiaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Sish Jukayu Me Chumu Dehlayiaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sish Jukayu Me Chumu Dehlayiaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Chaha Karu Raaj Muimuddi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chaha Karu Raaj Muimuddi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kripa Karo Maharaj Muimuddi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kripa Karo Maharaj Muimuddi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kripa Karo Maharaj
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kripa Karo Maharaj"/>
</div>
<div class="lyrico-lyrics-wrapper">Kripa Karo Maharaj Muimuddi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kripa Karo Maharaj Muimuddi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kripa Karo Maharaj
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kripa Karo Maharaj"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kripa Karo Maharaj Muimuddi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kripa Karo Maharaj Muimuddi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kripa Karo Maharaj
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kripa Karo Maharaj"/>
</div>
<div class="lyrico-lyrics-wrapper">Kripa Karo Maharaj Muimuddi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kripa Karo Maharaj Muimuddi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kripa Karo Maharaj
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kripa Karo Maharaj"/>
</div>
</pre>
