---
title: "enna aachi song lyrics"
album: "Vedi"
artist: "Vijay Antony"
lyricist: "Thamarai"
director: "Prabhu Deva"
path: "/albums/vedi-lyrics"
song: "Enna Aachi"
image: ../../images/albumart/vedi.jpg
date: 2011-09-30
lang: tamil
youtubeLink: "https://www.youtube.com/embed/T5ZSjrSvWFY"
type: "love"
singers:
  - Vijay Yesudas
  - Janaki Iyer
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Enna Aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Aachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennakenna Aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennakenna Aachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Engumae Unmugam Paarkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engumae Unmugam Paarkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Aachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennakenna Aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennakenna Aachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mounathil Un Kural Ketkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mounathil Un Kural Ketkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Vaanilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Vaanilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vennila Un Mugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vennila Un Mugam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaraamalae Pesudhae Ennidam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaraamalae Pesudhae Ennidam"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Kaadhala Kaadhala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Kaadhala Kaadhala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Aachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennakenna Aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennakenna Aachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Engumae Unmugam Paarkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engumae Unmugam Paarkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rathirigal Neelam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rathirigal Neelam"/>
</div>
<div class="lyrico-lyrics-wrapper">Rathi Dhevi Madhan Kolam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rathi Dhevi Madhan Kolam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavaaga Dhinam Thorum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavaaga Dhinam Thorum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vara Kandenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vara Kandenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saalaigalin Oram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saalaigalin Oram"/>
</div>
<div class="lyrico-lyrics-wrapper">Nizhal Thedum Veyil Neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nizhal Thedum Veyil Neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Thodapaarkum Siru Kaatraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodapaarkum Siru Kaatraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Kandenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Kandenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pudhai Mannilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhai Mannilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalai Vaithen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalai Vaithen"/>
</div>
<div class="lyrico-lyrics-wrapper">Nagakannilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nagakannilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oosi Thaithen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oosi Thaithen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Padum Vedhanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padum Vedhanai"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollum Kaadhalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollum Kaadhalai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Aachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennakenna Aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennakenna Aachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Engumae Unmugam Paarkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engumae Unmugam Paarkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veedu Varai Sendren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veedu Varai Sendren"/>
</div>
<div class="lyrico-lyrics-wrapper">Padi Yeravillai Nindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padi Yeravillai Nindren"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Thedi Varuvaayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Thedi Varuvaayo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ena Paarthenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ena Paarthenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paadam Padikaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadam Padikaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir Thozhi Pidikaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Thozhi Pidikaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nagaraadha Kedikaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nagaraadha Kedikaaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhai Paarthenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai Paarthenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nila Aandugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nila Aandugal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nooru Vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nooru Vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhae Polavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhae Polavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazha Vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazha Vendum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Udal Ennidam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udal Ennidam"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir Unnidam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Unnidam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Aachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennakenna Aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennakenna Aachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Engumae Unmugam Paarkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engumae Unmugam Paarkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Aachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennakenna Aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennakenna Aachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mounathil Un Kural Ketkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mounathil Un Kural Ketkiren"/>
</div>
</pre>
