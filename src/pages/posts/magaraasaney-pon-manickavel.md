---
title: "magaraasaney song lyrics"
album: "Pon Manickavel"
artist: "D. Imman"
lyricist: "Viveka"
director: "A.C. Mugil Chellappan"
path: "/albums/pon-manickavel-lyrics"
song: "Magaraasaney"
image: ../../images/albumart/pon-manickavel.jpg
date: 2021-11-19
lang: tamil
youtubeLink: "https://www.youtube.com/embed/F42jOwFTPZQ"
type: "melody"
singers:
  - Varsha Renjith
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Thaalo Thalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaalo Thalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaalo Thalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaalo Thalo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Magaarasanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Magaarasanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Manjal Vanna Poovae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manjal Vanna Poovae"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethaan Thangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethaan Thangam"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethan Thangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethan Thangam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Paarvaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Paarvaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullam Vellam Aagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullam Vellam Aagum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neethan Thangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethan Thangam"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethan Thangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethan Thangam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pinniravu Kadanthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pinniravu Kadanthum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Yenguven Thoongaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Yenguven Thoongaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vennilavum Paarkkumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vennilavum Paarkkumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaitha Kann Vaangaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaitha Kann Vaangaamal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Nizhalil Aayulai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Nizhalil Aayulai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhavae Peraasai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhavae Peraasai"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Vittu Neenginaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Vittu Neenginaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Oiyumae Uyirosai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oiyumae Uyirosai"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyae Ennuyir Aanaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyae Ennuyir Aanaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Magaarasanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Magaarasanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Manjal Vanna Poovae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manjal Vanna Poovae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neethaan Thangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethaan Thangam"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethan Thangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethan Thangam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Paarvaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Paarvaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullam Vellam Aagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullam Vellam Aagum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neethan Thangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethan Thangam"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethan Thangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethan Thangam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oviyangal Ellaam Vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oviyangal Ellaam Vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnidam Nindroru Paadam Ketkkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnidam Nindroru Paadam Ketkkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Poovinangal Ellaam Koodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovinangal Ellaam Koodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Unn Ezhil Poloru Poovai Pookkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unn Ezhil Poloru Poovai Pookkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maayaajaalam Kaattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maayaajaalam Kaattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan Kannalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan Kannalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaayai Naanum Maari Vandhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaayai Naanum Maari Vandhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Pinnalae Pinnalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pinnalae Pinnalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Mugamthaan Aiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Mugamthaan Aiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjinil Aalamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjinil Aalamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnidam Pesiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnidam Pesiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalangal Poguma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalangal Poguma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Sandhana Kaiviral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Sandhana Kaiviral"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanthidum Mannumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanthidum Mannumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thangam Endragum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangam Endragum"/>
</div>
<div class="lyrico-lyrics-wrapper">Manthiramaayamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manthiramaayamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Magaarasanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Magaarasanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Manjal Vanna Poovae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manjal Vanna Poovae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neethaan Thangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethaan Thangam"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethan Thangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethan Thangam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Paarvaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Paarvaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullam Vellam Aagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullam Vellam Aagum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neethan Thangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethan Thangam"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethan Thangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethan Thangam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pinniravu Kadanthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pinniravu Kadanthum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Yenguven Thoongaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Yenguven Thoongaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vennilavum Paarkkumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vennilavum Paarkkumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaitha Kann Vaangaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaitha Kann Vaangaamal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Nizhalil Aayulai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Nizhalil Aayulai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhavae Peraasai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhavae Peraasai"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Vittu Neenginaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Vittu Neenginaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Oiyumae Uyirosai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oiyumae Uyirosai"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyae Ennuyir Aanaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyae Ennuyir Aanaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Magaarasanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Magaarasanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Manjal Vanna Poovae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manjal Vanna Poovae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neethaan Thangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethaan Thangam"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethan Thangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethan Thangam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Paarvaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Paarvaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullam Vellam Aagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullam Vellam Aagum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neethan Thangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethan Thangam"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethan Thangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethan Thangam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neethan Thangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethan Thangam"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethan Thangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethan Thangam"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethan Thangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethan Thangam"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethan Thangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethan Thangam"/>
</div>
</pre>
