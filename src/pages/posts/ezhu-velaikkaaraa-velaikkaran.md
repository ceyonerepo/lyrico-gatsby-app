---
title: "ezhu velaikkaaraa song lyrics"
album: "Velaikkaran"
artist: "Anirudh Ravichander"
lyricist: "Viveka"
director: "Mohan Raja"
path: "/albums/velaikkaran-lyrics"
song: "Ezhu Velaikkaaraa"
image: ../../images/albumart/velaikkaran.jpg
date: 2017-12-22
lang: tamil
youtubeLink: "https://www.youtube.com/embed/3SnFtjCp4Ew"
type: "Motivational"
singers:
  - Siddharth Mahadevan
  - Chorus
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ezhu Velaikkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhu Velaikkara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ezhu Velaikkara Indrey Indrey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhu Velaikkara Indrey Indrey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oyaadhey Saayadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oyaadhey Saayadhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaai Moodi Vaazhadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaai Moodi Vaazhadhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ezhu Velaikkara Indrey Indrey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhu Velaikkara Indrey Indrey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ini Seiyum Velai Nandrae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Seiyum Velai Nandrae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Melum Keezhum Ondrey Ondrey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Melum Keezhum Ondrey Ondrey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varalaarai Maatru Vendrey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varalaarai Maatru Vendrey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vervai Theeye Dhesam Neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vervai Theeye Dhesam Neeye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Sol Kette Veesum Un Kaatre
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Sol Kette Veesum Un Kaatre"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poradu Oyaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poradu Oyaadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theyaadhey Saayaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theyaadhey Saayaadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaraadhey Soraadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraadhey Soraadhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veezhaaadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veezhaaadhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poraadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ezhu Velaikkara Indrae Indrae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhu Velaikkara Indrae Indrae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ini Seiyum Velai Nandrey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Seiyum Velai Nandrey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Melum Keezhum Ondrey Ondrey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Melum Keezhum Ondrey Ondrey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varalaarai Maatru Vendre
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varalaarai Maatru Vendre"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poraadu Poraadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraadu Poraadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poraadu Poraadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraadu Poraadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mudiyaatha Seyal Yethumey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudiyaatha Seyal Yethumey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Puvi Meedhu Kidaiyaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puvi Meedhu Kidaiyaadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ezhundhu Vaa Puyalai Pole
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhundhu Vaa Puyalai Pole"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Balam Enna Puriyaamaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Balam Enna Puriyaamaley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Panindhomey Kunindhomey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panindhomey Kunindhomey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nimirndhu Vaa Melae Melae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nimirndhu Vaa Melae Melae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Muraiye Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Muraiye Hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tharaiyinil Vaazhum Vaaippu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharaiyinil Vaazhum Vaaippu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adai Muraiye Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adai Muraiye Hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Payanura Vaazhum Vaazhkkai Aakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Payanura Vaazhum Vaazhkkai Aakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uzhaippavaney Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uzhaippavaney Hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ezhudhida Vendum Theerppu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhudhida Vendum Theerppu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidhaithavaney Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidhaithavaney Hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pasiyena Ponaal Engo Thappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pasiyena Ponaal Engo Thappu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oyaadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oyaadhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theyaadhey Saayaadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theyaadhey Saayaadhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaraadhey Soraadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraadhey Soraadhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veezhaaadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veezhaaadhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Porraddu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porraddu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ezhu Velaikkara Indrey Indrey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhu Velaikkara Indrey Indrey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ini Seiyum Velai Nandrey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Seiyum Velai Nandrey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Melum Keelum Ondrey Ondrey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Melum Keelum Ondrey Ondrey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varalaarai Maatru Vendrey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varalaarai Maatru Vendrey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vervai Theeye Desam Neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vervai Theeye Desam Neeye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Sol Kettey Veesum Un Kaatrey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Sol Kettey Veesum Un Kaatrey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poraaduu Oyaadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraaduu Oyaadhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theyaadhey Saayaadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theyaadhey Saayaadhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaradhey Soradhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaradhey Soradhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veezhaaadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veezhaaadhey"/>
</div>
</pre>
