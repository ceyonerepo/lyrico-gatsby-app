---
title: "saami saami song lyrics"
album: "Pushpa The Rise"
artist: "Devi Sri Prasad"
lyricist: "Viveka"
director: "Sukumar"
path: "/albums/pushpa-the-rise-song-lyrics"
song: "Saami Saami"
image: ../../images/albumart/pushpa-the-rise.jpg
date: 2021-12-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/BA3D02JNd0E"
type: "love"
singers:
  - 	Rajalakshmi Senthilganesh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nee Ammu Ammu Sollayile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Ammu Ammu Sollayile"/>
</div>
<div class="lyrico-lyrics-wrapper">Podaatiyaa Poorikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podaatiyaa Poorikkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Saami En Sami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saami En Sami"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa Saami Saami Solla Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Saami Saami Solla Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Purushanaana Feeling-U Than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Purushanaana Feeling-U Than"/>
</div>
<div class="lyrico-lyrics-wrapper">Saami En Saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saami En Saami"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Yedhira Yedhira Nadakkayila Aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Yedhira Yedhira Nadakkayila Aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Yedhira Yedhira Nadakkayilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Yedhira Yedhira Nadakkayilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yezhumalayaan Darisanam Da Saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yezhumalayaan Darisanam Da Saami"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Pakkam Pakkam Ninna Andha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Pakkam Pakkam Ninna Andha"/>
</div>
<div class="lyrico-lyrics-wrapper">Parameswaran Polo Thona Saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parameswaran Polo Thona Saami"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Illaama Naan Pogum Paadhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Illaama Naan Pogum Paadhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallum Mullum Kuththudhuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallum Mullum Kuththudhuda"/>
</div>
<div class="lyrico-lyrics-wrapper">Saami En Saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saami En Saami"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Saami Vaayya Saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Saami Vaayya Saami"/>
</div>
<div class="lyrico-lyrics-wrapper">Manmadha Saami Mandhira Saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manmadha Saami Mandhira Saami"/>
</div>
<div class="lyrico-lyrics-wrapper">Pokkiri Saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pokkiri Saami"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Saami Saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Saami Saami"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaayya Saami Saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaayya Saami Saami"/>
</div>
<div class="lyrico-lyrics-wrapper">Manmadha Saami Mandhira Saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manmadha Saami Mandhira Saami"/>
</div>
<div class="lyrico-lyrics-wrapper">Pokkiri Saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pokkiri Saami"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lungi-ya Yaeththi Katti Ee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lungi-ya Yaeththi Katti Ee"/>
</div>
<div class="lyrico-lyrics-wrapper">Local-aa Nadakkayila Aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Local-aa Nadakkayila Aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Lungi Ya Yaeththi Katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lungi Ya Yaeththi Katti"/>
</div>
<div class="lyrico-lyrics-wrapper">Local Aa Nadakkayila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Local Aa Nadakkayila"/>
</div>
<div class="lyrico-lyrics-wrapper">Angame Athiruthada Saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Angame Athiruthada Saami"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaampukilli Veththalayila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaampukilli Veththalayila"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadichu Ne Kodukkayila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadichu Ne Kodukkayila"/>
</div>
<div class="lyrico-lyrics-wrapper">En Udambu Sevvakkudhuda Saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Udambu Sevvakkudhuda Saami"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Katta Kurala Kaekkayila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Katta Kurala Kaekkayila"/>
</div>
<div class="lyrico-lyrics-wrapper">Ae Ae Ae Ae AeAe Ae Ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Ae Ae Ae AeAe Ae Ae"/>
</div>
<div class="lyrico-lyrics-wrapper">En Katta Thulludhu Saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Katta Thulludhu Saami"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Satta Button Aa Avuthutha Vitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Satta Button Aa Avuthutha Vitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarakku Bodha Saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarakku Bodha Saami"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rendu Gundu Kannayum Sozhattumbodhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendu Gundu Kannayum Sozhattumbodhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanduvadaththula Nandu Maeyudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanduvadaththula Nandu Maeyudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Saami En Saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saami En Saami"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Saami Vaayya Saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Saami Vaayya Saami"/>
</div>
<div class="lyrico-lyrics-wrapper">Manmadha Saami Mandhira Saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manmadha Saami Mandhira Saami"/>
</div>
<div class="lyrico-lyrics-wrapper">Pokkiri Saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pokkiri Saami"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Saami Saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Saami Saami"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaayya Saami Saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaayya Saami Saami"/>
</div>
<div class="lyrico-lyrics-wrapper">Manmadha Saami Mandhira Saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manmadha Saami Mandhira Saami"/>
</div>
<div class="lyrico-lyrics-wrapper">Pokkiri Saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pokkiri Saami"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pudhu Sela Katti Vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu Sela Katti Vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pugazhndhu Nee Sollalanaa Aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pugazhndhu Nee Sollalanaa Aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu Sela Katti Vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu Sela Katti Vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pugazhndhu Nee Sollalana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pugazhndhu Nee Sollalana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Selaikkaana Selavu Waste-u Saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Selaikkaana Selavu Waste-u Saami"/>
</div>
<div class="lyrico-lyrics-wrapper">Koonthalila Sirikkum Poova
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koonthalila Sirikkum Poova"/>
</div>
<div class="lyrico-lyrics-wrapper">Kojam Neeyum Paakkalana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kojam Neeyum Paakkalana"/>
</div>
<div class="lyrico-lyrics-wrapper">Poo Manasu Puzhingee Pogum Saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poo Manasu Puzhingee Pogum Saami"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Oram Jaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Oram Jaaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Theriyum Azhaga Aa AaAa Aa Aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theriyum Azhaga Aa AaAa Aa Aa"/>
</div>
<div class="lyrico-lyrics-wrapper">En Oram Jaaram Theriyum Azhaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Oram Jaaram Theriyum Azhaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Uththu Paaaru Saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uththu Paaaru Saami"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Uththu Paakkalnna Manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Uththu Paakkalnna Manam"/>
</div>
<div class="lyrico-lyrics-wrapper">Seththu Pogum Saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seththu Pogum Saami"/>
</div>
<div class="lyrico-lyrics-wrapper">En Aththana Azhagu Nee Illana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Aththana Azhagu Nee Illana"/>
</div>
<div class="lyrico-lyrics-wrapper">Aathula Karacha Perungaayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aathula Karacha Perungaayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Saami… En Saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saami… En Saami"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Saami Vaayya Saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Saami Vaayya Saami"/>
</div>
<div class="lyrico-lyrics-wrapper">Manmadha Saami Mandhira Saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manmadha Saami Mandhira Saami"/>
</div>
<div class="lyrico-lyrics-wrapper">Pokkiri Saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pokkiri Saami"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Saami Saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Saami Saami"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaayya Saami Saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaayya Saami Saami"/>
</div>
<div class="lyrico-lyrics-wrapper">Manmadha Saami Mandhira Saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manmadha Saami Mandhira Saami"/>
</div>
<div class="lyrico-lyrics-wrapper">Pokkiri Saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pokkiri Saami"/>
</div>
</pre>
