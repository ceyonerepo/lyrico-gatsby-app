---
title: "boomi thaduthalum song lyrics"
album: "Karungali"
artist: "Srikanth Deva"
lyricist: "Erodu Iraivan"
director: "Kalanjiyam"
path: "/albums/karungali-lyrics"
song: "Boomi Thaduthalum"
image: ../../images/albumart/karungali.jpg
date: 2011-07-29
lang: tamil
youtubeLink: "https://www.youtube.com/embed/eDOijcpIJIU"
type: "love"
singers:
  - Chinmayi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Boomi thaduthaalum saami thaduthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boomi thaduthaalum saami thaduthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">unna vittu ini naan vilagamaatten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna vittu ini naan vilagamaatten"/>
</div>
<div class="lyrico-lyrics-wrapper">andam odinjaalum aagaayam sarinjaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="andam odinjaalum aagaayam sarinjaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">unna vittu ini naan poagamaatten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna vittu ini naan poagamaatten"/>
</div>
<div class="lyrico-lyrics-wrapper">nilavukku vaanamundu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nilavukku vaanamundu"/>
</div>
<div class="lyrico-lyrics-wrapper">neerukku boomi undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neerukku boomi undu"/>
</div>
<div class="lyrico-lyrics-wrapper">kadalukku karaim undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadalukku karaim undu"/>
</div>
<div class="lyrico-lyrics-wrapper">enakkena yaarirukka unna vita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enakkena yaarirukka unna vita"/>
</div>
<div class="lyrico-lyrics-wrapper">enakkena yaarirukkaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enakkena yaarirukkaa"/>
</div>
<div class="lyrico-lyrics-wrapper">thiriya vilagi vilakku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thiriya vilagi vilakku "/>
</div>
<div class="lyrico-lyrics-wrapper">velicham thandhirukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="velicham thandhirukka"/>
</div>
<div class="lyrico-lyrics-wrapper">usurathalli odambu mattum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="usurathalli odambu mattum "/>
</div>
<div class="lyrico-lyrics-wrapper">vaazhndha kadhaiyirukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaazhndha kadhaiyirukka"/>
</div>
<div class="lyrico-lyrics-wrapper">uyir poala naan serndhiruppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyir poala naan serndhiruppen"/>
</div>
<div class="lyrico-lyrics-wrapper">unakkaaga kadaisivarai kaathirupen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unakkaaga kadaisivarai kaathirupen"/>
</div>
</pre>
