---
title: 'ajukku gumukku song lyrics'
album: 'Naan Sirithal'
artist: 'Hiphop Tamizha'
lyricist: 'Kabilan Vairamuthu'
director: 'Raana'
path: '/albums/naan-sirithal-song-lyrics'
song: 'Ajukku Gumukku'
image: ../../images/albumart/naan-sirithal.jpg  
date: 2020-03-02
lang: tamil
singers: 
- Hiphop Tamizha
youtubeLink: "https://www.youtube.com/embed/ji55jouN_8k"
type: 'love'
---

<pre class="lyrics-native">
    Coming Soon
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Idli-kku meen kuzhambu
<input type="checkbox" class="lyrico-select-lyric-line" value="Idli-kku meen kuzhambu"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayathil nee…
<input type="checkbox" class="lyrico-select-lyric-line" value="Idhayathil nee…"/>
</div>
<div class="lyrico-lyrics-wrapper">Salad-kku broccoli
<input type="checkbox" class="lyrico-select-lyric-line" value="Salad-kku broccoli"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhalukku nee…
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaadhalukku nee…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Nooru vaga barotta
<input type="checkbox" class="lyrico-select-lyric-line" value="Nooru vaga barotta"/>
</div>
  <div class="lyrico-lyrics-wrapper">Haan haan
<input type="checkbox" class="lyrico-select-lyric-line" value="Haan haan"/>
</div>
  <div class="lyrico-lyrics-wrapper">Oorukulla kaattata
<input type="checkbox" class="lyrico-select-lyric-line" value="Oorukulla kaattata"/>
</div>
  <div class="lyrico-lyrics-wrapper">Heyy heyy
<input type="checkbox" class="lyrico-select-lyric-line" value="Heyy heyy"/>
</div>
  <div class="lyrico-lyrics-wrapper">Nandu kari samosa
<input type="checkbox" class="lyrico-select-lyric-line" value="Nandu kari samosa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooti vidatta
<input type="checkbox" class="lyrico-select-lyric-line" value="Ooti vidatta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Car-u-kulla ukkanthu
<input type="checkbox" class="lyrico-select-lyric-line" value="Car-u-kulla ukkanthu"/>
</div>
  <div class="lyrico-lyrics-wrapper">Haan haan
<input type="checkbox" class="lyrico-select-lyric-line" value="Haan haan"/>
</div>
  <div class="lyrico-lyrics-wrapper">Kaapi thanni andaama
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaapi thanni andaama"/>
</div>
  <div class="lyrico-lyrics-wrapper">Heyy heyy
<input type="checkbox" class="lyrico-select-lyric-line" value="Heyy heyy"/>
</div>
  <div class="lyrico-lyrics-wrapper">Roadu kada soup-ah thaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Roadu kada soup-ah thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Alli vaaratta
<input type="checkbox" class="lyrico-select-lyric-line" value="Alli vaaratta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Namma gandhi thatha
<input type="checkbox" class="lyrico-select-lyric-line" value="Namma gandhi thatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Siripirikkuthu rooba note-la
<input type="checkbox" class="lyrico-select-lyric-line" value="Siripirikkuthu rooba note-la"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha gandhi paiyan siripirikkuthu
<input type="checkbox" class="lyrico-select-lyric-line" value="Indha gandhi paiyan siripirikkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un heart beat-la
<input type="checkbox" class="lyrico-select-lyric-line" value="Un heart beat-la"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Gandhi thatha siripirikkuthu
<input type="checkbox" class="lyrico-select-lyric-line" value="Gandhi thatha siripirikkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Rooba note-la
<input type="checkbox" class="lyrico-select-lyric-line" value="Rooba note-la"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha gandhi paiyan siripirikkuthu
<input type="checkbox" class="lyrico-select-lyric-line" value="Indha gandhi paiyan siripirikkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un heart beat-la
<input type="checkbox" class="lyrico-select-lyric-line" value="Un heart beat-la"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ajukku ajukku gumukku gumukku…
<input type="checkbox" class="lyrico-select-lyric-line" value="Ajukku ajukku gumukku gumukku…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vaa…
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaa…"/>
</div>
  <div class="lyrico-lyrics-wrapper">Ennai pinjula konjana vanjara
<input type="checkbox" class="lyrico-select-lyric-line" value="Ennai pinjula konjana vanjara"/>
</div>
<div class="lyrico-lyrics-wrapper">Jananga vanthurukayinga
<input type="checkbox" class="lyrico-select-lyric-line" value="Jananga vanthurukayinga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Yen…
<input type="checkbox" class="lyrico-select-lyric-line" value="Yen…"/>
</div>
  <div class="lyrico-lyrics-wrapper">En nenjula nozhanja
<input type="checkbox" class="lyrico-select-lyric-line" value="En nenjula nozhanja"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanjiya kanduka thaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Vanjiya kanduka thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vaa…
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaa…"/>
</div>
  <div class="lyrico-lyrics-wrapper">Namma thathakka buthakka
<input type="checkbox" class="lyrico-select-lyric-line" value="Namma thathakka buthakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Nethili pullainga vanthurukayinga
<input type="checkbox" class="lyrico-select-lyric-line" value="Nethili pullainga vanthurukayinga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Yen…
<input type="checkbox" class="lyrico-select-lyric-line" value="Yen…"/>
</div>
<div class="lyrico-lyrics-wrapper">En buthiyil puguntha
<input type="checkbox" class="lyrico-select-lyric-line" value="En buthiyil puguntha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thathaya kandukka thaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Thathaya kandukka thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">En kaalam enakku kaadhal bajjiya
<input type="checkbox" class="lyrico-select-lyric-line" value="En kaalam enakku kaadhal bajjiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Pottu thanthurucha
<input type="checkbox" class="lyrico-select-lyric-line" value="Pottu thanthurucha"/>
</div>
<div class="lyrico-lyrics-wrapper">En peepi manasu happy aagi
<input type="checkbox" class="lyrico-select-lyric-line" value="En peepi manasu happy aagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Paatu vanthurucha
<input type="checkbox" class="lyrico-select-lyric-line" value="Paatu vanthurucha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ah ajukku ajukku
<input type="checkbox" class="lyrico-select-lyric-line" value="Ah ajukku ajukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Gumukku gumukku thaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Gumukku gumukku thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava kaathula thandha kiss-il
<input type="checkbox" class="lyrico-select-lyric-line" value="Ava kaathula thandha kiss-il"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku kirukku pudichiricha
<input type="checkbox" class="lyrico-select-lyric-line" value="Enakku kirukku pudichiricha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ajukku ajukku gumukku gumukku…
<input type="checkbox" class="lyrico-select-lyric-line" value="Ajukku ajukku gumukku gumukku…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Hey soap-u kuchi gap-u-kulla
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey soap-u kuchi gap-u-kulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Nooru bubble oothutaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Nooru bubble oothutaa"/>
</div>
  <div class="lyrico-lyrics-wrapper">Ahaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Ahaan"/>
</div>
  <div class="lyrico-lyrics-wrapper">Oodhi varum bubble kulla
<input type="checkbox" class="lyrico-select-lyric-line" value="Oodhi varum bubble kulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Baby mogatha kaattata
<input type="checkbox" class="lyrico-select-lyric-line" value="Baby mogatha kaattata"/>
</div>
  <div class="lyrico-lyrics-wrapper">Hoo hoo
<input type="checkbox" class="lyrico-select-lyric-line" value="Hoo hoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vanna vanna raatinathil
<input type="checkbox" class="lyrico-select-lyric-line" value="Vanna vanna raatinathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai vechu suththatha
<input type="checkbox" class="lyrico-select-lyric-line" value="Unnai vechu suththatha"/>
</div>
  <div class="lyrico-lyrics-wrapper">Ahaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Ahaan"/>
</div>
  <div class="lyrico-lyrics-wrapper">Dinner-ku sundari akka
<input type="checkbox" class="lyrico-select-lyric-line" value="Dinner-ku sundari akka"/>
</div>
<div class="lyrico-lyrics-wrapper">Desserttuku more thaatha
<input type="checkbox" class="lyrico-select-lyric-line" value="Desserttuku more thaatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Sirikum singaaram
<input type="checkbox" class="lyrico-select-lyric-line" value="Sirikum singaaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Iva minikku oyyaaram
<input type="checkbox" class="lyrico-select-lyric-line" value="Iva minikku oyyaaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Oyyaaram
<input type="checkbox" class="lyrico-select-lyric-line" value="Oyyaaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Neruppu karpooram
<input type="checkbox" class="lyrico-select-lyric-line" value="Neruppu karpooram"/>
</div>
<div class="lyrico-lyrics-wrapper">Iva narukku nangooram
<input type="checkbox" class="lyrico-select-lyric-line" value="Iva narukku nangooram"/>
</div>
<div class="lyrico-lyrics-wrapper">Nangooram
<input type="checkbox" class="lyrico-select-lyric-line" value="Nangooram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ah ajukku ajukku
<input type="checkbox" class="lyrico-select-lyric-line" value="Ah ajukku ajukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Gumukku gumukku thaan…
<input type="checkbox" class="lyrico-select-lyric-line" value="Gumukku gumukku thaan…"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee ennikkumae ennikkumae
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee ennikkumae ennikkumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennakku ennakku thaan…
<input type="checkbox" class="lyrico-select-lyric-line" value="Ennakku ennakku thaan…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Namma gandhi thatha
<input type="checkbox" class="lyrico-select-lyric-line" value="Namma gandhi thatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Siripirikkuthu rooba note-la
<input type="checkbox" class="lyrico-select-lyric-line" value="Siripirikkuthu rooba note-la"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha gandhi paiyan siripirikkuthu
<input type="checkbox" class="lyrico-select-lyric-line" value="Indha gandhi paiyan siripirikkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un heart beat-la
<input type="checkbox" class="lyrico-select-lyric-line" value="Un heart beat-la"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Gandhi thatha siripirikkuthu
<input type="checkbox" class="lyrico-select-lyric-line" value="Gandhi thatha siripirikkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Rooba note-la
<input type="checkbox" class="lyrico-select-lyric-line" value="Rooba note-la"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha gandhi paiyan siripirikkuthu
<input type="checkbox" class="lyrico-select-lyric-line" value="Indha gandhi paiyan siripirikkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un heart beat-la
<input type="checkbox" class="lyrico-select-lyric-line" value="Un heart beat-la"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ajukku ajukku gumukku gumukku…
<input type="checkbox" class="lyrico-select-lyric-line" value="Ajukku ajukku gumukku gumukku…"/>
</div>
</pre>