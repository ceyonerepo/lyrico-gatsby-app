---
title: "last bench song lyrics"
album: "Kirrak Party"
artist: "B Ajaneesh Loknath"
lyricist: "Ramajogayya Sastry"
director: "Sharan Koppisetty"
path: "/albums/kirrak-party-lyrics"
song: "Last Bench"
image: ../../images/albumart/kirrak-party.jpg
date: 2018-03-16
lang: telugu
youtubeLink: "https://www.youtube.com/embed/R-5RBhqngUs"
type: "happy"
singers:
  -	Shashank Sheshagiri
  - Chintan Vikas
  - Varun Ramachandra
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Last bench ro party symbolu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Last bench ro party symbolu"/>
</div>
<div class="lyrico-lyrics-wrapper">Veseddam chalo wallu posteru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veseddam chalo wallu posteru"/>
</div>
<div class="lyrico-lyrics-wrapper">Geliche gang idhi collarlu ettharo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Geliche gang idhi collarlu ettharo"/>
</div>
<div class="lyrico-lyrics-wrapper">Campus motthamu campaign cheyyaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Campus motthamu campaign cheyyaro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dong daga daga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dong daga daga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ik daga ik daga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ik daga ik daga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ding dongu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ding dongu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dong daga daga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dong daga daga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ik daga ik daga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ik daga ik daga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ding dongu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ding dongu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Classroom lopale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Classroom lopale"/>
</div>
<div class="lyrico-lyrics-wrapper">Meeting pettaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meeting pettaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Prathi oka studentu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prathi oka studentu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gunde kollagottaroo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gunde kollagottaroo"/>
</div>
<div class="lyrico-lyrics-wrapper">Cigarette dhammu break lona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cigarette dhammu break lona"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaaka canteenlo chai thona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaka canteenlo chai thona"/>
</div>
<div class="lyrico-lyrics-wrapper">Full to recharge ay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Full to recharge ay"/>
</div>
<div class="lyrico-lyrics-wrapper">Modhaledadhaam hungama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Modhaledadhaam hungama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dong daga daga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dong daga daga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ik daga ik daga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ik daga ik daga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ding dongu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ding dongu "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dong daga daga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dong daga daga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ik daga ik daga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ik daga ik daga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ding dongu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ding dongu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dong daga daga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dong daga daga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ik daga ik daga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ik daga ik daga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ding dongu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ding dongu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dong daga daga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dong daga daga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ik daga ik daga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ik daga ik daga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ding dongu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ding dongu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vote veydam only formality
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vote veydam only formality"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappaka gelusthundi mandhe party
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappaka gelusthundi mandhe party"/>
</div>
<div class="lyrico-lyrics-wrapper">Chakram thippe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chakram thippe"/>
</div>
<div class="lyrico-lyrics-wrapper">Sri krishna thodunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sri krishna thodunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Anti-kouravulu andhariki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anti-kouravulu andhariki"/>
</div>
<div class="lyrico-lyrics-wrapper">Time bad ye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Time bad ye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dong daga daga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dong daga daga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ik daga ik daga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ik daga ik daga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ding dongu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ding dongu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dong daga daga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dong daga daga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ik daga ik daga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ik daga ik daga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ding dongu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ding dongu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye pettra chinna cooling glass
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye pettra chinna cooling glass"/>
</div>
<div class="lyrico-lyrics-wrapper">Vachelopu breaking news
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vachelopu breaking news"/>
</div>
<div class="lyrico-lyrics-wrapper">Shirt button theesesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shirt button theesesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Chesey lungi danceu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chesey lungi danceu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye kotra classu bunku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye kotra classu bunku"/>
</div>
<div class="lyrico-lyrics-wrapper">Ready cheyara inku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ready cheyara inku"/>
</div>
<div class="lyrico-lyrics-wrapper">Challeskundham holi lekka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Challeskundham holi lekka"/>
</div>
<div class="lyrico-lyrics-wrapper">Vachhesthunnay results
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vachhesthunnay results"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">All of you sing it once again
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="All of you sing it once again"/>
</div>
<div class="lyrico-lyrics-wrapper">Last bench ro party symbolu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Last bench ro party symbolu"/>
</div>
<div class="lyrico-lyrics-wrapper">Veseddam chalo wallu posteru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veseddam chalo wallu posteru"/>
</div>
<div class="lyrico-lyrics-wrapper">Geliche gang idi kaalarlettharoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Geliche gang idi kaalarlettharoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Campus motthamu campaign cheyyaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Campus motthamu campaign cheyyaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Start aindhi re-sound aagadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Start aindhi re-sound aagadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Jai bolo krishnnanna zindabad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jai bolo krishnnanna zindabad"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Danda nannaare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Danda nannaare"/>
</div>
</pre>
