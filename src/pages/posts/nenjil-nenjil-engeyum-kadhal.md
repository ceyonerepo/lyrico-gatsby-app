---
title: "nenjil nenjil song lyrics"
album: "Engeyum Kadhal"
artist: "Harris Jayaraj"
lyricist: "Madhan Karky"
director: "Prabhudeva"
path: "/albums/engeyum-kadhal-lyrics"
song: "Nenjil Nenjil"
image: ../../images/albumart/engeyum-kadhal.jpg
date: 2011-05-06
lang: tamil
youtubeLink: "https://www.youtube.com/embed/l98b32yBiQg"
type: "love"
singers:
  - Harish Raghavendra
  - Chinmayi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nenjil Nenjil Idho Idho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil Nenjil Idho Idho"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal Kadhal Piranthatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Kadhal Piranthatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjum Kaatril Mayangiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjum Kaatril Mayangiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam Mele Paranthatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Mele Paranthatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Maalai Velai Velai Kaattutho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maalai Velai Velai Kaattutho"/>
</div>
<div class="lyrico-lyrics-wrapper">En Oolai Vaanam Jwalai Mootutho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Oolai Vaanam Jwalai Mootutho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjil Nenjil Idho Idho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil Nenjil Idho Idho"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal Kadhal Piranthatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Kadhal Piranthatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjum Kaatril Mayangiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjum Kaatril Mayangiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam Mele Paranthatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Mele Paranthatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Maalai Velai Velai Kaattutho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maalai Velai Velai Kaattutho"/>
</div>
<div class="lyrico-lyrics-wrapper">En Oolai Vaanam Jwalai Mootutho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Oolai Vaanam Jwalai Mootutho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Nilaavil En Nilaavil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nilaavil En Nilaavil"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Minsaaral Thaan Thoovutho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Minsaaral Thaan Thoovutho"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kanaavil En Kanaavil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kanaavil En Kanaavil"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Bimba Thugal Inbangal Pozhigaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Bimba Thugal Inbangal Pozhigaiyil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjil Nenjil Idho Idho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil Nenjil Idho Idho"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal Kadhal Piranthatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Kadhal Piranthatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjum Kaatril Mayangiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjum Kaatril Mayangiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam Mele Paranthatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Mele Paranthatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Maalai Velai Velai Kaattutho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maalai Velai Velai Kaattutho"/>
</div>
<div class="lyrico-lyrics-wrapper">En Oolai Vaanam Jwalai Mootutho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Oolai Vaanam Jwalai Mootutho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Mounam Paravum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Mounam Paravum"/>
</div>
<div class="lyrico-lyrics-wrapper">Siru Kadhal Pozhuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru Kadhal Pozhuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kizhiyil Vizhaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kizhiyil Vizhaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mozhiyil Ethuvum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mozhiyil Ethuvum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavithaiyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavithaiyadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Asaiyum imaiyin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asaiyum imaiyin"/>
</div>
<div class="lyrico-lyrics-wrapper">Isaiyil Ethuvum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Isaiyil Ethuvum"/>
</div>
<div class="lyrico-lyrics-wrapper">Inimaiyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inimaiyadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vin Maarbil Padarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vin Maarbil Padarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Paarvai Thiravam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Paarvai Thiravam"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayam Putharil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam Putharil"/>
</div>
<div class="lyrico-lyrics-wrapper">Sithari Sithari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sithari Sithari"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhivathu Yen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhivathu Yen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oar Uthirum Thuliyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oar Uthirum Thuliyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Uthiram Muzhuthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uthiram Muzhuthum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nathirvathu Yen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nathirvathu Yen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Urugaathey Uyire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urugaathey Uyire"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilagaathey Manathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilagaathey Manathey"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kadhal Verai Kaanavendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kadhal Verai Kaanavendi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanam Thaandi Unakkul Nuzhaintha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam Thaandi Unakkul Nuzhaintha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjil Nenjil Idho Idho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil Nenjil Idho Idho"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal Kadhal Piranthatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Kadhal Piranthatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjum Kaatril Mayangiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjum Kaatril Mayangiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam Mele Paranthatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Mele Paranthatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Maalai Velai Velai Kaattutho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maalai Velai Velai Kaattutho"/>
</div>
<div class="lyrico-lyrics-wrapper">En Oolai Vaanam Jwalai Mootutho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Oolai Vaanam Jwalai Mootutho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pasiyurum Idhazhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pasiyurum Idhazhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pasiyerum Viralum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pasiyerum Viralum"/>
</div>
<div class="lyrico-lyrics-wrapper">Viratham Mudithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viratham Mudithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Iraiyai Viraiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iraiyai Viraiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Neramithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neramithu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyirin Munaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirin Munaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayirin Ezhaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayirin Ezhaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thooram Athu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thooram Athu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Vellai Thiraiyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Vellai Thiraiyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Ullam Thiranthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Ullam Thiranthaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Siruga Siruga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siruga Siruga"/>
</div>
<div class="lyrico-lyrics-wrapper">Iravai Thirudum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravai Thirudum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaarigaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaarigaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidiyum Varaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidiyum Varaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Viralum Idhazhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viralum Idhazhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoorigaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoorigaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidiyaathey Irave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidiyaathey Irave"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudiyathey Kanave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudiyathey Kanave"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Innum Konjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Innum Konjam"/>
</div>
<div class="lyrico-lyrics-wrapper">Neela Kori Kadhal Kaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neela Kori Kadhal Kaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Thudikka Thudikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thudikka Thudikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjil Nenjil Idho Idho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil Nenjil Idho Idho"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal Kadhal Piranthatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Kadhal Piranthatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjum Kaatril Mayangiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjum Kaatril Mayangiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam Mele Paranthatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Mele Paranthatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Maalai Velai Velai Kaattutho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maalai Velai Velai Kaattutho"/>
</div>
<div class="lyrico-lyrics-wrapper">En Oolai Vaanam Jwalai Mootutho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Oolai Vaanam Jwalai Mootutho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Nilaavil En Nilaavil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nilaavil En Nilaavil"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Minsaaral Thaan Thoovutho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Minsaaral Thaan Thoovutho"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kanaavil En Kanaavil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kanaavil En Kanaavil"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Bimba Thugal Inbangal Pozhigaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Bimba Thugal Inbangal Pozhigaiyil"/>
</div>
</pre>
