---
title: "aasai mugam marandhu poche song lyrics"
album: "Gypsy"
artist: "Susheela Raman - Sam Mills"
lyricist: "Subramaniya Bharathi"
director: "Raju Murugan"
path: "/albums/gypsy-song-lyrics"
song: "Aasai Mugam Marandhu Poche"
image: ../../images/albumart/gypsy.jpg
date: 2020-03-06
lang: tamil
youtubeLink: "https://www.youtube.com/embed/C6hq22Ell08"
type: "sad"
singers:
  - Susheela Raman
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aasai mugam marandhu pochae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai mugam marandhu pochae"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhai aaridam solvenadi thozhi…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhai aaridam solvenadi thozhi…"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasai mugam marandhu pochae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai mugam marandhu pochae"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhai aaridam solvenadi thozhi…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhai aaridam solvenadi thozhi…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nesam marakkavillai nenjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nesam marakkavillai nenjam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nesam marakkavillai nenjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nesam marakkavillai nenjam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nesam marakkavillai nenjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nesam marakkavillai nenjam"/>
</div>
<div class="lyrico-lyrics-wrapper">Enil ninaivu mugam marakkalaamo…..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enil ninaivu mugam marakkalaamo….."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nesam marakkavillai nenjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nesam marakkavillai nenjam"/>
</div>
<div class="lyrico-lyrics-wrapper">Enil ninaivu mugam marakkalaamo….
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enil ninaivu mugam marakkalaamo…."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aasai mugam marandhu pochae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai mugam marandhu pochae"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhai aaridam solvenadi thozhi…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhai aaridam solvenadi thozhi…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannil theriyudhoru thotram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannil theriyudhoru thotram"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhil kannan azhagu muzhudhillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhil kannan azhagu muzhudhillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannil theriyudhoru thotram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannil theriyudhoru thotram"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhil kannan azhagu muzhudhillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhil kannan azhagu muzhudhillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nannu muga vadivu kaanil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannu muga vadivu kaanil"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha nalla malarchirippai kaanom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha nalla malarchirippai kaanom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kangal purindhu vitta paavam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangal purindhu vitta paavam"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir kannan uru marakkalaachu….
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir kannan uru marakkalaachu…."/>
</div>
<div class="lyrico-lyrics-wrapper">Pengal inathil idhu polae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pengal inathil idhu polae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru pedhaiyai munbu kandadhundo…..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru pedhaiyai munbu kandadhundo….."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aasai mugam marandhu pochae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai mugam marandhu pochae"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhai aaridam solvenadi thozhi…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhai aaridam solvenadi thozhi…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannan mugam marandhu ponaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannan mugam marandhu ponaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannan mugam marandhu ponaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannan mugam marandhu ponaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannan mugam marandhu ponaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannan mugam marandhu ponaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha kangal irundhum payanundo…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha kangal irundhum payanundo…"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannan mugam marandhu ponaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannan mugam marandhu ponaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha kangal irundhum payanundo….
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha kangal irundhum payanundo…."/>
</div>
<div class="lyrico-lyrics-wrapper">Vanna padamum illai kandaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanna padamum illai kandaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini vaazhum vazhi ennadi thozhi….
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini vaazhum vazhi ennadi thozhi…."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aasai mugam marandhu pochae….
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai mugam marandhu pochae…."/>
</div>
<div class="lyrico-lyrics-wrapper">Aasai mugam marandhu pochae…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai mugam marandhu pochae…"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasai mugam marandhu pochae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai mugam marandhu pochae"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhai aaridam solvenadi thozhi…thozhiii…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhai aaridam solvenadi thozhi…thozhiii…"/>
</div>
</pre>
