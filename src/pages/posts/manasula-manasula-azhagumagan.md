---
title: "manasula manasula song lyrics"
album: "Azhagumagan"
artist: "James Vasanthan"
lyricist: "yugabharathi"
director: "Azhagan Selva"
path: "/albums/azhagumagan-lyrics"
song: "Manasula Manasula"
image: ../../images/albumart/azhagumagan.jpg
date: 2022-05-13
lang: tamil
youtubeLink: "https://www.youtube.com/embed/VAMXrHvje0M"
type: "love"
singers:
  - Hariharan
  - Pooja Vaidyanath
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">manasula manasula irukura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasula manasula irukura"/>
</div>
<div class="lyrico-lyrics-wrapper">nenapula thinam thinam thavikuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenapula thinam thinam thavikuren"/>
</div>
<div class="lyrico-lyrics-wrapper">kannukulla nulainju ullukkula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannukulla nulainju ullukkula"/>
</div>
<div class="lyrico-lyrics-wrapper">irangi usuraiyum edukuraye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irangi usuraiyum edukuraye"/>
</div>
<div class="lyrico-lyrics-wrapper">nee ethukku kanavula kanavula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee ethukku kanavula kanavula"/>
</div>
<div class="lyrico-lyrics-wrapper">miratura alagula theruvaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="miratura alagula theruvaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">athatura unna enni ipo nan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athatura unna enni ipo nan"/>
</div>
<div class="lyrico-lyrics-wrapper">kaathagi poren 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaathagi poren "/>
</div>
<div class="lyrico-lyrics-wrapper">aathadi nee oru thirudan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aathadi nee oru thirudan "/>
</div>
<div class="lyrico-lyrics-wrapper">ooroda pechula muradan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooroda pechula muradan"/>
</div>
<div class="lyrico-lyrics-wrapper">aanal nee unmaiyil peralagan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aanal nee unmaiyil peralagan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">manasula manasula irukuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasula manasula irukuren"/>
</div>
<div class="lyrico-lyrics-wrapper">manalula kayiraiyum thirikuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manalula kayiraiyum thirikuren"/>
</div>
<div class="lyrico-lyrics-wrapper">kanukullu nulainju ullukkula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanukullu nulainju ullukkula"/>
</div>
<div class="lyrico-lyrics-wrapper">virinju usuraiyum mithikiray
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="virinju usuraiyum mithikiray"/>
</div>
<div class="lyrico-lyrics-wrapper">nee ethuku kanavula 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee ethuku kanavula "/>
</div>
<div class="lyrico-lyrics-wrapper">kanavula methakura 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavula methakura "/>
</div>
<div class="lyrico-lyrics-wrapper">alagula theruvaiyum athatura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alagula theruvaiyum athatura"/>
</div>
<div class="lyrico-lyrics-wrapper">unna enni nan ipo kaathagi poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna enni nan ipo kaathagi poren"/>
</div>
<div class="lyrico-lyrics-wrapper">aathadi nee oru puluthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aathadi nee oru puluthi"/>
</div>
<div class="lyrico-lyrics-wrapper">aalana naal muthal maruvi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aalana naal muthal maruvi"/>
</div>
<div class="lyrico-lyrics-wrapper">pogatha aasaiyila urugi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pogatha aasaiyila urugi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">saamburaani naanum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saamburaani naanum "/>
</div>
<div class="lyrico-lyrics-wrapper">thoova kaalu neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoova kaalu neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">kootu sernthudaamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kootu sernthudaamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">vaalkai manakumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalkai manakumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">kota thanda naane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kota thanda naane"/>
</div>
<div class="lyrico-lyrics-wrapper">kupitalum kooda kovil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kupitalum kooda kovil"/>
</div>
<div class="lyrico-lyrics-wrapper">theru pola thengi kedakalama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theru pola thengi kedakalama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">onnum illa solla enaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="onnum illa solla enaku"/>
</div>
<div class="lyrico-lyrics-wrapper">nan othukitta vambu valaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan othukitta vambu valaku"/>
</div>
<div class="lyrico-lyrics-wrapper">vika vika thenankallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vika vika thenankallu"/>
</div>
<div class="lyrico-lyrics-wrapper">nalathena sonnathethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nalathena sonnathethu"/>
</div>
<div class="lyrico-lyrics-wrapper">romba romba thappu kanaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="romba romba thappu kanaku"/>
</div>
<div class="lyrico-lyrics-wrapper">aathadi nee oru puluthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aathadi nee oru puluthi"/>
</div>
<div class="lyrico-lyrics-wrapper">aalana naal muthal maruvi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aalana naal muthal maruvi"/>
</div>
<div class="lyrico-lyrics-wrapper">pogatha aasaiyila urugi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pogatha aasaiyila urugi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kattu theeye naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kattu theeye naan"/>
</div>
<div class="lyrico-lyrics-wrapper">kathaloda paathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathaloda paathu"/>
</div>
<div class="lyrico-lyrics-wrapper">veetu theebam pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veetu theebam pola"/>
</div>
<div class="lyrico-lyrics-wrapper">neeyum vanangura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeyum vanangura"/>
</div>
<div class="lyrico-lyrics-wrapper">aathu neera yeno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aathu neera yeno"/>
</div>
<div class="lyrico-lyrics-wrapper">aasaiyoda pesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aasaiyoda pesi"/>
</div>
<div class="lyrico-lyrics-wrapper">oothu neera pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oothu neera pola"/>
</div>
<div class="lyrico-lyrics-wrapper">ethuku kudika pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethuku kudika pora"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ulla mattum unna nenapen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulla mattum unna nenapen"/>
</div>
<div class="lyrico-lyrics-wrapper">nee ilaiyina sethu tholappen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee ilaiyina sethu tholappen"/>
</div>
<div class="lyrico-lyrics-wrapper">annam thanni enathuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="annam thanni enathuku"/>
</div>
<div class="lyrico-lyrics-wrapper">aatha appan enathuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aatha appan enathuku"/>
</div>
<div class="lyrico-lyrics-wrapper">nee iruntha pothum enaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee iruntha pothum enaku"/>
</div>
<div class="lyrico-lyrics-wrapper">aathadi nee oru thirudan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aathadi nee oru thirudan "/>
</div>
<div class="lyrico-lyrics-wrapper">ooroda pechula muradan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooroda pechula muradan"/>
</div>
<div class="lyrico-lyrics-wrapper">aanal nee unmaiyil peralagan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aanal nee unmaiyil peralagan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">manasula manasula irukuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasula manasula irukuren"/>
</div>
<div class="lyrico-lyrics-wrapper">manalula kayiraiyum thirikuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manalula kayiraiyum thirikuren"/>
</div>
<div class="lyrico-lyrics-wrapper">kanukullu nulainju ullukkula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanukullu nulainju ullukkula"/>
</div>
<div class="lyrico-lyrics-wrapper">virinju usuraiyum edukura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="virinju usuraiyum edukura"/>
</div>
<div class="lyrico-lyrics-wrapper">nee ethuku kanavula 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee ethuku kanavula "/>
</div>
<div class="lyrico-lyrics-wrapper">kanavula methakura 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavula methakura "/>
</div>
<div class="lyrico-lyrics-wrapper">alagula theruvaiyum athatura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alagula theruvaiyum athatura"/>
</div>
<div class="lyrico-lyrics-wrapper">unna enni nan ipo kaathagi poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna enni nan ipo kaathagi poren"/>
</div>
<div class="lyrico-lyrics-wrapper">aathadi nee oru thirudan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aathadi nee oru thirudan "/>
</div>
<div class="lyrico-lyrics-wrapper">ooroda pechula muradan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooroda pechula muradan"/>
</div>
<div class="lyrico-lyrics-wrapper">aanal nee unmaiyil peralagan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aanal nee unmaiyil peralagan"/>
</div>
</pre>
