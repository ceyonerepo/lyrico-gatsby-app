---
title: "thozhi song lyrics"
album: "Hey Sinamika"
artist: "Govind Vasantha"
lyricist: "Madhan Karky"
director: "Brinda"
path: "/albums/hey-sinamika-lyrics"
song: "Thozhi"
image: ../../images/albumart/hey-sinamika.jpg
date: 2022-03-03
lang: tamil
youtubeLink: "https://www.youtube.com/embed/206dnTcpKlI"
type: "love"
singers:
  - Pradeep Kumar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yaarodum kaanaadha thooimaiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarodum kaanaadha thooimaiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnil naan kaangiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnil naan kaangiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Mun endrum illaadha aasaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mun endrum illaadha aasaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaalae naan kolgiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaalae naan kolgiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhiyilae idhayathin nizhalaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhiyilae idhayathin nizhalaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Neelgindraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelgindraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan ooiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan ooiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhiyilae thelithidum kadalaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhiyilae thelithidum kadalaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aagindraai en seiven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagindraai en seiven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Solladee thozhi thozhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solladee thozhi thozhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennarundhozhi Solladee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennarundhozhi Solladee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey kannaadiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey kannaadiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">En bimbam ennaippol illaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En bimbam ennaippol illaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unil"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey en vaanoliyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey en vaanoliyae"/>
</div>
<div class="lyrico-lyrics-wrapper">En paechu thooral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En paechu thooral"/>
</div>
<div class="lyrico-lyrics-wrapper">Pol kaetkudhae unil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pol kaetkudhae unil"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey en nizharthunaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey en nizharthunaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Murattu maunam menmaiyaai pesuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Murattu maunam menmaiyaai pesuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey hey uyirkkadhavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hey uyirkkadhavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirakkumboadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirakkumboadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aayiram vaasam veesumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayiram vaasam veesumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thozhi thozhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozhi thozhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennarundhozhi Solladee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennarundhozhi Solladee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Needhaana ennullae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Needhaana ennullae"/>
</div>
<div class="lyrico-lyrics-wrapper">Veezhvadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veezhvadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Theera thooralgalaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theera thooralgalaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Needhaana ennullae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Needhaana ennullae"/>
</div>
<div class="lyrico-lyrics-wrapper">Moolvadhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moolvadhu "/>
</div>
<div class="lyrico-lyrics-wrapper">Thoongaadha theeppookkalaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoongaadha theeppookkalaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavidhaigal suvaithidum thunaiyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavidhaigal suvaithidum thunaiyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyaanaai neeyaanaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyaanaai neeyaanaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Purindhidaa varigalin porulai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Purindhidaa varigalin porulai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ketkindraai en seiven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketkindraai en seiven"/>
</div>
<div class="lyrico-lyrics-wrapper">Solladee solladee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solladee solladee"/>
</div>
<div class="lyrico-lyrics-wrapper">Solladee solladee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solladee solladee"/>
</div>
<div class="lyrico-lyrics-wrapper">Thozhi thozhi solladee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozhi thozhi solladee"/>
</div>
<div class="lyrico-lyrics-wrapper">Thozhi thozhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozhi thozhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennarundhozhi Solladee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennarundhozhi Solladee"/>
</div>
</pre>
