---
title: "saataiyin munaiyil - nee seitha neerodai song lyrics"
album: "Meendum Oru Mariyathai"
artist: "N. R. Raghunanthan - Sharran Surya - Yuvan Shankar Raja"
lyricist: "Kabilan Vairamuthu"
director: "Bharathiraja"
path: "/albums/meendum-oru-mariyathai-lyrics"
song: "Saataiyin Munaiyil - Nee Seitha Neerodai"
image: ../../images/albumart/meendum-oru-mariyathai.jpg
date: 2020-02-21
lang: tamil
youtubeLink: "https://www.youtube.com/embed/RaML_Agayes"
type: "melody"
singers:
  - Priya Himesh
  - Sai Shravanam
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">nee seitha neerodai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee seitha neerodai"/>
</div>
<div class="lyrico-lyrics-wrapper">nothal paaraayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nothal paaraayo"/>
</div>
<div class="lyrico-lyrics-wrapper">pethai engey sothai vella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pethai engey sothai vella"/>
</div>
<div class="lyrico-lyrics-wrapper">yaar yaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaar yaaro"/>
</div>
<div class="lyrico-lyrics-wrapper">thoondum yaazhaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoondum yaazhaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">aadum paathangal thaazha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aadum paathangal thaazha"/>
</div>
<div class="lyrico-lyrics-wrapper">aadai mooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aadai mooda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">saatayin munaiyil salangai oli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saatayin munaiyil salangai oli"/>
</div>
<div class="lyrico-lyrics-wrapper">sabaigalai nanaithida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sabaigalai nanaithida"/>
</div>
<div class="lyrico-lyrics-wrapper">avanilaye paavai abinayamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avanilaye paavai abinayamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">maayan kalaivadivam yaavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maayan kalaivadivam yaavum"/>
</div>
<div class="lyrico-lyrics-wrapper">manthira kanava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manthira kanava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">saatayin munaiyil salangai oli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saatayin munaiyil salangai oli"/>
</div>
<div class="lyrico-lyrics-wrapper">sabaigalai nanaithida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sabaigalai nanaithida"/>
</div>
<div class="lyrico-lyrics-wrapper">avanilaye paavai abinayamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avanilaye paavai abinayamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">adimayin thotramaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adimayin thotramaa"/>
</div>
<div class="lyrico-lyrics-wrapper">enthira inama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enthira inama"/>
</div>
<div class="lyrico-lyrics-wrapper">enthira inama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enthira inama"/>
</div>
<div class="lyrico-lyrics-wrapper">enthira inama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enthira inama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaanikai selutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaanikai selutha"/>
</div>
<div class="lyrico-lyrics-wrapper">kadavul illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadavul illai"/>
</div>
<div class="lyrico-lyrics-wrapper">ilamayum ilaithida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ilamayum ilaithida"/>
</div>
<div class="lyrico-lyrics-wrapper">umayavalin vanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="umayavalin vanna"/>
</div>
<div class="lyrico-lyrics-wrapper">imai tholaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="imai tholaya"/>
</div>
<div class="lyrico-lyrics-wrapper">naalum azhuthidavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naalum azhuthidavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">maayan vinthaigal palavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maayan vinthaigal palavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaanikai selutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaanikai selutha"/>
</div>
<div class="lyrico-lyrics-wrapper">kadavul illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadavul illai"/>
</div>
<div class="lyrico-lyrics-wrapper">ilamayum ilaithida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ilamayum ilaithida"/>
</div>
<div class="lyrico-lyrics-wrapper">umayavalin vanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="umayavalin vanna"/>
</div>
<div class="lyrico-lyrics-wrapper">imai tholaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="imai tholaya"/>
</div>
<div class="lyrico-lyrics-wrapper">pirivathu leelaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pirivathu leelaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">minnalin uruva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="minnalin uruva"/>
</div>
<div class="lyrico-lyrics-wrapper">minnalin uruva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="minnalin uruva"/>
</div>
<div class="lyrico-lyrics-wrapper">minnalin uruva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="minnalin uruva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaveri idhayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaveri idhayam"/>
</div>
<div class="lyrico-lyrics-wrapper">karai puralum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karai puralum"/>
</div>
<div class="lyrico-lyrics-wrapper">madaigalum podi pada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="madaigalum podi pada"/>
</div>
<div class="lyrico-lyrics-wrapper">sirumirugam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sirumirugam "/>
</div>
<div class="lyrico-lyrics-wrapper">yaavum arupadave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaavum arupadave"/>
</div>
<div class="lyrico-lyrics-wrapper">nyaalam udai padave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nyaalam udai padave"/>
</div>
<div class="lyrico-lyrics-wrapper">vaanam koonthalil vizhave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaanam koonthalil vizhave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaveri idhayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaveri idhayam"/>
</div>
<div class="lyrico-lyrics-wrapper">karai puralum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karai puralum"/>
</div>
<div class="lyrico-lyrics-wrapper">madaigalum podi pada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="madaigalum podi pada"/>
</div>
<div class="lyrico-lyrics-wrapper">sirumirugam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sirumirugam "/>
</div>
<div class="lyrico-lyrics-wrapper">yaavum arupadave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaavum arupadave"/>
</div>
<div class="lyrico-lyrics-wrapper">erithazhal polave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="erithazhal polave"/>
</div>
<div class="lyrico-lyrics-wrapper">naayanam ezhave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naayanam ezhave"/>
</div>
</pre>
