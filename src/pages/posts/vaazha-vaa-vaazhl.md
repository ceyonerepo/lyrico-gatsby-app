---
title: "vaazha vaa song lyrics"
album: "Vaazhl"
artist: "Pradeep Kumar"
lyricist: "Pradeep Kumar - Arun Prabhu Purushothaman"
director: "Arun Prabu Purushothaman"
path: "/albums/vaazhl-lyrics"
song: "Vaazha Vaa"
image: ../../images/albumart/vaazhl.jpg
date: 2021-07-16
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Fq47a4SnCtU"
type: "happy"
singers:
  - Pradeep Kumar
  - Radar with A Kay
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">vaazha vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaazha vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">nee veelavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee veelavo"/>
</div>
<div class="lyrico-lyrics-wrapper">vaalkaiyo nee valave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalkaiyo nee valave"/>
</div>
<div class="lyrico-lyrics-wrapper">vaalave nee vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalave nee vaa vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">vaa vaa vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaa vaa vaa vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaa vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">vaazha vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaazha vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">nee veelavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee veelavo"/>
</div>
<div class="lyrico-lyrics-wrapper">vaalkaiyo nee valave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalkaiyo nee valave"/>
</div>
<div class="lyrico-lyrics-wrapper">vaalave nee vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalave nee vaa vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">vaa vaa vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaa vaa vaa vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oora thedi poga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oora thedi poga"/>
</div>
<div class="lyrico-lyrics-wrapper">ooru theva illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooru theva illa"/>
</div>
<div class="lyrico-lyrics-wrapper">saava oram thalli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saava oram thalli"/>
</div>
<div class="lyrico-lyrics-wrapper">vaalthu paaru neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalthu paaru neeyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kai virincha vaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kai virincha vaanam"/>
</div>
<div class="lyrico-lyrics-wrapper">moochu kaatha megam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moochu kaatha megam"/>
</div>
<div class="lyrico-lyrics-wrapper">enthurucha pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enthurucha pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">ottam podu neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ottam podu neeyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thoongathe oyathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoongathe oyathe"/>
</div>
<div class="lyrico-lyrics-wrapper">moodathe thedathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moodathe thedathe"/>
</div>
<div class="lyrico-lyrics-wrapper">nee yaaro yethetho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee yaaro yethetho"/>
</div>
<div class="lyrico-lyrics-wrapper">pothatho pothatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pothatho pothatho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaa vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">vaazha vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaazha vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">nee veelavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee veelavo"/>
</div>
<div class="lyrico-lyrics-wrapper">vaalkaiyo nee valave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalkaiyo nee valave"/>
</div>
<div class="lyrico-lyrics-wrapper">vaalave nee vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalave nee vaa vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">vaa vaa vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaa vaa vaa vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaa vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">vaazha vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaazha vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">nee veelavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee veelavo"/>
</div>
<div class="lyrico-lyrics-wrapper">vaalkaiyo nee valave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalkaiyo nee valave"/>
</div>
<div class="lyrico-lyrics-wrapper">vaalave nee vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalave nee vaa vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">vaa vaa vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaa vaa vaa vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaa vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">vaazha vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaazha vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">nee veelavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee veelavo"/>
</div>
<div class="lyrico-lyrics-wrapper">vaalkaiyo nee valave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalkaiyo nee valave"/>
</div>
<div class="lyrico-lyrics-wrapper">vaalave nee vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalave nee vaa vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">vaa vaa vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaa vaa vaa vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaa vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">vaazha vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaazha vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">nee veelavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee veelavo"/>
</div>
<div class="lyrico-lyrics-wrapper">vaalkaiyo nee valave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalkaiyo nee valave"/>
</div>
<div class="lyrico-lyrics-wrapper">vaalave nee vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalave nee vaa vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">vaa vaa vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaa vaa vaa vaa"/>
</div>
</pre>
