---
title: "alathi anbai song lyrics"
album: "Asuravadham"
artist: "Govind Menon"
lyricist: "Karthik Netha"
director: "Maruthupandian"
path: "/albums/asuravadham-lyrics"
song: "Alathi Anbai"
image: ../../images/albumart/asuravadham.jpg
date: 2018-06-29
lang: tamil
youtubeLink: "https://www.youtube.com/embed/c7nLBfoX5Hs"
type: "sad"
singers:
  - Ananthu
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Naan alaathi anbai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan alaathi anbai"/>
</div>
<div class="lyrico-lyrics-wrapper">Neengi pogiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neengi pogiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Anaadhai vazhvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anaadhai vazhvai"/>
</div>
<div class="lyrico-lyrics-wrapper">Meendum vazha pogiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meendum vazha pogiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Ul saagiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ul saagiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thinam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thinam"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ohhh kanneeril moozhgum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohhh kanneeril moozhgum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarai aagiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarai aagiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennodu naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennodu naanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kelvi aagi pogiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelvi aagi pogiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Ul vegiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ul vegiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thinam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thinam"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeno vazhvae ododi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeno vazhvae ododi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaam yengi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaam yengi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pooradi yaar vazhvaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pooradi yaar vazhvaro"/>
</div>
<div class="lyrico-lyrics-wrapper">En vazhvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En vazhvai"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar paarparo en poovai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar paarparo en poovai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Moorkkamaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moorkkamaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha vazhkai panthadudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha vazhkai panthadudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yekkamaai oru jeevan pinnodudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yekkamaai oru jeevan pinnodudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Saambal kattil alaindhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saambal kattil alaindhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Siru kakkam koottai adaindhidumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru kakkam koottai adaindhidumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedal theerum oru naal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedal theerum oru naal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan paasam thanaai thirumbidumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan paasam thanaai thirumbidumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ohhh kanavil kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohhh kanavil kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Thookkam illaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thookkam illaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunjadhu kaanum kaatchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunjadhu kaanum kaatchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaavum poo mugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaavum poo mugam"/>
</div>
<div class="lyrico-lyrics-wrapper">Un nyabagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un nyabagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ohhh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohhh"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaamae ennai kooru podudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaamae ennai kooru podudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ondraga vazhntha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondraga vazhntha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam konjam thettrudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam konjam thettrudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vali maatrudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vali maatrudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbae"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tholil saindhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholil saindhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee pesum ponnaal yendhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee pesum ponnaal yendhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan vazhven varuvenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan vazhven varuvenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan en poove
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan en poove"/>
</div>
<div class="lyrico-lyrics-wrapper">Un madi saayum en vazhvae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un madi saayum en vazhvae"/>
</div>
</pre>
