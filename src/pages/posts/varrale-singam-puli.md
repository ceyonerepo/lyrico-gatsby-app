---
title: "varrale song lyrics"
album: "Singam Puli"
artist: "Mani Sharma"
lyricist: "Annamalai"
director: "Sai Ramani"
path: "/albums/singam-puli-lyrics"
song: "Poove Poove"
image: ../../images/albumart/singam-puli.jpg
date: 2011-03-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/LyzBNnBqgDc"
type: "happy"
singers:
  - Ranjith
  - Saindhavi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Varaale Varaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varaale Varaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Jil Jil Jil Singaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jil Jil Jil Singaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Varaale Varaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varaale Varaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Jil Jil Jil Singaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jil Jil Jil Singaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Raththathil Soodu Vachaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raththathil Soodu Vachaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varaane Varaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varaane Varaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Rajathi Rajane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rajathi Rajane"/>
</div>
<div class="lyrico-lyrics-wrapper">Muththathil Muththeduppaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muththathil Muththeduppaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thenilavupola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thenilavupola"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholainjitta Nee Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholainjitta Nee Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Balbu Potta Sirippil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Balbu Potta Sirippil"/>
</div>
<div class="lyrico-lyrics-wrapper">Pathikkitta Nee Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathikkitta Nee Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gondhuppottu Naama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gondhuppottu Naama"/>
</div>
<div class="lyrico-lyrics-wrapper">Ottikkavenum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ottikkavenum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konja Neram Pirinjaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konja Neram Pirinjaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasu Nogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu Nogum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sooriyane Vendaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sooriyane Vendaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Adha Vithupputtu Vaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adha Vithupputtu Vaanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Raathiriyil Mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raathiriyil Mattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini Vachikkalaam Thoonga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Vachikkalaam Thoonga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Powder Potta Kannam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Powder Potta Kannam"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu Palapalakkum Kinnam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu Palapalakkum Kinnam"/>
</div>
<div class="lyrico-lyrics-wrapper">En Echchil Eeram Pattaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Echchil Eeram Pattaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu Innum Konjam Minnum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu Innum Konjam Minnum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varaale Varaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varaale Varaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Jil Jil Jil Singaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jil Jil Jil Singaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Varaale Varaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Varaale Varaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Jil Jil Jil Singaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jil Jil Jil Singaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Raththathil Soodu Vachaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raththathil Soodu Vachaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varaane Varaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varaane Varaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Rajathi Rajane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rajathi Rajane"/>
</div>
<div class="lyrico-lyrics-wrapper">Muththathil Muththeduppaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muththathil Muththeduppaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal Endra Vaarthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Endra Vaarthai"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhuthaan Mandhiram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhuthaan Mandhiram"/>
</div>
<div class="lyrico-lyrics-wrapper">Maththadhellaam Inge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maththadhellaam Inge"/>
</div>
<div class="lyrico-lyrics-wrapper">Summa Thandhiram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summa Thandhiram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rendupperu Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendupperu Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini Onnaaganum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Onnaaganum"/>
</div>
<div class="lyrico-lyrics-wrapper">Onnu Aanappinne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnu Aanappinne"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu Moonaganum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu Moonaganum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjame Kaagidham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjame Kaagidham"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Adhil Oviyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Adhil Oviyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Varainju Vachi Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varainju Vachi Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhndhaal Thaan Kaaviyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhndhaal Thaan Kaaviyam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manasil Anbirundhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasil Anbirundhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu Podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu Podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanil Megamellaam Color Aagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanil Megamellaam Color Aagum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhum Indha Vaazhkkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhum Indha Vaazhkkai"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Vaanavilla Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Vaanavilla Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Irukkum Konjam Kaalathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irukkum Konjam Kaalathula"/>
</div>
<div class="lyrico-lyrics-wrapper">Nallabadi Vaazhavenum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nallabadi Vaazhavenum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varaale Varaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varaale Varaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Jil Jil Jil Singaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jil Jil Jil Singaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Varaale Varaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Varaale Varaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Jil Jil Jil Singaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jil Jil Jil Singaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Raththathil Soodu Vachaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raththathil Soodu Vachaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varaane Varaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varaane Varaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Rajathi Rajane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rajathi Rajane"/>
</div>
<div class="lyrico-lyrics-wrapper">Muththathil Muththeduppaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muththathil Muththeduppaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oththa Viral Neettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oththa Viral Neettu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhuve Podhume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhuve Podhume"/>
</div>
<div class="lyrico-lyrics-wrapper">Thottukkitte Vaazhuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottukkitte Vaazhuven"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Ennaalume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Ennaalume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Urangum Varai Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urangum Varai Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">En Manasil Irukkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Manasil Irukkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannamoodinaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannamoodinaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Kanavil Theriyanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kanavil Theriyanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sanga Ilakkiyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sanga Ilakkiyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Nee Padikkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Nee Padikkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Veroru Ponna Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veroru Ponna Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninaikka Marukkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaikka Marukkanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanil Ore Oru Vennilaathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanil Ore Oru Vennilaathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhvil Ore Oru Pennilaathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvil Ore Oru Pennilaathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pookkal Nooru Theda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pookkal Nooru Theda"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Pattaampoochiyilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Pattaampoochiyilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Maari Maari Jodi Sera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maari Maari Jodi Sera"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum Mirugam Illaiyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum Mirugam Illaiyadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varaale Varaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varaale Varaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Jil Jil Jil Singaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jil Jil Jil Singaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Varaale Varaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varaale Varaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Jil Jil Jil Singaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jil Jil Jil Singaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Raththathil Soodu Vachaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raththathil Soodu Vachaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varaane Varaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varaane Varaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Rajathi Rajane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rajathi Rajane"/>
</div>
<div class="lyrico-lyrics-wrapper">Muththathil Muththeduppaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muththathil Muththeduppaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Muththathil Muththeduppaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muththathil Muththeduppaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Muththathil Muththeduppaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muththathil Muththeduppaane"/>
</div>
</pre>
