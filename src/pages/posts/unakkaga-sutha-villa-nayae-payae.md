---
title: "unakkaga sutha villa song lyrics"
album: "Nayae Payae"
artist: "NR Raghunanthan"
lyricist: "Sakthi Vasan"
director: "Sakthi Vasan"
path: "/albums/nayae-payae-song-lyrics"
song: "Unakkaga Sutha Villa"
image: ../../images/albumart/nayae-payae.jpg
date: 2021-04-23
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Fi6exL5qDzk"
type: "Sad"
singers:
  - Chinmayi Sripada
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">unakkaga sutha villa boomi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unakkaga sutha villa boomi"/>
</div>
<div class="lyrico-lyrics-wrapper">inga enakkaga ethuvum illa saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inga enakkaga ethuvum illa saami"/>
</div>
<div class="lyrico-lyrics-wrapper">unakkaga sutha villa boomi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unakkaga sutha villa boomi"/>
</div>
<div class="lyrico-lyrics-wrapper">inga enakkaga ethuvum illa saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inga enakkaga ethuvum illa saami"/>
</div>
<div class="lyrico-lyrics-wrapper">oor uyir povathal nastam enna kuru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oor uyir povathal nastam enna kuru"/>
</div>
<div class="lyrico-lyrics-wrapper">ne savathale nintriduma 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ne savathale nintriduma "/>
</div>
<div class="lyrico-lyrics-wrapper">pethalum sethalum ondrum illada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pethalum sethalum ondrum illada"/>
</div>
<div class="lyrico-lyrics-wrapper">verum peru vacha pindam ithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="verum peru vacha pindam ithu"/>
</div>
<div class="lyrico-lyrics-wrapper">veru enna da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veru enna da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">unakkaga sutha villa boomi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unakkaga sutha villa boomi"/>
</div>
<div class="lyrico-lyrics-wrapper">inga enakkaga ethuvum illa saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inga enakkaga ethuvum illa saami"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">un naaku mooku kaathu kannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un naaku mooku kaathu kannu"/>
</div>
<div class="lyrico-lyrics-wrapper">unma illa da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unma illa da"/>
</div>
<div class="lyrico-lyrics-wrapper">nethu pootha rosa poova
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nethu pootha rosa poova"/>
</div>
<div class="lyrico-lyrics-wrapper">eduthu paaru da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eduthu paaru da"/>
</div>
<div class="lyrico-lyrics-wrapper">naaku mooku kaathu kannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naaku mooku kaathu kannu"/>
</div>
<div class="lyrico-lyrics-wrapper">unma illa da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unma illa da"/>
</div>
<div class="lyrico-lyrics-wrapper">nethu pootha rosa poova
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nethu pootha rosa poova"/>
</div>
<div class="lyrico-lyrics-wrapper">eduthu paaru da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eduthu paaru da"/>
</div>
<div class="lyrico-lyrics-wrapper">nooru vettu pesanju senja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nooru vettu pesanju senja"/>
</div>
<div class="lyrico-lyrics-wrapper">eera paana da athu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eera paana da athu"/>
</div>
<div class="lyrico-lyrics-wrapper">udaiyum pothu aluvathu ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="udaiyum pothu aluvathu ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">veru paana da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veru paana da"/>
</div>
<div class="lyrico-lyrics-wrapper">udambukulla aluku iruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="udambukulla aluku iruku"/>
</div>
<div class="lyrico-lyrics-wrapper">ellarukum theriyum da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellarukum theriyum da"/>
</div>
<div class="lyrico-lyrics-wrapper">intha odambe oru aluku nu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha odambe oru aluku nu"/>
</div>
<div class="lyrico-lyrics-wrapper">innaiki than puriyum da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="innaiki than puriyum da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">unakkaga sutha villa boomi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unakkaga sutha villa boomi"/>
</div>
<div class="lyrico-lyrics-wrapper">inga enakkaga ethuvum illa saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inga enakkaga ethuvum illa saami"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ooooh soru potu thani oothi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooooh soru potu thani oothi"/>
</div>
<div class="lyrico-lyrics-wrapper">odamba valakura atha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="odamba valakura atha"/>
</div>
<div class="lyrico-lyrics-wrapper">ketatharu apadiye potu oodura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ketatharu apadiye potu oodura"/>
</div>
<div class="lyrico-lyrics-wrapper">soru potu thani oothi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="soru potu thani oothi"/>
</div>
<div class="lyrico-lyrics-wrapper">odamba valakura atha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="odamba valakura atha"/>
</div>
<div class="lyrico-lyrics-wrapper">ketatharu apadiye potu oodura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ketatharu apadiye potu oodura"/>
</div>
<div class="lyrico-lyrics-wrapper">kaamam paathi kovam paathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaamam paathi kovam paathi"/>
</div>
<div class="lyrico-lyrics-wrapper">manitha odambu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manitha odambu da"/>
</div>
<div class="lyrico-lyrics-wrapper">ada kadaisi varaikum thirunthuravan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada kadaisi varaikum thirunthuravan"/>
</div>
<div class="lyrico-lyrics-wrapper">yaarum illa da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaarum illa da"/>
</div>
<div class="lyrico-lyrics-wrapper">tholu mele eeri eeri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tholu mele eeri eeri"/>
</div>
<div class="lyrico-lyrics-wrapper">palagi pochu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="palagi pochu da"/>
</div>
<div class="lyrico-lyrics-wrapper">aduthavan tholu mele eeri eeri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aduthavan tholu mele eeri eeri"/>
</div>
<div class="lyrico-lyrics-wrapper">palagi pochu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="palagi pochu da"/>
</div>
<div class="lyrico-lyrics-wrapper">setha pinnum antha puthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="setha pinnum antha puthi"/>
</div>
<div class="lyrico-lyrics-wrapper">nikka villa da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nikka villa da"/>
</div>
</pre>
