---
title: "Inky Pinky Ponky song lyrics"
album: "Sardar"
artist: "G. V. Prakash Kumar"
lyricist: "Arivu"
director: "P.S Mithran"
path: "/albums/sardar-lyrics"
song: "Inky Pinky Ponky"
image: ../../images/albumart/sardar.jpg
date: 2022-10-24
lang: tamil
youtubeLink: "https://www.youtube.com/embed/vk1IJROAv3E?controls=0"
type: "Mass"
singers:
  - Arivu
  - Santhosh hariharan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Hey yaarda avan areavulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey yaarda avan areavulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Hae vantaanda officer
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hae vantaanda officer"/>
</div>
<div class="lyrico-lyrics-wrapper">Laththi eduthaa officer
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laththi eduthaa officer"/>
</div>
<div class="lyrico-lyrics-wrapper">Vuttaanda officer
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vuttaanda officer"/>
</div>
<div class="lyrico-lyrics-wrapper">Kumthalakka officer
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kumthalakka officer"/>
</div>
<div class="lyrico-lyrics-wrapper">Officer officer
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Officer officer"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Karaarae kaappuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karaarae kaappuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Terroru jeeppuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Terroru jeeppuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Who is the donkey podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Who is the donkey podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Inky pinky ponky ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inky pinky ponky ma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kumpalaa thookkima
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kumpalaa thookkima"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaduven hockey ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaduven hockey ma"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaikkavemaaten baakki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaikkavemaaten baakki"/>
</div>
<div class="lyrico-lyrics-wrapper">Vera maari khakima
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vera maari khakima"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Naan khaki pangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan khaki pangu"/>
</div>
<div class="lyrico-lyrics-wrapper">En walkye trendu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En walkye trendu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan dutyku vanthaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan dutyku vanthaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Newsu undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Newsu undu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">En bp chillu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En bp chillu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru green tea sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru green tea sollu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan panbaana police-uh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan panbaana police-uh"/>
</div>
<div class="lyrico-lyrics-wrapper">People friendu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="People friendu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Karaarae kaappuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karaarae kaappuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Terroru jeeppuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Terroru jeeppuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Who is the donkey podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Who is the donkey podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Inky pinky ponky ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inky pinky ponky ma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kumpalaa thookkima
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kumpalaa thookkima"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaduven hockey ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaduven hockey ma"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaikkavemaaten baakki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaikkavemaaten baakki"/>
</div>
<div class="lyrico-lyrics-wrapper">Vera maari khakima
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vera maari khakima"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ye raavadi wronga pudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye raavadi wronga pudi"/>
</div>
<div class="lyrico-lyrics-wrapper">Roundu katti thookkuvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Roundu katti thookkuvaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Satta mela button pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Satta mela button pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Sattam mathippaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sattam mathippaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ye laththi kondu city fulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye laththi kondu city fulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Meratti kaattuvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meratti kaattuvaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Uppa thinnaa policekaaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uppa thinnaa policekaaran"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanni kuduppaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanni kuduppaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Walky talky mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Walky talky mela"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhkaya thaan nadathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkaya thaan nadathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Safety panna poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Safety panna poren"/>
</div>
<div class="lyrico-lyrics-wrapper">Oora thiruthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oora thiruthi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Jeeppil yeri poyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jeeppil yeri poyi"/>
</div>
<div class="lyrico-lyrics-wrapper">Accuseda thaan adichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Accuseda thaan adichi"/>
</div>
<div class="lyrico-lyrics-wrapper">Tweetu poda poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tweetu poda poren"/>
</div>
<div class="lyrico-lyrics-wrapper">Photo pidichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Photo pidichi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Karaarae kaappuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karaarae kaappuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Terroru jeeppuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Terroru jeeppuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Who is the donkey podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Who is the donkey podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Inky pinky ponky ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inky pinky ponky ma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kumpalaa thookkima
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kumpalaa thookkima"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaduven hockeyma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaduven hockeyma"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaikkavemaaten baakki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaikkavemaaten baakki"/>
</div>
<div class="lyrico-lyrics-wrapper">Vera maari khakima
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vera maari khakima"/>
</div>
</pre>
