---
title: "idhi varanasi song lyrics"
album: "Johaar"
artist: "Priyadarshan - Balasubramanian"
lyricist: "Asura - Psychlone "
director: "Marni Teja Chowdary"
path: "/albums/johaar-lyrics"
song: "Idhi Varanasi"
image: ../../images/albumart/johaar.jpg
date: 2020-08-15
lang: telugu
youtubeLink: "https://www.youtube.com/embed/lqwks7hmI7I"
type: "love"
singers:
  - Asura
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Utthara Dhikkuna Nelakonna Raajyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Utthara Dhikkuna Nelakonna Raajyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thella Vaarithe Gantala Shabdham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thella Vaarithe Gantala Shabdham"/>
</div>
<div class="lyrico-lyrics-wrapper">Manishi Korikala Aakari Payanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manishi Korikala Aakari Payanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Chaavu Puttukala Aakari Gattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chaavu Puttukala Aakari Gattam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sivuni Ganga Kadigenu Nee Paapam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sivuni Ganga Kadigenu Nee Paapam"/>
</div>
<div class="lyrico-lyrics-wrapper">Saadhu Aghorala Chitti Prapancham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saadhu Aghorala Chitti Prapancham"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaali Migilina Bhudidha Kavacham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaali Migilina Bhudidha Kavacham"/>
</div>
<div class="lyrico-lyrics-wrapper">Chutta Chutta Chudu Aaku Prasaadham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chutta Chutta Chudu Aaku Prasaadham"/>
</div>
<div class="lyrico-lyrics-wrapper">Moksha Michhu Ee Kaasila Ganga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moksha Michhu Ee Kaasila Ganga"/>
</div>
<div class="lyrico-lyrics-wrapper">Banaaras Chuse Nanu Chuttam Gaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Banaaras Chuse Nanu Chuttam Gaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaari Chupadha Shashwathamedhani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaari Chupadha Shashwathamedhani"/>
</div>
<div class="lyrico-lyrics-wrapper">Niranthara Jwaalala Manikarnikagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niranthara Jwaalala Manikarnikagaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idi Varanasi Aey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idi Varanasi Aey"/>
</div>
<div class="lyrico-lyrics-wrapper">Idi Varanasi Aey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idi Varanasi Aey"/>
</div>
<div class="lyrico-lyrics-wrapper">Idi Varanasi Aey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idi Varanasi Aey"/>
</div>
<div class="lyrico-lyrics-wrapper">Ganga Kaasi Arre Idi Varanasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ganga Kaasi Arre Idi Varanasi"/>
</div>
<div class="lyrico-lyrics-wrapper">Idi Varanasi Idi Varanasi Idi Varanasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idi Varanasi Idi Varanasi Idi Varanasi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ganga Kaasi Arre Idi Varanasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ganga Kaasi Arre Idi Varanasi"/>
</div>
<div class="lyrico-lyrics-wrapper">Hara Hara Shankara Shiva Shiva Shankara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hara Hara Shankara Shiva Shiva Shankara"/>
</div>
<div class="lyrico-lyrics-wrapper">Hara Hara Shankara Shiva Shiva Shankara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hara Hara Shankara Shiva Shiva Shankara"/>
</div>
<div class="lyrico-lyrics-wrapper">Hara Hara Shankara Shiva Shiva Shankara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hara Hara Shankara Shiva Shiva Shankara"/>
</div>
<div class="lyrico-lyrics-wrapper">Asurula Kinkara Shambho Shankara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asurula Kinkara Shambho Shankara"/>
</div>
<div class="lyrico-lyrics-wrapper">Hara Hara Shankara Shiva Shiva Shankara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hara Hara Shankara Shiva Shiva Shankara"/>
</div>
<div class="lyrico-lyrics-wrapper">Asurula Kinkara Shambho Shankara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asurula Kinkara Shambho Shankara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idi Varanasi Ganga Maadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idi Varanasi Ganga Maadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadi Shankara Raathana Kaasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadi Shankara Raathana Kaasi"/>
</div>
<div class="lyrico-lyrics-wrapper">Neelakantudi Vishame Thaagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelakantudi Vishame Thaagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Amrutham Kosam Dhustula Dhaati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amrutham Kosam Dhustula Dhaati"/>
</div>
<div class="lyrico-lyrics-wrapper">Lakshala Prajala Varadhaa Raathana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lakshala Prajala Varadhaa Raathana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thulasi Dhaasudi Raama Charithra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thulasi Dhaasudi Raama Charithra"/>
</div>
<div class="lyrico-lyrics-wrapper">Paandava Shaapa Bhrahmana Hathya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paandava Shaapa Bhrahmana Hathya"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalidasa Kala Kaala Puttuka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalidasa Kala Kaala Puttuka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aadi Seshudi Nruthya Pradarshana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadi Seshudi Nruthya Pradarshana"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadi Anthamu Naa Shiva Naasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadi Anthamu Naa Shiva Naasa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naasathaaraswara Ravi Shakara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naasathaaraswara Ravi Shakara"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaarala Dhrusya Janthara Manthara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaarala Dhrusya Janthara Manthara"/>
</div>
<div class="lyrico-lyrics-wrapper">Brahmadevadhaa Saswamedha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Brahmadevadhaa Saswamedha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaasi Viswanadha Jothirlingaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasi Viswanadha Jothirlingaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Navaradhinagara Dhurga Mandhira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Navaradhinagara Dhurga Mandhira"/>
</div>
<div class="lyrico-lyrics-wrapper">Saaranaathana Buddhudi Karma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saaranaathana Buddhudi Karma"/>
</div>
<div class="lyrico-lyrics-wrapper">Moksha Michhu Ee Kaasila Ganga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moksha Michhu Ee Kaasila Ganga"/>
</div>
<div class="lyrico-lyrics-wrapper">Banaaras Chuse Nanu Chuttamgaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Banaaras Chuse Nanu Chuttamgaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaari Chupadha Shashwathamedhani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaari Chupadha Shashwathamedhani"/>
</div>
<div class="lyrico-lyrics-wrapper">Niranthara Jwaalala Manikarnikagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niranthara Jwaalala Manikarnikagaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idi Varanasi Idi Varanasi Idi Varanasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idi Varanasi Idi Varanasi Idi Varanasi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ganga Kaasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ganga Kaasi"/>
</div>
<div class="lyrico-lyrics-wrapper">Arre Idi Varanasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arre Idi Varanasi"/>
</div>
<div class="lyrico-lyrics-wrapper">Idi Varanasi Idi Varanasi Idi Varanasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idi Varanasi Idi Varanasi Idi Varanasi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ganga Kaasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ganga Kaasi"/>
</div>
<div class="lyrico-lyrics-wrapper">Arre Idi Varanasi Idi Varanasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arre Idi Varanasi Idi Varanasi"/>
</div>
</pre>