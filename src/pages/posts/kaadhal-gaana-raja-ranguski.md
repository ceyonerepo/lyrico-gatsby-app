---
title: "kaadhal gaana song lyrics"
album: "Raja Ranguski"
artist: "Yuvan Shankar Raja"
lyricist: "Mohan Raj"
director: "Dharani Dharan"
path: "/albums/raja-ranguski-lyrics"
song: "Kaadhal Gaana"
image: ../../images/albumart/raja-ranguski.jpg
date: 2018-09-21
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ZVHn0Ky8lJM"
type: "gaana"
singers:
  - VM Mahalingam
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Naan paarthu paarthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan paarthu paarthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Valarthu vechen manasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valarthu vechen manasa"/>
</div>
<div class="lyrico-lyrics-wrapper">Atha partha unnai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atha partha unnai"/>
</div>
<div class="lyrico-lyrics-wrapper">Lavatukichae en sarasae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lavatukichae en sarasae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru dosthu pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru dosthu pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Palakki vachen manasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palakki vachen manasa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naisa pesi pesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naisa pesi pesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Isthukichi en sarasae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Isthukichi en sarasae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lovvu vantha lovvu vantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lovvu vantha lovvu vantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Lovvu vantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lovvu vantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Jivvunu thaan irukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jivvunu thaan irukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Poga poga poga poga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poga poga poga poga"/>
</div>
<div class="lyrico-lyrics-wrapper">Javva pola ilukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Javva pola ilukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Atthai meri atthai meri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atthai meri atthai meri"/>
</div>
<div class="lyrico-lyrics-wrapper">Onna serntha inikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna serntha inikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha kaadhaluthaan allikummae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha kaadhaluthaan allikummae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaviyila ventha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaviyila ventha"/>
</div>
<div class="lyrico-lyrics-wrapper">Athukku idliyinu peru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athukku idliyinu peru"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaviyae ventha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaviyae ventha"/>
</div>
<div class="lyrico-lyrics-wrapper">Athukku kaadhalinnu peru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athukku kaadhalinnu peru"/>
</div>
<div class="lyrico-lyrics-wrapper">Un mugamtha unakku kaatum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un mugamtha unakku kaatum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannadiya paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannadiya paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Un manasu usathi kaatum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un manasu usathi kaatum"/>
</div>
<div class="lyrico-lyrics-wrapper">Daavu katti paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daavu katti paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Antha gundu bulp-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha gundu bulp-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannam parthu bijili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannam parthu bijili"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan kuchi ice-ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan kuchi ice-ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Uruguven da daily
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uruguven da daily"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada avala pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada avala pola"/>
</div>
<div class="lyrico-lyrics-wrapper">White-ah na pagalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="White-ah na pagalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan parthathilaeUthikada bigulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan parthathilaeUthikada bigulu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paaka solla pesa solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaka solla pesa solla"/>
</div>
<div class="lyrico-lyrics-wrapper">Kekka solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kekka solla"/>
</div>
<div class="lyrico-lyrics-wrapper">Lovvu summa inikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lovvu summa inikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thitta solla otta solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thitta solla otta solla"/>
</div>
<div class="lyrico-lyrics-wrapper">Lovvu dream-ah kudukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lovvu dream-ah kudukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mutta solla motha solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mutta solla motha solla"/>
</div>
<div class="lyrico-lyrics-wrapper">Usura paduthi edukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usura paduthi edukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha feeling-u thaan pichchikumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha feeling-u thaan pichchikumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaviyila ventha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaviyila ventha"/>
</div>
<div class="lyrico-lyrics-wrapper">Athukku idliyinu peru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athukku idliyinu peru"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaviyae ventha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaviyae ventha"/>
</div>
<div class="lyrico-lyrics-wrapper">Athukku kaadhalinnu peru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athukku kaadhalinnu peru"/>
</div>
<div class="lyrico-lyrics-wrapper">Un mugamtha unakku kaatum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un mugamtha unakku kaatum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannadiya paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannadiya paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Un manasu usathi kaatum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un manasu usathi kaatum"/>
</div>
<div class="lyrico-lyrics-wrapper">Daavu katti paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daavu katti paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jinguri jinguri sundari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jinguri jinguri sundari"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasa pottu veppa laundary
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasa pottu veppa laundary"/>
</div>
<div class="lyrico-lyrics-wrapper">Munthari panthari munthuri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munthari panthari munthuri"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava pechi enakku janggari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava pechi enakku janggari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru nicharula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru nicharula"/>
</div>
<div class="lyrico-lyrics-wrapper">Elastic pol kaadhalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Elastic pol kaadhalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Atha ussara pudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atha ussara pudi"/>
</div>
<div class="lyrico-lyrics-wrapper">Loosu-aana bejaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Loosu-aana bejaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Un lover-ah pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un lover-ah pol"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakku illa doctor-ru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakku illa doctor-ru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava happyila irukathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava happyila irukathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un future-ru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un future-ru"/>
</div>
</pre>
