---
title: "kottankoochikkithan song lyrics"
album: "Nadodi Kanavu"
artist: "Sabesh Murali"
lyricist: "Annamalai"
director: "Veera Selva"
path: "/albums/nadodi-kanavu-lyrics"
song: "Kottankoochikkithan"
image: ../../images/albumart/nadodi-kanavu.jpg
date: 2018-08-03
lang: tamil
youtubeLink: "https://www.youtube.com/embed/lOLsWnDLTZM"
type: "happy"
singers:
  - Mukesh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">kottan kuchiku than 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kottan kuchiku than "/>
</div>
<div class="lyrico-lyrics-wrapper">moonu kannu thanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moonu kannu thanda"/>
</div>
<div class="lyrico-lyrics-wrapper">namma saami ku than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="namma saami ku than"/>
</div>
<div class="lyrico-lyrics-wrapper">ngaana kannu thanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ngaana kannu thanda"/>
</div>
<div class="lyrico-lyrics-wrapper">kottan kuchiku than 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kottan kuchiku than "/>
</div>
<div class="lyrico-lyrics-wrapper">moonu kannu thanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moonu kannu thanda"/>
</div>
<div class="lyrico-lyrics-wrapper">namma saami ku than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="namma saami ku than"/>
</div>
<div class="lyrico-lyrics-wrapper">ngaana kannu thanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ngaana kannu thanda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">theeya maari velicham thantharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theeya maari velicham thantharu"/>
</div>
<div class="lyrico-lyrics-wrapper">moochu kaatha namaku vantharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moochu kaatha namaku vantharu"/>
</div>
<div class="lyrico-lyrics-wrapper">veesum kathu poovu vaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veesum kathu poovu vaasam"/>
</div>
<div class="lyrico-lyrics-wrapper">saami unga pera pesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saami unga pera pesum"/>
</div>
<div class="lyrico-lyrics-wrapper">oda thanni pola vantharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oda thanni pola vantharu"/>
</div>
<div class="lyrico-lyrics-wrapper">namma theva theerthu vacharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="namma theva theerthu vacharu"/>
</div>
<div class="lyrico-lyrics-wrapper">engalukku neenga matum pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engalukku neenga matum pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">inime enga kootam vaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inime enga kootam vaalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kottan kuchiku than 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kottan kuchiku than "/>
</div>
<div class="lyrico-lyrics-wrapper">moonu kannu thanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moonu kannu thanda"/>
</div>
<div class="lyrico-lyrics-wrapper">namma saami ku than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="namma saami ku than"/>
</div>
<div class="lyrico-lyrics-wrapper">ngaana kannu thanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ngaana kannu thanda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oruthanukku oruthiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oruthanukku oruthiya"/>
</div>
<div class="lyrico-lyrics-wrapper">ulaguthula vaalvome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulaguthula vaalvome"/>
</div>
<div class="lyrico-lyrics-wrapper">sinna veedu nanga than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sinna veedu nanga than"/>
</div>
<div class="lyrico-lyrics-wrapper">vaikka maatom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaikka maatom"/>
</div>
<div class="lyrico-lyrics-wrapper">sothu sugam serthu than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sothu sugam serthu than"/>
</div>
<div class="lyrico-lyrics-wrapper">mosam panna matome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mosam panna matome"/>
</div>
<div class="lyrico-lyrics-wrapper">aduthavanin kaasa than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aduthavanin kaasa than"/>
</div>
<div class="lyrico-lyrics-wrapper">thodave maatom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thodave maatom"/>
</div>
<div class="lyrico-lyrics-wrapper">o saamy ohyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="o saamy ohyo"/>
</div>
<div class="lyrico-lyrics-wrapper">thupaaki than kaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thupaaki than kaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">kootavum than paiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kootavum than paiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">kaaka suttu thinathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaaka suttu thinathu"/>
</div>
<div class="lyrico-lyrics-wrapper">antha kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antha kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">oosi mani paasiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oosi mani paasiya"/>
</div>
<div class="lyrico-lyrics-wrapper">unga kitta vithathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unga kitta vithathu"/>
</div>
<div class="lyrico-lyrics-wrapper">ellam ipo maruthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellam ipo maruthu"/>
</div>
<div class="lyrico-lyrics-wrapper">intha kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">natukulla enna ennamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="natukulla enna ennamo"/>
</div>
<div class="lyrico-lyrics-wrapper">thappu thanda panuranga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thappu thanda panuranga"/>
</div>
<div class="lyrico-lyrics-wrapper">engala pol yarum illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engala pol yarum illa"/>
</div>
<div class="lyrico-lyrics-wrapper">nalla pilla than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nalla pilla than"/>
</div>
<div class="lyrico-lyrics-wrapper">nermai thane enga sothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nermai thane enga sothu"/>
</div>
<div class="lyrico-lyrics-wrapper">theva illa sothu pathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theva illa sothu pathu"/>
</div>
<div class="lyrico-lyrics-wrapper">setha inga onnum illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="setha inga onnum illa"/>
</div>
<div class="lyrico-lyrics-wrapper">ellam mannu thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellam mannu thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">iya engaluku solli thantharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iya engaluku solli thantharu"/>
</div>
<div class="lyrico-lyrics-wrapper">enga vaalkaiya than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga vaalkaiya than"/>
</div>
<div class="lyrico-lyrics-wrapper">maathi vacharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maathi vacharu"/>
</div>
<div class="lyrico-lyrics-wrapper">iya engaluku solli thantharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iya engaluku solli thantharu"/>
</div>
<div class="lyrico-lyrics-wrapper">enga vaalkaiya than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga vaalkaiya than"/>
</div>
<div class="lyrico-lyrics-wrapper">maathi vacharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maathi vacharu"/>
</div>
<div class="lyrico-lyrics-wrapper">iya neenga than engalin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iya neenga than engalin"/>
</div>
<div class="lyrico-lyrics-wrapper">ratha ootame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ratha ootame"/>
</div>
<div class="lyrico-lyrics-wrapper">ada ungala nambi than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada ungala nambi than"/>
</div>
<div class="lyrico-lyrics-wrapper">enga kootame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga kootame"/>
</div>
<div class="lyrico-lyrics-wrapper">iya neenga than engalin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iya neenga than engalin"/>
</div>
<div class="lyrico-lyrics-wrapper">ratha ootame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ratha ootame"/>
</div>
<div class="lyrico-lyrics-wrapper">ada ungala nambi than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada ungala nambi than"/>
</div>
<div class="lyrico-lyrics-wrapper">enga kootame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga kootame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kottan kuchiku than 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kottan kuchiku than "/>
</div>
<div class="lyrico-lyrics-wrapper">moonu kannu thanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moonu kannu thanda"/>
</div>
<div class="lyrico-lyrics-wrapper">namma saami ku than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="namma saami ku than"/>
</div>
<div class="lyrico-lyrics-wrapper">ngaana kannu thanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ngaana kannu thanda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oru vaati porakuran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru vaati porakuran"/>
</div>
<div class="lyrico-lyrics-wrapper">oru vaati erakuran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru vaati erakuran"/>
</div>
<div class="lyrico-lyrics-wrapper">pogum pothu ennatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pogum pothu ennatha"/>
</div>
<div class="lyrico-lyrics-wrapper">kondu poran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kondu poran"/>
</div>
<div class="lyrico-lyrics-wrapper">parambaraiku sekuran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="parambaraiku sekuran"/>
</div>
<div class="lyrico-lyrics-wrapper">pavatha than panuran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pavatha than panuran"/>
</div>
<div class="lyrico-lyrics-wrapper">pasiyil alum aelaiku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pasiyil alum aelaiku"/>
</div>
<div class="lyrico-lyrics-wrapper">enna senjaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna senjaan"/>
</div>
<div class="lyrico-lyrics-wrapper">vetta veli veedu than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetta veli veedu than"/>
</div>
<div class="lyrico-lyrics-wrapper">vaanam athu koora than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaanam athu koora than"/>
</div>
<div class="lyrico-lyrics-wrapper">onnu sernthu vaalurom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="onnu sernthu vaalurom"/>
</div>
<div class="lyrico-lyrics-wrapper">kattu koopai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kattu koopai"/>
</div>
<div class="lyrico-lyrics-wrapper">natta nadu raathiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="natta nadu raathiri"/>
</div>
<div class="lyrico-lyrics-wrapper">patta pagal neramum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="patta pagal neramum"/>
</div>
<div class="lyrico-lyrics-wrapper">namala pola vaala than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="namala pola vaala than"/>
</div>
<div class="lyrico-lyrics-wrapper">yaarum illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaarum illa"/>
</div>
<div class="lyrico-lyrics-wrapper">engalukku satta thittam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engalukku satta thittam"/>
</div>
<div class="lyrico-lyrics-wrapper">epothum ullathu inga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="epothum ullathu inga"/>
</div>
<div class="lyrico-lyrics-wrapper">pulli maan kootathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pulli maan kootathula"/>
</div>
<div class="lyrico-lyrics-wrapper">narigal seruma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="narigal seruma"/>
</div>
<div class="lyrico-lyrics-wrapper">ethanaiyo paadu patom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethanaiyo paadu patom"/>
</div>
<div class="lyrico-lyrics-wrapper">mela vara kasta patom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mela vara kasta patom"/>
</div>
<div class="lyrico-lyrics-wrapper">yarum ena kandukala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yarum ena kandukala"/>
</div>
<div class="lyrico-lyrics-wrapper">neram vanthuruchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neram vanthuruchu"/>
</div>
<div class="lyrico-lyrics-wrapper">iya engalukku sollu thantharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iya engalukku sollu thantharu"/>
</div>
<div class="lyrico-lyrics-wrapper">enga vaalkaiya than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga vaalkaiya than"/>
</div>
<div class="lyrico-lyrics-wrapper">mathi vacharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mathi vacharu"/>
</div>
<div class="lyrico-lyrics-wrapper">iya engalukku sollu thantharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iya engalukku sollu thantharu"/>
</div>
<div class="lyrico-lyrics-wrapper">enga vaalkaiya than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga vaalkaiya than"/>
</div>
<div class="lyrico-lyrics-wrapper">mathi vacharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mathi vacharu"/>
</div>
<div class="lyrico-lyrics-wrapper">iya neenga than engalin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iya neenga than engalin"/>
</div>
<div class="lyrico-lyrics-wrapper">ratha ootame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ratha ootame"/>
</div>
<div class="lyrico-lyrics-wrapper">ada ungala nambi than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada ungala nambi than"/>
</div>
<div class="lyrico-lyrics-wrapper">enga kootame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga kootame"/>
</div>
<div class="lyrico-lyrics-wrapper">iya neenga than engalin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iya neenga than engalin"/>
</div>
<div class="lyrico-lyrics-wrapper">ratha ootame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ratha ootame"/>
</div>
<div class="lyrico-lyrics-wrapper">ada ungala nambi than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada ungala nambi than"/>
</div>
<div class="lyrico-lyrics-wrapper">enga kootame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga kootame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kottan kuchiku than 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kottan kuchiku than "/>
</div>
<div class="lyrico-lyrics-wrapper">moonu kannu thanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moonu kannu thanda"/>
</div>
<div class="lyrico-lyrics-wrapper">namma saami ku than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="namma saami ku than"/>
</div>
<div class="lyrico-lyrics-wrapper">ngaana kannu thanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ngaana kannu thanda"/>
</div>
<div class="lyrico-lyrics-wrapper">theeya maari velicham thantharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theeya maari velicham thantharu"/>
</div>
<div class="lyrico-lyrics-wrapper">moochu kaatha namaku vantharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moochu kaatha namaku vantharu"/>
</div>
<div class="lyrico-lyrics-wrapper">veesum kathu poovu vaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veesum kathu poovu vaasam"/>
</div>
<div class="lyrico-lyrics-wrapper">saami unga pera pesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saami unga pera pesum"/>
</div>
<div class="lyrico-lyrics-wrapper">oda thanni pola vantharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oda thanni pola vantharu"/>
</div>
<div class="lyrico-lyrics-wrapper">namma theva theerthu vacharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="namma theva theerthu vacharu"/>
</div>
<div class="lyrico-lyrics-wrapper">engalukku neenga matum pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engalukku neenga matum pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">inime enga kootam vaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inime enga kootam vaalum"/>
</div>
</pre>
