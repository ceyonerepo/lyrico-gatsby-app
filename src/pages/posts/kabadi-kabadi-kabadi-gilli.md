---
title: "kabadi kabadi kabadi song lyrics"
album: "Gilli"
artist: "Vidyasagar"
lyricist: "Maran"
director: "Dharani"
path: "/albums/gilli-lyrics"
song: "Kabadi Kabadi Kabadi"
image: ../../images/albumart/gilli.jpg
date: 2004-04-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/YY3_59CnaNY"
type: "mass"
singers:
  - Maran
  - Jayamoorty
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kabadi Kabadi Kabadi Kabadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabadi Kabadi Kabadi Kabadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kabadi Kabadi Kabadi Kabadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabadi Kabadi Kabadi Kabadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kabadi Kabadi Kabadi Kabadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabadi Kabadi Kabadi Kabadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kabadi Kabadi Kabadi Kabadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabadi Kabadi Kabadi Kabadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aanj Poonj
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanj Poonj"/>
</div>
<div class="lyrico-lyrics-wrapper">Santhanam Pottachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Santhanam Pottachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pulla Nee Mooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pulla Nee Mooru"/>
</div>
<div class="lyrico-lyrics-wrapper">Sulli Potta Kaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sulli Potta Kaadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kabadi Kabadi Kabadi Kabadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabadi Kabadi Kabadi Kabadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kabadi Kabadi Kabadi Kabadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabadi Kabadi Kabadi Kabadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Hooi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Hooi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aanj Poonj
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanj Poonj"/>
</div>
<div class="lyrico-lyrics-wrapper">Santhanam Pottachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Santhanam Pottachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pulla Nee Mooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pulla Nee Mooru"/>
</div>
<div class="lyrico-lyrics-wrapper">Sulli Potta Kaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sulli Potta Kaadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kabadi Kabadi Kabadi Kabadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabadi Kabadi Kabadi Kabadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kabadi Kabadi Kabadi Kabadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabadi Kabadi Kabadi Kabadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Hooi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Hooi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aanj Poonj
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanj Poonj"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanj Poonj
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanj Poonj"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kabadi Kabadi Kabadi Kabadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabadi Kabadi Kabadi Kabadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kabadi Kabadi Kabadi Kabadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabadi Kabadi Kabadi Kabadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kabadi Kabadi Kabadi Kabadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabadi Kabadi Kabadi Kabadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kabadi Kabadi Kabadi Kabadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabadi Kabadi Kabadi Kabadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kabadi Kabadi Kabadi Kabadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabadi Kabadi Kabadi Kabadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kabadi Kabadi Kabadi Kabadi Kabadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabadi Kabadi Kabadi Kabadi Kabadi"/>
</div>
</pre>
