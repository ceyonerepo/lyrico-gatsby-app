---
title: "tum jo aaye song lyrics"
album: "Once Upon a Time in Mumbai"
artist: "Pritam"
lyricist: "Irshaad Kamil"
director: "Milan Luthria"
path: "/albums/once-upon-a-time-in-mumbai-lyrics"
song: "Tum Jo Aaye"
image: ../../images/albumart/once-upon-a-time-in-mumbai.jpg
date: 2010-07-30
lang: hindi
youtubeLink: "https://www.youtube.com/embed/w3K8zrbiLpI"
type: "love"
singers:
  - Rahat Fateh Ali Khan
  - Tulsi Kumar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Paaya maine paaya tumhe rab ne milaya tumhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaya maine paaya tumhe rab ne milaya tumhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Honthon pe sajaya tumhe nag mein sajaya tumhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Honthon pe sajaya tumhe nag mein sajaya tumhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Paaya maine paaya tumhe sab se chupaya tumhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaya maine paaya tumhe sab se chupaya tumhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Sapna banaya tumhe neendon mein bulaya tumhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sapna banaya tumhe neendon mein bulaya tumhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tum jo aaye zindagi mein baat ban gayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tum jo aaye zindagi mein baat ban gayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ishq mazhab ishq meri jaat ban gayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishq mazhab ishq meri jaat ban gayi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paaya maine paaya tumhe rab ne milaya tumhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaya maine paaya tumhe rab ne milaya tumhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Honthon pe sajaya tumhe nag mein sajaya tumhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Honthon pe sajaya tumhe nag mein sajaya tumhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Paaya maine paaya tumhe sab se chupaya tumhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaya maine paaya tumhe sab se chupaya tumhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Sapna banaya tumhe neendon mein bulaya tumhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sapna banaya tumhe neendon mein bulaya tumhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho tum jo aaye zindagi mein baat ban gayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho tum jo aaye zindagi mein baat ban gayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sapne teri chahaton ke sapne teri chahaton ke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sapne teri chahaton ke sapne teri chahaton ke"/>
</div>
<div class="lyrico-lyrics-wrapper">Dekhti hoon ab kayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dekhti hoon ab kayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Din hai sona aur chaandi raat ban gayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Din hai sona aur chaandi raat ban gayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho tum jo aaye zindagi mein baat ban gayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho tum jo aaye zindagi mein baat ban gayi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paaya maine paaya tumhe rab ne milaya tumhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaya maine paaya tumhe rab ne milaya tumhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Honthon pe sajaya tumhe nag mein sajaya tumhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Honthon pe sajaya tumhe nag mein sajaya tumhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Paaya maine paaya tumhe sab se chupaya tumhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaya maine paaya tumhe sab se chupaya tumhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Sapna banaya tumhe neendon mein bulaya tumhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sapna banaya tumhe neendon mein bulaya tumhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chahaton ka maza faaslon mein nahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chahaton ka maza faaslon mein nahi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa chupa loon tumhe haunslon mein kahin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa chupa loon tumhe haunslon mein kahin"/>
</div>
<div class="lyrico-lyrics-wrapper">Sab se upar likha, hai tere naam ko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sab se upar likha, hai tere naam ko"/>
</div>
<div class="lyrico-lyrics-wrapper">Khwaishon se jude silsilon mein kahin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khwaishon se jude silsilon mein kahin"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Khwaishein milne ki tumse khwaishein milne ki tumse
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khwaishein milne ki tumse khwaishein milne ki tumse"/>
</div>
<div class="lyrico-lyrics-wrapper">Roz hoti hai nayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Roz hoti hai nayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Mere dil ki jeet meri maat ban gayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mere dil ki jeet meri maat ban gayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho tum jo aaye zindagi mein baat ban gayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho tum jo aaye zindagi mein baat ban gayi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paaya maine paaya tumhe rab ne milaya tumhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaya maine paaya tumhe rab ne milaya tumhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Honthon pe sajaya tumhe nag mein sajaya tumhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Honthon pe sajaya tumhe nag mein sajaya tumhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Paaya maine paaya tumhe sab se chupaya tumhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaya maine paaya tumhe sab se chupaya tumhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Sapna banaya tumhe neendon mein bulaya tumhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sapna banaya tumhe neendon mein bulaya tumhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Zindagi bewafa hai yeh maana magar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zindagi bewafa hai yeh maana magar"/>
</div>
<div class="lyrico-lyrics-wrapper">Chhod kar raah mein jaoge tum agar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chhod kar raah mein jaoge tum agar"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheen laaunga main aasman se tumhein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheen laaunga main aasman se tumhein"/>
</div>
<div class="lyrico-lyrics-wrapper">Soona hoga na yeh do dilon ka nagar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soona hoga na yeh do dilon ka nagar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raunke hain dil ke dar pe, raunke hain dil ke dar pe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raunke hain dil ke dar pe, raunke hain dil ke dar pe"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhadkanein hain surmayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhadkanein hain surmayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Meri qismat bhi tumhari, saath ban gayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meri qismat bhi tumhari, saath ban gayi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho tum jo aaye zindagi mein baat ban gayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho tum jo aaye zindagi mein baat ban gayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ishq mazhab ishq meri jaat ban gayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishq mazhab ishq meri jaat ban gayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sapne teri chahaton ke sapne teri chahaton ke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sapne teri chahaton ke sapne teri chahaton ke"/>
</div>
<div class="lyrico-lyrics-wrapper">Dekhti hoon ab kayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dekhti hoon ab kayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Din hai sona aur chaandi raat ban gayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Din hai sona aur chaandi raat ban gayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho tum jo aaye zindagi mein baat ban gayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho tum jo aaye zindagi mein baat ban gayi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paaya maine paaya tumhe rab ne milaya tumhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaya maine paaya tumhe rab ne milaya tumhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Honthon pe sajaya tumhe nag mein sajaya tumhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Honthon pe sajaya tumhe nag mein sajaya tumhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Paaya maine paaya tumhe sab se chupaya tumhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaya maine paaya tumhe sab se chupaya tumhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Sapna banaya tumhe neendon mein bulaya tumhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sapna banaya tumhe neendon mein bulaya tumhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paaya maine paaya tumhe rab ne milaya tumhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaya maine paaya tumhe rab ne milaya tumhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Honthon pe sajaya tumhe nag mein sajaya tumhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Honthon pe sajaya tumhe nag mein sajaya tumhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Paaya maine paaya tumhe sab se chupaya tumhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaya maine paaya tumhe sab se chupaya tumhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Sapna banaya tumhe neendon mein bulaya tumhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sapna banaya tumhe neendon mein bulaya tumhe"/>
</div>
</pre>
