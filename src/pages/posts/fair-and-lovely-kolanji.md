---
title: "fair and lovely song lyrics"
album: "Kolanji"
artist: "Natarajan Sankaran"
lyricist: "Naveen"
director: "Dhanaram Saravanan"
path: "/albums/kolanji-lyrics"
song: "Fair And Lovely"
image: ../../images/albumart/kolanji.jpg
date: 2019-07-26
lang: tamil
youtubeLink: "https://www.youtube.com/embed/KPmqpP01WNU"
type: "love"
singers:
  - Natarajan Sankaran
  - N.R. Priyanka
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Thuu maanakettavanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuu maanakettavanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhukku andha kinnathulaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhukku andha kinnathulaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilunthu irukalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilunthu irukalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho ho ho ho ho ho ho ho ok
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho ho ho ho ho ho ho ho ok"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho ho ho ho ho ho ho ho super
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho ho ho ho ho ho ho ho super"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pabapabapap
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pabapabapap"/>
</div>
<div class="lyrico-lyrics-wrapper">Paba paba paba paba paba paba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paba paba paba paba paba paba"/>
</div>
<div class="lyrico-lyrics-wrapper">Pabapabapap
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pabapabapap"/>
</div>
<div class="lyrico-lyrics-wrapper">Paba paba paba paba paba paba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paba paba paba paba paba paba"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Fair and lovely venaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fair and lovely venaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Fairever vena venaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fairever vena venaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee orasi pona pothum di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee orasi pona pothum di"/>
</div>
<div class="lyrico-lyrics-wrapper">Pabapabapap
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pabapabapap"/>
</div>
<div class="lyrico-lyrics-wrapper">Complan horlicks vena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Complan horlicks vena"/>
</div>
<div class="lyrico-lyrics-wrapper">Kashmir apple vena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kashmir apple vena"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee siricha pushti aaven di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee siricha pushti aaven di"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnala thookkam kettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnala thookkam kettu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullooril perum kettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullooril perum kettu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ottadha gaandham pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ottadha gaandham pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum kaanji ninen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum kaanji ninen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pabapabapap
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pabapabapap"/>
</div>
<div class="lyrico-lyrics-wrapper">Fair and lovely venaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fair and lovely venaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Fairever vena venaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fairever vena venaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee orasi pona pothum di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee orasi pona pothum di"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho ho ho ho ho ho ho ho ok
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho ho ho ho ho ho ho ho ok"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho ho ho ho ho ho ho ho super
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho ho ho ho ho ho ho ho super"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaaa aaa aaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaa aaa aaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Haa aa aaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa aa aaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaaliya katti kittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaliya katti kittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maalaiya maathi kittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maalaiya maathi kittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalyanam panni kittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalyanam panni kittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaal mela kaala pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaal mela kaala pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaaliya katti kittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaliya katti kittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maalaiya maathi kittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maalaiya maathi kittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalyanam panni kittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalyanam panni kittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaal mela kaala pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaal mela kaala pottu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maapula gettapula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maapula gettapula"/>
</div>
<div class="lyrico-lyrics-wrapper">Maama un kannam thottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maama un kannam thottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maarula saachikuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maarula saachikuven"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbaana metta pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbaana metta pottu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaal mela kaal pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaal mela kaal pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thol mela saachittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thol mela saachittu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Touring talkis povom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Touring talkis povom"/>
</div>
<div class="lyrico-lyrics-wrapper">Thattu vada vaangi thinbom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thattu vada vaangi thinbom"/>
</div>
<div class="lyrico-lyrics-wrapper">Bitu bita kaadhal padippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bitu bita kaadhal padippom"/>
</div>
<div class="lyrico-lyrics-wrapper">Matinee show kulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Matinee show kulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Micha socha velichathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Micha socha velichathula"/>
</div>
<div class="lyrico-lyrics-wrapper">Ichu ichu naama koduppom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ichu ichu naama koduppom"/>
</div>
</pre>
