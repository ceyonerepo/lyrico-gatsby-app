---
title: 'ennada life idhu song lyrics'
album: 'Oh My Kadavule'
artist: 'Leon James'
lyricist: 'Ko Sesha'
director: 'Ashwath Marimuthu'
path: '/albums/oh-my-kadavule-song-lyrics'
song: 'Ennada life idhu'
image: ../../images/albumart/oh-my-kadavule.jpg
date: 2020-02-14
lang: tamil
singers: 
- Santhosh Narayanan
youtubeLink: 'https://www.youtube.com/embed/h4KgNUzn-gM'
type: 'sad'
---

<pre class="lyrics-native">
    Coming Soon
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Alarm kaadhukkulla
<input type="checkbox" class="lyrico-select-lyric-line" value="Alarm kaadhukkulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooonu sangu oodha
<input type="checkbox" class="lyrico-select-lyric-line" value="Ooonu sangu oodha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thallaadi bed-ah vittu
<input type="checkbox" class="lyrico-select-lyric-line" value="Thallaadi bed-ah vittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mabbaaga ezhundhu poga
<input type="checkbox" class="lyrico-select-lyric-line" value="Mabbaaga ezhundhu poga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Toilet kulla kooda
<input type="checkbox" class="lyrico-select-lyric-line" value="Toilet kulla kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">To do list mind-il oda
<input type="checkbox" class="lyrico-select-lyric-line" value="To do list mind-il oda"/>
</div>
<div class="lyrico-lyrics-wrapper">Mokka poduthae
<input type="checkbox" class="lyrico-select-lyric-line" value="Mokka poduthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha vaazhkaiyae
<input type="checkbox" class="lyrico-select-lyric-line" value="Indha vaazhkaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Naasthaava thattil pottu
<input type="checkbox" class="lyrico-select-lyric-line" value="Naasthaava thattil pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Fast-aaga kottikkittu
<input type="checkbox" class="lyrico-select-lyric-line" value="Fast-aaga kottikkittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Bike oda sandai pottu
<input type="checkbox" class="lyrico-select-lyric-line" value="Bike oda sandai pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kick start-ah panni vittu
<input type="checkbox" class="lyrico-select-lyric-line" value="Kick start-ah panni vittu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">One way-il wrong-ah poondhu
<input type="checkbox" class="lyrico-select-lyric-line" value="One way-il wrong-ah poondhu"/>
</div>
<div class="lyrico-lyrics-wrapper">O word-il thittu vaangi
<input type="checkbox" class="lyrico-select-lyric-line" value="O word-il thittu vaangi"/>
</div>
<div class="lyrico-lyrics-wrapper">Mokka poduthae
<input type="checkbox" class="lyrico-select-lyric-line" value="Mokka poduthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha vaazhkaiyae
<input type="checkbox" class="lyrico-select-lyric-line" value="Indha vaazhkaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ennadaa life idhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Ennadaa life idhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennadaa life idhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Ennadaa life idhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennadaa life idhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Ennadaa life idhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennadaa life idhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Ennadaa life idhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh stuck aagi nikkudhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Oh stuck aagi nikkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh stuck aagi nikkudhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Oh stuck aagi nikkudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Oh ennadaa life idhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Oh ennadaa life idhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennadaa life idhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Ennadaa life idhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennadaa life idhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Ennadaa life idhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidivu kaalam porakka thaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Vidivu kaalam porakka thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Break the cycle-u
<input type="checkbox" class="lyrico-select-lyric-line" value="Break the cycle-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Cabin kulla boss-u
<input type="checkbox" class="lyrico-select-lyric-line" value="Cabin kulla boss-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Kooptu vachi senji
<input type="checkbox" class="lyrico-select-lyric-line" value="Kooptu vachi senji"/>
</div>
<div class="lyrico-lyrics-wrapper">Kevla padithi naalum
<input type="checkbox" class="lyrico-select-lyric-line" value="Kevla padithi naalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Cool-ah sirikkirom
<input type="checkbox" class="lyrico-select-lyric-line" value="Cool-ah sirikkirom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Increment-ah ketta
<input type="checkbox" class="lyrico-select-lyric-line" value="Increment-ah ketta"/>
</div>
<div class="lyrico-lyrics-wrapper">Next year undu
<input type="checkbox" class="lyrico-select-lyric-line" value="Next year undu"/>
</div>
<div class="lyrico-lyrics-wrapper">HR solla kettu
<input type="checkbox" class="lyrico-select-lyric-line" value="HR solla kettu"/>
</div>
<div class="lyrico-lyrics-wrapper">Suththi alaiyarom
<input type="checkbox" class="lyrico-select-lyric-line" value="Suththi alaiyarom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Healthu gone-u gone-u
<input type="checkbox" class="lyrico-select-lyric-line" value="Healthu gone-u gone-u"/>
</div>
<div class="lyrico-lyrics-wrapper">BMI-um kenja
<input type="checkbox" class="lyrico-select-lyric-line" value="BMI-um kenja"/>
</div>
<div class="lyrico-lyrics-wrapper">Wealth enga maamu
<input type="checkbox" class="lyrico-select-lyric-line" value="Wealth enga maamu"/>
</div>
<div class="lyrico-lyrics-wrapper">EMI-ah minja
<input type="checkbox" class="lyrico-select-lyric-line" value="EMI-ah minja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">9 <div class="lyrico-lyrics-wrapper">to 6 lifeu</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="to <div class="lyrico-lyrics-wrapper">6 lifeu"/></div>
</div>
<div class="lyrico-lyrics-wrapper">Nachchu pannum wifeu
<input type="checkbox" class="lyrico-select-lyric-line" value="Nachchu pannum wifeu"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendum onnu dhaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Rendum onnu dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Oosi pona bunnu naan
<input type="checkbox" class="lyrico-select-lyric-line" value="Oosi pona bunnu naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ennadaa life idhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Ennadaa life idhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennadaa life idhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Ennadaa life idhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa…aa..oh ohhh
<input type="checkbox" class="lyrico-select-lyric-line" value="Aa…aa..oh ohhh"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennadaa life idhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Ennadaa life idhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh stuck aagi nikkudhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Oh stuck aagi nikkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa…aa..oh ohhh
<input type="checkbox" class="lyrico-select-lyric-line" value="Aa…aa..oh ohhh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Oh ennadaa life idhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Oh ennadaa life idhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennadaa life idhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Ennadaa life idhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa…aa..oh ohhh
<input type="checkbox" class="lyrico-select-lyric-line" value="Aa…aa..oh ohhh"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidivu kaalam porakka thaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Vidivu kaalam porakka thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Break the cycle-u
<input type="checkbox" class="lyrico-select-lyric-line" value="Break the cycle-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Neruppu maadhiri
<input type="checkbox" class="lyrico-select-lyric-line" value="Neruppu maadhiri"/>
</div>
<div class="lyrico-lyrics-wrapper">Velai seivom kumaaru
<input type="checkbox" class="lyrico-select-lyric-line" value="Velai seivom kumaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Thyagam thaan unnai uyarthum
<input type="checkbox" class="lyrico-select-lyric-line" value="Thyagam thaan unnai uyarthum"/>
</div>
(<div class="lyrico-lyrics-wrapper">Dialogue)
<input type="checkbox" class="lyrico-select-lyric-line" value="Dialogue)"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Enna enna enna enna..
<input type="checkbox" class="lyrico-select-lyric-line" value="Enna enna enna enna.."/>
</div>
<div class="lyrico-lyrics-wrapper">Ennadaa life idhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Ennadaa life idhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Indha vethu job ini seiya
<input type="checkbox" class="lyrico-select-lyric-line" value="Indha vethu job ini seiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaalu aattum dog-u naan illa
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaalu aattum dog-u naan illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Rosham yerum neram
<input type="checkbox" class="lyrico-select-lyric-line" value="Rosham yerum neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Phone-ah paathaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Phone-ah paathaa"/>
</div>
  <div class="lyrico-lyrics-wrapper">Your salary has been credited
<input type="checkbox" class="lyrico-select-lyric-line" value="Your salary has been credited"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ennadaa life idhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Ennadaa life idhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennadaa life idhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Ennadaa life idhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa…aa..oh ohhh
<input type="checkbox" class="lyrico-select-lyric-line" value="Aa…aa..oh ohhh"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennadaa life idhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Ennadaa life idhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh stuck aagi nikkudhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Oh stuck aagi nikkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa…aa..oh ohhh
<input type="checkbox" class="lyrico-select-lyric-line" value="Aa…aa..oh ohhh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Oh ennadaa life idhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Oh ennadaa life idhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennadaa life idhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Ennadaa life idhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa…aa..oh ohhh
<input type="checkbox" class="lyrico-select-lyric-line" value="Aa…aa..oh ohhh"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidivu kaalam porakka thaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Vidivu kaalam porakka thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Break the cycle-u
<input type="checkbox" class="lyrico-select-lyric-line" value="Break the cycle-u"/>
</div>
</pre>