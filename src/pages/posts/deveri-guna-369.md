---
title: "deveri song lyrics"
album: "Guna 369"
artist: "Chaitan Bharadwaj"
lyricist: "Ramajogayya Sastry"
director: "Arjun Jandyala"
path: "/albums/guna-369-lyrics"
song: "Deveri"
image: ../../images/albumart/guna-369.jpg
date: 2019-08-02
lang: telugu
youtubeLink: "https://www.youtube.com/embed/QuCXFOUBSV4"
type: "love"
singers:
  - Gowtham Bharadwaj
  - Ramya Behara
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Deveri Nuvve Naa Oopiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Deveri Nuvve Naa Oopiri"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Kosamm Puttadi Prema
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kosamm Puttadi Prema"/>
</div>
<div class="lyrico-lyrics-wrapper">Pujaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pujaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Deveri Janma Needhe Mari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Deveri Janma Needhe Mari"/>
</div>
<div class="lyrico-lyrics-wrapper">Nithone Vandella Poola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nithone Vandella Poola"/>
</div>
<div class="lyrico-lyrics-wrapper">Radhaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Radhaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvante Nenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvante Nenu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Vente Nenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vente Nenu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nijamidhi Nijam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijamidhi Nijam"/>
</div>
<div class="lyrico-lyrics-wrapper">Iruvuramokare Manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruvuramokare Manam"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheevenalidi Thadhasthani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheevenalidi Thadhasthani"/>
</div>
<div class="lyrico-lyrics-wrapper">Palikene Shubha Samayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palikene Shubha Samayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanuvula Sagam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanuvula Sagam "/>
</div>
<div class="lyrico-lyrics-wrapper">Okatiga Vodhige Kshanamm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okatiga Vodhige Kshanamm"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Madhuvula Pedhaalake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Madhuvula Pedhaalake"/>
</div>
<div class="lyrico-lyrics-wrapper">Parinayam Madhura Mayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parinayam Madhura Mayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu Naku Viluvaina Kaanuka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu Naku Viluvaina Kaanuka"/>
</div>
<div class="lyrico-lyrics-wrapper">Ledhu Jeevitham Nuvantu Leka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ledhu Jeevitham Nuvantu Leka"/>
</div>
<div class="lyrico-lyrics-wrapper">Undalenu Nenu Ninu Choodaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undalenu Nenu Ninu Choodaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanthi Neeve Kadha Naayakaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanthi Neeve Kadha Naayakaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvve Naku Pratyekam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvve Naku Pratyekam"/>
</div>
<div class="lyrico-lyrics-wrapper">Neetho Undhi Naa Lokam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neetho Undhi Naa Lokam"/>
</div>
<div class="lyrico-lyrics-wrapper">Lenidhedhi Ledhe Ika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lenidhedhi Ledhe Ika"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu Thula Maasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu Thula Maasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Neetho Sawasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neetho Sawasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavali Kada Dhaakaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavali Kada Dhaakaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nijamidhi Nijam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijamidhi Nijam"/>
</div>
<div class="lyrico-lyrics-wrapper">Iruvuramokare Manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruvuramokare Manam"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheevenalidi Thadhasthani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheevenalidi Thadhasthani"/>
</div>
<div class="lyrico-lyrics-wrapper">Palikene Shubha Samayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palikene Shubha Samayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanuvula Sagam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanuvula Sagam "/>
</div>
<div class="lyrico-lyrics-wrapper">Okatiga Vodhige Kshanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okatiga Vodhige Kshanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Madhuvula Pedhaalake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Madhuvula Pedhaalake"/>
</div>
<div class="lyrico-lyrics-wrapper">Parinayam Madhura Mayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parinayam Madhura Mayam"/>
</div>
</pre>
