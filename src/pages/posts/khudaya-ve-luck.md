---
title: "khudaya ve song lyrics"
album: "Luck"
artist: "Salim–Sulaiman - Amar Mohile"
lyricist: "Shabbir Ahmed"
director: "Soham Shah"
path: "/albums/luck-lyrics"
song: "Khudaya Ve"
image: ../../images/albumart/luck.jpg
date: 2009-07-24
lang: hindi
youtubeLink: "https://www.youtube.com/embed/54z7T3SIpYY"
type: "love"
singers:
  - Salim Merchant
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Khudaya ve haiii aaahh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khudaya ve haiii aaahh"/>
</div>
<div class="lyrico-lyrics-wrapper">Ishq hai kaisa yeh ajeeb
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishq hai kaisa yeh ajeeb"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Khudaya ve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khudaya ve"/>
</div>
<div class="lyrico-lyrics-wrapper">Ishq hai kaisa yeh ajeeb
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishq hai kaisa yeh ajeeb"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dil ke kareeb laaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil ke kareeb laaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Dil ka naseeb re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil ka naseeb re"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dil ke kareeb laaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil ke kareeb laaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Dil ka naseeb re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil ka naseeb re"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Khudaya ve haiii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khudaya ve haiii"/>
</div>
<div class="lyrico-lyrics-wrapper">Ishq hai kaisa yeh ajeeb re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishq hai kaisa yeh ajeeb re"/>
</div>
<div class="lyrico-lyrics-wrapper">Khudaya ve haiii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khudaya ve haiii"/>
</div>
<div class="lyrico-lyrics-wrapper">Ishq hai kaisa yeh ajeeb re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishq hai kaisa yeh ajeeb re"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaaa…aaaa……tana re tana….derena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaa…aaaa……tana re tana….derena"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaaa…aaaa……tana re tana….derena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaa…aaaa……tana re tana….derena"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaaa…aaaa……tana re tana….derena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaa…aaaa……tana re tana….derena"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aakhon se khawab ruthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aakhon se khawab ruthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Apno ke saath choote
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Apno ke saath choote"/>
</div>
<div class="lyrico-lyrics-wrapper">Tapti hue rahon mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tapti hue rahon mein"/>
</div>
<div class="lyrico-lyrics-wrapper">Pairon ke chaale phoote
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pairon ke chaale phoote"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aakhon se khawab ruthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aakhon se khawab ruthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Apno ke saath choote
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Apno ke saath choote"/>
</div>
<div class="lyrico-lyrics-wrapper">Tapti hue rahon mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tapti hue rahon mein"/>
</div>
<div class="lyrico-lyrics-wrapper">Pairon ke chaale phoote
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pairon ke chaale phoote"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pyaase tadap rahe hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pyaase tadap rahe hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Saahil kareeb re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saahil kareeb re"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pyaase tadap rahe hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pyaase tadap rahe hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Saahil kareeb re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saahil kareeb re"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Khudaya ve haiii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khudaya ve haiii"/>
</div>
<div class="lyrico-lyrics-wrapper">Ishq hai kaisa yeh ajeeb re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishq hai kaisa yeh ajeeb re"/>
</div>
<div class="lyrico-lyrics-wrapper">Khudaya ve haiii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khudaya ve haiii"/>
</div>
<div class="lyrico-lyrics-wrapper">Ishq hai kaisa yeh ajeeb re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishq hai kaisa yeh ajeeb re"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ni sa re ni sa re re 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ni sa re ni sa re re "/>
</div>
<div class="lyrico-lyrics-wrapper">ga re ga re re ga re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ga re ga re re ga re"/>
</div>
<div class="lyrico-lyrics-wrapper">Ma ga re sa ma 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ma ga re sa ma "/>
</div>
<div class="lyrico-lyrics-wrapper">ga re sa ma ga re sa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ga re sa ma ga re sa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aansu namak se lage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aansu namak se lage"/>
</div>
<div class="lyrico-lyrics-wrapper">Rishte hai kachhe dhaage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rishte hai kachhe dhaage"/>
</div>
<div class="lyrico-lyrics-wrapper">Raite pe apne saaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raite pe apne saaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Khud se kyu durr bhaage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khud se kyu durr bhaage"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aansu namak se lage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aansu namak se lage"/>
</div>
<div class="lyrico-lyrics-wrapper">Rishte hai kachhe dhaage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rishte hai kachhe dhaage"/>
</div>
<div class="lyrico-lyrics-wrapper">Raite pe apne saaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raite pe apne saaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Khud se kyu durr bhaage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khud se kyu durr bhaage"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kheech ke humko laaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kheech ke humko laaye"/>
</div>
<div class="lyrico-lyrics-wrapper">kahan pe rakeeb re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kahan pe rakeeb re"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kheech ke humko laaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kheech ke humko laaye"/>
</div>
<div class="lyrico-lyrics-wrapper">kahan pe rakeeb re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kahan pe rakeeb re"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Khudaya ve haiii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khudaya ve haiii"/>
</div>
<div class="lyrico-lyrics-wrapper">Ishq hai kaisa yeh ajeeb re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishq hai kaisa yeh ajeeb re"/>
</div>
<div class="lyrico-lyrics-wrapper">Khudaya ve haiii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khudaya ve haiii"/>
</div>
<div class="lyrico-lyrics-wrapper">Ishq hai kaisa yeh ajeeb re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishq hai kaisa yeh ajeeb re"/>
</div>
<div class="lyrico-lyrics-wrapper">Dil ke karib aaya dil ka naseeb ve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil ke karib aaya dil ka naseeb ve"/>
</div>
</pre>
