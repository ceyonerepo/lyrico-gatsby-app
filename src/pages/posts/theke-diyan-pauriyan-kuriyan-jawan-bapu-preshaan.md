---
title: "theke diyan pauriyan song lyrics"
album: "Kuriyan Jawan Bapu Preshaan"
artist: "Laddi Gill"
lyricist: "Talbi"
director: "Avtar Singh"
path: "/albums/kuriyan-jawan-bapu-preshaan-lyrics"
song: "Theke Diyan Pauriyan"
image: ../../images/albumart/kuriyan-jawan-bapu-preshaan.jpg
date: 2021-04-16
lang: punjabi
youtubeLink: "https://www.youtube.com/embed/MugC_KUd1Lc"
type: "Love"
singers:
  - Talbi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
7 <div class="lyrico-lyrics-wrapper">saalan da pyaar 7 minute'an ch bhulaagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saalan da pyaar 7 minute'an ch bhulaagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Saade challe di jagah te ring heereyan di aa gayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saade challe di jagah te ring heereyan di aa gayi"/>
</div>
7 <div class="lyrico-lyrics-wrapper">saalan da pyaar 7 minute'an ch bhulaagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saalan da pyaar 7 minute'an ch bhulaagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Saade challe di jagah te ring heereyan di aa gayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saade challe di jagah te ring heereyan di aa gayi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aake 4g zamane vich todgi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aake 4g zamane vich todgi"/>
</div>
<div class="lyrico-lyrics-wrapper">Lagi si telephone diyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lagi si telephone diyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Jatt theke diyan pauriyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jatt theke diyan pauriyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ton utre tu utre saloon diyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ton utre tu utre saloon diyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ni jatt theke diyan pauriyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ni jatt theke diyan pauriyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ton utre tu utre saloon diyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ton utre tu utre saloon diyan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
14 <div class="lyrico-lyrics-wrapper">khare kehan desi naate
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="khare kehan desi naate"/>
</div>
5 <div class="lyrico-lyrics-wrapper">killo kukdu banaya balliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="killo kukdu banaya balliye"/>
</div>
<div class="lyrico-lyrics-wrapper">Tight hoke pher bhanniya main bottlan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tight hoke pher bhanniya main bottlan"/>
</div>
<div class="lyrico-lyrics-wrapper">Te gussa tera laaya balliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Te gussa tera laaya balliye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
14 <div class="lyrico-lyrics-wrapper">khare kehan desi naate
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="khare kehan desi naate"/>
</div>
5 <div class="lyrico-lyrics-wrapper">killo kukdu banaya balliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="killo kukdu banaya balliye"/>
</div>
<div class="lyrico-lyrics-wrapper">Tight hoke pher bhanniya main bottlan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tight hoke pher bhanniya main bottlan"/>
</div>
<div class="lyrico-lyrics-wrapper">Te gussa tera laaya balliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Te gussa tera laaya balliye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tu tan laagi zamaan end
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu tan laagi zamaan end"/>
</div>
<div class="lyrico-lyrics-wrapper">Kehndi rahange friend
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kehndi rahange friend"/>
</div>
<div class="lyrico-lyrics-wrapper">Rahange friend tu tan laagi zamaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rahange friend tu tan laagi zamaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Utto kaam pehlan deke gallan kargi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Utto kaam pehlan deke gallan kargi"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu zakhma te loon diyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu zakhma te loon diyan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jatt theke diyan pauriyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jatt theke diyan pauriyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ton utre tu utre saloon diyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ton utre tu utre saloon diyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ni jatt theke diyan pauriyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ni jatt theke diyan pauriyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ton utre tu utre saloon diyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ton utre tu utre saloon diyan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pehle 4 din roya pher chupchap hoya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pehle 4 din roya pher chupchap hoya"/>
</div>
<div class="lyrico-lyrics-wrapper">Ant mileya sukoon tera challa modke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ant mileya sukoon tera challa modke"/>
</div>
<div class="lyrico-lyrics-wrapper">Sach jaani ohna laggi da swaad nai si aaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sach jaani ohna laggi da swaad nai si aaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Jihna aa gaya swaad teri sonh tod ke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jihna aa gaya swaad teri sonh tod ke"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sach jaani ohna laggi da swaad nai si aaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sach jaani ohna laggi da swaad nai si aaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Jihna aa gaya swaad teri sonh tod ke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jihna aa gaya swaad teri sonh tod ke"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hun shukar manava kalla behke hassi jawaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hun shukar manava kalla behke hassi jawaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Behke hassi jawaan, hun shukar manava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Behke hassi jawaan, hun shukar manava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kehde talbi banaake paunda unnatiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kehde talbi banaake paunda unnatiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Ni teri jehi toom diyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ni teri jehi toom diyan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jatt theke diyan pauriyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jatt theke diyan pauriyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ton utre tu utre saloon diyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ton utre tu utre saloon diyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ni jatt theke diyan pauriyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ni jatt theke diyan pauriyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ton utre tu utre saloon diyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ton utre tu utre saloon diyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ni jatt theke diyan pauriyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ni jatt theke diyan pauriyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ton utre tu utre saloon diyan hoye hoye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ton utre tu utre saloon diyan hoye hoye"/>
</div>
</pre>
