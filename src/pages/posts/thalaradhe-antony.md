---
title: "thalaradhe song lyrics"
album: "Antony"
artist: "Sivatmikha"
lyricist: "Sivam"
director: "Kutti Kumar"
path: "/albums/antony-lyrics"
song: "Thalaradhe"
image: ../../images/albumart/antony.jpg
date: 2018-06-01
lang: tamil
youtubeLink: 
type: "motivational"
singers:
  - Sivam
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">kathari aluthidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathari aluthidum"/>
</div>
<div class="lyrico-lyrics-wrapper">malaiyin thuligalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malaiyin thuligalil"/>
</div>
<div class="lyrico-lyrics-wrapper">nimidam poliyave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nimidam poliyave"/>
</div>
<div class="lyrico-lyrics-wrapper">thalaraathe thalaraathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalaraathe thalaraathe"/>
</div>
<div class="lyrico-lyrics-wrapper">thalaraathe thalaraathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalaraathe thalaraathe"/>
</div>
<div class="lyrico-lyrics-wrapper">unnal mudiyum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnal mudiyum "/>
</div>
<div class="lyrico-lyrics-wrapper">unnal thaan mudiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnal thaan mudiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu kadavul pota kanaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu kadavul pota kanaku"/>
</div>
<div class="lyrico-lyrics-wrapper">innum kadal pola irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="innum kadal pola irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu kadavul pota kanaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu kadavul pota kanaku"/>
</div>
<div class="lyrico-lyrics-wrapper">innum kadal pola irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="innum kadal pola irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">pola irukku pola irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pola irukku pola irukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ivanidam karunai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ivanidam karunai"/>
</div>
<div class="lyrico-lyrics-wrapper">kooda kenjum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kooda kenjum"/>
</div>
<div class="lyrico-lyrics-wrapper">sidamaagi narambil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sidamaagi narambil"/>
</div>
<div class="lyrico-lyrics-wrapper">theepori kilambum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theepori kilambum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">maranam unnai aatpinum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maranam unnai aatpinum"/>
</div>
<div class="lyrico-lyrics-wrapper">miralaathe kalavu pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="miralaathe kalavu pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">neram ithu alla eluthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neram ithu alla eluthu"/>
</div>
<div class="lyrico-lyrics-wrapper">veliye vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veliye vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ethirigalai podi aaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethirigalai podi aaka"/>
</div>
<div class="lyrico-lyrics-wrapper">podi aaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="podi aaka"/>
</div>
<div class="lyrico-lyrics-wrapper">kathiravanin oliyaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathiravanin oliyaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">oliyaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oliyaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">tharmathin thalai kaaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tharmathin thalai kaaka"/>
</div>
<div class="lyrico-lyrics-wrapper">unmai ennuyir kaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unmai ennuyir kaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">uyirkaaga thalaraathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyirkaaga thalaraathe"/>
</div>
<div class="lyrico-lyrics-wrapper">thalaraathe thalaraathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalaraathe thalaraathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thalaraathe ehehehe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalaraathe ehehehe"/>
</div>
<div class="lyrico-lyrics-wrapper">thalaraathe thalaraathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalaraathe thalaraathe"/>
</div>
<div class="lyrico-lyrics-wrapper">thalaraathe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalaraathe "/>
</div>
</pre>
