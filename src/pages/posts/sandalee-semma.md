---
title: "sandalee song lyrics"
album: "Semma"
artist: "G.V. Prakash Kumar"
lyricist: "Yugabharathi"
director: "Vallikanthan"
path: "/albums/semma-lyrics"
song: "Sandalee"
image: ../../images/albumart/semma.jpg
date: 2018-05-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Q-A62p6sJ5E"
type: "love"
singers:
  - Velmurugan
  - VM Mahalingam
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Sandalee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandalee"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Asaththura Azhagula Lesaagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Asaththura Azhagula Lesaagi"/>
</div>
<div class="lyrico-lyrics-wrapper">En Anthipagal Aththanaiyum Thoosaagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Anthipagal Aththanaiyum Thoosaagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Paya Kedakkura Tharaiyila Peesaagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paya Kedakkura Tharaiyila Peesaagi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sandalee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandalee"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Sirippula Parakkura Thoosaagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Sirippula Parakkura Thoosaagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Na Sevuththula Vitterunja Kaasaagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na Sevuththula Vitterunja Kaasaagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodi Pudikkuren Nenappula Maasaagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodi Pudikkuren Nenappula Maasaagi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaiyum Kaalum Unna Kandu Odavilla di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyum Kaalum Unna Kandu Odavilla di"/>
</div>
<div class="lyrico-lyrics-wrapper">Ra Vanthumkooda Kannurendum Moodavilladi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ra Vanthumkooda Kannurendum Moodavilladi"/>
</div>
<div class="lyrico-lyrics-wrapper">Paavi Pulla Enna Neeyum Aadavittadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paavi Pulla Enna Neeyum Aadavittadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaai Paasaththoda Nenja Vanthu Mothiputtadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaai Paasaththoda Nenja Vanthu Mothiputtadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theriyaladi Puriyaladi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theriyaladi Puriyaladi"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Iruvizhi Manusana Iduppula Thookkuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Iruvizhi Manusana Iduppula Thookkuthadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sandalee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandalee"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Asaththura Azhagula Lesaagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Asaththura Azhagula Lesaagi"/>
</div>
<div class="lyrico-lyrics-wrapper">En Anthipagal Aththanaiyum Thoosaagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Anthipagal Aththanaiyum Thoosaagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Paya Kedakkura Tharaiyila Peesaagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paya Kedakkura Tharaiyila Peesaagi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sandaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Sirippula Parakkura Thoosaagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Sirippula Parakkura Thoosaagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Na Sevuththula Vitterunja Kaasaagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na Sevuththula Vitterunja Kaasaagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodi Pudikkuren Nenappula Maasaagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodi Pudikkuren Nenappula Maasaagi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Munnaala Nee Vantha Ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munnaala Nee Vantha Ivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Mukka Mozham Poovaaguren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mukka Mozham Poovaaguren"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollama Nee Pona Ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollama Nee Pona Ivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pallaankuzhi Kaayaaguren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pallaankuzhi Kaayaaguren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Appuraane Unna Paaththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appuraane Unna Paaththu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ammi Vechcha Thengasila Nasungiputten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammi Vechcha Thengasila Nasungiputten"/>
</div>
<div class="lyrico-lyrics-wrapper">Moththama Nee Enna Sera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moththama Nee Enna Sera"/>
</div>
<div class="lyrico-lyrics-wrapper">Niththam Nenappukulla Kasangiputta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niththam Nenappukulla Kasangiputta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sottavaala Kutti Naanum Soru Thingala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sottavaala Kutti Naanum Soru Thingala"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Thottu Pesa Rendu Naala Veedu Thangala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Thottu Pesa Rendu Naala Veedu Thangala"/>
</div>
<div class="lyrico-lyrics-wrapper">Muttimodhum Un Nenappu Reelu Suththala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muttimodhum Un Nenappu Reelu Suththala"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Yettipova Seththu Poven Kaadhu Kuththala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Yettipova Seththu Poven Kaadhu Kuththala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadha Vidala Kalangidala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadha Vidala Kalangidala"/>
</div>
<div class="lyrico-lyrics-wrapper">Na Unna Vida Oruththiya Idhuvara Paaththidala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na Unna Vida Oruththiya Idhuvara Paaththidala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sandalee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandalee"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Asaththura Alagula Lesaagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Asaththura Alagula Lesaagi"/>
</div>
<div class="lyrico-lyrics-wrapper">En Anthipagal Aththanaiyum Thoosaagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Anthipagal Aththanaiyum Thoosaagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Paya Kedakkura Tharaiyila Peesaagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paya Kedakkura Tharaiyila Peesaagi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sandaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Sirippula Parakkura Thoosaagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Sirippula Parakkura Thoosaagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Na Sevuththula Vitterunja Kaasaagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na Sevuththula Vitterunja Kaasaagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodi Pudikkuraen Nenappula Maasaagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodi Pudikkuraen Nenappula Maasaagi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kattaanthara Unnaalathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattaanthara Unnaalathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kammaakkara Neeraaguren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kammaakkara Neeraaguren"/>
</div>
<div class="lyrico-lyrics-wrapper">Senthaamara Kannaala Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senthaamara Kannaala Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pongamale Soraaguren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pongamale Soraaguren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nongupola Enna Seevum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nongupola Enna Seevum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannukulla Kattipotta Aduchuputta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannukulla Kattipotta Aduchuputta"/>
</div>
<div class="lyrico-lyrics-wrapper">Uchchivaana Ninna Aala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uchchivaana Ninna Aala"/>
</div>
<div class="lyrico-lyrics-wrapper">Orey Othattasaippil Ulukkiputta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orey Othattasaippil Ulukkiputta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alliraani Enna Yendi Aattivaikkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alliraani Enna Yendi Aattivaikkara"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Anbil Enna Saavikoththa Maatti Vaikkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Anbil Enna Saavikoththa Maatti Vaikkara"/>
</div>
<div class="lyrico-lyrics-wrapper">Pullimaana Sekku Maadaa Maththi Vaikara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pullimaana Sekku Maadaa Maththi Vaikara"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Velli Kaasa Enna Yeno Seththi Vaikkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Velli Kaasa Enna Yeno Seththi Vaikkara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pazhamvidura Pazhagidura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pazhamvidura Pazhagidura"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Pagalaiyum Iravaiyum Padaiyalu Pottudura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Pagalaiyum Iravaiyum Padaiyalu Pottudura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sandali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandali"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Asaththura Azhagula Lesaagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Asaththura Azhagula Lesaagi"/>
</div>
<div class="lyrico-lyrics-wrapper">En Anthipagal Aththanaiyum Thoosaagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Anthipagal Aththanaiyum Thoosaagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Paya Kedakkura Tharaiyila Peesaagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paya Kedakkura Tharaiyila Peesaagi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sandali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandali"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Sirippula Parakura Thoosagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Sirippula Parakura Thoosagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Na Sevuthula Viterunja Kaasagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na Sevuthula Viterunja Kaasagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodi Pudikuren Nenapula Masaagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodi Pudikuren Nenapula Masaagi"/>
</div>
</pre>
