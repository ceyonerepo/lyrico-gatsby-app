---
title: "swasamae song lyrics"
album: "O2"
artist: "Vishal Chandrasekar"
lyricist: "Mohan Rajan"
director: "GS Viknesh"
path: "/albums/o2-lyrics"
song: "Swasamae"
image: ../../images/albumart/o2.jpg
date: 2022-06-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/-m-RbdLGmP0"
type: "melody"
singers:
  - Brindha Sivakumar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Enadhu karuvil pootha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enadhu karuvil pootha"/>
</div>
<div class="lyrico-lyrics-wrapper">Or ilaya nilavu nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Or ilaya nilavu nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Enadhu vizhiyil vaazhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enadhu vizhiyil vaazhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhirkaala kanavu nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhirkaala kanavu nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ulagilae ethavumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagilae ethavumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Muzhumai illaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muzhumai illaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhigalin nirangalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhigalin nirangalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Karuppu vellaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuppu vellaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Puyal kaatraai oru thedal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puyal kaatraai oru thedal"/>
</div>
<div class="lyrico-lyrics-wrapper">Indru thendral aanadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indru thendral aanadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Swasamae swasamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Swasamae swasamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedal indru mudindhadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedal indru mudindhadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidiyalai sernthu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidiyalai sernthu "/>
</div>
<div class="lyrico-lyrics-wrapper">naanum kaanavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naanum kaanavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Swasamae swasamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Swasamae swasamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirai theendum swasamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirai theendum swasamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakini veesum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakini veesum "/>
</div>
<div class="lyrico-lyrics-wrapper">kaatril pirakkumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatril pirakkumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhagin vadivam nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagin vadivam nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhan uyirin uruvam nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan uyirin uruvam nee"/>
</div>
<div class="lyrico-lyrics-wrapper">En idhazin oram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En idhazin oram"/>
</div>
<div class="lyrico-lyrics-wrapper">Malarum sirippu nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malarum sirippu nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Valarum kavidhai nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valarum kavidhai nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhan vaazhvin porulum nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan vaazhvin porulum nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Kann urangum pozhuthil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kann urangum pozhuthil"/>
</div>
<div class="lyrico-lyrics-wrapper">Malarum kanavu nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malarum kanavu nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha uravai pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha uravai pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagil veru urave illayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagil veru urave illayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyara parakkum paravai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyara parakkum paravai"/>
</div>
<div class="lyrico-lyrics-wrapper">Namakkum ellai illayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namakkum ellai illayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai eendra pozhudhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai eendra pozhudhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Meendum unara pogiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meendum unara pogiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Swasamae swasamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Swasamae swasamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedal indru mudindhadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedal indru mudindhadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidiyalai sernthu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidiyalai sernthu "/>
</div>
<div class="lyrico-lyrics-wrapper">naanum kaanavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naanum kaanavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Swasamae swasamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Swasamae swasamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirai theendum swasamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirai theendum swasamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakini veesum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakini veesum "/>
</div>
<div class="lyrico-lyrics-wrapper">kaatril pirakkumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatril pirakkumae"/>
</div>
</pre>
