---
title: "konjam veyilaga song lyrics"
album: "Margazhi 16"
artist: "E.K. Bobby"
lyricist: "Francis Kiruba"
director: "K. Stephen"
path: "/albums/margazhi-16-lyrics"
song: "Konjam Veyilaga"
image: ../../images/albumart/margazhi-16.jpg
date: 2011-02-18
lang: tamil
youtubeLink: "https://www.youtube.com/embed/qgpKL1PRy5g"
type: "love"
singers:
  - Hariharan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Konjam veyilaga konjam mazhaiyaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam veyilaga konjam mazhaiyaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam kuliraaga varuvaai anbe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam kuliraaga varuvaai anbe"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam nizhalaaga konjam kudaiyaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam nizhalaaga konjam kudaiyaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam kanavaaga varuven un aruge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam kanavaaga varuven un aruge"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enadhaayul pooraavum oru nodiyil paripogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enadhaayul pooraavum oru nodiyil paripogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha maayam enna endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha maayam enna endru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini yaaridathil ketka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini yaaridathil ketka"/>
</div>
<div class="lyrico-lyrics-wrapper">Sol en kanave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sol en kanave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konjam veyilaga konjam mazhaiyaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam veyilaga konjam mazhaiyaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam kuliraaga varuvaai anbe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam kuliraaga varuvaai anbe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee varum paadhai engum pookkalil paai virippen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee varum paadhai engum pookkalil paai virippen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ovvoru poovukkullum idhamaaga naan iruppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ovvoru poovukkullum idhamaaga naan iruppen"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeyilum un peyar ezhuthiye naan padippen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeyilum un peyar ezhuthiye naan padippen"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadavulin kavidhaigalai kadanaaga naan ketpen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadavulin kavidhaigalai kadanaaga naan ketpen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un vizhi paarvaikku kaathirunthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un vizhi paarvaikku kaathirunthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ovvoru nodi pozhuthum dhavam kidappen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ovvoru nodi pozhuthum dhavam kidappen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu kaatrillaadha pudhu bhoomi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu kaatrillaadha pudhu bhoomi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee thendralaaga vara vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thendralaaga vara vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu ootru illaadha oru paarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu ootru illaadha oru paarai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee thanneeraaga vara vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thanneeraaga vara vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollaadha sollellaam minsaaramaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollaadha sollellaam minsaaramaai"/>
</div>
<div class="lyrico-lyrics-wrapper">En meedhu paaigirathe vaa anbe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En meedhu paaigirathe vaa anbe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konjam veyilaga konjam mazhaiyaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam veyilaga konjam mazhaiyaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam kuliraaga varuvaai anbe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam kuliraaga varuvaai anbe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee tharum kaatrai vaithu azhagaana silai vadithen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee tharum kaatrai vaithu azhagaana silai vadithen"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee vidum moochai kooda nenjukkul sirai vaithen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee vidum moochai kooda nenjukkul sirai vaithen"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee tharum vaanam engum kaatraaga naan parappen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee tharum vaanam engum kaatraaga naan parappen"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee idum kolam ellaam idhamaaga naan pirappen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee idum kolam ellaam idhamaaga naan pirappen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nool mel nadakka pazhakki vittaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nool mel nadakka pazhakki vittaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Iruttukkul thanimaiyil sirikka vittaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruttukkul thanimaiyil sirikka vittaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu kadal illaadha oru kappal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu kadal illaadha oru kappal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee vazhigalaaga vara vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee vazhigalaaga vara vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu mozhi illaadha pudhu vaarthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu mozhi illaadha pudhu vaarthai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kavithaiyaaga thara vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kavithaiyaaga thara vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Veyilodu mazhai vanthu vilaiyaaduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veyilodu mazhai vanthu vilaiyaaduthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavukku vizhi thanthu pogum penne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavukku vizhi thanthu pogum penne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konjam veyilaga konjam mazhaiyaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam veyilaga konjam mazhaiyaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam kuliraaga varuvaai anbe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam kuliraaga varuvaai anbe"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam nizhalaaga konjam kudaiyaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam nizhalaaga konjam kudaiyaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam kanavaaga varuven un aruge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam kanavaaga varuven un aruge"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enadhaayul pooraavum oru nodiyil paripogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enadhaayul pooraavum oru nodiyil paripogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha maayam enna endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha maayam enna endru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini yaaridathil ketka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini yaaridathil ketka"/>
</div>
<div class="lyrico-lyrics-wrapper">Sol en kanave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sol en kanave"/>
</div>
</pre>
