---
title: "en aasai macha song lyrics"
album: "7"
artist: "Chaitan Bharadwaj"
lyricist: "Niranjan Bharathi"
director: "Nizar Shafi"
path: "/albums/7-song-lyrics"
song: "En Aasai Macha"
image: ../../images/albumart/7.jpg
date: 2019-06-05
lang: tamil
youtubeLink: "https://www.youtube.com/embed/4pJ04Ta05fk"
type: "melody"
singers:
  - Madhushree
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">En aasai machaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En aasai machaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Innum konja neram konjalaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum konja neram konjalaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooyaama naalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooyaama naalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Muthamittu sanda podalaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthamittu sanda podalaama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey chinna chinna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey chinna chinna"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarvaiyaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarvaiyaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanna vanna vaanavilla pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanna vanna vaanavilla pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhkai maaripochae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkai maaripochae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unaala.aaa.
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unaala.aaa."/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaamaeae.ae.
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaamaeae.ae."/>
</div>
<div class="lyrico-lyrics-wrapper">En naalum neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En naalum neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Enn munna vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enn munna vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Konnu podura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konnu podura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rendu itchai konda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendu itchai konda"/>
</div>
<div class="lyrico-lyrics-wrapper">Patchai pulla kannala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Patchai pulla kannala"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhum kaalam ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhum kaalam ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">On kaiya kokkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="On kaiya kokkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil saayanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil saayanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Konji theerkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konji theerkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Onna vaazhanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna vaazhanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnil seranum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnil seranum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna thaanganum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna thaanganum"/>
</div>
<div class="lyrico-lyrics-wrapper">Verenna venum ithu pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verenna venum ithu pothum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En aasai machaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En aasai machaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Innum konja neram konjalaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum konja neram konjalaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooyaama naalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooyaama naalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Muthamittu sanda podalaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthamittu sanda podalaama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unudaiya tholula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unudaiya tholula"/>
</div>
<div class="lyrico-lyrics-wrapper">Uppu mootai poganum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uppu mootai poganum"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinna pulla pola enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna pulla pola enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee konjanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee konjanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naama serndhirukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naama serndhirukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Neram ellaam sandhosam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neram ellaam sandhosam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai kollaama kollumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai kollaama kollumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un vaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un vaasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada unnala unnala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada unnala unnala"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna naanae maranthae ponnen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna naanae maranthae ponnen"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna paaka paaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna paaka paaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Inbam koodi poguthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inbam koodi poguthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjukullae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjukullae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mella mella en kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mella mella en kaadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Innum ellai meerudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum ellai meerudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhum kaalam ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhum kaalam ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">On kaiya kokkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="On kaiya kokkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil saayanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil saayanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Konji theerkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konji theerkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Onna vaazhanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna vaazhanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnil seranum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnil seranum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna thaanganum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna thaanganum"/>
</div>
<div class="lyrico-lyrics-wrapper">Verenna venum ithu pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verenna venum ithu pothum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enudaiya veetukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enudaiya veetukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna kooti poguren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna kooti poguren"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasa patta vaazhkai ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasa patta vaazhkai ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan vaazhuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan vaazhuren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjam intha naalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjam intha naalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Enni enni kondaadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enni enni kondaadum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kooda illatha neramo thindaadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooda illatha neramo thindaadum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada nee thaanae ellamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada nee thaanae ellamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vera enna enakku venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vera enna enakku venum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanaa kandathellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanaa kandathellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nesamaga maariyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nesamaga maariyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponadhenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponadhenna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Innum enna ver yedhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum enna ver yedhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenju ketka villaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenju ketka villaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhum kaalam ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhum kaalam ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">On kaiya kokkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="On kaiya kokkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil saayanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil saayanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Konji theerkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konji theerkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Onna vaazhanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna vaazhanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnil seranum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnil seranum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna thaanganum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna thaanganum"/>
</div>
<div class="lyrico-lyrics-wrapper">Verenna venum ithu pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verenna venum ithu pothum"/>
</div>
</pre>
