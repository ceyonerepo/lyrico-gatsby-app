---
title: "endi raa ee janala gola song lyrics"
album: "Needi Naadi Oke Katha"
artist: "Suresh Bobbili"
lyricist: "Shyam Kasarla"
director: "Venu Udugula"
path: "/albums/needi-naadi-oke-katha-lyrics"
song: "Endi Raa Ee Janala Gola"
image: ../../images/albumart/needi-naadi-oke-katha.jpg
date: 2018-03-23
lang: telugu
youtubeLink: "https://www.youtube.com/embed/JnoiFLWGRCg"
type: "love"
singers:
  -	Suresh Bobbili
  - Sathyavathi (Mangli)
  - Kailash	
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yè Yèndiraa èi Janaala Gola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yè Yèndiraa èi Janaala Gola"/>
</div>
<div class="lyrico-lyrics-wrapper">Illèy Cèntral Jail Aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illèy Cèntral Jail Aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Zindhagièè Lo Sèttlè Avvalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zindhagièè Lo Sèttlè Avvalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Compulsory Adhi Rulèaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Compulsory Adhi Rulèaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sodhi Sèrial Laaga Saala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sodhi Sèrial Laaga Saala"/>
</div>
<div class="lyrico-lyrics-wrapper">Undadura Vèèla Paalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undadura Vèèla Paalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Inthakanna Janthuvu Mhèla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inthakanna Janthuvu Mhèla"/>
</div>
<div class="lyrico-lyrics-wrapper">Manamunnadi Junglèy Aah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manamunnadi Junglèy Aah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Car Kontèy Bunglow 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Car Kontèy Bunglow "/>
</div>
<div class="lyrico-lyrics-wrapper">Untèy Gold Vèsukuntèy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Untèy Gold Vèsukuntèy"/>
</div>
<div class="lyrico-lyrics-wrapper">Status Ku Adhi Symbolaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Status Ku Adhi Symbolaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vadni Chusi Vèèdni 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadni Chusi Vèèdni "/>
</div>
<div class="lyrico-lyrics-wrapper">Chusi Ninnu Polchukuntèy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chusi Ninnu Polchukuntèy"/>
</div>
<div class="lyrico-lyrics-wrapper">Pèdataru Kaaki Golaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pèdataru Kaaki Golaa"/>
</div>
è<div class="lyrico-lyrics-wrapper">y Jimmèdaari Koyilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="y Jimmèdaari Koyilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jimmèydaari Koyilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jimmèydaari Koyilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yèvadu Ra Kanipèttindi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yèvadu Ra Kanipèttindi"/>
</div>
è<div class="lyrico-lyrics-wrapper">i Sèttlèmènt Dhoola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="i Sèttlèmènt Dhoola"/>
</div>
<div class="lyrico-lyrics-wrapper">Jimmèydaari Koyilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jimmèydaari Koyilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jimmèydaari Koyilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jimmèydaari Koyilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yèpuku Thintaru Yènduku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yèpuku Thintaru Yènduku"/>
</div>
<div class="lyrico-lyrics-wrapper">Youth Antèy Robolaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Youth Antèy Robolaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Makè It Go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Makè It Go"/>
</div>
<div class="lyrico-lyrics-wrapper">Chaduvukunètappudu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chaduvukunètappudu "/>
</div>
<div class="lyrico-lyrics-wrapper">Rank Rank Antaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rank Rank Antaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Rank Kottinaaka Manchi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rank Kottinaaka Manchi "/>
</div>
<div class="lyrico-lyrics-wrapper">Job Job Antaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Job Job Antaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Packagè èntha Ani 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Packagè èntha Ani "/>
</div>
<div class="lyrico-lyrics-wrapper">Aara Thèèsthuntaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aara Thèèsthuntaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Job Chèsi Jèbu Nimpi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Job Chèsi Jèbu Nimpi "/>
</div>
<div class="lyrico-lyrics-wrapper">Chakirantho Chèsthuntèy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chakirantho Chèsthuntèy"/>
</div>
<div class="lyrico-lyrics-wrapper">Pèlli Pèlli Antu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pèlli Pèlli Antu "/>
</div>
<div class="lyrico-lyrics-wrapper">Matrimony Ki Thosthaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Matrimony Ki Thosthaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Aada Padi èèda Padi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aada Padi èèda Padi "/>
</div>
<div class="lyrico-lyrics-wrapper">Chacchi Chèydi Vèthukulaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chacchi Chèydi Vèthukulaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalladani Tèlladani 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalladani Tèlladani "/>
</div>
<div class="lyrico-lyrics-wrapper">Potti Ani Podugu Ani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Potti Ani Podugu Ani"/>
</div>
<div class="lyrico-lyrics-wrapper">Saavadèngi Sèttlèmènt 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saavadèngi Sèttlèmènt "/>
</div>
<div class="lyrico-lyrics-wrapper">Gola Loki Laagutharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gola Loki Laagutharu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chi Nèè Abha Jèèvitham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chi Nèè Abha Jèèvitham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hèy Hèy Targètantaru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hèy Hèy Targètantaru "/>
</div>
<div class="lyrico-lyrics-wrapper">Goal Antaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Goal Antaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Chèppindèy Chèppi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chèppindèy Chèppi "/>
</div>
<div class="lyrico-lyrics-wrapper">Mammu Samputhuntaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mammu Samputhuntaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Forèign Antaru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Forèign Antaru "/>
</div>
<div class="lyrico-lyrics-wrapper">Poru Thuntaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poru Thuntaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Maakèda Kaalindo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maakèda Kaalindo "/>
</div>
<div class="lyrico-lyrics-wrapper">Soodakuntaaruu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soodakuntaaruu"/>
</div>
<div class="lyrico-lyrics-wrapper">Lèt’S Go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lèt’S Go"/>
</div>
<div class="lyrico-lyrics-wrapper">First Placè Kosamèy 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="First Placè Kosamèy "/>
</div>
<div class="lyrico-lyrics-wrapper">Poti èndiraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poti èndiraa"/>
</div>
èè <div class="lyrico-lyrics-wrapper">Racè Lona 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Racè Lona "/>
</div>
<div class="lyrico-lyrics-wrapper">Yèndukantha Thondaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yèndukantha Thondaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mèmu Kaaliga Untèy 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mèmu Kaaliga Untèy "/>
</div>
<div class="lyrico-lyrics-wrapper">Chusi Asalu Orvaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chusi Asalu Orvaraa"/>
</div>
è<div class="lyrico-lyrics-wrapper">y Function 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="y Function "/>
</div>
<div class="lyrico-lyrics-wrapper">Lo Junction Lo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lo Junction Lo"/>
</div>
<div class="lyrico-lyrics-wrapper">Yèda Choodu Vèèla 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yèda Choodu Vèèla "/>
</div>
<div class="lyrico-lyrics-wrapper">Torturè Aah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Torturè Aah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
è<div class="lyrico-lyrics-wrapper">y Jimmèydaari Koyilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="y Jimmèydaari Koyilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jimmèydaari Koyilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jimmèydaari Koyilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yèvadu Ra Kanipèttindi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yèvadu Ra Kanipèttindi"/>
</div>
è<div class="lyrico-lyrics-wrapper">i Sèttlèmènt Dhoola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="i Sèttlèmènt Dhoola"/>
</div>
<div class="lyrico-lyrics-wrapper">Jimmèydaari Koyilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jimmèydaari Koyilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jimmèydaari Koyilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jimmèydaari Koyilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yèpuku Thintaru Yènduku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yèpuku Thintaru Yènduku"/>
</div>
<div class="lyrico-lyrics-wrapper">Youth Antèy Robolaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Youth Antèy Robolaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yè Chaduvukora Babu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yè Chaduvukora Babu "/>
</div>
<div class="lyrico-lyrics-wrapper">Udhyogamè Kottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udhyogamè Kottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dabbèyra Paruvu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dabbèyra Paruvu "/>
</div>
<div class="lyrico-lyrics-wrapper">Lèdantènèy Karuvu Ani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lèdantènèy Karuvu Ani"/>
</div>
<div class="lyrico-lyrics-wrapper">Poddunèy Lèchèy 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poddunèy Lèchèy "/>
</div>
<div class="lyrico-lyrics-wrapper">Paadu Janam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadu Janam"/>
</div>
<div class="lyrico-lyrics-wrapper">Noru Thèrstèy 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Noru Thèrstèy "/>
</div>
<div class="lyrico-lyrics-wrapper">Adèy Japam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adèy Japam"/>
</div>
è<div class="lyrico-lyrics-wrapper">pudu Choosina 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pudu Choosina "/>
</div>
è<div class="lyrico-lyrics-wrapper">kkadikèllina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kkadikèllina"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakkanodi Mèèda 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkanodi Mèèda "/>
</div>
<div class="lyrico-lyrics-wrapper">Padèy Rakam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padèy Rakam"/>
</div>
<div class="lyrico-lyrics-wrapper">Lokam èy Antha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lokam èy Antha "/>
</div>
<div class="lyrico-lyrics-wrapper">Nèèla Nuvvuntè Thanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nèèla Nuvvuntè Thanta"/>
</div>
<div class="lyrico-lyrics-wrapper">Lokam èy Antha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lokam èy Antha "/>
</div>
<div class="lyrico-lyrics-wrapper">Nèèla Nuvvuntè Thanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nèèla Nuvvuntè Thanta"/>
</div>
è<div class="lyrico-lyrics-wrapper">ndi Mama è Drama 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ndi Mama è Drama "/>
</div>
<div class="lyrico-lyrics-wrapper">Burra Thinè Yèdava Karma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Burra Thinè Yèdava Karma"/>
</div>
<div class="lyrico-lyrics-wrapper">Intlonu Adèy Thanthu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intlonu Adèy Thanthu "/>
</div>
è<div class="lyrico-lyrics-wrapper">ndira èi Sèttlèmènt
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ndira èi Sèttlèmènt"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaèh Fuck
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaèh Fuck"/>
</div>
<div class="lyrico-lyrics-wrapper">Yèh Yèh Yèh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yèh Yèh Yèh"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Oh Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Oh Oh"/>
</div>
<div class="lyrico-lyrics-wrapper">Yèh Yèh Yèh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yèh Yèh Yèh"/>
</div>
<div class="lyrico-lyrics-wrapper">Lèt’S Go Givè Mè Thè Bèat
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lèt’S Go Givè Mè Thè Bèat"/>
</div>
<div class="lyrico-lyrics-wrapper">Givè Mè Thè Bèat
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Givè Mè Thè Bèat"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Hoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Hoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Bank Loan La Vunna Flat La
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bank Loan La Vunna Flat La"/>
</div>
<div class="lyrico-lyrics-wrapper">Sèttlèmènt Ku Yèndukkaya Mètèr
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sèttlèmènt Ku Yèndukkaya Mètèr"/>
</div>
<div class="lyrico-lyrics-wrapper">Bènz Tholina Ganji Thaagina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bènz Tholina Ganji Thaagina"/>
</div>
<div class="lyrico-lyrics-wrapper">Bathakadanikèmundi Mattèru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bathakadanikèmundi Mattèru"/>
</div>
<div class="lyrico-lyrics-wrapper">Lèt’S Go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lèt’S Go"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lifè Antèy Koodabèttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lifè Antèy Koodabèttu"/>
</div>
<div class="lyrico-lyrics-wrapper">kunna Lakshala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kunna Lakshala"/>
</div>
<div class="lyrico-lyrics-wrapper">Avi Lèkuntèy 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avi Lèkuntèy "/>
</div>
<div class="lyrico-lyrics-wrapper">Maakinni Shikshalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maakinni Shikshalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mèè Asthulni Pènchukonu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mèè Asthulni Pènchukonu "/>
</div>
<div class="lyrico-lyrics-wrapper">Intha Thippalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Thippalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu Chachayka Challèdi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu Chachayka Challèdi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nèè Mida Intha Chillara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nèè Mida Intha Chillara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
è<div class="lyrico-lyrics-wrapper">y Jimmèydaari Koyilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="y Jimmèydaari Koyilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jimmèydaari Koyilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jimmèydaari Koyilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yèvadu Ra Kanipèttindi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yèvadu Ra Kanipèttindi"/>
</div>
è<div class="lyrico-lyrics-wrapper">i Sèttlèmènt Dhoola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="i Sèttlèmènt Dhoola"/>
</div>
<div class="lyrico-lyrics-wrapper">Jimmèydaari Koyilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jimmèydaari Koyilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jimmèydaari Koyilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jimmèydaari Koyilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yèpuku Thintaru Yènduku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yèpuku Thintaru Yènduku"/>
</div>
<div class="lyrico-lyrics-wrapper">Youth Antèy Robolaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Youth Antèy Robolaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hèy Vaddhuraa Manakodduraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hèy Vaddhuraa Manakodduraa"/>
</div>
</pre>
