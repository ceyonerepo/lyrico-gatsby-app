---
title: "yevathive song lyrics"
album: "Hippi"
artist: "Nivas K. Prasanna"
lyricist: "Anantha Sriram"
director: "Krishna"
path: "/albums/hippi-lyrics"
song: "Yevathive"
image: ../../images/albumart/hippi.jpg
date: 2019-06-06
lang: telugu
youtubeLink: "https://www.youtube.com/embed/Nag4tPC06oM"
type: "love"
singers:
  - Karthik
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yevathive yevathive
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yevathive yevathive"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhanu patti vadhalave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhanu patti vadhalave"/>
</div>
<div class="lyrico-lyrics-wrapper">Yevathive yevathive
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yevathive yevathive"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallalonchi kadhalave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallalonchi kadhalave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hayyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hayyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Bye bye bye cheppesindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bye bye bye cheppesindhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa gunde nede
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa gunde nede"/>
</div>
<div class="lyrico-lyrics-wrapper">Ammo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee bike yekki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee bike yekki"/>
</div>
<div class="lyrico-lyrics-wrapper">Bayalello pothande choode
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bayalello pothande choode"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ney flat ayya nee choopuke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ney flat ayya nee choopuke"/>
</div>
<div class="lyrico-lyrics-wrapper">Fast beat ayya nee navvuke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fast beat ayya nee navvuke"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedho lotayya naalo ne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedho lotayya naalo ne"/>
</div>
<div class="lyrico-lyrics-wrapper">Nennallakennaallakee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nennallakennaallakee"/>
</div>
<div class="lyrico-lyrics-wrapper">Nen heat ayya nee kanthi ki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nen heat ayya nee kanthi ki"/>
</div>
<div class="lyrico-lyrics-wrapper">Ten feet ayya nee sparsha ki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ten feet ayya nee sparsha ki"/>
</div>
<div class="lyrico-lyrics-wrapper">Chala late ayya inthandham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chala late ayya inthandham"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallaara chudaanikee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallaara chudaanikee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yevathive yevathive
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yevathive yevathive"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallalonchi kadhalave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallalonchi kadhalave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaraaru kaalaalaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraaru kaalaalaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey yededu lokaalaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey yededu lokaalaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Mathi poyentha maaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mathi poyentha maaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaadhi nee dhaggaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaadhi nee dhaggaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhooraalu bharaalani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhooraalu bharaalani"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey aa thara theeralani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey aa thara theeralani"/>
</div>
<div class="lyrico-lyrics-wrapper">Kariginchentha kaipu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kariginchentha kaipu"/>
</div>
<div class="lyrico-lyrics-wrapper">Challaave nee chutturaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Challaave nee chutturaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oka maamulu kurraadine
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oka maamulu kurraadine"/>
</div>
<div class="lyrico-lyrics-wrapper">Ika nenentha nee mundhara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ika nenentha nee mundhara"/>
</div>
<div class="lyrico-lyrics-wrapper">Oka maikamla kammesilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oka maikamla kammesilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheripesaave naa niddharaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheripesaave naa niddharaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Bathikedelage nenu apsaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bathikedelage nenu apsaraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ney lock ayya nee needaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ney lock ayya nee needaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo joke ayya nee jaaliki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo joke ayya nee jaaliki"/>
</div>
<div class="lyrico-lyrics-wrapper">Heart cake ayyanivvala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heart cake ayyanivvala"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeloni mounaanikee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeloni mounaanikee"/>
</div>
<div class="lyrico-lyrics-wrapper">Ne crack ayya nee swaasa ki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne crack ayya nee swaasa ki"/>
</div>
<div class="lyrico-lyrics-wrapper">Fail break ayya naa aashaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fail break ayya naa aashaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey block ayya nee valla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey block ayya nee valla"/>
</div>
<div class="lyrico-lyrics-wrapper">Neejanmakai janmaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neejanmakai janmaki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hayyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hayyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Bye bye bye cheppesindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bye bye bye cheppesindhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa gunde nede
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa gunde nede"/>
</div>
<div class="lyrico-lyrics-wrapper">Ney flat ayya nee choopuke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ney flat ayya nee choopuke"/>
</div>
<div class="lyrico-lyrics-wrapper">Fast beat ayya nee navvuke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fast beat ayya nee navvuke"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedho lotayya naalo ne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedho lotayya naalo ne"/>
</div>
<div class="lyrico-lyrics-wrapper">Nennallakennaallakee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nennallakennaallakee"/>
</div>
<div class="lyrico-lyrics-wrapper">Nen heat ayya nee kanthi ki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nen heat ayya nee kanthi ki"/>
</div>
<div class="lyrico-lyrics-wrapper">Ten feet ayya nee sparsha ki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ten feet ayya nee sparsha ki"/>
</div>
<div class="lyrico-lyrics-wrapper">Chala late ayya inthandham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chala late ayya inthandham"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallaara chudaanikee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallaara chudaanikee"/>
</div>
</pre>
