---
title: "aga naga song lyrics"
album: "Ko"
artist: "Harris Jayaraj"
lyricist: "Pa. Vijay -Vanamali - Emcee Jesz"
director: "K.V. Anand"
path: "/albums/ko-lyrics"
song: "Aga Naga"
image: ../../images/albumart/ko.jpg
date: 2011-04-22
lang: tamil
youtubeLink: "https://www.youtube.com/embed/gH2evf1af-c"
type: "mass"
singers:
  - Vijay Prakash
  - Tippu
  - Ranina Reddy
  - Priya Subramaniam
  - Solar Sai
  - Srik
  - Emcee Jesz
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Bonja Boys and Girls
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bonja Boys and Girls"/>
</div>
<div class="lyrico-lyrics-wrapper">You people Look So Good Tonight
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You people Look So Good Tonight"/>
</div>
<div class="lyrico-lyrics-wrapper">Lets Party and Heat This Pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lets Party and Heat This Pa"/>
</div>
<div class="lyrico-lyrics-wrapper">As We Take It Higher
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="As We Take It Higher"/>
</div>
<div class="lyrico-lyrics-wrapper">We Gonna Set The Song On Fire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="We Gonna Set The Song On Fire"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">And Yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="And Yeah"/>
</div>
<div class="lyrico-lyrics-wrapper">Siragugalin Vanakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siragugalin Vanakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">Swagatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Swagatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Namaskaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namaskaaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthanam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vinnilum Mannilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinnilum Mannilum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulla Natchathirangalodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulla Natchathirangalodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Onna Senthu Inainthu Pinainthu Ninainthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna Senthu Inainthu Pinainthu Ninainthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pichchu Pinni Pethedukalaam Yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pichchu Pinni Pethedukalaam Yeah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aga Naga Naga Sirippugal Azhagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aga Naga Naga Sirippugal Azhagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thigu Thaga Thaga Nilavugal Azhagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thigu Thaga Thaga Nilavugal Azhagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Very Very Very Samathugal Azhagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Very Very Very Samathugal Azhagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kozhu Kozhupugal Azhagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kozhu Kozhupugal Azhagaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aga Naga Naga Sirippugal Azhagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aga Naga Naga Sirippugal Azhagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thigu Thaga Thaga Nilavugal Azhagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thigu Thaga Thaga Nilavugal Azhagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Very Very Very Samathugal Azhagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Very Very Very Samathugal Azhagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kozhu Kozhupugal Azhagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kozhu Kozhupugal Azhagaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lamborghini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lamborghini"/>
</div>
<div class="lyrico-lyrics-wrapper">Love O Many
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love O Many"/>
</div>
<div class="lyrico-lyrics-wrapper">Velli Sani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velli Sani"/>
</div>
<div class="lyrico-lyrics-wrapper">Iravellaam Ithazh Pani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravellaam Ithazh Pani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Paththini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Paththini"/>
</div>
<div class="lyrico-lyrics-wrapper">Thee Paththini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thee Paththini"/>
</div>
<div class="lyrico-lyrics-wrapper">Unmel ini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmel ini"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaradi Theni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaradi Theni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thol Saainthaal Anaippu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thol Saainthaal Anaippu"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan Thozhan Endra Ninaippu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan Thozhan Endra Ninaippu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Intha Padaippu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Intha Padaippu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingillai Kalaippu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingillai Kalaippu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thol Saainthaal Anaippu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thol Saainthaal Anaippu"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan Thozhan Endra Ninaippu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan Thozhan Endra Ninaippu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Intha Padaippu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Intha Padaippu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingillai Kalaippu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingillai Kalaippu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaakka Thaakkave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaakka Thaakkave"/>
</div>
<div class="lyrico-lyrics-wrapper">Thudikkuthey Nenjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thudikkuthey Nenjam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nokka Nokkave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nokka Nokkave"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudikkuthey Konjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudikkuthey Konjam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thoda Thoda Thoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoda Thoda Thoda"/>
</div>
<div class="lyrico-lyrics-wrapper">Rolex Minna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rolex Minna"/>
</div>
<div class="lyrico-lyrics-wrapper">Thona Thona Thona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thona Thona Thona"/>
</div>
<div class="lyrico-lyrics-wrapper">Virtue Pesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Virtue Pesa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jilu Jilu Champagne ponga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jilu Jilu Champagne ponga"/>
</div>
<div class="lyrico-lyrics-wrapper">Swiss Cheese Sai Mella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Swiss Cheese Sai Mella"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thoda Thoda Thoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoda Thoda Thoda"/>
</div>
<div class="lyrico-lyrics-wrapper">Rolex Minna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rolex Minna"/>
</div>
<div class="lyrico-lyrics-wrapper">Thona Thona Thona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thona Thona Thona"/>
</div>
<div class="lyrico-lyrics-wrapper">Virtue Pesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Virtue Pesa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jilu Jilu Champagne ponga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jilu Jilu Champagne ponga"/>
</div>
<div class="lyrico-lyrics-wrapper">Swiss Cheese Sai Mella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Swiss Cheese Sai Mella"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Brand Odu Vaazhkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Brand Odu Vaazhkai"/>
</div>
<div class="lyrico-lyrics-wrapper">Friend Odu Serkkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friend Odu Serkkai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiyodu Yaarkkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyodu Yaarkkai"/>
</div>
<div class="lyrico-lyrics-wrapper">Anandha Yaakkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anandha Yaakkai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manam Maarum Fashion
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam Maarum Fashion"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaraadhu Passion
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaraadhu Passion"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagalellaam Vesam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagalellaam Vesam"/>
</div>
<div class="lyrico-lyrics-wrapper">Maalaiyil Nesam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maalaiyil Nesam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa Kadhal Ferari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Kadhal Ferari"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilam Nenjai Allum Sonali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilam Nenjai Allum Sonali"/>
</div>
<div class="lyrico-lyrics-wrapper">Louis Vuitton Kannaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Louis Vuitton Kannaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pala Kangal Pinnaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala Kangal Pinnaadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa Kadhal Ferari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Kadhal Ferari"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilam Nenjai Allum Sonali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilam Nenjai Allum Sonali"/>
</div>
<div class="lyrico-lyrics-wrapper">Louis Vuitton Kannaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Louis Vuitton Kannaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pala Kangal Pinnaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala Kangal Pinnaadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gucci Hugo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gucci Hugo"/>
</div>
<div class="lyrico-lyrics-wrapper">Swiss Yeah Standard
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Swiss Yeah Standard"/>
</div>
<div class="lyrico-lyrics-wrapper">No Local Only Branded
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="No Local Only Branded"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhil Irunthaa Aaattam Poda Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhil Irunthaa Aaattam Poda Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudiyaatti Kaiya Katti Paar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudiyaatti Kaiya Katti Paar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaakka Thaakkave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaakka Thaakkave"/>
</div>
<div class="lyrico-lyrics-wrapper">Thudikkuthey Nenjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thudikkuthey Nenjam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeah"/>
</div>
<div class="lyrico-lyrics-wrapper">Fashionukillai Ration Aanaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fashionukillai Ration Aanaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadavul Thantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadavul Thantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Varam Thaan Passion Now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varam Thaan Passion Now"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Let Me Make My
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Let Me Make My"/>
</div>
<div class="lyrico-lyrics-wrapper">Little Confession
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Little Confession"/>
</div>
<div class="lyrico-lyrics-wrapper">Love Has No
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love Has No"/>
</div>
<div class="lyrico-lyrics-wrapper">Brands On This Thing
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Brands On This Thing"/>
</div>
<div class="lyrico-lyrics-wrapper">Got Right Now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Got Right Now"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kola Kolagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kola Kolagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnadu Lo Lona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnadu Lo Lona"/>
</div>
<div class="lyrico-lyrics-wrapper">Alla Alladha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alla Alladha"/>
</div>
<div class="lyrico-lyrics-wrapper">Chesethemu Aaona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chesethemu Aaona"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sommu Sokkula Koo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sommu Sokkula Koo"/>
</div>
<div class="lyrico-lyrics-wrapper">Eh Roatuleni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh Roatuleni"/>
</div>
<div class="lyrico-lyrics-wrapper">Eero Naamagudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eero Naamagudu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Roaju Mozhulakoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Roaju Mozhulakoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ra Ramanava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ra Ramanava"/>
</div>
<div class="lyrico-lyrics-wrapper">Jee Oh Egurudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jee Oh Egurudu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pudikkuthey Pudikkuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudikkuthey Pudikkuthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nooru Nooru Per
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nooru Nooru Per"/>
</div>
<div class="lyrico-lyrics-wrapper">Aruge Aruge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aruge Aruge"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaru Yaaru Sol Azhage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaru Yaaru Sol Azhage"/>
</div>
<div class="lyrico-lyrics-wrapper">Veru Veru Oor Maname Maname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veru Veru Oor Maname Maname"/>
</div>
<div class="lyrico-lyrics-wrapper">Verthidaatha Oru Iname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verthidaatha Oru Iname"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nooru Nooru Per
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nooru Nooru Per"/>
</div>
<div class="lyrico-lyrics-wrapper">Aruge Aruge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aruge Aruge"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaru Yaaru Sol Azhage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaru Yaaru Sol Azhage"/>
</div>
<div class="lyrico-lyrics-wrapper">Veru Veru Oor Maname Maname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veru Veru Oor Maname Maname"/>
</div>
<div class="lyrico-lyrics-wrapper">Verthidaatha Oru Iname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verthidaatha Oru Iname"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Min Minu Minu Pagal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Min Minu Minu Pagal"/>
</div>
<div class="lyrico-lyrics-wrapper">Min Veechugal Kottum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Min Veechugal Kottum"/>
</div>
<div class="lyrico-lyrics-wrapper">Silu Siluppaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silu Siluppaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Sila Silmisam Sottum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sila Silmisam Sottum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ithuthaan Ithuthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithuthaan Ithuthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilamai Ulagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilamai Ulagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetri Ondruthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetri Ondruthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingey Uthavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingey Uthavum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aga Naga Naga Sirippugal Azhagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aga Naga Naga Sirippugal Azhagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thigu Thaga Thaga Nilavugal Azhagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thigu Thaga Thaga Nilavugal Azhagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Very Very Very Samathugal Azhagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Very Very Very Samathugal Azhagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kozhu Kozhupugal Azhagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kozhu Kozhupugal Azhagaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aga Naga Naga Sirippugal Azhagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aga Naga Naga Sirippugal Azhagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thigu Thaga Thaga Nilavugal Azhagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thigu Thaga Thaga Nilavugal Azhagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Very Very Very Samathugal Azhagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Very Very Very Samathugal Azhagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kozhu Kozhupugal Azhagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kozhu Kozhupugal Azhagaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lamborghini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lamborghini"/>
</div>
<div class="lyrico-lyrics-wrapper">Love O Many
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love O Many"/>
</div>
<div class="lyrico-lyrics-wrapper">Velli Sani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velli Sani"/>
</div>
<div class="lyrico-lyrics-wrapper">Iravellaam Ithazh Pani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravellaam Ithazh Pani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Paththini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Paththini"/>
</div>
<div class="lyrico-lyrics-wrapper">Thee Paththini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thee Paththini"/>
</div>
<div class="lyrico-lyrics-wrapper">Unmel ini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmel ini"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaradi Theni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaradi Theni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thol Saainthaal Anaippu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thol Saainthaal Anaippu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aval Thozhi Endra Ninaippu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aval Thozhi Endra Ninaippu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Intha Padaippu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Intha Padaippu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingillai Kalaippu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingillai Kalaippu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thol Saainthaal Anaippu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thol Saainthaal Anaippu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aval Thozhi Endra Ninaippu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aval Thozhi Endra Ninaippu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Intha Padaippu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Intha Padaippu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingillai Kalaippu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingillai Kalaippu"/>
</div>
</pre>
