---
title: "othaiyadi pathaiyila song lyrics"
album: "kanaa"
artist: "Dhibu Ninan Thomas"
lyricist: "Arunraja Kamaraj"
director: " Arunraja Kamaraj"
path: "/albums/kanaa-lyrics"
song: "Othaiyadi Pathaiyila"
image: ../../images/albumart/kanaa.jpg
date: 2018-12-21
lang: tamil
youtubeLink: "https://www.youtube.com/embed/qP8e7lFdEho"
type: "Love"
singers:
  - Anirudh Ravichander
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Othaiyadi Pathayila Thaavi Oduren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Othaiyadi Pathayila Thaavi Oduren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aththa Peththa Poonguyila Thedi Vaaduren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aththa Peththa Poonguyila Thedi Vaaduren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sandhana Maala Alluthu Aala Vaasam Yeruthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhana Maala Alluthu Aala Vaasam Yeruthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Kizhi Mela Sangili Pozha Sera Thonuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kizhi Mela Sangili Pozha Sera Thonuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sakkara Aala Sokkuthu Aazha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sakkara Aala Sokkuthu Aazha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maala Maaththa Mama Varattumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maala Maaththa Mama Varattumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanmaniye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanmaniye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vazhiyila Pooththa Saamandhi Neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhiyila Pooththa Saamandhi Neeye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhiyila Seyththa Poongoththu Neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhiyila Seyththa Poongoththu Neeye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adiye Adiye Poongodiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiye Adiye Poongodiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kavala Marakkum Thaai Madiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavala Marakkum Thaai Madiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhage Azhage Pennazhage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhage Azhage Pennazhage"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tharaiyil Nadakkum Therazhage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharaiyil Nadakkum Therazhage"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nizhallaathaan Pinnaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nizhallaathaan Pinnaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa Odi Vandhene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Odi Vandhene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Vaatti Enna Paare Maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Vaatti Enna Paare Maa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Othaiyadi Pathayila Thaavi Odura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Othaiyadi Pathayila Thaavi Odura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aththa Peththa Poonguyila Thedi Vaadura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aththa Peththa Poonguyila Thedi Vaadura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pala Mura Neeyum Paakkaama Pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala Mura Neeyum Paakkaama Pona"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irumbukku Mela Thuruvena Aana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irumbukku Mela Thuruvena Aana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Usura Unakke Naenthu Vitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usura Unakke Naenthu Vitta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irundhu Nerunga Bayanthukkiten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irundhu Nerunga Bayanthukkiten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyire Uyire En Uyire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyire Uyire En Uyire"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ulagam Neethaan Vaa Uyire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagam Neethaan Vaa Uyire"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manasellam Kannaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasellam Kannaadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Odaikkaatha Panthaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odaikkaatha Panthaadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vadhaikkaatha Kanne Kanmaniye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadhaikkaatha Kanne Kanmaniye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Othaiyadi Pathayila Thaavi Odura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Othaiyadi Pathayila Thaavi Odura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aththa Peththa Poonguyila Thedi Vaadura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aththa Peththa Poonguyila Thedi Vaadura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjula Veesum Kanmani Vaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjula Veesum Kanmani Vaasam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaattu Senbagame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattu Senbagame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sangathi Pesum Kangalum Koosum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sangathi Pesum Kangalum Koosum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhal Sandhaname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Sandhaname"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Parava Pola Paranthu Poga Kooda Senthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parava Pola Paranthu Poga Kooda Senthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeyum Varuviya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Varuviya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanmaniye Konjidave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanmaniye Konjidave"/>
</div>
</pre>
