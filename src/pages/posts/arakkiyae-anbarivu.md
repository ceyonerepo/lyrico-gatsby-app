---
title: "arakkiyae song lyrics"
album: "Anbarivu"
artist: "Hiphop Tamizha"
lyricist: "Vivek"
director: "Aswin Raam"
path: "/albums/anbarivu-song-lyrics"
song: "Arakkiyae"
image: ../../images/albumart/anbarivu.jpg
date: 2022-01-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/YW2TT_-wB1s"
type: "love"
singers:
  - Yuvan Shankar Raja
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hey Kadhara Kadhara 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Kadhara Kadhara "/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhara Vittutiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhara Vittutiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Sedhari Sedhari 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Sedhari Sedhari "/>
</div>
<div class="lyrico-lyrics-wrapper">Sedhari Nikkuren 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sedhari Nikkuren "/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Thaniyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Thaniyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhara Kadhara Kadhara 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhara Kadhara Kadhara "/>
</div>
<div class="lyrico-lyrics-wrapper">Vittutiye Adiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vittutiye Adiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Sedhari Sedhari Sedhari 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sedhari Sedhari Sedhari "/>
</div>
<div class="lyrico-lyrics-wrapper">Nikkuren Naan Sariyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikkuren Naan Sariyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Vaatti Paathathukke 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Vaatti Paathathukke "/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Vaaram Thoongalaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Vaaram Thoongalaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Orasaama Paththi Eriyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orasaama Paththi Eriyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagal Mela Paal Thelichu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagal Mela Paal Thelichu "/>
</div>
<div class="lyrico-lyrics-wrapper">Uruvaana Oviyamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uruvaana Oviyamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorellaam Unna Theriyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorellaam Unna Theriyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arakkiyae Un Azhagile 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arakkiyae Un Azhagile "/>
</div>
<div class="lyrico-lyrics-wrapper">Adangunaaa Thaanaa Naa Naaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adangunaaa Thaanaa Naa Naaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kirukkiye Un Kaadhalaal Keranguren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kirukkiye Un Kaadhalaal Keranguren"/>
</div>
<div class="lyrico-lyrics-wrapper">Pappaali Pazhame Summa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pappaali Pazhame Summa "/>
</div>
<div class="lyrico-lyrics-wrapper">Pattaasa Vedikkariye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattaasa Vedikkariye"/>
</div>
<div class="lyrico-lyrics-wrapper">Vedikkariye Vedikkariye Vedikkariye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedikkariye Vedikkariye Vedikkariye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ayyo Therikkariye 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyo Therikkariye "/>
</div>
<div class="lyrico-lyrics-wrapper">Therikkariye Therikkariye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Therikkariye Therikkariye"/>
</div>
<div class="lyrico-lyrics-wrapper">Summaa Sirikkariye 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summaa Sirikkariye "/>
</div>
<div class="lyrico-lyrics-wrapper">Sirikkariye Sirikkariye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirikkariye Sirikkariye"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenja Norukkariye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenja Norukkariye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Unna Paatha Naalu Varaikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Unna Paatha Naalu Varaikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponna Paththi Pesa Neram Illadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponna Paththi Pesa Neram Illadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Unna Paathu Saanja Piragu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Unna Paathu Saanja Piragu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Vittaa Yethum Pechu Illadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Vittaa Yethum Pechu Illadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Patharudhey Patharuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Patharudhey Patharuthey"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarvai Onnu Paaththaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarvai Onnu Paaththaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Podhum Patharuthey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhum Patharuthey "/>
</div>
<div class="lyrico-lyrics-wrapper">Setharuthey Setharuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Setharuthey Setharuthey"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kalkka Smash Azhagaala 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kalkka Smash Azhagaala "/>
</div>
<div class="lyrico-lyrics-wrapper">Peesu Peesaa Setharuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peesu Peesaa Setharuthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kathara Kathara Kathara Kathara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathara Kathara Kathara Kathara"/>
</div>
<div class="lyrico-lyrics-wrapper">Sethara Vittutiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sethara Vittutiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Sethara Sethara Sethara Sethara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Sethara Sethara Sethara Sethara"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathara Vittutiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathara Vittutiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arakkiyae Un Azhagile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arakkiyae Un Azhagile"/>
</div>
<div class="lyrico-lyrics-wrapper">Kirukkiye Un Kaadhalaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kirukkiye Un Kaadhalaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhara Kadhara Kadhara Vittutiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhara Kadhara Kadhara Vittutiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Sethari Sethari Sethari 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Sethari Sethari Sethari "/>
</div>
<div class="lyrico-lyrics-wrapper">Nikkuren Naan Thaniyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikkuren Naan Thaniyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhara Kadhara Kadhara 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhara Kadhara Kadhara "/>
</div>
<div class="lyrico-lyrics-wrapper">Vittutiye Adiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vittutiye Adiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Sedhari Sedhari Sedhari 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sedhari Sedhari Sedhari "/>
</div>
<div class="lyrico-lyrics-wrapper">Nikuren Naan Sariyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikuren Naan Sariyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pappaali Pazhame Summa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pappaali Pazhame Summa "/>
</div>
<div class="lyrico-lyrics-wrapper">Pattaasa Vedikkariye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattaasa Vedikkariye"/>
</div>
<div class="lyrico-lyrics-wrapper">Pappaali Palame 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pappaali Palame "/>
</div>
<div class="lyrico-lyrics-wrapper">Pattaasa Vedikkariye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattaasa Vedikkariye"/>
</div>
<div class="lyrico-lyrics-wrapper">Vedikkariye Vedikkariye 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedikkariye Vedikkariye "/>
</div>
<div class="lyrico-lyrics-wrapper">Vedikkariye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedikkariye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ayyo Therikkariye 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyo Therikkariye "/>
</div>
<div class="lyrico-lyrics-wrapper">Therikkariye Therikkariye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Therikkariye Therikkariye"/>
</div>
<div class="lyrico-lyrics-wrapper">Summa Sirikkariye 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summa Sirikkariye "/>
</div>
<div class="lyrico-lyrics-wrapper">Sirikkariye Sirikkariye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirikkariye Sirikkariye"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenja Norukkuriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenja Norukkuriye"/>
</div>
</pre>
