---
title: "unaalathaan song lyrics"
album: "Sindhubaadh"
artist: "Yuvan Shankar Raja"
lyricist: "Pa. Vijay"
director: "S.U. Arun Kumar"
path: "/albums/sindhubaadh-lyrics"
song: "Unaalathaan"
image: ../../images/albumart/sindhubaadh.jpg
date: 2019-06-27
lang: tamil
youtubeLink: "https://www.youtube.com/embed/u1hZlgvGTt0"
type: "love"
singers:
  - Al Rufiyan
  - Priya Maali
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Unaaladhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unaaladhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Ul Moochu Vaangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Ul Moochu Vaangi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thannalaadhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannalaadhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhinam Thalayaatturen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam Thalayaatturen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha Chinna Chinna Mel Uthadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Chinna Chinna Mel Uthadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Konja Konja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Konja Konja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kenja Nenja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kenja Nenja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vatta Vatta Kannazhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vatta Vatta Kannazhagu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Etho Solla Solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Etho Solla Solla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sollen Pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollen Pulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnaaladhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaaladhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Oru Paadhi Nenju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Oru Paadhi Nenju"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thannalaadhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannalaadhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thalai Keezhaanathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalai Keezhaanathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yei Intha Ulagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei Intha Ulagam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Vandhu Ulagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vandhu Ulagam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Chinna Anbukkey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Chinna Anbukkey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ithu Yengi Suzhalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu Yengi Suzhalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yei Intha Kirukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei Intha Kirukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Thantha Kirukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Thantha Kirukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Una Mattum Nenaikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Una Mattum Nenaikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thimara Thaan Irukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thimara Thaan Irukka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Dhinam Paarthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Dhinam Paarthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjil Ilangaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil Ilangaathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chinna Viral Korthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna Viral Korthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Karai Serthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Karai Serthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnoda Moochil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnoda Moochil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Moocha Kalandheney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Moocha Kalandheney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnaaladhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaaladhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Oru Paadhi Nenju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Oru Paadhi Nenju"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thannalaadhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannalaadhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thalai Keezhaanathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalai Keezhaanathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha Chinna Chinna Mel Uthadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Chinna Chinna Mel Uthadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Konja Konja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Konja Konja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mm Hmm Kenja Nenja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mm Hmm Kenja Nenja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vatta Vatta Kannazhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vatta Vatta Kannazhagu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Etho Solla Solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Etho Solla Solla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sollen Pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollen Pulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnaaladhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaaladhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Oru Paadhi Nenju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Oru Paadhi Nenju"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thannalaadhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannalaadhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thalai Keezhaanathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalai Keezhaanathey"/>
</div>
</pre>
