---
title: "vavvunu song lyrics"
album: "Paadam"
artist: "Ganesh Raghavendra"
lyricist: "Sorkko"
director: "Rajasekhar"
path: "/albums/paadam-song-lyrics"
song: "Vavvunu"
image: ../../images/albumart/paadam.jpg
date: 2018-04-27
lang: tamil
youtubeLink: "https://www.youtube.com/embed/JeMkSG_gKr4"
type: "happy"
singers:
  - Guru Ayyadurai
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">vavvunu naai kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vavvunu naai kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">kuraikuthu parunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuraikuthu parunga"/>
</div>
<div class="lyrico-lyrics-wrapper">english perusunu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="english perusunu "/>
</div>
<div class="lyrico-lyrics-wrapper">yar sonna koorunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yar sonna koorunga"/>
</div>
<div class="lyrico-lyrics-wrapper">hitler tarcher english 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hitler tarcher english "/>
</div>
<div class="lyrico-lyrics-wrapper">teacher viduthala pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="teacher viduthala pa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vavvunu naai kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vavvunu naai kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">kuraikuthu parunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuraikuthu parunga"/>
</div>
<div class="lyrico-lyrics-wrapper">english perusunu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="english perusunu "/>
</div>
<div class="lyrico-lyrics-wrapper">yar sonna koorunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yar sonna koorunga"/>
</div>
<div class="lyrico-lyrics-wrapper">hitler tarcher english 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hitler tarcher english "/>
</div>
<div class="lyrico-lyrics-wrapper">teacher viduthala pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="teacher viduthala pa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">madam paadam alla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="madam paadam alla"/>
</div>
<div class="lyrico-lyrics-wrapper">laadatha aduchanga pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="laadatha aduchanga pa"/>
</div>
<div class="lyrico-lyrics-wrapper">thank you ex cuse me na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thank you ex cuse me na"/>
</div>
<div class="lyrico-lyrics-wrapper">romba than mathipanga pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="romba than mathipanga pa"/>
</div>
<div class="lyrico-lyrics-wrapper">english ah pesi rambama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="english ah pesi rambama"/>
</div>
<div class="lyrico-lyrics-wrapper">arukuranga mokaya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arukuranga mokaya "/>
</div>
<div class="lyrico-lyrics-wrapper">poduranga yen pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poduranga yen pa"/>
</div>
<div class="lyrico-lyrics-wrapper">adimaya pola appa than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adimaya pola appa than"/>
</div>
<div class="lyrico-lyrics-wrapper">ipothum thodaruthu yen pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ipothum thodaruthu yen pa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thus uh bush uh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thus uh bush uh"/>
</div>
<div class="lyrico-lyrics-wrapper">paamba seerurathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paamba seerurathu"/>
</div>
<div class="lyrico-lyrics-wrapper">bantha katurathu pidikalaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bantha katurathu pidikalaye"/>
</div>
<div class="lyrico-lyrics-wrapper">abcd ah kambar paduchara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="abcd ah kambar paduchara"/>
</div>
<div class="lyrico-lyrics-wrapper">kabilar paduchara padikalaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kabilar paduchara padikalaye"/>
</div>
<div class="lyrico-lyrics-wrapper">english enaku kuthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="english enaku kuthum"/>
</div>
<div class="lyrico-lyrics-wrapper">mullu kaada iruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mullu kaada iruku"/>
</div>
<div class="lyrico-lyrics-wrapper">shell ke she solliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="shell ke she solliye"/>
</div>
<div class="lyrico-lyrics-wrapper">enna othukuranga thaliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna othukuranga thaliye"/>
</div>
<div class="lyrico-lyrics-wrapper">ada world language english
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada world language english"/>
</div>
<div class="lyrico-lyrics-wrapper">gold language enga tamil da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gold language enga tamil da"/>
</div>
<div class="lyrico-lyrics-wrapper">thanga tamil nale jeyicharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanga tamil nale jeyicharu"/>
</div>
<div class="lyrico-lyrics-wrapper">valluvaru avara micha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valluvaru avara micha "/>
</div>
<div class="lyrico-lyrics-wrapper">yaru iruka kooru da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaru iruka kooru da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">maama sithappa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maama sithappa "/>
</div>
<div class="lyrico-lyrics-wrapper">ellam uncle than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellam uncle than"/>
</div>
<div class="lyrico-lyrics-wrapper">atha sithikum aunty than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="atha sithikum aunty than"/>
</div>
<div class="lyrico-lyrics-wrapper">daddy endrale thedi adikanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="daddy endrale thedi adikanum"/>
</div>
<div class="lyrico-lyrics-wrapper">appa amma na thani sugam than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="appa amma na thani sugam than"/>
</div>
<div class="lyrico-lyrics-wrapper">sekku maada irunthen ipo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sekku maada irunthen ipo"/>
</div>
<div class="lyrico-lyrics-wrapper">sittaga methakurene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sittaga methakurene"/>
</div>
<div class="lyrico-lyrics-wrapper">pattu poochi ivan than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pattu poochi ivan than"/>
</div>
<div class="lyrico-lyrics-wrapper">antha thattan na parakurene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antha thattan na parakurene"/>
</div>
<div class="lyrico-lyrics-wrapper">ada english england mozhi thane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada english england mozhi thane"/>
</div>
<div class="lyrico-lyrics-wrapper">namma ooril pesurathu yen pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="namma ooril pesurathu yen pa"/>
</div>
<div class="lyrico-lyrics-wrapper">tamil lila pesu athu namma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tamil lila pesu athu namma"/>
</div>
<div class="lyrico-lyrics-wrapper">uyir moochu vera mozhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyir moochu vera mozhi"/>
</div>
<div class="lyrico-lyrics-wrapper">thevai illa po pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thevai illa po pa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vavvunu naai kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vavvunu naai kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">kuraikuthu parunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuraikuthu parunga"/>
</div>
<div class="lyrico-lyrics-wrapper">english perusunu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="english perusunu "/>
</div>
<div class="lyrico-lyrics-wrapper">yar sonna koorunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yar sonna koorunga"/>
</div>
<div class="lyrico-lyrics-wrapper">hitler tarcher english 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hitler tarcher english "/>
</div>
<div class="lyrico-lyrics-wrapper">teacher viduthala pa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="teacher viduthala pa "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">madam paadam alla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="madam paadam alla"/>
</div>
<div class="lyrico-lyrics-wrapper">laadatha aduchanga pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="laadatha aduchanga pa"/>
</div>
<div class="lyrico-lyrics-wrapper">thank you ex cuse me na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thank you ex cuse me na"/>
</div>
<div class="lyrico-lyrics-wrapper">romba than mathipanga pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="romba than mathipanga pa"/>
</div>
</pre>
