---
title: "inthena inthena song lyrics"
album: "Suryakantham"
artist: "Mark K Robin"
lyricist: "Krishna Kanth"
director: "Pranith Bramandapally"
path: "/albums/suryakantham-lyrics"
song: "Inthena Inthena"
image: ../../images/albumart/suryakantham.jpg
date: 2019-03-29
lang: telugu
youtubeLink: "https://www.youtube.com/embed/W3vXZM1x7lA"
type: "love"
singers:
  - Sid Sriram
  - Shakthisree Gopalan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Inthena Inthena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inthena Inthena"/>
</div>
<div class="lyrico-lyrics-wrapper">Premante Inthena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Premante Inthena"/>
</div>
<div class="lyrico-lyrics-wrapper">Padina Daaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padina Daaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Teliyadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teliyadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Inthena Inthena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inthena Inthena"/>
</div>
<div class="lyrico-lyrics-wrapper">Neekaina Inthena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neekaina Inthena"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasu Lolo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu Lolo"/>
</div>
<div class="lyrico-lyrics-wrapper">Niluvadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niluvadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Nidura Ledu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nidura Ledu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuduru Ledu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuduru Ledu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nimishamaina Nake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nimishamaina Nake"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhalalenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhalalenu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vadhalalenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadhalalenu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaya Needhena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaya Needhena"/>
</div>
<div class="lyrico-lyrics-wrapper">Maatalaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maatalaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Raane Ravu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raane Ravu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pedhavidhate
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pedhavidhate"/>
</div>
<div class="lyrico-lyrics-wrapper">Paike
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paike"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakkanunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkanunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Vethukuthunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vethukuthunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenu Ninnena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenu Ninnena"/>
</div>
<div class="lyrico-lyrics-wrapper">Prema Aaakasham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prema Aaakasham"/>
</div>
<div class="lyrico-lyrics-wrapper">Saripoyena Deham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saripoyena Deham"/>
</div>
<div class="lyrico-lyrics-wrapper">Neetho Saavasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neetho Saavasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanu Chesena Maayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanu Chesena Maayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaaralanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaralanni"/>
</div>
<div class="lyrico-lyrics-wrapper">Raalipoye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raalipoye"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannulai Velige
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannulai Velige"/>
</div>
<div class="lyrico-lyrics-wrapper">Dooramantha Theeripoye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dooramantha Theeripoye"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasu Thanuvunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu Thanuvunu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Eduru Choodani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eduru Choodani"/>
</div>
<div class="lyrico-lyrics-wrapper">Snehame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Snehame"/>
</div>
<div class="lyrico-lyrics-wrapper">Eduru Vachina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eduru Vachina"/>
</div>
<div class="lyrico-lyrics-wrapper">Velalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Evaru Choodani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evaru Choodani"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaipuke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaipuke"/>
</div>
<div class="lyrico-lyrics-wrapper">Vethiki Vacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vethiki Vacha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoduve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoduve"/>
</div>
<div class="lyrico-lyrics-wrapper">Gundelo Maata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundelo Maata"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheppalekunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheppalekunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Maayalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Maayalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenu Unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenu Unna"/>
</div>
<div class="lyrico-lyrics-wrapper">Entha Choosthunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entha Choosthunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Chaala Ledamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chaala Ledamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Kallalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Kallalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Daage Povaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daage Povaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Inthena Inthena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inthena Inthena"/>
</div>
<div class="lyrico-lyrics-wrapper">Premante Inthena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Premante Inthena"/>
</div>
<div class="lyrico-lyrics-wrapper">Padina Daaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padina Daaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Teliyadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teliyadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Inthena Inthena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inthena Inthena"/>
</div>
<div class="lyrico-lyrics-wrapper">Neekaina Inthena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neekaina Inthena"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasu Lolo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu Lolo"/>
</div>
<div class="lyrico-lyrics-wrapper">Niluvadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niluvadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Nidura Ledu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nidura Ledu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuduru Ledu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuduru Ledu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nimishamaina Nake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nimishamaina Nake"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhalalenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhalalenu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vadhalalenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadhalalenu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaya Needhena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaya Needhena"/>
</div>
<div class="lyrico-lyrics-wrapper">Maatalaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maatalaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Raane Ravu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raane Ravu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pedhavidhate
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pedhavidhate"/>
</div>
<div class="lyrico-lyrics-wrapper">Paike
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paike"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakkanunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkanunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Vethukuthunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vethukuthunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenu Ninnena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenu Ninnena"/>
</div>
<div class="lyrico-lyrics-wrapper">Prema Aaakasham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prema Aaakasham"/>
</div>
<div class="lyrico-lyrics-wrapper">Saripoyena Deham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saripoyena Deham"/>
</div>
<div class="lyrico-lyrics-wrapper">Neetho Saavasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neetho Saavasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanu Chesena Maayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanu Chesena Maayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaaralanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaralanni"/>
</div>
<div class="lyrico-lyrics-wrapper">Raalipoye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raalipoye"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannulai Velige
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannulai Velige"/>
</div>
<div class="lyrico-lyrics-wrapper">Dooramantha Theeripoye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dooramantha Theeripoye"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasu Thanuvunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu Thanuvunu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakithe"/>
</div>
</pre>
