---
title: "en mannava song lyrics"
album: "Vaanam Kottattum"
artist: "Sid Sriram"
lyricist: "Siva Ananth"
director: "Dhana"
path: "/albums/vaanam-kottattum-lyrics"
song: "En Mannava"
image: ../../images/albumart/vaanam-kottattum.jpg
date: 2020-02-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Nc0YKu-oowk"
type: "Sad"
singers:
  - Shakthisree Gopalan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hmm mmm hmm mm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm mmm hmm mm"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeradha aasai ondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeradha aasai ondru"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatrodu pogindradhae..yen…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatrodu pogindradhae..yen…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aagayam bhoomi rendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagayam bhoomi rendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naal dhorum theigindradhae…yen…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naal dhorum theigindradhae…yen…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En anbe en mannava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En anbe en mannava"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan nenjin mel naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan nenjin mel naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kann thunja vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kann thunja vaa vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naam ullae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam ullae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen intha mounam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen intha mounam"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan seidha paavam enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan seidha paavam enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Solvaayaa…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solvaayaa…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaargaala megam ondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaargaala megam ondru"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannil aarambam ondru..paar…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannil aarambam ondru..paar…"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaa….aa…dhisai illaa vaanin melae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaa….aa…dhisai illaa vaanin melae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunaiyilla paravai ondru…paar…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunaiyilla paravai ondru…paar…"/>
</div>
<div class="lyrico-lyrics-wrapper">Hooo hoo ooo ooo….
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hooo hoo ooo ooo…."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un ozhiyaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un ozhiyaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ul eeram pookkida …vaa…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ul eeram pookkida …vaa…"/>
</div>
<div class="lyrico-lyrics-wrapper">Haaa…aaa…haa…aa….aa..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haaa…aaa…haa…aa….aa.."/>
</div>
<div class="lyrico-lyrics-wrapper">Un karam neeti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un karam neeti"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjodu alli chella…vaa…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjodu alli chella…vaa…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhi ondru thaa mannava…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhi ondru thaa mannava…"/>
</div>
<div class="lyrico-lyrics-wrapper">Haa….aaa….haa….aa…mmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa….aaa….haa….aa…mmm"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En anbe en mannava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En anbe en mannava"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan nenjin mel naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan nenjin mel naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kann thunja vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kann thunja vaa vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naam ullae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam ullae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen intha mounam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen intha mounam"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan seidha paavam enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan seidha paavam enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Solvaayaa…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solvaayaa…"/>
</div>
</pre>
