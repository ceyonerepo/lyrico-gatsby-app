---
title: "maayavalai song lyrics"
album: "Mei"
artist: "Prithvi Kumar"
lyricist: "Christoper Pradeep"
director: "SA Baskaran"
path: "/albums/mei-lyrics"
song: "Maayavalai"
image: ../../images/albumart/mei.jpg
date: 2019-08-23
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Au2N3IfNGY4"
type: "melody"
singers:
  - Prithvi Kumar
  - Sanjana Raja
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kaayangal Aaridum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaayangal Aaridum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thazhumbugal Aarumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thazhumbugal Aarumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oviyan Kaigalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oviyan Kaigalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilangum Needhaan Adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilangum Needhaan Adi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manadhilae Vedhanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manadhilae Vedhanai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naadagam Yenadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naadagam Yenadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Mounamae Thaimozhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mounamae Thaimozhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Saabam Thanthaayadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saabam Thanthaayadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ularugiren Aiyo Ularugiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ularugiren Aiyo Ularugiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalum Un Perai Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalum Un Perai Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pulambigiren Aiyo Pulambigiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pulambigiren Aiyo Pulambigiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatril Kalandhen Adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatril Kalandhen Adi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa Vaa Arugey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Vaa Arugey"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyire Thee Patri Punnaaghudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyire Thee Patri Punnaaghudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa Vaa Uyire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Vaa Uyire"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini Nee Indri Naan Illaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Nee Indri Naan Illaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Paarkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Paarkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandhaegam Ketkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhaegam Ketkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Kallaana Kaadhal Kadhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kallaana Kaadhal Kadhai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chinna Paarvaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna Paarvaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Solladha Vaarthaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solladha Vaarthaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Neeyae En Maayavalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Neeyae En Maayavalai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa Vaa Uyire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Vaa Uyire"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini Nee Indri Naan Illaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Nee Indri Naan Illaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaayangal Aaridum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaayangal Aaridum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thazhumbugal Aarumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thazhumbugal Aarumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oviyan Kaigalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oviyan Kaigalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaagidham Aaghavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaagidham Aaghavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unarugiren Unnai Unarugiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unarugiren Unnai Unarugiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Unarugiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Unarugiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum Un Pola Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum Un Pola Dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nerungugiren Unnai Nerungugiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nerungugiren Unnai Nerungugiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanam Thadai Podumaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanam Thadai Podumaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa Vaa Arugey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Vaa Arugey"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyire Thee Patri Punnaaghudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyire Thee Patri Punnaaghudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa Vaa Uyire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Vaa Uyire"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini Nee Indri Naan Illaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Nee Indri Naan Illaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Netrunnai Paarthadhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netrunnai Paarthadhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Indrunnai Paarpaadhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indrunnai Paarpaadhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Kallaana Kaadhal Kadhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kallaana Kaadhal Kadhai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Polladha Jaadaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Polladha Jaadaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kan Pesum Vaarthaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan Pesum Vaarthaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Neeyae En Maayavalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Neeyae En Maayavalai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vandhen Yedhai Thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhen Yedhai Thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholaindhen Unnilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholaindhen Unnilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanae Naan Illaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanae Naan Illaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Nizhalo Ingillaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Nizhalo Ingillaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannodu Kan Paarkavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannodu Kan Paarkavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Madimel Naan Thoongavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Madimel Naan Thoongavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nammai Naan Paarkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nammai Naan Paarkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Yennil Unnai Naan Serkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yennil Unnai Naan Serkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa Vaa Arugey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Vaa Arugey"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyire Thee Patri Punnaaghudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyire Thee Patri Punnaaghudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa Vaa Uyire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Vaa Uyire"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini Nee Indri Naan Illaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Nee Indri Naan Illaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Netrunnai Paarthadhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netrunnai Paarthadhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Indrunnai Paarpaadhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indrunnai Paarpaadhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Kallaana Kaadhal Kadhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kallaana Kaadhal Kadhai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Polladha Jaadaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Polladha Jaadaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kan Pesum Vaarthaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan Pesum Vaarthaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Neeyae En Maayavalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Neeyae En Maayavalai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa Vaa Uyire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Vaa Uyire"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini Nee Indri Naan Illaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Nee Indri Naan Illaiyae"/>
</div>
</pre>
