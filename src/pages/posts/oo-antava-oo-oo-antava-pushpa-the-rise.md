---
title: "oo antava oo oo antava song lyrics"
album: "Pushpa The Rise"
artist: "Devi Sri Prasad"
lyricist: "Chandrabose"
director: "Sukumar"
path: "/albums/pushpa-the-rise-lyrics"
song: "Oo Antava Oo Oo Antava"
image: ../../images/albumart/pushpa-the-rise.jpg
date: 2021-12-17
lang: telugu
youtubeLink: "https://www.youtube.com/embed/u6BoyOceiPE"
type: "happy"
singers:
  - Indravathi Chauhan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Koka Koka Koka Kadithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koka Koka Koka Kadithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Kora Kora Mantu Choosthaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kora Kora Mantu Choosthaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Potti Potti Gown-ey Vesthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Potti Potti Gown-ey Vesthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Patti Patti Choosthaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Patti Patti Choosthaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koka Kaadhu Gown-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koka Kaadhu Gown-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhu Kattulona Yemundi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhu Kattulona Yemundi"/>
</div>
<div class="lyrico-lyrics-wrapper">Mee Kallallone Antha Undhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mee Kallallone Antha Undhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Mee Maga Buddhe Vankara Buddhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mee Maga Buddhe Vankara Buddhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oo Antava Maava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Antava Maava"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Oo Antava Maava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Oo Antava Maava"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Antava Maava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Antava Maava"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Oo Antava Maava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Oo Antava Maava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thella Thella Gunte Okadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thella Thella Gunte Okadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thallaa Kindhulowthadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thallaa Kindhulowthadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalla Nalla Gunte Okadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla Nalla Gunte Okadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Allarallari Chesthadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allarallari Chesthadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thelupu Nalupu Kaadhu Meeku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thelupu Nalupu Kaadhu Meeku"/>
</div>
<div class="lyrico-lyrics-wrapper">Rangutho Paniyemundhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangutho Paniyemundhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandhu Dorikindhante Saalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhu Dorikindhante Saalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mee Maga Buddhe Vankara Buddhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mee Maga Buddhe Vankara Buddhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oo Antava Maava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Antava Maava"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Oo Antava Maava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Oo Antava Maava"/>
</div>
<div class="lyrico-lyrics-wrapper">Haaye Oo Antava Maava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haaye Oo Antava Maava"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Oo Antava Maava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Oo Antava Maava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yetthu Yetthu Gunte Okadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yetthu Yetthu Gunte Okadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yegiri Ganthulesthaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yegiri Ganthulesthaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kurasa Kurasa Gunte Okadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurasa Kurasa Gunte Okadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Murisi Murisi Pothaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Murisi Murisi Pothaadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yetthu Kaadhu Kurasa Kaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yetthu Kaadhu Kurasa Kaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Meeko Satthem Sebuthaanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meeko Satthem Sebuthaanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Andhina Dhraakshe Theepi Meeku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhina Dhraakshe Theepi Meeku"/>
</div>
<div class="lyrico-lyrics-wrapper">Mee Maga Buddhe Vankara Buddhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mee Maga Buddhe Vankara Buddhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oo Antava Maava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Antava Maava"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Oo Antava Maava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Oo Antava Maava"/>
</div>
<div class="lyrico-lyrics-wrapper">Haaye Oo Antava Maava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haaye Oo Antava Maava"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Oo Antava Maava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Oo Antava Maava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Boddhu Boddhu Gunte Okadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boddhu Boddhu Gunte Okadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Muddhu Gunnav Antaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muddhu Gunnav Antaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sanna Sannangunte Okadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sanna Sannangunte Okadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarada Padipothuntaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarada Padipothuntaadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Boddhu Kaadhu Sannam Kaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boddhu Kaadhu Sannam Kaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vompu Sompu Kaadhandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vompu Sompu Kaadhandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ontiga Sikkamante Saalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ontiga Sikkamante Saalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mee Maga Buddhe Vankara Buddhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mee Maga Buddhe Vankara Buddhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oo Antava Maava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Antava Maava"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Oo Antava Maava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Oo Antava Maava"/>
</div>
<div class="lyrico-lyrics-wrapper">Oye Oo Antava Maava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oye Oo Antava Maava"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Oo Antava Maava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Oo Antava Maava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Peddha Peddha Manishi Laaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peddha Peddha Manishi Laaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Okadu Pojulu Kodathaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okadu Pojulu Kodathaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Manchi Manchi Manasundantu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manchi Manchi Manasundantu"/>
</div>
<div class="lyrico-lyrics-wrapper">Okadu Neethulu Sebuthaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okadu Neethulu Sebuthaadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manchi Kaadhu Seddaa Kaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manchi Kaadhu Seddaa Kaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Anthaa Okate Jathandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anthaa Okate Jathandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Deepalanni Aarpesaaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Deepalanni Aarpesaaka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uu Uu Uu Uu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uu Uu Uu Uu"/>
</div>
<div class="lyrico-lyrics-wrapper">Deepalanni Aarpesaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Deepalanni Aarpesaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Andhari Buddhi Vankara Buddhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhari Buddhi Vankara Buddhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oo Antava Mava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Antava Mava"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Oo Antava Mava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Oo Antava Mava"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Antame Mapa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Antame Mapa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Oo Antama Papa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Oo Antama Papa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oo Antava Mava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Antava Mava"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Oo Antava Mava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Oo Antava Mava"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Antame Papa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Antame Papa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Oo Antama Papa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Oo Antama Papa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oo Antava Maava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Antava Maava"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Oo Antava Maava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Oo Antava Maava"/>
</div>
</pre>
