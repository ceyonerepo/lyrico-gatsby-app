---
title: "inky pinky song lyrics"
album: "Natpuna Ennanu Theriyuma"
artist: "Dharan"
lyricist: "Jeyachandra Hashmi"
director: "Shiva Arvind"
path: "/albums/natpuna-ennanu-theriyuma-lyrics"
song: "Inky Pinky"
image: ../../images/albumart/natpuna-ennanu-theriyuma.jpg
date: 2019-05-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ToiHHjCFaIw"
type: "happy"
singers:
  - Dharan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Inky Pinky Ponky
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inky Pinky Ponky"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasu Oru Monkey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu Oru Monkey"/>
</div>
<div class="lyrico-lyrics-wrapper">Monkey Drive
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Monkey Drive"/>
</div>
<div class="lyrico-lyrics-wrapper">Buthi Dive
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Buthi Dive"/>
</div>
<div class="lyrico-lyrics-wrapper">Inky Pinky Ponky
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inky Pinky Ponky"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inky Ponky
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inky Ponky"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasu Monkey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu Monkey"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponnu Oruthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponnu Oruthi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenja Norukki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenja Norukki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hayyaiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hayyaiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadanthu Varuvaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadanthu Varuvaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulaga Beauty Ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulaga Beauty Ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadandhu Varuvaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadandhu Varuvaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pada Pada Pattasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pada Pada Pattasa"/>
</div>
<div class="lyrico-lyrics-wrapper">Koluthi Varuvaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koluthi Varuvaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adadada Assaultu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adadada Assaultu"/>
</div>
<div class="lyrico-lyrics-wrapper">Panni Varuvaa Va Va Va Va
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panni Varuvaa Va Va Va Va"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hormone Horn Adikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hormone Horn Adikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Veri Yeri Warning Kodukum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veri Yeri Warning Kodukum"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinandhorum Morning Muzhikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinandhorum Morning Muzhikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanda Kanavula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanda Kanavula"/>
</div>
<div class="lyrico-lyrics-wrapper">Manda Kalangidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manda Kalangidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Anjala Nenjula Konjala Minjala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anjala Nenjula Konjala Minjala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hormone Horn Adikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hormone Horn Adikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Veri Yeri Warning Kodukum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veri Yeri Warning Kodukum"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinandhorum Morning Muzhikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinandhorum Morning Muzhikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanda Kanavula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanda Kanavula"/>
</div>
<div class="lyrico-lyrics-wrapper">Manda Kalangidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manda Kalangidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Anjala Nenjula Konjala Minjala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anjala Nenjula Konjala Minjala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inky Pinky Ponky
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inky Pinky Ponky"/>
</div>
<div class="lyrico-lyrics-wrapper">Horn Adikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Horn Adikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasu Oru Monkey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu Oru Monkey"/>
</div>
<div class="lyrico-lyrics-wrapper">Warning Kodukum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Warning Kodukum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Monkey Drive
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Monkey Drive"/>
</div>
<div class="lyrico-lyrics-wrapper">Morning Muzhikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Morning Muzhikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Buthi Dive
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Buthi Dive"/>
</div>
<div class="lyrico-lyrics-wrapper">Manda Kalangidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manda Kalangidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Adichu Norukum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adichu Norukum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal Therikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Therikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenja Norukki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenja Norukki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kan Moodi Adichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan Moodi Adichen"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu Goalu Aachu Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu Goalu Aachu Daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Out Aagi Ponen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Out Aagi Ponen"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu No Ball Aachu Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu No Ball Aachu Daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Climate Tu Maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Climate Tu Maari"/>
</div>
<div class="lyrico-lyrics-wrapper">Sun Nu Moonu Aachu Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sun Nu Moonu Aachu Daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Allu Vuttu Poyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allu Vuttu Poyi"/>
</div>
<div class="lyrico-lyrics-wrapper">Semma Scene Nu Aachu Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Semma Scene Nu Aachu Daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Podu Podu Podu Podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podu Podu Podu Podu"/>
</div>
</pre>
