---
title: "en uyira nee irrundha song lyrics"
album: "Kuppathu Raja"
artist: "G. V. Prakash Kumar"
lyricist: "Logan"
director: "Baba Bhaskar"
path: "/albums/kuppathu-raja-lyrics"
song: "En Uyira Nee Irrundha"
image: ../../images/albumart/kuppathu-raja.jpg
date: 2019-04-05
lang: tamil
youtubeLink: "https://www.youtube.com/embed/I7m0wPg2XsY"
type: "sad"
singers:
  - G. V. Prakash Kumar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">En Uyiraai Nee Irundhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Uyiraai Nee Irundhaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Uyiraai Naan Irundhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Uyiraai Naan Irundhen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Irundhaal Naan Pizhaippen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Irundhaal Naan Pizhaippen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Pirindhaal Naan Irappen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Pirindhaal Naan Irappen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pogudhey Nizhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogudhey Nizhal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Nenjam Thaangumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nenjam Thaangumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal Vedhanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Vedhanai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Kangal Moodumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kangal Moodumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koodirindha Uravu Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koodirindha Uravu Ellaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paadhiyil Poyiduchey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadhiyil Poyiduchey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ini Enakku Yaar Irukkaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Enakku Yaar Irukkaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhayam Kilinjiduchey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam Kilinjiduchey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanimayaa Irundhaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanimayaa Irundhaney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Uyiraai Nee Irundhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Uyiraai Nee Irundhaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Uyiraai Naan Irundhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Uyiraai Naan Irundhen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Irundhaal Naan Pizhaippen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Irundhaal Naan Pizhaippen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Pirindhaal Naan Irappen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Pirindhaal Naan Irappen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Ninaivil Vaazhndhiruppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Ninaivil Vaazhndhiruppen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Kanavaai Naan Iruppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kanavaai Naan Iruppen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manasukulley Pirivin Vazhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasukulley Pirivin Vazhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pogudhu En Kaadhal Kizhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogudhu En Kaadhal Kizhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Thozhil Saaindhida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Thozhil Saaindhida"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Jeevan Ketkudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Jeevan Ketkudhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Meendum Pesida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Meendum Pesida"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Nenjam Yengudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nenjam Yengudhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Thavara Nenachu Ippo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Thavara Nenachu Ippo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhuvuraney Yekkathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhuvuraney Yekkathula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Kanavellaam Kalanjiduchey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kanavellaam Kalanjiduchey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Aaduna Aatatthula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Aaduna Aatatthula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Idhaiyam Udayudhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Idhaiyam Udayudhadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uthiram Karaiyudhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uthiram Karaiyudhadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Azhudhaa Naan Azhuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Azhudhaa Naan Azhuven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Azha Yaar Varuvaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Azha Yaar Varuvaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kerangitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kerangitten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Uyiraai Nee Irundhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Uyiraai Nee Irundhaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Uyiraai Naan Irundhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Uyiraai Naan Irundhen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Irundhaal Naan Pizhaippen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Irundhaal Naan Pizhaippen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Pirindhaal Naan Irappen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Pirindhaal Naan Irappen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Ninaivil Vaazhndhiruppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Ninaivil Vaazhndhiruppen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Kanavaai Naan Iruppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kanavaai Naan Iruppen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manasukulley Pirivin Vazhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasukulley Pirivin Vazhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pogudhu En Kaadhal Kizhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogudhu En Kaadhal Kizhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pogudhey Nilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogudhey Nilaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Nenjam Thaangumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nenjam Thaangumaa"/>
</div>
</pre>
