---
title: "nilavedhu karaiyedhu song lyrics"
album: "Kadugu"
artist: "S N Arunagiri"
lyricist: "Madhan Karky"
director: "Vijay Milton"
path: "/albums/kadugu-lyrics"
song: "Nilavedhu karaiyedhu"
image: ../../images/albumart/kadugu.jpg
date: 2017-03-24
lang: tamil
youtubeLink: "https://www.youtube.com/embed/cs5BlQIHAnw"
type: "sad"
singers:
  - Sundar Narayana Rao
  - Vaikom Vijayalakshmi	
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nilavedhu karaiedhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilavedhu karaiedhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagedhu kuraiedhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagedhu kuraiedhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Evanadhu ezhudhiyadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evanadhu ezhudhiyadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhudhidum thuligalin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhudhidum thuligalin"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanam kandu ranam solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanam kandu ranam solla"/>
</div>
<div class="lyrico-lyrics-wrapper">Manamingu pazhagiyadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manamingu pazhagiyadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennullae theeyo theeyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennullae theeyo theeyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada nenjae yaarai ketpaayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada nenjae yaarai ketpaayo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nilavedhu karaiedhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilavedhu karaiedhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagedhu kuraiedhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagedhu kuraiedhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Evanadhu ezhudhiyadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evanadhu ezhudhiyadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhudhidum thuligalin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhudhidum thuligalin"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanam kandu ranam solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanam kandu ranam solla"/>
</div>
<div class="lyrico-lyrics-wrapper">Manamingu pazhagiyadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manamingu pazhagiyadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nyaayam aniyaayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nyaayam aniyaayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu naalum maaraadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu naalum maaraadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuttram adhan aazhaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuttram adhan aazhaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhai sattangal kooraadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai sattangal kooraadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaayam thanakkendraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaayam thanakkendraal"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan kaththal thaangaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan kaththal thaangaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaayam piraakkendraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaayam piraakkendraal"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan kaadhugal ketkaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan kaadhugal ketkaadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pavam adhil siridhenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pavam adhil siridhenna"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunbam adhil peridhenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunbam adhil peridhenna"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarai ketpaayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarai ketpaayo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nilavedhu karaiedhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilavedhu karaiedhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagedhu kuraiedhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagedhu kuraiedhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Evanadhu ezhudhiyadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evanadhu ezhudhiyadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhudhidum thuligalin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhudhidum thuligalin"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanam kandu ranam solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanam kandu ranam solla"/>
</div>
<div class="lyrico-lyrics-wrapper">Manamingu pazhagiyadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manamingu pazhagiyadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennullae theeyo theeyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennullae theeyo theeyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada nenjae yaarai ketpaayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada nenjae yaarai ketpaayo"/>
</div>
</pre>
