---
title: "muttathey muttathey song lyrics"
album: "Nenjamundu Nermaiyundu Odu Raja"
artist: "Shabir"
lyricist: "Kiran Varthan"
director: "Karthik Venugopalan"
path: "/albums/nenjamundu-nermaiyundu-odu-raja-lyrics"
song: "Muttathey Muttathey"
image: ../../images/albumart/nenjamundu-nermaiyundu-odu-raja.jpg
date: 2019-06-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/b_og86-G1l4"
type: "love"
singers:
  - Ranjith
  - Janani Venkat
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Idhazh Yenguthu Noguthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhazh Yenguthu Noguthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Muthamida Vaada Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthamida Vaada Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Viral Theduthu Meeruthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viral Theduthu Meeruthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yuthamida Vaada Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yuthamida Vaada Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muttathae Enna Muttathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muttathae Enna Muttathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Muttathae Enna Muttathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muttathae Enna Muttathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannoda Kanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannoda Kanna"/>
</div>
<div class="lyrico-lyrics-wrapper">Katti Ilukkaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katti Ilukkaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjoda Nenja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjoda Nenja"/>
</div>
<div class="lyrico-lyrics-wrapper">Otti Orasaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Otti Orasaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Munnadi Ninnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munnadi Ninnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Mayakatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Mayakatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Muttathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muttathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennoda Thimira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennoda Thimira"/>
</div>
<div class="lyrico-lyrics-wrapper">Thatti Adakaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thatti Adakaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullara Usura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullara Usura"/>
</div>
<div class="lyrico-lyrics-wrapper">Thachchu Kilikaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thachchu Kilikaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ammadi Neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammadi Neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thitti Moraikaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thitti Moraikaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Muttathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muttathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muttathae Enna Muttathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muttathae Enna Muttathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Muttathae Enna Muttathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muttathae Enna Muttathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Muttathae Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muttathae Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muttathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muttathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannorama Minnum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannorama Minnum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallapaarva Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallapaarva Enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaaloda Kolusaa Manasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaloda Kolusaa Manasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maattitu Thudikudhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maattitu Thudikudhadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Othatoramaa Sindhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Othatoramaa Sindhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oththa Sirippa Paartha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oththa Sirippa Paartha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooratha Kaadhal Theanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooratha Kaadhal Theanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oori Poguthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oori Poguthadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Konji Konji Unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Konji Konji Unna"/>
</div>
<div class="lyrico-lyrics-wrapper">Thitti Theeka Poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thitti Theeka Poren"/>
</div>
<div class="lyrico-lyrics-wrapper">Kothi Kothi Pesi Unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kothi Kothi Pesi Unna"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal Seiya Poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Seiya Poren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bathil Ketkkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bathil Ketkkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Paarvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Paarvai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhir Theerkum En Seva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhir Theerkum En Seva"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirikiren Moraikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirikiren Moraikkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Sikki Poi Setharinenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sikki Poi Setharinenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muttathae Enna Muttathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muttathae Enna Muttathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Muttathae Enna Muttathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muttathae Enna Muttathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannoda Kanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannoda Kanna"/>
</div>
<div class="lyrico-lyrics-wrapper">Katti Ilukkaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katti Ilukkaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjoda Nenja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjoda Nenja"/>
</div>
<div class="lyrico-lyrics-wrapper">Otti Orasaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Otti Orasaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Munnadi Ninnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munnadi Ninnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Mayakatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Mayakatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Muttathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muttathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennoda Thimira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennoda Thimira"/>
</div>
<div class="lyrico-lyrics-wrapper">Thatti Adakaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thatti Adakaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullara Usura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullara Usura"/>
</div>
<div class="lyrico-lyrics-wrapper">Thachchu Kilikaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thachchu Kilikaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ammadi Neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammadi Neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thitti Moraikaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thitti Moraikaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Muttathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muttathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muttathae Enna Muttathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muttathae Enna Muttathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Muttathae Enna Muttathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muttathae Enna Muttathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Muttathae Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muttathae Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muttathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muttathae"/>
</div>
</pre>
