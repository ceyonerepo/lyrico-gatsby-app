---
title: "thaarayadi nee enakku lyrics"
album: "Ka Pae Ranasingam"
artist: "Ghibran"
lyricist: "Mahakavi Subramania Bharathiyar"
director: "P Virumandi"
path: "/albums/ka-pae-ranasingam-lyrics"
song: "Thaarayadi Nee Enakku"
image: ../../images/albumart/ka-pae-ranasingam.jpg
date: 2020-10-16
lang: tamil
youtubeLink: "https://www.youtube.com/embed/snF-lu36pXc"
type: "love"
singers:
  - Gold Devaraj
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Thaarayadi nee enakku
<input type="checkbox" class="lyrico-select-lyric-line" value="Thaarayadi nee enakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanmathiyam naan unakku
<input type="checkbox" class="lyrico-select-lyric-line" value="Thanmathiyam naan unakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaarayadi nee enakku
<input type="checkbox" class="lyrico-select-lyric-line" value="Thaarayadi nee enakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanmathiyam naan unakku
<input type="checkbox" class="lyrico-select-lyric-line" value="Thanmathiyam naan unakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Veeramadi nee enakku
<input type="checkbox" class="lyrico-select-lyric-line" value="Veeramadi nee enakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetriyadi naan unakku
<input type="checkbox" class="lyrico-select-lyric-line" value="Vetriyadi naan unakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Veeramadi nee enakku
<input type="checkbox" class="lyrico-select-lyric-line" value="Veeramadi nee enakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetriyadi naan unakku
<input type="checkbox" class="lyrico-select-lyric-line" value="Vetriyadi naan unakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Tharaniyil vaanulagil
<input type="checkbox" class="lyrico-select-lyric-line" value="Tharaniyil vaanulagil"/>
</div>
<div class="lyrico-lyrics-wrapper">Saarnthirukkum inbamellam
<input type="checkbox" class="lyrico-select-lyric-line" value="Saarnthirukkum inbamellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oor uruvamai samainthai
<input type="checkbox" class="lyrico-select-lyric-line" value="Oor uruvamai samainthai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullamuthe kannamma
<input type="checkbox" class="lyrico-select-lyric-line" value="Ullamuthe kannamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Oor uruvamai samainthai
<input type="checkbox" class="lyrico-select-lyric-line" value="Oor uruvamai samainthai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullamuthe kannamma…..
<input type="checkbox" class="lyrico-select-lyric-line" value="Ullamuthe kannamma….."/>
</div>
</pre>
