---
title: "swaasalona desame song lyrics"
album: "Sye Raa Narasimha Reddy"
artist: "Amit Trivedi"
lyricist: "Chandrabose"
director: "Surender Reddy"
path: "/albums/sye-raa-narasimha-reddy-lyrics"
song: "Swaasalona Desame"
image: ../../images/albumart/sye-raa-narasimha-reddy.jpg
date: 2019-10-02
lang: telugu
youtubeLink: "https://www.youtube.com/embed/bjc8caZoTjE"
type: "happy"
singers:
  - Haricharan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Swaasalona Desame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Swaasalona Desame"/>
</div>
<div class="lyrico-lyrics-wrapper">Swaasalona Desame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Swaasalona Desame"/>
</div>
<div class="lyrico-lyrics-wrapper">Gunde Goshalona Desame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gunde Goshalona Desame"/>
</div>
<div class="lyrico-lyrics-wrapper">Gunde Goshalona Desame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gunde Goshalona Desame"/>
</div>
<div class="lyrico-lyrics-wrapper">Praana Naadilona Desameyyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Praana Naadilona Desameyyy"/>
</div>
<div class="lyrico-lyrics-wrapper">Praanamantha Thalli Kosamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Praanamantha Thalli Kosamey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maatalona Desame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maatalona Desame"/>
</div>
<div class="lyrico-lyrics-wrapper">Maatalona Desame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maatalona Desame"/>
</div>
<div class="lyrico-lyrics-wrapper">Katthi Vetulona Desame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katthi Vetulona Desame"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetulona Desame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetulona Desame"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaali Adugulona Desame 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaali Adugulona Desame "/>
</div>
<div class="lyrico-lyrics-wrapper">Kaali Boodidhaina 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaali Boodidhaina "/>
</div>
<div class="lyrico-lyrics-wrapper">Thalli Kosame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalli Kosame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Desamey Nuvvuraa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Desamey Nuvvuraa "/>
</div>
<div class="lyrico-lyrics-wrapper">Sandheshamayyeraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandheshamayyeraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Desamey Nuvvuraa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Desamey Nuvvuraa "/>
</div>
<div class="lyrico-lyrics-wrapper">Sandheshamayyeraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandheshamayyeraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandheshamayyaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandheshamayyaraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chinnari Praayamandhunaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinnari Praayamandhunaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Konnollanodhili Naavuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konnollanodhili Naavuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Konnollanodhili Naavuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konnollanodhili Naavuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanneeta Padhunu Teleraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneeta Padhunu Teleraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Khadgamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khadgamey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Prayaanamaina Poruloo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prayaanamaina Poruloo"/>
</div>
<div class="lyrico-lyrics-wrapper">Prayaanamaina Poruloo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prayaanamaina Poruloo"/>
</div>
<div class="lyrico-lyrics-wrapper">Preminka Inki Poyaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Preminka Inki Poyaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Preminka Inki Poyaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Preminka Inki Poyaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhositlo Dhaachinaavuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhositlo Dhaachinaavuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandhramey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhramey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Prajala Swecchakai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prajala Swecchakai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pranalanodhuluthuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pranalanodhuluthuu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pathaakamalle Yegirinaavuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathaakamalle Yegirinaavuraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Desamey Nuvvuraa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Desamey Nuvvuraa "/>
</div>
<div class="lyrico-lyrics-wrapper">Sandheshamayyeraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandheshamayyeraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Desamey Nuvvuraa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Desamey Nuvvuraa "/>
</div>
<div class="lyrico-lyrics-wrapper">Sandheshamayyeraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandheshamayyeraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandheshamayyaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandheshamayyaraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Desamey Nuvvuraa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Desamey Nuvvuraa "/>
</div>
<div class="lyrico-lyrics-wrapper">Sandheshamayyeraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandheshamayyeraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Desamey Nuvvuraa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Desamey Nuvvuraa "/>
</div>
<div class="lyrico-lyrics-wrapper">Sandheshamayyeraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandheshamayyeraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandheshamayyaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandheshamayyaraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Desamey Nuvvuraa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Desamey Nuvvuraa "/>
</div>
<div class="lyrico-lyrics-wrapper">Sandheshamayyeraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandheshamayyeraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Desamey Nuvvuraa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Desamey Nuvvuraa "/>
</div>
<div class="lyrico-lyrics-wrapper">Sandheshamayyeraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandheshamayyeraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandheshamayyaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandheshamayyaraa"/>
</div>
</pre>
