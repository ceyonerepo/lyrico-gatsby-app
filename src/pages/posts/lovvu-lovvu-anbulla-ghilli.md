---
title: "lovvu lovvu song lyrics"
album: "Anbulla Ghilli"
artist: "Arrol Corelli"
lyricist: "Rajavel Nagarajan"
director: "Srinath Ramalingam"
path: "/albums/anbulla-ghilli-lyrics"
song: "Lovvu Lovvu"
image: ../../images/albumart/anbulla-ghilli.jpg
date: 2022-02-06
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Yr9JQOpG1Zw"
type: "happy"
singers:
  - Yuvan Shankar Raja
  - Andrea Jeremiah
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hmm mm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm mm"/>
</div>
<div class="lyrico-lyrics-wrapper">Aazhiya en thozhiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aazhiya en thozhiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan jolly-ah kolamburen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan jolly-ah kolamburen"/>
</div>
<div class="lyrico-lyrics-wrapper">Jolly-ah namma vaaliya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jolly-ah namma vaaliya"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu kavithaiya polamburen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu kavithaiya polamburen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gethuaana ghilli naanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gethuaana ghilli naanadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Veththaagi ponen yenadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veththaagi ponen yenadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Maasaana piece-u naanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maasaana piece-u naanadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Loosagi ponen yenadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Loosagi ponen yenadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lovvu lovvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lovvu lovvu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullae poothuruchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullae poothuruchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai saachuruchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai saachuruchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pottu thaakkiruchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pottu thaakkiruchu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lovvu lovvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lovvu lovvu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai maathiruchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai maathiruchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai paarpathaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai paarpathaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">En velaiyakkiruchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En velaiyakkiruchu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lovvu lovvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lovvu lovvu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullae poothuruchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullae poothuruchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai saachuruchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai saachuruchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pottu thaakkiruchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pottu thaakkiruchu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lovvu lovvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lovvu lovvu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai maathiruchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai maathiruchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai paarpathaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai paarpathaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">En velaiyakkiruchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En velaiyakkiruchu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thedinen vaadinen Odinen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedinen vaadinen Odinen"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai kaana thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai kaana thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbae endhan vizhi yenguthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbae endhan vizhi yenguthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vegiren saigiren nuzhaigiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vegiren saigiren nuzhaigiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai sera thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai sera thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbae endhan uyir vaazhuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbae endhan uyir vaazhuthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En nenjil sevi vaithu kettaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En nenjil sevi vaithu kettaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu un perai thudikindrathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu un perai thudikindrathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un meedhu naan konda kaadhal thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un meedhu naan konda kaadhal thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu kollaamal kolgindrathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu kollaamal kolgindrathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arugil vaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arugil vaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Iruga thaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruga thaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal mazhai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal mazhai "/>
</div>
<div class="lyrico-lyrics-wrapper">ingae pozhigindrathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ingae pozhigindrathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chi chi poda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chi chi poda"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetkam yenda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetkam yenda"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal thee 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal thee "/>
</div>
<div class="lyrico-lyrics-wrapper">ingu yerigindrathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ingu yerigindrathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lovvu lovvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lovvu lovvu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullae poothuruchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullae poothuruchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai saachuruchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai saachuruchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pottu thaakkiruchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pottu thaakkiruchu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lovvu lovvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lovvu lovvu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai maathiruchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai maathiruchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai paarpathaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai paarpathaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">En velaiyakkiruchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En velaiyakkiruchu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lovvu lovvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lovvu lovvu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullae poothuruchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullae poothuruchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai saachuruchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai saachuruchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pottu thaakkiruchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pottu thaakkiruchu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lovvu lovvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lovvu lovvu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai maathiruchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai maathiruchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai paarpathaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai paarpathaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">En velaiyakkiruchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En velaiyakkiruchu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhala en kaadhala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhala en kaadhala"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan crazy-ah maaritten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan crazy-ah maaritten"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh kaadhala un kaadhalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh kaadhala un kaadhalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Romba easy-ah urugitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba easy-ah urugitten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kickaana ponnu naanada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kickaana ponnu naanada"/>
</div>
<div class="lyrico-lyrics-wrapper">Makkaagi ponen yenada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Makkaagi ponen yenada"/>
</div>
<div class="lyrico-lyrics-wrapper">Maasaana piece-u naanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maasaana piece-u naanadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Loosu loosagi ponen yenadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Loosu loosagi ponen yenadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hooo ooo ooo ooo oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hooo ooo ooo ooo oo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooo oo oo oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooo oo oo oo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lovvu lovvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lovvu lovvu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullae poothuruchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullae poothuruchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai saachuruchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai saachuruchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pottu thaakkiruchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pottu thaakkiruchu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lovvu lovvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lovvu lovvu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai maathiruchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai maathiruchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai paarpathaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai paarpathaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">En velaiyakkiruchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En velaiyakkiruchu"/>
</div>
</pre>
