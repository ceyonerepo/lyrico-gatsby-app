---
title: "saradhaga kashtanni song lyrics"
album: "Degree College"
artist: "Sunil Kashyap"
lyricist: "Vanamali"
director: "Narasimha Nandi"
path: "/albums/degree-college-lyrics"
song: "Saradhaga Kashtanni"
image: ../../images/albumart/degree-college.jpg
date: 2020-02-07
lang: telugu
youtubeLink: "https://www.youtube.com/embed/IC8ABC-s_kg"
type: "love"
singers:
  - Chinmayi Sripada
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Saradaga Kashtanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saradaga Kashtanni"/>
</div>
<div class="lyrico-lyrics-wrapper">Visiginchie Kaalanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Visiginchie Kaalanni"/>
</div>
<div class="lyrico-lyrics-wrapper">Gelicheddam Navvulu Pooyinchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gelicheddam Navvulu Pooyinchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Veelaithe Baadhalni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veelaithe Baadhalni"/>
</div>
<div class="lyrico-lyrics-wrapper">Loloni Bhaaranni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Loloni Bhaaranni"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharimeddam Paruge Theeyinchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharimeddam Paruge Theeyinchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Okatayye Premedhaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okatayye Premedhaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Venakaduge Vesenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venakaduge Vesenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Nimisham Jeevithamaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Nimisham Jeevithamaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhigaalu Padena THamashakaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhigaalu Padena THamashakaina"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saradaga Kashtanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saradaga Kashtanni"/>
</div>
<div class="lyrico-lyrics-wrapper">Visiginchie Kaalanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Visiginchie Kaalanni"/>
</div>
<div class="lyrico-lyrics-wrapper">Gelicheddam Navvulu Pooyinchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gelicheddam Navvulu Pooyinchi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enni Mullu Thaakina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enni Mullu Thaakina"/>
</div>
<div class="lyrico-lyrics-wrapper">Evvarentha Aapina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evvarentha Aapina"/>
</div>
<div class="lyrico-lyrics-wrapper">Lokamantha Chaati Cheppanee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lokamantha Chaati Cheppanee"/>
</div>
<div class="lyrico-lyrics-wrapper">Veyyi Sarlu Puttina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veyyi Sarlu Puttina"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheyyi Cheyyi Veedani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheyyi Cheyyi Veedani"/>
</div>
<div class="lyrico-lyrics-wrapper">Prema Janta Neevu Nenani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prema Janta Neevu Nenani"/>
</div>
<div class="lyrico-lyrics-wrapper">Okasari Preminchi Odicherithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okasari Preminchi Odicherithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Brathukaina Chaavaina Kalise Kadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Brathukaina Chaavaina Kalise Kadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Pree maa Nuvve Leka Memuntama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pree maa Nuvve Leka Memuntama"/>
</div>
<div class="lyrico-lyrics-wrapper">Saradaga Kashtanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saradaga Kashtanni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saradaga Kashtanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saradaga Kashtanni"/>
</div>
<div class="lyrico-lyrics-wrapper">Visiginchie Kaalanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Visiginchie Kaalanni"/>
</div>
<div class="lyrico-lyrics-wrapper">Gelicheddam Navvulu Pooyinchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gelicheddam Navvulu Pooyinchi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ee Prapancham Enthaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Prapancham Enthaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaripoye Vinthaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaripoye Vinthaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Prema Moodu Mullu Veyagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prema Moodu Mullu Veyagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Haayi Ningi Nantaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haayi Ningi Nantaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Gunde Nammanandhiga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gunde Nammanandhiga"/>
</div>
<div class="lyrico-lyrics-wrapper">Niti Sambaraala Veduka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niti Sambaraala Veduka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadadhaka Beathukantha Ee Theeruga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadadhaka Beathukantha Ee Theeruga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalalaaga Gadipesthe Adhi Chaaluga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalalaaga Gadipesthe Adhi Chaaluga"/>
</div>
<div class="lyrico-lyrics-wrapper">Pree maa Neevallega Maakee Dheema
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pree maa Neevallega Maakee Dheema"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saradaga Kashtanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saradaga Kashtanni"/>
</div>
<div class="lyrico-lyrics-wrapper">Visiginchie Kaalanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Visiginchie Kaalanni"/>
</div>
<div class="lyrico-lyrics-wrapper">Gelicheddam Navvulu Pooyinchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gelicheddam Navvulu Pooyinchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Veelaithe Baadhalni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veelaithe Baadhalni"/>
</div>
<div class="lyrico-lyrics-wrapper">Loloni Bhaaranni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Loloni Bhaaranni"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharimeddam Paruge Theeyinchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharimeddam Paruge Theeyinchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Okatayye Premedhaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okatayye Premedhaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Venakaduge Vesenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venakaduge Vesenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Nimisham Jeevithamaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Nimisham Jeevithamaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhigaalu Padena THamashakaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhigaalu Padena THamashakaina"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saradaga Kashtanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saradaga Kashtanni"/>
</div>
<div class="lyrico-lyrics-wrapper">Visiginchie Kaalanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Visiginchie Kaalanni"/>
</div>
<div class="lyrico-lyrics-wrapper">Gelicheddam Navvulu Pooyinchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gelicheddam Navvulu Pooyinchi"/>
</div>
</pre>
