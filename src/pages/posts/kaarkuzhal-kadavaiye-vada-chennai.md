---
title: "kaarkuzhal kadavaiye song lyrics"
album: "Vada Chennai"
artist: "Santhosh Narayanan"
lyricist: "Vivek"
director: "Vetrimaaran"
path: "/albums/vada-chennai-lyrics"
song: "Kaarkuzhal Kadavaiye"
image: ../../images/albumart/vada-chennai.jpg
date: 2018-10-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/3VBLaFC8QfQ"
type: "happy"
singers:
  - Santhosh Narayanan
  - Sriram Parthasarathy
  - Pradeep Kumar
  - Ananthu
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kaarkuzhal kadavaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaarkuzhal kadavaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai engae izhukkiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai engae izhukkiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaazhaga vazhiyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaazhaga vazhiyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavugal iraikkiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavugal iraikkiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannaadi koppai aazhiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaadi koppai aazhiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan kaimeeri serndha theiyilai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan kaimeeri serndha theiyilai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannangal moodi oramaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannangal moodi oramaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee nindraalae andrae theipirai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee nindraalae andrae theipirai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kiliyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kiliyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee pirindhaal saagiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee pirindhaal saagiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Viragaai un vizhiyae ketkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viragaai un vizhiyae ketkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Uliyae un urasal yerkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uliyae un urasal yerkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkaai en kuraigal thorkkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkaai en kuraigal thorkkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaarkuzhal kadavaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaarkuzhal kadavaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai engae izhukkiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai engae izhukkiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaazhaga vazhiyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaazhaga vazhiyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavugal iraikkiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavugal iraikkiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inneram minnalgal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inneram minnalgal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanodu naanum kandaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanodu naanum kandaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Angae nee punnagai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Angae nee punnagai"/>
</div>
<div class="lyrico-lyrics-wrapper">Seithanal engiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seithanal engiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inneram bhoogambam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inneram bhoogambam"/>
</div>
<div class="lyrico-lyrics-wrapper">En nenjai thaakkinaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En nenjai thaakkinaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Angae nee kanmoodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Angae nee kanmoodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirandhanal engiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirandhanal engiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh oooooo hooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh oooooo hooooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoooooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaarkuzhal kadavaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaarkuzhal kadavaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai engae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai engae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaazhaga vazhiyilae kanavugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaazhaga vazhiyilae kanavugal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannaadi koppai aazhiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaadi koppai aazhiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan kaimeeri serndha theiyilai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan kaimeeri serndha theiyilai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannangal moodi oramaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannangal moodi oramaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee nindraalae andrae theipirai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee nindraalae andrae theipirai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kiliyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kiliyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee pirindhaal saagiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee pirindhaal saagiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Viragaai un vizhiyae ketkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viragaai un vizhiyae ketkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Uliyae un urasal yerkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uliyae un urasal yerkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkaai en kuraigal thorkkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkaai en kuraigal thorkkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaarkuzhal kadavaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaarkuzhal kadavaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai engae izhukkiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai engae izhukkiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaazhaga vazhiyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaazhaga vazhiyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavugal iraikkiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavugal iraikkiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inneram minnalgal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inneram minnalgal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanodu naanum kandal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanodu naanum kandal"/>
</div>
<div class="lyrico-lyrics-wrapper">Angae nee punnagai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Angae nee punnagai"/>
</div>
<div class="lyrico-lyrics-wrapper">Seithanal engiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seithanal engiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inneram bhoogambam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inneram bhoogambam"/>
</div>
<div class="lyrico-lyrics-wrapper">En nenjai thaakkinaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En nenjai thaakkinaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Angae nee kanmoodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Angae nee kanmoodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirandhanal engiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirandhanal engiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un kottam paarthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kottam paarthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Poo vattam paarthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poo vattam paarthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kan vittam paarthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan vittam paarthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thee patrum kaatru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thee patrum kaatru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thol machcham paarthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thol machcham paarthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mel micham paarthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mel micham paarthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thean latcham paarthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thean latcham paarthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadai pizharittru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadai pizharittru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inayaai unai adaigiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inayaai unai adaigiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Enayae vazhi mozhigiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enayae vazhi mozhigiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Engae nenjin nallaal engae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engae nenjin nallaal engae"/>
</div>
<div class="lyrico-lyrics-wrapper">Inbam minjum illaal engae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inbam minjum illaal engae"/>
</div>
<div class="lyrico-lyrics-wrapper">Engum vanjam allaal engae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engum vanjam allaal engae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kondrai konjum sillaal engae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondrai konjum sillaal engae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kiliyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kiliyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee pirindhaal saagiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee pirindhaal saagiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Viragaai un vizhiyae ketkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viragaai un vizhiyae ketkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Uliyae un urasal yerkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uliyae un urasal yerkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkaai en kuraigal thorkkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkaai en kuraigal thorkkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaarkuzhal kadavaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaarkuzhal kadavaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai engae izhukkiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai engae izhukkiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaazhaga vazhiyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaazhaga vazhiyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavugal yeh aaa yeh aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavugal yeh aaa yeh aa"/>
</div>
</pre>
