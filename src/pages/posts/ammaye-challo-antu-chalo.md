---
title: "ammaye challo antu song lyrics"
album: "Chalo"
artist: "Mahati Swara Sagar"
lyricist: "Krishna Madineni"
director: "Venky Kudumula"
path: "/albums/chalo-lyrics"
song: "Ammaye Challo Antu"
image: ../../images/albumart/chalo.jpg
date: 2018-02-02
lang: telugu
youtubeLink: "https://www.youtube.com/embed/K2-0aaEI490"
type: "love"
singers:
  - Yazin Nizar
  - Lipsika
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ammaaye Challo Antu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammaaye Challo Antu "/>
</div>
<div class="lyrico-lyrics-wrapper">Naatho Vacchesindilaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naatho Vacchesindilaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Laife Antaa Neeto Unde 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laife Antaa Neeto Unde "/>
</div>
<div class="lyrico-lyrics-wrapper">Premundee Naalonaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Premundee Naalonaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Pillemo Tulli Tulli 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pillemo Tulli Tulli "/>
</div>
<div class="lyrico-lyrics-wrapper">Nanne Allesindila 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanne Allesindila "/>
</div>
<div class="lyrico-lyrics-wrapper">Neekosam Malli Putte 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neekosam Malli Putte "/>
</div>
<div class="lyrico-lyrics-wrapper">Picchundee Neepainaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Picchundee Neepainaa "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">I Love You Love You 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Love You Love You "/>
</div>
<div class="lyrico-lyrics-wrapper">Antu Naa Gunde Kottukundee 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antu Naa Gunde Kottukundee "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa Honey Honey Antu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Honey Honey Antu "/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Pere Palikinde 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Pere Palikinde "/>
</div>
<div class="lyrico-lyrics-wrapper">Emaindo Emo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emaindo Emo "/>
</div>
<div class="lyrico-lyrics-wrapper">Gaani Chedipotunaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaani Chedipotunaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvante Malli 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvante Malli "/>
</div>
<div class="lyrico-lyrics-wrapper">Malli Padipotunnaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malli Padipotunnaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Neekosam Nanne 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neekosam Nanne "/>
</div>
<div class="lyrico-lyrics-wrapper">Nenu Vadilestunnaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenu Vadilestunnaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Neetone Nimishaalanni 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neetone Nimishaalanni "/>
</div>
<div class="lyrico-lyrics-wrapper">Gadipestunnaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gadipestunnaa "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chalo Chalo Anee 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chalo Chalo Anee "/>
</div>
<div class="lyrico-lyrics-wrapper">Neetone Vastoo Unnaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neetone Vastoo Unnaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Yevaipaina Pada 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yevaipaina Pada "/>
</div>
<div class="lyrico-lyrics-wrapper">Pade Pade Ila 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pade Pade Ila "/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Maate Vintu Unna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Maate Vintu Unna "/>
</div>
<div class="lyrico-lyrics-wrapper">Ide Nijam Kadaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ide Nijam Kadaa "/>
</div>
<div class="lyrico-lyrics-wrapper">O Meri Lailaa Nee Valle 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Meri Lailaa Nee Valle "/>
</div>
<div class="lyrico-lyrics-wrapper">Enno Enno Naalo Maarene 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enno Enno Naalo Maarene "/>
</div>
<div class="lyrico-lyrics-wrapper">Nanne Maarchane 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanne Maarchane "/>
</div>
<div class="lyrico-lyrics-wrapper">E Pehali Nazar Nuvvante 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="E Pehali Nazar Nuvvante "/>
</div>
<div class="lyrico-lyrics-wrapper">Nanne Minche Ishtam Naadile 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanne Minche Ishtam Naadile "/>
</div>
<div class="lyrico-lyrics-wrapper">Duniyaa Needile 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Duniyaa Needile "/>
</div>
<div class="lyrico-lyrics-wrapper">Emaindo Emo Gaani Chedipotunaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emaindo Emo Gaani Chedipotunaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvante Malli Malli Padipotunnaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvante Malli Malli Padipotunnaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Neekosam Nanne Nenu Vadilestunnaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neekosam Nanne Nenu Vadilestunnaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Neetone Nimishaalanni Gadipestunnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neetone Nimishaalanni Gadipestunnaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tane Tane Kadaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tane Tane Kadaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Vaadu Antuu Undi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vaadu Antuu Undi "/>
</div>
<div class="lyrico-lyrics-wrapper">Made Nanne Tattee 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Made Nanne Tattee "/>
</div>
<div class="lyrico-lyrics-wrapper">Mude Pade Kathaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mude Pade Kathaa "/>
</div>
<div class="lyrico-lyrics-wrapper">E Naadu Antuu Undi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="E Naadu Antuu Undi "/>
</div>
<div class="lyrico-lyrics-wrapper">Gude Gante Kotti 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gude Gante Kotti "/>
</div>
<div class="lyrico-lyrics-wrapper">O Meri Jaanaa Nee Navve 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Meri Jaanaa Nee Navve "/>
</div>
<div class="lyrico-lyrics-wrapper">Nanne Patti Gunjelesane 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanne Patti Gunjelesane "/>
</div>
<div class="lyrico-lyrics-wrapper">Praanam Laagane 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Praanam Laagane "/>
</div>
<div class="lyrico-lyrics-wrapper">O Tuuhi Meraa Gundello 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Tuuhi Meraa Gundello "/>
</div>
<div class="lyrico-lyrics-wrapper">Ninne Uncha Nene Lenule 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninne Uncha Nene Lenule "/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvve Nenule 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvve Nenule "/>
</div>
<div class="lyrico-lyrics-wrapper">Emaindo Emo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emaindo Emo "/>
</div>
<div class="lyrico-lyrics-wrapper">Gaani Chedipotunaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaani Chedipotunaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvante Malli 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvante Malli "/>
</div>
<div class="lyrico-lyrics-wrapper">Malli Padipotunnaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malli Padipotunnaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Neekosam Nanne 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neekosam Nanne "/>
</div>
<div class="lyrico-lyrics-wrapper">Nenu Vadilestunnaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenu Vadilestunnaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Neetone Nimishaalanni 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neetone Nimishaalanni "/>
</div>
<div class="lyrico-lyrics-wrapper">Gadipestunnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gadipestunnaa"/>
</div>
</pre>
