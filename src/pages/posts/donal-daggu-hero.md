---
title: "donal daggu song lyrics"
album: "Hero"
artist: "Ghibran"
lyricist: "Roll Rida"
director: "Sriram Aditya"
path: "/albums/hero-lyrics"
song: "Donal Daggu"
image: ../../images/albumart/hero-telugu.jpg
date: 2022-01-15
lang: telugu
youtubeLink: "https://www.youtube.com/embed/hhwy3F1Lnpc"
type: "happy"
singers:
  - Roll Rida
  - Gold Devaraj
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ooru vaa choodu eeda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooru vaa choodu eeda"/>
</div>
<div class="lyrico-lyrics-wrapper">Anna game halchal undi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anna game halchal undi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kiraaku ne penchamandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kiraaku ne penchamandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Jilla motham ooguthondi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jilla motham ooguthondi"/>
</div>
<div class="lyrico-lyrics-wrapper">Galla ethi step lendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Galla ethi step lendi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Padi padi padi ichi padei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padi padi padi ichi padei"/>
</div>
<div class="lyrico-lyrics-wrapper">Veedeioora mass u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veedeioora mass u"/>
</div>
<div class="lyrico-lyrics-wrapper">Adugaduguna danchi kottei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adugaduguna danchi kottei"/>
</div>
<div class="lyrico-lyrics-wrapper">Andari kanna khaasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andari kanna khaasu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gala gala mani gola pettei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gala gala mani gola pettei"/>
</div>
<div class="lyrico-lyrics-wrapper">Style first class u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Style first class u"/>
</div>
<div class="lyrico-lyrics-wrapper">anna ku zara Goduku pattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anna ku zara Goduku pattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Full on jhakkassu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Full on jhakkassu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kotta kotta kottara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kotta kotta kottara"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinigettu kottara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinigettu kottara"/>
</div>
<div class="lyrico-lyrics-wrapper">Ekketattu kottara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ekketattu kottara"/>
</div>
<div class="lyrico-lyrics-wrapper">Irigettu kottara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irigettu kottara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Everybody come on sing it
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Everybody come on sing it"/>
</div>
<div class="lyrico-lyrics-wrapper">Let's play the beat now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Let's play the beat now"/>
</div>
<div class="lyrico-lyrics-wrapper">Everybody come on sing it
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Everybody come on sing it"/>
</div>
<div class="lyrico-lyrics-wrapper">Let's play the beat now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Let's play the beat now"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">I wanna do do
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I wanna do do"/>
</div>
<div class="lyrico-lyrics-wrapper">You wanna do do
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You wanna do do"/>
</div>
<div class="lyrico-lyrics-wrapper">We all do do
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="We all do do"/>
</div>
<div class="lyrico-lyrics-wrapper">Let's all do
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Let's all do"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Cow boy come uh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cow boy come uh"/>
</div>
<div class="lyrico-lyrics-wrapper">Cow boy come uh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cow boy come uh"/>
</div>
<div class="lyrico-lyrics-wrapper">Cow boy come uh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cow boy come uh"/>
</div>
<div class="lyrico-lyrics-wrapper">Come uh come uh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Come uh come uh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Little boy come uh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Little boy come uh"/>
</div>
<div class="lyrico-lyrics-wrapper">Little boy come uh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Little boy come uh"/>
</div>
<div class="lyrico-lyrics-wrapper">Little boy come uh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Little boy come uh"/>
</div>
<div class="lyrico-lyrics-wrapper">Come uh come uh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Come uh come uh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Step chupinchina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Step chupinchina"/>
</div>
<div class="lyrico-lyrics-wrapper">Chiranjeevi boss u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chiranjeevi boss u"/>
</div>
<div class="lyrico-lyrics-wrapper">Style neripinchina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Style neripinchina"/>
</div>
<div class="lyrico-lyrics-wrapper">Rajinikanth mass u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rajinikanth mass u"/>
</div>
<div class="lyrico-lyrics-wrapper">Senti avvalante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senti avvalante"/>
</div>
<div class="lyrico-lyrics-wrapper">Venki maama class u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venki maama class u"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoda kottalante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoda kottalante"/>
</div>
<div class="lyrico-lyrics-wrapper">Ballaya babu bestu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ballaya babu bestu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Screen mida atta 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Screen mida atta "/>
</div>
<div class="lyrico-lyrics-wrapper">nenu ochinante itta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenu ochinante itta"/>
</div>
<div class="lyrico-lyrics-wrapper">Jilelamma jitta 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jilelamma jitta "/>
</div>
<div class="lyrico-lyrics-wrapper">vestha banti poola butta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vestha banti poola butta"/>
</div>
<div class="lyrico-lyrics-wrapper">Beat beat edi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Beat beat edi "/>
</div>
<div class="lyrico-lyrics-wrapper">ochesthundi get ready
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ochesthundi get ready"/>
</div>
<div class="lyrico-lyrics-wrapper">Beat beat edi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Beat beat edi "/>
</div>
<div class="lyrico-lyrics-wrapper">kottu kottu mari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kottu kottu mari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Donal daggu margayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Donal daggu margayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Donal daggu margayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Donal daggu margayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Donal daggu margayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Donal daggu margayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Donal daggu margayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Donal daggu margayi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">I wanna do do
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I wanna do do"/>
</div>
<div class="lyrico-lyrics-wrapper">You wanna do do
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You wanna do do"/>
</div>
<div class="lyrico-lyrics-wrapper">We all do do
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="We all do do"/>
</div>
<div class="lyrico-lyrics-wrapper">Lets all do
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lets all do"/>
</div>
<div class="lyrico-lyrics-wrapper">Lets do
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lets do"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Cow boy bome uh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cow boy bome uh"/>
</div>
<div class="lyrico-lyrics-wrapper">Cow boy come uh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cow boy come uh"/>
</div>
<div class="lyrico-lyrics-wrapper">Cow boy come uh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cow boy come uh"/>
</div>
<div class="lyrico-lyrics-wrapper">Come uh come uh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Come uh come uh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Little boy come uh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Little boy come uh"/>
</div>
<div class="lyrico-lyrics-wrapper">Little boy come uh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Little boy come uh"/>
</div>
<div class="lyrico-lyrics-wrapper">Little boy come uh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Little boy come uh"/>
</div>
<div class="lyrico-lyrics-wrapper">Come uh come uh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Come uh come uh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Donal daggu margayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Donal daggu margayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Donal daggu margayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Donal daggu margayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Donal daggu margayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Donal daggu margayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Donal daggu margayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Donal daggu margayi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Donal daggu margayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Donal daggu margayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Donal daggu margayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Donal daggu margayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Donal daggu margayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Donal daggu margayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Donal daggu margayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Donal daggu margayi"/>
</div>
</pre>
