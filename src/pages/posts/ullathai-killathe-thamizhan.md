---
title: "ullathai killathe song lyrics"
album: "Thamizhan"
artist: "D. Imman"
lyricist: "Vairamuthu"
director: "Majith"
path: "/albums/thamizhan-lyrics"
song: "Ullathai Killathe"
image: ../../images/albumart/thamizhan.jpg
date: 2002-04-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/1hZyOydx7n4"
type: "love"
singers:
  - Vijay
  - Priyanka Chopra
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ullathai Killaathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullathai Killaathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Killi Vittu Sellaathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Killi Vittu Sellaathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaayathil Mutham Veiyappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaayathil Mutham Veiyappa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muthathin Mudivil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthathin Mudivil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muzhu Aalai Thinnuvaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muzhu Aalai Thinnuvaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaangaathu Yappa Yappappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaangaathu Yappa Yappappa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ullathai Killaathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullathai Killaathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Killi Vittu Sellaathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Killi Vittu Sellaathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaayathil Mutham Veiyappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaayathil Mutham Veiyappa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaayathil Mutham Veiyappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaayathil Mutham Veiyappa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muthathin Mudivil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthathin Mudivil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muzhu Aalai Thinnuvaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muzhu Aalai Thinnuvaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaangaathu Yappa Yappappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaangaathu Yappa Yappappa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yappappappaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yappappappaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oi Rettai Chattai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oi Rettai Chattai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rettai Dhegam Anaiya Koodaathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rettai Dhegam Anaiya Koodaathaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Innum Sila Naal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Innum Sila Naal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karppoda Inga Irukka Koodaathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karppoda Inga Irukka Koodaathaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Mutham Nooru Thanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Mutham Nooru Thanthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kattil Varumai Theerkka Koodaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattil Varumai Theerkka Koodaatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ullathai Killaathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullathai Killaathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Killi Vittu Sellaathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Killi Vittu Sellaathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaayathil Mutham Veiyappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaayathil Mutham Veiyappa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muthathin Mudivil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthathin Mudivil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muzhu Aalai Thinnuvaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muzhu Aalai Thinnuvaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaangaathu Yappa Yappappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaangaathu Yappa Yappappa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Undhan Perai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Undhan Perai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thirudi Konden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirudi Konden"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thirudi Kondaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirudi Kondaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naavukkul Pachai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naavukkul Pachai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuthi Konden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuthi Konden"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pachai Kuthi Kondaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pachai Kuthi Kondaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaarodum Sollaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarodum Sollaamal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Censor-ey Illaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Censor-ey Illaamal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Super Kaadhal Solli Tharavey Vandhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Super Kaadhal Solli Tharavey Vandhen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Love-ukku Yendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love-ukku Yendi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanney Tuition
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanney Tuition"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sollaamal Theriyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollaamal Theriyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal Music
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Music"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mukkodu Orasi Ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mukkodu Orasi Ennai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muthaadum Bodhu Nenjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthaadum Bodhu Nenjam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pakistan-in Border Poley Tension
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakistan-in Border Poley Tension"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Choo-ro-rey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choo-ro-rey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaiyodu Naan Konda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyodu Naan Konda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaayathil Marunthu Ittaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaayathil Marunthu Ittaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannukku Thondraatha Idangalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannukku Thondraatha Idangalil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaayam Seithaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaayam Seithaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Seitha Kaayangal Aarida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Seitha Kaayangal Aarida"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Seivaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Seivaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbey Naan Mutham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbey Naan Mutham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennum Marunthu Vaippen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennum Marunthu Vaippen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muthathil Kulithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthathil Kulithu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Kondu Thuvattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Kondu Thuvattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andraada Suttrum Kodiyey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andraada Suttrum Kodiyey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konjam Neram Vilaga Koodaathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Neram Vilaga Koodaathaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vilaga Koodaathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilaga Koodaathaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aanukkul Aaru Liter Ratham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanukkul Aaru Liter Ratham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaru Liter Ratham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaru Liter Ratham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pennukkul Ainthara Liter Ratham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pennukkul Ainthara Liter Ratham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ainthara Liter Ratham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ainthara Liter Ratham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rathangal Kurainthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rathangal Kurainthaalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unarchi Mattum Kuraivathu Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unarchi Mattum Kuraivathu Illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thalli Nindraal Innum Enna Artham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalli Nindraal Innum Enna Artham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">What’s The Matter Yaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="What’s The Matter Yaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eppodhu Theerum Unthan Kottam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppodhu Theerum Unthan Kottam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kotta Kottam Kotta Kottam Kottam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kotta Kottam Kotta Kottam Kottam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Karppai Sooraiyaadum Thittam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Karppai Sooraiyaadum Thittam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thittu Thittam Thittu Thittam Thittam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thittu Thittam Thittu Thittam Thittam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennullam Sollumbodhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennullam Sollumbodhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Latchiyangal Vellumbodhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Latchiyangal Vellumbodhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal Vandhu Enthan Kathavai Thattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Vandhu Enthan Kathavai Thattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">So Sad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="So Sad"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbey Unn Mugathukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbey Unn Mugathukkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mugam Vaikkum Sugam Pidikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mugam Vaikkum Sugam Pidikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanney Unn Nagathukkul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanney Unn Nagathukkul"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sikki Konda Thugal Pidikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sikki Konda Thugal Pidikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aanaalum Kadaimaigal Kanavukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanaalum Kadaimaigal Kanavukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thadai Vidhikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadai Vidhikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engey Selvaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engey Selvaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Mutha Satham Thalai Odaikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Mutha Satham Thalai Odaikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ullathai Killaathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullathai Killaathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Killi Vittu Sellaathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Killi Vittu Sellaathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaayathil Mutham Veiyappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaayathil Mutham Veiyappa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oi Rettai Chattai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oi Rettai Chattai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rettai Dhegam Anaiya Koodaathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rettai Dhegam Anaiya Koodaathaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Innum Sila Naal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Innum Sila Naal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karppoda Inga Irukka Koodaathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karppoda Inga Irukka Koodaathaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Mutham Nooru Thanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Mutham Nooru Thanthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kattil Varumai Theerkka Koodaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattil Varumai Theerkka Koodaatha"/>
</div>
</pre>
