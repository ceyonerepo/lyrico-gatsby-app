---
title: "ganesha song lyrics"
album: "Ashwamedham"
artist: "Charan Arjun"
lyricist: "Charan Arjun"
director: "Nitin Gawde"
path: "/albums/ashwamedham-lyrics"
song: "Ganesha"
image: ../../images/albumart/ashwamedham.jpg
date: 2019-12-06
lang: telugu
youtubeLink: "https://www.youtube.com/embed/3ceDgdco__Y"
type: "happy"
singers:
  - Vijay Yesudas
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">jai kanesha jai kanesha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jai kanesha jai kanesha"/>
</div>
<div class="lyrico-lyrics-wrapper">jai kanesha paahimaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jai kanesha paahimaam"/>
</div>
<div class="lyrico-lyrics-wrapper">jai kanesha jai kanesha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jai kanesha jai kanesha"/>
</div>
<div class="lyrico-lyrics-wrapper">jai kanesha rakshamaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jai kanesha rakshamaam"/>
</div>
<div class="lyrico-lyrics-wrapper">mudha karaatha modhakam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudha karaatha modhakam"/>
</div>
<div class="lyrico-lyrics-wrapper">sadhaa vimukti saadhakam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sadhaa vimukti saadhakam"/>
</div>
<div class="lyrico-lyrics-wrapper">kalaadharaa vathamsakam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalaadharaa vathamsakam"/>
</div>
<div class="lyrico-lyrics-wrapper">vilaasiloka rakshakam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vilaasiloka rakshakam "/>
</div>
<div class="lyrico-lyrics-wrapper">anaayakaika naayakam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anaayakaika naayakam"/>
</div>
<div class="lyrico-lyrics-wrapper">vinaashithebha dhaithyakam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vinaashithebha dhaithyakam"/>
</div>
<div class="lyrico-lyrics-wrapper">nathaashubhaashu naashakam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nathaashubhaashu naashakam"/>
</div>
<div class="lyrico-lyrics-wrapper">namaamitham vinaayakam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="namaamitham vinaayakam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">gana gana gajanana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gana gana gajanana"/>
</div>
<div class="lyrico-lyrics-wrapper">jai hoo jaha pana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jai hoo jaha pana"/>
</div>
<div class="lyrico-lyrics-wrapper">hara hara deva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hara hara deva"/>
</div>
<div class="lyrico-lyrics-wrapper">maha deva maha deva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maha deva maha deva"/>
</div>
<div class="lyrico-lyrics-wrapper">gana gana gajanana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gana gana gajanana"/>
</div>
<div class="lyrico-lyrics-wrapper">jai hoo janarthana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jai hoo janarthana"/>
</div>
<div class="lyrico-lyrics-wrapper">harathi gaikoni 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="harathi gaikoni "/>
</div>
<div class="lyrico-lyrics-wrapper">varam iyava 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varam iyava "/>
</div>
<div class="lyrico-lyrics-wrapper">varam iyava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varam iyava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">yelo yelo yelluvale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yelo yelo yelluvale"/>
</div>
<div class="lyrico-lyrics-wrapper">maalo yedadhi kocasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maalo yedadhi kocasi"/>
</div>
<div class="lyrico-lyrics-wrapper">nee raaka tho 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee raaka tho "/>
</div>
<div class="lyrico-lyrics-wrapper">gana gana gajanana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gana gana gajanana"/>
</div>
<div class="lyrico-lyrics-wrapper">jai hoo janarthana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jai hoo janarthana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">bholo bholo dil se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bholo bholo dil se"/>
</div>
<div class="lyrico-lyrics-wrapper">nuv bholo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuv bholo "/>
</div>
<div class="lyrico-lyrics-wrapper">maake varamistaavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maake varamistaavu"/>
</div>
<div class="lyrico-lyrics-wrapper">ee poojatho 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ee poojatho "/>
</div>
<div class="lyrico-lyrics-wrapper">gana gana gajanana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gana gana gajanana"/>
</div>
<div class="lyrico-lyrics-wrapper">jai hoo janarthana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jai hoo janarthana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ekadantha mahaasaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ekadantha mahaasaya"/>
</div>
<div class="lyrico-lyrics-wrapper">lokamanthaa needayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lokamanthaa needayaa"/>
</div>
<div class="lyrico-lyrics-wrapper">parvathi parameshuni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="parvathi parameshuni"/>
</div>
<div class="lyrico-lyrics-wrapper">varasathivam needha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varasathivam needha"/>
</div>
<div class="lyrico-lyrics-wrapper">maa dhairyam garvam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maa dhairyam garvam"/>
</div>
<div class="lyrico-lyrics-wrapper">sarvam nuvenayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sarvam nuvenayya"/>
</div>
<div class="lyrico-lyrics-wrapper">ganapathi bappaa moriyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ganapathi bappaa moriyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">ee jagathiki nuv
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ee jagathiki nuv"/>
</div>
<div class="lyrico-lyrics-wrapper">megaa staarayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="megaa staarayaa"/>
</div>
<div class="lyrico-lyrics-wrapper">aadhaa laddu kaaliyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aadhaa laddu kaaliyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">nee styluku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee styluku "/>
</div>
<div class="lyrico-lyrics-wrapper">janame thaaliyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="janame thaaliyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">gana gana gajanana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gana gana gajanana"/>
</div>
<div class="lyrico-lyrics-wrapper">jai hoo jaha pana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jai hoo jaha pana"/>
</div>
<div class="lyrico-lyrics-wrapper">gana gana gajanana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gana gana gajanana"/>
</div>
<div class="lyrico-lyrics-wrapper">jai hoo janarthana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jai hoo janarthana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">yelo yelo yelluvale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yelo yelo yelluvale"/>
</div>
<div class="lyrico-lyrics-wrapper">maalo yedadhi kocasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maalo yedadhi kocasi"/>
</div>
<div class="lyrico-lyrics-wrapper">nee raaka tho 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee raaka tho "/>
</div>
<div class="lyrico-lyrics-wrapper">bholo bholo dil se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bholo bholo dil se"/>
</div>
<div class="lyrico-lyrics-wrapper">nuv bholo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuv bholo "/>
</div>
<div class="lyrico-lyrics-wrapper">maake varamistaavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maake varamistaavu"/>
</div>
<div class="lyrico-lyrics-wrapper">ee poojatho 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ee poojatho "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">lakumuki lambhodaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lakumuki lambhodaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">elukanu ekkesiraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="elukanu ekkesiraa"/>
</div>
<div class="lyrico-lyrics-wrapper">chaka chaka chikkulanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chaka chaka chikkulanni"/>
</div>
<div class="lyrico-lyrics-wrapper">theercheyaraa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theercheyaraa "/>
</div>
<div class="lyrico-lyrics-wrapper">vinaraa vishwambharaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vinaraa vishwambharaa"/>
</div>
<div class="lyrico-lyrics-wrapper">nuv maa villanbhuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuv maa villanbhuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">vijayam dhanamanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vijayam dhanamanu"/>
</div>
<div class="lyrico-lyrics-wrapper">bhaanalaodalaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bhaanalaodalaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">nuv thaluchiukunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuv thaluchiukunte"/>
</div>
<div class="lyrico-lyrics-wrapper">chitikelona sarve janam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chitikelona sarve janam"/>
</div>
<div class="lyrico-lyrics-wrapper">aanantha kielilona 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aanantha kielilona "/>
</div>
<div class="lyrico-lyrics-wrapper">theluthundiraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theluthundiraa"/>
</div>
<div class="lyrico-lyrics-wrapper">vadapappu undralu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vadapappu undralu "/>
</div>
<div class="lyrico-lyrics-wrapper">thinna viswaasamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thinna viswaasamu"/>
</div>
<div class="lyrico-lyrics-wrapper">kaasthaina maapaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaasthaina maapaina"/>
</div>
<div class="lyrico-lyrics-wrapper">thigiri suparaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thigiri suparaa"/>
</div>
<div class="lyrico-lyrics-wrapper">ooranthataa vaadanthataa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooranthataa vaadanthataa"/>
</div>
<div class="lyrico-lyrics-wrapper">neekosame mataraataa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neekosame mataraataa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ganapathi bappaa moriyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ganapathi bappaa moriyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">ee jagathiki nuv
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ee jagathiki nuv"/>
</div>
<div class="lyrico-lyrics-wrapper">megaa staarayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="megaa staarayaa"/>
</div>
<div class="lyrico-lyrics-wrapper">aadhaa laddu kaaliyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aadhaa laddu kaaliyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">nee styluku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee styluku "/>
</div>
<div class="lyrico-lyrics-wrapper">janame thaaliyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="janame thaaliyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">jaya jaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jaya jaya"/>
</div>
<div class="lyrico-lyrics-wrapper">dwani gheenkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dwani gheenkara"/>
</div>
<div class="lyrico-lyrics-wrapper">jaya ho yuva shankara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jaya ho yuva shankara"/>
</div>
<div class="lyrico-lyrics-wrapper">jagathini shudhi chesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jagathini shudhi chesi"/>
</div>
<div class="lyrico-lyrics-wrapper">buddhi penchara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="buddhi penchara"/>
</div>
<div class="lyrico-lyrics-wrapper">bhajana ku pulakinchara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bhajana ku pulakinchara"/>
</div>
<div class="lyrico-lyrics-wrapper">bangaru sirulunchara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bangaru sirulunchara"/>
</div>
<div class="lyrico-lyrics-wrapper">chadhuvula thandri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chadhuvula thandri"/>
</div>
<div class="lyrico-lyrics-wrapper">mammu chalagunchara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mammu chalagunchara"/>
</div>
<div class="lyrico-lyrics-wrapper">airavathiswaroopadhari 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="airavathiswaroopadhari "/>
</div>
<div class="lyrico-lyrics-wrapper">ghajakesari nee namajapamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ghajakesari nee namajapamu"/>
</div>
<div class="lyrico-lyrics-wrapper">maaku nithya oopiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maaku nithya oopiri"/>
</div>
<div class="lyrico-lyrics-wrapper">ee dharini paina 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ee dharini paina "/>
</div>
<div class="lyrico-lyrics-wrapper">maanavari sukha shanthiku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maanavari sukha shanthiku"/>
</div>
<div class="lyrico-lyrics-wrapper">jeevinchaga nuvey gaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jeevinchaga nuvey gaa"/>
</div>
<div class="lyrico-lyrics-wrapper">peddha kapari 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="peddha kapari "/>
</div>
<div class="lyrico-lyrics-wrapper">harivaaradhi maa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="harivaaradhi maa "/>
</div>
<div class="lyrico-lyrics-wrapper">ratha sarathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ratha sarathi"/>
</div>
<div class="lyrico-lyrics-wrapper">needhey kadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="needhey kadha"/>
</div>
<div class="lyrico-lyrics-wrapper">maa brathukuna prathidhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maa brathukuna prathidhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ganapathi bappaa moriyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ganapathi bappaa moriyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">ee jagathiki nuv
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ee jagathiki nuv"/>
</div>
<div class="lyrico-lyrics-wrapper">megaa staarayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="megaa staarayaa"/>
</div>
<div class="lyrico-lyrics-wrapper">aadhaa laddu kaaliyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aadhaa laddu kaaliyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">nee styluku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee styluku "/>
</div>
<div class="lyrico-lyrics-wrapper">janame thaaliyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="janame thaaliyaa"/>
</div>
</pre>
