---
title: "kurumba reprise song lyrics"
album: "Tik Tik Tik"
artist: "D. Imman"
lyricist: "Madhan Karky"
director: "Shakti Soundar Rajan"
path: "/albums/tik-tik-tik-lyrics"
song: "Kurumba Reprise"
image: ../../images/albumart/tik-tik-tik.jpg
date: 2018-06-22
lang: tamil
youtubeLink: "https://www.youtube.com/embed/eq0Mh0TR3RY"
type: "happy"
singers:
  - Miruthula Siva
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kurumba aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurumba aaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kurumba aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurumba aaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kurumba aaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurumba aaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kurumba aaaa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurumba aaaa aaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Valigal magizhnthen unnaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valigal magizhnthen unnaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Palanaal siriththen unnaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palanaal siriththen unnaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Sila naal azhuthen unnaalaeae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sila naal azhuthen unnaalaeae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enai naan unarndhen unnaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai naan unarndhen unnaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Edaiyum kuraindhen unnaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edaiyum kuraindhen unnaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Varangal niraindhen unnaalae ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varangal niraindhen unnaalae ae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kovam vandhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kovam vandhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai adippaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai adippaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan irugiduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan irugiduven"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattikondu muththam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattikondu muththam"/>
</div>
<div class="lyrico-lyrics-wrapper">Koduppaai naan urugiduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koduppaai naan urugiduven"/>
</div>
<div class="lyrico-lyrics-wrapper">Urugiduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urugiduven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kurumba aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurumba aa"/>
</div>
<div class="lyrico-lyrics-wrapper">En ulagam neethaandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En ulagam neethaandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kurumba aaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurumba aaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">En vidiyal neethaandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En vidiyal neethaandaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kurumba aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurumba aa"/>
</div>
<div class="lyrico-lyrics-wrapper">En ulagam neethaandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En ulagam neethaandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kurumba aaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurumba aaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">En vidiyal neethaandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En vidiyal neethaandaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pallikkul ne pogumpotho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pallikkul ne pogumpotho"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetridam aagum nenjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetridam aagum nenjam"/>
</div>
<div class="lyrico-lyrics-wrapper">Un palli vidumurai endraal anjum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un palli vidumurai endraal anjum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saapida koopidathaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saapida koopidathaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Eththanai kodi lanjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eththanai kodi lanjam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanaalum thattil paadhi enjum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanaalum thattil paadhi enjum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endhan aadai ellaamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan aadai ellaamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kasangi kizhiyum unnaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kasangi kizhiyum unnaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhan kannam rendodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan kannam rendodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Muththa thazhumbum unnaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muththa thazhumbum unnaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Undhan kurumbu marabanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan kurumbu marabanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Evvazhi kondaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evvazhi kondaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku theriyaathaaa.aaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku theriyaathaaa.aaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kurumba aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurumba aa"/>
</div>
<div class="lyrico-lyrics-wrapper">En ulagam neethaandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En ulagam neethaandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kurumba aaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurumba aaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">En vidiyal neethaandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En vidiyal neethaandaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kurumba aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurumba aa"/>
</div>
<div class="lyrico-lyrics-wrapper">En ulagam neethaandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En ulagam neethaandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kurumba aaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurumba aaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">En vidiyal neethaandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En vidiyal neethaandaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuliyal araiyil oru por
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuliyal araiyil oru por"/>
</div>
<div class="lyrico-lyrics-wrapper">Cartoon niruththa oru por
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cartoon niruththa oru por"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinamum porgal nammullae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinamum porgal nammullae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Panikozh pagira oru por
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panikozh pagira oru por"/>
</div>
<div class="lyrico-lyrics-wrapper">Iravil uranga oru por
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravil uranga oru por"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagaai porgal nammullae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaai porgal nammullae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vinnaivittu ennil vandhaayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinnaivittu ennil vandhaayae"/>
</div>
<div class="lyrico-lyrics-wrapper">En ulam kulira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En ulam kulira"/>
</div>
<div class="lyrico-lyrics-wrapper">Annai endru pattam thanthaayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annai endru pattam thanthaayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan thalai nimira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan thalai nimira"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalai nimiraaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalai nimiraaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kurumba aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurumba aa"/>
</div>
<div class="lyrico-lyrics-wrapper">En ulagam neethaandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En ulagam neethaandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kurumba aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurumba aa"/>
</div>
<div class="lyrico-lyrics-wrapper">En vidiyal neethaandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En vidiyal neethaandaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kurumbaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurumbaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">En udhiram neethaandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En udhiram neethaandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kurumbaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurumbaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">En uyirae neethaandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En uyirae neethaandaa"/>
</div>
</pre>
