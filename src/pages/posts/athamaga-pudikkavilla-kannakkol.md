---
title: "athamaga pudikkavilla song lyrics"
album: "Kannakkol"
artist: "Bobby"
lyricist: "Muthuvijayan"
director: "V.A.Kumaresan"
path: "/albums/kannakkol-lyrics"
song: "Athamaga Pudikkavilla"
image: ../../images/albumart/kannakkol.jpg
date: 2018-06-15
lang: tamil
youtubeLink: "https://www.youtube.com/embed/kptJyW-A1P8"
type: "happy"
singers:
  - Velmurugan
  - Cheinnaponnu
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">atha mava pudikavilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="atha mava pudikavilla"/>
</div>
<div class="lyrico-lyrics-wrapper">maama mava pudikavilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maama mava pudikavilla"/>
</div>
<div class="lyrico-lyrics-wrapper">yaaru petha pillayo nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaaru petha pillayo nee"/>
</div>
<div class="lyrico-lyrics-wrapper">unna romba puduchu iruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna romba puduchu iruku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">malliga poo pudikavilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malliga poo pudikavilla"/>
</div>
<div class="lyrico-lyrics-wrapper">marigolunthum pudikavilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="marigolunthum pudikavilla"/>
</div>
<div class="lyrico-lyrics-wrapper">setha neram munne paatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="setha neram munne paatha"/>
</div>
<div class="lyrico-lyrics-wrapper">machan unna puduchu iruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="machan unna puduchu iruku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaaka vacha sithina 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaaka vacha sithina "/>
</div>
<div class="lyrico-lyrics-wrapper">vaasal sithiram pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaasal sithiram pola"/>
</div>
<div class="lyrico-lyrics-wrapper">ketka vacha sithirai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ketka vacha sithirai "/>
</div>
<div class="lyrico-lyrics-wrapper">maasathu kathiri pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maasathu kathiri pola"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">raasathi raasathi vachale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="raasathi raasathi vachale"/>
</div>
<div class="lyrico-lyrics-wrapper">aasa theeya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aasa theeya"/>
</div>
<div class="lyrico-lyrics-wrapper">rosa poo rosa poo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rosa poo rosa poo "/>
</div>
<div class="lyrico-lyrics-wrapper">poothale vaitheeya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poothale vaitheeya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">atha mava pudikavilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="atha mava pudikavilla"/>
</div>
<div class="lyrico-lyrics-wrapper">maama mava pudikavilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maama mava pudikavilla"/>
</div>
<div class="lyrico-lyrics-wrapper">yaaru petha pillayo nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaaru petha pillayo nee"/>
</div>
<div class="lyrico-lyrics-wrapper">unna romba puduchu iruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna romba puduchu iruku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">atha mava pudikavilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="atha mava pudikavilla"/>
</div>
<div class="lyrico-lyrics-wrapper">maama mava pudikavilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maama mava pudikavilla"/>
</div>
<div class="lyrico-lyrics-wrapper">yaaru petha pillayo nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaaru petha pillayo nee"/>
</div>
<div class="lyrico-lyrics-wrapper">unna romba puduchu iruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna romba puduchu iruku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">paada paduthuraya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paada paduthuraya"/>
</div>
<div class="lyrico-lyrics-wrapper">paathala pairaviye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paathala pairaviye"/>
</div>
<div class="lyrico-lyrics-wrapper">ooda elachu puten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooda elachu puten"/>
</div>
<div class="lyrico-lyrics-wrapper">unnala than vaa rathiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnala than vaa rathiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aana ulaga seemaiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aana ulaga seemaiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">unnattam aambalaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnattam aambalaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">maagaali sathiyamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maagaali sathiyamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">pathathu illa ithu varaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathathu illa ithu varaiyil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kungumatha kotti 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kungumatha kotti "/>
</div>
<div class="lyrico-lyrics-wrapper">senjan peralagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="senjan peralagu"/>
</div>
<div class="lyrico-lyrics-wrapper">machan paaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="machan paaka"/>
</div>
<div class="lyrico-lyrics-wrapper">pothi vacha nooralagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pothi vacha nooralagu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">adi nee thandi nee thandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi nee thandi nee thandi"/>
</div>
<div class="lyrico-lyrics-wrapper">ennoda sothu sogam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennoda sothu sogam"/>
</div>
<div class="lyrico-lyrics-wrapper">oor thandi ponalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oor thandi ponalum"/>
</div>
<div class="lyrico-lyrics-wrapper">en ullam unna suthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en ullam unna suthum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">atha mava pudikavilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="atha mava pudikavilla"/>
</div>
<div class="lyrico-lyrics-wrapper">maama mava pudikavilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maama mava pudikavilla"/>
</div>
<div class="lyrico-lyrics-wrapper">yaaru petha pillayo nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaaru petha pillayo nee"/>
</div>
<div class="lyrico-lyrics-wrapper">unna romba puduchu iruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna romba puduchu iruku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">atha mava pudikavilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="atha mava pudikavilla"/>
</div>
<div class="lyrico-lyrics-wrapper">maama mava pudikavilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maama mava pudikavilla"/>
</div>
<div class="lyrico-lyrics-wrapper">yaaru petha pillayo nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaaru petha pillayo nee"/>
</div>
<div class="lyrico-lyrics-wrapper">unna romba puduchu iruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna romba puduchu iruku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">porale porale potta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="porale porale potta"/>
</div>
<div class="lyrico-lyrics-wrapper">pulla porale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pulla porale"/>
</div>
<div class="lyrico-lyrics-wrapper">seevale en vayasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seevale en vayasa"/>
</div>
<div class="lyrico-lyrics-wrapper">iluthukittu porale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iluthukittu porale"/>
</div>
<div class="lyrico-lyrics-wrapper">kanaale aamabalya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanaale aamabalya"/>
</div>
<div class="lyrico-lyrics-wrapper">kavvikitu porale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kavvikitu porale"/>
</div>
<div class="lyrico-lyrics-wrapper">kaanatha kaatchi ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaanatha kaatchi ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">kaati puttu porale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaati puttu porale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vaalpam than paambaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalpam than paambaga"/>
</div>
<div class="lyrico-lyrics-wrapper">padam eduthu aaduthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="padam eduthu aaduthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">valluvaru sonna antha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valluvaru sonna antha"/>
</div>
<div class="lyrico-lyrics-wrapper">inbathu paal kondu vaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inbathu paal kondu vaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">pathikichu adi vayiru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathikichu adi vayiru"/>
</div>
<div class="lyrico-lyrics-wrapper">patiniyil eriyuthaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="patiniyil eriyuthaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">aaru kulam aeri thanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaru kulam aeri thanni"/>
</div>
<div class="lyrico-lyrics-wrapper">oothi pathu aathum ayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oothi pathu aathum ayya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">usippi vitta rathiriku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="usippi vitta rathiriku"/>
</div>
<div class="lyrico-lyrics-wrapper">muluchuva di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muluchuva di"/>
</div>
<div class="lyrico-lyrics-wrapper">enna aagumo vetiya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna aagumo vetiya "/>
</div>
<div class="lyrico-lyrics-wrapper">than maduchugada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="than maduchugada"/>
</div>
<div class="lyrico-lyrics-wrapper">yemmamma yemmamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yemmamma yemmamma"/>
</div>
<div class="lyrico-lyrics-wrapper">aerala konda thantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aerala konda thantha"/>
</div>
<div class="lyrico-lyrics-wrapper">ayyayo ayyayo unnale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ayyayo ayyayo unnale"/>
</div>
<div class="lyrico-lyrics-wrapper">thindantam than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thindantam than"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">atha mava pudikavilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="atha mava pudikavilla"/>
</div>
<div class="lyrico-lyrics-wrapper">maama mava pudikavilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maama mava pudikavilla"/>
</div>
<div class="lyrico-lyrics-wrapper">yaaru petha pillayo nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaaru petha pillayo nee"/>
</div>
<div class="lyrico-lyrics-wrapper">unna romba puduchu iruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna romba puduchu iruku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">malliga poo pudikavilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malliga poo pudikavilla"/>
</div>
<div class="lyrico-lyrics-wrapper">marigolunthum pudikavilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="marigolunthum pudikavilla"/>
</div>
<div class="lyrico-lyrics-wrapper">setha neram munne paatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="setha neram munne paatha"/>
</div>
<div class="lyrico-lyrics-wrapper">machan unna puduchu iruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="machan unna puduchu iruku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaaka vacha sithina 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaaka vacha sithina "/>
</div>
<div class="lyrico-lyrics-wrapper">vaasal sithiram pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaasal sithiram pola"/>
</div>
<div class="lyrico-lyrics-wrapper">ketka vacha sithirai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ketka vacha sithirai "/>
</div>
<div class="lyrico-lyrics-wrapper">maasathu kathiri pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maasathu kathiri pola"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">raasathi raasathi vachale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="raasathi raasathi vachale"/>
</div>
<div class="lyrico-lyrics-wrapper">aasa theeya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aasa theeya"/>
</div>
<div class="lyrico-lyrics-wrapper">rosa poo rosa poo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rosa poo rosa poo "/>
</div>
<div class="lyrico-lyrics-wrapper">poothale vaitheeya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poothale vaitheeya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">atha mava pudikavilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="atha mava pudikavilla"/>
</div>
<div class="lyrico-lyrics-wrapper">maama mava pudikavilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maama mava pudikavilla"/>
</div>
<div class="lyrico-lyrics-wrapper">yaaru petha pillayo nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaaru petha pillayo nee"/>
</div>
<div class="lyrico-lyrics-wrapper">unna romba puduchu iruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna romba puduchu iruku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">atha mava pudikavilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="atha mava pudikavilla"/>
</div>
<div class="lyrico-lyrics-wrapper">maama mava pudikavilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maama mava pudikavilla"/>
</div>
<div class="lyrico-lyrics-wrapper">yaaru petha pillayo nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaaru petha pillayo nee"/>
</div>
<div class="lyrico-lyrics-wrapper">unna romba puduchu iruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna romba puduchu iruku"/>
</div>
</pre>
