---
title: "pudhusaatam song lyrics"
album: "Thumbaa"
artist: "Anirudh Ravichander"
lyricist: "Madhan Karky"
director: "Harish Ram L.H."
path: "/albums/thumbaa-lyrics"
song: "Pudhusaatam"
image: ../../images/albumart/thumbaa.jpg
date: 2019-06-21
lang: tamil
youtubeLink: "https://www.youtube.com/embed/h9DKilyRDsA"
type: "love"
singers:
  - Anirudh Ravichander
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Pudhusaattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhusaattam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanu Mannu Naanu Ellaamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanu Mannu Naanu Ellaamey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pudhusaattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhusaattam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naakku Mookku Pookka Ellaamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naakku Mookku Pookka Ellaamey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaattaayi Enna Senja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattaayi Enna Senja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaattukkullaara Vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattukkullaara Vandhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pudhusaattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhusaattam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaththu Kaathu Keethu Ellaamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaththu Kaathu Keethu Ellaamey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pudhusaattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhusaattam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oda Kooda Paada Ellaamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oda Kooda Paada Ellaamey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaattaayi Enna Senja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattaayi Enna Senja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaattukkullaara Vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattukkullaara Vandhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poochiga Pechuga Keechuga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poochiga Pechuga Keechuga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kekkurandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kekkurandi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaadaga Rekkaiga Vaangida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadaga Rekkaiga Vaangida"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paakkurandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paakkurandi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pattana Paavatha Sattena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattana Paavatha Sattena"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pokkurandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pokkurandi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kai Thara Nee Vara Un Vira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai Thara Nee Vara Un Vira"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kokkurandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kokkurandi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaattukullaara Vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattukullaara Vandhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Puli Thedum Maanu Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puli Thedum Maanu Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sol Pechu Kelu Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sol Pechu Kelu Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bayamillaa Aalu Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bayamillaa Aalu Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakkonjam Paaru Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkonjam Paaru Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veliveshakkaarandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veliveshakkaarandi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Appapa Veerandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appapa Veerandi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Una Nambi Vaarandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Una Nambi Vaarandi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakkenna Guarantee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkenna Guarantee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaattaayi Enna Senja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattaayi Enna Senja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pudhusaattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhusaattam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanu Mannu Naanu Ellaamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanu Mannu Naanu Ellaamey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pudhusattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhusattam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naakku Mookku Pookka Ellaamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naakku Mookku Pookka Ellaamey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaattaayi Enna Senja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattaayi Enna Senja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaattukullaara Vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattukullaara Vandhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sila Naal Munnaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sila Naal Munnaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kurangaathan Thirinjomadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurangaathan Thirinjomadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maram Thavithaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maram Thavithaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pazham Thaedi Alanjomadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pazham Thaedi Alanjomadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arivillaama Sugamathan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arivillaama Sugamathan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kedanthomadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kedanthomadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Panam Thedi Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panam Thedi Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sirikkathaan Marandhomadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirikkathaan Marandhomadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ulagathil Yethum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagathil Yethum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Um Pola Azhagilla Sonnen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Um Pola Azhagilla Sonnen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha Kaattukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Kaattukulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adha Konjam Naan Maathikitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adha Konjam Naan Maathikitten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manasanga Nenju Ovvonnum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasanga Nenju Ovvonnum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thorakkadha Kaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thorakkadha Kaadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thoranthu Thaan Paatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoranthu Thaan Paatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ethuvum Ethuvum Azhagey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethuvum Ethuvum Azhagey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poochiga Pechuga Keechuga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poochiga Pechuga Keechuga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kekkurandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kekkurandi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaadaga Rekkaiga Vaangida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadaga Rekkaiga Vaangida"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paakkurandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paakkurandi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pattana Paavatha Sattena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattana Paavatha Sattena"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pokkurandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pokkurandi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kai Thara Nee Vara Un Vira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai Thara Nee Vara Un Vira"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kokkurandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kokkurandi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaattukullaara Inga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattukullaara Inga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vazhiyellam Minmini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhiyellam Minmini"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhiyellam Minmini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhiyellam Minmini"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maramellam Minmini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maramellam Minmini"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manasellam Minmini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasellam Minmini"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ulagellaam Minmini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagellaam Minmini"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Usurellaam Minmini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usurellaam Minmini"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Micha Vaazhkaikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Micha Vaazhkaikku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu Pothum Kanmani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Pothum Kanmani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaattaayi Enna Senja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattaayi Enna Senja"/>
</div>
</pre>
