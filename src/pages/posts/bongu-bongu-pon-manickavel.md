---
title: "bongu bongu song lyrics"
album: "Pon Manickavel"
artist: "D. Imman"
lyricist: "Kabilan"
director: "A.C. Mugil Chellappan"
path: "/albums/pon-manickavel-lyrics"
song: "Bongu Bongu"
image: ../../images/albumart/pon-manickavel.jpg
date: 2021-11-19
lang: tamil
youtubeLink: "https://www.youtube.com/embed/mwgJ2FITXjQ"
type: "happy"
singers:
  - Thiagaraja Subramaniam
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Bongu Bongu Bongammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bongu Bongu Bongammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaam Bongu Bongu Bongammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaam Bongu Bongu Bongammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Bangu Bangu Bangammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bangu Bangu Bangammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">En Songu Songu Songaamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Songu Songu Songaamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pommanatti Aampanatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pommanatti Aampanatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhai Kelu Oruvaatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhai Kelu Oruvaatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Puyal Vanthu Adichaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puyal Vanthu Adichaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Avuraathu En Veetti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avuraathu En Veetti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bongu Bongu Bongammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bongu Bongu Bongammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaam Bongu Bongu Bongammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaam Bongu Bongu Bongammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Bangu Bangu Bangammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bangu Bangu Bangammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">En Songu Songu Songaamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Songu Songu Songaamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Aatchi Manasaatchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Aatchi Manasaatchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Thaanae Mudhalsaatchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Thaanae Mudhalsaatchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Poluthellaam Poranthaachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poluthellaam Poranthaachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini Poovodu En Pechi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Poovodu En Pechi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bongu Bongu Bongammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bongu Bongu Bongammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaam Bongu Bongu Bongammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaam Bongu Bongu Bongammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Bangu Bangu Bangammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bangu Bangu Bangammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">En Songu Songu Songaamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Songu Songu Songaamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uzhaippalae Vandha Vervaikkumunnae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uzhaippalae Vandha Vervaikkumunnae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nayagara Onnum Illadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nayagara Onnum Illadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirippaalae Varum Osaikkumunnae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirippaalae Varum Osaikkumunnae"/>
</div>
<div class="lyrico-lyrics-wrapper">Koyil Mani Engae Solladaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koyil Mani Engae Solladaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bongu Bongu Bongammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bongu Bongu Bongammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaam Bongu Bongu Bongammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaam Bongu Bongu Bongammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Bangu Bangu Bangammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bangu Bangu Bangammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">En Songu Songu Songaamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Songu Songu Songaamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Biggu Biggu Biggu Bangu Bangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Biggu Biggu Biggu Bangu Bangu"/>
</div>
<div class="lyrico-lyrics-wrapper">Biggu Biggu Biggu Bangu Bangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Biggu Biggu Biggu Bangu Bangu"/>
</div>
<div class="lyrico-lyrics-wrapper">Bangaa Bangaa Thum Thum Thum Thum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bangaa Bangaa Thum Thum Thum Thum"/>
</div>
<div class="lyrico-lyrics-wrapper">Biggu Biggu Biggu Beengu Bongu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Biggu Biggu Biggu Beengu Bongu"/>
</div>
<div class="lyrico-lyrics-wrapper">Biggu Biggu Biggu Beengu Bongu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Biggu Biggu Biggu Beengu Bongu"/>
</div>
<div class="lyrico-lyrics-wrapper">Bongaamaa Bongaamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bongaamaa Bongaamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sariyaa Sollappona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sariyaa Sollappona"/>
</div>
<div class="lyrico-lyrics-wrapper">Nagara Vaazhkai Onnum Venaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nagara Vaazhkai Onnum Venaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Olachaalumae Oru Olaveedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Olachaalumae Oru Olaveedu"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendu Kaalamaadu Ingae Podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendu Kaalamaadu Ingae Podhum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yei Nagaram Nagaram Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei Nagaram Nagaram Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thagaram Thagaram Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thagaram Thagaram Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Edaikku Nee Thookki Podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edaikku Nee Thookki Podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yei Gramam Gramam Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei Gramam Gramam Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkum Enakkum Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkum Enakkum Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Santhosa Ellaakkodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Santhosa Ellaakkodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pommanatti Aampanatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pommanatti Aampanatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhai Sonnaa Oruvaatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhai Sonnaa Oruvaatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Vivasaayi Oruvan Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vivasaayi Oruvan Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaithukkae Vazhikaatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaithukkae Vazhikaatti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bongu Bongu Bongammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bongu Bongu Bongammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaam Bongu Bongu Bongammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaam Bongu Bongu Bongammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Bangu Bangu Bangammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bangu Bangu Bangammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">En Songu Songu Songaamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Songu Songu Songaamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bongu Bongu Bongammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bongu Bongu Bongammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaam Bongu Bongu Bongammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaam Bongu Bongu Bongammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Bangu Bangu Bangammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bangu Bangu Bangammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">En Songu Songu Songaamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Songu Songu Songaamaa"/>
</div>
</pre>
