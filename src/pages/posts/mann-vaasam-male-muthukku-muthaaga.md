---
title: "mann vaasam male song lyrics"
album: "Muthukku Muthaaga"
artist: "Kavi Periyathambi"
lyricist: "Na. Muthukumar - Nandalala"
director: "Rasu Madhuravan"
path: "/albums/muthukku-muthaaga-lyrics"
song: "Mann Vaasam - Male"
image: ../../images/albumart/muthukku-muthaaga.jpg
date: 2011-03-18
lang: tamil
youtubeLink: "https://www.youtube.com/embed/GvPJ-f9epYw"
type: "affection"
singers:
  - Madhu Balakrishnan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Man vaasam veesum enga ooru nalla ooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Man vaasam veesum enga ooru nalla ooru"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorukkulla appaavukku peru naalla peru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorukkulla appaavukku peru naalla peru"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga aaththaa oru koayil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga aaththaa oru koayil"/>
</div>
<div class="lyrico-lyrics-wrapper">Engappaa adhil saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engappaa adhil saami"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhuthaaney engaloada boomi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhuthaaney engaloada boomi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Man vaasam veesum enga ooru nalla ooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Man vaasam veesum enga ooru nalla ooru"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorukkulla appaavukku peru naalla peru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorukkulla appaavukku peru naalla peru"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga aaththaa oru koayil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga aaththaa oru koayil"/>
</div>
<div class="lyrico-lyrics-wrapper">Engappaa adhil saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engappaa adhil saami"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhuthaaney engaloada boomi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhuthaaney engaloada boomi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anjupperu annan thambi vaazhum indha veedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anjupperu annan thambi vaazhum indha veedu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paasathukku panjamilla naalum sollippaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paasathukku panjamilla naalum sollippaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Verum kaalil neenga nadandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verum kaalil neenga nadandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga usira thaangi sumandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga usira thaangi sumandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Verum kaalil neenga nadandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verum kaalil neenga nadandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga usira thaangi sumandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga usira thaangi sumandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhum indha kaalam porkkaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhum indha kaalam porkkaalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Man vaasam veesum enga ooru nalla ooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Man vaasam veesum enga ooru nalla ooru"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorukkulla appaavukku peru naalla peru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorukkulla appaavukku peru naalla peru"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga aaththaa oru koayil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga aaththaa oru koayil"/>
</div>
<div class="lyrico-lyrics-wrapper">Engappaa adhil saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engappaa adhil saami"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhuthaaney engaloada boomi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhuthaaney engaloada boomi"/>
</div>
</pre>
