---
title: "kannai padithen song lyrics"
album: "Ponnar Shankar"
artist: "Ilaiyaraaja"
lyricist: "Vairamuthu"
director: "Thiagarajan"
path: "/albums/ponnar-shankar-lyrics"
song: "Kannai Padithen"
image: ../../images/albumart/ponnar-shankar.jpg
date: 2011-04-09
lang: tamil
youtubeLink: "https://www.youtube.com/embed/E-kcBSyxHDU"
type: "love"
singers:
  - Shreya Ghoshal
  - Sriram Parthasarathy
  - Emmerson Mnangagwa
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kannai padithaen vinnai padithaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannai padithaen vinnai padithaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai padikka innum mudiyaliyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai padikka innum mudiyaliyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Innum mudiyaliyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum mudiyaliyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannai padithaen vinnai padithaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannai padithaen vinnai padithaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai padikka innum mudiyaliyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai padikka innum mudiyaliyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Innum mudiyaliyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum mudiyaliyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Suttri thrindhaen thitthitthirundhaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suttri thrindhaen thitthitthirundhaen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Suttri thrindhaen thitthitthirundhaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suttri thrindhaen thitthitthirundhaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhan idhaya kadhavai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan idhaya kadhavai"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaen vandhu thirandhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaen vandhu thirandhaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannai padithaen vinnai padithaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannai padithaen vinnai padithaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai padikka innum mudiyaliyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai padikka innum mudiyaliyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Innum mudiyaliyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum mudiyaliyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannai thirandhaalum marandhaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannai thirandhaalum marandhaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Theriyudhu undhan uruvam uyir uruvam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theriyudhu undhan uruvam uyir uruvam"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayam adhai varaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam adhai varaiyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konji chirithaalum thavithaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konji chirithaalum thavithaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Inikkudhu indha paruvam indha paruvam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inikkudhu indha paruvam indha paruvam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alai alaiyaai aasaigalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alai alaiyaai aasaigalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Adukkadukkai anuppugiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adukkadukkai anuppugiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konjam rasithu konjam rusithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam rasithu konjam rusithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Konji anaithaen kaatril midhakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konji anaithaen kaatril midhakka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannai padithaen vinnai padithaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannai padithaen vinnai padithaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai padikka innum mudiyaliyaeInnum mudiyaliyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai padikka innum mudiyaliyaeInnum mudiyaliyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vatta valaiyodu kolusodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vatta valaiyodu kolusodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai isaithida varavaa vanna nilavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai isaithida varavaa vanna nilavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharavaa thandhu peravaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharavaa thandhu peravaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chinna mazhaiyaaga veyilaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna mazhaiyaaga veyilaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Thottu thottu mutham tharavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottu thottu mutham tharavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaalam idavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaalam idavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhi asaindhaal uyir asaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhi asaindhaal uyir asaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu isaiyai meettugiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu isaiyai meettugiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha nimidam indha sparisam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha nimidam indha sparisam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nindru nilaikkum kaalam muzhudhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nindru nilaikkum kaalam muzhudhum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannai padithaen vinnai padithaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannai padithaen vinnai padithaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai padikka innum mudiyaliyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai padikka innum mudiyaliyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Innum mudiyaliyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum mudiyaliyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannai padithaen vinnai padithaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannai padithaen vinnai padithaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai padikka innum mudiyaliyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai padikka innum mudiyaliyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Innum mudiyaliyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum mudiyaliyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Suttri thrindhaen thitthitthirundhaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suttri thrindhaen thitthitthirundhaen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Suttri thrindhaen thitthitthirundhaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suttri thrindhaen thitthitthirundhaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhan idhaya kadhavai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan idhaya kadhavai"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaen vandhu thirandhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaen vandhu thirandhaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannai padithaen vinnai padithaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannai padithaen vinnai padithaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai padikka innum mudiyaliyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai padikka innum mudiyaliyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Innum mudiyaliyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum mudiyaliyae"/>
</div>
</pre>
