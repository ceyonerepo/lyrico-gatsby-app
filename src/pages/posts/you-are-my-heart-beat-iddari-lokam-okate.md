---
title: "you are my heart beat song lyrics"
album: "Iddari Lokam Okate"
artist: "Mickey J Meyer"
lyricist: "Balaji"
director: "	GR Krishna"
path: "/albums/prati-roju-pandage-lyrics"
song: "You Are My Heart Beat"
image: ../../images/albumart/prati-roju-pandage.jpg
date: 2019-12-25
lang: telugu
youtubeLink: "https://www.youtube.com/embed/tSnT5hrDfvM"
type: "love"
singers:
  - Anurag Kulkarni
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nuvve Nuvvee Nuvvee Kada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvve Nuvvee Nuvvee Kada"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Thodu Nuvvee Kada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Thodu Nuvvee Kada"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeku Naaku Undoo Katha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeku Naaku Undoo Katha"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaperu Preme Kada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaperu Preme Kada"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnallo Aasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnallo Aasa"/>
</div>
<div class="lyrico-lyrics-wrapper">Monnallo Dhyasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Monnallo Dhyasa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gundello Moshaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundello Moshaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Neeku Telusaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Neeku Telusaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Matalne Raasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Matalne Raasa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mounanga Chusa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mounanga Chusa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalone Lolone Dhachesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalone Lolone Dhachesa"/>
</div>
<div class="lyrico-lyrics-wrapper">You Are My Heart Beat
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Are My Heart Beat"/>
</div>
<div class="lyrico-lyrics-wrapper">You Are My Heart Beat
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Are My Heart Beat"/>
</div>
<div class="lyrico-lyrics-wrapper">You Are My Heart Beat
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Are My Heart Beat"/>
</div>
<div class="lyrico-lyrics-wrapper">You Are My Heart Beat
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Are My Heart Beat"/>
</div>
<div class="lyrico-lyrics-wrapper">You Are My Heart Beat
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Are My Heart Beat"/>
</div>
<div class="lyrico-lyrics-wrapper">You Are My Heart Beat
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Are My Heart Beat"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yenni Kalalakoo Nuvvu Badulani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenni Kalalakoo Nuvvu Badulani"/>
</div>
<div class="lyrico-lyrics-wrapper">Nedu Pongipothu Undi Yada Nijamani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nedu Pongipothu Undi Yada Nijamani"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu Yevarani Nannu Adigithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu Yevarani Nannu Adigithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Choopisthaa Nuvvanee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choopisthaa Nuvvanee"/>
</div>
<div class="lyrico-lyrics-wrapper">Yentha Dooralu Dooranni Penchinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yentha Dooralu Dooranni Penchinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu Daachaanu Naa Gunde Loothuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu Daachaanu Naa Gunde Loothuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnapranaale Ninnu Chooselaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnapranaale Ninnu Chooselaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Reppalloo Ninnunchi Poyaavee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Reppalloo Ninnunchi Poyaavee"/>
</div>
<div class="lyrico-lyrics-wrapper">You Are My Heart Beat
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Are My Heart Beat"/>
</div>
<div class="lyrico-lyrics-wrapper">You Are My Heart Beat
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Are My Heart Beat"/>
</div>
<div class="lyrico-lyrics-wrapper">You Are My Heart Beat
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Are My Heart Beat"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninu Taakina Chiru Gaalilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninu Taakina Chiru Gaalilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanne Cherukundi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanne Cherukundi "/>
</div>
<div class="lyrico-lyrics-wrapper">Aayivichhe Swasagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayivichhe Swasagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Pedavilo Prathi Palukuni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Pedavilo Prathi Palukuni"/>
</div>
<div class="lyrico-lyrics-wrapper">Panchaavee Premalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panchaavee Premalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu Lekunda Jeevincha Leenugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu Lekunda Jeevincha Leenugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Undipothaanu Nee Prema Sakshigaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undipothaanu Nee Prema Sakshigaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvanna Lokam Ledante Sunyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvanna Lokam Ledante Sunyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Naakantoo Ledantaa Ye Ardam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naakantoo Ledantaa Ye Ardam"/>
</div>
<div class="lyrico-lyrics-wrapper">You Are My Heart Beat
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Are My Heart Beat"/>
</div>
<div class="lyrico-lyrics-wrapper">You Are My Heart Beat
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Are My Heart Beat"/>
</div>
<div class="lyrico-lyrics-wrapper">You Are My Heart Beat
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Are My Heart Beat"/>
</div>
<div class="lyrico-lyrics-wrapper">You Are My Heart Beat
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Are My Heart Beat"/>
</div>
<div class="lyrico-lyrics-wrapper">You Are My Heart Beat
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Are My Heart Beat"/>
</div>
<div class="lyrico-lyrics-wrapper">You Are My Heart Beat
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Are My Heart Beat"/>
</div>
</pre>
