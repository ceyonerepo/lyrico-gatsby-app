---
title: "theipiraiya peththeduthe song lyrics"
album: "Jagame Thandhiram"
artist: "Santhosh Narayanan"
lyricist: "Madurai Babaraj"
director: "Karthik Subbaraj"
path: "/albums/jagame-thandhiram-song-lyrics"
song: "Theipiraiya peththeduthe"
image: ../../images/albumart/jagame-thanthiram.jpg
date: 2021-06-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/RmtRjRpJ5aY"
type: "sad"
singers:
  - Meenakshi Elaiyaraja
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">theipriraiya peththeduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theipriraiya peththeduthe"/>
</div>
<div class="lyrico-lyrics-wrapper">kan pola kaathirunthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kan pola kaathirunthen"/>
</div>
<div class="lyrico-lyrics-wrapper">kaththimuna soozhnilaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaththimuna soozhnilaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">kaapatha vazhiyumilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaapatha vazhiyumilla"/>
</div>
<div class="lyrico-lyrics-wrapper">kaapatha vazhiyumilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaapatha vazhiyumilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">enna vittu poriya en
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna vittu poriya en"/>
</div>
<div class="lyrico-lyrics-wrapper">uyir thuliye uraviname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyir thuliye uraviname"/>
</div>
<div class="lyrico-lyrics-wrapper">kannethire kalanguriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannethire kalanguriye"/>
</div>
<div class="lyrico-lyrics-wrapper">kathariththaan nikkuriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathariththaan nikkuriye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kai irunthum uthavavilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kai irunthum uthavavilla"/>
</div>
<div class="lyrico-lyrics-wrapper">kanaala paathirunthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanaala paathirunthen"/>
</div>
<div class="lyrico-lyrics-wrapper">entha naadu poreegalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="entha naadu poreegalo"/>
</div>
<div class="lyrico-lyrics-wrapper">eppadi poreegalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eppadi poreegalo"/>
</div>
<div class="lyrico-lyrics-wrapper">eppadi poreegalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eppadi poreegalo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">antha naadu eppadiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antha naadu eppadiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">antha makka eppadiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antha makka eppadiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">varavetru yerpaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varavetru yerpaaro"/>
</div>
<div class="lyrico-lyrics-wrapper">mugan juliththu verupaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mugan juliththu verupaaro"/>
</div>
<div class="lyrico-lyrics-wrapper">mugan juliththu verupaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mugan juliththu verupaaro"/>
</div>
<div class="lyrico-lyrics-wrapper">mugan juliththu verupaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mugan juliththu verupaaro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">eppo naama seruvamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eppo naama seruvamo"/>
</div>
<div class="lyrico-lyrics-wrapper">eppadi than seruvamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eppadi than seruvamo"/>
</div>
<div class="lyrico-lyrics-wrapper">alladum en usurum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alladum en usurum"/>
</div>
<div class="lyrico-lyrics-wrapper">athuvaraiyum thaangidumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athuvaraiyum thaangidumo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">eppo naama seruvamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eppo naama seruvamo"/>
</div>
<div class="lyrico-lyrics-wrapper">eppadi than seruvamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eppadi than seruvamo"/>
</div>
<div class="lyrico-lyrics-wrapper">alladum en usurum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alladum en usurum"/>
</div>
<div class="lyrico-lyrics-wrapper">athuvaraiyum thaangidumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athuvaraiyum thaangidumo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thayirukka pillaigala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thayirukka pillaigala"/>
</div>
<div class="lyrico-lyrics-wrapper">porkalamum setharadikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="porkalamum setharadikka"/>
</div>
<div class="lyrico-lyrics-wrapper">kai pesanja thaayum nikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kai pesanja thaayum nikka"/>
</div>
<div class="lyrico-lyrics-wrapper">pillai ippo yethilliaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pillai ippo yethilliaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thesam engum alangolam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thesam engum alangolam"/>
</div>
<div class="lyrico-lyrics-wrapper">thaai manasu kalanguthaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaai manasu kalanguthaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">poo mazhaiya paatha boomi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poo mazhaiya paatha boomi"/>
</div>
<div class="lyrico-lyrics-wrapper">gundu mazha paakkuthaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gundu mazha paakkuthaiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">theipriraiya peththeduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theipriraiya peththeduthe"/>
</div>
<div class="lyrico-lyrics-wrapper">kan pola kaathirunthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kan pola kaathirunthen"/>
</div>
<div class="lyrico-lyrics-wrapper">kaththimuna soozhnilaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaththimuna soozhnilaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">kaapatha vazhiyumilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaapatha vazhiyumilla"/>
</div>
<div class="lyrico-lyrics-wrapper">kaapatha vazhiyumilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaapatha vazhiyumilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">enna vittu poriya en
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna vittu poriya en"/>
</div>
<div class="lyrico-lyrics-wrapper">uyir thuliye uraviname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyir thuliye uraviname"/>
</div>
<div class="lyrico-lyrics-wrapper">kannethire kalanguriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannethire kalanguriye"/>
</div>
<div class="lyrico-lyrics-wrapper">kathariththaan nikkuriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathariththaan nikkuriye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kolamellam naala maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kolamellam naala maari"/>
</div>
<div class="lyrico-lyrics-wrapper">pogummaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pogummaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">kaalam varum nambikkaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaalam varum nambikkaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">poyee vaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poyee vaanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kolamellam naala maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kolamellam naala maari"/>
</div>
<div class="lyrico-lyrics-wrapper">pogummaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pogummaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">kaalam varum nambikkaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaalam varum nambikkaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">poyee vaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poyee vaanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">meendu vanthu vaazhnthidalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meendu vanthu vaazhnthidalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">poyee vaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poyee vaanga"/>
</div>
<div class="lyrico-lyrics-wrapper">thaai naadum kaathithirukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaai naadum kaathithirukku"/>
</div>
<div class="lyrico-lyrics-wrapper">poyee vaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poyee vaanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oru kathavu mooduchinna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru kathavu mooduchinna"/>
</div>
<div class="lyrico-lyrics-wrapper">maru kathavirukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maru kathavirukku"/>
</div>
<div class="lyrico-lyrics-wrapper">en pulla vaazha intha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en pulla vaazha intha"/>
</div>
<div class="lyrico-lyrics-wrapper">boomiyile yedamirukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="boomiyile yedamirukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">theipriraiya peththeduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theipriraiya peththeduthe"/>
</div>
<div class="lyrico-lyrics-wrapper">theipriraiya peththeduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theipriraiya peththeduthe"/>
</div>
<div class="lyrico-lyrics-wrapper">theipriraiya peththeduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theipriraiya peththeduthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">theipriraiya peththeduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theipriraiya peththeduthe"/>
</div>
<div class="lyrico-lyrics-wrapper">theipriraiya peththeduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theipriraiya peththeduthe"/>
</div>
<div class="lyrico-lyrics-wrapper">theipriraiya peththeduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theipriraiya peththeduthe"/>
</div>
</pre>
