---
title: "kadhal inikuthaiya song lyrics"
album: "Chidambaram Railwaygate"
artist: "Karthik Raja"
lyricist: "Priyan"
director: "Sivabalan"
path: "/albums/chidambaram-railwaygate-lyrics"
song: "Kadhal Inikuthaiya"
image: ../../images/albumart/chidambaram-railwaygate.jpg
date: 2021-02-05
lang: tamil
youtubeLink: "https://www.youtube.com/embed/XQW-hru8dbc"
type: "Love"
singers:
  - Venkat Prabhu
  - Premji Amaran
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">kadhal inikuthaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhal inikuthaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">dhinam dhinam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhinam dhinam"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhal inikuthaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhal inikuthaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">manam than 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manam than "/>
</div>
<div class="lyrico-lyrics-wrapper">love love nu adikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="love love nu adikum"/>
</div>
<div class="lyrico-lyrics-wrapper">love nu than thudikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="love nu than thudikum"/>
</div>
<div class="lyrico-lyrics-wrapper">kandabadi kuthikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kandabadi kuthikum"/>
</div>
<div class="lyrico-lyrics-wrapper">vazlka jaikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vazlka jaikum"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhal inikuthaiya...
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhal inikuthaiya..."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kadhalil vizhunthuputa...
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhalil vizhunthuputa..."/>
</div>
<div class="lyrico-lyrics-wrapper">kadhalil vizhunthuputa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhalil vizhunthuputa"/>
</div>
<div class="lyrico-lyrics-wrapper">kavalai illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kavalai illa"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjil kulapam illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjil kulapam illa"/>
</div>
<div class="lyrico-lyrics-wrapper">vazlkaiyila endrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vazlkaiyila endrum"/>
</div>
<div class="lyrico-lyrics-wrapper">varutham illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varutham illa"/>
</div>
<div class="lyrico-lyrics-wrapper">kalyanam ne panni paru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalyanam ne panni paru"/>
</div>
<div class="lyrico-lyrics-wrapper">thalavalinga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalavalinga"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhalicha maarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhalicha maarum"/>
</div>
<div class="lyrico-lyrics-wrapper">thalavithinga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalavithinga"/>
</div>
<div class="lyrico-lyrics-wrapper">keluda en pecha ne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="keluda en pecha ne"/>
</div>
<div class="lyrico-lyrics-wrapper">kan pesidum ondragidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kan pesidum ondragidum"/>
</div>
<div class="lyrico-lyrics-wrapper">idhayam para imayam eru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhayam para imayam eru"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjam than rendagi thundagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjam than rendagi thundagi"/>
</div>
<div class="lyrico-lyrics-wrapper">kondadum enakintha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kondadum enakintha"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhal inikuthaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhal inikuthaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">dhinam dhinam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhinam dhinam"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhal inikuthaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhal inikuthaiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ethanai cinema
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethanai cinema"/>
</div>
<div class="lyrico-lyrics-wrapper">ethanai drama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethanai drama"/>
</div>
<div class="lyrico-lyrics-wrapper">pathachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathachu"/>
</div>
<div class="lyrico-lyrics-wrapper">pasangalum pathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pasangalum pathu"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhalum senju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhalum senju"/>
</div>
<div class="lyrico-lyrics-wrapper">jaichachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jaichachu"/>
</div>
<div class="lyrico-lyrics-wrapper">pethavan patha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pethavan patha"/>
</div>
<div class="lyrico-lyrics-wrapper">ponna nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponna nee"/>
</div>
<div class="lyrico-lyrics-wrapper">katti ennachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="katti ennachu"/>
</div>
<div class="lyrico-lyrics-wrapper">maththalam pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maththalam pola"/>
</div>
<div class="lyrico-lyrics-wrapper">alivuthan vangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alivuthan vangi"/>
</div>
<div class="lyrico-lyrics-wrapper">punnachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="punnachu"/>
</div>
<div class="lyrico-lyrics-wrapper">MGR antha kalathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="MGR antha kalathula"/>
</div>
<div class="lyrico-lyrics-wrapper">duet-u ah padalaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="duet-u ah padalaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">nama sivaji
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nama sivaji"/>
</div>
<div class="lyrico-lyrics-wrapper">vantha kalathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vantha kalathula"/>
</div>
<div class="lyrico-lyrics-wrapper">love than pannalaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="love than pannalaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhal mannan kamal ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhal mannan kamal ah"/>
</div>
<div class="lyrico-lyrics-wrapper">ne pakkalaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ne pakkalaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">styleah romba style ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="styleah romba style ah"/>
</div>
<div class="lyrico-lyrics-wrapper">namma super star rajini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="namma super star rajini"/>
</div>
<div class="lyrico-lyrics-wrapper">paaniyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paaniyila"/>
</div>
<div class="lyrico-lyrics-wrapper">haiyo haiyo sami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="haiyo haiyo sami"/>
</div>
<div class="lyrico-lyrics-wrapper">ne love panni kaami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ne love panni kaami"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhal mattum pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhal mattum pona"/>
</div>
<div class="lyrico-lyrics-wrapper">ada suthathu intha boomi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada suthathu intha boomi"/>
</div>
<div class="lyrico-lyrics-wrapper">intha kaala ilaignar seiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha kaala ilaignar seiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhalukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhalukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">namma thala isaignaniyin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="namma thala isaignaniyin"/>
</div>
<div class="lyrico-lyrics-wrapper">paatirukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paatirukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">nengathan atha paadunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nengathan atha paadunga"/>
</div>
<div class="lyrico-lyrics-wrapper">epothume love pannunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="epothume love pannunga"/>
</div>
<div class="lyrico-lyrics-wrapper">namma thagappan kitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="namma thagappan kitta"/>
</div>
<div class="lyrico-lyrics-wrapper">thayum kitta sollitu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thayum kitta sollitu"/>
</div>
<div class="lyrico-lyrics-wrapper">neyum than love pannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neyum than love pannu"/>
</div>
<div class="lyrico-lyrics-wrapper">thappila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thappila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">namaku intha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="namaku intha "/>
</div>
<div class="lyrico-lyrics-wrapper">kadhal inikuthaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhal inikuthaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">dhinam dhinam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhinam dhinam"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhal inikuthaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhal inikuthaiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">manam than 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manam than "/>
</div>
<div class="lyrico-lyrics-wrapper">love lovenu adikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="love lovenu adikum"/>
</div>
<div class="lyrico-lyrics-wrapper">love nu than thudikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="love nu than thudikum"/>
</div>
<div class="lyrico-lyrics-wrapper">kandabadi kuthikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kandabadi kuthikum"/>
</div>
<div class="lyrico-lyrics-wrapper">vazlka jaikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vazlka jaikum"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhal inikuthaiya...
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhal inikuthaiya..."/>
</div>
</pre>
