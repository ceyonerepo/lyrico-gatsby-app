---
title: "o sye raa song lyrics"
album: "Sye Raa Narasimha Reddy"
artist: "Amit Trivedi"
lyricist: "Sirivennela Seetharama Sastry"
director: "Surender Reddy"
path: "/albums/sye-raa-narasimha-reddy-lyrics"
song: "O Sye Raa"
image: ../../images/albumart/sye-raa-narasimha-reddy.jpg
date: 2019-10-02
lang: telugu
youtubeLink: "https://www.youtube.com/embed/56qfU6_CR9U"
type: "happy"
singers:
  - Sunidhi Chauhan
  - Shreya Ghoshal
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Pavithra dhaathri bhaarataamba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pavithra dhaathri bhaarataamba"/>
</div>
<div class="lyrico-lyrics-wrapper">Muddhu biddavavuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muddhu biddavavuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyyaalavaada naarasimhudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyyaalavaada naarasimhudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Charithra putalu vismarincha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Charithra putalu vismarincha"/>
</div>
<div class="lyrico-lyrics-wrapper">Veelu leni veera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veelu leni veera"/>
</div>
<div class="lyrico-lyrics-wrapper">Renaati seema kanna sooryudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Renaati seema kanna sooryudaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mruthyuve swayaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mruthyuve swayaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Chiraayurasthu anagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chiraayurasthu anagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Prasoothi gandame jayinchinaavuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prasoothi gandame jayinchinaavuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ningi sirasuvanchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ningi sirasuvanchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Namosthu neeku anagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namosthu neeku anagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Navodhayanivai janinchinaavuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Navodhayanivai janinchinaavuraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">O Sye Raa O Sye Raa O Sye Raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Sye Raa O Sye Raa O Sye Raa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ushassu neeku oopiraayara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ushassu neeku oopiraayara"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Sye Raa O Sye Raa O Sye Raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Sye Raa O Sye Raa O Sye Raa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yashassu neeku roopamaayara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yashassu neeku roopamaayara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ahankarinchu aangla dhoralapaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahankarinchu aangla dhoralapaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Hunkarinchagalugu dhairyamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hunkarinchagalugu dhairyamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Talonchi brathuku saativarilona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Talonchi brathuku saativarilona"/>
</div>
<div class="lyrico-lyrics-wrapper">Saahasaanni nimpu souryamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saahasaanni nimpu souryamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Shrunkalaalane thenchukommani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shrunkalaalane thenchukommani"/>
</div>
<div class="lyrico-lyrics-wrapper">Swecha kosame swaasanimmani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Swecha kosame swaasanimmani"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninaadham neeveraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaadham neeveraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Okkokka bhindhuvalle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okkokka bhindhuvalle"/>
</div>
<div class="lyrico-lyrics-wrapper">Janulanokkachota cherchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Janulanokkachota cherchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Samudhramalle maarchinaavuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samudhramalle maarchinaavuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Maarchinaavuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maarchinaavuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Prapanchamonikipovu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prapanchamonikipovu"/>
</div>
<div class="lyrico-lyrics-wrapper">Penu thoofan laaga veechi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Penu thoofan laaga veechi"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhoralni dhikkarinchinaavuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhoralni dhikkarinchinaavuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Motta modhatisaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Motta modhatisaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Swathanthra samara bheri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Swathanthra samara bheri"/>
</div>
<div class="lyrico-lyrics-wrapper">Petillu mannadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Petillu mannadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Praajaali poridhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Praajaali poridhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalaraathri vanti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalaraathri vanti"/>
</div>
<div class="lyrico-lyrics-wrapper">Paraayi paalanaanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paraayi paalanaanni"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhahinchu jwaalalo prakasame idhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhahinchu jwaalalo prakasame idhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">O Sye Raa O Sye Raa O Sye Raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Sye Raa O Sye Raa O Sye Raa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ushassu neeku oopiraayara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ushassu neeku oopiraayara"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Sye Raa O Sye Raa O Sye Raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Sye Raa O Sye Raa O Sye Raa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yashassu neeku roopamaayara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yashassu neeku roopamaayara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Daasyaaana jeevinchadam kanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daasyaaana jeevinchadam kanna"/>
</div>
<div class="lyrico-lyrics-wrapper">Chaaventho melannadhi nee pourusham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chaaventho melannadhi nee pourusham"/>
</div>
<div class="lyrico-lyrics-wrapper">Manshulaithe manam anichivese julum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manshulaithe manam anichivese julum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oppukokandhi nee udyamam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oppukokandhi nee udyamam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aalani biddani ammani janmani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalani biddani ammani janmani"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhandhanaalanni vodili saagudhaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhandhanaalanni vodili saagudhaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvve lakshalai oke lakshyamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvve lakshalai oke lakshyamai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ate veyani prathi padham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ate veyani prathi padham"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhana rangamanthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhana rangamanthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhana rangamanthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhana rangamanthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodhama singamalle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodhama singamalle"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodhama singamalle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodhama singamalle"/>
</div>
<div class="lyrico-lyrics-wrapper">Aakraminchi aakraminchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aakraminchi aakraminchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vikraminchi vikramanchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vikraminchi vikramanchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharumuthoondhri ari 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharumuthoondhri ari "/>
</div>
<div class="lyrico-lyrics-wrapper">veera samhaaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veera samhaaraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho Sye Raa Ho Sye Raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho Sye Raa Ho Sye Raa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho Sye Raa Ho Sye Raa Ho Sye Raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho Sye Raa Ho Sye Raa Ho Sye Raa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ushassu neeku oopiraayara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ushassu neeku oopiraayara"/>
</div>
</pre>
