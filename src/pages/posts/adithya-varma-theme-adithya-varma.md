---
title: "adithya varma theme song lyrics"
album: "Adithya Varma"
artist: "Radhan"
lyricist: "Radhan"
director: "Gireesaaya"
path: "/albums/adithya-varma-lyrics"
song: "Adithya Varma Theme"
image: ../../images/albumart/adithya-varma.jpg
date: 2019-11-22
lang: tamil
youtubeLink: "https://www.youtube.com/embed/jehWJCqDx-4"
type: "theme"
singers:
  - Dhruv Vikram
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">yeah keep it hot
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yeah keep it hot"/>
</div>
<div class="lyrico-lyrics-wrapper">keep it goin like a rockstar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="keep it goin like a rockstar"/>
</div>
<div class="lyrico-lyrics-wrapper">fetch your bitches 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="fetch your bitches "/>
</div>
<div class="lyrico-lyrics-wrapper">keep it goin like a mobstar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="keep it goin like a mobstar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">tadai than unna taduthida nenaccha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tadai than unna taduthida nenaccha"/>
</div>
<div class="lyrico-lyrics-wrapper">ulagam unna otchinda nenaccha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulagam unna otchinda nenaccha"/>
</div>
<div class="lyrico-lyrics-wrapper">mavane atha udda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mavane atha udda"/>
</div>
<div class="lyrico-lyrics-wrapper">katha mudida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="katha mudida"/>
</div>
<div class="lyrico-lyrics-wrapper">idhu unnoda life da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhu unnoda life da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">so f the world f the people 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="so f the world f the people "/>
</div>
<div class="lyrico-lyrics-wrapper">f the pain
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="f the pain"/>
</div>
<div class="lyrico-lyrics-wrapper">gotta censor this sh 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gotta censor this sh "/>
</div>
<div class="lyrico-lyrics-wrapper">i m gonna make it rain
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="i m gonna make it rain"/>
</div>
<div class="lyrico-lyrics-wrapper">its adi v
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="its adi v"/>
</div>
<div class="lyrico-lyrics-wrapper">yeah remember the name it s adi v
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yeah remember the name it s adi v"/>
</div>
<div class="lyrico-lyrics-wrapper">adi v
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi v"/>
</div>
</pre>
