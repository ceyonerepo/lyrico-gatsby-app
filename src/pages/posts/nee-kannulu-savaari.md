---
title: "nee kannulu song lyrics"
album: "Savaari"
artist: "Shekar Chandra"
lyricist: "Kasarla Shyam"
director: "Saahith Mothkuri"
path: "/albums/savaari-lyrics"
song: "Nee Kannulu"
image: ../../images/albumart/savaari.jpg
date: 2020-02-07
lang: telugu
youtubeLink: "https://www.youtube.com/embed/zU8TWtSU0uE"
type: "happy"
singers:
  - Rahul Sipligunj
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Neeee Kannulo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeee Kannulo"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Dillulo Naatukunnaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Dillulo Naatukunnaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Pillo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pillo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Kannulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kannulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Dillulo Naatukunnaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Dillulo Naatukunnaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Osey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Osey"/>
</div>
<div class="lyrico-lyrics-wrapper">O Payyala Giragira Chuttura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Payyala Giragira Chuttura"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiruguthunnane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiruguthunnane"/>
</div>
<div class="lyrico-lyrics-wrapper">Arerey Sinnadhana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arerey Sinnadhana"/>
</div>
<div class="lyrico-lyrics-wrapper">Yama Kiraak Unnave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yama Kiraak Unnave"/>
</div>
<div class="lyrico-lyrics-wrapper">Yenake Nenu Raana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenake Nenu Raana"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Giraaki Nuvve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Giraaki Nuvve"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Sekalu Thai Thakkalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Sekalu Thai Thakkalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kick Ey Ekkindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kick Ey Ekkindhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Pilla Naa Akhalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pilla Naa Akhalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Debbaku Sukkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Debbaku Sukkala"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakkana Nakkindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkana Nakkindhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oooooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oooooooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Charminaru Meedhane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Charminaru Meedhane"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaalina Pavuramai Yeyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaalina Pavuramai Yeyy"/>
</div>
<div class="lyrico-lyrics-wrapper">Teenu Maaru Chesthunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teenu Maaru Chesthunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Bonallo Pothrajai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bonallo Pothrajai"/>
</div>
<div class="lyrico-lyrics-wrapper">Chod Diya Khaana Peena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chod Diya Khaana Peena"/>
</div>
<div class="lyrico-lyrics-wrapper">Tere Khayaal Mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere Khayaal Mein"/>
</div>
<div class="lyrico-lyrics-wrapper">Jeena Ya Marna Tere Bina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jeena Ya Marna Tere Bina"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Valla Avvadhu Le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Valla Avvadhu Le"/>
</div>
<div class="lyrico-lyrics-wrapper">Na Angiki O Atharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na Angiki O Atharu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottinattundhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottinattundhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Arre Naa Lungiki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arre Naa Lungiki"/>
</div>
<div class="lyrico-lyrics-wrapper">Chamanthul Athkavettinattundhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chamanthul Athkavettinattundhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Pillo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pillo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa Gurranki Makeup Eshi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Gurranki Makeup Eshi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thesthane Bharaat Ke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thesthane Bharaat Ke"/>
</div>
<div class="lyrico-lyrics-wrapper">Old City Gallillo Thesthane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Old City Gallillo Thesthane"/>
</div>
<div class="lyrico-lyrics-wrapper">Chandhini Raathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chandhini Raathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Dosthulandariki Dawath Istha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dosthulandariki Dawath Istha"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana Shaadi Lo Naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana Shaadi Lo Naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mukka Sukka Anni Vedtha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mukka Sukka Anni Vedtha"/>
</div>
<div class="lyrico-lyrics-wrapper">Dinner La Djvedthaaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dinner La Djvedthaaaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Pakkana Nenunnatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Pakkana Nenunnatu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalusukuntunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalusukuntunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Arre Calcutta Meenaakshi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arre Calcutta Meenaakshi"/>
</div>
<div class="lyrico-lyrics-wrapper">Paanu Notla Vettinattundhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paanu Notla Vettinattundhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Kannulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kannulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Dillulo Naatukunnaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Dillulo Naatukunnaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Osey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Osey"/>
</div>
<div class="lyrico-lyrics-wrapper">O Payyala Giragira Chuttura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Payyala Giragira Chuttura"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiruguthunnane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiruguthunnane"/>
</div>
<div class="lyrico-lyrics-wrapper">Arerey Sinnadhana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arerey Sinnadhana"/>
</div>
<div class="lyrico-lyrics-wrapper">Yama Kiraak Unnave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yama Kiraak Unnave"/>
</div>
<div class="lyrico-lyrics-wrapper">Yenake Nenu Raana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenake Nenu Raana"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Giraaki Nuvve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Giraaki Nuvve"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Sekalu Thai Thakkalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Sekalu Thai Thakkalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kick Ey Ekkindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kick Ey Ekkindhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Pilla Naa Akhalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pilla Naa Akhalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Debbaku Sukkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Debbaku Sukkala"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakkana Nakkindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkana Nakkindhe"/>
</div>
</pre>
