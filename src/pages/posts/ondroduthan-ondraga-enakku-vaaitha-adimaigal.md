---
title: "ondroduthan ondraga song lyrics"
album: "Enakku Vaaitha Adimaigal"
artist: "Santhosh Dhayanidhi"
lyricist: "Kabilan"
director: "Mahendran Rajamani"
path: "/albums/enakku-vaaitha-adimaigal-lyrics"
song: "Ondroduthan Ondraga"
image: ../../images/albumart/enakku-vaaitha-adimaigal.jpg
date: 2017-02-02
lang: tamil
youtubeLink: "https://www.youtube.com/embed/zwp-pVLsyDU"
type: "happy"
singers:
  - Anirudh Ravichander
  - Santhosh Dhayanidhi
  - Chennai Boys Choir
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ondrodu thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondrodu thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ondraaga Vaazhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondraaga Vaazhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanneerum meen pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanneerum meen pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaalumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaalumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Natpilla vaazhakai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natpilla vaazhakai"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppodhum bore nanbaa ah ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppodhum bore nanbaa ah ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee thaanada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thaanada"/>
</div>
<div class="lyrico-lyrics-wrapper">En thaayin anbai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En thaayin anbai"/>
</div>
<div class="lyrico-lyrics-wrapper">Natpendru paalaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natpendru paalaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Ootinaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Ootinaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Raththathil seraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raththathil seraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sontham dhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sontham dhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee nanbaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee nanbaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Udalaai pirinthu irunthiduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udalaai pirinthu irunthiduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Uriyaai inainthu nadanthiduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uriyaai inainthu nadanthiduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Thozhanae ellai tholviyae illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozhanae ellai tholviyae illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaada nee vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada nee vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veyilil kadal neer vadivathillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veyilil kadal neer vadivathillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Natpin ellai mudivathillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natpin ellai mudivathillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aathalaal natpai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aathalaal natpai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaai mozhi pola kondaaduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaai mozhi pola kondaaduvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ondrodu thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondrodu thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ondraaga Vaazhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondraaga Vaazhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanneerum meen pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanneerum meen pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaalumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaalumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Friendsilla vaazhakai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friendsilla vaazhakai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappagumae nanbaa ah ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappagumae nanbaa ah ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal tholviyaal pirinthidumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal tholviyaal pirinthidumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Natpu tholviyaal pirinthidumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natpu tholviyaal pirinthidumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thevaikaagavae pazhaaguvathillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thevaikaagavae pazhaaguvathillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanban eppothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanban eppothum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nanban koodavae irukkum varai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanban koodavae irukkum varai"/>
</div>
<div class="lyrico-lyrics-wrapper">Deivam kangalil therivathillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Deivam kangalil therivathillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Therthal koottani arasiyal pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Therthal koottani arasiyal pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Natpu seraathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natpu seraathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey kalapu mana kaadhalai serka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey kalapu mana kaadhalai serka"/>
</div>
<div class="lyrico-lyrics-wrapper">Government-u office-u povom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Government-u office-u povom"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadiyadi police-u thanthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadiyadi police-u thanthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaangi kolvomae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaangi kolvomae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanakkaai endrum vazhvathillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanakkaai endrum vazhvathillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaniyaai engum povathilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaniyaai engum povathilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Aatharavindri vazhkira podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aatharavindri vazhkira podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanthai thaayaai aavaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanthai thaayaai aavaanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kattam potta lungi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattam potta lungi"/>
</div>
<div class="lyrico-lyrics-wrapper">Katti kittu naanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katti kittu naanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Petti kada munnae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Petti kada munnae"/>
</div>
<div class="lyrico-lyrics-wrapper">Attam poduvomae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Attam poduvomae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pottikulla pamba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pottikulla pamba"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattu pada maatom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattu pada maatom"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetti pechi pesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetti pechi pesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirivomae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirivomae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey vanthupoga maamiyar veedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey vanthupoga maamiyar veedu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sonthangolla saamiyaar veedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sonthangolla saamiyaar veedu"/>
</div>
<div class="lyrico-lyrics-wrapper">Engalukku yaarumae illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engalukku yaarumae illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Natpae nammodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natpae nammodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuzhandhai pola irunthiduvomae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuzhandhai pola irunthiduvomae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuruvi pola paranthiduvomae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuruvi pola paranthiduvomae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadalil seraa nadhiya pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadalil seraa nadhiya pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudineer natpaagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudineer natpaagum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanakkaai endrum vazhvathillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanakkaai endrum vazhvathillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaniyaai engum povathilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaniyaai engum povathilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Aatharavindri vazhkira podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aatharavindri vazhkira podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanthai thaayaai aavaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanthai thaayaai aavaanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ondrodu thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondrodu thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ondraaga Vaazhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondraaga Vaazhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanneerum meen pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanneerum meen pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaalumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaalumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Natpilla vaazhakai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natpilla vaazhakai"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppodhum bore nanbaa ah ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppodhum bore nanbaa ah ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee thaanada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thaanada"/>
</div>
<div class="lyrico-lyrics-wrapper">En thaayin anbai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En thaayin anbai"/>
</div>
<div class="lyrico-lyrics-wrapper">Natpendru paalaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natpendru paalaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Ootinaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Ootinaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Raththathil seraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raththathil seraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sontham dhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sontham dhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee nanbaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee nanbaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ondrodu thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondrodu thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ondraaga Vaazhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondraaga Vaazhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanneerum meen pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanneerum meen pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaalumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaalumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Natpilla vaazhakai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natpilla vaazhakai"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppodhum bore nanbaa ah ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppodhum bore nanbaa ah ah"/>
</div>
</pre>
