---
title: "naan thaan goppan da song lyrics"
album: "Bruce Lee"
artist: "G V Prakash Kumar"
lyricist: "Arunraja Kamaraj"
director: "Prashanth Pandiraj"
path: "/albums/bruce-lee-lyrics"
song: "Naan Thaan Goppan Da"
image: ../../images/albumart/bruce-lee.jpg
date: 2017-03-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ILQ_8kEpyBw"
type: "mass"
singers:
  -	Arunraja Kamaraj
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Naan thaan goppan da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan thaan goppan da"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalla muthu peran da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla muthu peran da"/>
</div>
<div class="lyrico-lyrics-wrapper">Thammaa thundu sandaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thammaa thundu sandaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Perusa oothi viduvaan daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perusa oothi viduvaan daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aee yaar ra avan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aee yaar ra avan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarra avan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarra avan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaru avan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaru avan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennathukku durr aaguringa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennathukku durr aaguringa"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan peru ennathuda avanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan peru ennathuda avanthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bruce leee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bruce leee"/>
</div>
<div class="lyrico-lyrics-wrapper">He’s gonna show you the craziness
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="He’s gonna show you the craziness"/>
</div>
<div class="lyrico-lyrics-wrapper">Over laziness
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Over laziness"/>
</div>
<div class="lyrico-lyrics-wrapper">In every possible ways
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="In every possible ways"/>
</div>
<div class="lyrico-lyrics-wrapper">So many days he has been
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="So many days he has been"/>
</div>
<div class="lyrico-lyrics-wrapper">Looking for a hardcore chase
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Looking for a hardcore chase"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Do we really think who’s the man
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Do we really think who’s the man"/>
</div>
<div class="lyrico-lyrics-wrapper">Everybody says he is the one
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Everybody says he is the one"/>
</div>
<div class="lyrico-lyrics-wrapper">With lots of
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="With lots of"/>
</div>
<div class="lyrico-lyrics-wrapper">Fun n fun n fun n
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fun n fun n fun n"/>
</div>
<div class="lyrico-lyrics-wrapper">Banging around like a firing gun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Banging around like a firing gun"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan thaan goppan da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan thaan goppan da"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalla muthu peran da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla muthu peran da"/>
</div>
<div class="lyrico-lyrics-wrapper">Vachu seiyya ippa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vachu seiyya ippa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthurukken paaruda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthurukken paaruda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dappunu adichaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dappunu adichaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sevulu pagulu kizhiyumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sevulu pagulu kizhiyumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Udayumae kolayum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udayumae kolayum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadunga vappaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadunga vappaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bruce leee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bruce leee"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeahhhh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeahhhh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivan pera kettathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan pera kettathum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai kaal othara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai kaal othara"/>
</div>
<div class="lyrico-lyrics-wrapper">Vai kathara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vai kathara"/>
</div>
<div class="lyrico-lyrics-wrapper">Sithari oda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sithari oda"/>
</div>
<div class="lyrico-lyrics-wrapper">Ha ha haaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ha ha haaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Onn ezhumbathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onn ezhumbathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Udachu enni kaattu Vvan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udachu enni kaattu Vvan"/>
</div>
<div class="lyrico-lyrics-wrapper">Onn kodala vuruvi vuruvi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onn kodala vuruvi vuruvi"/>
</div>
<div class="lyrico-lyrics-wrapper">Maala poduvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maala poduvaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Su su super man pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Su su super man pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Suthi varuvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suthi varuvaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thae thae thae thera izhuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thae thae thae thera izhuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Theruvula viduvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theruvula viduvaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh oh Oththaya ninnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh oh Oththaya ninnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sanda poduvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sanda poduvaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan ketta paiyan illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan ketta paiyan illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Rompavum rompavum nalla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rompavum rompavum nalla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bruce Leee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bruce Leee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mummy kitta solli puttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mummy kitta solli puttu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pallikoodam poikittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pallikoodam poikittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pallikoodam pakkathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pallikoodam pakkathula"/>
</div>
<div class="lyrico-lyrics-wrapper">Goli gunda aadikittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Goli gunda aadikittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Goli gundu aadayila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Goli gundu aadayila"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandaiya thaan Izhuthukittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandaiya thaan Izhuthukittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandaiyathaan podayila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandaiyathaan podayila"/>
</div>
<div class="lyrico-lyrics-wrapper">Sattaiyadhaan kizhuchukittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sattaiyadhaan kizhuchukittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mandaiyatha odachu kittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandaiyatha odachu kittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Roattumela odipoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Roattumela odipoi"/>
</div>
<div class="lyrico-lyrics-wrapper">Veetukulla ozhunjukittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veetukulla ozhunjukittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Patcha pulla pola ninnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Patcha pulla pola ninnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mummy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mummy"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan enna adikiraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan enna adikiraan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan thaan goppan da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan thaan goppan da"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalla muthu peran da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla muthu peran da"/>
</div>
<div class="lyrico-lyrics-wrapper">Vachu seiyya ippa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vachu seiyya ippa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthurukken paaruda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthurukken paaruda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hahaa hahahaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hahaa hahahaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hahaa hahahaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hahaa hahahaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa othaikku othaikku vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa othaikku othaikku vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Evan munnaala nikkatha mattayava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evan munnaala nikkatha mattayava"/>
</div>
<div class="lyrico-lyrics-wrapper">Po odi po orama po
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Po odi po orama po"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannu munnadi nikkama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu munnadi nikkama"/>
</div>
<div class="lyrico-lyrics-wrapper">Thooramaa po
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thooramaa po"/>
</div>
<div class="lyrico-lyrics-wrapper">Va vaa vaa vaa vanthu paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Va vaa vaa vaa vanthu paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudunjakka mothi paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudunjakka mothi paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorukulla kettu paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorukulla kettu paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Nammaalu fighting staru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nammaalu fighting staru"/>
</div>
<div class="lyrico-lyrics-wrapper">Fighting staru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fighting staru"/>
</div>
<div class="lyrico-lyrics-wrapper">Fighting staru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fighting staru"/>
</div>
<div class="lyrico-lyrics-wrapper">Fightingfighting
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fightingfighting"/>
</div>
<div class="lyrico-lyrics-wrapper">Fightingfighting
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fightingfighting"/>
</div>
<div class="lyrico-lyrics-wrapper">Fy fy fy fy fy fy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fy fy fy fy fy fy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa othaikku othaikku vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa othaikku othaikku vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Evan munnaala nikkatha mattayava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evan munnaala nikkatha mattayava"/>
</div>
<div class="lyrico-lyrics-wrapper">Po odi po orama po
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Po odi po orama po"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannu munnadi nikkama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu munnadi nikkama"/>
</div>
<div class="lyrico-lyrics-wrapper">Thooramaa po
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thooramaa po"/>
</div>
<div class="lyrico-lyrics-wrapper">Va vaa vaa vaa vanthu paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Va vaa vaa vaa vanthu paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudunjakka mothi paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudunjakka mothi paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorukulla kettu paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorukulla kettu paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Nammaalu fighting staru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nammaalu fighting staru"/>
</div>
<div class="lyrico-lyrics-wrapper">Fighting staru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fighting staru"/>
</div>
<div class="lyrico-lyrics-wrapper">Fighting staru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fighting staru"/>
</div>
<div class="lyrico-lyrics-wrapper">Fightingfighting
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fightingfighting"/>
</div>
<div class="lyrico-lyrics-wrapper">Fighting staru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fighting staru"/>
</div>
<div class="lyrico-lyrics-wrapper">Fightingfighting
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fightingfighting"/>
</div>
<div class="lyrico-lyrics-wrapper">Fighting staru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fighting staru"/>
</div>
<div class="lyrico-lyrics-wrapper">Fightingfighting
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fightingfighting"/>
</div>
<div class="lyrico-lyrics-wrapper">Fighting staru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fighting staru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan thaan goppan da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan thaan goppan da"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalla muthu peran da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla muthu peran da"/>
</div>
<div class="lyrico-lyrics-wrapper">Fightingfighting
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fightingfighting"/>
</div>
<div class="lyrico-lyrics-wrapper">Fighting staru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fighting staru"/>
</div>
<div class="lyrico-lyrics-wrapper">Fightingfighting
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fightingfighting"/>
</div>
<div class="lyrico-lyrics-wrapper">Fighting staru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fighting staru"/>
</div>
<div class="lyrico-lyrics-wrapper">Vachu seiyya ippa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vachu seiyya ippa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthurukken paaruda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthurukken paaruda"/>
</div>
<div class="lyrico-lyrics-wrapper">Fightingfighting
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fightingfighting"/>
</div>
<div class="lyrico-lyrics-wrapper">Fighting staru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fighting staru"/>
</div>
<div class="lyrico-lyrics-wrapper">Fightingfighting
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fightingfighting"/>
</div>
<div class="lyrico-lyrics-wrapper">Fighting staru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fighting staru"/>
</div>
</pre>
