---
title: "karuvelang kaattukku song lyrics"
album: "Seemathurai"
artist: "Jose Franklin"
lyricist: "Veenai Maindhan"
director: "Santhosh Thiyagarajan"
path: "/albums/seemathurai-lyrics"
song: "Karuvelang Kaattukku"
image: ../../images/albumart/seemathurai.jpg
date: 2018-12-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/3UU7gYvrKng"
type: "sad"
singers:
  - Sathyaprakash
  - Anitha Karthikeyan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">karuvelang kattuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karuvelang kattuku"/>
</div>
<div class="lyrico-lyrics-wrapper">vedha pottatharu ingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vedha pottatharu ingu"/>
</div>
<div class="lyrico-lyrics-wrapper">sedha pola naan ingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sedha pola naan ingu"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjil thee aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjil thee aachu"/>
</div>
<div class="lyrico-lyrics-wrapper">uravunnu venthane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uravunnu venthane"/>
</div>
<div class="lyrico-lyrics-wrapper">usurukkul ninnane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="usurukkul ninnane"/>
</div>
<div class="lyrico-lyrics-wrapper">kalavani pattam thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalavani pattam thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">naane thanthene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naane thanthene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">penavi unna oothi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="penavi unna oothi"/>
</div>
<div class="lyrico-lyrics-wrapper">peraga nenjil theeti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="peraga nenjil theeti"/>
</div>
<div class="lyrico-lyrics-wrapper">uthattala usura thantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uthattala usura thantha"/>
</div>
<div class="lyrico-lyrics-wrapper">sagatha nesam thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sagatha nesam thaane"/>
</div>
<div class="lyrico-lyrics-wrapper">thavaraana vidhiya pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thavaraana vidhiya pochu"/>
</div>
<div class="lyrico-lyrics-wrapper">tharisana nilama achu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tharisana nilama achu"/>
</div>
<div class="lyrico-lyrics-wrapper">mazhaiyaga naane than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mazhaiyaga naane than"/>
</div>
<div class="lyrico-lyrics-wrapper">inge vanthene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inge vanthene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ini neeyum naanum than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ini neeyum naanum than"/>
</div>
<div class="lyrico-lyrics-wrapper">iru thesaiya povoma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iru thesaiya povoma"/>
</div>
<div class="lyrico-lyrics-wrapper">gathi yaarum illatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gathi yaarum illatha"/>
</div>
<div class="lyrico-lyrics-wrapper">thani marama avoma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thani marama avoma"/>
</div>
<div class="lyrico-lyrics-wrapper">thiruneeru naan poosa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thiruneeru naan poosa"/>
</div>
<div class="lyrico-lyrics-wrapper">en vithiyum marathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en vithiyum marathe"/>
</div>
<div class="lyrico-lyrics-wrapper">ini kanal neerilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ini kanal neerilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">en sabam theera 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en sabam theera "/>
</div>
<div class="lyrico-lyrics-wrapper">naan neerada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan neerada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vittil poochi than naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vittil poochi than naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">veetil yethina dheebam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veetil yethina dheebam"/>
</div>
<div class="lyrico-lyrics-wrapper">irugil thee pattu karugum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irugil thee pattu karugum"/>
</div>
<div class="lyrico-lyrics-wrapper">nerathil oodi vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nerathil oodi vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">kaaga kootula naane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaaga kootula naane"/>
</div>
<div class="lyrico-lyrics-wrapper">kuyilu mutta aanene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuyilu mutta aanene"/>
</div>
<div class="lyrico-lyrics-wrapper">unnodu kanner naan thane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnodu kanner naan thane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">mey masam sutta than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mey masam sutta than"/>
</div>
<div class="lyrico-lyrics-wrapper">mekathu kuliragum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mekathu kuliragum"/>
</div>
<div class="lyrico-lyrics-wrapper">nee vazha naane than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee vazha naane than"/>
</div>
<div class="lyrico-lyrics-wrapper">kaatha maruven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatha maruven"/>
</div>
<div class="lyrico-lyrics-wrapper">vidhiyoda poradi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidhiyoda poradi"/>
</div>
<div class="lyrico-lyrics-wrapper">vilaketha neeyum vayen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vilaketha neeyum vayen"/>
</div>
<div class="lyrico-lyrics-wrapper">theeratha sogam sagattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theeratha sogam sagattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">un kayangal aara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kayangal aara"/>
</div>
<div class="lyrico-lyrics-wrapper">naan viral neeti theda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan viral neeti theda"/>
</div>
<div class="lyrico-lyrics-wrapper">ingu nerupparu oda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ingu nerupparu oda"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjil eeram kaaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjil eeram kaaya"/>
</div>
<div class="lyrico-lyrics-wrapper">un nenjodu naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un nenjodu naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">en nenappodu neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en nenappodu neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">ini meendum mannil varanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ini meendum mannil varanum"/>
</div>
<div class="lyrico-lyrics-wrapper">intha senmam pogattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha senmam pogattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">una serathane naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="una serathane naan"/>
</div>
<div class="lyrico-lyrics-wrapper">en thaayin karuvane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en thaayin karuvane"/>
</div>
<div class="lyrico-lyrics-wrapper">intha kala padi thandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha kala padi thandi"/>
</div>
<div class="lyrico-lyrics-wrapper">naan verasa varuvene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan verasa varuvene"/>
</div>
<div class="lyrico-lyrics-wrapper">ethu munne vanthalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethu munne vanthalum"/>
</div>
<div class="lyrico-lyrics-wrapper">athu umiya pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu umiya pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">oru senmam pothume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru senmam pothume"/>
</div>
<div class="lyrico-lyrics-wrapper">na unna seruven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="na unna seruven"/>
</div>
</pre>
