---
title: "seeli nikkan song lyrics"
album: "Thanne Vandi"
artist: "Moses"
lyricist: "Mohanraja"
director: "Manika Vidya"
path: "/albums/thanne-vandi-lyrics"
song: "Seeli Nikkan"
image: ../../images/albumart/thanne-vandi.jpg
date: 2021-12-31
lang: tamil
youtubeLink: "https://www.youtube.com/embed/HGWMPGI0WNA"
type: "happy"
singers:
  - Anthony Daasan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">seeli nikkan silla nikkan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seeli nikkan silla nikkan"/>
</div>
<div class="lyrico-lyrics-wrapper">koovi nikkan kooti nikkan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koovi nikkan kooti nikkan"/>
</div>
<div class="lyrico-lyrics-wrapper">pesi nikkan parthu nikkan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pesi nikkan parthu nikkan"/>
</div>
<div class="lyrico-lyrics-wrapper">kathu pola odi nikkan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathu pola odi nikkan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kara murae kara murae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kara murae kara murae"/>
</div>
<div class="lyrico-lyrics-wrapper">kara murae kara murae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kara murae kara murae"/>
</div>
<div class="lyrico-lyrics-wrapper">love love love lovasae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="love love love lovasae"/>
</div>
<div class="lyrico-lyrics-wrapper">love love love lovasae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="love love love lovasae"/>
</div>
<div class="lyrico-lyrics-wrapper">love love love lovasae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="love love love lovasae"/>
</div>
<div class="lyrico-lyrics-wrapper">love love love lovasae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="love love love lovasae"/>
</div>
<div class="lyrico-lyrics-wrapper">lovasae lovasae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lovasae lovasae"/>
</div>
<div class="lyrico-lyrics-wrapper">thaga daga daga thamasae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaga daga daga thamasae"/>
</div>
<div class="lyrico-lyrics-wrapper">thaga daga daga thamasae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaga daga daga thamasae"/>
</div>
<div class="lyrico-lyrics-wrapper">thaga daga daga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaga daga daga"/>
</div>
<div class="lyrico-lyrics-wrapper">thaga daga daga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaga daga daga"/>
</div>
<div class="lyrico-lyrics-wrapper">thamasae thamasae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thamasae thamasae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">para para para para para
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="para para para para para"/>
</div>
<div class="lyrico-lyrics-wrapper">paramakudi bus standiku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paramakudi bus standiku"/>
</div>
<div class="lyrico-lyrics-wrapper">pakkathu theru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pakkathu theru "/>
</div>
<div class="lyrico-lyrics-wrapper">naan mudhal mudhala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan mudhal mudhala"/>
</div>
<div class="lyrico-lyrics-wrapper">ponna patha storya kelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponna patha storya kelu"/>
</div>
<div class="lyrico-lyrics-wrapper">beerula than more a kalanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="beerula than more a kalanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">kudika kuduthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kudika kuduthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">ava appan panna thappala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ava appan panna thappala"/>
</div>
<div class="lyrico-lyrics-wrapper">thappala vennanu sonnein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thappala vennanu sonnein"/>
</div>
<div class="lyrico-lyrics-wrapper">ponna vennanu sonnein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponna vennanu sonnein"/>
</div>
<div class="lyrico-lyrics-wrapper">vennanu sonnein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vennanu sonnein"/>
</div>
<div class="lyrico-lyrics-wrapper">ponna vennanu sonnein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponna vennanu sonnein"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">seeli nikkan siya nikkan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seeli nikkan siya nikkan"/>
</div>
<div class="lyrico-lyrics-wrapper">seeli nikkan siya nikkan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seeli nikkan siya nikkan"/>
</div>
<div class="lyrico-lyrics-wrapper">seeli nikkan siya nikkan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seeli nikkan siya nikkan"/>
</div>
<div class="lyrico-lyrics-wrapper">seeyaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seeyaan"/>
</div>
<div class="lyrico-lyrics-wrapper">seeli nikkan siya nikkan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seeli nikkan siya nikkan"/>
</div>
<div class="lyrico-lyrics-wrapper">seeli nikkan siya nikkan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seeli nikkan siya nikkan"/>
</div>
<div class="lyrico-lyrics-wrapper">seeli nikkan siya nikkan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seeli nikkan siya nikkan"/>
</div>
<div class="lyrico-lyrics-wrapper">seeyaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seeyaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaaraikudi pakathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaaraikudi pakathula"/>
</div>
<div class="lyrico-lyrics-wrapper">devakottai anga thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="devakottai anga thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">oru ponna pathen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru ponna pathen"/>
</div>
<div class="lyrico-lyrics-wrapper">neyum kelu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neyum kelu da"/>
</div>
<div class="lyrico-lyrics-wrapper">monitor pera kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="monitor pera kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">solla theriyala ava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="solla theriyala ava"/>
</div>
<div class="lyrico-lyrics-wrapper">annan mela kovapattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="annan mela kovapattu"/>
</div>
<div class="lyrico-lyrics-wrapper">kovapattu vennanu sonnein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kovapattu vennanu sonnein"/>
</div>
<div class="lyrico-lyrics-wrapper">ponna vennanu sonnein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponna vennanu sonnein"/>
</div>
<div class="lyrico-lyrics-wrapper">vennanu sonnein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vennanu sonnein"/>
</div>
<div class="lyrico-lyrics-wrapper">ponna vennanu sonnein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponna vennanu sonnein"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">seeli nikkan siya nikkan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seeli nikkan siya nikkan"/>
</div>
<div class="lyrico-lyrics-wrapper">seeli nikkan siya nikkan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seeli nikkan siya nikkan"/>
</div>
<div class="lyrico-lyrics-wrapper">seeli nikkan siya nikkan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seeli nikkan siya nikkan"/>
</div>
<div class="lyrico-lyrics-wrapper">seeyaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seeyaan"/>
</div>
<div class="lyrico-lyrics-wrapper">seeli nikkan siya nikkan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seeli nikkan siya nikkan"/>
</div>
<div class="lyrico-lyrics-wrapper">seeli nikkan siya nikkan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seeli nikkan siya nikkan"/>
</div>
<div class="lyrico-lyrics-wrapper">seeli nikkan siya nikkan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seeli nikkan siya nikkan"/>
</div>
<div class="lyrico-lyrics-wrapper">seeyaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seeyaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thoothukudi irukkankudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoothukudi irukkankudi"/>
</div>
<div class="lyrico-lyrics-wrapper">ittakudi puliyangudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ittakudi puliyangudi"/>
</div>
<div class="lyrico-lyrics-wrapper">arisikudi mannarkudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arisikudi mannarkudi"/>
</div>
<div class="lyrico-lyrics-wrapper">paramakudi sayalkudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paramakudi sayalkudi"/>
</div>
<div class="lyrico-lyrics-wrapper">puthuvaikudi sathirakudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthuvaikudi sathirakudi"/>
</div>
<div class="lyrico-lyrics-wrapper">mayakudi masinakudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mayakudi masinakudi"/>
</div>
<div class="lyrico-lyrics-wrapper">peyarukkulla kudi irukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="peyarukkulla kudi irukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">oorai ellam thedi parthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oorai ellam thedi parthen"/>
</div>
<div class="lyrico-lyrics-wrapper">enakku entha thanni vandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enakku entha thanni vandi"/>
</div>
<div class="lyrico-lyrics-wrapper">evanum illa pponnu kodukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="evanum illa pponnu kodukka"/>
</div>
<div class="lyrico-lyrics-wrapper">athuku hoi vennanu sonnein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athuku hoi vennanu sonnein"/>
</div>
<div class="lyrico-lyrics-wrapper">kalyanam vennanu sonnein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalyanam vennanu sonnein"/>
</div>
<div class="lyrico-lyrics-wrapper">vennanu sonnein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vennanu sonnein"/>
</div>
<div class="lyrico-lyrics-wrapper">kalyanam vennanu sonnein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalyanam vennanu sonnein"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">seeli nikkan siya nikkan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seeli nikkan siya nikkan"/>
</div>
<div class="lyrico-lyrics-wrapper">seeli nikkan siya nikkan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seeli nikkan siya nikkan"/>
</div>
<div class="lyrico-lyrics-wrapper">seeli nikkan siya nikkan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seeli nikkan siya nikkan"/>
</div>
<div class="lyrico-lyrics-wrapper">seeyaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seeyaan"/>
</div>
<div class="lyrico-lyrics-wrapper">seeli nikkan siya nikkan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seeli nikkan siya nikkan"/>
</div>
<div class="lyrico-lyrics-wrapper">seeli nikkan siya nikkan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seeli nikkan siya nikkan"/>
</div>
<div class="lyrico-lyrics-wrapper">seeli nikkan siya nikkan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seeli nikkan siya nikkan"/>
</div>
<div class="lyrico-lyrics-wrapper">seeyaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seeyaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kudikiravan onna sernthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kudikiravan onna sernthu"/>
</div>
<div class="lyrico-lyrics-wrapper">sangam onnu vaika venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sangam onnu vaika venum"/>
</div>
<div class="lyrico-lyrics-wrapper">kudimaganin kaeiveigala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kudimaganin kaeiveigala"/>
</div>
<div class="lyrico-lyrics-wrapper">onnu koodi dhinam pesa venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="onnu koodi dhinam pesa venum"/>
</div>
<div class="lyrico-lyrics-wrapper">kudichiputtu vetula than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kudichiputtu vetula than"/>
</div>
<div class="lyrico-lyrics-wrapper">prachana kichanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="prachana kichanai"/>
</div>
<div class="lyrico-lyrics-wrapper">panna venam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="panna venam"/>
</div>
<div class="lyrico-lyrics-wrapper">kudika neyum kaasu keta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kudika neyum kaasu keta"/>
</div>
<div class="lyrico-lyrics-wrapper">pichaiyun than eduka venam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pichaiyun than eduka venam"/>
</div>
<div class="lyrico-lyrics-wrapper">intha rendu kolgaiye thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha rendu kolgaiye thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">kadai picicha pothumada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadai picicha pothumada"/>
</div>
<div class="lyrico-lyrics-wrapper">oorukulla namaku kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oorukulla namaku kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">mathippu kidaikum purinjukoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mathippu kidaikum purinjukoda"/>
</div>
<div class="lyrico-lyrics-wrapper">ai injusuruvg kudikara sangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ai injusuruvg kudikara sangam"/>
</div>
<div class="lyrico-lyrics-wrapper">kudikara kolgai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kudikara kolgai"/>
</div>
<div class="lyrico-lyrics-wrapper">semma matteru sollitu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="semma matteru sollitu"/>
</div>
<div class="lyrico-lyrics-wrapper">un mater a vitiye maapla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un mater a vitiye maapla"/>
</div>
<div class="lyrico-lyrics-wrapper">ai sangam vara varaikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ai sangam vara varaikum"/>
</div>
<div class="lyrico-lyrics-wrapper">venam kalyanam enaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="venam kalyanam enaku"/>
</div>
<div class="lyrico-lyrics-wrapper">venam kalyanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="venam kalyanam"/>
</div>
<div class="lyrico-lyrics-wrapper">venam kalyanam enaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="venam kalyanam enaku"/>
</div>
<div class="lyrico-lyrics-wrapper">venam kalyanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="venam kalyanam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">seeli nikkan siya nikkan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seeli nikkan siya nikkan"/>
</div>
<div class="lyrico-lyrics-wrapper">seeli nikkan siya nikkan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seeli nikkan siya nikkan"/>
</div>
<div class="lyrico-lyrics-wrapper">seeli nikkan siya nikkan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seeli nikkan siya nikkan"/>
</div>
<div class="lyrico-lyrics-wrapper">seeyaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seeyaan"/>
</div>
<div class="lyrico-lyrics-wrapper">seeli nikkan siya nikkan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seeli nikkan siya nikkan"/>
</div>
<div class="lyrico-lyrics-wrapper">seeli nikkan siya nikkan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seeli nikkan siya nikkan"/>
</div>
<div class="lyrico-lyrics-wrapper">seeli nikkan siya nikkan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seeli nikkan siya nikkan"/>
</div>
<div class="lyrico-lyrics-wrapper">seeyaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seeyaan"/>
</div>
</pre>
