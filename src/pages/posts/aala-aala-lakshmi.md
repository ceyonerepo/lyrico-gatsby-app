---
title: "aala aala song lyrics"
album: "Lakshmi"
artist: "Sam CS"
lyricist: "Madhan Karky"
director: "AL Vijay"
path: "/albums/lakshmi-lyrics"
song: "Aala Aala"
image: ../../images/albumart/lakshmi.jpg
date: 2018-08-24
lang: tamil
youtubeLink: "https://www.youtube.com/embed/6IldC1pjjvM"
type: "happy"
singers:
  - GV Prakash Kumar
  - Saindhavi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aala aala vaanil yera vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aala aala vaanil yera vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vennilaavai moottai katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vennilaavai moottai katti"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannil kondu vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannil kondu vaa vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aala aala paranthu sella vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aala aala paranthu sella vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanavillai thaanda vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanavillai thaanda vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eera kaatrilae thooram sella vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eera kaatrilae thooram sella vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neela neelam thaanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neela neelam thaanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Paalam seiya vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paalam seiya vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mega medayil jodi sera vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mega medayil jodi sera vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Minnal vettil aada vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minnal vettil aada vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aala aala kaadhal kondadhaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aala aala kaadhal kondadhaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Thooram illai antha vennila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thooram illai antha vennila"/>
</div>
<div class="lyrico-lyrics-wrapper">Aala aala kaadhal kondadhaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aala aala kaadhal kondadhaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Inbam inbam kannil kannila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inbam inbam kannil kannila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa siru siraginil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa siru siraginil"/>
</div>
<div class="lyrico-lyrics-wrapper">Muzhu madhiyinai yendha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muzhu madhiyinai yendha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa unadhalaginil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa unadhalaginil"/>
</div>
<div class="lyrico-lyrics-wrapper">Yenadhazhaginai theenda vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenadhazhaginai theenda vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa uyar ulaginil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa uyar ulaginil"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir urasiyae aada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir urasiyae aada"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa mudiviliyinai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa mudiviliyinai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nodipozhudhinil thaanda vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nodipozhudhinil thaanda vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vinmeengal thaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinmeengal thaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolgal yaavum thaandi thaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolgal yaavum thaandi thaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Verroru boomi undaakkalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verroru boomi undaakkalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Megathil moodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megathil moodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kondu vandha vennilaavai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondu vandha vennilaavai"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhoosi thatti suththam seithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhoosi thatti suththam seithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal vaanil otti kollalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal vaanil otti kollalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aala aala kaadhal kondadhaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aala aala kaadhal kondadhaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Thooram illai antha vennila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thooram illai antha vennila"/>
</div>
<div class="lyrico-lyrics-wrapper">Aala aala kaadhal kondadhaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aala aala kaadhal kondadhaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Inbam inbam kannil kannila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inbam inbam kannil kannila"/>
</div>
</pre>
