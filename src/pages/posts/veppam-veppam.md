---
title: "veppam song lyrics"
album: "Veppam"
artist: "Joshua Sridhar"
lyricist: "Na. Muthukumar"
director: "Anjana"
path: "/albums/veppam-lyrics"
song: "Veppam"
image: ../../images/albumart/veppam.jpg
date: 2011-07-29
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ycHMeSMBhL4"
type: "mass"
singers:
  - Joshua Sridhar
  - Naresh Iyer
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Veppam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veppam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nerupilum Varum Veppam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nerupilum Varum Veppam"/>
</div>
<div class="lyrico-lyrics-wrapper">Neerilum Varum Veppam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neerilum Varum Veppam"/>
</div>
<div class="lyrico-lyrics-wrapper">Katrilum Varum Veppam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katrilum Varum Veppam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhidhanadhidhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhidhanadhidhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rattham Kodhikayil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rattham Kodhikayil"/>
</div>
<div class="lyrico-lyrics-wrapper">Veppam Azhaikalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veppam Azhaikalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veppam Azhaikayil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veppam Azhaikayil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kutram Nadakalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kutram Nadakalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kutram Nadakayil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kutram Nadakayil"/>
</div>
<div class="lyrico-lyrics-wrapper">Dharmam Marakalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dharmam Marakalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dharmam Marakayil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dharmam Marakayil"/>
</div>
<div class="lyrico-lyrics-wrapper">Veppam Thurathalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veppam Thurathalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veppam Thurathinal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veppam Thurathinal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhkai Nadungalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkai Nadungalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhkai Nadunginal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkai Nadunginal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjam Kalangalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjam Kalangalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjam Kalanginal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjam Kalanginal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nyayam Puriyalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nyayam Puriyalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nyayam Purigayil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nyayam Purigayil"/>
</div>
<div class="lyrico-lyrics-wrapper">Veppam Kodhikalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veppam Kodhikalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veppam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veppam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nerupilum Varum Veppam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nerupilum Varum Veppam"/>
</div>
<div class="lyrico-lyrics-wrapper">Neerilum Varum Veppam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neerilum Varum Veppam"/>
</div>
<div class="lyrico-lyrics-wrapper">Katrilum Varum Veppam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katrilum Varum Veppam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhidhanadhidhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhidhanadhidhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rattham Kodhikayil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rattham Kodhikayil"/>
</div>
<div class="lyrico-lyrics-wrapper">Veppam Azhaikalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veppam Azhaikalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veppam Azhaikayil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veppam Azhaikayil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kutram Nadakalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kutram Nadakalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kutram Nadakayil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kutram Nadakayil"/>
</div>
<div class="lyrico-lyrics-wrapper">Dharmam Marakalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dharmam Marakalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dharmam Marakayil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dharmam Marakayil"/>
</div>
<div class="lyrico-lyrics-wrapper">Veppam Thurathalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veppam Thurathalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veppam Thurathinal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veppam Thurathinal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhkai Nadungalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkai Nadungalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhkai Nadunginal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkai Nadunginal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjam Kalangalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjam Kalangalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjam Kalanginal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjam Kalanginal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nyayam Puriyalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nyayam Puriyalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nyayam Purigayil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nyayam Purigayil"/>
</div>
<div class="lyrico-lyrics-wrapper">Veppam Kodhikalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veppam Kodhikalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veppam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veppam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nerupilum Varum Veppam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nerupilum Varum Veppam"/>
</div>
<div class="lyrico-lyrics-wrapper">Neerilum Varum Veppam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neerilum Varum Veppam"/>
</div>
<div class="lyrico-lyrics-wrapper">Katrilum Varum Veppam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katrilum Varum Veppam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhidhanadhidhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhidhanadhidhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rattham Kodhikayil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rattham Kodhikayil"/>
</div>
<div class="lyrico-lyrics-wrapper">Veppam Azhaikalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veppam Azhaikalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veppam Azhaikayil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veppam Azhaikayil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kutram Nadakalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kutram Nadakalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kutram Nadakayil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kutram Nadakayil"/>
</div>
<div class="lyrico-lyrics-wrapper">Dharmam Marakalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dharmam Marakalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dharmam Marakayil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dharmam Marakayil"/>
</div>
<div class="lyrico-lyrics-wrapper">Veppam Thurathalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veppam Thurathalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veppam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veppam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nerupilum Varum Veppam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nerupilum Varum Veppam"/>
</div>
<div class="lyrico-lyrics-wrapper">Neerilum Varum Veppam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neerilum Varum Veppam"/>
</div>
<div class="lyrico-lyrics-wrapper">Katrilum Varum Veppam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katrilum Varum Veppam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhidhanadhidhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhidhanadhidhu"/>
</div>
</pre>
