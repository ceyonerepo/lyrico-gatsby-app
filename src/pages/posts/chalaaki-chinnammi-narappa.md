---
title: "chalaaki chinnammi song lyrics"
album: "Narappa"
artist: "Mani Sharma"
lyricist: "Ananta Sriram"
director: "Srikanth Addala"
path: "/albums/narappa-lyrics"
song: "Chalaaki Chinnammi"
image: ../../images/albumart/narappa.jpg
date: 2021-07-20
lang: telugu
youtubeLink: "https://www.youtube.com/embed/KsFHw-uK5ik"
type: "happy"
singers:
  - Aditya Iyengar
  - Nutana Mohan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Thandhane naanenenaa nana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandhane naanenenaa nana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thandhane naanenenaa nana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandhane naanenenaa nana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thandhane naanenenaa nana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandhane naanenenaa nana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thandhane naanenenaa nana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandhane naanenenaa nana"/>
</div>
<div class="lyrico-lyrics-wrapper">Chilipi choopula chalaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chilipi choopula chalaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinnammi chalaki chinnammi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinnammi chalaki chinnammi"/>
</div>
<div class="lyrico-lyrics-wrapper">Yelage ninnadichi elagey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yelage ninnadichi elagey "/>
</div>
<div class="lyrico-lyrics-wrapper">undedhi chalaki chinnammi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="undedhi chalaki chinnammi"/>
</div>
<div class="lyrico-lyrics-wrapper">Raagi muddhave nuvveraani kaaramu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raagi muddhave nuvveraani kaaramu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenelaage ninnidichi elage 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenelaage ninnidichi elage "/>
</div>
<div class="lyrico-lyrics-wrapper">undedhi chalaki chinnammi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="undedhi chalaki chinnammi"/>
</div>
<div class="lyrico-lyrics-wrapper">Raana neethoti ilage ninu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raana neethoti ilage ninu "/>
</div>
<div class="lyrico-lyrics-wrapper">nammi ilage ninnu nammo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nammi ilage ninnu nammo"/>
</div>
<div class="lyrico-lyrics-wrapper">Raalaseeminti oyaret vannello
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raalaseeminti oyaret vannello"/>
</div>
<div class="lyrico-lyrics-wrapper">Varaley virajimmi varaley virajimmi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varaley virajimmi varaley virajimmi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kindhalennaina chesthane neetho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kindhalennaina chesthane neetho"/>
</div>
<div class="lyrico-lyrics-wrapper">Unde noorellu chudale entho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unde noorellu chudale entho"/>
</div>
<div class="lyrico-lyrics-wrapper">Regadi nenaithey naagali nee navve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Regadi nenaithey naagali nee navve"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhunnithe pandale naa panta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhunnithe pandale naa panta"/>
</div>
<div class="lyrico-lyrics-wrapper">Manche kattaloy eedu polamlo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manche kattaloy eedu polamlo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanche tenchaloy kanne kalallo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanche tenchaloy kanne kalallo"/>
</div>
<div class="lyrico-lyrics-wrapper">Anche cheraloy koka chivarlo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anche cheraloy koka chivarlo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanchai mogaloy raika konallo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanchai mogaloy raika konallo"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaalo yaala kantelai kaayala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaalo yaala kantelai kaayala"/>
</div>
<div class="lyrico-lyrics-wrapper">Kshanale ee vela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kshanale ee vela"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu nen thoyyala jathai mooseyyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu nen thoyyala jathai mooseyyala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kandhi chelona joreegallaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandhi chelona joreegallaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Jpdai egireddam raaye saradhaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jpdai egireddam raaye saradhaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Vemana avataram ennade bangaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vemana avataram ennade bangaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Annadhi naa athram bharanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annadhi naa athram bharanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Challe chalabbi sambadamitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Challe chalabbi sambadamitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Lagga lekunda sandhaduletta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lagga lekunda sandhaduletta"/>
</div>
<div class="lyrico-lyrics-wrapper">Neekai daachane pallamu mitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neekai daachane pallamu mitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvve dhaatey naa siggula katta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvve dhaatey naa siggula katta"/>
</div>
<div class="lyrico-lyrics-wrapper">Pillagaale pichhiga oogaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pillagaale pichhiga oogaley"/>
</div>
<div class="lyrico-lyrics-wrapper">Pee pee pee dumdumle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pee pee pee dumdumle"/>
</div>
<div class="lyrico-lyrics-wrapper">Pee pee pee dumdumle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pee pee pee dumdumle"/>
</div>
<div class="lyrico-lyrics-wrapper">Pee pee pee dumdumle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pee pee pee dumdumle"/>
</div>
</pre>
