---
title: "kaki nemali ke votu song lyrics"
album: "Anubhavinchu Raja"
artist: "Gopi Sundar"
lyricist: "Bhaskarabhatla"
director: "Sreenu Gavireddy"
path: "/albums/anubhavinchu-raja-lyrics"
song: "Kaki Nemali Ke Votu"
image: ../../images/albumart/anubhavinchu-raja.jpg
date: 2021-11-26
lang: telugu
youtubeLink: "https://www.youtube.com/embed/Y7zhP1qAS9Q"
type: "happy"
singers:
  - Roll Rida
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nilabadi Haami Isthunnaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilabadi Haami Isthunnaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adagani Promise Chesthunnaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adagani Promise Chesthunnaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adigithe Pranam Pettesthaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adigithe Pranam Pettesthaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gathukula Brathukulu Maarchesthaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gathukula Brathukulu Maarchesthaadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Online Lone Untaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Online Lone Untaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">What Can I Do Antaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="What Can I Do Antaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Otami Erugani Kurraadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Otami Erugani Kurraadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vote La Kosam Vachhaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vote La Kosam Vachhaadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ladies Ki Antha Hard Work Vaddhani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ladies Ki Antha Hard Work Vaddhani"/>
</div>
<div class="lyrico-lyrics-wrapper">Icchesthadu Washing Machine
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Icchesthadu Washing Machine"/>
</div>
<div class="lyrico-lyrics-wrapper">Chethulu Noppedathaayani Cheppi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chethulu Noppedathaayani Cheppi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thecchisthaadu Grinding Machine
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thecchisthaadu Grinding Machine"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pelleedu Ochhina Paapala Kosam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pelleedu Ochhina Paapala Kosam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pamisthadu Make-up Kit
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pamisthadu Make-up Kit"/>
</div>
<div class="lyrico-lyrics-wrapper">Serials Choose Aunty La Kosam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Serials Choose Aunty La Kosam"/>
</div>
<div class="lyrico-lyrics-wrapper">Gift Isthaadu TV Set
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gift Isthaadu TV Set"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaki Nemali Ke Votu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaki Nemali Ke Votu"/>
</div>
<div class="lyrico-lyrics-wrapper">Meeku Undadhe Lotu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meeku Undadhe Lotu"/>
</div>
<div class="lyrico-lyrics-wrapper">Veedikesthe Mee Votu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veedikesthe Mee Votu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaruthaadhi Mee Fate-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaruthaadhi Mee Fate-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tharugu Ledhu Aa Bangaraaniki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharugu Ledhu Aa Bangaraaniki"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirugu Ledhu Ee Bangaraaniki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirugu Ledhu Ee Bangaraaniki"/>
</div>
<div class="lyrico-lyrics-wrapper">Aalochisthaarinka Dheniki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalochisthaarinka Dheniki"/>
</div>
<div class="lyrico-lyrics-wrapper">Guddhi Padeddhaam Kaki Nemali ki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Guddhi Padeddhaam Kaki Nemali ki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Randi Randi Randi Tharali Randi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Randi Randi Randi Tharali Randi"/>
</div>
<div class="lyrico-lyrics-wrapper">Randi Randi Randi Kadhali Randi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Randi Randi Randi Kadhali Randi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaki Nemali Ki Kaki Nemali 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaki Nemali Ki Kaki Nemali "/>
</div>
<div class="lyrico-lyrics-wrapper">Ki Kaki Nemali Ki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ki Kaki Nemali Ki"/>
</div>
<div class="lyrico-lyrics-wrapper">Randi Randi Randi Randi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Randi Randi Randi Randi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaki Nemali Ki Kaki Nemali Ki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaki Nemali Ki Kaki Nemali Ki"/>
</div>
</pre>
