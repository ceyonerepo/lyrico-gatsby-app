---
title: "nee nakhasikhale song lyrics"
album: "RDX Love"
artist: "Radhan"
lyricist: "Bhaskar Bhatla"
director: "Bhanu Shankar"
path: "/albums/rdx-love-lyrics"
song: "Nee Nakhasikhale"
image: ../../images/albumart/rdx-love.jpg
date: 2019-10-11
lang: telugu
youtubeLink: "https://www.youtube.com/embed/OaF65TT9czw"
type: "love"
singers:
  - Anudeep Dev
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">nee nakhasikhale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee nakhasikhale"/>
</div>
<div class="lyrico-lyrics-wrapper">naranaranamo melike vesaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naranaranamo melike vesaye"/>
</div>
<div class="lyrico-lyrics-wrapper">nee niganigale 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee niganigale "/>
</div>
<div class="lyrico-lyrics-wrapper">bhagabhagalai pranam teesaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bhagabhagalai pranam teesaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">tiyyani pedavvule tenele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tiyyani pedavvule tenele"/>
</div>
<div class="lyrico-lyrics-wrapper">oorina baavi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oorina baavi"/>
</div>
<div class="lyrico-lyrics-wrapper">teraga padavito
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="teraga padavito"/>
</div>
<div class="lyrico-lyrics-wrapper">takshanam todeyyali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="takshanam todeyyali"/>
</div>
<div class="lyrico-lyrics-wrapper">rechha gotte gaju kallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rechha gotte gaju kallu"/>
</div>
<div class="lyrico-lyrics-wrapper">pitich yekke mogali valloo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pitich yekke mogali valloo"/>
</div>
<div class="lyrico-lyrics-wrapper">andame aggilaa vunde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="andame aggilaa vunde"/>
</div>
<div class="lyrico-lyrics-wrapper">aajyame poyya mantonde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aajyame poyya mantonde"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">gotithoti gundepainaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gotithoti gundepainaa"/>
</div>
<div class="lyrico-lyrics-wrapper">pachchabottu veyyanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pachchabottu veyyanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">mudduthoti veepumeeaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudduthoti veepumeeaa"/>
</div>
<div class="lyrico-lyrics-wrapper">paddulenno rayanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paddulenno rayanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">sutipoti chuupule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sutipoti chuupule"/>
</div>
<div class="lyrico-lyrics-wrapper">eetelaga guchanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eetelaga guchanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">korameesam kodavalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="korameesam kodavalai"/>
</div>
<div class="lyrico-lyrics-wrapper">dora siggulu tempanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dora siggulu tempanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">meda vampu vedhullo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meda vampu vedhullo"/>
</div>
<div class="lyrico-lyrics-wrapper">sega sudulu tippisthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sega sudulu tippisthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">naduvampu vaagullo naagai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naduvampu vaagullo naagai"/>
</div>
<div class="lyrico-lyrics-wrapper">chemata pattisthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chemata pattisthan"/>
</div>
<div class="lyrico-lyrics-wrapper">nallani kurulatho 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nallani kurulatho "/>
</div>
<div class="lyrico-lyrics-wrapper">cheekatai chikate
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cheekatai chikate"/>
</div>
<div class="lyrico-lyrics-wrapper">korike minugurai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="korike minugurai "/>
</div>
<div class="lyrico-lyrics-wrapper">daarine chupisthonde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="daarine chupisthonde"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">cheera madathala adavilona 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cheera madathala adavilona "/>
</div>
<div class="lyrico-lyrics-wrapper">vetagannai tiraganaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetagannai tiraganaa"/>
</div>
<div class="lyrico-lyrics-wrapper">chengu chenguna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chengu chenguna"/>
</div>
<div class="lyrico-lyrics-wrapper">parugulette sogasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="parugulette sogasu"/>
</div>
<div class="lyrico-lyrics-wrapper">jinkani pattanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jinkani pattanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">manchu kurise velalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manchu kurise velalo"/>
</div>
<div class="lyrico-lyrics-wrapper">anchanaale penchanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anchanaale penchanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">kanche laanti kougilai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanche laanti kougilai"/>
</div>
<div class="lyrico-lyrics-wrapper">inchi inchi chuttanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inchi inchi chuttanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">agarotthu paruvamlo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="agarotthu paruvamlo"/>
</div>
<div class="lyrico-lyrics-wrapper">chumaghumalu dochesthaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chumaghumalu dochesthaa "/>
</div>
<div class="lyrico-lyrics-wrapper">sarikotta matthulo mottam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sarikotta matthulo mottam"/>
</div>
<div class="lyrico-lyrics-wrapper">munakaleisthaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munakaleisthaa "/>
</div>
<div class="lyrico-lyrics-wrapper">acchuluu halluluu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="acchuluu halluluu"/>
</div>
<div class="lyrico-lyrics-wrapper">koodikaa teesivethaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koodikaa teesivethaa"/>
</div>
<div class="lyrico-lyrics-wrapper">pacchigaa picchigaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pacchigaa picchigaa"/>
</div>
<div class="lyrico-lyrics-wrapper">vecchagaa nerpinchesthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vecchagaa nerpinchesthaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nee nakhasikhale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee nakhasikhale"/>
</div>
<div class="lyrico-lyrics-wrapper">naranaranamo melike vesaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naranaranamo melike vesaye"/>
</div>
<div class="lyrico-lyrics-wrapper">nee niganigale 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee niganigale "/>
</div>
<div class="lyrico-lyrics-wrapper">bhagabhagalai pranam teesaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bhagabhagalai pranam teesaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">tiyyani pedavvule tenele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tiyyani pedavvule tenele"/>
</div>
<div class="lyrico-lyrics-wrapper">oorina baavi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oorina baavi"/>
</div>
<div class="lyrico-lyrics-wrapper">teraga padavito
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="teraga padavito"/>
</div>
<div class="lyrico-lyrics-wrapper">takshanam todeyyali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="takshanam todeyyali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">tiyyani pedavvule tenele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tiyyani pedavvule tenele"/>
</div>
<div class="lyrico-lyrics-wrapper">oorina baavi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oorina baavi"/>
</div>
<div class="lyrico-lyrics-wrapper">teraga padavito
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="teraga padavito"/>
</div>
<div class="lyrico-lyrics-wrapper">takshanam todeyyali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="takshanam todeyyali"/>
</div>
</pre>
