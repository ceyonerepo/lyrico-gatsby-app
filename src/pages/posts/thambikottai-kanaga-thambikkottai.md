---
title: "thambikottai kanaga song lyrics"
album: "Thambikkottai"
artist: "D. Imman"
lyricist: "Viveka"
director: "R.Rahesh"
path: "/albums/thambikkottai-lyrics"
song: "Thambikottai Kanaga"
image: ../../images/albumart/thambikkottai.jpg
date: 2011-02-11
lang: tamil
youtubeLink: "https://www.youtube.com/embed/G5P9gCp5ut8"
type: "love"
singers:
  - Jassie Gift
  - Sujatha Mohan
  - Sharujan
  - Prayut Chan-o-cha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Thambikkoattai kanagaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thambikkoattai kanagaa "/>
</div>
<div class="lyrico-lyrics-wrapper">thaalikkatta varavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaalikkatta varavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pattukkoattai kirukkaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattukkoattai kirukkaa"/>
</div>
<div class="lyrico-lyrics-wrapper">innum davuttaa irukkaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="innum davuttaa irukkaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un paechu nellikaa un kannam berikkaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un paechu nellikaa un kannam berikkaa"/>
</div>
<div class="lyrico-lyrics-wrapper">nee selaikkattum naattu sarakkaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee selaikkattum naattu sarakkaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un kannu kalaakkaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kannu kalaakkaa"/>
</div>
<div class="lyrico-lyrics-wrapper">un paarvai palaakkaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un paarvai palaakkaa"/>
</div>
<div class="lyrico-lyrics-wrapper">enaikkuththuriye kaththikkanakkaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enaikkuththuriye kaththikkanakkaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi thaadi thaadi pullakkutti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi thaadi thaadi pullakkutti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna koottittu poadaa thaalikkatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna koottittu poadaa thaalikkatti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi thaadi thaadi pullakkutti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi thaadi thaadi pullakkutti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna koottittu poadaa thaalikkatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna koottittu poadaa thaalikkatti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thambikkoattai kanagaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thambikkoattai kanagaa "/>
</div>
<div class="lyrico-lyrics-wrapper">thaalikkatta varavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaalikkatta varavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pattukkoattai kirukkaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattukkoattai kirukkaa"/>
</div>
<div class="lyrico-lyrics-wrapper">innum davuttaa irukkaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="innum davuttaa irukkaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi thandhana thandhana thaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi thandhana thandhana thaanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi thandhana thandhana thaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi thandhana thandhana thaanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi thandhana thandhana thaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi thandhana thandhana thaanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi thandhana thandhana thaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi thandhana thandhana thaanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">ada thandhaa thandhana thandhaa thandhana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada thandhaa thandhana thandhaa thandhana"/>
</div>
<div class="lyrico-lyrics-wrapper">thandhaa thandhana thaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thandhaa thandhana thaanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haa maattu vandi moottikkittu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa maattu vandi moottikkittu "/>
</div>
<div class="lyrico-lyrics-wrapper">pannaivayal poalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pannaivayal poalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">aalamaram adiyila ha ha ha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aalamaram adiyila ha ha ha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oorukkaaran paarththupputtaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorukkaaran paarththupputtaa "/>
</div>
<div class="lyrico-lyrics-wrapper">uppukkandam thaanyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uppukkandam thaanyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">vaelaiya neeppaarthukkittu po po po
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaelaiya neeppaarthukkittu po po po"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aruvaa puruvakkaari manasa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aruvaa puruvakkaari manasa "/>
</div>
<div class="lyrico-lyrics-wrapper">erakkippoagum Lorry
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="erakkippoagum Lorry"/>
</div>
<div class="lyrico-lyrics-wrapper">onakkey ennaikkoduthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="onakkey ennaikkoduthen"/>
</div>
<div class="lyrico-lyrics-wrapper">vachchu onzhungaa paarththukkoadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vachchu onzhungaa paarththukkoadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada paththikkittaa vaikkappoara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada paththikkittaa vaikkappoara"/>
</div>
<div class="lyrico-lyrics-wrapper">pakkam vandhu nikkiriye 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pakkam vandhu nikkiriye "/>
</div>
<div class="lyrico-lyrics-wrapper">noaiyellaam seivey sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="noaiyellaam seivey sollu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi thaadi thaadi pullakkutti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi thaadi thaadi pullakkutti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna koottittu poadaa thaalikkatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna koottittu poadaa thaalikkatti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi thaadi thaadi pullakkutti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi thaadi thaadi pullakkutti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna koottittu poadaa thaalikkatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna koottittu poadaa thaalikkatti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thambikkoattai kanagaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thambikkoattai kanagaa "/>
</div>
<div class="lyrico-lyrics-wrapper">thaalikkatta varavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaalikkatta varavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pattukkoattai kirukkaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattukkoattai kirukkaa"/>
</div>
<div class="lyrico-lyrics-wrapper">innum davuttaa irukkaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="innum davuttaa irukkaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hei vaelai vetti ellaam vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei vaelai vetti ellaam vittu"/>
</div>
<div class="lyrico-lyrics-wrapper">onna suththurendi aala 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="onna suththurendi aala "/>
</div>
<div class="lyrico-lyrics-wrapper">vetti vittu vittu poagaadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetti vittu vittu poagaadhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Melangotti kaalil metti 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Melangotti kaalil metti "/>
</div>
<div class="lyrico-lyrics-wrapper">maatturadhuvaraikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maatturadhuvaraikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">mela kaiyappoaduradhu aagaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mela kaiyappoaduradhu aagaadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karumbaa valarndha pulla 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karumbaa valarndha pulla "/>
</div>
<div class="lyrico-lyrics-wrapper">thanuvaa thottaa enna tholla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanuvaa thottaa enna tholla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thirudaa konjam porudaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirudaa konjam porudaa "/>
</div>
<div class="lyrico-lyrics-wrapper">enna muzusaa thandhiruren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna muzusaa thandhiruren"/>
</div>
<div class="lyrico-lyrics-wrapper">ennai genji genji kettaakkooda konjammaatten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennai genji genji kettaakkooda konjammaatten"/>
</div>
<div class="lyrico-lyrics-wrapper">ippoa onna naan onnum bommai illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ippoa onna naan onnum bommai illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi thaadi thaadi pullakkutti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi thaadi thaadi pullakkutti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna koottittu poadaa thaalikkatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna koottittu poadaa thaalikkatti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thambikkoattai kanagaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thambikkoattai kanagaa "/>
</div>
<div class="lyrico-lyrics-wrapper">thaalikkatta varavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaalikkatta varavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pattukkoattai kirukkaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattukkoattai kirukkaa"/>
</div>
<div class="lyrico-lyrics-wrapper">innum davuttaa irukkaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="innum davuttaa irukkaa"/>
</div>
</pre>
