---
title: "netru illai illai song lyrics"
album: "Thambi Vettothi Sundaram"
artist: "Vidyasagar"
lyricist: "Vairamuthu"
director: "V.C. Vadivudaiyan"
path: "/albums/thambi-vettothi-sundaram-lyrics"
song: "Netru Illai Illai"
image: ../../images/albumart/thambi-vettothi-sundaram.jpg
date: 2011-11-11
lang: tamil
youtubeLink: 
type: "happy"
singers:
  - Madhu Balakrishnan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Naetru illai illai illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naetru illai illai illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha aanandham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha aanandham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanamellaam nilavaai maarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanamellaam nilavaai maarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhvil paerinbam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvil paerinbam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aiyo en aasai ullam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyo en aasai ullam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollaamal aadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollaamal aadum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pollaadha naanam kondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pollaadha naanam kondu"/>
</div>
<div class="lyrico-lyrics-wrapper">En kannai moodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kannai moodum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poiyaana naanam vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poiyaana naanam vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Meiyaana aasai solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meiyaana aasai solla"/>
</div>
<div class="lyrico-lyrics-wrapper">En kangal un vaasal thaedum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kangal un vaasal thaedum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En kaigal thaazhppaalai podum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kaigal thaazhppaalai podum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naetru illai illai illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naetru illai illai illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha aanandham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha aanandham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanamellaam nilavaai maarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanamellaam nilavaai maarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhvil paerinbam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvil paerinbam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Venn maegam saerththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venn maegam saerththu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru pon pandhal amaippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru pon pandhal amaippom"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaan meenaiyum thaen kaattraiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaan meenaiyum thaen kaattraiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa vaazhththa azhaippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa vaazhththa azhaippom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaaippulla naeram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaippulla naeram"/>
</div>
<div class="lyrico-lyrics-wrapper">Nam vaai thaenil kulippoam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam vaai thaenil kulippoam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaivin thuli theerum varai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaivin thuli theerum varai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa vaazhndhu mudippoam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa vaazhndhu mudippoam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaedhanai theeyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaedhanai theeyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaegudhae mogamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaegudhae mogamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaervaiyin aattrilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaervaiyin aattrilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Neendhinaal moatchamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neendhinaal moatchamae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inbamaana thunbam ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inbamaana thunbam ennai"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam konjam kolludhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam konjam kolludhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naetru illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naetru illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha aanandham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha aanandham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaai oorumpodhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaai oorumpodhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Siru thaen oora vaendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru thaen oora vaendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee thoongavae en koondhalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thoongavae en koondhalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Paai poda vaendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paai poda vaendum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En kaadhal baaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kaadhal baaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi nee thaanga vaendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi nee thaanga vaendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un maenikkul en aasaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un maenikkul en aasaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaeroada vaendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaeroada vaendum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kattilai paarththadhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattilai paarththadhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattalai poduvaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattalai poduvaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vettrilai poattu nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vettrilai poattu nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaiyae melluvaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaiyae melluvaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thoarppadhaaga solli kondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoarppadhaaga solli kondu"/>
</div>
<div class="lyrico-lyrics-wrapper">Niththam ennai velluvaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niththam ennai velluvaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naetru illai illai illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naetru illai illai illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha aanandham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha aanandham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanamellaam nilavaai maarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanamellaam nilavaai maarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhvil paerinbam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvil paerinbam"/>
</div>
</pre>
