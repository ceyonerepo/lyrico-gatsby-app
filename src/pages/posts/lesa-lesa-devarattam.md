---
title: "lesa lesa song lyrics"
album: "Devarattam"
artist: "Nivas K. Prasanna"
lyricist: "Vivek"
director: "M. Muthaiah"
path: "/albums/devarattam-lyrics"
song: "Lesa Lesa"
image: ../../images/albumart/devarattam.jpg
date: 2019-05-01
lang: tamil
youtubeLink: "https://www.youtube.com/embed/PC8xAWbPcGc"
type: "love"
singers:
  - Chinmayi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Lesa Lesa Lesa Lesa Hoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lesa Lesa Lesa Lesa Hoi"/>
</div>
<div class="lyrico-lyrics-wrapper">Lesa Pesa Aasai Lesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lesa Pesa Aasai Lesa"/>
</div>
<div class="lyrico-lyrics-wrapper">Lesa Lesa Lesa Lesa Hoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lesa Lesa Lesa Lesa Hoi"/>
</div>
<div class="lyrico-lyrics-wrapper">Lesa Yesa Aasai Lesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lesa Yesa Aasai Lesa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pesa Pesa Un Kuda Pesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesa Pesa Un Kuda Pesa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasai Mela Aasai Aasai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai Mela Aasai Aasai"/>
</div>
<div class="lyrico-lyrics-wrapper">Veesa Veesa Paarvai Nee Veesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veesa Veesa Paarvai Nee Veesa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum Pesa Yedhu Baashai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum Pesa Yedhu Baashai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Orangaama Kadhai Pesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orangaama Kadhai Pesa"/>
</div>
<div class="lyrico-lyrics-wrapper">Alungaama Yedha Pesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alungaama Yedha Pesa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaarthai Venam Kangal Pesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaarthai Venam Kangal Pesa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lesa Lesa Lesa Lesa Hoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lesa Lesa Lesa Lesa Hoi"/>
</div>
<div class="lyrico-lyrics-wrapper">Lesa Pesa Aasai Lesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lesa Pesa Aasai Lesa"/>
</div>
<div class="lyrico-lyrics-wrapper">Lesa Lesa Lesa Lesa Hoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lesa Lesa Lesa Lesa Hoi"/>
</div>
<div class="lyrico-lyrics-wrapper">Lesa Yesa Aasai Lesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lesa Yesa Aasai Lesa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaana Nadakkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaana Nadakkuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaniya Sirikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaniya Sirikkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaaththa Parakkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaththa Parakkuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaana Poguren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaana Poguren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poova Viriyiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poova Viriyiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Noola Neliyiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Noola Neliyiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Thootha Anuppuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thootha Anuppuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoola Norunguren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoola Norunguren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Padhiyam Pottu Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padhiyam Pottu Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenappa Valakkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenappa Valakkuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Suliyam Pola Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suliyam Pola Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Valanju Poguren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valanju Poguren"/>
</div>
<div class="lyrico-lyrics-wrapper">Naana Kolamburen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naana Kolamburen"/>
</div>
<div class="lyrico-lyrics-wrapper">Naana Theliyuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naana Theliyuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Mattum Usura Varaiyiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Mattum Usura Varaiyiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lesa Lesa Lesa Lesa Hoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lesa Lesa Lesa Lesa Hoi"/>
</div>
<div class="lyrico-lyrics-wrapper">Lesa Pesa Aasai Lesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lesa Pesa Aasai Lesa"/>
</div>
<div class="lyrico-lyrics-wrapper">Lesa Lesa Lesa Lesa Hoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lesa Lesa Lesa Lesa Hoi"/>
</div>
<div class="lyrico-lyrics-wrapper">Lesa Yesa Aasai Lesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lesa Yesa Aasai Lesa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Orangaama Kadhai Pesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orangaama Kadhai Pesa"/>
</div>
<div class="lyrico-lyrics-wrapper">Alungaama Yedha Pesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alungaama Yedha Pesa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaarthai Venam Kangal Pesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaarthai Venam Kangal Pesa"/>
</div>
</pre>
