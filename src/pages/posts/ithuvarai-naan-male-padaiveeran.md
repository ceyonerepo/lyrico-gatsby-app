---
title: "ithuvarai naan male song lyrics"
album: "Padaiveeran"
artist: "Karthik Raja"
lyricist: "Dhana Sekaran"
director: "Dhana Sekaran"
path: "/albums/padaiveeran-lyrics"
song: "Ithuvarai Naan Male"
image: ../../images/albumart/padaiveeran.jpg
date: 2018-02-02
lang: tamil
youtubeLink: "https://www.youtube.com/embed/YtdhvXgP-z0"
type: "sad"
singers:
  - Yuvan Shankar Raja
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Unakennadi unakennadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakennadi unakennadi"/>
</div>
<div class="lyrico-lyrics-wrapper">En uyirthaan thadumaaruthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En uyirthaan thadumaaruthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini ennadi ini ennadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini ennadi ini ennadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kelvigal aayiram soozhuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelvigal aayiram soozhuthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Munnillaa yedho ondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munnillaa yedho ondru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai thaakkum inneram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai thaakkum inneram"/>
</div>
<div class="lyrico-lyrics-wrapper">Yethotho aanathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethotho aanathae"/>
</div>
<div class="lyrico-lyrics-wrapper">En kaadhal ponathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kaadhal ponathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Netrintha sogam illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netrintha sogam illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Indru enna aanathendru sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indru enna aanathendru sollu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ithuvarai naan idhupol illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithuvarai naan idhupol illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini yenna naan seiven ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini yenna naan seiven ennai"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinam sirithaai nijamaa illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam sirithaai nijamaa illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai udaithaai nimmathi illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai udaithaai nimmathi illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaar yenna seithaar unnai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar yenna seithaar unnai"/>
</div>
<div class="lyrico-lyrics-wrapper">Poi solli vendraai ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poi solli vendraai ennai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee nee nee nee ee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee nee nee nee ee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muzhuvathumaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muzhuvathumaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai ninaithen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai ninaithen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai maranthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai maranthaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir thudithen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir thudithen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ohh hooo hooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ohh hooo hooooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vithi ariyaamalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vithi ariyaamalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhigalil vizhuntha naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhigalil vizhuntha naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Un sathi thalaamalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un sathi thalaamalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalellaam udaigiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalellaam udaigiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thisai theriyaamalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thisai theriyaamalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhigalai tholaitha naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhigalai tholaitha naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam aaraamalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam aaraamalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennullae thavikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennullae thavikkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Podhum poigal podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhum poigal podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Podhum vanjam podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhum vanjam podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Podhum vaazhkai podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhum vaazhkai podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Po po nee po
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Po po nee po"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muzhuvathumaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muzhuvathumaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai ninaithen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai ninaithen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai maranthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai maranthaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir thudithen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir thudithen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee illamalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee illamalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhuvarai selvathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhuvarai selvathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai ennaamalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai ennaamalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppadi vaazhvathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppadi vaazhvathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhi seramalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhi seramalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilagiyaa povathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilagiyaa povathu"/>
</div>
<div class="lyrico-lyrics-wrapper">En viral korkaamalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En viral korkaamalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalliyaa nirapathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalliyaa nirapathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeno vaanam maraiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeno vaanam maraiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeno thagam niraiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeno thagam niraiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhaiyaai ennai sera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhaiyaai ennai sera"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa vaa nee vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa vaa nee vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muzhuvathumaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muzhuvathumaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai ninaithen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai ninaithen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai maranthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai maranthaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir thudithen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir thudithen"/>
</div>
</pre>
