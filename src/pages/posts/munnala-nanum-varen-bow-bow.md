---
title: "munnala nanum varen song lyrics"
album: "Bow Bow"
artist: "Marc D Muse - Denis Vallaban. A"
lyricist: "Muthamil - Manju - Shiva - Rahul Ghandi"
director: "S. Pradeep Kilikar"
path: "/albums/bow-bow-lyrics"
song: "Munnala Nanum Varen"
image: ../../images/albumart/bow-bow.jpg
date: 2019-10-18
lang: tamil
youtubeLink: "https://www.youtube.com/embed/esA2yrKlbSM"
type: "happy"
singers:
  - Jeffrey George Biju
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">munnala nanum vara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munnala nanum vara"/>
</div>
<div class="lyrico-lyrics-wrapper">pinnala neeyum vara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pinnala neeyum vara"/>
</div>
<div class="lyrico-lyrics-wrapper">ellame koodi vara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellame koodi vara"/>
</div>
<div class="lyrico-lyrics-wrapper">enaku unai pudikuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enaku unai pudikuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">pollatha neram ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pollatha neram ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">illama poiduchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="illama poiduchu"/>
</div>
<div class="lyrico-lyrics-wrapper">sollama serthu vacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sollama serthu vacha"/>
</div>
<div class="lyrico-lyrics-wrapper">kanavugal palikuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavugal palikuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">munnala nanum vara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munnala nanum vara"/>
</div>
<div class="lyrico-lyrics-wrapper">pinnala neeyum vara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pinnala neeyum vara"/>
</div>
<div class="lyrico-lyrics-wrapper">ellame koodi vara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellame koodi vara"/>
</div>
<div class="lyrico-lyrics-wrapper">enaku unai pudikuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enaku unai pudikuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">pollatha neram ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pollatha neram ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">illama poiduchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="illama poiduchu"/>
</div>
<div class="lyrico-lyrics-wrapper">sollama serthu vacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sollama serthu vacha"/>
</div>
<div class="lyrico-lyrics-wrapper">kanavugal palikuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavugal palikuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kannala jaada katuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannala jaada katuna"/>
</div>
<div class="lyrico-lyrics-wrapper">thanala vaala aatuva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanala vaala aatuva"/>
</div>
<div class="lyrico-lyrics-wrapper">aatam pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aatam pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">paatu paadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paatu paadu"/>
</div>
<div class="lyrico-lyrics-wrapper">thaka thimi thaka thaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaka thimi thaka thaa"/>
</div>
<div class="lyrico-lyrics-wrapper">hai hai hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hai hai hai"/>
</div>
<div class="lyrico-lyrics-wrapper">nee yenakkini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee yenakkini"/>
</div>
<div class="lyrico-lyrics-wrapper">naan unakunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan unakunu"/>
</div>
<div class="lyrico-lyrics-wrapper">vaa vali thani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaa vali thani"/>
</div>
<div class="lyrico-lyrics-wrapper">namaku enna iruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="namaku enna iruku"/>
</div>
<div class="lyrico-lyrics-wrapper">vaa sirikalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaa sirikalam"/>
</div>
<div class="lyrico-lyrics-wrapper">vaa moraikalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaa moraikalam"/>
</div>
<div class="lyrico-lyrics-wrapper">vaa modikalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaa modikalam"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu namma kanaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu namma kanaku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">munnala nanum vara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munnala nanum vara"/>
</div>
<div class="lyrico-lyrics-wrapper">pinnala neeyum vara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pinnala neeyum vara"/>
</div>
<div class="lyrico-lyrics-wrapper">ellame koodi vara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellame koodi vara"/>
</div>
<div class="lyrico-lyrics-wrapper">enaku unai pudikuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enaku unai pudikuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">pollatha neram ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pollatha neram ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">illama poiduchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="illama poiduchu"/>
</div>
<div class="lyrico-lyrics-wrapper">sollama serthu vacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sollama serthu vacha"/>
</div>
<div class="lyrico-lyrics-wrapper">kanavugal palikuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavugal palikuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">maana pol oadura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maana pol oadura"/>
</div>
<div class="lyrico-lyrics-wrapper">meena pol thaavura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meena pol thaavura"/>
</div>
<div class="lyrico-lyrics-wrapper">yaanaiyai pol neyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaanaiyai pol neyum"/>
</div>
<div class="lyrico-lyrics-wrapper">palasaali thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="palasaali thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">mothi than paakave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mothi than paakave"/>
</div>
<div class="lyrico-lyrics-wrapper">yaarun than ilaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaarun than ilaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">kottaikku raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kottaikku raja"/>
</div>
<div class="lyrico-lyrics-wrapper">ippo naama thanada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ippo naama thanada"/>
</div>
<div class="lyrico-lyrics-wrapper">sonnatha kekum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sonnatha kekum"/>
</div>
<div class="lyrico-lyrics-wrapper">ennoda nanban
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennoda nanban"/>
</div>
<div class="lyrico-lyrics-wrapper">nee matum thaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee matum thaney"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu pothumey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu pothumey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kannala jaada katuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannala jaada katuna"/>
</div>
<div class="lyrico-lyrics-wrapper">thanala vaala aatuva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanala vaala aatuva"/>
</div>
<div class="lyrico-lyrics-wrapper">aatam pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aatam pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">paatu paadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paatu paadu"/>
</div>
<div class="lyrico-lyrics-wrapper">thaka thimi thaka thaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaka thimi thaka thaa"/>
</div>
<div class="lyrico-lyrics-wrapper">hai hai hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hai hai hai"/>
</div>
<div class="lyrico-lyrics-wrapper">nee yenakkini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee yenakkini"/>
</div>
<div class="lyrico-lyrics-wrapper">naan unakunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan unakunu"/>
</div>
<div class="lyrico-lyrics-wrapper">vaa vali thani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaa vali thani"/>
</div>
<div class="lyrico-lyrics-wrapper">namaku enna iruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="namaku enna iruku"/>
</div>
<div class="lyrico-lyrics-wrapper">vaa sirikalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaa sirikalam"/>
</div>
<div class="lyrico-lyrics-wrapper">vaa moraikalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaa moraikalam"/>
</div>
<div class="lyrico-lyrics-wrapper">vaa modikalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaa modikalam"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu namma kanaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu namma kanaku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">munnala nanum vara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munnala nanum vara"/>
</div>
<div class="lyrico-lyrics-wrapper">pinnala neeyum vara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pinnala neeyum vara"/>
</div>
<div class="lyrico-lyrics-wrapper">ellame koodi vara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellame koodi vara"/>
</div>
<div class="lyrico-lyrics-wrapper">enaku unai pudikuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enaku unai pudikuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">pollatha neram ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pollatha neram ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">illama poiduchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="illama poiduchu"/>
</div>
<div class="lyrico-lyrics-wrapper">sollama serthu vacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sollama serthu vacha"/>
</div>
<div class="lyrico-lyrics-wrapper">kanavugal palikuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavugal palikuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aasaikku aasaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aasaikku aasaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">pasatha kaatura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pasatha kaatura"/>
</div>
<div class="lyrico-lyrics-wrapper">tholuku thozhanaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tholuku thozhanaga"/>
</div>
<div class="lyrico-lyrics-wrapper">neeyum iruka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeyum iruka"/>
</div>
<div class="lyrico-lyrics-wrapper">maayangal kaatura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maayangal kaatura"/>
</div>
<div class="lyrico-lyrics-wrapper">saagasam seiyura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saagasam seiyura"/>
</div>
<div class="lyrico-lyrics-wrapper">maar than pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maar than pochu"/>
</div>
<div class="lyrico-lyrics-wrapper">namma kaalam neramey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="namma kaalam neramey"/>
</div>
<div class="lyrico-lyrics-wrapper">oorellam sernthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oorellam sernthu"/>
</div>
<div class="lyrico-lyrics-wrapper">kootatha kooti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kootatha kooti"/>
</div>
<div class="lyrico-lyrics-wrapper">nammoda natpa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nammoda natpa"/>
</div>
<div class="lyrico-lyrics-wrapper">thaaney pesumey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaaney pesumey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kannala jaada katuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannala jaada katuna"/>
</div>
<div class="lyrico-lyrics-wrapper">thanala vaala aatuva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanala vaala aatuva"/>
</div>
<div class="lyrico-lyrics-wrapper">aatam pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aatam pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">paatu paadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paatu paadu"/>
</div>
<div class="lyrico-lyrics-wrapper">thaka thimi thaka thaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaka thimi thaka thaa"/>
</div>
<div class="lyrico-lyrics-wrapper">hai hai hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hai hai hai"/>
</div>
<div class="lyrico-lyrics-wrapper">nee yenakkini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee yenakkini"/>
</div>
<div class="lyrico-lyrics-wrapper">naan unakunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan unakunu"/>
</div>
<div class="lyrico-lyrics-wrapper">vaa vali thani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaa vali thani"/>
</div>
<div class="lyrico-lyrics-wrapper">namaku enna iruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="namaku enna iruku"/>
</div>
<div class="lyrico-lyrics-wrapper">vaa sirikalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaa sirikalam"/>
</div>
<div class="lyrico-lyrics-wrapper">vaa moraikalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaa moraikalam"/>
</div>
<div class="lyrico-lyrics-wrapper">vaa modikalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaa modikalam"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu namma kanaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu namma kanaku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nee yenakkini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee yenakkini"/>
</div>
<div class="lyrico-lyrics-wrapper">naan unakunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan unakunu"/>
</div>
<div class="lyrico-lyrics-wrapper">vaa vali thani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaa vali thani"/>
</div>
<div class="lyrico-lyrics-wrapper">namaku enna iruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="namaku enna iruku"/>
</div>
<div class="lyrico-lyrics-wrapper">vaa sirikalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaa sirikalam"/>
</div>
<div class="lyrico-lyrics-wrapper">vaa moraikalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaa moraikalam"/>
</div>
<div class="lyrico-lyrics-wrapper">vaa modikalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaa modikalam"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu namma kanaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu namma kanaku"/>
</div>
</pre>
