---
title: "pepsi aunty song lyrics"
album: "Seetimaarr"
artist: "Mani Sharma"
lyricist: "Vipanchi"
director: "Sampath Nandi"
path: "/albums/seetimaarr-lyrics"
song: "Pepsi Aunty"
image: ../../images/albumart/seetimaarr.jpg
date: 2021-09-10
lang: telugu
youtubeLink: "https://www.youtube.com/embed/6Anj4GBzS28"
type: "happy"
singers:
  - Keerthana Sharmma
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hey south kaa chakori
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey south kaa chakori"/>
</div>
<div class="lyrico-lyrics-wrapper">Apna kahani sunaona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Apna kahani sunaona"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey em cheppanu em cheppanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey em cheppanu em cheppanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee baby bornu brought up 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee baby bornu brought up "/>
</div>
<div class="lyrico-lyrics-wrapper">emanu cheppanu baavoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="emanu cheppanu baavoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Maa ammaku pelli kakamundhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa ammaku pelli kakamundhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadupulo paddanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadupulo paddanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nelalu nindaka mundhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nelalu nindaka mundhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Bummidha paddanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bummidha paddanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Baarasaala kakamundhe borla paddanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baarasaala kakamundhe borla paddanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa thondhara chusi nenu edho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa thondhara chusi nenu edho"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayipothanu anukunnari kaani nenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayipothanu anukunnari kaani nenu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arey tenth loki raagane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey tenth loki raagane"/>
</div>
<div class="lyrico-lyrics-wrapper">Wall jumpule chesanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wall jumpule chesanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ahaa inter loki raagane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahaa inter loki raagane"/>
</div>
<div class="lyrico-lyrics-wrapper">Boy friendune maarchanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boy friendune maarchanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey degreeloki raagane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey degreeloki raagane"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhukaname terisanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhukaname terisanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mari Pg loku raagane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mari Pg loku raagane"/>
</div>
<div class="lyrico-lyrics-wrapper">Prapanchame chusanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prapanchame chusanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Atm paytm ye cardaina 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atm paytm ye cardaina "/>
</div>
<div class="lyrico-lyrics-wrapper">ok naa kaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ok naa kaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa perey pepsi aunty
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa perey pepsi aunty"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa pelliki nene anti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa pelliki nene anti"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee ooredhaithe enti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee ooredhaithe enti"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa vollera neeku ooty
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa vollera neeku ooty"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa perey pepsi aunty
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa perey pepsi aunty"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa pelliki nene anti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa pelliki nene anti"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee ooredhaithe enti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee ooredhaithe enti"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa vollera neeku ooty
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa vollera neeku ooty"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alasipoyina officerlaku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alasipoyina officerlaku "/>
</div>
<div class="lyrico-lyrics-wrapper">aaru dhaatithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaru dhaatithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Alcohol nenele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alcohol nenele"/>
</div>
<div class="lyrico-lyrics-wrapper">Bada bada nayakula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bada bada nayakula"/>
</div>
<div class="lyrico-lyrics-wrapper">Sheethakalam samaveshalu naathone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sheethakalam samaveshalu naathone"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee corporaterlaki care of
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee corporaterlaki care of"/>
</div>
<div class="lyrico-lyrics-wrapper">Kurrakarulaku whatsappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurrakarulaku whatsappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nayababulaku take offu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nayababulaku take offu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pellikodukulaku send off
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pellikodukulaku send off"/>
</div>
<div class="lyrico-lyrics-wrapper">Day ayina night ayina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Day ayina night ayina"/>
</div>
<div class="lyrico-lyrics-wrapper">Door anndhi muyanu naathodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Door anndhi muyanu naathodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohoo baby chaala busy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohoo baby chaala busy"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa perey pepsi aunty
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa perey pepsi aunty"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa pelliki nene anti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa pelliki nene anti"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee ooredhaithe enti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee ooredhaithe enti"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa vollera neeku ooty
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa vollera neeku ooty"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa perey pepsi aunty
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa perey pepsi aunty"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa pelliki nene anti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa pelliki nene anti"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee ooredhaithe enti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee ooredhaithe enti"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa vollera neeku ooty
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa vollera neeku ooty"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee girl friendulaku teliyani edho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee girl friendulaku teliyani edho"/>
</div>
<div class="lyrico-lyrics-wrapper">Garam naramune naaku telusu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Garam naramune naaku telusu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee bagaswamulaku palakani edho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee bagaswamulaku palakani edho"/>
</div>
<div class="lyrico-lyrics-wrapper">Theepi swarame naaku telusu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theepi swarame naaku telusu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa bangimale chusaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa bangimale chusaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Oscarule istharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oscarule istharu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa kolathalane kolisaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa kolathalane kolisaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Samsarule chastharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samsarule chastharu"/>
</div>
<div class="lyrico-lyrics-wrapper">Young ayina age ayina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Young ayina age ayina"/>
</div>
<div class="lyrico-lyrics-wrapper">Service lo undadhi ye theda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Service lo undadhi ye theda"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohoo south ki sunny leone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohoo south ki sunny leone"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa perey pepsi aunty
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa perey pepsi aunty"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa pelliki nene anti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa pelliki nene anti"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee ooredhaithe enti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee ooredhaithe enti"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa vollera neeku ooty
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa vollera neeku ooty"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa perey pepsi aunty
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa perey pepsi aunty"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa pelliki nene anti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa pelliki nene anti"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee ooredhaithe enti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee ooredhaithe enti"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa vollera neeku ooty
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa vollera neeku ooty"/>
</div>
</pre>
