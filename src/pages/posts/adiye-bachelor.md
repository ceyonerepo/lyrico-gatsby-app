---
title: "adiye song lyrics"
album: "Bachelor"
artist: "Dhibu Ninan Thomas"
lyricist: "GKB"
director: "Sathish Selvakumar"
path: "/albums/bachelor-lyrics"
song: "Adiye"
image: ../../images/albumart/bachelor.jpg
date: 2021-12-03
lang: tamil
youtubeLink: "https://www.youtube.com/embed/HRD2-_bU4K0"
type: "mass"
singers:
  - Kapil Kapilan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Adiyeee neethaanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiyeee neethaanadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Enbodhai thenae motha konju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enbodhai thenae motha konju"/>
</div>
<div class="lyrico-lyrics-wrapper">Saghiye neeyaradi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saghiye neeyaradi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai theendum pournami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai theendum pournami"/>
</div>
<div class="lyrico-lyrics-wrapper">Koncham nillu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koncham nillu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Penne penne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Penne penne"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan kayyil naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan kayyil naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Koodum neram vittu chellathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koodum neram vittu chellathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannae kannae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannae kannae"/>
</div>
<div class="lyrico-lyrics-wrapper">Neelam kaalam vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelam kaalam vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaarayo arugilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaarayo arugilae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poove kaadhal pookkum poove
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poove kaadhal pookkum poove"/>
</div>
<div class="lyrico-lyrics-wrapper">Saaral veesa eeram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saaral veesa eeram"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai koncham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai koncham"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhayil un vaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhayil un vaasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kenjum nenjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kenjum nenjam"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai kenjum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai kenjum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pularum kaalai vaendam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pularum kaalai vaendam"/>
</div>
<div class="lyrico-lyrics-wrapper">Iravin kulirae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravin kulirae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai kollaathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai kollaathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irvukal neela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irvukal neela"/>
</div>
<div class="lyrico-lyrics-wrapper">Imaigalum mooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imaigalum mooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Idaiveli aeno kanmaniyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idaiveli aeno kanmaniyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yugangalai thaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yugangalai thaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Viralgalai pootti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viralgalai pootti"/>
</div>
<div class="lyrico-lyrics-wrapper">Muthangalai thinmbhom adiyeee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthangalai thinmbhom adiyeee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saalarathin velichathilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saalarathin velichathilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Arumbukiraen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arumbukiraen"/>
</div>
<div class="lyrico-lyrics-wrapper">Mei kadalin alaikalilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mei kadalin alaikalilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Suzhaluthey kavizhuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suzhaluthey kavizhuthey"/>
</div>
<div class="lyrico-lyrics-wrapper">Manamadhuvae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manamadhuvae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adiyeee neethaanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiyeee neethaanadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Enbodhai thenae motha konju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enbodhai thenae motha konju"/>
</div>
<div class="lyrico-lyrics-wrapper">Saghiye neeyaradi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saghiye neeyaradi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai theendum pournami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai theendum pournami"/>
</div>
<div class="lyrico-lyrics-wrapper">Koncham nillu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koncham nillu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Penne penne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Penne penne"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan kayyil naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan kayyil naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Koodum neram vittu chellathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koodum neram vittu chellathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannae kannae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannae kannae"/>
</div>
<div class="lyrico-lyrics-wrapper">Neelam kaalam vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelam kaalam vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaarayo arugilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaarayo arugilae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poove kaadhal pookkum poove
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poove kaadhal pookkum poove"/>
</div>
<div class="lyrico-lyrics-wrapper">Saaral veesa eeram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saaral veesa eeram"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai koncham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai koncham"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhayil un vaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhayil un vaasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kenjum nenjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kenjum nenjam"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai kenjum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai kenjum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pularum kaalai vaendam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pularum kaalai vaendam"/>
</div>
<div class="lyrico-lyrics-wrapper">Iravin kulirae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravin kulirae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai kollaathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai kollaathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poove kaadhal pookkum poove
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poove kaadhal pookkum poove"/>
</div>
<div class="lyrico-lyrics-wrapper">Saaral veesa eeram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saaral veesa eeram"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai koncham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai koncham"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhayil un vaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhayil un vaasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kenjum nenjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kenjum nenjam"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai kenjum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai kenjum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pularum kaalai vaendam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pularum kaalai vaendam"/>
</div>
<div class="lyrico-lyrics-wrapper">Iravin kulirae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravin kulirae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai kollaathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai kollaathey"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey"/>
</div>
</pre>
