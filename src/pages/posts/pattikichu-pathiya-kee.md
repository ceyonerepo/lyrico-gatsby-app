---
title: "pattikichu pathiya song lyrics"
album: "Kee"
artist: "Vishal Chandrasekhar"
lyricist: "Thamarai"
director: "Kalees"
path: "/albums/kee-lyrics"
song: "Pattikichu Pathiya"
image: ../../images/albumart/kee.jpg
date: 2019-05-10
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ehpcmj1uIYI"
type: "friendship"
singers:
  - Devan Ekambaram
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">En nanbena pola vera yarumilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En nanbena pola vera yarumilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee thedipaaru ulagathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thedipaaru ulagathula"/>
</div>
<div class="lyrico-lyrics-wrapper">Sunday monday weekend mudinjaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sunday monday weekend mudinjaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaaththiruppaan iruppaan nanbanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaththiruppaan iruppaan nanbanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pathikichu pathiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathikichu pathiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Solliparthen kettiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solliparthen kettiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">En frienda yaarum seendinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En frienda yaarum seendinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Puncture aagum moonjiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puncture aagum moonjiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Baasha pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baasha pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee maasudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee maasudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pizza naandhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pizza naandhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee sauce-u daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee sauce-u daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kushkaala kedacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kushkaala kedacha"/>
</div>
<div class="lyrico-lyrics-wrapper">Piece-u daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Piece-u daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Amma appa anba minjum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma appa anba minjum"/>
</div>
<div class="lyrico-lyrics-wrapper">Friendship thaandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friendship thaandaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Feeling-u yendaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Feeling-u yendaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Friend-u irukandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friend-u irukandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee vizhindhenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee vizhindhenna"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaanguvaan naekkaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanguvaan naekkaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaasu illae naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasu illae naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aagaadha gaandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagaadha gaandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Friendoda pocket
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friendoda pocket"/>
</div>
<div class="lyrico-lyrics-wrapper">ATM dhaandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ATM dhaandaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nanba un lover trumpoda ponnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanba un lover trumpoda ponnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye machi solludaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye machi solludaa"/>
</div>
<div class="lyrico-lyrics-wrapper">White house-il poondhu thookuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="White house-il poondhu thookuren"/>
</div>
<div class="lyrico-lyrics-wrapper">World war-ae vandhaalum naan paathukkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="World war-ae vandhaalum naan paathukkuren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey nee kulikkavae maatta machi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey nee kulikkavae maatta machi"/>
</div>
<div class="lyrico-lyrics-wrapper">Paravayilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paravayilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Un natpula naan azhukkaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un natpula naan azhukkaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Paathathadhilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paathathadhilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Stunt-il dhaan mudhal punch-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Stunt-il dhaan mudhal punch-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Studyinna last bench-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Studyinna last bench-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaramellaan punishment-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaramellaan punishment-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaangi onnaa sirippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaangi onnaa sirippom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nana naanaa nana naanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nana naanaa nana naanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nana naanaa nana naanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nana naanaa nana naanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannana naanaa nannana naanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannana naanaa nannana naanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannana naanaa nannana naanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannana naanaa nannana naanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nanba oru ponnu un thozhi aanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanba oru ponnu un thozhi aanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee gethu kaattudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee gethu kaattudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponnu poyyil mayangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponnu poyyil mayangi"/>
</div>
<div class="lyrico-lyrics-wrapper">Love-va tharum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love-va tharum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nambikkaiyil mattum dhaan daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nambikkaiyil mattum dhaan daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Natpu varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natpu varum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada poravara ponnumela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada poravara ponnumela"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasai vaippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai vaippom"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava munnamattum buddhanaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava munnamattum buddhanaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Maari nippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maari nippom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aval aadai konjam vilagiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aval aadai konjam vilagiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhai unarndhidhum munnae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai unarndhidhum munnae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaigal adhai sari seiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaigal adhai sari seiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanba namma kaigalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanba namma kaigalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Feeling-u yendaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Feeling-u yendaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Friend-u irukandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friend-u irukandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee vizhindhenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee vizhindhenna"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaanguvaan naekkaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanguvaan naekkaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaasu illae naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasu illae naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aagaadha gaandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagaadha gaandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Friendoda pocket
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friendoda pocket"/>
</div>
<div class="lyrico-lyrics-wrapper">ATM dhaandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ATM dhaandaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey en nanbena pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey en nanbena pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Vera yarumilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vera yarumilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee thedipaaru ulagathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thedipaaru ulagathula"/>
</div>
<div class="lyrico-lyrics-wrapper">Sunday monday hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sunday monday hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Weekend mudinjaalum ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Weekend mudinjaalum ho"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaaththiruppaan iruppaan nanbanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaththiruppaan iruppaan nanbanae"/>
</div>
</pre>
