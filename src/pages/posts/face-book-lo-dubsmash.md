---
title: "face book lo song lyrics"
album: "Dubsmash"
artist: "Vamssih B"
lyricist: "Balavardhan"
director: "Keshav Depur"
path: "/albums/dubsmash-lyrics"
song: "Face Book Lo"
image: ../../images/albumart/dubsmash.jpg
date: 2020-01-30
lang: telugu
youtubeLink: "https://www.youtube.com/embed/G8wgDheT93E"
type: "happy"
singers:
  - Vamssih
  - Deepu
  - Jayavardhan
  - Iswarya
  - Hythaishi
  - Yashaswini
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">FaceBook Lo Gantalu Gantalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="FaceBook Lo Gantalu Gantalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Whats app lo Reyi Pagalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Whats app lo Reyi Pagalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Indtragram Lo Follow Avudham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indtragram Lo Follow Avudham"/>
</div>
<div class="lyrico-lyrics-wrapper">Baby O baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby O baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Dubsmash Thi Pota Poti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dubsmash Thi Pota Poti"/>
</div>
<div class="lyrico-lyrics-wrapper">Video Latho Vaadi Vedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Video Latho Vaadi Vedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottha Trend Ne Kanipedatham raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottha Trend Ne Kanipedatham raa"/>
</div>
<div class="lyrico-lyrics-wrapper">Baabu O Baabu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baabu O Baabu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pilla Choopulaku Pelipovadam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pilla Choopulaku Pelipovadam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pilla Gaalilo Sedha Theeradam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pilla Gaalilo Sedha Theeradam"/>
</div>
<div class="lyrico-lyrics-wrapper">Theripaara Vompulni Choodadam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theripaara Vompulni Choodadam"/>
</div>
<div class="lyrico-lyrics-wrapper">Theta Telugulo Thittukovadam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theta Telugulo Thittukovadam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">FaceBook Lo Gantalu Gantalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="FaceBook Lo Gantalu Gantalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Whats app Lo Reyi Pagalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Whats app Lo Reyi Pagalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Indtragram Lo Follow Avudham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indtragram Lo Follow Avudham"/>
</div>
<div class="lyrico-lyrics-wrapper">Baby O baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby O baby"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pedhavula Paina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pedhavula Paina"/>
</div>
<div class="lyrico-lyrics-wrapper">Rangulu Maarche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangulu Maarche"/>
</div>
<div class="lyrico-lyrics-wrapper">Lipstick Meme
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lipstick Meme"/>
</div>
<div class="lyrico-lyrics-wrapper">Avuthaame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avuthaame"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandils Meere
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandils Meere"/>
</div>
<div class="lyrico-lyrics-wrapper">Anukoni Roju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anukoni Roju"/>
</div>
<div class="lyrico-lyrics-wrapper">Vesuku Thirigaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vesuku Thirigaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Mimmalne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mimmalne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nijamaina Premane Nammaleru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijamaina Premane Nammaleru"/>
</div>
<div class="lyrico-lyrics-wrapper">Niluvella Vishamu Mee Aadavaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niluvella Vishamu Mee Aadavaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Disko Lu Pub Lani Thiruguthaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Disko Lu Pub Lani Thiruguthaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Midnight Rides Ki Bikey Yekkuthaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Midnight Rides Ki Bikey Yekkuthaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Debit Card Laa Meeru Vundagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Debit Card Laa Meeru Vundagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Tez Apple Maaku Dhandagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tez Apple Maaku Dhandagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pickup Drop le Meeru Cheyagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pickup Drop le Meeru Cheyagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ola Cable Maaku Dhandagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ola Cable Maaku Dhandagaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Campus A Kingdom Manki He
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Campus A Kingdom Manki He"/>
</div>
<div class="lyrico-lyrics-wrapper">Alwaysu Freedom Manaki He He
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alwaysu Freedom Manaki He He"/>
</div>
<div class="lyrico-lyrics-wrapper">Girl Friends a Image Manaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Girl Friends a Image Manaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaro Yaaro O O O Yaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaro Yaaro O O O Yaaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Friendship A Energy Manaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friendship A Energy Manaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Exams A Allergy Manaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Exams A Allergy Manaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Class rooms A Yuddhaboomulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Class rooms A Yuddhaboomulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaro Yaaro Yo Yo Yo Yaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaro Yaaro Yo Yo Yo Yaaro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maa Choopulake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa Choopulake"/>
</div>
<div class="lyrico-lyrics-wrapper">Mee Thala Thirige
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mee Thala Thirige"/>
</div>
<div class="lyrico-lyrics-wrapper">Navvu Visirithe Padathaare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Navvu Visirithe Padathaare"/>
</div>
<div class="lyrico-lyrics-wrapper">No Gaurenty No Waaranty
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="No Gaurenty No Waaranty"/>
</div>
<div class="lyrico-lyrics-wrapper">Chaina Pisule Ammaayile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chaina Pisule Ammaayile"/>
</div>
<div class="lyrico-lyrics-wrapper">Chali Manchu Lona Lu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chali Manchu Lona Lu"/>
</div>
<div class="lyrico-lyrics-wrapper">Meeru Kalakanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meeru Kalakanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Kosamoccheti Cheemalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kosamoccheti Cheemalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadavalla Adugulo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadavalla Adugulo"/>
</div>
<div class="lyrico-lyrics-wrapper">Vunna Neeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vunna Neeru"/>
</div>
<div class="lyrico-lyrics-wrapper">Raayesi Paiki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raayesi Paiki"/>
</div>
<div class="lyrico-lyrics-wrapper">Theppinchagalaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theppinchagalaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Site kottadam Break Fast Ra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Site kottadam Break Fast Ra"/>
</div>
<div class="lyrico-lyrics-wrapper">Date Kelladam Treat Sodharaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Date Kelladam Treat Sodharaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pongipovadam Danger A Kadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pongipovadam Danger A Kadha"/>
</div>
<div class="lyrico-lyrics-wrapper">kalisipothe Ika Choice Ledhura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalisipothe Ika Choice Ledhura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Friendship A Energy Manaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friendship A Energy Manaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Exams A Allergy Manaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Exams A Allergy Manaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Class rooms A Yuddhaboomulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Class rooms A Yuddhaboomulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaro Yaaro Yo Yo Yo Yaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaro Yaaro Yo Yo Yo Yaaro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Campus A Kingdom Manki He
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Campus A Kingdom Manki He"/>
</div>
<div class="lyrico-lyrics-wrapper">Alwaysu Freedom Manaki He He
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alwaysu Freedom Manaki He He"/>
</div>
<div class="lyrico-lyrics-wrapper">Girl Friends a Image Manaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Girl Friends a Image Manaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaro Yaaro O O O Yaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaro Yaaro O O O Yaaro"/>
</div>
</pre>
