---
title: "start stop song lyrics"
album: "Laxmii"
artist: "Tanishk Bagchi"
lyricist: "Vayu"
director: "Raghava Lawrence"
path: "/albums/laxmii-lyrics"
song: "Start Stop"
image: ../../images/albumart/laxmii.jpg
date: 2020-11-09
lang: hindi
youtubeLink: "https://www.youtube.com/embed/CS7j0pmGVlQ"
type: "happy"
singers:
  - Raja Hasan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Start stop
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Start stop"/>
</div>
<div class="lyrico-lyrics-wrapper">Start stop
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Start stop"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aey raja baja naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aey raja baja naa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Are dil ka kabootar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Are dil ka kabootar"/>
</div>
<div class="lyrico-lyrics-wrapper">Mohbbat ke scooter pe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mohbbat ke scooter pe"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaya hai pyaar karle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaya hai pyaar karle"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arre thumka lagado
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arre thumka lagado"/>
</div>
<div class="lyrico-lyrics-wrapper">Uthha do tornado
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uthha do tornado"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu aa dil pe vaar karle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu aa dil pe vaar karle"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karle na re..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karle na re.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Are dil ka kabootar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Are dil ka kabootar"/>
</div>
<div class="lyrico-lyrics-wrapper">Mohbbat ke scooter pe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mohbbat ke scooter pe"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaya hai pyaar karle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaya hai pyaar karle"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arre thumka lagado
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arre thumka lagado"/>
</div>
<div class="lyrico-lyrics-wrapper">Uthha do tornado
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uthha do tornado"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu aa dil pe vaar karle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu aa dil pe vaar karle"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey oh meri maina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey oh meri maina"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh meri maina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh meri maina"/>
</div>
<div class="lyrico-lyrics-wrapper">Loote kyun chaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Loote kyun chaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaja dono kare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaja dono kare"/>
</div>
<div class="lyrico-lyrics-wrapper">Fly fly fly fly
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fly fly fly fly"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Start stop
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Start stop"/>
</div>
<div class="lyrico-lyrics-wrapper">Start stop
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Start stop"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aey raja baja naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aey raja baja naa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rantak rantak ra..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rantak rantak ra.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Humari beat pe dance karne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Humari beat pe dance karne"/>
</div>
<div class="lyrico-lyrics-wrapper">Ke liye dhanyawad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ke liye dhanyawad"/>
</div>
<div class="lyrico-lyrics-wrapper">Khana khaake jaayiyega!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khana khaake jaayiyega!"/>
</div>
</pre>
