---
title: "anaganaganaga song lyrics"
album: "Oh Baby"
artist: "Mickey J. Meyer"
lyricist: "Lakshmi Bhupala"
director: "B.V. Nandini Reddy"
path: "/albums/oh-baby-lyrics"
song: "Anaganaganaga"
image: ../../images/albumart/oh-baby.jpg
date: 2019-07-05
lang: telugu
youtubeLink: "https://www.youtube.com/embed/TghnWuCF0ts"
type: "sad"
singers:
  - Sreerama Chandra
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Anaganaganaganaga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anaganaganaganaga "/>
</div>
<div class="lyrico-lyrics-wrapper">alasina manasokati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alasina manasokati"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharalenu thana gude vidichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharalenu thana gude vidichi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana vodi pasivaare 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana vodi pasivaare "/>
</div>
<div class="lyrico-lyrics-wrapper">tharamina vedhanalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tharamina vedhanalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanu yetu poyindho nadichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanu yetu poyindho nadichi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee adugulu leka ye alikidi ledhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee adugulu leka ye alikidi ledhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee vuniki leka oo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee vuniki leka oo "/>
</div>
<div class="lyrico-lyrics-wrapper">shoonya mayyaamaniee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="shoonya mayyaamaniee"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilaaa… nilichaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilaaa… nilichaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anaganaganaganaga alasina manasokati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anaganaganaganaga alasina manasokati"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharalenu thana gude vidichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharalenu thana gude vidichi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana vodi pasivaare 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana vodi pasivaare "/>
</div>
<div class="lyrico-lyrics-wrapper">tharamina vedhanalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tharamina vedhanalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanu yetu poyindho nadichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanu yetu poyindho nadichi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thittalanipinchaina vasthaavani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thittalanipinchaina vasthaavani"/>
</div>
<div class="lyrico-lyrics-wrapper">Anukunna thappedho cheyaalani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anukunna thappedho cheyaalani"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivi thegipoye bhandhaalenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivi thegipoye bhandhaalenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Maa voopirike moolam neevega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa voopirike moolam neevega"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanu paapanu kaache reppalaka kopam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanu paapanu kaache reppalaka kopam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye dhikkuna neevunna raava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye dhikkuna neevunna raava"/>
</div>
<div class="lyrico-lyrics-wrapper">Needhe memantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Needhe memantha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anaganaganaganaga alasina manasokati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anaganaganaganaga alasina manasokati"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharalenu thana gude vidichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharalenu thana gude vidichi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana vodi pasivaare tharamina vedhanalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana vodi pasivaare tharamina vedhanalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanu yetu poyindho nadichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanu yetu poyindho nadichi"/>
</div>
</pre>
