---
title: "edho solla song lyrics"
album: "Murungakkai Chips"
artist: "Dharan Kumar"
lyricist: "Ravindhar Chandrasekaran"
director: "Srijar"
path: "/albums/murungakkai-chips-song-lyrics"
song: "Edho Solla"
image: ../../images/albumart/murungakkai-chips.jpg
date: 2021-12-10
lang: tamil
youtubeLink: "https://www.youtube.com/embed/lzfnSBXsj68"
type: "love"
singers:
  - Sid Sriram
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Nerunjiyae en nenjai thaikka yengurenae…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nerunjiyae en nenjai thaikka yengurenae…"/>
</div>
<div class="lyrico-lyrics-wrapper">En nenjai thaikka yengurenae…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En nenjai thaikka yengurenae…"/>
</div>
<div class="lyrico-lyrics-wrapper">En nenjai thaikka yengurenae…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En nenjai thaikka yengurenae…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Nerunjiyae en nenjai thaikka yengurenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nerunjiyae en nenjai thaikka yengurenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Maranjiyae un nizhalula vaazhurenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maranjiyae un nizhalula vaazhurenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ooo…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooo…"/>
</div>
<div class="lyrico-lyrics-wrapper">Siru thooral podum megam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru thooral podum megam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuli kaadhal thoovadho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuli kaadhal thoovadho"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru minnal thaakka naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru minnal thaakka naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un viralai thedaatho…oo..oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un viralai thedaatho…oo..oo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Edho solla ulla thudikkuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edho solla ulla thudikkuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kitta vandha atha marakkuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kitta vandha atha marakkuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Katti vellathayae udachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katti vellathayae udachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjamaaga kadichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjamaaga kadichu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pechula karachiduva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pechula karachiduva"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendu puruvatha sarichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendu puruvatha sarichu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naduvula morachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naduvula morachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhaga paarthiduva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhaga paarthiduva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Edho solla ulla thudikkuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edho solla ulla thudikkuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kitta vandha atha marakkuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kitta vandha atha marakkuthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kaattu theeya kaadhal pinna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattu theeya kaadhal pinna"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiri theerndha dheepam pol aanen mella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiri theerndha dheepam pol aanen mella"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannadiyil paarthen enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannadiyil paarthen enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai pol naan kaana marandhen enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai pol naan kaana marandhen enna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Sundi vitta oththa kaasa pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sundi vitta oththa kaasa pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai suththi kittu varuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai suththi kittu varuven"/>
</div>
<div class="lyrico-lyrics-wrapper">Thatti vittu chinna thoosu pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thatti vittu chinna thoosu pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai ottikittu vizhuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai ottikittu vizhuven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vetti vitta un nagatha mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetti vitta un nagatha mela"/>
</div>
<div class="lyrico-lyrics-wrapper">En nagam vechu rasippen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En nagam vechu rasippen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada kottum paniyil vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada kottum paniyil vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vettum veyila pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vettum veyila pol"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhala nenjodu thaa uyirae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhala nenjodu thaa uyirae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Edho solla ulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edho solla ulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Kitta vandha atha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kitta vandha atha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Edho solla…aa…aa…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edho solla…aa…aa…"/>
</div>
<div class="lyrico-lyrics-wrapper">Marakkuthae…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marakkuthae…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Edho solla ulla thudikkuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edho solla ulla thudikkuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kitta vandha atha marakkuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kitta vandha atha marakkuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Katti vellathayae udachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katti vellathayae udachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjamaaga kadichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjamaaga kadichu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pechula karachiduva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pechula karachiduva"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendu puruvatha sarichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendu puruvatha sarichu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naduvula morachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naduvula morachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhaga paarthiduva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhaga paarthiduva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Edho solla ulla thudikkuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edho solla ulla thudikkuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thudikkuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thudikkuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kitta vandha atha marakkuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kitta vandha atha marakkuthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thaniyaga thudichenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaniyaga thudichenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaniyaga alanjanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaniyaga alanjanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaniyaga thudichenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaniyaga thudichenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaniyaga alanjanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaniyaga alanjanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaniyaga thudichenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaniyaga thudichenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaniyaga alanjanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaniyaga alanjanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaniyaga thudichenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaniyaga thudichenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaniyagathaan alanjanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaniyagathaan alanjanae"/>
</div>
</pre>
