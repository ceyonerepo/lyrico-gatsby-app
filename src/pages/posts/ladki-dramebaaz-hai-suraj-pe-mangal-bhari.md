---
title: "ladki dramebaaz hai song lyrics"
album: "Suraj pe mangal bhari"
artist: "Javed - Mohsin"
lyricist: "Danish Sabri"
director: "Abhishek Sharma"
path: "/albums/suraj-pe-mangal-bhari-lyrics"
song: "Ladki Dramebaaz Hai"
image: ../../images/albumart/suraj-pe-mangal-bhari.jpg
date: 2020-11-15
lang: hindi
youtubeLink: "https://www.youtube.com/embed/jh407047cNQ"
type: "happy"
singers:
  - Mohsin Shaikh
  - Jyotica Tangri
  - Mellow D
  - Aishwarya Bhandari
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Woofer ko thoda loud baja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Woofer ko thoda loud baja"/>
</div>
<div class="lyrico-lyrics-wrapper">Beat pe apni crowd nacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Beat pe apni crowd nacha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Woofer ko thoda loud baja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Woofer ko thoda loud baja"/>
</div>
<div class="lyrico-lyrics-wrapper">Beat pe apni crowd nacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Beat pe apni crowd nacha"/>
</div>
<div class="lyrico-lyrics-wrapper">Dim dim light main dikhte nahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dim dim light main dikhte nahi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kitne color hai apne bata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kitne color hai apne bata"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bholi bhaali din mein hai par
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bholi bhaali din mein hai par"/>
</div>
<div class="lyrico-lyrics-wrapper">Different iski raat hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Different iski raat hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Raat hai raat hai raat raat raat..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raat hai raat hai raat raat raat.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ladki dramebaaz hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ladki dramebaaz hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ladki dramebaaz hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ladki dramebaaz hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ladki dramebaaz hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ladki dramebaaz hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ladki ladki ladki..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ladki ladki ladki.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ladka dramebaaz hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ladka dramebaaz hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ladka dramebaaz hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ladka dramebaaz hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ladka dramebaaz hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ladka dramebaaz hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ladka ladka ladka..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ladka ladka ladka.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Billo ke nakhre mashallah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Billo ke nakhre mashallah"/>
</div>
<div class="lyrico-lyrics-wrapper">Poore club mein ho gaya halla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poore club mein ho gaya halla"/>
</div>
<div class="lyrico-lyrics-wrapper">Sacchi main bhi ho gaya jhalla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sacchi main bhi ho gaya jhalla"/>
</div>
<div class="lyrico-lyrics-wrapper">Jhalla jhal! jhal! jhal!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jhalla jhal! jhal! jhal!"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thodi crazy thodi shy woh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodi crazy thodi shy woh"/>
</div>
<div class="lyrico-lyrics-wrapper">Kisi ke bhi yeh hath na aaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kisi ke bhi yeh hath na aaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Drame karti 575
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Drame karti 575"/>
</div>
<div class="lyrico-lyrics-wrapper">Par cute badi lage aaye haaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Par cute badi lage aaye haaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Teri baaton se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teri baaton se"/>
</div>
<div class="lyrico-lyrics-wrapper">Meri baatein rhyming karti hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meri baatein rhyming karti hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Raat hote hi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raat hote hi"/>
</div>
<div class="lyrico-lyrics-wrapper">Meri jutti shining karti hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meri jutti shining karti hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Teri baaton se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teri baaton se"/>
</div>
<div class="lyrico-lyrics-wrapper">Meri baatein rhyming karti hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meri baatein rhyming karti hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Raat hote hi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raat hote hi"/>
</div>
<div class="lyrico-lyrics-wrapper">Meri jutti shining karti hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meri jutti shining karti hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Do naino ke nakhre hazaron
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Do naino ke nakhre hazaron"/>
</div>
<div class="lyrico-lyrics-wrapper">Nakhro se na humko maaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nakhro se na humko maaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Baal ghumaye angrezi swag mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baal ghumaye angrezi swag mein"/>
</div>
<div class="lyrico-lyrics-wrapper">Air mein iske hath hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Air mein iske hath hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ladki dramebaaz hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ladki dramebaaz hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ladki dramebaaz hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ladki dramebaaz hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ladki dramebaaz hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ladki dramebaaz hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ladki ladki ladki..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ladki ladki ladki.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ladka dramebaaz hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ladka dramebaaz hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ladka dramebaaz hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ladka dramebaaz hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ladka dramebaaz hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ladka dramebaaz hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ladka ladka ladka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ladka ladka ladka"/>
</div>
</pre>
