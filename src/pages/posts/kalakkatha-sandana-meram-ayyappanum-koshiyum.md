---
title: "kalakkatha sandana meram song lyrics"
album: "Ayyappanum Koshiyum"
artist: "Jakes Bejoy"
lyricist: "Nanjamma"
director: "Sachy"
path: "/albums/ayyappanum-koshiyum-lyrics"
song: "Kalakkatha Sandana Meram"
image: ../../images/albumart/ayyappanum-koshiyum.jpg
date: 2020-02-07
lang: malayalam
youtubeLink: "https://www.youtube.com/embed/UV-ng26Gt_U"
type: "title song"
singers:
  - Nanjamma
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kalakkatha sandhanameraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalakkatha sandhanameraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vegu vega poothirikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vegu vega poothirikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Poo parikka pokilaamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poo parikka pokilaamo"/>
</div>
<div class="lyrico-lyrics-wrapper">Vimenaathe pakkilammo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vimenaathe pakkilammo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalakkaatha sandhanameraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalakkaatha sandhanameraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vegu vega poothirikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vegu vega poothirikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Poo parikka pokilaamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poo parikka pokilaamo"/>
</div>
<div class="lyrico-lyrics-wrapper">Vimenaathe pakkilammo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vimenaathe pakkilammo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lala le lale lale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lala le lale lale"/>
</div>
<div class="lyrico-lyrics-wrapper">Lale lale la la le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lale lale la la le"/>
</div>
<div class="lyrico-lyrics-wrapper">Lala le lale lale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lala le lale lale"/>
</div>
<div class="lyrico-lyrics-wrapper">Lale lale la la le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lale lale la la le"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thekkatha sandhanameraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thekkatha sandhanameraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vegu vega poothirikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vegu vega poothirikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pooparikkaa pokilaamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pooparikkaa pokilaamo"/>
</div>
<div class="lyrico-lyrics-wrapper">Vimenaathe pakkilammo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vimenaathe pakkilammo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thille le lele lele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thille le lele lele"/>
</div>
<div class="lyrico-lyrics-wrapper">Le le lele lo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Le le lele lo"/>
</div>
<div class="lyrico-lyrics-wrapper">Thille le lele lele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thille le lele lele"/>
</div>
<div class="lyrico-lyrics-wrapper">Lele lele lelelo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lele lele lelelo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vadakkaatha punka mera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadakkaatha punka mera"/>
</div>
<div class="lyrico-lyrics-wrapper">Pooparikkaa pokilaamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pooparikkaa pokilaamo"/>
</div>
<div class="lyrico-lyrics-wrapper">Vadakkaatha punka mera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadakkaatha punka mera"/>
</div>
<div class="lyrico-lyrics-wrapper">Vegu vega poothirikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vegu vega poothirikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pooparikkaa pokilaamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pooparikkaa pokilaamo"/>
</div>
<div class="lyrico-lyrics-wrapper">Vimenaathe pakkilammo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vimenaathe pakkilammo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thille le lele lele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thille le lele lele"/>
</div>
<div class="lyrico-lyrics-wrapper">Le le lele lo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Le le lele lo"/>
</div>
<div class="lyrico-lyrics-wrapper">Thille le lele lele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thille le lele lele"/>
</div>
<div class="lyrico-lyrics-wrapper">Lele lele lelelo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lele lele lelelo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mogaatha nera mera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mogaatha nera mera"/>
</div>
<div class="lyrico-lyrics-wrapper">Vegu vega poothirikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vegu vega poothirikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pooparikkaa pokilaamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pooparikkaa pokilaamo"/>
</div>
<div class="lyrico-lyrics-wrapper">Vimenaathe pakkilammo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vimenaathe pakkilammo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thille le lele lele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thille le lele lele"/>
</div>
<div class="lyrico-lyrics-wrapper">Le le lele lo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Le le lele lo"/>
</div>
<div class="lyrico-lyrics-wrapper">Thille le lele lele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thille le lele lele"/>
</div>
<div class="lyrico-lyrics-wrapper">Lele lele lelelo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lele lele lelelo"/>
</div>
</pre>
